/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.controleacesso.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.mail.MailSendException;

import br.com.ggas.atendimento.chamado.dominio.ChamadoHistorico;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.controleacesso.ControladorHistoricoSenha;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.BooleanUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.controleacesso.SubRelatorioPapelVO;
import br.com.ggas.web.controleacesso.UsuarioVO;

/**
 * Controlador Usuario Implementação
 *
 */
class ControladorUsuarioImpl extends ControladorNegocioImpl implements ControladorUsuario {

	private static final String FROM = " from ";

	private static final String WHERE = " where ";

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final String EMAIL_ASSUNTO = "Senha GGAS";

	private static final String EMAIL_MENSAGEM_CONTEUDO = "Sua senha é: ";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Usuario.BEAN_ID_USUARIO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario #criarPapel()
	 */
	@Override
	public Papel criarPapel() {

		return (Papel) ServiceLocator.getInstancia().getBeanPorID(Papel.BEAN_ID_PAPEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Usuario.BEAN_ID_USUARIO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario
	 * #buscar(java.lang.String)
	 */
	@Override
	public Usuario buscar(String login) throws NegocioException {

		Criteria criteria = getCriteria().add(Restrictions.eq("login", login));

		criteria.setFetchMode("papeis", FetchMode.JOIN);
		criteria.setFetchMode("funcionario", FetchMode.JOIN);

		return (Usuario) criteria.uniqueResult();
	}

	/**
	 * Busca um usuário no banco a partir do seu login ldap
	 * @param usuario nome de usuário ldap
	 *
	 * SupressWarnings: Não necessário criar constante nesse caso uma vez que trata-se de consulta de banco
	 * @return retorna a {@link Usuario} correspondente
	 */
	@SuppressWarnings("squid:S1192")
	public Usuario buscarUsuarioBancoPorUsuarioLDAP(String usuario) {
		Criteria criteria = getCriteria().add(Restrictions.eq("usuarioDominio", usuario));

		criteria.setFetchMode("papeis", FetchMode.JOIN);
		criteria.setFetchMode("funcionario", FetchMode.JOIN);

		return (Usuario) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl
	 * #preAtualizacao(br.com .ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Usuario usuario = (Usuario) entidadeNegocio;

		verificarUsuarioJaExistente(usuario);
		this.criptografarSenha(usuario);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario#atualizarUsuario(br.com.ggas.controleacesso.Usuario)
	 */
	@Override
	public void atualizarUsuario(Usuario usuario) throws NegocioException, ConcorrenciaException {

		validarUsuarioDominio(usuario);
		getHibernateTemplate().getSessionFactory().getCurrentSession().clear();
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(usuario);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl
	 * #preInsercao(br.com.ggas .geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Usuario usuario = (Usuario) entidadeNegocio;

		verificarUsuarioJaExistente(usuario);
		this.criptografarSenha(usuario);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario#verificarEmailValido(java.lang.String)
	 */
	@Override
	public void verificarEmailValido(String email) throws NegocioException {

		if (!StringUtils.isEmpty(email) && !Util.validarDominio(email, Constantes.EXPRESSAO_REGULAR_EMAIL)) {
			throw new NegocioException(ERRO_NEGOCIO_VALOR, Usuario.USUARIO_ROTULO_EMAIL);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario
	 * #consultarPapeisUsuario()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Papel> consultarPapeisUsuario() throws NegocioException {


		Criteria criteria = createCriteria(getClasseEntidadePapel());
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
		return criteria.list();
	}

	private Class<?> getClasseEntidadePapel() {

		return ServiceLocator.getInstancia().getClassPorID(Papel.BEAN_ID_PAPEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario #obterPapel(long)
	 */
	@Override
	public Papel obterPapel(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Papel) super.obter(chavePrimaria, getClasseEntidadePapel(), propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario #obterPapel(long)
	 */
	@Override
	public Papel obterPapel(long chavePrimaria) throws NegocioException {

		return (Papel) super.obter(chavePrimaria, getClasseEntidadePapel());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario
	 * #consultarPapeis(java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Papel> consultarPapeis(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadePapel());

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if (!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricao, MatchMode.EXACT));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario
	 * #consultarUsuarios(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Usuario> consultarUsuarios(
					Map<String, Object> filtro, String... propriedadesLazy)
									throws NegocioException {

		Criteria criteria = getCriteria();

		for (int i = 0; i < propriedadesLazy.length; i++) {
			criteria.setFetchMode(propriedadesLazy[i], FetchMode.JOIN);
		}

		if (filtro != null) {
			String nome = (String) filtro.get("nome");
			if (!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.ilike("nome", nome.replace("*", "%")));
			}
			String email = (String) filtro.get("email");
			if (!StringUtils.isEmpty(email)) {
				criteria.add(Restrictions.ilike("email", email.replace("*", "%")));
			}

			Long funcionario = (Long) filtro.get("funcionario");
			if (funcionario != null) {
				criteria.setFetchMode("funcionario", FetchMode.JOIN);
				criteria.add(Restrictions.eq("funcionario.chavePrimaria", funcionario));
			}

			Long[] funcionarios = (Long[]) filtro.get("funcionarios");
			if (funcionarios != null && funcionarios.length > 0) {
				criteria.setFetchMode("funcionario", FetchMode.JOIN);
				criteria.add(Restrictions.in("funcionario.chavePrimaria", funcionarios));
			}

			Long[] chaves = (Long[]) filtro.get("chavesPapeis");
			if (chaves != null && chaves.length > 0) {
				criteria.setFetchMode("papeis", FetchMode.JOIN);
				Criteria novaCriteria = criteria.createCriteria("papeis");
				novaCriteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chaves));
			}
			String login = (String) filtro.get("login");
			if (!StringUtils.isEmpty(login)) {
				criteria.add(Restrictions.ilike("login", login.replace("*", "%")));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

		}

		criteria.addOrder(Order.asc("login"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario
	 * #consultarUsuario(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Usuario> consultarUsuario(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if (filtro != null) {
			String nome = (String) filtro.get("nome");
			if (!StringUtils.isEmpty(nome)) {
				criteria.createAlias("funcionario", "funcionario");
				criteria.add(Restrictions.ilike("funcionario.nome", Util.formatarTextoConsulta(nome)));
			}

			Long[] chaves = (Long[]) filtro.get("chavesPapeis");
			if (chaves != null && chaves.length > 0) {
				criteria.setFetchMode("papeis", FetchMode.JOIN);
				Criteria novaCriteria = criteria.createCriteria("papeis");
				novaCriteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chaves));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

		}
		criteria.setFetchMode("funcionario", FetchMode.JOIN);
		return criteria.list();
	}

	/**
	 * Criptografar senha.
	 *
	 * @param usuario
	 *            the usuario
	 */
	private void criptografarSenha(Usuario usuario) {

		String senhaConfirmada = usuario.getSenhaConfirmada();
		if (senhaConfirmada != null && senhaConfirmada.length() > 0) {
			String senhaCriptografada = Util.criptografarSenha(senhaConfirmada, senhaConfirmada, Constantes.HASH_CRIPTOGRAFIA);
			usuario.setSenha(senhaCriptografada);
		}
	}

	/**
	 * Verificar usuario ja existente.
	 *
	 * @param usuario
	 *            the usuario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarUsuarioJaExistente(Usuario usuario) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count (*) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" login = ?");
		if (usuario.getChavePrimaria() > 0) {
			hql.append(" and chavePrimaria != ?");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, usuario.getLogin());
		if (usuario.getChavePrimaria() > 0) {
			query.setLong(1, usuario.getChavePrimaria());
		}

		Long quantidadeUsuarios = (Long) query.uniqueResult();

		if (quantidadeUsuarios > 0) {
			throw new NegocioException(ControladorUsuario.ERRO_NEGOCIO_USUARIO_EXISTENTE, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario#verificarUsuarioParaExclusao(java.lang.Long)
	 */
	@Override
	public void verificarUsuarioParaExclusao(Long chavePrimaria) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count (*) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimaria);

		Long quantidadeUsuarios = (Long) query.uniqueResult();

		if (quantidadeUsuarios == 0) {
			throw new NegocioException(ControladorUsuario.ERRO_NEGOCIO_USUARIO_INEXISTENTE, true);
		}
	}

	@Override
	public void validarIncluirControleAcessoFunc(FuncionarioImpl funcionario) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;
		Long idEmpresa = null;
		Long idUnidadeOrganizacional = null;
		String login = null;
		Long idPapelSelecionado = null;
		Long idFuncionario = null;

		if(funcionario.getUsuario() != null) {
			idPapelSelecionado =  (long) funcionario.getUsuario().getPapeis().size();
		}

		if(funcionario.getUsuario() != null && funcionario.getUsuario().getFuncionario() != null) {
			idFuncionario = funcionario.getChavePrimaria();
		}

		if(funcionario.getEmpresa() != null) {
			idEmpresa = funcionario.getEmpresa().getChavePrimaria();
		}

		if(funcionario.getUnidadeOrganizacional() != null ) {
			idUnidadeOrganizacional = funcionario.getUnidadeOrganizacional().getChavePrimaria();
		}
		if(funcionario.getUsuario() != null) {
			login = funcionario.getUsuario().getLogin();
		}

		String email = funcionario.getEmail();

		if (idEmpresa == null || idEmpresa < 0) {
			stringBuilder.append("Empresa");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (idUnidadeOrganizacional == null || idUnidadeOrganizacional < 0) {
			stringBuilder.append("Unidade Organizacional");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (idFuncionario == null) {
			stringBuilder.append("Funcionário");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (login == null || StringUtils.isEmpty(login)) {
			stringBuilder.append("Login");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (email == null || StringUtils.isEmpty(email)) {
			stringBuilder.append("Email");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			verificarEmailValido(email);
		}

		if (idPapelSelecionado == null || idPapelSelecionado == 0) {
			stringBuilder.append("Perfil Cadastrado");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();
		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		if (erros != null && !erros.isEmpty()) {
			throw new NegocioException(erros);
		}
	}

	/**
	 * Enviar email.
	 *
	 * @param remetente
	 *            - {@link String}
	 * @param destinatario
	 *            - {@link String}
	 * @param assunto
	 *            - {@link String}
	 * @param conteudoEmail
	 *            - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	@Override
	public void enviarEmail(String remetente, String destinatario, String assunto, String conteudoEmail)
			throws NegocioException {

		boolean alertaEmail = false;
		try {
			this.enviarEmailFunc(remetente, destinatario, assunto, conteudoEmail);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			if (e.getClass().equals(MailSendException.class)) {
				alertaEmail = true;
			}
		}
		if (alertaEmail) {
			throw new NegocioException(Util.getMensagens().getString(Constantes.ERRO_EMAIL_NAO_ENVIADO));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario#enviarEmailFunc(java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void enviarEmailFunc(String remetente, String destinatario, String assunto, String conteudoEmail) throws GGASException {

		ParametroSistema parametroServidorEmailConfigurado =
						Fachada.getInstancia().obterParametroPorCodigo(
										Constantes.PARAMETRO_SERVIDOR_EMAIL_CONFIGURADO);

		if (parametroServidorEmailConfigurado != null
						&& BooleanUtil.converterStringCharParaBooleano(
										parametroServidorEmailConfigurado.getValor())) {
			getJavaMailUtil().enviar(remetente, destinatario,
							assunto, conteudoEmail);
		}
	}

	private JavaMailUtil getJavaMailUtil() {

		return (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario
	 * #recuperarSenha(java.lang.String, br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public Boolean recuperarSenha(String login, DadosAuditoria dadosAuditoria) throws GGASException {

		Boolean retorno = Boolean.FALSE;

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorHistoricoSenha controladorHistoricoSenha = (ControladorHistoricoSenha) ServiceLocator.getInstancia().getBeanPorID(
						ControladorHistoricoSenha.BEAN_ID_CONTROLADOR_HISTORICO_SENHA);

		ParametroSistema parametroSistema = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);

		Usuario usuarioAtualizar = this.buscar(login);

		if (usuarioAtualizar != null) {
			String novaSenha = this.gerarNovaSenha(login);

			String senhaCriptografada = Util.criptografarSenha(novaSenha, novaSenha, Constantes.HASH_CRIPTOGRAFIA);

			usuarioAtualizar.setTentativasSenhaErrada(0);
			usuarioAtualizar.setSenhaExpirada(Boolean.FALSE);
			usuarioAtualizar.setSenha(senhaCriptografada);
			usuarioAtualizar.setDadosAuditoria(dadosAuditoria);
			usuarioAtualizar.setPrimeiroAcesso(Boolean.TRUE);
			usuarioAtualizar.setDataCriacaoSenha(DataUtil.gerarDataHmsZerados(new Date()));
			this.atualizar(usuarioAtualizar);

			controladorHistoricoSenha.inserirHistoricoSenha(usuarioAtualizar);

			if (usuarioAtualizar.getFuncionario() != null && usuarioAtualizar.getFuncionario().getEmail() != null) {
				this.enviarEmailFunc(parametroSistema.getValor(), usuarioAtualizar.getFuncionario().getEmail(), this.EMAIL_ASSUNTO,
								this.EMAIL_MENSAGEM_CONTEUDO.concat(novaSenha));
			}

			retorno = Boolean.TRUE;
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_LOGIN_INVALIDO, true);
		}

		return retorno;
	}

	/**
	 * Gerar nova senha.
	 *
	 * @param login
	 *            the login
	 * @return the string
	 */
	private String gerarNovaSenha(String login) {

		String senhaCriptografada = Util.criptografarSenha(login, String.valueOf(Calendar.getInstance().getTimeInMillis()),
						Constantes.HASH_CRIPTOGRAFIA);
		senhaCriptografada = senhaCriptografada.substring(0, 8);

		return senhaCriptografada;
	}

	@Override
	public void validarRecuperarSenha(String login) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (login == null || StringUtils.isEmpty(login)) {
			stringBuilder.append("Login");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();
		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		if (!erros.isEmpty()) {
			throw new NegocioException(erros);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario#alterarSenha(java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public Boolean alterarSenha(String login, String senhaAtual, String novaSenha, String confirmacaoNovaSenha,
					DadosAuditoria dadosAuditoria) throws GGASException {

		Boolean senhaAlterada = Boolean.FALSE;

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorHistoricoSenha controladorHistoricoSenha = (ControladorHistoricoSenha) ServiceLocator.getInstancia().getBeanPorID(
						ControladorHistoricoSenha.BEAN_ID_CONTROLADOR_HISTORICO_SENHA);

		ParametroSistema parametroSistema = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);

		Usuario usuario = this.buscar(login);
		if (usuario != null) {

			this.validarMudancaSenha(senhaAtual, novaSenha, confirmacaoNovaSenha);
			this.validarRegrasSenha(novaSenha);

			String senhaCriptografada = Util.criptografarSenha(novaSenha, novaSenha, Constantes.HASH_CRIPTOGRAFIA);

			this.validarSenhaAtual(usuario, senhaCriptografada);

			controladorHistoricoSenha.verificarHistoricoSenha(usuario.getChavePrimaria(), senhaCriptografada);

			usuario.setPrimeiroAcesso(Boolean.FALSE);
			usuario.setTentativasSenhaErrada(0);
			usuario.setSenhaExpirada(Boolean.FALSE);
			usuario.setSenha(senhaCriptografada);
			usuario.setDadosAuditoria(dadosAuditoria);
			usuario.setDataCriacaoSenha(DataUtil.gerarDataHmsZerados(new Date()));

			this.atualizar(usuario);

			controladorHistoricoSenha.inserirHistoricoSenha(usuario);
			if (usuario.getFuncionario() != null && usuario.getFuncionario().getEmail() != null) {
				this.enviarEmailFunc(parametroSistema.getValor(), usuario.getFuncionario().getEmail(), this.EMAIL_ASSUNTO,
								this.EMAIL_MENSAGEM_CONTEUDO.concat(novaSenha));
			}
			senhaAlterada = Boolean.TRUE;
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_LOGIN_INVALIDO, true);
		}

		return senhaAlterada;
	}

	/**
	 * Validar mudanca senha.
	 *
	 * @param senhaAtual
	 *            the senha atual
	 * @param novaSenha
	 *            the nova senha
	 * @param confirmacaoNovaSenha
	 *            the confirmacao nova senha
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarMudancaSenha(String senhaAtual, String novaSenha, String confirmacaoNovaSenha) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (senhaAtual == null || StringUtils.isEmpty(senhaAtual)) {
			stringBuilder.append("Senha Atual");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (novaSenha == null || StringUtils.isEmpty(novaSenha)) {
			stringBuilder.append("Nova Senha");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (confirmacaoNovaSenha == null || StringUtils.isEmpty(confirmacaoNovaSenha)) {
			stringBuilder.append("Confirmação da Nova Senha");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();
		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		if (!erros.isEmpty()) {
			throw new NegocioException(erros);
		}

		if (novaSenha != null && !novaSenha.equals(confirmacaoNovaSenha)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SENHA_NOVA_CONFIRMACAO_DIFERENTES, true);
		} else if (novaSenha != null && novaSenha.equals(senhaAtual)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SENHA_ATUAL_NOVA_IGUAIS, true);
		}

	}

	/**
	 * Validar regras senha.
	 *
	 * @param novaSenha
	 *            the nova senha
	 * @throws GGASException
	 *             the GGAS exception
	 */
	/*
	 * Método para verificar se a senha está dentro das regras definidas nos
	 * parametros. Regras: Tamanho mínimo e máximo, Exigir caracteres Maiúsculos
	 * , Minúsculos e numéricos.
	 */
	private void validarRegrasSenha(String novaSenha) throws GGASException {

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		Integer senhaCaractereNumerico = Integer.valueOf(controladorParametroSistema.obterParametroPorCodigo(
						Constantes.PARAMETRO_SENHA_CODIGO_CARACTERE_NUMERO).getValor());

		Integer senhaCaractereMinusculo = Integer.valueOf(controladorParametroSistema.obterParametroPorCodigo(
						Constantes.PARAMETRO_SENHA_CODIGO_CARACTERE_MINUSCULO).getValor());

		Integer senhaCaractereMaisculo = Integer.valueOf(controladorParametroSistema.obterParametroPorCodigo(
						Constantes.PARAMETRO_SENHA_CODIGO_CARACTERE_MAIUSCULO).getValor());

		Integer tamanhoMinhimoSenha = Integer.valueOf(controladorParametroSistema.obterParametroPorCodigo(
						Constantes.PARAMETRO_SENHA_TAMANHO_MINIMO_SENHA).getValor());

		Integer tamanhoMaximoSenha = Integer.valueOf(controladorParametroSistema.obterParametroPorCodigo(
						Constantes.PARAMETRO_SENHA_TAMANHO_MAXIMO_SENHA).getValor());

		if (senhaCaractereNumerico != null && senhaCaractereNumerico > 0) {
			String padraoNumerico = "\\d";
			if (!Pattern.compile(padraoNumerico).matcher(novaSenha).find()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_SENHA_NAO_CONTEM_NUMERO, true);
			}
		}

		if (senhaCaractereMaisculo != null && senhaCaractereMaisculo > 0) {
			String padraoMaiusculas = "[A-Z]";
			if (!Pattern.compile(padraoMaiusculas).matcher(novaSenha).find()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_SENHA_NAO_CONTEM_MAIUSCULAS, true);
			}

		}

		if (senhaCaractereMinusculo != null && senhaCaractereMinusculo > 0) {
			String padraoMinusculas = "[a-z]";
			if (!Pattern.compile(padraoMinusculas).matcher(novaSenha).find()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_SENHA_NAO_CONTEM_MINUSCULAS, true);
			}
		}

		if ((novaSenha.length() < tamanhoMinhimoSenha) || (novaSenha.length() > tamanhoMaximoSenha)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SENHA_TAMANHO, new Object[] {tamanhoMinhimoSenha, tamanhoMaximoSenha});
		}

	}

	/**
	 * Validar senha atual.
	 *
	 * @param usuario
	 *            the usuario
	 * @param senhaCriptografada
	 *            the senha criptografada
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void validarSenhaAtual(Usuario usuario, String senhaCriptografada) throws GGASException {

		if (usuario.getSenha().equals(senhaCriptografada)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SENHA_ATUAL_INVALIDA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario
	 * #verificarSenhaExpirada(br.com.ggas. controleacesso.Usuario)
	 */
	@Override
	public Boolean verificarSenhaExpirada(Usuario usuario) throws GGASException {

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		Integer parametroPeriodicidadeSenha = Integer.valueOf(controladorParametroSistema.obterParametroPorCodigo(
						Constantes.PARAMETRO_PERIODICIDADE_SENHA_RENOVACAO).getValor());

		Boolean senhaExpirada = Boolean.FALSE;
		Integer periodicidade = 0;

		/*
		 * Se a periodicidade tiver setada no cadastro do usuário, utilizar a do
		 * cadastro. Senão, utilizar do parametro de sistema
		 */
		if (usuario.getPeriodicidadeSenha() != null && usuario.getPeriodicidadeSenha() > 0) {
			periodicidade = usuario.getPeriodicidadeSenha();
		} else if (parametroPeriodicidadeSenha != null && parametroPeriodicidadeSenha > 0) {
			periodicidade = parametroPeriodicidadeSenha;
		}

		if (periodicidade > 0 && usuario.getDataCriacaoSenha() != null) {
			Date dataAtual = DataUtil.gerarDataHmsZerados(new Date());
			DateTime dataLimite = new DateTime(usuario.getDataCriacaoSenha());

			dataLimite = dataLimite.plusDays(periodicidade);

			if (dataAtual.after(dataLimite.toDate())) {
				senhaExpirada = Boolean.TRUE;
			}
		}

		return senhaExpirada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario #obterUsuario(long)
	 */
	@Override
	public Usuario obterUsuario(long chavePrimaria) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" usuario ");
		hql.append(" left join fetch usuario.papeis as papeis ");
		hql.append(" left join fetch papeis.permissoes as permissoes ");
		hql.append(" left join fetch usuario.funcionario as funcionario ");
		hql.append(WHERE);
		hql.append(" usuario.chavePrimaria = :chaveUser");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveUser", chavePrimaria);

		return (Usuario) query.uniqueResult();

	}

	/**
	 * Atualizar.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void atualizar(Long chavePrimaria) throws NegocioException {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario#gerarRelatorio(java.util.Map, br.com.ggas.controleacesso.Usuario)
	 */
	@Override
	public byte[] gerarRelatorio(Map<String, Object> filtro, Usuario usuario) throws NegocioException, FormatoInvalidoException {

		Map<String, Object> parametros = new HashMap<>();
		if (this.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(this.obterEmpresaPrincipal().getChavePrimaria()));
		}

		Collection<UsuarioVO> usuarioVO = new ArrayList<>();
		byte[] relatorioUsuario;
		String tipoExibicao = "relatorioUsuario.jasper";

		usuarioVO = montaRelatorioUsuario(usuarioVO, usuario);

		if (usuarioVO != null && !usuarioVO.isEmpty()) {
			relatorioUsuario = RelatorioUtil.gerarRelatorio(usuarioVO, parametros, tipoExibicao,
							(FormatoImpressao) filtro.get(FORMATO_IMPRESSAO));
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return relatorioUsuario;
	}

	/**
	 * Obter empresa principal.
	 *
	 * @return the empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Empresa obterEmpresaPrincipal() throws NegocioException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Map<String, Object> filtro = new HashMap<>();
		filtro.put("principal", Boolean.TRUE);
		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);

		Empresa empresa = null;
		if ((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
			empresa = ((List<Empresa>) listaEmpresas).get(0);
		}

		return empresa;
	}

	/**
	 * Monta relatorio usuario.
	 *
	 * @param collUsuarioVO
	 *            the coll usuario vo
	 * @param usuario
	 *            the usuario
	 * @return the collection
	 */
	private Collection<UsuarioVO> montaRelatorioUsuario(Collection<UsuarioVO> collUsuarioVO, Usuario usuario) {

		UsuarioVO usuarioVO = new UsuarioVO();
		Collection<SubRelatorioPapelVO> collSubRelatorioPapelVO = new HashSet<>();

		usuarioVO.setEmpresa(usuario.getFuncionario().getEmpresa().getCliente().getNome());
		usuarioVO.setUnidadeOrganizacional(usuario.getFuncionario().getUnidadeOrganizacional().getDescricao());
		usuarioVO.setNomeFuncionario(usuario.getFuncionario().getNome());
		usuarioVO.setLogin(usuario.getLogin());
		usuarioVO.setEmail(usuario.getFuncionario().getEmail());

		if (usuario.isSenhaExpirada()) {
			usuarioVO.setSenhaExpirada("Sim");
		} else {
			usuarioVO.setSenhaExpirada("Não");
		}

		if (usuario.getPeriodicidadeSenha() != null) {
			usuarioVO.setPeriodicidadeSenha(String.valueOf(usuario.getPeriodicidadeSenha()));
		} else {
			usuarioVO.setPeriodicidadeSenha("");
		}

		for (Papel papel : usuario.getPapeis()) {
			SubRelatorioPapelVO subRelatorioPapelVO = new SubRelatorioPapelVO();
			subRelatorioPapelVO.setDescricao(papel.getDescricao());
			collSubRelatorioPapelVO.add(subRelatorioPapelVO);
		}
		usuarioVO.setSubRelatorioPapelVO(collSubRelatorioPapelVO);

		collUsuarioVO.add(usuarioVO);

		return collUsuarioVO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario#removerControleAcessoFunc(java.lang.Long[], br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerControleAcessoFunc(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		for (Long chavePrimaria : chavesPrimarias) {
			Collection<ChamadoHistorico> lista = this.obterChamadoHistoricoUsuario(chavePrimaria);
			if (lista != null && !lista.isEmpty()) {
				throw new NegocioException(Constantes.ERRO_NAO_REMOVER_USUARIO, true);
			}

			Usuario usuario = this.obterUsuario(chavePrimaria);

			usuario.getPapeis().clear();
			usuario.setPapeis(null);
			this.atualizarUsuario(usuario);

		}

	}

	/**
	 * Obter chamado historico usuario.
	 *
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	private Collection<ChamadoHistorico> obterChamadoHistoricoUsuario(Long chavePrimaria) {

		Criteria criteria = createCriteria(ChamadoHistorico.class);
		criteria.createAlias("usuario", "usuario", Criteria.LEFT_JOIN);
		criteria.add(Restrictions.eq("usuario.chavePrimaria", chavePrimaria));
		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorUsuario#removerUsuario(java.lang.Long[])
	 */
	@Override
	public void removerUsuario(Long[] chavesPrimarias) throws NegocioException {

		for (Long id : chavesPrimarias) {
			Usuario u = this.obterUsuario(id);
			super.remover(u);

		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.controleacesso.ControladorUsuario#listarAdministradoresAtendimento()
	 */
	@Override
	public Collection<Usuario> listarAdministradoresAtendimento() throws NegocioException {
		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.createAlias("funcionario", "funcionario").add(Restrictions.eq("administradorAtendimento", true));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.controleacesso.ControladorUsuario#obterUsuarioPorLoginDominio(java.lang.String)
	 */
	@Override
	public Usuario obterUsuarioPorLoginDominio(String loginDominio) {
		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode("papeis", FetchMode.JOIN);
		criteria.setFetchMode("funcionario", FetchMode.JOIN);

		criteria.add(Restrictions.eq("usuarioDominio", loginDominio));

		return (Usuario) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#inserir(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public long inserir(EntidadeNegocio entidadeNegocio) throws NegocioException {
		validarUsuarioDominio(entidadeNegocio);
		return super.inserir(entidadeNegocio);
	}

	/**
	 * Valida usuario dominio unico
	 *
	 * @param entidadeNegocio
	 * @throws NegocioException
	 */
	private void validarUsuarioDominio(EntidadeNegocio entidadeNegocio) throws NegocioException {
		Usuario usuario = (Usuario) entidadeNegocio;
		if (StringUtils.isNotEmpty(usuario.getUsuarioDominio())) {
			Criteria criteria = createCriteria(getClasseEntidade());
			criteria.add(Restrictions.eq("usuarioDominio", usuario.getUsuarioDominio()));
			criteria.add(Restrictions.ne("chavePrimaria", usuario.getChavePrimaria()));
			if (criteria.uniqueResult() != null) {
				throw new NegocioException(ControladorUsuario.ERRO_NEGOCIO_USUARIO_DOMINIO_EXISTENTE, true);
			}
		}
	}

	@Override
	public Usuario autenticarUsuarioProjetado(String login, String senha) {
		Usuario usuarioAutenticado = null;
		try {
			StringBuilder hql = new StringBuilder("SELECT new UsuarioImpl(u.chavePrimaria, u.login, u.senha, u.token) ");
			hql.append(" FROM UsuarioImpl u ");
			hql.append(" WHERE u.login = :login ");
			hql.append(" AND u.senha = :senha ");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameter("login", login);
			query.setParameter("senha", Util.criptografarSenha(senha, senha, Constantes.HASH_CRIPTOGRAFIA));

			usuarioAutenticado = (Usuario) query.uniqueResult();
			return usuarioAutenticado;
		} catch (Exception e) {
			logger.error("Erro durante a autenticação do usuário", e);
			return usuarioAutenticado;
		}
	}

	@Override
	public void atualizarToken(Long chavePrimaria, String token) {
		try {
			Usuario usuarioAtualizar = this.obterUsuario(chavePrimaria);
			usuarioAtualizar.setToken(token);
			this.atualizar(usuarioAtualizar);
		} catch (Exception e) {
			logger.error("Não foi possível atualizar o token do usuário", e);
		}
	}

	@Override
	public Usuario invalidarToken(String token) {
		try {
			Criteria criteria = getCriteria().add(Restrictions.eq("token", token));

			Usuario usuario = (Usuario) criteria.uniqueResult();
			if (usuario != null) {
				usuario.setToken(null);
				this.atualizarUsuario(usuario);
			}
			return usuario;
		} catch (Exception e) {
			logger.error("Não foi possível atualizar token do usuário", e);
			return null;
		}

	}

}
