/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.controleacesso.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.ControladorPapel;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.ModuloFuncOperacoesVO;
import br.com.ggas.controleacesso.Papel;
import br.com.ggas.controleacesso.Permissao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.web.controleacesso.PapelVO;
import br.com.ggas.web.controleacesso.PermissoesVO;

/**
 * 
 *
 */
class ControladorPapelImpl extends ControladorNegocioImpl implements ControladorPapel {

	private static final String SELECT = " select ";

	private static final String FROM = " from ";

	private static final String WHERE = " where ";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static ServiceLocator serviceLocator = ServiceLocator.getInstancia();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Papel.BEAN_ID_PAPEL);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorPapel
	 * #criarPermissao()
	 */
	@Override
	public EntidadeNegocio criarPermissao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Permissao.BEAN_ID_PERMISSAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Papel.BEAN_ID_PAPEL);
	}

	public Class<?> getClasseEntidadeUsuario() {

		return ServiceLocator.getInstancia().getClassPorID(Usuario.BEAN_ID_USUARIO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Papel papel = (Papel) entidadeNegocio;
		validarPapelExistente(papel);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Papel papel = (Papel) entidadeNegocio;
		validarPapelExistente(papel);
	}

	/**
	 * Validar papel existente.
	 * 
	 * @param papel
	 *            the papel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarPapelExistente(Papel papel) throws NegocioException {

		Map<String, Object> filtro = new HashMap<>();
		filtro.put(DESCRICAO, papel.getDescricao());
		List<Papel> papeis = (List<Papel>) this.getControladorUsuario().consultarPapeis(filtro);

		if(!papeis.isEmpty()) {
			Papel papelExistente = papeis.get(0);

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(papelExistente);

			if(papelExistente.getChavePrimaria() != papel.getChavePrimaria()) {
				throw new NegocioException(ControladorPapel.ERRO_NEGOCIO_PAPEL_EXISTENTE, true);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preRemocao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Papel papel = (Papel) entidadeNegocio;
		verificarSePapelPossuiRelacionamento(papel);
	}

	/**
	 * Verificar se papel possui relacionamento.
	 * 
	 * @param papel
	 *            the papel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarSePapelPossuiRelacionamento(Papel papel) throws NegocioException {

		Map<String, Object> filtro = null;
		Collection<Usuario> usuarios = this.getControladorUsuario().consultarUsuarios(filtro);
		Collection<Papel> papeis;
		for (Usuario usuario : usuarios) {
			papeis = usuario.getPapeis();
			for (Papel papelUsuario : papeis) {
				if(papelUsuario.getChavePrimaria() == papel.getChavePrimaria()) {
					throw new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, true);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorPapel
	 * #removerPapeis(java.lang.Long[])
	 */
	@Override
	public void removerPapeis(Long[] chavesPrimarias) throws NegocioException {


		if((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {

			Map<String, Object> filtro = new HashMap<>();
			filtro.put("chavesPrimarias", chavesPrimarias);
			Collection<Papel> papeis = this.getControladorUsuario().consultarPapeis(filtro);
			if((papeis != null) && (!papeis.isEmpty())) {
				for (Papel papel : papeis) {
					removerPermissoesDoPapel(papel);
					super.remover(papel);
				}
			}
		}
	}

	private void removerPermissoesDoPapel(Papel papel) {
		for (Permissao permissao : papel.getPermissoes()) {
			removerPermissaoSistema(permissao);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorPapel#validarRemoverPapeis(java.lang.Long[])
	 */
	@Override
	public void validarRemoverPapeis(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}
	}

	private ControladorUsuario getControladorUsuario() {

		return (ControladorUsuario) serviceLocator.getControladorNegocio(ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorPapel#consultarPapel(java.util.Map)
	 */
	@Override
	public Collection<Papel> consultarPapel(Map<String, Object> filtro) {

		Criteria criteria = this.createCriteria(getClasseEntidade());

		if(filtro != null) {
			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if((chavePrimaria != null) && (chavePrimaria > 0)) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			String nome = (String) filtro.get(DESCRICAO);
			if(!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.like(DESCRICAO, nome, MatchMode.ANYWHERE));
			}

			String habilitado = (String) filtro.get(HABILITADO);
			if(!StringUtils.isEmpty(habilitado)) {
				criteria.add(Restrictions.eq(HABILITADO, Boolean.valueOf(habilitado)));
			}
		}

		criteria.addOrder(Order.asc(DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorPapel#removerPermissaoSistema(br.com.ggas.controleacesso.Permissao)
	 */
	@Override
	public void removerPermissaoSistema(Permissao permissaoTmp) {

		Permissao permissao = permissaoTmp;
		Permissao sessionPermissao = (Permissao) getHibernateTemplate().getSessionFactory().getCurrentSession()
						.get(PermissaoImpl.class, permissao.getChavePrimaria());

		permissao = null;
		getHibernateTemplate().getSessionFactory().getCurrentSession().evict(sessionPermissao);
		getHibernateTemplate().getSessionFactory().getCurrentSession().clear();
		getHibernateTemplate().getSessionFactory().getCurrentSession().delete(sessionPermissao);
		getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorPapel
	 * #
	 * atualizarPapel(br.com.ggas.controleacesso.Papel
	 * )
	 */
	@Override
	public void atualizarPapel(Papel papel) throws GGASException {

		Collection<Permissao> listaPermissoes = this.consultarPermissoesPorPapel(papel.getChavePrimaria());
		ControladorAcesso controladorAcesso = (ControladorAcesso) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);
		if(listaPermissoes != null) {
			for (Permissao permissao : listaPermissoes) {
				permissao.setDadosAuditoria(papel.getDadosAuditoria());
				permissao.setPapel(null);

				controladorAcesso.remover(permissao);
			}
		}

		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		validarDadosEntidade(papel);
		validarVersaoEntidade(papel, PapelImpl.class);
		preAtualizacao(papel);
		papel.incrementarVersao();
		papel.setUltimaAlteracao(Calendar.getInstance().getTime());
		getHibernateTemplate().getSessionFactory().getCurrentSession().clear();
		getHibernateTemplate().getSessionFactory().getCurrentSession().merge(papel);
		posAtualizacao(papel);
		getSession().flush();

	}

	/**
	 * Método responsável por recuperar as
	 * permissões de um papel.
	 * 
	 * @param chavePrimaria
	 *            chave do papel
	 * @return as permissões do papel
	 */
	private Collection<Permissao> consultarPermissoesPorPapel(long chavePrimaria) {

		StringBuilder hql = new StringBuilder();

		hql.append(SELECT);
		hql.append(" papel.permissoes ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" papel ");
		hql.append(WHERE);
		hql.append(" papel.chavePrimaria = :chavePapel ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chavePapel", chavePrimaria);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorPapel#gerarRelatorioPapel(java.util.Map, br.com.ggas.controleacesso.Papel, java.util.List)
	 */
	@Override
	public byte[] gerarRelatorioPapel(Map<String, Object> filtro, Papel papel, List<ModuloFuncOperacoesVO> listaPermissoes)
					throws NegocioException, FormatoInvalidoException {

		Map<String, Object> parametros = new HashMap<>();
		if(this.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(this.obterEmpresaPrincipal().getChavePrimaria()));
		}

		Collection<PapelVO> papelVO = new ArrayList<>();
		byte[] relatorioUsuario;
		String tipoExibicao = "relatorioPerfil.jasper";

		papelVO = montaRelatorioUsuario(papelVO, papel, listaPermissoes);

		if(papelVO != null && !papelVO.isEmpty()) {
			relatorioUsuario = RelatorioUtil.gerarRelatorio(papelVO, parametros, tipoExibicao,
							(FormatoImpressao) filtro.get(FORMATO_IMPRESSAO));
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return relatorioUsuario;
	}

	/**
	 * Obter empresa principal.
	 * 
	 * @return the empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Empresa obterEmpresaPrincipal() throws NegocioException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Map<String, Object> filtro = new HashMap<>();
		filtro.put("principal", Boolean.TRUE);
		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);

		Empresa empresa = null;
		if((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
			empresa = ((List<Empresa>) listaEmpresas).get(0);
		}

		return empresa;
	}

	/**
	 * Monta relatorio usuario.
	 * 
	 * @param collPapelVO
	 *            the coll papel vo
	 * @param papel
	 *            the papel
	 * @param listaPermissoes
	 *            the lista permissoes
	 * @return the collection
	 */
	private Collection<PapelVO> montaRelatorioUsuario(Collection<PapelVO> collPapelVO, Papel papel,
					List<ModuloFuncOperacoesVO> listaPermissoes) {

		PapelVO papelVO = new PapelVO();
		Collection<PermissoesVO> collModuloFuncOperacoesVO = new ArrayList<>();

		if(papel.getFuncionario() != null) {
			papelVO.setResponsavel(papel.getFuncionario().getNome());
			papelVO.setEmail(papel.getFuncionario().getEmail());
		}

		papelVO.setNome(papel.getDescricao());
		
		if (papel.isHabilitado()) {
			papelVO.setStatus("Ativo");
		} else {
			papelVO.setStatus("Inativo");
		}

		papelVO.setDescricao(papel.getComplemento());

		for (ModuloFuncOperacoesVO auxModuloFuncOperacoesVO : listaPermissoes) {
			PermissoesVO permissoesVO = new PermissoesVO();

			permissoesVO.setModulo(auxModuloFuncOperacoesVO.getModulo().getDescricao());
			permissoesVO.setFuncionalidade(auxModuloFuncOperacoesVO.getFuncionalidade().getDescricao());
			permissoesVO.setOperacoes(auxModuloFuncOperacoesVO.getOperacoesFormatado());

			collModuloFuncOperacoesVO.add(permissoesVO);
		}

		papelVO.setPermissoesVO(collModuloFuncOperacoesVO);
		collPapelVO.add(papelVO);

		return collPapelVO;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorPapel
	 * #obterListaPapelPorIds(java.lang.Long[])
	 */
	@Override
	public Set<Papel> obterListaPapelPorIds(Long[] listaIdsPapeis) throws NegocioException {

		Set<Papel> listaPapeis = new HashSet<>();
		for (int i = 0; i < listaIdsPapeis.length; i++) {
			Papel papel = (Papel) this.obter(listaIdsPapeis[i]);
			listaPapeis.add(papel);
		}

		return listaPapeis;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.ControladorPapel
	 * #consultarPapeisPorFuncionario(long)
	 */
	@Override
	public Collection<Papel> consultarPapeisPorFuncionario(long chaveFuncionario) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(SELECT);
		hql.append(" usuario.papeis ");
		hql.append(FROM);
		hql.append(getClasseEntidadeUsuario().getSimpleName());
		hql.append(" usuario ");
		hql.append(WHERE);
		hql.append(" usuario.funcionario.chavePrimaria = :chaveFuncionario ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chaveFuncionario", chaveFuncionario);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorPapel#consultarPapeisPorUsuario(long)
	 */
	@Override
	public Collection<Papel> consultarPapeisPorUsuario(long chaveUsuario) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(SELECT);
		hql.append(" usuario.papeis ");
		hql.append(FROM);
		hql.append(getClasseEntidadeUsuario().getSimpleName());
		hql.append(" usuario ");
		hql.append(WHERE);
		hql.append(" usuario.chavePrimaria = :chaveUsuario ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chaveUsuario", chaveUsuario);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.ControladorPapel#atualizarPapel(br.com.ggas.controleacesso.Papel, br.com.ggas.controleacesso.Usuario,
	 * java.util.Set)
	 */
	@Override
	public void atualizarPapel(Papel papel, Usuario usuario, Set<Permissao> listaNova) throws ConcorrenciaException, NegocioException {

		Collection<Papel> papeis = usuario.getPapeis();

		for (Papel p : papeis) {
			if (p.getChavePrimaria() == papel.getChavePrimaria()) {
				throw new NegocioException(ControladorPapel.ERRO_PAPEL_EXISTE_USUARIO_LOGADO, true);
			}
		}
		Papel papelDB = (Papel) obter(papel.getChavePrimaria(), PapelImpl.class);

		for (Permissao permissao : papelDB.getPermissoes()) {
			removerPermissaoSistema(permissao);
		}
		// adicionar itens adicionados na tela
		Set<Permissao> listaPermissao = new HashSet<>();
		for (Permissao permissao : listaNova) {

			Permissao permissaoNew = new PermissaoImpl();

			permissaoNew.setMenu(permissao.getMenu());
			permissaoNew.setOperacao(permissao.getOperacao());
			permissaoNew.setPapel(permissao.getPapel());
			permissaoNew.setTipo(permissao.getTipo());
			permissaoNew.setUltimaAlteracao(new Date());

			listaPermissao.add(permissaoNew);

		}
		papel.setPermissoes(listaPermissao);
		// TODO validar auditoria
		getHibernateTemplate().merge(papel);
	}

}
