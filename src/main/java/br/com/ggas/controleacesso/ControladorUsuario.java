/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.controleacesso;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Classe responsável pela assinatura dos métodos relacionados ao Controlador de Usuário
 *
 */
public interface ControladorUsuario extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_USUARIO = "controladorUsuario";

	String ERRO_NEGOCIO_USUARIO_EXISTENTE = "ERRO_NEGOCIO_USUARIO_EXISTENTE";

	String ERRO_NEGOCIO_VALOR = "ERRO_NEGOCIO_VALOR";

	String ERRO_NEGOCIO_USUARIO_INEXISTENTE = "ERRO_NEGOCIO_USUARIO_INEXISTENTE";

	String ERRO_NEGOCIO_USUARIO_DOMINIO_EXISTENTE = "ERRO_NEGOCIO_USUARIO_DOMINIO_EXISTENTE";

	/**
	 * Método resposável por consultar o usuário
	 * por login.
	 * 
	 * @param login
	 *            O Login
	 * @return Um usuário
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Usuario buscar(String login) throws NegocioException;

	/**
	 * Método resposável por consultar o usuário do GGAS à partir do usuário do domínio
	 *
	 * @param usuarioLDAP
	 *            O usuário do domínio
	 * @return Um usuário
	 */
	Usuario buscarUsuarioBancoPorUsuarioLDAP(String usuarioLDAP);

	/**
	 * Método responsável por consultar todos os
	 * papéis dos usuários do sistema.
	 * 
	 * @return Uma lista de papéis.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Papel> consultarPapeisUsuario() throws NegocioException;

	/**
	 * Método responsável por obter um papel do
	 * usuário do sistema.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return Um papel do usuário do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Papel obterPapel(long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Método responsável por obter um papel do
	 * usuário do sistema.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the papel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Papel obterPapel(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por consultar os papéis
	 * dos usuários do sistema.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return uma coleção de papéis dos usuários
	 *         do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Papel> consultarPapeis(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * usuários a partir do filtro informado.
	 * 
	 * @param filtro
	 *            filtro contendo os parametros da
	 *            consulta.
	 * @param propriedadesLazy
	 * 			  propriedades para inicializar
	 * @return Uma lista de usuários.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Usuario> consultarUsuarios(
					Map<String, Object> filtro, String... propriedadesLazy) 
									throws NegocioException;

	/**
	 * Método responsável por verificar a
	 * existencia do usuário a ser removido no
	 * sistema.
	 * 
	 * @param chavePrimaria
	 *            Chave Primária do usuário do
	 *            sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void verificarUsuarioParaExclusao(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por criar um papel no
	 * sistema.
	 * 
	 * @return Um novo papel
	 */
	Papel criarPapel();

	/**
	 * Método responsável por validar os campos da
	 * tela de incluir controle de acesso ao
	 * funcionário.
	 * 
	 * @param funcionario {@link FuncionarioImpl}
	 *            
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarIncluirControleAcessoFunc(FuncionarioImpl funcionario) throws NegocioException;

	/**
	 * Método responsável por verificar se o email
	 * é valido.
	 * 
	 * @param email
	 *            the email
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void verificarEmailValido(String email) throws NegocioException;

	/**
	 * Método responsável por enviar um email ao
	 * funcionário.
	 * 
	 * @param remetente
	 *            , destinatario, assunto,
	 *            conteudoEmail
	 * @param destinatario
	 *            the destinatario
	 * @param assunto
	 *            the assunto
	 * @param conteudoEmail
	 *            the conteudo email
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void enviarEmailFunc(String remetente, String destinatario, String assunto, String conteudoEmail) throws GGASException;

	/**
	 * Método para recuperar a senha do usuário.
	 * 
	 * @param login
	 *            the login
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the boolean
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Boolean recuperarSenha(String login, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Validar a recuperação de senha.
	 * 
	 * @param login
	 *            the login
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarRecuperarSenha(String login) throws NegocioException;

	/**
	 * Método para alterar a senha do usuário.
	 * 
	 * @param login
	 *            the login
	 * @param senhaAtual
	 *            the senha atual
	 * @param novaSenha
	 *            the nova senha
	 * @param confirmacaoNovaSenha
	 *            the confirmacao nova senha
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the boolean
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Boolean alterarSenha(String login, String senhaAtual, String novaSenha, String confirmacaoNovaSenha, DadosAuditoria dadosAuditoria)
					throws GGASException;

	/**
	 * Método para validar se a senha está
	 * expirada.
	 * 
	 * @param usuario
	 *            the usuario
	 * @return the boolean
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Boolean verificarSenhaExpirada(Usuario usuario) throws GGASException;

	/**
	 * Método responsável por consultar um usuário.
	 * 
	 * @param chavePrimaria
	 *            chave do usuário
	 * @return o usuário localizado
	 */
	Usuario obterUsuario(long chavePrimaria);

	/**
	 * Método responsável por atualizar um usuário.
	 * 
	 * @param usuario
	 *            the usuario
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarUsuario(Usuario usuario) throws NegocioException, ConcorrenciaException;

	/**
	 * Gerar relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param usuario
	 *            the usuario
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	byte[] gerarRelatorio(Map<String, Object> filtro, Usuario usuario) throws NegocioException, FormatoInvalidoException;

	/**
	 * Método para consultar os usuários.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Usuario> consultarUsuario(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Remover controle acesso func.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void removerControleAcessoFunc(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException;

	/**
	 * Remover usuario.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerUsuario(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Enviar email.
	 * 
	 * @param remetente
	 *            - {@link String}
	 * @param destinatario
	 *            - {@link String}
	 * @param assunto
	 *            - {@link String}
	 * @param conteudoEmail
	 *            - {@link String}
	 * @throws NegocioException
	 *             - {@link NegocioException}
	 */
	void enviarEmail(String remetente, String destinatario, String assunto, String conteudoEmail) throws NegocioException;

	/**
	 * Lista os usuarios administradores de atendimento
	 * 
	 * @return administradores atendimento
	 * @throws NegocioException
	 * 			the negocio exception
	 */
	Collection<Usuario> listarAdministradoresAtendimento() throws NegocioException;

	/**
	 * Obtem usuario por login do dominio
	 * 
	 * @param loginDominio 
	 * 			the loginDominio
	 * @return usuario
	 */
	Usuario obterUsuarioPorLoginDominio(String loginDominio);
	
	/**
	 * Autentica Usuario Projetado
	 * 
	 * @param login 
	 * 		the login
	 * @param senha the senha
	 * @return usuario
	 */
	Usuario autenticarUsuarioProjetado(String login, String senha);
	/**
	 * Atualizar Token
	 * 
	 * @param chavePrimaria 
	 * 			the Chave Primaria
	 * @param token the token
	 * 
	 */
	void atualizarToken(Long chavePrimaria, String token);
	/**
	 * Invalidar Token
	 * 
	 * @param token 
	 * 			the token
	 * @return usuario
	 */
	Usuario invalidarToken(String token);

}
