/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.controleacesso;

import br.com.ggas.geral.Menu;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Interface responsável pelas operações referentes 
 * as operações e recursos dos módulos do sistema.
 * 
 *
 */
public interface ControladorModulo extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_MODULO = "controladorModulo";

	String ERRO_NEGOCIO_MODULO_EXISTE_DESCRICAO = "ERRO_NEGOCIO_MODULO_EXISTE_DESCRICAO";

	/**
	 * Método responsável por buscar a operação ao
	 * qual o recurso está associado.
	 * 
	 * @param recurso
	 *            O recurso acessado
	 * @return Uma operação do sistema
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Operacao> buscarOperacaoPorRecurso(String recurso) throws NegocioException;

	/**
	 * Cria uma operação.
	 * 
	 * @return uma nova operação
	 */
	EntidadeNegocio criarOperacao();

	/**
	 * Método responsável por consultar as
	 * operações batchs do sistema.
	 * 
	 * @return Uma coleção de operação.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Operacao> consultarOperacoesBatch() throws NegocioException;

	/**
	 * Cria um recurso.
	 * 
	 * @return Um novo recurso
	 */
	EntidadeNegocio criarRecurso();

	/**
	 * Método responsável por consultar as
	 * operações do sistema.
	 * 
	 * @param chaveModulo
	 *            Chave primária do módulo.
	 * @return coleção de operações do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Operacao> consultarOperacoes(Long chaveModulo) throws NegocioException;

	/**
	 * Método responsável por consultar as
	 * operações do sistema de acordo com o
	 * filtro.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @return coleção de operações do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Operacao> consultarOperacoes(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * recursos do sistema através da operação.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return coleção de recursos do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Recurso> consultarRecursos(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar os módulos
	 * do sistema.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return coleção de módulos do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Modulo> consultarModulos(Map<String, String> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar módulos do
	 * sistema pelas chaves primárias.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos módulos.
	 * @return Coleção de módulos do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Modulo> consultarModulosPorChaves(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por verificar se já
	 * existe um modulo com a mesma descrição
	 * cadastrada no sistema.
	 * 
	 * @param modulo
	 *            Módulo selecionado para
	 *            verificação.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método..
	 */
	void verificarSeExisteModuloCadastrado(Modulo modulo) throws NegocioException;

	/**
	 * Método responsável por verificar se já
	 * existe uma operação com o mesmo nome e tipo
	 * cadastrada no sistema.
	 * 
	 * @param modulo
	 *            Módulo onde é recuperada as
	 *            operações existentes.
	 * @param operacao
	 *            Operação a ser incluída.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void verificarSeExisteOperacaoCadastrada(Modulo modulo, Operacao operacao) throws NegocioException;

	/**
	 * Método responsável por verificar se já
	 * existe um recurso cadastrado com a mesma
	 * descrição.
	 * 
	 * @param operacao
	 *            Operação que possui os recursos
	 *            existentes.
	 * @param recurso
	 *            Recurso a ser incluido.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void verificarSeExisteRecursoCadastrado(Operacao operacao, Recurso recurso) throws NegocioException;

	/**
	 * Método responsável por buscar a operação
	 * pela chave primária.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da operação
	 * @return Uma operação do sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Operacao buscarOperacaoPorChave(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por buscar um recurso
	 * pela chave primária.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do recurso.
	 * @return Um recurso do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Recurso buscarRecursoPorChave(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por excluir recursos do
	 * sistema.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos recursos
	 *            selecionados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerRecursos(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por validar se algum
	 * recurso foi selecionado para remoção.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos recursos
	 *            selecionados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverRecursos(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método reponsável por remover operações do
	 * sistema.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias das operações
	 *            selecionadas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerOperacoes(Long[] chavesPrimarias) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por validar se alguma
	 * operação foi selecionada para remoção.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias das operações
	 *            selecionadas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverOperacoes(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por remover módulos do
	 * sistema.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos módulos
	 *            selecionados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerModulos(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por validar se algum
	 * modulo foi selecionado para remoção.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos módulos
	 *            selecionados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverModulos(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por consultar as
	 * operações batchs.
	 * 
	 * @param usuario
	 *            O usuário autenticado
	 * @return Uma colecao de operações batchs
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Operacao> consultarOperacoesBatch(Usuario usuario) throws NegocioException;

	/**
	 * Método responsável por consultar as
	 * operações batchs.
	 * 
	 * @param idModulo
	 *            A chave primária de um módulo do
	 *            sistema
	 * @return Uma coleção de operações
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Operacao> consultarOperacoesBatchPorModulo(long idModulo) throws NegocioException;

	/**
	 * Consultar todos modulos.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Modulo> consultarTodosModulos() throws NegocioException;

	/**
	 * Obter modulo por operacao sistema menu.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the modulo
	 */
	Modulo obterModuloPorOperacaoSistemaMenu(Long chavePrimaria);

	/**
	 * Buscar operacao por recurso.
	 * 
	 * @param listaRecursos
	 *            the lista recursos
	 * @return the collection
	 */
	Collection<Operacao> buscarOperacaoPorRecurso(Collection<Recurso> listaRecursos);

	/**
	 * Busca um determinado menu do sistema a partir de parâmetros de filtragem
	 * @param descricaoMenu descrição do menu
	 * @param descricaoOperacao descrição da operação
	 * @param modulo id do módulo
	 * @param papeis lista de ids dos papeis {@link Papel}
	 * @return retorna um Optional com um menu, caso este exista
	 */
	Optional<Menu> buscarMenu(String descricaoMenu, String descricaoOperacao, Long modulo, List<Long> papeis);
}
