/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.controleacesso;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pelas operações referentes 
 * ao papel de um usuário.
 * 
 *
 */
public interface ControladorPapel extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_PAPEL = "controladorPapel";

	String ERRO_NEGOCIO_PAPEL_EXISTENTE = "ERRO_NEGOCIO_PAPEL_EXISTENTE";

	String ERRO_PAPEL_EXISTE_USUARIO_LOGADO = "ERRO_PAPEL_EXISTE_USUARIO_LOGADO";

	/**
	 * Criar permissao.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarPermissao();

	/**
	 * Método responsável por remover papeis do
	 * sistema.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos papeis
	 *            selecionados para remoção.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerPapeis(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por verificar se algum
	 * papel foi selecionado para remoção.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos papeis
	 *            selecionados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverPapeis(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por consultar os Papéis
	 * que se enquadram no filtro.
	 * 
	 * @param filtro
	 *            o filtro
	 * @return os papéis encontrados
	 */
	Collection<Papel> consultarPapel(Map<String, Object> filtro);

	/**
	 * Atualizar papel.
	 * 
	 * @param papel
	 *            the papel
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void atualizarPapel(Papel papel) throws GGASException;

	/**
	 * Gerar relatorio papel.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param papel
	 *            the papel
	 * @param listaPermissoes
	 *            the lista permissoes
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	byte[] gerarRelatorioPapel(Map<String, Object> filtro, Papel papel, List<ModuloFuncOperacoesVO> listaPermissoes)
					throws NegocioException, FormatoInvalidoException;

	/**
	 * Método reponsável por montar uma lista de
	 * Papeis a partir de uma lista de Ids.
	 * 
	 * @param listaIdsPapeis
	 *            the lista ids papeis
	 * @return the sets the
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Set<Papel> obterListaPapelPorIds(Long[] listaIdsPapeis) throws NegocioException;

	/**
	 * Consulta a lista de papéis de um
	 * funcionário.
	 * 
	 * @param chaveFuncionario
	 *            chave do funcionário
	 * @return lista de papéis do funcionário
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<Papel> consultarPapeisPorFuncionario(long chaveFuncionario) throws NegocioException;

	/**
	 * Consulta a lista de papéis de um usuário.
	 * 
	 * @param chaveUsuario
	 *            the chave usuario
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Papel> consultarPapeisPorUsuario(long chaveUsuario) throws NegocioException;

	/**
	 * Remover permissao sistema.
	 * 
	 * @param permissao
	 *            the permissao
	 */
	void removerPermissaoSistema(Permissao permissao);

	/**
	 * Atualizar papel.
	 * 
	 * @param papel the papel
	 * @param usuario the usuario
	 * @param listaNova the lista nova
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	void atualizarPapel(Papel papel, Usuario usuario, Set<Permissao> listaNova) throws ConcorrenciaException, NegocioException;
}
