/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.controleacesso;

import java.util.Collection;
import java.util.Date;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.geral.Favoritos;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Classe responsável por representar um usuário
 *
 */
public interface Usuario extends EntidadeNegocio {

	/**
	 * ENTIDADE_ROTULO_USUARIO
	 */
	String ENTIDADE_ROTULO_USUARIO = "ENTIDADE_ROTULO_USUARIO";

	/**
	 * USUARIO_ROTULO_NOME
	 */
	String USUARIO_ROTULO_NOME = "USUARIO_ROTULO_NOME";

	/**
	 * USUARIO_ROTULO_EMAIL
	 */
	String USUARIO_ROTULO_EMAIL = "USUARIO_ROTULO_EMAIL";

	/**
	 * USUARIO_ROTULO_LOGIN
	 */
	String USUARIO_ROTULO_LOGIN = "USUARIO_ROTULO_LOGIN";

	/**
	 * USUARIO_ROTULO_SENHA
	 */
	String USUARIO_ROTULO_SENHA = "USUARIO_ROTULO_SENHA";

	/**
	 * USUARIO_ROTULO_PAPEL
	 */
	String USUARIO_ROTULO_PAPEL = "USUARIO_ROTULO_PAPEL";

	/**
	 * USUARIO_ROTULO_CONFIRMACAO_SENHA
	 */
	String USUARIO_ROTULO_CONFIRMACAO_SENHA = "USUARIO_ROTULO_CONFIRMACAO_SENHA";

	/**
	 * ERRO_NEGOCIO_SENHA_NAO_CONFERE
	 */
	String ERRO_NEGOCIO_SENHA_NAO_CONFERE = "ERRO_NEGOCIO_SENHA_NAO_CONFERE";

	String BEAN_ID_USUARIO = "usuario";

	/**
	 * @return the login
	 */
	String getLogin();

	/**
	 * @param login
	 *            the login to set
	 */
	void setLogin(String login);

	/**
	 * @return the senha
	 */
	String getSenha();

	/**
	 * @param senha
	 *            the senha to set
	 */
	void setSenha(String senha);

	/**
	 * @return the senhaConfirmada
	 */
	String getSenhaConfirmada();

	/**
	 * @param senhaConfirmada
	 *            the senhaConfirmada to set
	 */
	void setSenhaConfirmada(String senhaConfirmada);

	/**
	 * @return the papeis
	 */
	Collection<Papel> getPapeis();

	/**
	 * @param papeis
	 *            the papeis to set
	 */
	void setPapeis(Collection<Papel> papeis);

	/**
	 * @return the ultimoAcesso
	 */
	Date getUltimoAcesso();

	/**
	 * @param ultimoAcesso
	 *            the ultimoAcesso to set
	 */
	void setUltimoAcesso(Date ultimoAcesso);

	/**
	 * @return the senhaExpirada
	 */
	boolean isSenhaExpirada();

	/**
	 * @param senhaExpirada
	 *            the senhaExpirada to set
	 */
	void setSenhaExpirada(boolean senhaExpirada);

	/**
	 * @return the administradorAtendimento
	 */
	boolean isAdministradorAtendimento();

	/**
	 * @param administradorAtendimento the administradorAtendimento to set
	 */
	void setAdministradorAtendimento(boolean administradorAtendimento);

	/**
	 * @return the tentativasSenhaErrada
	 */
	int getTentativasSenhaErrada();

	/**
	 * @param tentativasSenhaErrada
	 *            the tentativasSenhaErrada to set
	 */
	void setTentativasSenhaErrada(int tentativasSenhaErrada);

	/**
	 * @return the Funcionario
	 */
	Funcionario getFuncionario();

	/**
	 * @param funcionario - Set Funcionário. 
	 */
	void setFuncionario(Funcionario funcionario);

	/**
	 * @return the dataExpirarSenha
	 */
	Date getDataExpirarSenha();

	/**
	 * @param dataExpirarSenha
	 *            the dataExpirarSenha to set
	 */
	void setDataExpirarSenha(Date dataExpirarSenha);

	/**
	 * Checks if is primeiro acesso.
	 * 
	 * @return the primeiroAcesso
	 */
	Boolean isPrimeiroAcesso();

	/**
	 * @param primeiroAcesso
	 *            the primeiroAcesso to set
	 */
	void setPrimeiroAcesso(Boolean primeiroAcesso);

	/**
	 * @return the periodicidadeSenha
	 */
	Integer getPeriodicidadeSenha();

	/**
	 * @param periodicidadeSenha
	 *            the periodicidadeSenha to set
	 */
	void setPeriodicidadeSenha(Integer periodicidadeSenha);

	/**
	 * @return dataCriacaoSenha
	 */
	Date getDataCriacaoSenha();

	/**
	 * @param dataCricaoSenha
	 *            the dataCriacaoSenha to set
	 */
	void setDataCriacaoSenha(Date dataCricaoSenha);

	/**
	 * @return the favoritos
	 */
	Collection<Favoritos> getFavoritos();

	/**
	 * @param favoritos
	 *            the favoritos to set
	 */
	void setFavoritos(Collection<Favoritos> favoritos);
	
	/**
	 * 
	 * @return Descricao apenas para tela de constante sistema.
	 */
	String getDescricao();

	/**
	 * @param usuarioDominio
	 */
	void setUsuarioDominio(String usuarioDominio);

	/**
	 * @return usuarioDominio
	 */
	String getUsuarioDominio();

	String getToken();
	void setToken(String token);
}
