
package br.com.ggas.controleacesso;

import br.com.ggas.geral.Menu;
/**
 * Classe responsável pela representação da entidade Permissao 
 *
 */
public class PermissaoVO {

	private Menu menu;

	private Operacao operacao;

	private int tipo;

	private Papel papel;

	public Menu getMenu() {

		return menu;
	}

	public void setMenu(Menu menu) {

		this.menu = menu;
	}

	public Operacao getOperacao() {

		return operacao;
	}

	public void setOperacao(Operacao operacao) {

		this.operacao = operacao;
	}

	public int getTipo() {

		return tipo;
	}

	public void setTipo(int tipo) {

		this.tipo = tipo;
	}

	public Papel getPapel() {

		return papel;
	}

	public void setPapel(Papel papel) {

		this.papel = papel;
	}
}
