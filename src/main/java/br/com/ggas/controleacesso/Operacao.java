/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.controleacesso;

import java.util.Collection;

import br.com.ggas.geral.Menu;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados a entidade Operação
 *
 */
public interface Operacao extends EntidadeNegocio {

	/**
	 * ENTIDADE_ROTULO_OPERACAO
	 */
	String ENTIDADE_ROTULO_OPERACAO = "ENTIDADE_ROTULO_OPERACAO";

	/**
	 * OPERACAO_ROTULO_DESCRICAO
	 */
	String OPERACAO_ROTULO_DESCRICAO = "OPERACAO_ROTULO_DESCRICAO";

	String BEAN_ID_OPERACAO = "operacao";

	/**
	 * Tipo de operacao Online
	 */
	int ONLINE = 0;

	/**
	 * Tipo de operacao bath
	 */
	int BATCH = 1;

	/**
	 * @return the tipo
	 */
	int getTipo();

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	void setTipo(int tipo);

	/**
	 * @return the modulo
	 */
	Modulo getModulo();

	/**
	 * @param modulo
	 *            the modulo to set
	 */
	void setModulo(Modulo modulo);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the recursos
	 */
	Collection<Recurso> getRecursos();

	/**
	 * @param recursos
	 *            the recursos to set
	 */
	void setRecursos(Collection<Recurso> recursos);

	/**
	 * @return the auditavel
	 */
	boolean isAuditavel();

	/**
	 * @param auditavel
	 *            the auditavel to set
	 */
	void setAuditavel(boolean auditavel);

	/**
	 * @return the menu
	 */
	Menu getMenu();

	/**
	 * @param menu
	 *            the menu to set
	 */
	void setMenu(Menu menu);

	boolean isExibir();
	void setExibir(boolean exibir);

}
