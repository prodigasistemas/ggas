/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.controleacesso;

import java.io.Serializable;
import java.util.Collection;

import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;
/**
 * Classe responsável pela representação da entidade AutorizacoesPendentes 
 *
 */
public class AutorizacoesPendentesVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Collection<CreditoDebitoNegociado> creditoDebitos;

	private Collection<Parcelamento> parcelamentos;

	private Collection<Tarifa> tarifa;

	private Collection<SupervisorioMedicaoDiaria> supervisorioMedicoesDiaria;

	private Collection<SupervisorioMedicaoHoraria> supervisorioMedicoesHoraria;

	private Collection<SolicitacaoConsumoPontoConsumo> solicitacaoConsumoPontoConsumo;

	public Collection<CreditoDebitoNegociado> getCreditoDebitos() {

		return creditoDebitos;
	}

	public void setCreditoDebitos(Collection<CreditoDebitoNegociado> creditoDebitos) {

		this.creditoDebitos = creditoDebitos;
	}

	public Collection<Parcelamento> getParcelamentos() {

		return parcelamentos;
	}

	public void setParcelamentos(Collection<Parcelamento> parcelamentos) {

		this.parcelamentos = parcelamentos;
	}

	public Collection<Tarifa> getTarifa() {

		return tarifa;
	}

	public void setTarifa(Collection<Tarifa> tarifa) {

		this.tarifa = tarifa;
	}

	/**
	 * Possui pendencias.
	 * 
	 * @return true, if successful
	 */
	public boolean possuiPendencias() {

		return (creditoDebitos != null && !creditoDebitos.isEmpty()) || (parcelamentos != null && !parcelamentos.isEmpty())
						|| (tarifa != null && !tarifa.isEmpty())
						|| (supervisorioMedicoesDiaria != null && !supervisorioMedicoesDiaria.isEmpty())
						|| (supervisorioMedicoesHoraria != null && !supervisorioMedicoesHoraria.isEmpty())
						|| (solicitacaoConsumoPontoConsumo != null && !solicitacaoConsumoPontoConsumo.isEmpty());
	}

	public Collection<SupervisorioMedicaoDiaria> getSupervisorioMedicoesDiaria() {

		return supervisorioMedicoesDiaria;
	}

	public void setSupervisorioMedicoesDiaria(Collection<SupervisorioMedicaoDiaria> supervisorioMedicoesDiaria) {

		this.supervisorioMedicoesDiaria = supervisorioMedicoesDiaria;
	}

	public Collection<SupervisorioMedicaoHoraria> getSupervisorioMedicoesHoraria() {

		return supervisorioMedicoesHoraria;
	}

	public void setSupervisorioMedicoesHoraria(Collection<SupervisorioMedicaoHoraria> supervisorioMedicoesHoraria) {

		this.supervisorioMedicoesHoraria = supervisorioMedicoesHoraria;
	}

	public Collection<SolicitacaoConsumoPontoConsumo> getSolicitacaoConsumoPontoConsumo() {

		return solicitacaoConsumoPontoConsumo;
	}

	public void setSolicitacaoConsumoPontoConsumo(Collection<SolicitacaoConsumoPontoConsumo> solicitacaoConsumoPontoConsumo) {

		this.solicitacaoConsumoPontoConsumo = solicitacaoConsumoPontoConsumo;
	}

}
