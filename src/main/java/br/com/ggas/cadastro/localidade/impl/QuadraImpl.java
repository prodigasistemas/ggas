/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.localidade.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import br.com.ggas.cadastro.dadocensitario.SetorCensitario;
import br.com.ggas.cadastro.localidade.AreaTipo;
import br.com.ggas.cadastro.localidade.PerfilQuadra;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.cadastro.localidade.Zeis;
import br.com.ggas.cadastro.operacional.ZonaBloqueio;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * 	Classe responsável pelos atributos da quadra.
 *
 */
public class QuadraImpl extends EntidadeNegocioImpl implements Quadra {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 3668581581909482220L;

	private String numeroQuadra;

	private SetorComercial setorComercial;

	private PerfilQuadra perfilQuadra;

	private Zeis zeis;

	private Collection<QuadraFace> quadrasFace = new HashSet<>();

	private SetorCensitario setorCensitario;

	private AreaTipo areaTipo;

	private ZonaBloqueio zonaBloqueio;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * getNumeroQuadra()
	 */
	@Override
	public String getNumeroQuadra() {

		return numeroQuadra;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * setNumeroQuadra(java.lang.Integer)
	 */
	@Override
	public void setNumeroQuadra(String numeroQuadra) {

		this.numeroQuadra = numeroQuadra;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * getSetorComercial()
	 */
	@Override
	public SetorComercial getSetorComercial() {

		return setorComercial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * setSetorComercial
	 * (br.com.ggas.cadastro.localidade
	 * .SetorComercial)
	 */
	@Override
	public void setSetorComercial(SetorComercial setorComercial) {

		this.setorComercial = setorComercial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * getPerfilQuadra()
	 */
	@Override
	public PerfilQuadra getPerfilQuadra() {

		return perfilQuadra;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * setPerfilQuadra
	 * (br.com.ggas.cadastro.localidade
	 * .PerfilQuadra)
	 */
	@Override
	public void setPerfilQuadra(PerfilQuadra perfilQuadra) {

		this.perfilQuadra = perfilQuadra;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * getZeis()
	 */
	@Override
	public Zeis getZeis() {

		return zeis;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * setZeis
	 * (br.com.ggas.cadastro.localidade.Zeis)
	 */
	@Override
	public void setZeis(Zeis zeis) {

		this.zeis = zeis;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * getQuadrasFace()
	 */
	@Override
	public Collection<QuadraFace> getQuadrasFace() {

		return quadrasFace;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * setQuadrasFace(java.util.Collection)
	 */
	@Override
	public void setQuadrasFace(Collection<QuadraFace> quadrasFace) {

		this.quadrasFace = quadrasFace;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * getSetorCensitario()
	 */
	@Override
	public SetorCensitario getSetorCensitario() {

		return setorCensitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * setSetorCensitario
	 * (br.com.ggas.cadastro.dadocensitario
	 * .SetorCensitario)
	 */
	@Override
	public void setSetorCensitario(SetorCensitario setorCensitario) {

		this.setorCensitario = setorCensitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * getAreaTipo()
	 */
	@Override
	public AreaTipo getAreaTipo() {

		return areaTipo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * setAreaTipo
	 * (br.com.ggas.cadastro.localidade.AreaTipo)
	 */
	@Override
	public void setAreaTipo(AreaTipo areaTipo) {

		this.areaTipo = areaTipo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * getZonaBloqueio()
	 */
	@Override
	public ZonaBloqueio getZonaBloqueio() {

		return zonaBloqueio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Quadra#
	 * setZonaBloqueio
	 * (br.com.ggas.cadastro.operacional
	 * .ZonaBloqueio)
	 */
	@Override
	public void setZonaBloqueio(ZonaBloqueio zonaBloqueio) {

		this.zonaBloqueio = zonaBloqueio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(setorComercial == null) {
			stringBuilder.append(SETOR_COMERCIAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(numeroQuadra == null || numeroQuadra.length() == 0) {
			stringBuilder.append(NUMERO_QUADRA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(perfilQuadra == null) {
			stringBuilder.append(PERFIL_QUADRA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.quadrasFace == null || this.quadrasFace.isEmpty()) {
			stringBuilder.append(QUADRA_FACE_QUADRA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.quadrasFace != null && !this.quadrasFace.isEmpty()) {
			QuadraFace quadraFace = null;
			Map<String, Object> errosQuadraFace = null;
			for (Iterator<QuadraFace> iterator = this.quadrasFace.iterator(); iterator.hasNext();) {
				quadraFace = iterator.next();
				errosQuadraFace = quadraFace.validarDados();
				if(errosQuadraFace != null && !errosQuadraFace.isEmpty()) {
					erros.putAll(errosQuadraFace);
				}
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}
}
