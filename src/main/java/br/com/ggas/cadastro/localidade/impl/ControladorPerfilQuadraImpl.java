/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade.impl;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.localidade.ControladorPerfilQuadra;
import br.com.ggas.cadastro.localidade.PerfilQuadra;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 * 
 */
class ControladorPerfilQuadraImpl extends ControladorNegocioImpl implements ControladorPerfilQuadra {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PerfilQuadra.BEAN_ID_PERFIL_QUADRA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(PerfilQuadra.BEAN_ID_PERFIL_QUADRA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localizacao.
	 * ControladorPerfilQuadra
	 * #consultarPerfilQuadra(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<PerfilQuadra> consultarPerfilQuadra(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
			}

			String descricao = (String) filtro.get(EntidadeClasse.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(EntidadeClasse.ATRIBUTO_DESCRICAO, descricao, MatchMode.ANYWHERE));
			}
		}

		criteria.addOrder(Order.asc(EntidadeClasse.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

}
