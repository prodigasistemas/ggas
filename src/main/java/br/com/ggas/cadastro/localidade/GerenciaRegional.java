/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade;

import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * a gerência regional
 *
 */
public interface GerenciaRegional extends EntidadeNegocio {

	String BEAN_ID_GERENCIA_REGIONAL = "gerenciaRegional";

	String GERENCIA_REGIONAL = "GERENCIA_REGIONAL";

	String NOME = "GERE_NOME";

	String NOME_ABREVIADO = "GERE_N0ME_ABREVIADO";

	String EMAIL = "GERE_EMAIL";

	String TELEFONE = "GERE_FONE";

	String RAMAL = "GERE_RAMAL";

	String FAX = "GERE_FAX";

	String NUMERO = "GERE_NUMERO";

	/**
	 * @return the nome
	 */
	String getNome();

	/**
	 * @param nome
	 *            the nome to set
	 */
	void setNome(String nome);

	/**
	 * @return the nomeAbreviado
	 */
	String getNomeAbreviado();

	/**
	 * @param nomeAbreviado
	 *            the nomeAbreviado to set
	 */
	void setNomeAbreviado(String nomeAbreviado);

	/**
	 * @return the fone
	 */
	String getFone();

	/**
	 * @param fone
	 *            the fone to set
	 */
	void setFone(String fone);

	/**
	 * @return the ramalFone
	 */
	String getRamalFone();

	/**
	 * @param ramalFone
	 *            the ramalFone to set
	 */
	void setRamalFone(String ramalFone);

	/**
	 * @return the fax
	 */
	String getFax();

	/**
	 * @param fax
	 *            the fax to set
	 */
	void setFax(String fax);

	/**
	 * @return the email
	 */
	String getEmail();

	/**
	 * @param email
	 *            the email to set
	 */
	void setEmail(String email);

	/**
	 * @return the endereco
	 */
	Endereco getEndereco();

	/**
	 * @param endereco
	 *            the endereco to set
	 */
	void setEndereco(Endereco endereco);

	/**
	 * @return the codigoFaxDDD
	 */
	Integer getCodigoFaxDDD();

	/**
	 * @param codigoFaxDDD
	 *            the codigoFaxDDD to set
	 */
	void setCodigoFaxDDD(Integer codigoFaxDDD);

	/**
	 * @return the codigoFoneDDD
	 */
	Integer getCodigoFoneDDD();

	/**
	 * @param codigoFoneDDD
	 *            the codigoFoneDDD to set
	 */
	void setCodigoFoneDDD(Integer codigoFoneDDD);

}
