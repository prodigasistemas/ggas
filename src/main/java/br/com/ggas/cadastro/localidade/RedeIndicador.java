/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.geral.dominio.EntidadeDominio;

/**
 * Classe responsável por marcação de indicadr
 * 
 */
public interface RedeIndicador extends EntidadeDominio {

	String BEAN_ID_REDE_INDICADOR = "redeIndicador";

	
	// FIXME: Substituir por constante
	/**
	 * @deprecated
	 */
	@Deprecated
	int TEM = 1;

	// FIXME: Substituir por constante
	/**
	 * @deprecated 
	 */
	@Deprecated
	static final int NAO_TEM = 2;

	// FIXME: Substituir por constante
	/**
	 * @deprecated
	 */
	@Deprecated
	static final int TEM_PARCIALMENTE = 3;

	/**
	 * @deprecated
	 */
	@Deprecated
	static final int AMBOS = 0;

	String DESCRICAO_TEM = "Tem";

	String DESCRICAO_NAO_TEM = "Não tem";

	String DESCRICAO_TEM_PARCIALMENTE = "Tem Parcialmente";

	String DESCRICAO_AMBOS = "Ambos";

	Map<Integer, String> INDICADORES_REDE = new HashMap<Integer, String>(){

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(TEM, DESCRICAO_TEM);
			put(NAO_TEM, DESCRICAO_NAO_TEM);
			put(TEM_PARCIALMENTE, DESCRICAO_TEM_PARCIALMENTE);
			put(AMBOS, DESCRICAO_AMBOS);

		}
	};
}
