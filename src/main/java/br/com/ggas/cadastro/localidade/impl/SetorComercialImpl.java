/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe SetorComercialImpl representa um setor comercial no sistema.
 *
 * @since 30/07/2009
 * 
 */

package br.com.ggas.cadastro.localidade.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pela implementação dos
 * métodos relacionados a entidade Setor Comercial
 * e o método validar dados da mesma.
 *
 */
public class SetorComercialImpl extends EntidadeNegocioImpl implements SetorComercial {

	private static final int LIMITE_CAMPO = 2;

	private static final int TAMANHO_MINIMO_DESCRICAO = 30;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 6475116638465255408L;

	private String codigo;

	private String descricao;

	private Municipio municipio;

	private Localidade localidade;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localizacao.impl.
	 * SetorComercial#getCodigo()
	 */
	@Override
	public String getCodigo() {

		return codigo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localizacao.impl.
	 * SetorComercial#setCodigo(java.lang.String)
	 */
	@Override
	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localizacao.impl.
	 * SetorComercial#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localizacao.impl.
	 * SetorComercial
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localizacao.impl.
	 * SetorComercial#getMunicipio()
	 */
	@Override
	public Municipio getMunicipio() {

		return municipio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localizacao.impl.
	 * SetorComercial
	 * #setMunicipio(br.com.ggas.cadastro
	 * .localizacao.Municipio)
	 */
	@Override
	public void setMunicipio(Municipio municipio) {

		this.municipio = municipio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localizacao.impl.
	 * SetorComercial#getLocalidade()
	 */
	@Override
	public Localidade getLocalidade() {

		return localidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localizacao.impl.
	 * SetorComercial
	 * #setLocalidade(br.com.ggas.cadastro
	 * .localizacao.Localidade)
	 */
	@Override
	public void setLocalidade(Localidade localidade) {

		this.localidade = localidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		StringBuilder tamanhoCamposAcumulados = new StringBuilder();
		String camposObrigatorios = null;
		String camposInvalidos = null;

		if(codigo == null) {
			stringBuilder.append(CODIGO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else if(descricao.length() > TAMANHO_MINIMO_DESCRICAO) {
			tamanhoCamposAcumulados.append(DESCRICAO);
			tamanhoCamposAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(municipio == null) {
			stringBuilder.append(MUNICIPIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(localidade == null) {
			stringBuilder.append(LOCALIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();
		camposInvalidos = tamanhoCamposAcumulados.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		if(camposInvalidos.length() > 0) {
			erros.put(Constantes.ERRO_TAMANHO_LIMITE, camposInvalidos.substring(0, tamanhoCamposAcumulados.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

}
