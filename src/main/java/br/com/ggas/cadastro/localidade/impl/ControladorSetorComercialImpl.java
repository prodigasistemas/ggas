/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorSetorComercialImpl representa um controlador de setor comercial no sistema.
 *
 * @since 30/07/2009
 * 
 */

package br.com.ggas.cadastro.localidade.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * 
 *
 */
class ControladorSetorComercialImpl extends ControladorNegocioImpl implements ControladorSetorComercial {

	private static final String CODIGO2 = "codigo";
	private ControladorQuadra controladorQuadra;
	
	private static final String CAMPO_ID_LOCALIDADE = "idLocalidade";
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(SetorComercial.BEAN_ID_SETOR_COMERCIAL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(SetorComercial.BEAN_ID_SETOR_COMERCIAL);
	}

	/**
	 * Config controlador quadra.
	 * 
	 * @return the controlador quadra
	 */
	public ControladorQuadra configControladorQuadra() {

		this.controladorQuadra = (ControladorQuadra) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorQuadra.BEAN_ID_CONTROLADOR_QUADRA);
		
		return this.controladorQuadra;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorSetorComercial
	 * #consultarSetorComercial(java.util.Map)
	 */
	@Override
	public Collection<SetorComercial> consultarSetorComercial(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			String codigo = (String) filtro.get(CODIGO2);
			if(codigo != null) {
				criteria.add(Restrictions.eq(CODIGO2, codigo));
			}
			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}
			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}
			criteria.createAlias("localidade", "localidade");
			Long idLocalidade = (Long) filtro.get(CAMPO_ID_LOCALIDADE);
			if(idLocalidade != null && idLocalidade > 0) {
				criteria.add(Restrictions.eq("localidade.chavePrimaria", idLocalidade));
			}
			criteria.createAlias("municipio", "municipio");
			Long idMunicipio = (Long) filtro.get("idMunicipio");
			if(idMunicipio != null && idMunicipio > 0) {
				criteria.add(Restrictions.eq("municipio.chavePrimaria", idMunicipio));
			}
			Long idUnidadeFederacao = (Long) filtro.get("idUnidadeFederacao");
			if(idUnidadeFederacao != null && idUnidadeFederacao > 0) {
				criteria.createAlias("municipio.unidadeFederacao", "unidadeFederacao");
				criteria.add(Restrictions.eq("unidadeFederacao.chavePrimaria", idUnidadeFederacao));
			}
			Long idGerenciaRegional = (Long) filtro.get("idGerenciaRegional");
			if(idGerenciaRegional != null && idGerenciaRegional > 0) {
				criteria.createAlias("localidade.gerenciaRegional", "gerenciaRegional");
				criteria.add(Restrictions.eq("gerenciaRegional.chavePrimaria", idGerenciaRegional));
			}
		}

		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		validarSetorComercialExistente((SetorComercial) entidadeNegocio);
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {
		
		validarSetorComercialExistente((SetorComercial) entidadeNegocio);
		super.preAtualizacao(entidadeNegocio);
	}

	/**
	 * Validar setor comercial existente.
	 * 
	 * @param setorComercial
	 *            the setor comercial
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarSetorComercialExistente(SetorComercial setorComercial) throws NegocioException {


		Map<String, Object> filtro = new HashMap<>();
		filtro.put(CODIGO2, setorComercial.getCodigo());
		filtro.put(CAMPO_ID_LOCALIDADE, setorComercial.getLocalidade().getChavePrimaria());
		List<SetorComercial> listaSetorComercial = (List<SetorComercial>) this.consultarSetorComercial(filtro);

		if (!listaSetorComercial.isEmpty()) {
			for (SetorComercial setorComercialExistente : listaSetorComercial) {
				Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();

				sessao.evict(setorComercialExistente);
				if (setorComercialExistente.getChavePrimaria() != setorComercial.getChavePrimaria()) {
					throw new NegocioException(ControladorSetorComercial.ERRO_NEGOCIO_SETOR_COMERCIAL_EXISTENTE,
							new String[] { String.valueOf(setorComercial.getCodigo()), setorComercial.getLocalidade().getDescricao() });
				}
			}
		}
		//filtro para checar duplicidade no nome do setor comercial
		filtro.clear();
		filtro.put("descricao", setorComercial.getDescricao());
		filtro.put(CAMPO_ID_LOCALIDADE, setorComercial.getLocalidade().getChavePrimaria());
		listaSetorComercial.clear();
		listaSetorComercial = (List<SetorComercial>) this.consultarSetorComercial(filtro);

		if(!listaSetorComercial.isEmpty()) {
			
			for(SetorComercial setorComercialExistente: listaSetorComercial){
				Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
					sessao.evict(setorComercialExistente);
					if(setorComercialExistente.getChavePrimaria() != setorComercial.getChavePrimaria()) {
					throw new NegocioException(ControladorSetorComercial.ERRO_NEGOCIO_SETOR_COMERCIAL_NOME_EXISTENTE,
									new String[] {String.valueOf(setorComercial.getDescricao()), setorComercial.getLocalidade().getDescricao()});
				}
			}
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorSetorComercial
	 * #validarRemoverSetoresComerciais
	 * (java.lang.Long[])
	 */
	@Override
	public void validarRemoverSetoresComerciais(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

		controladorQuadra = configControladorQuadra();
		for (Long chavePrimaria : chavesPrimarias) {
			if(chavePrimaria > 0) {
				Map<String, Object> params = new HashMap<>();
				params.put("idSetorComercial", chavePrimaria);
				Collection<Quadra> quadras = controladorQuadra.consultarQuadras(params);
				if (quadras != null && !quadras.isEmpty()) {
					Map<String, Object> filtro = new HashMap<>();
					filtro.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);
					List<SetorComercial> listaSetorComercial = (List<SetorComercial>) this.consultarSetorComercial(filtro);
					SetorComercial setorComercial = listaSetorComercial.get(0);
					
					throw new NegocioException(ERRO_NEGOCIO_SETOR_COMERCIAL_POSSUI_VINCULO, setorComercial.getCodigo());
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorSetorComercial
	 * #validarAtualizarSetorComercial
	 * (java.lang.Long[])
	 */
	@Override
	public void validarAtualizarSetorComercial(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length != 1) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#posValidarDadosEntidade(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void posValidarDadosEntidade(EntidadeNegocio entidadeNegocio) throws NegocioException {

		SetorComercial setorComercial = (SetorComercial) entidadeNegocio;
		if(setorComercial.getCodigo().equals("0")) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_VALOR_ZERO, new Object[] {SetorComercial.CODIGO});
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorSetorComercial
	 * #listarSetoresComerciais()
	 */
	@Override
	public Collection<SetorComercial> listarSetoresComerciais() {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" habilitado = true ");
		hql.append(" order by descricao");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}
}
