/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorLocalidadeImpl representa um controlador localidade no sistema.
 *
 * @since 30/07/2009
 * 
 */

package br.com.ggas.cadastro.localidade.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.LocalidadeClasse;
import br.com.ggas.cadastro.localidade.LocalidadePorte;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * 
 * 
 */
class ControladorLocalidadeImpl extends ControladorNegocioImpl implements ControladorLocalidade {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Localidade.BEAN_ID_LOCALIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Localidade.BEAN_ID_LOCALIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorLocalidade
	 * #consultarLocalidades(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Localidade> consultarLocalidades(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}
			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			Long idGerenciaRegional = (Long) filtro.get("idGerenciaRegional");
			if(idGerenciaRegional != null && idGerenciaRegional > 0) {
				criteria.createAlias("gerenciaRegional", "gerenciaRegional");
				criteria.add(Restrictions.eq("gerenciaRegional.chavePrimaria", idGerenciaRegional));
			}

			Long idUnidadeNegocio = (Long) filtro.get("idUnidadeNegocio");
			if(idUnidadeNegocio != null && idUnidadeNegocio > 0) {
				criteria.createAlias("unidadeNegocio", "unidadeNegocio");
				criteria.add(Restrictions.eq("unidadeNegocio.chavePrimaria", idUnidadeNegocio));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorLocalidade
	 * #consultarMedidorLocalArmazenagem
	 * (java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<MedidorLocalArmazenagem> consultarMedidorLocalArmazenagem(Map<String, Object> filtro) throws GGASException {

		Criteria criteria = createCriteria(getClasseEntidadeMedidorLocalArmazenagemClasse());

		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}
			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorLocalidade#
	 * validarExistenciaLocalidadesPorGerenciaRegional
	 * (java.lang.Long)
	 */
	@Override
	public boolean validarExistenciaLocalidadesPorGerenciaRegional(Long chaveGerenciaRegional) throws NegocioException {

		Long quantidade = 0L;
		boolean existeDependencia = false;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(chavePrimaria) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" gerenciaRegional.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chaveGerenciaRegional);

		quantidade = (Long) query.uniqueResult();

		if(quantidade > 0) {
			existeDependencia = true;
		}
		return existeDependencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorLocalidade#criarLocalidadeClasse()
	 */
	@Override
	public EntidadeNegocio criarLocalidadeClasse() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(LocalidadeClasse.BEAN_ID_LOCALIDADE_CLASSE);
	}

	public Class<?> getClasseEntidadeLocalidadeClasse() {

		return ServiceLocator.getInstancia().getClassPorID(LocalidadeClasse.BEAN_ID_LOCALIDADE_CLASSE);
	}

	public Class<?> getClasseEntidadeMedidorLocalArmazenagemClasse() {

		return ServiceLocator.getInstancia().getClassPorID(MedidorLocalArmazenagem.BEAN_ID_MEDIDOR_LOCAL_ARMAZENAGEM);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorLocalidade
	 * #consultarLocalidadeClasses(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<LocalidadeClasse> consultarLocalidadeClasses(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeLocalidadeClasse());

		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}
			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorLocalidade#criarLocalidadePorte()
	 */
	@Override
	public EntidadeNegocio criarLocalidadePorte() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(LocalidadePorte.BEAN_ID_LOCALIDADE_PORTE);
	}

	public Class<?> getClasseEntidadeLocalidadePorte() {

		return ServiceLocator.getInstancia().getClassPorID(LocalidadePorte.BEAN_ID_LOCALIDADE_PORTE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorLocalidade
	 * #consultarLocalidadePortes(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<LocalidadePorte> consultarLocalidadePortes(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeLocalidadePorte());

		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}
			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}
		}

		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorLocalidade
	 * #obterLocalidadeClasse(long)
	 */
	@Override
	public LocalidadeClasse obterLocalidadeClasse(long chavePrimaria) throws NegocioException {

		return (LocalidadeClasse) obter(chavePrimaria, getClasseEntidadeLocalidadeClasse());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorLocalidade
	 * #obterLocalidadePorte(long)
	 */
	@Override
	public LocalidadePorte obterLocalidadePorte(long chavePrimaria) throws NegocioException {

		return (LocalidadePorte) obter(chavePrimaria, getClasseEntidadeLocalidadePorte());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorLocalidade
	 * #obterMedidorLocalArmazenagem(long)
	 */
	@Override
	public MedidorLocalArmazenagem obterMedidorLocalArmazenagem(long chavePrimaria) throws NegocioException {

		return (MedidorLocalArmazenagem) obter(chavePrimaria, getClasseEntidadeMedidorLocalArmazenagemClasse());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Localidade localidade = (Localidade) entidadeNegocio;
		validarLocalidadeExistente(localidade);
		verificarEmailValido(localidade.getEmail());
		ControladorEndereco controladorEndereco = ServiceLocator.getInstancia().getControladorEndereco();

		if(localidade.getEndereco() != null && localidade.getEndereco().getCep() != null) {
			controladorEndereco.validarExistenciaCep(localidade.getEndereco().getCep());
		}
	}

	/**
	 * Validar localidade existente.
	 * 
	 * @param localidade
	 *            the localidade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarLocalidadeExistente(Localidade localidade) throws NegocioException {

		Map<String, Object> filtro = new HashMap<>();
		filtro.put(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, localidade.getChavePrimaria());
		List<Localidade> listaLocalidade = (List<Localidade>) this.consultarLocalidades(filtro);

		if(!listaLocalidade.isEmpty()) {
			throw new NegocioException(ControladorLocalidade.ERRO_NEGOCIO_CODIGO_LOCALIDADE_EXISTENTE, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorLocalidade#inserirLocalidade(br.com.ggas.cadastro.localidade.Localidade)
	 */
	@Override
	public Long inserirLocalidade(Localidade localidade) throws NegocioException {

		return super.inserir(localidade);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorLocalidade
	 * #validarAtualizarLocalidade
	 * (java.lang.Long[])
	 */
	@Override
	public void validarAtualizarLocalidade(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length != 1) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Localidade localidade = (Localidade) entidadeNegocio;
		verificarEmailValido(localidade.getEmail());

		ControladorEndereco controladorEndereco = ServiceLocator.getInstancia().getControladorEndereco();

		if(localidade.getEndereco() != null && localidade.getEndereco().getCep() != null) {
			controladorEndereco.validarExistenciaCep(localidade.getEndereco().getCep());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorLocalidade
	 * #validarRemoverLocalidade(java.lang.Long[])
	 */
	@Override
	public void validarRemoverLocalidades(Long[] chavesPrimarias) throws NegocioException {

		ControladorSetorComercial controladorSetorComercial = ServiceLocator.getInstancia().getControladorSetorComercial();
		Collection<SetorComercial> lista;

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
		for (Long idLocalidade : chavesPrimarias) {
			if(idLocalidade > 0) {
				Map<String, Object> filtro = new HashMap<>();
				filtro.put("idLocalidade", idLocalidade);
				lista = controladorSetorComercial.consultarSetorComercial(filtro);
				if (lista != null && !lista.isEmpty()) {
					throw new NegocioException(Constantes.ERRO_NAO_E_POSSIVEL_REMOVER_LOCALIDADE_EM_USO, true);
				}
			}
		}

	}

	/**
	 * Verificar email valido.
	 * 
	 * @param email
	 *            the email
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarEmailValido(String email) throws NegocioException {

		if (!StringUtils.isEmpty(email) && !Util.validarDominio(email.trim(), Constantes.EXPRESSAO_REGULAR_EMAIL)) {
			throw new NegocioException(ERRO_NEGOCIO_EMAIL_INVALIDO, GerenciaRegional.EMAIL);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorLocalidade#listarLocalidades()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Localidade> listarLocalidades() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}
}
