/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados 
 * ao controlador de localidade
 *
 */
public interface ControladorLocalidade extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_LOCALIDADE = "controladorLocalidade";

	String ERRO_NEGOCIO_LOCALIDADE_EXISTENTE = "ERRO_NEGOCIO_LOCALIDADE_EXISTENTE";

	String ERRO_NEGOCIO_EMAIL_INVALIDO = "ERRO_NEGOCIO_EMAIL_INVALIDO";

	String ERRO_NEGOCIO_CODIGO_LOCALIDADE_EXISTENTE = "ERRO_NEGOCIO_CODIGO_LOCALIDADE_EXISTENTE";

	/**
	 * Método responsável por consultar as
	 * localidades pelo filtro informado.
	 * 
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de Localidade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Localidade> consultarLocalidades(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por validar se existe
	 * localidades associadas a
	 * gerência regional informada.
	 * 
	 * @param chaveGerenciaRegional
	 *            A chave primária da gerência
	 *            regional.
	 * @return Um booleando indicando se existe ou
	 *         não localidades para a chave
	 *         primária da gerência regional
	 *         informada.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean validarExistenciaLocalidadesPorGerenciaRegional(Long chaveGerenciaRegional) throws NegocioException;

	/**
	 * Método responsável por criar uma entidade
	 * LocalidadeClasse.
	 * 
	 * @return Uma nova entidade LocalidadeClasse.
	 */
	EntidadeNegocio criarLocalidadeClasse();

	/**
	 * Método responsável por consultar as classes
	 * de localidade pelo filtro informado.
	 * 
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de LocalidadeClasse.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<LocalidadeClasse> consultarLocalidadeClasses(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por consultar os locais
	 * de armazenagem dos medidores pelo filtro
	 * informado.
	 * 
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção unidades de negócio.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<MedidorLocalArmazenagem> consultarMedidorLocalArmazenagem(Map<String, Object> filtro) throws GGASException;

	/**
	 * Método responsável por criar uma entidade
	 * LocalidadePorte.
	 * 
	 * @return Uma nova entidade LocalidadePorte.
	 */
	EntidadeNegocio criarLocalidadePorte();

	/**
	 * Método responsável por consultar os portes
	 * de localidade pelo filtro informado.
	 * 
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de LocalidadePorte.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<LocalidadePorte> consultarLocalidadePortes(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por obter uma classe de
	 * localidade.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da classe de
	 *            localidade.
	 * @return LocalidadeClasse
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de
	 *             negócio
	 */
	LocalidadeClasse obterLocalidadeClasse(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter um porte de
	 * localidade.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do porte de
	 *            localidade.
	 * @return LocalidadePorte
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de
	 *             negócio
	 */
	LocalidadePorte obterLocalidadePorte(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por buscar o medidor
	 * local armazenagem pela chave primária.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do medidor
	 *            local de armzenagem.
	 * @return Um MedidorLocalArmazenagem.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	MedidorLocalArmazenagem obterMedidorLocalArmazenagem(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para atualizar uma localidade.
	 * 
	 * @param chavesPrimarias
	 *            Chaves das localidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarAtualizarLocalidade(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover localidades.
	 * 
	 * @param chavesPrimarias
	 *            Chaves das localidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverLocalidades(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por listar as
	 * localidades ativas.
	 * 
	 * @return Uma coleção de localidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Localidade> listarLocalidades() throws NegocioException;

	/**
	 * Inserir localidade.
	 * 
	 * @param localidade
	 *            the localidade
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Long inserirLocalidade(Localidade localidade) throws NegocioException;
}
