/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.Rede;
import br.com.ggas.cadastro.localidade.RedeDistribuicaoTronco;
import br.com.ggas.cadastro.operacional.RedeDiametro;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável por implementar os métodos relacionados a entidade Rede
 * 
 */
public class RedeImpl extends EntidadeNegocioImpl implements Rede {

	private static final int LIMITE_CAMPO = 2;

	private static final int NUMERO_CASAS_DECIMAIS = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 818111203314931914L;

	private String descricao;

	private Localidade localidade;

	private RedeDiametro redeDiametro;

	private RedeMaterial redeMaterial;

	private BigDecimal extensao;

	private Unidade unidadeExtensao;

	private BigDecimal pressao;

	private Unidade unidadePressao;

	private Collection<RedeDistribuicaoTronco> listaRedeTronco = new HashSet<>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# getListaRedeTronco()
	 */
	@Override
	public Collection<RedeDistribuicaoTronco> getListaRedeTronco() {

		return listaRedeTronco;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# setListaRedeTronco(java.util.Collection)
	 */
	@Override
	public void setListaRedeTronco(Collection<RedeDistribuicaoTronco> listaRedeTronco) {

		this.listaRedeTronco = listaRedeTronco;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# getLocalidade()
	 */
	@Override
	public Localidade getLocalidade() {

		return localidade;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# setLocalidade (br.com.ggas.cadastro.localidade .Localidade)
	 */
	@Override
	public void setLocalidade(Localidade localidade) {

		this.localidade = localidade;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# getRedeDiametro()
	 */
	@Override
	public RedeDiametro getRedeDiametro() {

		return redeDiametro;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# setRedeDiametro (br.com.ggas.cadastro.operacional .RedeDiametro)
	 */
	@Override
	public void setRedeDiametro(RedeDiametro redeDiametro) {

		this.redeDiametro = redeDiametro;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# getRedeMaterial()
	 */
	@Override
	public RedeMaterial getRedeMaterial() {

		return redeMaterial;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# setRedeMaterial (br.com.ggas.cadastro.operacional .RedeMaterial)
	 */
	@Override
	public void setRedeMaterial(RedeMaterial redeMaterial) {

		this.redeMaterial = redeMaterial;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# getExtensao()
	 */
	@Override
	public BigDecimal getExtensao() {

		return extensao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# setExtensao(java.math.BigDecimal)
	 */
	@Override
	public void setExtensao(BigDecimal extensao) {

		this.extensao = extensao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# getUnidadeExtensao()
	 */
	@Override
	public Unidade getUnidadeExtensao() {

		return unidadeExtensao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# setUnidadeExtensao (br.com.ggas.cadastro.unidade.UnidadeTipo)
	 */
	@Override
	public void setUnidadeExtensao(Unidade unidadeExtensao) {

		this.unidadeExtensao = unidadeExtensao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede#getPressao ()
	 */
	@Override
	public BigDecimal getPressao() {

		return pressao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede#setPressao (java.math.BigDecimal)
	 */
	@Override
	public void setPressao(BigDecimal pressao) {

		this.pressao = pressao;
	}

	@Override
	public String getValorPressaoFormatado() {
		if(this.pressao != null){
			return Util.converterCampoValorDecimalParaString("", this.pressao, Constantes.LOCALE_PADRAO, NUMERO_CASAS_DECIMAIS);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# getUnidadePressao()
	 */
	@Override
	public Unidade getUnidadePressao() {

		return unidadePressao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.localidade.Rede# setUnidadePressao (br.com.ggas.cadastro.unidade.UnidadeTipo)
	 */
	@Override
	public void setUnidadePressao(Unidade unidadePressao) {

		this.unidadePressao = unidadePressao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (extensao == null || unidadeExtensao == null) {
			stringBuilder.append(UNIDADE_EXTENSAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (pressao == null || unidadePressao == null) {
			stringBuilder.append(UNIDADE_PRESSAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (localidade == null) {
			stringBuilder.append(LOCALIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (redeDiametro == null) {
			stringBuilder.append(REDE_DIAMETRO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (redeMaterial == null) {
			stringBuilder.append(REDE_MATERIAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

}
