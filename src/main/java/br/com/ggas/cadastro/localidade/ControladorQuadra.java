/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * ao controladorQandra
 *
 */
public interface ControladorQuadra extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_QUADRA = "controladorQuadra";

	String ERRO_NEGOCIO_QUADRA_NO_SETOR_COMERCIAL_EXISTENTE = "ERRO_NEGOCIO_QUADRA_NO_SETOR_COMERCIAL_EXISTENTE";

	String ERRO_NEGOCIO_NUMERO_QUADRA_EXISTENTE = "ERRO_NEGOCIO_NUMERO_QUADRA_EXISTENTE";

	String ERRO_NEGOCIO_NAO_E_POSSIVEL_ALTERAR_INTERLIGADO_PONTENCIAL = "ERRO_NEGOCIO_NAO_E_POSSIVEL_ALTERAR_INTERLIGADO_PONTENCIAL";

	String ERRO_A_QUADRA_ESTA_EM_USO = "ERRO_A_QUADRA_ESTA_EM_USO";

	/**
	 * Método responsável por consultar as quadras
	 * pelo filtro informado.
	 * 
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção quadras.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Quadra> consultarQuadras(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se foi
	 * selecionada alguma quadra para
	 * exclusão.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias das quadras
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarQuadrasSelecionadas(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por verificar se foi
	 * selecionada apenas uma Quadra para
	 * alteração.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias das Quadras
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarSelecaoDeAtualizacao(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por criar uma entidade
	 * QuadraFace.
	 * 
	 * @return Uma EntidadeNegóciode QuadraFace.
	 */
	EntidadeNegocio criarQuadraFace();

	/**
	 * Método responsável por consultar os Zeis
	 * vinculados as quadras cadastradas.
	 * 
	 * @return coleção de zeis das quadras.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Zeis> consultarZeisDasQuadras() throws NegocioException;

	/**
	 * Método responsável por consultar os perfis
	 * vinculados as quadras cadastradas.
	 * 
	 * @return coleção de perfis das quadras.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<PerfilQuadra> consultarPerfilDasQuadrasCadastradas() throws NegocioException;

	/**
	 * Método responsável por validar a face da
	 * quadra.
	 * 
	 * @param quadraFace
	 *            A face da quadra
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarDadosQuadraFace(QuadraFace quadraFace) throws NegocioException;

	/**
	 * Método responsável por consultar as quadras
	 * pelo cep passado por parâmetro.
	 * 
	 * @param cep
	 *            O número do cep.
	 * @return Um mapa de String
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Quadra> listarQuadraPorCep(String cep) throws NegocioException;

	/**
	 * método responsável por recuperar uma face
	 * de quadra pela chave primária.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da face de
	 *            quadra;
	 * @return A face da quadra.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public QuadraFace obterQuadraFace(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por verificar se já
	 * existe uma face da quadra com o mesmo
	 * número de endereço na
	 * lista de faces da quadra.
	 * 
	 * @param indexLista
	 *            O índice da face da quadra na
	 *            lista.
	 * @param listaQuadraFace
	 *            Lista de faces da quadra da
	 *            quadra.
	 * @param quadraFace
	 *            Face da quadra a ser verificada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarNumeroQuadraFace(Integer indexLista, Collection<QuadraFace> listaQuadraFace, QuadraFace quadraFace) throws NegocioException;

	/**
	 * método responsável por recuperar uma
	 * coleção de city gate pela chave primária da
	 * quadra Face.
	 * 
	 * @param idQuadraFace
	 *            the id quadra face
	 * @return uma coleção de city gates
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CityGate> listarCityGatePorQuadraFace(long idQuadraFace) throws NegocioException;

	/**
	 * Método responsável por obter uma quadra a
	 * partir da chave primaria da quadraFace
	 * informada.
	 * 
	 * @param idQuadraFace
	 *            A chave primária da quadraFace
	 * @return Uma Quadra Face
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Quadra obterQuadraPorFaceQuadra(Long idQuadraFace) throws NegocioException;

	/**
	 * Obter quadra face por quadra.
	 * 
	 * @param idQuadra
	 *            the id quadra
	 * @param cep
	 *            the cep
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<QuadraFace> obterQuadraFacePorQuadra(Long idQuadra, String cep) throws NegocioException;

	/**
	 * Alterar situacao imoveis factivel.
	 * 
	 * @param idQuadraFace
	 *            the id quadra face
	 * @return the collection
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Imovel> alterarSituacaoImoveisFactivel(long idQuadraFace) throws NegocioException;

	/**
	 * Alterar situacao imoveis pontencial.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Imovel> alterarSituacaoImoveisPontencial(long chavePrimaria) throws NegocioException;

	/**
	 * Obter imoveis por quadra face.
	 * 
	 * @param idQuadreFace
	 *            the id quadre face
	 * @return the collection
	 */
	Collection<Imovel> obterImoveisPorQuadraFace(long idQuadreFace);

	/**
	 * Alterar situacao imoveis.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Imovel> alterarSituacaoImoveis(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Remover quadras.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerQuadras(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Atualizar quadra.
	 * 
	 * @param quadra
	 *            the quadra
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarQuadra(Quadra quadra) throws NegocioException, ConcorrenciaException;
	
	/**
		 * Buscar Quadra
	 * 
		 * @param idCep Identificador do cep para consulta
		 * @return Retorna a lista de quadras para um determinado cep
		 * @throws NegocioException
	 *             the negocio exception
		 */
	Collection<Quadra> listarQuadraPorIdCep(Long idCep) throws NegocioException;
	
	/**
	 * Obter quadra face por quadra.
	 * 
	 * @param idQuadra
	 *            the id quadra
	 * @param idCep
	 *            the cep
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<QuadraFace> obterQuadraFacePorQuadraIdCep(Long idQuadra, String idCep) throws NegocioException;


}
