/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.localidade.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 *
 * Classe responsável por implementar métodos de atributos
 * e método de validação de GerenciaRegional
 *
 */
public class GerenciaRegionalImpl extends EntidadeNegocioImpl implements GerenciaRegional {

	private static final int LIMITE_CAMPO = 2;

	private static final int TAMANHO_MINIMO_NOME_ABREVIADO = 5;

	private static final int TAMANHO_MINIMO_NOME = 50;

	private static final long serialVersionUID = 3059671688015697774L;

	private String nome;

	private String nomeAbreviado;

	private String fone;

	private String ramalFone;

	private String fax;

	private String email;

	private Endereco endereco;

	private Integer codigoFaxDDD;

	private Integer codigoFoneDDD;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#getCodigoFaxDDD()
	 */
	@Override
	public Integer getCodigoFaxDDD() {

		return codigoFaxDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional
	 * #setCodigoFaxDDD(java.lang.Integer)
	 */
	@Override
	public void setCodigoFaxDDD(Integer codigoFaxDDD) {

		this.codigoFaxDDD = codigoFaxDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#getCodigoFoneDDD()
	 */
	@Override
	public Integer getCodigoFoneDDD() {

		return codigoFoneDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional
	 * #setCodigoFoneDDD(java.lang.Integer)
	 */
	@Override
	public void setCodigoFoneDDD(Integer codigoFoneDDD) {

		this.codigoFoneDDD = codigoFoneDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#getNome()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#getNomeAbreviado()
	 */
	@Override
	public String getNomeAbreviado() {

		return nomeAbreviado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional
	 * #setNomeAbreviado(java.lang.String)
	 */
	@Override
	public void setNomeAbreviado(String nomeAbreviado) {

		this.nomeAbreviado = nomeAbreviado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#getFone()
	 */
	@Override
	public String getFone() {

		return fone;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#setFone(java.lang.String)
	 */
	@Override
	public void setFone(String fone) {

		this.fone = fone;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#getRamalFone()
	 */
	@Override
	public String getRamalFone() {

		return ramalFone;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional
	 * #setRamalFone(java.lang.String)
	 */
	@Override
	public void setRamalFone(String ramalFone) {

		this.ramalFone = ramalFone;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#getFax()
	 */
	@Override
	public String getFax() {

		return fax;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#setFax(java.lang.String)
	 */
	@Override
	public void setFax(String fax) {

		this.fax = fax;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#getEmail()
	 */
	@Override
	public String getEmail() {

		return email;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(String email) {

		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional#getEndereco()
	 */
	@Override
	public Endereco getEndereco() {

		return endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * GerenciaRegional
	 * #setEndereco(br.com.ggas.cadastro
	 * .endereco.Endereco)
	 */
	@Override
	public void setEndereco(Endereco endereco) {

		this.endereco = endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder camposObrigatoriosAcumulados = new StringBuilder();
		StringBuilder tamanhoCamposAcumulados = new StringBuilder();
		String camposObrigatorios = null;
		String camposInvalidos = null;

		if(StringUtils.isEmpty(nome)) {
			camposObrigatoriosAcumulados.append(NOME);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		} else if(nome.trim().length() > TAMANHO_MINIMO_NOME) {
			tamanhoCamposAcumulados.append(NOME);
			tamanhoCamposAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(nomeAbreviado)) {
			camposObrigatoriosAcumulados.append(NOME_ABREVIADO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		} else if(nomeAbreviado.trim().length() > TAMANHO_MINIMO_NOME_ABREVIADO) {
			tamanhoCamposAcumulados.append(NOME_ABREVIADO);
			tamanhoCamposAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		// TODO pburlamaqui/tsilva validar cep e
		// numero de endereco

		camposObrigatorios = camposObrigatoriosAcumulados.toString();
		camposInvalidos = tamanhoCamposAcumulados.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
							camposObrigatorios.substring(0, camposObrigatoriosAcumulados.toString().length() - LIMITE_CAMPO));
		}

		if(camposInvalidos.length() > 0) {
			erros.put(Constantes.ERRO_TAMANHO_LIMITE, camposInvalidos.substring(0, tamanhoCamposAcumulados.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

}
