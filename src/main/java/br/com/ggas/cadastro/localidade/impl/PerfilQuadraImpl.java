/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.localidade.PerfilQuadra;
import br.com.ggas.geral.tabelaAuxiliar.impl.TabelaAuxiliarImpl;
import br.com.ggas.util.Constantes;

/**
 * 
 * Classe responsável por implementar método de validação
 * do perfilQuadra
 * 
 */
public class PerfilQuadraImpl extends TabelaAuxiliarImpl implements PerfilQuadra {

	private static final int LIMITE_CAMPO = 2;
	private static final int TAMANHO_MINIMO_DESCRICAO = 20;
	private static final long serialVersionUID = 3344191260212537420L;

	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder camposObrigatoriosAcumulados = new StringBuilder();
		StringBuilder tamanhoCamposAcumulados = new StringBuilder();
		String camposObrigatorios = null;
		String camposInvalidos = null;

		if(StringUtils.isEmpty(super.getDescricao())) {
			camposObrigatoriosAcumulados.append(DESCRICAO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		} else if(super.getDescricao().length() > TAMANHO_MINIMO_DESCRICAO) {
			tamanhoCamposAcumulados.append(DESCRICAO);
			tamanhoCamposAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = camposObrigatoriosAcumulados.toString();
		camposInvalidos = tamanhoCamposAcumulados.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
							camposObrigatorios.substring(0, camposObrigatoriosAcumulados.toString().length() - LIMITE_CAMPO));
		}

		if(camposInvalidos.length() > 0) {
			erros.put(Constantes.ERRO_TAMANHO_LIMITE, camposInvalidos.substring(0, tamanhoCamposAcumulados.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}
}
