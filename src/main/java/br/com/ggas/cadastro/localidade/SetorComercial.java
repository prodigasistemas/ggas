/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade;

import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Classe responsável pela representação de um setor comercial
 *
 */
public interface SetorComercial extends EntidadeNegocio {

	String BEAN_ID_SETOR_COMERCIAL = "setorComercial";

	String SETOR_COMERCIAL = "SETOR_COMERCIAL_ROTULO";

	String SETORES_COMERCIAIS = "SETORES_COMERCIAIS_ROTULO";

	String CODIGO = "SETOR_COMERCIAL_CODIGO";

	String DESCRICAO = "SETOR_COMERCIAL_DESCRICAO";

	String MUNICIPIO = "SETOR_COMERCIAL_MUNICIPIO";

	String LOCALIDADE = "SETOR_COMERCIAL_LOCALIDADE";

	/**
	 * @return the codigo
	 */
	String getCodigo();

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	void setCodigo(String codigo);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the municipio
	 */
	Municipio getMunicipio();

	/**
	 * @param municipio
	 *            the municipio to set
	 */
	void setMunicipio(Municipio municipio);

	/**
	 * @return the localidade
	 */
	Localidade getLocalidade();

	/**
	 * @param localidade
	 *            the localidade to set
	 */
	void setLocalidade(Localidade localidade);

}
