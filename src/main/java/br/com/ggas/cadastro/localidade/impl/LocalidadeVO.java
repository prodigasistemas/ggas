package br.com.ggas.cadastro.localidade.impl;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.LocalidadeClasse;
import br.com.ggas.cadastro.localidade.LocalidadePorte;
import br.com.ggas.cadastro.localidade.UnidadeNegocio;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;

/**
 *LocalidadeVO
 * 
 * @author pedro
 * 
 * Classe responsável pela transferência de dados entre as 
 * telas relacionadas a Localidade.
 * 
 */
public class LocalidadeVO {

	private Long chavePrimaria;
	private Long codigo;
	private Integer versao;
	private String descricao;
	private GerenciaRegional gerenciaRegional;
	private String numeroEndereco;
	private String cep;
	private String complementoEndereco;
	private String fone;
	private String ramalFone;
	private String fax;
	private String email;
	private UnidadeNegocio unidadeNegocio;
	private LocalidadeClasse localidadeClasse;
	private LocalidadePorte localidadePorte;
	private MedidorLocalArmazenagem medidorLocalArmazenagem;
	private Cliente cliente; 
	private String codigoCentroCusto;
	private boolean informatizada;
	private Boolean habilitado;
	private Integer dddTelefone;
	private Integer dddFax;
	private Long chaveCep;
	
	/**
	 * 
	 * @return chavePrimaria
	 */
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	/**
	 * 
	 * @param chavePrimaria
	 */
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	/**
	 * 
	 * @return codigo
	 */
	public Long getCodigo() {
		return codigo;
	}
	/**
	 * 
	 * @param codigo
	 */
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	/**
	 * 
	 * @return versao
	 */
	public Integer getVersao() {
		return versao;
	}
	/**
	 * 
	 * @param versao
	 */
	public void setVersao(Integer versao) {
		this.versao = versao;
	}
	/**
	 * 
	 * @return descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * 
	 * @param descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * 
	 * @return gerenciaRegional
	 */
	public GerenciaRegional getGerenciaRegional() {
		return gerenciaRegional;
	}
	/**
	 * 
	 * @param gerenciaRegional
	 */
	public void setGerenciaRegional(GerenciaRegional gerenciaRegional) {
		this.gerenciaRegional = gerenciaRegional;
	}
	/**
	 * 
	 * @return numeroEndereco
	 */
	public String getNumeroEndereco() {
		return numeroEndereco;
	}
	/**
	 * 
	 * @param numeroEndereco
	 */
	public void setNumeroEndereco(String numeroEndereco) {
		this.numeroEndereco = numeroEndereco;
	}
	/**
	 * 
	 * @return cep
	 */
	public String getCep() {
		return cep;
	}
	/**
	 * 
	 * @param numeroCep
	 */
	public void setCep(String numeroCep) {
		this.cep = numeroCep;
	}
	/**
	 * 
	 * @return complementoEndereco
	 */
	public String getComplementoEndereco() {
		return complementoEndereco;
	}
	/**
	 * 
	 * @param complementoEndereco
	 */
	public void setComplementoEndereco(String complementoEndereco) {
		this.complementoEndereco = complementoEndereco;
	}
	/**
	 * 
	 * @return fone
	 */
	public String getFone() {
		return fone;
	}
	/**
	 * 
	 * @param fone
	 */
	public void setFone(String fone) {
		this.fone = fone;
	}
	/**
	 * 
	 * @return ramalFone
	 */
	public String getRamalFone() {
		return ramalFone;
	}
	/**
	 * 
	 * @param ramalFone
	 */
	public void setRamalFone(String ramalFone) {
		this.ramalFone = ramalFone;
	}
	/**
	 * 
	 * @return fax
	 */
	public String getFax() {
		return fax;
	}
	/**
	 * 
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	/**
	 * 
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * 
	 * @return unidadeNegocio
	 */
	public UnidadeNegocio getUnidadeNegocio() {
		return unidadeNegocio;
	}
	/**
	 * 
	 * @param unidadeNegocio
	 */
	public void setUnidadeNegocio(UnidadeNegocio unidadeNegocio) {
		this.unidadeNegocio = unidadeNegocio;
	}
	/**
	 * 
	 * @return localidadeClasse
	 */
	public LocalidadeClasse getLocalidadeClasse() {
		return localidadeClasse;
	}
	/**
	 * 
	 * @param localidadeClasse
	 */
	public void setLocalidadeClasse(LocalidadeClasse localidadeClasse) {
		this.localidadeClasse = localidadeClasse;
	}
	/**
	 * 
	 * @return localidadePorte
	 */
	public LocalidadePorte getLocalidadePorte() {
		return localidadePorte;
	}
	/**
	 * 
	 * @param localidadePorte
	 */
	public void setLocalidadePorte(LocalidadePorte localidadePorte) {
		this.localidadePorte = localidadePorte;
	}
	/**
	 * 
	 * @return medidorLocalArmazenagem
	 */
	public MedidorLocalArmazenagem getMedidorLocalArmazenagem() {
		return medidorLocalArmazenagem;
	}
	/**
	 * 
	 * @param medidorLocalArmazenagem
	 */
	public void setMedidorLocalArmazenagem(MedidorLocalArmazenagem medidorLocalArmazenagem) {
		this.medidorLocalArmazenagem = medidorLocalArmazenagem;
	}
	/**
	 * 
	 * @return cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}
	/**
	 * 
	 * @param cliente
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	/**
	 * 
	 * @return codigoCentroCusto
	 */
	public String getCodigoCentroCusto() {
		return codigoCentroCusto;
	}
	/**
	 * 
	 * @param codigoCentroCusto
	 */
	public void setCodigoCentroCusto(String codigoCentroCusto) {
		this.codigoCentroCusto = codigoCentroCusto;
	}
	/**
	 * 
	 * @return informatizada
	 */
	public boolean getInformatizada() {
		return informatizada;
	}
	/**
	 * 
	 * @param informatizada
	 */
	public void setInformatizada(boolean informatizada) {
		this.informatizada = informatizada;
	}
	/**
	 * 
	 * @return habilitado
	 */
	public Boolean getHabilitado() {
		return habilitado;
	}
	/**
	 * 
	 * @param habilitado
	 */
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}
	/**
	 * 
	 * @return dddTelefone
	 */
	public Integer getDddTelefone() {
		return dddTelefone;
	}
	/**
	 * 
	 * @param integer
	 */
	public void setDddTelefone(Integer integer) {
		this.dddTelefone = integer;
	}
	/**
	 * 
	 * @return dddFax
	 */
	public Integer getDddFax() {
		return dddFax;
	}
	/**
	 * 
	 * @param integer
	 */
	public void setDddFax(Integer integer) {
		this.dddFax = integer;
	}
	/**
	 * 
	 * @return chaveCep
	 */
	public Long getChaveCep() {
		return chaveCep;
	}
	/**
	 * 
	 * @param chaveCep
	 */
	public void setChaveCep(Long chaveCep) {
		this.chaveCep = chaveCep;
	}
	

}
