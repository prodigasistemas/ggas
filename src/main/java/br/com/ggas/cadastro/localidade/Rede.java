/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade;

import java.math.BigDecimal;
import java.util.Collection;

import br.com.ggas.cadastro.operacional.RedeDiametro;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.vazaocorretor.Unidade;

/**
 * Classe responsável pela representação da rede
 * 
 */
public interface Rede extends EntidadeNegocio {

	String BEAN_ID_REDE = "rede";

	String REDE_CAMPO_STRING = "REDE";

	String DESCRICAO = "REDE_DESCRICAO";

	String LOCALIDADE = "REDE_LOCALIDADE";

	String REDE_DIAMETRO = "REDE_REDE_DIAMETRO";

	String REDE_MATERIAL = "REDE_REDE_MATERIAL";

	String EXTENSAO = "REDE_EXTENSAO";

	String PRESSAO = "REDE_PRESSAO";

	String REDE_ENDERECO = "REDE_ENDERECO";

	String UNIDADE_EXTENSAO = "UNIDADE_EXTENSAO";

	String UNIDADE_PRESSAO = "UNIDADE_PRESSAO";

	String[] PROPRIEDADES_LAZY = new String[] { "listaRedeTronco", "unidadeExtensao", "unidadePressao" };

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the localidade
	 */
	public Localidade getLocalidade();

	/**
	 * @param localidade the localidade to set
	 */
	void setLocalidade(Localidade localidade);

	/**
	 * @return the redeDiametro
	 */
	RedeDiametro getRedeDiametro();

	/**
	 * @param redeDiametro the redeDiametro to set
	 */
	void setRedeDiametro(RedeDiametro redeDiametro);

	/**
	 * @return the redeMaterial
	 */
	RedeMaterial getRedeMaterial();

	/**
	 * @param redeMaterial the redeMaterial to set
	 */
	void setRedeMaterial(RedeMaterial redeMaterial);

	/**
	 * @return the extensao
	 */
	BigDecimal getExtensao();

	/**
	 * @param extensao the extensao to set
	 */
	void setExtensao(BigDecimal extensao);

	/**
	 * @return the unidadeExtensao
	 */
	Unidade getUnidadeExtensao();

	/**
	 * @param unidadeExtensao the unidadeExtensao to set
	 */
	void setUnidadeExtensao(Unidade unidadeExtensao);

	/**
	 * @return the pressao
	 */
	BigDecimal getPressao();

	/**
	 * @param pressao the pressao to set
	 */
	void setPressao(BigDecimal pressao);

	/**
	 * @return pressao formatada
	 */
	String getValorPressaoFormatado();

	/**
	 * @return unidadePressao
	 */
	Unidade getUnidadePressao();

	/**
	 * @param unidadePressao the unidadePressao to set
	 */
	void setUnidadePressao(Unidade unidadePressao);

	/**
	 * @return the listaRedeTronco
	 */
	Collection<RedeDistribuicaoTronco> getListaRedeTronco();

	/**
	 * @param listaRedeTronco the listaRedeTronco to set
	 */
	void setListaRedeTronco(Collection<RedeDistribuicaoTronco> listaRedeTronco);

}
