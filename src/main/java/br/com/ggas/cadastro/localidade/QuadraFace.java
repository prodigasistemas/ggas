/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade;

import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * 
 * Inerface responsável pela assinatura dos métodos relacionados
 * à quadraFace
 * 
 */
public interface QuadraFace extends EntidadeNegocio {

	String BEAN_ID_QUADRA_FACE = "quadraFace";

	String QUADRA = "QUADRA_FACE_QUADRA";

	String REDE_INDICADOR = "QUADRA_FACE_REDE_INDICADOR";

	String REDE = "QUADRA_FACE_REDE";

	String CEP = "QUADRA_FACE_CEP";

	String NUMERO = "QUADRA_FACE_NUMERO";

	String COMPLEMENTO = "QUADRA_FACE_COMPLEMENTO";

	/**
	 * @return the endereco
	 */
	Endereco getEndereco();

	/**
	 * @param endereco
	 *            the endereco to set
	 */
	void setEndereco(Endereco endereco);

	/**
	 * @return the redeIndicador
	 */
	RedeIndicador getRedeIndicador();

	/**
	 * @param redeIndicador
	 *            the redeIndicador to set
	 */
	void setRedeIndicador(RedeIndicador redeIndicador);

	/**
	 * @return the rede
	 */
	Rede getRede();

	/**
	 * @param rede
	 *            the rede to set
	 */
	void setRede(Rede rede);

	/**
	 * @return the quadra
	 */
	Quadra getQuadra();

	/**
	 * @param quadra
	 *            the quadra to set
	 */
	void setQuadra(Quadra quadra);

	/**
	 * Obtém uma descrição formatada da face de
	 * quadra.
	 * 
	 * @return descrição formatada da face de
	 *         quadra.
	 */

	String getQuadraFaceFormatada();
}
