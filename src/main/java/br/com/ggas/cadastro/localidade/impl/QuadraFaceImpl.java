/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe QuadraFaceImpl representa uma QuadraFaceImpl no sistema.
 *
 * @since 13/08/2009
 * 
 */

package br.com.ggas.cadastro.localidade.impl;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.localidade.Rede;
import br.com.ggas.cadastro.localidade.RedeIndicador;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 *	Classe responsável pela face da quadra, e seus atributos. 
 *
 */
public class QuadraFaceImpl extends EntidadeNegocioImpl implements QuadraFace {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 5682534604366131600L;

	private Quadra quadra;

	private Endereco endereco;

	private RedeIndicador redeIndicador;

	private Rede rede;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.QuadraFace
	 * #getEndereco()
	 */
	@Override
	public Endereco getEndereco() {

		return endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.QuadraFace
	 * #setEndereco
	 * (br.com.ggas.cadastro.endereco.Endereco)
	 */
	@Override
	public void setEndereco(Endereco endereco) {

		this.endereco = endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.QuadraFace
	 * #getRedeIndicador()
	 */
	@Override
	public RedeIndicador getRedeIndicador() {

		return redeIndicador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.QuadraFace
	 * #setRedeIndicador
	 * (br.com.ggas.cadastro.localidade
	 * .RedeIndicador)
	 */
	@Override
	public void setRedeIndicador(RedeIndicador redeIndicador) {

		this.redeIndicador = redeIndicador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.QuadraFace
	 * #getRede()
	 */
	@Override
	public Rede getRede() {

		return rede;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.QuadraFace
	 * #setRede
	 * (br.com.ggas.cadastro.localidade.Rede)
	 */
	@Override
	public void setRede(Rede rede) {

		this.rede = rede;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.QuadraFace
	 * #getQuadra()
	 */
	@Override
	public Quadra getQuadra() {

		return quadra;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.QuadraFace
	 * #setQuadra
	 * (br.com.ggas.cadastro.localidade.Quadra)
	 */
	@Override
	public void setQuadra(Quadra quadra) {

		this.quadra = quadra;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (redeIndicador == null) {
			stringBuilder.append(REDE_INDICADOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else if (redeIndicador.getCodigo() != RedeIndicador.NAO_TEM && rede == null) {
			stringBuilder.append(REDE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(endereco == null || endereco.getCep() == null) {
			stringBuilder.append(CEP);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(endereco == null || endereco.getNumero() == null || endereco.getNumero().length() <= 0) {
			stringBuilder.append(NUMERO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.QuadraFace
	 * #getQuadraFaceFormatada()
	 */
	@Override
	public String getQuadraFaceFormatada() {

		String retorno = "";

		if(this.getEndereco() != null) {
			retorno = this.getEndereco().getNumero();
		}

		return retorno;
	}

}
