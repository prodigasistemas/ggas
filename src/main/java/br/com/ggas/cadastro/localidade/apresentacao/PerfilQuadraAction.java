/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade.apresentacao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.localidade.PerfilQuadra;
import br.com.ggas.cadastro.localidade.impl.PerfilQuadraImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;

/**
 * Controller responsável por exibir as telas da entidade PerfilQuadra.
 */

@Controller
public class PerfilQuadraAction extends GenericAction {

	private static final String EXIBIR_PESQUISA_PERFIL_QUADRA = "exibirPesquisaPerfilQuadra";

	private static final String PERFIL_QUADRA = "perfilQuadra";

	private static final String EXIBIR_ATUALIZAR_PERFIL_QUADRA = "exibirAtualizarPerfilQuadra";

	private static final String EXIBIR_INSERIR_PERFIL_QUADRA = "exibirInserirPerfilQuadra";

	private static final Class<PerfilQuadraImpl> CLASSE = PerfilQuadraImpl.class;
	
	private static final String CLASSE_STRING = "br.com.ggas.cadastro.localidade.impl.PerfilQuadraImpl";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String LISTA_PERFIL_QUADRA = "listaPerfilQuadra";

	@Autowired
	@Qualifier(ControladorTabelaAuxiliar.BEAN_ID_CONTROLADOR_TABELA_AUXILIAR)
	private ControladorTabelaAuxiliar controladorTabelaAuxiliar;

	/**
	 * Método responsável por exibir a tela de pesquisar perfil quadra.
	 * 
	 * @param model {@link Model}
	 * @param session {@link HttpSession}
	 * @return String {@link String}
	 */
	@RequestMapping(EXIBIR_PESQUISA_PERFIL_QUADRA)
	public String exibirPesquisaPerfilQuadra(Model model, HttpSession session) {

		if (!model.containsAttribute(HABILITADO)) {
			model.addAttribute(HABILITADO, Boolean.TRUE);
		}

		return EXIBIR_PESQUISA_PERFIL_QUADRA;
	}

	/**
	 * Método responsável por exibir o resultado da pesquisa de perfil quadra.
	 * 
	 * @param perfilQuadra {@link PerfilQuadraImpl}
	 * @param result {@link BindingResult}
	 * @param habilitado {@link Boolean}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarPerfilQuadra")
	public String pesquisarPerfilQuadra(PerfilQuadraImpl perfilQuadra, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		Map<String, Object> filtro = prepararFiltro(perfilQuadra, habilitado);

		Collection<TabelaAuxiliar> listaPerfilQuadra = controladorTabelaAuxiliar.pesquisarTabelaAuxiliar(filtro, CLASSE_STRING);

		model.addAttribute(LISTA_PERFIL_QUADRA, listaPerfilQuadra);
		model.addAttribute(PERFIL_QUADRA, perfilQuadra);
		model.addAttribute(HABILITADO, habilitado);

		return EXIBIR_PESQUISA_PERFIL_QUADRA;
	}

	/**
	 * Método responsável por exibir a tela de detalhamento perfil quadra.
	 * 
	 * @param chavePrimaria {@link Long}
	 * @param habilitado {@link Boolean}
	 * @param model {@link Model}
	 * @return String {@link String}
	 */
	@RequestMapping("exibirDetalhamentoPerfilQuadra")
	public String exibirDetalhamentoPerfilQuadra(@RequestParam(value = "chavePrimaria", required = false) Long chavePrimaria,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) {

		try {
			PerfilQuadraImpl perfilQuadra = (PerfilQuadraImpl) controladorTabelaAuxiliar.obter(chavePrimaria, CLASSE);
			model.addAttribute(PERFIL_QUADRA, perfilQuadra);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaPerfilQuadra";
		}

		return "exibirDetalhamentoPerfilQuadra";
	}

	/**
	 * Método responsável por exibir a tela de inserir perfil quadra.
	 * 
	 * @return String
	 */
	@RequestMapping(EXIBIR_INSERIR_PERFIL_QUADRA)
	public String exibirInserirEquipamento() {

		return EXIBIR_INSERIR_PERFIL_QUADRA;
	}

	/**
	 * Método responsável por inserir perfil quadra.
	 * 
	 * @param perfilQuadra
	 * @param result
	 * @param habilitado
	 * @param model
	 * @param request
	 * @return String
	 * @throws Exception
	 */
	@RequestMapping("inserirPerfilQuadra")
	private String inserirPerfilQuadra(PerfilQuadraImpl perfilQuadra, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model, HttpServletRequest request)
					throws GGASException {

		String retorno = null;

		model.addAttribute(PERFIL_QUADRA, perfilQuadra);

		if (!perfilQuadra.validarDados().isEmpty()) {
			mensagemErroParametrizado(model, request, new NegocioException(perfilQuadra.validarDados()));
			return EXIBIR_INSERIR_PERFIL_QUADRA;
		}

		try {
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(perfilQuadra, "Perfil Quadra");
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(perfilQuadra);
			controladorTabelaAuxiliar.inserir(perfilQuadra);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, request, PerfilQuadra.PERFIL_QUADRA);
			retorno = pesquisarPerfilQuadra(perfilQuadra, result, perfilQuadra.isHabilitado(), model);
		} catch (NegocioException e) {
			retorno = EXIBIR_INSERIR_PERFIL_QUADRA;
			mensagemErroParametrizado(model, request, e);
		}

		return retorno;
	}

	/**
	 * Método responsável por exibir a tela de atualizar perfil quadra.
	 * 
	 * @param perfilQuadra {@link PerfilQuadraImpl}
	 * @param result {@link BindingResult}
	 * @param habilitado {@link Boolean}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping(EXIBIR_ATUALIZAR_PERFIL_QUADRA)
	public String exibirAtualizarPerfilQuadra(PerfilQuadraImpl perfilQuadra, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, Model model) throws GGASException {

		PerfilQuadraImpl perfil = perfilQuadra;

		try {
			perfil = (PerfilQuadraImpl) controladorTabelaAuxiliar.obter(perfilQuadra.getChavePrimaria(), CLASSE);
			model.addAttribute(PERFIL_QUADRA, perfil);
			model.addAttribute(HABILITADO, habilitado);
		} catch (NegocioException e) {
			mensagemErro(model, e);
			return "forward:exibirPesquisaPerfilQuadra";
		}

		return EXIBIR_ATUALIZAR_PERFIL_QUADRA;
	}

	/**
	 * Método responsável por remover o perfil quadra.
	 * 
	 * @param perfilQuadra
	 * @param result
	 * @param habilitado
	 * @param request
	 * @param model
	 * @return String
	 * @throws GGASException
	 * @throws ConcorrenciaException
	 */
	@RequestMapping("atualizarPerfilQuadra")
	private String atualizarPerfilQuadra(PerfilQuadraImpl perfilQuadra, BindingResult result,
					@RequestParam(value = HABILITADO, required = false) Boolean habilitado, HttpServletRequest request, Model model)
					throws GGASException {

		String retorno = null;

		try {
			controladorTabelaAuxiliar.pesquisaDescricaoTabelaAuxiliar(perfilQuadra, "Perfil Quadra");
			controladorTabelaAuxiliar.validarDadosTabelaAuxiliar(perfilQuadra);
			controladorTabelaAuxiliar.atualizar(perfilQuadra, CLASSE);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, request, PerfilQuadra.PERFIL_QUADRA);
			retorno = pesquisarPerfilQuadra(perfilQuadra, result, habilitado, model);
		} catch (NegocioException e) {
			model.addAttribute(PERFIL_QUADRA, perfilQuadra);
			mensagemErroParametrizado(model, request, e);
			retorno = EXIBIR_ATUALIZAR_PERFIL_QUADRA;
		}

		return retorno;
	}

	/**
	 * Método responsável por remover o perfil quadra.
	 * 
	 * @param chavesPrimarias {@link Long}
	 * @param request {@link HttpServletRequest}
	 * @param model {@link Model}
	 * @return String {@link String}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("removerPerfilQuadra")
	public String removerPerfilQuadra(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request, Model model)
					throws GGASException {

		DadosAuditoria dadosAuditoria = getDadosAuditoria(request);

		try {
			controladorTabelaAuxiliar.removerTabelaAuxiliar(chavesPrimarias, CLASSE, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, request, PerfilQuadra.PERFIL_QUADRA);
			return pesquisarPerfilQuadra(null, null, Boolean.TRUE, model);
		} catch (DataIntegrityViolationException e) {
			mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_INTEGRIDADE_RELACIONAL, e));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return "forward:pesquisarPerfilQuadra";
	}

	/**
	 * Método responsável por Preparar filtro.
	 * 
	 * @param perfilQuadra
	 * @param habilitado
	 * @return filtro
	 */
	private Map<String, Object> prepararFiltro(PerfilQuadraImpl perfilQuadra, Boolean habilitado) {

		Map<String, Object> filtro = new HashMap<>();

		if (perfilQuadra != null) {

			if (perfilQuadra.getChavePrimaria() > 0) {
				filtro.put(CHAVE_PRIMARIA, perfilQuadra.getChavePrimaria());
			}

			if (perfilQuadra.getDescricao() != null && !perfilQuadra.getDescricao().isEmpty()) {
				filtro.put(DESCRICAO, perfilQuadra.getDescricao());
			}
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		return filtro;
	}

}
