/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorQuadraImpl representa um ControladorQuadraImpl no sistema.
 *
 * @since 06/09/2009
 *
 */

package br.com.ggas.cadastro.localidade.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.SituacaoImovel;
import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.cadastro.localidade.PerfilQuadra;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.localidade.RedeIndicador;
import br.com.ggas.cadastro.localidade.Zeis;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.ControladorRede;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 *
 *
 */
class ControladorQuadraImpl extends ControladorNegocioImpl implements ControladorQuadra {
	
	private static final String IMOVEL = " imovel ";

	private static final String SELECT_DISTINCT = " select distinct";

	private static final String SELECT = " select ";

	private static final String QUADRA_FACE = " quadraFace ";

	private static final String WHERE = " where ";

	private static final String FROM = " from ";

	private static final Logger LOG = Logger.getLogger(ControladorQuadraImpl.class);

	private ControladorRede controladorRede;

	private ControladorConstanteSistema controladorConstanteSistema;

	private ControladorImovel controladorImovel;	
	

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Quadra.BEAN_ID_QUADRA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Quadra.BEAN_ID_QUADRA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#criarQuadraFace()
	 */
	@Override
	public EntidadeNegocio criarQuadraFace() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(QuadraFace.BEAN_ID_QUADRA_FACE);
	}

	public Class<?> getClasseEntidadeQuadraFace() {

		return ServiceLocator.getInstancia().getClassPorID(QuadraFace.BEAN_ID_QUADRA_FACE);
	}

	public Class<?> getClasseEntidadeImovel() {

		return ServiceLocator.getInstancia().getClassPorID(Imovel.BEAN_ID_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#consultarQuadras(java.util.Map)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<Quadra> consultarQuadras(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Long idSetorComercial = (Long) filtro.get("idSetorComercial");
			if(idSetorComercial != null && idSetorComercial > 0) {
				criteria.createAlias("setorComercial", "setorComercial");
				criteria.add(Restrictions.eq("setorComercial.chavePrimaria", idSetorComercial));
			} else {
				criteria.setFetchMode("setorComercial", FetchMode.JOIN);
			}

			String numeroQuadra = (String) filtro.get("numeroQuadra");
			if(numeroQuadra != null && numeroQuadra.length() > 0) {
				criteria.add(Restrictions.eq("numeroQuadra", numeroQuadra));
			}

			Long idPerfilQuadra = (Long) filtro.get("idPerfilQuadra");
			if(idPerfilQuadra != null && idPerfilQuadra > 0) {
				criteria.createAlias("perfilQuadra", "perfilQuadra");
				criteria.add(Restrictions.eq("perfilQuadra.chavePrimaria", idPerfilQuadra));
			} else {
				criteria.setFetchMode("perfilQuadra", FetchMode.JOIN);
			}

			Long idZeis = (Long) filtro.get("idZeis");
			if(idZeis != null && idZeis > 0) {
				criteria.createAlias("zeis", "zeis");
				criteria.add(Restrictions.eq("zeis.chavePrimaria", idZeis));
			} else {
				criteria.setFetchMode("zeis", FetchMode.JOIN);
			}

			Long idSetorCensitario = (Long) filtro.get("idSetorCensitario");
			if(idSetorCensitario != null && idSetorCensitario > 0) {
				criteria.createAlias("setorCensitario", "setorCensitario");
				criteria.add(Restrictions.eq("setorCensitario.chavePrimaria", idSetorCensitario));
			} else {
				criteria.setFetchMode("setorCensitario", FetchMode.JOIN);
			}

			Long idZonaBloqueio = (Long) filtro.get("idZonaBloqueio");
			if(idZonaBloqueio != null && idZonaBloqueio > 0) {
				criteria.createAlias("zonaBloqueio", "zonaBloqueio");
				criteria.add(Restrictions.eq("zonaBloqueio.chavePrimaria", idZonaBloqueio));
			} else {
				criteria.setFetchMode("zonaBloqueio", FetchMode.JOIN);
			}

			Long idAreaTipo = (Long) filtro.get("idAreaTipo");
			if(idAreaTipo != null && idAreaTipo > 0) {
				criteria.createAlias("areaTipo", "areaTipo");
				criteria.add(Restrictions.eq("areaTipo.chavePrimaria", idAreaTipo));
			} else {
				criteria.setFetchMode("areaTipo", FetchMode.JOIN);
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			String cep = (String) filtro.get("cep");
			if (StringUtils.isNotBlank(cep)) {
				cep = "%".concat(cep).concat("%");
				criteria.createAlias("quadrasFace", "quadrasFace").createAlias("quadrasFace.endereco.cep", "cep")
						.add(Restrictions.ilike("cep.cep", cep));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#consultarZeisDasQuadras()
	 */
	@Override
	public Collection<Zeis> consultarZeisDasQuadras() throws NegocioException {

		// TODO: [Todos] Refactory, quando a consultar tiver parametros fixos,
		// usar Query ao invés de Criteria
		Criteria criteria = getCriteria();
		criteria.setProjection(Projections.property("zeis"));
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.setProjection(Projections.groupProperty("zeis").as("z")).addOrder(Order.asc("z"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#consultarPerfilDasQuadrasCadastradas()
	 */
	@Override
	public Collection<PerfilQuadra> consultarPerfilDasQuadrasCadastradas() throws NegocioException {

		// TODO: [Todos] Refactory, quando a consultar tiver parametros fixos,
		// usar Query ao invés de Criteria
		Criteria criteria = getCriteria();
		criteria.setProjection(Projections.property("perfilQuadra"));
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.setProjection(Projections.groupProperty("perfilQuadra").as("perfil")).addOrder(Order.asc("perfil"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade. ControladorQuadra
	 * #validarQuadrasSelecionadas (java.lang.Long[])
	 */
	@Override
	public void validarQuadrasSelecionadas(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade. ControladorQuadra
	 * #validarSelecaoDeAtualizacao (java.lang.Long[])
	 */
	@Override
	public void validarSelecaoDeAtualizacao(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length != 1) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade. ControladorQuadra
	 * #validarDadosQuadraFace(br. com.ggas.cadastro.localidade.QuadraFace)
	 */
	@Override
	public void validarDadosQuadraFace(QuadraFace quadraFace) throws NegocioException {

		Map<String, Object> errosQuadraFace = quadraFace.validarDados();

		if(errosQuadraFace != null && !errosQuadraFace.isEmpty()) {
			throw new NegocioException(errosQuadraFace);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Quadra quadra = (Quadra) entidadeNegocio;
		validarQuadraExistente(quadra);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Quadra quadra = (Quadra) entidadeNegocio;
		validarQuadraExistente(quadra);
	}

	/**
	 * Validar quadra existente.
	 * 
	 * @param quadra
	 *            the quadra
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarQuadraExistente(Quadra quadra) throws NegocioException {

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("idSetorComercial", quadra.getSetorComercial().getChavePrimaria());
		filtro.put("numeroQuadra", quadra.getNumeroQuadra());

		List<Quadra> listaQuadrasAtual = (List<Quadra>) consultarQuadras(filtro);

		if(!listaQuadrasAtual.isEmpty()) {
			Quadra quadraExistente = listaQuadrasAtual.get(0);

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(quadraExistente);

			if(quadraExistente.getChavePrimaria() != quadra.getChavePrimaria()) {
				throw new NegocioException(ControladorQuadra.ERRO_NEGOCIO_QUADRA_NO_SETOR_COMERCIAL_EXISTENTE,
								new Object[] {quadra.getNumeroQuadra(), quadra.getSetorComercial().getDescricao()});
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#listarQuadraPorCep(java.lang.String)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<Quadra> listarQuadraPorCep(String cep) throws NegocioException {

		Collection<Quadra> listaQuadra = new ArrayList<>();
		Collection<Quadra> resultado = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct quadraFace.quadra, quadraFace.quadra.numeroQuadra ");
		hql.append(FROM);
		hql.append(getClasseEntidadeQuadraFace().getSimpleName());
		hql.append(QUADRA_FACE);
		hql.append(" where quadraFace.habilitado = true ");
		hql.append(" and  quadraFace.quadra.habilitado = true ");
		hql.append(" and quadraFace.endereco.cep.cep = ?");
		hql.append(" order by quadraFace.quadra.numeroQuadra asc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, cep);

		resultado = query.list();

		for (Object currentItem : resultado.toArray()) {
			Object[] arrayItem = (Object[]) currentItem;
			listaQuadra.add((Quadra) arrayItem[0]);
		}

		return listaQuadra;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#obterQuadraFace(long)
	 */
	@Override
	public QuadraFace obterQuadraFace(long chavePrimaria) throws NegocioException {

		return (QuadraFace) super.obter(chavePrimaria, getClasseEntidadeQuadraFace());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#validarNumeroQuadraFace(java.lang.Integer, java.util.Collection,
	 * br.com.ggas.cadastro.localidade.QuadraFace)
	 */
	@Override
	public void validarNumeroQuadraFace(Integer indexLista, Collection<QuadraFace> listaQuadraFace, QuadraFace quadraFace)
					throws NegocioException {

		for (int i = 0; i < listaQuadraFace.size(); i++) {

			QuadraFace faceNumeroExistente = ((List<QuadraFace>) listaQuadraFace).get(i);
			String numeroFace = null;
			String cep = null;

			if(faceNumeroExistente.getEndereco() != null) {
				numeroFace = faceNumeroExistente.getEndereco().getNumero();
				if(faceNumeroExistente.getEndereco().getCep() != null) {
					cep = faceNumeroExistente.getEndereco().getCep().getCep();
				}
			}

			if (numeroFace != null && numeroFace.equals(quadraFace.getEndereco().getNumero()) && cep != null
							&& cep.equals(quadraFace.getEndereco().getCep().getCep()) && indexLista != i) {
				throw new NegocioException(ERRO_NEGOCIO_NUMERO_QUADRA_EXISTENTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#listarCityGatePorQuadraFace(long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<CityGate> listarCityGatePorQuadraFace(long idQuadraFace) throws NegocioException {

		this.controladorRede = (ControladorRede) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorRede.BEAN_ID_CONTROLADOR_REDE);

		StringBuilder hql = new StringBuilder();

		hql.append(SELECT_DISTINCT);
		hql.append(" cityGate ");
		hql.append(FROM);
		hql.append(getClasseEntidadeQuadraFace().getSimpleName());
		hql.append(" quadraFace , ");
		hql.append(this.controladorRede.getClasseEntidadeRedeDistribuicaoTronco().getSimpleName());
		hql.append(" redeTronco ");
		hql.append(" inner join redeTronco.tronco tronco");
		hql.append(" inner join tronco.cityGate cityGate");
		hql.append(WHERE);
		hql.append(" quadraFace.chavePrimaria = ? ");
		hql.append(" and quadraFace.rede = redeTronco.rede ");
		hql.append(" order by cityGate.descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idQuadraFace);

		return query.list();
	}

	/**
	 * Método responsável por obter uma quadra a partir da chave primaria da
	 * quadraFace informada.
	 * 
	 * @param idQuadraFace
	 *            A chave primária da quadraFace
	 * @return Uma Quadra Face
	 * @throws NegocioException
	 *             Caso ocorra algum erro na invocação do método.
	 */
	@Override
	public Quadra obterQuadraPorFaceQuadra(Long idQuadraFace) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(SELECT);

		hql.append(QUADRA_FACE);
		hql.append(FROM);
		hql.append(getClasseEntidadeQuadraFace().getSimpleName());
		hql.append(QUADRA_FACE);
		hql.append(WHERE);
		hql.append(" quadraFace.chavePrimaria = ? ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idQuadraFace);

		return (Quadra) query.uniqueResult();
	}

	/**
	 * Obter quadra face por quadra.
	 * 
	 * @param idQuadra
	 *            the id quadra
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public List<QuadraFace> obterQuadraFacePorQuadra(Long idQuadra) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(SELECT);

		hql.append(QUADRA_FACE);
		hql.append(FROM);
		hql.append(getClasseEntidadeQuadraFace().getSimpleName());
		hql.append(QUADRA_FACE);
		hql.append(WHERE);
		hql.append(" quadraFace.quadra.chavePrimaria = ? ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idQuadra);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#obterQuadraFacePorQuadra(java.lang.Long, java.lang.String)
	 */
	@Override
	public List<QuadraFace> obterQuadraFacePorQuadra(Long idQuadra, String cep) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(SELECT);

		hql.append(QUADRA_FACE);
		hql.append(FROM);
		hql.append(getClasseEntidadeQuadraFace().getSimpleName());
		hql.append(QUADRA_FACE);
		hql.append(" inner join quadraFace.quadra quadra ");
		hql.append(WHERE);
		hql.append(" quadra.chavePrimaria = ? and ");
		hql.append(" quadraFace.endereco.cep.cep = ? ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idQuadra);
		query.setString(1, cep);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#alterarSituacaoImoveisFactivel(long)
	 */
	@Override
	public Collection<Imovel> alterarSituacaoImoveisFactivel(long idQuadraFace) throws NegocioException {

		Collection<Imovel> imoveisAux = new ArrayList<>();
		Collection<Imovel> imoveis = obterImoveisPorQuadraFace(idQuadraFace);
		for (Imovel imovel : imoveis) {
			configControladorConstanteSistema();
			ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_IMOVEL_FACTIVEL);

			Imovel imov = configSituacaoImovel(imovel, constanteSituacaoImovel);
			imoveisAux.add(imov);
		}
		return imoveisAux;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#alterarSituacaoImoveis(java.lang.Long[])
	 */
	@Override
	public Collection<Imovel> alterarSituacaoImoveis(Long[] idImoveis) throws NegocioException {

		Collection<Imovel> imoveisAux = new ArrayList<>();
		for (Long idImovel : idImoveis) {
			if(idImovel != 0) {
				Imovel imovel = obterImovel(idImovel);
				Integer redeIndicador = imovel.getQuadraFace().getRedeIndicador().getCodigo();
				if(redeIndicador == RedeIndicador.TEM) {
					if(imovel.getSituacaoImovel().getChavePrimaria() != SituacaoImovel.SITUACAO_INTERLIGADO) {
						configControladorConstanteSistema();
						ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema
										.obterConstantePorCodigo(Constantes.C_IMOVEL_POTENCIAL);

						configSituacaoImovel(imovel, constanteSituacaoImovel);
						imoveisAux.add(imovel);
					}
				} else if (redeIndicador == RedeIndicador.NAO_TEM
								&& imovel.getSituacaoImovel().getChavePrimaria() != SituacaoImovel.SITUACAO_INTERLIGADO) {
					configControladorConstanteSistema();
					ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema
									.obterConstantePorCodigo(Constantes.C_IMOVEL_FACTIVEL);

					configSituacaoImovel(imovel, constanteSituacaoImovel);
					imoveisAux.add(imovel);

				}

			}

		}
		return imoveisAux;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#alterarSituacaoImoveisPontencial(long)
	 */
	@Override
	public Collection<Imovel> alterarSituacaoImoveisPontencial(long idQuadraFace) throws NegocioException {

		Collection<Imovel> imoveisAux = new ArrayList<>();
		Collection<Imovel> imoveis = obterImoveisPorQuadraFace(idQuadraFace);
		for (Imovel imovel : imoveis) {
			configControladorConstanteSistema();
			ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_IMOVEL_POTENCIAL);
			if(imovel.getSituacaoImovel().getChavePrimaria() != SituacaoImovel.SITUACAO_INTERLIGADO) {
				Imovel imov = configSituacaoImovel(imovel, constanteSituacaoImovel);
				imoveisAux.add(imov);
			}
		}
		return imoveisAux;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#obterImoveisPorQuadraFace(long)
	 */
	@Override
	public Collection<Imovel> obterImoveisPorQuadraFace(long idQuadraFace) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeImovel().getSimpleName());
		hql.append(IMOVEL);
		hql.append(WHERE);
		hql.append(" imovel.quadraFace = ? ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idQuadraFace);

		return query.list();
	}

	/**
	 * Obter imovel.
	 * 
	 * @param idImovel
	 *            the id imovel
	 * @return the imovel
	 */
	public Imovel obterImovel(long idImovel) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeImovel().getSimpleName());
		hql.append(IMOVEL);
		hql.append(WHERE);
		hql.append(" imovel.chavePrimaria = ? ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idImovel);

		return (Imovel) query.uniqueResult();
	}

	/**
	 * Config controlador constante sistema.
	 */
	private void configControladorConstanteSistema() {

		this.controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
	}

	/**
	 * Config controlador imovel.
	 * 
	 * @return the controlador imovel
	 */
	public ControladorImovel configControladorImovel() {
		
		this.controladorImovel = (ControladorImovel) ServiceLocator.getInstancia().getControladorNegocio(
				ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
		
		return controladorImovel;
	}

	/**
	 * Config situacao imovel.
	 * 
	 * @param imovel
	 *            the imovel
	 * @param constanteSituacaoImovel
	 *            the constante situacao imovel
	 * @return the imovel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Imovel configSituacaoImovel(Imovel imovel, ConstanteSistema constanteSituacaoImovel) throws NegocioException {

		this.controladorImovel = configControladorImovel();
		SituacaoImovel situacaoImovel = this.controladorImovel.obterSituacaoImovel(Long.parseLong(constanteSituacaoImovel.getValor()));
		imovel.setSituacaoImovel(situacaoImovel);
		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#removerQuadras(java.lang.Long[], br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerQuadras(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		for (Long idQuadra : chavesPrimarias) {
			if(idQuadra != null && idQuadra > 0) {
				Quadra quadra = (Quadra) this.obter(idQuadra);

				if(listarImovelPorQuadra(idQuadra)) {
					try {
						remover(quadra);
					} catch(NegocioException n) {
						LOG.error(n.getMessage(), n);
						throw new NegocioException(ERRO_A_QUADRA_ESTA_EM_USO, quadra.getNumeroQuadra());

					}
				} else {
					throw new NegocioException(ERRO_A_QUADRA_ESTA_EM_USO, quadra.getNumeroQuadra());
				}

			}

		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.ControladorQuadra#atualizarQuadra(br.com.ggas.cadastro.localidade.Quadra)
	 */
	@Override
	public void atualizarQuadra(Quadra quadra) throws NegocioException, ConcorrenciaException {

		Collection<QuadraFace> quadraFaces = quadra.getQuadrasFace();
		for (QuadraFace quadraFace : quadraFaces) {
			if(quadraFace.getChavePrimaria() == 0) {
				getHibernateTemplate().saveOrUpdate(quadraFace);
			}
		}
		try {
			this.atualizar(quadra);
		} catch(ConstraintViolationException ex) {
			LOG.error(ex.getMessage(), ex);
			throw new NegocioException(Constantes.ERRO_NAO_POSSIVEL_REMOVER_QUADRAFACE_ASSOCIADA_PONTO_CONSUMO, true);
		}

	}

	/**
	 * Listar imovel por quadra.
	 * 
	 * @param idQuadra
	 *            the id quadra
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private boolean listarImovelPorQuadra(Long idQuadra) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeImovel(), "imov");

		criteria.createAlias("imov.quadraFace", "qufa");
		criteria.createAlias("qufa.quadra", "quad");

		criteria.add(Restrictions.eq("quad.chavePrimaria", idQuadra));

		Collection<Imovel> lista = criteria.list();

		if(lista != null && !lista.isEmpty()) {
			return false;
		}
		return true;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.cadastro.localidade.ControladorQuadra#listarQuadraPorCep(java.
	 * lang.String)
	 */
	@Override
	public Collection<Quadra> listarQuadraPorIdCep(final Long idCep) throws NegocioException {

		final Collection<Quadra> listaQuadra = new ArrayList<>();
		Collection<Quadra> resultado = null;
		Query query = null;

		final StringBuilder hql = new StringBuilder();
		hql.append(" select distinct quadraFace.quadra, quadraFace.quadra.numeroQuadra ");
		hql.append(FROM);
		hql.append(getClasseEntidadeQuadraFace().getSimpleName());
		hql.append(QUADRA_FACE);
		hql.append(" where quadraFace.habilitado = true ");
		hql.append(" and quadraFace.endereco.cep.chavePrimaria = ?");
		hql.append(" order by quadraFace.quadra.numeroQuadra asc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idCep);

		resultado = query.list();

		for (final Object currentItem : resultado.toArray()) {
			final Object[] arrayItem = (Object[]) currentItem;
			listaQuadra.add((Quadra) arrayItem[0]);
		}

		return listaQuadra;

	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.cadastro.localidade.ControladorQuadra#obterQuadraFacePorQuadra(
	 * java.lang.Long, java.lang.String)
	 */
	@Override
	public List<QuadraFace> obterQuadraFacePorQuadraIdCep(final Long idQuadra, final String idCep)
			throws NegocioException {

		final StringBuilder hql = new StringBuilder();

		hql.append(SELECT);

		hql.append(QUADRA_FACE);
		hql.append(FROM);
		hql.append(getClasseEntidadeQuadraFace().getSimpleName());
		hql.append(QUADRA_FACE);
		hql.append(" inner join quadraFace.quadra quadra ");
		hql.append(WHERE);
		hql.append(" quadra.chavePrimaria = ? and ");
		hql.append(" quadraFace.endereco.cep.chavePrimaria = ? ");

		final Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idQuadra);
		query.setString(1, idCep);

		return query.list();
	}


}
