/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.localidade;

import java.util.Collection;

import br.com.ggas.cadastro.dadocensitario.SetorCensitario;
import br.com.ggas.cadastro.operacional.ZonaBloqueio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * à quadra
 *
 */
public interface Quadra extends EntidadeNegocio {

	String BEAN_ID_QUADRA = "quadra";

	String QUADRA_ROTULO = "QUADRA_ROTULO";

	String LOCALIDADE = "QUADRA_LOCALIDADE";

	String SETOR_COMERCIAL = "QUADRA_SETOR_COMERCIAL";

	String NUMERO_QUADRA = "NUMERO_QUADRA";

	String PERFIL_QUADRA = "PERFIL_QUADRA";

	String SETOR_CENSITARIO = "QUADRA_SETOR_CENSITARIO";

	String AREA_TIPO = "QUADRA_AREA_TIPO";

	String ZONA_BLOQUEIO = "QUADRA_ZONA_BLOQUEIO";

	String QUADRA_FACE_QUADRA = "QUADRA_FACE_QUADRA";

	/**
	 * @return the numeroQuadra
	 */
	String getNumeroQuadra();

	/**
	 * @param numeroQuadra
	 *            the numeroQuadra to set
	 */
	void setNumeroQuadra(String numeroQuadra);

	/**
	 * @return the setorComercial
	 */
	SetorComercial getSetorComercial();

	/**
	 * @param setorComercial
	 *            the setorComercial to set
	 */
	void setSetorComercial(SetorComercial setorComercial);

	/**
	 * @return the perfilQuadra
	 */
	PerfilQuadra getPerfilQuadra();

	/**
	 * @param perfilQuadra
	 *            the perfilQuadra to set
	 */
	void setPerfilQuadra(PerfilQuadra perfilQuadra);

	/**
	 * @return the zeis
	 */
	Zeis getZeis();

	/**
	 * @param zeis
	 *            the zeis to set
	 */
	void setZeis(Zeis zeis);

	/**
	 * @return the quadrasFace
	 */
	Collection<QuadraFace> getQuadrasFace();

	/**
	 * @param quadrasFace
	 *            the quadrasFace to set
	 */
	void setQuadrasFace(Collection<QuadraFace> quadrasFace);

	/**
	 * @return the setorCensitario
	 */
	SetorCensitario getSetorCensitario();

	/**
	 * @param setorCensitario
	 *            the setorCensitario to set
	 */
	void setSetorCensitario(SetorCensitario setorCensitario);

	/**
	 * @return the areaTipo
	 */
	AreaTipo getAreaTipo();

	/**
	 * @param areaTipo
	 *            the areaTipo to set
	 */
	void setAreaTipo(AreaTipo areaTipo);

	/**
	 * @return the zonaBloqueio
	 */
	ZonaBloqueio getZonaBloqueio();

	/**
	 * @param zonaBloqueio
	 *            the zonaBloqueio to set
	 */
	void setZonaBloqueio(ZonaBloqueio zonaBloqueio);
}
