/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe LocalidadeImpl representa uma localidade no sistema.
 *
 * @since 03/07/2009
 * 
 */

package br.com.ggas.cadastro.localidade.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.LocalidadeClasse;
import br.com.ggas.cadastro.localidade.LocalidadePorte;
import br.com.ggas.cadastro.localidade.UnidadeNegocio;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * 
 * Classe responsável por implementar métodos de atributos
 * e método de validação de Localidade
 *
 */
public class LocalidadeImpl extends EntidadeNegocioImpl implements Localidade {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = -6697589655577284591L;

	private String descricao;

	private GerenciaRegional gerenciaRegional;

	private Endereco endereco;

	private String fone;

	private String ramalFone;

	private String fax;

	private String email;

	private UnidadeNegocio unidadeNegocio;

	private LocalidadeClasse classe;

	private LocalidadePorte porte;

	private String codigoCentroCusto;

	private boolean informatizada;

	private Cliente cliente;

	private MedidorLocalArmazenagem medidorLocalArmazenagem;

	private Integer codigoFaxDDD;

	private Integer codigoFoneDDD;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getGerenciaRegional()
	 */
	@Override
	public GerenciaRegional getGerenciaRegional() {

		return gerenciaRegional;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #setGerenciaRegional(br.com.ggas.cadastro.
	 * localidade.GerenciaRegional)
	 */
	@Override
	public void setGerenciaRegional(GerenciaRegional gerenciaRegional) {

		this.gerenciaRegional = gerenciaRegional;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getEndereco()
	 */
	@Override
	public Endereco getEndereco() {

		return endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #setEndereco(br.com.ggas.cadastro.endereco.
	 * Endereco)
	 */
	@Override
	public void setEndereco(Endereco endereco) {

		this.endereco = endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getFone()
	 */
	@Override
	public String getFone() {

		return fone;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #setFone(java.lang.String)
	 */
	@Override
	public void setFone(String fone) {

		this.fone = fone;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getRamalFone()
	 */
	@Override
	public String getRamalFone() {

		return ramalFone;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #setRamalFone(java.lang.String)
	 */
	@Override
	public void setRamalFone(String ramalFone) {

		this.ramalFone = ramalFone;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getFax()
	 */
	@Override
	public String getFax() {

		return fax;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #setFax(java.lang.String)
	 */
	@Override
	public void setFax(String fax) {

		this.fax = fax;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getEmail()
	 */
	@Override
	public String getEmail() {

		return email;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(String email) {

		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getUnidadeNegocio()
	 */
	@Override
	public UnidadeNegocio getUnidadeNegocio() {

		return unidadeNegocio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #setUnidadeNegocio(br.com.ggas.cadastro.
	 * localidade.impl.UnidadeNegocio)
	 */
	@Override
	public void setUnidadeNegocio(UnidadeNegocio unidadeNegocio) {

		this.unidadeNegocio = unidadeNegocio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getClasse()
	 */
	@Override
	public LocalidadeClasse getClasse() {

		return classe;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #setClasse(br.com.ggas.cadastro.localidade.
	 * LocalidadeClasse)
	 */
	@Override
	public void setClasse(LocalidadeClasse classe) {

		this.classe = classe;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getPorte()
	 */
	@Override
	public LocalidadePorte getPorte() {

		return porte;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.Localidade#setPorte(br.com.ggas.cadastro.localidade.LocalidadePorte)
	 */
	@Override
	public void setPorte(LocalidadePorte porte) {

		this.porte = porte;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getCodigoCentroCusto()
	 */
	@Override
	public String getCodigoCentroCusto() {

		return codigoCentroCusto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #setCodigoCentroCusto(java.lang.String)
	 */
	@Override
	public void setCodigoCentroCusto(String codigoCentroCusto) {

		this.codigoCentroCusto = codigoCentroCusto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #isInformatizada()
	 */
	@Override
	public boolean isInformatizada() {

		return informatizada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #setInformatizada(boolean)
	 */
	@Override
	public void setInformatizada(boolean informatizada) {

		this.informatizada = informatizada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #getCliente()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.impl.Localidade
	 * #
	 * setCliente(br.com.ggas.cadastro.cliente.Cliente
	 * )
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Localidade
	 * #getMedidorLocalArmazenagem()
	 */
	@Override
	public MedidorLocalArmazenagem getMedidorLocalArmazenagem() {

		return medidorLocalArmazenagem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Localidade
	 * #getCodigoFaxDDD()
	 */
	@Override
	public Integer getCodigoFaxDDD() {

		return codigoFaxDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Localidade
	 * #setCodigoFaxDDD(java.lang.Integer)
	 */
	@Override
	public void setCodigoFaxDDD(Integer codigoFaxDDD) {

		this.codigoFaxDDD = codigoFaxDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Localidade
	 * #getCodigoFoneDDD()
	 */
	@Override
	public Integer getCodigoFoneDDD() {

		return codigoFoneDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Localidade
	 * #setCodigoFoneDDD(java.lang.Integer)
	 */
	@Override
	public void setCodigoFoneDDD(Integer codigoFoneDDD) {

		this.codigoFoneDDD = codigoFoneDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.Localidade
	 * #setMedidorLocalArmazenagem
	 * (br.com.ggas.cadastro
	 * .operacional.MedidorLocalArmazenagem)
	 */
	@Override
	public void setMedidorLocalArmazenagem(MedidorLocalArmazenagem medidorLocalArmazenagem) {

		this.medidorLocalArmazenagem = medidorLocalArmazenagem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(getChavePrimaria() == 0) {
			stringBuilder.append(CODIGO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(gerenciaRegional == null) {
			stringBuilder.append(GERENCIA_REGIONAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(classe == null) {
			stringBuilder.append(CLASSE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(porte == null) {
			stringBuilder.append(PORTE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

}
