/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.localidade;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos metodos relacionados a Localidade
 *
 */
public interface Localidade extends EntidadeNegocio {

	String BEAN_ID_LOCALIDADE = "localidade";

	String DESCRICAO = "LOCALIDADE_DESCRICAO";

	String GERENCIA_REGIONAL = "LOCALIDADE_GERENCIA_REGIONAL";

	String ENDERECO = "LOCALIDADE_ENDERECO";

	String TELEFONE = "LOCALIDADE_TELEFONE";

	String RAMAL = "LOCALIDADE_RAMAL";

	String FAX = "LOCALIDADE_FAX";

	String EMAIL = "LOCALIDADE_EMAIL";

	String UNIDADE_NEGOCIO = "LOCALIDADE_UNIDADE_NEGOCIO";

	String CLASSE = "LOCALIDADE_CLASSE";

	String PORTE = "LOCALIDADE_PORTE";

	String CODIGO_CENTRO_CUSTO = "LOCALIDADE_CODIGO_CENTRO_CUSTO";

	String INFORMATIZADA = "LOCALIDADE_INFORMATIZADA";

	String CLIENTE = "LOCALIDADE_CLIENTE";

	String LOCALIDADE_CAMPO_STRING = "LOCALIDADE";

	String LOCALIDADES = "LOCALIDADES";

	String MEDIDOR_LOCAL_ARMAZENAGEM = "LOCALIDADE_MEDIDOR_LOCAL_ARMAZENAGEM";

	String CODIGO = "LOCALIDADE_CODIGO";

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the gerenciaRegional
	 */
	GerenciaRegional getGerenciaRegional();

	/**
	 * @param gerenciaRegional
	 *            the gerenciaRegional to set
	 */
	void setGerenciaRegional(GerenciaRegional gerenciaRegional);

	/**
	 * @return the endereco
	 */
	Endereco getEndereco();

	/**
	 * @param endereco
	 *            the endereco to set
	 */
	void setEndereco(Endereco endereco);

	/**
	 * @return the fone
	 */
	String getFone();

	/**
	 * @param fone
	 *            the fone to set
	 */
	void setFone(String fone);

	/**
	 * @return the ramalFone
	 */
	String getRamalFone();

	/**
	 * @param ramalFone
	 *            the ramalFone to set
	 */
	void setRamalFone(String ramalFone);

	/**
	 * @return the fax
	 */
	String getFax();

	/**
	 * @param fax
	 *            the fax to set
	 */
	void setFax(String fax);

	/**
	 * @return the email
	 */
	String getEmail();

	/**
	 * @param email
	 *            the email to set
	 */
	void setEmail(String email);

	/**
	 * @return the unidadeNegocio
	 */
	UnidadeNegocio getUnidadeNegocio();

	/**
	 * @param unidadeNegocio
	 *            the unidadeNegocio to set
	 */
	void setUnidadeNegocio(UnidadeNegocio unidadeNegocio);

	/**
	 * @return the classe
	 */
	LocalidadeClasse getClasse();

	/**
	 * @param classe
	 *            the classe to set
	 */
	void setClasse(LocalidadeClasse classe);

	/**
	 * @return the porte
	 */
	LocalidadePorte getPorte();

	/**
	 * @param porte
	 *            the porte to set
	 */
	void setPorte(LocalidadePorte porte);

	/**
	 * @return the codigoCentroCusto
	 */
	String getCodigoCentroCusto();

	/**
	 * @param codigoCentroCusto
	 *            the codigoCentroCusto to set
	 */
	void setCodigoCentroCusto(String codigoCentroCusto);

	/**
	 * @return the informatizada
	 */
	boolean isInformatizada();

	/**
	 * @param informatizada
	 *            the informatizada to set
	 */
	void setInformatizada(boolean informatizada);

	/**
	 * @return the cliente
	 */
	Cliente getCliente();

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	void setCliente(Cliente cliente);

	/**
	 * @return the medidorLocalArmazenagem
	 */
	MedidorLocalArmazenagem getMedidorLocalArmazenagem();

	/**
	 * @param medidorLocalArmazenagem
	 *            the medidorLocalArmazenagem to
	 *            set
	 */
	void setMedidorLocalArmazenagem(MedidorLocalArmazenagem medidorLocalArmazenagem);

	/**
	 * @return the codigoFaxDDD
	 */
	Integer getCodigoFaxDDD();

	/**
	 * @param codigoFaxDDD
	 *            the codigoFaxDDD to set
	 */
	void setCodigoFaxDDD(Integer codigoFaxDDD);

	/**
	 * @return the codigoFoneDDD
	 */
	Integer getCodigoFoneDDD();

	/**
	 * @param codigoFoneDDD
	 *            the codigoFoneDDD to set
	 */
	void setCodigoFoneDDD(Integer codigoFoneDDD);

}
