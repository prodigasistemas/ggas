
package br.com.ggas.cadastro.levantamentomercado.dominio;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * 
 * Classe responsável pelos métodos de atributos 
 * e validação de Levantamento Mercado Tipo Combustivel Aparelho
 *
 */
public class LevantamentoMercadoTipoCombustivelAparelho extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;

	private LevantamentoMercadoTipoCombustivel levantamentoMercadoTipoCombustivel;

	private EntidadeConteudo tipoAparelho;

	public LevantamentoMercadoTipoCombustivel getLevantamentoMercadoTipoCombustivel() {

		return levantamentoMercadoTipoCombustivel;
	}

	public void setLevantamentoMercadoTipoCombustivel(LevantamentoMercadoTipoCombustivel levantamentoMercadoTipoCombustivel) {

		this.levantamentoMercadoTipoCombustivel = levantamentoMercadoTipoCombustivel;
	}

	public EntidadeConteudo getTipoAparelho() {

		return tipoAparelho;
	}

	public void setTipoAparelho(EntidadeConteudo tipoAparelho) {

		this.tipoAparelho = tipoAparelho;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(tipoAparelho == null) {
			stringBuilder.append(Constantes.ERRO_TIPO_APARELHO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

}
