
package br.com.ggas.cadastro.levantamentomercado.agente.repositorio;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.cadastro.levantamentomercado.agente.dominio.Agente;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * 
 * Classe responsável pelas consultas relacionadas ao Agente
 *
 */
@Repository
public class RepositorioAgente extends RepositorioGenerico {

	private static final String ALIAS_FUNCIONARIO = "funcionario";

	/**
	 * Instantiates a new repositorio agente.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioAgente(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new Agente();
	}

	@Override
	public Class<?> getClasseEntidade() {

		return Agente.class;
	}

	/**
	 * Consultar agente.
	 * 
	 * @param agente
	 *            the agente
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<Agente> consultarAgente(Agente agente, String habilitado) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		if (agente.getFuncionario() != null) {
			criteria.createAlias(ALIAS_FUNCIONARIO, ALIAS_FUNCIONARIO);
			criteria.add(Restrictions.eq("funcionario.chavePrimaria", agente.getFuncionario().getChavePrimaria()));
		}

		if (agente.getChavePrimaria() > 0) {
			criteria.add(Restrictions.eq("chavePrimaria", agente.getChavePrimaria()));
		}

		if (!"todos".equalsIgnoreCase(habilitado)) {
			criteria.add(Restrictions.eq("habilitado", Boolean.valueOf(habilitado)));
		}

		return criteria.list();
	}

	/**
	 * Obter agente.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the agente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Agente obterAgente(Long chavePrimaria) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.createAlias(ALIAS_FUNCIONARIO, ALIAS_FUNCIONARIO);

		if (chavePrimaria != null && chavePrimaria > 0) {
			criteria.add(Restrictions.eq("chavePrimaria", chavePrimaria));
		}

		return (Agente) criteria.uniqueResult();

	}

	/**
	 * Obter agente por chave funcionario.
	 * 
	 * @param chaveFuncionario
	 *            the chave funcionario
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<Agente> obterAgentePorChaveFuncionario(Long chaveFuncionario, String habilitado) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.createAlias(ALIAS_FUNCIONARIO, ALIAS_FUNCIONARIO);
		if (chaveFuncionario != null && chaveFuncionario > 0) {
			criteria.add(Restrictions.eq("funcionario.chavePrimaria", chaveFuncionario));
		}

		if (!"todos".equalsIgnoreCase(habilitado)) {
			criteria.add(Restrictions.eq("habilitado", Boolean.valueOf(habilitado)));
		}

		return criteria.list();
	}
}
