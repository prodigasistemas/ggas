
package br.com.ggas.cadastro.levantamentomercado.dominio;

import br.com.ggas.cadastro.imovel.Imovel;
/**
 * 
 * Classe responsável pela representação de uma entidade
 * 
 */
public class LevantamentoMercadoVO {

	private Long chavePrimaria;

	private Imovel imovel;

	private String contato;

	private String status;

	private boolean habilitado;

	private boolean possuiAgendamento;
	
	
	public boolean isPossuiAgendamento() {
	
		return possuiAgendamento;
	}

	
	public void setPossuiAgendamento(boolean possuiAgendamento) {
	
		this.possuiAgendamento = possuiAgendamento;
	}

	public Imovel getImovel() {

		return imovel;
	}

	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	public String getContato() {

		return contato;
	}

	public void setContato(String contato) {

		this.contato = contato;
	}

	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {

		this.status = status;
	}

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public boolean isHabilitado() {

		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}
}
