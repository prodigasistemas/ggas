
package br.com.ggas.cadastro.levantamentomercado.negocio;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.levantamentomercado.dominio.AgendaVisitaAgente;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercado;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercadoTipoCombustivel;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.webservice.AgendaVisitaAgenteTO;
import br.com.ggas.webservice.LevantamentoMercadoTO;
import br.com.ggas.webservice.TipoComboBoxTO;

/**
 * 
 * Interface responsável pela assinatura dos métodos relacionados
 * ao Controlador do levantamento de mercado
 *
 */
public interface ControladorLevantamentoMercado {

	/**
	 * Inserir.
	 * 
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Long inserir(LevantamentoMercado levantamentoMercado) throws NegocioException;

	/**
	 * Consultar agente.
	 * 
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<LevantamentoMercado> consultarAgente(LevantamentoMercado levantamentoMercado, Boolean habilitado) throws NegocioException;

	/**
	 * Remover levantamento mercado.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerLevantamentoMercado(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Atualizar levantamento mercado.
	 * 
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarLevantamentoMercado(LevantamentoMercado levantamentoMercado) throws NegocioException, ConcorrenciaException;

	/**
	 * Remover levantamento mercado.
	 * 
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerLevantamentoMercado(LevantamentoMercado levantamentoMercado) throws NegocioException;

	/**
	 * Listar levantamento mercado.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<LevantamentoMercado> listarLevantamentoMercado() throws NegocioException;

	/**
	 * Obter levantamento mercado.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the levantamento mercado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	LevantamentoMercado obterLevantamentoMercado(long chavePrimaria) throws NegocioException;

	/**
	 * Obter levantamento mercado por id imovel.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the levantamento mercado
	 */
	LevantamentoMercado obterLevantamentoMercadoPorIdImovel(Long chavePrimaria);

	/**
	 * Listar levantamento mercado.
	 * 
	 * @param filtro {@link Map}
	 * @return the collection
	 * @throws GGASException  {@link GGASException}
	 */
	Collection<LevantamentoMercado> listarLevantamentoMercado(Map<String, Object> filtro) throws GGASException;

	/**
	 * Inserir agenda.
	 * 
	 * @param agendaVisitaAgente {@link AgendaVisitaAgente}
	 * @return Long {@link Long}
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Long inserirAgenda(AgendaVisitaAgente agendaVisitaAgente) throws NegocioException;

	/**
	 * Atualizar agenda.
	 * 
	 * @param agendaVisitaAgente
	 *            the agenda visita agente
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public void atualizarAgenda(AgendaVisitaAgente agendaVisitaAgente) throws NegocioException, ConcorrenciaException;

	/**
	 * Obter agenda levantamento mercado.
	 * 
	 * Obter agenda visita agente.
	 * 
	 * @param chave
	 *            the chave
	 * @return the agenda visita agente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public AgendaVisitaAgente obterAgendaVisitaAgente(Long chave) throws NegocioException;

	/**
	 * Validar data.
	 * 
	 * @param data
	 *            the data
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void validarData(Date data) throws NegocioException;

	/**
	 * Validar data vigencia.
	 * 
	 * @param vigenciaInicio
	 *            the vigencia inicio
	 * @param vigenciaFim
	 *            the vigencia fim
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void validarDataVigencia(Date vigenciaInicio, Date vigenciaFim) throws NegocioException;

	/**
	 * Listar levantamento mercado tipo combustivel por lm.
	 * 
	 * @param chave
	 *            the chave
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<LevantamentoMercadoTipoCombustivel> listarLevantamentoMercadoTipoCombustivelPorLM(Long chave) throws NegocioException;

	/**
	 * Validar dados obrigatorio levantamento mercado tipo combustivel.
	 * 
	 * @param levantamentoMercadoTipoCombustivel
	 *            the levantamento mercado tipo combustivel
	 * @return true, if successful
	 * @throws NegocioException  {@link NegocioException}
	 */
	public boolean validarDadosObrigatorioLevantamentoMercadoTipoCombustivel(
					LevantamentoMercadoTipoCombustivel levantamentoMercadoTipoCombustivel) throws NegocioException;

	/**
	 * Obtém o levantamento de mercado pela chave primária da proposta.
	 * 
	 * @param chaveProposta
	 *            the chave proposta
	 * @return LevantamentoMercado
	 */
	public LevantamentoMercado obterLevantamentoMercadoPorChaveProposta(Long chaveProposta);

	/**
	 * Conveter listas.
	 * 
	 * @return Map<String, Collection<TipoComboBoxTO>
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Map<String, Collection<TipoComboBoxTO>> conveterListas() throws GGASException;

	/**
	 * Converter levantamento mercado t oem levantamento mercado.
	 * 
	 * @param levantamentoMercadoTO
	 *            the levantamento mercado to
	 * @return {@link LevantamentoMercado}
	 * @throws GGASException
	 *             the GGAS exception
	 */

	public LevantamentoMercado converterLevantamentoMercadoTOemLevantamentoMercado(LevantamentoMercadoTO levantamentoMercadoTO)
					throws GGASException;

	/**
	 * Converter levantamento mercado em levantamento mercado to.
	 * 
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @return {@link LevantamentoMercadoTO}
	 * @throws GGASException
	 *             the GGAS exception
	 */

	public LevantamentoMercadoTO converterLevantamentoMercadoEmLevantamentoMercadoTO(LevantamentoMercado levantamentoMercado)
					throws GGASException;

	/**
	 * Listar levantamento mercado por chaves.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @return Collection<LevantamentoMercado>
	 */
	public Collection<LevantamentoMercado> listarLevantamentoMercadoPorChaves(Long[] chavesPrimarias);

	/**
	 * Converter agenda visita agente to em agente visita agente.
	 * 
	 * @param agendaVisitaAgenteTO
	 *            the agenda visita agente to
	 * @return {@link AgendaVisitaAgente}
	 * @throws GGASException
	 *             the GGAS exception
	 */
	AgendaVisitaAgente converterAgendaVisitaAgenteTOEmAgenteVisitaAgente(AgendaVisitaAgenteTO agendaVisitaAgenteTO) throws GGASException;

	/**
	 * Converter agenda visita agente em agenda visita agente to.
	 * 
	 * @param agendaVisitaAgente
	 *            the agenda visita agente
	 * @return {@link AgendaVisitaAgenteTO}
	 */
	AgendaVisitaAgenteTO converterAgendaVisitaAgenteEmAgendaVisitaAgenteTO(AgendaVisitaAgente agendaVisitaAgente);
	
	/**
	 * Obter Levantamento Mercado Chave Agenda. 
	 * 
	 * @param chaveAgenda the chave agenda
	 * @return LevantamentoMercado
	 */
	public LevantamentoMercado obterLevantamentoMercadoChaveAgenda(Long chaveAgenda);
}
