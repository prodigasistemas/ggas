
package br.com.ggas.cadastro.levantamentomercado.dominio;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;

import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contrato.proposta.Proposta;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável 
 * pelos métodos de LevantamentoMercado
 *
 */
public class LevantamentoMercado extends EntidadeNegocioImpl implements Serializable {

	private static final int LIMITE_CAMPO = 2;


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Imovel imovel;

	private Segmento segmento;

	private Date vigenciaInicio;

	private Date vigenciaFim;

	private boolean unidadesConsumidoras;

	private String quantidadeUnidadesConsumidoras;

	private Proposta proposta;

	private Collection<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel = new HashSet<>();

	private EntidadeConteudo status;
	
	private AgendaVisitaAgente agendaVisitaAgente;
	
	public Proposta getProposta() {

		return proposta;
	}

	public void setProposta(Proposta proposta) {

		this.proposta = proposta;
	}

	public Segmento getSegmento() {

		return segmento;
	}

	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	public boolean isUnidadesConsumidoras() {

		return unidadesConsumidoras;
	}

	public void setUnidadesConsumidoras(boolean unidadesConsumidoras) {

		this.unidadesConsumidoras = unidadesConsumidoras;
	}

	public String getQuantidadeUnidadesConsumidoras() {

		return quantidadeUnidadesConsumidoras;
	}

	public void setQuantidadeUnidadesConsumidoras(String quantidadeUnidadesConsumidoras) {

		this.quantidadeUnidadesConsumidoras = quantidadeUnidadesConsumidoras;
	}

	public Imovel getImovel() {

		return imovel;
	}

	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	public Date getVigenciaInicio() {
		Date data = null;
		if(this.vigenciaInicio != null) {
			data = (Date) vigenciaInicio.clone();
		}
		return data;
	}

	public void setVigenciaInicio(Date vigenciaInicio) {
		if(vigenciaInicio != null){
			this.vigenciaInicio = (Date) vigenciaInicio.clone();
		} else {
			this.vigenciaInicio = null;
		}
	}

	public Date getVigenciaFim() {

		return vigenciaFim;
	}

	public void setVigenciaFim(Date vigenciaFim) {
		if(vigenciaFim != null){
			this.vigenciaFim = (Date) vigenciaFim.clone();
		} else {
			this.vigenciaFim = null;
		}
	}

	public Collection<LevantamentoMercadoTipoCombustivel> getListaTipoCombustivel() {

		return listaTipoCombustivel;
	}

	public void setListaTipoCombustivel(Collection<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel) {

		this.listaTipoCombustivel = listaTipoCombustivel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(imovel == null) {
			stringBuilder.append(Constantes.ERRO_IMOVEL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(segmento == null) {
			stringBuilder.append(Constantes.ERRO_SEGMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(vigenciaInicio == null) {
			stringBuilder.append(Constantes.ERRO_VIGENCIA_INICIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(vigenciaFim == null) {
			stringBuilder.append(Constantes.ERRO_VIGENCIA_FIM);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(unidadesConsumidoras && (quantidadeUnidadesConsumidoras == null || quantidadeUnidadesConsumidoras.length() == 0)) {
			stringBuilder.append(Constantes.ERRO_QUANTIDADE_UNIDADES_CONSUMIDORAS);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(CollectionUtils.isEmpty(listaTipoCombustivel)){
			stringBuilder.append(Constantes.ERRO_TIPO_COMBUSTIVEL_OBRIGATORIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

	public EntidadeConteudo getStatus() {

		return status;
	}

	public void setStatus(EntidadeConteudo status) {

		this.status = status;
	}

	/**
	 * @return the agendaVisitaAgente
	 */
	public AgendaVisitaAgente getAgendaVisitaAgente() {

		return agendaVisitaAgente;
	}

	/**
	 * @param agendaVisitaAgente the agendaVisitaAgente to set
	 */
	public void setAgendaVisitaAgente(AgendaVisitaAgente agendaVisitaAgente) {

		this.agendaVisitaAgente = agendaVisitaAgente;
	}

	public boolean getAgendamentoVisita() {
		if (this.agendaVisitaAgente != null) {
			return true;
		}

		return false;
	}
}
