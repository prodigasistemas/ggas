
package br.com.ggas.cadastro.levantamentomercado.negocio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.TipoBotijao;
import br.com.ggas.cadastro.levantamentomercado.dominio.AgendaVisitaAgente;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercado;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercadoTipoCombustivel;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercadoTipoCombustivelAparelho;
import br.com.ggas.cadastro.levantamentomercado.repositorio.RepositorioLevantamentoMercado;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.contrato.proposta.ControladorProposta;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.webservice.AgendaVisitaAgenteTO;
import br.com.ggas.webservice.ImovelTO;
import br.com.ggas.webservice.LevantamentoMercadoTO;
import br.com.ggas.webservice.LevantamentoMercadoTipoCombustivelAparelhoTO;
import br.com.ggas.webservice.LevantamentoMercadoTipoCombustivelTO;
import br.com.ggas.webservice.PropostaTO;
import br.com.ggas.webservice.TipoComboBoxTO;

/**
 * 
 * Classe responsável pelas operações de negócio 
 * referentes ao levantamento de mercado.
 *
 */
@Service("controladorLevantamentoMercado")
@Transactional
public class ControladorLevantamentoMercadoImpl implements ControladorLevantamentoMercado {

	private static final String CONSTANTE_NAO_SE_APLICA = "Não se Aplica";

	private static final int NUMERO_CASAS_DECIMAIS = 2;

	private static final int CHAVE_CAMPO_NAO_APLICAVEL = -2;

	private static final int LIMITE_CAMPO = 2;

	private static final String APARELHOS_TO = "aparelhosTO";

	private static final String FORNECEDORES = "fornecedores";

	private static final String FONTES_ENERGETICAS_TO = "fontesEnergeticasTO";

	private static final String LISTA_ESTADO_REDE_TO = "listaEstadoRedeTO";

	private static final String LISTA_REDE_MATERIAL_TO = "listaRedeMaterialTO";

	private static final String SEGMENTOS_TO = "segmentosTO";

	private static final String LISTA_MODALIDADE_USO_TO = "listaModalidadeUsoTO";

	private static final String LISTA_TIPO_CILINDRO_TO = "listaTipoCilindroTO";

	private static final String LISTA_PERIODICIDADES_TO = "listaPeriodicidadesTO";

	private Fachada fachada = Fachada.getInstancia();

	/**
	 * Auto injeção do repositório para uso na classe.
	 */
	@Autowired
	public RepositorioLevantamentoMercado repositorioLevantamentoMercado;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#inserir(br.com.ggas.cadastro.levantamentomercado.
	 * dominio.
	 * LevantamentoMercado)
	 */
	@Override
	public Long inserir(LevantamentoMercado levantamentoMercado) throws NegocioException {

		for (LevantamentoMercadoTipoCombustivel lmtc : levantamentoMercado.getListaTipoCombustivel()) {
			lmtc.setLevantamentoMercado(levantamentoMercado);
			lmtc.setUltimaAlteracao(new Date());
			lmtc.setHabilitado(Boolean.TRUE);

			for (LevantamentoMercadoTipoCombustivelAparelho leAparelho : lmtc.getListaAparelhos()) {
				leAparelho.setLevantamentoMercadoTipoCombustivel(lmtc);
				leAparelho.setHabilitado(Boolean.TRUE);
				leAparelho.setUltimaAlteracao(new Date());
			}
		}

		return repositorioLevantamentoMercado.inserir(levantamentoMercado);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#consultarAgente(br.com.ggas.cadastro.levantamentomercado
	 * .dominio
	 * .LevantamentoMercado, java.lang.Boolean)
	 */
	@Override
	public Collection<LevantamentoMercado> consultarAgente(LevantamentoMercado levantamentoMercado, Boolean habilitado)
					throws NegocioException {

		return Collections.emptyList();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#removerLevantamentoMercado(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerLevantamentoMercado(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		repositorioLevantamentoMercado.remover(chavesPrimarias, dadosAuditoria);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#atualizarLevantamentoMercado(br.com.ggas.cadastro.
	 * levantamentomercado.dominio.LevantamentoMercado)
	 */
	@Override
	public void atualizarLevantamentoMercado(LevantamentoMercado levantamentoMercado) throws NegocioException, ConcorrenciaException {

		for (LevantamentoMercadoTipoCombustivel lmtc : levantamentoMercado.getListaTipoCombustivel()) {
			lmtc.setLevantamentoMercado(levantamentoMercado);
			lmtc.setUltimaAlteracao(new Date());
			lmtc.setHabilitado(Boolean.TRUE);

			for (LevantamentoMercadoTipoCombustivelAparelho leAparelho : lmtc.getListaAparelhos()) {
				leAparelho.setLevantamentoMercadoTipoCombustivel(lmtc);
				leAparelho.setHabilitado(Boolean.TRUE);
				leAparelho.setUltimaAlteracao(new Date());
			}
		}

		repositorioLevantamentoMercado.atualizar(levantamentoMercado);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#removerLevantamentoMercado(br.com.ggas.cadastro.
	 * levantamentomercado
	 * .dominio.LevantamentoMercado)
	 */
	@Override
	public void removerLevantamentoMercado(LevantamentoMercado levantamentoMercado) throws NegocioException {

		repositorioLevantamentoMercado.remover(levantamentoMercado, true);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#listarLevantamentoMercado()
	 */
	@Override
	public Collection<LevantamentoMercado> listarLevantamentoMercado() throws NegocioException {

		return Collections.emptyList();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#obterLevantamentoMercado(long)
	 */
	@Override
	public LevantamentoMercado obterLevantamentoMercado(long chavePrimaria) {

		return repositorioLevantamentoMercado.obterLevantamentoMercadoPorChave(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#obterLevantamentoMercadoPorIdImovel(java.lang.Long)
	 */
	@Override
	public LevantamentoMercado obterLevantamentoMercadoPorIdImovel(Long chavePrimaria) {

		return repositorioLevantamentoMercado.obterLevantamentoMercadoPorIdImovel(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#listarLevantamentoMercado(java.util.HashMap)
	 */
	@Override
	public Collection<LevantamentoMercado> listarLevantamentoMercado(Map<String, Object> filtro) throws GGASException {

		return repositorioLevantamentoMercado.listarLevantamentoMercado(filtro);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#inserirAgenda(br.com.ggas.cadastro.levantamentomercado
	 * .dominio
	 * .AgendaVisitaAgente)
	 */
	@Override
	public Long inserirAgenda(AgendaVisitaAgente agendaVisitaAgente) throws NegocioException {

		return repositorioLevantamentoMercado.inserir(agendaVisitaAgente);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#atualizarAgenda(br.com.ggas.cadastro.levantamentomercado
	 * .dominio
	 * .AgendaVisitaAgente)
	 */
	@Override
	public void atualizarAgenda(AgendaVisitaAgente agendaVisitaAgente) throws NegocioException, ConcorrenciaException {

		repositorioLevantamentoMercado.atualizar(agendaVisitaAgente, AgendaVisitaAgente.class);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#obterAgendaLevantamentoMercado(java.lang.Long)
	 */
	@Override
	public AgendaVisitaAgente obterAgendaVisitaAgente(Long chave) throws NegocioException {

		return repositorioLevantamentoMercado.obterAgendaVisitaAgente(chave);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#validarData(java.util.Date)
	 */
	@Override
	public void validarData(Date data) throws NegocioException {

		Date dataAtual = new Date();
		dataAtual.setHours(0);
		dataAtual.setMinutes(0);
		dataAtual.setSeconds(0);

		if (Util.compararDatas(data, dataAtual) < 0) {
			throw new NegocioException(Constantes.DATA_MENOR_QUE_DATA_ATUAL, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#validarDataVigencia(java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public void validarDataVigencia(Date vigenciaInicio, Date vigenciaFim) throws NegocioException {

		if (vigenciaInicio != null && vigenciaFim != null && vigenciaInicio.compareTo(vigenciaFim) > 0) {

			throw new NegocioException(Constantes.VIGENCIA_INICIO_MAIOR_VIGENCIA_FIM, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#listarLevantamentoMercadoTipoCombustivelPorLM(java.lang
	 * .Long)
	 */
	@Override
	public Collection<LevantamentoMercadoTipoCombustivel> listarLevantamentoMercadoTipoCombustivelPorLM(Long chave) throws NegocioException {

		return repositorioLevantamentoMercado.listarLevantamentoMercadoTipoCombustivelPorLM(chave);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#
	 * validarDadosObrigatorioLevantamentoMercadoTipoCombustivel
	 * (br
	 * .com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercadoTipoCombustivel)
	 */
	@Override
	public boolean validarDadosObrigatorioLevantamentoMercadoTipoCombustivel(
					LevantamentoMercadoTipoCombustivel levantamentoMercadoTipoCombustivel) {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;
		if (levantamentoMercadoTipoCombustivel != null) {
			if (levantamentoMercadoTipoCombustivel.getTipoCombustivel() == null
							|| levantamentoMercadoTipoCombustivel.getTipoCombustivel().getChavePrimaria() == -1) {
				stringBuilder.append(Constantes.TIPO_COMBUSTIVEL);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (levantamentoMercadoTipoCombustivel.getTipoCombustivel().getChavePrimaria() > 0) {
				if (levantamentoMercadoTipoCombustivel.getConsumo() == null
								|| levantamentoMercadoTipoCombustivel.getConsumo().compareTo(new BigDecimal(0)) == 0) {
					stringBuilder.append(Constantes.CONSUMO);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
				if (levantamentoMercadoTipoCombustivel.getPreco() == null
								|| levantamentoMercadoTipoCombustivel.getPreco().compareTo(new BigDecimal(0)) == 0) {
					stringBuilder.append(Constantes.PRECO);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			}

			if (levantamentoMercadoTipoCombustivel.getFornecedor() == null) {
				stringBuilder.append(Constantes.FORNECEDOR);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (levantamentoMercadoTipoCombustivel.getPeriodicidade() == null) {
				stringBuilder.append(Constantes.PERIODICIDADE_CONTRATO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (levantamentoMercadoTipoCombustivel.getModalidadeUso() == null) {
				stringBuilder.append(Constantes.MODALIDA_USO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (levantamentoMercadoTipoCombustivel.getRedeEstado() == null) {
				stringBuilder.append(Constantes.ESTADO_REDE);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (levantamentoMercadoTipoCombustivel.isCentral() && (!levantamentoMercadoTipoCombustivel.isCentral())) {
				stringBuilder.append(Constantes.CENTRAL);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (levantamentoMercadoTipoCombustivel.getRedeMaterial() == null) {
				stringBuilder.append(Constantes.MATERIAL_REDE);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (levantamentoMercadoTipoCombustivel.isVentilacao() && (!levantamentoMercadoTipoCombustivel.isVentilacao())) {
				stringBuilder.append(Constantes.VENTILACAO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if (CollectionUtils.isEmpty(levantamentoMercadoTipoCombustivel.getListaAparelhos())) {
				stringBuilder.append(Constantes.APARELHO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			camposObrigatorios = stringBuilder.toString();

			if (camposObrigatorios.length() > 0) {
				erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
								camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
			}
		}
		return !erros.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#obterLevantamentoMercadoPorChaveProposta(java.lang
	 * .Long)
	 */
	@Override
	public LevantamentoMercado obterLevantamentoMercadoPorChaveProposta(Long chaveProposta) {

		return repositorioLevantamentoMercado.obterLevantamentoMercadoPorChaveProposta(chaveProposta);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#conveterListas()
	 */
	@Override
	public Map<String, Collection<TipoComboBoxTO>> conveterListas() throws GGASException {

		Map<String, Collection<TipoComboBoxTO>> listas = new HashMap<>();

		Collection<EntidadeConteudo> fornecedores = fachada.obterListaFornecedoresCombustiveis();
		Collection<EntidadeConteudo> aparelhos = fachada.obterListaAparelhosMaiorIndiceConsumo();
		Collection<EntidadeConteudo> fontesEnergeticas = fachada.obterListaTiposFontesEnergeticas();
		Collection<EntidadeConteudo> listaEstadoRede = fachada.obterListaEstadoRede();
		Collection<RedeMaterial> listaRedeMaterial = fachada.listarRedeMaterial();
		Collection<Segmento> segmentos = fachada.consultarSegmento(new HashMap<String, Object>());
		Collection<EntidadeConteudo> listaModalidadeUso = fachada.listarModalidadeUso();
		Collection<TipoBotijao> listaTipoCilindro = fachada.listarTipoBotijao();
		Collection<Periodicidade> listaPeriodicidades = fachada.listarPeriodicidades();

		adicionaListasMapa(listas, fornecedores, aparelhos, fontesEnergeticas, listaEstadoRede, listaRedeMaterial,
				segmentos, listaModalidadeUso, listaTipoCilindro, listaPeriodicidades);

		return listas;

	}

	private void adicionaListasMapa(Map<String, Collection<TipoComboBoxTO>> listas,
			Collection<EntidadeConteudo> fornecedores, Collection<EntidadeConteudo> aparelhos,
			Collection<EntidadeConteudo> fontesEnergeticas, Collection<EntidadeConteudo> listaEstadoRede,
			Collection<RedeMaterial> listaRedeMaterial, Collection<Segmento> segmentos,
			Collection<EntidadeConteudo> listaModalidadeUso, Collection<TipoBotijao> listaTipoCilindro,
			Collection<Periodicidade> listaPeriodicidades) {
		if (!CollectionUtils.isEmpty(fornecedores)) {
			Collection<TipoComboBoxTO> fornecedoresTO = new ArrayList<>();
			for (EntidadeConteudo fornecedor : fornecedores) {
				TipoComboBoxTO combo = new TipoComboBoxTO();
				combo.setChavePrimaria(fornecedor.getChavePrimaria());
				combo.setDescricao(fornecedor.getDescricao());
				fornecedoresTO.add(combo);
			}
			listas.put(FORNECEDORES, fornecedoresTO);
		}

		if (!CollectionUtils.isEmpty(aparelhos)) {
			Collection<TipoComboBoxTO> aparelhosTO = new ArrayList<>();
			for (EntidadeConteudo aparelho : aparelhos) {
				TipoComboBoxTO combo = new TipoComboBoxTO();
				combo.setChavePrimaria(aparelho.getChavePrimaria());
				combo.setDescricao(aparelho.getDescricao());
				aparelhosTO.add(combo);
			}
			listas.put(APARELHOS_TO, aparelhosTO);
		}

		if (!CollectionUtils.isEmpty(fontesEnergeticas)) {
			Collection<TipoComboBoxTO> fontesEnergeticasTO = new ArrayList<>();
			for (EntidadeConteudo fonteEnergetica : fontesEnergeticas) {
				TipoComboBoxTO combo = new TipoComboBoxTO();
				combo.setChavePrimaria(fonteEnergetica.getChavePrimaria());
				combo.setDescricao(fonteEnergetica.getDescricao());
				fontesEnergeticasTO.add(combo);
			}
			listas.put(FONTES_ENERGETICAS_TO, fontesEnergeticasTO);
		}

		if (!CollectionUtils.isEmpty(listaEstadoRede)) {
			Collection<TipoComboBoxTO> listaEstadoRedeTO = new ArrayList<>();
			for (EntidadeConteudo estadoRede : listaEstadoRede) {
				TipoComboBoxTO combo = new TipoComboBoxTO();
				combo.setChavePrimaria(estadoRede.getChavePrimaria());
				combo.setDescricao(estadoRede.getDescricao());
				listaEstadoRedeTO.add(combo);
			}
			listas.put(LISTA_ESTADO_REDE_TO, listaEstadoRedeTO);
		}

		if (!CollectionUtils.isEmpty(listaRedeMaterial)) {
			Collection<TipoComboBoxTO> listaRedeMaterialTO = new ArrayList<>();
			for (RedeMaterial redeMaterial : listaRedeMaterial) {
				TipoComboBoxTO combo = new TipoComboBoxTO();
				combo.setChavePrimaria(redeMaterial.getChavePrimaria());
				combo.setDescricao(redeMaterial.getDescricao());
				listaRedeMaterialTO.add(combo);
			}
			listas.put(LISTA_REDE_MATERIAL_TO, listaRedeMaterialTO);
		}

		if (!CollectionUtils.isEmpty(segmentos)) {
			Collection<TipoComboBoxTO> segmentosTO = new ArrayList<>();
			for (Segmento segmento : segmentos) {
				TipoComboBoxTO combo = new TipoComboBoxTO();
				combo.setChavePrimaria(segmento.getChavePrimaria());
				combo.setDescricao(segmento.getDescricao());
				segmentosTO.add(combo);
			}
			listas.put(SEGMENTOS_TO, segmentosTO);
		}

		if (!CollectionUtils.isEmpty(listaModalidadeUso)) {
			Collection<TipoComboBoxTO> listaModalidadeUsoTO = new ArrayList<>();
			for (EntidadeConteudo modalidadeUso : listaModalidadeUso) {
				TipoComboBoxTO combo = new TipoComboBoxTO();
				combo.setChavePrimaria(modalidadeUso.getChavePrimaria());
				combo.setDescricao(modalidadeUso.getDescricao());
				listaModalidadeUsoTO.add(combo);
			}
			listas.put(LISTA_MODALIDADE_USO_TO, listaModalidadeUsoTO);
		}

		if (!CollectionUtils.isEmpty(listaTipoCilindro)) {
			Collection<TipoComboBoxTO> listaTipoCilindroTO = new ArrayList<>();
			for (TipoBotijao cilindro : listaTipoCilindro) {
				TipoComboBoxTO combo = new TipoComboBoxTO();
				combo.setChavePrimaria(cilindro.getChavePrimaria());
				combo.setDescricao(cilindro.getDescricao());
				listaTipoCilindroTO.add(combo);
			}
			listas.put(LISTA_TIPO_CILINDRO_TO, listaTipoCilindroTO);
		}

		if (!CollectionUtils.isEmpty(listaPeriodicidades)) {
			Collection<TipoComboBoxTO> listaPeriodicidadesTO = new ArrayList<>();
			for (Periodicidade periodicidade : listaPeriodicidades) {
				TipoComboBoxTO combo = new TipoComboBoxTO();
				combo.setChavePrimaria(periodicidade.getChavePrimaria());
				combo.setDescricao(periodicidade.getDescricao());
				listaPeriodicidadesTO.add(combo);
			}
			listas.put(LISTA_PERIODICIDADES_TO, listaPeriodicidadesTO);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#converterLevantamentoMercadoTOemLevantamentoMercado(br
	 * .com.
	 * ggas.webservice.LevantamentoMercadoTO)
	 */
	@Override
	public LevantamentoMercado converterLevantamentoMercadoTOemLevantamentoMercado(LevantamentoMercadoTO levantamentoMercadoTO)
					throws GGASException {

		LevantamentoMercado levantamentoMercado = new LevantamentoMercado();

		if (levantamentoMercadoTO.getChavePrimaria() != null && levantamentoMercadoTO.getChavePrimaria() > 0) {
			levantamentoMercado.setChavePrimaria(levantamentoMercadoTO.getChavePrimaria());
			LevantamentoMercado levantamentoOrigi = this.obterLevantamentoMercado(levantamentoMercadoTO.getChavePrimaria());
			levantamentoMercado.setVersao(levantamentoOrigi.getVersao());
		}

		if (levantamentoMercadoTO.getImovelTO() != null && levantamentoMercadoTO.getImovelTO().getChavePrimaria() != null
						&& levantamentoMercadoTO.getImovelTO().getChavePrimaria() > 0) {
			
			levantamentoMercado.setImovel(fachada.buscarImovelPorChave(levantamentoMercadoTO.getImovelTO().getChavePrimaria()));
		}

		if (levantamentoMercadoTO.getSegmento() != null && levantamentoMercadoTO.getSegmento().getChavePrimaria() != null
						&& levantamentoMercadoTO.getSegmento().getChavePrimaria() > 0) {
	
			levantamentoMercado.setSegmento(fachada.buscarSegmentoChave(levantamentoMercadoTO.getSegmento().getChavePrimaria()));
		}

		if (!StringUtils.isEmpty(levantamentoMercadoTO.getVigenciaInicio())) {
			levantamentoMercado.setVigenciaInicio(Util.converterCampoStringParaData("Data Início",
							levantamentoMercadoTO.getVigenciaInicio(), Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(levantamentoMercadoTO.getVigenciaFim())) {
			levantamentoMercado.setVigenciaFim(Util.converterCampoStringParaData("Data Fim", levantamentoMercadoTO.getVigenciaFim(),
							Constantes.FORMATO_DATA_BR));
		}

		levantamentoMercado.setUnidadesConsumidoras(levantamentoMercadoTO.isUnidadesConsumidoras());

		levantamentoMercado.setQuantidadeUnidadesConsumidoras(levantamentoMercadoTO.getQuantidadeUnidadesConsumidoras());

		if (levantamentoMercadoTO.getStatus() != null) {
			EntidadeConteudo status = fachada.obterEntidadeConteudo(levantamentoMercadoTO.getStatus().getChavePrimaria());
			levantamentoMercado.setStatus(status);
		}

		if (!CollectionUtils.isEmpty(levantamentoMercadoTO.getListaTipoCombustivel())) {
			Collection<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel = new HashSet<>();
			for (LevantamentoMercadoTipoCombustivelTO levaMercadoCombustivelTO : levantamentoMercadoTO.getListaTipoCombustivel()) {

				LevantamentoMercadoTipoCombustivel levantamentoMercadoTipoCombustivel = new LevantamentoMercadoTipoCombustivel();

				levantamentoMercadoTipoCombustivel.setLevantamentoMercado(levantamentoMercado);

				if (levaMercadoCombustivelTO.getChavePrimaria() != null && levaMercadoCombustivelTO.getChavePrimaria() > 0) {
					levantamentoMercadoTipoCombustivel.setChavePrimaria(levaMercadoCombustivelTO.getChavePrimaria());
				}

				if (levaMercadoCombustivelTO.getTipoCombustivel() != null
								&& levaMercadoCombustivelTO.getTipoCombustivel().getChavePrimaria() != CHAVE_CAMPO_NAO_APLICAVEL) {
					levantamentoMercadoTipoCombustivel.setTipoCombustivel(fachada.obterEntidadeConteudo(levaMercadoCombustivelTO
									.getTipoCombustivel().getChavePrimaria()));
				}

				if (!StringUtils.isEmpty(levaMercadoCombustivelTO.getConsumo())) {
					levantamentoMercadoTipoCombustivel.setConsumo(Util.converterCampoStringParaValorBigDecimal("Consumo",
									levaMercadoCombustivelTO.getConsumo(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
				}

				if (!StringUtils.isEmpty(levaMercadoCombustivelTO.getPreco())) {
					levantamentoMercadoTipoCombustivel.setPreco(Util.converterCampoStringParaValorBigDecimal("Consumo",
									levaMercadoCombustivelTO.getPreco(), Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
				}

				if (levaMercadoCombustivelTO.getFornecedor() != null
						&& levaMercadoCombustivelTO.getFornecedor().getChavePrimaria() != CHAVE_CAMPO_NAO_APLICAVEL) {
					levantamentoMercadoTipoCombustivel.setFornecedor(fachada.obterEntidadeConteudo(levaMercadoCombustivelTO.getFornecedor()
									.getChavePrimaria()));
				}

				if (levaMercadoCombustivelTO.getPeriodicidade() != null
								&& levaMercadoCombustivelTO.getPeriodicidade().getChavePrimaria() != CHAVE_CAMPO_NAO_APLICAVEL) {
					levantamentoMercadoTipoCombustivel.setPeriodicidade(fachada.obterPeriodicidade(levaMercadoCombustivelTO
									.getPeriodicidade().getChavePrimaria()));
				}

				if (levaMercadoCombustivelTO.getModalidadeUso() != null
								&& levaMercadoCombustivelTO.getModalidadeUso().getChavePrimaria() != CHAVE_CAMPO_NAO_APLICAVEL) {
					levantamentoMercadoTipoCombustivel.setModalidadeUso(fachada.obterEntidadeConteudo(levaMercadoCombustivelTO
									.getModalidadeUso().getChavePrimaria()));
				}

				if (levaMercadoCombustivelTO.getRedeEstado() != null) {
					levantamentoMercadoTipoCombustivel.setRedeEstado(fachada.obterEntidadeConteudo(levaMercadoCombustivelTO.getRedeEstado()
									.getChavePrimaria()));
				}

				levantamentoMercadoTipoCombustivel.setCentral(levaMercadoCombustivelTO.isCentral());

				if (!StringUtils.isEmpty(levaMercadoCombustivelTO.getLocalizacaoCentral())) {
					levantamentoMercadoTipoCombustivel.setLocalizacaoCentral(levaMercadoCombustivelTO.getLocalizacaoCentral());
				}

				if (!StringUtils.isEmpty(levaMercadoCombustivelTO.getLocalizacaoAbrigo())) {
					levantamentoMercadoTipoCombustivel.setLocalizacaoAbrigo(levaMercadoCombustivelTO.getLocalizacaoAbrigo());
				}

				if (levaMercadoCombustivelTO.getTipoBotijao() != null) {
					levantamentoMercadoTipoCombustivel.setTipoBotijao(fachada.buscarTipoBotijaoPorChave(levaMercadoCombustivelTO
									.getTipoBotijao().getChavePrimaria()));
				}

				if (levaMercadoCombustivelTO.getRedeMaterial() != null
								&& levaMercadoCombustivelTO.getRedeMaterial().getChavePrimaria() != CHAVE_CAMPO_NAO_APLICAVEL) {
					levantamentoMercadoTipoCombustivel.setRedeMaterial(fachada.buscarRedeMaterialPorChave(levaMercadoCombustivelTO
									.getRedeMaterial().getChavePrimaria()));
				}

				if (!StringUtils.isEmpty(levaMercadoCombustivelTO.getRedeTempo())) {
					levantamentoMercadoTipoCombustivel.setRedeTempo(levaMercadoCombustivelTO.getRedeTempo());
				}

				levantamentoMercadoTipoCombustivel.setVentilacao(levaMercadoCombustivelTO.isVentilacao());

				if (!StringUtils.isEmpty(levaMercadoCombustivelTO.getObservacoes())) {
					levantamentoMercadoTipoCombustivel.setObservacoes(levaMercadoCombustivelTO.getObservacoes());
				}

				levantamentoMercadoTipoCombustivel.setUltimaAlteracao(new Date());

				levantamentoMercadoTipoCombustivel.setHabilitado(Boolean.TRUE);

				if (!CollectionUtils.isEmpty(levaMercadoCombustivelTO.getListaAparelhos())) {
					Collection<LevantamentoMercadoTipoCombustivelAparelho> listaAparelhos = new HashSet<>();
					for (LevantamentoMercadoTipoCombustivelAparelhoTO tipoAparelhoTO : levaMercadoCombustivelTO.getListaAparelhos()) {
						LevantamentoMercadoTipoCombustivelAparelho levantamentoMercadoTipoCombustivelAparelho = 
										new LevantamentoMercadoTipoCombustivelAparelho();

						if (tipoAparelhoTO.getChavePrimaria() != null && tipoAparelhoTO.getChavePrimaria() > 0) {
							levantamentoMercadoTipoCombustivelAparelho.setChavePrimaria(tipoAparelhoTO.getChavePrimaria());
						}

						if (tipoAparelhoTO.getTipoAparelho() != null && tipoAparelhoTO.getTipoAparelho().getChavePrimaria() != null
										&& tipoAparelhoTO.getTipoAparelho().getChavePrimaria() > 0) {

							levantamentoMercadoTipoCombustivelAparelho.setTipoAparelho(fachada.obterEntidadeConteudo(tipoAparelhoTO
											.getTipoAparelho().getChavePrimaria()));
						}

						levantamentoMercadoTipoCombustivelAparelho
										.setLevantamentoMercadoTipoCombustivel(levantamentoMercadoTipoCombustivel);

						levantamentoMercadoTipoCombustivelAparelho.setUltimaAlteracao(new Date());

						levantamentoMercadoTipoCombustivelAparelho.setHabilitado(Boolean.TRUE);

						listaAparelhos.add(levantamentoMercadoTipoCombustivelAparelho);
					}

					levantamentoMercadoTipoCombustivel.getListaAparelhos().addAll(listaAparelhos);
				}
				listaTipoCombustivel.add(levantamentoMercadoTipoCombustivel);
			}
			levantamentoMercado.getListaTipoCombustivel().clear();
			levantamentoMercado.getListaTipoCombustivel().addAll(listaTipoCombustivel);
		}

		levantamentoMercado.setHabilitado(levantamentoMercadoTO.isHabilitado());

		levantamentoMercado.setUltimaAlteracao(new Date());

		if (levantamentoMercadoTO.getAgendaVisitaAgente() != null) {
			AgendaVisitaAgente agenda = converterAgendaVisitaAgenteTOEmAgenteVisitaAgente(levantamentoMercadoTO.getAgendaVisitaAgente());
			levantamentoMercado.setAgendaVisitaAgente(agenda);
		}

		return levantamentoMercado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#converterLevantamentoMercadoEmLevantamentoMercadoTO(br
	 * .com.
	 * ggas.cadastro.levantamentomercado.dominio.LevantamentoMercado)
	 */
	@Override
	public LevantamentoMercadoTO converterLevantamentoMercadoEmLevantamentoMercadoTO(LevantamentoMercado levantamentoMercado)
					throws GGASException {

		ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();

		LevantamentoMercadoTO levantamentoMercadoTO = new LevantamentoMercadoTO();

		if (levantamentoMercado.getChavePrimaria() > 0) {
			levantamentoMercadoTO.setChavePrimaria(levantamentoMercado.getChavePrimaria());
		}

		if (levantamentoMercado.getImovel() != null) {
			ImovelTO imovelTO = controladorImovel.converterImovelEmImovelTO(levantamentoMercado.getImovel());
			levantamentoMercadoTO.setImovelTO(imovelTO);
		}

		if (levantamentoMercado.getSegmento() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(levantamentoMercado.getSegmento().getChavePrimaria());
			combo.setDescricao(levantamentoMercado.getSegmento().getDescricao());
			levantamentoMercadoTO.setSegmento(combo);
		}

		if (levantamentoMercado.getVigenciaInicio() != null) {
			levantamentoMercadoTO.setVigenciaInicio(Util.converterDataParaString(levantamentoMercado.getVigenciaInicio()));
		}

		if (levantamentoMercado.getVigenciaFim() != null) {
			levantamentoMercadoTO.setVigenciaFim(Util.converterDataParaString(levantamentoMercado.getVigenciaFim()));
		}

		levantamentoMercadoTO.setUnidadesConsumidoras(levantamentoMercado.isUnidadesConsumidoras());

		if (!StringUtils.isEmpty(levantamentoMercado.getQuantidadeUnidadesConsumidoras())) {
			levantamentoMercadoTO.setQuantidadeUnidadesConsumidoras(levantamentoMercado.getQuantidadeUnidadesConsumidoras());
		}

		if (levantamentoMercado.getStatus() != null) {
			TipoComboBoxTO status = new TipoComboBoxTO();
			status.setChavePrimaria(levantamentoMercado.getStatus().getChavePrimaria());
			status.setDescricao(levantamentoMercado.getStatus().getDescricao());
			levantamentoMercadoTO.setStatus(status);
		}

		if (!CollectionUtils.isEmpty(levantamentoMercado.getListaTipoCombustivel())) {
			Collection<LevantamentoMercadoTipoCombustivelTO> listaTipoCombustivel = new ArrayList<>();
			for (LevantamentoMercadoTipoCombustivel levaMercadoCombustivel : levantamentoMercado.getListaTipoCombustivel()) {

				LevantamentoMercadoTipoCombustivelTO tipoCombustivelTO = new LevantamentoMercadoTipoCombustivelTO();

				if (levaMercadoCombustivel.getChavePrimaria() > 0) {
					tipoCombustivelTO.setChavePrimaria(levaMercadoCombustivel.getChavePrimaria());
				}

				if (levaMercadoCombustivel.getTipoCombustivel() != null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(levaMercadoCombustivel.getTipoCombustivel().getChavePrimaria());
					combo.setDescricao(levaMercadoCombustivel.getTipoCombustivel().getDescricao());
					tipoCombustivelTO.setTipoCombustivel(combo);
				} else if (levaMercadoCombustivel.getTipoCombustivel() == null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(Long.valueOf(CHAVE_CAMPO_NAO_APLICAVEL));
					combo.setDescricao(CONSTANTE_NAO_SE_APLICA);
					tipoCombustivelTO.setTipoCombustivel(combo);
				}

				if (levaMercadoCombustivel.getConsumo() != null) {
					tipoCombustivelTO.setConsumo(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor",
									levaMercadoCombustivel.getConsumo(), Constantes.LOCALE_PADRAO, NUMERO_CASAS_DECIMAIS));
				}

				if (levaMercadoCombustivel.getPreco() != null) {
					tipoCombustivelTO.setPreco(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor",
									levaMercadoCombustivel.getPreco(), Constantes.LOCALE_PADRAO, NUMERO_CASAS_DECIMAIS));
				}

				if (levaMercadoCombustivel.getFornecedor() != null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(levaMercadoCombustivel.getFornecedor().getChavePrimaria());
					combo.setDescricao(levaMercadoCombustivel.getFornecedor().getDescricao());
					tipoCombustivelTO.setFornecedor(combo);
				} else if (levaMercadoCombustivel.getFornecedor() == null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(Long.valueOf(CHAVE_CAMPO_NAO_APLICAVEL));
					combo.setDescricao(CONSTANTE_NAO_SE_APLICA);
					tipoCombustivelTO.setFornecedor(combo);
				}

				if (levaMercadoCombustivel.getPeriodicidade() != null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(levaMercadoCombustivel.getPeriodicidade().getChavePrimaria());
					combo.setDescricao(levaMercadoCombustivel.getPeriodicidade().getDescricao());
					tipoCombustivelTO.setPeriodicidade(combo);
				} else if (levaMercadoCombustivel.getPeriodicidade() == null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(Long.valueOf(CHAVE_CAMPO_NAO_APLICAVEL));
					combo.setDescricao(CONSTANTE_NAO_SE_APLICA);
					tipoCombustivelTO.setPeriodicidade(combo);
				}

				if (levaMercadoCombustivel.getModalidadeUso() != null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(levaMercadoCombustivel.getModalidadeUso().getChavePrimaria());
					combo.setDescricao(levaMercadoCombustivel.getModalidadeUso().getDescricao());
					tipoCombustivelTO.setModalidadeUso(combo);
				} else if (levaMercadoCombustivel.getModalidadeUso() == null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(Long.valueOf(CHAVE_CAMPO_NAO_APLICAVEL));
					combo.setDescricao(CONSTANTE_NAO_SE_APLICA);
					tipoCombustivelTO.setModalidadeUso(combo);
				}

				if (levaMercadoCombustivel.getRedeEstado() != null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(levaMercadoCombustivel.getRedeEstado().getChavePrimaria());
					combo.setDescricao(levaMercadoCombustivel.getRedeEstado().getDescricao());
					tipoCombustivelTO.setRedeEstado(combo);
				}

				tipoCombustivelTO.setCentral(levaMercadoCombustivel.isCentral());

				if (!StringUtils.isEmpty(levaMercadoCombustivel.getLocalizacaoCentral())) {
					tipoCombustivelTO.setLocalizacaoCentral(levaMercadoCombustivel.getLocalizacaoCentral());
				}

				if (!StringUtils.isEmpty(levaMercadoCombustivel.getLocalizacaoAbrigo())) {
					tipoCombustivelTO.setLocalizacaoAbrigo(levaMercadoCombustivel.getLocalizacaoAbrigo());
				}

				if (levaMercadoCombustivel.getTipoBotijao() != null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(levaMercadoCombustivel.getTipoBotijao().getChavePrimaria());
					combo.setDescricao(levaMercadoCombustivel.getTipoBotijao().getDescricao());
					tipoCombustivelTO.setTipoBotijao(combo);
				}

				if (levaMercadoCombustivel.getRedeMaterial() != null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(levaMercadoCombustivel.getRedeMaterial().getChavePrimaria());
					combo.setDescricao(levaMercadoCombustivel.getRedeMaterial().getDescricao());
					tipoCombustivelTO.setRedeMaterial(combo);
				} else if (levaMercadoCombustivel.getRedeMaterial() == null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(Long.valueOf(CHAVE_CAMPO_NAO_APLICAVEL));
					combo.setDescricao(CONSTANTE_NAO_SE_APLICA);
					tipoCombustivelTO.setRedeMaterial(combo);
				}

				if (!StringUtils.isEmpty(levaMercadoCombustivel.getRedeTempo())) {
					tipoCombustivelTO.setRedeTempo(levaMercadoCombustivel.getRedeTempo());
				}

				tipoCombustivelTO.setVentilacao(levaMercadoCombustivel.isVentilacao());

				if (!StringUtils.isEmpty(levaMercadoCombustivel.getObservacoes())) {
					tipoCombustivelTO.setObservacoes(levaMercadoCombustivel.getObservacoes());
				}

				tipoCombustivelTO.setHabilitado(Boolean.TRUE);

				if (!CollectionUtils.isEmpty(levaMercadoCombustivel.getListaAparelhos())) {
					Collection<LevantamentoMercadoTipoCombustivelAparelhoTO> listaAparelhosTO = 
									new ArrayList<>();
					for (LevantamentoMercadoTipoCombustivelAparelho tipoAparelho : levaMercadoCombustivel.getListaAparelhos()) {
						LevantamentoMercadoTipoCombustivelAparelhoTO levantamentoMercadoTipoCombustivelAparelhoTO = 
										new LevantamentoMercadoTipoCombustivelAparelhoTO();

						if (tipoAparelho.getChavePrimaria() > 0) {
							levantamentoMercadoTipoCombustivelAparelhoTO.setChavePrimaria(tipoAparelho.getChavePrimaria());
						}

						if (tipoAparelho.getTipoAparelho() != null) {
							TipoComboBoxTO combo = new TipoComboBoxTO();
							combo.setChavePrimaria(tipoAparelho.getTipoAparelho().getChavePrimaria());
							combo.setDescricao(tipoAparelho.getTipoAparelho().getDescricao());
							levantamentoMercadoTipoCombustivelAparelhoTO.setTipoAparelho(combo);
						}

						levantamentoMercadoTipoCombustivelAparelhoTO.setHabilitado(Boolean.TRUE);

						listaAparelhosTO.add(levantamentoMercadoTipoCombustivelAparelhoTO);

					}
					tipoCombustivelTO.getListaAparelhos().addAll(listaAparelhosTO);
				}

				listaTipoCombustivel.add(tipoCombustivelTO);
			}
			levantamentoMercadoTO.getListaTipoCombustivel().addAll(listaTipoCombustivel);
		}

		levantamentoMercadoTO.setHabilitado(levantamentoMercado.isHabilitado());

		ControladorProposta controladorProposta = (ControladorProposta) ServiceLocator.getInstancia().getBeanPorID(
						ControladorProposta.BEAN_ID_CONTROLADOR_PROPOSTA);

		if (levantamentoMercado.getProposta() != null) {
			PropostaTO propostaTO = controladorProposta.converterPropostaEmPropostaTO(levantamentoMercado.getProposta());
			levantamentoMercadoTO.setPropostaTO(propostaTO);
		}

		if (levantamentoMercado.getAgendaVisitaAgente() != null) {
			AgendaVisitaAgenteTO agendaTO = converterAgendaVisitaAgenteEmAgendaVisitaAgenteTO(levantamentoMercado.getAgendaVisitaAgente());
			levantamentoMercadoTO.setAgendaVisitaAgente(agendaTO);
		}

		return levantamentoMercadoTO;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#listarLevantamentoMercadoPorChaves(java.lang.Long[])
	 */
	@Override
	public Collection<LevantamentoMercado> listarLevantamentoMercadoPorChaves(Long[] chavesPrimarias) {

		return repositorioLevantamentoMercado.listarLevantamentoMercadoPorChaves(chavesPrimarias);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#converterAgendaVisitaAgenteTOEmAgenteVisitaAgente(br.com
	 * .ggas
	 * .webservice.AgendaVisitaAgenteTO)
	 */
	@Override
	public AgendaVisitaAgente converterAgendaVisitaAgenteTOEmAgenteVisitaAgente(AgendaVisitaAgenteTO agendaVisitaAgenteTO)
					throws GGASException {

		AgendaVisitaAgente agendaVisitaAgente = new AgendaVisitaAgente();

		if (agendaVisitaAgenteTO.getChavaPramaria() != null && agendaVisitaAgenteTO.getChavaPramaria() > 0) {
			agendaVisitaAgente.setChavePrimaria(agendaVisitaAgenteTO.getChavaPramaria());
			AgendaVisitaAgente agendaOrigi = this.obterAgendaVisitaAgente(agendaVisitaAgenteTO.getChavaPramaria());
			agendaVisitaAgente.setVersao(agendaOrigi.getVersao());
		}

		agendaVisitaAgente.setDataVisita(Util.converterCampoStringParaData("Data Apresentação Condomínio",
						agendaVisitaAgenteTO.getDataVisita(), Constantes.FORMATO_DATA_BR));

		agendaVisitaAgente.setHoraVisita(agendaVisitaAgenteTO.getHoraVisita());

		agendaVisitaAgente.setHabilitado(Boolean.TRUE);

		return agendaVisitaAgente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado#converterAgendaVisitaAgenteEmAgendaVisitaAgenteTO(br.com
	 * .ggas
	 * .cadastro.levantamentomercado.dominio.AgendaVisitaAgente)
	 */
	@Override
	public AgendaVisitaAgenteTO converterAgendaVisitaAgenteEmAgendaVisitaAgenteTO(AgendaVisitaAgente agendaVisitaAgente) {

		AgendaVisitaAgenteTO agendaVisitaAgenteTO = new AgendaVisitaAgenteTO();

		if (agendaVisitaAgente.getChavePrimaria() > 0) {
			agendaVisitaAgenteTO.setChavaPramaria(agendaVisitaAgente.getChavePrimaria());
		}

		if (agendaVisitaAgente.getDataVisita() != null) {
			agendaVisitaAgenteTO.setDataVisita(Util.converterDataParaString(agendaVisitaAgente.getDataVisita()));
		}

		if (agendaVisitaAgente.getHoraVisita() != null) {
			agendaVisitaAgenteTO.setHoraVisita(agendaVisitaAgente.getHoraVisita());
		}

		return agendaVisitaAgenteTO;

	}

	@Override
	public LevantamentoMercado obterLevantamentoMercadoChaveAgenda(Long chaveAgenda) {

		return repositorioLevantamentoMercado.obterLevantamentoMercadoChaveAgenda(chaveAgenda);
	}

}
