
package br.com.ggas.cadastro.levantamentomercado.repositorio;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.cadastro.levantamentomercado.dominio.AgendaVisitaAgente;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercado;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercadoTipoCombustivel;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * 
 * Classe responsável pelos métodos relacionados a inserção e consulta
 * do levantamento de mercado
 *
 */
@Repository
public class RepositorioLevantamentoMercado extends RepositorioGenerico {

	
	
	private static final String CONSTANTE_SEGMENTO = "segmento";

	private static final String CONSTANTE_IMOVEL = "imovel";

	private static final int TAMANHO_PERIODO_VISITA_FINAL = 10;

	private static final int TAMANHO_VISITA_INICIAL = 10;

	private static final int TAMANHO_DATA_VISITA = 10;

	private static final String ID_LM = "chavePrimariaLM";

	private static final String DESCRICAO_IMOVEL = "nome";

	private static final String MATRICULA_IMOVEL = "chavePrimaria";

	private static final String NUMERO_IMOVEL = "numeroImovel";

	private static final String COMPLEMENTO_IMOVEL = "complementoImovel";

	private static final String INDICADOR_CONDOMINIO = "indicadorCondominioAmbos";

	private static final String HABILITADO = "habilitado";
	
	private static final String CONSTANTE_BOOLEAN_FALSE = "false";
	
	private static final String CONSTANTE_BOOLEAN_TRUE = "true";

	/**
	 * Instantiates a new repositorio levantamento mercado.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioLevantamentoMercado(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new LevantamentoMercado();
	}

	@Override
	public Class<?> getClasseEntidade() {

		return LevantamentoMercado.class;
	}

	public Class<?> getClasseEntidadeAgenda() {

		return AgendaVisitaAgente.class;
	}

	public Class<?> getClasseLevantamentoMercadoTipoCombustivel() {

		return LevantamentoMercadoTipoCombustivel.class;
	}
	
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if(entidadeNegocio instanceof LevantamentoMercado){
			LevantamentoMercado levantamentoMercado = (LevantamentoMercado) entidadeNegocio;
			if(!CollectionUtils.isEmpty(levantamentoMercado.getListaTipoCombustivel())){
				for(LevantamentoMercadoTipoCombustivel levanTipoCombustivel: levantamentoMercado.getListaTipoCombustivel()){
					if(levanTipoCombustivel.getTipoCombustivel() != null && levanTipoCombustivel.getTipoCombustivel().getChavePrimaria() < 0){
						levanTipoCombustivel.setTipoCombustivel(null);
					}
					
					if(levanTipoCombustivel.getFornecedor() != null && levanTipoCombustivel.getFornecedor().getChavePrimaria() < 0){
						levanTipoCombustivel.setFornecedor(null);
					}
					
					if(levanTipoCombustivel.getPeriodicidade() != null && levanTipoCombustivel.getPeriodicidade().getChavePrimaria() < 0){
						levanTipoCombustivel.setPeriodicidade(null);
					}
					
					if(levanTipoCombustivel.getModalidadeUso() != null && levanTipoCombustivel.getModalidadeUso().getChavePrimaria() < 0){
						levanTipoCombustivel.setModalidadeUso(null);
					}
					
					if(levanTipoCombustivel.getRedeMaterial() != null && levanTipoCombustivel.getRedeMaterial().getChavePrimaria() < 0){
						levanTipoCombustivel.setRedeMaterial(null);
					}
				}
			}
		}
	}
	
	
	/**
	 * Obter levantamento mercado por id imovel.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return {@link LevantamentoMercado}
	 */
	@SuppressWarnings("squid:S1192")
	public LevantamentoMercado obterLevantamentoMercadoPorIdImovel(Long chavePrimaria) {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias(CONSTANTE_IMOVEL, CONSTANTE_IMOVEL);
		criteria.createAlias(CONSTANTE_SEGMENTO, CONSTANTE_SEGMENTO);

		if(chavePrimaria != null && chavePrimaria > 0) {
			criteria.add(Restrictions.eq("imovel.chavePrimaria", chavePrimaria));
		}

		return (LevantamentoMercado) criteria.uniqueResult();
	}

	/**
	 * Listar levantamento mercado.
	 * 
	 * @param filtro {@link Map}
	 * @return {@link Collection<LevantamentoMercado>}
	 * @throws GGASException  {@link GGASException}
	 */
	@SuppressWarnings("squid:S1192")
	public Collection<LevantamentoMercado> listarLevantamentoMercado(Map<String, Object> filtro) throws GGASException{
		
		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias(CONSTANTE_IMOVEL, CONSTANTE_IMOVEL);
		criteria.createAlias(CONSTANTE_SEGMENTO, CONSTANTE_SEGMENTO, Criteria.INNER_JOIN);

		if(filtro != null) {
			Long chavePrimariaLM = (Long) filtro.get(ID_LM);
			String descricaoImovel = (String) filtro.get(DESCRICAO_IMOVEL);
			Long matriculaImovel = (Long) filtro.get(MATRICULA_IMOVEL);
			String complementoImovel = (String) filtro.get(COMPLEMENTO_IMOVEL);
			String numeroImovel = (String) filtro.get(NUMERO_IMOVEL);
			String indicadorCondominioAmbos = (String) filtro.get(INDICADOR_CONDOMINIO);
			String habilitado = (String) filtro.get(HABILITADO);
			Long idAgente = (Long) filtro.get("idAgente");
			String dataVisita = (String) filtro.get("dataVisita");
			String periodoVisitaInicial = (String) filtro.get("periodoVisitaInicial");
			String periodoVisitaFinal = (String) filtro.get("periodoVisitaFinal");
			String visitaAgendada = (String) filtro.get("visitaAgendada");
			Long idStatus = (Long) filtro.get("idStatus");
			String bairro = (String) filtro.get("bairro");
			String rua = (String) filtro.get("rua");

			if(chavePrimariaLM != null && chavePrimariaLM > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimariaLM));
			}

			if(matriculaImovel != null && matriculaImovel > 0) {
				criteria.add(Restrictions.eq("imovel.chavePrimaria", matriculaImovel));

			}

			if(descricaoImovel != null && descricaoImovel.length() > 0) {
				criteria.add(Restrictions.ilike("imovel.nome", Util.formatarTextoConsulta(descricaoImovel)));
			}

			if(!StringUtils.isEmpty(complementoImovel)) {
				criteria.add(Restrictions.like("imovel.descricaoComplemento", Util.formatarTextoConsulta(complementoImovel)));
			}

			if(numeroImovel != null && numeroImovel.length() > 0) {
				criteria.add(Restrictions.like("imovel.numeroImovel", numeroImovel));
			}

			if(CONSTANTE_BOOLEAN_FALSE.equalsIgnoreCase(habilitado) || CONSTANTE_BOOLEAN_TRUE.equalsIgnoreCase(habilitado)) {
				criteria.add(Restrictions.eq(HABILITADO, Boolean.valueOf(habilitado)));
			}
			
			if(idStatus != null && idStatus > 0){
				criteria.createCriteria("status", "status", Criteria.INNER_JOIN).add(Restrictions.eq("status.chavePrimaria", idStatus));
			}

			if(!StringUtils.isEmpty(bairro) || !StringUtils.isEmpty(rua)){
				criteria.createCriteria("imovel.quadraFace", "quadraFace", Criteria.INNER_JOIN);
				criteria.createCriteria("quadraFace.endereco", "endereco", Criteria.INNER_JOIN);
				criteria.createCriteria("endereco.cep", "cep", Criteria.INNER_JOIN);
			}
			
			if(!StringUtils.isEmpty(bairro)){
				criteria.add(Restrictions.ilike("cep.bairro", Util.formatarTextoConsulta(bairro)));
			}

			if(!StringUtils.isEmpty(rua)){
				criteria.add(Restrictions.ilike("cep.logradouro", Util.formatarTextoConsulta(rua)));
			}

			if(indicadorCondominioAmbos != null) {
				if("ambos".equalsIgnoreCase(indicadorCondominioAmbos)) {
					Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
					if((chavePrimaria != null) && (chavePrimaria > 0)) {
						criteria.add(Restrictions.eq("imovel.chavePrimaria", chavePrimaria));
					} else {
						criteria.setFetchMode("imovel.imovelCondominio", FetchMode.JOIN);
					}
				} else if(CONSTANTE_BOOLEAN_TRUE.equalsIgnoreCase(indicadorCondominioAmbos)) {
					Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
					if(chavePrimaria != null && chavePrimaria > 0) {
						criteria.add(Restrictions.eq("imovel.chavePrimaria", chavePrimaria));
					}
					criteria.add(Restrictions.eq("imovel.condominio", Boolean.TRUE));
				} else if(CONSTANTE_BOOLEAN_FALSE.equalsIgnoreCase(indicadorCondominioAmbos)) {
					Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
					if(chavePrimaria != null && chavePrimaria > 0) {
						criteria.add(Restrictions.eq("imovel.chavePrimaria", chavePrimaria));
					}
					criteria.add(Restrictions.eq("imovel.condominio", Boolean.FALSE));
				}
			}
			
			if(idAgente != null){
				criteria.createCriteria("imovel.agente", "agente", Criteria.INNER_JOIN).add(Restrictions.eq("agente.chavePrimaria", idAgente));
			}
			
			if(dataVisita != null && dataVisita.length() == TAMANHO_DATA_VISITA){
				Date data = Util.converterCampoStringParaData("Data Visita", dataVisita, Constantes.FORMATO_DATA_BR);
				criteria.createCriteria("agendaVisitaAgente", "agendaVisitaAgente", Criteria.INNER_JOIN).add(
								Restrictions.eq("agendaVisitaAgente.dataVisita", data));
			}
			
			if(periodoVisitaInicial != null && periodoVisitaInicial.length() == TAMANHO_VISITA_INICIAL){
				Date data = Util.converterCampoStringParaData("Data Visita", periodoVisitaInicial, Constantes.FORMATO_DATA_BR);
				criteria.createCriteria("agendaVisitaAgente", "agendaVisitaAgente", Criteria.INNER_JOIN).add(
								Restrictions.ge("agendaVisitaAgente.dataVisita", data));
			}
			
			if(periodoVisitaFinal != null && periodoVisitaFinal.length() == TAMANHO_PERIODO_VISITA_FINAL){
				Date data = Util.converterCampoStringParaData("Data Visita", periodoVisitaFinal, Constantes.FORMATO_DATA_BR);
				criteria.add(Restrictions.le("agendaVisitaAgente.dataVisita", data));
			}
			
			if(!"todos".equalsIgnoreCase(visitaAgendada)){
				if(CONSTANTE_BOOLEAN_TRUE.equalsIgnoreCase(visitaAgendada)){
					criteria.add(Restrictions.isNotNull("agendaVisitaAgente"));
				}else if(CONSTANTE_BOOLEAN_FALSE.equalsIgnoreCase(visitaAgendada)){
					criteria.add(Restrictions.isNull("agendaVisitaAgente"));
				}
			}
			
			criteria.addOrder(Order.asc(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA));
		}

		return (Collection<LevantamentoMercado>) criteria.list();
	}

	/**
	 * Obter levantamento mercado por chave.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return {@link LevantamentoMercado}
	 */
	public LevantamentoMercado obterLevantamentoMercadoPorChave(Long chavePrimaria) {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias(CONSTANTE_IMOVEL, CONSTANTE_IMOVEL);
		criteria.createAlias(CONSTANTE_SEGMENTO, CONSTANTE_SEGMENTO);

		if(chavePrimaria != null && chavePrimaria > 0) {
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
		}

		return (LevantamentoMercado) criteria.uniqueResult();
	}

	/**
	 * Obter agenda.
	 * 
	 * @param chave
	 *            the chave
	 * @return {@link AgendaVisitaAgente}
	 */
	public AgendaVisitaAgente obterAgendaVisitaAgente(Long chave) {

		Criteria criteria = createCriteria(getClasseEntidadeAgenda());

		if(chave != null && chave > 0) {
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chave));
		}

		return (AgendaVisitaAgente) criteria.uniqueResult();
	}

	/**
	 * Listar levantamento mercado tipo combustivel por lm.
	 * 
	 * @param chave
	 *            the chave
	 * @return the collection
	 */
	public Collection<LevantamentoMercadoTipoCombustivel> listarLevantamentoMercadoTipoCombustivelPorLM(Long chave) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseLevantamentoMercadoTipoCombustivel().getSimpleName());
		hql.append(" WHERE ");
		hql.append(" levantamentoMercado.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chave);

		return query.list();
	}

	/**
	 * Obter levantamento mercado por chave proposta.
	 * 
	 * @param chaveProposta
	 *            the chave proposta
	 * @return {@link LevantamentoMercado}
	 */
	public LevantamentoMercado obterLevantamentoMercadoPorChaveProposta(Long chaveProposta) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" levantamentoMercado ");
		hql.append(" WHERE ");
		hql.append(" levantamentoMercado.proposta.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chaveProposta);

		return (LevantamentoMercado) query.uniqueResult();
	}

	/**
	 * Listar levantamento mercado por chaves.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @return Collection<LevantamentoMercado>
	 */
	public Collection<LevantamentoMercado> listarLevantamentoMercadoPorChaves(Long[] chavesPrimarias) {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" chavePrimaria in (:chavesPrimarias) ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("chavesPrimarias", chavesPrimarias);

		return query.list();
	}
	
	/**]
	 * 
	 * @param chaveAgenda the chave agenda
	 * @return LevantamentoMercado
	 */
	public LevantamentoMercado obterLevantamentoMercadoChaveAgenda(Long chaveAgenda){
			
		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.createAlias(CONSTANTE_IMOVEL, CONSTANTE_IMOVEL);
		criteria.createAlias(CONSTANTE_SEGMENTO, CONSTANTE_SEGMENTO);
		criteria.createAlias("agendaVisitaAgente", "agendaVisitaAgente");
		
		criteria.add(Restrictions.eq("agendaVisitaAgente.chavePrimaria", chaveAgenda));
		
		return (LevantamentoMercado) criteria.uniqueResult();
	}
	
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if(entidadeNegocio instanceof LevantamentoMercado){
			LevantamentoMercado levantamentoMercado = (LevantamentoMercado) entidadeNegocio;
			if(!CollectionUtils.isEmpty(levantamentoMercado.getListaTipoCombustivel())){
				for(LevantamentoMercadoTipoCombustivel levanTipoCombustivel: levantamentoMercado.getListaTipoCombustivel()){
					if(levanTipoCombustivel.getTipoCombustivel() != null && levanTipoCombustivel.getTipoCombustivel().getChavePrimaria() < 0){
						levanTipoCombustivel.setTipoCombustivel(null);
					}
					
					if(levanTipoCombustivel.getFornecedor() != null && levanTipoCombustivel.getFornecedor().getChavePrimaria() < 0){
						levanTipoCombustivel.setFornecedor(null);
					}
					
					if(levanTipoCombustivel.getPeriodicidade() != null && levanTipoCombustivel.getPeriodicidade().getChavePrimaria() < 0){
						levanTipoCombustivel.setPeriodicidade(null);
					}
					
					if(levanTipoCombustivel.getModalidadeUso() != null && levanTipoCombustivel.getModalidadeUso().getChavePrimaria() < 0){
						levanTipoCombustivel.setModalidadeUso(null);
					}
					
					if(levanTipoCombustivel.getRedeMaterial() != null && levanTipoCombustivel.getRedeMaterial().getChavePrimaria() < 0){
						levanTipoCombustivel.setRedeMaterial(null);
					}
				}
			}
		}
	}
}
