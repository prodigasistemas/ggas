
package br.com.ggas.cadastro.levantamentomercado.agente.negocio;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.levantamentomercado.agente.dominio.Agente;
import br.com.ggas.cadastro.levantamentomercado.agente.repositorio.RepositorioAgente;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
/**
 * 
 * Classe responsável por implementar a chamada ao repositório
 * relaconado a inserção e consulta de Agente 
 *
 */
@Service("controladorAgenteImpl")
@Transactional
public class ControladorAgenteImpl implements ControladorAgente {

	/**
	 * Auto injeção do repositório para uso na classe.
	 */
	@Autowired
	public RepositorioAgente repositorioAgente;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente#inserir(br.com.ggas.cadastro.levantamentomercado.agente
	 * .dominio.Agente)
	 */
	@Override
	public Agente inserir(Agente agente) throws NegocioException {

		repositorioAgente.inserir(agente);

		return agente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente#consultarAgente(br.com.ggas.cadastro.levantamentomercado
	 * .agente.dominio.Agente, java.lang.String)
	 */
	@Override
	public Collection<Agente> consultarAgente(Agente agente, String habilitado) throws NegocioException {

		return repositorioAgente.consultarAgente(agente, habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente#removerAgente(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerAgente(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		repositorioAgente.remover(chavesPrimarias, dadosAuditoria);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente#atualizarAgente(br.com.ggas.cadastro.levantamentomercado
	 * .agente.dominio.Agente)
	 */
	@Override
	public void atualizarAgente(Agente agente) throws NegocioException, ConcorrenciaException {

		this.repositorioAgente.atualizar(agente, Agente.class);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente#removerAgente(br.com.ggas.cadastro.levantamentomercado.
	 * agente.dominio.Agente)
	 */
	@Override
	public void removerAgente(Agente agente) throws NegocioException {

		repositorioAgente.remover(agente);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente#obterAgentePorChaveFuncionario(java.lang.Long,
	 * java.lang.String)
	 */
	@Override
	public Collection<Agente> obterAgentePorChaveFuncionario(Long chaveFuncionario, String habilitado) throws NegocioException {

		return repositorioAgente.obterAgentePorChaveFuncionario(chaveFuncionario, habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente#obterAgente(java.lang.Long)
	 */
	@Override
	public Agente obterAgente(Long chavePrimaria) throws NegocioException {

		return repositorioAgente.obterAgente(chavePrimaria);
	}
}
