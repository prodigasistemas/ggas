
package br.com.ggas.cadastro.levantamentomercado.agente.negocio;

import java.util.Collection;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.levantamentomercado.agente.dominio.Agente;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
/**
 * 
 * Interface responsável pela assinatura dos métodos
 * relacionados ao ControladorAgenteImpl
 *
 */
public interface ControladorAgente {

	/**
	 * Inserir.
	 * 
	 * @param agente
	 *            the agente
	 * @return the agente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Agente inserir(Agente agente) throws NegocioException;

	/**
	 * Consultar agente.
	 * 
	 * @param agente
	 *            the agente
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Agente> consultarAgente(Agente agente, String habilitado) throws NegocioException;

	/**
	 * Remover agente.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerAgente(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Atualizar agente.
	 * 
	 * @param agente
	 *            the agente
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarAgente(Agente agente) throws NegocioException, ConcorrenciaException;

	/**
	 * Remover agente.
	 * 
	 * @param agente
	 *            the agente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerAgente(Agente agente) throws NegocioException;

	/**
	 * Obter agente.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the agente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Agente obterAgente(Long chavePrimaria) throws NegocioException;

	/**
	 * Obter agente por chave funcionario.
	 * 
	 * @param chaveFuncionario
	 *            the chave funcionario
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<Agente> obterAgentePorChaveFuncionario(Long chaveFuncionario, String habilitado) throws NegocioException;

}
