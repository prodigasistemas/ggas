
package br.com.ggas.cadastro.levantamentomercado.lm.apresentacao;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.ContatoImovel;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.TipoBotijao;
import br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente;
import br.com.ggas.cadastro.levantamentomercado.dominio.AgendaVisitaAgente;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercado;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercadoTipoCombustivel;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercadoTipoCombustivelAparelho;
import br.com.ggas.cadastro.levantamentomercado.dominio.LevantamentoMercadoVO;
import br.com.ggas.cadastro.levantamentomercado.negocio.ControladorLevantamentoMercado;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.cadastro.operacional.impl.RedeMaterialImpl;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.impl.PeriodicidadeImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
/**
 * 
 * Classe responsável pelas telas relacionadas 
 * a classe LevantamentoMercado
 *
 */
@Controller
public class LevantamentoMercadoAction extends GenericAction {

	private static final int TAMANHO_HORARIO = 5;

	private static final int NUMERO_CASAS_DECIMAIS = 2;

	private static final int CHAVE_CAMPO_NAO_APLICAVEL = -2;

	private static final int TAMANHO_PERIODO_VISITA_FINAL = 10;

	private static final int TAMANHO_PERIODO_VISITA_INICIAL = 10;

	private static final int TAMANHO_DATA = 10;

	private static final String VALOR = "Valor";

	private static final String TELA_GRID_TIPO_APARELHO = "gridTipoAparelho";

	private static final String TELA_AJAX_ERRO = "ajaxErro";

	private static final String PERIODO_VISITA_FINAL = "periodoVisitaFinal";

	private static final String PERIODO_VISITA_INICIAL = "periodoVisitaInicial";

	private static final String TELA_FORWARD_PESQUISAR_LEVANTAMENTO_MERCADO = "forward:/pesquisarLevantamentoMercado";

	private static final String TELA_EXIBIR_PESQUISA_LEVANTAMENTO_MERCADO = "exibirPesquisaLevantamentoMercado";

	private static final String LISTA_STATUS = "listaStatus";

	private static final String LISTA_AGENTES = "listaAgentes";

	private static final String VISITA_AGENDADA = "visitaAgendada";

	private static final String TODOS = "todos";

	private static final String ID_AGENTE = "idAgente";

	private static final String LISTA_TIPO_APARELHO = "listaTipoAparelho";

	private static final String CHAVE_AGENDAMENTO = "chaveAgendamento";

	private static final String FLUXO_AGENDA_ATUALIZACAO = "fluxoAgendaAtualizacao";

	private static final String AGENDA_VISITA_AGENTE = "AgendaVisitaAgente";

	private static final String LEVANTAMENTO_MERCADO2 = "levantamentoMercado";

	private static final String LEVANTAMENTO_DE_MERCADO = "Levantamento de Mercado";

	private static final String INICIO_VIGENCIA = "inicioVigencia";

	private static final String FINAL_VIGENCIA = "finalVigencia";

	private static final String ID_IMOVEL = "idImovel";

	private static final String IMOVEIS = "imoveis";

	private static final String IMOVEL = "imovel";

	private static final String LISTA_LMVO = "listaLMVO";

	private static final String INDICADOR_CONDOMINIO = "indicadorCondominio";

	private static final String NUMERO_IMOVEL_TEXTO = "numeroImovelTexto";

	private static final String MATRICULA_IMOVEL_TEXTO = "matriculaImovelTexto";

	private static final String COMPLEMENTO_TEXTO = "complementoTexto";

	private static final String DESCRICAO_IMOVEL_TEXTO = "descricaoImovelTexto";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String VIGENCIA_INICIO = "vigenciaInicio";

	private static final String VIGENCIA_FIM = "vigenciaFim";

	private static final String CONDOMINIO = "condominio";

	private static final String SIM = "Sim";

	private static final String NAO = "Não";

	private static final String UNIDADES = "unidades";

	private static final String LISTA_MATERIAL_REDE = "listaMaterialRede";

	private static final String LISTA_APARELHO = "listaAparelho";

	private static final String LISTA_TIPO_COMBUSTIVEL = "tiposCombustiveis";

	private static final String LISTA_FORNECEDOR = "listaFornecedor";

	private static final String LISTA_ESTADO_REDE = "listaEstadoRede";

	private static final String LISTA_MODALIDADE_USO = "listaModalidadeUso";

	private static final String DATA_INICIAL = "Data Inicial";

	private static final String DATA_FINAL = "Data Final";

	private static final String DATA_VISITA = "Data Visita";

	private static final String TRUE = "true";

	private static final String AGENDA = "agenda";

	private static final String DATA_VISITA_AGENTE = "dataVisitaAgente";

	private static final String HORA_VISITA = "horaVisita";

	private static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String CHAVE_PRIMARIA_LM = "chavePrimariaLM";

	private static final String LEVANTAMENTO_MERCADO = "Levantamento Mercado";

	private static final String LISTA_TIPO_COMBUSTIVEL_LM = "listaTipoCombustivelLM";

	private static final String PRECO = "Preço";

	private static final String CONSUMO = "Consumo";

	private Fachada fachada = Fachada.getInstancia();

	private static final String DESCRICAO_IMOVEL = "nome";

	private static final String MATRICULA_IMOVEL = CHAVE_PRIMARIA;

	private static final String NUMERO_IMOVEL = "numeroImovel";

	private static final String COMPLEMENTO_IMOVEL = "complementoImovel";

	private static final String INDICADOR_CONDOMINIO_AMBOS = "indicadorCondominioAmbos";

	private static final String HABILITADO = "habilitado";

	private static final String ENDERECO_IMOVEL = "enderecoImovel";

	private static final String FLUXO_AGENDA_ALTERACAO = FLUXO_AGENDA_ATUALIZACAO;

	private static final String SITUACAO_PROPOSTA_APROVADA = "APROVADA";
	
	@Autowired
	private ControladorAgente controladorAgente;

	@Autowired
	private ControladorLevantamentoMercado controladorLevantamentoMercado;

	/**
	 * Método responsável por exibir a tela de
	 * Pesquisa Levantamento de Mercado.
	 * 
	 * @param session
	 *            the session
	 * @return {@link ModelAndView}
	 * @throws GGASException
	 *             Caso ocorra algum erro
	 */

	@RequestMapping(TELA_EXIBIR_PESQUISA_LEVANTAMENTO_MERCADO)
	public ModelAndView exibirPesquisaLevantamentoMercado(HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_LEVANTAMENTO_MERCADO);

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		ConstanteSistema constanteSistema = fachada.obterConstantePorCodigo(Constantes.C_STATUS_LEVANTAMENTO_MERCADO);
		Collection<EntidadeConteudo> listaStatus = controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanteSistema
						.getValor()));
		session.removeAttribute(LISTA_TIPO_COMBUSTIVEL_LM);
		model.addObject(HABILITADO, TRUE);
		model.addObject(VISITA_AGENDADA, TODOS);
		session.setAttribute(LISTA_AGENTES, controladorAgente.obterAgentePorChaveFuncionario(null, TRUE));
		session.setAttribute(LISTA_STATUS, listaStatus);
		limparSessao(session);

		return model;
	}

	/**
	 * Método responsável por realizar pesquisa de Levantamento de Mercado.
	 * 
	 * @param descricaoImovelTexto descricao do imovel
	 * @param complementoTexto Complemento
	 * @param matriculaImovelTexto Matrícula do Imóvel para realizar a pesquisa
	 * @param numeroImovelTexto Número do Imóvel
	 * @param indicadorCondominio booleano indica se o imóvel é condomínio
	 * @param habilitado booleano indica se o imóvel é ativo.
	 * @param idAgente id do agente
	 * @param dataVisita data de realização da visita
	 * @param periodoVisitaInicial período inicial da visita
	 * @param periodoVisitaFinal período final da visita
	 * @param visitaAgendada {@link String}
	 * @param idStatus status atual
	 * @param descricaoBairro bairo da visita
	 * @param descricaoRua rua da visita
	 * @return {@link ModelAndView} Retorna a ação
	 * @throws GGASException Caso Ocorra Algum Erro.
	 */
	@RequestMapping("pesquisarLevantamentoMercado")
	public ModelAndView pesquisarLevantamentoMercado(@RequestParam(DESCRICAO_IMOVEL_TEXTO) String descricaoImovelTexto,
			@RequestParam(COMPLEMENTO_TEXTO) String complementoTexto, @RequestParam(MATRICULA_IMOVEL_TEXTO) String matriculaImovelTexto,
			@RequestParam(NUMERO_IMOVEL_TEXTO) String numeroImovelTexto, @RequestParam(INDICADOR_CONDOMINIO) String indicadorCondominio,
			@RequestParam(HABILITADO) String habilitado, @RequestParam(ID_AGENTE) Long idAgente,
			@RequestParam("dataVisita") String dataVisita, @RequestParam(PERIODO_VISITA_INICIAL) String periodoVisitaInicial,
			@RequestParam(PERIODO_VISITA_FINAL) String periodoVisitaFinal, @RequestParam(VISITA_AGENDADA) String visitaAgendada,
			@RequestParam("idStatus") Long idStatus, @RequestParam("descricaoBairro") String descricaoBairro,
			@RequestParam("descricaoRua") String descricaoRua) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_LEVANTAMENTO_MERCADO);

		Imovel imovel = fachada.criarImovel();

		Map<String, Object> filtro = this.popularFiltro(null, descricaoImovelTexto, complementoTexto, matriculaImovelTexto,
				numeroImovelTexto, indicadorCondominio, habilitado, imovel, idAgente, dataVisita, periodoVisitaInicial, periodoVisitaFinal,
				visitaAgendada, idStatus, descricaoBairro, descricaoRua);

		Collection<LevantamentoMercado> listaLM = controladorLevantamentoMercado.listarLevantamentoMercado(filtro);

		List<LevantamentoMercadoVO> listaLMVO = this.montarVO(listaLM);

		popularFormPesquisa(model, descricaoImovelTexto, complementoTexto, matriculaImovelTexto, numeroImovelTexto, indicadorCondominio,
				idAgente, dataVisita, periodoVisitaInicial, periodoVisitaFinal, visitaAgendada, idStatus, descricaoBairro,
				descricaoRua);
		model.addObject(LISTA_LMVO, listaLMVO);

		model.addObject(IMOVEL, imovel);

		model.addObject(HABILITADO, Boolean.valueOf(habilitado));

		return model;
	}

	/**
	 * Método responsável por realizar a pesquisa de imóveis.
	 * 
	 * @param descricaoImovelTexto a descricao imóvel
	 * @param complementoTexto o complemento
	 * @param matriculaImovelTexto a matricula imóvel
	 * @param numeroImovelTexto o número do imóvel
	 * @param indicadorCondominio o indicador se é um condomínio
	 * @param habilitado o indiador de habilitado
	 * @param descricaoBairro o bairro
	 * @param descricaoRua a rua
	 * @param visitaAgendada {@link String}
	 * @param request the request
	 * @return {@link ModelAndView}
	 * @throws GGASException Caso ocorra algum erro
	 */
	@RequestMapping("pesquisarImoveis")
	public ModelAndView pesquisarImoveis(@RequestParam(DESCRICAO_IMOVEL_TEXTO) String descricaoImovelTexto,
			@RequestParam(COMPLEMENTO_TEXTO) String complementoTexto, @RequestParam(MATRICULA_IMOVEL_TEXTO) String matriculaImovelTexto,
			@RequestParam(NUMERO_IMOVEL_TEXTO) String numeroImovelTexto, @RequestParam(INDICADOR_CONDOMINIO) String indicadorCondominio,
			@RequestParam(HABILITADO) String habilitado, @RequestParam("descricaoBairro") String descricaoBairro,
			@RequestParam("descricaoRua") String descricaoRua, @RequestParam(VISITA_AGENDADA) String visitaAgendada,
			HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_LEVANTAMENTO_MERCADO);
		Imovel imovelPesquisa = fachada.criarImovel();

		DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
				(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());

		Map<String, Object> filtro =
				this.popularFiltro(null, descricaoImovelTexto, complementoTexto, matriculaImovelTexto, numeroImovelTexto,
						indicadorCondominio, habilitado, imovelPesquisa, null, null, null, null, null, null, descricaoBairro, descricaoRua);
		Funcionario funcionario = null;
		if (dadosAuditoria != null) {
			funcionario = dadosAuditoria.getUsuario().getFuncionario();
		}
		Collection<Imovel> imoveis = null;
		if (funcionario != null) {
			imoveis = fachada.listarImoveisEmProspeccao(filtro, funcionario.getChavePrimaria());
		}

		popularFormPesquisaImovel(habilitado, visitaAgendada, model, imovelPesquisa, imoveis);

		return model;
	}

	private void popularFormPesquisaImovel(String habilitado, String visitaAgendada, ModelAndView model, Imovel imovelPesquisa,
					Collection<Imovel> imoveis) {

		model.addObject(IMOVEIS, imoveis);

		model.addObject(IMOVEL, imovelPesquisa);

		model.addObject(HABILITADO, habilitado);

		if (!TODOS.equalsIgnoreCase(visitaAgendada)) {
			model.addObject(VISITA_AGENDADA, Boolean.valueOf(visitaAgendada));
		} else {
			model.addObject(VISITA_AGENDADA, visitaAgendada);
		}
	}

	/**
	 * Método responsável por
	 * realizar pesquisa de Levantamento de Mercado.
	 * 
	 * @param idImovelSelecionado
	 *            Chave Primaria do imóvel
	 * @param descricaoImovelTexto
	 *            Descrição do imóvel
	 * @param complementoTexto
	 *            Complemento
	 * @param matriculaImovelTexto
	 *            Matrícula do Imóvel para realizar a pesquisa
	 * @param numeroImovelTexto
	 *            Número do Imóvel
	 * @param indicadorCondominio
	 *            booleano indica se o imóvel é condomínio
	 * @param habilitado
	 *            booleano indica se o imóvel é ativo.
	 * @param session
	 *            sessão
	 * @return {@link ModelAndView} Retorna a ação
	 * @exception GGASException
	 *                Caso Ocorra Algum Erro.
	 */

	@RequestMapping("exibirInclusaoLevantamentoMercado")
	public ModelAndView exibirInclusaoLevantamentoMercado(@RequestParam(ID_IMOVEL) Long idImovelSelecionado,
					@RequestParam(DESCRICAO_IMOVEL_TEXTO) String descricaoImovelTexto,
					@RequestParam(COMPLEMENTO_TEXTO) String complementoTexto,
					@RequestParam(MATRICULA_IMOVEL_TEXTO) String matriculaImovelTexto,
					@RequestParam(NUMERO_IMOVEL_TEXTO) String numeroImovelTexto,
					@RequestParam(INDICADOR_CONDOMINIO) String indicadorCondominio, @RequestParam() String habilitado, HttpSession session)
					throws GGASException {

		Imovel imovelSelecionado = fachada.buscarImovelPorChave(idImovelSelecionado);

		ModelAndView model = new ModelAndView("exibirInclusaoLevantamentoMercado");

		this.popularCampos(model, imovelSelecionado, null, false, false, true, null);
		model.addObject(IMOVEL, imovelSelecionado);
		model.addObject(habilitado, TRUE);
		model.addObject(LEVANTAMENTO_MERCADO, new LevantamentoMercado());
		limparSessao(session);

		return model;
	}

	/**
	 * Método responsável por
	 * incluir um novo levantamento de mercado.
	 * 
	 * @param idImovel
	 *            Chave Primaria do imóvel.
	 * @param finalVigencia
	 *            data final da vigência do levantamento de mercado.
	 * @param inicioVigencia
	 *            data inicial da Vigência do levantamento de mercado.
	 * @param levantamentoMercado
	 *            objeto a ser incluído.
	 * @param session
	 *            sessão
	 * @return {@link ModelAndView} Retorna a ação
	 * @throws NegocioException
	 *             the negocio exception
	 * @exception GGASException
	 *                Caso Ocorra Algum Erro.
	 */

	@RequestMapping("incluirLevantamentoMercado")
	public ModelAndView incluirLevantamentoMercado(@RequestParam(ID_IMOVEL) Long idImovel,
					@RequestParam(FINAL_VIGENCIA) String finalVigencia, @RequestParam(INICIO_VIGENCIA) String inicioVigencia,
					@ModelAttribute("levantamentoMercado") LevantamentoMercado levantamentoMercado, HttpSession session)
					throws GGASException {

		ModelAndView model = null;

		levantamentoMercado.setHabilitado(true);

		try {
			popularObjeto(levantamentoMercado, inicioVigencia, finalVigencia, idImovel, false, session);
			validarEntidade(levantamentoMercado);
			controladorLevantamentoMercado.validarDataVigencia(levantamentoMercado.getVigenciaInicio(),
							levantamentoMercado.getVigenciaFim());
			Long chave = controladorLevantamentoMercado.inserir(levantamentoMercado);
			levantamentoMercado.setChavePrimaria(chave);
			model = popularModelPesquisa(levantamentoMercado);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, LEVANTAMENTO_DE_MERCADO);
		} catch (GGASException e) {
			model = new ModelAndView("exibirInclusaoLevantamentoMercado");
			this.popularCampos(model, fachada.buscarImovelPorChave(levantamentoMercado.getImovel().getChavePrimaria()), null, false, false,
							true, null);
			model.addObject(LEVANTAMENTO_MERCADO2, levantamentoMercado);
			model.addObject(IMOVEL, levantamentoMercado.getImovel());
			model.addObject(HABILITADO, TRUE);
			if (!CollectionUtils.isEmpty(levantamentoMercado.getListaTipoCombustivel())) {
				session.setAttribute(LISTA_TIPO_COMBUSTIVEL_LM, converterListaHashSet(levantamentoMercado.getListaTipoCombustivel()));
			}
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Método responsável por
	 * exibir a tela de alteração do levantamento de mercado.
	 * 
	 * @param chavePrimaria
	 *            Chave Primaria do Levantamento de Mercado.
	 * @param session
	 *            sessão
	 * @return {@link ModelAndView} Retorna a ação
	 * @exception GGASException
	 *                Caso Ocorra Algum Erro.
	 */
	@RequestMapping("exibirAlteracaoLevantamentoMercado")
	public ModelAndView exibirAlteracaoLevantamentoMercado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpSession session)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirAlteracaoLevantamentoMercado");

		LevantamentoMercado levantamentoMercado = new LevantamentoMercado();
		try {
			levantamentoMercado = controladorLevantamentoMercado.obterLevantamentoMercado(chavePrimaria);
			validarAlteracaoLevantamentoMercado(levantamentoMercado);
			model.addObject(LEVANTAMENTO_MERCADO2, levantamentoMercado);
			popularCampos(model, levantamentoMercado.getImovel(), levantamentoMercado, true, false, false, session);
			Collection<LevantamentoMercadoTipoCombustivel> combustiveis = new ArrayList<>();
			for (LevantamentoMercadoTipoCombustivel levantamentoMercadoTipoCombustivel : levantamentoMercado.getListaTipoCombustivel()) {
				combustiveis.add(levantamentoMercadoTipoCombustivel);
			}
			session.setAttribute(LISTA_TIPO_COMBUSTIVEL_LM, combustiveis);

		} catch (GGASException e) {
			model = pesquisarLevantamentoMercado(levantamentoMercado.getImovel().getNome(), null,
							String.valueOf(levantamentoMercado.getImovel().getChavePrimaria()), null, null, "true", null, null, null, null,
							null, null, null, null);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Método responsável por alterar o levantamento de mercado.
	 * 
	 * @param lMAtualizado
	 *            objeto a ser incluído.
	 * @param finalVigencia
	 *            data final da vigência do levantamento de mercado.
	 * @param inicioVigencia
	 *            data inicial da Vigência do levantamento de mercado.
	 * @param session
	 *            sessão
	 * @param request
	 *            the request
	 * @return {@link ModelAndView} Retorna a ação
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws GGASException
	 *             the GGAS exception
	 */

	@RequestMapping("alterarLevantamentoMercado")
	public ModelAndView alterarLevantamentoMercado(@ModelAttribute(LEVANTAMENTO_MERCADO2) LevantamentoMercado lMAtualizado,
					@RequestParam(FINAL_VIGENCIA) String finalVigencia, @RequestParam(INICIO_VIGENCIA) String inicioVigencia,
					HttpSession session, HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_LEVANTAMENTO_MERCADO);

		try {
			this.popularObjeto(lMAtualizado, inicioVigencia, finalVigencia, null, true, session);
			controladorLevantamentoMercado.validarDataVigencia(lMAtualizado.getVigenciaInicio(), lMAtualizado.getVigenciaFim());
			controladorLevantamentoMercado.atualizarLevantamentoMercado(lMAtualizado);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, LEVANTAMENTO_MERCADO);
		} catch (GGASException e) {
			model = new ModelAndView("exibirAlteracaoLevantamentoMercado");
			request.setAttribute(LEVANTAMENTO_MERCADO2, lMAtualizado);
			popularCampos(model, lMAtualizado.getImovel(), lMAtualizado, true, false, false, session);
			mensagemErroParametrizado(model, e);
		}

		return model;

	}

	/**
	 * Exibir detalhamento levantamento mercado.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */

	@RequestMapping("exibirDetalhamentoLevantamentoMercado")
	public ModelAndView exibirDetalhamentoLevantamentoMercado(@RequestParam(CHAVE_PRIMARIA) Long chavePrimaria, HttpSession session)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirDetalhamentoLevantamentoMercado");

		LevantamentoMercado levantamentoMercado = controladorLevantamentoMercado.obterLevantamentoMercado(chavePrimaria);

		popularDetalhamentoTipoCombustivel(null, levantamentoMercado, model, session);

		popularCampos(model, levantamentoMercado.getImovel(), levantamentoMercado, false, true, false, session);

		return model;
	}

	/**
	 * Incluir agenda.
	 * 
	 * @param chaveLM
	 *            the chave lm
	 * @param dataVisitaAgente
	 *            the data visita agente
	 * @param agendaVisitaAgente
	 *            the agenda visita agente
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("incluirAgenda")
	public ModelAndView incluirAgenda(@RequestParam(CHAVE_PRIMARIA) Long chaveLM,
					@RequestParam(DATA_VISITA_AGENTE) String dataVisitaAgente,
					@ModelAttribute(AGENDA_VISITA_AGENTE) AgendaVisitaAgente agendaVisitaAgente, HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView("forward:/exibirPesquisaLevantamentoMercado");

		popularAgenda(agendaVisitaAgente, dataVisitaAgente, null, null);
		try {

			validarHorario(agendaVisitaAgente.getHoraVisita(), agendaVisitaAgente.getDataVisita());
			LevantamentoMercado levantamentoMercado = controladorLevantamentoMercado.obterLevantamentoMercado(chaveLM);
			Long chave = controladorLevantamentoMercado.inserirAgenda(agendaVisitaAgente);
			AgendaVisitaAgente agenda = controladorLevantamentoMercado.obterAgendaVisitaAgente(chave);
			levantamentoMercado.setAgendaVisitaAgente(agenda);
			EntidadeConteudo statusPropostaAnalise = fachada.obterEntidadeConteudo(Long.valueOf(fachada.obterConstantePorCodigo(
							Constantes.C_LEVANTAMENTO_MERCADO_EM_ANALISE).getValor()));
			levantamentoMercado.setStatus(statusPropostaAnalise);
			controladorLevantamentoMercado.atualizarLevantamentoMercado(levantamentoMercado);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, AGENDA);
		} catch (GGASException e) {
			model = new ModelAndView("forward:/exibirAgendaVisitaAgente");
			model.addObject(AGENDA_VISITA_AGENTE, agendaVisitaAgente);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Exibir agenda visita agente.
	 * 
	 * @param chaveLevantamentoMercado
	 *            the chave levantamento mercado
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirAgendaVisitaAgente")
	public ModelAndView exibirAgendaVisitaAgente(@RequestParam(CHAVE_PRIMARIA) Long chaveLevantamentoMercado, HttpSession sessao)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirAgendaVisitaAgente");

		LevantamentoMercado levantamentoMercado = new LevantamentoMercado();
		try {
			levantamentoMercado = controladorLevantamentoMercado.obterLevantamentoMercado(chaveLevantamentoMercado);
			validarStatusParaAgendamemto(levantamentoMercado);
			popularCamposAgenda(levantamentoMercado, model);
			model.addObject(LEVANTAMENTO_MERCADO2, controladorLevantamentoMercado.obterLevantamentoMercado(chaveLevantamentoMercado));

		} catch (GGASException e) {
			model = pesquisarLevantamentoMercado(levantamentoMercado.getImovel().getNome(), null,
							String.valueOf(levantamentoMercado.getImovel().getChavePrimaria()), null, null, "true", null, null, null, null,
							null, null, null, null);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Atualizar agendamento.
	 * 
	 * @param chaveAgendamento
	 *            the chave agendamento
	 * @param fluxoAgendaAtualizacao
	 *            the fluxo agenda atualizacao
	 * @param chaveLM
	 *            the chave lm
	 * @param dataVisitaAgente
	 *            the data visita agente
	 * @param agendaVisitaAgente
	 *            the agenda visita agente
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("atualizarAgendamento")
	public ModelAndView atualizarAgendamento(@RequestParam(CHAVE_AGENDAMENTO) Long chaveAgendamento,
					@RequestParam(FLUXO_AGENDA_ATUALIZACAO) String fluxoAgendaAtualizacao, @RequestParam(CHAVE_PRIMARIA) Long chaveLM,
					@RequestParam(DATA_VISITA_AGENTE) String dataVisitaAgente,
					@ModelAttribute(AGENDA_VISITA_AGENTE) AgendaVisitaAgente agendaVisitaAgente, HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView("forward:/exibirPesquisaLevantamentoMercado");

		try {

			LevantamentoMercado levantamentoMercado = controladorLevantamentoMercado.obterLevantamentoMercado(chaveLM);
			popularAgenda(agendaVisitaAgente, dataVisitaAgente, fluxoAgendaAtualizacao, chaveAgendamento);
			validarHorario(agendaVisitaAgente.getHoraVisita(), agendaVisitaAgente.getDataVisita());
			controladorLevantamentoMercado.atualizarAgenda(agendaVisitaAgente);
			levantamentoMercado.setAgendaVisitaAgente(agendaVisitaAgente);

			if (levantamentoMercado.getStatus() == null) {
				EntidadeConteudo statusPropostaAnalise = fachada.obterEntidadeConteudo(Long.valueOf(fachada.obterConstantePorCodigo(
								Constantes.C_LEVANTAMENTO_MERCADO_EM_ANALISE).getValor()));
				levantamentoMercado.setStatus(statusPropostaAnalise);
				controladorLevantamentoMercado.atualizarLevantamentoMercado(levantamentoMercado);
			} else {
				controladorLevantamentoMercado.atualizarLevantamentoMercado(levantamentoMercado);
			}

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, "Agenda");
		} catch (GGASException e) {
			model = new ModelAndView("forward:/exibirAgendaVisitaAgente");
			model.addObject(AGENDA_VISITA_AGENTE, agendaVisitaAgente);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Popular campos.
	 * 
	 * @param model
	 *            the model
	 * @param imovel
	 *            the imovel
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @param isAlteracao
	 *            the is alteracao
	 * @param isDetalhamento
	 *            the is detalhamento
	 * @param isInclusao
	 *            the is inclusao
	 * @param session
	 *            the session
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularCampos(ModelAndView model, Imovel imovel, LevantamentoMercado levantamentoMercado, boolean isAlteracao,
					boolean isDetalhamento, boolean isInclusao, HttpSession session) throws GGASException {

		model.addObject(LEVANTAMENTO_MERCADO, levantamentoMercado);
		if (levantamentoMercado != null && !CollectionUtils.isEmpty(levantamentoMercado.getListaTipoCombustivel())) {
			List<LevantamentoMercadoTipoCombustivel> lista = new ArrayList<>();
			for (LevantamentoMercadoTipoCombustivel lev : levantamentoMercado.getListaTipoCombustivel()) {
				lista.add(lev);
			}
			session.setAttribute(LISTA_TIPO_COMBUSTIVEL_LM, lista);
		}
		if (isInclusao || isAlteracao || isDetalhamento) {
			// Carrega combos
			Collection<EntidadeConteudo> fornecedores = fachada.obterListaFornecedoresCombustiveis();
			Collection<EntidadeConteudo> aparelhos = fachada.obterListaAparelhosMaiorIndiceConsumo();
			Collection<EntidadeConteudo> fontesEnergeticas = fachada.obterListaTiposFontesEnergeticas();
			Collection<EntidadeConteudo> listaEstadoRede = fachada.obterListaEstadoRede();
			Collection<RedeMaterial> listaRedeMaterial = fachada.listarRedeMaterial();
			Collection<Segmento> segmentos = fachada.consultarSegmento(new HashMap<String, Object>());
			Collection<EntidadeConteudo> listaModalidadeUso = fachada.listarModalidadeUso();
			Collection<TipoBotijao> listaTipoCilindro = fachada.listarTipoBotijao();
			Collection<Periodicidade> listaPeriodicidades = fachada.listarPeriodicidades();

			model.addObject(LISTA_MATERIAL_REDE, listaRedeMaterial);
			model.addObject(LISTA_SEGMENTO, segmentos);
			model.addObject(LISTA_APARELHO, aparelhos);
			model.addObject(LISTA_TIPO_COMBUSTIVEL, fontesEnergeticas);
			model.addObject(LISTA_FORNECEDOR, fornecedores);
			model.addObject(LISTA_ESTADO_REDE, listaEstadoRede);
			model.addObject(LISTA_MODALIDADE_USO, listaModalidadeUso);
			model.addObject("listaTipoCilindro", listaTipoCilindro);
			model.addObject("listaPeriodicidades", listaPeriodicidades);

			Collection<ContatoImovel> listaContatos = null;
			if (imovel != null) {
				// Popula dados do imóvel
				model.addObject(IMOVEL, imovel);
				if (imovel.getCondominio()) {
					model.addObject(CONDOMINIO, SIM);
				} else {
					model.addObject(CONDOMINIO, NAO);
				}

				model.addObject(ENDERECO_IMOVEL, imovel.getEnderecoFormatado());

				listaContatos = fachada.listarContatoImovelPorChaveImovel(imovel.getChavePrimaria());
			}

			ContatoImovel contato = null;
			if (!CollectionUtils.isEmpty(listaContatos)) {
				contato = this.obterContatoPrincipal(listaContatos);
				model.addObject("contato", contato);
			}
		}

		if (isAlteracao || isDetalhamento) {
			model.addObject(LEVANTAMENTO_MERCADO2, levantamentoMercado);
			model.addObject(IMOVEL, levantamentoMercado.getImovel());

			if (levantamentoMercado.getVigenciaInicio() != null) {
				model.addObject(VIGENCIA_INICIO, DataUtil.converterDataParaString(levantamentoMercado.getVigenciaInicio()));
			}

			if (levantamentoMercado.getVigenciaFim() != null) {
				model.addObject(VIGENCIA_FIM, DataUtil.converterDataParaString(levantamentoMercado.getVigenciaFim()));
			}

		}

		if (isDetalhamento) {
			boolean unidades = levantamentoMercado.isUnidadesConsumidoras();

			if (unidades) {
				model.addObject(UNIDADES, SIM);
			} else {
				model.addObject(UNIDADES, NAO);
			}

			Collection<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel = levantamentoMercado.getListaTipoCombustivel();
			if (!CollectionUtils.isEmpty(listaTipoCombustivel)) {
				List<LevantamentoMercadoTipoCombustivel> leCombustivels = new ArrayList<>();
				for (LevantamentoMercadoTipoCombustivel levCombustivel : listaTipoCombustivel) {
					leCombustivels.add(levCombustivel);
				}
				session.removeAttribute(LISTA_TIPO_COMBUSTIVEL_LM);
				session.setAttribute(LISTA_TIPO_COMBUSTIVEL_LM, leCombustivels);
			}

			verificaCamposNaoSeAplica(levantamentoMercado);

		}

		if (isAlteracao) {
			// Carrega combos
			Collection<EntidadeConteudo> fornecedores = fachada.obterListaFornecedoresCombustiveis();
			Collection<EntidadeConteudo> aparelhos = fachada.obterListaAparelhosMaiorIndiceConsumo();
			Collection<EntidadeConteudo> fontesEnergeticas = fachada.obterListaTiposFontesEnergeticas();
			Collection<EntidadeConteudo> listaEstadoRede = fachada.obterListaEstadoRede();
			Collection<RedeMaterial> listaRedeMaterial = fachada.listarRedeMaterial();
			Collection<Segmento> segmentos = fachada.consultarSegmento(new HashMap<String, Object>());
			Collection<EntidadeConteudo> listaModalidadeUso = fachada.listarModalidadeUso();
			Collection<TipoBotijao> listaTipoCilindro = fachada.listarTipoBotijao();
			Collection<Periodicidade> listaPeriodicidades = fachada.listarPeriodicidades();

			model.addObject(LISTA_MATERIAL_REDE, listaRedeMaterial);
			model.addObject(LISTA_SEGMENTO, segmentos);
			model.addObject(LISTA_APARELHO, aparelhos);
			model.addObject(LISTA_TIPO_COMBUSTIVEL, fontesEnergeticas);
			model.addObject(LISTA_FORNECEDOR, fornecedores);
			model.addObject(LISTA_ESTADO_REDE, listaEstadoRede);
			model.addObject(LISTA_MODALIDADE_USO, listaModalidadeUso);
			model.addObject("listaTipoCilindro", listaTipoCilindro);
			model.addObject("listaPeriodicidades", listaPeriodicidades);
			verificaCamposNaoSeAplica(levantamentoMercado);
		}
	}

	/**
	 * Popular objeto.
	 * 
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFinal
	 *            the data final
	 * @param idImovel
	 *            the id imovel
	 * @param isAlteracao
	 *            the is alteracao
	 * @param session
	 *            the session
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularObjeto(LevantamentoMercado levantamentoMercado, String dataInicio, String dataFinal, Long idImovel,
					boolean isAlteracao, HttpSession session) throws GGASException {

		if (isAlteracao) {

			LevantamentoMercado lmDesatualizado = controladorLevantamentoMercado.obterLevantamentoMercado(levantamentoMercado
							.getChavePrimaria());
			levantamentoMercado.setVersao(lmDesatualizado.getVersao());
			levantamentoMercado.setImovel(lmDesatualizado.getImovel());
			levantamentoMercado.setStatus(lmDesatualizado.getStatus());
			levantamentoMercado.setAgendaVisitaAgente(lmDesatualizado.getAgendaVisitaAgente());
			if (dataInicio != null && dataInicio.length() > 0) {
				levantamentoMercado.setVigenciaInicio(Util.converterCampoStringParaData(DATA_INICIAL, dataInicio,
								Constantes.FORMATO_DATA_BR));
			} else {
				levantamentoMercado.setVigenciaInicio(null);
			}

			if (dataFinal != null && dataFinal.length() > 0) {
				levantamentoMercado.setVigenciaFim(Util.converterCampoStringParaData(DATA_FINAL, dataFinal, Constantes.FORMATO_DATA_BR));
			} else {
				levantamentoMercado.setVigenciaFim(null);
			}

		} else {
			Imovel imovel = fachada.buscarImovelPorChave(idImovel);
			levantamentoMercado.setImovel(imovel);
			if (dataInicio != null && dataInicio.length() > 0) {
				levantamentoMercado.setVigenciaInicio(Util.converterCampoStringParaData(DATA_INICIAL, dataInicio,
								Constantes.FORMATO_DATA_BR));
			} else {
				levantamentoMercado.setVigenciaInicio(null);
			}

			if (dataFinal != null && dataFinal.length() > 0) {
				levantamentoMercado.setVigenciaFim(Util.converterCampoStringParaData(DATA_FINAL, dataFinal, Constantes.FORMATO_DATA_BR));
			} else {
				levantamentoMercado.setVigenciaFim(null);

			}
		}

		List<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel = (ArrayList<LevantamentoMercadoTipoCombustivel>) session
						.getAttribute(LISTA_TIPO_COMBUSTIVEL_LM);

		if (listaTipoCombustivel != null) {
			Collection<LevantamentoMercadoTipoCombustivel> listaTiposCombustiveisSet = new HashSet<>();
			listaTiposCombustiveisSet.addAll(listaTipoCombustivel);
			listaTipoCombustivel.clear();
			levantamentoMercado.setListaTipoCombustivel(listaTiposCombustiveisSet);
		} else {
			levantamentoMercado.setListaTipoCombustivel(null);
		}
	}

	/**
	 * Popular filtro.
	 * 
	 * @param chaveLM
	 *            the chave lm
	 * @param descricaoImovelTexto
	 *            the descricao imovel texto
	 * @param complementoTexto
	 *            the complemento texto
	 * @param matriculaImovelTexto
	 *            the matricula imovel texto
	 * @param numeroImovelTexto
	 *            the numero imovel texto
	 * @param indicadorCondominio
	 *            the indicador condominio
	 * @param habilitado
	 *            the habilitado
	 * @param imovel
	 *            the imovel
	 * @return the hash map
	 */
	private Map<String, Object> popularFiltro(Long chaveLM, String descricaoImovelTexto, String complementoTexto,
					String matriculaImovelTexto, String numeroImovelTexto, String indicadorCondominio, String habilitado, Imovel imovel,
					Long idAgente, String dataVisita, String periodoVisitaInicial, String periodoVisitaFinal, String visitaAgendada,
					Long idStatus, String bairro, String rua) {

		Map<String, Object> filtro = new HashMap<>();

		if (imovel != null) {

			if (matriculaImovelTexto != null && matriculaImovelTexto.length() > 0) {
				imovel.setChavePrimaria(Long.parseLong(matriculaImovelTexto));
			}

			imovel.setNome(descricaoImovelTexto);
			imovel.setDescricaoComplemento(complementoTexto);
			imovel.setNumeroImovel(numeroImovelTexto);

			if (indicadorCondominio != null && indicadorCondominio.length() > 0) {
				imovel.setCondominio(Boolean.valueOf(indicadorCondominio));
			}

			if (habilitado != null && habilitado.length() > 0) {
				imovel.setHabilitado(Boolean.valueOf(habilitado));
			}

		}

		if (chaveLM != null && chaveLM > 0) {
			filtro.put(CHAVE_PRIMARIA_LM, chaveLM);
		}

		if (descricaoImovelTexto != null && descricaoImovelTexto.length() > 0) {
			filtro.put(DESCRICAO_IMOVEL, descricaoImovelTexto.toUpperCase());
		}

		if (complementoTexto != null && complementoTexto.length() > 0) {
			filtro.put(COMPLEMENTO_IMOVEL, complementoTexto.toUpperCase());
		}

		if (numeroImovelTexto != null && numeroImovelTexto.length() > 0) {
			filtro.put(NUMERO_IMOVEL, numeroImovelTexto.toUpperCase());
		}

		if (matriculaImovelTexto != null && matriculaImovelTexto.length() > 0) {
			filtro.put(MATRICULA_IMOVEL, Long.valueOf(matriculaImovelTexto));
		}

		if (indicadorCondominio != null && indicadorCondominio.length() > 0) {
			filtro.put(INDICADOR_CONDOMINIO_AMBOS, indicadorCondominio);
		}

		if (habilitado != null) {
			filtro.put(HABILITADO, habilitado);
		}

		if (idAgente != null && idAgente > 0) {
			filtro.put(ID_AGENTE, idAgente);
		}

		if (dataVisita != null && dataVisita.length() == TAMANHO_DATA) {
			filtro.put("dataVisita", dataVisita);
		}

		if (periodoVisitaInicial != null && periodoVisitaInicial.length() == TAMANHO_PERIODO_VISITA_INICIAL) {
			filtro.put(PERIODO_VISITA_INICIAL, periodoVisitaInicial);
		} else if (periodoVisitaFinal != null && periodoVisitaFinal.length() == TAMANHO_PERIODO_VISITA_FINAL) {
			filtro.put(PERIODO_VISITA_INICIAL, periodoVisitaFinal);
		}

		if (periodoVisitaFinal != null && periodoVisitaFinal.length() == TAMANHO_PERIODO_VISITA_FINAL) {
			filtro.put(PERIODO_VISITA_FINAL, periodoVisitaFinal);
		} else if (periodoVisitaInicial != null && periodoVisitaInicial.length() == TAMANHO_PERIODO_VISITA_INICIAL) {
			filtro.put(PERIODO_VISITA_FINAL, periodoVisitaInicial);
		}

		if (idStatus != null && idStatus > 0) {
			filtro.put("idStatus", idStatus);
		}

		if (visitaAgendada != null && visitaAgendada.length() > 0) {
			filtro.put(VISITA_AGENDADA, visitaAgendada);
		}

		if (!StringUtils.isEmpty(bairro)) {
			filtro.put("bairro", bairro);
		}

		if (!StringUtils.isEmpty(rua)) {
			filtro.put("rua", rua);
		}

		return filtro;
	}

	/**
	 * Montar vo.
	 * 
	 * @param listaLM the lista lm
	 * @return the array list
	 */
	private List<LevantamentoMercadoVO> montarVO(Collection<LevantamentoMercado> listaLM) {

		List<LevantamentoMercadoVO> listaLMVO = new ArrayList<>();

		for (LevantamentoMercado lm : listaLM) {
			LevantamentoMercadoVO lmVO = new LevantamentoMercadoVO();
			lmVO.setImovel(lm.getImovel());
			lmVO.setChavePrimaria(lm.getChavePrimaria());
			lmVO.setHabilitado(lm.isHabilitado());
			if (lm.getStatus() != null) {
				lmVO.setStatus(lm.getStatus().getDescricao());
			}
			if (lm.getImovel().getContatos() != null && !lm.getImovel().getContatos().isEmpty()) {
				ContatoImovel contato = lm.getImovel().getContatos().iterator().next();
				lmVO.setContato("(" + contato.getCodigoDDD() + ")" + contato.getFone());
			}

			lmVO.setPossuiAgendamento(lm.getAgendamentoVisita());

			listaLMVO.add(lmVO);
		}

		return listaLMVO;
	}

	/**
	 * Popular agenda.
	 * 
	 * @param agenda
	 *            the agenda
	 * @param dataVisita
	 *            the data visita
	 * @param fluxoAgendaAtualizacao
	 *            the fluxo agenda atualizacao
	 * @param chaveAgendamento
	 *            the chave agendamento
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularAgenda(AgendaVisitaAgente agenda, String dataVisita, String fluxoAgendaAtualizacao, Long chaveAgendamento)
					throws GGASException {

		agenda.setHabilitado(true);
		if (dataVisita != null && dataVisita.length() > 0) {
			agenda.setDataVisita(Util.converterCampoStringParaData(DATA_VISITA, dataVisita, Constantes.FORMATO_DATA_BR));

		} else {
			agenda.setDataVisita(null);
		}

		if (fluxoAgendaAtualizacao != null && TRUE.equals(fluxoAgendaAtualizacao)) {

			agenda.setVersao(controladorLevantamentoMercado.obterAgendaVisitaAgente(chaveAgendamento).getVersao());
			agenda.setChavePrimaria(chaveAgendamento);
		}

		agenda.setHoraVisita(agenda.getHoraVisita().trim());

	}

	/**
	 * Popular campos agenda.
	 * 
	 * @param chaveLM
	 *            the chave lm
	 * @param model
	 *            the model
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void popularCamposAgenda(LevantamentoMercado levantamentoMercado, ModelAndView model) throws NegocioException {

		if (levantamentoMercado.getAgendaVisitaAgente() != null) {
			model.addObject(AGENDA, levantamentoMercado.getAgendaVisitaAgente());
			model.addObject(FLUXO_AGENDA_ALTERACAO, Boolean.TRUE);
			model.addObject(DATA_VISITA_AGENTE, Util.converterDataParaString(levantamentoMercado.getAgendaVisitaAgente().getDataVisita()));
			model.addObject(HORA_VISITA, levantamentoMercado.getAgendaVisitaAgente().getHoraVisita());
		} else {
			model.addObject(FLUXO_AGENDA_ALTERACAO, Boolean.FALSE);
		}
	}

	/**
	 * Exibir inclusao proposta levatamento mercado.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirInclusaoPropostaLevatamentoMercado")
	public ModelAndView exibirInclusaoPropostaLevatamentoMercado(@RequestParam(CHAVE_PRIMARIA_LM) Long chavePrimaria) throws GGASException {

		LevantamentoMercado levMercado = controladorLevantamentoMercado.obterLevantamentoMercado(chavePrimaria);

		ModelAndView model = null;
		try {
			if (!levMercado.isHabilitado()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_NAO_E_POSSIVEL_GERAR_PROPOSTA, true);
			}

			if (levMercado.getProposta() != null) {
				model = new ModelAndView("forward:/exibirAlteracaoProposta");
				model.addObject(CHAVE_PRIMARIA_LM, chavePrimaria);
			} else {
				model = new ModelAndView("forward:/exibirInclusaoProposta");
				model.addObject(CHAVE_PRIMARIA_LM, chavePrimaria);
			}

		} catch (NegocioException e) {
			model = pesquisarLevantamentoMercado(null, null, String.valueOf(levMercado.getImovel().getChavePrimaria()), null, null, null,
							null, null, null, null, null, null, null, null);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Remover levantamento mercado.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("removerLevantamentoMercado")
	public ModelAndView removerLevantamentoMercado(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_LEVANTAMENTO_MERCADO);
		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());

			validarRemocaoLevantamentoMercado(chavesPrimarias);
			controladorLevantamentoMercado.removerLevantamentoMercado(chavesPrimarias, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, LEVANTAMENTO_MERCADO);
		} catch (NegocioException e) {

			mensagemErroParametrizado(model, e);
		}
		return model;
	}

	/**
	 * Adicionar tipo combustivel.
	 * 
	 * @param idTipoCombustivel
	 *            the id tipo combustivel
	 * @param consumo
	 *            the consumo
	 * @param preco
	 *            the preco
	 * @param idFornecedor
	 *            the id fornecedor
	 * @param idPeriodicidade
	 *            the id periodicidade
	 * @param idModalidadeUso
	 *            the id modalidade uso
	 * @param idRedeEstado
	 *            the id rede estado
	 * @param localizacaoCentral
	 *            the localizacao central
	 * @param central
	 *            the central
	 * @param localizacaoAbrigo
	 *            the localizacao abrigo
	 * @param idTipoCilindro
	 *            the id tipo cilindro
	 * @param idRedeMaterial
	 *            the id rede material
	 * @param redeTempo
	 *            the rede tempo
	 * @param ventilacao
	 *            the ventilacao
	 * @param observacoes
	 *            the observacoes
	 * @param indexList
	 *            the index list
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("adicionarTipoCombustivel")
	public ModelAndView adicionarTipoCombustivel(@RequestParam("tipoCombustivel") Long idTipoCombustivel,
					@RequestParam("consumo") String consumo, @RequestParam("preco") String preco,
					@RequestParam("fornecedor") Long idFornecedor, @RequestParam("periodicidade") Long idPeriodicidade,
					@RequestParam("modalidadeUso") Long idModalidadeUso, @RequestParam("redeEstado") Long idRedeEstado,
					@RequestParam("localizacaoCentral") String localizacaoCentral, @RequestParam("central") boolean central,
					@RequestParam("localizacaoAbrigo") String localizacaoAbrigo, @RequestParam("tipoCilindro") Long idTipoCilindro,
					@RequestParam("redeMaterial") Long idRedeMaterial, @RequestParam("redeTempo") String redeTempo,
					@RequestParam("ventilacao") boolean ventilacao, @RequestParam("observacoes") String observacoes,
					@RequestParam("indexList") Integer indexList, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridTipoCombustivel");

		try {
			if (existsCombustivel(session, idTipoCombustivel) && indexList == -1) {
				model = new ModelAndView(TELA_AJAX_ERRO);
				mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_TIPO_COMBUSTIVEL_EXISTENTE);
				return model;
			}
			popularTipoCombustivelLM(idTipoCombustivel, consumo, preco, idFornecedor, idPeriodicidade, idModalidadeUso, idRedeEstado,
							central, localizacaoCentral, localizacaoAbrigo, idTipoCilindro, idRedeMaterial, redeTempo, ventilacao,
							observacoes, indexList, session);
			this.popularCampos(model, null, null, false, false, true, session);

		} catch (NegocioException e) {
			model = new ModelAndView(TELA_AJAX_ERRO);
			this.popularCampos(model, null, null, false, false, true, session);
			mensagemErroParametrizado(model, e);
		}
		return model;
	}

	/**
	 * Popular tipo combustivel lm.
	 * 
	 * @param idTipoCombustivel
	 *            the id tipo combustivel
	 * @param consumo
	 *            the consumo
	 * @param preco
	 *            the preco
	 * @param idFornecedor
	 *            the id fornecedor
	 * @param idPeriodicidade
	 *            the id periodicidade
	 * @param idModalidadeUso
	 *            the id modalidade uso
	 * @param idRedeEstado
	 *            the id rede estado
	 * @param central
	 *            the central
	 * @param localizacaoCentral
	 *            the localizacao central
	 * @param localizacaoAbrigo
	 *            the localizacao abrigo
	 * @param idTipoCilindro
	 *            the id tipo cilindro
	 * @param idRedeMaterial
	 *            the id rede material
	 * @param redeTempo
	 *            the rede tempo
	 * @param ventilacao
	 *            the ventilacao
	 * @param observacoes
	 *            the observacoes
	 * @param indexList
	 *            the index list
	 * @param session
	 *            the session
	 * @return the levantamento mercado tipo combustivel
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private LevantamentoMercadoTipoCombustivel popularTipoCombustivelLM(Long idTipoCombustivel, String consumo, String preco,
					Long idFornecedor, Long idPeriodicidade, Long idModalidadeUso, Long idRedeEstado, boolean central,
					String localizacaoCentral, String localizacaoAbrigo, Long idTipoCilindro, Long idRedeMaterial, String redeTempo,
					boolean ventilacao, String observacoes, Integer indexList, HttpSession session) throws GGASException {

		List<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel = (ArrayList<LevantamentoMercadoTipoCombustivel>) session
						.getAttribute(LISTA_TIPO_COMBUSTIVEL_LM);

		LevantamentoMercadoTipoCombustivel levantamentoMercadoTipoCombustivel = null;
		if (indexList != null && indexList > -1) {
			for (int i = 0; i < listaTipoCombustivel.size(); i++) {
				if (i == indexList && listaTipoCombustivel.get(i) != null) {
					levantamentoMercadoTipoCombustivel = listaTipoCombustivel.get(i);
				}
			}
		} else {
			levantamentoMercadoTipoCombustivel = new LevantamentoMercadoTipoCombustivel();
			if (listaTipoCombustivel == null) {
				listaTipoCombustivel = new ArrayList<>();
			}

		}

		if (idTipoCombustivel != null && idTipoCombustivel > 0) {
			EntidadeConteudo tipoCombustivel = fachada.obterEntidadeConteudo(idTipoCombustivel);
			if (tipoCombustivel != null) {
				levantamentoMercadoTipoCombustivel.setTipoCombustivel(tipoCombustivel);
			}
		}

		if (!StringUtils.isEmpty(consumo)) {
			levantamentoMercadoTipoCombustivel.setConsumo(Util.converterCampoStringParaValorBigDecimal(CONSUMO, consumo,
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		} else {
			levantamentoMercadoTipoCombustivel.setConsumo(null);
		}

		if (!StringUtils.isEmpty(preco)) {
			levantamentoMercadoTipoCombustivel.setPreco(Util.converterCampoStringParaValorBigDecimal(PRECO, preco,
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		} else {
			levantamentoMercadoTipoCombustivel.setPreco(null);
		}

		if (idFornecedor != null && idFornecedor > 0) {
			EntidadeConteudo fornecedor = fachada.obterEntidadeConteudo(idFornecedor);
			if (fornecedor != null) {
				levantamentoMercadoTipoCombustivel.setFornecedor(fornecedor);
			}
		}

		if (idPeriodicidade != null && idPeriodicidade > 0) {
			Periodicidade periodicidade = Fachada.getInstancia().obterPeriodicidade(idPeriodicidade);
			if (periodicidade != null) {
				levantamentoMercadoTipoCombustivel.setPeriodicidade(periodicidade);
			}
		}

		if (idModalidadeUso != null && idModalidadeUso > 0) {
			EntidadeConteudo modalidadeUso = fachada.obterEntidadeConteudo(idModalidadeUso);
			if (modalidadeUso != null) {
				levantamentoMercadoTipoCombustivel.setModalidadeUso(modalidadeUso);
			}
		}

		if (idRedeEstado != null && idRedeEstado > 0) {
			EntidadeConteudo redeEstado = fachada.obterEntidadeConteudo(idRedeEstado);
			if (redeEstado != null) {
				levantamentoMercadoTipoCombustivel.setRedeEstado(redeEstado);
			}
		} else {
			levantamentoMercadoTipoCombustivel.setRedeEstado(null);
		}

		levantamentoMercadoTipoCombustivel.setCentral(central);

		if (!StringUtils.isEmpty(localizacaoCentral)) {
			levantamentoMercadoTipoCombustivel.setLocalizacaoCentral(localizacaoCentral);
		} else {
			levantamentoMercadoTipoCombustivel.setLocalizacaoCentral(null);
		}

		if (!StringUtils.isEmpty(localizacaoAbrigo)) {
			levantamentoMercadoTipoCombustivel.setLocalizacaoAbrigo(localizacaoAbrigo);
		} else {
			levantamentoMercadoTipoCombustivel.setLocalizacaoAbrigo(null);
		}

		if (idTipoCilindro != null && idTipoCilindro > 0) {
			TipoBotijao tipoCilindro = fachada.buscarTipoBotijaoPorChave(idTipoCilindro);
			if (tipoCilindro != null) {
				levantamentoMercadoTipoCombustivel.setTipoBotijao(tipoCilindro);
			}
		} else {
			levantamentoMercadoTipoCombustivel.setTipoBotijao(null);
		}

		if (idRedeMaterial != null && idRedeMaterial > 0) {
			RedeMaterial redeMaterial = fachada.buscarRedeMaterialPorChave(idRedeMaterial);
			if (redeMaterial != null) {
				levantamentoMercadoTipoCombustivel.setRedeMaterial(redeMaterial);
			}
		}

		if (!StringUtils.isEmpty(redeTempo)) {
			levantamentoMercadoTipoCombustivel.setRedeTempo(redeTempo);
		} else {
			levantamentoMercadoTipoCombustivel.setRedeTempo(null);
		}

		levantamentoMercadoTipoCombustivel.setVentilacao(ventilacao);

		if (!StringUtils.isEmpty(observacoes)) {
			levantamentoMercadoTipoCombustivel.setObservacoes(observacoes);
		}


		tratarlevantamentoMercadoTipoCombustivel(indexList, session, listaTipoCombustivel, levantamentoMercadoTipoCombustivel);

		return levantamentoMercadoTipoCombustivel;
	}

	private void tratarlevantamentoMercadoTipoCombustivel(Integer indexList, HttpSession session,
			List<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel,
			LevantamentoMercadoTipoCombustivel levantamentoMercadoTipoCombustivel) throws NegocioException {
		List<EntidadeConteudo> listaTipoAparelho = (ArrayList<EntidadeConteudo>) session.getAttribute(LISTA_TIPO_APARELHO);

		if (!CollectionUtils.isEmpty(listaTipoAparelho)) {
			Collection<LevantamentoMercadoTipoCombustivelAparelho> listaTiposAparelhoSet = new HashSet<>();
			for (EntidadeConteudo aparelho : listaTipoAparelho) {
				LevantamentoMercadoTipoCombustivelAparelho lmtca = new LevantamentoMercadoTipoCombustivelAparelho();
				lmtca.setTipoAparelho(aparelho);
				listaTiposAparelhoSet.add(lmtca);
			}
			levantamentoMercadoTipoCombustivel.setListaAparelhos(listaTiposAparelhoSet);
		} else {
			levantamentoMercadoTipoCombustivel.setListaAparelhos(null);
		}
		validarEntidade(levantamentoMercadoTipoCombustivel);

		session.removeAttribute(LISTA_TIPO_APARELHO);

		if (indexList != null && indexList == -1) {
			listaTipoCombustivel.add(levantamentoMercadoTipoCombustivel);
		}

		session.setAttribute(LISTA_TIPO_COMBUSTIVEL_LM, listaTipoCombustivel);
	}

	/**
	 * Limpar sessao.
	 * 
	 * @param session
	 *            the session
	 */
	private void limparSessao(HttpSession session) {

		session.removeAttribute(LISTA_TIPO_COMBUSTIVEL_LM);
		session.removeAttribute(LISTA_TIPO_APARELHO);
	}

	/**
	 * Adicionar tipo aparelho.
	 * 
	 * @param idTipoAparelho
	 *            the id tipo aparelho
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("adicionarTipoAparelho")
	public ModelAndView adicionarTipoAparelho(@RequestParam("idAparelho") Long idTipoAparelho, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_TIPO_APARELHO);

		List<EntidadeConteudo> aparelhos = (ArrayList<EntidadeConteudo>) session.getAttribute(LISTA_TIPO_APARELHO);
		if (aparelhos == null) {
			aparelhos = new ArrayList<>();
		}

		if (aparelhoExists(aparelhos, idTipoAparelho)) {
			model = new ModelAndView(TELA_AJAX_ERRO);
			mensagemAdvertencia(model, Constantes.ERRO_NEGOCIO_APARELHO_EXISTENTE);

		} else {
			aparelhos.add(fachada.obterEntidadeConteudo(idTipoAparelho));
		}

		model.addObject(LISTA_APARELHO, fachada.obterListaAparelhosMaiorIndiceConsumo());

		session.setAttribute(LISTA_TIPO_APARELHO, aparelhos);

		return model;
	}

	/**
	 * Remover aparelho.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("removerAparelho")
	public ModelAndView removerAparelho(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_TIPO_APARELHO);

		List<EntidadeConteudo> aparelhos = (ArrayList<EntidadeConteudo>) session.getAttribute(LISTA_TIPO_APARELHO);
		if (aparelhos != null) {
			for (EntidadeConteudo aparelho : aparelhos) {
				if (aparelho.getChavePrimaria() == chavePrimaria) {
					aparelhos.remove(aparelho);
					break;
				}
			}
		}

		model.addObject(LISTA_APARELHO, fachada.obterListaAparelhosMaiorIndiceConsumo());

		session.setAttribute(LISTA_TIPO_APARELHO, aparelhos);

		return model;
	}

	/**
	 * Aparelho exists.
	 * 
	 * @param aparelhos
	 *            the aparelhos
	 * @param idAparelho
	 *            the id aparelho
	 * @return true, if successful
	 */
	private boolean aparelhoExists(List<EntidadeConteudo> aparelhos, Long idAparelho) {

		boolean exists = false;

		if (!CollectionUtils.isEmpty(aparelhos) && idAparelho != null) {
			for (EntidadeConteudo aparelho : aparelhos) {
				if (aparelho.getChavePrimaria() == idAparelho) {
					exists = true;
				}
			}
		}
		return exists;
	}

	
	/**
	 * Remover tipo combustível.
	 * 
	 * @param indexLista - {@link Integer}
	 * @param session - {@link HttpSession}
	 * @return model - {@link ModelAndView}
	 * @throws GGASException - {@link GGASException}
	 */
	@RequestMapping("removerTipoCombustivel")
	public ModelAndView removerTipoCombustivel(@RequestParam("indexLista") Integer indexLista, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridTipoCombustivel");

		@SuppressWarnings("unchecked")
		List<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel =
				(ArrayList<LevantamentoMercadoTipoCombustivel>) session.getAttribute(LISTA_TIPO_COMBUSTIVEL_LM);

		this.removerTP(listaTipoCombustivel, indexLista);

		this.popularCampos(model, null, null, false, false, true, session);

		session.setAttribute(LISTA_TIPO_COMBUSTIVEL_LM, listaTipoCombustivel);

		return model;
	}

	/**
	 * Remover tp.
	 * 
	 * @param listaTipoCombustivel
	 *            the lista tipo combustivel
	 * @param idTipoCombustivel
	 *            the id tipo combustivel
	 * @return true, if successful
	 */
	private void removerTP(List<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel, Integer indexLista) {

		if (indexLista + 1 <= listaTipoCombustivel.size()) {
			for (int i = 0; i < listaTipoCombustivel.size(); i++) {
				if (i == indexLista) {
					listaTipoCombustivel.remove(listaTipoCombustivel.get(indexLista));
				}
			}
		}
	}

	/**
	 * Atualizar grid tipo aparelho.
	 * 
	 * @param idTipoCombustivel
	 *            the id tipo combustivel
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("atualizarGridTipoAparelho")
	public ModelAndView atualizarGridTipoAparelho(@RequestParam("idTipoCombustivel") Long idTipoCombustivel, HttpSession session)
					throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_TIPO_APARELHO);

		List<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel = (ArrayList<LevantamentoMercadoTipoCombustivel>) session
						.getAttribute(LISTA_TIPO_COMBUSTIVEL_LM);

		for (LevantamentoMercadoTipoCombustivel leveCombustivel : listaTipoCombustivel) {
			if (leveCombustivel.getTipoCombustivel().getChavePrimaria() == idTipoCombustivel) {
				Collection<EntidadeConteudo> aparelhos = new ArrayList<>();
				for (LevantamentoMercadoTipoCombustivelAparelho leveAparelho : leveCombustivel.getListaAparelhos()) {
					aparelhos.add(leveAparelho.getTipoAparelho());
				}
				session.setAttribute(LISTA_TIPO_APARELHO, aparelhos);
				break;
			}
		}

		model.addObject(LISTA_APARELHO, fachada.obterListaAparelhosMaiorIndiceConsumo());

		return model;
	}

	/**
	 * Exists combustivel.
	 * 
	 * @param session
	 *            the session
	 * @param idTipoCombustivel
	 *            the id tipo combustivel
	 * @return true, if successful
	 */
	private boolean existsCombustivel(HttpSession session, Long idTipoCombustivel) {

		List<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel = (ArrayList<LevantamentoMercadoTipoCombustivel>) session
						.getAttribute(LISTA_TIPO_COMBUSTIVEL_LM);
		boolean exists = false;
		if (!CollectionUtils.isEmpty(listaTipoCombustivel)) {
			for (LevantamentoMercadoTipoCombustivel levCombustivel : listaTipoCombustivel) {
				if (idTipoCombustivel != CHAVE_CAMPO_NAO_APLICAVEL && levCombustivel.getTipoCombustivel().getChavePrimaria() == idTipoCombustivel) {
					exists = true;
				}

			}
		}

		return exists;
	}

	/**
	 * Obter contato principal.
	 * 
	 * @param contatos
	 *            the contatos
	 * @return the contato imovel
	 */
	private ContatoImovel obterContatoPrincipal(Collection<ContatoImovel> contatos) {

		ContatoImovel contatoPrincipal = null;
		for (ContatoImovel contato : contatos) {
			if (contato.isPrincipal()) {
				contatoPrincipal = contato;
			}
		}
		return contatoPrincipal;
	}

	/**
	 * Converter lista hash set.
	 * 
	 * @param lista
	 *            the lista
	 * @return the collection
	 */
	private Collection<LevantamentoMercadoTipoCombustivel> converterListaHashSet(Collection<LevantamentoMercadoTipoCombustivel> lista) {

		Collection<LevantamentoMercadoTipoCombustivel> listaNova = new ArrayList<>();
		for (LevantamentoMercadoTipoCombustivel levantamento : lista) {
			listaNova.add(levantamento);
		}
		return listaNova;
	}

	/**
	 * Detalhar tipo combustível.
	 * 
	 * @param chavePrimaria a chave primária
	 * @param indexLista o índice da lista
	 * @param session a sessão do sistema
	 * @return {@link ModelAndView}
	 * @throws GGASException a exceção GGAS exception
	 */
	@RequestMapping("detalharTipoCombustivel")
	public ModelAndView detalharTipoCombustivel(@RequestParam("chavePrimariaLM") Long chavePrimaria,
			@RequestParam("indexLista") Integer indexLista, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView("gridDetalhamentoTipoCombustivel");

		LevantamentoMercado levaMercado = controladorLevantamentoMercado.obterLevantamentoMercado(chavePrimaria);
		popularDetalhamentoTipoCombustivel(indexLista, null, model, session);

		model.addObject(LEVANTAMENTO_MERCADO2, levaMercado);
		return model;
	}

	private void popularDetalhamentoTipoCombustivel(Integer indexLista, LevantamentoMercado levaMercado, ModelAndView model,
					HttpSession session) throws GGASException {

		List<LevantamentoMercadoTipoCombustivel> listaTipoCombustivel = (ArrayList<LevantamentoMercadoTipoCombustivel>) session
						.getAttribute(LISTA_TIPO_COMBUSTIVEL_LM);
		LevantamentoMercadoTipoCombustivel leveCombustivel = null;

		if (indexLista == null && !CollectionUtils.isEmpty(levaMercado.getListaTipoCombustivel())) {
			leveCombustivel = levaMercado.getListaTipoCombustivel().iterator().next();
		} else if (listaTipoCombustivel != null && indexLista + 1 <= listaTipoCombustivel.size()) {
			leveCombustivel = listaTipoCombustivel.get(indexLista);
		}

		model.addObject("tipoCombustivelLevantamentoMercado", leveCombustivel);

		if (leveCombustivel != null && leveCombustivel.getConsumo() != null) {
			model.addObject("consumo", Util.converterCampoValorDecimalParaStringComCasasDecimais(VALOR, leveCombustivel.getConsumo(),
							Constantes.LOCALE_PADRAO, NUMERO_CASAS_DECIMAIS));
		} else {
			model.addObject("consumo", Util.converterCampoValorDecimalParaStringComCasasDecimais(VALOR, BigDecimal.ZERO,
							Constantes.LOCALE_PADRAO, NUMERO_CASAS_DECIMAIS));
		}

		if (leveCombustivel != null && leveCombustivel.getPreco() != null) {
			model.addObject("preco", Util.converterCampoValorDecimalParaStringComCasasDecimais(VALOR, leveCombustivel.getPreco(),
							Constantes.LOCALE_PADRAO, NUMERO_CASAS_DECIMAIS));
		} else {
			model.addObject("preco", Util.converterCampoValorDecimalParaStringComCasasDecimais(VALOR, BigDecimal.ZERO,
							Constantes.LOCALE_PADRAO, NUMERO_CASAS_DECIMAIS));
		}

		if (leveCombustivel != null && leveCombustivel.isCentral()) {
			model.addObject("central", "Sim");
		} else {
			model.addObject("central", "Não");
		}

		if (leveCombustivel != null && leveCombustivel.isVentilacao()) {
			model.addObject("ventilacao", "Sim");
		} else {
			model.addObject("ventilacao", "Não");
		}

		if (leveCombustivel != null && !CollectionUtils.isEmpty(leveCombustivel.getListaAparelhos())) {
			Collection<LevantamentoMercadoTipoCombustivelAparelho> lista = new ArrayList<>();
			for (LevantamentoMercadoTipoCombustivelAparelho aparelho : leveCombustivel.getListaAparelhos()) {
				lista.add(aparelho);
			}
			model.addObject(LISTA_TIPO_APARELHO, lista);
		}
	}

	/**
	 * Validar horario.
	 * 
	 * @param horarioTmp
	 *            the horario
	 * @param dataAgendada
	 *            the data agendada
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarHorario(String horarioTmp, Date dataAgendada) throws NegocioException {

		String horario = "";
		if (horarioTmp != null) {
			horario = horarioTmp.replace("_", "");
		}
		if (StringUtils.isNotBlank(horario) && horario.length() == TAMANHO_HORARIO) {

			String horaSemSeparacao = horario.replace(":", "");

			Calendar data = Calendar.getInstance();

			int diferenca = Util.compararDatas(dataAgendada, data.getTime());

			if (diferenca == -1) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_DATA_AGENDAMENTO_INVALIDA, true);
			} else if (diferenca == 0) {
				int horaAtual = data.get(Calendar.HOUR_OF_DAY);
				int minutosAtual = data.get(Calendar.MINUTE);

				String horaStringAtual = String.valueOf(horaAtual);
				String minutoStringAtual = String.valueOf(minutosAtual);

				String horaAtualString = horaStringAtual + minutoStringAtual;

				if (Integer.parseInt(horaSemSeparacao) < Integer.parseInt(horaAtualString)) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_HORARIO_ANTERIOR_ATUAL, true);
				}
			}

		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_HORARIO_ANTERIOR_ATUAL, true);
		}
	}

	/**
	 * Validar remocao levantamento mercado.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarRemocaoLevantamentoMercado(Long[] chavesPrimarias) throws NegocioException {

		Collection<LevantamentoMercado> listaLevantamentoMercado = controladorLevantamentoMercado
						.listarLevantamentoMercadoPorChaves(chavesPrimarias);
		boolean existeProposta = false;
		for (LevantamentoMercado levantamentoMercado : listaLevantamentoMercado) {
			if (levantamentoMercado.getProposta() != null) {
				existeProposta = true;
				break;
			}
		}

		if (existeProposta) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_POSSUI_PROPOSTA, true);
		}
	}

	/**
	 * Gerar evte.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("gerarEVTE")
	public ModelAndView gerarEVTE(@RequestParam("chavePrimariaLM") Long chavePrimaria, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_LEVANTAMENTO_MERCADO);

		try {
			LevantamentoMercado levaMercado = controladorLevantamentoMercado.obterLevantamentoMercado(chavePrimaria);
			verificarProposta(levaMercado);
			atualizarStatusLevantamentoMercado(levaMercado);

		} catch (NegocioException e) {

			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Verificar proposta.
	 * 
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarProposta(LevantamentoMercado levantamentoMercado) throws NegocioException {

		if (levantamentoMercado.getProposta() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_NAO_POSSUI_PROPOSTA, true);

		} else if (levantamentoMercado.getProposta() != null) {
			if (levantamentoMercado.getProposta().getSituacaoProposta() != null) {
				if (!levantamentoMercado.getProposta().getSituacaoProposta().getDescricao().equalsIgnoreCase(SITUACAO_PROPOSTA_APROVADA)) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_PROPOSTA_NAO_APROVADA, true);
				}
			} else {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_PROPOSTA_NAO_APROVADA, true);
			}
		}
	}

	/**
	 * Atualizar status levantamento mercado.
	 * 
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void atualizarStatusLevantamentoMercado(LevantamentoMercado levantamentoMercado) throws GGASException {

		EntidadeConteudo status = fachada.obterEntidadeConteudo(Long.valueOf(fachada.obterConstantePorCodigo(
						Constantes.C_LEVANTAMENTO_MERCADO_EVTE_EM_ELABORACAO).getValor()));
		levantamentoMercado.setStatus(status);

		controladorLevantamentoMercado.atualizarLevantamentoMercado(levantamentoMercado);
	}

	/**
	 * Validar alteracao levantamento mercado.
	 * 
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarAlteracaoLevantamentoMercado(LevantamentoMercado levantamentoMercado) throws NegocioException {

		if (levantamentoMercado.getStatus() != null) {
			if ("Proposta Enviada".equalsIgnoreCase(levantamentoMercado.getStatus().getDescricao())) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_NAO_PODE_SER_EDITADO, true);
			} else if ("Encerrado".equalsIgnoreCase(levantamentoMercado.getStatus().getDescricao())) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_NAO_PODE_SER_EDITADO, true);
			} else if ("EVTE Em Elaboração".equalsIgnoreCase(levantamentoMercado.getStatus().getDescricao())) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_NAO_PODE_SER_EDITADO, true);
			}
		}

	}

	/**
	 * Validar status para agendamemto.
	 * 
	 * @param levantamentoMercado
	 *            the levantamento mercado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarStatusParaAgendamemto(LevantamentoMercado levantamentoMercado) throws NegocioException {

		if (levantamentoMercado.getStatus() != null && "Encerrado".equalsIgnoreCase(levantamentoMercado.getStatus().getDescricao())) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_NAO_PODE_SER_EDITADO, true);
		}

		if (!levantamentoMercado.isHabilitado()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_LEVANTAMENTO_MERCADO_INATIVO, true);
		}
	}

	/**
	 * Limpar Grid Aparelhos.
	 * 
	 * @param idTipoAparelho {@link Long}
	 * @param session {@link HttpSession}
	 * @return ModelAndView {@link ModelAndView}
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("limparGridAparelhos")
	public ModelAndView limparGridAparelhos(@RequestParam("idAparelho") Long idTipoAparelho, HttpSession session) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_GRID_TIPO_APARELHO);

		List<EntidadeConteudo> aparelhos = (ArrayList<EntidadeConteudo>) session.getAttribute(LISTA_TIPO_APARELHO);
		if (!CollectionUtils.isEmpty(aparelhos)) {
			aparelhos = new ArrayList<>();
		}

		model.addObject(LISTA_APARELHO, fachada.obterListaAparelhosMaiorIndiceConsumo());

		session.setAttribute(LISTA_TIPO_APARELHO, aparelhos);

		return model;
	}

	private void verificaCamposNaoSeAplica(LevantamentoMercado levantamentoMercado) {

		if (!CollectionUtils.isEmpty(levantamentoMercado.getListaTipoCombustivel())) {
			for (LevantamentoMercadoTipoCombustivel levaCombustivel : levantamentoMercado.getListaTipoCombustivel()) {
				if (levaCombustivel.getTipoCombustivel() == null) {
					EntidadeConteudo tipoCombustivel = new EntidadeConteudoImpl();
					tipoCombustivel.setChavePrimaria(CHAVE_CAMPO_NAO_APLICAVEL);
					levaCombustivel.setTipoCombustivel(tipoCombustivel);
				}

				if (levaCombustivel.getFornecedor() == null) {
					EntidadeConteudo fornecedor = new EntidadeConteudoImpl();
					fornecedor.setChavePrimaria(CHAVE_CAMPO_NAO_APLICAVEL);
					levaCombustivel.setFornecedor(fornecedor);
				}

				if (levaCombustivel.getPeriodicidade() == null) {
					Periodicidade periodicidade = new PeriodicidadeImpl();
					periodicidade.setChavePrimaria(CHAVE_CAMPO_NAO_APLICAVEL);
					levaCombustivel.setPeriodicidade(periodicidade);
				}

				if (levaCombustivel.getModalidadeUso() == null) {
					EntidadeConteudo modalidadeUso = new EntidadeConteudoImpl();
					modalidadeUso.setChavePrimaria(CHAVE_CAMPO_NAO_APLICAVEL);
					levaCombustivel.setModalidadeUso(modalidadeUso);
				}

				if (levaCombustivel.getRedeMaterial() == null) {
					RedeMaterial redeMaterial = new RedeMaterialImpl();
					redeMaterial.setChavePrimaria(CHAVE_CAMPO_NAO_APLICAVEL);
					levaCombustivel.setRedeMaterial(redeMaterial);
				}
			}
		}
	}

	private ModelAndView popularModelPesquisa(LevantamentoMercado levantamentoMercado) throws GGASException {

		return pesquisarLevantamentoMercado(levantamentoMercado.getImovel().getNome(), null, String.valueOf(levantamentoMercado.getImovel()
						.getChavePrimaria()), levantamentoMercado.getImovel().getNumeroImovel(), String.valueOf(levantamentoMercado
						.getImovel().getCondominio()), String.valueOf(levantamentoMercado.isHabilitado()), null, null, null, null, null,
						null, null, null);
	}

	private void popularFormPesquisa(ModelAndView model, String descricaoImovelTexto, String complementoTexto, String matriculaImovelTexto,
					String numeroImovelTexto, String indicadorCondominio, Long idAgente, String dataVisita,
					String periodoVisitaInicial, String periodoVisitaFinal, String visitaAgendada, Long idStatus, String bairro, String rua) {

		model.addObject(DESCRICAO_IMOVEL_TEXTO, descricaoImovelTexto);
		model.addObject(COMPLEMENTO_TEXTO, complementoTexto);
		model.addObject(MATRICULA_IMOVEL_TEXTO, matriculaImovelTexto);
		model.addObject(NUMERO_IMOVEL_TEXTO, numeroImovelTexto);
		model.addObject(INDICADOR_CONDOMINIO, indicadorCondominio);
		model.addObject("dataVisita", dataVisita);
		model.addObject(ID_AGENTE, idAgente);
		if (periodoVisitaInicial != null && periodoVisitaInicial.length() == TAMANHO_PERIODO_VISITA_INICIAL) {
			model.addObject(PERIODO_VISITA_INICIAL, periodoVisitaInicial);
		} else if (periodoVisitaFinal != null && periodoVisitaFinal.length() == TAMANHO_PERIODO_VISITA_FINAL) {
			model.addObject(PERIODO_VISITA_INICIAL, periodoVisitaFinal);

		}

		if (periodoVisitaFinal != null && periodoVisitaFinal.length() == TAMANHO_PERIODO_VISITA_FINAL) {
			model.addObject(PERIODO_VISITA_FINAL, periodoVisitaFinal);
		} else if (periodoVisitaInicial != null && periodoVisitaInicial.length() == TAMANHO_PERIODO_VISITA_INICIAL) {
			model.addObject(PERIODO_VISITA_FINAL, periodoVisitaInicial);
		}
		if (!TODOS.equalsIgnoreCase(visitaAgendada)) {
			model.addObject(VISITA_AGENDADA, Boolean.valueOf(visitaAgendada));
		} else {
			model.addObject(VISITA_AGENDADA, visitaAgendada);
		}
		model.addObject("idStatus", idStatus);

		model.addObject("bairro", bairro);

		model.addObject("rua", rua);
	}
}
