
package br.com.ggas.cadastro.levantamentomercado.apresentacao;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.levantamentomercado.agente.dominio.Agente;
import br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;
/**
 * 
 * Classe responsável pelas telas
 * relacionadas a Agente
 *
 */
@Controller
public class AgenteAction extends GenericAction {

	private static final String AGENTE_UPPERCASE_A = "Agente";

	private static final String TELA_FORWARD_PESQUISAR_AGENTE = "forward:/pesquisarAgente";

	private static final String AGENTE = "agente";

	private static final Logger LOG = Logger.getLogger(AgenteAction.class);

	@Autowired
	private ControladorAgente controladorAgente;

	public ControladorAgente getControladorAgente() {

		return controladorAgente;
	}

	/**
	 * Exibir pesquisa agente.
	 * Retorna a tela de pesquisar agentes
	 * @return the model and
	 * view
	 */
	@RequestMapping("exibirPesquisaAgente")
	public ModelAndView exibirPesquisaAgente() {

		ModelAndView model = new ModelAndView("exibirPesquisaAgente");
		model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, "true");

		return model;
	}

	/**
	 * Exibir inclusao agente.
	 *
	 * 
	 * @param request
	 *            the request
	 * @return the model
	 * and view
	 */
	@RequestMapping("exibirInclusaoAgente")
	public ModelAndView exibirInclusaoAgente(HttpServletRequest request) {

		return new ModelAndView("exibirInclusaoAgente"); 
	}

	/**
	 * Exibir alteracao agente.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirAlteracaoAgente")
	public ModelAndView exibirAlteracaoAgente(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao)
					throws NegocioException {

		ModelAndView model = new ModelAndView("exibirAlteracaoAgente");

		Agente agente = controladorAgente.obterAgente(chavePrimaria);
		model.addObject(AGENTE, agente);

		return model;
	}

	/**
	 * Exibir detalhamento agente.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirDetalhamentoAgente")
	public ModelAndView exibirDetalhamentoAgente(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao)
					throws NegocioException {

		ModelAndView model = new ModelAndView("exibirDetalhamentoAgente");

		model.addObject(AGENTE, controladorAgente.obterAgente(chavePrimaria));

		return model;
	}

	/**
	 * Incluir agente.
	 * 
	 * @param idFuncionario
	 *            the id funcionario
	 * @param agente
	 *            the agente
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("incluirAgente")
	public ModelAndView incluirAgente(@RequestParam("idFuncionario") Long idFuncionario, Agente agente, HttpServletRequest request)
					throws GGASException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_AGENTE);
		agente.setHabilitado(true);
		if (idFuncionario != null && idFuncionario > 0) {
			agente.setFuncionario(Fachada.getInstancia().buscarFuncionarioPorChave(idFuncionario));
		} else {
			agente.setFuncionario(null);
		}

		try {
			validarEntidade(agente);
			validarFuncionario(agente, idFuncionario, false);
			controladorAgente.inserir(agente);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, AGENTE_UPPERCASE_A);
			model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, true);
		} catch (GGASException e) {
			model = new ModelAndView("forward:/exibirInclusaoAgente");
			request.setAttribute(AGENTE, agente);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Pesquisar agente.
	 * 
	 * @param idFuncionario
	 *            the id funcionario
	 * @param habilitado
	 *            the habilitado
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarAgente")
	public ModelAndView pesquisarAgente(@RequestParam("idFuncionario") Long idFuncionario, @RequestParam("habilitado") String habilitado)
					throws GGASException {

		Collection<Agente> listaAgente = controladorAgente.obterAgentePorChaveFuncionario(idFuncionario, habilitado);
		ModelAndView model = new ModelAndView("exibirPesquisaAgente");
		model.addObject("listaAgente", listaAgente);
		model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.valueOf(habilitado));
		return model;
	}

	/**
	 * Alterar agente.
	 * 
	 * @param idFuncionario
	 *            the id funcionario
	 * @param agente
	 *            the agente
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("alterarAgente")
	public ModelAndView alterarAgente(@RequestParam("idFuncionario") Long idFuncionario, @ModelAttribute(AGENTE) Agente agente,
					HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_AGENTE);
		Agente agenteAtualizado = popularObjeto(agente, idFuncionario);

		try {
			validarEntidade(agenteAtualizado);
			validarFuncionario(agente, idFuncionario, true);
			controladorAgente.atualizarAgente(agenteAtualizado);
			model.addObject(AGENTE, agente);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, AGENTE_UPPERCASE_A);
		} catch (GGASException e) {
			model = new ModelAndView("forward:/exibirAlteracaoAgente");
			request.setAttribute("agenteAtualizado", agenteAtualizado);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Popular objeto.
	 * 
	 * @param agente
	 *            the agente
	 * @param chaveFuncionario
	 *            the chave funcionario
	 * @return the agente
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Agente popularObjeto(Agente agente, Long chaveFuncionario) throws GGASException {

		Agente agenteAtualizado = controladorAgente.obterAgente(agente.getChavePrimaria());

		if (agenteAtualizado != null) {
			agente.setVersao(agenteAtualizado.getVersao());
		}

		if (chaveFuncionario != null && chaveFuncionario > 0) {
			agente.setFuncionario(Fachada.getInstancia().buscarFuncionarioPorChave(chaveFuncionario));
		} else {
			agente.setFuncionario(null);
		}
		return agente;
	}

	/**
	 * Remover agente.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("removerAgente")
	public ModelAndView removerAgente(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_AGENTE);

		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());

			controladorAgente.removerAgente(chavesPrimarias, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, AGENTE_UPPERCASE_A);
		} catch (DataIntegrityViolationException ex) {
			LOG.error(ex.getMessage(), ex);
			try {
				throw new NegocioException(Constantes.ERRO_AGENTE_ESTA_SENDO_USANDO, true);
			} catch (NegocioException e) {
				mensagemErroParametrizado(model, e);
			}

		}

		return model;
	}

	/**
	 * Validar funcionario.
	 * 
	 * @param agente
	 *            the agente
	 * @param idFuncionario
	 *            the id funcionario
	 * @param isAlteracao
	 *            the is alteracao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarFuncionario(Agente agente, Long idFuncionario, boolean isAlteracao) throws NegocioException {

		Collection<Agente> listaAgente = controladorAgente.obterAgentePorChaveFuncionario(idFuncionario, "todos");

		if (!isAlteracao) {
			if (listaAgente != null && !listaAgente.isEmpty()) {
				throw new NegocioException(Constantes.ERRO_FUNCIONARIO_VINCULADO_OUTRO_AGENTE, true);
			}
		} else {
			if (listaAgente != null && !listaAgente.isEmpty()) {
				Agente agenteEmUso = listaAgente.iterator().next();
				if (agente.getChavePrimaria() != agenteEmUso.getChavePrimaria()) {
					throw new NegocioException(Constantes.ERRO_FUNCIONARIO_VINCULADO_OUTRO_AGENTE, true);
				}
			}
		}
	}
}
