
package br.com.ggas.cadastro.levantamentomercado.dominio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelos métodos relacionados
 * a agenda de visita do agente
 *
 */
public class AgendaVisitaAgente extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;

	private static final int LIMITE_HORA_AGENDA = 5;

	private Date dataVisita;

	private String horaVisita;

	private String observacoes;

	public Date getDataVisita() {
		Date data = null;
		if(this.dataVisita != null) {
			data = (Date) dataVisita.clone();
		}
		return data;
	}

	public void setDataVisita(Date dataVisita) {
		if(dataVisita != null){
			this.dataVisita = (Date) dataVisita.clone();
		} else {
			this.dataVisita = null;
		}
	}

	public String getHoraVisita() {

		return horaVisita;
	}

	public void setHoraVisita(String horaVisita) {

		this.horaVisita = horaVisita;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(dataVisita == null) {
			stringBuilder.append(Constantes.ERRO_AGENDA_LM_DATA_VISITA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(horaVisita == null || horaVisita.length() < LIMITE_HORA_AGENDA) {
			stringBuilder.append(Constantes.ERRO_AGENDA_LM_HORA_VISITA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if( horaVisita != null && horaVisita.length() >= LIMITE_HORA_AGENDA){
			SimpleDateFormat formato;
			try {
				formato = new SimpleDateFormat(Constantes.FORMATO_HORA);
				formato.setLenient(false);
				formato.parse(horaVisita);
			} catch(ParseException e) {
				stringBuilder.append(Constantes.ERRO_AGENDA_LM_HORA_VISITA_INVALIDA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

	/**
	 * @return the observacoes
	 */
	public String getObservacoes() {

		return observacoes;
	}

	/**
	 * @param observacoes the observacoes to set
	 */
	public void setObservacoes(String observacoes) {

		this.observacoes = observacoes;
	}
}
