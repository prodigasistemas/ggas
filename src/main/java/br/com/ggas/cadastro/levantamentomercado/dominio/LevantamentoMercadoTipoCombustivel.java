
package br.com.ggas.cadastro.levantamentomercado.dominio;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

import br.com.ggas.cadastro.imovel.TipoBotijao;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.util.Constantes;

/**
 * 
 * Classe responsável pelos métodos de LevantamentoMercadoTipoCombustivel
 *
 */
public class LevantamentoMercadoTipoCombustivel extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private LevantamentoMercado levantamentoMercado;

	private EntidadeConteudo redeEstado;

	private EntidadeConteudo fornecedor;

	private EntidadeConteudo modalidadeUso;

	private Periodicidade periodicidade;

	private TipoBotijao tipoBotijao;

	private RedeMaterial redeMaterial;

	private BigDecimal preco;

	private BigDecimal consumo;

	private String redeTempo;

	private boolean central;

	private String localizacaoCentral;

	private String localizacaoAbrigo;

	private boolean ventilacao;

	private String observacoes;

	private EntidadeConteudo tipoCombustivel;

	private Collection<LevantamentoMercadoTipoCombustivelAparelho> listaAparelhos = new HashSet<>();

	public LevantamentoMercado getLevantamentoMercado() {

		return levantamentoMercado;
	}

	public void setLevantamentoMercado(LevantamentoMercado levantamentoMercado) {

		this.levantamentoMercado = levantamentoMercado;
	}

	public EntidadeConteudo getRedeEstado() {

		return redeEstado;
	}

	public void setRedeEstado(EntidadeConteudo redeEstado) {

		this.redeEstado = redeEstado;
	}

	public EntidadeConteudo getModalidadeUso() {

		return modalidadeUso;
	}

	public void setModalidadeUso(EntidadeConteudo modalidadeUso) {

		this.modalidadeUso = modalidadeUso;
	}

	public Periodicidade getPeriodicidade() {

		return periodicidade;
	}

	public void setPeriodicidade(Periodicidade periodicidade) {

		this.periodicidade = periodicidade;
	}

	public RedeMaterial getRedeMaterial() {

		return redeMaterial;
	}

	public void setRedeMaterial(RedeMaterial redeMaterial) {

		this.redeMaterial = redeMaterial;
	}

	public BigDecimal getPreco() {

		return preco;
	}

	public void setPreco(BigDecimal preco) {

		this.preco = preco;
	}

	public BigDecimal getConsumo() {

		return consumo;
	}

	public void setConsumo(BigDecimal consumo) {

		this.consumo = consumo;
	}

	public EntidadeConteudo getFornecedor() {

		return fornecedor;
	}

	public void setFornecedor(EntidadeConteudo fornecedor) {

		this.fornecedor = fornecedor;
	}

	public String getRedeTempo() {

		return redeTempo;
	}

	public void setRedeTempo(String redeTempo) {

		this.redeTempo = redeTempo;
	}

	public boolean isCentral() {

		return central;
	}

	public void setCentral(boolean central) {

		this.central = central;
	}

	public String getLocalizacaoCentral() {

		return localizacaoCentral;
	}

	public void setLocalizacaoCentral(String localizacaoCentral) {

		this.localizacaoCentral = localizacaoCentral;
	}

	public String getLocalizacaoAbrigo() {

		return localizacaoAbrigo;
	}

	public void setLocalizacaoAbrigo(String localizacaoAbrigo) {

		this.localizacaoAbrigo = localizacaoAbrigo;
	}

	public boolean isVentilacao() {

		return ventilacao;
	}

	public void setVentilacao(boolean ventilacao) {

		this.ventilacao = ventilacao;
	}

	public String getObservacoes() {

		return observacoes;
	}

	public void setObservacoes(String observacoes) {

		this.observacoes = observacoes;
	}

	public EntidadeConteudo getTipoCombustivel() {

		return tipoCombustivel;
	}

	public void setTipoCombustivel(EntidadeConteudo tipoCombustivel) {

		this.tipoCombustivel = tipoCombustivel;
	}

	public TipoBotijao getTipoBotijao() {

		return tipoBotijao;
	}

	public void setTipoBotijao(TipoBotijao tipoBotijao) {

		this.tipoBotijao = tipoBotijao;
	}

	public Collection<LevantamentoMercadoTipoCombustivelAparelho> getListaAparelhos() {

		return listaAparelhos;
	}

	public void setListaAparelhos(Collection<LevantamentoMercadoTipoCombustivelAparelho> listaAparelhos) {

		this.listaAparelhos = listaAparelhos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(tipoCombustivel == null || tipoCombustivel.getChavePrimaria() == -1) {
			stringBuilder.append(Constantes.TIPO_COMBUSTIVEL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			if(consumo == null || consumo.compareTo(new BigDecimal(0)) == 0){
				stringBuilder.append(Constantes.CONSUMO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(preco == null || preco.compareTo(new BigDecimal(0)) == 0) {
				stringBuilder.append(Constantes.PRECO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		if(tipoCombustivel != null && tipoCombustivel.getChavePrimaria() > 0) {
			if(consumo == null || consumo.compareTo(new BigDecimal(0)) == 0){
				stringBuilder.append(Constantes.CONSUMO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(preco == null || preco.compareTo(new BigDecimal(0)) == 0) {
				stringBuilder.append(Constantes.PRECO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		if(fornecedor == null || fornecedor.getChavePrimaria() == -1) {
			stringBuilder.append(Constantes.FORNECEDOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(periodicidade == null || periodicidade.getChavePrimaria() == -1) {
			stringBuilder.append(Constantes.PERIODICIDADE_CONTRATO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(modalidadeUso == null || modalidadeUso.getChavePrimaria() == -1) {
			stringBuilder.append(Constantes.MODALIDA_USO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(redeEstado == null) {
			stringBuilder.append(Constantes.ESTADO_REDE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}


		if(redeMaterial == null || redeMaterial.getChavePrimaria() == -1) {
			stringBuilder.append(Constantes.MATERIAL_REDE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(CollectionUtils.isEmpty(listaAparelhos)) {
			stringBuilder.append(Constantes.APARELHO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}
}
