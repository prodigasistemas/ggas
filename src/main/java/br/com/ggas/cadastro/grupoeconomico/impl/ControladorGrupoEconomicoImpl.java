/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.grupoeconomico.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.grupoeconomico.ControladorGrupoEconomico;
import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe ControladorGrupoEconomicoImpl
 * 
 * @author Procenge
 */
public class ControladorGrupoEconomicoImpl extends ControladorNegocioImpl implements ControladorGrupoEconomico {

	protected static final String LISTA_GRUPO_ECONOMICO = "listaGrupoEconomico";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final String ID_CLIENTE = "idCliente";

	protected static final String LISTA_SEGMENTO = "listaSegmento";

	private static final String ID_SEGMENTO = "idSegmento";

	private final ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getBeanPorID(
					ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
	
	@Override
	public GrupoEconomico criar() {

		return (GrupoEconomico) ServiceLocator.getInstancia().getBeanPorID(GrupoEconomico.BEAN_ID_GRUPO_ECONOMICO);
	}

	public Class<?> getClasseEntidadeSegmento() {

		return ServiceLocator.getInstancia().getClassPorID(Segmento.BEAN_ID_SEGMENTO);
	}

	public Class<?> getClasseEntidadeCliente() {

		return ServiceLocator.getInstancia().getClassPorID(Cliente.BEAN_ID_CLIENTE);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(GrupoEconomico.BEAN_ID_GRUPO_ECONOMICO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.grupoeconomico.ControladorGrupoEconomico#listarGrupoEconomico()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<GrupoEconomico> listarGrupoEconomico() throws NegocioException {

		Criteria criteria = getCriteria();
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.grupoeconomico.ControladorGrupoEconomico#consultarGrupoEconomico(java.util.Map)
	 */
	@Override
	@SuppressWarnings({"unchecked"})
	public Collection<GrupoEconomico> consultarGrupoEconomico(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if (filtro != null) {
			Long idCliente = (Long) filtro.get(ID_CLIENTE);
			if ((idCliente != null) && (idCliente > 0)) {
				Cliente cliente = controladorCliente.obterCliente(idCliente, "enderecos", "fones");
				if (cliente != null) {
					GrupoEconomico grupoEconomicoCliente = cliente.getGrupoEconomico();
					if (grupoEconomicoCliente != null) {
						Long idGrupoEconomicoCliente = grupoEconomicoCliente.getChavePrimaria();
						if (idGrupoEconomicoCliente != null && idGrupoEconomicoCliente > 0) {
							criteria.add(Restrictions.eq(CHAVE_PRIMARIA, idGrupoEconomicoCliente));
						}
					} else {
						Collection<GrupoEconomico> listaGrupoEconomico = criteria.list();
						listaGrupoEconomico.clear();
						return listaGrupoEconomico;
					}
				}
			}
			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			String descricao = (String) filtro.get(DESCRICAO);
			if (descricao != null && !descricao.isEmpty()) {
				criteria.add(Restrictions.like(DESCRICAO, "%" + descricao + "%"));
			}
			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}
			Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
			if (idSegmento != null && idSegmento > 0) {
				criteria.add(Restrictions.eq("segmento.chavePrimaria", idSegmento));
			}
		}
		criteria.addOrder(Order.asc(DESCRICAO));

		Collection<GrupoEconomico> listaGrupoEconomico = criteria.list();

		Iterator<GrupoEconomico> iterator = listaGrupoEconomico.iterator();

		while (iterator.hasNext()) {
			GrupoEconomico grupoEconomico = iterator.next();
			Collection<Cliente> listaCliente = controladorCliente.listarClientePorGrupoEconomico(grupoEconomico.getChavePrimaria());
			Integer qtdCliente = 0;
			if (listaCliente != null && !listaCliente.isEmpty()) {
				qtdCliente = listaCliente.size();
				grupoEconomico.setQtdCliente(qtdCliente);
			}
		}

		return listaGrupoEconomico;
	}

	/**
	 * Remover cliente grupo economico.
	 * 
	 * @param grupoEconomico
	 *            the grupo economico
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void removerClienteGrupoEconomico(GrupoEconomico grupoEconomico) throws GGASException {

		Long idGrupoEconomico = grupoEconomico.getChavePrimaria();
		Collection<Cliente> listaCliente = controladorCliente.listarClientePorGrupoEconomico(idGrupoEconomico);
		if (listaCliente != null && !listaCliente.isEmpty()) {
			for (Cliente cliente : listaCliente) {
				cliente.setGrupoEconomico(null);
				controladorCliente.atualizar(cliente);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.grupoeconomico.ControladorGrupoEconomico#vincularClienteGrupoEconomico(java.lang.Long[],
	 * br.com.ggas.cadastro.grupoeconomico.GrupoEconomico)
	 */
	@Override
	public void vincularClienteGrupoEconomico(Long[] listaIdCliente, GrupoEconomico grupoEconomico) throws GGASException {

		Collection<Cliente> listaCliente = controladorCliente.listarClientePorGrupoEconomico(grupoEconomico.getChavePrimaria());
		if (listaCliente != null && !listaCliente.isEmpty()) {
			if (listaIdCliente != null && listaIdCliente.length > 0) {

				Iterator<Cliente> it = listaCliente.iterator();
				while (it.hasNext()) {
					it.next().setGrupoEconomico(null);
				}

				for (Long chave : listaIdCliente) {
					Boolean achou = false;
					it = listaCliente.iterator();
					achou = this.encontrarCliente(it, chave, grupoEconomico, achou);
					this.adicionarClienteEmLista(achou, chave, grupoEconomico, listaCliente);
				}

				for (Cliente cliente : listaCliente) {
					cliente.getEnderecos();
					cliente.getFones();
					controladorCliente.atualizar(cliente);
				}
			} else {
				for (Cliente cliente : listaCliente) {
					cliente.setGrupoEconomico(null);
					controladorCliente.atualizar(cliente);
				}
			}
		} else if (listaIdCliente != null && listaIdCliente.length > 0) {
			listaCliente = controladorCliente.consultarCliente(listaIdCliente);
			for (Cliente cliente : listaCliente) {
				cliente.setGrupoEconomico(grupoEconomico);
				controladorCliente.atualizar(cliente);
			}
		}
	}
	
	private boolean encontrarCliente(Iterator<Cliente> it, Long chave, GrupoEconomico grupoEconomico, boolean achouAux) {
		boolean achou = achouAux;
		while (it.hasNext()) {
			Cliente cliente = it.next();
			if (cliente.getChavePrimaria() == chave) {
				cliente.setGrupoEconomico(grupoEconomico);
				achou = true;
			}
		}
		return achou;
	}

	private void adicionarClienteEmLista(boolean achou, Long chave, GrupoEconomico grupoEconomico, Collection<Cliente> listaCliente)
			throws NegocioException {
		if (!achou) {
			Cliente cli = controladorCliente.obterCliente(chave, "enderecos", "fones");
			cli.setGrupoEconomico(grupoEconomico);
			listaCliente.add(cli);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.grupoeconomico.ControladorGrupoEconomico#removerGrupoEconomico(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerGrupoEconomico(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		for (Long chavePrimaria : chavesPrimarias) {
			GrupoEconomico grupoEconomico = (GrupoEconomico) obter(chavePrimaria);
			removerClienteGrupoEconomico(grupoEconomico);
		}
		remover(chavesPrimarias, dadosAuditoria);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.grupoeconomico.ControladorGrupoEconomico#validarGrupoEconomico(br.com.ggas.cadastro.grupoeconomico.GrupoEconomico
	 * )
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void validarGrupoEconomico(GrupoEconomico grupoEconomico) throws GGASException {

		if (grupoEconomico != null) {
			Criteria criteria = getCriteria();
			String descricao = grupoEconomico.getDescricao();
			criteria.add(Restrictions.eq(DESCRICAO, descricao));
			Collection<GrupoEconomico> listaGrupoEconomico = criteria.list();
			if (listaGrupoEconomico != null && !listaGrupoEconomico.isEmpty()) {
				if (listaGrupoEconomico.size() == 1) {
					this.verificarGrupoEconomicoExistente(listaGrupoEconomico, grupoEconomico);
				} else if (listaGrupoEconomico.size() > 1) {
					throw new NegocioException(ControladorGrupoEconomico.ERRO_GRUPO_ECONOMICO_EXISTENTE, true);
				}
			}
		}
	}
	
	private void verificarGrupoEconomicoExistente(Collection<GrupoEconomico> listaGrupoEconomico, GrupoEconomico grupoEconomico)
			throws NegocioException {
		for (GrupoEconomico grupoEconomicoAux : listaGrupoEconomico) {
			Long idGEAux = grupoEconomicoAux.getChavePrimaria();
			if (idGEAux != grupoEconomico.getChavePrimaria()) {
				throw new NegocioException(ControladorGrupoEconomico.ERRO_GRUPO_ECONOMICO_EXISTENTE, true);
			}
		}
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.grupoeconomico.ControladorGrupoEconomico#incluirGrupoEconomico(br.com.ggas.cadastro.grupoeconomico.GrupoEconomico
	 * )
	 */
	@Override
	public Long incluirGrupoEconomico(GrupoEconomico grupoEconomico) throws GGASException {

		validarGrupoEconomico(grupoEconomico);

		return inserir(grupoEconomico);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.grupoeconomico.ControladorGrupoEconomico#atualizarGrupoEconomico(br.com.ggas.cadastro.grupoeconomico.GrupoEconomico
	 * )
	 */
	@Override
	public void atualizarGrupoEconomico(GrupoEconomico grupoEconomico) throws GGASException {

		validarGrupoEconomico(grupoEconomico);
		atualizar(grupoEconomico);
	}
	

}


