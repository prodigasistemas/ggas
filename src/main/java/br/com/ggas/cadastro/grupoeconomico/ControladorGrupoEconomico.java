/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.grupoeconomico;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface ControladorGrupoEconomico
 * 
 * @author Procenge
 */
public interface ControladorGrupoEconomico extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_GRUPO_ECONOMICO = "controladorGrupoEconomico";

	String ERRO_GRUPO_ECONOMICO_EXISTENTE = "ERRO_GRUPO_ECONOMICO_EXISTENTE";

	String ERRO_CLIENTE_GRUPO_ECONOMICO = "ERRO_CLIENTE_GRUPO_ECONOMICO";

	String ERRO_GRUPO_ECONOMICO_NOME_VAZIO = "ERRO_GRUPO_ECONOMICO_NOME_VAZIO";

	/**
	 * Consultar grupo economico.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<GrupoEconomico> consultarGrupoEconomico(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Listar grupo economico.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<GrupoEconomico> listarGrupoEconomico() throws NegocioException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.ControladorNegocio#criar()
	 */
	@Override
	GrupoEconomico criar();

	/**
	 * Incluir grupo economico.
	 * 
	 * @param grupoEconomico the grupo economico
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	Long incluirGrupoEconomico(GrupoEconomico grupoEconomico) throws GGASException;

	/**
	 * Metodo responsável por verificar a existência de um grupo econômico e atualizar os campos relacionados
	 * 
	 * @param grupoEconomico {@link GrupoEconomico}
	 * @throws GGASException {@link GGASException}
	 */
	void atualizarGrupoEconomico(GrupoEconomico grupoEconomico) throws GGASException;

	/**
	 * Vincular cliente grupo economico.
	 * 
	 * @param listaIdCliente the lista id cliente
	 * @param grupoEconomico the grupo economico
	 * @throws GGASException the GGAS exception
	 */
	void vincularClienteGrupoEconomico(Long[] listaIdCliente, GrupoEconomico grupoEconomico) throws GGASException;

	/**
	 * Remover grupo economico.
	 * 
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	void removerGrupoEconomico(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Validar grupo economico.
	 * 
	 * @param grupoEconomico the grupo economico
	 * @throws GGASException the GGAS exception
	 */
	void validarGrupoEconomico(GrupoEconomico grupoEconomico) throws GGASException;

}
