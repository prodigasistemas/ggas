/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.endereco;

import java.io.Serializable;

/**
 * Interface Endereco
 * 
 * @author Procenge
 */
public interface Endereco extends Serializable {

	String BEAN_ID_ENDERECO = "endereco";

	String ENDERECO_NUMERO = "ENDERECO_NUMERO";

	String ENDERECO_CEP = "ENDERECO_CEP";

	/**
	 * @return the cep
	 */
	Cep getCep();

	/**
	 * @param cep
	 *            the cep to set
	 */
	void setCep(Cep cep);

	/**
	 * @return the numero
	 */
	String getNumero();

	/**
	 * @param numero
	 *            the numero to set
	 */
	void setNumero(String numero);

	/**
	 * @return the complemento
	 */
	String getComplemento();

	/**
	 * @param complemento
	 *            the complemento to set
	 */
	void setComplemento(String complemento);

	/**
	 * @return the enderecoReferencia
	 */
	String getEnderecoReferencia();

	/**
	 * @param enderecoReferencia
	 *            the enderecoReferencia to set
	 */
	void setEnderecoReferencia(String enderecoReferencia);

	/**
	 * Recupera o endereço formatado.
	 * 
	 * @return Um String com o endereço formatado.
	 */
	String getEnderecoFormatado();
}
