/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.endereco.impl;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.Endereco;

/**
 * Classe responsável por implementar os métodos relacionados ao Endereço
 *
 */
public class EnderecoImpl implements Endereco {

	private static final long serialVersionUID = -441537545045828518L;

	private Cep cep;

	private String numero;

	private String complemento;

	private String enderecoReferencia;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * getCep()
	 */
	@Override
	public Cep getCep() {

		return cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * setCep(br.com.ggas.cadastro.endereco.Cep)
	 */
	@Override
	public void setCep(Cep cep) {

		this.cep = cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * getNumero()
	 */
	@Override
	public String getNumero() {

		return numero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * setNumero(java.lang.String)
	 */
	@Override
	public void setNumero(String numero) {

		this.numero = numero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * getComplemento()
	 */
	@Override
	public String getComplemento() {

		return complemento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * setComplemento(java.lang.String)
	 */
	@Override
	public void setComplemento(String complemento) {

		this.complemento = complemento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * getEnderecoReferencia()
	 */
	@Override
	public String getEnderecoReferencia() {

		return enderecoReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * setEnderecoReferencia(java.lang.String)
	 */
	@Override
	public void setEnderecoReferencia(String enderecoReferencia) {

		this.enderecoReferencia = enderecoReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * getEnderecoFormatado()
	 */
	@Override
	public String getEnderecoFormatado() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";
		
		if (this.getCep() != null && this.getCep().getTipoLogradouro() != null) {
			enderecoFormatado.append(this.getCep().getTipoLogradouro());
		}
		
		if (this.getCep() != null && this.getCep().getLogradouro() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = " ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getLogradouro());
		}
		
		if (this.getNumero() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getNumero());
		}
		
		if (this.getCep() != null && this.getCep().getBairro() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getBairro());
		}
		
		if (this.getCep() != null && this.getCep().getMunicipio() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getMunicipio());
		}
		
		if (this.getCep() != null && this.getEnderecoReferencia() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getEnderecoReferencia());
		}

		return enderecoFormatado.toString();
	}

}
