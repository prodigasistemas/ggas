/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.endereco;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.impl.EnderecoTemporarioFaturamento;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface ControladorEndereco
 * 
 * @author Procenge
 */
public interface ControladorEndereco extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_ENDERECO = "controladorEndereco";

	/**
	 * Método responsável por criar um CEP.
	 * 
	 * @return retorna um CEP
	 */
	EntidadeNegocio criarCep();

	/**
	 * Método responsável por criar um endereço.
	 * 
	 * @return Um endereço
	 */
	Endereco criarEndereco();
	
	/**
	 *  Método responsável por criar uma entidade Cep.
	 * @return EntidadeCep.
	 */
	
	Class<?> getClasseEntidadeCep();

	/**
	 * Método responsável por obter um CEP pela
	 * chave primária informada.
	 * 
	 * @param chavePrimaria
	 *            Chave primária do CEP.
	 * @return Uma entidade Cep.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Cep obterCepPorChave(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter um CEP.
	 * 
	 * @param cep
	 *            o Código do CEP
	 * @return Um CEP
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	Cep obterCep(String cep) throws NegocioException;

	/**
	 * Método responsável por obter todos os CEPs
	 * pelo número de CEP.
	 * 
	 * @param cep
	 *            Número do CEP.
	 * @param habilitado
	 *            the habilitado
	 * @return Um coleção de CEPs.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             execução do método.
	 */
	Collection<Cep> obterCeps(String cep, boolean habilitado) throws NegocioException;

	/**
	 * Método que importa os Ceps informados no
	 * arquivo selecionado na tela de
	 * importar cep.
	 * 
	 * @param arquivo
	 *            InputStream
	 * @param tipoArquivo
	 *            String
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	Map<String, Integer> importarCep(InputStream arquivo, String tipoArquivo) throws NegocioException, ConcorrenciaException;

	/**
	 * Método que consulta a tabela de Ceps pela
	 * UF, Município e logradouro informados como
	 * parâmetro.
	 * 
	 * @param uf
	 *            the uf
	 * @param municipio
	 *            the municipio
	 * @param logradouro
	 *            the logradouro
	 * @return listaDeCeps
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Cep> consultarCeps(String uf, String municipio, String logradouro) throws NegocioException;

	/**
	 * Método que valida a existência do cep
	 * informado.
	 * 
	 * @param cep
	 *            Um cep.
	 * @return true caso o cep esteja cadastrado.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean validarExistenciaCep(Cep cep) throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * unidades federação.
	 * 
	 * @return coleção de UnidadeFederacao.
	 */
	Collection<UnidadeFederacao> listarTodasUnidadeFederacao();

	/**
	 * Método responsável por obter uma unidade
	 * federação.
	 * 
	 * @param chavePrimaria
	 *            o Código da unidade federação.
	 * @return Um UnidadeFederacao
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	UnidadeFederacao obterUnidadeFederacao(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter uma referência
	 * de endereço.
	 * 
	 * @param chavePrimaria
	 *            o Código da Referência de
	 *            endereço.
	 * @return Um EnderecoReferencia
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	EnderecoReferencia obterEnderecoReferenciaPorChave(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por listar os endereços
	 * de referências possíveis.
	 * 
	 * @return coleção de endereços referências.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EnderecoReferencia> listarEnderecoReferencia() throws NegocioException;

	/**
	 * Consultar ceps.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Cep> consultarCeps(Map<String, Object> filtro) throws NegocioException;
	
	/**
	 * Inserir endereco temporario faturamento.
	 * 
	 * @param enderecoTemporarioFaturamento
	 *            the endereco temporario faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirEnderecoTemporarioFaturamento(EnderecoTemporarioFaturamento enderecoTemporarioFaturamento) throws NegocioException;

	/**
	 * Remover endereco temporario faturamento.
	 * 
	 * @param enderecoTemporarioFaturamento
	 *            the endereco temporario faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerEnderecoTemporarioFaturamento(EnderecoTemporarioFaturamento enderecoTemporarioFaturamento) throws NegocioException;

	/**
	 * Obter endereco temporario faturamento.
	 * 
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @return the endereco temporario faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	EnderecoTemporarioFaturamento obterEnderecoTemporarioFaturamento(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException;

	/**
	 * Consultar bairros por município
	 * 
	 * @param nomeMunicipio {@link String}
	 * @return Collection {@link Collection}
	 */
	Collection<String> consultarBairros(String nomeMunicipio);
	
	/**
	 * Método responsável por trazer cep, número do imóvel, complemento
	 * 
	 * @param contratoPontoConsumo {@link ContratoPontoConsumo}
	 * @return EnderecoTemporarioFaturamento {@link EnderecoTemporarioFaturamento}
	 * @throws NegocioException {@link NegocioException}
	 */
	public EnderecoTemporarioFaturamento obterPrinciapaisAtributosEnderecoTemporarioFaturamento(
			ContratoPontoConsumo contratoPontoConsumo) throws NegocioException;
	
	/**
	 * Consultar ceps por logradouro
	 * 
	 * @param logradouro
	 * @return List<Cep> {@link Cep}
	 * @throws NegocioException {@link NegocioException}
	 */
	List<Cep> consultarCepsPorLougradouro(String logradouro, String tipoLogradouro) throws NegocioException;
}
