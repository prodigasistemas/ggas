/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.endereco.impl;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.endereco.EnderecoReferencia;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.impl.EnderecoTemporarioFaturamento;
import br.com.ggas.geral.dominio.EntidadeDominio;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * 
 */
class ControladorEnderecoImpl extends ControladorNegocioImpl implements ControladorEndereco {

	private static final int TAMANHO_MAXIMO_TIPO_LOGRADOURO = 255;
	private static final int INDICE_TIPO_LOGRADOURO = 4;
	private static final int TAMANHO_MAXIMO_INTERVALO_NUMERACAO = 255;
	private static final int INDICE_INTERVALO_NUMERACAO = 6;
	private static final int TAMANHO_MAXIMO_BAIRRO = 255;
	private static final int INDICE_BAIRRO = 5;
	private static final int TAMANHO_MAXIMO_LOGRADOURO = 255;
	private static final int INDICE_LOGRADOURO = 3;
	private static final int TAMANHO_CEP = 9;
	private static final int INDICE_CEP = 2;
	private static final int TAMANHO_MUNICIPIO = 255;
	private static final int TAMANHO_MAXIMO_UF = 2;
	private static final int LIMITE_OBJETOS = 1001;
	private static final Logger LOG = Logger.getLogger(ControladorEnderecoImpl.class);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.ControladorEndereco#criarEndereco()
	 */
	@Override
	public Endereco criarEndereco() {

		return (Endereco) ServiceLocator.getInstancia().getBeanPorID(Endereco.BEAN_ID_ENDERECO);
	}

	/**
	 * Criar unidade federacao.
	 * 
	 * @return the entidade dominio
	 */
	public EntidadeDominio criarUnidadeFederacao() {

		return (EntidadeDominio) ServiceLocator.getInstancia().getBeanPorID(UnidadeFederacao.BEAN_ID_UNIDADE_FEDERACAO);
	}

	/**
	 * Método que cria a entidade Cep.
	 * 
	 * @return Instância de Cep
	 */
	@Override
	public EntidadeNegocio criarCep() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Cep.BEAN_ID_CEP);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Endereco.BEAN_ID_ENDERECO);
	}

	@Override
	public Class<?> getClasseEntidadeCep() {

		return ServiceLocator.getInstancia().getClassPorID(Cep.BEAN_ID_CEP);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.
	 * ControladorEndereco
	 * #obterCepPorChave(java.lang.Long)
	 */
	@Override
	public Cep obterCepPorChave(Long chavePrimaria) throws NegocioException {

		return (Cep) obter(chavePrimaria, getClasseEntidadeCep());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.
	 * ControladorEndereco
	 * #obterUnidadeFederacao(java.lang
	 * .Long)
	 */
	@Override
	public UnidadeFederacao obterUnidadeFederacao(Long chavePrimaria) throws NegocioException {

		return (UnidadeFederacao) obter(chavePrimaria, getClasseEntidadeUnidadeFederacao());
	}

	public Class<?> getClasseEntidadeUnidadeFederacao() {

		return ServiceLocator.getInstancia().getClassPorID(UnidadeFederacao.BEAN_ID_UNIDADE_FEDERACAO);
	}

	public Class<?> getClasseEntidadeEnderecoReferencia() {

		return ServiceLocator.getInstancia().getClassPorID(EnderecoReferencia.BEAN_ID_ENDERECO_REFERENCIA);
	}

	public Class<?> getClasseEntidadeMunicipio() {

		return ServiceLocator.getInstancia().getClassPorID(Municipio.BEAN_ID_MUNICIPIO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.
	 * ControladorEndereco
	 * #obterEnderecoReferenciaPorChave
	 * (java.lang.Long)
	 */
	@Override
	public EnderecoReferencia obterEnderecoReferenciaPorChave(Long chavePrimaria) throws NegocioException {

		return (EnderecoReferencia) obter(chavePrimaria, getClasseEntidadeEnderecoReferencia());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.
	 * ControladorEndereco
	 * #obterCep(java.lang.String)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Cep obterCep(String cep) throws NegocioException {

		Cep cepEncontrado = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCep().getSimpleName());
		hql.append(" where ");
		hql.append(" cep = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, cep);
		query.setMaxResults(1);

		cepEncontrado = (Cep) query.uniqueResult();

		if(cepEncontrado == null) {
			throw new NegocioException(Constantes.ERRO_CEP_NAO_ENCONTRADO, cep);
		}

		return cepEncontrado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.
	 * ControladorEndereco
	 * #obterCeps(java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Cep> obterCeps(String cep, boolean habilitado) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCep().getSimpleName());
		hql.append(" where ");
		hql.append(" cep = ? ");
		if(habilitado) {
			hql.append(" and habilitado = true ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, cep);

		Collection<Cep> listaCeps = query.list();

		if((listaCeps == null) || (listaCeps.isEmpty())) {
			throw new NegocioException(Constantes.ERRO_CEP_NAO_ENCONTRADO, cep);
		}

		return listaCeps;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.
	 * ControladorEndereco
	 * #consultarCeps(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Cep> consultarCeps(String uf, String municipio, String logradouro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeCep());

		if(!StringUtils.isEmpty(uf)) {
			criteria.add(Restrictions.eq("uf", uf));
		}

		if(!StringUtils.isEmpty(municipio)) {
			criteria.add(Restrictions.sqlRestriction("UPPER(" + Util.removerAcentuacaoQuerySql("CEP_NM_MUNICIPIO") + ") like '"
					+ Util.removerAcentuacao(Util.formatarTextoConsulta(municipio)).toUpperCase() + "'"));
		}

		if(!StringUtils.isEmpty(logradouro)) {
			criteria.add(Restrictions.sqlRestriction("UPPER(" + Util.removerAcentuacaoQuerySql("CEP_NM_LOGRADOURO") + ") like '"
					+ Util.removerAcentuacao(Util.formatarTextoConsulta(logradouro)).toUpperCase() + "'"));
		}
		
		criteria.setMaxResults(LIMITE_OBJETOS);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.ControladorEndereco#consultarCeps(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Cep> consultarCeps(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeCep());
		if(filtro != null) {

			Long idMunicipio = (Long) filtro.get("idMunicipio");
			if(idMunicipio != null && idMunicipio > 0) {
				criteria.add(Restrictions.eq("municipio.chavePrimaria", idMunicipio));
			}
			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.
	 * ControladorEndereco
	 * #importarCep(java.io.InputStream,
	 * java.lang.String)
	 */
	@Override
	@SuppressWarnings({"unchecked", "squid:S1943"})
	public Map<String, Integer> importarCep(InputStream arquivo, String tipoArquivo) throws NegocioException, ConcorrenciaException {

		Map<String, Integer> resumoImportacao = new HashMap<>();

		LOG.info("Inicio da importação de Cep : " + new Date());

		if((arquivo != null) && (Cep.EXTENCAO_ARQUIVO_IMPORTACAO.equals(tipoArquivo))) {

			Reader reader = null;
			BufferedReader leitor = null;
			Query query = null;
			Session session = getSession();

			StringBuilder hql = null;

			// Variáveis usadas dentro do while
			Cep cepObj;
			String uf;
			String municipio;
			String cep;
			String logradouro;
			String tipoLogradouro;
			String bairro;
			String intervaloNumeracao;
			boolean isInclusao;

			try {
				reader = new InputStreamReader(arquivo);
				leitor = new BufferedReader(reader);

				String linha = null;

				while(!StringUtils.isEmpty(linha = leitor.readLine())) {
					String[] colunas = linha.split(Constantes.DELIMITADOR_ARQUIVO_IMPORTACA0);

					if(colunas.length == Cep.QTD_COLUNAS_ARQUIVO_IMPORTACA0) {

						// *************** ---
						// validar UF ---
						// ***************
						uf = colunas[0].trim();
						if(("".equals(uf)) || (uf.length() != TAMANHO_MAXIMO_UF)) {
							incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS);
							continue;
						}

						hql = new StringBuilder();
						hql.append(" from ");
						hql.append(getClasseEntidadeUnidadeFederacao().getSimpleName());
						hql.append(" where ");
						hql.append(" lower(sigla) = :uf");
						query = session.createQuery(hql.toString());
						query.setString("uf", uf.toLowerCase());
						UnidadeFederacao ufConsultada = (UnidadeFederacao) query.setMaxResults(1).uniqueResult();
						if(ufConsultada == null) {
							incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS);
							continue;
						}

						// *************** ---
						// validar municipio ---
						// ***************
						municipio = colunas[1].trim();
						if(("".equals(municipio)) || (municipio.length() > TAMANHO_MUNICIPIO)) {
							incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS);
							continue;
						}

						hql = new StringBuilder();
						hql.append(" from ");
						hql.append(getClasseEntidadeMunicipio().getSimpleName());
						hql.append(" where lower(descricao) = :descricao");
						hql.append(" and unidadeFederacao = :uf");
						query = session.createQuery(hql.toString());
						query.setString("descricao", municipio.toLowerCase());
						query.setEntity("uf", ufConsultada);
						Municipio municipioConsultado = (Municipio) query.setMaxResults(1).uniqueResult();
						if(municipioConsultado == null) {
							incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS);
							continue;
						}

						cep = colunas[INDICE_CEP].trim();
						if(("".equals(cep)) || (cep.length() != TAMANHO_CEP)) {
							incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS);
							continue;
						}

						logradouro = colunas[INDICE_LOGRADOURO].trim();
						if(("".equals(logradouro)) || (logradouro.length() > TAMANHO_MAXIMO_LOGRADOURO)) {
							incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS);
							continue;
						}

						tipoLogradouro = colunas[INDICE_TIPO_LOGRADOURO].trim();
						if(("".equals(tipoLogradouro)) || (tipoLogradouro.length() > TAMANHO_MAXIMO_TIPO_LOGRADOURO)) {
							incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS);
							continue;
						}

						bairro = colunas[INDICE_BAIRRO].trim();
						if(("".equals(bairro)) || (bairro.length() > TAMANHO_MAXIMO_BAIRRO)) {
							incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS);
							continue;
						}

						intervaloNumeracao = colunas[INDICE_INTERVALO_NUMERACAO].trim();

						if(!("".equals(intervaloNumeracao)) && (intervaloNumeracao.length() > TAMANHO_MAXIMO_INTERVALO_NUMERACAO)) {
							incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS);
							continue;
						}

						isInclusao = false;

						hql = new StringBuilder();
						hql.append(" from ");
						hql.append(getClasseEntidadeCep().getSimpleName());
						hql.append(" where ");
						hql.append(" cep = ?");

						query = session.createQuery(hql.toString());
						query.setString(0, colunas[INDICE_CEP]);

						Collection<Cep> listaCeps = query.list();
						cepObj = null;
						if(listaCeps != null && !listaCeps.isEmpty()) {
							cepObj = ((List<Cep>) listaCeps).get(0);
						}

						if(cepObj == null) {
							cepObj = (Cep) criarCep();
							isInclusao = true;
						}

						cepObj.setUf(uf);
						cepObj.setNomeMunicipio(municipio);
						cepObj.setCep(cep);
						cepObj.setLogradouro(logradouro);
						cepObj.setTipoLogradouro(tipoLogradouro);
						cepObj.setBairro(bairro);
						cepObj.setIntervaloNumeracao(intervaloNumeracao);

						if(isInclusao) {
							cepObj.setVersao(0);
							this.inserirCep(cepObj);
							incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_INCLUIDAS);
						} else {
							cepObj.setVersao(cepObj.getVersao() + 1);
							this.atualizarCep(cepObj);
							incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_ALTERADAS);
						}

					} else {
						incrementarResumoImportacao(resumoImportacao, Cep.CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS);
					}
				}
			} catch(FileNotFoundException e) {
				LOG.error(e.getMessage(), e);
			} catch(IOException e) {
				LOG.error(e.getMessage(), e);
			} 
			Util.limparRecursos(arquivo);
			Util.limparRecursos(reader);
			Util.limparRecursos(leitor);
		}

		LOG.info("@@@@@@@@@@@@@@@@@@@@@@@ Fim da rotina de importação do CEP: " + new Date() + " @@@@@@@@@@@@@@@@@@@@@@");

		return resumoImportacao;
	}

	/**
	 * Inserir cep.
	 * 
	 * @param cep
	 *            the cep
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void inserirCep(Cep cep) throws NegocioException {

		inserir(cep);

	}

	/**
	 * Atualizar cep.
	 * 
	 * @param cep
	 *            the cep
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void atualizarCep(Cep cep) throws NegocioException, ConcorrenciaException {

		atualizar(cep, CepImpl.class);

	}

	/**
	 * Incrementar resumo importacao.
	 * 
	 * @param resumoImportacao
	 *            the resumo importacao
	 * @param chave
	 *            the chave
	 */
	private void incrementarResumoImportacao(Map<String, Integer> resumoImportacao, String chave) {

		Integer qtd = resumoImportacao.get(chave);
		if(qtd == null) {
			qtd = 0;
		}
		qtd = qtd + 1;
		resumoImportacao.put(chave, qtd);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.
	 * ControladorEndereco
	 * #validarExistenciaCep(br.com.procenge
	 * .ggas.cadastro.endereco.Cep)
	 */
	@Override
	public boolean validarExistenciaCep(Cep cep) throws NegocioException {

		Long quantidade = 0L;
		boolean cepExistente = false;
		Criteria criteria = createCriteria(this.getClasseEntidadeCep());
		criteria.setProjection(Projections.rowCount());
		criteria.add(Restrictions.eq("cep", cep.getCep()));
		quantidade = (Long) criteria.uniqueResult();

		if(quantidade <= 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CEP_INEXISTENTE, true);
		} else {
			cepExistente = true;
		}

		return cepExistente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.
	 * ControladorEndereco
	 * #listarTodasUnidadeFederacao()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<UnidadeFederacao> listarTodasUnidadeFederacao() {

		return createCriteria(getClasseEntidadeUnidadeFederacao()).addOrder(Order.asc("descricao")).list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.ControladorEndereco#listarEnderecoReferencia()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<EnderecoReferencia> listarEnderecoReferencia() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeEnderecoReferencia());
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.ControladorEndereco#inserirEnderecoTemporarioFaturamento(br.com.ggas.contrato.contrato.impl.
	 * EnderecoTemporarioFaturamento)
	 */
	@Override
	public void inserirEnderecoTemporarioFaturamento(EnderecoTemporarioFaturamento enderecoTemporarioFaturamento) throws NegocioException {

		this.inserir(enderecoTemporarioFaturamento);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.ControladorEndereco#removerEnderecoTemporarioFaturamento(br.com.ggas.contrato.contrato.impl.
	 * EnderecoTemporarioFaturamento)
	 */
	@Override
	public void removerEnderecoTemporarioFaturamento(EnderecoTemporarioFaturamento enderecoTemporarioFaturamento) throws NegocioException {

		this.remover(enderecoTemporarioFaturamento);

	}

	public Class<?> getClasseEntidadeEnderecoTemporarioFaturamento() {

		return EnderecoTemporarioFaturamento.class;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.ControladorEndereco#obterEnderecoTemporarioFaturamento(br.com.ggas.contrato.contrato.ContratoPontoConsumo
	 * )
	 */
	@Override
	public EnderecoTemporarioFaturamento obterEnderecoTemporarioFaturamento(ContratoPontoConsumo contratoPontoConsumo)
					throws NegocioException {

		EnderecoTemporarioFaturamento enderecoTemporarioFaturamento = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeEnderecoTemporarioFaturamento().getSimpleName());
		hql.append(" enderecoTemporarioFaturamento ");
		hql.append(" inner join fetch enderecoTemporarioFaturamento.contratoPontoConsumo contratoPontoConsumo ");
		hql.append(" inner join fetch enderecoTemporarioFaturamento.cep cep ");
		hql.append(" where ");
		hql.append(" contratoPontoConsumo.chavePrimaria = :chavePrimaria ");
		hql.append(" and rownum = 1 ");
		hql.append(" order by enderecoTemporarioFaturamento.ultimaAlteracao desc ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimaria", contratoPontoConsumo.getChavePrimaria());
		query.setMaxResults(1);

		enderecoTemporarioFaturamento = (EnderecoTemporarioFaturamento) query.uniqueResult();

		return enderecoTemporarioFaturamento;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<String> consultarBairros(String nomeMunicipio) {
		
		Collection<String> bairro = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct cep.bairro ");
		hql.append(" from ");
		hql.append(getClasseEntidadeCep().getSimpleName());
		hql.append(" cep ");
		hql.append(" where ");
		hql.append(" cep.nomeMunicipio = :nomeMunicipio");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("nomeMunicipio", nomeMunicipio);

		bairro = (Collection<String>) query.list();

		return bairro;
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.ControladorEndereco#obterEnderecoTemporarioFaturamento(br.com.ggas.contrato.contrato.ContratoPontoConsumo
	 * )
	 */
	@Override
	public EnderecoTemporarioFaturamento obterPrinciapaisAtributosEnderecoTemporarioFaturamento(ContratoPontoConsumo contratoPontoConsumo)
					throws NegocioException {

		EnderecoTemporarioFaturamento enderecoTemporarioFaturamento = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select enderecoTemporarioFaturamento.cep as cep, ");
		hql.append(" enderecoTemporarioFaturamento.numeroImovel as numeroImovel, ");
		hql.append(" enderecoTemporarioFaturamento.complemento as complemento ");
		hql.append(" from ");
		hql.append(getClasseEntidadeEnderecoTemporarioFaturamento().getSimpleName());
		hql.append(" enderecoTemporarioFaturamento ");
		hql.append(" inner join enderecoTemporarioFaturamento.contratoPontoConsumo contratoPontoConsumo ");
		hql.append(" inner join enderecoTemporarioFaturamento.cep cep ");
		hql.append(" where ");
		hql.append(" contratoPontoConsumo.chavePrimaria = :chavePrimaria ");
		hql.append(" and rownum = 1 ");
		hql.append(" order by enderecoTemporarioFaturamento.ultimaAlteracao desc ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(new GGASTransformer(getClasseEntidadeEnderecoTemporarioFaturamento(),
				super.getSessionFactory().getAllClassMetadata()));
		query.setParameter("chavePrimaria", contratoPontoConsumo.getChavePrimaria());
		query.setMaxResults(1);

		enderecoTemporarioFaturamento = (EnderecoTemporarioFaturamento) query.uniqueResult();

		return enderecoTemporarioFaturamento;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Cep> consultarCepsPorLougradouro(String logradouro, String tipoLogradouro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeCep());

		if(!StringUtils.isEmpty(logradouro)) {
			criteria.add(Restrictions.sqlRestriction("UPPER(" + Util.removerAcentuacaoQuerySql("CEP_NM_LOGRADOURO") + ") like '"
					+ Util.removerAcentuacao(Util.formatarTextoConsulta(logradouro)).toUpperCase() + "'"));
		}
		if(!StringUtils.isEmpty(tipoLogradouro)) {
			criteria.add(Restrictions.sqlRestriction("UPPER(" + Util.removerAcentuacaoQuerySql("CEP_DS_TIPO_LOGRADOURO") + ") like '"
					+ Util.removerAcentuacao(Util.formatarTextoConsulta(tipoLogradouro)).toUpperCase() + "'"));
		}
		
		criteria.setMaxResults(LIMITE_OBJETOS);

		return criteria.list();
	}
}
