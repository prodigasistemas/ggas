/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.endereco.impl;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe CepImpl
 * 
 * @author Procenge
 */
public class CepImpl extends EntidadeNegocioImpl implements Cep {

	private static final long serialVersionUID = 5843966420232381538L;

	private String uf;

	private String nomeMunicipio;

	private Municipio municipio;

	private String cep;

	private String logradouro;

	private String tipoLogradouro;

	private String bairro;

	private String intervaloNumeracao;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Cep#getUf()
	 */
	@Override
	public String getUf() {

		return uf;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Cep#setUf
	 * (java.lang.String)
	 */
	@Override
	public void setUf(String uf) {

		this.uf = uf;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Cep#getCep()
	 */
	@Override
	public String getCep() {

		return cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Cep#setCep
	 * (java.lang.String)
	 */
	@Override
	public void setCep(String cep) {

		this.cep = cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Cep#getLogradouro
	 * ()
	 */
	@Override
	public String getLogradouro() {

		return logradouro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Cep#setLogradouro
	 * (java.lang.String)
	 */
	@Override
	public void setLogradouro(String logradouro) {

		this.logradouro = logradouro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.Cep#
	 * getTipoLogradouro()
	 */
	@Override
	public String getTipoLogradouro() {

		return tipoLogradouro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.Cep#
	 * setTipoLogradouro(java.lang.String)
	 */
	@Override
	public void setTipoLogradouro(String tipoLogradouro) {

		this.tipoLogradouro = tipoLogradouro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Cep#getBairro
	 * ()
	 */
	@Override
	public String getBairro() {

		return bairro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Cep#setBairro
	 * (java.lang.String)
	 */
	@Override
	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.Cep#
	 * getIntervaloNumeracao()
	 */
	@Override
	public String getIntervaloNumeracao() {

		return intervaloNumeracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.endereco.Cep#
	 * setIntervaloNumeracao(java.lang.String)
	 */
	@Override
	public void setIntervaloNumeracao(String intervaloNumeracao) {

		this.intervaloNumeracao = intervaloNumeracao;
	}

	@Override
	public String getNomeMunicipio() {

		return nomeMunicipio;
	}

	@Override
	public void setNomeMunicipio(String nomeMunicipio) {

		this.nomeMunicipio = nomeMunicipio;
	}

	@Override
	public Municipio getMunicipio() {

		return municipio;
	}

	@Override
	public void setMunicipio(Municipio municipio) {

		this.municipio = municipio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return new HashMap<>();
	}

}
