/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.endereco;

import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface Cep
 * 
 * @author Procenge
 */
public interface Cep extends EntidadeNegocio {

	String BEAN_ID_CEP = "cep";

	String EXTENCAO_ARQUIVO_IMPORTACAO = "text/plain";

	String CHAVE_MAPA_RESUMO_IMPORTACAO_INCLUIDAS = "qtdIncluidas";

	String CHAVE_MAPA_RESUMO_IMPORTACAO_ALTERADAS = "qtdAlteradas";

	String CHAVE_MAPA_RESUMO_IMPORTACAO_REJEITADAS = "qtdRejeitadas";

	int QTD_COLUNAS_ARQUIVO_IMPORTACA0 = 7;

	/**
	 * @return the uf
	 */
	String getUf();

	/**
	 * @param uf
	 *            the uf to set
	 */
	void setUf(String uf);

	/**
	 * @return the cep
	 */
	String getCep();

	/**
	 * @param cep
	 *            the cep to set
	 */
	void setCep(String cep);

	/**
	 * @return the logradouro
	 */
	String getLogradouro();

	/**
	 * @param logradouro
	 *            the logradouro to set
	 */
	void setLogradouro(String logradouro);

	/**
	 * @return the tipoLogradouro
	 */
	String getTipoLogradouro();

	/**
	 * @param tipoLogradouro
	 *            the tipoLogradouro to set
	 */
	void setTipoLogradouro(String tipoLogradouro);

	/**
	 * @return the bairro
	 */
	String getBairro();

	/**
	 * @param bairro
	 *            the bairro to set
	 */
	void setBairro(String bairro);

	/**
	 * @return the intervaloNumeracao
	 */
	String getIntervaloNumeracao();

	/**
	 * @param intervaloNumeracao
	 *            the intervaloNumeracao to set
	 */
	void setIntervaloNumeracao(String intervaloNumeracao);

	/**
	 * @return String - Retorna município.
	 */
	String getNomeMunicipio();

	/**
	 * @param nomeMunicipio - Set nome município.
	 */
	void setNomeMunicipio(String nomeMunicipio);

	/**
	 * @return Municio - Retorna muncípio. 
	 */
	Municipio getMunicipio();

	/**
	 * @param municipio - Set munícipio.
	 */
	void setMunicipio(Municipio municipio);

}
