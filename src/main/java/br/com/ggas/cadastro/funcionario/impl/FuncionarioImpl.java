/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.funcionario.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.FuncionarioAfastamento;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe FuncionarioImpl
 * 
 * @author Procenge
 */
public class FuncionarioImpl extends EntidadeNegocioImpl implements Funcionario {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 8230258841765486308L;

	private String matricula;

	private String descricaoCargo;

	private String nome;

	private Empresa empresa;

	private UnidadeOrganizacional unidadeOrganizacional;

	private String email;

	private Integer codigoDDD;

	private String fone;

	private Usuario usuario;

	private Boolean indicadorFiscal;

	private Boolean indicadorVendedor;

	private byte[] fotoFuncionario;
	
	private Collection<FuncionarioAfastamento> listaAfastamento = new HashSet<>();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.impl.
	 * Funcionario#getDescricaoCargo()
	 */
	@Override
	public String getDescricaoCargo() {

		return descricaoCargo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.impl.
	 * Funcionario
	 * #setDescricaoCargo(java.lang.String)
	 */
	@Override
	public void setDescricaoCargo(String descricaoCargo) {

		this.descricaoCargo = descricaoCargo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.impl.
	 * Funcionario#getNome()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.impl.
	 * Funcionario#setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #getMatricula()
	 */
	@Override
	public String getMatricula() {

		return matricula;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #setMatricula(java.lang.Integer)
	 */
	@Override
	public void setMatricula(String matricula) {

		this.matricula = matricula;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #getEmpresa()
	 */
	@Override
	public Empresa getEmpresa() {

		return empresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #
	 * setEmpresa(br.com.ggas.cadastro.empresa.Empresa
	 * )
	 */
	@Override
	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #getUnidadeOrganizacional()
	 */
	@Override
	public UnidadeOrganizacional getUnidadeOrganizacional() {

		return unidadeOrganizacional;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #
	 * setUnidadeOrganizacional(br.com.ggas.cadastro
	 * .unidade.UnidadeOrganizacional)
	 */
	@Override
	public void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional) {

		this.unidadeOrganizacional = unidadeOrganizacional;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #getEmail()
	 */
	@Override
	public String getEmail() {

		return email;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(String email) {

		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #getCodigoDDD()
	 */
	@Override
	public Integer getCodigoDDD() {

		return codigoDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #setCodigoDDD(java.lang.Integer)
	 */
	@Override
	public void setCodigoDDD(Integer codigoDDD) {

		this.codigoDDD = codigoDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #getFone()
	 */
	@Override
	public String getFone() {

		return fone;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.funcionario.Funcionario
	 * #setFone(java.lang.String)
	 */
	@Override
	public void setFone(String fone) {

		this.fone = fone;
	}

	@Override
	public Usuario getUsuario() {

		return usuario;
	}

	@Override
	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.nome == null || this.nome.length() == 0) {
			stringBuilder.append(NOME);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.descricaoCargo == null || this.descricaoCargo.length() == 0) {
			stringBuilder.append(DESCRICAO_CARGO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(empresa == null) {
			stringBuilder.append(EMPRESA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(unidadeOrganizacional == null) {
			stringBuilder.append(UNIDADE_ORGANIZACIONAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	@Override
	public String getNomeFoneFormatado() {

		StringBuilder nomeFone = new StringBuilder();
		if(!StringUtils.isEmpty(this.getNome())) {
			nomeFone.append(this.getNome());
		}
		if(!StringUtils.isEmpty(this.getFone())) {
			if(nomeFone.length() > 0) {
				nomeFone.append(" - ");
			}
			nomeFone.append(this.getFone());
		}

		return nomeFone.toString();
	}

	@Override
	public Boolean getIndicadorFiscal() {

		return indicadorFiscal;
	}

	@Override
	public void setIndicadorFiscal(Boolean indicadorFiscal) {

		this.indicadorFiscal = indicadorFiscal;
	}

	@Override
	public Boolean getIndicadorVendedor() {

		return indicadorVendedor;
	}

	@Override
	public void setIndicadorVendedor(Boolean indicadorVendedor) {

		this.indicadorVendedor = indicadorVendedor;
	}

	@Override
	public byte[] getFotoFuncionario() {
		return this.fotoFuncionario;
	}

	@Override
	public void setFotoFuncionario(byte[] fotoFuncionario) {
		this.fotoFuncionario = fotoFuncionario;	
	}
	
	@Override
	public String[] getExtensoesArquivoFotoFuncionario() {
		
		return EXTENSOES_ARQUIVO_FOTO_FUNCIONARIO;
	}

	@Override
	public Collection<FuncionarioAfastamento> getListaAfastamento() {
		return listaAfastamento;
	}

	@Override
	public void setListaAfastamento(Collection<FuncionarioAfastamento> listaAfastamento) {
		this.listaAfastamento = listaAfastamento;
	}

}
