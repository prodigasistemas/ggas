/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.funcionario.impl;

import java.util.Date;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.FuncionarioAfastamento;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.DataUtil;


/**
 * Classe Funcionario Afastamento
 * 
 * @author Prodiga
 */
public class FuncionarioAfastamentoImpl extends EntidadeNegocioImpl implements FuncionarioAfastamento{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4202364966672674991L;
	
	private Date dataInicioAfastamento;
	private Date dataFimAfastamento;
	private String observacao;
	private EntidadeConteudo motivoAfastamento;
	private Funcionario funcionario;
	
	@Override
	public Date getDataInicioAfastamento() {
		return dataInicioAfastamento;
	}

	@Override
	public void setDataInicioAfastamento(Date dataInicioAfastamento) {
		this.dataInicioAfastamento = dataInicioAfastamento;
	}

	@Override
	public Date getDataFimAfastamento() {
		return dataFimAfastamento;
	}

	@Override
	public void setDataFimAfastamento(Date dataFimAfastamento) {
		this.dataFimAfastamento = dataFimAfastamento;
	}

	@Override
	public String getObservacao() {
		return observacao;
	}

	@Override
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	@Override
	public EntidadeConteudo getMotivoAfastamento() {
		return motivoAfastamento;
	}

	@Override
	public void setMotivoAfastamento(EntidadeConteudo motivoAfastamento) {
		this.motivoAfastamento = motivoAfastamento;
	}

	@Override
	public Map<String, Object> validarDados() {
		return null;
	}
	
	
	@Override
	public String getAfastamentoJSON() throws JSONException {
		JSONObject json = new JSONObject();

		json.put("motivoAfastamento", this.getMotivoAfastamento().getChavePrimaria());
		json.put("dataInicio", this.getDataInicioAfastamento() != null ? DataUtil.converterDataParaString(this.getDataInicioAfastamento()) : "");
		json.put("dataFim", this.getDataFimAfastamento() != null ? DataUtil.converterDataParaString(this.getDataFimAfastamento()) : "");
		json.put("observacao", this.getObservacao());

		return json.toString();
	}

	@Override
	public Funcionario getFuncionario() {
		return funcionario;
	}

	@Override
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

}
