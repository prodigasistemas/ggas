/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.funcionario;

import java.util.Collection;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface Funcionario
 * 
 * @author Procenge
 */
public interface Funcionario extends EntidadeNegocio {

	String BEAN_ID_FUNCIONARIO = "funcionario";

	String FUNCIONARIO_CAMPO_STRING = "Funcionário";

	String MATRICULA = "Matrícula";

	String DESCRICAO_CARGO = "Descrição do cargo";

	String EMPRESA = "Empresa";

	String UNIDADE_ORGANIZACIONAL = "Unidade Organizacional";

	String NOME = "Nome";

	String EMAIL = "Email";

	String CODIGODDD = "Código DDD";

	String FONE = "FUNCIONARIO_FONE";
	
	/**
	 * Extensoes de arquivo aceitas para a logo da empresa
	 */
	String[] EXTENSOES_ARQUIVO_FOTO_FUNCIONARIO = {".PNG"};

	/**
	 * Tamanho do arquivo maximo para a logo da empresa
	 */
	int TAMANHO_MAXIMO_ARQUIVO_FOTO_FUNCIONARIO = 204800;

	/**
	 * @return the descricaoCargo
	 */
	String getDescricaoCargo();

	/**
	 * @param descricaoCargo
	 *            the descricaoCargo to set
	 */
	void setDescricaoCargo(String descricaoCargo);

	/**
	 * @return the nome
	 */
	String getNome();

	/**
	 * @param nome
	 *            the nome to set
	 */
	void setNome(String nome);

	/**
	 * @return the matricula
	 */
	String getMatricula();

	/**
	 * @param matricula
	 *            the matricula to set
	 */
	void setMatricula(String matricula);

	/**
	 * @return the empresa
	 */
	Empresa getEmpresa();

	/**
	 * @param empresa
	 *            the empresa to set
	 */
	void setEmpresa(Empresa empresa);

	/**
	 * @return the unidadeOrganizacional
	 */
	UnidadeOrganizacional getUnidadeOrganizacional();

	/**
	 * @param unidadeOrganizacional
	 *            the unidadeOrganizacional to set
	 */
	void setUnidadeOrganizacional(UnidadeOrganizacional unidadeOrganizacional);

	/**
	 * @return the email
	 */
	String getEmail();

	/**
	 * @param email
	 *            the email to set
	 */
	void setEmail(String email);

	/**
	 * @return the codigoDDD
	 */
	Integer getCodigoDDD();

	/**
	 * @param codigoDDD
	 *            the codigoDDD to set
	 */
	void setCodigoDDD(Integer codigoDDD);

	/**
	 * @return the fone
	 */
	String getFone();

	/**
	 * @param fone
	 *            the fone to set
	 */
	void setFone(String fone);

	/**
	 * @return O número do fone formatado
	 */
	String getNomeFoneFormatado();

	/**
	 * @return Usuario - Retorna Um Usuário.
	 */
	Usuario getUsuario();

	/**
	 * @param usuario - Set usuario.
	 */
	void setUsuario(Usuario usuario);

	/**
	 * @return
	 */
	Boolean getIndicadorFiscal();

	/**
	 * @param indicadorFiscal
	 */
	void setIndicadorFiscal(Boolean indicadorFiscal);

	/**
	 * @return
	 */
	Boolean getIndicadorVendedor();

	/**
	 * @param indicadorVendedor
	 */
	void setIndicadorVendedor(Boolean indicadorVendedor);
	
	/**
	 * @return the logoEmpresa
	 */
	byte[] getFotoFuncionario();

	/**
	 * @param logoEmpresa
	 *            the logoEmpresa to sete
	 */
	void setFotoFuncionario(byte[] fotoFuncionario);

	/**
	 * @return
	 */
	String[] getExtensoesArquivoFotoFuncionario();

	/**
	 * @return
	 */
	Collection<FuncionarioAfastamento> getListaAfastamento();

	/**
	 * @param listaAfastamento
	 */
	void setListaAfastamento(Collection<FuncionarioAfastamento> listaAfastamento);

}
