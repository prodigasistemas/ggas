/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.funcionario.impl;

import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.FuncionarioAfastamento;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Classe ControladorFuncionarioImpl
 * 
 * @author Procenge
 */
public class ControladorFuncionarioImpl extends ControladorNegocioImpl implements ControladorFuncionario {

	private static final String UNIDADE_ORGANIZACIONAL = "unidadeOrganizacional";
	private static final String EMPRESA = "empresa";
	private static final String MATRICULA = "matricula";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Funcionario.BEAN_ID_FUNCIONARIO);
	}
	
	
	@Override
	public EntidadeNegocio criarFuncionarioAfastamento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(FuncionarioAfastamento.BEAN_ID_FUNCIONARIO_AFASTAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Funcionario.BEAN_ID_FUNCIONARIO);
	}

	public Class<?> getClasseEntidadeLeiturista() {

		return ServiceLocator.getInstancia().getClassPorID(Leiturista.BEAN_ID_LEITURISTA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.
	 * ControladorFuncionario
	 * #consultarFuncionarios(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Funcionario> consultarFuncionarios(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");

			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String matricula = (String) filtro.get(MATRICULA);
			if(matricula != null && !matricula.isEmpty()) {
				criteria.add(Restrictions.like(MATRICULA, matricula + "%"));
			}

			String nome = (String) filtro.get("nome");
			if(!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.ilike("nome", Util.formatarTextoConsulta(nome)));
			}

			String descricaoCargo = (String) filtro.get("descricaoCargo");
			if(!StringUtils.isEmpty(descricaoCargo)) {
				criteria.add(Restrictions.ilike("descricaoCargo", Util.formatarTextoConsulta(descricaoCargo)));
			}

			Long idEmpresa = (Long) filtro.get("idEmpresa");
			if(idEmpresa != null && idEmpresa > 0) {
				criteria.createAlias(EMPRESA, EMPRESA);
				criteria.add(Restrictions.eq("empresa.chavePrimaria", idEmpresa));
			} else {
				criteria.setFetchMode(EMPRESA, FetchMode.JOIN);
			}

			Long idUnidadeOrganizacional = (Long) filtro.get("idUnidadeOrganizacional");
			if(idUnidadeOrganizacional != null && idUnidadeOrganizacional > 0) {
				criteria.createAlias(UNIDADE_ORGANIZACIONAL, UNIDADE_ORGANIZACIONAL);
				criteria.add(Restrictions.eq("unidadeOrganizacional.chavePrimaria", idUnidadeOrganizacional));
			} else {
				criteria.setFetchMode(UNIDADE_ORGANIZACIONAL, FetchMode.JOIN);
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			criteria.addOrder(Order.asc("nome"));

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.
	 * ControladorFuncionario
	 * #listarFuncionariosPorEmpresa
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings({"squid:S1192", "unchecked"})
	public Collection<Funcionario> listarFuncionariosEmpresaPrincipal() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select ");
		hql.append(" funcionario ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" funcionario ");
		hql.append(" inner join ");
		hql.append(" funcionario.empresa as empresa with empresa.principal is true ");
		hql.append(" order by funcionario.nome ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Funcionario funcionario = (Funcionario) entidadeNegocio;
		validarMatriculaExistente(funcionario);
		verificarEmailValido(funcionario.getEmail());
	}

	/**
	 * Verificar email valido.
	 * 
	 * @param email
	 *            the email
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarEmailValido(String email) throws NegocioException {

		if(!StringUtils.isEmpty(email) && !Util.validarDominio(email.trim(), Constantes.EXPRESSAO_REGULAR_EMAIL)) {

			throw new NegocioException(ERRO_NEGOCIO_EMAIL_INVALIDO, Funcionario.EMAIL);
		}
	}

	/**
	 * Consultar funcionario por matricula.
	 * 
	 * @param matricula
	 *            the matricula
	 * @return the funcionario
	 */
	private Funcionario consultarFuncionarioPorMatricula(String matricula) {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" funcionario ");
		hql.append(" where ");
		hql.append(" funcionario.matricula = :matricula ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter(MATRICULA, matricula);

		return (Funcionario) query.uniqueResult();

	}

	/**
	 * Validar matricula existente.
	 * 
	 * @param funcionario
	 *            the funcionario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarMatriculaExistente(Funcionario funcionario) throws NegocioException {

		Long chavePrimaria = funcionario.getChavePrimaria();
		Funcionario funcionarioConsulta = this.consultarFuncionarioPorMatricula(funcionario.getMatricula());

		if(funcionarioConsulta != null && funcionarioConsulta.getChavePrimaria() != chavePrimaria) {

			throw new NegocioException(ControladorFuncionario.ERRO_NEGOCIO_MATRICULA_EXISTENTE, funcionario.getMatricula());
		}
		getHibernateTemplate().getSessionFactory().getCurrentSession().evict(funcionarioConsulta);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Funcionario funcionario = (Funcionario) entidadeNegocio;
		validarInativacaoFuncionario(funcionario);
		validarMatriculaExistente(funcionario);
		verificarEmailValido(funcionario.getEmail());
	}

	/**
	 * Validar inativacao funcionario.
	 * 
	 * @param funcionario
	 *            the funcionario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarInativacaoFuncionario(Funcionario funcionario) throws NegocioException {

		if(!funcionario.isHabilitado()) {

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" select count(chavePrimaria) from ");
			hql.append(getClasseEntidadeLeiturista().getSimpleName());
			hql.append(" where funcionario.chavePrimaria = :idFuncionario ");
			hql.append(" and funcionario.habilitado = true ");
			hql.append(" and habilitado = true ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong("idFuncionario", funcionario.getChavePrimaria());

			Long quantidade = (Long) query.uniqueResult();

			if(quantidade != null && quantidade > 0) {
				throw new NegocioException(ERRO_NEGOCIO_FUNCIONARIO_ASSOCIADO_LEITURISTA, true);
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.
	 * ControladorFuncionario
	 * #validarRemoverFuncionarios
	 * (java.lang.Long[])
	 */
	@Override
	public void validarRemoverFuncionarios(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.ControladorFuncionario#exigeMatriculaFuncionario()
	 */
	@Override
	public boolean exigeMatriculaFuncionario() throws NegocioException {

		boolean retorno = false;

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ParametroSistema parametroExigeMatriculaFuncionario = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_EXIGE_MATRICULA_FUNCIONARIO);

		if(parametroExigeMatriculaFuncionario.getValor() != null 
				&& Integer.parseInt(parametroExigeMatriculaFuncionario.getValor()) > 0) {
			retorno = true;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.ControladorFuncionario#verificarOrigatoriedadeMatricula(java.lang.String)
	 */
	@Override
	public void verificarOrigatoriedadeMatricula(String matricula) throws NegocioException {

		boolean exigeMatricula = this.exigeMatriculaFuncionario();

		if(matricula != null && matricula.isEmpty()) {
			throw new NegocioException(Constantes.FUNCIONARIO_ERRO_MATRICULA, Funcionario.MATRICULA);
		}

		if(exigeMatricula && (matricula == null || matricula.isEmpty())) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, Funcionario.MATRICULA);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.ControladorFuncionario#listarFuncionarioUnidadeOrganizacional()
	 */
	@Override
	public Collection<Funcionario> listarFuncionarioUnidadeOrganizacional() throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.setFetchMode(UNIDADE_ORGANIZACIONAL, FetchMode.JOIN);
		criteria.addOrder(Order.asc("nome"));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.ControladorFuncionario#listarFuncionarioPorUnidadeOrganizacional(java.lang.Long)
	 */
	@Override
	public Collection<Funcionario> listarFuncionarioPorUnidadeOrganizacional(Long chaveUnidadeOrganizacional) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" funcionario ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" funcionario ");
		hql.append(" inner join funcionario.unidadeOrganizacional unidadeOrganizacional ");
		hql.append(" inner join funcionario.usuario usuario ");
		hql.append(" where funcionario.habilitado = true ");
		if(chaveUnidadeOrganizacional != null) {
			hql.append(" and unidadeOrganizacional.chavePrimaria = :chaveUnidadeOrganizacional ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(chaveUnidadeOrganizacional != null) {
			query.setParameter("chaveUnidadeOrganizacional", chaveUnidadeOrganizacional);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.ControladorFuncionario#listarFuncionarioFiscalVendedor(boolean, boolean)
	 */
	@Override
	public Collection<Funcionario> listarFuncionarioFiscalVendedor(boolean isFuncionarioFiscal, boolean isFuncionarioVendedor)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" funcionario ");
		hql.append(" where funcionario.habilitado = true ");

		if(isFuncionarioFiscal) {
			hql.append(" and funcionario.indicadorFiscal = :indicadorFiscal ");
		}

		if(isFuncionarioVendedor) {
			hql.append(" and funcionario.indicadorVendedor = :indicadorVendedor ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(isFuncionarioFiscal) {
			query.setParameter("indicadorFiscal", isFuncionarioFiscal);
		}

		if(isFuncionarioVendedor) {
			query.setParameter("indicadorVendedor", isFuncionarioVendedor);
		}

		return query.list();
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.ControladorFuncionario#listarVendedores()
	 */
	@Override
	public Collection<Funcionario> listarVendedores() throws NegocioException {
		return FluentIterable
			.from(listarFuncionarioPorUnidadeOrganizacional(null))
			.filter(new Predicate<Funcionario>() {
				@Override
				public boolean apply(Funcionario funcionario) {
					return funcionario.getIndicadorVendedor();
				}
			}).toList();
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.cadastro.funcionario.ControladorFuncionario#listarFiscais()
	 */
	@Override
	public Collection<Funcionario> listarFiscais() throws NegocioException {
		return FluentIterable
			.from(listarFuncionarioPorUnidadeOrganizacional(null))
			.filter(new Predicate<Funcionario>() {
				@Override
				public boolean apply(Funcionario funcionario) {
					return funcionario.getIndicadorFiscal();
				}
			}).toList();
	}


	@Override
	public void removerListaAfastamento(List<Long> chavesAfastamento) throws NegocioException {

		if (chavesAfastamento != null && !chavesAfastamento.isEmpty()) {
			for (Long chaveAfastamento : chavesAfastamento) {
				FuncionarioAfastamento funcionarioAfastamento = (FuncionarioAfastamento) this.obter(chaveAfastamento,
						FuncionarioAfastamentoImpl.class);
				this.remover(funcionarioAfastamento);
			}
		}

	}
}
