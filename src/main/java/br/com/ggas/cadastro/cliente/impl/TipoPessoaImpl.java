/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.cliente.impl;

import br.com.ggas.cadastro.cliente.TipoPessoa;
import br.com.ggas.geral.dominio.impl.EntidadeDominioImpl;

/**
 * Classe responsável por representar o tipo de pessoa
 *
 */
public class TipoPessoaImpl extends EntidadeDominioImpl implements TipoPessoa {

	/**
     * 
     */
	private static final long serialVersionUID = 3691848015013728551L;

	/**
	 * Instantiates a new tipo pessoa impl.
	 */
	public TipoPessoaImpl() {

		super();
	}

	/**
	 * Instantiates a new tipo pessoa impl.
	 * 
	 * @param codigo
	 *            the codigo
	 */
	public TipoPessoaImpl(int codigo) {

		super();
		this.setCodigo(codigo);
		// by gmatos
		// on 15/10/09
		// 10:19
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.dominio.impl.
	 * EntidadeDominioImpl#setCodigo(int)
	 */
	@Override
	public void setCodigo(int codigo) {

		super.setCodigo(codigo);

		if(codigo == PESSOA_FISICA) {
			super.setDescricao(DESCRICAO_PESSOA_FISICA);
		} else if(codigo == PESSOA_JURIDICA) {
			super.setDescricao(DESCRICAO_PESSOA_JURIDICA);
		}
	}

}
