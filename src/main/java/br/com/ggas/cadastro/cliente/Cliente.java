/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.cliente;

import java.util.Collection;
import java.util.Date;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface Cliente.
 */
public interface Cliente extends EntidadeNegocio {

	/** The cliente data envio brindes. */
	String CLIENTE_DATA_ENVIO_BRINDES = "CLIENTE_DATA_ENVIO_BRINDES";
	
	/** The bean id cliente. */
	String BEAN_ID_CLIENTE = "cliente";

	/** The cliente. */
	String CLIENTE_CAMPO_STRING = "Cliente";

	/** The clientes. */
	String CLIENTES = "Pessoa(s)";

	/** The nome. */
	String NOME = "Nome";

	/** The razao social. */
	String RAZAO_SOCIAL = "Razão Social";

	/** The cpf. */
	String CPF = "CPF";

	/** The rg. */
	String RG = "RG";

	/** The data emissao rg. */
	String DATA_EMISSAO_RG = "Emissão RG";

	/** The data nascimento. */
	String DATA_NASCIMENTO = "Data de Nascimento";

	/** The cnpj. */
	String CNPJ = "CNPJ";

	/** The tipo cliente. */
	String TIPO_CLIENTE = "Tipo de Cliente";

	/** The passaporte. */
	String PASSAPORTE = "Passaporte";

	/** The sexo. */
	String SEXO = "Sexo";

	/** The inscricao estadual. */
	String INSCRICAO_ESTADUAL = "Inscrição estadual";

	/** The inscricao municipal. */
	String INSCRICAO_MUNICIPAL = "Inscrição Municipal";

	/** The inscricao rural. */
	String INSCRICAO_RURAL = "Inscrição Rural";

	/** The nome fantasia. */
	String NOME_FANTASIA = "Nome Fantasia";

	/** The profissao. */
	String PROFISSAO = "Profissão";

	/** The nome mae. */
	String NOME_MAE = "Nome da Mãe";

	/** The nome pai. */
	String NOME_PAI = "Nome do Pai";

	/** The renda familiar. */
	String RENDA_FAMILIAR = "Renda Familiar";

	/** The nacionalidade. */
	String NACIONALIDADE = "Nacionalidade";

	/** The orgao expedidor. */
	String ORGAO_EXPEDIDOR = "Orgão Expeditor";

	/** The unidade federacao. */
	String UNIDADE_FEDERACAO = "Unidade Federação";

	/** The atividade economica. */
	String ATIVIDADE_ECONOMICA = "Atividade Econômica";

	/** The email principal. */
	String EMAIL_PRINCIPAL = "Email Principal";

	/** The email secundario. */
	String EMAIL_SECUNDARIO = "Email Secundário";

	/** The propriedades lazy. */
	String[] PROPRIEDADES_LAZY = new String[] {"fones", "contatos", "enderecos", "anexos", "funcionarioFiscal", "funcionarioVendedor"};

	/** The grupo economico. */
	String GRUPO_ECONOMICO = "Grupo Econômico";

	/** The senha. */
	String SENHA = "CLIENTE_SENHA";
	
	/** Cliente. */
	String PESSOA = "Pessoa";
	
	/** Segmento */
	String SEGMENTO = "Segmento";

	/**
	 * Gets the data ultimo acesso.
	 *
	 * @return the data ultimo acesso
	 */
	Date getDataUltimoAcesso();

	/**
	 * Sets the data ultimo acesso.
	 *
	 * @param dataUltimoAcesso the new data ultimo acesso
	 */
	void setDataUltimoAcesso(Date dataUltimoAcesso);

	/**
	 * Gets the cliente publico.
	 *
	 * @return the cliente publico
	 */
	Boolean getClientePublico();

	/**
	 * Sets the cliente publico.
	 *
	 * @param clientePublico the new cliente publico
	 */
	void setClientePublico(Boolean clientePublico);

	/**
	 * Gets the nome.
	 *
	 * @return the nome
	 */
	String getNome();

	/**
	 * Sets the nome.
	 *
	 * @param nome            the nome to set
	 */
	void setNome(String nome);

	/**
	 * Gets the nome abreviado.
	 *
	 * @return the nomeAbreviado
	 */
	String getNomeAbreviado();

	/**
	 * Sets the nome abreviado.
	 *
	 * @param nomeAbreviado            the nomeAbreviado to set
	 */
	void setNomeAbreviado(String nomeAbreviado);

	/**
	 * Gets the email principal.
	 *
	 * @return the emailPrincipal
	 */
	String getEmailPrincipal();

	/**
	 * Sets the email principal.
	 *
	 * @param emailPrincipal            the emailPrincipal to set
	 */
	void setEmailPrincipal(String emailPrincipal);

	/**
	 * Gets the email secundario.
	 *
	 * @return the emailSecundario
	 */
	String getEmailSecundario();

	/**
	 * Sets the email secundario.
	 *
	 * @param emailSecundario            the emailSecundario to set
	 */
	void setEmailSecundario(String emailSecundario);

	/**
	 * Gets the cpf.
	 *
	 * @return the cpf
	 */
	String getCpf();

	/**
	 * Sets the cpf.
	 *
	 * @param cpf            the cpf to set
	 */
	void setCpf(String cpf);

	/**
	 * Gets the rg.
	 *
	 * @return the rg
	 */
	String getRg();

	/**
	 * Sets the rg.
	 *
	 * @param rg            the rg to set
	 */
	void setRg(String rg);

	/**
	 * Gets the data emissao rg.
	 *
	 * @return the dataEmissaoRG
	 */
	Date getDataEmissaoRG();

	/**
	 * Sets the data emissao rg.
	 *
	 * @param dataEmissaoRG            the dataEmissaoRG to set
	 */
	void setDataEmissaoRG(Date dataEmissaoRG);

	/**
	 * Gets the numero passaporte.
	 *
	 * @return the numeroPassaporte
	 */
	String getNumeroPassaporte();

	/**
	 * Sets the numero passaporte.
	 *
	 * @param numeroPassaporte            the numeroPassaporte to set
	 */
	void setNumeroPassaporte(String numeroPassaporte);

	/**
	 * Gets the data nascimento.
	 *
	 * @return the dataNascimento
	 */
	Date getDataNascimento();

	/**
	 * Sets the data nascimento.
	 *
	 * @param dataNascimento            the dataNascimento to set
	 */
	void setDataNascimento(Date dataNascimento);

	/**
	 * Gets the nome mae.
	 *
	 * @return the nomeMae
	 */
	String getNomeMae();

	/**
	 * Sets the nome mae.
	 *
	 * @param nomeMae            the nomeMae to set
	 */
	void setNomeMae(String nomeMae);

	/**
	 * Gets the nome pai.
	 *
	 * @return the nomePai
	 */
	String getNomePai();

	/**
	 * Sets the nome pai.
	 *
	 * @param nomePai            the nomePai to set
	 */
	void setNomePai(String nomePai);

	/**
	 * Gets the nome fantasia.
	 *
	 * @return the nomeFantasia
	 */
	String getNomeFantasia();

	/**
	 * Sets the nome fantasia.
	 *
	 * @param nomeFantasia            the nomeFantasia to set
	 */
	void setNomeFantasia(String nomeFantasia);

	/**
	 * Gets the cnpj.
	 *
	 * @return the cnpj
	 */
	String getCnpj();

	/**
	 * Gets the cnpj formatado.
	 *
	 * @return the cnpj formatado
	 */
	String getCnpjFormatado();

	/**
	 * Sets the cnpj.
	 *
	 * @param cnpj            the cnpj to set
	 */
	void setCnpj(String cnpj);

	/**
	 * Gets the inscricao estadual.
	 *
	 * @return the inscricaoEstadual
	 */
	String getInscricaoEstadual();

	/**
	 * Sets the inscricao estadual.
	 *
	 * @param inscricaoEstadual            the inscricaoEstadual to set
	 */
	void setInscricaoEstadual(String inscricaoEstadual);

	/**
	 * Gets the inscricao municipal.
	 *
	 * @return the inscricaoMunicipal
	 */
	String getInscricaoMunicipal();

	/**
	 * Sets the inscricao municipal.
	 *
	 * @param inscricaoMunicipal            the inscricaoMunicipal to set
	 */
	void setInscricaoMunicipal(String inscricaoMunicipal);

	/**
	 * Gets the inscricao rural.
	 *
	 * @return the inscricaoRural
	 */
	String getInscricaoRural();

	/**
	 * Sets the inscricao rural.
	 *
	 * @param inscricaoRural            the inscricaoRural to set
	 */
	void setInscricaoRural(String inscricaoRural);

	/**
	 * Gets the cliente situacao.
	 *
	 * @return the clienteSituacao
	 */
	ClienteSituacao getClienteSituacao();

	/**
	 * Sets the cliente situacao.
	 *
	 * @param clienteSituacao            the clienteSituacao to set
	 */
	void setClienteSituacao(ClienteSituacao clienteSituacao);

	/**
	 * Gets the tipo cliente.
	 *
	 * @return the tipoCliente
	 */
	TipoCliente getTipoCliente();

	/**
	 * Sets the tipo cliente.
	 *
	 * @param tipoCliente            the tipoCliente to set
	 */
	void setTipoCliente(TipoCliente tipoCliente);

	/**
	 * Gets the orgao expedidor.
	 *
	 * @return the orgaoExpedidor
	 */
	OrgaoExpedidor getOrgaoExpedidor();

	/**
	 * Sets the orgao expedidor.
	 *
	 * @param orgaoExpedidor            the orgaoExpedidor to set
	 */
	void setOrgaoExpedidor(OrgaoExpedidor orgaoExpedidor);

	/**
	 * Gets the unidade federacao.
	 *
	 * @return the unidadeFederacao
	 */
	UnidadeFederacao getUnidadeFederacao();

	/**
	 * Sets the unidade federacao.
	 *
	 * @param unidadeFederacao            the unidadeFederacao to set
	 */
	void setUnidadeFederacao(UnidadeFederacao unidadeFederacao);

	/**
	 * Gets the nacionalidade.
	 *
	 * @return the nacionalidade
	 */
	Nacionalidade getNacionalidade();

	/**
	 * Sets the nacionalidade.
	 *
	 * @param nacionalidade            the nacionalidade to set
	 */
	void setNacionalidade(Nacionalidade nacionalidade);

	/**
	 * Gets the profissao.
	 *
	 * @return the profissao
	 */
	Profissao getProfissao();

	/**
	 * Sets the profissao.
	 *
	 * @param profissao            the profissao to set
	 */
	void setProfissao(Profissao profissao);

	/**
	 * Gets the sexo.
	 *
	 * @return the sexo
	 */
	PessoaSexo getSexo();

	/**
	 * Sets the sexo.
	 *
	 * @param sexo            the sexo to set
	 */
	void setSexo(PessoaSexo sexo);

	/**
	 * Gets the renda familiar.
	 *
	 * @return the rendaFamiliar
	 */
	FaixaRendaFamiliar getRendaFamiliar();

	/**
	 * Sets the renda familiar.
	 *
	 * @param rendaFamiliar            the rendaFamiliar to set
	 */
	void setRendaFamiliar(FaixaRendaFamiliar rendaFamiliar);

	/**
	 * Gets the atividade economica.
	 *
	 * @return the atividadeEconomica
	 */
	AtividadeEconomica getAtividadeEconomica();

	/**
	 * Sets the atividade economica.
	 *
	 * @param atividadeEconomica            the atividadeEconomica to set
	 */
	void setAtividadeEconomica(AtividadeEconomica atividadeEconomica);

	/**
	 * Gets the enderecos.
	 *
	 * @return the enderecos
	 */
	Collection<ClienteEndereco> getEnderecos();

	/**
	 * Sets the enderecos.
	 *
	 * @param enderecos            the enderecos to set
	 */
	void setEnderecos(Collection<ClienteEndereco> enderecos);
	
	/**
	 * Sets the enderecos.
	 *
	 * @param endereco the new enderecos
	 */
	void setEnderecos(ClienteEndereco endereco);
	
	/**
	 * Gets the fones.
	 *
	 * @return the fones
	 */
	Collection<ClienteFone> getFones();
	
	/**
	 * Sets the fones.
	 *
	 * @param fones            the fones to set
	 */
	void setFones(Collection<ClienteFone> fones);

	/**
	 * Gets the contatos.
	 *
	 * @return the contatos
	 */
	Collection<ContatoCliente> getContatos();

	/**
	 * Sets the contatos.
	 *
	 * @param contatos            the contatos to set
	 */
	void setContatos(Collection<ContatoCliente> contatos);

	/**
	 * Gets the cliente responsavel.
	 *
	 * @return the clienteResponsavel
	 */
	Cliente getClienteResponsavel();

	/**
	 * Sets the cliente responsavel.
	 *
	 * @param clienteResponsavel            the clienteResponsavel to set
	 */
	void setClienteResponsavel(Cliente clienteResponsavel);

	/**
	 * Gets the cpf formatado.
	 *
	 * @return the cpf formatado
	 */
	String getCpfFormatado();

	/**
	 * the cpf ou cnpj.
	 *
	 * @return the numero cpf cnpj
	 */
	String getNumeroCpfCnpj();

	/**
	 * Gets the endereco principal.
	 *
	 * @return Endereço principal do cliente
	 */
	ClienteEndereco getEnderecoPrincipal();

	/**
	 * Gets the numero documento formatado.
	 *
	 * @return Número do documento do cliente
	 *         formatado.
	 */
	String getNumeroDocumentoFormatado();

	/**
	 * Gets the regime recolhimento.
	 *
	 * @return the regimeRecolhimento
	 */
	String getRegimeRecolhimento();

	/**
	 * Sets the regime recolhimento.
	 *
	 * @param regimeRecolhimento            the regimeRecolhimento to set
	 */
	void setRegimeRecolhimento(String regimeRecolhimento);

	/**
	 * Gets the conta auxiliar.
	 *
	 * @return the conta auxiliar
	 */
	String getContaAuxiliar();

	/**
	 * Sets the conta auxiliar.
	 *
	 * @param contaAuxiliar the new conta auxiliar
	 */
	void setContaAuxiliar(String contaAuxiliar);

	/**
	 * Gets the grupo economico.
	 *
	 * @return the grupo economico
	 */
	GrupoEconomico getGrupoEconomico();

	/**
	 * Sets the grupo economico.
	 *
	 * @param grupoEconomico the new grupo economico
	 */
	void setGrupoEconomico(GrupoEconomico grupoEconomico);

	/**
	 * Gets the anexos.
	 *
	 * @return the anexos
	 */
	Collection<ClienteAnexo> getAnexos();

	/**
	 * Sets the anexos.
	 *
	 * @param anexos the new anexos
	 */
	void setAnexos(Collection<ClienteAnexo> anexos);

	/**
	 * Checks if is indicador denegado.
	 *
	 * @return true, if is indicador denegado
	 */
	public boolean isIndicadorDenegado();

	/**
	 * Sets the indicador denegado.
	 *
	 * @param indicadorDenegado the new indicador denegado
	 */
	public void setIndicadorDenegado(boolean indicadorDenegado);

	/**
	 * Gets the senha.
	 *
	 * @return the senha
	 */
	String getSenha();

	/**
	 * Sets the senha.
	 *
	 * @param senha the new senha
	 */
	void setSenha(String senha);

	/**
	 * Gets the data envio brindes.
	 *
	 * @return the data envio brindes
	 */
	Date getDataEnvioBrindes();

	/**
	 * Sets the data envio brindes.
	 *
	 * @param dataEnvioBrindes the new data envio brindes
	 */
	void setDataEnvioBrindes(Date dataEnvioBrindes);

	/**
	 * Gets the funcionario vendedor.
	 *
	 * @return the funcionario vendedor
	 */
	Funcionario getFuncionarioVendedor();

	/**
	 * Sets the funcionario vendedor.
	 *
	 * @param funcionarioVendedor the new funcionario vendedor
	 */
	void setFuncionarioVendedor(Funcionario funcionarioVendedor);

	/**
	 * Gets the funcionario fiscal.
	 *
	 * @return the funcionario fiscal
	 */
	Funcionario getFuncionarioFiscal();

	/**
	 * Sets the funcionario fiscal.
	 *
	 * @param funcionarioFiscal the new funcionario fiscal
	 */
	void setFuncionarioFiscal(Funcionario funcionarioFiscal);

	/**
	 * Gets the segmento.
	 *
	 * @return the segmento
	 */
	Segmento getSegmento();

	/**
	 * Sets the segmento.
	 *
	 * @param segmento the new segmento
	 */
	void setSegmento(Segmento segmento);
	
	boolean isPessoaFisica();
	
	boolean isPessoaJuridica();
	
	/**
	 * Gets the indicadorNegativado.
	 *
	 * @return the indicadorNegativado
	 */
	boolean isIndicadorNegativado();

	/**
	 * Sets the indicadorNegativado.
	 *
	 * @param indicadorNegativado the new indicadorNegativado
	 */
	void setIndicadorNegativado(boolean indicadorNegativado);

	String retornarTelefonePrincipal();
	
}
