/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 24/01/2014 14:46:59
 @author ccavalcanti
 */

package br.com.ggas.cadastro.cliente.dominio;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.cadastro.cliente.AtividadeEconomica;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.FaixaRendaFamiliar;
import br.com.ggas.cadastro.cliente.Nacionalidade;
import br.com.ggas.cadastro.cliente.OrgaoExpedidor;
import br.com.ggas.cadastro.cliente.PessoaSexo;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoCliente;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pela representação da alteração de cliente no chamado
 */
public class ChamadoAlteracaoCliente extends EntidadeNegocioImpl {

	private static final long serialVersionUID = 5794659622398257880L;

	private Cliente cliente;

	private Chamado chamado;

	private TipoCliente tipoCliente;

	private OrgaoExpedidor orgaoExpedidor;

	private Nacionalidade nacionalidade;

	private Profissao profissao;

	private PessoaSexo sexo;

	private UnidadeFederacao unidadeFederacao;

	private FaixaRendaFamiliar rendaFamiliar;

	private AtividadeEconomica atividadeEconomica;

	private GrupoEconomico grupoEconomico;

	private String nome;

	private String nomeAbreviado;

	private String cpf;

	private String cnpj;

	private String emailPrincipal;

	private String emailSecundario;

	private String nomeFantasia;

	private String inscricaoEstadual;

	private String inscricaoMunicipal;

	private String inscricaoRural;

	private String rg;

	private String dataEmissaoRGString;

	private String dataEmissaoRGClienteString;

	private Date dataEmissaoRG;

	private String numeroPassaporte;

	private String dataNascimentoString;

	private String dataNascimentoClienteString;

	private Date dataNascimento;

	private String nomeMae;

	private String nomePai;

	private String regimeRecolhimento;

	private ChamadoAlteracaoClienteEndereco chamadoAlteracaoClienteEndereco;

	private Collection<ChamadoAlteracaoClienteEndereco> enderecosChamados = new HashSet<>();

	private Collection<ChamadoAlteracaoClienteTelefone> fonesChamados = new HashSet<>();

	private Collection<ChamadoAlteracaoClienteContato> contatosChamados = new HashSet<>();

	private Collection<ChamadoClienteAnexo> anexosChamados = new HashSet<>();

	public static final String[] PROPRIEDADES_LAZY = new String[] {"fonesChamados", "contatosChamados", "enderecosChamados", "anexosChamados"};

	public Collection<ChamadoClienteAnexo> getAnexosChamados() {

		return anexosChamados;
	}

	public void setAnexosChamados(Collection<ChamadoClienteAnexo> anexosChamados) {

		this.anexosChamados = anexosChamados;
	}

	public Collection<ChamadoAlteracaoClienteEndereco> getEnderecosChamados() {

		return enderecosChamados;
	}

	public void setEnderecosChamados(Collection<ChamadoAlteracaoClienteEndereco> enderecosChamados) {

		this.enderecosChamados = enderecosChamados;
	}

	public Collection<ChamadoAlteracaoClienteTelefone> getFonesChamados() {

		return fonesChamados;
	}

	public void setFonesChamados(Collection<ChamadoAlteracaoClienteTelefone> fonesChamados) {

		this.fonesChamados = fonesChamados;
	}

	public Collection<ChamadoAlteracaoClienteContato> getContatosChamados() {

		return contatosChamados;
	}

	public void setContatosChamados(Collection<ChamadoAlteracaoClienteContato> contatosChamados) {

		this.contatosChamados = contatosChamados;
	}

	public Cliente getCliente() {

		return cliente;
	}

	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	public Chamado getChamado() {

		return chamado;
	}

	public void setChamado(Chamado chamado) {

		this.chamado = chamado;
	}

	public TipoCliente getTipoCliente() {

		return tipoCliente;
	}

	public void setTipoCliente(TipoCliente tipoCliente) {

		this.tipoCliente = tipoCliente;
	}

	public OrgaoExpedidor getOrgaoExpedidor() {

		return orgaoExpedidor;
	}

	public void setOrgaoExpedidor(OrgaoExpedidor orgaoExpedidor) {

		this.orgaoExpedidor = orgaoExpedidor;
	}

	public Nacionalidade getNacionalidade() {

		return nacionalidade;
	}

	public void setNacionalidade(Nacionalidade nacionalidade) {

		this.nacionalidade = nacionalidade;
	}

	public Profissao getProfissao() {

		return profissao;
	}

	public void setProfissao(Profissao profissao) {

		this.profissao = profissao;
	}

	public PessoaSexo getSexo() {

		return sexo;
	}

	public void setSexo(PessoaSexo sexo) {

		this.sexo = sexo;
	}

	public UnidadeFederacao getUnidadeFederacao() {

		return unidadeFederacao;
	}

	public void setUnidadeFederacao(UnidadeFederacao unidadeFederacao) {

		this.unidadeFederacao = unidadeFederacao;
	}

	public FaixaRendaFamiliar getRendaFamiliar() {

		return rendaFamiliar;
	}

	public void setRendaFamiliar(FaixaRendaFamiliar rendaFamiliar) {

		this.rendaFamiliar = rendaFamiliar;
	}

	public AtividadeEconomica getAtividadeEconomica() {

		return atividadeEconomica;
	}

	public void setAtividadeEconomica(AtividadeEconomica atividadeEconomica) {

		this.atividadeEconomica = atividadeEconomica;
	}

	public GrupoEconomico getGrupoEconomico() {

		return grupoEconomico;
	}

	public void setGrupoEconomico(GrupoEconomico grupoEconomico) {

		this.grupoEconomico = grupoEconomico;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public String getNomeAbreviado() {

		return nomeAbreviado;
	}

	public void setNomeAbreviado(String nomeAbreviado) {

		this.nomeAbreviado = nomeAbreviado;
	}

	public String getCpf() {

		return cpf;
	}

	public void setCpf(String cpf) {

		this.cpf = cpf;
	}

	public String getCnpj() {

		return cnpj;
	}

	public void setCnpj(String cnpj) {

		this.cnpj = cnpj;
	}

	public String getEmailPrincipal() {

		return emailPrincipal;
	}

	public void setEmailPrincipal(String emailPrincipal) {

		this.emailPrincipal = emailPrincipal;
	}

	public String getEmailSecundario() {

		return emailSecundario;
	}

	public void setEmailSecundario(String emailSecundario) {

		this.emailSecundario = emailSecundario;
	}

	public String getNomeFantasia() {

		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {

		this.nomeFantasia = nomeFantasia;
	}

	public String getInscricaoEstadual() {

		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {

		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getInscricaoMunicipal() {

		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {

		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public String getInscricaoRural() {

		return inscricaoRural;
	}

	public void setInscricaoRural(String inscricaoRural) {

		this.inscricaoRural = inscricaoRural;
	}

	public String getRg() {

		return rg;
	}

	public void setRg(String rg) {

		this.rg = rg;
	}

	public Date getDataEmissaoRG() {
		Date dataEmissaoRGTmp = null;
		if(dataEmissaoRG != null){
			dataEmissaoRGTmp = (Date) dataEmissaoRG.clone();
		}
		return dataEmissaoRGTmp;
	}

	public void setDataEmissaoRG(Date dataEmissaoRG) {
		if(dataEmissaoRG != null){
			this.dataEmissaoRG = (Date) dataEmissaoRG.clone();
		} else {
			this.dataEmissaoRG = null;
		}
	}

	public String getNumeroPassaporte() {

		return numeroPassaporte;
	}

	public void setNumeroPassaporte(String numeroPassaporte) {

		this.numeroPassaporte = numeroPassaporte;
	}

	public Date getDataNascimento() {
		Date dataNascimentoTmp = null;
		if(dataNascimento != null){
			dataNascimentoTmp = (Date) dataNascimento.clone();
		}
		return dataNascimentoTmp;
	}

	public void setDataNascimento(Date dataNascimento) {
		if (dataNascimento != null) {
			this.dataNascimento = (Date) dataNascimento.clone();
		} else {
			this.dataNascimento = null;
		}
	}

	public String getNomeMae() {

		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {

		this.nomeMae = nomeMae;
	}

	public String getNomePai() {

		return nomePai;
	}

	public void setNomePai(String nomePai) {

		this.nomePai = nomePai;
	}

	public String getRegimeRecolhimento() {

		return regimeRecolhimento;
	}

	public void setRegimeRecolhimento(String regimeRecolhimento) {

		this.regimeRecolhimento = regimeRecolhimento;
	}

	public ChamadoAlteracaoClienteEndereco getChamadoAlteracaoClienteEndereco() {

		return chamadoAlteracaoClienteEndereco;
	}

	public void setChamadoAlteracaoClienteEndereco(ChamadoAlteracaoClienteEndereco chamadoAlteracaoClienteEndereco) {

		this.chamadoAlteracaoClienteEndereco = chamadoAlteracaoClienteEndereco;
	}

	public String getDataEmissaoRGString() {

		return dataEmissaoRGString;
	}

	public void setDataEmissaoRGString(String dataEmissaoRGString) {

		this.dataEmissaoRGString = dataEmissaoRGString;
	}

	public String getDataNascimentoString() {

		return dataNascimentoString;
	}

	public void setDataNascimentoString(String dataNascimentoString) {

		this.dataNascimentoString = dataNascimentoString;
	}

	public String getDataEmissaoRGClienteString() {

		return dataEmissaoRGClienteString;
	}

	public void setDataEmissaoRGClienteString(String dataEmissaoRGClienteString) {

		this.dataEmissaoRGClienteString = dataEmissaoRGClienteString;
	}

	public String getDataNascimentoClienteString() {

		return dataNascimentoClienteString;
	}

	public void setDataNascimentoClienteString(String dataNascimentoClienteString) {

		this.dataNascimentoClienteString = dataNascimentoClienteString;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
