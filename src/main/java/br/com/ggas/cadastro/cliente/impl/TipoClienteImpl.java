/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.cliente.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.cliente.EsferaPoder;
import br.com.ggas.cadastro.cliente.TipoCliente;
import br.com.ggas.cadastro.cliente.TipoPessoa;
import br.com.ggas.geral.tabelaAuxiliar.impl.TabelaAuxiliarImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável por representar o tipo do cliente
 *
 */
public class TipoClienteImpl extends TabelaAuxiliarImpl implements TipoCliente {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 190308392535085287L;

	private String descricao;

	private TipoPessoa tipoPessoa;

	private EsferaPoder esferaPoder;

	@Override
	public String getDescricao() {

		return descricao;
	}

	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	@Override
	public TipoPessoa getTipoPessoa() {

		return tipoPessoa;
	}

	@Override
	public void setTipoPessoa(TipoPessoa tipoPessoa) {

		this.tipoPessoa = tipoPessoa;
	}

	@Override
	public EsferaPoder getEsferaPoder() {

		return esferaPoder;
	}

	@Override
	public void setEsferaPoder(EsferaPoder esferaPoder) {

		this.esferaPoder = esferaPoder;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.impl.TabelaAuxiliarImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(getDescricao())) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(getTipoPessoa() == null) {
			stringBuilder.append(TIPO_PESSOA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}
}
