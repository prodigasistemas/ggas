package br.com.ggas.cadastro.cliente.impl;

/**
 * Classe responsável pela representação de uma entidade ClienteAnexoEndereco.
 */
public class ClienteAnexoEnderecoVO {

	private String descricaoAnexoEndVO;
	private String anexoAbaEndereco;
	private Long tipoAnexoDeEndereco;
	private Integer indexListaAnexoAbaEndereco;
	
	/**
	 * @return tipoAnexoDeEndereco
	 */
	public Long getTipoAnexoDeEndereco() {
		return tipoAnexoDeEndereco;
	}	
	/**
	 * 
	 * @param tipoAnexoDeEndereco
	 */	
	public void setTipoAnexoDeEndereco(Long tipoAnexoDeEndereco) {
		this.tipoAnexoDeEndereco = tipoAnexoDeEndereco;
	}
	/**
	 * 
	 * @return descricaoAnexoEndVO
	 */
	public String getDescricaoAnexoEndVO() {
		return descricaoAnexoEndVO;
	}
	/**
	 * 
	 * @param descricaoAnexoEndVO
	 */
	public void setDescricaoAnexoEndVO(String descricaoAnexoEndVO) {
		this.descricaoAnexoEndVO = descricaoAnexoEndVO;
	}
	/**
	 * 
	 * @return anexoAbaEndereco
	 */
	public String getAnexoAbaEndereco() {
		return anexoAbaEndereco;
	}
	/**
	 * 
	 * @param anexoAbaEndereco
	 */
	public void setAnexoAbaEndereco(String anexoAbaEndereco) {
		this.anexoAbaEndereco = anexoAbaEndereco;
	}
	/**
	 * 
	 * @return indexListaAnexoAbaEndereco
	 */
	public Integer getIndexListaAnexoAbaEndereco() {
		return indexListaAnexoAbaEndereco;
	}
	/**
	 * 
	 * @param indexListaAnexoAbaEndereco
	 */
	public void setIndexListaAnexoAbaEndereco(Integer indexListaAnexoAbaEndereco) {
		this.indexListaAnexoAbaEndereco = indexListaAnexoAbaEndereco;
	}
}
