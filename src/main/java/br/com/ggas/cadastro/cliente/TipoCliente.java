/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.cliente;

import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;

/**
 * The Interface TipoCliente.
 */
public interface TipoCliente extends TabelaAuxiliar {

	/** The bean id tipo cliente. */
	String BEAN_ID_TIPO_CLIENTE = "tipoCliente";

	/** The descricao. */
	String DESCRICAO = "TIPO_CLIENTE_DESCRICAO";

	/** The tipo pessoa. */
	String TIPO_PESSOA = "TIPO_CLIENTE_TIPO_PESSOA";

	/** The esfera poder. */
	String ESFERA_PODER = "TIPO_CLIENTE_ESFERA_PODER";

	/** The tipo cliente. */
	String TIPO_CLIENTE = "TIPO_CLIENTE_ROTULO";

	/** The tipos cliente. */
	String TIPOS_CLIENTE = "TIPOS_CLIENTE_ROTULO";

	/**
	 * Gets the descricao.
	 *
	 * @return the descricao
	 */
	@Override
	String getDescricao();

	/**
	 * Sets the descricao.
	 *
	 * @param descricao            the descricao to set
	 */
	@Override
	void setDescricao(String descricao);

	/**
	 * Gets the tipo pessoa.
	 *
	 * @return the tipoPessoa
	 */
	@Override
	TipoPessoa getTipoPessoa();

	/**
	 * Sets the tipo pessoa.
	 *
	 * @param tipoPessoa            the tipoPessoa to set
	 */
	@Override
	void setTipoPessoa(TipoPessoa tipoPessoa);

	/**
	 * Gets the esfera poder.
	 *
	 * @return the esferaPoder
	 */
	@Override
	EsferaPoder getEsferaPoder();

	/**
	 * Sets the esfera poder.
	 *
	 * @param esferaPoder            the esferaPoder to set
	 */
	@Override
	void setEsferaPoder(EsferaPoder esferaPoder);

}
