/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.cliente;

import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface ClienteFone.
 */
public interface ClienteFone extends EntidadeNegocio {

	/** The bean id cliente fone. */
	String BEAN_ID_CLIENTE_FONE = "clienteFone";

	/** The tipo. */
	String TIPO = "Tipo de telefone";

	/** The codigo ddd. */
	String CODIGO_DDD = "DDD do telefone do cliente";

	/** The numero. */
	String NUMERO = "Número do telefone";

	/** The ramal. */
	String RAMAL = "Ramal do telefone";

	/**
	 * Gets the codigo ddd.
	 *
	 * @return the codigoDDD
	 */
	Integer getCodigoDDD();

	/**
	 * Sets the codigo ddd.
	 *
	 * @param codigoDDD            the codigoDDD to set
	 */
	void setCodigoDDD(Integer codigoDDD);

	/**
	 * Gets the numero.
	 *
	 * @return the numero
	 */
	Integer getNumero();

	/**
	 * Sets the numero.
	 *
	 * @param numero            the numero to set
	 */
	void setNumero(Integer numero);

	/**
	 * Gets the ramal.
	 *
	 * @return the ramal
	 */
	Integer getRamal();

	/**
	 * Sets the ramal.
	 *
	 * @param ramal            the ramal to set
	 */
	void setRamal(Integer ramal);

	/**
	 * Gets the indicador principal.
	 *
	 * @return the indicadorPrincipal
	 */
	Boolean getIndicadorPrincipal();

	/**
	 * Sets the indicador principal.
	 *
	 * @param indicadorPrincipal            the indicadorPrincipal to set
	 */
	void setIndicadorPrincipal(Boolean indicadorPrincipal);

	/**
	 * Gets the tipo fone.
	 *
	 * @return the tipoFone
	 */
	TipoFone getTipoFone();

	/**
	 * Sets the tipo fone.
	 *
	 * @param tipoFone            the tipoFone to set
	 */
	void setTipoFone(TipoFone tipoFone);

	/**
	 * Gets the cliente responsavel.
	 *
	 * @return the cliente responsavel
	 */
	Cliente getClienteResponsavel();

	/**
	 * Sets the cliente responsavel.
	 *
	 * @param clienteResponsavel the new cliente responsavel
	 */
	void setClienteResponsavel(Cliente clienteResponsavel);

}
