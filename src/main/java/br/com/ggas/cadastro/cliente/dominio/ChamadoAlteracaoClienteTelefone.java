/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 24/01/2014 17:13:48
 @author ccavalcanti
 */

package br.com.ggas.cadastro.cliente.dominio;

import java.util.Map;

import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.TipoFone;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe VO usada para transferencia de dados usada na classe Serviço
 */
public class ChamadoAlteracaoClienteTelefone extends EntidadeNegocioImpl {

	private static final long serialVersionUID = -8025527348037546986L;

	private Integer codigoDDD;

	private Integer numero;

	private Integer ramal;

	private Boolean indicadorPrincipal;

	private TipoFone tipoFone;

	private ClienteFone clienteFone;

	private EntidadeConteudo tipoAcao;

	public Integer getCodigoDDD() {

		return codigoDDD;
	}

	public void setCodigoDDD(Integer codigoDDD) {

		this.codigoDDD = codigoDDD;
	}

	public Integer getNumero() {

		return numero;
	}

	public void setNumero(Integer numero) {

		this.numero = numero;
	}

	public Integer getRamal() {

		return ramal;
	}

	public void setRamal(Integer ramal) {

		this.ramal = ramal;
	}

	public Boolean getIndicadorPrincipal() {

		return indicadorPrincipal;
	}

	public void setIndicadorPrincipal(Boolean indicadorPrincipal) {

		this.indicadorPrincipal = indicadorPrincipal;
	}

	public TipoFone getTipoFone() {

		return tipoFone;
	}

	public void setTipoFone(TipoFone tipoFone) {

		this.tipoFone = tipoFone;
	}

	public ClienteFone getClienteFone() {

		return clienteFone;
	}

	public void setClienteFone(ClienteFone clienteFone) {

		this.clienteFone = clienteFone;
	}

	public EntidadeConteudo getTipoAcao() {

		return tipoAcao;
	}

	public void setTipoAcao(EntidadeConteudo tipoAcao) {

		this.tipoAcao = tipoAcao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
