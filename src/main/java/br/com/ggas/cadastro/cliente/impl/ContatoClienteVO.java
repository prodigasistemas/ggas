package br.com.ggas.cadastro.cliente.impl;
/**
 * Classe responsável pela representação de uma entidade ContatoCliente
 * 
 * @author pedro
 */
public class ContatoClienteVO {
	
	private Long tipoContato;

	private String nomeContato;

	private Integer codigoDDD1Contato;

	private Integer foneContato;

	private Integer ramalContato;

	private Integer codigoDDD2Contato;

	private Integer fone2Contato;

	private Integer ramal2Contato;

	private Long profissaoContato;

	private String descricaoAreaContato;

	private String emailContato;

	private String cpfContato;

	private String rgContato;

	private boolean principal;
	
	private String clienteAba;

	/**
	 * 
	 * @return tipoContato
	 */
	public Long getTipoContato() {
		return tipoContato;
	}
	/**
	 * 
	 * @param tipoContato
	 */
	public void setTipoContato(Long tipoContato) {
		this.tipoContato = tipoContato;
	}
	/**
	 * 
	 * @return nomeContato
	 */
	public String getNomeContato() {
		return nomeContato;
	}
	/**
	 * 
	 * @param nomeContato
	 */
	public void setNomeContato(String nomeContato) {
		this.nomeContato = nomeContato;
	}
	/**
	 * 
	 * @return codigoDDD1Contato
	 */
	public Integer getCodigoDDDContato() {
		return codigoDDD1Contato;
	}
	/**
	 * 
	 * @param codigoDDDContato
	 */
	public void setCodigoDDDContato(Integer codigoDDDContato) {
		this.codigoDDD1Contato = codigoDDDContato;
	}
	/**
	 * 
	 * @return foneContato
	 */
	public Integer getFoneContato() {
		return foneContato;
	}
	/**
	 * 
	 * @param foneContato
	 */
	public void setFoneContato(Integer foneContato) {
		this.foneContato = foneContato;
	}
	/**
	 * 
	 * @return ramalContato
	 */
	public Integer getRamalContato() {
		return ramalContato;
	}
	/**
	 * 
	 * @param ramalContato
	 */
	public void setRamalContato(Integer ramalContato) {
		this.ramalContato = ramalContato;
	}
	/**
	 * 
	 * @return codigoDDD2Contato
	 */
	public Integer getCodigoDDD2Contato() {
		return codigoDDD2Contato;
	}
	/**
	 * 
	 * @param codigoDDD2Contato
	 */
	public void setCodigoDDD2Contato(Integer codigoDDD2Contato) {
		this.codigoDDD2Contato = codigoDDD2Contato;
	}
	/**
	 * 
	 * @return fone2Contato
	 */
	public Integer getFone2Contato() {
		return fone2Contato;
	}
	/**
	 * 
	 * @param fone2Contato
	 */
	public void setFone2Contato(Integer fone2Contato) {
		this.fone2Contato = fone2Contato;
	}
	/**
	 * 
	 * @return ramal2Contato
	 */
	public Integer getRamal2Contato() {
		return ramal2Contato;
	}
	/**
	 * 
	 * @param ramal2Contato
	 */
	public void setRamal2Contato(Integer ramal2Contato) {
		this.ramal2Contato = ramal2Contato;
	}
	/**
	 * 
	 * @return profissaoContato
	 */
	public Long getProfissaoContato() {
		return profissaoContato;
	}
	/**
	 * 
	 * @param profissaoContato
	 */
	public void setProfissaoContato(Long profissaoContato) {
		this.profissaoContato = profissaoContato;
	}
	/**
	 * 
	 * @return descricaoAreaContato
	 */
	public String getDescricaoAreaContato() {
		return descricaoAreaContato;
	}
	/**
	 * 
	 * @param descricaoAreaContato
	 */
	public void setDescricaoAreaContato(String descricaoAreaContato) {
		this.descricaoAreaContato = descricaoAreaContato;
	}
	/**
	 * 
	 * @return emailContato
	 */
	public String getEmailContato() {
		return emailContato;
	}
	/**
	 * 
	 * @param emailContato
	 */
	public void setEmailContato(String emailContato) {
		this.emailContato = emailContato;
	}
	/**
	 * 
	 * @return cpfContato
	 */
	public String getCpfContato() {
		return cpfContato;
	}
	/**
	 * 
	 * @param cpfContato
	 */
	public void setCpfContato(String cpfContato) {
		this.cpfContato = cpfContato;
	}
	/**
	 * 
	 * @return rgContato
	 */
	public String getRgContato() {
		return rgContato;
	}
	/**
	 * 
	 * @param rgContato
	 */
	public void setRgContato(String rgContato) {
		this.rgContato = rgContato;
	}
	/**
	 * 
	 * @return principal
	 */
	public boolean isPrincipal() {
		return principal;
	}
	/**
	 * 
	 * @param principal
	 */
	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}
	/**
	 * 
	 * @return clienteAba
	 */
	public String getClienteAba() {
		return clienteAba;
	}
	/**
	 * 
	 * @param clienteAba
	 */
	public void setClienteAba(String clienteAba) {
		this.clienteAba = clienteAba;
	}

}
