/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.cliente;

import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface ContatoCliente.
 */
public interface ContatoCliente extends EntidadeNegocio {

	/** The bean id contato cliente. */
	String BEAN_ID_CONTATO_CLIENTE = "contatoCliente";

	/** The cliente. */
	String CLIENTE = "CONTATO_CLIENTE";

	/** The tipo. */
	String TIPO = "Tipo de Contato";

	/** The nome. */
	String NOME = "Nome do Contato";

	/** The email. */
	String EMAIL = "CONTATO_EMAIL";

	/**
	 * @return Telefone1Formatado
	 */
	String getTelefone1Formatado();

	/**
	 * @return Telefone2Formatado
	 */
	String getTelefone2Formatado();

	/**
	 * Gets the tipo contato.
	 *
	 * @return the tipoContato
	 */
	TipoContato getTipoContato();

	/**
	 * Sets the tipo contato.
	 *
	 * @param tipoContato            the tipoContato to set
	 */
	void setTipoContato(TipoContato tipoContato);

	/**
	 * Gets the nome.
	 *
	 * @return the nome
	 */
	String getNome();

	/**
	 * Sets the nome.
	 *
	 * @param nome            the nome to set
	 */
	void setNome(String nome);

	/**
	 * Gets the codigo ddd.
	 *
	 * @return the codigoDDD
	 */
	Integer getCodigoDDD();

	/**
	 * Sets the codigo ddd.
	 *
	 * @param codigoDDD            the codigoDDD to set
	 */
	void setCodigoDDD(Integer codigoDDD);

	/**
	 * Gets the codigo dd d2.
	 *
	 * @return the codigo dd d2
	 */
	Integer getCodigoDDD2();

	/**
	 * Sets the codigo dd d2.
	 *
	 * @param codigoDDD2 the new codigo dd d2
	 */
	void setCodigoDDD2(Integer codigoDDD2);

	/**
	 * Gets the fone.
	 *
	 * @return the fone
	 */
	Integer getFone();

	/**
	 * Sets the fone.
	 *
	 * @param fone            the fone to set
	 */
	void setFone(Integer fone);

	/**
	 * Gets the fone2.
	 *
	 * @return the fone2
	 */
	Integer getFone2();

	/**
	 * Sets the fone2.
	 *
	 * @param fone2 the new fone2
	 */
	void setFone2(Integer fone2);

	/**
	 * Gets the ramal.
	 *
	 * @return the ramal
	 */
	Integer getRamal();

	/**
	 * Sets the ramal.
	 *
	 * @param ramal            the ramal to set
	 */
	void setRamal(Integer ramal);

	/**
	 * Gets the ramal2.
	 *
	 * @return the ramal2
	 */
	Integer getRamal2();

	/**
	 * Sets the ramal2.
	 *
	 * @param ramal2 the new ramal2
	 */
	void setRamal2(Integer ramal2);

	/**
	 * Gets the descricao area.
	 *
	 * @return the descricaoArea
	 */
	String getDescricaoArea();

	/**
	 * Sets the descricao area.
	 *
	 * @param descricaoArea            the descricaoArea to set
	 */
	void setDescricaoArea(String descricaoArea);

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	String getEmail();

	/**
	 * Sets the email.
	 *
	 * @param email            the email to set
	 */
	void setEmail(String email);

	/**
	 * Gets the cpf.
	 *
	 * @return the cpf
	 */
	String getCpf();

	/**
	 * Sets the cpf.
	 *
	 * @param cpf the cpf to set
	 */
	void setCpf(String cpf);

	/**
	 * Gets the rg.
	 *
	 * @return the rg
	 */
	String getRg();

	/**
	 * Sets the rg.
	 *
	 * @param rg the rg to set
	 */
	void setRg(String rg);

	/**
	 * Checks if is principal.
	 *
	 * @return the principal
	 */
	boolean isPrincipal();

	/**
	 * Sets the principal.
	 *
	 * @param principal            the principal to set
	 */
	void setPrincipal(boolean principal);

	/**
	 * Gets the profissao.
	 *
	 * @return the profissao
	 */
	Profissao getProfissao();

	/**
	 * Sets the profissao.
	 *
	 * @param profissao            the profissao to set
	 */
	void setProfissao(Profissao profissao);

}
