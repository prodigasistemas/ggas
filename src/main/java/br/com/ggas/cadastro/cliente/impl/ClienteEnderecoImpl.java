/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.cliente.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.TipoEndereco;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável por representar o endereço de um cliente
 * 
 */
public class ClienteEnderecoImpl extends EntidadeNegocioImpl implements ClienteEndereco {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 6796922703707873551L;

	private Cep cep;

	private String numero;

	private String complemento;

	private Integer caixaPostal;

	private TipoEndereco tipoEndereco;

	private String enderecoReferencia;

	private Boolean correspondencia;

	private Municipio municipio;

	private Integer indicadorPrincipal;

	/**
	 * @return the correspondencia
	 */
	@Override
	public Boolean getCorrespondencia() {

		return correspondencia;
	}

	/**
	 * @param correspondencia
	 *            the correspondencia to set
	 */
	@Override
	public void setCorrespondencia(final Boolean correspondencia) {

		this.correspondencia = correspondencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		final Map<String, Object> erros = new HashMap<>();
		final StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(cep == null) {
			stringBuilder.append(CEP);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tipoEndereco == null) {
			stringBuilder.append(TIPO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(numero == null || "".equals(numero)) {
			stringBuilder.append(NUMERO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * getCep()
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco#getCep()
	 */
	@Override
	public Cep getCep() {

		return cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * setCep(br.com.ggas.cadastro.endereco.Cep)
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco
	 * #setCep(br.com.ggas.cadastro.endereco.Cep)
	 */
	@Override
	public void setCep(final Cep cep) {

		this.cep = cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * getNumero()
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco#getNumero()
	 */
	@Override
	public String getNumero() {

		return numero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * setNumero(java.lang.Integer)
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco#setNumero(java.lang.String)
	 */
	@Override
	public void setNumero(final String numero) {

		this.numero = numero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * getComplemento()
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco#getComplemento()
	 */
	@Override
	public String getComplemento() {

		return complemento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * setComplemento(java.lang.String)
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco
	 * #setComplemento(java.lang.String)
	 */
	@Override
	public void setComplemento(final String complemento) {

		this.complemento = complemento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * getEnderecoReferencia()
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco#getEnderecoReferencia()
	 */
	@Override
	public String getEnderecoReferencia() {

		return enderecoReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.endereco.Endereco#
	 * setEnderecoReferencia(java.lang.String)
	 */
	@Override
	public void setEnderecoReferencia(final String enderecoReferencia) {

		this.enderecoReferencia = enderecoReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco#getCaixaPostal()
	 */
	@Override
	public Integer getCaixaPostal() {

		return caixaPostal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco
	 * #setCaixaPostal(java.lang.String)
	 */
	@Override
	public void setCaixaPostal(final Integer caixaPostal) {

		this.caixaPostal = caixaPostal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco#getTipoEndereco()
	 */
	@Override
	public TipoEndereco getTipoEndereco() {

		return tipoEndereco;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco
	 * #setTipoEndereco(br.com.ggas.
	 * cadastro.cliente.TipoEndereco)
	 */
	@Override
	public void setTipoEndereco(final TipoEndereco tipoEndereco) {

		this.tipoEndereco = tipoEndereco;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco#getEnderecoFormatado()
	 */
	@Override
	public String getEnderecoLogradouro() {

		final StringBuilder enderecoLogradouro = new StringBuilder();
		if(this.getCep() != null && this.getCep().getTipoLogradouro() != null) {
			enderecoLogradouro.append(this.getCep().getTipoLogradouro());
		}
		if(this.getCep() != null && this.getCep().getLogradouro() != null) {
			final String separador;
			if(enderecoLogradouro.length() > 0){
				separador = " ";
			}else{
				separador = "";
			}
			enderecoLogradouro.append(separador);
			enderecoLogradouro.append(this.getCep().getLogradouro());
		}

		return enderecoLogradouro.toString();
	}

	@Override
	public String getEnderecoFormatadoRuaNumeroComplemento() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		String logradouro = this.getEnderecoLogradouro();

		if (logradouro != null && !logradouro.isEmpty()) {
			enderecoFormatado.append(logradouro);
		}

		if (this.getNumero() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getNumero());
		}
		
		if (this.getComplemento() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getComplemento());
		}

		return enderecoFormatado.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ClienteEndereco#getEnderecoFormatado()
	 */
	@Override
	public String getEnderecoFormatado() {

		final StringBuilder enderecoFormatado = new StringBuilder();

		String logradouroNumeroComplemento = this.getEnderecoFormatadoRuaNumeroComplemento();

		if (logradouroNumeroComplemento != null && !logradouroNumeroComplemento.isEmpty()) {
			enderecoFormatado.append(logradouroNumeroComplemento);
		}

		if (this.getCep() != null && this.getCep().getBairro() != null) {
			final String separador;
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getBairro());
		}

		if (this.getCep() != null && this.getCep().getMunicipio() != null) {
			
			final String separador;
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getNomeMunicipio());
			
		} else if (this.getCep() != null && this.getCep().getNomeMunicipio() != null) {
			
			final String separador;
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getNomeMunicipio());
		}

		if (this.getCep().getUf() != null) {
			
			final String separador;
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getUf());
		}

		if (this.getCep() != null && this.getEnderecoReferencia() != null) {
			
			final String separador;
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getEnderecoReferencia());
		}

		return enderecoFormatado.toString();
	}

	@Override
	public String getEnderecoFormatadoMunicipioUF() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		if (this.getCep().getNomeMunicipio() != null) {
			enderecoFormatado.append(this.getCep().getNomeMunicipio());

		} else if (this.getMunicipio() != null) {
			enderecoFormatado.append(this.getMunicipio().getDescricao());

		} else if (this.getCep().getMunicipio() != null) {
			enderecoFormatado.append(this.getCep().getMunicipio().getDescricao());
		}

		if (this.getCep().getUf() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = " - ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getUf());
			
		} else if (this.getMunicipio() != null && this.getMunicipio().getUnidadeFederacao() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = " - ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getMunicipio().getUnidadeFederacao().getSigla());
		}

		return enderecoFormatado.toString();
	}

	@Override
	public String getEnderecoFormatadoBairroMunicipioUFCEP() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		if(this.getCep().getBairro() != null) {
			enderecoFormatado.append(this.getCep().getBairro());
		}
		if(this.getCep().getMunicipio() != null) {
			if(enderecoFormatado.length() > 0){
				separador = ", ";
			}else{
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getMunicipio().getDescricao());
		} else if(this.getCep().getNomeMunicipio() != null) {
			if(enderecoFormatado.length() > 0){
				separador = ", ";
			}else{
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getNomeMunicipio());
		}
		
		if(this.getCep().getUf() != null) {
			if(enderecoFormatado.length() > 0){
				separador = " - ";
			}else{
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getUf());
		}
		if(this.getCep().getCep() != null) {
			if(enderecoFormatado.length() > 0){
				separador = " CEP ";
			}else{
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getCep());
		}

		return enderecoFormatado.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ClienteEndereco
	 * #getMunicipio()
	 */
	@Override
	public Municipio getMunicipio() {

		return this.municipio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ClienteEndereco
	 * #
	 * setMunicipio(br.com.ggas.cadastro.geografico
	 * .Municipio)
	 */
	@Override
	public void setMunicipio(Municipio municipio) {

		this.municipio = municipio;
	}

	@Override
	public String getEnderecoFormatadoBairro() {

		StringBuilder enderecoFormatado = new StringBuilder();
		if(this.getCep().getBairro() != null) {
			enderecoFormatado.append(this.getCep().getBairro());
		}

		return enderecoFormatado.toString();
	}

	@Override
	public String getEnderecoFormatadoMunicipioUFCEP() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		if (this.getCep().getNomeMunicipio() != null) {
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getNomeMunicipio());
		} else if (this.getCep().getMunicipio() != null) {
			enderecoFormatado.append(this.getCep().getMunicipio().getDescricao());
		}

		if (this.getCep().getUf() != null) {
			if (enderecoFormatado.length() > 0) {
				separador = " - ";
			} else {
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getUf());
		}
		if (this.getCep().getCep() != null) {
			if (enderecoFormatado.length() > 0) {
				separador = " CEP ";
			} else {
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getCep());
		}

		return enderecoFormatado.toString();
	}

	@Override
	public Integer getIndicadorPrincipal() {

		return indicadorPrincipal;
	}

	@Override
	public void setIndicadorPrincipal(Integer indicadorPrincipal) {

		this.indicadorPrincipal = indicadorPrincipal;
	}
	
	@Override
	public String getEnderecoFormatadoRuaNumeroBairro() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		String logradouro = this.getEnderecoLogradouro();

		if (logradouro != null && !logradouro.isEmpty()) {
			enderecoFormatado.append(logradouro);
		}

		if (this.getNumero() != null) {

			if (enderecoFormatado.length() > 0) {
				separador = ", N° ";
			} else {
				separador = "";
			}

			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getNumero());
		}

		if (this.getCep().getBairro() != null) {

			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}

			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getBairro());
		}

		return enderecoFormatado.toString();
	}

	@Override
	public String getEnderecoFormatadoCepCidadeEstado() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		if (this.getCep().getCep() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = "";
			} else {
				separador = "CEP: ";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getCep());
		}

		if (this.getCep().getMunicipio() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = " ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getMunicipio().getDescricao());
			
		} else if (this.getCep().getNomeMunicipio() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = " ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getNomeMunicipio());
		}

		if (this.getCep().getUf() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = "-";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getUf());
		}

		return enderecoFormatado.toString();
	}
	
	@Override
	public String getEnderecoFormatadoRuaNumeroBairroMunicipioUF() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		String logradouro = this.getEnderecoLogradouro();

		if (logradouro != null && !logradouro.isEmpty()) {
			enderecoFormatado.append(logradouro);
		}

		if (this.getNumero() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getNumero());
			enderecoFormatado.append(" - ");
		}
		
		if (this.getCep().getBairro() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getBairro());
			enderecoFormatado.append(" - ");
		}

		if (!StringUtils.isEmpty(this.getCep().getNomeMunicipio())) {
			
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getNomeMunicipio());
		}

		if (!StringUtils.isEmpty(this.getCep().getUf())) {
			enderecoFormatado.append("/");
			enderecoFormatado.append(this.getCep().getUf());
		}
		
		return enderecoFormatado.toString();
	}

}
