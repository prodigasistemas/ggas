/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.cadastro.cliente.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.AtividadeEconomica;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteAnexo;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ClienteSituacao;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.EsferaPoder;
import br.com.ggas.cadastro.cliente.FaixaRendaFamiliar;
import br.com.ggas.cadastro.cliente.Nacionalidade;
import br.com.ggas.cadastro.cliente.OrgaoExpedidor;
import br.com.ggas.cadastro.cliente.PessoaSexo;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoCliente;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.cliente.TipoEndereco;
import br.com.ggas.cadastro.cliente.TipoFone;
import br.com.ggas.cadastro.cliente.TipoPessoa;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoCliente;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoClienteContato;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoClienteEndereco;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoClienteTelefone;
import br.com.ggas.cadastro.cliente.dominio.ChamadoClienteAnexo;
import br.com.ggas.cadastro.cliente.repositorio.RepositorioCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.dominio.EntidadeDominio;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe responsável pelas ações relacionadas a um cliente
 *
 */
public class ControladorClienteImpl extends ControladorNegocioImpl implements ControladorCliente {

	private static final String CLIENTE_RESPONSAVEL = "clienteResponsavel";

	private static final String CHAMADO_CHAVE = "chamadoChave";

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String ATIVIDADE_ECONOMICA = "atividadeEconomica";

	private static final String GRUPO_ECONOMICO = "grupoEconomico";

	private static final String PASSAPORTE = "passaporte";

	private static final String QUALQUER_NOME = "qualquerNome";

	private static final String NOME_FANTASIA = "nomeFantasia";

	private static final String TIPO_CLIENTE = "tipoCliente";
	
	private static final int PESSOA_FISICA = 1;
	
	private static final int PESSOA_JURIDICA = 2;
	
	private static final int TAMANHO_CPF = 11;

	private static final int INDICE_FINAL_INSC_EST_AL = 9;

	private static final String SEPARADOR_EMAILS = ";";

	private static final Logger LOG = Logger.getLogger(ControladorClienteImpl.class);

	private static final String CEP = "cep";

	private static final String INSCRICAO_RURAL = "inscricaoRural";

	private static final String INSCRICAO_MUNICIPAL = "inscricaoMunicipal";

	private static final String INSCRICAO_ESTADUAL = "inscricaoEstadual";

	private static final String CPF = "cpf";

	private static final String CNPJ = "cnpj";

	private static final String DESCRICAO = "descricao";

	private static final String HABILITADO = "habilitado";

	private static final Integer PRINCIPAL = 1;

	@Autowired
	private RepositorioCliente repositorioCliente;
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Cliente.BEAN_ID_CLIENTE);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #criarTipoCliente()
	 */
	@Override
	public EntidadeNegocio criarTipoCliente() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(TipoCliente.BEAN_ID_TIPO_CLIENTE);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #criarEsferaPoder()
	 */
	@Override
	public EntidadeNegocio criarEsferaPoder() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(EsferaPoder.BEAN_ID_ESFERA_PODER);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarContatoCliente()
	 */
	@Override
	public EntidadeNegocio criarContatoCliente() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ContatoCliente.BEAN_ID_CONTATO_CLIENTE);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #criarTipoPessoa()
	 */
	@Override
	public EntidadeDominio criarTipoPessoa() {

		return (EntidadeDominio) ServiceLocator.getInstancia().getBeanPorID(TipoPessoa.BEAN_ID_TIPO_PESSOA);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #criarClienteEndereco()
	 */
	@Override
	public EntidadeNegocio criarClienteEndereco() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ClienteEndereco.BEAN_ID_CLIENTE_ENDERECO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarClienteAnexo()
	 */
	@Override
	public EntidadeNegocio criarClienteAnexo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ClienteAnexo.BEAN_ID_CLIENTE_ANEXO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #criarClienteFone()
	 */
	@Override
	public EntidadeNegocio criarClienteFone() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ClienteFone.BEAN_ID_CLIENTE_FONE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarOrgaoExpedidor()
	 */
	@Override
	public EntidadeNegocio criarOrgaoExpedidor() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(OrgaoExpedidor.BEAN_ID_ORGAO_EXPEDIDOR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarUnidadeFederacao()
	 */
	@Override
	public EntidadeNegocio criarUnidadeFederacao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(UnidadeFederacao.BEAN_ID_UNIDADE_FEDERACAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarNacionalidade()
	 */
	@Override
	public EntidadeNegocio criarNacionalidade() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Nacionalidade.BEAN_ID_NACIONALIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarProfissao()
	 */
	@Override
	public EntidadeNegocio criarProfissao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Profissao.BEAN_ID_PROFISSAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarPessoaSexo()
	 */
	@Override
	public EntidadeNegocio criarPessoaSexo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PessoaSexo.BEAN_ID_SEXO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarFaixaRendaFamiliar()
	 */
	@Override
	public EntidadeNegocio criarFaixaRendaFamiliar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(FaixaRendaFamiliar.BEAN_ID_FAIXA_RENDA_FAMILIAR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarAtividadeEconomica()
	 */
	@Override
	public EntidadeNegocio criarAtividadeEconomica() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AtividadeEconomica.BEAN_ID_ATIVIDADE_ECONOMICA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarTipoFone()
	 */
	@Override
	public EntidadeNegocio criarTipoFone() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(TipoFone.BEAN_ID_TIPO_FONE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarCep()
	 */
	@Override
	public EntidadeNegocio criarCep() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Cep.BEAN_ID_CEP);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarTipoEndereco()
	 */
	@Override
	public EntidadeNegocio criarTipoEndereco() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(TipoEndereco.BEAN_ID_TIPO_ENDERECO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarTipoContato()
	 */
	@Override
	public EntidadeNegocio criarTipoContato() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(TipoContato.BEAN_ID_TIPO_CONTATO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarMunicipio()
	 */
	@Override
	public EntidadeNegocio criarMunicipio() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Municipio.BEAN_ID_MUNICIPIO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#criarTipoAcao()
	 */
	@Override
	public EntidadeNegocio criarTipoAcao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Cliente.BEAN_ID_CLIENTE);
	}

	public Class<?> getClasseEntidadeTipoCliente() {

		return ServiceLocator.getInstancia().getClassPorID(TipoCliente.BEAN_ID_TIPO_CLIENTE);
	}

	public Class<?> getClasseEntidadeClienteSituacao() {

		return ServiceLocator.getInstancia().getClassPorID(ClienteSituacao.BEAN_ID_SITUACAO);
	}

	public Class<?> getClasseEntidadeProfissao() {

		return ServiceLocator.getInstancia().getClassPorID(Profissao.BEAN_ID_PROFISSAO);
	}

	public Class<?> getClasseEntidadeClienteEndereco() {

		return ServiceLocator.getInstancia().getClassPorID(ClienteEndereco.BEAN_ID_CLIENTE_ENDERECO);
	}

	public Class<?> getClasseEntidadeContatoCliente() {

		return ServiceLocator.getInstancia().getClassPorID(ContatoCliente.BEAN_ID_CONTATO_CLIENTE);
	}

	public Class<?> getClasseEntidadeClienteTelefone() {

		return ServiceLocator.getInstancia().getClassPorID(ClienteFone.BEAN_ID_CLIENTE_FONE);
	}

	public Class<?> getClasseEntidadeNacionalidade() {

		return ServiceLocator.getInstancia().getClassPorID(Nacionalidade.BEAN_ID_NACIONALIDADE);
	}

	public Class<?> getClasseEntidadeAtividadeEconomica() {

		return ServiceLocator.getInstancia().getClassPorID(AtividadeEconomica.BEAN_ID_ATIVIDADE_ECONOMICA);
	}

	public Class<?> getClasseEntidadeTipoEndereco() {

		return ServiceLocator.getInstancia().getClassPorID(TipoEndereco.BEAN_ID_TIPO_ENDERECO);
	}

	public Class<?> getClasseEntidadeTipoFone() {

		return ServiceLocator.getInstancia().getClassPorID(TipoFone.BEAN_ID_TIPO_FONE);
	}

	public Class<?> getClasseEntidadeTipoContato() {

		return ServiceLocator.getInstancia().getClassPorID(TipoContato.BEAN_ID_TIPO_CONTATO);
	}

	public Class<?> getClasseEntidadeOrgaoExpedidor() {

		return ServiceLocator.getInstancia().getClassPorID(OrgaoExpedidor.BEAN_ID_ORGAO_EXPEDIDOR);
	}

	public Class<?> getClasseEntidadeFaixaRendaFamiliar() {

		return ServiceLocator.getInstancia().getClassPorID(FaixaRendaFamiliar.BEAN_ID_FAIXA_RENDA_FAMILIAR);
	}

	public Class<?> getClasseEntidadePessoaSexo() {

		return ServiceLocator.getInstancia().getClassPorID(PessoaSexo.BEAN_ID_SEXO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);

	}

	public Class<?> getClasseEntidadeContratoCliente() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoCliente.BEAN_ID_CONTRATO_CLIENTE);

	}

	public Class<?> getClasseEntidadeContrato() {

		return ServiceLocator.getInstancia().getClassPorID(Contrato.BEAN_ID_CONTRATO);

	}

	public Class<?> getClasseEntidadeContratoPontoConsumoModalidade() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoModalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE);

	}

	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeCreditoDebitoARealizar() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoARealizar.BEAN_ID_CREDITO_DEBITO_A_REALIZAR);
	}

	public Class<?> getClasseEntidadeChamadoAlteracaoCliente() {

		return ChamadoAlteracaoCliente.class;
	}

	/**
	 * Montar consulta.
	 *
	 * @param classe
	 *            the classe
	 * @return the criteria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Criteria montarConsulta(Class<?> classe) throws NegocioException {

		Criteria criteria = createCriteria(classe);
		criteria.add(Restrictions.eq(HABILITADO, Boolean.TRUE));
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarTipoCliente()
	 */
	@Override
	public Collection<TipoCliente> listarTipoCliente() throws NegocioException {

		Criteria criteria = montarConsulta(getClasseEntidadeTipoCliente());

		Order ordenacao = Order.asc(DESCRICAO);
		criteria.addOrder(ordenacao);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarClienteSituacao()
	 */
	@Override
	public Collection<ClienteSituacao> listarClienteSituacao() throws NegocioException {

		Criteria criteria = montarConsulta(getClasseEntidadeClienteSituacao());

		Order ordenacao = Order.asc(DESCRICAO);
		criteria.addOrder(ordenacao);

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarProfissao()
	 */
	@Override
	public Collection<Profissao> listarProfissao() throws NegocioException {

		return montarConsulta(getClasseEntidadeProfissao()).addOrder(Order.asc(DESCRICAO)).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarNacionalidade()
	 */
	@Override
	public Collection<Nacionalidade> listarNacionalidade() throws NegocioException {

		return montarConsulta(getClasseEntidadeNacionalidade()).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarAtividadeEconomica()
	 */
	@Override
	public Collection<AtividadeEconomica> listarAtividadeEconomica() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeAtividadeEconomica());
		criteria.add(Restrictions.eq(HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc(DESCRICAO));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarTipoEndereco()
	 */
	@Override
	public Collection<TipoEndereco> listarTipoEndereco() throws NegocioException {

		return montarConsulta(getClasseEntidadeTipoEndereco()).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarTipoFone()
	 */
	@Override
	public Collection<TipoFone> listarTipoFone() throws NegocioException {

		return montarConsulta(getClasseEntidadeTipoFone()).list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarTipoContato()
	 */
	@Override
	public Collection<TipoContato> listarTipoContato() throws NegocioException {

		Criteria criteria = montarConsulta(getClasseEntidadeTipoContato());

		Order ordenacao = Order.asc(DESCRICAO);
		criteria.addOrder(ordenacao);

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarOrgaoExpedidor()
	 */
	@Override
	public Collection<OrgaoExpedidor> listarOrgaoExpedidor() throws NegocioException {

		return montarConsulta(getClasseEntidadeOrgaoExpedidor()).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#listarFaixaRendaFamiliar()
	 */
	@Override
	public Collection<FaixaRendaFamiliar> listarFaixaRendaFamiliar() throws NegocioException {

		Criteria criteria = montarConsulta(getClasseEntidadeFaixaRendaFamiliar());

		Order ordenacao = Order.asc("valorFaixaInicio");
		criteria.addOrder(ordenacao);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#buscarTipoContato(java.lang.Long)
	 */
	@Override
	public TipoContato buscarTipoContato(Long chavePrimaria) throws NegocioException {

		return (TipoContato) obter(chavePrimaria, getClasseEntidadeTipoContato());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #buscarTipoFone(java.lang.Long)
	 */
	@Override
	public TipoFone buscarTipoFone(Long chavePrimaria) throws NegocioException {

		return (TipoFone) obter(chavePrimaria, getClasseEntidadeTipoFone());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #buscarTipoEndereco(java.lang.Long)
	 */
	@Override
	public TipoEndereco buscarTipoEndereco(Long chavePrimaria) throws NegocioException {

		return (TipoEndereco) obter(chavePrimaria, getClasseEntidadeTipoEndereco());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #validarDadosContatoCliente(br.com
	 * .procenge.ggas.cadastro.cliente.ContatoCliente
	 * )
	 */
	@Override
	public void validarDadosContatoCliente(ContatoCliente contato) throws NegocioException {

		Map<String, Object> errosContato = contato.validarDados();

		if (errosContato != null && !errosContato.isEmpty()) {
			throw new NegocioException(errosContato);
		}

		if (!StringUtils.isEmpty(contato.getEmail()) && !Util.validarDominio(contato.getEmail(), Constantes.EXPRESSAO_REGULAR_EMAIL)) {
			throw new NegocioException(ERRO_NEGOCIO_EMAIL_INVALIDO, ContatoCliente.EMAIL);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#validarDadosClienteAnexo(br.com.ggas.cadastro.cliente.ClienteAnexo)
	 */
	@Override
	public void validarDadosClienteAnexo(ClienteAnexo clienteAnexo) throws NegocioException {

		Map<String, Object> errosClienteAnexo = clienteAnexo.validarDados();

		if (errosClienteAnexo != null && !errosClienteAnexo.isEmpty()) {
			throw new NegocioException(errosClienteAnexo);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #verificarEmailContatoExistente(java
	 * .util.Collection,
	 * br.com.ggas.cadastro.cliente
	 * .ContatoCliente)
	 */
	@Override
	public void verificarEmailContatoExistente(Integer indexLista, Collection<ContatoCliente> listaContatoCliente,
					ContatoCliente contatoCliente) throws NegocioException {

		for (int i = 0; i < listaContatoCliente.size(); i++) {

			ContatoCliente contatoClienteExistente = ((List<ContatoCliente>) listaContatoCliente).get(i);

			String email = contatoClienteExistente.getEmail();
			if ((email != null && email.equals(contatoCliente.getEmail())) && (indexLista != i)) {
				throw new NegocioException(ERRO_NEGOCIO_EMAIL_CONTATO_EXISTENTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #validarDadosClienteEndereco(br.com
	 * .procenge.ggas.cadastro.cliente.impl.
	 * ClienteEndereco)
	 */
	@Override
	public void validarDadosClienteEndereco(ClienteEndereco clienteEndereco) throws NegocioException {

		Map<String, Object> errosClienteEndereco = clienteEndereco.validarDados();

		if (errosClienteEndereco != null && !errosClienteEndereco.isEmpty()) {
			throw new NegocioException(errosClienteEndereco);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #verificarNumeroClienteEnderecoExistente
	 * (java.util.Collection,
	 * br.com.ggas.cadastro.
	 * cliente.impl.ClienteEndereco)
	 */
	@Override
	public void verificarNumeroClienteEnderecoExistente(Integer indexLista, Collection<ClienteEndereco> listaClienteEndereco,
					ClienteEndereco clienteEndereco) throws NegocioException {

		for (int i = 0; i < listaClienteEndereco.size(); i++) {

			ClienteEndereco clienteEnderecoExistente = ((List<ClienteEndereco>) listaClienteEndereco).get(i);

			String numero = clienteEnderecoExistente.getNumero();
			String logradouro = clienteEnderecoExistente.getCep().getLogradouro();
			Integer indPrincipal = clienteEnderecoExistente.getIndicadorPrincipal();
			if (numero != null && numero.equals(clienteEndereco.getNumero()) && indexLista != i &&
							logradouro.equals(clienteEndereco.getCep().getLogradouro())) {

				throw new NegocioException(ERRO_NEGOCIO_NUMERO_ENDERECO_EXISTENTE, true);
			}

			if (clienteEndereco.getIndicadorPrincipal().equals(PRINCIPAL) && indexLista != i && indPrincipal.equals(PRINCIPAL)) {
				throw new NegocioException(ERRO_NEGOCIO_ENDERECO_PRINCIPAL_EXISTENTE, true);

			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#verificarTipoAnexoAnexo(java.lang.Integer, java.util.Collection,
	 * br.com.ggas.cadastro.cliente.ClienteAnexo)
	 */
	@Override
	public void verificarTipoAnexoAnexo(Integer indexLista, Collection<ClienteAnexo> listaClienteAnexo, ClienteAnexo clienteAnexo)
					throws NegocioException {

		for (int i = 0; i < listaClienteAnexo.size(); i++) {

			ClienteAnexo clienteAnexoExistente = ((List<ClienteAnexo>) listaClienteAnexo).get(i);

			Long idTipoAnexo = clienteAnexoExistente.getTipoAnexo().getChavePrimaria();
			if (idTipoAnexo != null && idTipoAnexo == clienteAnexo.getTipoAnexo().getChavePrimaria() && indexLista != i) {
				throw new NegocioException(ERRO_NEGOCIO_TIPO_ANEXO_EXISTENTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#verificarDescricaoAnexo(java.lang.Integer, java.util.Collection,
	 * br.com.ggas.cadastro.cliente.ClienteAnexo)
	 */
	@Override
	public void verificarDescricaoAnexo(Integer indexLista, Collection<ClienteAnexo> listaClienteAnexo, ClienteAnexo clienteAnexo)
					throws NegocioException {

		for (int i = 0; i < listaClienteAnexo.size(); i++) {

			ClienteAnexo clienteAnexoExistente = ((List<ClienteAnexo>) listaClienteAnexo).get(i);

			String descricao = clienteAnexoExistente.getDescricaoAnexo();
			if (descricao != null && descricao.equals(clienteAnexo.getDescricaoAnexo()) && indexLista != i) {
				throw new NegocioException(ERRO_NEGOCIO_DESCRICAO_ANEXO_EXISTENTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #validarDadosClienteFone(br.com.procenge
	 * .ggas.cadastro.cliente.ClienteFone)
	 */
	@Override
	public void validarDadosClienteFone(ClienteFone clienteFone) throws NegocioException {

		Map<String, Object> errosClienteFone = clienteFone.validarDados();

		if (errosClienteFone != null && !errosClienteFone.isEmpty()) {
			throw new NegocioException(errosClienteFone);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #verificarNumeroTelefoneExistente
	 * (java.util.Collection,
	 * br.com.ggas.cadastro.cliente.ClienteFone)
	 */
	@Override
	public void verificarNumeroTelefoneExistente(Integer indexLista, Collection<ClienteFone> listaClienteFone, ClienteFone clienteFone)
					throws NegocioException {

		for (int i = 0; i < listaClienteFone.size(); i++) {

			ClienteFone clienteFoneExistente = ((List<ClienteFone>) listaClienteFone).get(i);

			Integer numero = clienteFoneExistente.getNumero();
			Integer ddd = clienteFoneExistente.getCodigoDDD();
			if (numero != null && numero.equals(clienteFone.getNumero()) && ddd.equals(clienteFone.getCodigoDDD()) && indexLista != i) {

				throw new NegocioException(ERRO_NEGOCIO_NUMERO_TELEFONE_EXISTENTE, true);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #consultarClientesPessoaJuridica(
	 * java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public Collection<Cliente> consultarClientesPessoaJuridica(String cnpj, String nome) throws NegocioException {

		Criteria criteria = getCriteria();

		if (!StringUtils.isEmpty(cnpj)) {
			String cnpjNumerico = Util.removerCaracteresEspeciais(cnpj);
			criteria.add(Restrictions.eq(CNPJ, cnpjNumerico));
		}

		if (!StringUtils.isEmpty(nome)) {
			criteria.add(Restrictions.sqlRestriction("UPPER(" + Util.removerAcentuacaoQuerySql("CLIE_NM") + ") like '"
					+ Util.removerAcentuacao(Util.formatarTextoConsulta(nome)).toUpperCase() + "'"));
		}

		criteria.createAlias(TIPO_CLIENTE, TIPO_CLIENTE);
		criteria.add(Restrictions.eq("tipoCliente.tipoPessoa.codigo", PESSOA_JURIDICA));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#consultarClientes(java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public List<Cliente> consultarClientes(Long[] chavesPrimarias) throws NegocioException {

		List<Cliente> listaCliente = new ArrayList<>();
		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where ");
			hql.append(" chavePrimaria in (:chavesPrimarias) ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("chavesPrimarias", chavesPrimarias);
			Collection<Cliente> clientes = query.list();
			if ((clientes != null) && (!clientes.isEmpty())) {
				listaCliente.addAll(clientes);
			}
		}
		return listaCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #consultarClientes(java.util.Map)
	 */
	@Override
	public Collection<Cliente> consultarClientes(Map<String, Object> filtro) {
		Criteria criteria = criarCriteriaConsultaClientes(filtro);
		return criteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Cliente> consultarClientesPaginados(Map<String, Object> filtro, int quantidadeMax, int offset) {
		final Criteria criteria = criarCriteriaConsultaClientes(filtro).setMaxResults(quantidadeMax).setFirstResult(offset);
		return criteria.list();
	}

	private Criteria criarCriteriaConsultaClientes(Map<String, Object> filtro) {
		Criteria criteria = getCriteria();

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String nome = (String) filtro.get("nome");
			if (!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.sqlRestriction("UPPER(" + Util.removerAcentuacaoQuerySql("CLIE_NM") + ") like '"
						+ Util.removerAcentuacao(Util.formatarTextoConsulta(nome)).toUpperCase() + "'"));
			}

			String nomeFantasia = (String) filtro.get(NOME_FANTASIA);
			if (!StringUtils.isEmpty(nomeFantasia)) {
				criteria.add(Restrictions.ilike(NOME_FANTASIA, Util.formatarTextoConsulta(nomeFantasia)));
			}

			String qualquerNome = (String) filtro.get(QUALQUER_NOME);
			if (!StringUtils.isEmpty(qualquerNome)) {
				Criterion restricao1 = Restrictions.or(Restrictions.ilike("nome", Util.formatarTextoConsulta(qualquerNome)),
								Restrictions.ilike(NOME_FANTASIA, Util.formatarTextoConsulta(qualquerNome)));
				criteria.add(Restrictions.or(restricao1, Restrictions.ilike("nomeAbreviado", Util.formatarTextoConsulta(qualquerNome))));

			}

			String cnpj = (String) filtro.get(CNPJ);
			if (!StringUtils.isEmpty(cnpj)) {
				String cnpjNumerico = Util.removerCaracteresEspeciais(cnpj);
				criteria.add(Restrictions.eq(CNPJ, cnpjNumerico));
			}

			String cpf = (String) filtro.get(CPF);
			if (!StringUtils.isEmpty(cpf)) {
				String cpfNumerico = Util.removerCaracteresEspeciais(cpf);
				criteria.add(Restrictions.eq(CPF, cpfNumerico));
			}

			String passaporte = (String) filtro.get(PASSAPORTE);
			if (!StringUtils.isEmpty(passaporte)) {
				criteria.add(Restrictions.eq("numeroPassaporte", Util.removerCaracteresEspeciais(passaporte)));
			}

			String email = (String) filtro.get("email");
			if (!StringUtils.isEmpty(email)) {
				criteria.add(Restrictions.ilike("emailPrincipal", email));
			}

			Boolean comGrupoEconomico = (Boolean) filtro.get("comGrupoEconomico");
			if (comGrupoEconomico != null && comGrupoEconomico) {
				Long idGrupoEconomico = (Long) filtro.get("idGrupoEconomico");
				if (idGrupoEconomico != null && idGrupoEconomico > 0) {
					criteria.createAlias(GRUPO_ECONOMICO, GRUPO_ECONOMICO);
					criteria.add(Restrictions.or(Restrictions.eq(GRUPO_ECONOMICO, null),
									Restrictions.eq("grupoEconomico.chvePrimaria", idGrupoEconomico)));
				} else {
					criteria.add(Restrictions.isNull(GRUPO_ECONOMICO));
				}
			}

			String inscricaoEstadual = (String) filtro.get(INSCRICAO_ESTADUAL);
			if (!StringUtils.isEmpty(inscricaoEstadual)) {
				criteria.add(Restrictions.eq(INSCRICAO_ESTADUAL, Util.removerCaracteresEspeciais(inscricaoEstadual)));
			}

			String inscricaoMunicipal = (String) filtro.get(INSCRICAO_MUNICIPAL);
			if (!StringUtils.isEmpty(inscricaoMunicipal)) {
				criteria.add(Restrictions.eq(INSCRICAO_MUNICIPAL, Util.removerCaracteresEspeciais(inscricaoMunicipal)));
			}

			String inscricaoRural = (String) filtro.get(INSCRICAO_RURAL);
			if (!StringUtils.isEmpty(inscricaoRural)) {
				criteria.add(Restrictions.eq(INSCRICAO_RURAL, Util.removerCaracteresEspeciais(inscricaoRural)));
			}

			Long idAtividadeEconomia = (Long) filtro.get("idAtividadeEconomia");
			if ((idAtividadeEconomia != null) && (idAtividadeEconomia > 0)) {
				criteria.createAlias(ATIVIDADE_ECONOMICA, ATIVIDADE_ECONOMICA);
				criteria.add(Restrictions.eq("atividadeEconomica.chavePrimaria", idAtividadeEconomia));
			} else {
				criteria.setFetchMode(ATIVIDADE_ECONOMICA, FetchMode.JOIN);
			}

			String cep = (String) filtro.get(CEP);
			if (!StringUtils.isEmpty(cep)) {
				criteria.createCriteria("enderecos").add(Restrictions.eq("indicadorPrincipal", 1)).createCriteria(CEP)
						.add(Restrictions.eq(CEP, cep));
			}

			Integer codigoTipoPessoa = (Integer) filtro.get("codigoTipoPessoa");
			if (codigoTipoPessoa != null) {
				criteria.add(Restrictions.eq("tipoCliente.tipoPessoa.codigo", codigoTipoPessoa));
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}

			Long unidadeFederacao = (Long) filtro.get("idUnidadeFederacao");
			if ((unidadeFederacao != null) && (unidadeFederacao > 0)) {
				criteria.createAlias("unidadeFederacao", "unidadeFederacao");
				criteria.add(Restrictions.eq("unidadeFederacao.chavePrimaria", unidadeFederacao));
			}

			Long idTipoCliente = (Long) filtro.get("idTipoCliente");
			if ((idTipoCliente != null) && (idTipoCliente > 0)) {
				criteria.add(Restrictions.eq("tipoCliente.chavePrimaria", idTipoCliente));
			}

			Long idProfissao = (Long) filtro.get("idProfissao");
			if ((idProfissao != null) && (idProfissao > 0)) {
				criteria.createAlias("profissao", "profissao");
				criteria.add(Restrictions.eq("profissao.chavePrimaria", idProfissao));
			}

			Long idClienteSituacao = (Long) filtro.get("idClienteSituacao");
			if ((idClienteSituacao != null) && (idClienteSituacao > 0)) {
				criteria.add(Restrictions.eq("clienteSituacao.chavePrimaria", idClienteSituacao));
			}
			Long idGrupoEconomico = (Long) filtro.get("idGrupoEconomico");
			if ((idGrupoEconomico != null) && (idGrupoEconomico > 0)) {
				criteria.createAlias(GRUPO_ECONOMICO, GRUPO_ECONOMICO);
				criteria.add(Restrictions.eq("grupoEconomico.chavePrimaria", idGrupoEconomico));
			}
			String senha = (String) filtro.get("senha");
			if (!StringUtils.isEmpty(senha)) {
				criteria.add(Restrictions.eq("senha", senha));
			}

		}

		criteria.createAlias(TIPO_CLIENTE, TIPO_CLIENTE);
		criteria.createAlias("clienteSituacao", "clienteSituacao", CriteriaSpecification.LEFT_JOIN);

		criteria.addOrder(Order.asc("nome"));
		criteria.addOrder(Order.asc("tipoCliente.descricao"));
		criteria.addOrder(Order.asc("clienteSituacao.descricao"));
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#consultarClientesContrato(java.util.Map)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<Cliente> consultarClientesContrato(Map<String, Object> filtro) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		String funcionalidade = (String) filtro.get("funcionalidade");
		String nome = (String) filtro.get("nome");
		String nomeFantasia = (String) filtro.get(NOME_FANTASIA);
		String qualquerNome = (String) filtro.get(QUALQUER_NOME);
		String cnpj = (String) filtro.get(CNPJ);
		String cpf = (String) filtro.get(CPF);
		String passaporte = (String) filtro.get(PASSAPORTE);
		String inscricaoEstadual = (String) filtro.get(INSCRICAO_ESTADUAL);
		String inscricaoMunicipal = (String) filtro.get(INSCRICAO_MUNICIPAL);
		String inscricaoRural = (String) filtro.get(INSCRICAO_RURAL);
		String cep = (String) filtro.get(CEP);

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct cliente ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cliente, ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPontoConsumo ");
		hql.append(" left outer join cliente.enderecos as enderecos");
		hql.append(" where ");
		hql.append(" (cliente = contratoPontoConsumo.contrato.clienteAssinatura ");
		hql.append(" or (select count(contratoCliente) from ");
		hql.append(getClasseEntidadeContratoCliente().getSimpleName());
		hql.append(" 	contratoCliente ");
		hql.append("	where ");
		hql.append("	contratoCliente.cliente = cliente ");
		hql.append("	and contratoCliente.pontoConsumo = contratoPontoConsumo.pontoConsumo ");
		hql.append("	and contratoCliente.contrato =  contratoPontoConsumo.contrato) > 0 ) ");

		if ("paradaProgramada".equals(funcionalidade)) {
			hql.append(" and contratoPontoConsumo.indicadorParadaProgramada = true ");
		} else if ("programacaoConsumo".equals(funcionalidade)) {

			hql.append(" and exists (select contratoPontoConsumo from ");
			hql.append(getClasseEntidadeContratoPontoConsumoModalidade().getSimpleName());
			hql.append(" modalidade ");
			hql.append(" where ");
			hql.append(" modalidade.contratoPontoConsumo = contratoPontoConsumo ");
			hql.append(" and modalidade.indicadorProgramacaoConsumo = true)  ");

		}
		if (!StringUtils.isEmpty(nome)) {
			hql.append(" and upper(cliente.nome) like upper(:nome) ");
		}
		if (!StringUtils.isEmpty(qualquerNome)) {
			hql.append(" and (upper(cliente.nome) like upper(:qualquerNome) ");
			hql.append(" or upper(cliente.nomeFantasia) like upper(:qualquerNome) ");
			hql.append(" or upper(cliente.nomeAbreviado) like upper(:qualquerNome)) ");
		}
		if (!StringUtils.isEmpty(nomeFantasia)) {
			hql.append(" and upper(cliente.nomeFantasia) like upper(:nomeFantasia) ");
		}
		if (!StringUtils.isEmpty(cnpj)) {
			cnpj = Util.removerCaracteresEspeciais(cnpj);
			hql.append(" and cliente.cnpj = :cnpj ");
		}
		if (!StringUtils.isEmpty(cpf)) {
			hql.append(" and cliente.cpf = :cpf ");
		}
		if (!StringUtils.isEmpty(passaporte)) {
			hql.append(" and cliente.numeroPassaporte = :passaporte ");
		}
		if (!StringUtils.isEmpty(inscricaoEstadual)) {
			hql.append(" and cliente.inscricaoEstadual = :inscricaoEstadual ");
		}
		if (!StringUtils.isEmpty(inscricaoMunicipal)) {
			hql.append(" and cliente.inscricaoMunicipal = :inscricaoMunicipal ");
		}
		if (!StringUtils.isEmpty(inscricaoRural)) {
			hql.append(" and cliente.inscricaoRural = :inscricaoRural ");
		}
		if (!StringUtils.isEmpty(cep)) {
			hql.append(" and enderecos.cep.cep = :cep ");
		}

		hql.append(" and cliente.habilitado = true ");
		hql.append(" and contratoPontoConsumo.contrato.habilitado = true ");
		hql.append(" and contratoPontoConsumo.contrato.situacao.chavePrimaria = :valorSituacaoContratoAtivo");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (!StringUtils.isEmpty(nome)) {
			query.setString("nome", Util.formatarTextoConsulta(nome));
		}
		if (!StringUtils.isEmpty(qualquerNome)) {
			query.setString(QUALQUER_NOME, Util.formatarTextoConsulta(qualquerNome));
		}
		if (!StringUtils.isEmpty(nomeFantasia)) {
			query.setString(NOME_FANTASIA, Util.formatarTextoConsulta(nomeFantasia));
		}
		if (!StringUtils.isEmpty(cnpj)) {
			query.setString(CNPJ, cnpj);
		}
		if (!StringUtils.isEmpty(cpf)) {
			query.setString(CPF, Util.removerCaracteresEspeciais(cpf));
		}
		if (!StringUtils.isEmpty(passaporte)) {
			query.setString(PASSAPORTE, passaporte);
		}
		if (!StringUtils.isEmpty(inscricaoEstadual)) {
			query.setString(INSCRICAO_ESTADUAL, inscricaoEstadual);
		}
		if (!StringUtils.isEmpty(inscricaoMunicipal)) {
			query.setString(INSCRICAO_MUNICIPAL, inscricaoMunicipal);
		}
		if (!StringUtils.isEmpty(inscricaoRural)) {
			query.setString(INSCRICAO_RURAL, inscricaoRural);
		}
		if (!StringUtils.isEmpty(cep)) {
			query.setString(CEP, cep);
		}

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String valorSituacaoContrato = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);
		Long valorSituacaoContratoLong = null;
		try {
			valorSituacaoContratoLong = Util.converterCampoStringParaValorLong("Situação do Contrato", valorSituacaoContrato);
		} catch(FormatoInvalidoException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException();
		}

		query.setLong("valorSituacaoContratoAtivo", valorSituacaoContratoLong);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#obterClientePrincipal(java.lang.Long)
	 */
	@Override
	public Cliente obterClientePrincipal(Long idPontoConsumo) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select cliente from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cliente, ");
		hql.append(getClasseEntidadeContrato().getSimpleName());
		hql.append(" contrato, ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPontoConsumo ");
		hql.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" where ");
		hql.append(" contratoPontoConsumo.contrato.chavePrimaria = contrato.chavePrimaria ");
		hql.append(" and cliente.chavePrimaria = contrato.clienteAssinatura.chavePrimaria ");
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema)
						ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String situacaoContratoAtivo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
						Constantes.C_CONTRATO_ATIVO);
		hql.append(" and contrato.situacao = ");
		hql.append(situacaoContratoAtivo);
		hql.append(" and contratoPontoConsumo.pontoConsumo.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idPontoConsumo);

		return (Cliente) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterTipoCliente(java.lang.Long)
	 */
	@Override
	public TipoCliente obterTipoCliente(long chavePrimaria) throws NegocioException {

		return (TipoCliente) super.obter(chavePrimaria, getClasseEntidadeTipoCliente());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterClienteSituacao(long)
	 */
	@Override
	public ClienteSituacao obterClienteSituacao(long chavePrimaria) throws NegocioException {

		return (ClienteSituacao) super.obter(chavePrimaria, getClasseEntidadeClienteSituacao());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterProfissao(long)
	 */
	@Override
	public Profissao obterProfissao(long chavePrimaria) throws NegocioException {

		return (Profissao) super.obter(chavePrimaria, getClasseEntidadeProfissao());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterFaixaRendaFamiliar(long)
	 */
	@Override
	public FaixaRendaFamiliar obterFaixaRendaFamiliar(long chavePrimaria) throws NegocioException {

		return (FaixaRendaFamiliar) super.obter(chavePrimaria, getClasseEntidadeFaixaRendaFamiliar());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterNacionalidade(long)
	 */
	@Override
	public Nacionalidade obterNacionalidade(long chavePrimaria) throws NegocioException {

		return (Nacionalidade) super.obter(chavePrimaria, getClasseEntidadeNacionalidade());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterAtividadeEconomica(long)
	 */
	@Override
	public AtividadeEconomica obterAtividadeEconomica(long chavePrimaria) throws NegocioException {

		return (AtividadeEconomica) super.obter(chavePrimaria, getClasseEntidadeAtividadeEconomica());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterOrgaoExpedidor(long)
	 */
	@Override
	public OrgaoExpedidor obterOrgaoExpedidor(long chavePrimaria) throws NegocioException {

		return (OrgaoExpedidor) super.obter(chavePrimaria, getClasseEntidadeOrgaoExpedidor());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterPessoaSexo(long)
	 */
	@Override
	public PessoaSexo obterPessoaSexo(long chavePrimaria) throws NegocioException {

		return (PessoaSexo) super.obter(chavePrimaria, getClasseEntidadePessoaSexo());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarPessoaSexo()
	 */
	@Override
	public Collection<PessoaSexo> listarPessoaSexo() throws NegocioException {

		return montarConsulta(getClasseEntidadePessoaSexo()).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		this.validarDadosDoCliente((Cliente) (entidadeNegocio));
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if (!(entidadeNegocio instanceof ContratoPontoConsumoImpl)) {
			Cliente cliente = (Cliente) (entidadeNegocio);
			this.validarDadosDoCliente(cliente);
			this.validarAlteracaoTipoCliente(cliente.getChavePrimaria(), cliente.getTipoCliente().getChavePrimaria());
		}
	}

	/**
	 * Validar dados do cliente.
	 *
	 * @param cliente
	 *            the cliente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosDoCliente(Cliente cliente) throws NegocioException {

		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();
		ControladorEndereco controladorEndereco = ServiceLocator.getInstancia().getControladorEndereco();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		ParametroSistema parametroSistema;

		TipoCliente tipoCliente = cliente.getTipoCliente();

		 
		parametroSistema = controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_PRINCIPAL_OBRIGATORIO_CADASTRO_PESSOA);

		if (parametroSistema.getValorInteger() != 0 || StringUtils.isNotEmpty(cliente.getEmailPrincipal())) {
			validarEmailsConcatenados(cliente.getEmailPrincipal(), Cliente.EMAIL_PRINCIPAL);
		}


		if(StringUtils.isNotEmpty(cliente.getEmailSecundario())){
			validarEmailsConcatenados(cliente.getEmailSecundario(), Cliente.EMAIL_SECUNDARIO);
		}

		Collection<ClienteEndereco> enderecos = cliente.getEnderecos();
		if (enderecos == null || enderecos.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_CLIENTE_SEM_ENDERECO, true);
		}else{
			Boolean temEnderecoPrincipal = Boolean.FALSE;
			for(ClienteEndereco clienteEndereco : enderecos){
				if(clienteEndereco.getIndicadorPrincipal() == 1){
					temEnderecoPrincipal = Boolean.TRUE;
				}
			}
			if(!temEnderecoPrincipal){
				throw new NegocioException(
								ERRO_NEGOCIO_CLIENTE_SEM_ENDERECO_PRINCIPAL, true);
			}
		}

		if (tipoCliente != null) {
			if (tipoCliente.getTipoPessoa() == null) {
				tipoCliente = obterTipoCliente(tipoCliente.getChavePrimaria());
			}
			TipoPessoa tipoPessoa = tipoCliente.getTipoPessoa();
			if (tipoPessoa.getCodigo() == PESSOA_FISICA) {

				cliente.setRegimeRecolhimento(null);
				cliente.setInscricaoEstadual(null);
				Date dataAtual = Calendar.getInstance().getTime();
				Date dataEmissaoRG = null;
				Date dataNascimento = null;
				if (cliente.getDataEmissaoRG() != null) {
					dataEmissaoRG = cliente.getDataEmissaoRG();
				}
				if (cliente.getDataNascimento() != null) {
					dataNascimento = cliente.getDataNascimento();
				}

				if (dataEmissaoRG != null && dataAtual.before(dataEmissaoRG)) {
					throw new NegocioException(ERRO_NEGOCIO_DATA_MAIOR_QUE_DATA_ATUAL, Cliente.DATA_EMISSAO_RG);
				}

				if (dataNascimento != null && dataAtual.before(dataNascimento)) {
					throw new NegocioException(ERRO_NEGOCIO_DATA_MAIOR_QUE_DATA_ATUAL, Cliente.DATA_NASCIMENTO);
				} else if (dataEmissaoRG != null && (dataNascimento != null && dataEmissaoRG.before(dataNascimento))) {
					throw new NegocioException(ERRO_NEGOCIO_DATA_EMISSAO_ANTERIOR_DATA_NASCIMENTO, Cliente.DATA_EMISSAO_RG);
				}

				try {
					this.validarCpfEverificarClienteExistente(cliente);
				} catch (FormatoInvalidoException e) {
					LOG.error(e.getStackTrace(), e);
					throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.CPF);
				}

				try {
					this.validarRgCliente(cliente);
				} catch (FormatoInvalidoException e) {
					LOG.error(e.getMessage(), e);
					throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.RG);
				}

			} else {
				Cliente clienteResponsavel = cliente.getClienteResponsavel();
				if ((clienteResponsavel != null)
								&& (cliente.equals(clienteResponsavel) || (cliente.getCnpj().equals(clienteResponsavel.getCnpj())
												&& cliente.getNome().equals(clienteResponsavel.getNome())))) {
					throw new NegocioException(ERRO_NEGOCIO_CLIENTE_RESPONSAVEL_POR_ELE_MESMO, true);
				}
			}
			try {
				this.validarCnpjEverificarClienteExistente(cliente);
			} catch (FormatoInvalidoException e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.CNPJ);
			}

			//Validar o preenchimento de algum dado da Inscrição Estadual
			
			if(tipoPessoa.getDescricao().equalsIgnoreCase(TipoPessoa.DESCRICAO_PESSOA_JURIDICA) && 
					(cliente.getInscricaoEstadual() == null || "".equals(cliente.getInscricaoEstadual())) &&
					(cliente.getRegimeRecolhimento() == null || "".equals(cliente.getRegimeRecolhimento()))) {
				
				throw new NegocioException(Constantes.ERRO_DADOS_FALTANTES, Cliente.INSCRICAO_ESTADUAL);
				
			}
			
			try {
				if (cliente.getInscricaoEstadual() != null && !"null".equalsIgnoreCase(cliente.getInscricaoEstadual())
								&& !"isento".equalsIgnoreCase(cliente.getInscricaoEstadual()) && !"".equals(cliente.getInscricaoEstadual())) {

					Map<String, Object> filtro = new HashMap<>();
					filtro.put("principal", Boolean.TRUE);
					Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);


					String uf = this.encontrarUfDeClienteDaEmpresa(listaEmpresas, controladorEndereco);

					tryObterParametroCodigo(controladorParametroSistema, cliente.getInscricaoEstadual(), uf);
				}

			} catch (GGASException e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, Cliente.INSCRICAO_ESTADUAL);
			}
		}
	}

	private void tryObterParametroCodigo(ControladorParametroSistema controladorParametroSistema, String inscricaoEstadual, String uf)
			throws NegocioException {
		ParametroSistema parametroSistema;
		try {
			parametroSistema = controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_VALIDAR_MASCARA_INSCRICAO_ESTADUAL);

			if (!"".equals(uf) && (parametroSistema != null && Constantes.ATIVO.equals(parametroSistema.getValor()))) {
				if ("AL".equals(uf)) {
					Util.validarInscricaoEstadualAL(inscricaoEstadual.substring(0, INDICE_FINAL_INSC_EST_AL));
				} else if ("PR".equals(uf)) {
					Util.validarInscricaoEstadualPR(inscricaoEstadual);
				} else if ("BA".equals(uf)) {
					Util.validarInscricaoEstadualBA(inscricaoEstadual);
				} else {
					Util.validarInscricaoEstadual(inscricaoEstadual, uf);
				}
			}

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS, Constantes.INSCRICAO_ESTADUAL);
		}
	}

	private void validarEmailsConcatenados(String lista, String campo) throws NegocioException {
		if(StringUtils.isEmpty(lista)) {
			throw new NegocioException(ERRO_NEGOCIO_EMAIL_INVALIDO, campo);
		} else {
			String[] emails = lista.split(SEPARADOR_EMAILS);
			for (String email : emails) {
				if (!Util.validarDominio(email, Constantes.EXPRESSAO_REGULAR_EMAIL)) {
					throw new NegocioException(ERRO_NEGOCIO_EMAIL_INVALIDO, campo);
				}
			}
		}
	}

	private void validarRgCliente(Cliente cliente) throws FormatoInvalidoException{
		if (StringUtils.isNotEmpty(cliente.getRg()) && !"null".equalsIgnoreCase(cliente.getRg())) {
			Util.validarRG(cliente.getRg());
		}
	}

	private void validarCnpjEverificarClienteExistente(Cliente cliente) throws FormatoInvalidoException, NegocioException{

		if (StringUtils.isNotEmpty(cliente.getCnpj()) && !"null".equalsIgnoreCase(cliente.getCnpj())) {
			Util.validarCNPJ(cliente.getCnpj());
			this.verificarClienteExistente(cliente);
		}
	}

	private void validarCpfEverificarClienteExistente(Cliente cliente) throws FormatoInvalidoException, NegocioException{
		if (StringUtils.isNotEmpty(cliente.getCpf())) {
			Util.validarCPF(cliente.getCpf());
			this.verificarClienteExistente(cliente);
		}
	}

	private String encontrarUfDeClienteDaEmpresa(Collection<Empresa> listaEmpresas,  ControladorEndereco controladorEndereco)
			throws NegocioException {

		String uf = "";

		if ((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {

			Empresa empresaPrincipal = ((List<Empresa>) listaEmpresas).get(0);
			Cliente clienteEmpresa = empresaPrincipal.getCliente();

			if (clienteEmpresa.getEnderecoPrincipal() != null) {

				Cep cep = clienteEmpresa.getEnderecoPrincipal().getCep();
				cep = controladorEndereco.obterCepPorChave(cep.getChavePrimaria());
				uf = cep.getUf();
			}
		}

		return uf;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#verificarClienteExistente(br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public void verificarClienteExistente(Cliente cliente) throws NegocioException {

		Integer quantidadeClientes = null;
		Query query = null;
		Long resultado = 0L;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(cliente.chavePrimaria) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cliente ");
		hql.append(" where ");
		if (StringUtils.isEmpty(cliente.getCpf())) {
			hql.append(" cliente.cnpj = ? ");
		} else {
			hql.append(" cliente.cpf = ? ");
		}
		if (cliente.getChavePrimaria() > 0) {
			hql.append(" and cliente.chavePrimaria != ?");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if (StringUtils.isEmpty(cliente.getCpf())) {
			query.setString(0, cliente.getCnpj());
		} else {
			query.setString(0, cliente.getCpf());
		}
		if (cliente.getChavePrimaria() > 0) {
			query.setLong(1, cliente.getChavePrimaria());
		}

		resultado = (Long) query.uniqueResult();
		quantidadeClientes = resultado.intValue();

		if (quantidadeClientes != null && quantidadeClientes > 0) {
			if (StringUtils.isEmpty(cliente.getCpf())) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CLIENTE_CNPJ_DUPLICADO, Constantes.CNPJ);
			} else {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CLIENTE_CPF_DUPLICADO, Constantes.CPF);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #validarAlteracaoTipoCliente(long,
	 * long)
	 */
	@Override
	public void validarAlteracaoTipoCliente(long chavePrimaria, long chavePrimariaTipoCliente) throws NegocioException {

		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();

		TipoCliente tipoCliente = this.obterTipoCliente(chavePrimariaTipoCliente);

		Cliente cliente = (Cliente) this.obter(chavePrimaria);

		Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
		sessao.evict(cliente);

		if (tipoCliente.getTipoPessoa().getCodigo() == PESSOA_FISICA
						&& cliente.getTipoCliente().getTipoPessoa().getCodigo() == PESSOA_JURIDICA) {

			Map<String, Object> filtro = new HashMap<>();
			filtro.put("idCliente", chavePrimaria);
			Collection<Empresa> empresas = controladorEmpresa.consultarEmpresas(filtro);
			if (empresas != null && !empresas.isEmpty()) {
				throw new NegocioException(ControladorCliente.ERRO_NEGOCIO_ALTERACAO_TIPO_CLIENTE_NAO_PERMITIDA, true);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #consultarAtividadeEconomica(java
	 * .util.Map)
	 */
	@Override
	public Collection<AtividadeEconomica> consultarAtividadeEconomica(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeAtividadeEconomica());

		if (filtro != null) {
			String codigo = (String) filtro.get("codigo");
			if (!StringUtils.isEmpty(codigo)) {
				criteria.add(Restrictions.ilike("codigo", Util.formatarTextoConsulta(codigo)));
			}

			Long codigoCNAE = (Long) filtro.get("codigoCNAE");
			if (codigoCNAE != null) {
				criteria.add(Restrictions.eq("codigoOriginal", codigoCNAE));
			}

			String descricao = (String) filtro.get(DESCRICAO);
			if (!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}
		}
		criteria.addOrder(Order.asc(DESCRICAO));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterClientePorContratoPontoConsumo
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Cliente obterClientePorPontoConsumo(Long idPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select contr.clienteAssinatura from ");
		hql.append(getClasseEntidadeContrato().getSimpleName());
		hql.append(" contr ");
		hql.append(" inner join fetch contr.clienteAssinatura.enderecos enderecos ");
		hql.append(" where ");
		hql.append(" contr.chavePrimaria = ( ");
		hql.append(" select contratoPonto.contrato.chavePrimaria from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" where ");
		hql.append(" contratoPonto.contrato.chavePrimaria = contr.chavePrimaria ");
		hql.append(" and contratoPonto.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = 1 ");
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(ID_PONTO_CONSUMO, idPontoConsumo);

		return (Cliente) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #
	 * obterClientePorPontoConsumoDeContratoAtivo(java
	 * .lang.Long)
	 */
	@Override
	public Cliente obterClientePorPontoConsumoDeContratoAtivo(Long idPontoConsumo) throws NegocioException {
		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String situacaoAtivaContrato = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);

		StringBuilder hql = new StringBuilder();
		hql.append(" select contr.clienteAssinatura from ");
		hql.append(getClasseEntidadeContrato().getSimpleName());
		hql.append(" contr ");
		hql.append(" where ");
		hql.append(" contr.chavePrimaria = ( ");
		hql.append(" select contratoPonto.contrato.chavePrimaria from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" where ");
		hql.append(" contratoPonto.contrato.chavePrimaria = contr.chavePrimaria ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = :idSituacao ");
		hql.append(" and contratoPonto.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and contratoPonto.contrato.habilitado = true ");
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter(ID_PONTO_CONSUMO, idPontoConsumo);
		query.setParameter("idSituacao", Long.valueOf(situacaoAtivaContrato));

		return (Cliente) query.uniqueResult();
	}
	
	
	@Override
	@Cacheable("clienteDeContratoAtivo")
	public Cliente obterChaveClientePorPontoConsumoDeContratoAtivo(Long idPontoConsumo) throws NegocioException {

		String situacaoAtivaContrato = ServiceLocator.getInstancia().getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);

		StringBuilder hql = new StringBuilder();
		hql.append(" select contr.clienteAssinatura.chavePrimaria as chavePrimaria from ");
		hql.append(getClasseEntidadeContrato().getSimpleName());
		hql.append(" contr ");
		hql.append(" where ");
		hql.append(" contr.chavePrimaria = ( ");
		hql.append(" select contratoPonto.contrato.chavePrimaria from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" where ");
		hql.append(" contratoPonto.contrato.chavePrimaria = contr.chavePrimaria ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = :idSituacao ");
		hql.append(" and contratoPonto.pontoConsumo.chavePrimaria = :idPontoConsumo ");

		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidade()));
		query.setParameter(ID_PONTO_CONSUMO, idPontoConsumo);
		query.setParameter("idSituacao", Long.valueOf(situacaoAtivaContrato));

		return (Cliente) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterClienteAssinaturaPeloNumeroContrato
	 * (java.lang.String)
	 */
	@Override
	public Cliente obterClienteAssinaturaPeloNumeroContrato(String numeroContrato) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select contr.clienteAssinatura from ");
		hql.append(getClasseEntidadeContrato().getSimpleName());
		hql.append(" contr ");
		hql.append(" where ");
		hql.append(" cast((contr.anoContrato*100000)+contr.numero, string) = :numeroContrato ");
		hql.append(" and contr.habilitado = :habilitado ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("numeroContrato", numeroContrato);
		query.setBoolean(HABILITADO, true);

		return (Cliente) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterClienteAssinaturaPeloNumeroContrato
	 * (java.lang.String)
	 */
	@Override
	public Cliente obterClienteAssinaturaPeloIdContrato(Long idContrato) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select contr.clienteAssinatura from ");
		hql.append(getClasseEntidadeContrato().getSimpleName());
		hql.append(" contr ");
		hql.append(" where ");
		hql.append(" contr.chavePrimaria = :idContrato ");
		hql.append(" and contr.habilitado = :habilitado ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idContrato", idContrato);
		query.setBoolean(HABILITADO, true);

		return (Cliente) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarClientesComFaturasQuitadas()
	 */
	@Override
	public Collection<Cliente> listarClientesParaFaturasSituacaoPagamento(String anoFaturas, Long[] situacaoPagamento,
					Long[] tiposDocumento, Boolean anosAnteriores) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cliente ");
		hql.append(" where ");
		hql.append(" cliente.chavePrimaria in ( ");
		hql.append(" select fatura.cliente.chavePrimaria ");
		hql.append(" from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");
		hql.append(" where ");
		if (anosAnteriores == null) {
			hql.append(" cast(year(fatura.dataVencimento) as string) <= :anoFaturas ");
		} else if (anosAnteriores) {
			hql.append(" cast(year(fatura.dataVencimento) as string) < :anoFaturas ");
		} else {
			hql.append(" cast(year(fatura.dataVencimento) as string) = :anoFaturas ");
		}
		hql.append(" and fatura.situacaoPagamento.chavePrimaria in (:situacaoPagamento) ");
		hql.append(" and fatura.tipoDocumento.chavePrimaria in (:tiposDocumento) ");
		hql.append(" group by fatura.cliente.chavePrimaria ) ");
		hql.append(" order by cliente.chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("anoFaturas", anoFaturas);
		query.setParameterList("situacaoPagamento", situacaoPagamento);
		query.setParameterList("tiposDocumento", tiposDocumento);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterDadosRelatorioCodigoBarrasEndereco
	 * ()
	 */
	@Override
	public Map<String, Object> obterDadosRelatorioCodigoBarrasEndereco(Cliente cliente) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		Map<String, Object> filtro = new HashMap<>();

		Collection<ClienteEndereco> enderecos = cliente.getEnderecos();

		for (ClienteEndereco clienteEndereco : enderecos) {
			if (clienteEndereco.getCorrespondencia()) {

				String codigoBarra = Util.calculoCodigoBarrasEndereco(clienteEndereco.getCep().getCep());

				filtro.put("nomeDestinatario", cliente.getNome());
				filtro.put("ruaNumeroComplemento", clienteEndereco.getEnderecoFormatadoRuaNumeroComplemento());
				filtro.put("bairro", clienteEndereco.getCep().getBairro());
				filtro.put("municipioUF", clienteEndereco.getEnderecoFormatadoMunicipioUF());
				filtro.put(CEP, clienteEndereco.getCep());
				filtro.put("codigoBarraEndereco", codigoBarra);

				break;
			}

		}

		return filtro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterDadosRelatorioCodigoBarrasEndereco
	 * ()
	 */
	@Override
	public Map<String, Object> obterDadosRelatorioCodigoBarrasEndereco(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException {

		Map<String, Object> filtro = new HashMap<>();

		String digitoVerificador = Util.calculoCodigoBarrasEndereco(contratoPontoConsumo.getCep().getCep());

		filtro.put("nomeDestinatario", contratoPontoConsumo.getContrato().getClienteAssinatura().getNome());
		filtro.put("ruaNumeroComplemento", contratoPontoConsumo.getEnderecoFormatadoRuaNumeroComplemento());
		filtro.put("bairro", contratoPontoConsumo.getCep().getBairro());
		filtro.put("MunicipioUF", contratoPontoConsumo.getEnderecoFormatadoMunicipioUF());
		filtro.put(CEP, contratoPontoConsumo.getCep().getCep());
		filtro.put("dv", digitoVerificador);

		return filtro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #consultarClientesComCreditoDebito()
	 */
	@Override
	public Collection<Cliente> consultarClientesComCreditoDebito() throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		ParametroSistema situacao = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL);
		ParametroSistema referencia = controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO);

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct ");
		hql.append(" creditoDebitoARealizar.creditoDebitoNegociado.cliente ");
		hql.append(" from ");
		hql.append(getClasseEntidadeCreditoDebitoARealizar().getSimpleName());
		hql.append(" creditoDebitoARealizar ");

		hql.append(" where ");
		hql.append(" creditoDebitoARealizar.creditoDebitoSituacao.chavePrimaria =  :situacao ");
		hql.append(" and creditoDebitoARealizar.anoMesCobranca is null ");
		hql.append(" and creditoDebitoARealizar.anoMesFaturamento <= :referencia ");
		hql.append(" and creditoDebitoARealizar.creditoDebitoNegociado.pontoConsumo is null ");
		hql.append(" AND creditoDebitoARealizar.creditoDebitoNegociado.dataInicioCobranca <= :dataHoje ");
		hql.append(" and creditoDebitoARealizar.creditoDebitoNegociado.numeroPrestacaoCobrada < "
						+ "creditoDebitoARealizar.creditoDebitoNegociado.quantidadeTotalPrestacoes ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("situacao", Long.parseLong(situacao.getValor()));
		query.setInteger("referencia", Integer.parseInt(referencia.getValor()));
		query.setDate("dataHoje", Calendar.getInstance().getTime());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #obterFatura(long,
	 * java.lang.String[])
	 */
	@Override
	public Cliente obterCliente(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Cliente) super.obter(chavePrimaria, getClasseEntidade(), propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ControladorCliente
	 * #listarEsferaPoder()
	 */
	@Override
	public Collection<EsferaPoder> listarEsferaPoder() throws NegocioException {

		Criteria criteria = this.createCriteria(EsferaPoder.class);
		criteria.add(Restrictions.eq(HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc(DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#consultarClienteEndereco(java.util.Map)
	 */
	@Override
	public Collection<ClienteEndereco> consultarClienteEndereco(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeClienteEndereco());
		if (filtro != null) {

			Long idMunicipio = (Long) filtro.get("idMunicipio");
			if (idMunicipio != null && idMunicipio > 0) {
				criteria.add(Restrictions.eq("municipio.chavePrimaria", idMunicipio));
			}
			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}

			Long idTipoEndereco = (Long) filtro.get("idTipoEndereco");
			if (idTipoEndereco != null && idTipoEndereco > 0) {
				criteria.add(Restrictions.eq("tipoEndereco.chavePrimaria", idTipoEndereco));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#consultarContatoCliente(java.util.Map)
	 */
	@Override
	public Collection<ContatoCliente> consultarContatoCliente(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeContatoCliente());
		if (filtro != null) {

			Long idProfissao = (Long) filtro.get("idProfissao");
			if (idProfissao != null && idProfissao > 0) {
				criteria.add(Restrictions.eq("profissao.chavePrimaria", idProfissao));
			}

			Long idContatoTipo = (Long) filtro.get("idContatoTipo");
			if (idContatoTipo != null && idContatoTipo > 0) {
				criteria.add(Restrictions.eq("tipoContato.chavePrimaria", idContatoTipo));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#consultarClienteTelefone(java.util.Map)
	 */
	@Override
	public Collection<ClienteFone> consultarClienteTelefone(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeClienteTelefone());
		if (filtro != null) {

			Long idTipoTelefone = (Long) filtro.get("idTipoTelefone");
			if (idTipoTelefone != null && idTipoTelefone > 0) {
				criteria.add(Restrictions.eq("tipoFone.chavePrimaria", idTipoTelefone));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#inserirCliente(br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public Long inserirCliente(Cliente cliente) throws GGASException {

		ControladorIntegracao controladorIntegracao = ServiceLocator.getInstancia().getControladorIntegracao();

		if (cliente.getFones() == null || cliente.getFones().isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_EMPTY_FONE, true);
		}

		Long chavePrimaria = null;
		chavePrimaria = this.inserir(cliente);
		controladorIntegracao.inserirIntegracaoCliente(cliente);
		return chavePrimaria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#inserirChamadoAlteracaoCliente(br.com.ggas.cadastro.cliente.dominio.
	 * ChamadoAlteracaoCliente)
	 */
	@Override
	public Long inserirChamadoAlteracaoCliente(ChamadoAlteracaoCliente chamadoAlteracaoCliente) throws GGASException {

		Long chavePrimaria = null;
		chavePrimaria = this.inserir(chamadoAlteracaoCliente);
		return chavePrimaria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#atualizarCliente(br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public void atualizarCliente(Cliente cliente) throws GGASException {
		atualizarCliente(cliente, true);
	}

	/**
	 * Atualizar Cliente
	 *
	 * @param cliente
	 * @param atualizarContrato
	 * @throws GGASException
	 */
	public void atualizarCliente(Cliente cliente, boolean atualizarContrato) throws GGASException {
		ControladorIntegracao controladorIntegracao = ServiceLocator.getInstancia().getControladorIntegracao();
		ServiceLocator.getInstancia().getControladorContrato();

		if (cliente.getFones() == null || cliente.getFones().isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_EMPTY_FONE, true);
		}

		this.atualizar(cliente, ClienteImpl.class);
		if (cliente.getEnderecoPrincipal() != null && cliente.getEnderecoPrincipal().getCep() != null && atualizarContrato) {
			Collection<ContratoPontoConsumo> listaContratoPontoConsumoCliente = consultarContratoPontoConsumoPorCliente(cliente
							.getChavePrimaria());

			for (ContratoPontoConsumo contratoPontoConsumoCurrent : listaContratoPontoConsumoCliente) {
				contratoPontoConsumoCurrent.setCep(cliente.getEnderecoPrincipal().getCep());
				contratoPontoConsumoCurrent.setComplementoEndereco(cliente.getEnderecoPrincipal().getComplemento());
				contratoPontoConsumoCurrent.setNumeroImovel(cliente.getEnderecoPrincipal().getNumero());
				getHibernateTemplate().getSessionFactory().getCurrentSession().clear();
				super.atualizar(contratoPontoConsumoCurrent, contratoPontoConsumoCurrent.getClass());
			}
		}

		controladorIntegracao.atualizarIntegracaoCliente(cliente);
	}

	/**
	 * Consultar contrato ponto consumo por cliente.
	 *
	 * @param idCliente
	 *            the id cliente
	 * @return the collection
	 */
	private Collection<ContratoPontoConsumo> consultarContratoPontoConsumoPorCliente(Long idCliente) {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		
		String situacao = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);
		
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPontoConsumo ");
		hql.append(" inner join fetch contratoPontoConsumo.contrato.clienteAssinatura.enderecos endereco ");
		hql.append(" inner join fetch contratoPontoConsumo.contrato contrato ");
		hql.append(" where ");
		hql.append(" endereco.indicadorPrincipal = 1 ");
		hql.append(" and contratoPontoConsumo.contrato.clienteAssinatura.chavePrimaria = ? ");
		hql.append(" and contrato.habilitado = true ");
		hql.append(" and contrato.situacao.chavePrimaria = ? ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, idCliente);
		query.setLong(1, Long.parseLong(situacao));

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#removerClientes(java.lang.Long[], br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerClientes(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		ControladorIntegracao controladorIntegracao = ServiceLocator.getInstancia().getControladorIntegracao();

		List<Cliente> listaCliente = this.consultarClientes(chavesPrimarias);
		for (Cliente cliente : listaCliente) {
			controladorIntegracao.removerIntegracaoCliente(cliente);
			cliente.setDadosAuditoria(dadosAuditoria);
			super.remover(cliente, ClienteImpl.class);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#consultarClientePorTelefone(java.lang.String)
	 */
	@Override
	public Cliente consultarClientePorTelefone(String numeroTelefone) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select telefone.clienteResponsavel ");
		hql.append(" from ");
		hql.append(getClasseEntidadeClienteTelefone().getSimpleName());
		hql.append(" telefone ");
		hql.append(" where telefone.numero = :numeroTelefone ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("numeroTelefone", numeroTelefone);

		return (Cliente) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#obterTipoClientePorIdTipoCliente(java.lang.Long)
	 */
	@Override
	public TipoCliente obterTipoClientePorIdTipoCliente(Long idTipoCliente) {

		Criteria criteria = createCriteria(TipoClienteImpl.class);

		if (idTipoCliente != null && idTipoCliente > 0) {
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idTipoCliente));
		}

		return (TipoCliente) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#listarGrupoEconomico()
	 */
	@Override
	public Collection<GrupoEconomico> listarGrupoEconomico() throws NegocioException {

		Criteria criteria = montarConsulta(GrupoEconomico.class);

		Order ordenacao = Order.asc(DESCRICAO);
		criteria.addOrder(ordenacao);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#obterChamadoAlteracaoClientePorChamado(long, java.lang.String[])
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public ChamadoAlteracaoCliente obterChamadoAlteracaoClientePorChamado(long chamadoChavePrimaria, String... propriedadesLazy)
					throws NegocioException {

		ChamadoAlteracaoCliente chamadoAlteracaoCliente = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeChamadoAlteracaoCliente().getSimpleName());
		hql.append(" chamadoAlteracaoCliente ");
		hql.append(" inner join fetch chamadoAlteracaoCliente.chamado chamado ");
		hql.append(" where ");
		hql.append(" chamado.chavePrimaria = :chamadoChavePrimaria ");
		hql.append(" and chamado.habilitado = :habilitado ");
		hql.append(" and chamadoAlteracaoCliente.habilitado = :habilitado ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chamadoChavePrimaria", chamadoChavePrimaria);
		query.setBoolean(HABILITADO, Boolean.TRUE);

		chamadoAlteracaoCliente = (ChamadoAlteracaoCliente) query.uniqueResult();

		if (chamadoAlteracaoCliente == null) {
			throw new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA, getClasseEntidadeChamadoAlteracaoCliente().getName());
		}

		inicializarLazy(chamadoAlteracaoCliente, propriedadesLazy);

		return chamadoAlteracaoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#obterChamadoAlteracaoCliente(long, java.lang.String[])
	 */
	@Override
	public ChamadoAlteracaoCliente obterChamadoAlteracaoCliente(long chamadoChavePrimaria, String... propriedadesLazy)
					throws NegocioException {

		ChamadoAlteracaoCliente chamadoAlteracaoCliente = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeChamadoAlteracaoCliente().getSimpleName());
		hql.append(" chamadoAlteracaoCliente ");
		hql.append(" inner join fetch chamadoAlteracaoCliente.chamado chamado ");
		hql.append(" where ");
		hql.append(" and chamado.habilitado = :habilitado ");
		hql.append(" and chamadoAlteracaoCliente.habilitado = :habilitado ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chamadoChavePrimaria);
		query.setBoolean(HABILITADO, Boolean.TRUE);

		chamadoAlteracaoCliente = (ChamadoAlteracaoCliente) query.uniqueResult();

		if (chamadoAlteracaoCliente == null) {
			throw new NegocioException(Constantes.ERRO_ENTIDADE_NAO_ENCONTRADA, getClasseEntidadeChamadoAlteracaoCliente().getName());
		}

		inicializarLazy(chamadoAlteracaoCliente, propriedadesLazy);

		return chamadoAlteracaoCliente;
	}

	/**
	 * Obter chamado alteracao cliente fone por cliente fone.
	 *
	 * @param chavePrimariaClienteFone
	 *            the chave primaria cliente fone
	 * @param chamadoChave
	 *            the chamado chave
	 * @return the chamado alteracao cliente telefone
	 */
	@SuppressWarnings("squid:S1192")
	public ChamadoAlteracaoClienteTelefone obterChamadoAlteracaoClienteFonePorClienteFone(Long chavePrimariaClienteFone, Long chamadoChave) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(ChamadoAlteracaoCliente.class.getSimpleName());
		hql.append(" chamadoAlteracaoCliente ");
		hql.append(" inner join fetch chamadoAlteracaoCliente.fonesChamados fonesChamados ");
		hql.append(" inner join fetch chamadoAlteracaoCliente.chamado chamado ");
		hql.append(" where ");
		hql.append(" fonesChamados.clienteFone = :chavePrimaria ");
		hql.append(" and fonesChamados.habilitado = :habilitado ");
		hql.append(" and chamado.chavePrimaria = :chamadoChave ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimariaClienteFone);
		query.setLong(CHAMADO_CHAVE, chamadoChave);
		query.setBoolean(HABILITADO, Boolean.TRUE);

		return (ChamadoAlteracaoClienteTelefone) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#atualizarDependencias(java.util.List, java.lang.Long)
	 */
	@Override
	public void atualizarDependencias(List<Long> listaIdsChamadoClienteFone, Long chamadoChave) throws GGASException {

		if (listaIdsChamadoClienteFone != null && !listaIdsChamadoClienteFone.isEmpty()) {
			for (Long chave : listaIdsChamadoClienteFone) {
				ChamadoAlteracaoClienteTelefone clienteFoneChamado = obterChamadoAlteracaoClienteFonePorClienteFone(chave, chamadoChave);
				clienteFoneChamado.setClienteFone(null);
				clienteFoneChamado.setHabilitado(false);
				atualizar(clienteFoneChamado, ChamadoAlteracaoClienteTelefone.class);
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#obterChamadoAlteracaoClienteContatoPorClienteContato(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public ChamadoAlteracaoClienteContato obterChamadoAlteracaoClienteContatoPorClienteContato(Long chamadoChaveClienteContato,
					Long chamadoChave) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(ChamadoAlteracaoCliente.class.getSimpleName());
		hql.append(" chamadoAlteracaoCliente ");
		hql.append(" inner join fetch chamadoAlteracaoCliente.contatosChamados contatosChamados ");
		hql.append(" inner join fetch chamadoAlteracaoCliente.chamado chamado ");
		hql.append(" where ");
		hql.append(" contatosChamados.contatoCliente = :chavePrimaria ");
		hql.append(" and contatosChamados.habilitado = :habilitado ");
		hql.append(" and chamado.chavePrimaria = :chamadoChave ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chamadoChaveClienteContato);
		query.setLong(CHAMADO_CHAVE, chamadoChave);
		query.setBoolean(HABILITADO, Boolean.TRUE);

		return (ChamadoAlteracaoClienteContato) query.uniqueResult();
	}

	/**
	 * Obter chamado alteracao cliente endereco por cliente endereco.
	 *
	 * @param chamadoChaveClienteEndereco
	 *            the chamado chave cliente endereco
	 * @param chamadoChave
	 *            the chamado chave
	 * @return the chamado alteracao cliente endereco
	 */
	public ChamadoAlteracaoClienteEndereco obterChamadoAlteracaoClienteEnderecoPorClienteEndereco(Long chamadoChaveClienteEndereco,
					Long chamadoChave) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(ChamadoAlteracaoCliente.class.getSimpleName());
		hql.append(" chamadoAlteracaoCliente ");
		hql.append(" inner join fetch chamadoAlteracaoCliente.enderecosChamados enderecosChamados ");
		hql.append(" inner join fetch chamadoAlteracaoCliente.chamado chamado ");
		hql.append(" where ");
		hql.append(" enderecosChamados.clienteEndereco = :chavePrimaria ");
		hql.append(" and enderecosChamados.habilitado = :habilitado ");
		hql.append(" and chamado.chavePrimaria = :chamadoChave ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chamadoChaveClienteEndereco);
		query.setLong(CHAMADO_CHAVE, chamadoChave);
		query.setBoolean(HABILITADO, Boolean.TRUE);

		ChamadoAlteracaoCliente chamadoAlteracaoCliente = (ChamadoAlteracaoCliente) query.uniqueResult();
		ChamadoAlteracaoClienteEndereco chamadoAlteracaoClienteEndereco = null;

		if (chamadoAlteracaoCliente != null && chamadoAlteracaoCliente.getEnderecosChamados() != null
						&& !chamadoAlteracaoCliente.getEnderecosChamados().isEmpty()) {
			chamadoAlteracaoClienteEndereco = chamadoAlteracaoCliente.getEnderecosChamados().iterator().next();
		}

		return chamadoAlteracaoClienteEndereco;
	}

	/**
	 * Obter chamado alteracao cliente endereco por cliente anexo.
	 *
	 * @param chamadoChaveClienteAnexo
	 *            the chamado chave cliente anexo
	 * @param chamadoChave
	 *            the chamado chave
	 * @return the chamado cliente anexo
	 */
	public ChamadoClienteAnexo obterChamadoAlteracaoClienteEnderecoPorClienteAnexo(Long chamadoChaveClienteAnexo, Long chamadoChave) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(ChamadoAlteracaoCliente.class.getSimpleName());
		hql.append(" chamadoAlteracaoCliente ");
		hql.append(" inner join fetch chamadoAlteracaoCliente.anexosChamados anexosChamados ");
		hql.append(" inner join fetch chamadoAlteracaoCliente.chamado chamado ");
		hql.append(" where ");
		hql.append(" anexosChamados.clienteAnexo = :chavePrimaria ");
		hql.append(" and anexosChamados.habilitado = :habilitado ");
		hql.append(" and chamado.chavePrimaria = :chamadoChave ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chamadoChaveClienteAnexo);
		query.setLong(CHAMADO_CHAVE, chamadoChave);
		query.setBoolean(HABILITADO, Boolean.TRUE);

		ChamadoAlteracaoCliente chamadoAlteracaoCliente = (ChamadoAlteracaoCliente) query.uniqueResult();
		ChamadoClienteAnexo chamadoClienteAnexo = null;

		if (chamadoAlteracaoCliente != null && chamadoAlteracaoCliente.getEnderecosChamados() != null
						&& !chamadoAlteracaoCliente.getEnderecosChamados().isEmpty()) {
			chamadoClienteAnexo = chamadoAlteracaoCliente.getAnexosChamados().iterator().next();
		}

		return chamadoClienteAnexo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#atualizarDependenciasChamadoClienteContato(java.util.List, java.lang.Long)
	 */
	@Override
	public void atualizarDependenciasChamadoClienteContato(List<Long> listaIdsChamadoClienteContato, Long chamadoChave)
					throws GGASException {

		if (listaIdsChamadoClienteContato != null && !listaIdsChamadoClienteContato.isEmpty()) {
			for (Long chave : listaIdsChamadoClienteContato) {
				ChamadoAlteracaoClienteContato clienteContatoChamado = obterChamadoAlteracaoClienteContatoPorClienteContato(chave,
								chamadoChave);
				if (clienteContatoChamado != null) {
					clienteContatoChamado.setContatoCliente(null);
					atualizar(clienteContatoChamado, ChamadoAlteracaoClienteContato.class);
				}
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#atualizarDependenciasChamadoClienteEndereco(java.util.List, java.lang.Long)
	 */
	@Override
	public void atualizarDependenciasChamadoClienteEndereco(List<Long> listaIdsChamadoClienteEndereco, Long chamadoChave)
					throws GGASException {

		if (listaIdsChamadoClienteEndereco != null && !listaIdsChamadoClienteEndereco.isEmpty()) {
			for (Long chave : listaIdsChamadoClienteEndereco) {
				ChamadoAlteracaoClienteEndereco clienteEnderecoChamado = this.obterChamadoAlteracaoClienteEnderecoPorClienteEndereco(chave,
								chamadoChave);
				if (clienteEnderecoChamado != null) {
					clienteEnderecoChamado.setClienteEndereco(null);
					atualizar(clienteEnderecoChamado, ChamadoAlteracaoClienteEndereco.class);
				}
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#atualizarDependenciasChamadoClienteAnexo(java.util.List, java.lang.Long)
	 */
	@Override
	public void atualizarDependenciasChamadoClienteAnexo(List<Long> listaIdsChamadoClienteAnexo, Long chamadoChave) throws GGASException {

		if (listaIdsChamadoClienteAnexo != null && !listaIdsChamadoClienteAnexo.isEmpty()) {
			for (Long chave : listaIdsChamadoClienteAnexo) {
				ChamadoClienteAnexo chamadoClienteAnexo = this.obterChamadoAlteracaoClienteEnderecoPorClienteAnexo(chave, chamadoChave);
				if (chamadoClienteAnexo != null) {
					chamadoClienteAnexo.setClienteAnexo(null);
					atualizar(chamadoClienteAnexo, ChamadoClienteAnexo.class);
				}
			}

		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#listarClientePorGrupoEconomico(java.lang.Long)
	 */
	@Override
	public Collection<Cliente> listarClientePorGrupoEconomico(Long idGrupoEconomico) throws NegocioException {

		Criteria criteria = getCriteria();
		if (idGrupoEconomico != null && idGrupoEconomico > 0) {
			criteria.add(Restrictions.eq("grupoEconomico.chavePrimaria", idGrupoEconomico));
			criteria.createAlias(CLIENTE_RESPONSAVEL, CLIENTE_RESPONSAVEL, CriteriaSpecification.LEFT_JOIN);
			criteria.createAlias(TIPO_CLIENTE, TIPO_CLIENTE);
		}
		Collection<Cliente> listaCliente = criteria.list();
		if (listaCliente.isEmpty()) {
			return Collections.emptyList();
		}
		criteria.addOrder(Order.asc("nome"));
		criteria.addOrder(Order.asc(NOME_FANTASIA));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#listarClienteSemGrupoEconomico(java.util.Map)
	 */
	@Override
	public Collection<Cliente> listarClienteSemGrupoEconomico(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		criteria.add(Restrictions.eq(GRUPO_ECONOMICO, null));
		criteria.createAlias(CLIENTE_RESPONSAVEL, CLIENTE_RESPONSAVEL, CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias(TIPO_CLIENTE, TIPO_CLIENTE);

		Collection<Cliente> listaCliente = criteria.list();
		if (listaCliente.isEmpty()) {
			return Collections.emptyList();
		}
		criteria.addOrder(Order.asc("nome"));
		criteria.addOrder(Order.asc(NOME_FANTASIA));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#consultarCliente(java.lang.Long[])
	 */
	@Override
	public Collection<Cliente> consultarCliente(Long[] chavesPrimarias) throws NegocioException {

		Criteria criteria = getCriteria();
		if (chavesPrimarias != null && chavesPrimarias.length > 0) {
			criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
		}
		Collection<Cliente> listaCliente = criteria.list();
		if (listaCliente.isEmpty()) {
			return null;
		}
		criteria.addOrder(Order.asc("nome"));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#listarCliente(java.lang.Long[])
	 */
	@Override
	public Collection<Cliente> listarCliente(Long[] chavesPrimarias) throws NegocioException {

		Criteria criteria = getCriteria();
		if (chavesPrimarias != null && chavesPrimarias.length > 0) {
			criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
		}
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#validarNumeroEmpenho(java.lang.Long, java.lang.String)
	 */
	@Override
	public void validarNumeroEmpenho(Long idCliente, String numeroEmpenho) throws NegocioException {

		Cliente cliente = this.obterCliente(idCliente, TIPO_CLIENTE);

		if (StringUtils.isNotEmpty(numeroEmpenho) && cliente.getClientePublico() != null && !cliente.getClientePublico()) {
			throw new NegocioException(Constantes.ERRO_NUMERO_EMPENHO_CLIENTE_NAO_PUBLICO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#verificarEmailPrinciapObrigatorio(java.lang.String)
	 */
	@Override
	public void verificarEmailPrinciapObrigatorio(String emailPrincipal) throws NegocioException {

		boolean exigeMatricula = this.exigeEmailPrincipal();

		if (exigeMatricula && (emailPrincipal == null || emailPrincipal.isEmpty())) {
			throw new NegocioException(Constantes.ERRO_EMAIL_PRINCIPAL_OBRIGATORIO, true);
		}

	}

	/**
	 * Exige email principal.
	 *
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private boolean exigeEmailPrincipal() throws NegocioException {

		boolean retorno = false;

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		String valorParametro = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_EMAIL_PRINCIPAL_OBRIGATORIO_CADASTRO_PESSOA);

		if (valorParametro != null && Integer.parseInt(valorParametro) > 0) {
			retorno = true;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.ControladorCliente#obterClientePorPontoConsumoContrato(long)
	 */
	@Override
	public Cliente obterClientePorPontoConsumoContrato(long idPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select contr.clienteAssinatura from ");
		hql.append(getClasseEntidadeContrato().getSimpleName());
		hql.append(" contr ");
		hql.append(" inner join fetch contr.clienteAssinatura.enderecos enderecos ");
		hql.append(" where ");
		hql.append(" contr.chavePrimaria in ( ");
		hql.append(" select contratoPonto.contrato.chavePrimaria from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" where ");
		hql.append(" contratoPonto.contrato.chavePrimaria = contr.chavePrimaria ");
		hql.append(" and contratoPonto.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_PONTO_CONSUMO, idPontoConsumo);
		return (Cliente) query.uniqueResult();
	}

	/**
	 * Obtem cliente pelo número de determinada fatura
	 * @param idFatura
	 * @return
	 */
	public Cliente obterClientePorFatura(long idFatura) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select cliente from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura inner join fatura.cliente cliente ");
		hql.append("where fatura.chavePrimaria = :idFatura ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idFatura", idFatura);
		return (Cliente) query.uniqueResult();
	}

	/**
	 * Realiza a consulta de clientes por chaves primárias de entidade do tipo ContratoPontoConsumo.
	 * Obtém do ContratoPontoConsumo, apenas a chave primária.
	 *
	 * @param listaContratoPontoConsumo
	 * @return mapa de Cliente por ContratoPontoConsumo
	 */
	@Override
	public Map<ContratoPontoConsumo, Cliente> consultarCliente(Collection<ContratoPontoConsumo> listaContratoPontoConsumo)
					throws NegocioException {

		Map<ContratoPontoConsumo, Cliente> mapaContratoPontoConsumoCliente = new HashMap<>();
		if (listaContratoPontoConsumo != null && !listaContratoPontoConsumo.isEmpty()) {
			Long[] chaves = Util.collectionParaArrayChavesPrimarias(listaContratoPontoConsumo);

			StringBuilder hql = new StringBuilder();
			hql.append(" select contratoPontoConsumo.chavePrimaria, cliente ");
			hql.append(" from ");
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(" contratoPontoConsumo ");
			hql.append(" inner join contratoPontoConsumo.contrato contrato ");
			hql.append(" inner join contrato.clienteAssinatura cliente ");
			hql.append(" where ");
			Map<String, List<Long>> mapaPropriedades = 
					HibernateHqlUtil.adicionarClausulaIn(hql, "contratoPontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chaves));
			hql.append(" and cliente.habilitado = :habilitado ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
			query.setBoolean(HABILITADO, Boolean.TRUE);

			mapaContratoPontoConsumoCliente.putAll(montarMapaClientes(query.list()));
		}
		return mapaContratoPontoConsumoCliente;
	}

	/**
	 * Constrói um mapa de Cliente por ContratoPontoConsumo.
	 * Obtém do ContratoPontoConsumo, apenas a chave primária.
	 *
	 * @param list
	 * @return mapa de Cliente por ContratoPontoConsumo
	 */
	private Map<ContratoPontoConsumo, Cliente> montarMapaClientes(List<Object[]> list) {

		Map<ContratoPontoConsumo, Cliente> mapa = new HashMap<>();
		for (Object[] array : list) {
			Cliente cliente = (Cliente) array[1];
			ContratoPontoConsumo contratoPonto = new ContratoPontoConsumoImpl();
			contratoPonto.setChavePrimaria((Long) array[0]);
			mapa.put(contratoPonto, cliente);
		}
		return mapa;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel #obterTipoBotijao(long)
	 */
	@Override
	public ContatoCliente obterContatoCliente(long chavePrimaria) throws NegocioException {

		return (ContatoCliente) super.obter(chavePrimaria, getClasseEntidadeContatoCliente());
	}

	/**
	 * Listar tipo contado por tipo pessoa do cliente.
	 *
	 * @param idTipoCliente the id tipo cliente
	 * @return the collection
	 * @throws NegocioException
	 * @throws GGASException the GGAS exception
	 */
	@Override
	public Collection<TipoContato> listarTipoContadoPorTipoPessoaDoCliente(Long idTipoCliente) throws NegocioException {

		Collection<TipoContato> listaTipoContato = new ArrayList<>();

		if (idTipoCliente != null && idTipoCliente > 0) {

			Collection<TipoContato> listaTipoContatoAux = listarTipoContato();
			for (TipoContato tipoContato : listaTipoContatoAux) {
				int pessoaTipo = tipoContato.getPessoaTipo();
				if (pessoaTipo == obterTipoCliente(idTipoCliente).getTipoPessoa().getCodigo() || pessoaTipo == 0) {
					listaTipoContato.add(tipoContato);
				}
			}
		}

		return listaTipoContato;
	}

	@Override
	public Collection<Cliente> listarClientesNaoIntegrados() {
		return repositorioCliente.listarClientesNaoIntegrados();
	}

	/**
	 * O cliente por cpf ou cnpj.
	 * 
	 * @param cpfCnpj the cpfCnpj
	 * @return um Cliente
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	@Override
	public Cliente obterClientePorCpfCnpj(String cpfCnpj) throws NegocioException{
		StringBuilder hql = new StringBuilder();
		hql.append("from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" clie ");
		hql.append("where ");
		if(cpfCnpj != null){
			if(cpfCnpj.length() == TAMANHO_CPF){
				hql.append("cpf = :cpfCnpj ");
			}else{
				hql.append("cnpj = :cpfCnpj ");
			}
		}
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("cpfCnpj", cpfCnpj);
		return (Cliente) query.uniqueResult();
	}

}
