/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.cliente.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import br.com.ggas.cadastro.cliente.AtividadeEconomica;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteAnexo;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ClienteSituacao;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.FaixaRendaFamiliar;
import br.com.ggas.cadastro.cliente.Nacionalidade;
import br.com.ggas.cadastro.cliente.OrgaoExpedidor;
import br.com.ggas.cadastro.cliente.PessoaSexo;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoCliente;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável por representar um cliente
 * 
 */
public class ClienteImpl extends EntidadeNegocioImpl implements Cliente {

	private static final int TIPO_PESSOA_JURIDICA = 2;
	
	private static final int TIPO_PESSOA_FISICA = 1;


	private static final int ULTIMO_TIPO_CLIENTE = 5;

	private static final int PRIMEIRO_TIPO_CLIENTE = 2;

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 2472638432785352020L;

	private Cliente clienteResponsavel;

	private String nome;

	private String nomeAbreviado;

	private String emailPrincipal;

	private String emailSecundario;

	private String cpf;

	private String rg;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataEmissaoRG;

	private String numeroPassaporte;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataNascimento;

	private String nomeMae;

	private String nomePai;

	private String nomeFantasia;

	private String cnpj;

	private String inscricaoEstadual;

	private String inscricaoMunicipal;

	private String inscricaoRural;

	private ClienteSituacao clienteSituacao;

	private TipoCliente tipoCliente;

	private OrgaoExpedidor orgaoExpedidor;

	private UnidadeFederacao unidadeFederacao;

	private Nacionalidade nacionalidade;

	private Profissao profissao;

	private PessoaSexo sexo;

	private FaixaRendaFamiliar rendaFamiliar;

	private AtividadeEconomica atividadeEconomica;

	private String regimeRecolhimento;

	private GrupoEconomico grupoEconomico;

	private Collection<ClienteEndereco> enderecos = new HashSet<>();

	private Collection<ClienteFone> fones = new HashSet<>();

	private Collection<ContatoCliente> contatos = new HashSet<>();

	private Collection<ClienteAnexo> anexos = new HashSet<>();

	private String contaAuxiliar;

	private boolean indicadorDenegado;

	private Boolean clientePublico;

	private String senha;

	private Date dataUltimoAcesso;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date dataEnvioBrindes;

	private Funcionario funcionarioFiscal;

	private Funcionario funcionarioVendedor;

	private Segmento segmento;
	
	private boolean indicadorNegativado;
	
	private Cliente cliente;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getClienteResponsavel()
	 */
	@Override
	public Cliente getClienteResponsavel() {

		return clienteResponsavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setClienteResponsavel
	 * (br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public void setClienteResponsavel(Cliente clienteResponsavel) {

		this.clienteResponsavel = clienteResponsavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#getNome
	 * ()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#setNome
	 * (java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getNomeAbreviado()
	 */
	@Override
	public String getNomeAbreviado() {

		return nomeAbreviado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setNomeAbreviado(java.lang.String)
	 */
	@Override
	public void setNomeAbreviado(String nomeAbreviado) {

		this.nomeAbreviado = nomeAbreviado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getEmailPrincipal()
	 */
	@Override
	public String getEmailPrincipal() {

		return emailPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setEmailPrincipal(java.lang.String)
	 */
	@Override
	public void setEmailPrincipal(String emailPrincipal) {

		this.emailPrincipal = emailPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getEmailSecundario()
	 */
	@Override
	public String getEmailSecundario() {

		return emailSecundario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setEmailSecundario(java.lang.String)
	 */
	@Override
	public void setEmailSecundario(String emailSecundario) {

		this.emailSecundario = emailSecundario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#getCpf
	 * ()
	 */
	@Override
	public String getCpf() {

		return cpf;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#setCpf
	 * (java.lang.String)
	 */
	@Override
	public void setCpf(String cpf) {

		this.cpf = cpf;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#getRg
	 * ()
	 */
	@Override
	public String getRg() {

		return rg;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#setRg
	 * (java.lang.String)
	 */
	@Override
	public void setRg(String rg) {

		this.rg = rg;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getDataEmissaoRG()
	 */
	@Override
	public Date getDataEmissaoRG() {
		Date dataEmissaoRGTmp = null;
		if(dataEmissaoRG != null){
			dataEmissaoRGTmp = (Date) dataEmissaoRG.clone();
		}
		return dataEmissaoRGTmp;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setDataEmissaoRG(java.util.Date)
	 */
	@Override
	public void setDataEmissaoRG(Date dataEmissaoRG) {
		if(dataEmissaoRG != null){
			this.dataEmissaoRG = (Date) dataEmissaoRG.clone();
		} else {
			this.dataEmissaoRG = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getNumeroPassaporte()
	 */
	@Override
	public String getNumeroPassaporte() {

		return numeroPassaporte;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setNumeroPassaporte(java.lang.String)
	 */
	@Override
	public void setNumeroPassaporte(String numeroPassaporte) {

		this.numeroPassaporte = numeroPassaporte;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getDataNascimento()
	 */
	@Override
	public Date getDataNascimento() {
		Date dataNascimentoTmp = null;
		if(dataNascimento != null){
			dataNascimentoTmp = (Date) dataNascimento.clone();
		}
		return dataNascimentoTmp;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setDataNascimento(java.util.Date)
	 */
	@Override
	public void setDataNascimento(Date dataNascimento) {
		if(dataNascimento != null){
			this.dataNascimento = (Date) dataNascimento.clone();
		} else {
			this.dataNascimento = null;
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#getNomeMae
	 * ()
	 */
	@Override
	public String getNomeMae() {

		return nomeMae;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#setNomeMae
	 * (java.lang.String)
	 */
	@Override
	public void setNomeMae(String nomeMae) {

		this.nomeMae = nomeMae;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#getNomePai
	 * ()
	 */
	@Override
	public String getNomePai() {

		return nomePai;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#setNomePai
	 * (java.lang.String)
	 */
	@Override
	public void setNomePai(String nomePai) {

		this.nomePai = nomePai;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getNomeFantasia()
	 */
	@Override
	public String getNomeFantasia() {

		return nomeFantasia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setNomeFantasia(java.lang.String)
	 */
	@Override
	public void setNomeFantasia(String nomeFantasia) {

		this.nomeFantasia = nomeFantasia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#getCnpj
	 * ()
	 */
	@Override
	public String getCnpj() {

		return cnpj;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#setCnpj
	 * (java.lang.String)
	 */
	@Override
	public void setCnpj(String cnpj) {

		this.cnpj = cnpj;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getInscricaoEstadual()
	 */
	@Override
	public String getInscricaoEstadual() {

		return inscricaoEstadual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setInscricaoEstadual(java.lang.String)
	 */
	@Override
	public void setInscricaoEstadual(String inscricaoEstadual) {

		this.inscricaoEstadual = inscricaoEstadual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getInscricaoMunicipal()
	 */
	@Override
	public String getInscricaoMunicipal() {

		return inscricaoMunicipal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setInscricaoMunicipal(java.lang.String)
	 */
	@Override
	public void setInscricaoMunicipal(String inscricaoMunicipal) {

		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getInscricaoRural()
	 */
	@Override
	public String getInscricaoRural() {

		return inscricaoRural;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setInscricaoRural(java.lang.String)
	 */
	@Override
	public void setInscricaoRural(String inscricaoRural) {

		this.inscricaoRural = inscricaoRural;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getClienteSituacao()
	 */
	@Override
	public ClienteSituacao getClienteSituacao() {

		return clienteSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setClienteSituacao
	 * (br.com.ggas.cadastro.cliente
	 * .ClienteSituacao)
	 */
	@Override
	public void setClienteSituacao(ClienteSituacao clienteSituacao) {

		this.clienteSituacao = clienteSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getTipoCliente()
	 */
	@Override
	public TipoCliente getTipoCliente() {

		return tipoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setTipoCliente
	 * (br.com.ggas.cadastro.cliente.TipoCliente)
	 */
	@Override
	public void setTipoCliente(TipoCliente tipoCliente) {

		this.tipoCliente = tipoCliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getOrgaoExpedidor()
	 */
	@Override
	public OrgaoExpedidor getOrgaoExpedidor() {

		return orgaoExpedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setOrgaoExpedidor
	 * (br.com.ggas.cadastro.cliente
	 * .OrgaoExpedidor)
	 */
	@Override
	public void setOrgaoExpedidor(OrgaoExpedidor orgaoExpedidor) {

		this.orgaoExpedidor = orgaoExpedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getUnidadeFederacao()
	 */
	@Override
	public UnidadeFederacao getUnidadeFederacao() {

		return unidadeFederacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setUnidadeFederacao
	 * (br.com.ggas.cadastro.geografico
	 * .UnidadeFederacao)
	 */
	@Override
	public void setUnidadeFederacao(UnidadeFederacao unidadeFederacao) {

		this.unidadeFederacao = unidadeFederacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getNacionalidade()
	 */
	@Override
	public Nacionalidade getNacionalidade() {

		return nacionalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setNacionalidade
	 * (br.com.ggas.cadastro.cliente
	 * .Nacionalidade)
	 */
	@Override
	public void setNacionalidade(Nacionalidade nacionalidade) {

		this.nacionalidade = nacionalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getProfissao()
	 */
	@Override
	public Profissao getProfissao() {

		return profissao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setProfissao
	 * (br.com.ggas.cadastro.cliente.Profissao)
	 */
	@Override
	public void setProfissao(Profissao profissao) {

		this.profissao = profissao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#getSexo
	 * ()
	 */
	@Override
	public PessoaSexo getSexo() {

		return sexo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#setSexo
	 * (br.com.ggas.cadastro.cliente.PessoaSexo)
	 */
	@Override
	public void setSexo(PessoaSexo sexo) {

		this.sexo = sexo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getRendaFamiliar()
	 */
	@Override
	public FaixaRendaFamiliar getRendaFamiliar() {

		return rendaFamiliar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setRendaFamiliar
	 * (br.com.ggas.cadastro.cliente
	 * .FaixaRendaFamiliar)
	 */
	@Override
	public void setRendaFamiliar(FaixaRendaFamiliar rendaFamiliar) {

		this.rendaFamiliar = rendaFamiliar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getAtividadeEconomica()
	 */
	@Override
	public AtividadeEconomica getAtividadeEconomica() {

		return atividadeEconomica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setAtividadeEconomica
	 * (br.com.ggas.cadastro.cliente
	 * .AtividadeEconomica)
	 */
	@Override
	public void setAtividadeEconomica(AtividadeEconomica atividadeEconomica) {

		this.atividadeEconomica = atividadeEconomica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getEnderecos()
	 */
	@Override
	public Collection<ClienteEndereco> getEnderecos() {

		return enderecos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setEnderecos(java.util.Collection)
	 */
	@Override
	public void setEnderecos(Collection<ClienteEndereco> enderecos) {

		this.enderecos = enderecos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#getFones
	 * ()
	 */
	@Override
	public Collection<ClienteFone> getFones() {

		return fones;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.Cliente#setFones
	 * (java.util.Collection)
	 */
	@Override
	public void setFones(Collection<ClienteFone> fones) {

		this.fones = fones;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getContatos()
	 */
	@Override
	public Collection<ContatoCliente> getContatos() {

		return contatos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * setContatos(java.util.Collection)
	 */
	@Override
	public void setContatos(Collection<ContatoCliente> contatos) {

		this.contatos = contatos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getCnpjFormatado()
	 */
	@Override
	public String getCnpjFormatado() {

		if (this.cnpj == null) {
			return this.cnpj;
		} else {
			return Util.formatarValor(this.cnpj, Constantes.MASCARA_CNPJ);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getCpfFormatado()
	 */
	@Override
	public String getCpfFormatado() {

		if (this.cpf == null) {
			return this.cpf;
		} else {
			return Util.formatarValor(this.cpf, Constantes.MASCARA_CPF);
		}

	}

	/**
	 * @return the regimeRecolhimento
	 */
	@Override
	public String getRegimeRecolhimento() {

		return regimeRecolhimento;
	}

	/**
	 * @param regimeRecolhimento
	 *            the regimeRecolhimento to set
	 */
	@Override
	public void setRegimeRecolhimento(String regimeRecolhimento) {

		this.regimeRecolhimento = regimeRecolhimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(nome)) {
			if(getTipoCliente() == null) {
				stringBuilder.append(NOME);
			} else if(getTipoCliente().getTipoPessoa().getCodigo() == TIPO_PESSOA_FISICA) {
				stringBuilder.append(NOME);
			} else if(getTipoCliente().getTipoPessoa().getCodigo() == TIPO_PESSOA_JURIDICA) {
				stringBuilder.append(RAZAO_SOCIAL);
			}

			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tipoCliente == null) {
			stringBuilder.append(TIPO_CLIENTE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {

			if(getTipoPessoa(Long.valueOf(tipoCliente.getTipoPessoa().getCodigo())) == TIPO_PESSOA_FISICA) {
				stringBuilder.append(this.validarDadosPessoaFisica());
			} else {
				stringBuilder.append(this.validarDadosPessoaJuridica());
			}

		}
		
		if(segmento == null || segmento.getChavePrimaria() <= 0) {
			stringBuilder.append(SEGMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	/**
	 * Gets the tipo pessoa.
	 * 
	 * @param tipoCliente
	 *            the tipo cliente
	 * @return the tipo pessoa
	 */
	private int getTipoPessoa(Long tipoCliente) {
		int varTipoCliente =Integer.parseInt(tipoCliente.toString());

		if (varTipoCliente >= PRIMEIRO_TIPO_CLIENTE && varTipoCliente <= ULTIMO_TIPO_CLIENTE) {
			return TIPO_PESSOA_JURIDICA;
		} else {
			return TIPO_PESSOA_FISICA;
		}
	}

	/**
	 * Validar dados pessoa juridica.
	 * 
	 * @return the string
	 */
	private String validarDadosPessoaJuridica() {

		StringBuilder stringBuilder = new StringBuilder();

		if(StringUtils.isEmpty(nomeFantasia)) {
			stringBuilder.append(NOME_FANTASIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(StringUtils.isEmpty(cnpj)) {
			stringBuilder.append(CNPJ);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(regimeRecolhimento != null && regimeRecolhimento.equals("0") && inscricaoEstadual == null) {
			stringBuilder.append(INSCRICAO_ESTADUAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		return stringBuilder.toString();
	}

	/**
	 * Validar dados pessoa fisica.
	 * 
	 * @return the string
	 */
	private String validarDadosPessoaFisica() {

		StringBuilder stringBuilder = new StringBuilder();

		if(nacionalidade == null) {
			stringBuilder.append(NACIONALIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if(nacionalidade.isEstrangeira()) {
				if(StringUtils.isEmpty(numeroPassaporte)) {
					stringBuilder.append(PASSAPORTE);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			} else {

				if(StringUtils.isEmpty(cpf)) {
					stringBuilder.append(CPF);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			}
		}

		if(sexo == null) {
			stringBuilder.append(SEXO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		return stringBuilder.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.Cliente#
	 * getEnderecoPrincipal()
	 */
	@Override
	public ClienteEndereco getEnderecoPrincipal() {

		ClienteEndereco enderecoPrincipal = null;
		if (this.getEnderecos() != null && !this.getEnderecos().isEmpty()) {
			for (ClienteEndereco endereco : this.getEnderecos()) {
				if (endereco.getCorrespondencia() != null
						&& endereco.getCorrespondencia()) {
					enderecoPrincipal = endereco;
					break;
				}
			}
		}
		if(enderecoPrincipal == null && this.getEnderecos() != null && !this.getEnderecos().isEmpty()) {
			enderecoPrincipal = this.getEnderecos().iterator().next();
		}
		return enderecoPrincipal;
	}

	@Override
	public String getNumeroDocumentoFormatado() {

		String retorno = "";

		if(!StringUtils.isEmpty(this.getCpf())) {
			retorno = this.getCpfFormatado();
		} else {
			if(!StringUtils.isEmpty(this.getCnpj())) {
				retorno = this.getCnpjFormatado();
			}
		}
		return retorno;

	}

	@Override
	public String getNumeroCpfCnpj() {

		String retorno = "";

		if(!StringUtils.isEmpty(this.getCpf())) {
			retorno = this.getCpf();
		} else {
			if(!StringUtils.isEmpty(this.getCnpj())) {
				retorno = this.getCnpj();
			}
		}
		return retorno;

	}

	@Override
	public String getContaAuxiliar() {

		return contaAuxiliar;
	}

	@Override
	public void setContaAuxiliar(String contaAuxiliar) {

		this.contaAuxiliar = contaAuxiliar;
	}

	@Override
	public GrupoEconomico getGrupoEconomico() {

		return grupoEconomico;
	}

	@Override
	public void setGrupoEconomico(GrupoEconomico grupoEconomico) {

		this.grupoEconomico = grupoEconomico;
	}

	@Override
	public Collection<ClienteAnexo> getAnexos() {

		return anexos;
	}

	@Override
	public void setAnexos(Collection<ClienteAnexo> anexos) {

		this.anexos = anexos;
	}

	@Override
	public boolean isIndicadorDenegado() {

		return indicadorDenegado;
	}

	@Override
	public void setIndicadorDenegado(boolean indicadorDenegado) {

		this.indicadorDenegado = indicadorDenegado;
	}

	@Override
	public Boolean getClientePublico() {

		return clientePublico;
	}

	@Override
	public void setClientePublico(Boolean clientePublico) {

		this.clientePublico = clientePublico;

	}

	@Override
	public String getSenha() {

		return senha;
	}

	@Override
	public void setSenha(String senha) {

		this.senha = senha;
	}

	@Override
	public Date getDataUltimoAcesso() {
		Date dataUltimoAcessoTmp = null;
		if(dataUltimoAcesso != null){
			dataUltimoAcessoTmp = (Date) dataUltimoAcesso.clone();
		}
		return dataUltimoAcessoTmp;
	}

	@Override
	public void setDataUltimoAcesso(Date dataUltimoAcesso) {
		if(dataUltimoAcesso != null){
			this.dataUltimoAcesso = (Date) dataUltimoAcesso.clone();
		} else {
			this.dataUltimoAcesso = null;
		}
	}

	@Override
	public Date getDataEnvioBrindes() {
		Date dataEnvioBrindesTmp = null;
		if(dataEnvioBrindes != null){
			dataEnvioBrindesTmp = (Date) dataEnvioBrindes.clone();
		}
		return dataEnvioBrindesTmp; 
	}

	@Override
	public void setDataEnvioBrindes(Date dataEnvioBrindes) {
		if(dataEnvioBrindes != null){
			this.dataEnvioBrindes = (Date) dataEnvioBrindes.clone();
		} else {
			this.dataEnvioBrindes = null;
		}
		
	}

	@Override
	public Funcionario getFuncionarioVendedor() {

		return funcionarioVendedor;
	}

	@Override
	public void setFuncionarioVendedor(Funcionario funcionarioVendedor) {

		this.funcionarioVendedor = funcionarioVendedor;
	}

	@Override
	public Funcionario getFuncionarioFiscal() {

		return funcionarioFiscal;
	}

	@Override
	public void setFuncionarioFiscal(Funcionario funcionarioFiscal) {

		this.funcionarioFiscal = funcionarioFiscal;
	}

	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	@Override
	public void setEnderecos(ClienteEndereco endereco) {
		enderecos.add(endereco);
		
	}

	public boolean isPessoaFisica() {
		return this.tipoCliente.getTipoPessoa().getCodigo() == TIPO_PESSOA_FISICA && this.cpf != null;
	}
	
	public boolean isPessoaJuridica() {
		return this.tipoCliente.getTipoPessoa().getCodigo() == TIPO_PESSOA_JURIDICA && this.cnpj != null;
	}
	
	public String getDenegadoFormatado(){
		if(indicadorDenegado){
			return "Sim";
		}
		return "Não"; 
	}

	@Override
	public boolean isIndicadorNegativado() {
		return indicadorNegativado;
	}

	@Override
	public void setIndicadorNegativado(boolean indicadorNegativado) {
		this.indicadorNegativado = indicadorNegativado;
	}
	
	@Override
	public String retornarTelefonePrincipal() {
		String telefone = "";
		
		if(fones != null & !fones.isEmpty()) {
			ClienteFone clienteFone = fones.stream().filter(p -> p.getIndicadorPrincipal()).findFirst().orElse(null);
			
			if(clienteFone != null) {
				telefone = "(" + clienteFone.getCodigoDDD() + ") " + clienteFone.getNumero();
			}
		}
		
		return telefone;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}
