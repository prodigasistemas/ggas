/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ClienteFoneImpl representa uma ClienteFoneImpl no sistema.
 *
 * @since 27/08/2009
 * 
 */

package br.com.ggas.cadastro.cliente.impl;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.TipoFone;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * Classe responsável por representar o telefone de um cliente
 *
 */
public class ClienteFoneImpl extends EntidadeNegocioImpl implements ClienteFone {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4018466387143785572L;

	private Integer codigoDDD;

	private Integer numero;

	private Integer ramal;

	private Boolean indicadorPrincipal;

	private TipoFone tipoFone;

	private Cliente clienteResponsavel;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ClienteFone
	 * #getCodigoDDD()
	 */
	@Override
	public Integer getCodigoDDD() {

		return codigoDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ClienteFone
	 * #setCodigoDDD(java.lang.Integer)
	 */
	@Override
	public void setCodigoDDD(Integer codigoDDD) {

		this.codigoDDD = codigoDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ClienteFone
	 * #getNumero()
	 */
	@Override
	public Integer getNumero() {

		return numero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ClienteFone
	 * #setNumero(java.lang.Integer)
	 */
	@Override
	public void setNumero(Integer numero) {

		this.numero = numero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ClienteFone
	 * #getRamal()
	 */
	@Override
	public Integer getRamal() {

		return ramal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ClienteFone
	 * #setRamal(java.lang.Integer)
	 */
	@Override
	public void setRamal(Integer ramal) {

		this.ramal = ramal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ClienteFone
	 * #getIndicadorPrincipal()
	 */
	@Override
	public Boolean getIndicadorPrincipal() {

		return indicadorPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ClienteFone
	 * #setIndicadorPrincipal(java.lang.Boolean)
	 */
	@Override
	public void setIndicadorPrincipal(Boolean indicadorPrincipal) {

		this.indicadorPrincipal = indicadorPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ClienteFone
	 * #getTipoFone()
	 */
	@Override
	public TipoFone getTipoFone() {

		return tipoFone;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ClienteFone
	 * #setTipoFone(br.com.ggas.cadastro.cliente.
	 * TipoFone)
	 */
	@Override
	public void setTipoFone(TipoFone tipoFone) {

		this.tipoFone = tipoFone;
	}

	@Override
	public Cliente getClienteResponsavel() {

		return clienteResponsavel;
	}

	@Override
	public void setClienteResponsavel(Cliente clienteResponsavel) {

		this.clienteResponsavel = clienteResponsavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(tipoFone == null) {
			stringBuilder.append(TIPO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if((codigoDDD == null) || (codigoDDD <= 0)){
			stringBuilder.append(CODIGO_DDD);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if((numero == null) || (numero <= 0)){
			stringBuilder.append(NUMERO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	/**
	 * Necessário reimplementar para utilizar em listas do tipo Set
	 * @return o hashcode baseado no número do telefone
	 */
	@Override
	public int hashCode() {
		if(this.getNumero() != null && this.getNumero() > 0) {
			return this.getNumero().toString().hashCode();
		}
		return super.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		boolean equals;

		if (this == o) {
			equals = true;
		} else if (o == null || getClass() != o.getClass()) {
			equals = false;
		} else {
			ClienteFoneImpl that = (ClienteFoneImpl) o;

			equals = new EqualsBuilder()
					.appendSuper(super.equals(o)).append(codigoDDD, that.codigoDDD).append(numero, that.numero)
					.append(ramal, that.ramal).append(indicadorPrincipal, that.indicadorPrincipal)
					.append(tipoFone, that.tipoFone)
					.append(clienteResponsavel, that.clienteResponsavel)
					.isEquals();
		}

		return equals;
	}
}
