/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 24/01/2014 17:50:21
 @author ccavalcanti
 */

package br.com.ggas.cadastro.cliente.dominio;

import java.util.Map;

import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 *	Classe VO usada para transferencia de dados na classe Servico
 */
public class ChamadoAlteracaoClienteContato extends EntidadeNegocioImpl {

	private static final long serialVersionUID = -4059712899391466217L;

	private TipoContato tipoContato;

	private Profissao profissao;

	private String nome;

	private Integer codigoDDD;

	private Integer fone;

	private Integer ramal;

	private String descricaoArea;

	private String email;

	private boolean principal;

	private ContatoCliente contatoCliente;

	private EntidadeConteudo tipoAcao;

	public TipoContato getTipoContato() {

		return tipoContato;
	}

	public void setTipoContato(TipoContato tipoContato) {

		this.tipoContato = tipoContato;
	}

	public Profissao getProfissao() {

		return profissao;
	}

	public void setProfissao(Profissao profissao) {

		this.profissao = profissao;
	}

	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public Integer getCodigoDDD() {

		return codigoDDD;
	}

	public void setCodigoDDD(Integer codigoDDD) {

		this.codigoDDD = codigoDDD;
	}

	public Integer getFone() {

		return fone;
	}

	public void setFone(Integer fone) {

		this.fone = fone;
	}

	public Integer getRamal() {

		return ramal;
	}

	public void setRamal(Integer ramal) {

		this.ramal = ramal;
	}

	public String getDescricaoArea() {

		return descricaoArea;
	}

	public void setDescricaoArea(String descricaoArea) {

		this.descricaoArea = descricaoArea;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public boolean isPrincipal() {

		return principal;
	}

	public void setPrincipal(boolean principal) {

		this.principal = principal;
	}

	public ContatoCliente getContatoCliente() {

		return contatoCliente;
	}

	public void setContatoCliente(ContatoCliente contatoCliente) {

		this.contatoCliente = contatoCliente;
	}

	public EntidadeConteudo getTipoAcao() {

		return tipoAcao;
	}

	public void setTipoAcao(EntidadeConteudo tipoAcao) {

		this.tipoAcao = tipoAcao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
