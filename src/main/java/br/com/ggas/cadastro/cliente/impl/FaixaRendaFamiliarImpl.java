/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe FaixaRendaFamiliarImpl representa uma FaixaRendaFamiliarImpl no sistema.
 *
 * @since 27/08/2009
 * 
 */

package br.com.ggas.cadastro.cliente.impl;

import java.math.BigDecimal;
import java.util.Map;

import br.com.ggas.cadastro.cliente.FaixaRendaFamiliar;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável por representar a faixa de renda familiar
 *
 */
public class FaixaRendaFamiliarImpl extends EntidadeNegocioImpl implements FaixaRendaFamiliar {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5053455650949584439L;

	private BigDecimal valorFaixaInicio;

	private BigDecimal valorFaixaFim;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * FaixaRendaFamiliar#getValorFaixaInicio()
	 */
	@Override
	public BigDecimal getValorFaixaInicio() {

		return valorFaixaInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * FaixaRendaFamiliar
	 * #setValorFaixaInicio(java.math.BigDecimal)
	 */
	@Override
	public void setValorFaixaInicio(BigDecimal valorFaixaInicio) {

		this.valorFaixaInicio = valorFaixaInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * FaixaRendaFamiliar#getValorFaixaFim()
	 */
	@Override
	public BigDecimal getValorFaixaFim() {

		return valorFaixaFim;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * FaixaRendaFamiliar
	 * #setValorFaixaFim(java.math.BigDecimal)
	 */
	@Override
	public void setValorFaixaFim(BigDecimal valorFaixaFim) {

		this.valorFaixaFim = valorFaixaFim;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

}
