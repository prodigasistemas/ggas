package br.com.ggas.cadastro.cliente.repositorio;

import java.util.Collection;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * 
 * Classe responsável pelas consultas relacionadas ao Cliente
 *
 */
@Repository
public class RepositorioCliente extends RepositorioGenerico {

	private static final int LIMITE_RESULTADOS_RETORNADOS = 2000;
	private static final String FROM = " from ";
	private static final String WHERE = " where ";
	
	/**
	 * Classe de RepositorioCliente
	 * @param sessionFactory the sessionFactory
	 * @return sessionFactory - {@link SessionFactory}
	 */
	@Autowired
	public RepositorioCliente(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}
	
	@Override
	public EntidadeNegocio criar() {
		return new ClienteImpl();
	}

	@Override
	public Class<?> getClasseEntidade() {
		return ClienteImpl.class;
	}
	/**
	 *listarClientesNaoIntegrados
	 *
	 * @return the collection
	 */	
	public Collection<Cliente> listarClientesNaoIntegrados() {
		
		StringBuilder hql = new StringBuilder();
		
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" cliente ");
		
		boolean possuiClientesIntegrados = this.possuiClientesIntegrados();
		
		System.out.println("possuiClientesIntegrados? " + possuiClientesIntegrados);
		if (possuiClientesIntegrados) {
			hql.append(WHERE);
			hql.append(" cliente.chavePrimaria not in (select integracaoCliente.clienteOrigem from IntegracaoClienteImpl integracaoCliente )");
		}
		
		hql.append(" order by cliente.chavePrimaria ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (Collection<Cliente>) query.setMaxResults(LIMITE_RESULTADOS_RETORNADOS).list();
		
	}
	/**
	 * Verifica se há clientes integrados.
	 * 
	 * @return boolean {@link boolean}
	 */
	public boolean possuiClientesIntegrados() {
		
		StringBuilder hql = new StringBuilder();
		
		hql.append(" select count(integracaoCliente.clienteOrigem) ");
		hql.append(FROM);
		hql.append(" IntegracaoClienteImpl integracaoCliente");

		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		Long qtdCLientesIntegrados = (Long) query.uniqueResult(); 
		return qtdCLientesIntegrados != null && qtdCLientesIntegrados > 0;
		
	}

}
