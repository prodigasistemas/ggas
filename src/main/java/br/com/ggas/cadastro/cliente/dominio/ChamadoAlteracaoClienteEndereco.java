/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 24/01/2014 16:46:39
 @author ccavalcanti
 */

package br.com.ggas.cadastro.cliente.dominio;

import java.util.Map;

import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.TipoEndereco;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 *	Classe VO para transferencia de dados ultilizada na classe Serviço
 */
public class ChamadoAlteracaoClienteEndereco extends EntidadeNegocioImpl {

	private static final long serialVersionUID = -3256124632550656907L;

	private Cep cep;

	private TipoEndereco tipoEndereco;

	private Municipio municipio;

	private String numero;

	private String complemento;

	private Integer caixaPostal;

	private String enderecoReferencia;

	private Boolean correspondencia;

	private ClienteEndereco clienteEndereco;

	private EntidadeConteudo tipoAcao;

	public Cep getCep() {

		return cep;
	}

	public void setCep(Cep cep) {

		this.cep = cep;
	}

	public TipoEndereco getTipoEndereco() {

		return tipoEndereco;
	}

	public void setTipoEndereco(TipoEndereco tipoEndereco) {

		this.tipoEndereco = tipoEndereco;
	}

	public Municipio getMunicipio() {

		return municipio;
	}

	public void setMunicipio(Municipio municipio) {

		this.municipio = municipio;
	}

	public String getNumero() {

		return numero;
	}

	public void setNumero(String numero) {

		this.numero = numero;
	}

	public String getComplemento() {

		return complemento;
	}

	public void setComplemento(String complemento) {

		this.complemento = complemento;
	}

	public Integer getCaixaPostal() {

		return caixaPostal;
	}

	public void setCaixaPostal(Integer caixaPostal) {

		this.caixaPostal = caixaPostal;
	}

	public String getEnderecoReferencia() {

		return enderecoReferencia;
	}

	public void setEnderecoReferencia(String enderecoReferencia) {

		this.enderecoReferencia = enderecoReferencia;
	}

	public Boolean getCorrespondencia() {

		return correspondencia;
	}

	public void setCorrespondencia(Boolean correspondencia) {

		this.correspondencia = correspondencia;
	}

	public ClienteEndereco getClienteEndereco() {

		return clienteEndereco;
	}

	public void setClienteEndereco(ClienteEndereco clienteEndereco) {

		this.clienteEndereco = clienteEndereco;
	}

	public EntidadeConteudo getTipoAcao() {

		return tipoAcao;
	}

	public void setTipoAcao(EntidadeConteudo tipoAcao) {

		this.tipoAcao = tipoAcao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
