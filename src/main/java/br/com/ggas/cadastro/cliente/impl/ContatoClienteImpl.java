/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.cliente.impl;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável por representar o contato de um cliente
 */
public class ContatoClienteImpl extends EntidadeNegocioImpl implements ContatoCliente {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 6674507386882569552L;

	private TipoContato tipoContato;

	private String nome;

	private Integer codigoDDD;

	private Integer fone;

	private Integer ramal;

	private Integer codigoDDD2;

	private Integer fone2;

	private Integer ramal2;

	private Profissao profissao;

	private String descricaoArea;

	private String email;

	private String cpf;

	private String rg;

	private boolean principal;

	/**
	 * @return Telefone1Formatado
	 */
	public String getTelefone1Formatado() {

		StringBuilder telefone1 = new StringBuilder();

		if (this.getFone() != null) {

			if (this.getCodigoDDD() != null) {
				telefone1.append("(").append(this.getCodigoDDD()).append(") ");
			}

			telefone1.append(this.getFone());

			if (this.getRamal() != null) {
				telefone1.append(" - ").append(this.getRamal());
			}
		}

		return telefone1.toString();

	}

	/**
	 * @return Telefone2Formatado
	 */
	public String getTelefone2Formatado() {
		StringBuilder telefone2 = new StringBuilder();

		if (this.getFone2() != null) {

			if (this.getCodigoDDD2() != null) {
				telefone2.append("(").append(this.getCodigoDDD2()).append(") ");
			}

			telefone2.append(this.getFone2());

			if (this.getRamal2() != null) {
				telefone2.append(" - ").append(this.getRamal2());
			}
		}

		return telefone2.toString();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#getTipoContato()
	 */
	@Override
	public TipoContato getTipoContato() {

		return tipoContato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente
	 * #setTipoContato(br.com.ggas.cadastro
	 * .cliente.TipoContato)
	 */
	@Override
	public void setTipoContato(TipoContato tipoContato) {

		this.tipoContato = tipoContato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#getNome()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#getCodigoDDD()
	 */
	@Override
	public Integer getCodigoDDD() {

		return codigoDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente
	 * #setCodigoDDD(java.lang.String)
	 */
	@Override
	public void setCodigoDDD(Integer codigoDDD) {

		this.codigoDDD = codigoDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#getFone()
	 */
	@Override
	public Integer getFone() {

		return fone;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#setFone(java.lang.String)
	 */
	@Override
	public void setFone(Integer fone) {

		this.fone = fone;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#getRamal()
	 */
	@Override
	public Integer getRamal() {

		return ramal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#setRamal(java.lang.String)
	 */
	@Override
	public void setRamal(Integer ramal) {

		this.ramal = ramal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#getDescricaoArea()
	 */
	@Override
	public String getDescricaoArea() {

		return descricaoArea;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente
	 * #setDescricaoArea(java.lang.String)
	 */
	@Override
	public void setDescricaoArea(String descricaoArea) {

		this.descricaoArea = descricaoArea;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#getEmail()
	 */
	@Override
	public String getEmail() {

		return email;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(String email) {

		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#isPrincipal()
	 */
	@Override
	public boolean isPrincipal() {

		return principal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.cliente.impl.
	 * ContatoCliente#setPrincipal(boolean)
	 */
	@Override
	public void setPrincipal(boolean principal) {

		this.principal = principal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(tipoContato == null) {
			stringBuilder.append(TIPO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if((nome == null) || (nome.trim().length() <= 0)) {
			stringBuilder.append(NOME);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ContatoCliente
	 * #getProfissao()
	 */
	@Override
	public Profissao getProfissao() {

		return profissao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.ContatoCliente
	 * #setProfissao(br.com.ggas.cadastro.cliente.
	 * Profissao)
	 */
	@Override
	public void setProfissao(Profissao profissao) {

		this.profissao = profissao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.cliente.ContatoCliente#getCodigoDDD2()
	 */
	@Override
	public Integer getCodigoDDD2() {

		return codigoDDD2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.cliente.ContatoCliente#setCodigoDDD2(java.lang.Integer)
	 */
	@Override
	public void setCodigoDDD2(Integer codigoDDD2) {

		this.codigoDDD2 = codigoDDD2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.cliente.ContatoCliente#getFone2()
	 */
	@Override
	public Integer getFone2() {

		return fone2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.cliente.ContatoCliente#setFone2(java.lang.Integer)
	 */
	@Override
	public void setFone2(Integer fone2) {

		this.fone2 = fone2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.cliente.ContatoCliente#getRamal2()
	 */
	@Override
	public Integer getRamal2() {

		return ramal2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.cliente.ContatoCliente#setRamal2(java.lang.Integer)
	 */
	@Override
	public void setRamal2(Integer ramal2) {

		this.ramal2 = ramal2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.cliente.ContatoCliente#getCpf()
	 */
	@Override
	public String getCpf() {
		return cpf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.cliente.ContatoCliente#setCpf(java.lang.String)
	 */
	@Override
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.cliente.ContatoCliente#getRg()
	 */
	@Override
	public String getRg() {
		return rg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.cliente.ContatoCliente#setRg(java.lang.String)
	 */
	@Override
	public void setRg(String rg) {
		this.rg = rg;
	}

}
