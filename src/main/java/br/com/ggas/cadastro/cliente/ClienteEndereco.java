/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.cliente;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface ClienteEndereco.
 */
public interface ClienteEndereco extends EntidadeNegocio {

	/** The bean id cliente endereco. */
	String BEAN_ID_CLIENTE_ENDERECO = "clienteEndereco";

	/** The cep. */
	String CEP = "CEP";

	String INDICADOR_PRINCIPAL = "indicadorPrincipal";

	/** The tipo. */
	String TIPO = "Tipo de Endereço";

	/** The numero. */
	String NUMERO = "Número do endereço";

	/**
	 * Gets the cep.
	 *
	 * @return the cep
	 */
	Cep getCep();

	/**
	 * Sets the cep.
	 *
	 * @param cep            the cep to set
	 */
	void setCep(Cep cep);

	/**
	 * Gets the numero.
	 *
	 * @return the numero
	 */
	String getNumero();

	/**
	 * Sets the numero.
	 *
	 * @param numero            the numero to set
	 */
	void setNumero(String numero);

	/**
	 * Gets the complemento.
	 *
	 * @return the complemento
	 */
	String getComplemento();

	/**
	 * Sets the complemento.
	 *
	 * @param complemento            the complemento to set
	 */
	void setComplemento(String complemento);

	/**
	 * Gets the endereco referencia.
	 *
	 * @return the enderecoReferencia
	 */
	String getEnderecoReferencia();

	/**
	 * Sets the endereco referencia.
	 *
	 * @param enderecoReferencia            the enderecoReferencia to set
	 */
	void setEnderecoReferencia(String enderecoReferencia);

	/**
	 * Gets the caixa postal.
	 *
	 * @return the caixaPostal
	 */
	Integer getCaixaPostal();

	/**
	 * Sets the caixa postal.
	 *
	 * @param caixaPostal            the caixaPostal to set
	 */
	void setCaixaPostal(Integer caixaPostal);

	/**
	 * Gets the tipo endereco.
	 *
	 * @return the tipoEndereco
	 */
	TipoEndereco getTipoEndereco();

	/**
	 * Sets the tipo endereco.
	 *
	 * @param tipoEndereco            the tipoEndereco to set
	 */
	void setTipoEndereco(TipoEndereco tipoEndereco);

	/**
	 * Gets the correspondencia.
	 *
	 * @return the correspondencia
	 */
	Boolean getCorrespondencia();

	/**
	 * Sets the correspondencia.
	 *
	 * @param correspondencia            the correspondencia to set
	 */
	void setCorrespondencia(Boolean correspondencia);

	/**
	 * Recupera o nome do logradouro do endereco.
	 * 
	 * @return Um String com o nome do logradouro .
	 */
	String getEnderecoLogradouro();

	/**
	 * Recupera o endereço formatado.
	 * 
	 * @return Um String com o endereço formatado.
	 */
	String getEnderecoFormatado();

	/**
	 * Gets the endereco formatado municipio uf.
	 *
	 * @return Um String com Municipio e UF
	 *         formatado.
	 */

	String getEnderecoFormatadoMunicipioUF();

	/**
	 * Gets the endereco formatado rua numero complemento.
	 *
	 * @return Um String com nome da rua e número
	 *         complemento
	 */
	String getEnderecoFormatadoRuaNumeroComplemento();

	/**
	 * Gets the endereco formatado bairro municipio ufcep.
	 *
	 * @return Um String com Bairro, Municipio, UF
	 *         e CEP
	 */
	String getEnderecoFormatadoBairroMunicipioUFCEP();

	/**
	 * Gets the municipio.
	 *
	 * @return Municipio
	 */
	Municipio getMunicipio();

	/**
	 * Sets the municipio.
	 *
	 * @param municipio the new municipio
	 */
	void setMunicipio(Municipio municipio);

	/**
	 * Gets the endereco formatado bairro.
	 *
	 * @return the endereco formatado bairro
	 */
	String getEnderecoFormatadoBairro();

	/**
	 * Gets the endereco formatado municipio ufcep.
	 *
	 * @return the endereco formatado municipio ufcep
	 */
	String getEnderecoFormatadoMunicipioUFCEP();

	/**
	 * Gets the indicador principal.
	 *
	 * @return the indicador principal
	 */
	public Integer getIndicadorPrincipal();

	/**
	 * Sets the indicador principal.
	 *
	 * @param indicadorPrincipal the new indicador principal
	 */
	public void setIndicadorPrincipal(Integer indicadorPrincipal);

	/**
	 * Gets the endereco formatado rua numero bairro.
	 *
	 * @return the endereco formatado rua numero bairro
	 */
	String getEnderecoFormatadoRuaNumeroBairro();

	/**
	 * Gets the endereco formatado cep cidade estado.
	 *
	 * @return the endereco formatado cep cidade estado
	 */
	String getEnderecoFormatadoCepCidadeEstado();

	/**
	 * Gets the endereco formatado rua numero bairro municipio uf.
	 *
	 * @return String
	 */
	String getEnderecoFormatadoRuaNumeroBairroMunicipioUF();

}

