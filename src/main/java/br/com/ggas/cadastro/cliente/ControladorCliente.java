/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.cliente;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoCliente;
import br.com.ggas.cadastro.cliente.dominio.ChamadoAlteracaoClienteContato;
import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.geral.dominio.EntidadeDominio;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface ControladorCliente
 * 
 * @author Procenge
 */
public interface ControladorCliente extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_CLIENTE = "controladorCliente";

	String ERRO_NEGOCIO_EMAIL_INVALIDO = "ERRO_NEGOCIO_EMAIL_INVALIDO";

	String ERRO_NEGOCIO_EMPTY_FONE = "ERRO_NEGOCIO_EMPTY_FONE";

	String ERRO_NEGOCIO_NUMERO_ENDERECO_EXISTENTE = "ERRO_NEGOCIO_NUMERO_ENDERECO_EXISTENTE";

	String ERRO_NEGOCIO_NUMERO_TELEFONE_EXISTENTE = "ERRO_NEGOCIO_NUMERO_TELEFONE_EXISTENTE";

	String ERRO_NEGOCIO_EMAIL_CONTATO_EXISTENTE = "ERRO_NEGOCIO_EMAIL_CONTATO_EXISTENTE";

	String ERRO_NEGOCIO_DATA_MAIOR_QUE_DATA_ATUAL = "ERRO_NEGOCIO_DATA_MAIOR_QUE_DATA_ATUAL";

	String ERRO_NEGOCIO_DATA_EMISSAO_ANTERIOR_DATA_NASCIMENTO = "ERRO_NEGOCIO_DATA_EMISSAO_ANTERIOR_DATA_NASCIMENTO";

	String ERRO_NEGOCIO_CLIENTE_RESPONSAVEL_POR_ELE_MESMO = "ERRO_NEGOCIO_CLIENTE_RESPONSAVEL_POR_ELE_MESMO";

	String ERRO_NEGOCIO_CLIENTE_SEM_ENDERECO = "ERRO_NEGOCIO_CLIENTE_SEM_ENDERECO";

	String ERRO_NEGOCIO_CLIENTE_SEM_ENDERECO_PRINCIPAL = "ERRO_NEGOCIO_CLIENTE_SEM_ENDERECO_PRINCIPAL";

	String ERRO_NEGOCIO_ALTERACAO_TIPO_CLIENTE_NAO_PERMITIDA = "ERRO_NEGOCIO_ALTERACAO_TIPO_CLIENTE_NAO_PERMITIDA";

	String ERRO_NEGOCIO_REMOVER_ATIVIDADE_ECONOMICA = "ERRO_NEGOCIO_REMOVER_ATIVIDADE_ECONOMICA";

	String ERRO_NEGOCIO_REMOVER_TIPO_CLIENTE = "ERRO_NEGOCIO_REMOVER_TIPO_CLIENTE";

	String ERRO_NEGOCIO_REMOVER_CLIENTE_SITUACAO = "ERRO_NEGOCIO_REMOVER_CLIENTE_SITUACAO";

	String ERRO_NEGOCIO_REMOVER_PROFISSAO = "ERRO_NEGOCIO_REMOVER_PROFISSAO";

	String ERRO_NEGOCIO_REMOVER_TIPO_CONTATO = "ERRO_NEGOCIO_REMOVER_TIPO_CONTATO";

	String ERRO_NEGOCIO_REMOVER_TIPO_TELEFONE = "ERRO_NEGOCIO_REMOVER_TIPO_TELEFONE";

	String ERRO_NEGOCIO_REMOVER_TIPO_ENDERECO = "ERRO_NEGOCIO_REMOVER_TIPO_ENDERECO";

	String ERRO_NEGOCIO_DESCRICAO_ANEXO_EXISTENTE = "ERRO_NEGOCIO_DESCRICAO_ANEXO_EXISTENTE";

	String ERRO_NEGOCIO_TIPO_ANEXO_EXISTENTE = "ERRO_NEGOCIO_TIPO_ANEXO_EXISTENTE";

	String ERRO_NEGOCIO_ENDERECO_PRINCIPAL_EXISTENTE = "ERRO_NEGOCIO_ENDERECO_PRINCIPAL_EXISTENTE";

	/**
	 * Método que cria a entidade TipoCliente.
	 * 
	 * @return Instância de TipoCliente
	 */
	EntidadeNegocio criarTipoCliente();

	/**
	 * Método que cria a entidade EsferaPoder.
	 * 
	 * @return Instância de EsferaPoder
	 */
	EntidadeNegocio criarEsferaPoder();

	/**
	 * Método que cria a entidade TipoPessoa.
	 * 
	 * @return Instância de TipoPessoa
	 */
	EntidadeDominio criarTipoPessoa();

	/**
	 * Método que cria a entidade ContatoCliente.
	 * 
	 * @return Instância de ContatoCliente
	 */
	EntidadeNegocio criarContatoCliente();

	/**
	 * Método que cria a entidade ClienteEndereco.
	 * 
	 * @return Instância de ClienteEndereco
	 */
	EntidadeNegocio criarClienteEndereco();

	/**
	 * Método que cria a entidade ClienteFone.
	 * 
	 * @return Instância de ClienteFone.
	 */
	EntidadeNegocio criarClienteFone();

	/**
	 * Método responsável por listar os tipos de clientes existentes no sistema.
	 * 
	 * @return coleção de tipos de clientes.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<TipoCliente> listarTipoCliente() throws NegocioException;

	/**
	 * Método responsável por listar as situações dos clientes existentes no sistema.
	 * 
	 * @return coleção de situações dos clientes.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<ClienteSituacao> listarClienteSituacao() throws NegocioException;

	/**
	 * Método responsável por listar as profissões dos clientes existentes no sistema.
	 * 
	 * @return coleção de profissões.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Profissao> listarProfissao() throws NegocioException;

	/**
	 * Método responsável por listar as nacionalidades dos clientes existentes no sistema.
	 * 
	 * @return coleção de nacionalidades.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Nacionalidade> listarNacionalidade() throws NegocioException;

	/**
	 * Método responsável por listar as atividades economicas dos clientes existentes no sistema.
	 * 
	 * @return coleção de atividades economicas.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<AtividadeEconomica> listarAtividadeEconomica() throws NegocioException;

	/**
	 * Método responsável por listar os tipos de endereços dos clientes existentes no sistema.
	 * 
	 * @return coleção de tipos de endereços.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<TipoEndereco> listarTipoEndereco() throws NegocioException;

	/**
	 * Método responsável por listar os tipos de telefones dos clientes existentes no sistema.
	 * 
	 * @return coleção de tipos de telefones.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<TipoFone> listarTipoFone() throws NegocioException;

	/**
	 * Método responsável por listar os tipos de contatos dos clientes existentes no sistema.
	 * 
	 * @return coleção de tipos de contatos.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<TipoContato> listarTipoContato() throws NegocioException;

	/**
	 * Método responsável por listar os orgãos expedidores.
	 * 
	 * @return coleção dos orgãos expedidores.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<OrgaoExpedidor> listarOrgaoExpedidor() throws NegocioException;

	/**
	 * Método responsável por listar a faixa da renda familiar.
	 * 
	 * @return coleção da faixa da renda familiar
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<FaixaRendaFamiliar> listarFaixaRendaFamiliar() throws NegocioException;

	/**
	 * Método responsável por obter o tipo de contato do cliente.
	 * 
	 * @param chavePrimaria A chave primária do tipo contato.
	 * @return o tipo de contato do cliente.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	TipoContato buscarTipoContato(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por consultar os clientes pessoa jurídica.
	 * 
	 * @param cnpj O CNPJ co cliente
	 * @param nome O nome do cliente
	 * @return Uma coleção de clientes
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Cliente> consultarClientesPessoaJuridica(String cnpj, String nome) throws NegocioException;

	/**
	 * Método responsável por consultar clientes.
	 * 
	 * @param filtro O filtro com os parâmetros para a pesquisa.
	 * @return Uma coleção de clientes.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Cliente> consultarClientes(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Semelhante a {@link #consultarClientes(Map)}, consulta a lista de clientes que casam
	 * com o filtro informado, paginando a consulta
	 * @param filtro lista de filtros
	 * @param quantidadeMax quantidade máxima de itens por página
	 * @param offset índice do número da página atual (começando por 0)
	 * @return retorna a lista de clientes que casam com a consulta numa quantidade de itens informada
	 */
	Collection<Cliente> consultarClientesPaginados(Map<String, Object> filtro, int quantidadeMax, int offset);

	/**
	 * Método responsável por consultar clientes.
	 * 
	 * @param chavesPrimarias the chaves primarias
	 * @return Uma coleção de clientes.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	List<Cliente> consultarClientes(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por consultar ContratoCliente para o requisito de manter parada programada.
	 * 
	 * @param filtro O filtro com os parâmetros para a pesquisa.
	 * @return Uma coleção de ContratoCliente
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Cliente> consultarClientesContrato(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por gerar os dados de endereço do cliente que será utilizado no código de barras de endereço.
	 * 
	 * @param cliente O cliente cadastrado
	 * @return Um mapa com os dados para o relatorio
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */

	Map<String, Object> obterDadosRelatorioCodigoBarrasEndereco(Cliente cliente) throws NegocioException;

	/**
	 * Obter dados relatorio codigo barras endereco.
	 * 
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @return Um mapa com os dados para o relatorio
	 * @throws NegocioException the negocio exception
	 */

	Map<String, Object> obterDadosRelatorioCodigoBarrasEndereco(ContratoPontoConsumo contratoPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter o tipo de telefone do cliente.
	 * 
	 * @param chavePrimaria A chave primária do tipo fone.
	 * @return Um tipo do telefone do cliente.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	TipoFone buscarTipoFone(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o tipo de endereço do cliente.
	 * 
	 * @param chavePrimaria A chave primária do tipo endereco.
	 * @return Um tipo de endereço do cliente.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	TipoEndereco buscarTipoEndereco(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por validar os dados do contato do cliente.
	 * 
	 * @param contato O contato do cliente.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	void validarDadosContatoCliente(ContatoCliente contato) throws NegocioException;

	/**
	 * Método responsável por verificar se já existe um contato do cliente com o mesmo email.
	 * 
	 * @param indexLista the index lista
	 * @param listaContatoCliente A lista contendo os contatos do cliente a serem verificados.
	 * @param contatoCliente O contato do cliente a ser incluído.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	void verificarEmailContatoExistente(Integer indexLista, Collection<ContatoCliente> listaContatoCliente, ContatoCliente contatoCliente)
			throws NegocioException;

	/**
	 * Método responsável por validar os dados do endereço do cliente.
	 * 
	 * @param clienteEndereco O endereço do cliente.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	void validarDadosClienteEndereco(ClienteEndereco clienteEndereco) throws NegocioException;

	/**
	 * Método responsável por verificar se já existe um endereço no cliente com o mesmo número.
	 * 
	 * @param indexLista the index lista
	 * @param listaClienteEndereco A lista contendo os endereços do cliente a serem verificados.
	 * @param clienteEndereco O endereco do cliente a ser incluído.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	void verificarNumeroClienteEnderecoExistente(Integer indexLista, Collection<ClienteEndereco> listaClienteEndereco,
			ClienteEndereco clienteEndereco) throws NegocioException;

	/**
	 * Método responsável por validar os dados do telefone do cliente.
	 * 
	 * @param clienteFone O telefone do cliente.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	void validarDadosClienteFone(ClienteFone clienteFone) throws NegocioException;

	/**
	 * Método responsável por verificar se já existe um telefone no cliente com o mesmo número.
	 * 
	 * @param indexLista the index lista
	 * @param listaClienteFone A lista contendo os telefones do cliente a serem verificados.
	 * @param clienteFone O telefone a ser incluído.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	void verificarNumeroTelefoneExistente(Integer indexLista, Collection<ClienteFone> listaClienteFone, ClienteFone clienteFone)
			throws NegocioException;

	/**
	 * Método responsável por obter um tipo de cliente.
	 * 
	 * @param chavePrimaria A chave primaria
	 * @return Um tipo de cliente
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	TipoCliente obterTipoCliente(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável em obter a situação do cliente.
	 * 
	 * @param chavePrimaria A chave primaria
	 * @return Uma situação do cliente
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	ClienteSituacao obterClienteSituacao(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter uma profissão.
	 * 
	 * @param chavePrimaria A chave primaria
	 * @return Uma profissão
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Profissao obterProfissao(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter uma renda familiar.
	 * 
	 * @param chavePrimaria A chave primaria
	 * @return Uma faixa de renda familiar
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	FaixaRendaFamiliar obterFaixaRendaFamiliar(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter a nacionalidade.
	 * 
	 * @param chavePrimaria A chave primaria
	 * @return uma nacionalidade
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Nacionalidade obterNacionalidade(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável pot obter a atividade economica.
	 * 
	 * @param chavePrimaria A chave primaria
	 * @return uma atividade economica
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	AtividadeEconomica obterAtividadeEconomica(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o orgão expedidor.
	 * 
	 * @param chavePrimaria A chave primaria
	 * @return Um orgão expedidor
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	OrgaoExpedidor obterOrgaoExpedidor(long chavePrimaria) throws NegocioException;

	/**
	 * Método reponsável por listar os sexos.
	 * 
	 * @return Uma coleção de sexo
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PessoaSexo> listarPessoaSexo() throws NegocioException;

	/**
	 * Método responsável por obter o sexo.
	 * 
	 * @param chavePrimaria A chave primaria
	 * @return Um sexo
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	PessoaSexo obterPessoaSexo(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por validar se o tipo do cliente pode ser alterado.
	 * 
	 * @param chavePrimaria A chave primaria do cliente.
	 * @param chavePrimariaTipoCliente A chave primária de um tipo de cliente.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	void validarAlteracaoTipoCliente(long chavePrimaria, long chavePrimariaTipoCliente) throws NegocioException;

	/**
	 * Método responsável por consultar atividades econômicas.
	 * 
	 * @param filtro O filtro com os parâmetros para a pesquisa.
	 * @return Uma coleção de atividades econômicas.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<AtividadeEconomica> consultarAtividadeEconomica(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por obter um cliente a partir do ponto de consumo.
	 * 
	 * @param idPontoConsumo Chave primária do ponto de consumo.
	 * @return Um Cliente.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Cliente obterClientePorPontoConsumo(Long idPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter um cliente a partir do ponto de consumo que possui contrato ativo.
	 * 
	 * @param idPontoConsumo Chave primária do ponto de consumo.
	 * @return Um Cliente.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Cliente obterClientePorPontoConsumoDeContratoAtivo(Long idPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter o cliente pelo número do contrato.
	 * 
	 * @param numeroContrato Número do Contrato.
	 * @return Um cliente.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Cliente obterClienteAssinaturaPeloNumeroContrato(String numeroContrato) throws NegocioException;

	/**
	 * Método responsável por listar os clientes que possuem faturas quitadas de acordo com a data de vencimento e a situação de pagamento
	 * das faturas.
	 * 
	 * @param anoFaturas Ano da data de vencimento das faturas.
	 * @param situacaoPagamento Array com chaves primárias das situações de pagamento das faturas.
	 * @param tiposDocumento Tipo de documento da fatura.
	 * @param anosAnteriores Indicador para buscar as faturas com data de vencimento menor, igual ou até o ano desejado. anosAnteriores null
	 *            para faturas com data de vecimento até o 'anoFaturas', true para faturas com data de vecimento antes do 'anoFaturas' e
	 *            false para faturas com data de vencimento igual ao 'anoFaturas'.
	 * @return Uma coleção de clientes.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Cliente> listarClientesParaFaturasSituacaoPagamento(String anoFaturas, Long[] situacaoPagamento, Long[] tiposDocumento,
			Boolean anosAnteriores) throws NegocioException;

	/**
	 * Obter cliente principal.
	 * 
	 * @param idPontoConsumo the id ponto consumo
	 * @return the cliente
	 * @throws NegocioException the negocio exception
	 */
	Cliente obterClientePrincipal(Long idPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter os clientes com débitos/créditos e que não possuem contrato para o faturarClientesBatch.
	 * 
	 * @return os clientes que foram encontrados
	 * @throws NegocioException caso ocorra algum erro
	 */
	Collection<Cliente> consultarClientesComCreditoDebito() throws NegocioException;

	/**
	 * Método para obter o cliente.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the cliente
	 * @throws NegocioException the negocio exception
	 */
	Cliente obterCliente(long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Método responsável por listar todas as esferas poder.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<EsferaPoder> listarEsferaPoder() throws NegocioException;

	/**
	 * Consultar cliente endereco.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ClienteEndereco> consultarClienteEndereco(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar contato cliente.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContatoCliente> consultarContatoCliente(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar cliente telefone.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ClienteFone> consultarClienteTelefone(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Inserir cliente.
	 * 
	 * @param cliente the cliente
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	Long inserirCliente(Cliente cliente) throws GGASException;

	/**
	 * Atualizar cliente.
	 * 
	 * @param cliente the cliente {@link Cliente}
	 * @throws GGASException {@link GGASException}
	 */
	void atualizarCliente(Cliente cliente) throws GGASException;

	/**
	 * Método responsável por atualizar Clientes
	 * 
	 * @param cliente {@link Cliente}
	 * @param atualizarContrato {@link boolean}
	 * @throws GGASException {@link GGASException}
	 */
	void atualizarCliente(Cliente cliente, boolean atualizarContrato) throws GGASException;

	/**
	 * Remover clientes.
	 * 
	 * @param chavesPrimarias the chaves primarias
	 * @param dadosAuditoria the dados auditoria
	 * @throws GGASException the GGAS exception
	 */
	void removerClientes(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Consultar cliente por telefone.
	 * 
	 * @param numeroTelefone the numero telefone
	 * @return the cliente
	 */
	Cliente consultarClientePorTelefone(String numeroTelefone);

	/**
	 * Obter tipo cliente por id tipo cliente.
	 * 
	 * @param idTipoCliente the id tipo cliente
	 * @return the tipo cliente
	 */
	TipoCliente obterTipoClientePorIdTipoCliente(Long idTipoCliente);

	/**
	 * Criar orgao expedidor.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarOrgaoExpedidor();

	/**
	 * Criar unidade federacao.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarUnidadeFederacao();

	/**
	 * Criar nacionalidade.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarNacionalidade();

	/**
	 * Criar profissao.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarProfissao();

	/**
	 * Criar pessoa sexo.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarPessoaSexo();

	/**
	 * Criar faixa renda familiar.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarFaixaRendaFamiliar();

	/**
	 * Criar atividade economica.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarAtividadeEconomica();

	/**
	 * Criar tipo fone.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarTipoFone();

	/**
	 * Criar tipo endereco.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarTipoEndereco();

	/**
	 * Criar tipo contato.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarTipoContato();

	/**
	 * Criar cep.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarCep();

	/**
	 * Listar grupo economico.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<GrupoEconomico> listarGrupoEconomico() throws NegocioException;

	/**
	 * Inserir chamado alteracao cliente.
	 * 
	 * @param chamadoAlteracaoCliente the chamado alteracao cliente
	 * @return the long
	 * @throws GGASException the GGAS exception
	 */
	Long inserirChamadoAlteracaoCliente(ChamadoAlteracaoCliente chamadoAlteracaoCliente) throws GGASException;

	/**
	 * Criar municipio.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarMunicipio();

	/**
	 * Criar tipo acao.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarTipoAcao();

	/**
	 * Criar cliente anexo.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarClienteAnexo();

	/**
	 * Validar dados cliente anexo.
	 * 
	 * @param clienteAnexo the cliente anexo
	 * @throws NegocioException the negocio exception
	 */
	void validarDadosClienteAnexo(ClienteAnexo clienteAnexo) throws NegocioException;

	/**
	 * Verificar descricao anexo.
	 * 
	 * @param indexLista the index lista
	 * @param listaClienteAnexo the lista cliente anexo
	 * @param clienteAnexo the cliente anexo
	 * @throws NegocioException the negocio exception
	 */
	void verificarDescricaoAnexo(Integer indexLista, Collection<ClienteAnexo> listaClienteAnexo, ClienteAnexo clienteAnexo)
			throws NegocioException;

	/**
	 * Verificar tipo anexo anexo.
	 * 
	 * @param indexLista the index lista
	 * @param listaClienteAnexo the lista cliente anexo
	 * @param clienteAnexo the cliente anexo
	 * @throws NegocioException the negocio exception
	 */
	void verificarTipoAnexoAnexo(Integer indexLista, Collection<ClienteAnexo> listaClienteAnexo, ClienteAnexo clienteAnexo)
			throws NegocioException;

	/**
	 * Obter chamado alteracao cliente por chamado.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the chamado alteracao cliente
	 * @throws NegocioException the negocio exception
	 */
	ChamadoAlteracaoCliente obterChamadoAlteracaoClientePorChamado(long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Obter chamado alteracao cliente.
	 * 
	 * @param chamadoChavePrimaria the chamado chave primaria
	 * @param propriedadesLazy the propriedades lazy
	 * @return the chamado alteracao cliente
	 * @throws NegocioException the negocio exception
	 */
	ChamadoAlteracaoCliente obterChamadoAlteracaoCliente(long chamadoChavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Atualizar dependencias.
	 * 
	 * @param listaIdsChamadoClienteFone the lista ids chamado cliente fone
	 * @param chamadoChave the chamado chave
	 * @throws GGASException the GGAS exception
	 */
	void atualizarDependencias(List<Long> listaIdsChamadoClienteFone, Long chamadoChave) throws GGASException;

	/**
	 * Obter chamado alteracao cliente contato por cliente contato.
	 * 
	 * @param chamadoChaveClienteContato the chamado chave cliente contato
	 * @param chamadoChave the chamado chave
	 * @return the chamado alteracao cliente contato
	 */
	ChamadoAlteracaoClienteContato obterChamadoAlteracaoClienteContatoPorClienteContato(Long chamadoChaveClienteContato, Long chamadoChave);

	/**
	 * Atualizar dependencias chamado cliente contato.
	 * 
	 * @param listaIdsChamadoClienteContato the lista ids chamado cliente contato
	 * @param chamadoChave the chamado chave
	 * @throws GGASException the GGAS exception
	 */
	void atualizarDependenciasChamadoClienteContato(List<Long> listaIdsChamadoClienteContato, Long chamadoChave) throws GGASException;

	/**
	 * Atualizar dependencias chamado cliente endereco.
	 * 
	 * @param listaIdsChamadoClienteEndereco the lista ids chamado cliente endereco
	 * @param chamadoChave the chamado chave
	 * @throws GGASException the GGAS exception
	 */
	void atualizarDependenciasChamadoClienteEndereco(List<Long> listaIdsChamadoClienteEndereco, Long chamadoChave) throws GGASException;

	/**
	 * Atualizar dependencias chamado cliente anexo.
	 * 
	 * @param listaIdsChamadoClienteAnexo the lista ids chamado cliente anexo
	 * @param chamadoChave the chamado chave
	 * @throws GGASException the GGAS exception
	 */
	void atualizarDependenciasChamadoClienteAnexo(List<Long> listaIdsChamadoClienteAnexo, Long chamadoChave) throws GGASException;

	/**
	 * Listar cliente por grupo economico.
	 * 
	 * @param idGrupoEconomico the id grupo economico
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Cliente> listarClientePorGrupoEconomico(Long idGrupoEconomico) throws NegocioException;

	/**
	 * Consultar cliente.
	 * 
	 * @param chavesPrimarias the chaves primarias
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Cliente> consultarCliente(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Listar cliente.
	 * 
	 * @param chavesPrimarias the chaves primarias
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Cliente> listarCliente(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Listar cliente sem grupo economico.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Cliente> listarClienteSemGrupoEconomico(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Validar numero empenho.
	 * 
	 * @param idCliente the id cliente
	 * @param numeroEmpenho the numero empenho
	 * @throws NegocioException the negocio exception
	 */
	void validarNumeroEmpenho(Long idCliente, String numeroEmpenho) throws NegocioException;

	/**
	 * Verificar cliente existente.
	 * 
	 * @param cliente the cliente
	 * @throws NegocioException the negocio exception
	 */
	void verificarClienteExistente(Cliente cliente) throws NegocioException;

	/**
	 * Verificar email princiap obrigatorio.
	 * 
	 * @param emailPrincipal the email principal
	 * @throws NegocioException the negocio exception
	 */
	void verificarEmailPrinciapObrigatorio(String emailPrincipal) throws NegocioException;

	/**
	 * Obter cliente por ponto consumo contrato.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the cliente
	 * @throws NegocioException the negocio exception
	 */
	Cliente obterClientePorPontoConsumoContrato(long chavePrimaria) throws NegocioException;

	/**
	 * Realiza a consulta de clientes por chaves primárias de entidade do tipo ContratoPontoConsumo. Obtém do ContratoPontoConsumo, apenas a
	 * chave primária.
	 * 
	 * @param values {@link Collection}
	 * @return mapa de Cliente por ContratoPontoConsumo
	 * @throws NegocioException {@link NegocioException}
	 */
	Map<ContratoPontoConsumo, Cliente> consultarCliente(Collection<ContratoPontoConsumo> values) throws NegocioException;

	/**
	 * Consulta o cliente de determinado contrato
	 * 
	 * @param idContrato - chave primaria do contrato
	 * @return Cliente assinatura do contrato
	 * @throws NegocioException {@link NegocioException}
	 */
	Cliente obterClienteAssinaturaPeloIdContrato(Long idContrato) throws NegocioException;
	
	/**
	 * Consulta o cliente de determinada fatura
	 * 
	 * @param idFatura {@link long}
	 * @return Cliente {@link Cliente}
	 */
	Cliente obterClientePorFatura(long idFatura);

	/**
	 * Obtem um Contato de Cliente através de chave primaria.
	 * 
	 * @param chavePrimaria {@link long}
	 * @return ContratoCliente  {@link ContratoCliente}
	 * @throws NegocioException {@link NegocioException}
	 */
	ContatoCliente obterContatoCliente(long chavePrimaria) throws NegocioException;

	/**
	 * Lista Tipo de Contato por Tipo Pessoa através da chave primaria
	 * 
	 * @param idTipoCliente {@link Long}
	 * @return listaTipoContato {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	Collection<TipoContato> listarTipoContadoPorTipoPessoaDoCliente(Long idTipoCliente) throws NegocioException;
	
	/**
	 * Método responsável por listar somente os clientes que nao foram integrados.
	 * 
	 * @return uma lista de objeto Cliente
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Cliente> listarClientesNaoIntegrados() throws NegocioException;

	/**
	 * Método responsável por Carregar cliente por cpf ou cnpj.
	 * @param cpfCnpj cpfCnpj do Cliente
	 * @return um Cliente
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Cliente obterClientePorCpfCnpj(String cpfCnpj) throws NegocioException;

	/**
	 * Obtém a chave primária do cliente de um contrato ativo.
	 * @param idPontoConsumo - {@link Long}
	 * @return cliente - {@link Cliente}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Cliente obterChaveClientePorPontoConsumoDeContratoAtivo(Long idPontoConsumo) throws NegocioException;
}
