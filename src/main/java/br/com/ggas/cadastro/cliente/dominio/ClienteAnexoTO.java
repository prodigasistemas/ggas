/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 07/02/2014 09:25:28
 @author ccavalcanti
 */

package br.com.ggas.cadastro.cliente.dominio;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import br.com.ggas.webservice.TipoComboBoxTO;
/**
 *	Classe TO para transferencia de dados usada na classe Servico
 */
@XmlRootElement
public class ClienteAnexoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1631421758808798987L;

	private Long chavePrimaria;

	private TipoComboBoxTO abaAnexo;

	private TipoComboBoxTO tipoAnexo;

	private byte[] documentoAnexo;

	private TipoComboBoxTO tipoAcao;

	private String descricaoAnexo;

	private Date ultimaAlteracao;

	private boolean indicadorExclusao;

	private boolean indicadorRegistroExistente;

	private String dataAlteracao;

	private String nomeArquivo;

	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	public TipoComboBoxTO getAbaAnexo() {

		return abaAnexo;
	}

	public void setAbaAnexo(TipoComboBoxTO abaAnexo) {

		this.abaAnexo = abaAnexo;
	}

	public TipoComboBoxTO getTipoAnexo() {

		return tipoAnexo;
	}

	public void setTipoAnexo(TipoComboBoxTO tipoAnexo) {

		this.tipoAnexo = tipoAnexo;
	}

	public byte[] getDocumentoAnexo() {
		byte[] documentoAnexoTmp = null;
		if(documentoAnexo != null){
			documentoAnexoTmp = documentoAnexo.clone();
		}
		return documentoAnexoTmp;
	}

	public void setDocumentoAnexo(byte[] documentoAnexo) {
		if(documentoAnexo != null){
			this.documentoAnexo = documentoAnexo.clone();
		} else {
			this.documentoAnexo = null;
		}
	}

	public TipoComboBoxTO getTipoAcao() {

		return tipoAcao;
	}

	public void setTipoAcao(TipoComboBoxTO tipoAcao) {

		this.tipoAcao = tipoAcao;
	}

	public String getDescricaoAnexo() {

		return descricaoAnexo;
	}

	public void setDescricaoAnexo(String descricaoAnexo) {

		this.descricaoAnexo = descricaoAnexo;
	}

	public Date getUltimaAlteracao() {
		Date ultimaAlteracaoTmp = null;
		if(ultimaAlteracao != null){
			ultimaAlteracaoTmp = (Date) ultimaAlteracao.clone();
		}
		return ultimaAlteracaoTmp;
	}

	public void setUltimaAlteracao(Date ultimaAlteracao) {
		if(ultimaAlteracao != null){
			this.ultimaAlteracao = (Date) ultimaAlteracao.clone();
		} else {
			this.ultimaAlteracao = null;
		}
	}

	public boolean isIndicadorExclusao() {

		return indicadorExclusao;
	}

	public void setIndicadorExclusao(boolean indicadorExclusao) {

		this.indicadorExclusao = indicadorExclusao;
	}

	public boolean isIndicadorRegistroExistente() {

		return indicadorRegistroExistente;
	}

	public void setIndicadorRegistroExistente(boolean indicadorRegistroExistente) {

		this.indicadorRegistroExistente = indicadorRegistroExistente;
	}

	public String getDataAlteracao() {

		return dataAlteracao;
	}

	public void setDataAlteracao(String dataAlteracao) {

		this.dataAlteracao = dataAlteracao;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
}
