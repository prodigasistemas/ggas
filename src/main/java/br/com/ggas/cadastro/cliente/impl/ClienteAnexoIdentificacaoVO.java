package br.com.ggas.cadastro.cliente.impl;
/**
 * Classe para cliente anexo identificacaoVO
 * 
 * @author pedro
 * Classe responsável pela representação de uma 
 * entidade ClienteAnexoIdentificacao
 */
public class ClienteAnexoIdentificacaoVO {

	private String descricaoAnexoIdentificacao;
	private Long tipoAnexo;
	private String anexoAbaIdentificacao;
	private Integer indexListaAnexoAbaIdentificacao;
	
	/**
	 * 
	 * @return descricaoAnexoIdentificacao
	 */
	public String getDescricaoAnexoIdentificacao() {
		return descricaoAnexoIdentificacao;
	}
	/**
	 * 
	 * @param descricaoAnexoIdentificacao
	 */
	public void setDescricaoAnexoIdentificacao(String descricaoAnexoIdentificacao) {
		this.descricaoAnexoIdentificacao = descricaoAnexoIdentificacao;
	}
	/**
	 * 
	 * @return tipoAnexo
	 */
	public Long getTipoAnexo() {
		return tipoAnexo;
	}
	/**
	 * 
	 * @param tipoAnexo
	 */
	public void setTipoAnexo(Long tipoAnexo) {
		this.tipoAnexo = tipoAnexo;
	}
	/**
	 * 
	 * @return anexoAbaIdentificacao
	 */
	public String getAnexoAbaIdentificacao() {
		return anexoAbaIdentificacao;
	}
	/**
	 * 
	 * @param anexoAbaIdentificacao
	 */
	public void setAnexoAbaIdentificacao(String anexoAbaIdentificacao) {
		this.anexoAbaIdentificacao = anexoAbaIdentificacao;
	}
	/**
	 * 
	 * @return indexListaAnexoAbaIdentificacao
	 */
	public Integer getIndexListaAnexoAbaIdentificacao() {
		return indexListaAnexoAbaIdentificacao;
	}
	/**
	 * 
	 * @param indexListaAnexoAbaIdentificacao
	 */
	public void setIndexListaAnexoAbaIdentificacao(Integer indexListaAnexoAbaIdentificacao) {
		this.indexListaAnexoAbaIdentificacao = indexListaAnexoAbaIdentificacao;
	}
	
}
