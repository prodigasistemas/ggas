/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 28/01/2014 17:14:56
 @author ccavalcanti
 */

package br.com.ggas.cadastro.cliente.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.cliente.ClienteAnexo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.impl.AnexoImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável por representar o docuemnto anexo de um cliente
 */
public class ClienteAnexoImpl extends AnexoImpl implements ClienteAnexo {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = -3438944783086209430L;

	private EntidadeConteudo abaAnexo;

	private EntidadeConteudo tipoAnexo;

	@Override
	public EntidadeConteudo getAbaAnexo() {

		return abaAnexo;
	}

	@Override
	public void setAbaAnexo(EntidadeConteudo abaAnexo) {

		this.abaAnexo = abaAnexo;
	}

	@Override
	public EntidadeConteudo getTipoAnexo() {

		return tipoAnexo;
	}

	@Override
	public void setTipoAnexo(EntidadeConteudo tipoAnexo) {

		this.tipoAnexo = tipoAnexo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		final Map<String, Object> erros = new HashMap<>();
		final StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		camposObrigatorios = (String) super.validarDados().get(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS);

		if (StringUtils.isNotBlank(camposObrigatorios)) {
			stringBuilder.append(camposObrigatorios);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tipoAnexo == null) {
			stringBuilder.append("Tipo de Documento");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

}
