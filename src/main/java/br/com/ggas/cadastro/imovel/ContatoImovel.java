/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface ContatoImovel
 * 
 * @author Procenge
 */
public interface ContatoImovel extends EntidadeNegocio {

	String BEAN_ID_CONTATO_IMOVEL = "contatoImovel";

	String TIPO = "CONTATO_IMOVEL_TIPO";

	String NOME = "CONTATO_IMOVEL_NOME";

	String DDD = "CONTATO_IMOVEL_DDD";

	String DDD2 = "CONTATO_IMOVEL_DDD2";

	/**
	 * @return the imovel
	 */
	Imovel getImovel();

	/**
	 * @param imovel
	 *            the imovel to set
	 */
	void setImovel(Imovel imovel);

	/**
	 * @return the tipoContato
	 */
	TipoContato getTipoContato();

	/**
	 * @param tipoContato
	 *            the tipoContato to set
	 */
	void setTipoContato(TipoContato tipoContato);

	/**
	 * @return the nome
	 */
	String getNome();

	/**
	 * @param nome
	 *            the nome to set
	 */
	void setNome(String nome);

	/**
	 * @return the descricaoArea
	 */
	String getDescricaoArea();

	/**
	 * @param descricaoArea
	 *            the descricaoArea to set
	 */
	void setDescricaoArea(String descricaoArea);

	/**
	 * @return the codigoDDD
	 */
	Integer getCodigoDDD();

	/**
	 * @param codigoDDD
	 *            the codigoDDD to set
	 */
	void setCodigoDDD(Integer codigoDDD);

	/**
	 * @return the fone
	 */
	String getFone();

	/**
	 * @param fone
	 *            the fone to set
	 */
	void setFone(String fone);

	/**
	 * @return the ramal
	 */
	String getRamal();

	/**
	 * @param ramal
	 *            the ramal to set
	 */
	void setRamal(String ramal);

	/**
	 * @return the codigoDDD
	 */
	Integer getCodigoDDD2();

	/**
	 * @param codigoDDD the codigoDDD to set
	 */
	void setCodigoDDD2(Integer codigoDDD);

	/**
	 * @return the fone
	 */
	String getFone2();

	/**
	 * @param fone the fone to set
	 */
	void setFone2(String fone);

	/**
	 * @return the ramal
	 */
	String getRamal2();

	/**
	 * @param ramal the ramal to set
	 */
	void setRamal2(String ramal);

	/**
	 * @return the email
	 */
	String getEmail();

	/**
	 * @param email
	 *            the email to set
	 */
	void setEmail(String email);

	/**
	 * @return the principal
	 */
	boolean isPrincipal();

	/**
	 * @param principal
	 *            the principal to set
	 */
	void setPrincipal(boolean principal);

	/**
	 * @return the profissao
	 */
	Profissao getProfissao();

	/**
	 * @param profissao
	 *            the profissao to set
	 */
	void setProfissao(Profissao profissao);

	/**
	 * @return telefoneFormatado
	 */
	String getTelefone2Formatado();

	/**
	 * @return telefoneFormatado
	 */
	String getTelefone1Formatado();

}
