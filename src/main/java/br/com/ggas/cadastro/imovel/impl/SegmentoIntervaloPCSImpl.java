/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Map;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SegmentoIntervaloPCS;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.consumo.IntervaloPCS;

/**
 * 
 * 
 */
class SegmentoIntervaloPCSImpl extends EntidadeNegocioImpl implements SegmentoIntervaloPCS {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6801695195711912316L;

	private IntervaloPCS intervaloPCS;

	private Integer prioridade;

	private Integer tamanho;

	private Segmento segmento;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * SegmentoIntervaloPCS#getIntervaloPCS()
	 */
	@Override
	public IntervaloPCS getIntervaloPCS() {

		return intervaloPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.
	 * SegmentoIntervaloPCS
	 * #setIntervaloPCS(br.com.
	 * ggas.medicao.consumo
	 * .IntervaloPCS)
	 */
	@Override
	public void setIntervaloPCS(IntervaloPCS intervaloPCS) {

		this.intervaloPCS = intervaloPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * SegmentoIntervaloPCS#getPrioridade()
	 */
	@Override
	public Integer getPrioridade() {

		return prioridade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * SegmentoIntervaloPCS
	 * #setPrioridade(java.lang.Integer)
	 */
	@Override
	public void setPrioridade(Integer prioridade) {

		this.prioridade = prioridade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * SegmentoIntervaloPCS#getTamanho()
	 */
	@Override
	public Integer getTamanho() {

		return tamanho;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * SegmentoIntervaloPCS
	 * #setTamanho(java.lang.Integer)
	 */
	@Override
	public void setTamanho(Integer tamanho) {

		this.tamanho = tamanho;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * SegmentoIntervaloPCS#getSegmento()
	 */
	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * SegmentoIntervaloPCS
	 * #setSegmento(br.com.ggas
	 * .cadastro.imovel.Segmento
	 * )
	 */
	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
