/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Collection;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.ControladorClienteImovel;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ServiceLocator;
/**
 * Classe responsável pelos métodos
 * de inserção e consulta da relação 
 * entre cliente e imóvel
 * 
 *
 */
public class ControladorClienteImovelImpl extends ControladorNegocioImpl implements ControladorClienteImovel {

	private static final int CONSTANTE_ORDEM_DOIS = 2;
	private static final int CONSTANTE_ORDEM_UM = 1;
	private static final int CONSTANTE_ORDEM_ZERO = 0;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ClienteImovel.BEAN_ID_CLIENTE_IMOVEL);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(ClienteImovel.BEAN_ID_CLIENTE_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorClienteImovel#inserirClienteImovel(java.util.Collection)
	 */
	@Override
	public void inserirClienteImovel(Collection<ClienteImovel> listaClienteImovel) throws NegocioException, ConcorrenciaException {

		for (ClienteImovel clienteImovel : listaClienteImovel) {
			this.inserir(clienteImovel);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #
	 * obterClienteImovelPrincipal(java.lang.Long)
	 */
	@Override
	public ClienteImovel obterClienteImovel(Long idImovel, Long idCliente, Long tipoRelacionamentoClienteImovel) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select clienteImovel from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" clienteImovel ");
		hql.append(" where ");
		hql.append(" clienteImovel.imovel.chavePrimaria = ? ");
		hql.append(" and clienteImovel.cliente.chavePrimaria = ? ");
		hql.append(" and clienteImovel.tipoRelacionamentoClienteImovel = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CONSTANTE_ORDEM_ZERO, idImovel);
		query.setLong(CONSTANTE_ORDEM_UM, idCliente);
		query.setLong(CONSTANTE_ORDEM_DOIS, tipoRelacionamentoClienteImovel);

		return (ClienteImovel) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorClienteImovel#consultarClienteImovel(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ClienteImovel> consultarClienteImovel(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		if(filtro != null) {

			Long idTipoRelacionamentoClienteImovel = (Long) filtro.get("idTipoRelacionamentoClienteImovel");
			if(idTipoRelacionamentoClienteImovel != null && idTipoRelacionamentoClienteImovel > 0) {
				criteria.add(Restrictions.eq("tipoRelacionamentoClienteImovel.chavePrimaria", idTipoRelacionamentoClienteImovel));
			}

		}

		return criteria.list();
	}

}
