/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.util.Collection;

import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;

/**
 * interface RamoAtividade
 * 
 * @author arthur
 *
 */
public interface RamoAtividade extends TabelaAuxiliar {

	String BEAN_ID_RAMO_ATIVIDADE = "ramoAtividade";

	String RAMO_ATIVIDADE = "RAMO_ATIVIDADE";

	String DESCRICAO = "DESCRICAO_RAMO_ATIVIDADE";

	String SEGMENTO = "RAMO_ATIVIDADE_SEGMENTO";

	String ERRO_TIPO_CONSUMO_FATURAMENTO = "ERRO_TIPO_CONSUMO_FATURAMENTO";

	String[] RAMO_ATIVIDADE_SUBSTITUICAO_TRIBUTARIA = new String[] {"ramosAtividadeSubstituicaoTributaria"};

	/**
	 * @return Collection<RamoAtividadeSubstituiçãoTributaria> - Retorna uma coleção ramo atividade substituição tributária.
	 */
	Collection<RamoAtividadeSubstituicaoTributaria> getRamoAtividadeSubstituicaoTributaria();

	/**
	 * @param ramoAtividadeSubstituicaoTributaria - Set ramo atividade substituição tributárioa.
	 */
	void setRamoAtividadeSubstituicaoTributaria(Collection<RamoAtividadeSubstituicaoTributaria> ramoAtividadeSubstituicaoTributaria);

	/**
	 * @return Collection<RamoAtividadeAmostragemPCS> - Retorna uma coleção ramo atividade intervalo pcs.
	 */
	Collection<RamoAtividadeAmostragemPCS> getRamoAtividadeAmostragemPCS();

	/**
	 * @return Collection<RamoAtividadeIntervaloPCS> - Retorna uma coleção ramo atividade intervalo pcs.
	 */
	Collection<RamoAtividadeIntervaloPCS> getRamoAtividadeIntervaloPCS();

	/**
	 * @param ramoAtividadeIntervaloPCS - Set ramoAtividade intervalo Pcs.
	 */
	void setRamoAtividadeIntervaloPCS(Collection<RamoAtividadeIntervaloPCS> ramoAtividadeIntervaloPCS);

	/**
	 * @param ramoAtividadeAmostragemPCS - Set ramo atividade amostragem pcs.
	 */
	void setRamoAtividadeAmostragemPCS(Collection<RamoAtividadeAmostragemPCS> ramoAtividadeAmostragemPCS);

	/**
	 * Gets o tipo de consumo no faturamento
	 * @return o tipo de consumo no faturamento
	 */
	EntidadeConteudo getTipoConsumoFaturamento();

	/**
	 * Sets o tipo de consumo no faturamento
	 * @param tipoConsumoFaturamento o tipo de consumo no faturamento
	 */
	void setTipoConsumoFaturamento(EntidadeConteudo tipoConsumoFaturamento);
}
