package br.com.ggas.cadastro.imovel;

/**
 * Classe responsável pela representação dos campos de um Pop-Up de Pesquisa de Imóvel.
 * @author esantana
 *
 */
public class ImovelPopupVO {

	private Long idImovel;
	
	private String nomeFantasiaImovel;
	
	private String matriculaImovel;
	
	private String numeroImovel;
	
	private String cidadeImovel;
	
	private boolean condominio;

	public Long getIdImovel() {
		return idImovel;
	}

	public void setIdImovel(Long idImovel) {
		this.idImovel = idImovel;
	}

	public String getNomeFantasiaImovel() {
		return nomeFantasiaImovel;
	}

	public void setNomeFantasiaImovel(String nomeFantasiaImovel) {
		this.nomeFantasiaImovel = nomeFantasiaImovel;
	}

	public String getMatriculaImovel() {
		return matriculaImovel;
	}

	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}

	public String getNumeroImovel() {
		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}

	public String getCidadeImovel() {
		return cidadeImovel;
	}

	public void setCidadeImovel(String cidadeImovel) {
		this.cidadeImovel = cidadeImovel;
	}

	public boolean isCondominio() {
		return condominio;
	}

	public void setCondominio(boolean condominio) {
		this.condominio = condominio;
	}

	
	
}
