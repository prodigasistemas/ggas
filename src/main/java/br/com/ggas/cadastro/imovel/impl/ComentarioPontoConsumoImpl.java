/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * 
 * 
 */
class ComentarioPontoConsumoImpl extends EntidadeNegocioImpl implements ComentarioPontoConsumo {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -57778866697974959L;

	private Integer numero;

	private String descricao;

	private PontoConsumo pontoConsumo;

	private Usuario usuario;

	private Operacao operacao;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo#getNumero()
	 */
	@Override
	public Integer getNumero() {

		return numero;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo
	 * #setNumero(java.lang.Integer)
	 */
	@Override
	public void setNumero(Integer numero) {

		this.numero = numero;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo#getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo
	 * #setPontoConsumo(br.com
	 * .ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo#getUsuario()
	 */
	@Override
	public Usuario getUsuario() {

		return usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo
	 * #setUsuario(br.com.ggas
	 * .controleacesso.Usuario)
	 */
	@Override
	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo#getOperacao()
	 */
	@Override
	public Operacao getOperacao() {

		return operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo
	 * #setOperacao(br.com.ggas
	 * .controleacesso.Operacao)
	 */
	@Override
	public void setOperacao(Operacao operacao) {

		this.operacao = operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(numero == null || numero <= 0) {
			stringBuilder.append(NUMERO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(pontoConsumo == null) {
			stringBuilder.append(PONTO_CONSUMO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(usuario == null) {
			stringBuilder.append(USUARIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(operacao == null) {
			stringBuilder.append(OPERACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

}
