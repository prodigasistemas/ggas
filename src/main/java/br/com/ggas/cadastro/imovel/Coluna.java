/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface Coluna
 * 
 * @author Procenge
 */
public interface Coluna extends EntidadeNegocio {

	String BEAN_ID_COLUNA_TABELA = "colunaTabela";

	/**
	 * @return the nome
	 */
	String getNome();

	/**
	 * @param nome
	 */
	void setNome(String nome);

	/**
	 * @return the nomePropriedade
	 */
	String getNomePropriedade();

	/**
	 * @param nomePropriedade
	 */
	void setNomePropriedade(String nomePropriedade);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 */
	void setDescricao(String descricao);

	/**
	 * @return the consultaDinamica
	 */
	Boolean getConsultaDinamica();

	/**
	 * @param consultaDinamica
	 */
	void setConsultaDinamica(Boolean consultaDinamica);

	/**
	 * @return the atributosConsultaDinamica
	 */
	String getAtributosConsultaDinamica();

	/**
	 * @param atributosConsultaDinamica
	 */
	void setAtributosConsultaDinamica(String atributosConsultaDinamica);

	/**
	 * @return the tabela
	 */
	Tabela getTabela();

	/**
	 * @param tabela
	 */
	void setTabela(Tabela tabela);

	/**
	 * @return auditavel
	 */
	Boolean getAuditavel();

	/**
	 * @param auditavel
	 */
	void setAuditavel(Boolean auditavel);

	/**
	 * Checks if is alcada.
	 * 
	 * @return alcada
	 */
	Boolean isAlcada();

	/**
	 * @param alcada
	 *            the alcada to set
	 */
	void setAlcada(Boolean alcada);

	/**
	 * @return Boolean - retorna variávle.
	 */
	Boolean getVariavel();

	/**
	 * @param variavel - Set variável. 
	 */
	void setVariavel(Boolean variavel);

	/**
	 * @return String - retorna código variável.
	 */
	String getCodigoVariavel();

	/**
	 * @param codigoVariavel - Set código variável 
	 */
	void setCodigoVariavel(String codigoVariavel);

}
