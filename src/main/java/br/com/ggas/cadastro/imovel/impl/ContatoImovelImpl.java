/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.imovel.ContatoImovel;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * 
 * Classe responsável pelos métodos relacionados 
 * aos contatos de imóvel
 *
 */
public class ContatoImovelImpl extends EntidadeNegocioImpl implements ContatoImovel {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = -5841956077760493544L;

	private Imovel imovel;

	private TipoContato tipoContato;

	private Profissao profissao;

	private String nome;

	private String descricaoArea;

	private Integer codigoDDD;

	private String fone;

	private String ramal;

	private Integer codigoDDD2;

	private String fone2;

	private String ramal2;

	private String email;

	private boolean principal;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ContatoImovel
	 * #getProfissao()
	 */
	@Override
	public Profissao getProfissao() {

		return profissao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ContatoImovel
	 * #setProfissao
	 * (br.com.ggas.cadastro.cliente.Profissao)
	 */
	@Override
	public void setProfissao(Profissao profissao) {

		this.profissao = profissao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #getImovel()
	 */
	@Override
	public Imovel getImovel() {

		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #
	 * setImovel(br.com.ggas.cadastro.imovel.Imovel
	 * )
	 */
	@Override
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #getTipoContato()
	 */
	@Override
	public TipoContato getTipoContato() {

		return tipoContato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #
	 * setTipoContato(br.com.ggas.cadastro.cliente
	 * .TipoContato)
	 */
	@Override
	public void setTipoContato(TipoContato tipoContato) {

		this.tipoContato = tipoContato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #getNome()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ContatoImovel
	 * #getDescricaoArea()
	 */
	@Override
	public String getDescricaoArea() {

		return descricaoArea;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ContatoImovel
	 * #setDescricaoArea(java.lang.String)
	 */
	@Override
	public void setDescricaoArea(String descricaoArea) {

		this.descricaoArea = descricaoArea;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #getCodigoDDD()
	 */
	@Override
	public Integer getCodigoDDD() {

		return codigoDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #setCodigoDDD(java.lang.Integer)
	 */
	@Override
	public void setCodigoDDD(Integer codigoDDD) {

		this.codigoDDD = codigoDDD;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #getFone()
	 */
	@Override
	public String getFone() {

		return fone;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #setFone(java.lang.String)
	 */
	@Override
	public void setFone(String fone) {

		this.fone = fone;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #getRamal()
	 */
	@Override
	public String getRamal() {

		return ramal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #setRamal(java.lang.String)
	 */
	@Override
	public void setRamal(String ramal) {

		this.ramal = ramal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #getEmail()
	 */
	@Override
	public String getEmail() {

		return email;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(String email) {

		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #isPrincipal()
	 */
	@Override
	public boolean isPrincipal() {

		return principal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.cliente.impl.ContatoImovel
	 * #setPrincipal(boolean)
	 */
	@Override
	public void setPrincipal(boolean principal) {

		this.principal = principal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {


		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(tipoContato == null) {
			stringBuilder.append("Tipo do contato");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(nome)) {
			stringBuilder.append("Nome do contato");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(codigoDDD == null) {
			stringBuilder.append("DDD");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(fone)) {
			stringBuilder.append("Telefone");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

	/**
	 * @return the codigoDDD2
	 */
	public Integer getCodigoDDD2() {
		return codigoDDD2;
	}

	/**
	 * @param codigoDDD2 the codigoDDD2 to set
	 */
	public void setCodigoDDD2(Integer codigoDDD2) {
		this.codigoDDD2 = codigoDDD2;
	}

	/**
	 * @return the fone2
	 */
	public String getFone2() {
		return fone2;
	}

	/**
	 * @param fone2 the fone2 to set
	 */
	public void setFone2(String fone2) {
		this.fone2 = fone2;
	}

	/**
	 * @return the ramal2
	 */
	public String getRamal2() {
		return ramal2;
	}

	/**
	 * @param ramal2 the ramal2 to set
	 */
	public void setRamal2(String ramal2) {
		this.ramal2 = ramal2;
	}

	/**
	 * formata o telefone 1
	 * 
	 * @return telefoneFormatado
	 */
	public String getTelefone1Formatado() {

		StringBuilder telefone = new StringBuilder();

		if (this.getFone() != null) {

			if (this.getCodigoDDD() != null) {
				telefone.append("(").append(this.getCodigoDDD()).append(") ");
			}

			telefone.append(this.getFone());

			if (this.getRamal() != null) {
				telefone.append(" - ").append(this.getRamal());
			}
		}

		return telefone.toString();

	}

	/**
	 * formata o telefone 2
	 * 
	 * @return telefoneFormatado
	 */
	public String getTelefone2Formatado() {

		StringBuilder telefone = new StringBuilder();

		if (this.getFone2() != null) {

			if (this.getCodigoDDD2() != null) {
				telefone.append("(").append(this.getCodigoDDD2()).append(") ");
			}

			telefone.append(this.getFone2());

			if (this.getRamal2() != null) {
				telefone.append(" - ").append(this.getRamal2());
			}
		}

		return telefone.toString();

	}

}
