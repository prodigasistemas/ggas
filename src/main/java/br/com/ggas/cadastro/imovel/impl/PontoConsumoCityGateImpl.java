/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.cadastro.imovel.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pela implementação dos métodos
 * relacionados ao CityGate do ponto de consumo
 *
 */
public class PontoConsumoCityGateImpl extends EntidadeNegocioImpl implements PontoConsumoCityGate {

	private static final int PERCENTUAL_PARTICIPACAO_TOTAL = 100;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 433324933506366686L;

	private CityGate cityGate;

	private PontoConsumo pontoConsumo;

	private BigDecimal percentualParticipacao;

	private Date dataVigencia;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * PontoConsumoCityGate#getCityGate()
	 */
	@Override
	public CityGate getCityGate() {

		return cityGate;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * PontoConsumoCityGate
	 * #setCityGate(br.com.ggas
	 * .cadastro.operacional.CityGate)
	 */
	@Override
	public void setCityGate(CityGate cityGate) {

		this.cityGate = cityGate;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * PontoConsumoCityGate#getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * PontoConsumoCityGate
	 * #setPontoConsumo(br.com.
	 * ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * PontoConsumoCityGate
	 * #getPercentualParticipacao()
	 */
	@Override
	public BigDecimal getPercentualParticipacao() {

		return percentualParticipacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * PontoConsumoCityGate
	 * #setPercentualParticipacao
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPercentualParticipacao(BigDecimal percentualParticipacao) {

		this.percentualParticipacao = percentualParticipacao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * PontoConsumoCityGate#getDataVigencia()
	 */
	@Override
	public Date getDataVigencia() {
		Date data = null;
		if (this.dataVigencia != null) {
			data = (Date) dataVigencia.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel.impl. PontoConsumoCityGate
	 * #setDataVigencia(java.util.Date)
	 */
	@Override
	public void setDataVigencia(Date dataVigencia) {
		if (dataVigencia != null) {
			this.dataVigencia = (Date) dataVigencia.clone();
		} else {
			this.dataVigencia = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		StringBuilder stringBuilder = new StringBuilder();

		if(cityGate == null) {
			stringBuilder.append(CITY_GATE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(pontoConsumo == null) {
			stringBuilder.append(PONTO_CONSUMO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(dataVigencia == null) {
			stringBuilder.append(DATA_VIGENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(percentualParticipacao == null || percentualParticipacao.doubleValue() <= 0) {
			stringBuilder.append(PERCENTUAL_POR_CTIYGATE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if (percentualParticipacao.compareTo(new BigDecimal(PERCENTUAL_PARTICIPACAO_TOTAL)) != 0) {
				stringBuilder.append(PERCENTUAL_POR_CITYGATE_FAIXA_PERCENTUAL);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		return null;
	}

}
