package br.com.ggas.cadastro.imovel;


/**
 * Classe responsável por representar a potencia fixa
 *
 */
public class PotenciaFixaEquipamentoVO {
	
	private Long codPotenciaFixa;
	
	private String descricao;

	/**
	 * @return the descPotenciaFixa
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * @param descricao the descPotenciaFixa to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * @return the codPotenciaFixa
	 */
	public Long getCodPotenciaFixa() {
		return codPotenciaFixa;
	}

	/**
	 * @param codPotenciaFixa the codPotenciaFixa to set
	 */
	public void setCodPotenciaFixa(Long codPotenciaFixa) {
		this.codPotenciaFixa = codPotenciaFixa;
	}

}
