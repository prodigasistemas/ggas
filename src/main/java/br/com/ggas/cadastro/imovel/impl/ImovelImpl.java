/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.imovel.AreaConstruidaFaixa;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.ContatoImovel;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ImovelRamoAtividade;
import br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.PadraoConstrucao;
import br.com.ggas.cadastro.imovel.PavimentoCalcada;
import br.com.ggas.cadastro.imovel.PavimentoRua;
import br.com.ggas.cadastro.imovel.PerfilImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.RedeInternaImovel;
import br.com.ggas.cadastro.imovel.SituacaoImovel;
import br.com.ggas.cadastro.imovel.TipoBotijao;
import br.com.ggas.cadastro.levantamentomercado.agente.dominio.Agente;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Constantes;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

/**
 * Classe responsável pela representação de um imovel
 *
 */
public class ImovelImpl extends EntidadeNegocioImpl implements Imovel {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = -1347476139984790251L;

	private Imovel imovelCondominio;

	private Quadra quadra;

	private QuadraFace quadraFace;

	private String enderecoReferencia;

	private AreaConstruidaFaixa areaConstruidaFaixa;

	private PavimentoCalcada pavimentoCalcada;

	private PavimentoRua pavimentoRua;

	private SituacaoImovel situacaoImovel;

	private PerfilImovel perfilImovel;

	private PadraoConstrucao padraoConstrucao;

	private TipoBotijao tipoBotijao;

	private Empresa empresa;

	private RedeInternaImovel redeInternaImovel;

	private ModalidadeMedicaoImovel modalidadeMedicaoImovel;

	private Integer numeroLote;

	private Integer numeroSublote;

	private Integer numeroTestada;

	private Integer quantidadeUnidadeConsumidora;

	private Integer quantidadePontoConsumo;

	private Integer numeroSequenciaLeitura;

	private Integer quantidadeBloco;

	private Integer quantidadeAndar;

	private Integer quantidadeBanheiro;

	private Integer quantidadeApartamentoAndar;

	private String numeroImovel;

	private String descricaoComplemento;

	private String nome;

	private String cep;

	private Date dataEntrega;

	private Boolean condominio;

	private Boolean portaria;

	private Boolean valvulaBloqueio;

	private Boolean redePreexistente;

	private Rota rota;

	private Date dataPrevisaoEncerramentoObra;

	private Boolean indicadorObraTubulacao;

	private Collection<ClienteImovel> listaClienteImovel = new HashSet<>();

	private Collection<ContatoImovel> contatos = new HashSet<>();

	private Collection<ImovelRamoAtividade> unidadesConsumidoras = new HashSet<>();

	private Collection<PontoConsumo> listaPontoConsumo = new HashSet<>();

	private Collection<ImovelServicoTipoRestricao> listaImovelServicoTipoRestricao = new HashSet<>();

	private Agente agente;

	private EntidadeConteudo tipoCombustivel;
	
	private Boolean indicadorAlterarImovelCondominioFilho;
	
	private Boolean indicadorRestricaoServico;
	
	private Long idImovelCondominio;
	
	private String situacaoPontoConsumo;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * getQuantidadeBanheiro()
	 */

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * getImovelCondominio()
	 */
	@Override
	public Imovel getImovelCondominio() {

		return imovelCondominio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * setImovelCondominio
	 * (br.com.ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public void setImovelCondominio(Imovel imovelCondominio) {

		this.imovelCondominio = imovelCondominio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Imovel#getQuadra
	 * ()
	 */
	@Override
	public Quadra getQuadra() {

		return quadra;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Imovel#setQuadra
	 * (br.com.ggas.cadastro.localidade.Quadra)
	 */
	@Override
	public void setQuadra(Quadra quadra) {

		this.quadra = quadra;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getQuadraFace()
	 */
	@Override
	public QuadraFace getQuadraFace() {

		return quadraFace;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setQuadraFace
	 * (br.com.ggas.cadastro.localidade
	 * .QuadraFace)
	 */
	@Override
	public void setQuadraFace(QuadraFace quadraFace) {

		this.quadraFace = quadraFace;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getNumeroLote()
	 */
	@Override
	public Integer getNumeroLote() {

		return numeroLote;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setNumeroLote(java.lang.Integer)
	 */
	@Override
	public void setNumeroLote(Integer numeroLote) {

		this.numeroLote = numeroLote;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getNumeroSublote()
	 */
	@Override
	public Integer getNumeroSublote() {

		return numeroSublote;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setNumeroSublote(java.lang.Integer)
	 */
	@Override
	public void setNumeroSublote(Integer numeroSublote) {

		this.numeroSublote = numeroSublote;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getNumeroTestada()
	 */
	@Override
	public Integer getNumeroTestada() {

		return numeroTestada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setNumeroTestada(java.lang.Integer)
	 */
	@Override
	public void setNumeroTestada(Integer numeroTestada) {

		this.numeroTestada = numeroTestada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getNumeroImovel()
	 */
	@Override
	public String getNumeroImovel() {

		return numeroImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setNumeroImovel(java.lang.String)
	 */
	@Override
	public void setNumeroImovel(String numeroImovel) {

		this.numeroImovel = numeroImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getDescricaoComplemento()
	 */
	@Override
	public String getDescricaoComplemento() {

		return descricaoComplemento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setDescricaoComplemento(java.lang.String)
	 */
	@Override
	public void setDescricaoComplemento(String descricaoComplemento) {

		this.descricaoComplemento = descricaoComplemento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getEnderecoReferencia()
	 */
	@Override
	public String getEnderecoReferencia() {

		return enderecoReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setEnderecoReferencia(java.lang.String)
	 */
	@Override
	public void setEnderecoReferencia(String enderecoReferencia) {

		this.enderecoReferencia = enderecoReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getNome()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getAreaConstruidaFaixa()
	 */
	@Override
	public AreaConstruidaFaixa getAreaConstruidaFaixa() {

		return areaConstruidaFaixa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setAreaConstruidaFaixa
	 * (br.com.ggas.cadastro
	 * .imovel.AreaConstruidaFaixa)
	 */
	@Override
	public void setAreaConstruidaFaixa(AreaConstruidaFaixa areaConstruidaFaixa) {

		this.areaConstruidaFaixa = areaConstruidaFaixa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getPavimentoCalcada()
	 */
	@Override
	public PavimentoCalcada getPavimentoCalcada() {

		return pavimentoCalcada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setPavimentoCalcada
	 * (br.com.ggas.cadastro.imovel
	 * .PavimentoCalcada)
	 */
	@Override
	public void setPavimentoCalcada(PavimentoCalcada pavimentoCalcada) {

		this.pavimentoCalcada = pavimentoCalcada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getPavimentoRua()
	 */
	@Override
	public PavimentoRua getPavimentoRua() {

		return pavimentoRua;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setPavimentoRua
	 * (br.com.ggas.cadastro.imovel.PavimentoRua)
	 */
	@Override
	public void setPavimentoRua(PavimentoRua pavimentoRua) {

		this.pavimentoRua = pavimentoRua;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getSituacaoImovel()
	 */
	@Override
	public SituacaoImovel getSituacaoImovel() {

		return situacaoImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setSituacaoImovel
	 * (br.com.ggas.cadastro.imovel
	 * .SituacaoImovel)
	 */
	@Override
	public void setSituacaoImovel(SituacaoImovel situacaoImovel) {

		this.situacaoImovel = situacaoImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getPerfilImovel()
	 */
	@Override
	public PerfilImovel getPerfilImovel() {

		return perfilImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setPerfilImovel
	 * (br.com.ggas.cadastro.imovel.PerfilImovel)
	 */
	@Override
	public void setPerfilImovel(PerfilImovel perfilImovel) {

		this.perfilImovel = perfilImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getPadraoConstrucao()
	 */
	@Override
	public PadraoConstrucao getPadraoConstrucao() {

		return padraoConstrucao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setPadraoConstrucao
	 * (br.com.ggas.cadastro.imovel
	 * .PadraoConstrucao)
	 */
	@Override
	public void setPadraoConstrucao(PadraoConstrucao padraoConstrucao) {

		this.padraoConstrucao = padraoConstrucao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getQuantidadeUnidadeConsumidora()
	 */
	@Override
	public Integer getQuantidadeUnidadeConsumidora() {

		return quantidadeUnidadeConsumidora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setQuantidadeUnidadeConsumidora
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadeUnidadeConsumidora(Integer quantidadeUnidadeConsumidora) {

		this.quantidadeUnidadeConsumidora = quantidadeUnidadeConsumidora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getQuantidadePontoConsumo()
	 */
	@Override
	public Integer getQuantidadePontoConsumo() {

		return quantidadePontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setQuantidadePontoConsumo
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadePontoConsumo(Integer quantidadePontoConsumo) {

		this.quantidadePontoConsumo = quantidadePontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getNumeroSequenciaLeitura()
	 */
	@Override
	public Integer getNumeroSequenciaLeitura() {

		return numeroSequenciaLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setNumeroSequenciaLeitura
	 * (java.lang.Integer)
	 */
	@Override
	public void setNumeroSequenciaLeitura(Integer numeroSequenciaLeitura) {

		this.numeroSequenciaLeitura = numeroSequenciaLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getCondominio()
	 */
	@Override
	public Boolean getCondominio() {

		return condominio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setCondominio(java.lang.Boolean)
	 */
	@Override
	public void setCondominio(Boolean condominio) {

		this.condominio = condominio;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel.impl.Imovel #getDataEntrega()
	 */
	@Override
	public Date getDataEntrega() {
		Date data = null;
		if (this.dataEntrega != null) {
			data = (Date) dataEntrega.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setDataEntrega(java.util.Date)
	 */
	@Override
	public void setDataEntrega(Date dataEntrega) {
		if(dataEntrega != null){
			this.dataEntrega = (Date) dataEntrega.clone();
		} else {
			this.dataEntrega = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getPortaria()
	 */
	@Override
	public Boolean getPortaria() {

		return portaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setPortaria(java.lang.Boolean)
	 */
	@Override
	public void setPortaria(Boolean portaria) {

		this.portaria = portaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getValvulaBloqueio()
	 */
	@Override
	public Boolean getValvulaBloqueio() {

		return valvulaBloqueio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setValvulaBloqueio(java.lang.Boolean)
	 */
	@Override
	public void setValvulaBloqueio(Boolean valvulaBloqueio) {

		this.valvulaBloqueio = valvulaBloqueio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getTipoBotijao()
	 */
	@Override
	public TipoBotijao getTipoBotijao() {

		return tipoBotijao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setTipoBotijao
	 * (br.com.ggas.cadastro.imovel.TipoBotijao)
	 */
	@Override
	public void setTipoBotijao(TipoBotijao tipoBotijao) {

		this.tipoBotijao = tipoBotijao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getEmpresa()
	 */
	@Override
	public Empresa getEmpresa() {

		return empresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setEmpresa
	 * (br.com.ggas.cadastro.empresa.Empresa)
	 */
	@Override
	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getQuantidadeBloco()
	 */
	@Override
	public Integer getQuantidadeBloco() {

		return quantidadeBloco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setQuantidadeBloco(java.lang.Integer)
	 */
	@Override
	public void setQuantidadeBloco(Integer quantidadeBloco) {

		this.quantidadeBloco = quantidadeBloco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getQuantidadeAndar()
	 */
	@Override
	public Integer getQuantidadeAndar() {

		return quantidadeAndar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setQuantidadeAndar(java.lang.Integer)
	 */
	@Override
	public void setQuantidadeAndar(Integer quantidadeAndar) {

		this.quantidadeAndar = quantidadeAndar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getQuantidadeApartamentoAndar()
	 */
	@Override
	public Integer getQuantidadeApartamentoAndar() {

		return quantidadeApartamentoAndar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setQuantidadeApartamentoAndar
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadeApartamentoAndar(Integer quantidadeApartamentoAndar) {

		this.quantidadeApartamentoAndar = quantidadeApartamentoAndar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #getRedePreexistente()
	 */
	@Override
	public Boolean getRedePreexistente() {

		return redePreexistente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Imovel
	 * #setRedePreexistente(java.lang.Boolean)
	 */
	@Override
	public void setRedePreexistente(Boolean redePreexistente) {

		this.redePreexistente = redePreexistente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * getListaClienteImovel()
	 */
	@Override
	public Collection<ClienteImovel> getListaClienteImovel() {

		return listaClienteImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * setListaClienteImovel(java.util.Collection)
	 */
	@Override
	public void setListaClienteImovel(Collection<ClienteImovel> listaClienteImovel) {

		this.listaClienteImovel = listaClienteImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Imovel#getContatos
	 * ()
	 */
	@Override
	public Collection<ContatoImovel> getContatos() {

		return contatos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Imovel#setContatos
	 * (java.util.Collection)
	 */
	@Override
	public void setContatos(Collection<ContatoImovel> contatos) {

		this.contatos = contatos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * getUnidadesConsumidoras()
	 */
	@Override
	public Collection<ImovelRamoAtividade> getUnidadesConsumidoras() {

		return unidadesConsumidoras;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * setUnidadesConsumidoras
	 * (java.util.Collection)
	 */
	@Override
	public void setUnidadesConsumidoras(Collection<ImovelRamoAtividade> unidadesConsumidoras) {

		this.unidadesConsumidoras = unidadesConsumidoras;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * getRedeInternaImovel()
	 */
	@Override
	public RedeInternaImovel getRedeInternaImovel() {

		return redeInternaImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * setRedeInternaImovel
	 * (br.com.ggas.cadastro.imovel
	 * .RedeInternaImovel)
	 */
	@Override
	public void setRedeInternaImovel(RedeInternaImovel redeInternaImovel) {

		this.redeInternaImovel = redeInternaImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * getModalidadeMedicaoImovel()
	 */
	@Override
	public ModalidadeMedicaoImovel getModalidadeMedicaoImovel() {

		return modalidadeMedicaoImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * setModalidadeMedicaoImovel
	 * (br.com.ggas.cadastro
	 * .imovel.ModalidadeMedicaoImovel)
	 */
	@Override
	public void setModalidadeMedicaoImovel(ModalidadeMedicaoImovel modalidadeMedicaoImovel) {

		this.modalidadeMedicaoImovel = modalidadeMedicaoImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * getListaPontoConsumo()
	 */
	@Override
	public Collection<PontoConsumo> getListaPontoConsumo() {

		return listaPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * setListaPontoConsumo(java.util.Collection)
	 */
	@Override
	public void setListaPontoConsumo(Collection<PontoConsumo> listaPontoConsumo) {

		this.listaPontoConsumo = listaPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Imovel#getCep()
	 */
	@Override
	public String getCep() {

		return cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Imovel#setCep
	 * (java.lang.String)
	 */
	@Override
	public void setCep(String cep) {

		this.cep = cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Imovel#getInscricao
	 * ()
	 */
	@Override
	public String getInscricao() {

		StringBuilder stringBuilder = new StringBuilder();

		if (this.getQuadraFace() != null && this.getQuadraFace().getQuadra() != null
						&& this.getQuadraFace().getQuadra().getSetorComercial() != null
						&& this.getQuadraFace().getQuadra().getSetorComercial().getLocalidade() != null) {
			stringBuilder.append(this.getQuadraFace().getQuadra().getSetorComercial().getLocalidade().getChavePrimaria());
			stringBuilder.append(".");
			stringBuilder.append(this.getQuadraFace().getQuadra().getSetorComercial().getCodigo());
			stringBuilder.append(".");
			stringBuilder.append(this.getQuadraFace().getQuadra().getNumeroQuadra());
			stringBuilder.append(".");
			stringBuilder.append(this.getQuadraFace().getChavePrimaria());
			if (this.getNumeroLote() != null) {
				stringBuilder.append(".");
				stringBuilder.append(this.numeroLote);
			}
			if (this.getNumeroSublote() != null) {
				stringBuilder.append(".");
				stringBuilder.append(this.numeroSublote);
			}

		}

		return stringBuilder.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Imovel#
	 * getEnderecoFormatado()
	 */
	@Override
	public String getEnderecoFormatado() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";
		Cep cep = null;
		Endereco endereco = null;
		if (this.getQuadraFace() != null) {
			endereco = this.getQuadraFace().getEndereco();
			if (endereco != null) {
				cep = endereco.getCep();
				if (cep != null) {
					if (cep.getTipoLogradouro() != null) {
						enderecoFormatado.append(cep.getTipoLogradouro());
					}
					if (cep.getLogradouro() != null) {
						if(enderecoFormatado.length() > 0){
							separador = " ";
						}else{
							separador = "";
						}
						enderecoFormatado.append(separador);
						enderecoFormatado.append(cep.getLogradouro());
					}
					if (this.numeroImovel != null) {
						if(enderecoFormatado.length() > 0){
							separador = ", ";
						}else{
							separador = "";
						}
						enderecoFormatado.append(separador);
						enderecoFormatado.append(this.numeroImovel);
					}

					if (cep.getBairro() != null) {
						if(enderecoFormatado.length() > 0){
							separador = ", ";
						}else{
							separador = "";
						}
						enderecoFormatado.append(separador);
						enderecoFormatado.append(cep.getBairro());
					}
					if (cep.getNomeMunicipio() != null) {
						if(enderecoFormatado.length() > 0){
							separador = ", ";
						}else{
							separador = "";
						}
						enderecoFormatado.append(separador);
						enderecoFormatado.append(cep.getNomeMunicipio());
					}
					if (cep.getUf() != null) {
						if(enderecoFormatado.length() > 0){
							separador = ", ";
						}else{
							separador = "";
						}
						enderecoFormatado.append(separador);
						enderecoFormatado.append(cep.getUf());
					}
				}
			}

		}
		return enderecoFormatado.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel.Imovel# getEnderecoLogNumBairro()
	 */
	@Override
	public String getEnderecoLogNumBairro() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";
		Cep cepEndereco = null;
		Endereco endereco = null;

		if (this.getQuadraFace() != null && this.getQuadraFace().getEndereco() != null
				&& this.getQuadraFace().getEndereco().getCep() != null) {

			endereco = this.getQuadraFace().getEndereco();
			cepEndereco = endereco.getCep();

			if (cepEndereco.getTipoLogradouro() != null) {
				enderecoFormatado.append(cepEndereco.getTipoLogradouro());
			}

			if (cepEndereco.getLogradouro() != null) {
				if (enderecoFormatado.length() > 0) {
					separador = " ";
				} else {
					separador = "";
				}
				enderecoFormatado.append(separador);
				enderecoFormatado.append(cepEndereco.getLogradouro());
			}

			if (this.numeroImovel != null) {
				if (enderecoFormatado.length() > 0) {
					separador = ", ";
				} else {
					separador = "";
				}
				enderecoFormatado.append(separador);
				enderecoFormatado.append(this.numeroImovel);
			}

			if (cepEndereco.getBairro() != null) {
				if (enderecoFormatado.length() > 0) {
					separador = ", ";
				} else {
					separador = "";
				}
				enderecoFormatado.append(separador);
				enderecoFormatado.append(cepEndereco.getBairro());
			}
		}

		return enderecoFormatado.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel.Imovel# getEnderecoMuniUF()
	 */
	@Override
	public String getEnderecoMuniUF() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";
		Cep cepEndereco = null;
		Endereco endereco = null;

		if (this.getQuadraFace() != null && this.getQuadraFace().getEndereco() != null
				&& this.getQuadraFace().getEndereco().getCep() != null) {

			endereco = this.getQuadraFace().getEndereco();
			cepEndereco = endereco.getCep();

			if (cepEndereco.getNomeMunicipio() != null) {
				if (enderecoFormatado.length() > 0) {
					separador = ", ";
				} else {
					separador = "";
				}
				enderecoFormatado.append(separador);
				enderecoFormatado.append(cepEndereco.getNomeMunicipio());
			}
			if (cepEndereco.getUf() != null) {
				if (enderecoFormatado.length() > 0) {
					separador = "/";
				} else {
					separador = "";
				}
				enderecoFormatado.append(separador);
				enderecoFormatado.append(cepEndereco.getUf());
			}
		}

		return enderecoFormatado.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (nome == null) {
			stringBuilder.append(NOME);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (StringUtils.isEmpty(numeroImovel)) {
			stringBuilder.append(NUMERO_IMOVEL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (quadraFace == null) {
			stringBuilder.append(QUADRA_FACE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (situacaoImovel == null) {
			stringBuilder.append(SITUACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (condominio == null) {
			stringBuilder.append(CONDOMINIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (stringBuilder.length() > 0) {
			stringBuilder.insert(0, "da aba Identificação e Localização: ");
		}


		if (condominio != null && condominio) {
			final StringBuilder stringBuilderAbaCaracteristica = new StringBuilder();
			boolean erroAbaCaracteristica = false;

			final boolean todosCamposNullos = (modalidadeMedicaoImovel == null) && (quantidadeAndar == null)
					&& (quantidadeApartamentoAndar == null) && (quantidadeBloco == null);

			if (!todosCamposNullos) {
				if (modalidadeMedicaoImovel == null) {
					stringBuilderAbaCaracteristica.append(MODALIDADE_MEDICAO);
					stringBuilderAbaCaracteristica.append(Constantes.STRING_VIRGULA_ESPACO);
					erroAbaCaracteristica = true;
				}
				if (quantidadeBloco == null) {
					stringBuilderAbaCaracteristica.append(QUANTIDADE_BLOCO);
					stringBuilderAbaCaracteristica.append(Constantes.STRING_VIRGULA_ESPACO);
					erroAbaCaracteristica = true;
				}
				if (quantidadeAndar == null) {
					stringBuilderAbaCaracteristica.append(QUANTIDADE_ANDAR);
					stringBuilderAbaCaracteristica.append(Constantes.STRING_VIRGULA_ESPACO);
					erroAbaCaracteristica = true;
				}
				if (quantidadeApartamentoAndar == null) {
					stringBuilderAbaCaracteristica.append(QUANTIDADE_APARTAMENTO_ANDAR);
					stringBuilderAbaCaracteristica.append(Constantes.STRING_VIRGULA_ESPACO);
					erroAbaCaracteristica = true;
				}
			}
			if (erroAbaCaracteristica) {
				stringBuilderAbaCaracteristica.insert(0, " e da aba Característica: ");
				stringBuilder.append(stringBuilderAbaCaracteristica.toString());
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

	@Override
	public Integer getQuantidadeBanheiro() {

		return quantidadeBanheiro;
	}

	@Override
	public void setQuantidadeBanheiro(Integer quantidadeBanheiro) {

		this.quantidadeBanheiro = quantidadeBanheiro;
	}

	@Override
	public Rota getRota() {

		return rota;
	}

	@Override
	public void setRota(Rota rota) {

		this.rota = rota;
	}

	@Override
	public Date getDataPrevisaoEncerramentoObra() {
		Date data = null;
		if(this.dataPrevisaoEncerramentoObra != null) {
			data = (Date) dataPrevisaoEncerramentoObra.clone();
		}
		return data;
	}

	@Override
	public void setDataPrevisaoEncerramentoObra(Date dataPrevisaoEncerramentoObra) {
		if(dataPrevisaoEncerramentoObra != null){
			this.dataPrevisaoEncerramentoObra = (Date) dataPrevisaoEncerramentoObra.clone();
		} else {
			this.dataPrevisaoEncerramentoObra = null;
		}
	}

	@Override
	public Boolean getIndicadorObraTubulacao() {

		return indicadorObraTubulacao;
	}

	@Override
	public void setIndicadorObraTubulacao(Boolean indicadorObraTubulacao) {

		this.indicadorObraTubulacao = indicadorObraTubulacao;
	}

	@Override
	public Agente getAgente() {

		return agente;
	}

	@Override
	public void setAgente(Agente agente) {

		this.agente = agente;
	}

	@Override
	public EntidadeConteudo getTipoCombustivel() {
		return this.tipoCombustivel;
	}

	@Override
	public void setTipoCombustivel(EntidadeConteudo tipoCombustivel) {
		this.tipoCombustivel = tipoCombustivel;
	}

	@Override
	public Boolean getIndicadorAlterarImovelCondominioFilho() {
		return indicadorAlterarImovelCondominioFilho;
	}

	@Override
	public void setIndicadorAlterarImovelCondominioFilho(Boolean indicadorAlterarImovelCondominioFilho) {
		this.indicadorAlterarImovelCondominioFilho = indicadorAlterarImovelCondominioFilho;
	}

	@Override
	public Boolean getIndicadorRestricaoServico() {
		return indicadorRestricaoServico;
	}

	@Override
	public void setIndicadorRestricaoServico(Boolean indicadorRestricaoServico) {
		this.indicadorRestricaoServico = indicadorRestricaoServico;
	}

	/**
	 * @return the listaImovelServicoTipoRestricao
	 */
	@Override
	public Collection<ImovelServicoTipoRestricao> getListaImovelServicoTipoRestricao() {
		return listaImovelServicoTipoRestricao;
	}

	/**
	 * @param listaImovelServicoTipoRestricao the listaImovelServicoTipoRestricao to set
	 */
	@Override
	public void setListaImovelServicoTipoRestricao(Collection<ImovelServicoTipoRestricao> listaImovelServicoTipoRestricao) {
		this.listaImovelServicoTipoRestricao = listaImovelServicoTipoRestricao;
	}

	@Override
	public Boolean getHabilitado() {
		return isHabilitado();
	}
	
	@Override
	public Boolean isIndividual() {
		return Optional.ofNullable(getModalidadeMedicaoImovel())
				.map(a -> a.getCodigo() == ModalidadeMedicaoImovel.INDIVIDUAL).orElse(null);
	}

	public Long getIdImovelCondominio() {
		return idImovelCondominio;
	}

	public void setIdImovelCondominio(Long idImovelCondominio) {
		this.idImovelCondominio = idImovelCondominio;
	}

	@Override
	public String getSituacaoPontoConsumo() {
		return situacaoPontoConsumo;
	}

	@Override
	public void setSituacaoPontoConsumo(String situacaoPontoConsumo) {
		this.situacaoPontoConsumo = situacaoPontoConsumo;
	}
}
