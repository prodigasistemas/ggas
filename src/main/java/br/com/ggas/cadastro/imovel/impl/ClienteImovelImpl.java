/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.MotivoFimRelacionamentoClienteImovel;
import br.com.ggas.cadastro.imovel.TipoRelacionamentoClienteImovel;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

class ClienteImovelImpl extends EntidadeNegocioImpl implements ClienteImovel {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = -4196381400018812050L;

	private Imovel imovel;

	private Cliente cliente;

	private TipoRelacionamentoClienteImovel tipoRelacionamentoClienteImovel;

	private MotivoFimRelacionamentoClienteImovel motivoFimRelacionamentoClienteImovel;

	private Date relacaoInicio;

	private Date relacaoFim;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #getImovel()
	 */
	@Override
	public Imovel getImovel() {

		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #
	 * setImovel(br.com.ggas.cadastro.imovel.Imovel
	 * )
	 */
	@Override
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #getCliente()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #
	 * setCliente(br.com.ggas.cadastro.cliente.Cliente
	 * )
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #getTipoRelacionamentoClienteImovel()
	 */
	@Override
	public TipoRelacionamentoClienteImovel getTipoRelacionamentoClienteImovel() {

		return tipoRelacionamentoClienteImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #
	 * setTipoRelacionamentoClienteImovel(br.com.ggas
	 * .
	 * cadastro.imovel.TipoRelacionamentoClienteImovel
	 * )
	 */
	@Override
	public void setTipoRelacionamentoClienteImovel(TipoRelacionamentoClienteImovel tipoRelacionamentoClienteImovel) {

		this.tipoRelacionamentoClienteImovel = tipoRelacionamentoClienteImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #getMotivoFimRelacionamentoClienteImovel()
	 */
	@Override
	public MotivoFimRelacionamentoClienteImovel getMotivoFimRelacionamentoClienteImovel() {

		return motivoFimRelacionamentoClienteImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #
	 * setMotivoFimRelacionamentoClienteImovel(br.
	 * com.ggas.cadastro.imovel.
	 * MotivoFimRelacionamentoClienteImovel)
	 */
	@Override
	public void setMotivoFimRelacionamentoClienteImovel(MotivoFimRelacionamentoClienteImovel motivoFimRelacionamentoClienteImovel) {

		this.motivoFimRelacionamentoClienteImovel = motivoFimRelacionamentoClienteImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #getRelacaoInicio()
	 */
	@Override
	public Date getRelacaoInicio() {

		return relacaoInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #setRelacaoInicio(java.util.Date)
	 */
	@Override
	public void setRelacaoInicio(Date relacaoInicio) {

		this.relacaoInicio = relacaoInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #getRelacaoFim()
	 */
	@Override
	public Date getRelacaoFim() {

		return relacaoFim;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.ClienteImovel
	 * #setRelacaoFim(java.util.Date)
	 */
	@Override
	public void setRelacaoFim(Date relacaoFim) {

		this.relacaoFim = relacaoFim;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.cliente == null) {
			stringBuilder.append(CLIENTE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(this.tipoRelacionamentoClienteImovel == null) {
			stringBuilder.append(TIPO_RELACIOMANENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(this.relacaoInicio == null) {
			stringBuilder.append(RELACAO_INICIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(this.motivoFimRelacionamentoClienteImovel != null || this.relacaoFim != null) {
			if(this.motivoFimRelacionamentoClienteImovel == null) {
				stringBuilder.append(MOTIVO_FIM_RELACIONAMENTO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(this.relacaoFim == null) {
				stringBuilder.append(RELACAO_FIM);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

}
