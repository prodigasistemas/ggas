/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 28/03/2014 14:41:20
 @author ifrancisco
 */

package br.com.ggas.cadastro.imovel;

import java.math.BigDecimal;

/**
 * Ponto de Consumo DetalhadoVO
 * 
 * @author ifrancisco
 */
public class PontoConsumoDetalhadoVO {

	private String anoMesLeitura;

	private BigDecimal numeroLeituraInformada;

	private BigDecimal consumoInformado;

	private BigDecimal fatorPTZCorretor;

	private BigDecimal consumoReal;

	/**
	 * @return {@link String} anoMesLeitura
	 */
	public String getAnoMesLeitura() {

		return anoMesLeitura;
	}

	/**
	 * Define o anoMesLeitura.
	 * @param String anoMesLeitura
	 */
	public void setAnoMesLeitura(String anoMesLeitura) {

		this.anoMesLeitura = anoMesLeitura;
	}

	/**
	 * @return {@link BigDecimal} numeroLeituraInformada
	 */
	public BigDecimal getNumeroLeituraInformada() {

		return numeroLeituraInformada;
	}

	/**
	 * Define o número de leitura informada.
	 * @param numeroLeituraInformada
	 */
	public void setNumeroLeituraInformada(BigDecimal numeroLeituraInformada) {
		this.numeroLeituraInformada = numeroLeituraInformada;
	}

	/**
	 * @return {@link BigDecimal} consumoInformado
	 */
	public BigDecimal getConsumoInformado() {

		return consumoInformado;
	}

	/**
	 * Define o consumo informado.
	 * @param consumoInformado
	 */
	public void setConsumoInformado(BigDecimal consumoInformado) {

		this.consumoInformado = consumoInformado;
	}

	/**
	 * @return {@link BigDecimal} fatorPTZCorretor
	 */
	public BigDecimal getFatorPTZCorretor() {

		return fatorPTZCorretor;
	}

	/**
	 * Define o fator PTZ Corretor
	 * @param fatorPTZCorretor
	 */
	public void setFatorPTZCorretor(BigDecimal fatorPTZCorretor) {

		this.fatorPTZCorretor = fatorPTZCorretor;
	}

	/**
	 * @return {@link BigDecimal} consumoReal 
	 */
	public BigDecimal getConsumoReal() {

		return consumoReal;
	}

	/**
	 * Define o valor de consumo Real
	 * @param consumoReal
	 */
	public void setConsumoReal(BigDecimal consumoReal) {

		this.consumoReal = consumoReal;
	}

}
