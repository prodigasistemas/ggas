/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.util.Collection;
import java.util.Date;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.levantamentomercado.agente.dominio.Agente;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.rota.Rota;

/**
 * Classe responsável pela representação de um imovel
 *
 */
public interface Imovel extends EntidadeNegocio {

	String BEAN_ID_IMOVEL = "imovel";

	String IMOVEL_ROTULO = "IMOVEL_ROTULO";

	String IMOVEIS = "IMOVEIS_ROTULO";

	String NOME = "IMOVEL_NOME";

	String NUMERO_IMOVEL = "IMOVEL_NUMERO_IMOVEL";

	String CEP_IMOVEL = "IMOVEL_CEP_IMOVEL";

	String COMPLEMENTO_IMOVEL = "IMOVEL_COMPLEMENTO_IMOVEL";

	String QUADRA = "IMOVEL_QUADRA";

	String QUADRA_FACE = "IMOVEL_QUADRA_FACE";

	String NUMERO_LOTE = "IMOVEL_NUMERO_LOTE";

	String NUMERO_SUBLOTE = "IMOVEL_NUMERO_SUBLOTE";

	String TESTADA_LOTE = "IMOVEL_TESTADA_LOTE";

	String SITUACAO = "IMOVEL_SITUACAO";

	String PAVIMENTO_CALCADA = "IMOVEL_PAVIMENTO_CALCADA";

	String PAVIMENTO_RUA = "IMOVEL_PAVIMENTO_RUA";

	String CONDOMINIO = "IMOVEL_CONDOMINIO";

	String MODALIDADE_MEDICAO = "IMOVEL_MODALIDADE_MEDICAO";

	String TIPO_BOTIJAO = "IMOVEL_TIPO_BOTIJAO";

	String DATA_ENTREGA = "IMOVEL_DATA_ENTREGA";

	String QUANTIDADE_PRUMADA = "IMOVEL_QUANTIDADE_PRUMADA";

	String QUANTIDADE_REGULADOR_HALL = "IMOVEL_QUANTIDADE_REGULADOR_HALL";

	String MATRICULA = "IMOVEL_MATRICULA";

	String MATRICULA_CONDOMINIO = "IMOVEL_MATRICULA_CONDOMINIO";

	String NOME_MEDICAO = "IMOVEL_NOME_MEDICAO";

	String QUANTIDADE_BLOCO = "IMOVEL_QUANTIDADE_BLOCO";

	String QUANTIDADE_ANDAR = "IMOVEL_QUANTIDADE_ANDAR";

	String QUANTIDADE_APARTAMENTO_ANDAR = "IMOVEL_QUANTIDADE_APARTAMENTO_ANDAR";

	String QUANTIDADE_BANHEIRO = "IMOVEL_QUANTIDADE_BANHEIRO";

	String IMOVEL_BLOCO = "IMOVEL_BLOCO";

	String IMOVEL_APARTAMENTO = "IMOVEL_APARTAMENTO";

	String IMOVEL_DATA_PREVISAO_ENCERRAMENTO_OBRA = "IMOVEL_DATA_PREVISAO_ENCERRAMENTO_OBRA";

	String LISTA_PONTO_CONSUMO = "listaPontoConsumo";

	/**
	 * @return the imovelCondominio
	 */
	Imovel getImovelCondominio();

	/**
	 * @param imovelCondominio
	 *            the imovelCondominio to set
	 */
	void setImovelCondominio(Imovel imovelCondominio);

	/**
	 * @return the quadra
	 */
	Quadra getQuadra();

	/**
	 * @param quadra
	 *            the quadra to set
	 */
	void setQuadra(Quadra quadra);

	/**
	 * @return the quadraFace
	 */
	QuadraFace getQuadraFace();

	/**
	 * @param quadraFace
	 *            the quadraFace to set
	 */
	void setQuadraFace(QuadraFace quadraFace);

	/**
	 * @return the numeroLote
	 */
	Integer getNumeroLote();

	/**
	 * @param numeroLote
	 *            the numeroLote to set
	 */
	void setNumeroLote(Integer numeroLote);

	/**
	 * @return the numeroSublote
	 */
	Integer getNumeroSublote();

	/**
	 * @param numeroSublote
	 *            the numeroSublote to set
	 */
	void setNumeroSublote(Integer numeroSublote);

	/**
	 * @return the numeroTestada
	 */
	Integer getNumeroTestada();

	/**
	 * @param numeroTestada
	 *            the numeroTestada to set
	 */
	void setNumeroTestada(Integer numeroTestada);

	/**
	 * @return the numeroImovel
	 */
	String getNumeroImovel();

	/**
	 * @param numeroImovel
	 *            the numeroImovel to set
	 */
	void setNumeroImovel(String numeroImovel);

	/**
	 * @return the descricaoComplemento
	 */
	String getDescricaoComplemento();

	/**
	 * @param descricaoComplemento
	 *            the descricaoComplemento to set
	 */
	void setDescricaoComplemento(String descricaoComplemento);

	/**
	 * @return the enderecoReferencia
	 */
	String getEnderecoReferencia();

	/**
	 * @param enderecoReferencia
	 *            the enderecoReferencia to set
	 */
	void setEnderecoReferencia(String enderecoReferencia);

	/**
	 * @return the nome
	 */
	String getNome();

	/**
	 * @param nome
	 *            the nome to set
	 */
	void setNome(String nome);

	/**
	 * @return the areaConstruidaFaixa
	 */
	AreaConstruidaFaixa getAreaConstruidaFaixa();

	/**
	 * @param areaConstruidaFaixa
	 *            the areaConstruidaFaixa to set
	 */
	void setAreaConstruidaFaixa(AreaConstruidaFaixa areaConstruidaFaixa);

	/**
	 * @return the pavimentoCalcada
	 */
	PavimentoCalcada getPavimentoCalcada();

	/**
	 * @param pavimentoCalcada
	 *            the pavimentoCalcada to set
	 */
	void setPavimentoCalcada(PavimentoCalcada pavimentoCalcada);

	/**
	 * @return the pavimentoRua
	 */
	PavimentoRua getPavimentoRua();

	/**
	 * @param pavimentoRua
	 *            the pavimentoRua to set
	 */
	void setPavimentoRua(PavimentoRua pavimentoRua);

	/**
	 * @return the situacaoImovel
	 */
	SituacaoImovel getSituacaoImovel();

	/**
	 * @param situacaoImovel
	 *            the situacaoImovel to set
	 */
	void setSituacaoImovel(SituacaoImovel situacaoImovel);

	/**
	 * @return the perfilImovel
	 */
	PerfilImovel getPerfilImovel();

	/**
	 * @param perfilImovel
	 *            the perfilImovel to set
	 */
	void setPerfilImovel(PerfilImovel perfilImovel);

	/**
	 * @return the padraoConstrucao
	 */
	PadraoConstrucao getPadraoConstrucao();

	/**
	 * @param padraoConstrucao
	 *            the padraoConstrucao to set
	 */
	void setPadraoConstrucao(PadraoConstrucao padraoConstrucao);

	/**
	 * @return the quantidadeUnidadeConsumidora
	 */
	Integer getQuantidadeUnidadeConsumidora();

	/**
	 * @param quantidadeUnidadeConsumidora
	 *            the quantidadeUnidadeConsumidora
	 *            to set
	 */
	void setQuantidadeUnidadeConsumidora(Integer quantidadeUnidadeConsumidora);

	/**
	 * @return the quantidadePontoConsumo
	 */
	Integer getQuantidadePontoConsumo();

	/**
	 * @param quantidadePontoConsumo
	 *            the quantidadePontoConsumo to
	 *            set
	 */
	void setQuantidadePontoConsumo(Integer quantidadePontoConsumo);

	/**
	 * @return the numeroSequenciaLeitura
	 */
	Integer getNumeroSequenciaLeitura();

	/**
	 * @param numeroSequenciaLeitura
	 *            the numeroSequenciaLeitura to
	 *            set
	 */
	void setNumeroSequenciaLeitura(Integer numeroSequenciaLeitura);

	/**
	 * @return the condominio
	 */
	Boolean getCondominio();

	/**
	 * @param condominio
	 *            the condominio to set
	 */
	void setCondominio(Boolean condominio);

	/**
	 * @return the dataEntrega
	 */
	Date getDataEntrega();

	/**
	 * @param dataEntrega
	 *            the dataEntrega to set
	 */
	void setDataEntrega(Date dataEntrega);

	/**
	 * @return the portaria
	 */
	Boolean getPortaria();

	/**
	 * @param portaria
	 *            the portaria to set
	 */
	void setPortaria(Boolean portaria);

	/**
	 * @return the valvulaBloqueio
	 */
	Boolean getValvulaBloqueio();

	/**
	 * @param valvulaBloqueio
	 *            the valvulaBloqueio to set
	 */
	void setValvulaBloqueio(Boolean valvulaBloqueio);

	/**
	 * @return the tipoBotijao
	 */
	TipoBotijao getTipoBotijao();

	/**
	 * @param tipoBotijao
	 *            the tipoBotijao to set
	 */
	void setTipoBotijao(TipoBotijao tipoBotijao);

	/**
	 * @return the empresa
	 */
	Empresa getEmpresa();

	/**
	 * @param empresa
	 *            the empresa to set
	 */
	void setEmpresa(Empresa empresa);

	/**
	 * @return the quantidadeBloco
	 */
	Integer getQuantidadeBloco();

	/**
	 * @param quantidadeBloco
	 *            the quantidadeBloco to set
	 */
	void setQuantidadeBloco(Integer quantidadeBloco);

	/**
	 * @return the quantidadeAndar
	 */
	Integer getQuantidadeAndar();

	/**
	 * @param quantidadeAndar
	 *            the quantidadeAndar to set
	 */
	void setQuantidadeAndar(Integer quantidadeAndar);

	/**
	 * @return the quantidadeApartamentoAndar
	 */
	Integer getQuantidadeApartamentoAndar();

	/**
	 * @param quantidadeApartamentoAndar
	 *            the quantidadeApartamentoAndar
	 *            to set
	 */
	void setQuantidadeApartamentoAndar(Integer quantidadeApartamentoAndar);

	/**
	 * @return the redePreexistente
	 */
	Boolean getRedePreexistente();

	/**
	 * @param redePreexistente
	 *            the redePreexistente to set
	 */
	void setRedePreexistente(Boolean redePreexistente);

	/**
	 * @return the listaClienteImovel
	 */
	Collection<ClienteImovel> getListaClienteImovel();

	/**
	 * @param listaClienteImovel
	 *            the listaClienteImovel to set
	 */
	void setListaClienteImovel(Collection<ClienteImovel> listaClienteImovel);

	/**
	 * @return the contatos
	 */
	Collection<ContatoImovel> getContatos();

	/**
	 * @param contatos
	 *            the contatos to set
	 */
	void setContatos(Collection<ContatoImovel> contatos);

	/**
	 * @return the unidadesConsumidoras
	 */
	Collection<ImovelRamoAtividade> getUnidadesConsumidoras();

	/**
	 * @param unidadesConsumidoras
	 *            the unidadesConsumidoras to set
	 */
	void setUnidadesConsumidoras(Collection<ImovelRamoAtividade> unidadesConsumidoras);

	/**
	 * @return the redeInternaImovel
	 */
	RedeInternaImovel getRedeInternaImovel();

	/**
	 * @param redeInternaImovel
	 *            the redeInternaImovel to set
	 */
	void setRedeInternaImovel(RedeInternaImovel redeInternaImovel);

	/**
	 * @return the modalidadeMedicaoImovel
	 */
	ModalidadeMedicaoImovel getModalidadeMedicaoImovel();

	/**
	 * @param modalidadeMedicaoImovel
	 *            the modalidadeMedicaoImovel to
	 *            set
	 */
	void setModalidadeMedicaoImovel(ModalidadeMedicaoImovel modalidadeMedicaoImovel);

	/**
	 * @return the listaPontoConsumo
	 */
	Collection<PontoConsumo> getListaPontoConsumo();

	/**
	 * @param listaPontoConsumo
	 *            the listaPontoConsumo to set
	 */
	void setListaPontoConsumo(Collection<PontoConsumo> listaPontoConsumo);

	/**
	 * @return the cep
	 */
	String getCep();

	/**
	 * @param cep
	 *            the cep to set
	 */
	void setCep(String cep);

	/**
	 * @return the inscricao
	 */
	String getInscricao();

	/**
	 * @return the enderecoFormatado
	 */
	String getEnderecoFormatado();

	/**
	 * @param quantidadeBanheiro
	 *            quantidade de banheiro
	 */
	void setQuantidadeBanheiro(Integer quantidadeBanheiro);

	/**
	 * @return quantidadeBanheiro
	 */
	Integer getQuantidadeBanheiro();

	/**
	 * @return
	 */
	Rota getRota();

	/**
	 * @param rota
	 */
	void setRota(Rota rota);

	/**
	 * @return
	 */
	Date getDataPrevisaoEncerramentoObra();

	/**
	 * @param dataPrevisaoEncerramentoObra
	 */
	void setDataPrevisaoEncerramentoObra(Date dataPrevisaoEncerramentoObra);

	/**
	 * @return
	 */
	Boolean getIndicadorObraTubulacao();

	/**
	 * @param indicadorObraTubulacao
	 */
	void setIndicadorObraTubulacao(Boolean indicadorObraTubulacao);

	/**
	 * @return
	 */
	Agente getAgente();

	/**
	 * @param agente
	 */
	void setAgente(Agente agente);

	/**
	 * @return EntidadeConteudo - Retorna tipo de combustivel.
	 */
	EntidadeConteudo getTipoCombustivel();

	/**
	 * @param tipoCombustivel - Set Tiopo combustivel.
	 */
	void setTipoCombustivel(EntidadeConteudo tipoCombustivel);
	
	/**
	 * @return indicador
	 */
	Boolean getIndicadorAlterarImovelCondominioFilho();
	
	/**
	 * @param indicadorAlterarImovelCondominioFilho
	 */
	void setIndicadorAlterarImovelCondominioFilho(Boolean indicadorAlterarImovelCondominioFilho);

	/**
	 * @return logradouro numero e bairro do endereco
	 */
	public String getEnderecoLogNumBairro();

	/**
	 * @return municipio e estado do endereco
	 */
	public String getEnderecoMuniUF();
	

	/**
	 * @return the indicadorRestricaoServico
	 */
	public Boolean getIndicadorRestricaoServico();
	
	/**
	 * @param indicadorRestricaoServico the indicadorRestricaoServico to set
	 */
	public void setIndicadorRestricaoServico(Boolean indicadorRestricaoServico);

	/**
	 * @return the Collection<ImovelServicoTipoRestricao>
	 */
	public Collection<ImovelServicoTipoRestricao> getListaImovelServicoTipoRestricao();

	/**
	 * @param listaImovelServicoTipoRestricao
	 */
	public void setListaImovelServicoTipoRestricao(Collection<ImovelServicoTipoRestricao> listaImovelServicoTipoRestricao);

	
	/**
	 * 
	 * @return Método de Habilitado
	 */
	public Boolean getHabilitado();
	
	/**
	 * @return imovel eh individual
	 */
	public Boolean isIndividual();
	
	
	/**
		 * retorna i id do imovelcondominio
		 * 
		 * @return
		 */
	public Long getIdImovelCondominio();

	String getSituacaoPontoConsumo();

	void setSituacaoPontoConsumo(String situacaoPontoConsumo);

}
