package br.com.ggas.cadastro.imovel.impl;

/**
 * Classe responsável pela representação dos atributos 
 * relativos a Unidade Consumidora de um Imóvel com um Cliente.
 * 
 * @author esantana
 *
 */
public class ImovelUnidadeConsumidoraVO {

	private Long idSegmento;
	
	private Long idRamoAtividade;
	
	private String quantidadeEconomia;

	public Long getIdSegmento() {
		return idSegmento;
	}

	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}

	public Long getIdRamoAtividade() {
		return idRamoAtividade;
	}

	public void setIdRamoAtividade(Long idRamoAtividade) {
		this.idRamoAtividade = idRamoAtividade;
	}

	public String getQuantidadeEconomia() {
		return quantidadeEconomia;
	}

	public void setQuantidadeEconomia(String quantidadeEconomia) {
		this.quantidadeEconomia = quantidadeEconomia;
	}
}
