/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface ControladorClienteImovel
 * 
 * @author Procenge
 */
public interface ControladorClienteImovel extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_CLIENTE_IMOVEL = "controladorClienteImovel";
	String ERRO_NEGOCIO_REMOVER_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL = "ERRO_NEGOCIO_REMOVER_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL";

	/**
	 * Método responsável por inserir uma lista de cliente imóvel.
	 * 
	 * @param listaClienteImovel the lista cliente imovel
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	void inserirClienteImovel(Collection<ClienteImovel> listaClienteImovel) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por consultar um cliente imóvel.
	 * 
	 * @param idImovel the id imovel
	 * @param idCliente the id cliente
	 * @param tipoRelacionamentoClienteImovel the tipo relacionamento cliente imovel
	 * @return the cliente imovel
	 * @throws NegocioException the negocio exception
	 */
	ClienteImovel obterClienteImovel(Long idImovel, Long idCliente, Long tipoRelacionamentoClienteImovel) throws NegocioException;

	/**
	 * Consultar cliente imovel.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ClienteImovel> consultarClienteImovel(Map<String, Object> filtro) throws NegocioException;
}
