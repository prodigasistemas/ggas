/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.ArrayList;
import java.util.Collection;

import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.geral.dominio.impl.EntidadeDominioImpl;

/**
 * Classe responsável pelos métodos relacionados 
 * para identificação da modalidade de medição
 * 
 */
public class ModalidadeMedicaoImovelImpl extends EntidadeDominioImpl implements ModalidadeMedicaoImovel {

	/**
     * 
     */
	private static final long serialVersionUID = -3458706804331489525L;

	/**
	 * Instantiates a new modalidade medicao imovel impl.
	 */
	public ModalidadeMedicaoImovelImpl() {

		super();
	}

	/**
	 * Instantiates a new modalidade medicao imovel impl.
	 * 
	 * @param codigo
	 *            the codigo
	 * @param descricao
	 *            the descricao
	 */
	public ModalidadeMedicaoImovelImpl(int codigo, String descricao) {

		super();
		this.setCodigo(codigo);
		// by gmatos
		// on 15/10/09
		// 10:19
		this.setDescricao(descricao);
	}

	@Override
	public void setCodigo(int codigo) {

		super.setCodigo(codigo);

		switch(codigo) {
			case INDIVIDUAL :
				super.setDescricao(DESCRICAO_INDIVIDUAL);
				break;
			case COLETIVA :
				super.setDescricao(DESCRICAO_COLETIVA);
				break;
			default :
				break;
		}
	}

	/**
	 * Listar.
	 * 
	 * @return the collection
	 */
	public static Collection<ModalidadeMedicaoImovel> listar() {

		Collection<ModalidadeMedicaoImovel> retorno = new ArrayList<>();
		retorno.add(new ModalidadeMedicaoImovelImpl(COLETIVA, DESCRICAO_COLETIVA));
		retorno.add(new ModalidadeMedicaoImovelImpl(INDIVIDUAL, DESCRICAO_INDIVIDUAL));
		return retorno;
	}

	/**
	 * Gets the descricao por codigo.
	 * 
	 * @param codigo
	 *            the codigo
	 * @return the descricao por codigo
	 */
	public static String getDescricaoPorCodigo(int codigo) {

		String descricao = null;

		switch(codigo) {
			case INDIVIDUAL :
				descricao = DESCRICAO_INDIVIDUAL;
				break;
			case COLETIVA :
				descricao = DESCRICAO_COLETIVA;
				break;
			default :
				break;
		}

		return descricao;
	}
}
