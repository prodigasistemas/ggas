/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *30/12/2013
 * vpessoa
 * 15:52:51
 */

package br.com.ggas.cadastro.imovel;

import java.util.Collection;

import br.com.ggas.cadastro.imovel.impl.SegmentoPrevisaoCaptacao;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de  
 * 
 * @author vpessoa
 */
public interface ControladorPrevisaoCaptacao {

	/**
	 * Obter previsao captacao.
	 * 
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<EntidadeNegocio> obterPrevisaoCaptacao(boolean habilitado) throws NegocioException;

	/**
	 * Obter previsao captacao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the segmento previsao captacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public SegmentoPrevisaoCaptacao obterPrevisaoCaptacao(Long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Inserir.
	 * 
	 * @param segmentoPrevisaoCaptacao
	 *            the segmento previsao captacao
	 * @return the segmento previsao captacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public SegmentoPrevisaoCaptacao inserir(SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao) throws NegocioException;

	/**
	 * Consultar segmento previsao captacao.
	 * 
	 * @param segmentoPrevisaoCaptacao
	 *            the segmento previsao captacao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<SegmentoPrevisaoCaptacao> consultarSegmentoPrevisaoCaptacao(SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao)
					throws NegocioException;

	/**
	 * Atualizar.
	 * 
	 * @param segmentoPrevisaoCaptacao
	 *            the segmento previsao captacao
	 * @return the segmento previsao captacao
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public SegmentoPrevisaoCaptacao atualizar(SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao) throws NegocioException,
					ConcorrenciaException;
}
