package br.com.ggas.cadastro.imovel.impl;

/**
 * Classe responsável pela representação dos atributos 
 * relativos aos contatos de um Imóvel.
 * 
 * @author esantana
 *
 */
public class ImovelContatoVO {

	
	private Long idTipoContato;
	private Long idProfissao;
	private String nomeContato;
	private String dddContato;
	private String telefoneContato;
	private String ramalContato;
	private String areaContato;
	private String emailContato;
	private String dddContato2;
	private String telefoneContato2;
	private String ramalContato2;
	
	/**
	 * Obtém o identificador de tipo de contato.
	 * 
	 * @return idTipoContato - {@link Long}
	 */
	public Long getIdTipoContato() {
		return idTipoContato;
	}
	
	/**
	 * Altera o identificador de tipo de contato.
	 * 
	 * @param idTipoContato - {@link Long}
	 */
	public void setIdTipoContato(Long idTipoContato) {
		this.idTipoContato = idTipoContato;
	}
	
	/**
	 * Obtém o identificador da profissão.
	 * 
	 * @return idProfissao - {@link Long}
	 */
	public Long getIdProfissao() {
		return idProfissao;
	}
	
	/**
	 * Altera o identificador da profissão.
	 * 
	 * @param idProfissao - {@link Long}
	 */
	public void setIdProfissao(Long idProfissao) {
		this.idProfissao = idProfissao;
	}
	
	/**
	 * Obtém o nome do contato.
	 * 
	 * @return nomeContato - {@link String}
	 */
	public String getNomeContato() {
		return nomeContato;
	}
	
	/**
	 * Altera o nome do Contato
	 * 
	 * @param nomeContato - {@link String}
	 */
	public void setNomeContato(String nomeContato) {
		this.nomeContato = nomeContato;
	}
	
	public String getDddContato() {
		return dddContato;
	}
	public void setDddContato(String dddContato) {
		this.dddContato = dddContato;
	}
	public String getTelefoneContato() {
		return telefoneContato;
	}
	public void setTelefoneContato(String telefoneContato) {
		this.telefoneContato = telefoneContato;
	}
	public String getRamalContato() {
		return ramalContato;
	}
	public void setRamalContato(String ramalContato) {
		this.ramalContato = ramalContato;
	}
	public String getAreaContato() {
		return areaContato;
	}
	public void setAreaContato(String areaContato) {
		this.areaContato = areaContato;
	}
	public String getEmailContato() {
		return emailContato;
	}
	public void setEmailContato(String emailContato) {
		this.emailContato = emailContato;
	}
	public String getDddContato2() {
		return dddContato2;
	}
	public void setDddContato2(String dddContato2) {
		this.dddContato2 = dddContato2;
	}
	public String getTelefoneContato2() {
		return telefoneContato2;
	}
	public void setTelefoneContato2(String telefoneContato2) {
		this.telefoneContato2 = telefoneContato2;
	}
	public String getRamalContato2() {
		return ramalContato2;
	}
	public void setRamalContato2(String ramalContato2) {
		this.ramalContato2 = ramalContato2;
	}
	
	
}
