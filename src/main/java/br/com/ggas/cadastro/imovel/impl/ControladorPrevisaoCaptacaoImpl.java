/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *30/12/2013
 * vpessoa
 * 15:52:51
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.imovel.ControladorPrevisaoCaptacao;
import br.com.ggas.cadastro.imovel.repositorio.RepositorioSegmentoPrevisaoCaptacao;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Constantes;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de  ControladorPrevisaoCaptacaoImpl
 * 
 * @author vpessoa
 */
@Service("controladorPrevisaoCaptacao")
@Transactional
public class ControladorPrevisaoCaptacaoImpl implements ControladorPrevisaoCaptacao {

	@Autowired
	private RepositorioSegmentoPrevisaoCaptacao repositorioSegmentoPrevisaoCaptacao;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPrevisaoCaptacao#obterPrevisaoCaptacao(boolean)
	 */
	@Override
	public Collection<EntidadeNegocio> obterPrevisaoCaptacao(boolean habilitado) throws NegocioException {

		return repositorioSegmentoPrevisaoCaptacao.obterTodas(habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPrevisaoCaptacao#obterPrevisaoCaptacao(java.lang.Long, java.lang.String[])
	 */
	@Override
	public SegmentoPrevisaoCaptacao obterPrevisaoCaptacao(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (SegmentoPrevisaoCaptacao) repositorioSegmentoPrevisaoCaptacao.obter(chavePrimaria, propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPrevisaoCaptacao#inserir(br.com.ggas.cadastro.imovel.impl.SegmentoPrevisaoCaptacao)
	 */
	@Override
	public SegmentoPrevisaoCaptacao inserir(SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao) throws NegocioException {

		if(segmentoPrevisaoCaptacao.getSegmento() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, Constantes.SEGMENTO);
		}
		Collection<SegmentoPrevisaoCaptacao> lista = this.consultarSegmentoPrevisaoCaptacao(segmentoPrevisaoCaptacao);
		if(lista != null && !lista.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_PREVISAO_CADASTRADA, true);
		} else {
			repositorioSegmentoPrevisaoCaptacao.inserir(segmentoPrevisaoCaptacao);
		}
		return segmentoPrevisaoCaptacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPrevisaoCaptacao#consultarSegmentoPrevisaoCaptacao(br.com.ggas.cadastro.imovel.impl.
	 * SegmentoPrevisaoCaptacao)
	 */
	@Override
	public Collection<SegmentoPrevisaoCaptacao> consultarSegmentoPrevisaoCaptacao(SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao)
					throws NegocioException {

		return repositorioSegmentoPrevisaoCaptacao.consultarSegmentoPrevisaoCaptacao(segmentoPrevisaoCaptacao);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPrevisaoCaptacao#atualizar(br.com.ggas.cadastro.imovel.impl.SegmentoPrevisaoCaptacao)
	 */
	@Override
	public SegmentoPrevisaoCaptacao atualizar(SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao) throws NegocioException,
					ConcorrenciaException {

		repositorioSegmentoPrevisaoCaptacao.atualizar(segmentoPrevisaoCaptacao);

		return segmentoPrevisaoCaptacao;
	}

}
