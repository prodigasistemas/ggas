/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoEstendida;
import br.com.ggas.cadastro.imovel.impl.ComentarioPontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoVencimento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.faturamento.anomalia.HistoricoAnomaliaFaturamento;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.fatura.DadosResumoFatura;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.consumo.ImovelPCSZ;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Pair;
import br.com.ggas.web.medicao.rota.PontoConsumoRotaVO;
import br.com.ggas.web.relatorio.faturamento.PesquisaRelatorioComercialMensalVO;
import br.com.ggas.web.relatorio.faturamento.PesquisaRelatorioPrevisaoCaptacaoVO;
import br.com.ggas.webservice.PontoConsumoTO;

/**
 * Interface ControladorPontoConsumo
 * @author arthur
 *
 */
public interface ControladorPontoConsumo extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_PONTO_CONSUMO = "controladorPontoConsumo";

	String ERRO_FILTROS_NAO_SELECIONADOS = "ERRO_FILTROS_NAO_SELECIONADOS";

	String ERRO_PONTO_CONSUMO_NAO_ENCONTRADO_SIMULACAO = "ERRO_PONTO_CONSUMO_NAO_ENCONTRADO_SIMULACAO";

	/**
	 * Método que cria a entidade PontoConsumoCityGate.
	 * 
	 * @return Instância de PontoConsumoCityGate
	 */
	EntidadeNegocio criarPontoConsumoCityGate();

	/**
	 * Método que retorna uma lista os pontos de consumo das chaves passadas pelo parâmetro.
	 * 
	 * @param chavesPrimarias the chaves primarias
	 * @return Lista de pontos de consumo
	 */
	Collection<PontoConsumo> listarPontosConsumo(String[] chavesPrimarias);

	/**
	 * Método responsável por listar todos os pontos de consumo em alerta.
	 * 
	 * @return Uma coleção de pontos de consumo.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> listarPontosConsumoEmAlerta() throws NegocioException;

	/**
	 * Método responsável por consultar pontos de consumo em aberto para geração de roteiro.
	 * 
	 * @param filtro O filtro com os parâmetros para a pesquisa.
	 * @param atividadeSistema the atividade sistema
	 * @return Uma coleção de pontos de consumo.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> consultarPontosConsumoEmAbertoLeitura(Map<String, Object> filtro, AtividadeSistema atividadeSistema)
			throws NegocioException;

	/**
	 * Método responsável por listar os pontos de consumo que não possuem número de sequência de leitura.
	 * 
	 * @param grupoFaturamento the grupo faturamento
	 * @return Coleção de pontos de consumo.
	 */
	Collection<PontoConsumo> listarPontosConsumoNaoSequenciadas(GrupoFaturamento grupoFaturamento);

	/**
	 * Método responsável por consultar pontos de consumo pelo filtro informado.
	 * 
	 * @param filtro O filtro com os parâmetros para a pesquisa.
	 * @param propriedadesLazy {@link String}
	 * @return Uma coleção de pontos de consumo.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> consultarPontosConsumo(Map<String, Object> filtro, String... propriedadesLazy) throws NegocioException;

	/**
	 * Método responsável por listar os pontos de consumo de uma rota.
	 * 
	 * @param chavePrimariaRota Chave primária de uma rota
	 * @return Coleção de pontos de consumo.
	 */
	Collection<PontoConsumo> listarPontosConsumoPorChaveRota(long chavePrimariaRota);

	/**
	 * Método que obtem a localidade do ponto de consumo passado pelo parâmetro.
	 * 
	 * @param pontoConsumo the ponto consumo
	 * @return Localidade do ponto de consumo
	 */
	String obterLocalidadePontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * Método reponsável por obter a referencia e o ciclo atual da rota do ponto de consumo.
	 * 
	 * @param pontoConsumo O ponto de consumo
	 * @return Um mapa contendo a referencia e o ciclo
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Map<String, Integer> obterReferenciaCicloAtual(PontoConsumo pontoConsumo) throws NegocioException;

	/**
	 * Método responsável por consultar os pontos de consumo de um grupo de faturamento.
	 * 
	 * @param grupoFaturamento O grupo de faturamento
	 * @param rota Rota ligada ao Grupo (não-obrigatório)
	 * @return Uma coleção de pontos de consumo
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> consultarPontoConsumo(GrupoFaturamento grupoFaturamento, Rota rota) throws NegocioException;

	/**
	 * Método que cria a entidade ComentarioPontoConsumo.
	 * 
	 * @return Instância de ComentarioPontoConsumo
	 */
	EntidadeNegocio criarComentarioPontoConsumo();

	/**
	 * Método responsável por inserir um comentário de ponto de consumo.
	 * 
	 * @param comentarioPontoConsumo the comentario ponto consumo
	 * @return long a chave primária do comentário de ponto de consumo
	 * @throws NegocioException Caso ocorra alguma violação nas regras de negócio
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	void inserirComentarioPontoConsumo(ComentarioPontoConsumo comentarioPontoConsumo) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por consultar o PSCZ vigente do ponto de consumo informado para um data referência.
	 * 
	 * @param chavePrimariaPontoConsumo Chave primária do ponto de consumo
	 * @param dataReferencia Data de referência
	 * @return Um PontoConsumoPCSZ.
	 * @throws NegocioException Caso ocorra alguma violação nas regras de negócio.
	 */
	ImovelPCSZ obterPontoConsumoPCSZVigente(Long chavePrimariaPontoConsumo, Date dataReferencia) throws NegocioException;

	/**
	 * Listar situacoes ponto consumo.
	 * 
	 * @return Collection de Situações de Ponto de Consumo habilitadas
	 */
	Collection<SituacaoConsumo> listarSituacoesPontoConsumo();

	/**
	 * Método responsável por listar os pontos de consumo a partir da chave primária do contrato.
	 * 
	 * @param chavePrimariaContrato Chave primária do contrato.
	 * @param idCliente the id cliente
	 * @return Uma coleção de pontos de consumo.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> listarPontoConsumoPorChaveContrato(Long chavePrimariaContrato, Long idCliente) throws NegocioException;

	/**
	 * Método responsável por obter uma(s) entidade PontoConsumoCityGate a partir da chave primaria de um ponto de consumo .
	 * 
	 * @param idPontoConsumo the id ponto consumo
	 * @return Uma coleção de PontoConsumoCityGate.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumoCityGate> obterPontoConsumoCityGate(Long idPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter uma(s) entidade PontoConsumoCityGate a partir da chave primaria de um ponto de consumo ordenado pelo
	 * city gate.
	 * 
	 * @param idPontoConsumo the id ponto consumo
	 * @param idCityGate the id city gate
	 * @return Uma coleção de PontoConsumoCityGate.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumoCityGate> obterPontoConsumoCityGateOrdenadoPorCityGate(Long idPontoConsumo, Long idCityGate)
			throws NegocioException;

	/**
	 * Método responsável por validar o percentual de participacao dos city gate pertencentes ao pontoConsumo.
	 * 
	 * @param percentualCityGate Um array de string com os percentuais de cada city gate.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	void validarPercentualParticipacao(String[] percentualCityGate) throws NegocioException;
    
	/**
	 * Método responsável por atualizar o campo de leitura movimento.
	 * @return 
	 *
	 * @return Ordenação dos pontos de consumo por leitura movimento
	 * @throws NegocioException 
	 * @throws ConcorrenciaException 
	 */
	 void ordenarPontoConsumo() throws ConcorrenciaException, NegocioException;
	
	
	
	
	/**
	 * Método responsável por buscar um ponto de consumo.
	 * 
	 * @param chavePrimaria A chavePrimaria do ponto de consumo
	 * @return Um ponto de consumo
	 */
	PontoConsumo buscarPontoConsumo(Long chavePrimaria);
	
	
	/**
	 * Obtem o ponto de consumo
	 * 
	 * @param chavePrimaria - codigo do ponto de consumo
	 * @return ponto de consumo
	 **/	
	PontoConsumo obterPontoConsumo(long chavePrimaria);

	/**
	 * Método responsável por retornar todos os pontos de consumo pertencentes a um determinado cliente.
	 * 
	 * @param idCliente A chavePrimaria do cliente
	 * @return Coleção dos pontos de consumo do cliente
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> listarPontoConsumoPorCliente(Long idCliente) throws NegocioException;

	/**
	 * Listar contrato ponto consumo por cliente.
	 *
	 * @param idCliente the id cliente
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<ContratoPontoConsumo> listarContratoPontoConsumoPorCliente(Long... idCliente) throws NegocioException;

	/**
	 * Método responsável por retornar todos os pontos de consumo pertencentes a um determinado cliente.
	 * 
	 * @param idCliente A chavePrimaria do cliente
	 * @return Coleção dos pontos de consumo do cliente
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */

	Collection<PontoConsumo> listarPontoConsumoPorChaveCliente(Long idCliente) throws NegocioException;

	/**
	 * Método responsável por retornar todos os pontos de consumo pertencentes a um determinado cliente e que estejam aptos para simulação
	 * do cálculo de fornecimento de gás;.
	 * 
	 * @param idCliente A chavePrimaria do cliente
	 * @return Coleção dos pontos de consumo do cliente
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> listarPontoConsumoPorClienteSimulacaoCalculo(Long idCliente) throws NegocioException;

	/**
	 * Método responsável por retornar todos os pontos de consumo pertencentes a um determinado cliente e que estejam aptos para simulação
	 * do cálculo de fornecimento de gás;.
	 * 
	 * @param idImovel A chavePrimaria do imóvel.
	 * @return Coleção dos pontos de consumo do cliente
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> listarPontoConsumoPorChaveImovelSimulacaoCalculo(Long idImovel) throws NegocioException;

	/**
	 * Método responsável por retornar todos os pontos de consumo que tem o faturamento agrupado por contrato.
	 * 
	 * @param idContrato A chavePrimaria do contrato.
	 * @param tipoAgrupamento Tipo de Agrupamento.
	 * @return Coleção dos pontos de consumo com faturamento agrupado.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> listarPontoConsumoFaturamentoAgrupado(long idContrato, EntidadeConteudo tipoAgrupamento)
			throws NegocioException;

	/**
	 * Método responsável por listar os pontos de consumo de um contrato.
	 * 
	 * @param idContrato Chave primária do contrato.
	 * @return Uma coleção de pontos de consumo.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> listaPontoConsumoPorContrato(Long idContrato) throws NegocioException;

	/**
	 * Obtem uma lista com todos os pontos de consumo que possuem contrato ativo no imovel.
	 * 
	 * @param chaveImovel the chave imovel
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<PontoConsumo> listarPontoConsumoComContratoAtivoPorImovel(Long chaveImovel) throws NegocioException;

	/**
	 * Obtem um ponto de consumo que possuem contrato mais atual no imovel.
	 * 
	 * @param chaveImovel the chave imovel
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<PontoConsumo> listarPontoConsumoComContratoPorImovel(Long chaveImovel) throws NegocioException;

	/**
	 * Obtem uma lista com todos os pontos de consumo que possuem contrato ativo no imovel.
	 * 
	 * @param chaveCliente the chave cliente
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<PontoConsumo> listarPontoConsumoComContratoAtivoPorCliente(Long chaveCliente) throws NegocioException;

	/**
	 * Obtem um ponto de consumo que possuem contrato mais atual por cliente.
	 * 
	 * @param chaveCliente the chave cliente
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<PontoConsumo> listarPontoConsumoComContratoPorCliente(Long chaveCliente) throws NegocioException;

	/**
	 * Método responsável por consultar os pontos de consumo a partir de uma rota.
	 * 
	 * @param rota A rota
	 * @return Coleção dos pontos de consumo ordenados pela sequencia de leitura
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> consultarPontoConsumoPorRota(Rota rota) throws NegocioException;

	/**
	 * Método responsável por consultar os pontos de consumo a partir de uma rota.
	 * 
	 * @param indicadorLeitura {@link Boolean}
	 * @param rota A rota
	 * @return Coleção dos pontos de consumo ordenados pela sequencia de leitura
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> consultarPontoConsumoPorRotaFaturavel(Rota rota, Boolean indicadorLeitura) throws NegocioException;

	/**
	 * Método responsável por consultar os pontos de consumo bloqueados com consumo.
	 * 
	 * @param rota A rota
	 * @param anoMes ano mes de faturamento
	 * @param numeroCiclo numero do ciclo de faturamento
	 * @return Coleção dos pontos de consumo ordenados pela sequencia de leitura
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> consultarPontoConsumoBloqueadoComConsumo(Rota rota, Integer anoMes, Integer numeroCiclo)
			throws NegocioException;

	/**
	 * Método responsável por consultar os pontos de consumo bloqueados leitura de bloqueio.
	 * 
	 * @param rota A rota
	 * @param anoMes ano mes de faturamento
	 * @param numeroCiclo numero do ciclo de faturamento
	 * @return Coleção dos pontos de consumo ordenados pela sequencia de leitura
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	public Collection<PontoConsumo> consultarPontoConsumoBloqueadoComLeituraBloqueio(Rota rota, Integer anoMes, Integer numeroCiclo)
			throws NegocioException;

	/**
	 * Método responsável por consultar os pontos de consumo a partir de um grupo de faturamento.
	 * 
	 * @param grupoFaturamento O grupo de faturamento
	 * @return Coleção dos pontos de consumo ordenados pela sequencia de leitura
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> consultarPontoConsumoPorGrupoFaturamento(GrupoFaturamento grupoFaturamento) throws NegocioException;

	/**
	 * Quantidade leitura por rota.
	 * 
	 * @param idRota the id rota
	 * @return the int
	 * @throws NegocioException the negocio exception
	 */
	int quantidadeLeituraPorRota(Long idRota) throws NegocioException;

	/**
	 * Quantidade medidores por grupo faturamento.
	 * 
	 * @param idGrupoFaturamento the id grupo faturamento
	 * @return the int
	 * @throws NegocioException the negocio exception
	 */
	int quantidadeMedidoresPorGrupoFaturamento(Long idGrupoFaturamento) throws NegocioException;

	/**
	 * Método responsável por consultar os pontos de consumo a partir de um grupo de faturamento.
	 * 
	 * @param chaveRota the chave rota
	 * @param cronogramaAtividadeFaturamento the cronograma atividade faturamento
	 * @return Coleção dos pontos de consumo ordenados pela sequencia de leitura
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<PontoConsumo> listarPontosConsumoPorRotaLeituraMovimento(Long chaveRota,
			CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento) throws NegocioException;

	/**
	 * Método responsável por montar uma lista de pontos de consumo validados, apenas 1 ponto de consumo por contrato.
	 * 
	 * @param listaPontoConsumo Lista de Pontos de Consumo pesquisados
	 * @param grupoFaturamento o gurpo de faturamento
	 * @param cronogramaAtividadeFaturamento o cronogramatividadeFaturamento
	 * @param dadosAuditoria dadosAuditoria
	 * @param mapaAnomalias o mapa de anomalias
	 * @param isSimulacao indicador se é ou não simulação
	 * @return coleção de pontos de consumo sem agrupamentos
	 * @throws NegocioException caso ocorra algum erro
	 */
	Collection<PontoConsumo> montarListaPontoConsumoFaturarGrupo(Collection<PontoConsumo> listaPontoConsumo,
			GrupoFaturamento grupoFaturamento, CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento, DadosAuditoria dadosAuditoria,
			Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias, boolean isSimulacao) throws NegocioException;

	/**
	 * Metodo respons[avel por montar uma lista de pontos de consumo.
	 * 
	 * @param filtro the filtro
	 * @return Lista de pontos de consumo
	 * @throws NegocioException the negocio exception
	 */
	List<PontoConsumo> listarPontosConsumo(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Metodo responsável por montar uma lista de pontos de consumo filhos de um imóvel condomínio.
	 *
	 * @param idImovel A chave primária do imóvel condomínio
	 * @return Lista de pontos de consumo
	 * @throws NegocioException the negocio exception
	 */
	List<PontoConsumo> listarPontoDeConsumoFilhoDeImovelCondominio(Long idImovel);

	/**
	 * Método resposavel por retornar uma instancia de SituacaoConsumo.
	 * 
	 * @return SituacaoConsumo
	 */
	EntidadeNegocio criarSituacaoConsumo();

	/**
	 * método responsável por validar um ponto de consumo (e seus respectivos agrupados), devolvendo aqueles que não são faturáveis.
	 * 
	 * @param cronogramaAtividadeFaturamento o cronogramaAtividadeFaturamento
	 * @param referencia o ano/mês de referencia
	 * @param ciclo o ciclo
	 * @param dadosAuditoria o objeto dados de auditoria
	 * @param mapaAnomalias mapa de anomalias
	 * @param isSimulacao indicador de simulação
	 * @param pontoConsumo o ponto de consumo que será analisado
	 * @return pontos de consumo não faturáveis
	 * @throws NegocioException caso ocorra algum erro
	 */
	Collection<PontoConsumo> obterPontosConsumoNaoFaturaveis(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento,
			Integer referencia, Integer ciclo, DadosAuditoria dadosAuditoria,
			Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias, boolean isSimulacao, PontoConsumo pontoConsumo)
			throws NegocioException;

	/**
	 * Método resposavel por retornar lista de pontos de consumo por periodicidade.
	 * 
	 * @param idPeriodicidade the id periodicidade
	 * @param idTipoLeitura the id tipo leitura
	 * @return coleção Periodicidade
	 */
	Collection<PontoConsumo> listarPontosConsumoNaoSequenciadas(Long idPeriodicidade, Long idTipoLeitura);

	/**
	 * método responsável por validar um ponto de consumo (e seus respectivos agrupados), devolvendo aqueles que são faturáveis.
	 * 
	 * @param listaPontoConsumo the lista ponto consumo
	 * @param grupoFaturamento the grupo faturamento
	 * @param cronogramaAtividadeFaturamento o cronogramaAtividadeFaturamento
	 * @param dadosAuditoria o objeto dados de auditoria
	 * @param mapaAnomalias mapa de anomalias
	 * @param isSimulacao indicador de simulação
	 * @param isDrawback the is drawback
	 * @return pontos de consumo faturáveis
	 * @throws NegocioException caso ocorra algum erro
	 */
	Collection<PontoConsumo> validarPontosConsumoFaturaveis(Collection<PontoConsumo> listaPontoConsumo, GrupoFaturamento grupoFaturamento,
			CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento, DadosAuditoria dadosAuditoria,
			Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias, boolean isSimulacao, Boolean isDrawback)
			throws NegocioException;

	/**
	 * método responsável por validar pontos de consumos com a mesma data e ciclo da medição.
	 * 
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @param grupoFaturamento grupo de faturamento
	 * @return pontos de consumo
	 * @throws NegocioException caso ocorra algum erro Collection<PontoConsumo> retirarPontoConsumoComMedicaoCronograma (List<Long> chaves,
	 *             int anoMesLeitura, int numeroCiclo) throws NegocioException;
	 */

	Collection<PontoConsumo> retirarPontoConsumoLeituraMovimento(Long[] chavesPontoConsumo, GrupoFaturamento grupoFaturamento)
			throws NegocioException;

	/**
	 * Processa os dados de resumo de fatura obtidos a partir de um ponto de consumo faturável. A cada chamada deste método uma nova
	 * transação será criada.
	 * 
	 * @param dadosResumoFatura Dados de resumo de fatura a processar.
	 * @param mapaAnomalias Mapa de anormalidades de faturamento e os pontos de consumo para os quais elas ocorreram.
	 * @param listaFatura Lista de faturas inseridas a popular em caso de sucesso.
	 * @return isInseridoComSucesso.
	 * @throws GGASException the GGAS exception
	 */
	boolean processarDadosResumoFaturaPontoConsumo(DadosResumoFatura dadosResumoFatura,
			Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias, Collection<Fatura> listaFatura) throws GGASException;

	/**
	 * Consultar pontos consumo city gate.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<PontoConsumoCityGate> consultarPontosConsumoCityGate(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Listar ponto consumo por codigo supervisorio.
	 * 
	 * @param codigoPontoConsumoSupervisorio the codigo ponto consumo supervisorio
	 * @return the ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	PontoConsumo listarPontoConsumoPorCodigoSupervisorio(String codigoPontoConsumoSupervisorio) throws NegocioException;

	/**
	 * Validar ponto consumo e codigo legado.
	 * 
	 * @param codigoLegado the codigo legado
	 * @param idPontoConsumo the id ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	void validarPontoConsumoECodigoLegado(String codigoLegado, Long idPontoConsumo) throws NegocioException;

	/**
	 * Consultar ponto consumo por codigo legado.
	 * 
	 * @param codigoLegado the codigo legado
	 * @return the collection
	 */
	Collection<PontoConsumo> consultarPontoConsumoPorCodigoLegado(String codigoLegado);

	/**
	 * Listar ids pontos consumo por chave rota.
	 * 
	 * @param chavePrimaria the chave primaria
	 * @return the long[]
	 */
	Long[] listarIdsPontosConsumoPorChaveRota(Long chavePrimaria);

	/**
	 * Remanejar ponto consumo.
	 * 
	 * @param idPontosConsumoASeremRemanejados the id pontos consumo a serem remanejados
	 * @param idRotaDestino the id rota destino
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	Collection<PontoConsumo> remanejarPontoConsumo(Long[] idPontosConsumoASeremRemanejados, Long idRotaDestino) throws GGASException;

	/**
	 * Inserir ponco consumo vencimento.
	 * 
	 * @param dadosPontoConsumoVencimento the dados ponto consumo vencimento
	 * @param idContrato the id contrato
	 * @throws NegocioException the negocio exception
	 */
	void inserirPoncoConsumoVencimento(Map<Long, Object> dadosPontoConsumoVencimento, long idContrato) throws NegocioException;

	/**
	 * Listar ponto consumo vencimento.
	 * 
	 * @param idPontoConsumo the id ponto consumo
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<PontoConsumoVencimento> listarPontoConsumoVencimento(long idPontoConsumo) throws NegocioException;

	/**
	 * Remover ponto consumo vencimento.
	 * 
	 * @param dadosPontoConsumoVencimento the dados ponto consumo vencimento
	 * @param idContrato the id contrato
	 * @throws NegocioException the negocio exception
	 */
	void removerPontoConsumoVencimento(Map<Long, Object> dadosPontoConsumoVencimento, long idContrato) throws NegocioException;

	/**
	 * Remover ponto consumo vencimento.
	 * 
	 * @param idContrato the id contrato
	 * @throws NegocioException the negocio exception
	 */
	void removerPontoConsumoVencimento(long idContrato) throws NegocioException;

	/**
	 * Consultar lista dias ponto consumo vencimento.
	 * 
	 * @param idPontoConsumo the id ponto consumo
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	List<Long> consultarListaDiasPontoConsumoVencimento(long idPontoConsumo) throws NegocioException;

	/**
	 * Listar ponto consumo por cliente.
	 * 
	 * @param cpfCnpj the cpf cnpj
	 * @return the list
	 */
	List<PontoConsumo> listarPontoConsumoPorCliente(String cpfCnpj);

	/**
	 * Consultar ponto consumo por comando acao.
	 * 
	 * @param acaoComandoEstendida Acão de comando.
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<PontoConsumo> consultarPontoConsumoPorComandoAcao(AcaoComandoEstendida acaoComandoEstendida) throws NegocioException;

	/**
	 * Consultar pontos consumo consistir leitura processado sem anormalidade.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	// consistir leitura
	Collection<Long> consultarPontosConsumoConsistirLeituraProcessadoSemAnormalidade(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo consistir leitura processado anormalidade.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	Collection<PontoConsumo> consultarPontosConsumoConsistirLeituraProcessadoAnormalidade(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo consistir leitura pronto processar.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	Collection<Long> consultarPontosConsumoConsistirLeituraProntoProcessar(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo registrar leitura processado sem anormalidade.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	// registrar leitura
	Collection<Long> consultarPontosConsumoRegistrarLeituraProcessadoSemAnormalidade(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo registrar leitura processado anormalidade.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	Collection<Long> consultarPontosConsumoRegistrarLeituraProcessadoAnormalidade(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo registrar leitura pronto processar.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	Collection<Long> consultarPontosConsumoRegistrarLeituraProntoProcessar(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo registrar leitura pronto processar coletor.
	 * 
	 * @param filtro {@link Map}
	 * @return Coleção de ponto de consumo
	 */
	Collection<Long> consultarPontosConsumoRegistrarLeituraProntoProcessarColetor(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo faturar grupo processado sem anormalidade.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	// faturar grupo
	Collection<Long> consultarPontosConsumoFaturarGrupoProcessadoSemAnormalidade(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo faturar grupo processado anormalidade.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	Collection<Long> consultarPontosConsumoFaturarGrupoProcessadoAnormalidade(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo faturar grupo pronto processar.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	Collection<Long> consultarPontosConsumoFaturarGrupoProntoProcessar(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo emitir fatura processado sem anormalidade.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	// emitir fatura
	Collection<Long> consultarPontosConsumoEmitirFaturaProcessadoSemAnormalidade(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo emitir fatura processado anormalidade.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	Collection<Long> consultarPontosConsumoEmitirFaturaProcessadoAnormalidade(Map<String, Object> filtro);

	/**
	 * Consultar pontos consumo emitir fatura pronto processar.
	 * 
	 * @param filtro the filtro
	 * @return the collection
	 */
	Collection<Long> consultarPontosConsumoEmitirFaturaProntoProcessar(Map<String, Object> filtro);

	/**
	 * Validar fatura avulso.
	 * 
	 * @param pontoConsumo the ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	void validarFaturaAvulso(PontoConsumo pontoConsumo) throws NegocioException;

	/**
	 * Remover imovel filho com pai medicao coletiva.
	 * 
	 * @param listaPontoConsumo the lista ponto consumo
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	List<PontoConsumo> removerImovelFilhoComPaiMedicaoColetiva(List<PontoConsumo> listaPontoConsumo) throws GGASException;

	/**
	 * Gerar relatorio previsao captacao.
	 * 
	 * @param pesquisaRelatorioPrevisaoCaptacaoVO the pesquisa relatorio previsao captacao vo
	 * @param formatoImpressao the formato impressao
	 * @return the byte[]
	 * @throws NegocioException the negocio exception
	 */
	byte[] gerarRelatorioPrevisaoCaptacao(PesquisaRelatorioPrevisaoCaptacaoVO pesquisaRelatorioPrevisaoCaptacaoVO,
			FormatoImpressao formatoImpressao) throws NegocioException;

	/**
	 * Gerar relatorio comercial mensal.
	 * 
	 * @param pesquisaRelatorioComercialMensalVO the pesquisa relatorio comercial mensal vo
	 * @param formatoImpressao the formato impressao
	 * @return the byte[]
	 * @throws NegocioException the negocio exception
	 */
	byte[] gerarRelatorioComercialMensal(PesquisaRelatorioComercialMensalVO pesquisaRelatorioComercialMensalVO,
			FormatoImpressao formatoImpressao) throws NegocioException;

	/**
	 * Lista ponto consumo por numero contrato.
	 * 
	 * @param numeroContrato the numero contrato
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<PontoConsumo> listaPontoConsumoPorNumeroContrato(String numeroContrato) throws NegocioException;

	/**
	 * Gerar relatorio residencial mensal.
	 * 
	 * @param pesquisaRelatorioComercialMensalVO the pesquisa relatorio comercial mensal vo
	 * @param formatoImpressao the formato impressao
	 * @return the byte[]
	 * @throws NegocioException the negocio exception
	 */
	byte[] gerarRelatorioResidencialMensal(PesquisaRelatorioComercialMensalVO pesquisaRelatorioComercialMensalVO,
			FormatoImpressao formatoImpressao) throws NegocioException;

	/**
	 * Consultar ponto consumo por rota fiscalizacao.
	 * 
	 * @param rota the rota
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<PontoConsumo> consultarPontoConsumoPorRotaFiscalizacao(Rota rota) throws NegocioException;

	/**
	 * Listar ponto consumo contrato ativo.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<PontoConsumo> listarPontoConsumoContratoAtivo() throws NegocioException;

	/**
	 * Listar ponto consumo cliente.
	 * 
	 * @param idCliente the id cliente
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<PontoConsumo> listarPontoConsumoCliente(Long idCliente) throws NegocioException;

	/**
	 * Listar ponto consumo por contrato ativo e cliente.
	 * 
	 * @param idCliente the id cliente
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<PontoConsumo> listarPontoConsumoPorContratoAtivoECliente(Long idCliente) throws NegocioException;

	/**
	 * Carregar pontos consumo associados.
	 * 
	 * @param rota the rota
	 * @param list the list
	 * @param pontosConsumoAssociados the pontos consumo associados
	 * @return the pair
	 * @throws GGASException the GGAS exception
	 */
	Pair<Collection<PontoConsumo>, Map<Long, Object>> carregarPontosConsumoAssociados(Rota rota, Collection<PontoConsumo> list,
			Map<Long, Object> pontosConsumoAssociados) throws GGASException;

	/**
	 * Listar pontos consumo por chave cliente.
	 * 
	 * @param idCliente the id cliente
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	public Collection<PontoConsumo> listarPontosConsumoPorChaveCliente(Long idCliente) throws NegocioException;

	/**
	 * Converter ponto consumo em ponto consumo to.
	 * 
	 * @param pontoConsumo the ponto consumo
	 * @return {@link PontoConsumoTO}
	 * @throws GGASException the GGAS exception
	 */
	public PontoConsumoTO converterPontoConsumoEmPontoConsumoTO(PontoConsumo pontoConsumo) throws GGASException;

	/**
	 * Converter ponto consumo to em ponto consumo.
	 * 
	 * @param pontoConsumoTO the ponto consumo to
	 * @return {@link PontoConsumo}
	 * @throws GGASException the GGAS exception
	 */
	public PontoConsumo converterPontoConsumoTOEmPontoConsumo(PontoConsumoTO pontoConsumoTO) throws GGASException;

	/**
	 * Consultar ponto consumo por medidor.
	 * 
	 * @param medidorVirtual the medidor virtual
	 * @return the ponto consumo
	 */
	PontoConsumo consultarPontoConsumoPorMedidor(Medidor medidorVirtual);

	/**
	 * Consulta os pontos de consumo por códigos do ponto de consumo no Supervisório, obtendo os seguintes atributos:
	 * {@code codigoPontoConsumoSupervisorio}, {@code chavePrimaria}, {@code rota}, {@code segmento}, {@code ramoAtividade}
	 * 
	 * @param codigosPontoConsumoSupervisorio - Códigos do Ponto Consumo no Supervisório
	 * @return Map - Pontos de Consumo por Código do Supervisório
	 **/
	Map<String, PontoConsumo> consultarPontoConsumoPorCodigoSupervisorio(String[] codigosPontoConsumoSupervisorio);

	/**
	 * Método responsável por listar os pontos de consumo ativos dentro de um determinado periodo de tempo
	 * 
	 * @param parametros filtros
	 * @return Coleção de pontos de consumo.
	 */
	Collection<PontoConsumo> listarPontosConsumoAtivoPorData(Map<String, Object> parametros);

	/**
	 * Obtém as chaves primárias dos pontos de consumo de acordo com o {@code grupoFaturamento} e {@code rota} especificados no parâmetro.
	 * 
	 * @param grupoFaturamento {@link GrupoFaturamento}
	 * @param rota {@link Rota}
	 * @return chaves primárias dos pontos de consumo
	 * @throws NegocioException the Negocio exception
	 */
	List<PontoConsumo> consultarChavesPrimariasPontosDeConsumo(GrupoFaturamento grupoFaturamento, Rota rota) throws NegocioException;

	/**
	 * Realiza a consulta de atributos do Ponto de Consumo por chaves do Ponto de Consumo. Obtém os seguintes atributos dos pontos de
	 * consumo: {@code imovel}, {@code rota}, {@code instalacaoMedidor}, {@code situacaoConsumo}, {@code ramoAtividade}, {@code segmento}.
	 * Ordenados por descrição do ponto de consumo.
	 * 
	 * @param listaChavesPontoConsumo - {@link List}
	 * @return pontos de consumo
	 **/
	Collection<PontoConsumo> consultarAtributosDePontosDeConsumo(List<PontoConsumo> listaChavesPontoConsumo);

	/**
	 * Agrupa os pontos de consumo por rota.
	 * 
	 * @param listaPontoConsumo - {@link Collection}
	 * @return mapa de pontos de consumo por rota - {@link Map}
	 */
	Map<Rota, Collection<PontoConsumo>> agruparPontosDeConsumoPorRota(Collection<PontoConsumo> listaPontoConsumo);

	/**
	 * Remove de uma lista de pontos de consumo, os pontos com instalacaoMedidor e com situação de aguardando ativação.
	 * 
	 * @param pontosConsumo {@link Collection}
	 * @return pontos de consumo filtrados
	 */
	Collection<PontoConsumo> removerPontosConsumoAguardandoAtivacaoComInstalacaoMedidor(Collection<PontoConsumo> pontosConsumo);

	/**
	 * Consulta os pontos de consumo por chaves de Rota. Obtém os seguintes atributos do ponto de consumo: {@code chavePrimaria},
	 * {@code descricao}, {@code situacaoConsumo}. Obtém os seguintes atributos da rota do ponto de consumo: {@code chavePrimaria}. Obtém os
	 * seguintes do imóvel do ponto de consumo: {@code nome}, {@code chavePrimaria}. Obtém os seguintes atributos da instação do medidor do
	 * ponto de consumo: {@code chavePrimaria}.
	 * 
	 * @param chavesPrimariasRotas {@link Long}
	 * @return mapa de coleção de PontoConsumo por Rota
	 */
	Map<Rota, Collection<PontoConsumo>> consultarPontosDeConsumoPorRotas(Long[] chavesPrimariasRotas);
	
	/**
	 * Método responsável por ordenar os VO's de
	 * ponto de consumo pela descrição
	 * formatada.
	 * 
	 * @param listaPontoConsumoVO {@link List}
	 */
	void ordernarListaPontoConsumoRotaVOPorDescricao(List<PontoConsumoRotaVO> listaPontoConsumoVO);
	
	/**
	 * Converter lista ponto consumo.
	 * 
	 * @param listaPontoConsumo {@link Collection}
	 * @param pontosConsumoAssociados {@link Map}
	 * @param rota {@link Rota}
	 * @return List {@link List}
	 */
	public List<PontoConsumoRotaVO> converterListaPontoConsumo(Collection<PontoConsumo> listaPontoConsumo,
			Map<Long, Object> pontosConsumoAssociados, Rota rota);

	/**
	 * Converter ponto consumo vo para ponto consumo.
	 * 
	 * @param pontosConsumoAssociados {@link Map}
	 * @param listaPontoConsumoAssociados {@link Collection}
	 * @param listaPontoConsumoVO {@link List}
	 * @throws GGASException {@link GGASException}
	 */
	void converterPontoConsumoVOParaPontoConsumo(Map<Long, Object> pontosConsumoAssociados,
			Collection<PontoConsumo> listaPontoConsumoAssociados, List<PontoConsumoRotaVO> listaPontoConsumoVO) throws GGASException;

	/**
	 * Ordenar lista ponto consumo.
	 * 
	 * @param listaPontoConsumoAssociados {@link Collection}
	 * @param listaPontoConsumoVO {@link List}
	 * @return List  {@link List}
	 */
	List<PontoConsumo> ordenarListaPontoConsumo(Collection<PontoConsumo> listaPontoConsumoAssociados,
			List<PontoConsumoRotaVO> listaPontoConsumoVO);

	/**
	 * Remove ponto consumo que já foram associados da lista dos que ainda não foram associados
	 * 
	 * @param listaPontoConsumo {@link Collection}
	 * @param listaPontoConsumoVO {@link Collection}
	 */
	void removerPontosConsumoAssociados(Collection<PontoConsumo> listaPontoConsumo, List<PontoConsumoRotaVO> listaPontoConsumoVO);

	/**
	 * Descrição Formatada
	 * 
	 * @param pontoConsumo {@link PontoConsumo}
	 * @return descrição {@link String}
	 */
	String descricaoFormatada(PontoConsumo pontoConsumo);

	/**
	 * Determina se o ponto de consumo está ativo.
	 * 
	 * @param pontoConsumo {@link PontoConsumo}
	 * @return isAtivo {@link boolean}
	 */
	boolean isPontoConsumoAtivo(PontoConsumo pontoConsumo);

	/**
	 * Determina se o ponto de consumo é Suprimido Definitivo.
	 * 
	 * @param pontoConsumo {@link PontoConsumo}
	 * @return isSuprimidoDefinitivo {@link boolean}
	 */
	boolean isPontoConsumoSuprimidoDefinitivo(PontoConsumo pontoConsumo);

	/**
	 * Determina se o Ponto de Consumo está aguardando ativação.
	 * 
	 * @param pontoConsumo {@link PontoConsumo}
	 * @return isAguardandoAtivacao {@link boolean}
	 */
	boolean isPontoConsumoAguardandoAtivacao(PontoConsumo pontoConsumo);

	/**
	 * Obtem o medidor atraves do ponto de consumo
	 * 
	 * @param idPontoConsumo chave primaria do ponto de consumo
	 * @return Medidor
	 */
	Medidor obterMedidorPorPontoConsumo(Long idPontoConsumo);
	
	/**
	 * Busca os pontos de consumo pela descrição.
	 * A busca é realizada de forma paginada.
	 *
	 * @param descricao descrição a ser buscada
	 * @param quantidadeMax quantidade máxima de registros retornados
	 * @param offset offset da consulta
	 * @return retorna a lista de pontos que obedecem a clausula de descrição
	 */
	Collection<PontoConsumo> listarPontosConsumoDescricao(String descricao, int quantidadeMax, int offset);

	/**
	 * Obtem os Pontos de Consumo dos imveis filhos
	 * @param idMovel Id Do imovel Pai
	 * @return Lista de Pontos de consumo
	 */
	List<PontoConsumo> obterPontosConsumoImoveisFilhos(Long idMovel);

	/**
	 * Consulta a lista de pontos de consumo filhos do imovel condominio informado
	 * @param imovel chave primaria do imovel condominio
	 * @return retorna a lsita de pontos de consumo com imoveis associados ao imovel condominio
	 */
	Collection<PontoConsumo> consultarPontosConsumoFilhosImovelCondominio(Long imovel);
	
	
	/**
	 * Metodo utilizado para recuperar o ponto de consumo com o imovel pai carregado
	 * @param chavePrimaria Identificador do ponto de consumo
	 * @return Ponto de Consumo carregado
	 * @throws NegocioException Regra de negocio que pode ser violada
	 */
	PontoConsumo obterPontoConsumoImovel(long chavePrimaria) throws NegocioException;

	/**
	 * Atualiza os números sequenciais de leitura dos pontos de consumo de uma rota.
	 * 
	 * @param pontosConsumo - {@link Collection}
	 * @param rota - {@link Rota}
	 */
	void atualizarSequencialLeituraDePontosConsumoPorRota(Collection<PontoConsumo> pontosConsumo, Rota rota);

	/**
	 * Carrega um par de coleção de pontos de consumo e de VOs mapeados por chave primária associados a uma rota.
	 * 
	 * @param rota - {@link Rota}
	 * @param list - {@link Collection}
	 * @param pontosConsumoAssociados - {@link Map}
	 * @return par de coleção e VOs - {@link Pair}
	 * @throws GGASException - {@link GGASException}
	 */
	Pair<Collection<PontoConsumo>, Map<Long, Object>> carregarAtributosDePontosConsumoAssociados(Rota rota,
			Collection<PontoConsumo> list, Map<Long, Object> pontosConsumoAssociados) throws GGASException;

	/**
	 * Consultar Pontos Consumo Detalhamento Cronograma
	 * 
	 * @param filtro {@link Map}
	 * @param propriedadesLazy {@link String}
	 * @return {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	Collection<PontoConsumo> consultarPontosConsumoDetalhamentoCronograma(Map<String, Object> filtro, String... propriedadesLazy)
			throws NegocioException;

	/**
	 * Método responsável por consultar os pontos de consumo a partir de uma rota.
	 * 
	 * @param indicadorLeitura {@link Boolean}
	 * @param listaRotas {@link listaRotas}
	 * @param chavesPrimariasRotas {@link Long[]}
	 * @return Coleção dos pontos de consumo ordenados pela sequencia de leitura
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Map<Long, Collection<PontoConsumo>> consultarMapearPontoConsumoPorRotaFaturavel(List<Rota> listaRotas,
			Long[] chavesPrimariasRotas, Boolean indicadorLeitura);

	/**
	 * Método responsável por consultar os pontos de consumo bloqueados com consumo.
	 * 
	 * @param chavesPrimariasRotas as chaves Primarias de Rotas
	 * @param anoMes ano mes de faturamento
	 * @param numeroCiclo numero do ciclo de faturamento
	 * @return Coleção dos pontos de consumo ordenados pela sequencia de leitura
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Map<Long, Collection<PontoConsumo>> consultarMapearPontoConsumoBloqueadoComConsumo(Long[] chavesPrimariasRotas,
			Integer anoMes, Integer numeroCiclo) throws NegocioException;
	
	/**
	 * Método responsável por retornar a lista de pontos de consumo de uma rota, que 
	 * foram gerados para leitura
	 * @param rota {@link Rota}}
	 * @return Lista d epontos de consumo {@link List}
	 */
	List<PontoConsumo> obterPontosConsumoLeituraMovumento(Rota rota);

	List<PontoConsumo> obterPontosConsumoSuperMedicaoDiaria(Rota rota);

	/**
	 * Método responsável por retornar a quantidade de pontos de consumo que possuem faturas geradas
	 * @param rota {@link Rota}
	 * @return quantidade de pontos de consumo {@link Long}
	 */
	Long obterQuantidadePontosConsumoFaturados(Rota rota);

	Collection<Long> consultarPontosConsumoConsistirLeituraProcessadoComAnormalidadeMasAnalisada(
			Map<String, Object> filtro);

	String obterProximoCodigoLegadoPontoContumo();

	Map<Integer, Long>listarPontoConsumoPorCep(String chaveCep);

	Collection<PontoConsumo> listarPontoConsumoChavesPrimarias(Long[] chavesPrimarias);
}
