/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.util.Collection;

import br.com.ggas.geral.Menu;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura de métodos relacionados as tabelas do GGAS
 *
 */
public interface Tabela extends EntidadeNegocio {

	String BEAN_ID_TABELA = "tabela";

	String TABELA_UNIDADE = "UNIDADE";

	String TABELA_ENTIDADE_CONTEUDO = "ENTIDADE_CONTEUDO entidade";

	/**
	 * @return the nome
	 */
	String getNome();

	/**
	 * @param nome
	 *            the nome to set
	 */
	void setNome(String nome);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the mnemonico
	 */
	String getMnemonico();

	/**
	 * @param mnemonico
	 *            the mnemonico to set
	 */
	void setMnemonico(String mnemonico);

	/**
	 * @return the atributoDinamico
	 */
	Boolean getAtributoDinamico();

	/**
	 * @param atributoDinamico
	 *            the atributoDinamico to set
	 */
	void setAtributoDinamico(Boolean atributoDinamico);

	/**
	 * @return the listaColunaTabela
	 */
	Collection<Coluna> getListaColunaTabela();

	/**
	 * @param listaColunaTabela
	 */
	void setListaColunaTabela(Collection<Coluna> listaColunaTabela);

	/**
	 * @param nomeClasse
	 */
	void setNomeClasse(String nomeClasse);

	/**
	 * @return Retorna Atributos Consulta Dinamica
	 */
	String getAtributosConsultaDinamica();

	/**
	 * @param atributosConsultaDinamica
	 */
	void setAtributosConsultaDinamica(String atributosConsultaDinamica);

	/**
	 * @return Retorno Nome da Classe
	 */
	String getNomeClasse();

	/**
	 * @return Retorna Coleção de Colunas
	 */
	Collection<Coluna> getColunas();

	/**
	 * @param colunas - Set colunas.
	 */
	void setColunas(Collection<Coluna> colunas);

	/**
	 * @return Retorna Consulta Dinamica
	 */
	Boolean getConsultaDinamica();

	/**
	 * Checks if is consulta dinamica.
	 * 
	 * @return consultaDinamica
	 */
	Boolean isConsultaDinamica();

	/**
	 * @param consultaDinamica
	 */
	void setConsultaDinamica(Boolean consultaDinamica);

	/**
	 * @return auditavel
	 */
	Boolean getAuditavel();

	/**
	 * @param auditavel
	 */
	void setAuditavel(Boolean auditavel);

	/**
	 * @return menu
	 */
	Menu getMenu();

	/**
	 * @param menu
	 *            the menu to set
	 */
	void setMenu(Menu menu);

	/**
	 * Checks if is alcada.
	 * 
	 * @return alcada
	 */
	Boolean isAlcada();

	/**
	 * @param alcada
	 *            the alcada to set
	 */
	void setAlcada(Boolean alcada);

	/**
	 * @return the numeroMesesDescarte
	 */
	Long getNumeroMesesDescarte();

	/**
	 * @param numeroMesesDescarte
	 *            the numeroMesesDescarte to set
	 */
	void setNumeroMesesDescarte(Long numeroMesesDescarte);

	/**
	 * @return Boolean - Retorna Integração.
	 */
	Boolean getIntegracao();

	/**
	 * @param integracao - Set integração.
	 */
	void setIntegracao(Boolean integracao);

	/**
	 * @return Boolean - Retorna Mapeamento.
	 */
	Boolean getMapeamento();

	/**
	 * @param mapeamento - Set mapeamento.
	 */
	void setMapeamento(Boolean mapeamento);

	/**
	 * @return Boolean - Retorno Constante
	 */
	public Boolean getConstante();

	/**
	 * @param constante - Set constante.
	 */
	public void setConstante(Boolean constante);

}
