/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 28/03/2014 15:02:36
 @author ifrancisco
 */

package br.com.ggas.cadastro.imovel;

import java.math.BigDecimal;

/**
 * Ponto Consumo AgrupadoVO
 * 
 * @author ifrancisco
 */
public class PontoConsumoAgrupadoVO {

	private String anoMesLeitura;

	private BigDecimal consumoInformado;

	private BigDecimal consumoReal;

	private BigDecimal leituraInformada;

	private Double fatorCorrecao;

	/**
	 * @return {@link String} anoMesLeitura
	 */
	public String getAnoMesLeitura() {

		return anoMesLeitura;
	}

	/**
	 * Define o anoMesLeitura
	 * 
	 * @param anoMesLeitura
	 */
	public void setAnoMesLeitura(String anoMesLeitura) {

		this.anoMesLeitura = anoMesLeitura;
	}

	/**
	 * @return {@link BigDecimal} consumoInformado
	 */
	public BigDecimal getConsumoInformado() {

		return consumoInformado;
	}

	/**
	 * Define o consumo informado
	 * @param consumoInformado
	 */
	public void setConsumoInformado(BigDecimal consumoInformado) {

		this.consumoInformado = consumoInformado;
	}

	/**
	 * @return {@link BigDecimal} consumoReal
	 */
	public BigDecimal getConsumoReal() {

		return consumoReal;
	}

	/**
	 * Define o valor do consumo real
	 * @param consumoReal
	 */
	public void setConsumoReal(BigDecimal consumoReal) {

		this.consumoReal = consumoReal;
	}

	/**
	 * @return {@link BigDecimal} leituraInformada
	 */
	public BigDecimal getLeituraInformada() {

		return leituraInformada;
	}

	/**
	 * DEfine a leitura informada
	 * @param leituraInformada
	 */
	public void setLeituraInformada(BigDecimal leituraInformada) {

		this.leituraInformada = leituraInformada;
	}

	/**
	 * @return {@link Double} getFatorCorrecao
	 */
	public Double getFatorCorrecao() {

		return fatorCorrecao;
	}

	/**
	 * Define o Fator Correção
	 * @param fatorCorrecao
	 */
	public void setFatorCorrecao(Double fatorCorrecao) {

		this.fatorCorrecao = fatorCorrecao;
	}

}
