/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface ControladorRamoAtividadeSubstituicaoTributaria
 * 
 * @author arthur
 *
 */
public interface ControladorRamoAtividadeSubstituicaoTributaria extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_RAMO_ATIVIDADE_SUBS_TRIB = "controladorRamoAtividadeSubstituicaoTributaria";

	String ERRO_NEGOCIO_REMOVER_RAMO_ATIVIDADE_SUBSTITUICAO_TRIBUTARIA = "ERRO_NEGOCIO_REMOVER_RAMO_ATIVIDADE_SUBSTITUICAO_TRIBUTARIA";

	/**
	 * Consultar
	 * RamoAtividadeSubstituicaoTributaria por
	 * filtro.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return Coleção de
	 *         RamoAtividadeSubstituicaoTributaria
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<RamoAtividadeSubstituicaoTributaria> consultarRamoAtividadeSubstituicaoTributaria(Map<String, Object> filtro)
					throws GGASException;

	/**
	 * Consultar
	 * RamoAtividadeSubstituicaoTributaria por
	 * chavePrimaria.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return RamoAtividadeSubstituicaoTributaria
	 * @throws GGASException
	 *             the GGAS exception
	 */
	RamoAtividadeSubstituicaoTributaria obterRamoAtividadeSubstituicaoTributaria(Long chavePrimaria) throws GGASException;

	/**
	 * Consulta
	 * RamoAtividadeSubstituicaoTributaria pelo
	 * RamoAtividade.
	 *
	 * @param ramoAtividade            o RamoAtividade
	 * @param ultimaLeitura the ultima leitura
	 * @return RamoAtividadeSubstituicaoTributaria
	 * @throws NegocioException             the negocio exception
	 */
	RamoAtividadeSubstituicaoTributaria obterRamoAtividadeSubstTribPorRamoAtividade(RamoAtividade ramoAtividade, Date ultimaLeitura) 
			throws NegocioException;

	/**
	 * Método responsável por listar os
	 * RamoAtivdadeSubstituicaoTributaria de
	 * acordo com seus Ramos de Atividade.
	 * 
	 * @return mapa de
	 *         RamoAtivdadeSubstituicaoTributaria
	 */
	Map<RamoAtividade, RamoAtividadeSubstituicaoTributaria> listarRamoAtividadeSubstituicaoTributariaPorRamoAtividade();

	/**
	 * Listar ramo atividade substituicao tributaria por ramo atividade.
	 * 
	 * @param ramoAtividade
	 *            the ramo atividade
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<RamoAtividadeSubstituicaoTributaria> listarRamoAtividadeSubstituicaoTributariaPorRamoAtividade(RamoAtividade ramoAtividade)
					throws GGASException;

	/**
	 * Obtém uma coleção de RamoAtividadeSubstituicaoTributaria por RamoAtividade.
	 * 
	 * @param ramosDeAtividade {@link Collection}
	 * @return mapa de RamoAtividadeSubstituicaoTributaria por RamoAtividade
	 */
	Map<RamoAtividade, Collection<RamoAtividadeSubstituicaoTributaria>> obterRamoAtividadeSubstituicaoTributariaPorRamoAtividade(
					Collection<RamoAtividade> ramosDeAtividade);

	/**
	 * Seleciona o RamoAtividadeSubstituicaoTributaria com 
	 * data de inicio da vigência menor ou igual que a data leitura 
	 * e data fim de vigência maior ou igual que a data de leitura.
	 * @param listaRamoAtividadeSubstituicaoTributaria {@link Collection}
	 * @param ultimaLeitura data da última leitura 
	 * @return RamoAtividadeSubstituicaoTributaria em vigencia
	 * @throws NegocioException the Negocio exception
	 */
	RamoAtividadeSubstituicaoTributaria escolherRamoAtividadeSubstituicaoTributariaEmVigencia(
					Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria, Date ultimaLeitura) throws NegocioException;
}
