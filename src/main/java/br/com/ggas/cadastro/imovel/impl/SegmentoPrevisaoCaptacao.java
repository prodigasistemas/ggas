/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 29/10/2014 16:14:03
 @author vpessoa
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Segmento Previsao Captacao
 * 
 * @author vpessoa
 */
public class SegmentoPrevisaoCaptacao extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = -4780825820461602649L;

	private Segmento segmento;

	private Integer anoReferencia;

	private Integer previsaoJaneiro;

	private Integer previsaoFevereiro;

	private Integer previsaoMarco;

	private Integer previsaoAbril;

	private Integer previsaoMaio;

	private Integer previsaoJunho;

	private Integer previsaoJulho;

	private Integer previsaoAgosto;

	private Integer previsaoSetembro;

	private Integer previsaoOutubro;

	private Integer previsaoNovembro;

	private Integer previsaoDezembro;

	public Segmento getSegmento() {

		return segmento;
	}

	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	public Integer getAnoReferencia() {

		return anoReferencia;
	}

	public void setAnoReferencia(Integer anoReferencia) {

		this.anoReferencia = anoReferencia;
	}

	public Integer getPrevisaoJaneiro() {

		return previsaoJaneiro;
	}

	public void setPrevisaoJaneiro(Integer previsaoJaneiro) {

		this.previsaoJaneiro = previsaoJaneiro;
	}

	public Integer getPrevisaoFevereiro() {

		return previsaoFevereiro;
	}

	public void setPrevisaoFevereiro(Integer previsaoFevereiro) {

		this.previsaoFevereiro = previsaoFevereiro;
	}

	public Integer getPrevisaoMarco() {

		return previsaoMarco;
	}

	public void setPrevisaoMarco(Integer previsaoMarco) {

		this.previsaoMarco = previsaoMarco;
	}

	public Integer getPrevisaoAbril() {

		return previsaoAbril;
	}

	public void setPrevisaoAbril(Integer previsaoAbril) {

		this.previsaoAbril = previsaoAbril;
	}

	public Integer getPrevisaoMaio() {

		return previsaoMaio;
	}

	public void setPrevisaoMaio(Integer previsaoMaio) {

		this.previsaoMaio = previsaoMaio;
	}

	public Integer getPrevisaoJunho() {

		return previsaoJunho;
	}

	public void setPrevisaoJunho(Integer previsaoJunho) {

		this.previsaoJunho = previsaoJunho;
	}

	public Integer getPrevisaoJulho() {

		return previsaoJulho;
	}

	public void setPrevisaoJulho(Integer previsaoJulho) {

		this.previsaoJulho = previsaoJulho;
	}

	public Integer getPrevisaoAgosto() {

		return previsaoAgosto;
	}

	public void setPrevisaoAgosto(Integer previsaoAgosto) {

		this.previsaoAgosto = previsaoAgosto;
	}

	public Integer getPrevisaoSetembro() {

		return previsaoSetembro;
	}

	public void setPrevisaoSetembro(Integer previsaoSetembro) {

		this.previsaoSetembro = previsaoSetembro;
	}

	public Integer getPrevisaoOutubro() {

		return previsaoOutubro;
	}

	public void setPrevisaoOutubro(Integer previsaoOutubro) {

		this.previsaoOutubro = previsaoOutubro;
	}

	public Integer getPrevisaoNovembro() {

		return previsaoNovembro;
	}

	public void setPrevisaoNovembro(Integer previsaoNovembro) {

		this.previsaoNovembro = previsaoNovembro;
	}

	public Integer getPrevisaoDezembro() {

		return previsaoDezembro;
	}

	public void setPrevisaoDezembro(Integer previsaoDezembro) {

		this.previsaoDezembro = previsaoDezembro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(segmento == null) {
			stringBuilder.append(Constantes.SEGMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(anoReferencia == null || anoReferencia.equals(0)) {
			stringBuilder.append(Constantes.ANO_REFERENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

}
