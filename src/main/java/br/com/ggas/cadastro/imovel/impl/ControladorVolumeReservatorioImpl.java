/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.imovel.ControladorAreaConstruidaFaixa;
import br.com.ggas.cadastro.imovel.ControladorVolumeReservatorio;
import br.com.ggas.cadastro.imovel.VolumeReservatorio;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

class ControladorVolumeReservatorioImpl extends ControladorNegocioImpl implements ControladorVolumeReservatorio {

	private static final String FILTRO_VOLUME_MAIOR_FAIXA = "volumeMaiorFaixa";
	private static final String FILTRO_VOLUME_MENOR_FAIXA = "volumeMenorFaixa";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(VolumeReservatorio.BEAN_ID_VOLUME_RESERVATORIO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(VolumeReservatorio.BEAN_ID_VOLUME_RESERVATORIO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.volumereservatorio
	 * .ControladorVolumeReservatorio#
	 * consultarVolumeReservatorios(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<VolumeReservatorio> consultarVolumeReservatorios(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			Integer volumeMenorFaixa = (Integer) filtro.get(FILTRO_VOLUME_MENOR_FAIXA);
			if(volumeMenorFaixa != null) {
				criteria.add(Restrictions.eq(FILTRO_VOLUME_MENOR_FAIXA, volumeMenorFaixa));
			}
			Integer volumeMaiorFaixa = (Integer) filtro.get(FILTRO_VOLUME_MAIOR_FAIXA);
			if(volumeMaiorFaixa != null) {
				criteria.add(Restrictions.eq(FILTRO_VOLUME_MAIOR_FAIXA, volumeMaiorFaixa));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		validarIntervaloVolumeFaixa((VolumeReservatorio) entidadeNegocio);
		validarVolumeFaixaExistente((VolumeReservatorio) entidadeNegocio);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		validarIntervaloVolumeFaixa((VolumeReservatorio) entidadeNegocio);
		validarVolumeFaixaExistente((VolumeReservatorio) entidadeNegocio);
	}

	/**
	 * Validar intervalo volume faixa.
	 * 
	 * @param volumeReservatorio
	 *            the volume reservatorio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarIntervaloVolumeFaixa(VolumeReservatorio volumeReservatorio) throws NegocioException {

		if(volumeReservatorio.getVolumeMenorFaixa() > volumeReservatorio.getVolumeMaiorFaixa()) {
			throw new NegocioException(ControladorAreaConstruidaFaixa.ERRO_NEGOCIO_INTERVALO_FAIXA, true);
		}
	}

	/**
	 * Validar volume faixa existente.
	 * 
	 * @param volumeReservatorio
	 *            the volume reservatorio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarVolumeFaixaExistente(VolumeReservatorio volumeReservatorio) throws NegocioException {


		Map<String, Object> filtro = new HashMap<>();
		filtro.put(FILTRO_VOLUME_MENOR_FAIXA, volumeReservatorio.getVolumeMenorFaixa());
		filtro.put(FILTRO_VOLUME_MAIOR_FAIXA, volumeReservatorio.getVolumeMaiorFaixa());
		List<VolumeReservatorio> volumeReservatorios = (List<VolumeReservatorio>) this.consultarVolumeReservatorios(filtro);

		if(!volumeReservatorios.isEmpty()) {
			VolumeReservatorio volumeReservatorioExistente = volumeReservatorios.get(0);

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(volumeReservatorioExistente);

			if(volumeReservatorioExistente.getChavePrimaria() != volumeReservatorio.getChavePrimaria()) {
				throw new NegocioException(ControladorVolumeReservatorio.ERRO_NEGOCIO_VOLUME_FAIXA_EXISTENTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.volumereservatorio
	 * .ControladorVolumeReservatorio#
	 * validarRemoverVolumeReservatorios
	 * (java.lang.Long[])
	 */
	@Override
	public void validarRemoverVolumeReservatorios(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

}
