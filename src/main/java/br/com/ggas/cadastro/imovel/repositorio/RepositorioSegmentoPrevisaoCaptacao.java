/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *30/12/2013
 * vpessoa
 * 15:58:14
 */

package br.com.ggas.cadastro.imovel.repositorio;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.cadastro.imovel.impl.SegmentoPrevisaoCaptacao;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de  
 * 
 * @author vpessoa
 */
@Repository
public class RepositorioSegmentoPrevisaoCaptacao extends RepositorioGenerico {

	/**
	 * Instantiates a new repositorio segmento previsao captacao.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioSegmentoPrevisaoCaptacao(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new SegmentoPrevisaoCaptacao();
	}

	@Override
	public Class<SegmentoPrevisaoCaptacao> getClasseEntidade() {

		return SegmentoPrevisaoCaptacao.class;

	}

	/**
	 * Consultar segmento previsao captacao.
	 * 
	 * @param segmentoPrevisaoCaptacao
	 *            the segmento previsao captacao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<SegmentoPrevisaoCaptacao> consultarSegmentoPrevisaoCaptacao(SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao)
					throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		if(segmentoPrevisaoCaptacao.getSegmento() != null) {
			criteria.add(Restrictions.eq("segmento.chavePrimaria", segmentoPrevisaoCaptacao.getSegmento().getChavePrimaria()));
		}

		if(segmentoPrevisaoCaptacao.getAnoReferencia() != null && segmentoPrevisaoCaptacao.getAnoReferencia() != -1) {
			criteria.add(Restrictions.eq("anoReferencia", segmentoPrevisaoCaptacao.getAnoReferencia()));
		}

		return (Collection<SegmentoPrevisaoCaptacao>) (Collection<?>) criteria.list();

	}
}
