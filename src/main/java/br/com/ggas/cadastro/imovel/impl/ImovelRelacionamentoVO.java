package br.com.ggas.cadastro.imovel.impl;


/**
 * Classe responsável pela representação dos atributos 
 * relativos ao relacionamento de um Imóvel com um Cliente.
 * 
 * @author esantana
 *
 */
public class ImovelRelacionamentoVO {

	private Long idCliente;
	
	private String nomeCompletoCliente;
	
	private String documentoFormatado;
	
	private String emailCliente;
	
	private String enderecoFormatadoCliente;
	
	private Long idTipoRelacionamento;
	
	private String relacaoInicio;
	
	private Long idMotivoFimRelacionamentoClienteImovel;
	
	private String relacaoFim;

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdTipoRelacionamento() {
		return idTipoRelacionamento;
	}

	public void setIdTipoRelacionamento(Long idTipoRelacionamento) {
		this.idTipoRelacionamento = idTipoRelacionamento;
	}

	public String getRelacaoInicio() {
		return relacaoInicio;
	}

	public void setRelacaoInicio(String relacaoInicio) {
		this.relacaoInicio = relacaoInicio;
	}

	public Long getIdMotivoFimRelacionamentoClienteImovel() {
		return idMotivoFimRelacionamentoClienteImovel;
	}

	public void setIdMotivoFimRelacionamentoClienteImovel(Long idMotivoFimRelacionamentoClienteImovel) {
		this.idMotivoFimRelacionamentoClienteImovel = idMotivoFimRelacionamentoClienteImovel;
	}

	public String getRelacaoFim() {
		return relacaoFim;
	}

	public void setRelacaoFim(String relacaoFim) {
		this.relacaoFim = relacaoFim;
	}

	public String getNomeCompletoCliente() {
		return nomeCompletoCliente;
	}

	public void setNomeCompletoCliente(String nomeCompletoCliente) {
		this.nomeCompletoCliente = nomeCompletoCliente;
	}

	public String getDocumentoFormatado() {
		return documentoFormatado;
	}

	public void setDocumentoFormatado(String documentoFormatado) {
		this.documentoFormatado = documentoFormatado;
	}

	public String getEmailCliente() {
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public String getEnderecoFormatadoCliente() {
		return enderecoFormatadoCliente;
	}

	public void setEnderecoFormatadoCliente(String enderecoFormatadoCliente) {
		this.enderecoFormatadoCliente = enderecoFormatadoCliente;
	}
}
