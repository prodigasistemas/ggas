/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.math.BigDecimal;
import java.util.Collection;

import br.com.ggas.contabil.ContaContabil;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.rota.Periodicidade;


/**
 * Interface responsável pela assinatura dos métodos de Segmento
 * 
 */
public interface Segmento extends EntidadeNegocio {

	String BEAN_ID_SEGMENTO = "segmento";

	String DESCRICAO = "SEGMENTO_DESCRICAO";

	String DESCRICAO_ABREVIADA = "SEGMENTO_DESCRICAO_ABREVIADA";

	String TIPO_SEGMENTO = "SEGMENTO_TIPO_SEGMENTO";

	String CONSUMO_MINIMO = "SEGMENTO_CONSUMO_MINIMO";

	String CONSUMO_ESTOURO = "SEGMENTO_CONSUMO_ESTOURO";

	String CONSUMO_BAIXO = "SEGMENTO_CONSUMO_BAIXO";

	String PERCENTUAL_CONSUMO_BAIXO = "SEGMENTO_PERCENTUAL_CONSUMO_BAIXO";

	String CONSUMO_ALTO = "SEGMENTO_CONSUMO_ALTO";

	String FATOR_CONSUMO_ALTO = "SEGMENTO_FATOR_CONSUMO_ALTO";

	String SEGMENTO_ROTULO = "SEGMENTO_ROTULO";

	String SEGMENTOS_ROTULO = "SEGMENTOS_ROTULO";

	String NUMERO_CICLOS = "SEGMENTO_NUMERO_CICLOS";

	String NUMERO_DIAS_FATURAMENTO = "SEGMENTO_NUMERO_DIAS_FATURAMENTO";

	String PERIODICIDADE = "SEGMENTO_PERIODICIDADE";
	
	String[] RAMO_ATIVIDADE = new String[] {"ramosAtividades"};

	String VOLUME_VIRADA_MEDIDOR = "SEGMENTO_VOLUME_VIRADA_MEDIDOR";

	String NUMERO_VEZES_MEDIA_VIRADA_MEDIDOR = "SEGMENTO_NUMERO_VEZES_MEDIA_VIRADA_MEDIDOR";

	String CONTA_CONTABIL = "ContaContábil";
	
	String ERRO_PERIODICIDA = "ERRO_PERIODICIDA";

	String ERRO_NUMERO_DIAS_FATURAMENTO = "ERRO_NUMERO_DIAS_FATURAMENTO";
	
	String ERRO_NUMERO_CICLOS = "ERRO_NUMERO_CICLOS";
	
	String ERRO_TIPO_CONSUMO_FATURAMENTO = "ERRO_TIPO_CONSUMO_FATURAMENTO";
	/**
	 * Retorna uma {@link Collection} {@link RamoAtividade}.
	 * @return the ramoAtividades
	 */
	Collection<RamoAtividade> getRamosAtividades();

	/**
	 * Sets the ramos atividades.
	 *
	 * @param ramosAtividades            the ramosAtividades to set
	 */
	void setRamosAtividades(Collection<RamoAtividade> ramosAtividades);

	/**
	 * Gets the descricao.
	 *
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * Sets the descricao.
	 *
	 * @param descricao            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * Gets the descricao abreviada.
	 *
	 * @return the descricaoAbreviada
	 */
	String getDescricaoAbreviada();

	/**
	 * Sets the descricao abreviada.
	 *
	 * @param descricaoAbreviada            the descricaoAbreviada to set
	 */
	void setDescricaoAbreviada(String descricaoAbreviada);

	/**
	 * Gets the consumo estouro.
	 *
	 * @return the consumoEstouro
	 */
	BigDecimal getConsumoEstouro();

	/**
	 * Sets the consumo estouro.
	 *
	 * @param consumoEstouro            the consumoEstouro to set
	 */
	void setConsumoEstouro(BigDecimal consumoEstouro);

	/**
	 * Gets the tipo segmento.
	 *
	 * @return the tipoSegmento
	 */
	TipoSegmento getTipoSegmento();

	/**
	 * Sets the tipo segmento.
	 *
	 * @param tipoSegmento            the tipoSegmento to set
	 */
	void setTipoSegmento(TipoSegmento tipoSegmento);

	/**
	 * Gets the numero ciclos.
	 *
	 * @return the numeroCiclos
	 */
	Integer getNumeroCiclos();

	/**
	 * Sets the numero ciclos.
	 *
	 * @param numeroMeses            the numeroMeses to set
	 */
	void setNumeroCiclos(Integer numeroMeses);

	/**
	 * Gets the numero dias faturamento.
	 *
	 * @return the numero dias faturamento
	 */
	Integer getNumeroDiasFaturamento();

	/**
	 * Sets the numero dias faturamento.
	 *
	 * @param numeroDiasFaturamento            the numeroDiasFaturamento to set
	 */
	void setNumeroDiasFaturamento(Integer numeroDiasFaturamento);

	/**
	 * Gets the periodicidade.
	 *
	 * @return the periodicidade
	 */
	Periodicidade getPeriodicidade();

	/**
	 * Sets the periodicidade.
	 *
	 * @param periodicidade            the periodicidade to set
	 */
	void setPeriodicidade(Periodicidade periodicidade);

	/**
	 * Gets the conta contabil.
	 *
	 * @return the conta contabil
	 */
	ContaContabil getContaContabil();

	/**
	 * Sets the conta contabil.
	 *
	 * @param contaContabil the new conta contabil
	 */
	void setContaContabil(ContaContabil contaContabil);

	/**
	 * Gets the segmento amostragem PC ss.
	 *
	 * @return the segmento amostragem PC ss
	 */
	Collection<SegmentoAmostragemPCS> getSegmentoAmostragemPCSs();

	/**
	 * Sets the segmento amostragem PC ss.
	 *
	 * @param segmentoAmostragemPCSs the new segmento amostragem PC ss
	 */
	void setSegmentoAmostragemPCSs(Collection<SegmentoAmostragemPCS> segmentoAmostragemPCSs);

	/**
	 * Gets the segmento intervalo PC ss.
	 *
	 * @return the segmento intervalo PC ss
	 */
	Collection<SegmentoIntervaloPCS> getSegmentoIntervaloPCSs();

	/**
	 * Sets the segmento intervalo PC ss.
	 *
	 * @param segmentoIntervaloPCSs the new segmento intervalo PC ss
	 */
	void setSegmentoIntervaloPCSs(Collection<SegmentoIntervaloPCS> segmentoIntervaloPCSs);

	/**
	 * Gets the tamanho reducao.
	 *
	 * @return the tamanho reducao
	 */
	Integer getTamanhoReducao();

	/**
	 * Sets the tamanho reducao.
	 *
	 * @param tamanhoReducao the new tamanho reducao
	 */
	void setTamanhoReducao(Integer tamanhoReducao);

	/**
	 * Gets the volume medio estouro.
	 *
	 * @return the volume medio estouro
	 */
	Integer getVolumeMedioEstouro();

	/**
	 * Sets the volume medio estouro.
	 *
	 * @param volumeMedioEstouro the new volume medio estouro
	 */
	void setVolumeMedioEstouro(Integer volumeMedioEstouro);

	/**
	 * Gets the previsao janeiro.
	 *
	 * @return the previsao janeiro
	 */
	public Integer getPrevisaoJaneiro();

	/**
	 * Sets the previsao janeiro.
	 *
	 * @param previsaoJaneiro the new previsao janeiro
	 */
	public void setPrevisaoJaneiro(Integer previsaoJaneiro);

	/**
	 * Gets the previsao fevereiro.
	 *
	 * @return the previsao fevereiro
	 */
	public Integer getPrevisaoFevereiro();

	/**
	 * Sets the previsao fevereiro.
	 *
	 * @param previsaoFevereiro the new previsao fevereiro
	 */
	public void setPrevisaoFevereiro(Integer previsaoFevereiro);

	/**
	 * Gets the previsao marco.
	 *
	 * @return the previsao marco
	 */
	public Integer getPrevisaoMarco();

	/**
	 * Sets the previsao marco.
	 *
	 * @param previsaoMarco the new previsao marco
	 */
	public void setPrevisaoMarco(Integer previsaoMarco);

	/**
	 * Gets the previsao abril.
	 *
	 * @return the previsao abril
	 */
	public Integer getPrevisaoAbril();

	/**
	 * Sets the previsao abril.
	 *
	 * @param previsaoAbril the new previsao abril
	 */
	public void setPrevisaoAbril(Integer previsaoAbril);

	/**
	 * Gets the previsao maio.
	 *
	 * @return the previsao maio
	 */
	public Integer getPrevisaoMaio();

	/**
	 * Sets the previsao maio.
	 *
	 * @param previsaoMaio the new previsao maio
	 */
	public void setPrevisaoMaio(Integer previsaoMaio);

	/**
	 * Gets the previsao junho.
	 *
	 * @return the previsao junho
	 */
	public Integer getPrevisaoJunho();

	/**
	 * Sets the previsao junho.
	 *
	 * @param previsaoJunho the new previsao junho
	 */
	public void setPrevisaoJunho(Integer previsaoJunho);

	/**
	 * Gets the previsao julho.
	 *
	 * @return the previsao julho
	 */
	public Integer getPrevisaoJulho();

	/**
	 * Sets the previsao julho.
	 *
	 * @param previsaoJulho the new previsao julho
	 */
	public void setPrevisaoJulho(Integer previsaoJulho);

	/**
	 * Gets the previsao agosto.
	 *
	 * @return the previsao agosto
	 */
	public Integer getPrevisaoAgosto();

	/**
	 * Sets the previsao agosto.
	 *
	 * @param previsaoAgosto the new previsao agosto
	 */
	public void setPrevisaoAgosto(Integer previsaoAgosto);

	/**
	 * Gets the previsao setembro.
	 *
	 * @return the previsao setembro
	 */
	public Integer getPrevisaoSetembro();

	/**
	 * Sets the previsao setembro.
	 *
	 * @param previsaoSetembro the new previsao setembro
	 */
	public void setPrevisaoSetembro(Integer previsaoSetembro);

	/**
	 * Gets the previsao outubro.
	 *
	 * @return the previsao outubro
	 */
	public Integer getPrevisaoOutubro();

	/**
	 * Sets the previsao outubro.
	 *
	 * @param previsaoOutubro the new previsao outubro
	 */
	public void setPrevisaoOutubro(Integer previsaoOutubro);

	/**
	 * Gets the previsao novembro.
	 *
	 * @return the previsao novembro
	 */
	public Integer getPrevisaoNovembro();

	/**
	 * Sets the previsao novembro.
	 *
	 * @param previsaoNovembro the new previsao novembro
	 */
	public void setPrevisaoNovembro(Integer previsaoNovembro);

	/**
	 * Gets the previsao dezembro.
	 *
	 * @return the previsao dezembro
	 */
	public Integer getPrevisaoDezembro();

	/**
	 * Sets the previsao dezembro.
	 *
	 * @param previsaoDezembro the new previsao dezembro
	 */
	public void setPrevisaoDezembro(Integer previsaoDezembro);

	/**
	 * Gets the usa programacao consumo.
	 *
	 * @return the usa programacao consumo
	 */
	public Boolean getUsaProgramacaoConsumo();

	/**
	 * Sets the usa programacao consumo.
	 *
	 * @param usaProgramacaoConsumo the new usa programacao consumo
	 */
	public void setUsaProgramacaoConsumo(Boolean usaProgramacaoConsumo);
	
	
	/**
	 * @return the indicadorEquipamentoPotenciaFixa
	 */
	public Boolean getindicadorEquipamentoPotenciaFixa();

	/**
	 * setindicador Equipamento Potencia Fixa
	 * 
	 * @param indicadorEquipamentoPotenciaFixa the indicadorEquipamentoPotenciaFixa to set
	 */
	public void setindicadorEquipamentoPotenciaFixa(Boolean indicadorEquipamentoPotenciaFixa);
	

	/**
	 * @return the tipoConsumoFaturamento
	 */
	public EntidadeConteudo getTipoConsumoFaturamento();

	/**
	 * @param itipoConsumoFaturamento the tipoConsumoFaturamento to set
	 */
	public void setTipoConsumoFaturamento(EntidadeConteudo tipoConsumoFaturamento);
}
