/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SegmentoAmostragemPCS;
import br.com.ggas.cadastro.imovel.SegmentoIntervaloPCS;
import br.com.ggas.cadastro.imovel.TipoSegmento;
import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.consumo.ControladorFaixaConsumoVariacao;
import br.com.ggas.medicao.consumo.FaixaConsumoVariacao;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 * Classe responsável pelos métodos de consulta 
 * inserção, validação, atualização e remoção
 * de Segmento
 *
 */
public class ControladorSegmentoImpl extends ControladorNegocioImpl implements ControladorSegmento {

	private static final String FROM = " from ";
	private static final String NUMERO_DIAS_FATURAMENTO = "numeroDiasFaturamento";
	private static final String NUMERO_CICLOS = "numeroCiclos";
	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Segmento.BEAN_ID_SEGMENTO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Segmento.BEAN_ID_SEGMENTO);
	}

	public Class<?> getClasseEntidadeSegmentoAmostragemPCS() {

		return ServiceLocator.getInstancia().getClassPorID(SegmentoAmostragemPCS.BEAN_ID_SEGMENTO_AMOSTRAGEM_PCS);
	}

	public Class<?> getClasseEntidadeSegmentoIntervaloPCS() {

		return ServiceLocator.getInstancia().getClassPorID(SegmentoIntervaloPCS.BEAN_ID_SEGMENTO_INTERVALO_PCS);
	}

	public Class<?> getClasseEntidadeConteudo() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}

	public Class<?> getClasseIntervaloPCS() {

		return ServiceLocator.getInstancia().getClassPorID(IntervaloPCS.BEAN_ID_INTERVALO_PCS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#consultarSegmento(java.util.Map)
	 */
	@Override
	public Collection<Segmento> consultarSegmento(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricao, MatchMode.ANYWHERE));
			}
			String descricaoAbreviada = (String) filtro.get(DESCRICAO_ABREVIADA);
			if(!StringUtils.isEmpty(descricaoAbreviada)) {
				criteria.add(Restrictions.ilike(DESCRICAO_ABREVIADA, descricaoAbreviada, MatchMode.ANYWHERE));
			}
			Integer codigoTipoSegmento = (Integer) filtro.get("codigoTipoSegmento");
			if(codigoTipoSegmento != null && codigoTipoSegmento > 0) {
				criteria.add(Restrictions.eq("tipoSegmento.codigo", codigoTipoSegmento));
			}
			Integer periodicidade = (Integer) filtro.get("periodicidade");
			if(periodicidade != null && periodicidade > 0) {
				criteria.add(Restrictions.eq("periodicidade.codigo", periodicidade));
			}
			Long idPeriodicidade = (Long) filtro.get("idPeriodicidade");
			if(idPeriodicidade != null && idPeriodicidade > 0) {
				criteria.add(Restrictions.eq("periodicidade.chavePrimaria", idPeriodicidade));
			}

			Integer consumoMinimo = (Integer) filtro.get("consumoMinimo");
			if(consumoMinimo != null) {
				criteria.add(Restrictions.eq("consumoMinimo", consumoMinimo));
			}
			Integer consumoEstouro = (Integer) filtro.get("consumoEstouro");
			if(consumoEstouro != null) {
				criteria.add(Restrictions.eq("consumoEstouro", consumoEstouro));
			}
			BigDecimal fatorConsumoEstouro = (BigDecimal) filtro.get("fatorConsumoEstouro");
			if(fatorConsumoEstouro != null) {
				criteria.add(Restrictions.eq("fatorConsumoEstouro", fatorConsumoEstouro));
			}
			Integer consumoBaixo = (Integer) filtro.get("consumoBaixo");
			if(consumoBaixo != null) {
				criteria.add(Restrictions.eq("consumoBaixo", consumoBaixo));
			}
			BigDecimal percentualConsumoBaixo = (BigDecimal) filtro.get("percentualConsumoBaixo");
			if(percentualConsumoBaixo != null) {
				criteria.add(Restrictions.eq("percentualConsumoBaixo", percentualConsumoBaixo));
			}
			Integer consumoAlto = (Integer) filtro.get("consumoAlto");
			if(consumoAlto != null) {
				criteria.add(Restrictions.eq("consumoAlto", consumoAlto));
			}
			BigDecimal fatorConsumoAlto = (BigDecimal) filtro.get("fatorConsumoAlto");
			if(fatorConsumoAlto != null) {
				criteria.add(Restrictions.eq("fatorConsumoAlto", fatorConsumoAlto));
			}
			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}
			Integer numeroCiclos = (Integer) filtro.get(NUMERO_CICLOS);
			if(consumoAlto != null) {
				criteria.add(Restrictions.eq(NUMERO_CICLOS, numeroCiclos));
			}
			Integer numeroDiasFaturamento = (Integer) filtro.get(NUMERO_DIAS_FATURAMENTO);
			if(consumoAlto != null) {
				criteria.add(Restrictions.eq(NUMERO_DIAS_FATURAMENTO, numeroDiasFaturamento));
			}
			Long idTipoSegmento = (Long) filtro.get("idTipoSegmento");
			if(idTipoSegmento != null && idTipoSegmento > 0) {
				criteria.add(Restrictions.eq("tipoSegmento.chavePrimaria", idTipoSegmento));
			}
		}
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#consultarSegmentoInclusaoFaixa(java.util.Map)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<Segmento> consultarSegmentoInclusaoFaixa(Map<String, Object> filtro) throws NegocioException {

		getCriteria();
		String id = null;
		ControladorFaixaConsumoVariacao controladorFaixaConsumoVariacao = (ControladorFaixaConsumoVariacao) ServiceLocator.getInstancia()
						.getBeanPorID(ControladorFaixaConsumoVariacao.BEAN_ID_CONTROLADOR_ACESSO);
		Collection<FaixaConsumoVariacao> lista = controladorFaixaConsumoVariacao.consultarTodosFaixaConsumoVariacao(null);

		StringBuilder ids = new StringBuilder();
		if(!lista.isEmpty()){
			for (FaixaConsumoVariacao faixa : lista) {
				ids.append(faixa.getSegmento().getChavePrimaria());
				ids.append(",");
			}
		}

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeSegmento().getSimpleName());
		hql.append(" where habilitado = true ");

		if(ids.length() > 0) {
			id = ids.toString().substring(0, ids.length() - 1);
			
			hql.append(" and chavePrimaria not in (");
			hql.append(id);
			hql.append(") ");
		}
		
		hql.append(" order by descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#criarTipoSegmento()
	 */
	@Override
	public EntidadeNegocio criarTipoSegmento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(TipoSegmento.BEAN_ID_TIPO_SEGMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#obterTipoSegmento(java.lang.Long)
	 */
	@Override
	public TipoSegmento obterTipoSegmento(Long chavePrimaria) throws NegocioException {

		return (TipoSegmento) this.obter(chavePrimaria, getClasseEntidadeTipoSegmento());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#obterSegmento(java.lang.Long)
	 */
	@Override
	public Segmento obterSegmento(Long chavePrimaria) throws NegocioException {

		return (Segmento) this.obter(chavePrimaria, SegmentoImpl.class, "indicadorEquipamentoPotenciaFixa");
	}

	public Class<?> getClasseEntidadeTipoSegmento() {

		return ServiceLocator.getInstancia().getClassPorID(TipoSegmento.BEAN_ID_TIPO_SEGMENTO);
	}

	public Class<?> getClasseEntidadeSegmento() {

		return ServiceLocator.getInstancia().getClassPorID(Segmento.BEAN_ID_SEGMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		validarSegmentoExistente((Segmento) entidadeNegocio);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		validarSegmentoExistente((Segmento) entidadeNegocio);
	}

	/**
	 * Validar segmento existente.
	 * 
	 * @param segmento
	 *            the segmento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private void validarSegmentoExistente(Segmento segmento) throws NegocioException {

		Criteria criteria = getCriteria();
		// FIXME: Utilizar upper na comparação de strings
		criteria.add(Restrictions.eq(TabelaAuxiliar.ATRIBUTO_DESCRICAO, segmento.getDescricao()));

		List<Segmento> listaSegmento = criteria.list();

		if(!listaSegmento.isEmpty()) {
			Segmento segmentoExistente = listaSegmento.get(0);

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(segmentoExistente);

			if(segmentoExistente.getChavePrimaria() != segmento.getChavePrimaria()) {
				throw new NegocioException(ControladorSegmento.ERRO_NEGOCIO_SEGMENTO_EXISTENTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#validarRemoverSegmentos(java.lang.Long[])
	 */
	@Override
	public void validarRemoverSegmentos(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#listarTodosTipoSegmento()
	 */
	@Override
	public Collection<TipoSegmento> listarTodosTipoSegmento() {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeTipoSegmento().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorSegmento
	 * #listarTodosTipoSegmento()
	 */

	@Override
	public Collection<Segmento> listarSegmento() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeSegmento().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#listarAmostragensPCSPorSegmento(br.com.ggas.cadastro.imovel.Segmento)
	 */
	@Override
	public Collection<EntidadeConteudo> listarAmostragensPCSPorSegmento(Segmento segmento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select amostragem.localAmostragemPCS from ");
		hql.append(getClasseEntidadeSegmentoAmostragemPCS().getSimpleName());
		hql.append(" amostragem ");
		hql.append(" where amostragem.segmento.chavePrimaria = :chaveSegmento ");
		hql.append(" order by amostragem.prioridade ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveSegmento", segmento.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#listarIntervalosPCSPorSegmento(br.com.ggas.cadastro.imovel.Segmento)
	 */
	@Override
	public Collection<IntervaloPCS> listarIntervalosPCSPorSegmento(Segmento segmento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select intervalo.intervaloPCS from ");
		hql.append(getClasseEntidadeSegmentoIntervaloPCS().getSimpleName());
		hql.append(" intervalo ");
		hql.append(" where intervalo.segmento.chavePrimaria = :chaveSegmento ");
		hql.append(" order by intervalo.prioridade ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveSegmento", segmento.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#atualizarSegmento(br.com.ggas.cadastro.imovel.Segmento,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void atualizarSegmento(Segmento segmento, DadosAuditoria dadosAuditoria) throws GGASException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" delete from ");
		hql.append(getClasseEntidadeSegmentoAmostragemPCS().getSimpleName());
		hql.append(" where segmento = ? ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, segmento.getChavePrimaria());
		query.executeUpdate();

		hql = new StringBuilder();
		hql.append(" delete from ");
		hql.append(getClasseEntidadeSegmentoIntervaloPCS().getSimpleName());
		hql.append(" where segmento = ? ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, segmento.getChavePrimaria());
		query.executeUpdate();
		
		removerSubsTribPorSegmento(segmento);

		if(segmento.getRamosAtividades() != null && !segmento.getRamosAtividades().isEmpty()) {
			for (RamoAtividade ramoAtividade : segmento.getRamosAtividades()) {
				if(ramoAtividade.getSegmento() == null) {
					ramoAtividade.setSegmento(segmento);
					ramoAtividade.setUltimaAlteracao(new Date());
					ramoAtividade.setHabilitado(true);
				}
				ramoAtividade.setDadosAuditoria(dadosAuditoria);
			}
		}

		if(segmento.getSegmentoAmostragemPCSs() != null && !segmento.getSegmentoAmostragemPCSs().isEmpty()) {
			for (SegmentoAmostragemPCS segmentoAmostragemPCS : segmento.getSegmentoAmostragemPCSs()) {
				segmentoAmostragemPCS.setUltimaAlteracao(new Date());
				segmentoAmostragemPCS.setHabilitado(true);
				segmentoAmostragemPCS.setDadosAuditoria(dadosAuditoria);
			}
		}

		if(segmento.getSegmentoIntervaloPCSs() != null && !segmento.getSegmentoIntervaloPCSs().isEmpty()) {
			for (SegmentoIntervaloPCS segmentoIntervaloPCS : segmento.getSegmentoIntervaloPCSs()) {
				segmentoIntervaloPCS.setUltimaAlteracao(new Date());
				segmentoIntervaloPCS.setHabilitado(true);
				segmentoIntervaloPCS.setDadosAuditoria(dadosAuditoria);
			}
		}

		this.atualizar(segmento);

	}

	/**
	 * Remove todas as substituições tributárias atreladas aos ramos de atividade
	 * pertencentes ao segmento passado como argumento. É uma abordagem provisória
	 * para superar o problema que ocorre quando o cliente remove uma substituição
	 * tributária e salva o segmento associado, a entrada não é removida da tabela
	 * RAMO_ATIVIDADE_SUBS_TRIB e o banco fica inconsistente.
	 *
	 * @param segmento the segmento
	 */
	private void removerSubsTribPorSegmento(Segmento segmento) {
		//FIXME o ideal é usar o mecanismo de remoção de órfãos do hibernate
		Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
		Criteria criteria = session.createCriteria(RamoAtividadeSubstituicaoTributaria.class, "rast")
				.createAlias("rast.ramoAtividade", "raat")
				.createAlias("raat.segmento", "segm")
				.add(Restrictions.eq("segm.chavePrimaria", segmento.getChavePrimaria()));
		
		Iterator<RamoAtividadeSubstituicaoTributaria> rastParaRemover = criteria.list().iterator();
		while(rastParaRemover.hasNext()) {
			RamoAtividadeSubstituicaoTributaria rast = rastParaRemover.next();
			session.delete(rast);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#inserirSegmentoRamoAtividade(br.com.ggas.cadastro.imovel.Segmento,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public long inserirSegmentoRamoAtividade(Segmento segmento, DadosAuditoria dadosAuditoria) throws NegocioException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorRamoAtividade.BEAN_ID_CONTROLADOR_RAMO_ATIVIDADE);

		segmento.getRamosAtividades();

		return this.inserir(segmento);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#validarSegmento(java.lang.Long)
	 */
	@Override
	public void validarSegmento(Long segmento) throws NegocioException {

		if(segmento == null || segmento == -1) {
			throw new NegocioException(ControladorSegmento.ERRO_TIPO_SEGMENTO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#listarSegmentoAmostragemPCS(java.lang.Long)
	 */
	@Override
	public Collection<SegmentoAmostragemPCS> listarSegmentoAmostragemPCS(Long segmento) throws NegocioException {

		Criteria criteria = this.createCriteria(SegmentoAmostragemPCS.class);
		criteria.createCriteria("localAmostragemPCS", "localAmostragemPCS", Criteria.LEFT_JOIN);
		criteria.add(Restrictions.eq("segmento.chavePrimaria", segmento));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#listarSegmentoIntervaloPCS(java.lang.Long)
	 */
	@Override
	public Collection<SegmentoIntervaloPCS> listarSegmentoIntervaloPCS(Long segmento) throws NegocioException {

		Criteria criteria = this.createCriteria(SegmentoIntervaloPCS.class);
		criteria.createCriteria("intervaloPCS", "intervaloPCS", Criteria.LEFT_JOIN);
		criteria.add(Restrictions.eq("segmento.chavePrimaria", segmento));

		return criteria.list();
	}

	/**
	 * Método responsavel por retornar
	 * a lista de AmostragemPCS disponível
	 * à um segmento.
	 * 
	 * @param segmento
	 *            the segmento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public Collection<EntidadeConteudo> listarSegmentoAmostragemPCSDisponiveis(Long segmento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeConteudo().getSimpleName());
		hql.append(" as enco ");
		hql.append(" inner join fetch enco.entidadeClasse encl ");
		hql.append(" where encl.descricao = 'Local de amostragem do PCS' ");
		hql.append("and enco.chavePrimaria not in(");
		hql.append(" select localAmostragemPCS.chavePrimaria from ");
		hql.append(getClasseEntidadeSegmentoAmostragemPCS().getSimpleName());
		hql.append(" where segmento.chavePrimaria = ?)");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, segmento);

		return query.list();
	}

	/**
	 * Método responsavel por retornar
	 * a lista de IntervalosPCS disponível
	 * à um segmento.
	 * 
	 * @param segmento
	 *            the segmento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public Collection<IntervaloPCS> listarSegmentoIntervaloPCSDisponiveis(Long segmento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseIntervaloPCS().getSimpleName());
		hql.append(" as pcin ");
		hql.append(" where pcin.chavePrimaria not in ( ");
		hql.append(" select intervaloPCS.chavePrimaria from ");
		hql.append(getClasseEntidadeSegmentoIntervaloPCS().getSimpleName());
		hql.append(" where segmento.chavePrimaria = ? ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, segmento);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#obterSegmentoAmostragemPCS(java.lang.Long)
	 */
	@Override
	public SegmentoAmostragemPCS obterSegmentoAmostragemPCS(Long chavePrimaria) throws NegocioException {

		Criteria criteria = this.createCriteria(SegmentoAmostragemPCS.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
		return (SegmentoAmostragemPCS) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#obterSegmentoIntervaloPCS(java.lang.Long)
	 */
	@Override
	public SegmentoIntervaloPCS obterSegmentoIntervaloPCS(Long chavePrimaria) throws NegocioException {

		Criteria criteria = this.createCriteria(SegmentoIntervaloPCS.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
		return (SegmentoIntervaloPCS) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#criarSegmentoAmostragemPCS()
	 */
	@Override
	public SegmentoAmostragemPCS criarSegmentoAmostragemPCS() {

		return (SegmentoAmostragemPCS) ServiceLocator.getInstancia().getBeanPorID(SegmentoAmostragemPCS.BEAN_ID_SEGMENTO_AMOSTRAGEM_PCS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#criarSegmentoIntervaloPCS()
	 */
	@Override
	public SegmentoIntervaloPCS criarSegmentoIntervaloPCS() {

		return (SegmentoIntervaloPCS) ServiceLocator.getInstancia().getBeanPorID(SegmentoIntervaloPCS.BEAN_ID_SEGMENTO_INTERVALO_PCS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#obterIntervaloPCSParaSegmento(java.lang.Long)
	 */
	@Override
	public IntervaloPCS obterIntervaloPCSParaSegmento(Long chavePrimaria) throws NegocioException {

		return (IntervaloPCS) this.obter(chavePrimaria, this.getClasseIntervaloPCS());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorSegmento#removerSegmentos(java.lang.Long[], br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerSegmentos(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		ControladorRamoAtividade controladorRamoAtividade = (ControladorRamoAtividade) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRamoAtividade.BEAN_ID_CONTROLADOR_RAMO_ATIVIDADE);

		for (Long chavePrimaria : chavesPrimarias) {
			Segmento segmento = (Segmento) obter(chavePrimaria);
			Long[] chavePrimarias = {chavePrimaria};
			Collection<RamoAtividade> listaRamoAtividade = controladorRamoAtividade.consultarRamoAtividadePorSegmentos(chavePrimarias);
			controladorRamoAtividade.removerRamoAtividade(listaRamoAtividade);
			this.remover(segmento);
		}
		
	}
	
	/**
	 * Obtém as chaves dos Segmentos dos pontos de consumo
	 * especificados no parâmetro.
	 * 
	 * @param pontosConsumo
	 * @return segmentos dos pontos de consumo
	 **/
	@Override
	public Long[] obterChavesDeSegmentosDePontosConsumo(Collection<PontoConsumo> pontosConsumo) {

		List<Long> novoArray = new ArrayList<>();
		for (PontoConsumo pontoConsumo : pontosConsumo) {
			Segmento segmento = pontoConsumo.getSegmento();
			if (!novoArray.contains(segmento.getChavePrimaria())) {
				novoArray.add(segmento.getChavePrimaria());
			}
		}
		Long[] chaves = new Long[novoArray.size()];
		novoArray.toArray(chaves);

		return chaves;
	}

}
