/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.dominio.EntidadeDominio;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Pair;
import br.com.ggas.web.faturamento.leitura.dto.ImovelDTO;
import br.com.ggas.web.faturamento.leitura.dto.PesquisaPontoConsumo;
import br.com.ggas.webservice.ImovelTO;

/**
 * Interface ControladorImovel
 *
 */
public interface ControladorImovel extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_IMOVEL = "controladorImovel";

	// Chaves para as mensagens de erro do imóvel
	String ERRO_NEGOCIO_EMAIL_CONTATO_EXISTENTE = "ERRO_NEGOCIO_EMAIL_CONTATO_EXISTENTE";

	String ERRO_NEGOCIO_EMAIL_INVALIDO = "ERRO_NEGOCIO_EMAIL_INVALIDO";

	String ERRO_NEGOCIO_ENDERECO_REMOTO_INVALIDO = "ERRO_NEGOCIO_ENDERECO_REMOTO_INVALIDO";

	String ERRO_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL_JA_EXISTENTE = "ERRO_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL_JA_EXISTENTE";
	
	String ERRO_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL_TIPO_JA_EXISTENTE = "ERRO_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL_TIPO_JA_EXISTENTE";

	String ERRO_SITUACAO_IMOVEL_POTENCIAL = "ERRO_SITUACAO_IMOVEL_POTENCIAL";

	String ERRO_SITUACAO_IMOVEL_FACTIVEL = "ERRO_SITUACAO_IMOVEL_FACTIVEL";

	String ERRO_IMOVEL_JA_EXISTENTE = "ERRO_IMOVEL_JA_EXISTENTE";

	String ERRO_EXCLUSAO_PONTO_CONSUMO = "ERRO_EXCLUSAO_PONTO_CONSUMO";

	String ERRO_ALTERACAO_IMOVEL_CONDOMINIO = "ERRO_ALTERACAO_IMOVEL_CONDOMINIO";

	String ERRO_NEGOCIO_IMOVEL_NAO_EXISTENTE = "ERRO_NEGOCIO_IMOVEL_NAO_EXISTENTE";

	String ERRO_NEGOCIO_IMOVEL_SEM_PONTO_CONSUMO = "ERRO_NEGOCIO_IMOVEL_SEM_PONTO_CONSUMO";

	String ERRO_NEGOCIO_QUANTIDADE_BLOCO_INVALIDA = "ERRO_NEGOCIO_QUANTIDADE_BLOCO_INVALIDA";

	String ERRO_NEGOCIO_APARTAMENTOS_REPETIDOS = "ERRO_NEGOCIO_APARTAMENTOS_REPETIDOS";

	String ERRO_NEGOCIO_BLOCO_JA_CADASTRADO = "ERRO_NEGOCIO_BLOCO_JA_CADASTRADO";

	String ERRO_NEGOCIO_QUANTIDADE_BLOCOS_EXCEDIDA = "ERRO_NEGOCIO_QUANTIDADE_BLOCOS_EXCEDIDA";

	String ERRO_NEGOCIO_NUMERO_BLOCO_INVALIDO = "ERRO_NEGOCIO_NUMERO_BLOCO_INVALIDO";

	String ERRO_NEGOCIO_INCREMENTO_MENOR = "ERRO_NEGOCIO_INCREMENTO_MENOR";

	String ERRO_NEGOCIO_CONTRATO_ATIVO = "ERRO_NEGOCIO_CONTRATO_ATIVO";

	String ERRO_NEGOCIO_PONTO_CONSUMO_ATIVO = "ERRO_NEGOCIO_PONTO_CONSUMO_ATIVO";

	String ERRO_NEGOCIO_ASSOCIADO_PONTO_CONSUMO = "ERRO_NEGOCIO_ASSOCIADO_PONTO_CONSUMO";

	String ERRO_DATA_RELACIONAMENTO_INICIO_INVALIDA = "ERRO_NEGOCIO_DATA_RELACIONAMENTO_INICIO_INVALIDA";

	String ERRO_DATA_RELACIONAMENTO_FIM_ANTERIOR_INICIO = "ERRO_NEGOCIO_DATA_RELACIONAMENTO_FIM_ANTERIOR_INICIO";

	String ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_ZERO = "ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_ZERO";

	String ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_TRIBUTO_DUPLICADO = "ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_TRIBUTO_DUPLICADO";

	String ERRO_PONTO_CONSUMO_TRIBUTO_PORCENTAGEM_MAXIMO_100 = "ERRO_PONTO_CONSUMO_TRIBUTO_PORCENTAGEM_MAXIMO_100";

	String ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_DATA_INICIO_VIGENCIA_MENOR_ATUAL = 
					"ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_DATA_INICIO_VIGENCIA_MENOR_ATUAL";

	String ERRO_IMOVEL_LOTE_BLOCO_IGUAL_ZERO = "ERRO_IMOVEL_LOTE_BLOCO_IGUAL_ZERO";

	String ERRO_NEGOCIO_NUMERO_LEITURA_REPETIDOS = "ERRO_NEGOCIO_NUMERO_LEITURA_REPETIDOS";

	String TRIBUTO_ATIVO = "TRIBUTO_ATIVO";

	String ERRO_NEGOCIO_REMOVER_SITUACAO_IMOVEL = "ERRO_NEGOCIO_REMOVER_SITUACAO_IMOVEL";

	String ERRO_NEGOCIO_REMOVER_PAVIMENTO_CALCADA = "ERRO_NEGOCIO_REMOVER_PAVIMENTO_CALCADA";

	String ERRO_NEGOCIO_REMOVER_PAVIMENTO_RUA = "ERRO_NEGOCIO_REMOVER_PAVIMENTO_RUA";

	String ERRO_NEGOCIO_REMOVER_PADRAO_CONSTRUCAO = "ERRO_NEGOCIO_REMOVER_PADRAO_CONSTRUCAO";

	String ERRO_NEGOCIO_REMOVER_TIPO_BOTIJAO = "ERRO_NEGOCIO_REMOVER_TIPO_BOTIJAO";

	String ERRO_NEGOCIO_REMOVER_PERFIL_IMOVEL = "ERRO_NEGOCIO_REMOVER_PERFIL_IMOVEL";
	
	String ERRO_PONTO_CONSUMO_EQUIPAMENTO_VAZA0_ZERO = "ERRO_PONTO_CONSUMO_TRIBUTO_EQUIPAMENTO";

	String ERRO_PONTO_CONSUMO_EQUIPAMENTO_HORAS_ZERO = "ERRO_PONTO_CONSUMO_EQUIPAMENTO_HORAS_ZERO";
	
	String ERRO_PONTO_CONSUMO_EQUIPAMENTO_HORAS_MAXIMAS_24 = "ERRO_PONTO_CONSUMO_EQUIPAMENTO_HORAS_MAXIMAS_24";
	
	String ERRO_PONTO_CONSUMO_EQUIPAMENTO_DIAS_0 = "ERRO_PONTO_CONSUMO_EQUIPAMENTO_DIAS_0";
	
	String ERRO_PONTO_CONSUMO_EQUIPAMENTO_DIAS_MAXIMOS_7= "ERRO_PONTO_CONSUMO_EQUIPAMENTO_DIAS_MAXIMOS_7";
	
	String ERRO_PONTO_CONSUMO_EQUIPAMENTO_DIFERENTES_SEGMENTOS = "ERRO_PONTO_CONSUMO_EQUIPAMENTO_DIFERENTES_SEGMENTOS";
	
	

	/**
	 * Criar atributo imovel.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarAtributoImovel();

	/**
	 * Criar rede interna imovel.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarRedeInternaImovel();

	/**
	 * Criar ponto consumo.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarPontoConsumo();

	/**
	 * Criar ponto consumo tributo aliquota.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarPontoConsumoTributoAliquota();
	
	/**
	 * Criar ponto consumo equipamento.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarPontoConsumoEquipamento();

	/**
	 * Criar modalidade aquecimento.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarModalidadeAquecimento();

	/**
	 * Criar cliente imovel.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarClienteImovel();

	/**
	 * Criar tipo relacionamento cliente imovel.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarTipoRelacionamentoClienteImovel();

	/**
	 * Criar motivo fim relacionamento cliente imovel.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarMotivoFimRelacionamentoClienteImovel();

	/**
	 * Criar contato imovel.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarContatoImovel();

	/**
	 * Criar situacao consumo.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarSituacaoConsumo();

	/**
	 * Criar padrao construcao.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarPadraoConstrucao();

	/**
	 * Criar pavimento rua.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarPavimentoRua();

	/**
	 * Criar pavimento calcada.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarPavimentoCalcada();

	/**
	 * Criar situacao imovel.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarSituacaoImovel();

	/**
	 * Criar perfil imovel.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarPerfilImovel();

	/**
	 * Criar tipo botijao.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarTipoBotijao();

	/**
	 * Criar grupo faturamento.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarGrupoFaturamento();

	/**
	 * Criar tipo leitura.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarTipoLeitura();

	/**
	 * Criar imovel ramo atividade.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarImovelRamoAtividade();

	/**
	 * Método responsável por consultar imoveis.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção paginada de imoveis.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Imovel> consultarImoveis(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se já
	 * existe um contato do imóvel com o mesmo
	 * email.
	 * 
	 * @param indexLista
	 *            the index lista
	 * @param listaContatoImovel
	 *            A lista contendo os contatos do
	 *            imóvel a serem verificados.
	 * @param contatoImovel
	 *            O contato do imóvel a ser
	 *            incluído.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void verificarEmailContatoExistente(Integer indexLista, Collection<ContatoImovel> listaContatoImovel, ContatoImovel contatoImovel)
					throws NegocioException;

	/**
	 * Método responsável por validar os dados do
	 * contato do imóvel.
	 * 
	 * @param contato
	 *            O contato do imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarDadosContatoImovel(ContatoImovel contato) throws NegocioException;

	/**
	 * Método responsável por listas as situações
	 * de imóvel.
	 * 
	 * @return coleção de situações de imóvel.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SituacaoImovel> listarImovelSituacao() throws NegocioException;

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * imóveis.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primárias dos imóveis.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverImoveis(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por listas os tipos de
	 * relacionamento entre cliente e
	 * imóvel.
	 * 
	 * @return coleção de tipos de relacionamento
	 *         entre cliente e imóvel.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<TipoRelacionamentoClienteImovel> listarTipoRelacionamentoClienteImovel() throws NegocioException;

	/**
	 * Método implementado para atender a regra [RN004]: Só deverá haver um cliente por tipo de relacionamento (usuário, proprietário,
	 * responsável) associado a um imóvel.
	 * 
	 * @param clienteImovel clienteImovel
	 * @param listaClienteImovel listaClienteImovel
	 * @param atualizar flag que indica atualização
	 * @throws NegocioException {@link NegocioException}
	 */
	void validarClientesRelacionados(ClienteImovel clienteImovel, Collection<ClienteImovel> listaClienteImovel, Boolean atualizar)
			throws NegocioException;

	/**
	 * Método responsável por listar os clientes
	 * que estão relacionados com um
	 * imóvel.
	 * 
	 * @param chavePrimariaImovel
	 *            Chave primária de um imóvel.
	 * @return coleção de relacionamentos entre
	 *         clientes e imóvel.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ClienteImovel> listarClienteImovelPorChaveImovel(Long chavePrimariaImovel) throws NegocioException;

	/**
	 * Método responsável por listar os clientes
	 * que estão relacionados com um
	 * imóvel que possuem endereço null ou não.
	 * 
	 * @param chavePrimariaImovel
	 *            Chave primária de um imóvel.
	 * @return coleção de relacionamentos entre
	 *         clientes e imóvel.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ClienteImovel> listarCliImovelPorChaveImovel(Long chavePrimariaImovel) throws NegocioException;

	/**
	 * Método responsável por listar os imóveis
	 * que estão relacionados com um cliente.
	 * 
	 * @param chavePrimariaCliente
	 *            the chave primaria cliente
	 * @param listarInativos
	 *            the listar inativos
	 * @return coleção de relacionamentos entre
	 *         clientes e imóvel.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ClienteImovel> listarClienteImovelPorChaveCliente(Long chavePrimariaCliente, boolean listarInativos) throws NegocioException;

	/**
	 * Método responsável por validar os dados do
	 * relacionamento de cliente com
	 * o imóvel.
	 * 
	 * @param clienteImovel
	 *            Um relacionamento de cliente com
	 *            o imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarDadosClienteImovel(ClienteImovel clienteImovel) throws NegocioException;

	/**
	 * Método responsável por obter o tipo de
	 * relacionamento de cliente com
	 * imóvel.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do tipo de
	 *            relacionamento de cliente com
	 *            imóvel.
	 * @return o tipo de relacionamento de cliente
	 *         com imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoRelacionamentoClienteImovel buscarTipoRelacionamentoClienteImovel(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por listar as unidades
	 * consumidoras.
	 * 
	 * @param chavePrimariaImovel
	 *            Chave primária de um imóvel.
	 * @return coleção de unidades consumidoras.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ImovelRamoAtividade> listarUnidadeConsumidoraPorChaveImovel(Long chavePrimariaImovel) throws NegocioException;

	/**
	 * Método responsável por listar os pontos de
	 * consumo pela chave do imóvel passado por
	 * parâmetro.
	 * 
	 * @param chavePrimariaImovel
	 *            Chave primária de um imóvel.
	 * @return coleção de pontos de consumo.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<PontoConsumo> listarPontoConsumoPorChaveImovel(Long chavePrimariaImovel) throws GGASException;

	/**
	 * Listar ponto consumo por chave imovel ativos inativos.
	 * 
	 * @param chavePrimariaImovel
	 *            the chave primaria imovel
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<PontoConsumo> listarPontoConsumoPorChaveImovelAtivosInativos(Long chavePrimariaImovel) throws NegocioException;

	/**
	 * Carregar ponto consumo por chave imovel.
	 * 
	 * @param chavePrimariaImovel
	 *            the chave primaria imovel
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<PontoConsumo> carregarPontoConsumoPorChaveImovel(Long chavePrimariaImovel, Boolean isPontoAtivo) throws GGASException;

	/**
	 * Retorna o ClienteImovel pela chavePrimaria
	 * passada por parãmetro.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return ClienteImovel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ClienteImovel obterClienteImovel(long chavePrimaria) throws NegocioException;

	/**
	 * Retorna a ModalidadeAquecimento pela
	 * chavePrimaria passada por parãmetro.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return ModalidadeAquecimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ModalidadeAquecimento obterModalidadeAquecimento(long chavePrimaria) throws NegocioException;

	/**
	 * Método implementado para atender o fluxo
	 * alternativo - FA1:
	 * Se nos subfluxos de inclusão ou de
	 * alteração, for informado um Imóvel com
	 * inscrição
	 * (localidade, setor, quadra, face, lote e
	 * sublote) já
	 * cadastrada, então: O sistema exibe a
	 * mensagem de Imóvel com inscrição já
	 * cadastrada.
	 * 
	 * @param imovel
	 *            the imovel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarImovelJaExistente(Imovel imovel) throws NegocioException;

	/**
	 * Método responsável por listar os pontos de
	 * consumo pela chave do imóvel passada pelo
	 * parâmetro.
	 * 
	 * @param chavePrimariaImovel
	 *            the chave primaria imovel
	 * @return lista com o ponto de consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<PontoConsumo> listarPontoConsumo(Long chavePrimariaImovel) throws NegocioException;

	/**
	 * Método responsável por validar os dados do
	 * ramo de atividade de imóvel.
	 * 
	 * @param chavePrimariaSegmento
	 *            Chave primária de segmento.
	 * @param chavePrimariaRamoAtividade
	 *            Chave primária de ramo de
	 *            atividade .
	 * @param quantidadeEconomia
	 *            Quantidade economia.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosImovelRamoAtividade(Long chavePrimariaSegmento, Long chavePrimariaRamoAtividade, Integer quantidadeEconomia)
					throws NegocioException;

	/**
	 * Método responsável por validar os dados do
	 * ponto de consumo de imóvel.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosPontoConsumo(PontoConsumo pontoConsumo) throws NegocioException;

	/**
	 * Método responsável por listas os pavimentos
	 * de calçada.
	 * 
	 * @return coleção de pavimentos de calçada.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<PavimentoCalcada> listarPavimentoCalcada() throws NegocioException;

	/**
	 * Método responsável por listas os pavimento
	 * de rua.
	 * 
	 * @return coleção de pavimentos de rua.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<PavimentoRua> listarPavimentoRua() throws NegocioException;

	/**
	 * Método responsável por listas os padrões de
	 * construção.
	 * 
	 * @return coleção de padrões de construção.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<PadraoConstrucao> listarPadraoConstrucao() throws NegocioException;

	/**
	 * Método responsável por listas os perfis de
	 * imóvel.
	 * 
	 * @return coleção de perfis de imóvel.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<PerfilImovel> listarPerfilImovel() throws NegocioException;

	/**
	 * Método responsável por listas os tipos de
	 * botijão.
	 * 
	 * @return coleção de tipos de botijão.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<TipoBotijao> listarTipoBotijao() throws NegocioException;

	/**
	 * Método responsável por listas os tipos de
	 * combustível.
	 * 
	 * @return coleção de tipos de combustível.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarTipoCombustivel() throws NegocioException;
	
	/**
	 * Método responsável por listas as
	 * modalidades uso.
	 * 
	 * @return coleção de modalidades uso.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarModalidadeUso() throws NegocioException;

	/**
	 * Método responsável por obter a situação de
	 * consumo pela descrição.
	 * 
	 * @param idSituacaoConsumo
	 *            the id situacao consumo
	 * @return SituacaoConsumo
	 */
	SituacaoConsumo obterSituacaoConsumo(long idSituacaoConsumo);

	/**
	 * Método responsável por criar uma entidade
	 * modalidade de medição de
	 * imóvel.
	 * 
	 * @return Uma EntidadeDominio de
	 *         ModalidadeMedicaoImovel.
	 */
	EntidadeDominio criarModalidadeMedicaoImovel();

	/**
	 * Método responsável por listas as
	 * modalidades de medição de imóvel.
	 * 
	 * @return coleção de modalidades de medição
	 *         de imóvel.
	 */
	Collection<ModalidadeMedicaoImovel> listarModalidadeMedicaoImovel();

	/**
	 * Método responsável por retornar o
	 * ModalidadeMedicaoImovel pela
	 * chavePrimaria passada por parâmetro.
	 * 
	 * @param codigo
	 *            o código da modalidade de
	 *            medição de imóvel.
	 * @return ModalidadeMedicaoImovel
	 */
	ModalidadeMedicaoImovel obterModalidadeMedicaoImovel(Integer codigo);

	/**
	 * Método responsável por retornar a situação
	 * de imóvel pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da situação de
	 *            imóvel.
	 * @return Uma situação de imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	SituacaoImovel obterSituacaoImovel(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por retornar o pavimento
	 * de calçada pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do pavimento de
	 *            calçada.
	 * @return Um pavimento de calçada.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	PavimentoCalcada obterPavimentoCalcada(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por retornar o pavimento
	 * de rua pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do pavimento de
	 *            rua .
	 * @return Um pavimento de rua .
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	PavimentoRua obterPavimentoRua(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por retornar o padrão de
	 * construção pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do padrão de
	 *            construção.
	 * @return Um padrão de construção.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	PadraoConstrucao obterPadraoConstrucao(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por retornar o perfil de
	 * imóvel pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do perfil de
	 *            imóvel.
	 * @return Um perfil de imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	PerfilImovel obterPerfilImovel(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por retornar o tipo de
	 * botijão pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do tipo de
	 *            botijão.
	 * @return Um tipo de botijão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoBotijao obterTipoBotijao(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por retornar o motivo de
	 * fim de relacionamento entre
	 * cliente e imóvel pela chavePrimaria passada
	 * por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do motivo de
	 *            fim de relacionamento
	 *            entre cliente e imóvel.
	 * @return Um motivo de fim de relacionamento
	 *         entre cliente e imóvel
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	MotivoFimRelacionamentoClienteImovel obterMotivoFimRelacionamentoClienteImovel(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por listar os contatos
	 * que estão relacionados com um imóvel.
	 * 
	 * @param chavePrimariaImovel
	 *            Chave primária de um imóvel.
	 * @return coleção de relacionamentos entre
	 *         clientes e imóvel.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ContatoImovel> listarContatoImovelPorChaveImovel(Long chavePrimariaImovel) throws NegocioException;

	/**
	 * Método responsável por listas os motivos de
	 * fim de relacionamento entre
	 * cliente e imóvel.
	 * 
	 * @return coleção de motivos de fim de
	 *         relacionamento entre cliente e
	 *         imóvel.
	 */
	Collection<MotivoFimRelacionamentoClienteImovel> listarMotivoFimRelacionamentoClienteImovel();

	/**
	 * Método que gera imóveis para serem
	 * preenchidos na tela de inclusão em lote de
	 * imóveis.
	 * 
	 * @param imovel
	 *            the imovel
	 * @param numeroBlocos
	 *            the numero blocos
	 * @param numeroAndares
	 *            the numero andares
	 * @param numeroAptAndar
	 *            the numero apt andar
	 * @return listaImoveis
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<Imovel> gerarImoveisEmLote(Imovel imovel, String numeroBlocos, String numeroAndares, String numeroAptAndar)
					throws NegocioException;

	/**
	 * Método para validar a inclusão em lote de
	 * imóveis.
	 * 
	 * @param imovel
	 *            the imovel
	 * @param apartamento
	 *            the apartamento
	 * @param bloco
	 *            the bloco
	 * @param numeroBlocos
	 *            the numero blocos
	 * @param tipoUnidade
	 *            the tipo unidade
	 * @param numeroSequenciaLeitura
	 *            the numero sequencia leitura
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarInclusaoImoveisEmLote(Imovel imovel, String[] apartamento, String[] bloco, String numeroBlocos, String tipoUnidade,
					String[] numeroSequenciaLeitura) throws NegocioException;

	/**
	 * Método que consulta os imóveis filhos do
	 * condominio passado pelo parâmetro.
	 * 
	 * @param imovel
	 *            the imovel
	 * @return ListaImoveis
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Imovel> obterImoveisFilhosDoCondominio(Imovel imovel) throws NegocioException;

	/**
	 * Método responsável por obter o ponto de
	 * consumo através do medidor.
	 * 
	 * @param numeroSerieMedidor
	 *            the numero serie medidor
	 * @return Um ponto de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	PontoConsumo obterPontoConsumoPorNumeroSerieMedidor(String numeroSerieMedidor) throws NegocioException;

	/**
	 * Método responsável por obter o ponto de
	 * consumo através do medidor.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return Um ponto de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	PontoConsumo obterPontoConsumoPorChaveMedidor(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por validar se um imóvel
	 * está cadastrado e ativo.
	 * 
	 * @param matricula
	 *            Matrícula do imóvel.
	 * @return true caso cadastrado e ativo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean existeImovelPorMatricula(Long matricula) throws NegocioException;

	/**
	 * Método reponsável por validar se o imovel
	 * possui uma lista de ponto de consumos.
	 * 
	 * @param listaPontosConsumo
	 *            A lista de pontos de consumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarImovelSemPontoConsumo(Collection<PontoConsumo> listaPontosConsumo) throws NegocioException;

	/**
	 * Método responsável por validar se imóvel
	 * pode carregar mais imóveis em lote.
	 * 
	 * @param imovel - {@link Imovel}
	 * @param numeroBlocos - {@link String}
	 * @param numeroAndares - {@link String}
	 * @param numeroAptAndar - {@link String}
	 * @param inicioApartamento - {@link String}
	 * @param incrementoApartamentos - {@link String}
	 * @param tipoUnidade - {@link String}
	 * @throws NegocioException aso ocorra algum erro na invocação do método. - {@link NegocioException}
	 */
	void validarCarregarImoveisEmLote(Imovel imovel, String numeroBlocos, String numeroAndares, String numeroAptAndar,
			String inicioApartamento, String incrementoApartamentos, String tipoUnidade) throws NegocioException;

	/**
	 * Método responsável por gerar o número dos
	 * apartamentos de um imóvel condomínio.
	 * 
	 * @param numeroBlocos
	 *            O número de blocos do imóvel.
	 * @param numeroAndares
	 *            O número de andares de cada
	 *            bloco do imóvel.
	 * @param numeroAptAndar
	 *            O número de apartamentos por
	 *            andar do imóvel.
	 * @param inicioApartamento
	 *            O número do primeiro apartamento
	 *            de cada bloco do imóvel.
	 * @param incrementoApartamento
	 *            O valor que é incrementado no
	 *            número dos apartamentos a cada
	 *            andar.
	 * @return Um vetor com o número dos
	 *         apartamentos de cada bloco do
	 *         imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<String> gerarApartamentosImoveisEmLote(String numeroBlocos, String numeroAndares, String numeroAptAndar, String inicioApartamento,
					String incrementoApartamento) throws NegocioException;

	/**
	 * Método responsável por replicar o nome dos
	 * blocos de um imóvel condomínio pelos
	 * apartamentos.
	 * 
	 * @param numeroBlocos
	 *            O número de blocos do imóvel.
	 * @param numeroAndares
	 *            O número de andares de cada
	 *            bloco do imóvel.
	 * @param numeroAptAndar
	 *            O número de apartamentos por
	 *            andar do imóvel.
	 * @param nomeBlocos
	 *            Os nomes que serão replicados
	 *            pelos apartamentos.
	 * @return Um vetor com os nomes dos blocos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<String> gerarBlocosImoveisEmLote(String numeroBlocos, String numeroAndares, String numeroAptAndar, String[] nomeBlocos)
					throws NegocioException;

	/**
	 * Método responsável por obter a quantidade
	 * de blocos cadastrados do imóvel.
	 * 
	 * @param imovel
	 *            the imovel
	 * @return A quantidade de blocos cadastrados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Integer obterQtdBlocosCadastrados(Imovel imovel) throws NegocioException;

	/**
	 * Método resposável por obter a quantidade de
	 * blocos que ainda podem ser cadastrados para
	 * o
	 * imóvel.
	 * 
	 * @param imovel
	 *            O imóvel condomínio
	 * @return A quantidade de blocos que ainda
	 *         podem ser cadastrados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Integer obterQtdBlocosRestantesImoveisEmLote(Imovel imovel) throws NegocioException;

	/**
	 * Metódo responsável por validar se é
	 * obrigatória a sequência de leitura na
	 * alteração do ponto de consumo.
	 * 
	 * @param chavePrimariaImovel
	 *            Chave primária do imovel
	 * @return true caso obrigatória.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean exigeSequenciaLeituraAlteracaoPontoConsumo(Long chavePrimariaImovel) throws NegocioException;

	/**
	 * Método responsável por listas as situações
	 * de imóvel para as
	 * funcionalidades de cadastro de imóvel.
	 * 
	 * @return coleção de situações de imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<SituacaoImovel> listarSituacaoImovelInterligado() throws NegocioException;

	/**
	 * Método responsável por consultar os ramos
	 * de atividades do imovel.
	 * 
	 * @param idImovel
	 *            O código do imovel
	 * @param idSegmento
	 *            O código do segmento
	 * @return Uma coleção de ImovelRamoAtividade
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ImovelRamoAtividade> consultarImovelRamoAtividadePorSegmento(Long idImovel, Long idSegmento) throws NegocioException;

	/**
	 * Método responsável por obter o cliente
	 * principal do imovel.
	 * 
	 * @param idImovel
	 *            A chave primária do imóvel
	 * @return Um Cliente Imovel
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	ClienteImovel obterClienteImovelPrincipal(Long idImovel) throws NegocioException;

	/**
	 * Método responsável por obter uma quadra a
	 * partir da chave primaria da quadraFace
	 * informada.
	 * 
	 * @param idQuadraFace
	 *            A chave primária da quadraFace
	 * @return Uma Quadra Face
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Quadra obterQuadraPorFaceQuadra(Long idQuadraFace) throws NegocioException;

	/**
	 * Método responsável por obter todos os
	 * pontos de consumos cadastrados no sistema.
	 * 
	 * @return Uma colecao de ponto consumo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<PontoConsumo> listarPontoConsumo() throws NegocioException;

	/**
	 * Método responsável por validar a remoção de
	 * um ponto de consumo de um imóvel.
	 * 
	 * @param chavePontoConsumo
	 *            A chave primária do ponto
	 *            consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemocaoPontoConsumoImovel(long chavePontoConsumo) throws NegocioException;

	/**
	 * Método responsável por validar as datas de
	 * um relacionamento entre Cliente e Imóvel.
	 * 
	 * @param dataRelacaoInicio
	 *            Data de início do
	 *            relacionamento.
	 * @param dataRelacaoFim
	 *            Data de fim do relacionamento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	void validarDatasAbaRelacionamento(Date dataRelacaoInicio, Date dataRelacaoFim) throws NegocioException;

	/**
	 * Metodo responsavel por consultar se existe
	 * algum imovel associado ao imovel consultado.
	 * 
	 * @param chavePrimariaImovelCondominio
	 *            A chave primaria do imovel
	 *            consultado
	 * @return a quantidade de imoveis associados
	 *         ao imovel consultado
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	int quantidadeImoveisFilhos(Long chavePrimariaImovelCondominio) throws NegocioException;

	/**
	 * Método responsável por validar o ponto de
	 * consumo tributo aliquota.
	 * 
	 * @param pontosConsumoTributoAliquota
	 *            Lista de pontos de consumo
	 *            tributo aliquota
	 * @param pontoConsumoTributoAliquota
	 *            Um ponto de consumo tributo
	 *            aliquota
	 * @param indexPontoConsumoTributoAliquota
	 *            the index ponto consumo tributo aliquota
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	void validarPontoConsumoTributoAliquota(Collection<PontoConsumoTributoAliquota> pontosConsumoTributoAliquota,
					PontoConsumoTributoAliquota pontoConsumoTributoAliquota, Integer indexPontoConsumoTributoAliquota)
					throws NegocioException;

	/**
	 * Método responsável por inserir um imóvel.
	 * 
	 * @param imovel
	 *            Um imóvel
	 * @return A chave do imóvel inserido
	 * @throws GGASException {@link GGASException}
	 *             Caso ocorra algum erro.
	 */
	Long inserirImovel(Imovel imovel) throws GGASException;

	/**
	 * Método responsável por inserir um imóvel.
	 * 
	 * @param imovel Um imóvel
	 * @param veioDoChamado {@link Boolean}
	 * @return A chave do imóvel inserido
	 * @throws GGASException Caso ocorra algum erro.
	 */
	Long inserirImovel(Imovel imovel, Boolean veioDoChamado) throws GGASException;

	/**
	 * Método responsável por inserir um ponto de consumo tributo aliquota.
	 * 
	 * @param pontoConsumoTributoAliquota Um ponto de consumo tributo aliquota a ser inserido.
	 * @throws NegocioException Caso ocorra algum erro.
	 */
	void inserirPontoConsumoTributoAliquota(PontoConsumoTributoAliquota pontoConsumoTributoAliquota) throws NegocioException;

	/**
	 * Método responsável por atualizar o ponto de
	 * consumo tributo aliquota.
	 * 
	 * @param pontoConsumoTributoAliquota
	 *            Um ponto de Consumo tributo
	 *            aliquota
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	void atualizarPontoConsumoTributoAliquota(PontoConsumoTributoAliquota pontoConsumoTributoAliquota) throws NegocioException;

	/**
	 * Método responsável por atualizar o imóvel.
	 * 
	 * @param imovel
	 *            Um imóvel
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	void atualizarImovel(Imovel imovel) throws NegocioException;

	/**
	 * Método responsável por consultar os pontos
	 * de consumo tributo aliquota através do
	 * filtro
	 * informado.
	 * 
	 * @param filtro
	 *            O filtro com os parametros da
	 *            consulta
	 * @return Uma lista de Ponto de consumo
	 *         tributo aliquota
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<PontoConsumoTributoAliquota> consultarPontosConsumoTributoAliquota(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por remover os imóveis.
	 * 
	 * @param chavesPrimarias
	 *            A chaves dos imóveis.
	 * @param dadosAuditoria
	 *            Dados da auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	void removerImovel(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Método responsável por obter a quadra por
	 * face da quadra.
	 * 
	 * @param idQuadraFace
	 *            Chave da quadra face
	 * @return Uma Quadra
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Quadra obterQuadraPorFaceQuadraPontoConsumo(Long idQuadraFace) throws NegocioException;

	/**
	 * Método responsável por gerar o número da
	 * leitura dos imóveis em lote.
	 * 
	 * @param qtdApartamentos
	 *            Quantidade de apartamento
	 * @return Uma lista de numeros
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	List<String> gerarNumeroLeituraImoveisEmLote(Integer qtdApartamentos) throws NegocioException;

	/**
	 * Validar excluir clientes relacionados.
	 * 
	 * @param clienteImovel
	 *            the cliente imovel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #validarExcluirClientesRelacionados
	 * (java.util.List)
	 */
	void validarExcluirClientesRelacionados(ClienteImovel clienteImovel) throws NegocioException;

	/**
	 * Obter cliente imovel por chave.
	 * 
	 * @param idImovel
	 *            the id imovel
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterClienteImovelPrincipal(java.lang
	 * .Long)
	 */
	Collection<ClienteImovel> obterClienteImovelPorChave(Long idImovel) throws NegocioException;

	/**
	 * Método responsável por obter o último
	 * Ano/Mês de Faturamento dos Grupos de
	 * Faturamento.
	 * 
	 * @return último Ano/Mês de Faturamento dos
	 *         Grupos de Faturamento
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Integer obterUltimoAnoMesFaturamento() throws NegocioException;

	/**
	 * Consultar imovel ramo atividade.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ImovelRamoAtividade> consultarImovelRamoAtividade(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar contato imovel.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ContatoImovel> consultarContatoImovel(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Verificar situcao imovel por rede indicador.
	 * 
	 * @param chaveQuadraFace
	 *            the chave quadra face
	 * @param selectChavePrimaria
	 *            the select chave primaria
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<SituacaoImovel> verificarSitucaoImovelPorRedeIndicador(String chaveQuadraFace, String selectChavePrimaria)
					throws GGASException;

	/**
	 * Validar endereco remoto.
	 * 
	 * @param enderecoRemotoPC
	 *            the endereco remoto pc
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarEnderecoRemoto(String enderecoRemotoPC) throws NegocioException;

	/**
	 * Verificar duplicidade endereco remoto.
	 * 
	 * @param indexLista
	 *            the index lista
	 * @param enderecoRemoto
	 *            the endereco remoto
	 * @param listaPontoConsumo
	 *            the lista ponto consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarDuplicidadeEnderecoRemoto(Integer indexLista, String enderecoRemoto, List<PontoConsumo> listaPontoConsumo)
					throws NegocioException;

	/**
	 * Alterar situacao imoveis factivel.
	 * 
	 * @param imoveis
	 *            the imoveis
	 */
	void alterarSituacaoImoveisFactivel(Collection<Imovel> imoveis);

	/**
	 * Atualizar colecao.
	 * 
	 * @param listaImoveis
	 *            the lista imoveis
	 * @param classe
	 *            the classe
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void atualizarColecao(Collection<Imovel> listaImoveis, Class<Imovel> classe) throws ConcorrenciaException, NegocioException;

	/**
	 * Validar camapos imovel em lote.
	 * 
	 * @param apartamento
	 *            the apartamento
	 * @param bloco
	 *            the bloco
	 * @param quantidadeBanheiro
	 *            the quantidade banheiro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarCamaposImovelEmLote(String[] apartamento, String[] bloco, String[] quantidadeBanheiro) throws NegocioException;

	/**
	 * Listar imovel por chave rota.
	 * 
	 * @param chavePrimariaRota
	 *            the chave primaria rota
	 * @return the collection
	 */
	Collection<Imovel> listarImovelPorChaveRota(long chavePrimariaRota);

	/**
	 * Listar ponto consumo sem rota.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param chavesPontoConsumo
	 *            the chaves ponto consumo
	 * @return the collection
	 */
	Collection<PontoConsumo> listarPontoConsumoSemRota(Map<String, Object> filtro, List<Long> chavesPontoConsumo);
	
	/**
	 * Obter uma lista de imóveis incluindo os pontos de consumo filhos.
	 * @param pesquisa O filtro que será aplicado na busca dos imóveis.
	 * @return Um par representando a lista dos imóveis encontrados e o total de resultados obtidos.
	 */
	Pair<List<ImovelDTO>, Long> listarImovelComPontosDeConsumoFilho(PesquisaPontoConsumo pesquisa);

	/**
	 * Obter uma lista de imóveis de uma rota incluindo os pontos de consumo filhos.
	 * Esse método considera informações de contrato e instalação de medidor para obter o resultado.
	 * @param pesquisa O filtro que será aplicado na busca dos imóveis.
	 * @return Um par representando a lista dos imóveis encontrados e o total de resultados obtidos.
	 */
	Pair<List<ImovelDTO>, Long> listarImovelRotaComPontosDeConsumoFilho(PesquisaPontoConsumo pesquisa);

	/**
	 * Atualizar logradouro imovel.
	 * 
	 * @param listaImovel
	 *            the lista imovel
	 * @param dadosAuditoria {@link DadosAuditoria}
	 * @throws GGASException {@link GGASException}
	 */
	void atualizarLogradouroImovel(Collection<Imovel> listaImovel, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Alterar situacao imovel por indicador rede.
	 * 
	 * @param listaImovel
	 *            the lista imovel
	 * @param novaQuadraFace
	 *            the nova quadra face
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void alterarSituacaoImovelPorIndicadorRede(Collection<Imovel> listaImovel, QuadraFace novaQuadraFace) throws NegocioException;

	/**
	 * Listar nome imoveis.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @return the collection
	 */
	Collection<String> listarNomeImoveis(Long[] chavesPrimarias);

	/**
	 * Listar imoveis em prospeccao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param idFuncionario
	 *            the id funcionario
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Imovel> listarImoveisEmProspeccao(Map<String, Object> filtro, Long idFuncionario) throws NegocioException;

	/**
	 * obtém a situação do imóvel pela descrição.
	 * 
	 * @param descricao
	 *            the descricao
	 * @return {@link SituacaoImovel}
	 */
	SituacaoImovel obterSituacaoImovelPorDescricao(String descricao);

	/**
	 * Usado na conversão do imovelTO para Imovel.
	 * 
	 * @param imovelTO
	 *            the imovel to
	 * @return imovel
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Imovel converterImovelTOEmImovel(ImovelTO imovelTO) throws GGASException;

	/**
	 * Usado na conversão de Imovel para ImovelTO.
	 * 
	 * @param imovel
	 *            the imovel
	 * @return {@link ImovelTO}
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public ImovelTO converterImovelEmImovelTO(Imovel imovel) throws GGASException;

	/**
	 * Obter rede inter do imovel.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return {@link RedeInternaImovel}
	 */
	RedeInternaImovel obterRedeInterDoImovel(Long chavePrimaria);
	
	
	/**
	 * Método responsável por validar o ponto de consumo equipamento.
	 * 
	 * @param listPontosConsumoEquipamento Lista de pontos de consumo equipamento
	 * @param pontoConsumoEquipamento Um ponto de consumo equipamento
	 * @param indexPontoConsumoEquipamento the index ponto consumo equipamento
	 * @throws NegocioException Caso ocorra algum erro.
	 */
	public void validarPontoConsumoEquipamento(Collection<PontoConsumoEquipamento> listPontosConsumoEquipamento,
			PontoConsumoEquipamento pontoConsumoEquipamento, Integer indexPontoConsumoEquipamento)
					throws NegocioException;

	
	/**
	 * Método responsável por validar se o ponto de
	 * consumo e o equipamento adicionado são do mesmo segmento.
	 * 
	 * @param pontoConsumo
	 *            Um ponto de consumo
	 * @param pontoConsumoEquipamento
	 *            Um ponto de consumo equipamento
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	public void validarPontoConsumoEEquipamento(PontoConsumo pontoConsumo, PontoConsumoEquipamento pontoConsumoEquipamento)
			throws NegocioException;

	/**
	 * Método responsável por retornar o contato do imovel pela chavePrimaria passada por parâmetro.
	 * 
	 * @param chavePrimaria A chave primária do contato do imovel.
	 * @return Um contato do imovel.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	ContatoImovel obterContatoImovel(long chavePrimaria) throws NegocioException;

	/**
	 * consultar Servico Tipo Ordenados
	 * 
	 * @return servicoTipo collection
	 * @throws NegocioException
	 */
	Collection<ServicoTipo> consultarServicoTipoOrdenados();

	/**
	 * @return ImovelServicoTipoRestricao
	 */
	EntidadeNegocio criarImovelServicoTipoRestricao();

	/**
	 * Classe consultar Servico Tipo
	 *
	 * @param chavePrimaria - {@link Long}
	 * @return ServicoTipo - {@link ServicoTipo}
	 */
	ServicoTipo consultarServicoTipo(Long chavePrimaria);

	/**	
	 * consultarImovelServicoTipoRestricaoPorImovel
	 * 
     * @param chavePrimariaImovel - {@link Long}
	 * @return lista de ImovelServicoTipoRestricao - {@link Collection}
	 */
	Collection<ImovelServicoTipoRestricao> consultarImovelServicoTipoRestricaoPorImovel(Long chavePrimariaImovel);

	Collection<Imovel> consultarImovelCondominio(String descricaoCondominio, Integer offset);

}
