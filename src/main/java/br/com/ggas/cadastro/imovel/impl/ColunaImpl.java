/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Map;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

class ColunaImpl extends EntidadeNegocioImpl implements Coluna {

	private static final long serialVersionUID = 7050658896945321531L;

	private String nome;

	private String nomePropriedade;

	private String descricao;

	private Boolean consultaDinamica;

	private String atributosConsultaDinamica;

	private Tabela tabela;

	private Boolean auditavel;

	private Boolean alcada;

	private Boolean variavel;

	private String codigoVariavel;

	@Override
	public String getNome() {

		return nome;
	}

	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	@Override
	public String getNomePropriedade() {

		return nomePropriedade;
	}

	@Override
	public void setNomePropriedade(String nomePropriedade) {

		this.nomePropriedade = nomePropriedade;
	}

	@Override
	public String getDescricao() {

		return descricao;
	}

	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	@Override
	public Boolean getConsultaDinamica() {

		return consultaDinamica;
	}

	@Override
	public void setConsultaDinamica(Boolean consultaDinamica) {

		this.consultaDinamica = consultaDinamica;
	}

	@Override
	public String getAtributosConsultaDinamica() {

		return atributosConsultaDinamica;
	}

	@Override
	public void setAtributosConsultaDinamica(String atributosConsultaDinamica) {

		this.atributosConsultaDinamica = atributosConsultaDinamica;
	}

	@Override
	public Tabela getTabela() {

		return tabela;
	}

	@Override
	public void setTabela(Tabela tabela) {

		this.tabela = tabela;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ColunaTabela
	 * #isAuditavel()
	 */
	@Override
	public Boolean getAuditavel() {

		return auditavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ColunaTabela
	 * #setAuditavel(java.lang.Boolean)
	 */
	@Override
	public void setAuditavel(Boolean auditavel) {

		this.auditavel = auditavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Coluna#isAlcada
	 * ()
	 */
	@Override
	public Boolean isAlcada() {

		return alcada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Coluna#setAlcada
	 * (java.lang.Boolean)
	 */
	@Override
	public void setAlcada(Boolean alcada) {

		this.alcada = alcada;
	}

	@Override
	public Boolean getVariavel() {

		return variavel;
	}

	@Override
	public void setVariavel(Boolean variavel) {

		this.variavel = variavel;
	}

	@Override
	public String getCodigoVariavel() {

		return codigoVariavel;
	}

	@Override
	public void setCodigoVariavel(String codigoVariavel) {

		this.codigoVariavel = codigoVariavel;
	}
}
