
package br.com.ggas.cadastro.imovel.impl;

import java.math.BigDecimal;
/**
 * 
 * Classe responsável pelos métodos 
 * relacionados a representação
 * de acompanhamento do cronograma de
 * ponto de consumo
 *
 */
public class PontoConsumoAcompanhamentoCronogramaVO {

	private String descricaoSegmento;

	private String codigoPontoConsumo;

	private String descricaoPontoConsumo;

	private String situacao;

	private BigDecimal consumo;

	public String getDescricaoSegmento() {

		return descricaoSegmento;
	}

	public void setDescricaoSegmento(String descricaoSegmento) {

		this.descricaoSegmento = descricaoSegmento;
	}

	public String getCodigoPontoConsumo() {

		return codigoPontoConsumo;
	}

	public void setCodigoPontoConsumo(String codigoPontoConsumo) {

		this.codigoPontoConsumo = codigoPontoConsumo;
	}

	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getSituacao() {

		return situacao;
	}

	public void setSituacao(String situacaoProcessado) {

		this.situacao = situacaoProcessado;
	}

	public BigDecimal getConsumo() {

		return consumo;
	}

	public void setConsumo(BigDecimal consumo) {

		this.consumo = consumo;
	}

}
