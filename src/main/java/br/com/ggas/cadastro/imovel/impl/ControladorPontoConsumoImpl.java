/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.cadastro.imovel.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.log4j.Logger;
import org.displaytag.properties.SortOrderEnum;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoEstendida;
import br.com.ggas.batch.logprocesso.negocio.ControladorLogProcesso;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorPrevisaoCaptacao;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.localidade.RedeDistribuicaoTronco;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoQDC;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.faturamento.ControladorFaturamentoAnormalidade;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaImpressao;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.faturamento.anomalia.ControladorHistoricoAnomaliaFaturamento;
import br.com.ggas.faturamento.anomalia.HistoricoAnomaliaFaturamento;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.DadosResumoFatura;
import br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.ImovelPCSZ;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.leitura.impl.InstalacaoMedidorImpl;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.rota.impl.RotaImpl;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Constantes.TipoLeitura;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.OrdenacaoEspecial;
import br.com.ggas.util.Pair;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.medicao.rota.PontoConsumoRotaVO;
import br.com.ggas.web.relatorio.faturamento.PesquisaRelatorioComercialMensalVO;
import br.com.ggas.web.relatorio.faturamento.PesquisaRelatorioPrevisaoCaptacaoVO;
import br.com.ggas.web.relatorio.faturamento.RelatorioComercialMensalVO;
import br.com.ggas.web.relatorio.faturamento.RelatorioComercialMensalWrapper;
import br.com.ggas.web.relatorio.faturamento.RelatorioPrevisaoCaptacaoVO;
import br.com.ggas.web.relatorio.faturamento.RelatorioPrevisaoCaptacaoWrapper;
import br.com.ggas.webservice.PontoConsumoTO;
import br.com.ggas.webservice.TipoComboBoxTO;
/**
 * Classe responsável por implementar os métodos relacionados
 * ao Controlador do Ponto de Consumo.
 *
 * SupressWarnings: S1192 Duplicated string literals: Foi Suprimido devido ao fato das strings
 * serem utilizadas para representar o nome das colunas das entidades. A adição de constantes neste caso
 * iria poluir o código.
 */
@SuppressWarnings("squid:S1192")
class ControladorPontoConsumoImpl extends ControladorNegocioImpl implements ControladorPontoConsumo {

	private static final int TAMANHO_MAXIMO_LISTA = 1000;

	private static final String ID_CONTRATO = "idContrato";

	private static final int TAMANHO_CPF = 11;

	private static final String GRUPO_FATURAMENTO = "grupoFaturamento";

	private static final int INDICE_NUMERO_UDA = 8;

	private static final int DEZEMBRO = 12;

	private static final int NOVEMBRO = 11;

	private static final int OUTUBRO = 10;

	private static final int SETEMBRO = 9;

	private static final int AGOSTO = 8;

	private static final int JULHO = 7;

	private static final int JUNHO = 6;

	private static final int MAIO = 5;

	private static final int ABRIL = 4;

	private static final int MARCO = 3;

	private static final int FEVEREIRO = 2;
	
	private static final int PARAMETRO_NUMERO_CICLO = 2;

	private static final int INDICE_IDENTIFICACAO_MES = 3;

	private static final int INDICE_QTD_CAPTACAO = 2;

	private static final int INDICE_MES = 4;

	private static final int INDICE_CONSUMO_APURADO = 5;

	private static final int INDICE_DATA = 4;

	private static final int INDICE_DATA_INICIO = 3;

	private static final int INDICE_CONTRATO_PONTO_CONSUMO = 2;

	private static final int INDICE_VALOR = 7;

	private static final int INDICE_CONSUMO_ANTERIOR = 6;

	private static final int INDICE_CONSUMO = 5;

	private static final int ESCALA_DOIS = 2;

	private static final int CONSTANTE_DOIS = 2;

	private static final int INDICE_RAMO_SEGMENTO = 3;

	private static final int INDICE_RAMO_ATIVIDADE = 4;

	private static final int INDICE_IMOVEL_NOME = 6;

	private static final int INDICE_IMOVEL_CHAVE_PRIMARIA = 5;

	private static final int INDICE_ROTA = 2;

	private static final int INDICE_CHAVE_ROTA = 4;

	private static final int INDICE_INSTALACAO_MEDIDOR = 3;

	private static final double PORCENTAGEM = 100.00;

	private static final int INDICE_SITUACAO = 2;

	private static final Logger LOG = Logger.getLogger(ControladorPontoConsumoImpl.class);

	private static final String CAMPO_FILTRO_MATRICULA = "matricula";

	private static final String ID_ROTA = "idRota";

	private static final String ID_SEGMENTO = "idSegmento"; //NOSONAR desnecessário criação de constante.

	private static final String IMOVEL_CHAVE_PRIMARIA = "imovel.chavePrimaria";

	private static final String IMOVEL_NUMERO_SEQUENCIA_LEITURA = "imovel.numeroSequenciaLeitura";

	public static final String RELATORIO_PREVISAO_CAPTACAO = "relatorioPrevisaoCaptacao.jasper";

	public static final String RELATORIO_COMERCIAL_MENSAL = "relatorioComercialMensal.jasper";

	public static final String RELATORIO_RESIDENCIAL_MENSAL = "relatorioResidencialMensal.jasper";

	public static final String WHERE = " where ";

	public static final String FROM = " from ";

	public static final String PONTO = " ponto ";

	public static final String ORDER_BY_NUMERO_SEQUENCIAL =" order by ponto.numeroSequenciaLeitura ";

	private static final int MAX_IN = 900;

	private final ControladorLogProcesso controladorLogProcesso = (ControladorLogProcesso) ServiceLocator.getInstancia()
			.getControladorNegocio(ControladorLogProcesso.BEAN_ID_CONTROLADOR_LOG_PROCESSO);

	@Autowired
	private ControladorPrevisaoCaptacao controladorPrevisaoCaptacao;
	
	@Autowired
	private ControladorLeituraMovimento controladorLeituraMovimento;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeAnormalidadeConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(AnormalidadeConsumo.BEAN_ID_ANORMALIDADE_CONSUMO);
	}

	public Class<?> getClasseEntidadeFaturaImpressao() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaImpressao.BEAN_ID_FATURA_IMPRESSAO);
	}

	public Class<?> getClasseEntidadeFaturaItem() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaItem.BEAN_ID_FATURA_ITEM);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeLeituraMovimento() {

		return ServiceLocator.getInstancia().getClassPorID(LeituraMovimento.BEAN_ID_LEITURA_MOVIMENTO);
	}

	public Class<?> getClasseEntidadeFaturamentoAnormalidade() {

		return ServiceLocator.getInstancia().getClassPorID(FaturamentoAnormalidade.BEAN_ID_FATURAMENTO_ANORMALIDADE);
	}

	public Class<?> getClasseEntidadeHistoricoAnomaliaFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoAnomaliaFaturamento.BEAN_ID_HISTORICO_ANOMALIA_FATURAMENTO);
	}

	public Class<?> getClasseEntidadeSupervisorioMedicaoDiaria() {

		return ServiceLocator.getInstancia().getClassPorID(SupervisorioMedicaoDiaria.BEAN_ID_SUPERVISORIO_MEDICAO_DIARIA);
	}

	public Class<?> getClasseEntidadeImovelPCSZ() {

		return ServiceLocator.getInstancia().getClassPorID(ImovelPCSZ.BEAN_ID_IMOVEL_PCS_Z);
	}

	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeRota() {

		return ServiceLocator.getInstancia().getClassPorID(Rota.BEAN_ID_ROTA);
	}

	public Class<?> getClasseEntidadeSituacaoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(SituacaoConsumo.BEAN_ID_SITUACAO_CONSUMO);
	}

	public Class<?> getClasseEntidadeHistoricoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeContratoCliente() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoCliente.BEAN_ID_CONTRATO_CLIENTE);
	}

	public Class<?> getClasseEntidadeCronogramaAtividadeFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(CronogramaAtividadeFaturamento.BEAN_ID_CRONOGRAMA_ATIVIDADE_FATURAMENTO);
	}

	public Class<?> getClasseEntidadeCronogramaRota() {

		return ServiceLocator.getInstancia().getClassPorID(CronogramaRota.BEAN_ID_CRONOGRAMA_ROTA);
	}

	public Class<?> getClasseEntidadeHistoricoMedicao() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoMedicao.BEAN_ID_MEDICAO_HISTORICO);
	}

	public Class<?> getClasseEntidadeImovel() {

		return ServiceLocator.getInstancia().getClassPorID(Imovel.BEAN_ID_IMOVEL);
	}

	public Class<?> getClasseEntidadeInstalacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(InstalacaoMedidor.BEAN_ID_INSTALACAO_MEDIDOR);
	}

	private ControladorMedidor getControladorMedidor() {

		return (ControladorMedidor) ServiceLocator.getInstancia().getBeanPorID(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);
	}

	private ControladorConstanteSistema getControladorConstanteSistema() {

		return (ControladorConstanteSistema) ServiceLocator.getInstancia().getBeanPorID(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
	}

	private ControladorFatura getControladorFatura() {

		return (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #criarPontoConsumoCityGate()
	 */
	@Override
	public EntidadeNegocio criarPontoConsumoCityGate() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PontoConsumoCityGate.BEAN_ID_PONTO_CONSUMO_CITY_GATE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#criarSituacaoConsumo()
	 */
	@Override
	public EntidadeNegocio criarSituacaoConsumo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(SituacaoConsumo.BEAN_ID_SITUACAO_CONSUMO);
	}

	public Class<?> getClasseEntidadePontoConsumoCityGate() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumoCityGate.BEAN_ID_PONTO_CONSUMO_CITY_GATE);

	}

	public Class<?> getClasseEntidadeHistoricoOperacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoOperacaoMedidor.BEAN_ID_HISTORICO_OPERACAO_MEDIDOR);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #obterPontosConsumoCityGate(java
	 * .lang.Long)
	 */
	@Override
	public Collection<PontoConsumoCityGate> obterPontoConsumoCityGate(Long idPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumoCityGate");
		hql.append(FROM);
		hql.append(getClasseEntidadePontoConsumoCityGate().getSimpleName());
		hql.append(" pontoConsumoCityGate ");
		hql.append(" inner join fetch pontoConsumoCityGate.pontoConsumo ");
		hql.append(" inner join fetch pontoConsumoCityGate.cityGate ");
		hql.append(WHERE);
		hql.append(" pontoConsumoCityGate.pontoConsumo.chavePrimaria = ? ");
		hql.append(" order by ");
		hql.append(" pontoConsumoCityGate.dataVigencia");
		hql.append(" desc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idPontoConsumo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel
	 * .ControladorPontoConsumo
	 * #obterPontoConsumoCityGateOrdenadoPorCityGate(
	 * java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumoCityGate> obterPontoConsumoCityGateOrdenadoPorCityGate(Long idPontoConsumo, Long idCityGate)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumoCityGate from ");
		hql.append(getClasseEntidadePontoConsumoCityGate().getSimpleName());
		hql.append(" pontoConsumoCityGate ");
		hql.append(" inner join fetch pontoConsumoCityGate.pontoConsumo ");
		hql.append(" inner join fetch pontoConsumoCityGate.cityGate ");
		hql.append(" where pontoConsumoCityGate.pontoConsumo.chavePrimaria = :idPontoConsumo ");

		if (idCityGate != null && idCityGate > 0) {
			hql.append(" and exists ( ");
			hql.append(" select pontoConsumoCityGate2 from ");
			hql.append(getClasseEntidadePontoConsumoCityGate().getSimpleName());
			hql.append(" pontoConsumoCityGate2 ");
			hql.append(" inner join pontoConsumoCityGate2.pontoConsumo pontoConsumo2 ");
			hql.append(" inner join pontoConsumoCityGate2.cityGate cityGate2 ");
			hql.append(" where pontoConsumo2.chavePrimaria = :idPontoConsumo ");
			hql.append(" and cityGate2.chavePrimaria = :idCityGate ");
			hql.append(" ) ");
		}

		hql.append(" order by ");
		hql.append(" pontoConsumoCityGate.cityGate.chavePrimaria asc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo); // NOSONAR: A regra 'String literals should not be duplicated' não se aplica nesse
															// caso

		if (idCityGate != null && idCityGate > 0) {
			query.setLong("idCityGate", idCityGate);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #listarPontosConsumo(java.lang
	 * .String[])
	 */
	@Override
	public Collection<PontoConsumo> listarPontosConsumo(String[] chavesPrimarias) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" chavePrimaria in (:chaves)");
		hql.append(" and habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("chaves", Util.arrayStringParaArrayLong(chavesPrimarias)); // NOSONAR: A regra 'String literals should not be
																							// duplicated' não se aplica nesse caso
		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #listarPontosConsumoPorRota(java
	 * .lang.Long,
	 * br.com.ggas.faturamento.cronograma
	 * .CronogramaAtividadeFaturamento)
	 */
	@Override
	public Collection<PontoConsumo> listarPontosConsumoPorRotaLeituraMovimento(Long chaveRota,
					CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo "); // NOSONAR: A regra 'String literals should not be duplicated' não se aplica nesse caso
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo, ");
		hql.append(getClasseEntidadeRota().getSimpleName());
		hql.append(" rota, ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append(" cronogramaAtividadeFaturamento ");
		hql.append(WHERE);
		hql.append(" rota = pontoConsumo.rota ");
		hql.append(" and rota.grupoFaturamento.chavePrimaria = "
						+ "cronogramaAtividadeFaturamento.cronogramaFaturamento.grupoFaturamento.chavePrimaria ");
		hql.append(" and cronogramaAtividadeFaturamento.chavePrimaria = :idAtividadeCronograma ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = true "); // NOSONAR: A regra 'String literals should not be
																					// duplicated' não se aplica nesse caso
		hql.append(" and rota.chavePrimaria = :chave ");
		hql.append(" and pontoConsumo.chavePrimaria not in  ");
		hql.append(" (select leituraMovimento.pontoConsumo.chavePrimaria ");
		hql.append(FROM);
		hql.append(getClasseEntidadeLeituraMovimento().getSimpleName());
		hql.append(" leituraMovimento ");
		hql.append(WHERE);
		hql.append(" leituraMovimento.rota.chavePrimaria = rota.chavePrimaria ");
		hql.append(" and leituraMovimento.anoMesFaturamento = cronogramaAtividadeFaturamento.cronogramaFaturamento.anoMesFaturamento ");
		hql.append(" and leituraMovimento.ciclo = cronogramaAtividadeFaturamento.cronogramaFaturamento.numeroCiclo ");
		hql.append(" and leituraMovimento.situacaoLeitura.chavePrimaria in (:situacaoLeitura) )");

		hql.append(" and pontoConsumo.chavePrimaria in  ");
		hql.append(" (select leituraMovimento.pontoConsumo.chavePrimaria ");
		hql.append(FROM);
		hql.append(getClasseEntidadeLeituraMovimento().getSimpleName());
		hql.append(" leituraMovimento ");
		hql.append(WHERE);
		hql.append(" leituraMovimento.rota.chavePrimaria = rota.chavePrimaria ");
		hql.append(" and leituraMovimento.anoMesFaturamento = cronogramaAtividadeFaturamento.cronogramaFaturamento.anoMesFaturamento ");
		hql.append(" and leituraMovimento.ciclo = cronogramaAtividadeFaturamento.cronogramaFaturamento.numeroCiclo ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chave", chaveRota);
		query.setLong("idAtividadeCronograma", cronogramaAtividadeFaturamento.getChavePrimaria());
		Long[] situacao = new Long[INDICE_SITUACAO];
		situacao[0] = SituacaoLeituraMovimento.PROCESSADO;
		situacao[1] = SituacaoLeituraMovimento.LEITURA_RETORNADA;
		query.setParameterList("situacaoLeitura", situacao);

		return query.list();

	}

	/**
	 * Listar situacoes ponto consumo.
	 *
	 * @return Collection de Situações de Ponto de
	 *         Consumo habilitadas
	 */
	@Override
	public Collection<SituacaoConsumo> listarSituacoesPontoConsumo() {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeSituacaoPontoConsumo().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #listarPontosConsumoEmAlerta()
	 */
	@Override
	public Collection<PontoConsumo> listarPontosConsumoEmAlerta() {

		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo from "); // NOSONAR: A regra 'String literals should not be duplicated' não se aplica nesse caso
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel imovel "); // NOSONAR: A regra 'String literals should not be duplicated' não se
																		// aplica nesse caso
		hql.append(" inner join fetch imovel.quadraFace quadraFace ");
		hql.append(WHERE);
		hql.append(" pontoConsumo.numeroSequenciaLeitura is null AND ");
		hql.append(" pontoConsumo.habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo#
	 * consultarPontosConsumoEmAbertoLeitura(java.util
	 * .Map)
	 * ebandeira 12/10/2009
	 */
	@Override
	public Collection<PontoConsumo> consultarPontosConsumoEmAbertoLeitura(Map<String, Object> filtro, AtividadeSistema atividadeSistema)
					throws NegocioException {

		Class entidadeCronogramaRota = ServiceLocator.getInstancia().getClassPorID(CronogramaRota.BEAN_ID_CRONOGRAMA_ROTA);
		Class entidadeLeituraMovimento = ServiceLocator.getInstancia().getClassPorID(LeituraMovimento.BEAN_ID_LEITURA_MOVIMENTO);
		Query query;

		boolean filtrosSelecionados = false;
		if (filtro != null && filtro.get(ID_ROTA) != null && filtro.get(CAMPO_FILTRO_MATRICULA) != null
				&& (Long) filtro.get(ID_ROTA) > 0 && (Long) filtro.get(CAMPO_FILTRO_MATRICULA) > 0) {

			filtrosSelecionados = true;
		}

		if (filtrosSelecionados) {

			String filtroRota = "";

			StringBuilder hql = new StringBuilder();
			hql.append(FROM);
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" pc ");
			hql.append(" inner join fetch pc.imovel imovel ");
			hql.append(" inner join fetch imovel.quadraFace quadraFace ");
			hql.append(WHERE);
			hql.append(" pc.situacaoConsumo.indicadorLeitura = true ");
			hql.append(" and pc.chavePrimaria not in  ");
			hql.append("  (select pontoConsumo.chavePrimaria from ");
			hql.append(entidadeLeituraMovimento.getSimpleName());
			hql.append(" lm ");
			hql.append("   where (lm.anoMesFaturamento, lm.ciclo, lm.rota) in   ");
			hql.append("      (select cr.anoMesReferencia, cr.numeroCiclo, cr.rota  ");
			hql.append("       from ");
			hql.append(entidadeCronogramaRota.getSimpleName());
			hql.append(" cr ");
			hql.append("       inner join cr.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
			hql.append("       where cr.cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :idAtividadeCronograma ");
			hql.append("   	   and cr.dataPrevista = 	  	");
			hql.append("   		  (select min(crr.dataPrevista) from ");
			hql.append(entidadeCronogramaRota.getSimpleName());
			hql.append(" crr ");
			hql.append("               inner join crr.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
			hql.append("             where crr.cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :idAtividadeCronograma ");
			hql.append("             and crr.dataPrevista <= :dataAtual  and crr.dataRealizada is null ");
			hql.append("             and crr.rota = cr.rota ");
			hql.append("   	   	   )");
			hql.append(filtroRota);
			hql.append("   	   )");
			hql.append("   )");

			Long idRota = (Long) filtro.get(ID_ROTA);
			if ((idRota != null) && (idRota > 0)) {
				hql.append(" and pc.rota.chavePrimaria = :idRota ");
			}

			Long matriculaImovel = (Long) filtro.get(CAMPO_FILTRO_MATRICULA);
			if ((matriculaImovel != null) && (matriculaImovel > 0)) {
				hql.append(" and imovel.chavePrimaria = :matriculaImovel ");
			}

			hql.append(" order by pc.numeroSequenciaLeitura");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameter("dataAtual", Calendar.getInstance().getTime());
			query.setLong("idAtividadeCronograma", atividadeSistema.getChavePrimaria());

			if ((idRota != null) && (idRota > 0)) {
				query.setParameter(ID_ROTA, idRota);
			}
			if ((matriculaImovel != null) && (matriculaImovel > 0)) {
				query.setParameter("matriculaImovel", matriculaImovel);
			}
		} else {
			throw new NegocioException(ControladorPontoConsumo.ERRO_FILTROS_NAO_SELECIONADOS, true);
		}

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #listarPontosConsumoNaoSequenciadas
	 * ()
	 */
	@Override
	public Collection<PontoConsumo> listarPontosConsumoNaoSequenciadas(GrupoFaturamento grupoFaturamento) {

		Collection<PontoConsumo> retorno = null;

		if (grupoFaturamento != null) {

			StringBuilder hql = new StringBuilder();
			hql.append(" select pc from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" pc ");
			hql.append(" inner join pc.ramoAtividade ramoAtividade ");
			hql.append(" inner join ramoAtividade.segmento segmento ");
			hql.append(" inner join segmento.periodicidade periodicidade ");
			hql.append(" inner join pc.situacaoConsumo situacaoConsumo ");
			hql.append(" inner join pc.segmento segmento ");
			hql.append(" inner join fetch pc.imovel imovel ");
			hql.append(WHERE);
			hql.append(" situacaoConsumo.indicadorLeitura is true");
			hql.append(" and periodicidade.chavePrimaria = :idPeriodicidade "); // NOSONAR: A regra 'String literals should not be
																				// duplicated' não se aplica nesse caso
			hql.append(" or segmento.periodicidade.chavePrimaria = :idPeriodicidade ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong("idPeriodicidade", grupoFaturamento.getPeriodicidade().getChavePrimaria()); // NOSONAR: A regra 'String literals
																										// should not be duplicated' não se
																										// aplica nesse caso

			retorno = query.list();
		}

		if (retorno == null) {
			retorno = new ArrayList<>();
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #listarPontosConsumoNaoSequenciadas
	 * ()
	 */
	@Override
	public Collection<PontoConsumo> listarPontosConsumoNaoSequenciadas(Long idPeriodicidade, Long idTipoLeitura) {

		Collection<PontoConsumo> lista = new HashSet<>();

		if (idPeriodicidade != null) {
			Collection<PontoConsumo> retorno = null;

			// obter os pontos de consumo que possuem contratos ativos
			StringBuilder hql1 = new StringBuilder();
			hql1.append(" select pontoConsumo from ");
			hql1.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql1.append(" contratoPontoConsumo "); // NOSONAR: A regra 'String literals should not be duplicated' não se aplica nesse caso
			hql1.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo ");
			hql1.append(" inner join fetch pontoConsumo.imovel imovel ");
			// o imovel é utilizado no ajax
			hql1.append(" inner join contratoPontoConsumo.contrato contrato ");
			hql1.append(" inner join contrato.situacao situacao ");
			hql1.append(" inner join contratoPontoConsumo.periodicidade periodicidade ");
			hql1.append(" inner join pontoConsumo.instalacaoMedidor instalacaoMedidor "); // NOSONAR: A regra 'String literals should not be
																							// duplicated' não se aplica nesse caso

			hql1.append(WHERE);
			hql1.append(" situacao.faturavel is true ");
			hql1.append(" and pontoConsumo.rota is null ");
			hql1.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura is true");
			hql1.append(" and periodicidade.chavePrimaria = :idPeriodicidade ");

			if (String.valueOf(idTipoLeitura).equals(TipoLeitura.ELETROCORRETOR.getValor())) {
				hql1.append(" and instalacaoMedidor.vazaoCorretor is not null ");
			} else {
				hql1.append(" and instalacaoMedidor.vazaoCorretor is null ");
			}

			hql1.append(" order by imovel.nome, pontoConsumo.descricao ");

			Query query1 = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql1.toString());
			query1.setLong("idPeriodicidade", idPeriodicidade);

			retorno = query1.list();

			List<Long> chaves = new ArrayList<>();
			int i = 0;
			for (PontoConsumo pontoConsumo : retorno) {
				i++;
				chaves.add(pontoConsumo.getChavePrimaria());
				pontoConsumo.setNumeroSequenciaLeitura(i);
				lista.add(pontoConsumo);
			}

			// obter as chaves primárias dos pontos de consumo que tem contrato ativo
			hql1 = new StringBuilder();
			hql1.append(" select pontoConsumo.chavePrimaria from ");
			hql1.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql1.append(" contratoPontoConsumo ");
			hql1.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo ");
			hql1.append(" inner join contratoPontoConsumo.contrato contrato ");
			hql1.append(" inner join contrato.situacao situacao ");
			hql1.append(" inner join contratoPontoConsumo.periodicidade periodicidade ");

			hql1.append(" inner join pontoConsumo.instalacaoMedidor instalacaoMedidor ");

			hql1.append(WHERE);
			hql1.append(" situacao.faturavel is true ");
			hql1.append(" and pontoConsumo.rota is null ");
			hql1.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura is true");

			if (String.valueOf(idTipoLeitura).equals(TipoLeitura.ELETROCORRETOR.getValor())) {
				hql1.append(" and instalacaoMedidor.vazaoCorretor is not null ");
			} else {
				hql1.append(" and instalacaoMedidor.vazaoCorretor is null ");
			}

			List<Long> chavesComContrato =
							getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql1.toString()).list();

			// obter a periodicidade pelo ramo de atividade
			hql1 = new StringBuilder();
			hql1.append(" select pontoConsumo from ");
			hql1.append(getClasseEntidade().getSimpleName());
			hql1.append(" pontoConsumo ");
			hql1.append(" inner join fetch pontoConsumo.imovel imovel ");
			// o imovel é utilizado no ajax
			hql1.append(" inner join pontoConsumo.ramoAtividade ramoAtividade ");
			hql1.append(" inner join ramoAtividade.periodicidade periodicidade ");

			hql1.append(" inner join pontoConsumo.instalacaoMedidor instalacaoMedidor ");

			hql1.append(WHERE);
			hql1.append(" pontoConsumo.rota is null ");
			hql1.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura is true");
			hql1.append(" and periodicidade.chavePrimaria = :idPeriodicidade ");

			if (String.valueOf(idTipoLeitura).equals(TipoLeitura.ELETROCORRETOR.getValor())) {
				hql1.append(" and instalacaoMedidor.vazaoCorretor is not null ");
			} else {
				hql1.append(" and instalacaoMedidor.vazaoCorretor is null ");
			}

			if (!chaves.isEmpty()) {
				hql1.append(" and pontoConsumo.chavePrimaria not in(:chaves) ");
			}
			if (!chavesComContrato.isEmpty()) {
				hql1.append(" and pontoConsumo.chavePrimaria not in (:chavesComContrato)");
			}
			hql1.append(" order by imovel.nome, pontoConsumo.descricao ");
			query1 = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql1.toString());
			query1.setLong("idPeriodicidade", idPeriodicidade);
			if (!chaves.isEmpty()) {
				query1.setParameterList("chaves", chaves);
			}
			if (!chavesComContrato.isEmpty()) {
				query1.setParameterList("chavesComContrato", chavesComContrato);
			}
			retorno = query1.list();
			for (PontoConsumo pontoConsumo : retorno) {
				i++;
				pontoConsumo.setNumeroSequenciaLeitura(i);
				lista.add(pontoConsumo);
				chaves.add(pontoConsumo.getChavePrimaria());
			}

			// obter a periodicidade pelo segmento
			hql1 = new StringBuilder();
			hql1.append(" select pontoConsumo from ");
			hql1.append(getClasseEntidade().getSimpleName());
			hql1.append(" pontoConsumo ");
			hql1.append(" inner join fetch pontoConsumo.imovel imovel ");
			// o imovel é utilizado no ajax
			hql1.append(" inner join pontoConsumo.segmento segmento ");
			hql1.append(" inner join segmento.periodicidade periodicidade ");

			hql1.append(" inner join pontoConsumo.instalacaoMedidor instalacaoMedidor ");

			hql1.append(WHERE);
			hql1.append(" pontoConsumo.rota is null ");
			hql1.append(" and pontoConsumo.ramoAtividade.periodicidade is null ");
			hql1.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura is true");
			hql1.append(" and periodicidade.chavePrimaria = :idPeriodicidade ");

			if (String.valueOf(idTipoLeitura).equals(TipoLeitura.ELETROCORRETOR.getValor())) {
				hql1.append(" and instalacaoMedidor.vazaoCorretor is not null ");
			} else {
				hql1.append(" and instalacaoMedidor.vazaoCorretor is null ");
			}

			if (!chaves.isEmpty()) {
				hql1.append(" and pontoConsumo.chavePrimaria not in(:chaves) ");
			}
			if (!chavesComContrato.isEmpty()) {
				hql1.append(" and pontoConsumo.chavePrimaria not in (:chavesComContrato)");
			}
			hql1.append(" order by imovel.nome, pontoConsumo.descricao ");
			query1 = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql1.toString());
			query1.setLong("idPeriodicidade", idPeriodicidade);
			if (!chaves.isEmpty()) {
				query1.setParameterList("chaves", chaves);
			}
			if (!chavesComContrato.isEmpty()) {
				query1.setParameterList("chavesComContrato", chavesComContrato);
			}
			retorno = query1.list();
			for (PontoConsumo pontoConsumo : retorno) {
				i++;
				pontoConsumo.setNumeroSequenciaLeitura(i);
				lista.add(pontoConsumo);
				chaves.add(pontoConsumo.getChavePrimaria());
			}
		}

		return lista;
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #consultarPontosConsumo(java.
	 * util.Map)
	 */	
	@Override
	public void ordenarPontoConsumo() throws ConcorrenciaException, NegocioException{
		   
			StringBuilder hql = new StringBuilder();		
			hql.append(" CALL ATUALIZARLEITURAPONTO()");                                                                 																				

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(hql.toString());
			query.executeUpdate();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #consultarPontosConsumo(java.
	 * util.Map)
	 */
	@Override
	public Collection<PontoConsumo> consultarPontosConsumo(Map<String, Object> filtro, String... propriedadesLazy) throws NegocioException {

		Criteria criteria = getCriteria();

		tratamentoFiltroConsultaPontosConsumo(filtro, criteria);

		Collection<Object> lista = criteria.list();

		if (propriedadesLazy != null) {

			lista = inicializarLazyColecao(lista, propriedadesLazy);

		}

		return (Collection<PontoConsumo>) (Collection<?>) lista;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #consultarPontosConsumo(java.
	 * util.Map)
	 */
	@Override
	public Collection<PontoConsumo> consultarPontosConsumoDetalhamentoCronograma(Map<String, Object> filtro, String... propriedadesLazy) 
			throws NegocioException {

		Criteria criteria = createCriteria(PontoConsumo.class);
		criteria.createAlias("segmento", "segmento");

		criteria.setProjection(Projections.projectionList()
				.add(Projections.distinct(Projections.property("chavePrimaria")), "chavePrimaria")
				.add(Projections.property("descricao"), "descricao")
				.add(Projections.property("segmento.descricao"), "segmento_descricao")
				.add(Projections.property("rota"), "rota"));
				
		tratamentoFiltroConsultaPontosConsumo(filtro, criteria);

		criteria.setResultTransformer(
				new GGASTransformer(getClasseEntidadePontoConsumo(), getSessionFactory().getAllClassMetadata()));

		return criteria.list();
	}

	private void tratamentoFiltroConsultaPontosConsumo(Map<String, Object> filtro, Criteria criteria) {
		
		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String codigoPontoConsumoSupervisorio = (String) filtro.get("codigoPontoConsumoSupervisorio");
			if (codigoPontoConsumoSupervisorio != null && !codigoPontoConsumoSupervisorio.isEmpty()) {
				criteria.add(Restrictions.eq("codigoPontoConsumoSupervisorio", codigoPontoConsumoSupervisorio));
			}

			Long idRota = (Long) filtro.get(ID_ROTA);
			if ((idRota != null) && (idRota > 0)) {
				criteria.createAlias("rota", "rota");
				criteria.add(Restrictions.eq("rota.chavePrimaria", idRota));
			} else {
				criteria.setFetchMode("rota", FetchMode.JOIN);
			}

			Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
			if ((idSegmento != null) && (idSegmento > 0)) {
				criteria.createAlias("segmento", "segmento");
				criteria.add(Restrictions.eq("segmento.chavePrimaria", idSegmento));
			} else {
				criteria.setFetchMode("segmento", FetchMode.JOIN);
			}

			Long[] chavesPrimariasImoveis = (Long[]) filtro.get("chavesPrimariasImoveis");
			if (chavesPrimariasImoveis != null && chavesPrimariasImoveis.length > 0) {
				criteria.add(Restrictions.in(IMOVEL_CHAVE_PRIMARIA, chavesPrimariasImoveis));
			}

			Long idRamoAtividade = (Long) filtro.get("idRamoAtividade");
			if (idRamoAtividade != null && idRamoAtividade > 0) {
				criteria.add(Restrictions.eq("ramoAtividade.chavePrimaria", idRamoAtividade));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {

				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}
			
			criteria.setFetchMode("imovel", FetchMode.JOIN);
			criteria.setFetchMode("imovel.quadraFace", FetchMode.JOIN);
			criteria.setFetchMode("instalacaoMedidor", FetchMode.JOIN);
			criteria.setFetchMode("instalacaoMedidor.medidor", FetchMode.JOIN);
			criteria.setFetchMode("rota.periodicidade", FetchMode.JOIN);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #obterLocalidadePontoConsumo(
	 * br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public String obterLocalidadePontoConsumo(PontoConsumo pontoConsumo) {

		return pontoConsumo.getImovel().getQuadraFace().getQuadra().getSetorComercial().getLocalidade().getDescricao();
	}

	/**
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #listarPontosConsumoPorChaveRota
	 * (long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontosConsumoPorChaveRota(long chavePrimariaRota) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select ponto from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(PONTO);
		hql.append(" inner join fetch ponto.imovel imovel");
		hql.append(" inner join fetch imovel.quadraFace quadraFace");
		hql.append(" inner join fetch quadraFace.endereco.cep cep");
		hql.append(" inner join fetch ponto.segmento segmento");
		hql.append(WHERE);
		hql.append(" ponto.rota.chavePrimaria = ?");
		hql.append(ORDER_BY_NUMERO_SEQUENCIAL);

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaRota);

		Collection<PontoConsumo> retorno =  query.list();
		if (retorno != null && !retorno.isEmpty()) {
			for (PontoConsumo pontoConsumo : retorno) {
				Hibernate.initialize(pontoConsumo.getImovel().getImovelCondominio());
			}
		}
		return retorno;
	}

	/**
	 * Lista os atributos de Ponto de consumo através da chave primaria da rota
	 * 
	 * @param chavePrimariaRota - {@link Long}
	 * @return pontos de consumo - {@link Collection}
	 */
	private Collection<PontoConsumo> listarAtributosDePontosConsumoPorChaveRota(Long chavePrimariaRota) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select ponto.chavePrimaria as chavePrimaria, ");
		hql.append(" ponto.descricaoComplemento as descricaoComplemento, ");
		hql.append(" ponto.numeroSequenciaLeitura as numeroSequenciaLeitura, ");
		hql.append(" ponto.descricao as descricao, ");
		hql.append(" ponto.cep as cep, ");
		hql.append(" rota.chavePrimaria as rota_chavePrimaria, ");
		hql.append(" setorComercial.codigo as rota_setorComercial_codigo, ");
		hql.append(" quadraFace as imovel_quadraFace, ");
		hql.append(" imovel.chavePrimaria as imovel_chavePrimaria, ");
		hql.append(" imovel.numeroImovel as imovel_numeroImovel, ");
		hql.append(" imovel.nome as imovel_nome, ");
		hql.append(" imovel.condominio as imovel_condominio, ");
		hql.append(" imovel.idImovelCondominio as imovel_idImovelCondominio, ");
		hql.append(" imovelCondominio.chavePrimaria as imovel_imovelCondominio_chavePrimaria, ");
		hql.append(" imovelCondominio.nome as imovel_imovelCondominio_nome, ");
		hql.append(" imovelCondominio.numeroImovel as imovel_imovelCondominio_numeroImovel, ");
		hql.append(" condominioQuadraFace as imovel_imovelCondominio_quadraFace, ");
		hql.append(" imovelCondominio.condominio as imovel_imovelCondominio_condominio ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(PONTO);
		hql.append(" inner join ponto.imovel imovel");
		hql.append(" left join ponto.rota as rota ");
		hql.append(" left join rota.setorComercial as setorComercial ");
		hql.append(" left join imovel.imovelCondominio imovelCondominio ");
		hql.append(" left join imovelCondominio.quadraFace condominioQuadraFace ");
		hql.append(" inner join imovel.quadraFace quadraFace");
		hql.append(" inner join quadraFace.endereco.cep cep");
		hql.append(" inner join ponto.segmento segmento");
		hql.append(WHERE);
		hql.append(" ponto.rota.chavePrimaria = ?");
		hql.append(ORDER_BY_NUMERO_SEQUENCIAL);

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setResultTransformer(new GGASTransformer(getClasseEntidade(), getSessionFactory().getAllClassMetadata()));

		query.setLong(0, chavePrimariaRota);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #obterReferenciaCicloAtual(br
	 * .com.procenge.ggas.cadastro.imovel.PontoConsumo
	 * )
	 */
	@Override
	public Map<String, Integer> obterReferenciaCicloAtual(PontoConsumo pontoConsumo) throws NegocioException {

		Map<String, Integer> referenciaCiclo = new HashMap<>();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		if ((pontoConsumo != null) && (pontoConsumo.getRota() != null) && (pontoConsumo.getRota().getGrupoFaturamento() != null)) {

			referenciaCiclo.put("ciclo", pontoConsumo.getRota().getGrupoFaturamento().getNumeroCiclo());
			referenciaCiclo.put("referencia", pontoConsumo.getRota().getGrupoFaturamento().getAnoMesReferencia());

		} else {

			referenciaCiclo.put("ciclo", 1);
			referenciaCiclo.put("referencia", Integer.valueOf((String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(LeituraMovimento.REFERENCIA_FATURAMENTO)));
		}

		return referenciaCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #consultarPontoConsumo(br.com
	 * .procenge.ggas.cadastro.imovel.GrupoFaturamento
	 * )
	 * [ebandeira] Inclusão de Parametro de Rota
	 * (Não-obrigatório)
	 */
	@Override
	public Collection<PontoConsumo> consultarPontoConsumo(GrupoFaturamento grupoFaturamento, Rota rota) throws NegocioException {

		ControladorHistoricoMedicao controladorHistoricoMedicao =
						(ControladorHistoricoMedicao) ServiceLocator.getInstancia().getBeanPorID(
										ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);
		ControladorHistoricoConsumo controladorHistoricoConsumo =
						(ControladorHistoricoConsumo) ServiceLocator.getInstancia().getBeanPorID(
										ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);

		Criteria criteria = this.createCriteria(PontoConsumo.class, "pc");

		criteria.createAlias("rota", "rota", Criteria.INNER_JOIN);
		criteria.createAlias("rota.grupoFaturamento", GRUPO_FATURAMENTO, Criteria.INNER_JOIN);
		criteria.createAlias("situacaoConsumo", "situacaoConsumo", Criteria.INNER_JOIN);

		criteria.add(Restrictions.eq("situacaoConsumo.indicadorLeitura", Boolean.TRUE));
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));

		if (rota != null) {
			criteria.add(Restrictions.eq("rota.chavePrimaria", rota.getChavePrimaria()));
		}

		if (grupoFaturamento != null) {
			criteria.add(Restrictions.eq("grupoFaturamento.chavePrimaria", grupoFaturamento.getChavePrimaria()));
		}

		DetachedCriteria subQuerieHc = DetachedCriteria.forClass(controladorHistoricoConsumo.getClasseEntidade(), "historicoConsumo");
		subQuerieHc.createAlias("historicoConsumo.historicoAtual", "historicoAtual");
		subQuerieHc.add(Restrictions.eq("indicadorFaturamento", Boolean.FALSE));
		subQuerieHc.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		subQuerieHc.add(Restrictions.eq("indicadorConsumoCiclo", Boolean.TRUE));
		subQuerieHc.add(Restrictions.eqProperty("grupoFaturamento.anoMesReferencia", "historicoAtual.anoMesLeitura"));
		subQuerieHc.add(Restrictions.eqProperty("grupoFaturamento.numeroCiclo", "historicoAtual.numeroCiclo"));
		subQuerieHc.add(Restrictions.eqProperty("pc.chavePrimaria", "historicoAtual.pontoConsumo.chavePrimaria"));
		subQuerieHc.setProjection(Projections.id());

		DetachedCriteria subQuerieHcM =
						DetachedCriteria.forClass(controladorHistoricoConsumo.getClasseEntidade(), "historicoConsumoMedicao");
		subQuerieHcM.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		subQuerieHcM.add(Restrictions.eqProperty("historicoMedicao.chavePrimaria", "historicoConsumoMedicao.historicoAtual.chavePrimaria"));
		subQuerieHcM.setProjection(Projections.id());

		DetachedCriteria subQuerieHm = DetachedCriteria.forClass(controladorHistoricoMedicao.getClasseEntidade(), "historicoMedicao");
		subQuerieHm.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		subQuerieHm.add(Restrictions.eqProperty("grupoFaturamento.anoMesReferencia", "anoMesLeitura"));
		subQuerieHm.add(Restrictions.eqProperty("grupoFaturamento.numeroCiclo", "numeroCiclo"));
		subQuerieHm.add(Restrictions.eqProperty("pc.chavePrimaria", "pontoConsumo.chavePrimaria"));
		subQuerieHm.add(Subqueries.notExists(subQuerieHcM));

		subQuerieHm.setProjection(Projections.id());

		LogicalExpression or = Restrictions.or(Subqueries.exists(subQuerieHc), Subqueries.exists(subQuerieHm));

		Conjunction e = Restrictions.conjunction();
		e.add(or);

		criteria.add(e);

		criteria.addOrder(Order.asc(PontoConsumo.ATRIBUTO_DESCRICAO));

		return criteria.list();

	}

	/**
	 * Obtém as chaves primárias dos pontos de consumo de acordo
	 * com o {@code grupoFaturamento} e {@code rota} especificados
	 * no parâmetro.
	 *
	 * @param grupoFaturamento
	 * @param rota
	 * @return chaves primárias dos pontos de consumo
	 */
	@Override
	public List<PontoConsumo> consultarChavesPrimariasPontosDeConsumo(GrupoFaturamento grupoFaturamento, Rota rota) throws NegocioException {

		ControladorHistoricoMedicao controladorHistoricoMedicao =
						(ControladorHistoricoMedicao) ServiceLocator.getInstancia().getBeanPorID(
										ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);
		ControladorHistoricoConsumo controladorHistoricoConsumo =
						(ControladorHistoricoConsumo) ServiceLocator.getInstancia().getBeanPorID(
										ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);

		Criteria criteria = this.createCriteria(PontoConsumo.class, "pc");
		ProjectionList atributosObrigatorios = Projections.projectionList();
		atributosObrigatorios.add(Projections.property("chavePrimaria"), "chavePrimaria");
		criteria.setProjection(atributosObrigatorios);

		criteria.createAlias("rota", "rota", Criteria.INNER_JOIN);
		criteria.createAlias("rota.grupoFaturamento", GRUPO_FATURAMENTO, Criteria.INNER_JOIN);
		criteria.createAlias("situacaoConsumo", "situacaoConsumo", Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));

		if (rota != null) {
			criteria.add(Restrictions.eq("rota.chavePrimaria", rota.getChavePrimaria()));
		}

		if (grupoFaturamento != null) {
			criteria.add(Restrictions.eq("grupoFaturamento.chavePrimaria", grupoFaturamento.getChavePrimaria()));
		}

		DetachedCriteria subQuerieHc = DetachedCriteria.forClass(controladorHistoricoConsumo.getClasseEntidade(), "historicoConsumo");
		subQuerieHc.createAlias("historicoConsumo.historicoAtual", "historicoAtual");
		subQuerieHc.add(Restrictions.eq("indicadorFaturamento", Boolean.FALSE));
		subQuerieHc.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		subQuerieHc.add(Restrictions.eq("indicadorConsumoCiclo", Boolean.TRUE));
		subQuerieHc.add(Restrictions.eqProperty("grupoFaturamento.anoMesReferencia", "historicoAtual.anoMesLeitura"));
		subQuerieHc.add(Restrictions.eqProperty("grupoFaturamento.numeroCiclo", "historicoAtual.numeroCiclo"));
		subQuerieHc.add(Restrictions.eqProperty("pc.chavePrimaria", "historicoAtual.pontoConsumo.chavePrimaria"));
		subQuerieHc.setProjection(Projections.id());

		DetachedCriteria subQuerieHcM =
						DetachedCriteria.forClass(controladorHistoricoConsumo.getClasseEntidade(), "historicoConsumoMedicao");
		subQuerieHcM.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		subQuerieHcM.add(Restrictions.eqProperty("historicoMedicao.chavePrimaria", "historicoConsumoMedicao.historicoAtual.chavePrimaria"));
		subQuerieHcM.setProjection(Projections.id());

		DetachedCriteria subQuerieHm = DetachedCriteria.forClass(controladorHistoricoMedicao.getClasseEntidade(), "historicoMedicao");
		subQuerieHm.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		subQuerieHm.add(Restrictions.eqProperty("grupoFaturamento.anoMesReferencia", "anoMesLeitura"));
		subQuerieHm.add(Restrictions.eqProperty("grupoFaturamento.numeroCiclo", "numeroCiclo"));
		subQuerieHm.add(Restrictions.eqProperty("pc.chavePrimaria", "pontoConsumo.chavePrimaria"));
		subQuerieHm.add(Subqueries.notExists(subQuerieHcM));

		subQuerieHm.setProjection(Projections.id());

		LogicalExpression or = Restrictions.or(Subqueries.exists(subQuerieHc), Subqueries.exists(subQuerieHm));

		Conjunction e = Restrictions.conjunction();
		e.add(or);

		criteria.add(e);

		criteria.addOrder(Order.asc(PontoConsumo.ATRIBUTO_DESCRICAO));
		criteria.setResultTransformer(Transformers.aliasToBean(getClasseEntidade()));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #criarComentarioPontoConsumo()
	 */
	@Override
	public EntidadeNegocio criarComentarioPontoConsumo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ComentarioPontoConsumo.BEAN_ID_COMENTARIO_PONTO_CONSUMO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #inserirComentarioPontoConsumo
	 * (br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo)
	 */
	@Override
	public void inserirComentarioPontoConsumo(ComentarioPontoConsumo comentarioPontoConsumo) throws NegocioException, ConcorrenciaException {

		this.validarDadosEntidade(comentarioPontoConsumo);

		PontoConsumo pontoConsumo = (PontoConsumo) obter(comentarioPontoConsumo.getPontoConsumo().getChavePrimaria());
		comentarioPontoConsumo.setPontoConsumo(pontoConsumo);
		pontoConsumo.getComentarios().add(comentarioPontoConsumo);

		super.atualizar(pontoConsumo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #obterPontoConsumoPCSVigente(
	 * java.lang.Long,
	 * java.util.Date)
	 */
	@Override
	public ImovelPCSZ obterPontoConsumoPCSZVigente(Long chavePrimariaImovel, Date dataReferencia) throws NegocioException {

		ImovelPCSZ retorno = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeImovelPCSZ().getSimpleName());
		hql.append(" where imovel.chavePrimaria = :idImovel ");
		hql.append(" and dataVigencia <= :dataReferencia ");
		hql.append(" order by dataVigencia desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idImovel", chavePrimariaImovel);
		query.setDate("dataReferencia", dataReferencia);
		query.setMaxResults(1);

		Collection<ImovelPCSZ> resultado = query.list();

		if (resultado != null && !resultado.isEmpty()) {
			retorno = resultado.iterator().next();
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #listarPontoConsumoPorChaveContrato
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoPorChaveContrato(Long chavePrimariaContrato, Long idCliente) throws NegocioException {

		Query query = null;

		Collection<PontoConsumo> listaPontoConsumo = new ArrayList<>();
		if ((chavePrimariaContrato != null) && (chavePrimariaContrato > 0) || ((idCliente != null) && (idCliente > 0))) {
			StringBuilder hql = new StringBuilder();
			hql.append(FROM);
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" pontoConsumo ");
			hql.append(" inner join fetch pontoConsumo.imovel ");
			hql.append(WHERE);
			hql.append(" pontoConsumo.chavePrimaria in ( ");
			hql.append(" select contratoPonto.pontoConsumo.chavePrimaria ");
			hql.append(FROM);
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(" contratoPonto ");
			hql.append(WHERE);
			hql.append(" contratoPonto.indicadorParadaProgramada = true ");

			if ((chavePrimariaContrato != null) && (chavePrimariaContrato > 0)) {
				hql.append(" and contratoPonto.contrato.chavePrimaria = :idContrato ");
			}

			if ((idCliente != null) && (idCliente > 0)) {
				hql.append(" and ( contratoPonto.contrato.clienteAssinatura.chavePrimaria = :idCliente ");
				hql.append(" or ( select count(contratoCliente.chavePrimaria) from ");
				hql.append(getClasseEntidadeContratoCliente().getSimpleName());
				hql.append(" contratoCliente ");
				hql.append(WHERE);
				hql.append(" contratoCliente.contrato.chavePrimaria = contratoPonto.contrato.chavePrimaria ");
				hql.append(" and contratoCliente.pontoConsumo.chavePrimaria = contratoPonto.pontoConsumo.chavePrimaria ");
				hql.append(" and contratoCliente.cliente.chavePrimaria = :idCliente ");
				hql.append(" ) > 0 ) ");
			}
			hql.append(" ) ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			if ((chavePrimariaContrato != null) && (chavePrimariaContrato > 0)) {
				query.setLong(ID_CONTRATO, chavePrimariaContrato);
			}

			if ((idCliente != null) && (idCliente > 0)) {
				query.setLong("idCliente", idCliente);
			}

			listaPontoConsumo = query.list();
		}

		return listaPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #validarPercentualParticipacao
	 * (java.lang.String[])
	 */
	@Override
	public void validarPercentualParticipacao(String[] percentualCityGate) throws NegocioException {

		if (percentualCityGate != null && percentualCityGate.length > 0) {
			BigDecimal soma = BigDecimal.valueOf(0.00);
			BigDecimal maior = BigDecimal.valueOf(PORCENTAGEM);
			String teste = "";

			for (int j = 0; j < percentualCityGate.length; j++) {
				if (percentualCityGate[j] == null || StringUtils.isEmpty(percentualCityGate[j]) || percentualCityGate[j].equals(teste)) {
					throw new NegocioException(PontoConsumoCityGate.PERCENTUAL_POR_CITYGATE_CAMPOS_OBRIGATORIOS, true);
				}
				try {
					soma =
									soma.add(Util.converterCampoStringParaValorBigDecimal("Soma", percentualCityGate[j],
													Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO));
				} catch (FormatoInvalidoException e) {
					throw new NegocioException(e);
				}
			}
			if (!(soma.compareTo(maior) == 0)) {
				throw new NegocioException(PontoConsumoCityGate.PERCENTUAL_POR_CITYGATE_100, true);
			}

		} else {
			throw new NegocioException(PontoConsumoCityGate.QUADRA_NAO_ASSOCIADA_CITYGATE, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#buscarPontoConsumo(java.lang.Long)
	 */
	@Override
	public PontoConsumo buscarPontoConsumo(Long chavePrimaria) {

		PontoConsumo pontoConsumo = null;

		if (chavePrimaria != null && chavePrimaria > 0) {
			pontoConsumo =
							(PontoConsumo) getHibernateTemplate().getSessionFactory().getCurrentSession()
											.get(getClasseEntidade(), chavePrimaria);
		}

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#inserirPoncoConsumoVencimento(java.util.Map, long)
	 */
	@Override
	public void inserirPoncoConsumoVencimento(Map<Long, Object> dadosPontoConsumoVencimento, long idContrato) throws NegocioException {

		ControladorContrato controladorContrato =
						(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		List<ContratoPontoConsumo> listaContratoPontoConsumo =
						(List<ContratoPontoConsumo>) controladorContrato.listarContratoPontoConsumo(idContrato);

		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {
			@SuppressWarnings("unchecked")
			List<PontoConsumoVencimento> listaPontoConsumoVencimento =
							(List<PontoConsumoVencimento>) dadosPontoConsumoVencimento.get(contratoPontoConsumo.getPontoConsumo()
											.getChavePrimaria());

			if (listaPontoConsumoVencimento != null && !listaPontoConsumoVencimento.isEmpty()) {

				for (PontoConsumoVencimento pontoConsumoVencimento : listaPontoConsumoVencimento) {
					super.inserir(pontoConsumoVencimento);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#removerPontoConsumoVencimento(java.util.Map, long)
	 */
	@Override
	public void removerPontoConsumoVencimento(Map<Long, Object> dadosPontoConsumoVencimento, long idContrato) throws NegocioException {

		ControladorContrato controladorContrato =
						(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		List<ContratoPontoConsumo> listaContratoPontoConsumo =
						(List<ContratoPontoConsumo>) controladorContrato.listarContratoPontoConsumo(idContrato);

		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

			List<PontoConsumoVencimento> listarPontoConsumoVencimentoConsulta =
							(List<PontoConsumoVencimento>) listarPontoConsumoVencimento(contratoPontoConsumo.getPontoConsumo()
											.getChavePrimaria());

			@SuppressWarnings("unchecked")
			List<PontoConsumoVencimento> listaPontoConsumoVencimento =
							(List<PontoConsumoVencimento>) dadosPontoConsumoVencimento.get(contratoPontoConsumo.getPontoConsumo()
											.getChavePrimaria());

			if (!listarPontoConsumoVencimentoConsulta.isEmpty() && listaPontoConsumoVencimento != null
							&& listaPontoConsumoVencimento.isEmpty()) {
				listaPontoConsumoVencimento = listarPontoConsumoVencimentoConsulta;
			}

			if (listaPontoConsumoVencimento != null && !listaPontoConsumoVencimento.isEmpty()) {
				for (PontoConsumoVencimento pontoConsumoVencimento : listarPontoConsumoVencimentoConsulta) {
					super.remover(pontoConsumoVencimento, PontoConsumoVencimento.class.getClass(), true);
				}
			}

		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#removerPontoConsumoVencimento(long)
	 */
	@Override
	public void removerPontoConsumoVencimento(long idContrato) throws NegocioException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		List<ContratoPontoConsumo> listaContratoPontoConsumo =
				(List<ContratoPontoConsumo>) controladorContrato.listarContratoPontoConsumo(idContrato);

		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

			List<PontoConsumoVencimento> listarPontoConsumoVencimentoConsulta =
					(List<PontoConsumoVencimento>) listarPontoConsumoVencimento(contratoPontoConsumo.getPontoConsumo().getChavePrimaria());

			for (PontoConsumoVencimento pontoConsumoVencimento : listarPontoConsumoVencimentoConsulta) {
				super.remover(pontoConsumoVencimento, PontoConsumoVencimento.class.getClass(), true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarPontoConsumoVencimento(long)
	 */
	@Override
	public Collection<PontoConsumoVencimento> listarPontoConsumoVencimento(long idPontoConsumo) throws NegocioException {

		Criteria criteria = createCriteria(PontoConsumoVencimento.class);

		criteria.setFetchMode("pontoConsumo", FetchMode.JOIN);

		criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", idPontoConsumo));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarListaDiasPontoConsumoVencimento(long)
	 */
	@Override
	public List<Long> consultarListaDiasPontoConsumoVencimento(long idPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append("select diaDisponivel from");
		hql.append(PontoConsumoVencimento.class);
		hql.append("pontoConsunmoVencimento");
		hql.append("inner join fetch pontoConsunmoVencimento.pontoConsumo pontoConsumo");
		hql.append("where");
		hql.append("pontoConsumo.chavePrimaria = :idPontoConsumo");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("idPontoConsumo", idPontoConsumo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarPontoConsumoPorChaveCliente(java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoPorChaveCliente(Long idCliente) {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join pontoConsumo.imovel ");
		hql.append(" inner join pontoConsumo.segmento ");
		hql.append(" inner join contratoPonto.contrato.clienteAssinatura cliente ");
		hql.append(" left join pontoConsumo.instalacaoMedidor ");
		hql.append(" left join pontoConsumo.instalacaoMedidor.medidor ");
		hql.append(" where cliente.chavePrimaria = :idCliente ");
		hql.append(" and contratoPonto.contrato.situacao = :idSituacaoContrato ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idCliente", idCliente);

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		Long idSituacaoContrato =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
		query.setLong("idSituacaoContrato", idSituacaoContrato);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #listarPontoConsumoPorCliente
	 * (java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoPorCliente(Long idCliente) throws NegocioException {

		return CollectionUtils.collect(listarContratoPontoConsumoPorCliente(idCliente), new BeanToPropertyValueTransformer("pontoConsumo"));
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarContratoPontoConsumoPorCliente(java.lang.Long[])
	 */
	@Override
	public Collection<ContratoPontoConsumo> listarContratoPontoConsumoPorCliente(Long... idCliente) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel imovel");
		hql.append(" left join fetch  imovel.quadraFace quadraFace");
		hql.append(" left join fetch  quadraFace.endereco");
		hql.append(" inner join fetch pontoConsumo.segmento ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor.medidor ");
		hql.append(" where contratoPonto.contrato.clienteAssinatura.chavePrimaria in (:idCliente) ");
		hql.append(" and contratoPonto.contrato.situacao = :idSituacaoContrato ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("idCliente", idCliente);

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idSituacaoContrato =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
		query.setLong("idSituacaoContrato", idSituacaoContrato);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarPontoConsumoPorContratoAtivoECliente(java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoPorContratoAtivoECliente(Long idCliente) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel imovel");
		hql.append(" left join fetch  imovel.quadraFace quadraFace");
		hql.append(" left join fetch  quadraFace.endereco");
		hql.append(" inner join fetch pontoConsumo.segmento ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor.medidor ");
		hql.append(" where contratoPonto.contrato.clienteAssinatura.chavePrimaria = :idCliente ");
		hql.append(" and contratoPonto.contrato.situacao = :idSituacaoContrato ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idCliente", idCliente);

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idSituacaoContrato =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
		query.setLong("idSituacaoContrato", idSituacaoContrato);

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = query.list();

		Set<PontoConsumo> listaPontoConsumoAux = new HashSet<>();

		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

			listaPontoConsumoAux.add(contratoPontoConsumo.getPontoConsumo());
		}

		return listaPontoConsumoAux;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarPontoConsumoCliente(java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoCliente(Long idCliente) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel ");
		hql.append(" inner join fetch pontoConsumo.segmento ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor.medidor ");
		hql.append(" where contratoPonto.contrato.clienteAssinatura.chavePrimaria = :idCliente ");
		hql.append(" and contratoPonto.contrato.situacao = :idSituacaoContrato ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idCliente", idCliente);

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idSituacaoContrato =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
		query.setLong("idSituacaoContrato", idSituacaoContrato);

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = query.list();

		Set<PontoConsumo> listaPontoConsumoAux = new HashSet<>();

		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

			listaPontoConsumoAux.add(contratoPontoConsumo.getPontoConsumo());
		}

		return listaPontoConsumoAux;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo#
	 * listarPontoConsumoPorClienteSimulacaoCalculo(
	 * java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoPorClienteSimulacaoCalculo(Long idCliente) throws NegocioException {

		Collection<PontoConsumo> listaPontoConsumoSimulacao = null;
		ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct contratoPonto.pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.imovel ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.segmento ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.situacaoConsumo ");
		hql.append(" inner join contratoPonto.pontoConsumo.quadraFace.rede.listaRedeTronco ");

		hql.append(" where contratoPonto.contrato.clienteAssinatura.chavePrimaria = :idCliente ");
		hql.append(" and contratoPonto.contrato.situacao = :idSituacaoContrato ");
		hql.append(" and contratoPonto.pontoConsumo.situacaoConsumo.indicadorLeitura = true ");
		hql.append(" and contratoPonto.listaContratoPontoConsumoItemFaturamento is not empty ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idCliente", idCliente);

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idSituacaoContrato =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));

		query.setLong("idSituacaoContrato", idSituacaoContrato);

		listaPontoConsumoSimulacao = query.list();

		if (listaPontoConsumoSimulacao.isEmpty()) {
			throw new NegocioException(ControladorPontoConsumo.ERRO_PONTO_CONSUMO_NAO_ENCONTRADO_SIMULACAO, true);
		}

		Collection<RedeDistribuicaoTronco> redesDistribuicaoTroncos = new HashSet<>();
		for (PontoConsumo pontoConsumo : listaPontoConsumoSimulacao) {
			redesDistribuicaoTroncos.addAll(pontoConsumo.getQuadraFace().getRede().getListaRedeTronco());
		}
		for (RedeDistribuicaoTronco redeDistribuicaoTronco : redesDistribuicaoTroncos) {
			Hibernate.initialize(redeDistribuicaoTronco.getTronco());
			Hibernate.initialize(redeDistribuicaoTronco.getTronco().getCityGate());
		}

		return listaPontoConsumoSimulacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo#
	 * listarPontoConsumoPorChaveImovelSimulacaoCalculo
	 * (java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoPorChaveImovelSimulacaoCalculo(Long chavePrimariaImovel) throws NegocioException {

		Collection<PontoConsumo> listaPontoConsumoSimulacao = null;
		Query query = null;
		ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct contratoPonto.pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.imovel ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.segmento ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.situacaoConsumo ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.quadraFace quadraFace ");
		hql.append(" inner join fetch quadraFace.rede rede ");
		hql.append(" inner join fetch rede.listaRedeTronco listaRedeTronco");
		hql.append(" inner join fetch listaRedeTronco.tronco tronco");
		hql.append(" inner join fetch tronco.cityGate cityGate");

		hql.append(" where contratoPonto.pontoConsumo.imovel.chavePrimaria = :idImovel ");
		hql.append(" and contratoPonto.contrato.situacao = :idSituacaoContrato ");
		hql.append(" and contratoPonto.pontoConsumo.situacaoConsumo.indicadorLeitura = true ");
		hql.append(" and contratoPonto.listaContratoPontoConsumoItemFaturamento is not empty ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idImovel", chavePrimariaImovel);

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idSituacaoContrato =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));

		query.setLong("idSituacaoContrato", idSituacaoContrato);

		listaPontoConsumoSimulacao = query.list();

		if (listaPontoConsumoSimulacao.isEmpty()) {
			throw new NegocioException(ControladorPontoConsumo.ERRO_PONTO_CONSUMO_NAO_ENCONTRADO_SIMULACAO, true);
		}

		return listaPontoConsumoSimulacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo#
	 * listarPontoConsumoFaturamentoAgrupado(long,
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoFaturamentoAgrupado(long idContrato, EntidadeConteudo tipoAgrupamento)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select contratoPonto.pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join contratoPonto.contrato ");
		hql.append(" inner join contratoPonto.contrato.tipoAgrupamento ");
		hql.append(" where contratoPonto.contrato.chavePrimaria = :idContrato ");
		hql.append(" and contratoPonto.contrato.agrupamentoCobranca = true ");
		hql.append(" and contratoPonto.pontoConsumo.situacaoConsumo.faturavel = true ");

		if (tipoAgrupamento != null) {

			hql.append(" and contratoPonto.contrato.tipoAgrupamento.chavePrimaria = ");
			hql.append(tipoAgrupamento.getChavePrimaria());
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_CONTRATO, idContrato);
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #listaPontoConsumoPorContrato
	 * (java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listaPontoConsumoPorContrato(Long idContrato) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select contratoPonto.pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.imovel imovel ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.quadraFace quadraFace ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.habilitado = true and ");
		hql.append(" contratoPonto.contrato.chavePrimaria = :idContrato ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_CONTRATO, idContrato);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo#
	 * listarPontoConsumoComContratoAtivoPorImovel(
	 * java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoComContratoAtivoPorImovel(Long chaveImovel) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select");
		hql.append(" contratoPonto.pontoConsumo");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.segmento segmento ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.imovel.chavePrimaria = ? ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chaveImovel);
		query.setLong(1, SituacaoContrato.ATIVO);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarPontoConsumoComContratoPorImovel(java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoComContratoPorImovel(Long chaveImovel) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select");
		hql.append(" contratoPonto.pontoConsumo");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.segmento segmento ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.imovel.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chaveImovel);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo#
	 * listarPontoConsumoComContratoAtivoPorCliente(
	 * java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoComContratoAtivoPorCliente(Long chaveCliente) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select");
		hql.append(" contratoPonto.pontoConsumo");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.segmento segmento ");
		hql.append(WHERE);
		hql.append(" contratoPonto.contrato.clienteAssinatura.chavePrimaria = ? ");
		hql.append(" and contratoPonto.contrato.situacao.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chaveCliente);
		query.setLong(1, SituacaoContrato.ATIVO);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarPontoConsumoComContratoPorCliente(java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoComContratoPorCliente(Long chaveCliente) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select");
		hql.append(" contratoPonto.pontoConsumo");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.segmento segmento ");
		hql.append(WHERE);
		hql.append(" contratoPonto.contrato.clienteAssinatura.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chaveCliente);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #consultarPontoConsumoPorRota
	 * (br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public Collection<PontoConsumo> consultarPontoConsumoPorRota(Rota rota) throws NegocioException {

		Criteria criteria = getCriteria();

		criteria.createAlias("rota", "rota");
		criteria.createAlias("imovel", "imovel", Criteria.INNER_JOIN);
		criteria.createAlias("situacaoConsumo", "situacaoConsumo", Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq("rota.chavePrimaria", rota.getChavePrimaria()));
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
		criteria.add(Restrictions.eq("situacaoConsumo.indicadorLeitura", Boolean.TRUE));

		criteria.addOrder(Order.asc("rota.chavePrimaria"));
		criteria.addOrder(Order.asc(IMOVEL_NUMERO_SEQUENCIA_LEITURA));
		criteria.addOrder(Order.asc("numeroSequenciaLeitura"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #consultarPontoConsumoPorRota
	 * (br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public Collection<PontoConsumo> consultarPontoConsumoPorRotaFaturavel(Rota rota, Boolean indicadorLeitura) throws NegocioException {

		Criteria criteria = getCriteria();

		criteria.createAlias("rota", "rota");
		criteria.createAlias("imovel", "imovel", Criteria.INNER_JOIN);
		criteria.createAlias("imovel.imovelCondominio", "imovelCondominio", Criteria.LEFT_JOIN);
		criteria.createAlias("instalacaoMedidor", "instalacaoMedidor", Criteria.LEFT_JOIN);
		criteria.createAlias("situacaoConsumo", "situacaoConsumo", Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq("rota.chavePrimaria", rota.getChavePrimaria()));
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
		criteria.add(Restrictions.eq("situacaoConsumo.faturavel", Boolean.TRUE));

		if (indicadorLeitura!=null) {
			criteria.add(Restrictions.eq("situacaoConsumo.indicadorLeitura", indicadorLeitura));
		}

		criteria.addOrder(Order.asc("rota.chavePrimaria"));
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	/**
	 * Consulta os pontos de consumo por chaves de Rota. Obtém os seguintes atributos do
	 * ponto de consumo: {@code chavePrimaria}, {@code descricao}, {@code situacaoConsumo}.
	 * Obtém os seguintes atributos da rota do ponto de consumo: {@code chavePrimaria}.
	 * Obtém os seguintes do imóvel do ponto de consumo: {@code nome}, {@code chavePrimaria}.
	 * Obtém os seguintes atributos da instação do medidor do ponto de consumo: {@code chavePrimaria}.
	 *
	 * @param chavesPrimariasRotas
	 * @return mapa de coleção de PontoConsumo por Rota
	 */
	@Override
	public Map<Rota, Collection<PontoConsumo>> consultarPontosDeConsumoPorRotas(Long[] chavesPrimariasRotas) {

		Map<Rota, Collection<PontoConsumo>> mapaPontosPorRota = new HashMap<>();

		if (chavesPrimariasRotas != null && chavesPrimariasRotas.length > 0) {

			Criteria criteria = getCriteria();

			ProjectionList projections = Projections.projectionList();
			projections.add(Projections.property("chavePrimaria"));
			projections.add(Projections.property("descricao"));
			projections.add(Projections.property("situacaoConsumo"));
			projections.add(Projections.property("instalacaoMedidor.chavePrimaria"));
			projections.add(Projections.property("rota.chavePrimaria"));
			projections.add(Projections.property(IMOVEL_CHAVE_PRIMARIA));
			projections.add(Projections.property("imovel.nome"));

			criteria.setProjection(projections);

			criteria.createAlias("rota", "rota");
			criteria.createAlias("imovel", "imovel", Criteria.INNER_JOIN);
			criteria.createAlias("instalacaoMedidor", "instalacaoMedidor", Criteria.LEFT_JOIN);
			criteria.createAlias("situacaoConsumo", "situacaoConsumo", Criteria.INNER_JOIN);
			criteria.add(Restrictions.in("rota.chavePrimaria", chavesPrimariasRotas));
			criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
			criteria.add(Restrictions.eq("situacaoConsumo.indicadorLeitura", Boolean.TRUE));
			criteria.add(Restrictions.eq("situacaoConsumo.faturavel", Boolean.TRUE));

			criteria.addOrder(Order.asc("rota.chavePrimaria"));
			criteria.addOrder(Order.asc(IMOVEL_NUMERO_SEQUENCIA_LEITURA));
			criteria.addOrder(Order.asc("numeroSequenciaLeitura"));

			mapaPontosPorRota.putAll(this.construirMapaPontosDeConsumoPorRota(criteria.list()));
		}
		return mapaPontosPorRota;
	}

	/**
	 * Constrói um mapa de pontos de consumo por Rota. Obtém os seguintes atributos do
	 * ponto de consumo: {@code chavePrimaria}, {@code descricao}, {@code situacaoConsumo}.
	 * Obtém os seguintes atributos da rota do ponto de consumo: {@code chavePrimaria}.
	 * Obtém os seguintes do imóvel do ponto de consumo: {@code nome}, {@code chavePrimaria}.
	 * Obtém os seguintes atributos da instação do medidor do ponto de consumo: {@code chavePrimaria}.
	 *
	 * @param listaAtributosPontoDeConsumo
	 * @return mapa de coleção de PontoConsumo por Rota
	 */
	private Map<Rota, Collection<PontoConsumo>> construirMapaPontosDeConsumoPorRota(List<Object[]> listaAtributosPontoDeConsumo) {

		Map<Rota, Collection<PontoConsumo>> mapaPontosPorRota = new HashMap<>();
		for (Object[] atributosPontoConsumo : listaAtributosPontoDeConsumo) {
			PontoConsumo pontoConsumo = (PontoConsumo) criar();
			pontoConsumo.setChavePrimaria((long) atributosPontoConsumo[0]);
			pontoConsumo.setDescricao((String) atributosPontoConsumo[1]);
			pontoConsumo.setSituacaoConsumo((SituacaoConsumo) atributosPontoConsumo[INDICE_SITUACAO]);
			InstalacaoMedidor instalacaoMedidor = new InstalacaoMedidorImpl();
			instalacaoMedidor.setChavePrimaria((long) atributosPontoConsumo[INDICE_INSTALACAO_MEDIDOR]);
			pontoConsumo.setInstalacaoMedidor(instalacaoMedidor);
			Rota rota = new RotaImpl();
			rota.setChavePrimaria((long) atributosPontoConsumo[INDICE_CHAVE_ROTA]);
			pontoConsumo.setRota(rota);
			Imovel imovel = new ImovelImpl();
			imovel.setChavePrimaria((long) atributosPontoConsumo[INDICE_IMOVEL_CHAVE_PRIMARIA]);
			imovel.setNome((String) atributosPontoConsumo[INDICE_IMOVEL_NOME]);
			pontoConsumo.setImovel(imovel);
			if (mapaPontosPorRota.containsKey(rota)) {
				mapaPontosPorRota.get(rota).add(pontoConsumo);
			} else {
				Collection<PontoConsumo> listaPontosConsumo = new ArrayList<>();
				listaPontosConsumo.add(pontoConsumo);
				if (pontoConsumo.getRota() != null) {
					mapaPontosPorRota.put(pontoConsumo.getRota(), listaPontosConsumo);
				}
			}
		}
		return mapaPontosPorRota;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontoConsumoBloqueadoComConsumo
	 * (br.com.ggas.medicao.rota.Rota, Integer anoMes, Integer numeroCiclo)
	 */
	@Override
	public Collection<PontoConsumo> consultarPontoConsumoBloqueadoComConsumo(Rota rota, Integer anoMes, Integer numeroCiclo)
					throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" inner join pontoConsumo.imovel ");
		hql.append(" left join pontoConsumo.imovel.imovelCondominio ");
		hql.append(" inner join pontoConsumo.instalacaoMedidor ");
		hql.append(" where pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = false ");
		hql.append(" and pontoConsumo.situacaoConsumo.faturavel = true ");
		hql.append(" and pontoConsumo.chavePrimaria in ( ");
		hql.append(" 	select historicoConsumo.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" 	historicoConsumo ");
		hql.append(" 	where historicoConsumo.pontoConsumo.chavePrimaria in ( ");
		hql.append(" 		select pontoConsumo2.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append("		pontoConsumo2 ");
		hql.append("		where pontoConsumo2.rota.chavePrimaria = ? ");
		hql.append(" 	) ");
		hql.append(" 	and historicoConsumo.anoMesFaturamento = ? ");
		hql.append(" 	and historicoConsumo.numeroCiclo = ? ");
		hql.append(" ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, rota.getChavePrimaria());
		query.setInteger(1, anoMes);
		query.setInteger(PARAMETRO_NUMERO_CICLO, numeroCiclo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontoConsumoBloqueadoComLeituraBloqueio
	 * (br.com.ggas.medicao.rota.Rota, Integer anoMes, Integer numeroCiclo)
	 */
	@Override
	public Collection<PontoConsumo> consultarPontoConsumoBloqueadoComLeituraBloqueio(Rota rota, Integer anoMes, Integer numeroCiclo)
					throws NegocioException {

		Map<String, Integer> mapaAnoMesCicloAnterior = Util.regredirReferenciaCiclo(anoMes, numeroCiclo, 1);

		Integer anoMesAnterior = mapaAnoMesCicloAnterior.get("referencia");

		Integer numeroCicloAnterior = mapaAnoMesCicloAnterior.get("ciclo");

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" inner join pontoConsumo.imovel ");
		hql.append(" left join pontoConsumo.imovel.imovelCondominio ");
		hql.append(" inner join pontoConsumo.instalacaoMedidor ");
		hql.append(" where pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = false ");
		hql.append(" and pontoConsumo.situacaoConsumo.faturavel = true ");
		// subconsulta ponto de consumo nao foi para leitura
		hql.append(" and pontoConsumo.chavePrimaria not in ( ");
		hql.append(" 	select leituraMovimento.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeLeituraMovimento().getSimpleName());
		hql.append(" 	leituraMovimento ");
		hql.append(" 	where leituraMovimento.rota.chavePrimaria in ( :chaveRota ) ");
		hql.append(" 	and leituraMovimento.anoMesFaturamento = :anoMes ");
		hql.append(" 	and leituraMovimento.ciclo = :numeroCiclo ");
		hql.append(" ) ");
		// subconsulta ponto de consumo nao registrou leitura
		hql.append(" and pontoConsumo.chavePrimaria not in ( ");
		hql.append(" 	select historicoMedicao.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeHistoricoMedicao().getSimpleName());
		hql.append(" 	historicoMedicao ");
		hql.append(" 	where historicoMedicao.pontoConsumo.chavePrimaria in ( ");
		hql.append(" 		select pontoConsumo2.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append("		pontoConsumo2 ");
		hql.append("		where pontoConsumo2.rota.chavePrimaria = :chaveRota ");
		hql.append(" 	) ");
		hql.append(" 	and historicoMedicao.anoMesLeitura = :anoMes ");
		hql.append(" 	and historicoMedicao.numeroCiclo = :numeroCiclo ");
		hql.append(" ) ");
		// subconsulta ponto de consumo foi para leitura no ciclo anterior
		hql.append(" and pontoConsumo.chavePrimaria in ( ");
		hql.append(" 	select leituraMovimento.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeLeituraMovimento().getSimpleName());
		hql.append(" 	leituraMovimento ");
		hql.append(" 	where leituraMovimento.rota.chavePrimaria in ( :chaveRota ) ");
		hql.append(" 	and leituraMovimento.anoMesFaturamento = :anoMesAnterior ");
		hql.append(" 	and leituraMovimento.ciclo = :numeroCicloAnterior ");
		hql.append(" ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveRota", rota.getChavePrimaria());
		query.setInteger("anoMes", anoMes);
		query.setInteger("numeroCiclo", numeroCiclo);
		query.setInteger("anoMesAnterior", anoMesAnterior);
		query.setInteger("numeroCicloAnterior", numeroCicloAnterior);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontoConsumoPorRotaFiscalizacao(br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public Collection<PontoConsumo> consultarPontoConsumoPorRotaFiscalizacao(Rota rota) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" inner join pontoConsumo.imovel ");
		hql.append(" left join pontoConsumo.imovel.imovelCondominio ");
		hql.append(" inner join pontoConsumo.instalacaoMedidor ");
		hql.append(" where pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = true ");
		hql.append(" and pontoConsumo.situacaoConsumo.faturavel = false ");
		hql.append(" and pontoConsumo.rota.chavePrimaria = ? ");
		hql.append(" order by pontoConsumo.rota.chavePrimaria, pontoConsumo.imovel.numeroSequenciaLeitura, pontoConsumo.numeroSequenciaLeitura ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, rota.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo#
	 * consultarPontoConsumoPorGrupoFaturamento
	 * (br.com.ggas.cadastro.imovel.GrupoFaturamento
	 * )
	 */
	@Override
	public Collection<PontoConsumo> consultarPontoConsumoPorGrupoFaturamento(GrupoFaturamento grupoFaturamento) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" inner join pontoConsumo.imovel ");
		hql.append(" inner join pontoConsumo.rota ");
		hql.append(" inner join pontoConsumo.rota.grupoFaturamento ");
		hql.append(" where pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = true ");
		hql.append(" and pontoConsumo.rota.grupoFaturamento.chavePrimaria = ? ");
		hql.append(" order by pontoConsumo.rota.chavePrimaria, pontoConsumo.imovel.numeroSequenciaLeitura, pontoConsumo.numeroSequenciaLeitura ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, grupoFaturamento.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo#
	 * montarListaPontoConsumoFaturarGrupo
	 */
	@Override
	public Collection<PontoConsumo>
					montarListaPontoConsumoFaturarGrupo(Collection<PontoConsumo> listaPontoConsumo, GrupoFaturamento grupoFaturamento,
									CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento, DadosAuditoria dadosAuditoria,
									Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias, boolean isSimulacao)
									throws NegocioException {

		ControladorContrato controladorContrato =
						(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		Collection<PontoConsumo> listaPontoConsumoAux = new ArrayList<>();
		listaPontoConsumoAux.addAll(listaPontoConsumo);

		for (PontoConsumo pontoConsumo : listaPontoConsumo) {

			ContratoPontoConsumo contratoPontoConsumo =
							controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(pontoConsumo.getChavePrimaria());

			if (contratoPontoConsumo == null) {
				// remove o ponto de consumo dos
				// faturaveis
				Collection<PontoConsumo> listaPontoConsumoContrato = new ArrayList<>();
				listaPontoConsumoContrato.add(pontoConsumo);
				this.removerPontoConsumoAgrupamento(listaPontoConsumoAux, listaPontoConsumoContrato, grupoFaturamento,
								cronogramaAtividadeFaturamento, dadosAuditoria, mapaAnomalias, isSimulacao);

				if (!isSimulacao) {
					// inclui anomalia para o
					// ponto de consumo
					ControladorFatura controladorFatura =
									(ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
													ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
					controladorFatura.inserirAnormalidade(FaturamentoAnormalidade.CODIGO_PONTO_CONSUMO_SEM_CONTRATO_ATIVO, pontoConsumo,
									grupoFaturamento.getAnoMesReferencia(), grupoFaturamento.getNumeroCiclo(), dadosAuditoria, null);
				}

			} else if (contratoPontoConsumo.getContrato().getAgrupamentoCobranca()) {

				Collection<PontoConsumo> listaPontoConsumoContrato =
								this.listaPontoConsumoPorContrato(contratoPontoConsumo.getContrato().getChavePrimaria());
				this.removerPontoConsumoAgrupamento(listaPontoConsumoAux, listaPontoConsumoContrato, grupoFaturamento,
								cronogramaAtividadeFaturamento, dadosAuditoria, mapaAnomalias, isSimulacao);

			}
		}

		this.validarPontosDeConsumoFaturaveis(listaPontoConsumoAux, grupoFaturamento, cronogramaAtividadeFaturamento, dadosAuditoria,
						mapaAnomalias, isSimulacao);
		listaPontoConsumo.clear();
		listaPontoConsumo.addAll(listaPontoConsumoAux);

		return listaPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #validarPontosConsumoFaturaveis
	 * (java.util.Collection,
	 * br.com.ggas.cadastro.
	 * imovel.GrupoFaturamento,
	 * br.com.ggas.faturamento.cronograma.
	 * CronogramaAtividadeFaturamento,
	 * br.com.ggas.auditoria.DadosAuditoria,
	 * java.util.Map, boolean)
	 */
	@Override
	public Collection<PontoConsumo> validarPontosConsumoFaturaveis(Collection<PontoConsumo> listaPontoConsumo,
					GrupoFaturamento grupoFaturamento, CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento,
					DadosAuditoria dadosAuditoria, Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias,
					boolean isSimulacao, Boolean isDrawback) throws NegocioException {

		Collection<PontoConsumo> listaPontosConsumoFaturaveis = new ArrayList<>();

		ControladorContrato controladorContrato =
						(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ControladorHistoricoConsumo controladorHistoricoConsumo =
						(ControladorHistoricoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);
		

		ControladorFaturamentoAnormalidade controladoFaturamentoAnormalidade = (ControladorFaturamentoAnormalidade) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorFaturamentoAnormalidade.BEAN_ID_CONTROLADOR_FATURAMENTO_ANORMALIDADE);

		/*
		 * Copia da lista original de pontos de
		 * consumo. Dela serão excluídos os
		 * possuem Contrato
		 * Ao final do processamento, os pontos de
		 * consumo que sobrarem aqui devem receber
		 * uma
		 * anomalia
		 * de ponto de consumo sem contrato ativo
		 */
		Collection<PontoConsumo> listaPontosConsumoSemContrato = new ArrayList<>();
		listaPontosConsumoSemContrato.addAll(listaPontoConsumo);

		Long[] chavesPontoConsumo = Util.collectionParaArrayChavesPrimarias(listaPontoConsumo);

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo =
						controladorContrato.consultarContratoPontoConsumoPorPontoConsumo(chavesPontoConsumo);

		Collection<Long> listaPontoConsumoChaves = controladorContrato.consultarPontoConsumoPorContratoAtivo(chavesPontoConsumo);

		chavesPontoConsumo = listaPontoConsumoChaves.toArray(new Long[listaPontoConsumoChaves.size()]);

		Map<Long, List<HistoricoConsumo>> mapaHistoricoConsumo =
						controladorHistoricoConsumo.consultarHistoricoConsumoPorAnoMesFaturamentoEnumeroCiclo(chavesPontoConsumo,
										grupoFaturamento.getAnoMesReferencia(), grupoFaturamento.getNumeroCiclo(), null);

		Map<Long, HistoricoConsumo> mapaHistoricoReferenciaPorPontoConsumo = super.obterMapa(
				(listaChavesPrimarias) -> controladorHistoricoConsumo.obterHistoricosDeConsumoNaoFaturados(
						listaChavesPrimarias, grupoFaturamento.getAnoMesReferencia(),
						grupoFaturamento.getNumeroCiclo()),
				new ArrayList<>(listaPontoConsumoChaves), MAX_IN);

		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

			listaPontosConsumoSemContrato.remove(contratoPontoConsumo.getPontoConsumo());

			PontoConsumo pontoConsumoValidacao = contratoPontoConsumo.getPontoConsumo();
			Long chavePontoConsumoValidacao = null;
			if (pontoConsumoValidacao != null && mapaHistoricoReferenciaPorPontoConsumo != null) {
				chavePontoConsumoValidacao = pontoConsumoValidacao.getChavePrimaria();
				HistoricoConsumo historicoConsumo = mapaHistoricoReferenciaPorPontoConsumo.get(chavePontoConsumoValidacao);
				carregarDadosResumoPontoConsumo(contratoPontoConsumo, pontoConsumoValidacao.getDadosResumoPontoConsumo(), historicoConsumo);
			}

			if (contratoPontoConsumo.getContrato().getAgrupamentoCobranca()) {

				Collection<PontoConsumo> listaPontoConsumoContrato = this
						.listaPontoConsumoPorContrato(contratoPontoConsumo.getContrato().getChavePrimaria());

				boolean isAgrupamentoFaturavel = validarPontosDeConsumoFaturaveis(grupoFaturamento,
						cronogramaAtividadeFaturamento, dadosAuditoria, mapaAnomalias, isSimulacao, isDrawback,
						mapaHistoricoConsumo, contratoPontoConsumo, listaPontoConsumoContrato);

				// Só deve passar para a lista de
				// pontos de consumo faturáveis, 1
				// ponto de consumo
				// do agrupamento
				if (isAgrupamentoFaturavel) {
					this.removerPontoConsumoFavoravel(listaPontoConsumoContrato, listaPontosConsumoFaturaveis);

					listaPontosConsumoFaturaveis.add(pontoConsumoValidacao);
				}

			} else {
				if (validarPontoConsumoFaturavel(pontoConsumoValidacao, contratoPontoConsumo, grupoFaturamento.getAnoMesReferencia(),
								grupoFaturamento.getNumeroCiclo(), cronogramaAtividadeFaturamento, dadosAuditoria, mapaAnomalias,
								isSimulacao, isDrawback, mapaHistoricoConsumo)) {
					listaPontosConsumoFaturaveis.add(pontoConsumoValidacao);
				}

			}
		}

		if (!isSimulacao) {
			
			// inclui anomalia para os pontos
			// de consumo sem contrato
			ControladorFatura controladorFatura =
					(ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
			
			FaturamentoAnormalidade anormalidade = controladoFaturamentoAnormalidade
					.obterAnormalidadePorClasse(FaturamentoAnormalidade.CODIGO_PONTO_CONSUMO_SEM_CONTRATO_ATIVO);

			for (PontoConsumo pontoConsumo : listaPontosConsumoSemContrato) {
				

				if ((mapaAnomalias.containsKey(anormalidade)
						&& !mapaAnomalias.get(anormalidade).contains(pontoConsumo))
						|| (!mapaAnomalias.containsKey(anormalidade))) {
					controladorFatura.inserirAnormalidade(
							FaturamentoAnormalidade.CODIGO_PONTO_CONSUMO_SEM_CONTRATO_ATIVO, pontoConsumo,
							grupoFaturamento.getAnoMesReferencia(), grupoFaturamento.getNumeroCiclo(), dadosAuditoria,
							null);

					atualizarMapaAnomalias(pontoConsumo, mapaAnomalias, anormalidade);
				}
			}
		}

		getSessionFactory().getCurrentSession().flush();
		
		return listaPontosConsumoFaturaveis;
	}
	
	private boolean validarPontosDeConsumoFaturaveis(GrupoFaturamento grupoFaturamento,
			CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento, DadosAuditoria dadosAuditoria,
			Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias, boolean isSimulacao,
			Boolean isDrawback, Map<Long, List<HistoricoConsumo>> mapaHistoricoConsumo,
			ContratoPontoConsumo contratoPontoConsumo, Collection<PontoConsumo> listaPontoConsumoContrato)
			throws NegocioException {

		boolean isAgrupamentoFaturavel = Boolean.TRUE;

		for (PontoConsumo pontoConsumo : listaPontoConsumoContrato) {

			if (!validarPontoConsumoFaturavel(pontoConsumo, contratoPontoConsumo,
					grupoFaturamento.getAnoMesReferencia(), grupoFaturamento.getNumeroCiclo(),
					cronogramaAtividadeFaturamento, dadosAuditoria, mapaAnomalias, isSimulacao, isDrawback,
					mapaHistoricoConsumo)) {
				isAgrupamentoFaturavel = Boolean.FALSE;
				break;
			}

		}
		return isAgrupamentoFaturavel;
	}

	private void removerPontoConsumoFavoravel(Collection<PontoConsumo> listaPontoConsumoContrato,
												Collection<PontoConsumo> listaPontosConsumoFaturaveis ){

		for (PontoConsumo pontoConsumo : listaPontoConsumoContrato) {
			if (listaPontosConsumoFaturaveis.contains(pontoConsumo)) {
				listaPontosConsumoFaturaveis.remove(pontoConsumo);
			}
		}
	}

	/**
	 * Adiciona no dados de resumo do ponto de consumo,
	 * o histórico e os dados do contrato.
	 *
	 * @param dadosResumoPontoConsumo {@link DadosResumoPontoConsumo}
	 * @param historicoConsumo {@link HistoricoConsumo}
	 * @param contratoPontoConsumo {@link ContratoPontoConsumo}
	 */
	public void carregarDadosResumoPontoConsumo(ContratoPontoConsumo contratoPontoConsumo, DadosResumoPontoConsumo dadosResumoPontoConsumo,
					HistoricoConsumo historicoConsumo) {

		if (dadosResumoPontoConsumo != null) {
			dadosResumoPontoConsumo.setContratoPontoConsumo(contratoPontoConsumo);
			if (contratoPontoConsumo != null) {
				dadosResumoPontoConsumo.setContrato(contratoPontoConsumo.getContrato());
			}
			dadosResumoPontoConsumo.setHistoricoConsumo(historicoConsumo);
		}
	}

	/**
	 * Método responsável por remover os pontos de
	 * consumo agrupados, deixando apenas um ponto
	 * de
	 * consumo por agrupamento.
	 *
	 * @param listaPontoConsumo
	 *            lista com todos os pontos de
	 *            consumo
	 * @param listaPontoConsumoContrato
	 *            lista dos pontos de consumo do
	 *            contrato agrupado
	 * @param grupoFaturamento
	 *            GrupoFaturamento
	 * @param cronogramaAtividadeFaturamento
	 *            CronogramaAtividadeFaturamento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param mapaAnomalias
	 *            the mapa anomalias
	 * @param isSimulacao
	 *            the is simulacao
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	private void removerPontoConsumoAgrupamento(Collection<PontoConsumo> listaPontoConsumo,
					Collection<PontoConsumo> listaPontoConsumoContrato, GrupoFaturamento grupoFaturamento,
					CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento, DadosAuditoria dadosAuditoria,
					Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias, boolean isSimulacao) throws NegocioException {

		if (listaPontoConsumo != null && listaPontoConsumoContrato != null) {

			Collection<PontoConsumo> listaPontoConsumoAux = listaPontoConsumoContrato;

			this.validarPontosDeConsumoFaturaveis(listaPontoConsumoAux, grupoFaturamento, cronogramaAtividadeFaturamento, dadosAuditoria,
							mapaAnomalias, isSimulacao);

			// Se todos os pontos de consumo do agrupamento não forem faturáveis, não deve faturar nenhum.
			if (!listaPontoConsumoAux.isEmpty() && listaPontoConsumoAux.size() != listaPontoConsumoContrato.size()) {
				listaPontoConsumo.removeAll(listaPontoConsumoContrato);
				listaPontoConsumoContrato.clear();
			}
			// Só deve passar para a lista de pontos de consumo faturáveis, 1 ponto de consumo do agrupamento
			if (listaPontoConsumoContrato.size() > 1) {
				this.removerPontoDeConsumoDeListaPontoConsumoContrato(listaPontoConsumo, listaPontoConsumoContrato);
			}
		}

	}

	private void removerPontoDeConsumoDeListaPontoConsumoContrato(Collection<PontoConsumo> listaPontoConsumo,
																	Collection<PontoConsumo> listaPontoConsumoContrato){

		for (PontoConsumo pontoConsumo : listaPontoConsumoContrato) {
			if (listaPontoConsumo.contains(pontoConsumo)) {
				listaPontoConsumoContrato.remove(pontoConsumo);
				listaPontoConsumo.removeAll(listaPontoConsumoContrato);
				break;
			}
		}
	}

	/**
	 * Validar pontos de consumo faturaveis.
	 *
	 * @param listaPontoConsumo
	 *            the lista ponto consumo
	 * @param grupoFaturamento
	 *            the grupo faturamento
	 * @param cronogramaAtividadeFaturamento
	 *            the cronograma atividade faturamento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param mapaAnomalias
	 *            the mapa anomalias
	 * @param isSimulacao
	 *            the is simulacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarPontosDeConsumoFaturaveis(Collection<PontoConsumo> listaPontoConsumo, GrupoFaturamento grupoFaturamento,
					CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento, DadosAuditoria dadosAuditoria,
					Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias, boolean isSimulacao) throws NegocioException {

		Collection<PontoConsumo> listaPontoConsumoNaoFaturavel = new ArrayList<>();

		for (PontoConsumo pontoConsumo : listaPontoConsumo) {

			listaPontoConsumoNaoFaturavel.addAll(obterPontosConsumoNaoFaturaveis(cronogramaAtividadeFaturamento,
							grupoFaturamento.getAnoMesReferencia(), grupoFaturamento.getNumeroCiclo(), dadosAuditoria, mapaAnomalias,
							isSimulacao, pontoConsumo));
		}

		listaPontoConsumo.removeAll(listaPontoConsumoNaoFaturavel);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #obterPontosConsumoNaoFaturaveis
	 * (br.com.ggas.faturamento.cronograma.
	 * CronogramaAtividadeFaturamento,
	 * java.lang.Integer, java.lang.Integer,
	 * br.com.ggas.auditoria.DadosAuditoria,
	 * java.util.Map, boolean,
	 * br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public Collection<PontoConsumo> obterPontosConsumoNaoFaturaveis(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento,
					Integer referencia, Integer ciclo, DadosAuditoria dadosAuditoria,
					Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias, boolean isSimulacao, PontoConsumo pontoConsumo)
					throws NegocioException {

		Collection<PontoConsumo> listaPontoConsumoNaoFaturavel = new ArrayList<>();
		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorHistoricoAnomaliaFaturamento controladorHistoricoAnomaliaFaturamento =
						(ControladorHistoricoAnomaliaFaturamento) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorHistoricoAnomaliaFaturamento.BEAN_ID_CONTROLADOR_HISTORICO_ANOMALIA_FATURAMENTO);

		ControladorCliente controladorCliente =
						(ControladorCliente) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

		ControladorHistoricoConsumo controladorHistoricoConsumo =
						(ControladorHistoricoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		if (!pontoConsumo.getSituacaoConsumo().isFaturavel()) {
			listaPontoConsumoNaoFaturavel.add(pontoConsumo);
		} else {

			// Recupera os históricos de consumo
			// não faturados do ponto para a
			// referência ciclo
			// informados.
			Collection<HistoricoConsumo> listaHistoricoConsumo =
							controladorHistoricoConsumo.consultarHistoricoConsumo(pontoConsumo.getChavePrimaria(), referencia, ciclo,
											Boolean.TRUE, Boolean.FALSE);

			if (listaHistoricoConsumo == null || listaHistoricoConsumo.isEmpty()) {
				listaPontoConsumoNaoFaturavel.add(pontoConsumo);
			} else {
				HistoricoConsumo historicoConsumo = listaHistoricoConsumo.iterator().next();
				if (historicoConsumo.getAnormalidadeConsumo() != null) {
					if (historicoConsumo.getAnormalidadeConsumo().getBloquearFaturamento()
									&& !historicoConsumo.getHistoricoAtual().isAnalisada()) {

						ControladorFaturamentoAnormalidade controladoFaturamentoAnormalidade =
										(ControladorFaturamentoAnormalidade) ServiceLocator.getInstancia().getControladorNegocio(
														ControladorFaturamentoAnormalidade.BEAN_ID_CONTROLADOR_FATURAMENTO_ANORMALIDADE);

						FaturamentoAnormalidade anormalidade =
										controladoFaturamentoAnormalidade
														.obterAnormalidadePorClasse(FaturamentoAnormalidade.CODIGO_PONTO_CONSUMO_POSSUI_BLOQUEIO_MEDICAO);

						if (!isSimulacao) {
							criarHistoricoFaturamentoAnormalidade(pontoConsumo, referencia, ciclo, dadosAuditoria,
									controladorHistoricoAnomaliaFaturamento, controladorCliente, anormalidade);
						}

						listaPontoConsumoNaoFaturavel.add(pontoConsumo);

						atualizarMapaAnomalias(pontoConsumo, mapaAnomalias, anormalidade);

					}

					// avaliar a data limite
					if (cronogramaAtividadeFaturamento != null) {
						String codigoAnormalidadeLeituraNaoInformada = null;
						SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						try {
							Date dataAtual = df.parse(df.format(new Date()));
							Date dataLimite = df.parse(df.format(cronogramaAtividadeFaturamento.getDataFim()));

							if (dataLimite.compareTo(dataAtual) >= 0) {
								// Pontos de
								// Consumo sem
								// Leitura
								// Informada e que
								// a data limite
								// não
								// tenha sido
								// atingida
								codigoAnormalidadeLeituraNaoInformada =
												controladorConstanteSistema
																.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_LEITURA_NAO_INFORMADA);
								if (String.valueOf(historicoConsumo.getAnormalidadeConsumo().getChavePrimaria()).equals(
												codigoAnormalidadeLeituraNaoInformada)) {
									listaPontoConsumoNaoFaturavel.add(pontoConsumo);
								}
							}
						} catch (ParseException e) {
							// Não precisa lançar
							// exceção específica
							// pois não é uma
							// anomalia.

							throw new NegocioException(e.getMessage());
						}
					}

				}
			}
		}

		return listaPontoConsumoNaoFaturavel;
	}

	/**
	 * Validar ponto consumo faturavel.
	 *
	 * @param pontoConsumo                   the ponto consumo
	 * @param referencia                     the referencia
	 * @param ciclo                          the ciclo
	 * @param cronogramaAtividadeFaturamento the cronograma atividade faturamento
	 * @param dadosAuditoria                 the dados auditoria
	 * @param mapaAnomalias                  the mapa anomalias
	 * @param isSimulacao                    the is simulacao
	 * @param isDrawback                     the is drawback
	 * @param contratoPontoConsumo           - {@link ContratoPontoConsumo}
	 * @param mapaHistoricoConsumo           {@link Map}
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	public boolean validarPontoConsumoFaturavel(PontoConsumo pontoConsumo, ContratoPontoConsumo contratoPontoConsumo,
			Integer referencia, Integer ciclo, CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento,
			DadosAuditoria dadosAuditoria, Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias,
			boolean isSimulacao, boolean isDrawback, Map<Long, List<HistoricoConsumo>> mapaHistoricoConsumo)
			throws NegocioException {

		Contrato contrato = contratoPontoConsumo.getContrato();

		boolean isFaturavel = Boolean.TRUE;
		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorHistoricoAnomaliaFaturamento controladorHistoricoAnomaliaFaturamento = (ControladorHistoricoAnomaliaFaturamento) ServiceLocator
				.getInstancia().getControladorNegocio(
						ControladorHistoricoAnomaliaFaturamento.BEAN_ID_CONTROLADOR_HISTORICO_ANOMALIA_FATURAMENTO);

		ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

		ControladorFaturamentoAnormalidade controladoFaturamentoAnormalidade = (ControladorFaturamentoAnormalidade) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorFaturamentoAnormalidade.BEAN_ID_CONTROLADOR_FATURAMENTO_ANORMALIDADE);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		System.out.println("nao faturavel: " + pontoConsumo.getChavePrimaria());

		// Ponto em situação não faturável
		if (!pontoConsumo.getSituacaoConsumo().isFaturavel()) {
			System.out.println("nao faturavel 1 ");
			isFaturavel = Boolean.FALSE;
		} else {

			Collection<HistoricoConsumo> listaHistoricoConsumo = mapaHistoricoConsumo
					.get(pontoConsumo.getChavePrimaria());

			Collection<HistoricoConsumo> listaHistoricoConsumoRemover = new ArrayList<>();
			if (listaHistoricoConsumo != null) {

				for (HistoricoConsumo historicoConsumo : listaHistoricoConsumo) {

					this.adicionarHistoricoDeConsumo(isDrawback, historicoConsumo, listaHistoricoConsumoRemover);
				}

				listaHistoricoConsumo.removeAll(listaHistoricoConsumoRemover);
			}

			// Tratamento de histórico de consumo
			// não informado.
			if ((listaHistoricoConsumo == null || listaHistoricoConsumo.isEmpty()) && !isDrawback) {
				
				if (controladorLeituraMovimento.consultaLeituraMovimentoComAnormalidadeNaoFaturar(
						pontoConsumo.getChavePrimaria(), referencia, ciclo)) {
					isFaturavel = Boolean.FALSE;
				} else {
					FaturamentoAnormalidade anormalidade = controladoFaturamentoAnormalidade
							.obterAnormalidadePorClasse(FaturamentoAnormalidade.CODIGO_CONSUMO_NAO_INFORMADO);

					// Caso seja um faturamento, gera
					// o historico de anomalia de
					// faturamento
					// correspondente à situação.
					if (!isSimulacao && anormalidade.isHabilitado()) {
						HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento = (HistoricoAnomaliaFaturamento) controladorHistoricoAnomaliaFaturamento
								.criar();

						Cliente clienteContrato = definirCliente(pontoConsumo, controladorCliente);

						historicoAnomaliaFaturamento.setPontoConsumo(pontoConsumo);
						historicoAnomaliaFaturamento.setAnoMesFaturamento(referencia);
						historicoAnomaliaFaturamento.setNumeroCiclo(ciclo);
						historicoAnomaliaFaturamento.setVersao(0);
						historicoAnomaliaFaturamento.setDadosAuditoria(dadosAuditoria);

						historicoAnomaliaFaturamento.setDataGeracao(Calendar.getInstance().getTime());
						historicoAnomaliaFaturamento.setAnalisada(Boolean.FALSE);
						historicoAnomaliaFaturamento.setCliente(clienteContrato);

						historicoAnomaliaFaturamento.setFaturamentoAnormalidade(anormalidade);

						controladorHistoricoAnomaliaFaturamento.inserir(historicoAnomaliaFaturamento);
					}

					// Inclui o ponto de consumo para
					// a anormalidade ou inclui a
					// anormalidade e o ponto
					// de consumo.
					if (anormalidade.isHabilitado()) {
						atualizarMapaAnomalias(pontoConsumo, mapaAnomalias, anormalidade);
					}
					if (anormalidade.getIndicadorImpedeFaturamento()) {
						System.out.println("nao faturavel 2 ");
						isFaturavel = Boolean.FALSE;
					}
				}
			} else if (listaHistoricoConsumo != null && !listaHistoricoConsumo.isEmpty()) {

				HistoricoConsumo historicoConsumo = this
						.verificarIndicadorDeFaturamentoNaoExistente(listaHistoricoConsumo);

				// Ponto já faturado via Batch
				// para a referência e ciclo
				// informados.
				if (historicoConsumo != null && historicoConsumo.getIndicadorFaturamento()) {
					System.out.println("nao faturavel 3 ");
					isFaturavel = Boolean.FALSE;
				}

				if (historicoConsumo != null && historicoConsumo.getAnormalidadeConsumo() != null) {
					if (historicoConsumo.getAnormalidadeConsumo().getBloquearFaturamento()
							&& !historicoConsumo.getHistoricoAtual().isAnalisada()) {

						FaturamentoAnormalidade anormalidade = controladoFaturamentoAnormalidade
								.obterAnormalidadePorClasse(
										FaturamentoAnormalidade.CODIGO_PONTO_CONSUMO_POSSUI_BLOQUEIO_MEDICAO);

						if (anormalidade != null) {

							if (anormalidade.isHabilitado() && ((mapaAnomalias.containsKey(anormalidade)
									&& !mapaAnomalias.get(anormalidade).contains(pontoConsumo))
									|| (!mapaAnomalias.containsKey(anormalidade)))) {

								if (!isSimulacao) {
									criarHistoricoFaturamentoAnormalidade(pontoConsumo, contrato, referencia, ciclo,
											dadosAuditoria, controladorHistoricoAnomaliaFaturamento, anormalidade);
								}

								atualizarMapaAnomalias(pontoConsumo, mapaAnomalias, anormalidade);
							}

							if (anormalidade.getIndicadorImpedeFaturamento()) {
								System.out.println("nao faturavel 4 ");
								isFaturavel = Boolean.FALSE;
							}
						}
					}

					// avaliar a data limite
					isFaturavel = avaliarDataLimite(cronogramaAtividadeFaturamento, dadosAuditoria, isFaturavel,
							controladorConstanteSistema, historicoConsumo);
				}

			} else {
				System.out.println("nao faturavel 6 ");
				isFaturavel = Boolean.FALSE;
			}

		}

		return isFaturavel;
	}

	private boolean avaliarDataLimite(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento,
			DadosAuditoria dadosAuditoria, boolean isFaturavel, ControladorConstanteSistema controladorConstanteSistema,
			HistoricoConsumo historicoConsumo) throws NegocioException {
		if (cronogramaAtividadeFaturamento != null) {
			String codigoAnormalidadeLeituraNaoInformada = null;
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			try {
				Date dataAtual = df.parse(df.format(new Date()));
				Date dataLimite = df.parse(df.format(cronogramaAtividadeFaturamento.getDataFim()));

				if (dataLimite.compareTo(dataAtual) >= 0) {
					// Pontos de
					// Consumo sem
					// Leitura
					// Informada e que
					// a data limite
					// não
					// tenha sido
					// atingida
					codigoAnormalidadeLeituraNaoInformada = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(
									Constantes.C_ANORMALIDADE_CONSUMO_LEITURA_NAO_INFORMADA);
					if (String.valueOf(historicoConsumo.getAnormalidadeConsumo().getChavePrimaria())
							.equals(codigoAnormalidadeLeituraNaoInformada)) {
						isFaturavel = Boolean.FALSE;
					}
				}
			} catch (ParseException e) {
				// Não precisa lançar
				// exceção específica
				// pois não é uma
				// anomalia.
				controladorLogProcesso.gerarInsetirLogProcesso(dadosAuditoria, e);
				throw new NegocioException(e.getMessage());
			}
		}
		return isFaturavel;
	}

	private void criarHistoricoFaturamentoAnormalidade(PontoConsumo pontoConsumo, Contrato contrato, Integer referencia,
			Integer ciclo, DadosAuditoria dadosAuditoria,
			ControladorHistoricoAnomaliaFaturamento controladorHistoricoAnomaliaFaturamento,
			FaturamentoAnormalidade anormalidade) throws NegocioException {

		Cliente clienteContrato = contrato.getClienteAssinatura();

		this.criarHistoricoFaturamentoAnormalidade(pontoConsumo, clienteContrato, referencia, ciclo, dadosAuditoria,
				controladorHistoricoAnomaliaFaturamento, anormalidade);
	}

	private void criarHistoricoFaturamentoAnormalidade(PontoConsumo pontoConsumo, Integer referencia, Integer ciclo,
			DadosAuditoria dadosAuditoria,
			ControladorHistoricoAnomaliaFaturamento controladorHistoricoAnomaliaFaturamento,
			ControladorCliente controladorCliente, FaturamentoAnormalidade anormalidade) throws NegocioException {

		Cliente clienteContrato = controladorCliente
				.obterClientePorPontoConsumoDeContratoAtivo(pontoConsumo.getChavePrimaria());

		this.criarHistoricoFaturamentoAnormalidade(pontoConsumo, clienteContrato, referencia, ciclo, dadosAuditoria,
				controladorHistoricoAnomaliaFaturamento, anormalidade);
	}

	private void criarHistoricoFaturamentoAnormalidade(PontoConsumo pontoConsumo, Cliente clienteContrato,
			Integer referencia, Integer ciclo, DadosAuditoria dadosAuditoria,
			ControladorHistoricoAnomaliaFaturamento controladorHistoricoAnomaliaFaturamento,
			FaturamentoAnormalidade anormalidade) throws NegocioException {

		HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento = (HistoricoAnomaliaFaturamento) controladorHistoricoAnomaliaFaturamento
				.criar();

		historicoAnomaliaFaturamento.setPontoConsumo(pontoConsumo);
		historicoAnomaliaFaturamento.setAnoMesFaturamento(referencia);
		historicoAnomaliaFaturamento.setNumeroCiclo(ciclo);
		historicoAnomaliaFaturamento.setVersao(0);
		historicoAnomaliaFaturamento.setDadosAuditoria(dadosAuditoria);

		historicoAnomaliaFaturamento.setDataGeracao(Calendar.getInstance().getTime());
		historicoAnomaliaFaturamento.setAnalisada(Boolean.FALSE);
		historicoAnomaliaFaturamento.setCliente(clienteContrato);

		historicoAnomaliaFaturamento.setFaturamentoAnormalidade(anormalidade);

		controladorHistoricoAnomaliaFaturamento.inserirEmBatch(historicoAnomaliaFaturamento, Boolean.TRUE);
	}

	private void atualizarMapaAnomalias(PontoConsumo pontoConsumo, Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias,
			FaturamentoAnormalidade anormalidade) {
		if (mapaAnomalias.containsKey(anormalidade)) {
			mapaAnomalias.get(anormalidade).add(pontoConsumo);
		} else {
			Collection<PontoConsumo> colecaoPontoConsumo = new ArrayList<>();
			colecaoPontoConsumo.add(pontoConsumo);
			mapaAnomalias.put(anormalidade, colecaoPontoConsumo);
		}
	}

	private Cliente definirCliente(PontoConsumo pontoConsumo, ControladorCliente controladorCliente) throws NegocioException {

		Cliente clienteContrato = null;
		if (pontoConsumo.getDadosResumoPontoConsumo() != null && pontoConsumo.getDadosResumoPontoConsumo().getContrato() != null) {
			clienteContrato = pontoConsumo.getDadosResumoPontoConsumo().getContrato().getClienteAssinatura();
		} else {
			clienteContrato = controladorCliente.obterClientePorPontoConsumoDeContratoAtivo(pontoConsumo.getChavePrimaria());
		}
		return clienteContrato;
	}

	private HistoricoConsumo verificarIndicadorDeFaturamentoNaoExistente(Collection<HistoricoConsumo> listaHistoricoConsumo) {

		HistoricoConsumo historicoConsumo = null;

		for (Iterator<HistoricoConsumo> iterator = listaHistoricoConsumo.iterator(); iterator.hasNext();) {
			historicoConsumo = iterator.next();
			if (!historicoConsumo.getIndicadorFaturamento()) {
				break;
			}
		}
		return historicoConsumo;
	}

	private void adicionarHistoricoDeConsumo(boolean isDrawback, HistoricoConsumo historicoConsumo,
												Collection<HistoricoConsumo> listaHistoricoConsumoRemover){


		if (isDrawback && historicoConsumo.getIndicadorDrawback() != null
				&& !historicoConsumo.getIndicadorDrawback()) {

			listaHistoricoConsumoRemover.add(historicoConsumo);
		} else if (!isDrawback && historicoConsumo.getIndicadorDrawback() != null && historicoConsumo.getIndicadorDrawback()) {
			listaHistoricoConsumoRemover.add(historicoConsumo);
		}
	}

	/**
	 * 
	 * Contagem dos medidores por Grupo de Faturamento
	 * 
	 * @param Long idGrupoFaturamento
	 * @return int
	 * @throws NegocioException
	 */
	@Override
	public int quantidadeMedidoresPorGrupoFaturamento(Long idGrupoFaturamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(pontoConsumo.chavePrimaria) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where pontoConsumo.rota.grupoFaturamento.chavePrimaria = ? ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura is true  ");
		hql.append(" and pontoConsumo.habilitado is true  ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idGrupoFaturamento);
		
		Long total = (Long) query.uniqueResult();

		return total.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #quantidadeLeituraPorRota(java
	 * .lang.Long)
	 */
	@Override
	public int quantidadeLeituraPorRota(Long idRota) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(pontoConsumo.chavePrimaria) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where pontoConsumo.rota.chavePrimaria = ? and ");
		hql.append(" pontoConsumo.situacaoConsumo.indicadorLeitura is true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idRota);

		Long total = (Long) query.uniqueResult();

		return total.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #listarPontosConsumo(java.util
	 * .Map)
	 */
	@Override
	public List<PontoConsumo> listarPontosConsumo(Map<String, Object> filtro) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_MODALIDADE_MEDICAO_INDIVIDUAL);

		Criteria criteria = this.createCriteria(PontoConsumo.class);
		criteria.createAlias("imovel", "imovel");
		criteria.createAlias("instalacaoMedidor", "instalacaoMedidor", Criteria.LEFT_JOIN);
		criteria.createAlias("instalacaoMedidor.medidor", "medidor", Criteria.LEFT_JOIN);
		criteria.createAlias("instalacaoMedidor.vazaoCorretor", "vazaoCorretor", Criteria.LEFT_JOIN);
		criteria.createAlias("situacaoConsumo", "situacaoConsumo", Criteria.LEFT_JOIN);

		if (filtro.get("idImovel") != null) {
			criteria.add(Restrictions.eq(IMOVEL_CHAVE_PRIMARIA, filtro.get("idImovel")));
		}

		if (filtro.get("idCliente") != null) {
			criteria.createCriteria("imovel.listaClienteImovel").createCriteria("cliente")
							.add(Restrictions.eq("chavePrimaria", filtro.get("idCliente")));
		}

		if (filtro.get("numeroMedidor") != null) {
			criteria.add(Restrictions.eq("medidor.numeroSerie", filtro.get("numeroMedidor")));
		}

		if (filtro.get("numeroCorretorVazao") != null) {
			criteria.add(Restrictions.eq("vazaoCorretor.numeroSerie", filtro.get("numeroCorretorVazao")));
		}
		Boolean habilitado = (Boolean) filtro.get("habilitado");
		if (habilitado != null) {
			criteria.add(Restrictions.eq("habilitado", habilitado));
		}

		// Paginação em banco dados
		boolean paginacaoPadrao = false;
		if (filtro.containsKey("colecaoPaginada")) {

			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
			colecaoPaginada.adicionarOrdenacaoEspecial("medidor.numeroSerie", new OrdenacaoEspecialNumeroMedidor());
			colecaoPaginada.adicionarOrdenacaoEspecial("vazaoCorretor.numeroSerie", new OrdenacaoEspecialNumeroCorretorVazao());
			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if (paginacaoPadrao) {
			criteria.addOrder(Order.asc("descricao"));
		}

		return criteria.list();
	}

	public List<PontoConsumo> listarPontoDeConsumoFilhoDeImovelCondominio(Long idImovel) {
		Criteria criteria = this.createCriteria(PontoConsumo.class);
		criteria.createAlias("imovel", "imovel");
		criteria.createAlias("instalacaoMedidor", "instalacaoMedidor", Criteria.LEFT_JOIN);
		criteria.createAlias("instalacaoMedidor.medidor", "medidor", Criteria.LEFT_JOIN);
		criteria.createAlias("instalacaoMedidor.vazaoCorretor", "vazaoCorretor", Criteria.LEFT_JOIN);
		criteria.createAlias("situacaoConsumo", "situacaoConsumo", Criteria.LEFT_JOIN);

		Criterion primeiraOpcao = Restrictions.eq(IMOVEL_CHAVE_PRIMARIA, idImovel);
		Criterion segundaOpcao = Restrictions.eq("imovel.idImovelCondominio", idImovel);
		criteria.add(Restrictions.or(primeiraOpcao, segundaOpcao));
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	private class OrdenacaoEspecialNumeroMedidor implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {

			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			criteria.addOrder(Order.desc("medidor.numeroSerie"));

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}

	private class OrdenacaoEspecialNumeroCorretorVazao implements OrdenacaoEspecial {

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {

			throw new UnsupportedOperationException();
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			criteria.addOrder(Order.asc("vazaoCorretor.numeroSerie"));

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}

	@Override
	public Collection<PontoConsumo> retirarPontoConsumoLeituraMovimento(Long[] chavesPontoConsumo, GrupoFaturamento grupoFaturamento)
					throws NegocioException {

		Criteria criteria = this.createCriteria(getClasseEntidadeLeituraMovimento(), "movimento");
		criteria.createAlias("movimento.pontoConsumo", "pontoConsumo");
		criteria.add(Util.construirInCriterion("movimento.pontoConsumo.chavePrimaria",
						new ArrayList<Long>(Arrays.asList(chavesPontoConsumo)), CONSTANTE_DOIS));
		criteria.add(Restrictions.eq("movimento.anoMesFaturamento", grupoFaturamento.getAnoMesReferencia()));
		criteria.add(Restrictions.eq("movimento.ciclo", grupoFaturamento.getNumeroCiclo()));
		criteria.add(Restrictions.eq("movimento.grupoFaturamento.chavePrimaria", grupoFaturamento.getChavePrimaria()));

		criteria.setProjection(Projections.distinct(Projections.property("pontoConsumo")));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo#
	 * processarDadosResumoFaturaPontoConsumo
	 * (br.com.ggas.faturamento.fatura.
	 * DadosResumoFatura, java.util.Map,
	 * java.util.Collection)
	 */
	@Override
	public boolean processarDadosResumoFaturaPontoConsumo(DadosResumoFatura dadosResumoFatura,
					Map<FaturamentoAnormalidade, Collection<PontoConsumo>> mapaAnomalias, Collection<Fatura> listaFatura)
					throws GGASException {

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		LOG.info("\n----------------------------------------------- BURACO");
		/*
		 * Verifica se existem ciclos anteriores
		 * não processados [buraco]
		 */
		boolean isInseridoComSucesso = true;

		Collection<HistoricoConsumo> consumosEmAberto = controladorFatura.mapearConsumoEmAberto(dadosResumoFatura);

		if (consumosEmAberto != null && !consumosEmAberto.isEmpty() && dadosResumoFatura.getListaPontoConsumo() != null) {

			PontoConsumo pontoConsumo = dadosResumoFatura.getListaPontoConsumo().get(0);

			for (HistoricoConsumo historicoConsumo : consumosEmAberto) {

				Collection<PontoConsumo> pontosConsumoNaoFaturaveis =
								this.obterPontosConsumoNaoFaturaveis(null, historicoConsumo.getAnoMesFaturamento(),
												historicoConsumo.getNumeroCiclo(), dadosResumoFatura.getDadosAuditoria(), mapaAnomalias,
												Boolean.FALSE, pontoConsumo);

				if (pontosConsumoNaoFaturaveis == null || !pontosConsumoNaoFaturaveis.contains(pontoConsumo)) {

					try {
						// Monta dadosResumoFatura
						// para o ciclo anterior
						DadosResumoFatura dadosResumoCicloAnterior =
										controladorFatura.obterDadosFatura(pontoConsumo, historicoConsumo.getAnoMesFaturamento(),
														historicoConsumo.getNumeroCiclo(), Boolean.FALSE, null, null);
						// hls
						dadosResumoCicloAnterior.setDadosAuditoria(dadosResumoFatura.getDadosAuditoria());

						Collection<Fatura> faturasInseridas =
										controladorFatura.processarInclusaoFatura(dadosResumoCicloAnterior, Boolean.FALSE, Boolean.TRUE,
														dadosResumoFatura.getDadosAuditoria());

						if (faturasInseridas != null && !faturasInseridas.isEmpty()) {
							Long[] faturasPendentesAntigas = dadosResumoFatura.getChavesFaturasPendentes();
							Long[] faturasPendentesNovas = Util.collectionParaArrayChavesPrimarias(faturasInseridas);
							dadosResumoFatura.setChavesFaturasPendentes((Long[]) ArrayUtils.addAll(faturasPendentesAntigas,
											faturasPendentesNovas));
						}
					} catch (Exception e) {
						LOG.error(e);
						isInseridoComSucesso = Boolean.FALSE;
					}

					// insere os ciclos anteriores
					if (!isInseridoComSucesso) {
						ControladorFaturamentoAnormalidade controladorFaturamentoAnormalidade =
										(ControladorFaturamentoAnormalidade) ServiceLocator.getInstancia().getControladorNegocio(
														ControladorFaturamentoAnormalidade.BEAN_ID_CONTROLADOR_FATURAMENTO_ANORMALIDADE);

						ControladorHistoricoAnomaliaFaturamento controladorHistoricoAnomaliaFaturamento =
										(ControladorHistoricoAnomaliaFaturamento) ServiceLocator
														.getInstancia()
														.getControladorNegocio(
																		ControladorHistoricoAnomaliaFaturamento.BEAN_ID_CONTROLADOR_HISTORICO_ANOMALIA_FATURAMENTO);

						ControladorCliente controladorCliente =
										(ControladorCliente) ServiceLocator.getInstancia().getControladorNegocio(
														ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

						HistoricoAnomaliaFaturamento historico =
										(HistoricoAnomaliaFaturamento) controladorHistoricoAnomaliaFaturamento.criar();
						historico.setAnoMesFaturamento(dadosResumoFatura.getReferencia());
						historico.setNumeroCiclo(dadosResumoFatura.getCiclo());
						historico.setPontoConsumo(pontoConsumo);

						Cliente clienteContrato = null;
						if (pontoConsumo.getDadosResumoPontoConsumo().getContrato() != null) {
							clienteContrato = pontoConsumo.getDadosResumoPontoConsumo().getContrato().getClienteAssinatura();
						} else {
							clienteContrato =
											controladorCliente.obterClientePorPontoConsumoDeContratoAtivo(pontoConsumo.getChavePrimaria());
						}

						historico.setCliente(clienteContrato);

						FaturamentoAnormalidade anormalidade =
										controladorFaturamentoAnormalidade
														.obterAnormalidadePorClasse(FaturamentoAnormalidade.CODIGO_PONTO_CONSUMO_POSSUI_CICLO_NAO_FATURADO);
						historico.setFaturamentoAnormalidade(anormalidade);

						historico.setDadosAuditoria(dadosResumoFatura.getDadosAuditoria());
						historico.setDataGeracao(Calendar.getInstance().getTime());
						historico.setAnalisada(Boolean.FALSE);

						Map<String, Object> filtro = new HashMap<>();
						filtro.put("referencia", dadosResumoFatura.getReferencia().toString());
						filtro.put("ciclo", dadosResumoFatura.getCiclo().toString());
						filtro.put("analisada", false);
						Long[] idsAnormalidadeFatura = {anormalidade.getChavePrimaria()};
						filtro.put("idsAnormalidadeFatura", idsAnormalidadeFatura);
						filtro.put("idPontoConsumo", pontoConsumo.getChavePrimaria());

						Collection<HistoricoAnomaliaFaturamento> listaHistoricoAnomaliaFaturamento =
										controladorHistoricoAnomaliaFaturamento.consultarHistoricoAnomaliaFaturamento(filtro);
						for (HistoricoAnomaliaFaturamento historicoAnomaliaFaturamento : listaHistoricoAnomaliaFaturamento) {
							controladorHistoricoAnomaliaFaturamento.remover(historicoAnomaliaFaturamento);
						}

						controladorHistoricoAnomaliaFaturamento.inserir(historico);
						controladorFatura.popularMapaAnomalias(mapaAnomalias, anormalidade, pontoConsumo);
					}
				}
			}
		}

		LOG.info("\n----------------------------------------------- INSERIR");

		if (isInseridoComSucesso) {
			listaFatura.addAll(controladorFatura.processarInclusaoFatura(
					dadosResumoFatura, Boolean.TRUE, Boolean.TRUE,dadosResumoFatura.getDadosAuditoria()));
		}

		return isInseridoComSucesso;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoCityGate(java.util.Map)
	 */
	@Override
	public Collection<PontoConsumoCityGate> consultarPontosConsumoCityGate(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadePontoConsumoCityGate());

		if (filtro != null) {

			Long idCityGate = (Long) filtro.get("idCityGate");
			if (idCityGate != null && idCityGate > 0) {
				criteria.add(Restrictions.eq("cityGate.chavePrimaria", idCityGate));
			}

		}
		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarPontoConsumoPorCodigoSupervisorio(java.lang.String)
	 */
	@Override
	public PontoConsumo listarPontoConsumoPorCodigoSupervisorio(String codigoPontoConsumoSupervisorio) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);
		hql.append(" pontoConsumo.codigoPontoConsumoSupervisorio = ? ");
		hql.append(" and pontoConsumo.habilitado = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, codigoPontoConsumoSupervisorio);
		query.setBoolean(1, Boolean.TRUE);

		return (PontoConsumo) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontoConsumoPorCodigoLegado(java.lang.String)
	 */
	@Override
	public Collection<PontoConsumo> consultarPontoConsumoPorCodigoLegado(String codigoLegado) {

		Criteria criteria = getCriteria();

		criteria.createCriteria("listaPontoConsumo").add(
						Restrictions.or(Restrictions.eq("codigoLegado", codigoLegado), Restrictions.eq("chavePrimaria", codigoLegado)));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#validarPontoConsumoECodigoLegado(java.lang.String, java.lang.Long)
	 */
	@Override
	public void validarPontoConsumoECodigoLegado(String codigoLegado, Long idPontoConsumo) throws NegocioException {

		Criteria criteria = getCriteria();

		criteria.add(Restrictions.eq("codigoLegado", codigoLegado));

		if (criteria.list().size() > 1) {
			throw new NegocioException(Constantes.ERRO_CODIGO_LEGADO_EM_USO, true);
		} else if (criteria.list().size() == 1) {

			PontoConsumo pontoConsumo = (PontoConsumo) criteria.uniqueResult();

			if (pontoConsumo != null && pontoConsumo.getChavePrimaria() != idPontoConsumo) {
				throw new NegocioException(Constantes.ERRO_PONTO_CONSUMO_JA_EXISTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarIdsPontosConsumoPorChaveRota(java.lang.Long)
	 */
	@Override
	public Long[] listarIdsPontosConsumoPorChaveRota(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(PONTO);
		hql.append(" inner join fetch ponto.imovel imovel");
		hql.append(" inner join fetch imovel.quadraFace quadraFace");
		hql.append(" inner join fetch quadraFace.endereco.cep cep");
		hql.append(" inner join fetch ponto.segmento segmento");
		hql.append(WHERE);
		hql.append(" ponto.rota.chavePrimaria = ?");
		hql.append(ORDER_BY_NUMERO_SEQUENCIAL);

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimaria);

		List<Long> listaId = query.list();

		return listaId.toArray(new Long[listaId.size()]);
	}

	/**
	 * Remanejar rota.
	 *
	 * @param idPontosConsumoASeremRemanejados
	 *            the id pontos consumo a serem remanejados
	 * @param pontosConsumoRemanejados
	 *            the pontos consumo remanejados
	 * @param rotaNova
	 *            the rota nova
	 * @return the list
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private List<PontoConsumo> remanejarRota(Long[] idPontosConsumoASeremRemanejados, List<PontoConsumo> pontosConsumoRemanejados,
					Rota rotaNova) throws GGASException {

		for (Long id : idPontosConsumoASeremRemanejados) {
			if (id != 0) {
				PontoConsumo ponto = (PontoConsumo) obter(id, "rota");
				ponto.setNumeroSequenciaLeitura(rotaNova.getPontosConsumo().size()+1);
				rotaNova.getPontosConsumo().add(ponto);
				ponto.setRota(rotaNova);
				pontosConsumoRemanejados.add(ponto);
			}
		}
		return pontosConsumoRemanejados;
	}

	@Override
	public Collection<PontoConsumo> consultarPontoConsumoPorComandoAcao(AcaoComandoEstendida acaoComandoEstendida) throws NegocioException {

		Criteria criteria = this.createCriteria(PontoConsumo.class, "pc");

		criteria.createAlias("situacaoConsumo", "situacaoConsumo", Criteria.INNER_JOIN);

		boolean usaVazaoCorrtor = acaoComandoEstendida.getMarcaCorretor() != null || acaoComandoEstendida.getModeloCorretor() != null;

		boolean usaMedidor = acaoComandoEstendida.getModeloMedidor() != null || acaoComandoEstendida.getMarcaMedidor() != null
				|| acaoComandoEstendida.getAnoFabricacaoFim() != null || acaoComandoEstendida.getAnoFabricacaoInicio() != null;

		if (usaVazaoCorrtor || usaMedidor || acaoComandoEstendida.getDataInstalacaoInicio() != null
				|| acaoComandoEstendida.getDataInstalacaoFim() != null) {
			criteria.createAlias("instalacaoMedidor", "instalacaoMedidor", Criteria.INNER_JOIN);
		}
		if (usaVazaoCorrtor) {
			criteria.createAlias("instalacaoMedidor.vazaoCorretor", "vazaoCorretor", Criteria.INNER_JOIN);
		}
		if (usaMedidor) {
			criteria.createAlias("instalacaoMedidor.medidor", "medidor", Criteria.INNER_JOIN);
		}
		if (acaoComandoEstendida.getIndicadorCondominio() != null || acaoComandoEstendida.getModalidadeMedicaoImovel() != null) {
			criteria.createAlias("imovel", "imovel", Criteria.INNER_JOIN);
		}
		if (acaoComandoEstendida.getTronco() != null || acaoComandoEstendida.getZonaBloqueio() != null) {
			criteria.createAlias("quadraFace", "quadraFace", Criteria.INNER_JOIN);
		}

		aplicaFiltrosApartirDaRota(acaoComandoEstendida, criteria);

		aplicaFiltrosMedidor(acaoComandoEstendida, criteria);

		if (acaoComandoEstendida.getDataInstalacaoInicio() != null) {
			criteria.add(Restrictions.ge("instalacaoMedidor.data", acaoComandoEstendida.getDataInstalacaoInicio()));
		}

		if (acaoComandoEstendida.getDataInstalacaoFim() != null) {
			criteria.add(Restrictions.le("instalacaoMedidor.data", acaoComandoEstendida.getDataInstalacaoFim()));
		}

		if (acaoComandoEstendida.getSituacaoConsumo() != null) {
			criteria.add(Restrictions.eq("situacaoConsumo.chavePrimaria", acaoComandoEstendida.getSituacaoConsumo().getChavePrimaria()));
		}

		if (acaoComandoEstendida.getSegmento() != null) {
			criteria.createAlias("segmento", "segmento", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("segmento.chavePrimaria", acaoComandoEstendida.getSegmento().getChavePrimaria()));
		}

		if (acaoComandoEstendida.getIndicadorCondominio() != null) {
			criteria.add(Restrictions.eq("imovel.condominio", acaoComandoEstendida.getIndicadorCondominio()));
		}

		if (acaoComandoEstendida.getModalidadeMedicaoImovel() != null) {
			criteria.add(Restrictions.eq("imovel.modalidadeMedicaoImovel.codigo",
					acaoComandoEstendida.getModalidadeMedicaoImovel().getCodigo()));
		}

		if (acaoComandoEstendida.getCityGate() != null) {
			criteria.createAlias("listaPontoConsumoCityGate", "listaPontoConsumoCityGate", Criteria.INNER_JOIN);
			criteria.createAlias("listaPontoConsumoCityGate.cityGate", "cityGate", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("cityGate.chavePrimaria", acaoComandoEstendida.getCityGate().getChavePrimaria()));
		}

		if (acaoComandoEstendida.getTronco() != null) {
			criteria.createAlias("quadraFace.rede", "rede", Criteria.INNER_JOIN);
			criteria.createAlias("rede.listaRedeTronco", "listaRedeTronco", Criteria.INNER_JOIN);
			criteria.createAlias("listaRedeTronco.tronco", "tronco", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("tronco.chavePrimaria", acaoComandoEstendida.getTronco().getChavePrimaria()));
		}

		if (acaoComandoEstendida.getZonaBloqueio() != null) {
			criteria.createAlias("quadraFace.quadra", "quadra", Criteria.INNER_JOIN);
			criteria.createAlias("quadra.zonaBloqueio", "zonaBloqueio", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("zonaBloqueio.chavePrimaria", acaoComandoEstendida.getZonaBloqueio().getChavePrimaria()));
		}

		return criteria.list();
	}

	private void aplicaFiltrosApartirDaRota(AcaoComandoEstendida acaoComandoEstendida, Criteria criteria) {
		if (acaoComandoEstendida.getGrupoFaturamento() != null || acaoComandoEstendida.getSetorComercial() != null
				|| acaoComandoEstendida.getLocalidade() != null || acaoComandoEstendida.getMunicipio() != null) {
			criteria.createAlias("rota", "rota", Criteria.INNER_JOIN);
			if (acaoComandoEstendida.getGrupoFaturamento() != null) {
				criteria.createAlias("rota.grupoFaturamento", GRUPO_FATURAMENTO, Criteria.INNER_JOIN);
				criteria.add(
						Restrictions.eq("grupoFaturamento.chavePrimaria", acaoComandoEstendida.getGrupoFaturamento().getChavePrimaria()));
			}
			if (acaoComandoEstendida.getSetorComercial() != null || acaoComandoEstendida.getLocalidade() != null
					|| acaoComandoEstendida.getMunicipio() != null) {
				aplicaFiltrosSetorComercial(acaoComandoEstendida, criteria);
			}
		}
	}

	private void aplicaFiltrosSetorComercial(AcaoComandoEstendida acaoComandoEstendida, Criteria criteria) {
		criteria.createAlias("rota.setorComercial", "setorComercial", Criteria.INNER_JOIN);
		if (acaoComandoEstendida.getSetorComercial() != null) {
			criteria.add(
					Restrictions.eq("setorComercial.chavePrimaria", acaoComandoEstendida.getSetorComercial().getChavePrimaria()));
		}
		if (acaoComandoEstendida.getLocalidade() != null) {
			criteria.createAlias("setorComercial.localidade", "localidade", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("localidade.chavePrimaria", acaoComandoEstendida.getLocalidade().getChavePrimaria()));
		}
		if (acaoComandoEstendida.getMunicipio() != null) {
			criteria.createAlias("setorComercial.municipio", "municipio", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("municipio.chavePrimaria", acaoComandoEstendida.getMunicipio().getChavePrimaria()));
		}
	}

	private void aplicaFiltrosMedidor(AcaoComandoEstendida acaoComandoEstendida, Criteria criteria) {
		if (acaoComandoEstendida.getMarcaCorretor() != null) {
			criteria.createAlias("vazaoCorretor.marcaCorretor", "marcaCorretor", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("marcaCorretor.chavePrimaria", acaoComandoEstendida.getMarcaCorretor().getChavePrimaria()));
		}

		if (acaoComandoEstendida.getModeloCorretor() != null) {
			criteria.createAlias("vazaoCorretor.modelo", "modeloCorretor", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("modeloCorretor.chavePrimaria", acaoComandoEstendida.getModeloCorretor().getChavePrimaria()));
		}

		if (acaoComandoEstendida.getMarcaMedidor() != null) {
			criteria.createAlias("medidor.marcaMedidor", "marcaMedidor", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("marcaMedidor.chavePrimaria", acaoComandoEstendida.getMarcaMedidor().getChavePrimaria()));
		}

		if (acaoComandoEstendida.getModeloMedidor() != null) {
			criteria.createAlias("medidor.modelo", "modeloMedidor", Criteria.INNER_JOIN);
			criteria.add(Restrictions.eq("modeloMedidor.chavePrimaria", acaoComandoEstendida.getModeloMedidor().getChavePrimaria()));
		}

		if (acaoComandoEstendida.getAnoFabricacaoInicio() != null) {
			criteria.add(Restrictions.ge("medidor.anoFabricacao", acaoComandoEstendida.getAnoFabricacaoInicio()));
		}

		if (acaoComandoEstendida.getAnoFabricacaoFim() != null) {
			criteria.add(Restrictions.le("medidor.anoFabricacao", acaoComandoEstendida.getAnoFabricacaoFim()));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoConsistirLeituraProcessadoSemAnormalidade(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoConsistirLeituraProcessadoSemAnormalidade(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" as pontoConsumo, ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" as hc ");
		hql.append(" where pontoConsumo.chavePrimaria = hc.pontoConsumo.chavePrimaria ");
		hql.append(" and hc.numeroCiclo = :numeroCiclo ");
		hql.append(" and hc.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and hc.anormalidadeConsumo is null ");
		hql.append(" and hc.habilitado = true ");
		hql.append(" and hc.indicadorConsumoCiclo = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesFaturamento", filtro.get("anoMes"));

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoConsistirLeituraProcessadoAnormalidade(java.util.Map)
	 */
	@Override
	public Collection<PontoConsumo> consultarPontosConsumoConsistirLeituraProcessadoAnormalidade(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct hc.pontoConsumo from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" as hc, ");
		hql.append(getClasseEntidadeLeituraMovimento().getSimpleName());
		hql.append(" as l ");
		hql.append(" join  hc.pontoConsumo.rota r");
		hql.append("  left join hc.anormalidadeConsumo ac");
		hql.append(" where (hc.pontoConsumo.chavePrimaria = hc.pontoConsumo.chavePrimaria " +
				"or l.pontoConsumo.chavePrimaria = hc.pontoConsumo.chavePrimaria) ");
		hql.append(" and hc.numeroCiclo = :numeroCiclo ");
		hql.append(" and ac.bloquearFaturamento = :bloqueiaFaturamento ");
		hql.append(" and hc.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and hc.anormalidadeConsumo is not null ");
		hql.append(" and hc.habilitado = true ");
		hql.append(" and r.grupoFaturamento.chavePrimaria = :grupoFaturamento ");
		hql.append(" and hc.indicadorConsumoCiclo = true ");


		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("grupoFaturamento", filtro.get(GRUPO_FATURAMENTO));
		query.setParameter("anoMesFaturamento", filtro.get("anoMes"));
		query.setParameter("bloqueiaFaturamento", filtro.get("bloqueiaFaturamento"));

		if(query.list().isEmpty()){
			StringBuilder newHql = new StringBuilder();
			newHql.append(" select distinct l.pontoConsumo from ");
			newHql.append(getClasseEntidadeLeituraMovimento().getSimpleName());
			newHql.append(" as l, ");
			newHql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
			newHql.append(" as hc ");
			newHql.append(" join  l.pontoConsumo.rota r");
			newHql.append(" left join l.anormalidadeConsumo ac");
			newHql.append(" where (hc.pontoConsumo.chavePrimaria = hc.pontoConsumo.chavePrimaria " +
					"or l.pontoConsumo.chavePrimaria = hc.pontoConsumo.chavePrimaria) ");
			newHql.append(" and l.ciclo = :numeroCiclo ");
			newHql.append(" and ac.bloquearFaturamento = :bloqueiaFaturamento ");
			newHql.append(" and l.anoMesFaturamento = :anoMesFaturamento ");
			newHql.append(" and l.anormalidadeConsumo is not null ");
			newHql.append(" and l.habilitado = true ");
			newHql.append(" and r.grupoFaturamento.chavePrimaria = :grupoFaturamento ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(newHql.toString());

			query.setParameter("numeroCiclo", filtro.get("ciclo"));
			query.setParameter("grupoFaturamento", filtro.get(GRUPO_FATURAMENTO));
			query.setParameter("anoMesFaturamento", filtro.get("anoMes"));
			query.setParameter("bloqueiaFaturamento", filtro.get("bloqueiaFaturamento"));

		}

		return query.list();
	}
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#remanejarPontoConsumo(java.lang.Long[], java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> remanejarPontoConsumo(Long[] idPontosConsumoASeremRemanejados, Long idRotaDestino) throws GGASException {

		List<PontoConsumo> pontosConsumoRemanejados = new ArrayList<>();
		ControladorRota controladorRota =
						(ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
		Rota rotaNova = (Rota) controladorRota.obter(idRotaDestino);

		return remanejarRota(idPontosConsumoASeremRemanejados, pontosConsumoRemanejados, rotaNova);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoConsistirLeituraProntoProcessar(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoConsistirLeituraProntoProcessar(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);

		hql.append(" (select count(mh.chavePrimaria) from ");
		hql.append(getClasseEntidadeHistoricoMedicao().getSimpleName());
		hql.append(" mh ");
		hql.append(" where pontoConsumo.chavePrimaria = mh.pontoConsumo.chavePrimaria ");
		hql.append(" and mh.numeroCiclo = :numeroCiclo and mh.anoMesLeitura = :anoMesLeitura ");
		hql.append(" and mh.habilitado = true and mh.anormalidadeLeituraInformada is null ");
		hql.append(" and mh.anormalidadeLeituraFaturada is null) > 0 ");

		hql.append(" and ");

		hql.append(" (select count(mh.chavePrimaria) from ");
		hql.append(getClasseEntidadeHistoricoMedicao().getSimpleName());
		hql.append(" mh ");
		hql.append(" where pontoConsumo.chavePrimaria = mh.pontoConsumo.chavePrimaria ");
		hql.append(" and mh.numeroCiclo = :numeroCiclo and mh.anoMesLeitura = :anoMesLeitura ");
		hql.append(" and mh.habilitado = true ");
		hql.append(" and (mh.anormalidadeLeituraInformada is not null or mh.anormalidadeLeituraFaturada is not null) ) = 0");

		hql.append(" and ");

		hql.append(" (select count(hc.chavePrimaria) from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" hc ");
		hql.append(" where pontoConsumo.chavePrimaria = hc.pontoConsumo.chavePrimaria ");
		hql.append(" and hc.numeroCiclo = :numeroCiclo and hc.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and hc.habilitado = true and hc.indicadorConsumoCiclo = true) = 0 ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesFaturamento", filtro.get("anoMes"));
		query.setParameter("anoMesLeitura", filtro.get("anoMes"));

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoRegistrarLeituraProcessadoSemAnormalidade(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoRegistrarLeituraProcessadoSemAnormalidade(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);

		hql.append(" (select count(mh.chavePrimaria) from ");
		hql.append(getClasseEntidadeHistoricoMedicao().getSimpleName());
		hql.append(" mh ");
		hql.append(" where pontoConsumo.chavePrimaria = mh.pontoConsumo.chavePrimaria ");
		hql.append(" and mh.numeroCiclo = :numeroCiclo and mh.anoMesLeitura = :anoMesLeitura ");
		hql.append(" and mh.habilitado = true and mh.anormalidadeLeituraInformada is null ");
		hql.append(" and mh.anormalidadeLeituraFaturada is null) > 0 ");

		hql.append(" and ");

		hql.append(" (select count(mh.chavePrimaria) from ");
		hql.append(getClasseEntidadeHistoricoMedicao().getSimpleName());
		hql.append(" mh ");
		hql.append(" where pontoConsumo.chavePrimaria = mh.pontoConsumo.chavePrimaria ");
		hql.append(" and mh.numeroCiclo = :numeroCiclo and mh.anoMesLeitura = :anoMesLeitura ");
		hql.append(" and mh.habilitado = true ");
		hql.append(" and (mh.anormalidadeLeituraInformada is not null or mh.anormalidadeLeituraFaturada is not null) ) = 0");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesLeitura", filtro.get("anoMes"));

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoRegistrarLeituraProcessadoAnormalidade(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoRegistrarLeituraProcessadoAnormalidade(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);

		hql.append(" (select count(mh.chavePrimaria) from ");
		hql.append(getClasseEntidadeHistoricoMedicao().getSimpleName());
		hql.append(" mh ");
		hql.append(" where pontoConsumo.chavePrimaria = mh.pontoConsumo.chavePrimaria ");
		hql.append(" and mh.numeroCiclo = :numeroCiclo and mh.anoMesLeitura = :anoMesLeitura ");
		hql.append(" and mh.habilitado = true and (mh.anormalidadeLeituraInformada is not null ");
		hql.append(" and mh.anormalidadeLeituraFaturada is not null)) > 0 ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesLeitura", filtro.get("anoMes"));

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoRegistrarLeituraProntoProcessar(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoRegistrarLeituraProntoProcessar(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);

		hql.append(" (select count(sumd.chavePrimaria) from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoDiaria().getSimpleName());
		hql.append(" sumd ");
		hql.append(" where sumd.codigoPontoConsumoSupervisorio = pontoConsumo.codigoPontoConsumoSupervisorio ");
		hql.append(" and sumd.numeroCiclo = :numeroCiclo and sumd.dataReferencia = :anoMesLeitura ");
		hql.append(" and sumd.habilitado = true and sumd.indicadorIntegrado = true ");
		hql.append(" and sumd.indicadorProcessado = true) > 0 ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesLeitura", filtro.get("anoMes"));

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoRegistrarLeituraProntoProcessarColetor(java.util
	 * .Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoRegistrarLeituraProntoProcessarColetor(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);

		hql.append(" (select count(lemo.chavePrimaria) from ");
		hql.append(getClasseEntidadeLeituraMovimento().getSimpleName());
		hql.append(" lemo ");
		hql.append(" where lemo.situacaoLeitura.chavePrimaria = :idLeituraMovimentoEmitir ");
		hql.append(" and lemo.anoMesFaturamento = :anoMesFaturamento and lemo.ciclo = :ciclo ");
		hql.append(" and lemo.habilitado = true ");
		hql.append(" and lemo.pontoConsumo.chavePrimaria = pontoConsumo.chavePrimaria) > 0 ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("ciclo", filtro.get("ciclo"));
		query.setParameter("anoMesFaturamento", filtro.get("anoMes"));
		query.setParameter("idLeituraMovimentoEmitir", filtro.get("idLeituraMovimentoEmitir"));

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoFaturarGrupoProcessadoSemAnormalidade(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoFaturarGrupoProcessadoSemAnormalidade(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);

		hql.append(" (select count(fatura.chavePrimaria) from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");
		hql.append(" where fatura.pontoConsumo.chavePrimaria = pontoConsumo.chavePrimaria ");
		hql.append(" and fatura.anoMesReferencia = :anoMesReferencia and fatura.numeroCiclo = :numeroCiclo ");
		hql.append(" and fatura.habilitado = true ");
		hql.append(" and cast(fatura.creditoDebitoSituacao.chavePrimaria as string) not in (:listaExcecaoCreditoDebitoSituacao)) > 0 ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesReferencia", filtro.get("anoMes"));
		query.setParameter("listaExcecaoCreditoDebitoSituacao", String.valueOf(CreditoDebitoSituacao.CANCELADA_POR_REFATURAMENTO) + ","
						+ CreditoDebitoSituacao.CANCELADA);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoFaturarGrupoProcessadoAnormalidade(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoFaturarGrupoProcessadoAnormalidade(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);

		hql.append(" (select count(fah.chavePrimaria) from ");
		hql.append(getClasseEntidadeHistoricoAnomaliaFaturamento().getSimpleName());
		hql.append(" as fah, ");
		hql.append(getClasseEntidadeFaturamentoAnormalidade().getSimpleName());
		hql.append(" as fa ");
		hql.append(" where fa.chavePrimaria = fah.faturamentoAnormalidade.chavePrimaria ");
		hql.append(" and fah.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and fah.numeroCiclo = :numeroCiclo ");
		hql.append(" and fah.analisada = false ");
		hql.append(" and fah.habilitado = true ");
		hql.append(" and (fa.indicadorImpedeFaturamento = true or fa.indicadorBloqueiaFaturamento = true) ");
		hql.append(" and fah.pontoConsumo.chavePrimaria = pontoConsumo.chavePrimaria) > 0 ");

		hql.append(" and ");

		hql.append(" (select count(fatura.chavePrimaria) from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" as fatura ");
		hql.append(WHERE);
		hql.append(" fatura.pontoConsumo.chavePrimaria = pontoConsumo.chavePrimaria ");
		hql.append(" and fatura.anoMesReferencia = :anoMesReferencia ");
		hql.append(" and fatura.numeroCiclo = :numeroCiclo ");
		hql.append(" and fatura.habilitado = true ");
		hql.append(" and cast(fatura.creditoDebitoSituacao.chavePrimaria as string) not in (:listaExcecaoCreditoDebitoSituacao)) = 0 ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesFaturamento", filtro.get("anoMes"));
		query.setParameter("anoMesReferencia", filtro.get("anoMes"));
		query.setParameter("listaExcecaoCreditoDebitoSituacao", String.valueOf(CreditoDebitoSituacao.CANCELADA_POR_REFATURAMENTO) + ","
						+ CreditoDebitoSituacao.CANCELADA);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoFaturarGrupoProntoProcessar(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoFaturarGrupoProntoProcessar(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);

		hql.append(" (select count(fah.chavePrimaria) from ");
		hql.append(getClasseEntidadeHistoricoAnomaliaFaturamento().getSimpleName());
		hql.append(" as fah, ");
		hql.append(getClasseEntidadeFaturamentoAnormalidade().getSimpleName());
		hql.append(" as fa ");

		hql.append(" where fa.chavePrimaria = fah.faturamentoAnormalidade.chavePrimaria ");
		hql.append(" and fah.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and fah.numeroCiclo = :numeroCiclo ");
		hql.append(" and fah.analisada = false ");
		hql.append(" and fah.habilitado = true ");
		hql.append(" and (fa.indicadorImpedeFaturamento = true or fa.indicadorBloqueiaFaturamento = true) ");
		hql.append(" and pontoConsumo.chavePrimaria = fah.pontoConsumo.chavePrimaria) = 0 ");

		hql.append(" and ");

		hql.append(" (select count(fatura.chavePrimaria) from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");
		hql.append(" where fatura.anoMesReferencia = :anoMesReferencia ");
		hql.append(" and fatura.numeroCiclo = :numeroCiclo ");
		hql.append(" and fatura.pontoConsumo.chavePrimaria = pontoConsumo.chavePrimaria ");
		hql.append(" and fatura.habilitado = true ");
		hql.append(" and cast(fatura.creditoDebitoSituacao.chavePrimaria as string) not in (:listaExcecaoCreditoDebitoSituacao)) = 0 ");

		hql.append(" and ");

		hql.append(" (select count(hc.chavePrimaria) from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" as hc, ");
		hql.append(getClasseEntidadeAnormalidadeConsumo().getSimpleName());
		hql.append(" as ac ");
		hql.append(WHERE);
		hql.append(" (ac.chavePrimaria = hc.anormalidadeConsumo.chavePrimaria or hc.anormalidadeConsumo is null) ");
		hql.append(" and hc.chavePrimaria = pontoConsumo.chavePrimaria ");
		hql.append(" and hc.numeroCiclo = :numeroCiclo ");
		hql.append(" and hc.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and hc.habilitado = true ");
		hql.append(" and hc.indicadorConsumoCiclo = true ");
		hql.append(" and hc.indicadorFaturamento = false ");
		hql.append(" and (hc.anormalidadeConsumo is null or ac.bloquearFaturamento = false)) > 0 ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesReferencia", filtro.get("anoMes"));
		query.setParameter("anoMesFaturamento", filtro.get("anoMes"));
		query.setParameter("listaExcecaoCreditoDebitoSituacao", String.valueOf(CreditoDebitoSituacao.CANCELADA_POR_REFATURAMENTO) + ","
						+ CreditoDebitoSituacao.CANCELADA);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoEmitirFaturaProcessadoSemAnormalidade(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoEmitirFaturaProcessadoSemAnormalidade(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);

		hql.append(" (select count(fatura.chavePrimaria) from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" as fatura, ");
		hql.append(getClasseEntidadeFaturaImpressao().getSimpleName());
		hql.append(" as faturaImpressao ");
		hql.append(" where fatura.chavePrimaria = faturaImpressao.fatura.chavePrimaria ");
		hql.append(" and fatura.anoMesReferencia = :anoMesReferencia ");
		hql.append(" and fatura.numeroCiclo = :numeroCiclo ");
		hql.append(" and fatura.habilitado = true ");
		hql.append(" and cast(fatura.creditoDebitoSituacao.chavePrimaria as string) not in (:listaExcecaoCreditoDebitoSituacao) ");
		hql.append(" and faturaImpressao.indicadorImpressao = true ");
		hql.append(" and fatura.pontoConsumo.chavePrimaria = pontoConsumo.chavePrimaria) > 0 ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesReferencia", filtro.get("anoMes"));
		query.setParameter("listaExcecaoCreditoDebitoSituacao", String.valueOf(CreditoDebitoSituacao.CANCELADA_POR_REFATURAMENTO) + ","
						+ CreditoDebitoSituacao.CANCELADA);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoEmitirFaturaProcessadoAnormalidade(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoEmitirFaturaProcessadoAnormalidade(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);

		hql.append(" (select count(faturamentoAnomaliaHistorico.chavePrimaria) from ");
		hql.append(getClasseEntidadeHistoricoAnomaliaFaturamento().getSimpleName());
		hql.append(" as faturamentoAnomaliaHistorico, ");
		hql.append(getClasseEntidadeFaturamentoAnormalidade().getSimpleName());
		hql.append(" as faturamentoAnormalidade ");
		hql.append(" where faturamentoAnormalidade.chavePrimaria = faturamentoAnomaliaHistorico.faturamentoAnormalidade.chavePrimaria ");
		hql.append(" and faturamentoAnomaliaHistorico.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and faturamentoAnomaliaHistorico.numeroCiclo = :numeroCiclo ");
		hql.append(" and faturamentoAnomaliaHistorico.analisada = false ");
		hql.append(" and faturamentoAnomaliaHistorico.habilitado = true ");
		hql.append(" and (faturamentoAnormalidade.indicadorImpedeFaturamento = true or "
						+ "faturamentoAnormalidade.indicadorBloqueiaFaturamento = true) ");
		hql.append(" and faturamentoAnomaliaHistorico.pontoConsumo.chavePrimaria = pontoConsumo.chavePrimaria) > 0 ");

		hql.append(" and ");

		hql.append(" (select count(fatura.chavePrimaria) from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");
		hql.append(" where fatura.anoMesReferencia = :anoMesReferencia ");
		hql.append(" and fatura.numeroCiclo = :numeroCiclo ");
		hql.append(" and fatura.habilitado = true ");
		hql.append(" and fatura.numeroCiclo = :numeroCiclo ");
		hql.append(" and cast(fatura.creditoDebitoSituacao.chavePrimaria as string) not in (:listaExcecaoCreditoDebitoSituacao) ");
		hql.append(" and fatura.pontoConsumo.chavePrimaria = pontoConsumo.chavePrimaria) = 0");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesFaturamento", filtro.get("anoMes"));
		query.setParameter("anoMesReferencia", filtro.get("anoMes"));
		query.setParameter("listaExcecaoCreditoDebitoSituacao", String.valueOf(CreditoDebitoSituacao.CANCELADA_POR_REFATURAMENTO) + ","
						+ CreditoDebitoSituacao.CANCELADA);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoEmitirFaturaProntoProcessar(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoEmitirFaturaProntoProcessar(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(WHERE);

		hql.append(" (select count(fatura.chavePrimaria) from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" as fatura, ");
		hql.append(getClasseEntidadeFaturaImpressao().getSimpleName());
		hql.append(" as faturaImpressao ");
		hql.append(" where fatura.anoMesReferencia = :anoMesReferencia ");
		hql.append(" and fatura.numeroCiclo = :numeroCiclo ");
		hql.append(" and fatura.habilitado = true ");
		hql.append(" and fatura.pontoConsumo.chavePrimaria = pontoConsumo.chavePrimaria ");
		hql.append(" and cast(fatura.creditoDebitoSituacao.chavePrimaria as string) not in (:listaExcecaoCreditoDebitoSituacao) ");
		hql.append(" and (faturaImpressao.indicadorImpressao is null or faturaImpressao.indicadorImpressao = false)) > 0 ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesReferencia", filtro.get("anoMes"));
		query.setParameter("listaExcecaoCreditoDebitoSituacao", String.valueOf(CreditoDebitoSituacao.CANCELADA_POR_REFATURAMENTO) + ","
						+ CreditoDebitoSituacao.CANCELADA);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarPontoConsumoPorCliente(java.lang.String)
	 */
	@Override
	public List<PontoConsumo> listarPontoConsumoPorCliente(String documento) {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		String cpfCnpj = Util.removerCaracteresEspeciais(documento);
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel ");
		hql.append(" inner join fetch pontoConsumo.segmento ");
		hql.append(" inner join fetch  contratoPonto.contrato.clienteAssinatura cliente ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor.medidor ");
		if (cpfCnpj.length() == TAMANHO_CPF) {
			hql.append(" where cliente.cpf = :cpfCnpj ");
		} else {
			hql.append(" where cliente.cnpj = :cpfCnpj ");
		}

		hql.append(" and contratoPonto.contrato.situacao = :idSituacaoContrato ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("cpfCnpj", cpfCnpj);

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idSituacaoContrato =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
		query.setLong("idSituacaoContrato", idSituacaoContrato);

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = query.list();

		List<PontoConsumo> listaPontoConsumoAux = new ArrayList<>();

		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

			listaPontoConsumoAux.add(contratoPontoConsumo.getPontoConsumo());
		}

		return listaPontoConsumoAux;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#validarFaturaAvulso(br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void validarFaturaAvulso(PontoConsumo pontoConsumo) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoSegmentoVeicular =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_SEGMENTO_FATURA_AVULSO);

		String codigoMedidorInstalado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_INSTALADO);

		if (pontoConsumo != null) {
			if (!String.valueOf(pontoConsumo.getSegmento().getChavePrimaria()).equals(codigoSegmentoVeicular)) {
				throw new NegocioException(Constantes.ERRO_SEGMENTO_NAO_PERMITIDO_FATURA_AVULSO, pontoConsumo.getDescricao());
			}

			if (pontoConsumo.getInstalacaoMedidor() != null) {
				VazaoCorretor vazao = pontoConsumo.getInstalacaoMedidor().getVazaoCorretor();
				if (vazao != null && String.valueOf(vazao.getSituacaoMedidor().getChavePrimaria()).equals(codigoMedidorInstalado)) {
					throw new NegocioException(Constantes.ERRO_NAO_E_POSSIVEL_INCLUIR_FATURA_AVULSO_CORRETOR_VAZAO, true);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#removerImovelFilhoComPaiMedicaoColetiva(java.util.List)
	 */
	@Override
	public List<PontoConsumo> removerImovelFilhoComPaiMedicaoColetiva(List<PontoConsumo> listaPontoConsumo) throws GGASException {

		ControladorImovel controladorImovel =
						(ControladorImovel) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);

		List<PontoConsumo> listaRemoverPontoConsumo = new ArrayList<>();
		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			Imovel imovelPontoConsumo = pontoConsumo.getImovel();
			boolean condominio = imovelPontoConsumo.getCondominio();
			if (!condominio) {
				Imovel imovelPai = imovelPontoConsumo.getImovelCondominio();
				if (imovelPai != null) {
					Map<String, Object> filtro = new HashMap<>();
					filtro.put("chavePrimaria", imovelPai.getChavePrimaria());
					Collection<Imovel> listaImovel = controladorImovel.consultarImoveis(filtro);

					imovelPai = listaImovel.iterator().next();
					if (imovelPai.getModalidadeMedicaoImovel() != null) {
						Integer medicaoImovel = imovelPai.getModalidadeMedicaoImovel().getCodigo();
						this.adicionarPontoConsumo(listaRemoverPontoConsumo, pontoConsumo, medicaoImovel);
					}
				}
			}
		}

		if (!listaRemoverPontoConsumo.isEmpty()) {
			listaPontoConsumo.removeAll(listaRemoverPontoConsumo);
		}

		return listaPontoConsumo;

	}

	private void adicionarPontoConsumo(List<PontoConsumo> listaRemoverPontoConsumo, PontoConsumo pontoConsumo, Integer medicaoImovel){

		if (medicaoImovel == ModalidadeMedicaoImovel.COLETIVA) {
			listaRemoverPontoConsumo.add(pontoConsumo);
		}
	}


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#gerarRelatorioPrevisaoCaptacao(br.com.ggas.web.relatorio.faturamento.
	 * PesquisaRelatorioPrevisaoCaptacaoVO, br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorioPrevisaoCaptacao(PesquisaRelatorioPrevisaoCaptacaoVO pesquisaRelatorioPrevisaoCaptacaoVO,
					FormatoImpressao formatoImpressao) throws NegocioException {

		ControladorSegmento controladorSegmento = ServiceLocator.getInstancia().getControladorSegmento();
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select pocn.segmento.chavePrimaria, pocn.segmento.descricao, count(*) as quantidade_ativados, "
						+ "EXTRACT(MONTH FROM mein.dataAtivacao) as mn");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pocn ");
		hql.append(" inner join pocn.instalacaoMedidor mein");
		hql.append(" inner join pocn.segmento segm ");
		hql.append(" where EXTRACT(YEAR FROM mein.dataAtivacao) = :anoReferencia ");
		if (pesquisaRelatorioPrevisaoCaptacaoVO.getSegmento() != null && pesquisaRelatorioPrevisaoCaptacaoVO.getSegmento() != -1) {
			hql.append(" and segm.chavePrimaria = :segmento ");
		}
		hql.append(" group by pocn.segmento.chavePrimaria, pocn.segmento.descricao, EXTRACT(MONTH FROM mein.dataAtivacao) ");
		hql.append(" order by pocn.segmento.chavePrimaria, mn asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (pesquisaRelatorioPrevisaoCaptacaoVO.getSegmento() != null && pesquisaRelatorioPrevisaoCaptacaoVO.getSegmento() != -1) {
			query.setLong("segmento", pesquisaRelatorioPrevisaoCaptacaoVO.getSegmento());
		}
		query.setString("anoReferencia", pesquisaRelatorioPrevisaoCaptacaoVO.getAnoReferencia());
		List<Object[]> objetos = query.list();

		Iterator<Object[]> it = objetos.iterator();
		List<RelatorioPrevisaoCaptacaoVO> lista = new ArrayList<>();
		RelatorioPrevisaoCaptacaoVO vo = null;
		Long chaveSegmento = 0L;
		SegmentoPrevisaoCaptacao segmentoPrevisaoCaptacao = new SegmentoPrevisaoCaptacao();
		Segmento segmento = null;
		while (it.hasNext()) {
			Object[] obj = it.next();
			if (!chaveSegmento.equals(obj[0])) {
				segmentoPrevisaoCaptacao.setAnoReferencia(Integer.valueOf(pesquisaRelatorioPrevisaoCaptacaoVO.getAnoReferencia()));
				vo = new RelatorioPrevisaoCaptacaoVO();
				chaveSegmento = (Long) obj[0];
				vo.setSegmento((String) obj[1]);
				segmento = controladorSegmento.obterSegmento(chaveSegmento);
				segmentoPrevisaoCaptacao.setSegmento(segmento);
				Collection<SegmentoPrevisaoCaptacao> segmentoPrevisaoList =
								controladorPrevisaoCaptacao.consultarSegmentoPrevisaoCaptacao(segmentoPrevisaoCaptacao);
				if (segmentoPrevisaoList != null && !segmentoPrevisaoList.isEmpty()) {
					segmentoPrevisaoCaptacao =
									(SegmentoPrevisaoCaptacao) ((List) controladorPrevisaoCaptacao
													.consultarSegmentoPrevisaoCaptacao(segmentoPrevisaoCaptacao)).get(0);
				} else {
					segmentoPrevisaoCaptacao = new SegmentoPrevisaoCaptacao();
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoJaneiro() != null) {
					vo.setPrevisaoJaneiro(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoJaneiro()));
				} else {
					vo.setPrevisaoJaneiro((long) 0);
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoFevereiro() != null) {
					vo.setPrevisaoFevereiro(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoFevereiro()));
				} else {
					vo.setPrevisaoFevereiro((long) 0);
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoMarco() != null) {
					vo.setPrevisaoMarco(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoMarco()));
				} else {
					vo.setPrevisaoMarco((long) 0);
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoAbril() != null) {
					vo.setPrevisaoAbril(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoAbril()));
				} else {
					vo.setPrevisaoAbril((long) 0);
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoMaio() != null) {
					vo.setPrevisaoMaio(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoMaio()));
				} else {
					vo.setPrevisaoMaio((long) 0);
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoJunho() != null) {
					vo.setPrevisaoJunho(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoJunho()));
				} else {
					vo.setPrevisaoJunho((long) 0);
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoJulho() != null) {
					vo.setPrevisaoJulho(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoJulho()));
				} else {
					vo.setPrevisaoJulho((long) 0);
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoAgosto() != null) {
					vo.setPrevisaoAgosto(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoAgosto()));
				} else {
					vo.setPrevisaoAgosto((long) 0);
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoSetembro() != null) {
					vo.setPrevisaoSetembro(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoSetembro()));
				} else {
					vo.setPrevisaoSetembro((long) 0);
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoOutubro() != null) {
					vo.setPrevisaoOutubro(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoOutubro()));
				} else {
					vo.setPrevisaoOutubro((long) 0);
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoNovembro() != null) {
					vo.setPrevisaoNovembro(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoNovembro()));
				} else {
					vo.setPrevisaoNovembro((long) 0);
				}
				if (segmentoPrevisaoCaptacao.getPrevisaoDezembro() != null) {
					vo.setPrevisaoDezembro(Long.valueOf(segmentoPrevisaoCaptacao.getPrevisaoDezembro()));
				} else {
					vo.setPrevisaoDezembro((long) 0);
				}
				lista.add(vo);
			}
			Long quantidadeCaptacao = (Long) obj[INDICE_QTD_CAPTACAO];
			if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == 1) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoJaneiro(quantidadeCaptacao);
				} else {
					vo.setCaptacaoJaneiro((long) 0);
				}
			} else if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == FEVEREIRO) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoFevereiro(quantidadeCaptacao);
				} else {
					vo.setCaptacaoFevereiro((long) 0);
				}
			} else if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == MARCO) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoMarco(quantidadeCaptacao);
				} else {
					vo.setCaptacaoMarco((long) 0);
				}
			} else if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == ABRIL) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoAbril(quantidadeCaptacao);
				} else {
					vo.setCaptacaoAbril((long) 0);
				}
			} else if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == MAIO) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoMaio(quantidadeCaptacao);
				} else {
					vo.setCaptacaoMaio((long) 0);
				}
			} else if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == JUNHO) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoJunho(quantidadeCaptacao);
				} else {
					vo.setCaptacaoJunho((long) 0);
				}
			} else if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == JULHO) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoJulho(quantidadeCaptacao);
				} else {
					vo.setCaptacaoJulho((long) 0);
				}
			} else if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == AGOSTO) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoAgosto(quantidadeCaptacao);
				} else {
					vo.setCaptacaoAgosto((long) 0);
				}
			} else if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == SETEMBRO) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoSetembro(quantidadeCaptacao);
				} else {
					vo.setCaptacaoSetembro((long) 0);
				}
			} else if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == OUTUBRO) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoOutubro(quantidadeCaptacao);
				} else {
					vo.setCaptacaoOutubro((long) 0);
				}
			} else if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == NOVEMBRO) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoNovembro(quantidadeCaptacao);
				} else {
					vo.setCaptacaoNovembro((long) 0);
				}
			} else if ((Integer) obj[INDICE_IDENTIFICACAO_MES] == DEZEMBRO) {
				if (quantidadeCaptacao != null) {
					vo.setCaptacaoDezembro(quantidadeCaptacao);
				} else {
					vo.setCaptacaoDezembro((long) 0);
				}
			}
		}

		RelatorioPrevisaoCaptacaoWrapper wrapper = new RelatorioPrevisaoCaptacaoWrapper();
		wrapper.setListaCaptacoes(lista);

		Map<String, Object> parametros = new HashMap<>();
		ControladorEmpresa controladorEmpresa =
						(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		parametros.put("imagem",
						Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		if (!pesquisaRelatorioPrevisaoCaptacaoVO.getAnoReferencia().isEmpty()
						|| (pesquisaRelatorioPrevisaoCaptacaoVO.getSegmento() != null && pesquisaRelatorioPrevisaoCaptacaoVO.getSegmento() != -1)) {
			parametros.put("exibirFiltros", Boolean.TRUE);
		}
		if (!pesquisaRelatorioPrevisaoCaptacaoVO.getAnoReferencia().isEmpty()) {
			parametros.put("anoReferencia", pesquisaRelatorioPrevisaoCaptacaoVO.getAnoReferencia());
		}
		if (pesquisaRelatorioPrevisaoCaptacaoVO.getSegmento() != null && pesquisaRelatorioPrevisaoCaptacaoVO.getSegmento() != -1) {
			segmento = controladorSegmento.obterSegmento(pesquisaRelatorioPrevisaoCaptacaoVO.getSegmento());
			parametros.put("segmento", segmento.getDescricao());
		}
		Collection<Object> collRelatorio = new ArrayList<>();
		collRelatorio.add(wrapper);
		if (lista != null && !lista.isEmpty()) {
			return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_PREVISAO_CAPTACAO, formatoImpressao);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#gerarRelatorioComercialMensal(br.com.ggas.web.relatorio.faturamento.
	 * PesquisaRelatorioComercialMensalVO, br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorioComercialMensal(PesquisaRelatorioComercialMensalVO pesquisaRelatorioComercialMensalVO,
					FormatoImpressao formatoImpressao) throws NegocioException {

		ServiceLocator.getInstancia().getControladorSegmento();
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select pocn.chavePrimaria, ");
		hql.append(" (select pontoConsumo from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo where pontoConsumo.chavePrimaria = pocn.chavePrimaria) ");
		hql.append(" , (select contratoPontoConsumo from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPontoConsumo ");
		hql.append(" left join contratoPontoConsumo.contrato contrato");
		hql.append(" left join contrato.clienteAssinatura cliente ");
		hql.append(" where contratoPontoConsumo.pontoConsumo.chavePrimaria = pocn.chavePrimaria and rownum = 1) ");
		hql.append(" , (select max(instalacaoMedidor.dataAtivacao) from ");
		hql.append(getClasseEntidadeInstalacaoMedidor().getSimpleName());
		hql.append(" instalacaoMedidor where instalacaoMedidor.pontoConsumo.chavePrimaria = pocn.chavePrimaria ");
		hql.append(" ) ");
		hql.append(" , cohi.anoMesFaturamento ");
		hql.append(" , sum(cohi.consumoApurado) ");
		hql.append(" , (select sum(coHistorico.consumoApurado) from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" coHistorico where coHistorico.pontoConsumo.chavePrimaria = pocn.chavePrimaria and "
						+ "to_char(coHistorico.anoMesFaturamento) like :anoAnteriorReferencia ");
		hql.append(" ) ");
		hql.append(" , (select sum(fatItem.valorTotal) from ");
		hql.append(getClasseEntidadeFaturaItem().getSimpleName());
		hql.append(" fatItem where fatItem.fatura.pontoConsumo.chavePrimaria = pocn.chavePrimaria and "
						+ "to_char(EXTRACT(YEAR FROM fatItem.fatura.dataEmissao)) like :anoReferencia ");
		hql.append(" ) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" cohi ");
		hql.append(" inner join cohi.pontoConsumo pocn ");
		hql.append(" inner join pocn.imovel imov ");

		hql.append(" where to_char(cohi.anoMesFaturamento) like :anoReferencia ");
		if (pesquisaRelatorioComercialMensalVO.getIdImovel() != null) {
			hql.append(" and imov.chavePrimaria = :idImovel ");
		}
		if (pesquisaRelatorioComercialMensalVO.getSegmento() != null && pesquisaRelatorioComercialMensalVO.getSegmento() != -1) {
			hql.append(" and pocn.segmento.chavePrimaria = :idSegmento ");
		}
		if (pesquisaRelatorioComercialMensalVO.getIdCliente() != null) {
			hql.append(" and exists (select contratoPontoConsumo from ");
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(" contratoPontoConsumo ");
			hql.append(" left join contratoPontoConsumo.contrato contrato");
			hql.append(" left join contrato.clienteAssinatura cliente ");
			hql.append(" where contratoPontoConsumo.pontoConsumo.chavePrimaria = pocn.chavePrimaria and cliente.chavePrimaria = :idCliente) ");
		}
		hql.append(" group by cohi.anoMesFaturamento, pocn.chavePrimaria");
		hql.append(" order by pocn.chavePrimaria, cohi.anoMesFaturamento ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("anoReferencia", pesquisaRelatorioComercialMensalVO.getAnoReferencia() + "%");
		Integer anoAnterior = Integer.parseInt(pesquisaRelatorioComercialMensalVO.getAnoReferencia()) - 1;
		query.setParameter("anoAnteriorReferencia", anoAnterior + "%");

		if (pesquisaRelatorioComercialMensalVO.getIdImovel() != null) {
			query.setParameter("idImovel", pesquisaRelatorioComercialMensalVO.getIdImovel());
		}
		if (pesquisaRelatorioComercialMensalVO.getSegmento() != null && pesquisaRelatorioComercialMensalVO.getSegmento() != -1) {
			query.setParameter(ID_SEGMENTO, pesquisaRelatorioComercialMensalVO.getSegmento());
		}
		if (pesquisaRelatorioComercialMensalVO.getIdCliente() != null) {
			query.setParameter("idCliente", pesquisaRelatorioComercialMensalVO.getIdCliente());
		}

		List<Object[]> objetos = query.list();

		Iterator<Object[]> it = objetos.iterator();
		List<RelatorioComercialMensalVO> lista = new ArrayList<>();
		RelatorioComercialMensalVO vo = null;
		Long chaveImovel = 0L;
		ContratoPontoConsumo contratoPontoConsumo = null;
		BigDecimal consumo = null;
		BigDecimal consumoAnoAnterior = null;
		BigDecimal totalAno = null;
		BigDecimal totalAnoAnterior = null;
		BigDecimal valorTotal = null;
		BigDecimal valor = null;
		Imovel imovel = null;
		PontoConsumo pontoConsumo = null;
		while (it.hasNext()) {
			Object[] obj = it.next();

			consumoAnoAnterior = (BigDecimal) obj[INDICE_CONSUMO_ANTERIOR];
			valor = (BigDecimal) obj[INDICE_VALOR];
			if (!chaveImovel.equals(obj[0])) {
				vo = new RelatorioComercialMensalVO();
				totalAno = BigDecimal.ZERO;
				totalAnoAnterior = BigDecimal.ZERO;
				valorTotal = BigDecimal.ZERO;
				chaveImovel = (Long) obj[0];
				lista.add(vo);
			}

			if (consumoAnoAnterior != null) {
				totalAnoAnterior = totalAnoAnterior.add(consumoAnoAnterior);
			}

			if (valor != null) {
				valorTotal = valorTotal.add(valor);
			}

			if (obj[1] != null) {
				pontoConsumo = (PontoConsumo) obj[1];
				imovel = pontoConsumo.getImovel();
				if (imovel.getQuadra() != null && imovel.getQuadra().getZonaBloqueio() != null) {
					vo.setZonaBloqueio(imovel.getQuadra().getZonaBloqueio().getDescricao());
				}
				vo.setNomePontoConsumo(pontoConsumo.getDescricao());
			}

			if (obj[INDICE_CONTRATO_PONTO_CONSUMO] != null) {
				contratoPontoConsumo = (ContratoPontoConsumo) obj[INDICE_CONTRATO_PONTO_CONSUMO];
				vo.setNomeFantasia(contratoPontoConsumo.getContrato().getClienteAssinatura().getNomeFantasia());
				vo.setVolumeContratado(retonarQDCContrato(contratoPontoConsumo.getContrato()));
				if (contratoPontoConsumo.getContrato().getClienteAssinatura().getAtividadeEconomica() != null) {
					vo.setAtividadeEconomica(contratoPontoConsumo.getContrato().getClienteAssinatura().getAtividadeEconomica()
									.getDescricao());
				}
				vo.setSegmento(contratoPontoConsumo.getPontoConsumo().getSegmento().getDescricao());
				vo.setSituacao(contratoPontoConsumo.getPontoConsumo().getSituacaoConsumo().getDescricao());
				vo.setRazaoSocial(contratoPontoConsumo.getContrato().getClienteAssinatura().getNome());
				vo.setDataAssinaturaContrato(contratoPontoConsumo.getContrato().getDataAssinatura());
			}
			if (obj[INDICE_DATA_INICIO] != null) {
				vo.setDataInicioConsumo((Date) obj[INDICE_DATA_INICIO]);
			}

			HistoricoOperacaoMedidor obterUltimoBloqueio =
							getControladorMedidor().obterUltimoBloqueio(contratoPontoConsumo.getPontoConsumo().getChavePrimaria());

			Integer data = (Integer) obj[INDICE_DATA];

			BigDecimal consumoApuradoConsumoHistorico = (BigDecimal) obj[INDICE_CONSUMO_APURADO];
			BigDecimal consumoFaturaItem =
							getControladorFatura().somaMedidaConsumoPorPontoConsumoAnoMesReferencia(
											contratoPontoConsumo.getPontoConsumo().getChavePrimaria(), data);

			consumo = NumeroUtil.maior(consumoApuradoConsumoHistorico, consumoFaturaItem);

			if (consumo != null) {
				totalAno = totalAno.add(consumo);
			}

			if (obterUltimoBloqueio != null) {
				Date dataRealizada = obterUltimoBloqueio.getDataRealizada();
				vo.setDataDesligamento(dataRealizada);
			}

			String mes = data.toString().substring(INDICE_MES);
			if ("01".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoJaneiro(consumo);
				} else {
					vo.setConsumoJaneiro(BigDecimal.valueOf(0));
				}
			} else if ("02".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoFevereiro(consumo);
				} else {
					vo.setConsumoFevereiro(BigDecimal.valueOf(0));
				}
			} else if ("03".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoMarco(consumo);
				} else {
					vo.setConsumoMarco(BigDecimal.valueOf(0));
				}
			} else if ("04".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoAbril(consumo);
				} else {
					vo.setConsumoAbril(BigDecimal.valueOf(0));
				}
			} else if ("05".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoMaio(consumo);
				} else {
					vo.setConsumoMaio(BigDecimal.valueOf(0));
				}
			} else if ("06".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoJunho(consumo);
				} else {
					vo.setConsumoJunho(BigDecimal.valueOf(0));
				}
			} else if ("07".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoJulho(consumo);
				} else {
					vo.setConsumoJulho(BigDecimal.valueOf(0));
				}
			} else if ("08".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoAgosto(consumo);
				} else {
					vo.setConsumoAgosto(BigDecimal.valueOf(0));
				}
			} else if ("09".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoSetembro(consumo);
				} else {
					vo.setConsumoSetembro(BigDecimal.valueOf(0));
				}
			} else if ("10".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoOutubro(consumo);
				} else {
					vo.setConsumoOutubro(BigDecimal.valueOf(0));
				}
			} else if ("11".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoNovembro(consumo);
				} else {
					vo.setConsumoNovembro(BigDecimal.valueOf(0));
				}
			} else if ("12".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoDezembro(consumo);
				} else {
					vo.setConsumoDezembro(BigDecimal.valueOf(0));
				}
			}
			vo.setTotalAno(totalAno);
			vo.setTotalAnoAnterior(totalAnoAnterior);
			vo.setValorTotal(valorTotal);
			if (valorTotal.compareTo(BigDecimal.valueOf(0)) <= 0) {
				vo.setTarifa(BigDecimal.valueOf(0));
			} else {
				if (NumeroUtil.maiorQueZero(totalAno)) {
					vo.setTarifa(valorTotal.divide(totalAno, ESCALA_DOIS, RoundingMode.HALF_UP));
				} else {
					vo.setTarifa(BigDecimal.ZERO);
				}
			}

		}

		RelatorioComercialMensalWrapper wrapper = new RelatorioComercialMensalWrapper();
		wrapper.setLista(lista);

		Map<String, Object> parametros = new HashMap<>();
		ControladorEmpresa controladorEmpresa =
						(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		parametros.put("imagem",
						Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		if (!pesquisaRelatorioComercialMensalVO.getAnoReferencia().isEmpty()
						|| (pesquisaRelatorioComercialMensalVO.getSegmento() != null && pesquisaRelatorioComercialMensalVO.getSegmento() != -1)) {
			parametros.put("exibirFiltros", Boolean.TRUE);
		}
		if (!pesquisaRelatorioComercialMensalVO.getAnoReferencia().isEmpty()) {
			parametros.put("anoReferencia", pesquisaRelatorioComercialMensalVO.getAnoReferencia());
		}
		if (pesquisaRelatorioComercialMensalVO.getSegmento() != null && pesquisaRelatorioComercialMensalVO.getSegmento() != -1) {
			parametros.put("segmento", pesquisaRelatorioComercialMensalVO.getSegmento().toString());
		}
		Collection<Object> collRelatorio = new ArrayList<>();
		collRelatorio.add(wrapper);
		if (lista != null && !lista.isEmpty()) {
			return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_COMERCIAL_MENSAL, formatoImpressao);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#gerarRelatorioResidencialMensal(br.com.ggas.web.relatorio.faturamento.
	 * PesquisaRelatorioComercialMensalVO, br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorioResidencialMensal(PesquisaRelatorioComercialMensalVO pesquisaRelatorioComercialMensalVO,
					FormatoImpressao formatoImpressao) throws NegocioException {

		ServiceLocator.getInstancia().getControladorSegmento();
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select imov.imovelCondominio.chavePrimaria, ");
		hql.append(" (select imovel from ");
		hql.append(getClasseEntidadeImovel().getSimpleName());
		hql.append(" imovel where imovel.chavePrimaria = imov.imovelCondominio.chavePrimaria) ");
		hql.append(" , (select contratoPontoConsumo from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPontoConsumo ");
		hql.append(" left join contratoPontoConsumo.contrato contrato");
		hql.append(" left join contrato.clienteAssinatura cliente ");
		hql.append(" where contratoPontoConsumo.pontoConsumo.imovel.imovelCondominio.chavePrimaria = "
						+ "imov.imovelCondominio.chavePrimaria and rownum = 1) ");
		hql.append(" , (select max(instalacaoMedidor.dataAtivacao) from ");
		hql.append(getClasseEntidadeInstalacaoMedidor().getSimpleName());
		hql.append(" instalacaoMedidor where instalacaoMedidor.pontoConsumo.imovel.imovelCondominio.chavePrimaria = "
						+ "imov.imovelCondominio.chavePrimaria ");
		hql.append(" ) ");
		hql.append(" , cohi.anoMesFaturamento, sum(cohi.consumoApurado) ");
		hql.append(" , (select sum(coHistorico.consumoApurado) from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" coHistorico where coHistorico.pontoConsumo.imovel.imovelCondominio.chavePrimaria = imov.imovelCondominio.chavePrimaria "
						+ "and to_char(coHistorico.anoMesFaturamento) like :anoAnteriorReferencia ");
		hql.append(" ) ");
		hql.append(" , (select sum(fatItem.valorTotal) from ");
		hql.append(getClasseEntidadeFaturaItem().getSimpleName());
		hql.append(" fatItem where fatItem.fatura.pontoConsumo.imovel.imovelCondominio.chavePrimaria = imov.imovelCondominio.chavePrimaria "
						+ "and to_char(EXTRACT(YEAR FROM fatItem.fatura.dataEmissao)) like :anoReferencia ");
		hql.append(" ) ");
		hql.append(" , (select count(*) from ");
		hql.append(getClasseEntidadeImovel().getSimpleName());
		hql.append(" imovelFilho where imovelFilho.imovelCondominio.chavePrimaria = imov.imovelCondominio.chavePrimaria ");
		hql.append(" ) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" cohi ");
		hql.append(" inner join cohi.pontoConsumo pocn ");
		hql.append(" inner join pocn.imovel imov ");
		hql.append(" where to_char(cohi.anoMesFaturamento) like :anoReferencia ");
		hql.append(" and imov.imovelCondominio.chavePrimaria is not null ");
		if (pesquisaRelatorioComercialMensalVO.getIdImovel() != null) {
			hql.append(" and imov.chavePrimaria = :idImovel ");
		}
		if (pesquisaRelatorioComercialMensalVO.getSegmento() != null && pesquisaRelatorioComercialMensalVO.getSegmento() != -1) {
			hql.append(" and pocn.segmento.chavePrimaria = :idSegmento ");
		}
		if (pesquisaRelatorioComercialMensalVO.getModalidadeMedicao() != null
						&& pesquisaRelatorioComercialMensalVO.getModalidadeMedicao() != -1) {
			hql.append(" and imov.imovelCondominio.modalidadeMedicaoImovel.codigo = :modalidade ");
		}
		if (pesquisaRelatorioComercialMensalVO.getIdCliente() != null) {
			hql.append(" and exists (select contratoPontoConsumo from ");
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(" contratoPontoConsumo ");
			hql.append(" left join contratoPontoConsumo.contrato contrato");
			hql.append(" left join contrato.clienteAssinatura cliente ");
			hql.append(" where contratoPontoConsumo.pontoConsumo.imovel.imovelCondominio.chavePrimaria = "
							+ "imov.imovelCondominio.chavePrimaria and cliente.chavePrimaria = :idCliente) ");
		}
		hql.append(" group by cohi.anoMesFaturamento, imov.imovelCondominio.chavePrimaria");
		hql.append(" order by imov.imovelCondominio.chavePrimaria, cohi.anoMesFaturamento ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("anoReferencia", pesquisaRelatorioComercialMensalVO.getAnoReferencia() + "%");
		Integer anoAnterior = Integer.parseInt(pesquisaRelatorioComercialMensalVO.getAnoReferencia()) - 1;
		query.setParameter("anoAnteriorReferencia", anoAnterior + "%");
		if (pesquisaRelatorioComercialMensalVO.getIdImovel() != null) {
			query.setParameter("idImovel", pesquisaRelatorioComercialMensalVO.getIdImovel());
		}
		if (pesquisaRelatorioComercialMensalVO.getSegmento() != null && pesquisaRelatorioComercialMensalVO.getSegmento() != -1) {
			query.setParameter(ID_SEGMENTO, pesquisaRelatorioComercialMensalVO.getSegmento());
		}
		if (pesquisaRelatorioComercialMensalVO.getIdCliente() != null) {
			query.setParameter("idCliente", pesquisaRelatorioComercialMensalVO.getIdCliente());
		}

		if (pesquisaRelatorioComercialMensalVO.getModalidadeMedicao() != null
						&& pesquisaRelatorioComercialMensalVO.getModalidadeMedicao() != -1) {
			query.setParameter("modalidade", pesquisaRelatorioComercialMensalVO.getModalidadeMedicao());
		}

		List<Object[]> objetos = query.list();

		Iterator<Object[]> it = objetos.iterator();
		List<RelatorioComercialMensalVO> lista = new ArrayList<>();
		RelatorioComercialMensalVO vo = null;
		Long chaveImovel = 0L;
		ContratoPontoConsumo contratoPontoConsumo = null;
		BigDecimal consumo = null;
		BigDecimal consumoAnoAnterior = null;
		BigDecimal totalAno = null;
		BigDecimal totalAnoAnterior = null;
		BigDecimal valorTotal = null;
		BigDecimal valor = null;
		Imovel imovel = null;
		while (it.hasNext()) {
			Object[] obj = it.next();
			consumo = (BigDecimal) obj[INDICE_CONSUMO];
			consumoAnoAnterior = (BigDecimal) obj[INDICE_CONSUMO_ANTERIOR];
			valor = (BigDecimal) obj[INDICE_VALOR];
			if (!chaveImovel.equals(obj[0])) {
				vo = new RelatorioComercialMensalVO();
				totalAno = BigDecimal.valueOf(0);
				totalAnoAnterior = BigDecimal.valueOf(0);
				valorTotal = BigDecimal.valueOf(0);
				chaveImovel = (Long) obj[0];
				lista.add(vo);
			}

			if (consumo != null) {
				totalAno = totalAno.add(consumo);
			}
			if (consumoAnoAnterior != null) {
				totalAnoAnterior = totalAnoAnterior.add(consumoAnoAnterior);
			}

			if (valor != null) {
				valorTotal = valorTotal.add(valor);
			}

			if (obj[1] != null) {
				imovel = (Imovel) obj[1];
				if (imovel.getQuadra() != null && imovel.getQuadra().getZonaBloqueio() != null) {
					vo.setZonaBloqueio(imovel.getQuadra().getZonaBloqueio().getDescricao());
				}
				vo.setImovel(imovel.getChavePrimaria() + " - " + imovel.getNome());
				if (imovel.getModalidadeMedicaoImovel() != null) {
					vo.setModalidade(imovel.getModalidadeMedicaoImovel().getDescricao());
				}
			}

			if (obj[INDICE_CONTRATO_PONTO_CONSUMO] != null) {
				contratoPontoConsumo = (ContratoPontoConsumo) obj[INDICE_CONTRATO_PONTO_CONSUMO];
				vo.setNomeFantasia(contratoPontoConsumo.getContrato().getClienteAssinatura().getNomeFantasia());
				vo.setVolumeContratado(retonarQDCContrato(contratoPontoConsumo.getContrato()));
				if (contratoPontoConsumo.getContrato().getClienteAssinatura().getAtividadeEconomica() != null) {
					vo.setAtividadeEconomica(contratoPontoConsumo.getContrato().getClienteAssinatura().getAtividadeEconomica()
									.getDescricao());
				}
				vo.setSegmento(contratoPontoConsumo.getPontoConsumo().getSegmento().getDescricao());
				vo.setSituacao(contratoPontoConsumo.getPontoConsumo().getSituacaoConsumo().getDescricao());
				vo.setRazaoSocial(contratoPontoConsumo.getContrato().getClienteAssinatura().getNome());
				vo.setDataAssinaturaContrato(contratoPontoConsumo.getContrato().getDataAssinatura());
			}
			if (obj[INDICE_DATA_INICIO] != null) {
				vo.setDataInicioConsumo((Date) obj[INDICE_DATA_INICIO]);
			}

			HistoricoOperacaoMedidor obterUltimoBloqueio =
							getControladorMedidor().obterUltimoBloqueio(contratoPontoConsumo.getPontoConsumo().getChavePrimaria());
			if (obterUltimoBloqueio != null) {
				Date dataRealizada = obterUltimoBloqueio.getDataRealizada();
				vo.setDataDesligamento(dataRealizada);
			}

			Integer data = (Integer) obj[INDICE_DATA];
			String mes = data.toString().substring(INDICE_DATA);
			if ("01".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoJaneiro(consumo);
				} else {
					vo.setConsumoJaneiro(BigDecimal.valueOf(0));
				}
			} else if ("02".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoFevereiro(consumo);
				} else {
					vo.setConsumoFevereiro(BigDecimal.valueOf(0));
				}
			} else if ("03".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoMarco(consumo);
				} else {
					vo.setConsumoMarco(BigDecimal.valueOf(0));
				}
			} else if ("04".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoAbril(consumo);
				} else {
					vo.setConsumoAbril(BigDecimal.valueOf(0));
				}
			} else if ("05".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoMaio(consumo);
				} else {
					vo.setConsumoMaio(BigDecimal.valueOf(0));
				}
			} else if ("06".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoJunho(consumo);
				} else {
					vo.setConsumoJunho(BigDecimal.valueOf(0));
				}
			} else if ("07".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoJulho(consumo);
				} else {
					vo.setConsumoJulho(BigDecimal.valueOf(0));
				}
			} else if ("08".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoAgosto(consumo);
				} else {
					vo.setConsumoAgosto(BigDecimal.valueOf(0));
				}
			} else if ("09".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoSetembro(consumo);
				} else {
					vo.setConsumoSetembro(BigDecimal.valueOf(0));
				}
			} else if ("10".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoOutubro(consumo);
				} else {
					vo.setConsumoOutubro(BigDecimal.valueOf(0));
				}
			} else if ("11".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoNovembro(consumo);
				} else {
					vo.setConsumoNovembro(BigDecimal.valueOf(0));
				}
			} else if ("12".equals(mes)) {
				if (consumo != null) {
					vo.setConsumoDezembro(consumo);
				} else {
					vo.setConsumoDezembro(BigDecimal.valueOf(0));
				}
			}

			if (obj[INDICE_NUMERO_UDA] != null) {
				vo.setNumeroUDA(Integer.valueOf(((Long) obj[INDICE_NUMERO_UDA]).toString()));
			}

			vo.setTotalAno(totalAno);
			vo.setTotalAnoAnterior(totalAnoAnterior);
			vo.setValorTotal(valorTotal);
			if (valorTotal.compareTo(BigDecimal.valueOf(0)) <= 0) {
				vo.setTarifa(BigDecimal.valueOf(0));
			} else {
				vo.setTarifa(valorTotal.divide(totalAno, ESCALA_DOIS, RoundingMode.HALF_UP));
			}

		}

		RelatorioComercialMensalWrapper wrapper = new RelatorioComercialMensalWrapper();
		wrapper.setLista(lista);

		Map<String, Object> parametros = new HashMap<>();
		ControladorEmpresa controladorEmpresa =
						(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		parametros.put("imagem",
						Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		if (!pesquisaRelatorioComercialMensalVO.getAnoReferencia().isEmpty()
						|| (pesquisaRelatorioComercialMensalVO.getSegmento() != null && pesquisaRelatorioComercialMensalVO
										.getModalidadeMedicao() != -1)) {
			parametros.put("exibirFiltros", Boolean.TRUE);
		}
		if (!pesquisaRelatorioComercialMensalVO.getAnoReferencia().isEmpty()) {
			parametros.put("anoReferencia", pesquisaRelatorioComercialMensalVO.getAnoReferencia());
		}
		if (pesquisaRelatorioComercialMensalVO.getModalidadeMedicao() != null
						&& pesquisaRelatorioComercialMensalVO.getModalidadeMedicao() != -1) {
			parametros.put("segmento", pesquisaRelatorioComercialMensalVO.getModalidadeMedicao());
		}
		Collection<Object> collRelatorio = new ArrayList<>();
		collRelatorio.add(wrapper);
		if (lista != null && !lista.isEmpty()) {
			return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_RESIDENCIAL_MENSAL, formatoImpressao);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

	}

	/**
	 * Retonar qdc contrato.
	 *
	 * @param contrato
	 *            the contrato
	 * @return the big decimal
	 */
	public BigDecimal retonarQDCContrato(Contrato contrato) {

		Date data = null;
		BigDecimal volume = null;
		for (ContratoQDC qdc : contrato.getListaContratoQDC()) {
			if (data == null) {
				data = qdc.getData();
			}
			if (qdc.getData().compareTo(data) >= 0) {
				data = qdc.getData();
				volume = qdc.getMedidaVolume();
			}
		}
		return volume;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listaPontoConsumoPorNumeroContrato(java.lang.String)
	 */
	@Override
	public Collection<PontoConsumo> listaPontoConsumoPorNumeroContrato(String numeroContrato) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select contratoPonto.pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.imovel imovel ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo.quadraFace quadraFace ");
		hql.append(WHERE);
		hql.append(" contratoPonto.contrato.numeroCompletoContrato like  :numeroContrato ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("numeroContrato", "%" + numeroContrato + "%");

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarPontoConsumoContratoAtivo()
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoContratoAtivo() throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join fetch contratoPonto.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel imovel");
		hql.append(" left join fetch  imovel.quadraFace quadraFace");
		hql.append(" left join fetch  quadraFace.endereco");
		hql.append(" inner join fetch pontoConsumo.segmento ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor.medidor ");
		hql.append(" where contratoPonto.contrato.situacao = :idSituacaoContrato ");
		hql.append(" and contratoPonto.contrato.habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idSituacaoContrato =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
		query.setLong("idSituacaoContrato", idSituacaoContrato);

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = query.list();

		Set<PontoConsumo> listaPontoConsumoAux = new HashSet<>();

		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

			listaPontoConsumoAux.add(contratoPontoConsumo.getPontoConsumo());
		}

		return listaPontoConsumoAux;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#carregarPontosConsumoAssociados(br.com.ggas.medicao.rota.Rota,
	 * java.util.Collection,
	 * java.util.Map)
	 */
	@Override
	public Pair<Collection<PontoConsumo>, Map<Long, Object>> carregarPontosConsumoAssociados(Rota rota,
			Collection<PontoConsumo> list, Map<Long, Object> pontosConsumoAssociados) throws GGASException {

		Collection<PontoConsumo> collPontosConsumoPorRota = new ArrayList<>();
		if (rota != null && rota.getPontosConsumo() != null) {
			collPontosConsumoPorRota = listarPontosConsumoPorChaveRota(rota.getChavePrimaria());
		}

		return this.carregarPontosConsumoAssociados(rota, list, pontosConsumoAssociados, collPontosConsumoPorRota);
	}
	
	/**
	 * Carrega um par de coleção de pontos de consumo e de VOs mapeados por chave
	 * primária associados a uma rota.
	 * 
	 * @param rota                    - {@link Rota}
	 * @param list                    - {@link Collection}
	 * @param pontosConsumoAssociados - {@link Map}
	 * @return par de coleção e VOs - {@link Pair}
	 * @throws GGASException - {@link GGASException}
	 */
	@Override
	public Pair<Collection<PontoConsumo>, Map<Long, Object>> carregarAtributosDePontosConsumoAssociados(Rota rota,
			Collection<PontoConsumo> list, Map<Long, Object> pontosConsumoAssociados) throws GGASException {

		Collection<PontoConsumo> collPontosConsumoPorRota = new ArrayList<>();
		if (rota != null && rota.getPontosConsumo() != null) {
			collPontosConsumoPorRota = listarAtributosDePontosConsumoPorChaveRota(rota.getChavePrimaria());
		}

		return this.carregarPontosConsumoAssociados(rota, list, pontosConsumoAssociados, collPontosConsumoPorRota);
	}
	
	
	private Pair<Collection<PontoConsumo>, Map<Long, Object>> carregarPontosConsumoAssociados(Rota rota, Collection<PontoConsumo> list,
					Map<Long, Object> pontosConsumoAssociados, Collection<PontoConsumo> collPontosConsumoPorRota) throws GGASException {

		Collection<PontoConsumo> listaPontoConsumoAssociados = new LinkedList<>();
		if (collPontosConsumoPorRota != null && !collPontosConsumoPorRota.isEmpty()) {
			listaPontoConsumoAssociados.addAll(collPontosConsumoPorRota);
		}

		if (rota != null) {
			pontosConsumoAssociados = new HashMap<>();
			if (list == null) {
				List<PontoConsumoRotaVO> listaPontoConsumoVO =
								converterListaPontoConsumo(listaPontoConsumoAssociados, pontosConsumoAssociados, rota);
				if (existePontoConsumoSemNumeroSequencia(listaPontoConsumoVO)) {
					ordernarListaPontoConsumoRotaVOPorDescricao(listaPontoConsumoVO);
					converterPontoConsumoVOParaPontoConsumo(pontosConsumoAssociados, listaPontoConsumoAssociados, listaPontoConsumoVO);
					list = ordenarListaPontoConsumo(listaPontoConsumoAssociados);
				} else {
					ordernarListaPontoConsumoRotaVOPorNumeroSequencia(listaPontoConsumoVO);
					converterPontoConsumoVOParaPontoConsumo(pontosConsumoAssociados, listaPontoConsumoAssociados, listaPontoConsumoVO);
					list = ordenarListaPontoConsumo(listaPontoConsumoAssociados);
				}

			} else {
				List<PontoConsumoRotaVO> listaPontoConsumoVO = converterListaPontoConsumo(list, pontosConsumoAssociados, rota);
				if (existePontoConsumoSemNumeroSequencia(listaPontoConsumoVO)) {
					ordernarListaPontoConsumoRotaVOPorDescricao(listaPontoConsumoVO);
					converterPontoConsumoVOParaPontoConsumo(pontosConsumoAssociados, list, listaPontoConsumoVO);
					list = ordenarListaPontoConsumo(list);
				} else {
					ordernarListaPontoConsumoRotaVOPorNumeroSequencia(listaPontoConsumoVO);
					converterPontoConsumoVOParaPontoConsumo(pontosConsumoAssociados, list, listaPontoConsumoVO);
					list = ordenarListaPontoConsumo(listaPontoConsumoAssociados);
				}
			}
		}
		
		return new Pair<>(list, pontosConsumoAssociados);
	}

	/**
	 * Existe ponto consumo sem numero sequencia.
	 *
	 * @param listaPontoConsumoVO
	 *            the lista ponto consumo vo
	 * @return true, if successful
	 */
	private boolean existePontoConsumoSemNumeroSequencia(List<PontoConsumoRotaVO> listaPontoConsumoVO) {

		boolean result = false;
		for (PontoConsumoRotaVO pontoConsumoRotaVO : listaPontoConsumoVO) {
			if (pontoConsumoRotaVO.getNumeroSequenciaLeitura() == null) {
				result = true;
				break;
			}

		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#ordernarListaPontoConsumoRotaVOPorDescricao(java.util.List)
	 */
	@Override
	public void ordernarListaPontoConsumoRotaVOPorDescricao(List<PontoConsumoRotaVO> listaPontoConsumoVO) {

		Collections.sort(listaPontoConsumoVO, new Comparator<PontoConsumoRotaVO>(){

			@Override
			public int compare(PontoConsumoRotaVO o1, PontoConsumoRotaVO o2) {

				return new CompareToBuilder().append(o1.getCpSetorComercial(), o2.getCpSetorComercial())
								.append(o1.getBairro(), o2.getBairro()).append(o1.getCep(), o2.getCep())
								.append(o1.getNumeroImovel(), o2.getNumeroImovel()).append(o1.getNomeImovel(), o2.getNomeImovel())
								.append(o1.getNomeImovelNumero(), o2.getNomeImovelNumero())
								.append(o1.getDescricaoComplementoNome(), o2.getDescricaoComplementoNome())
								.append(o1.getDescricaoComplementoNumero(), o2.getDescricaoComplementoNumero()).toComparison();

			}
		});

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#converterPontoConsumoVOParaPontoConsumo(java.util.Map, java.util.Collection, java.util.List)
	 */
	@Override
	public void converterPontoConsumoVOParaPontoConsumo(Map<Long, Object> pontosConsumoAssociados,
					Collection<PontoConsumo> listaPontoConsumoAssociados, List<PontoConsumoRotaVO> listaPontoConsumoVO)
					throws GGASException {

		for (PontoConsumoRotaVO pontoConsumoRotaVO : listaPontoConsumoVO) {
			pontosConsumoAssociados.put(pontoConsumoRotaVO.getIdPontoConsumo(), pontoConsumoRotaVO);
		}
	}

	@Override
	public List<PontoConsumo> ordenarListaPontoConsumo(Collection<PontoConsumo> listaPontoConsumoAssociados,
					List<PontoConsumoRotaVO> listaPontoConsumoVO) {

		listaPontoConsumoVO = Util.ordenarColecaoPorAtributo(listaPontoConsumoVO, "numeroSequenciaLeitura", true);
		Integer numero = 0;
		for (PontoConsumoRotaVO pontoConsumoRotaVO : listaPontoConsumoVO) {
			if (pontoConsumoRotaVO.getNumeroSequenciaLeitura() != null) {
				numero = pontoConsumoRotaVO.getNumeroSequenciaLeitura();
				break;
			}
		}
		List<PontoConsumo> listaOrdenada = new LinkedList<>();
		boolean isPermitido = true;
		Collection<PontoConsumo> list = atualizarOrdenacaoListaPonto(listaPontoConsumoAssociados, numero, listaOrdenada, isPermitido);
		while (!list.isEmpty()) {
			atualizarOrdenacaoListaPonto(listaPontoConsumoAssociados, numero, listaOrdenada, isPermitido);
			numero++;
		}

		return listaOrdenada;
	}
	
	private List<PontoConsumo> ordenarListaPontoConsumo(Collection<PontoConsumo> listaPontoConsumo) {
		return listaPontoConsumo.stream().sorted(Comparator.nullsLast(Comparator
				.comparing(PontoConsumo::getNumeroSequenciaLeitura, Comparator.nullsLast(Comparator.naturalOrder()))))
				.collect(Collectors.toList());
	}

	/**
	 * Atualizar ordenacao lista ponto.
	 * 
	 * @param listaPontoConsumoAssociados
	 *            the lista ponto consumo associados
	 * @param numeroSequencia
	 *            the numero
	 * @param listaOrdenada
	 *            the lista ordenada
	 * @param isPermitido
	 *            the is permitido
	 * @return the collection
	 */
	private Collection<PontoConsumo> atualizarOrdenacaoListaPonto(Collection<PontoConsumo> listaPontoConsumoAssociados,
			Integer numeroSequencia, List<PontoConsumo> listaOrdenada, boolean permitido) {
		boolean isPermitido = permitido;
		Integer numero = numeroSequencia;
		Iterator it = listaPontoConsumoAssociados.iterator();
		while (it.hasNext()) {
			PontoConsumo pontoConsumo = (PontoConsumo) it.next();

			if (numero.equals(pontoConsumo.getNumeroSequenciaLeitura())) {
				listaOrdenada.add(pontoConsumo);
				it.remove();
				listaPontoConsumoAssociados.remove(pontoConsumo);
				numero++;
				isPermitido = true;
			} else if (pontoConsumo.getNumeroSequenciaLeitura() == null && isPermitido) {
				listaOrdenada.add(pontoConsumo);
				it.remove();
			} else {
				isPermitido = false;
			}
		}
		return listaPontoConsumoAssociados;
	}

	/**
	 * Ordernar lista ponto consumo rota vo por numero sequencia.
	 *
	 * @param listaPontoConsumoVO
	 *            the lista ponto consumo vo
	 */
	private void ordernarListaPontoConsumoRotaVOPorNumeroSequencia(List<PontoConsumoRotaVO> listaPontoConsumoVO) {

		Collections.sort(listaPontoConsumoVO, new Comparator<PontoConsumoRotaVO>(){

			@Override
			public int compare(PontoConsumoRotaVO o1, PontoConsumoRotaVO o2) {

				return o1.getNumeroSequenciaLeitura().compareTo(o2.getNumeroSequenciaLeitura());

			}
		});

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#descricaoFormatada(br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public String descricaoFormatada(PontoConsumo pontoConsumo) {

		StringBuilder descricaoFormatada = new StringBuilder();
		String separador = "";
		SetorComercial setorComercial = null;
		if(pontoConsumo.getRota()!=null){
			setorComercial = pontoConsumo.getRota().getSetorComercial();
		}
		if (setorComercial != null && setorComercial.getCodigo() != null
				&& StringUtils.isNotEmpty(String.valueOf(setorComercial.getCodigo()))) {
			descricaoFormatada.append(String.valueOf(pontoConsumo.getRota().getSetorComercial().getCodigo()));
			separador = " - ";
		}

		if (pontoConsumo.getCep() != null && StringUtils.isNotEmpty(pontoConsumo.getCep().getBairro())) {
			descricaoFormatada.append(separador);
			descricaoFormatada.append(pontoConsumo.getImovel().getQuadraFace().getEndereco().getCep().getBairro());
			separador = " - ";
		}

		if (pontoConsumo.getImovel().getQuadraFace() != null) {
			descricaoFormatada.append(separador);
			descricaoFormatada.append(pontoConsumo.getImovel().getQuadraFace().getEndereco().getCep().getCep());
			separador = " - ";
		}

		if (StringUtils.isNotEmpty(pontoConsumo.getImovel().getNumeroImovel())) {
			descricaoFormatada.append(separador);
			descricaoFormatada.append(pontoConsumo.getImovel().getNumeroImovel());
			separador = " - ";
		}
		if (StringUtils.isNotEmpty(pontoConsumo.getImovel().getNome())) {
			descricaoFormatada.append(separador);
			descricaoFormatada.append(pontoConsumo.getImovel().getNome());
			separador = " - ";
		}

		return descricaoFormatada.toString().toUpperCase();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#converterListaPontoConsumo(java.util.Collection, java.util.Map, br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public List<PontoConsumoRotaVO> converterListaPontoConsumo(Collection<PontoConsumo> listaPontoConsumo,
					Map<Long, Object> pontosConsumoAssociados, Rota rota) {

		List<PontoConsumoRotaVO> listaVOPontoConsumoRota = new ArrayList<>();

		if (listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			for (PontoConsumo pontoConsumo : listaPontoConsumo) {
				PontoConsumoRotaVO voPontoConsumoRota = null;

				if (pontosConsumoAssociados != null && !pontosConsumoAssociados.isEmpty()) {

					voPontoConsumoRota = (PontoConsumoRotaVO) pontosConsumoAssociados.get(pontoConsumo.getChavePrimaria());

				} else {

					voPontoConsumoRota = new PontoConsumoRotaVO();
					voPontoConsumoRota.setIdPontoConsumo(pontoConsumo.getChavePrimaria());

					if (pontoConsumo.getRota() != null) {
						voPontoConsumoRota.setIdRotaOrigem(pontoConsumo.getRota().getChavePrimaria());
					}

					voPontoConsumoRota.setNumeroSequenciaLeitura(pontoConsumo.getNumeroSequenciaLeitura());
					voPontoConsumoRota.setDescricaoPontoConsumoFormatada(this.descricaoFormatada(pontoConsumo));
					voPontoConsumoRota.definirEndereco(pontoConsumo);

					if (rota != null) {
						voPontoConsumoRota.setIdRotaDestino(rota.getChavePrimaria());
					}

				}
				if (voPontoConsumoRota != null) {

					listaVOPontoConsumoRota.add(voPontoConsumoRota);

				}
			}
		}

		return listaVOPontoConsumoRota;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#listarPontosConsumoPorChaveCliente(java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontosConsumoPorChaveCliente(Long idCliente) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(" inner join fetch contratoPonto.contrato contrato ");
		hql.append(" inner join fetch contrato.clienteAssinatura cliente");
		hql.append(" where cliente.chavePrimaria = :idcliente ");
		hql.append(" and contrato.habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idcliente", idCliente);

		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = query.list();

		Collection<PontoConsumo> listaPontosConsumo = new ArrayList<>();

		for (ContratoPontoConsumo contratoPontoConsumo : listaContratoPontoConsumo) {

			listaPontosConsumo.add(contratoPontoConsumo.getPontoConsumo());
		}

		return listaPontosConsumo;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#converterPontoConsumoTOEmPontoConsumo(br.com.ggas.webservice.PontoConsumoTO)
	 */
	@Override
	public PontoConsumo converterPontoConsumoTOEmPontoConsumo(PontoConsumoTO pontoConsumoTO) throws GGASException {

		Fachada fachada = Fachada.getInstancia();

		PontoConsumo pontoConsumo = null;
		if (pontoConsumoTO.getChavePrimaria() != null && pontoConsumoTO.getChavePrimaria() > 0) {
			pontoConsumo = fachada.buscarPontoConsumoPorChave(pontoConsumoTO.getChavePrimaria());
		} else {
			pontoConsumo = (PontoConsumo) this.criar();
		}

		if (!StringUtils.isEmpty(pontoConsumoTO.getCodigoLegado())) {
			pontoConsumo.setCodigoLegado(pontoConsumoTO.getCodigoLegado());
		}

		if (!StringUtils.isEmpty(pontoConsumoTO.getDescricao())) {
			pontoConsumo.setDescricao(pontoConsumoTO.getDescricao());
		}

		if (pontoConsumoTO.getSegmentoCombo() != null) {
			Segmento segmento = fachada.buscarSegmentoChave(pontoConsumoTO.getSegmentoCombo().getChavePrimaria());
			pontoConsumo.setSegmento(segmento);
		}

		if (pontoConsumoTO.getRamoAtividadePontoConsumo() != null) {
			RamoAtividade ramoAtividade =
							fachada.buscarRamoAtividadeChave(pontoConsumoTO.getRamoAtividadePontoConsumo().getChavePrimaria());
			pontoConsumo.setRamoAtividade(ramoAtividade);
		}

		if (pontoConsumoTO.getModalidadeUsoPontoConsumo() != null) {
			EntidadeConteudo modalidadeUso =
							fachada.obterEntidadeConteudo(pontoConsumoTO.getModalidadeUsoPontoConsumo().getChavePrimaria());
			pontoConsumo.setModalidadeUso(modalidadeUso);
		}

		if (!StringUtils.isEmpty(pontoConsumoTO.getNumeroPontoConsumo())) {
			pontoConsumo.setNumeroImovel(pontoConsumoTO.getNumeroPontoConsumo());
		}

		if (!StringUtils.isEmpty(pontoConsumoTO.getComplementoPontoConsumo())) {
			pontoConsumo.setDescricaoComplemento(pontoConsumoTO.getComplementoPontoConsumo());
		}

		if (!StringUtils.isEmpty(pontoConsumoTO.getReferenciaPontoConsumo())) {
			pontoConsumo.setEnderecoReferencia(pontoConsumoTO.getReferenciaPontoConsumo());
		}

		if (pontoConsumoTO.getFaceQuadraPontoConsumo() != null) {
			QuadraFace quadraFace = fachada.buscarQuadraFacePorChave(pontoConsumoTO.getFaceQuadraPontoConsumo().getChavePrimaria());
			pontoConsumo.setQuadraFace(quadraFace);

			if (quadraFace != null && quadraFace.getEndereco() != null) {
				pontoConsumo.setCep(quadraFace.getEndereco().getCep());
			}
		}

		if (pontoConsumoTO.getRotaPontoConsumo() != null) {
			Rota rota = fachada.buscarRota(pontoConsumoTO.getRotaPontoConsumo().getChavePrimaria());
			pontoConsumo.setRota(rota);
		}

		if (!StringUtils.isEmpty(pontoConsumoTO.getLatGrau())) {
			pontoConsumo.setLatitudeGrau(Util.converterCampoStringParaValorBigDecimal("Latitude Grau", pontoConsumoTO.getLatGrau(),
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(pontoConsumoTO.getLongGrau())) {
			pontoConsumo.setLongitudeGrau(Util.converterCampoStringParaValorBigDecimal("Latitude Grau", pontoConsumoTO.getLongGrau(),
							Constantes.FORMATO_VALOR_BR, Constantes.LOCALE_PADRAO));
		}

		if (!StringUtils.isEmpty(pontoConsumoTO.getEnderecoRemoto())) {
			pontoConsumo.setCodigoPontoConsumoSupervisorio(pontoConsumoTO.getEnderecoRemoto());
		}

		pontoConsumo.setHabilitado(pontoConsumoTO.isHabilitadoPontoConsumo());

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorPontoConsumo#converterPontoConsumoEmPontoConsumoTO(br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public PontoConsumoTO converterPontoConsumoEmPontoConsumoTO(PontoConsumo pontoConsumo) throws GGASException {

		PontoConsumoTO pontoConsumoTO = new PontoConsumoTO();
		if (pontoConsumo.getChavePrimaria() > 0) {
			pontoConsumoTO.setChavePrimaria(pontoConsumo.getChavePrimaria());
		}

		if (!StringUtils.isEmpty(pontoConsumo.getCodigoLegado())) {
			pontoConsumoTO.setCodigoLegado(pontoConsumo.getCodigoLegado());
		}

		if (!StringUtils.isEmpty(pontoConsumo.getDescricao())) {
			pontoConsumoTO.setDescricao(pontoConsumo.getDescricao());
		}

		if (pontoConsumo.getSegmento() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(pontoConsumo.getSegmento().getChavePrimaria());
			combo.setDescricao(pontoConsumo.getSegmento().getDescricao());
			pontoConsumoTO.setSegmentoCombo(combo);
		}

		if (pontoConsumo.getRamoAtividade() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(pontoConsumo.getRamoAtividade().getChavePrimaria());
			combo.setDescricao(pontoConsumo.getRamoAtividade().getDescricao());
			pontoConsumoTO.setRamoAtividadePontoConsumo(combo);
		}

		if (pontoConsumo.getModalidadeUso() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(pontoConsumo.getModalidadeUso().getChavePrimaria());
			combo.setDescricao(pontoConsumo.getModalidadeUso().getDescricao());
			pontoConsumoTO.setModalidadeUsoPontoConsumo(combo);
		}

		if (!StringUtils.isEmpty(pontoConsumo.getNumeroImovel())) {
			pontoConsumoTO.setNumeroPontoConsumo(pontoConsumo.getNumeroImovel());
		}

		if (!StringUtils.isEmpty(pontoConsumo.getDescricaoComplemento())) {
			pontoConsumoTO.setComplementoPontoConsumo(pontoConsumo.getDescricaoComplemento());
		}

		if (!StringUtils.isEmpty(pontoConsumo.getEnderecoReferencia())) {
			pontoConsumoTO.setReferenciaPontoConsumo(pontoConsumo.getEnderecoReferencia());
		}

		if (pontoConsumo.getQuadraFace() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(pontoConsumo.getQuadraFace().getChavePrimaria());
			combo.setDescricao(pontoConsumo.getQuadraFace().getQuadraFaceFormatada());
			pontoConsumoTO.setFaceQuadraPontoConsumo(combo);

			if (pontoConsumo.getQuadraFace().getQuadra() != null) {
				TipoComboBoxTO comboQuadra = new TipoComboBoxTO();
				comboQuadra.setChavePrimaria(pontoConsumo.getQuadraFace().getQuadra().getChavePrimaria());
				comboQuadra.setDescricao(pontoConsumo.getQuadraFace().getQuadra().getNumeroQuadra());
				pontoConsumoTO.setQuadraPontoConsumo(comboQuadra);

				if (pontoConsumo.getQuadraFace().getEndereco().getCep() != null) {
					pontoConsumoTO.setCepPontoConsumo(pontoConsumo.getQuadraFace().getEndereco().getCep().getCep());
				}
			}

		}

		if (pontoConsumo.getRota() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(pontoConsumo.getRota().getChavePrimaria());
			combo.setDescricao(pontoConsumo.getRota().getNumeroRota());
			pontoConsumoTO.setRotaPontoConsumo(combo);
		}

		if (pontoConsumo.getLongitudeGrau() != null) {
			pontoConsumoTO.setLongGrau(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor", pontoConsumo.getLongitudeGrau(),
							Constantes.LOCALE_PADRAO, CONSTANTE_DOIS));
		}

		if (pontoConsumo.getLatitudeGrau() != null) {
			pontoConsumoTO.setLatGrau(Util.converterCampoValorDecimalParaStringComCasasDecimais("Valor", pontoConsumo.getLatitudeGrau(),
							Constantes.LOCALE_PADRAO, CONSTANTE_DOIS));
		}

		if (!StringUtils.isEmpty(pontoConsumo.getCodigoPontoConsumoSupervisorio())) {
			pontoConsumoTO.setEnderecoRemoto(pontoConsumo.getCodigoPontoConsumoSupervisorio());
		}

		pontoConsumoTO.setHabilitadoPontoConsumo(pontoConsumo.isHabilitado());

		return pontoConsumoTO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontoConsumoPorMedidor(br.com.ggas.medicao.medidor.Medidor)
	 */
	@Override
	public PontoConsumo consultarPontoConsumoPorMedidor(Medidor medidor) {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode("instalacaoMedidor", FetchMode.JOIN);
		criteria.createAlias("instalacaoMedidor", "instalacaoMedidor");
		if (medidor != null) {
			criteria.add(Restrictions.eq("instalacaoMedidor.medidor", medidor.getChavePrimaria()));
		}
		Collection<PontoConsumo> list = criteria.list();
		for (PontoConsumo pontoConsumo : list) {
			return pontoConsumo;
		}
		return null;
	}

	/**
	 * Obtem o ponto de consumo
	 *
	 * @param chavePrimaria - codigo do ponto de consumo
	 * @return ponto de consumo
	 **/
	@Override
	public PontoConsumo obterPontoConsumo(long chavePrimaria) {

		Criteria criteria = createCriteria(getClasseEntidade());

		StringBuilder hql = new StringBuilder();
		hql.append(" select  pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" as pontoConsumo ");
		hql.append(" left join fetch pontoConsumo.instalacaoMedidor instalacaoMedidor");
		hql.append(" where pontoConsumo.chavePrimaria = (:codigo) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("codigo", chavePrimaria);

		criteria.add(Restrictions.eq("chavePrimaria", chavePrimaria));

		Collection<PontoConsumo> list = query.list();
		for (PontoConsumo pontoConsumo : list) {
			return pontoConsumo;
		}
		return null;
	}

	/**
	 * Consulta os pontos de consumo por códigos do ponto de consumo no Supervisório,
	 * obtendo os seguintes atributos: {@code codigoPontoConsumoSupervisorio}, {@code chavePrimaria}, {@code rota}, {@code segmento},
	 * {@code ramoAtividade}
	 *
	 * @param codigosPontoConsumoSupervisorio - Códigos do Ponto Consumo no Supervisório
	 * @return Map - Pontos de Consumo por Código do Supervisório
	 **/
	@Override
	public Map<String, PontoConsumo> consultarPontoConsumoPorCodigoSupervisorio(String[] codigosPontoConsumoSupervisorio) {

		Map<String, PontoConsumo> mapaPontoConsumo = new HashMap<>();
		if (codigosPontoConsumoSupervisorio != null && codigosPontoConsumoSupervisorio.length > 0) {

			StringBuilder hql = new StringBuilder();
			hql.append(" select  pontoConsumo.codigoPontoConsumoSupervisorio, ");
			hql.append(" pontoConsumo.chavePrimaria, ");
			hql.append(" pontoConsumo.rota, ");
			hql.append(" pontoConsumo.segmento, ");
			hql.append(" pontoConsumo.ramoAtividade, ");
			hql.append(" pontoConsumo.descricao, ");
			hql.append(" pontoConsumo.instalacaoMedidor ");
			hql.append(FROM);
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" as pontoConsumo ");
			hql.append(" where pontoConsumo.codigoPontoConsumoSupervisorio in (:codigos) ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("codigos", codigosPontoConsumoSupervisorio);

			List<Object[]> listaDeAtributos = query.list();
			for (Object[] atributos : listaDeAtributos) {
				String codigoSupervisorio = (String) atributos[0];
				PontoConsumo pontoConsumo = (PontoConsumo) this.criar();
				pontoConsumo.setCodigoPontoConsumoSupervisorio(codigoSupervisorio);
				pontoConsumo.setChavePrimaria((long) atributos[1]);
				pontoConsumo.setRota((Rota) atributos[INDICE_ROTA]);
				pontoConsumo.setSegmento((Segmento) atributos[INDICE_RAMO_SEGMENTO]);
				pontoConsumo.setRamoAtividade((RamoAtividade) atributos[INDICE_RAMO_ATIVIDADE]);
				pontoConsumo.setDescricao((String) atributos[5]);
				pontoConsumo.setInstalacaoMedidor((InstalacaoMedidor)atributos[6]);
				if (!mapaPontoConsumo.containsKey(codigoSupervisorio)) {
					mapaPontoConsumo.put(codigoSupervisorio, pontoConsumo);
				}
			}
		}

		return mapaPontoConsumo;
	}

	@Override
	public Collection<PontoConsumo> listarPontosConsumoAtivoPorData(Map<String, Object> parametros) {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		Date dataInicio;
		Date dataFinal;

		dataInicio = (Date) parametros.get("dataInicio");
		dataFinal = (Date) parametros.get("dataFinal");

		StringBuilder hql = new StringBuilder();
		hql.append(" select *");
		hql.append(FROM);
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel imovel ");
		hql.append(" inner join fetch clienteImovel.imovel imovel ");
		hql.append(" inner join fetch clienteImovel.cliente cliente ");
		hql.append(" inner join fetch instalacaoMedidorImpl.pontoConsumo pontoConsumo");
		hql.append(" inner join fetch instalacaoMedidorImpl.medidor medidor");
		hql.append(WHERE);
		hql.append(" instalacaoMedidorImpl.dataAtiva >= :inicio And instalacaoMedidorImpl.dataAtiva <= :fim ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("inicio", "%" + dataInicio + "%");
		query.setParameter("fim", "%" + dataFinal + "%");

		return query.list();

	}

	/**
	 * Realiza a consulta de atributos do Ponto de Consumo por chaves do Ponto de
	 * Consumo. Obtém os seguintes atributos dos pontos de consumo: {@code imovel},
	 * {@code rota}, {@code instalacaoMedidor}, {@code situacaoConsumo},
	 * {@code ramoAtividade}, {@code segmento}. Ordenados por descrição do ponto de
	 * consumo.
	 *
	 * @param chavesPrimarias
	 * @return pontos de consumo
	 **/
	@Override
	public Collection<PontoConsumo> consultarAtributosDePontosDeConsumo(List<PontoConsumo> listaPontoConsumo) {

		StringBuilder camposObrigatorios = new StringBuilder();
		camposObrigatorios.append("{ \"imovel\":{ \"quadraFace\":{ \"rede\":{}, ");
		camposObrigatorios
				.append(" \"quadra\":{ \"setorComercial\":{ \"municipio\":{ \"microrregiao\":{\"regiao\":{}}, ");
		camposObrigatorios.append(" \"regiao\":{}, \"unidadeFederacao\":{} }, \"localidade\":{} } } } },  ");
		camposObrigatorios.append(" \"rota\":{ \"grupoFaturamento\":{} }, ");
		camposObrigatorios.append("\"instalacaoMedidor\":{ \"medidor\":{} }, \"situacaoConsumo\":{}, ");
		camposObrigatorios.append("\"segmento\":{ \"periodicidade\":{}}, ");
		camposObrigatorios.append(" \"cep\":{}, ");
		camposObrigatorios.append(" \"ramoAtividade\":{ \"periodicidade\":{} } } ");

		Collection<PontoConsumo> pontosConsumo = null;
		String ordenacao = PontoConsumo.ATRIBUTO_DESCRICAO.concat(" asc ");

		return super.obter(TAMANHO_MAXIMO_LISTA, listaPontoConsumo,
				(chavesPrimariasPontoConsumo) -> {
					try {
						return super.obter(chavesPrimariasPontoConsumo, camposObrigatorios.toString(),
								ordenacao);
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
					}
					return pontosConsumo;
				});
	}

	/**
	 * Agrupa os pontos de consumo por rota.
	 *
	 * @param listaPontoConsumo
	 * @return mapa de pontos de consumo por rota
	 */
	@Override
	public Map<Rota, Collection<PontoConsumo>> agruparPontosDeConsumoPorRota(Collection<PontoConsumo> listaPontoConsumo) {

		Map<Rota, Collection<PontoConsumo>> mapaPontoConsumoPorRota = new HashMap<>();
		Collection<PontoConsumo> pontosDaRota;
		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			Rota rota = pontoConsumo.getRota();
			if (!mapaPontoConsumoPorRota.containsKey(rota)) {
				pontosDaRota = new ArrayList<>();
				mapaPontoConsumoPorRota.put(rota, pontosDaRota);
			} else {
				pontosDaRota = mapaPontoConsumoPorRota.get(rota);
			}
			pontosDaRota.add(pontoConsumo);

		}
		return mapaPontoConsumoPorRota;
	}

	/**
	 * Remove de uma lista de pontos de consumo, os pontos com instalacaoMedidor e
	 * com situação de aguardando ativação.
	 *
	 * @param pontosConsumo
	 * @return pontos de consumo filtrados
	 */
	@Override
	public Collection<PontoConsumo> removerPontosConsumoAguardandoAtivacaoComInstalacaoMedidor(Collection<PontoConsumo> pontosConsumo) {

		Collection<PontoConsumo> pontosFiltrados = new ArrayList<>();
		for (PontoConsumo pontoConsumo : pontosConsumo) {
			SituacaoConsumo situacaoConsumo = pontoConsumo.getSituacaoConsumo();
			if (!(pontoConsumo.getInstalacaoMedidor() != null && situacaoConsumo != null && PontoConsumo.SITUACAO_PONTO_CONSUMO_AGUARDANDO_ATIVACAO
							.equals(situacaoConsumo.getDescricao()))) {
				pontosFiltrados.add(pontoConsumo);
			}
		}
		return pontosFiltrados;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#removerPontosConsumoAssociados(java.util.Collection, java.util.List)
	 */
	@Override
	public void removerPontosConsumoAssociados(Collection<PontoConsumo> listaPontoConsumo, List<PontoConsumoRotaVO> listaPontoConsumoVO){
		for (PontoConsumoRotaVO pontoConsumoRotaVO : listaPontoConsumoVO) {
			for (Iterator<PontoConsumo> iterator = listaPontoConsumo.iterator(); iterator.hasNext();) {
				PontoConsumo pontoConsumo = iterator.next();
				if(pontoConsumo.getChavePrimaria()==pontoConsumoRotaVO.getIdPontoConsumo()){
					iterator.remove();
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#isPontoConsumoAtivo(br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public boolean isPontoConsumoAtivo(PontoConsumo pontoConsumo){
		String situacaoAtivo =
				getControladorConstanteSistema().obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_ATIVO);
		return pontoConsumo.getSituacaoConsumo().getChavePrimaria()==Long.parseLong(situacaoAtivo);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#isPontoConsumoSuprimidoDefinitivo(br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public boolean isPontoConsumoSuprimidoDefinitivo(PontoConsumo pontoConsumo){
		String situacaoSuprimidoDefinitivo = getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_SUPRIMIDO_DEFINITIVO);
		return pontoConsumo.getSituacaoConsumo().getChavePrimaria()==Long.parseLong(situacaoSuprimidoDefinitivo);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#isPontoConsumoAguardandoAtivacao(br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public boolean isPontoConsumoAguardandoAtivacao(PontoConsumo pontoConsumo){
		String situacaoAguardandoAtivacao = getControladorConstanteSistema()
				.obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO);
		return pontoConsumo.getSituacaoConsumo().getChavePrimaria()==Long.parseLong(situacaoAguardandoAtivacao);
	}


	/**
	 * Obtem o medidor atraves do ponto de consumo
	 *
	 * @param idPontoConsumo chave primaria do ponto de consumo
	 * @return Medidor
	 */
	@Override
	public Medidor obterMedidorPorPontoConsumo(Long idPontoConsumo){
		Criteria criteria = createCriteria(getClasseEntidadePontoConsumo(), "pocn");
		criteria.createAlias("pocn.instalacaoMedidor", "mein", Criteria.INNER_JOIN);
		criteria.createAlias("mein.medidor", "medi", Criteria.INNER_JOIN);

		criteria.add(Restrictions.eq("pocn.chavePrimaria", idPontoConsumo));

		PontoConsumo pontoConsumo = (PontoConsumoImpl) criteria.uniqueResult();

		if (pontoConsumo != null) {
			return pontoConsumo.getInstalacaoMedidor().getMedidor();
		}

		return null;
	}

	/**
	 * Obtem os pontos de consumo dos imoveis filhos
	 *
	 * @param idImovel Id do imovel pai
	 * @return Retorna a lista com Pontos de Consumo dos filhos do imovel pai
	 */
	@Override
	public List<PontoConsumo> obterPontosConsumoImoveisFilhos(Long idMovel) {

		Query queryImoveis = null;

		StringBuilder hqlImoveis = new StringBuilder();
		hqlImoveis.append("select imovel.chavePrimaria");
		hqlImoveis.append(FROM);
		hqlImoveis.append(getClasseEntidadeImovel().getSimpleName());
		hqlImoveis.append(" imovel");
		hqlImoveis.append(WHERE);
		hqlImoveis.append(" imovel.idImovelCondominio = :idImovelPai ");

		queryImoveis = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hqlImoveis.toString());
		queryImoveis.setLong("idImovelPai", idMovel);

		List<Long> idsImoveis = queryImoveis.list();

		if (idsImoveis!=null && !idsImoveis.isEmpty()) {
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append("select pontoConsumo");
			hql.append(FROM);
			hql.append(getClasseEntidadePontoConsumo().getSimpleName());
			hql.append(" pontoConsumo");
			hql.append(WHERE);
			hql.append(" pontoConsumo.imovel.chavePrimaria in (:ids) ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("ids", idsImoveis);

			return query.list();
		}

		return Collections.emptyList();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<PontoConsumo> listarPontosConsumoDescricao(String descricao, int quantidadeMax, int offset) {
		final Conjunction conjuction = Restrictions.conjunction();
		Arrays.stream(descricao.split(" ")).forEach(d -> conjuction.add(Restrictions.ilike("descricao", "%" + d + "%")));
		return getCriteria().add(conjuction)
				.addOrder(Order.asc("descricao"))
				.setMaxResults(quantidadeMax).setFirstResult(offset).list();
	}

	@Override
	public Collection<PontoConsumo> consultarPontosConsumoFilhosImovelCondominio(Long imovel) {
		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(
				"from " + getClasseEntidade().getSimpleName()  + " where imovel.imovelCondominio.chavePrimaria = :imovel " + "order by descricao"
		).setParameter("imovel", imovel).list();
	}
	@Override
	public PontoConsumo obterPontoConsumoImovel(long chavePrimaria) throws NegocioException {
		PontoConsumo pontoConsumo = (PontoConsumo )super.obter(chavePrimaria, "rota", "imovel");
		Hibernate.initialize(pontoConsumo.getImovel());
		if (pontoConsumo.getImovel().getIdImovelCondominio()!=null) {
			ImovelImpl imovelImplPai = new ImovelImpl();
			imovelImplPai.setChavePrimaria(pontoConsumo.getImovel().getIdImovelCondominio());
			imovelImplPai.setQuadraFace(pontoConsumo.getQuadraFace());
			imovelImplPai.setQuadra(pontoConsumo.getImovel().getQuadra());
			imovelImplPai.setNome(recuperaNomeImovel(pontoConsumo.getImovel().getIdImovelCondominio()));
			pontoConsumo.getImovel().setImovelCondominio(imovelImplPai);
		}

		return pontoConsumo;
	}

	/**
	 * Recupera Nome Imovel
	 * @param idImovelCondominio Chave Primeiria do imovel
	 * @return Nome do imovel
	 */
	private String recuperaNomeImovel(Long idImovelCondominio) {
		return (String) getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createSQLQuery("SELECT IMOV_NM FROM IMOVEL WHERE IMOV_CD = :idImovel")
				.setLong("idImovel", idImovelCondominio).uniqueResult();
	}

	 
	@Override
	protected Collection<Object> inicializarLazyColecao(Collection<Object> colecao, String... propriedadesLazy) {
		Collection<Object> listaRetorno = super.inicializarLazyColecao(colecao, propriedadesLazy);
		if (listaRetorno!=null && !listaRetorno.isEmpty()){
			for (Iterator<Object> iterator = listaRetorno.iterator(); iterator.hasNext();) {
				PontoConsumo pontoConsumo = (PontoConsumo) iterator.next();
				Hibernate.initialize(pontoConsumo.getImovel().getImovelCondominio());
			}
		}
		return listaRetorno;
	}
	
	/**
	 * Atualiza os números sequenciais de leitura dos pontos de consumo de uma rota.
	 * 
	 * @param pontosConsumo - {@link Collection}
	 * @param rota - {@link Rota}
	 */
	@Override
	public void atualizarSequencialLeituraDePontosConsumoPorRota(Collection<PontoConsumo> pontosConsumo, Rota rota) {

		if(pontosConsumo != null) {
			for (PontoConsumo pontoConsumo : pontosConsumo) {
				StringBuilder hql = new StringBuilder();

				hql.append(" update ");
				hql.append(getClasseEntidade().getSimpleName());
				hql.append(" ponto ");
				hql.append(" set numeroSequenciaLeitura = :sequencial, ultimaAlteracao = :ultima, ");
				hql.append(" rota = :rota, ponto.versao = (ponto.versao + 1) ");
				hql.append(" where chavePrimaria = :chavePrimaria ");

				Query query = super.getSession().createQuery(hql.toString());

				query.setParameter("sequencial", pontoConsumo.getNumeroSequenciaLeitura());
				query.setParameter("rota", rota);
				query.setParameter("ultima", new Date());
				query.setParameter(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, pontoConsumo.getChavePrimaria());
				
				query.executeUpdate();
			}
		}
		super.getSession().flush();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontoConsumoBloqueadoComConsumo
	 * (br.com.ggas.medicao.rota.Rota, Integer anoMes, Integer numeroCiclo)
	 */
	@Override
	public Map<Long, Collection<PontoConsumo>> consultarMapearPontoConsumoBloqueadoComConsumo(
			Long[] chavesPrimariasRotas, Integer anoMes, Integer numeroCiclo) throws NegocioException {
		Map<Long, Collection<PontoConsumo>> pontosConsumoPorRota = Collections.emptyMap();
		
		if (chavesPrimariasRotas.length > 0) {
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" select pontoConsumo ");
			hql.append(FROM);
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" pontoConsumo ");
			hql.append(" inner join pontoConsumo.imovel ");
			hql.append(" left join pontoConsumo.imovel.imovelCondominio ");
			hql.append(" inner join pontoConsumo.instalacaoMedidor ");
			hql.append(" where pontoConsumo.habilitado = true ");
			hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = false ");
			hql.append(" and pontoConsumo.situacaoConsumo.faturavel = true ");
			hql.append(" and pontoConsumo.chavePrimaria in ( ");
			hql.append(" 	select historicoConsumo.pontoConsumo.chavePrimaria from ");
			hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
			hql.append(" 	historicoConsumo ");
			hql.append(" 	where historicoConsumo.pontoConsumo.chavePrimaria in ( ");
			hql.append(" 		select pontoConsumo2.chavePrimaria from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append("		pontoConsumo2 ");
			hql.append("		where pontoConsumo2.rota.chavePrimaria in (:chavesPrimarias)");
			hql.append(" 	) ");
			hql.append(" 	and historicoConsumo.anoMesFaturamento = :anoMesFaturamento");
			hql.append(" 	and historicoConsumo.numeroCiclo = :numeroCiclo");
			hql.append(" ) ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setResultTransformer(new GGASTransformer(getClasseEntidadePontoConsumo(), 
					super.getSessionFactory().getAllClassMetadata(), "rota_chavePrimaria")); 
			query.setParameterList(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, chavesPrimariasRotas);
			query.setInteger("anoMesFaturamento", anoMes);
			query.setInteger("numeroCiclo", numeroCiclo);
			pontosConsumoPorRota = (Map<Long, Collection<PontoConsumo>>) query.uniqueResult();
		}
		
		return pontosConsumoPorRota;
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo
	 * #consultarPontoConsumoPorRota
	 * (br.com.ggas.medicao.rota.Rota)
	 * 
	 * Método responsável por retornar:
	 *  chave primária de ponto de consumo
	 *  descrição do ponto de consumo
	 *  chave primária de rota
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, Collection<PontoConsumo>> consultarMapearPontoConsumoPorRotaFaturavel(List<Rota> listaRotas,
			Long[] chavesPrimarias, Boolean indicadorLeitura) {

		Map<Long, Collection<PontoConsumo>> pontosConsumoPorRota = Collections.emptyMap();
		
		if (chavesPrimarias.length > 0) {
			Criteria criteria = createCriteria(PontoConsumo.class);
			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("chavePrimaria"), "chavePrimaria")
					.add(Projections.property("descricao"), "descricao")
					.add(Projections.property("rota.chavePrimaria"), "rota_chavePrimaria"));

			criteria.createAlias("rota", "rota");
			criteria.createAlias("imovel", "imovel", Criteria.INNER_JOIN);
			criteria.createAlias("imovel.imovelCondominio", "imovel_imovelCondominio", Criteria.LEFT_JOIN);
			criteria.createAlias("instalacaoMedidor", "instalacaoMedidor", Criteria.LEFT_JOIN);
			criteria.createAlias("situacaoConsumo", "situacaoConsumo", Criteria.INNER_JOIN);
		
			criteria.add(Restrictions.in("rota.chavePrimaria", chavesPrimarias));
			criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
			criteria.add(Restrictions.eq("situacaoConsumo.faturavel", Boolean.TRUE));
			
			if (indicadorLeitura != null) {
				criteria.add(Restrictions.eq("situacaoConsumo.indicadorLeitura", indicadorLeitura));
			}

			criteria.addOrder(Order.asc("rota.chavePrimaria"));
			criteria.addOrder(Order.asc(IMOVEL_NUMERO_SEQUENCIA_LEITURA));
			criteria.addOrder(Order.asc("numeroSequenciaLeitura"));
			
			criteria.setResultTransformer(new GGASTransformer(getClasseEntidadePontoConsumo(), 
					super.getSessionFactory().getAllClassMetadata(), "rota_chavePrimaria"));
			
			pontosConsumoPorRota = (Map<Long, Collection<PontoConsumo>>) criteria.uniqueResult();
		}
		
		return pontosConsumoPorRota;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#obterPontosConsumoLeituraMovumento(java.util.Collection)
	 */
	@Override
	public List<PontoConsumo> obterPontosConsumoLeituraMovumento(Rota rota) {
		
		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.chavePrimaria in ( ");
		hql.append(" 	select leituramovimento.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeLeituraMovimento().getSimpleName());
		hql.append(" 	leituramovimento ");
		hql.append(" 	where leituramovimento.rota.chavePrimaria = :idRota ");
		hql.append(" 	and leituramovimento.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" 	and leituramovimento.ciclo = :ciclo ");
		hql.append(" 	) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setInteger("anoMesFaturamento", rota.getGrupoFaturamento().getAnoMesReferencia());
		query.setInteger("ciclo", rota.getGrupoFaturamento().getNumeroCiclo());
		query.setLong("idRota", rota.getChavePrimaria());
		
		return query.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#obterPontosConsumoLeituraMovumento(java.util.Collection)
	 */
	@Override
	public List<PontoConsumo> obterPontosConsumoSuperMedicaoDiaria(Rota rota) {
		
		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.rota.chavePrimaria = :idRota ");
		hql.append(" and pontoConsumo.codigoPontoConsumoSupervisorio in ( ");
		hql.append(" 	select smd.codigoPontoConsumoSupervisorio from ");
		hql.append(getClasseEntidadeSupervisorioMedicaoDiaria().getSimpleName());
		hql.append(" 	smd ");
		hql.append(" 	where smd.dataReferencia = :anoMesFaturamento ");
		hql.append(" 	and smd.numeroCiclo = :ciclo ");
		hql.append(" 	) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setInteger("anoMesFaturamento", rota.getGrupoFaturamento().getAnoMesReferencia());
		query.setInteger("ciclo", rota.getGrupoFaturamento().getNumeroCiclo());
		query.setLong("idRota", rota.getChavePrimaria());
		
		return query.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#obterQuantidadePontosConsumoFaturados(java.util.Map)
	 */
	@Override
	public Long obterQuantidadePontosConsumoFaturados(Rota rota) {


		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select count(distinct fatura.pontoConsumo) from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");
		hql.append(" inner join fatura.rota rota ");
		hql.append(" where rota.chavePrimaria = :rota ");
		hql.append(" and fatura.anoMesReferencia = :anoMesReferencia and fatura.numeroCiclo = :numeroCiclo ");
		hql.append(" and fatura.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setParameter("rota", rota.getChavePrimaria());
		query.setParameter("numeroCiclo", rota.getGrupoFaturamento().getNumeroCiclo());
		query.setParameter("anoMesReferencia", rota.getGrupoFaturamento().getAnoMesReferencia());

		return (Long)query.uniqueResult();
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorPontoConsumo#consultarPontosConsumoConsistirLeituraProcessadoComAnormalidadeMasAnalisada(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarPontosConsumoConsistirLeituraProcessadoComAnormalidadeMasAnalisada(Map<String, Object> filtro) {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" as pontoConsumo, ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" as hc ");
		hql.append(" where pontoConsumo.chavePrimaria = hc.pontoConsumo.chavePrimaria ");
		hql.append(" and hc.numeroCiclo = :numeroCiclo ");
		hql.append(" and hc.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and hc.habilitado = true ");
		hql.append(" and hc.historicoAtual.analisada = true ");
		hql.append(" and hc.indicadorConsumoCiclo = true ");
		hql.append(" and hc.anormalidadeConsumo is not null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("numeroCiclo", filtro.get("ciclo"));
		query.setParameter("anoMesFaturamento", filtro.get("anoMes"));

		return query.list();
	}
	
	@Override
	public String obterProximoCodigoLegadoPontoContumo() {
		
		StringBuilder sql = new StringBuilder();
		sql.append("select trim(to_char((substr(ultimo,1,4) + 1),'0000') || to_char(sysdate,'yy')) ");
		sql.append("from (select nvl(max(pocn_cd_legado),0) ultimo ");
		sql.append("from ponto_consumo where pocn_cd_legado like '%' || to_char(sysdate,'yy')) ");
		
		SQLQuery query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString());
		
		Object codigo = query.uniqueResult();
		
		return codigo.toString();
	}
	
	
	@Override
	public Map<Integer, Long> listarPontoConsumoPorCep(String chaveCep) {
		
		Map<Integer, Long> mapaCepNumero = new HashMap<>();
		
		String nomeQuery = "pontoConsumo.buscaPontoCep";
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.getNamedQuery(nomeQuery);
		query.setParameter("chaveCep", chaveCep);
		
		
		
		for (Object objeto : query.list()) {
			Object[] resultado = (Object[]) objeto;
			
			if (!mapaCepNumero.containsKey(Integer.valueOf(resultado[0].toString()))) {
				mapaCepNumero.put(Integer.valueOf(resultado[0].toString()), Long.valueOf(resultado[1].toString()));
			}
		}
		
		return mapaCepNumero;

	}
	
	@Override
	public Collection<PontoConsumo> listarPontoConsumoChavesPrimarias(Long[] chavesPrimarias) {
		Map<Long, PontoConsumo> mapa = new LinkedHashMap<>();
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where ");
		Map<String, List<Long>> mapaPropriedades = HibernateHqlUtil.adicionarClausulaIn(hql,
				"pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPrimarias));

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

		Collection<PontoConsumo> listaPontosConsumo = query.list();

		if (listaPontosConsumo.isEmpty()) {
			return Collections.emptyList();
		}

		for (Long chave : chavesPrimarias) {
			mapa.put(chave, null);
		}

		for (PontoConsumo ponto : listaPontosConsumo) {
			mapa.put(ponto.getChavePrimaria(), ponto);
		}

		return mapa.values();
	}
	
}
