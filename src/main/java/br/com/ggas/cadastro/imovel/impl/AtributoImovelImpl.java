/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Map;

import br.com.ggas.cadastro.imovel.Atributo;
import br.com.ggas.cadastro.imovel.AtributoImovel;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * 
 *
 */
class AtributoImovelImpl extends EntidadeNegocioImpl implements AtributoImovel {

	private static final long serialVersionUID = -5855603468111786193L;

	private Imovel imovel;

	private Atributo atributo;

	private String descricaoValor;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.AtributoImovel
	 * #getImovel()
	 */
	@Override
	public Imovel getImovel() {

		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.AtributoImovel
	 * #
	 * setImovel(br.com.ggas.cadastro.imovel.Imovel
	 * )
	 */
	@Override
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.AtributoImovel
	 * #getAtributo()
	 */
	@Override
	public Atributo getAtributo() {

		return atributo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.AtributoImovel
	 * #setAtributo(br.com.ggas.cadastro.imovel.
	 * Atributo)
	 */
	@Override
	public void setAtributo(Atributo atributo) {

		this.atributo = atributo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.AtributoImovel
	 * #getDescricaoValor()
	 */
	@Override
	public String getDescricaoValor() {

		return descricaoValor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.AtributoImovel
	 * #setDescricaoValor(java.lang.String)
	 */
	@Override
	public void setDescricaoValor(String descricaoValor) {

		this.descricaoValor = descricaoValor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
