/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.imovel.impl.ComentarioPontoConsumo;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.rota.Rota;

/**
 * Classe responsável pela representação de um ponto de consumo
 *
 */

public interface PontoConsumo extends EntidadeNegocio {

	String BEAN_ID_PONTO_CONSUMO = "pontoConsumo";

	/**
	 * @deprecated
	 */
	@Deprecated
	// FIXME: URGENTE retirar validação pela descrição
	String SITUACAO_PONTO_CONSUMO_AGUARDANDO_ATIVACAO = "Aguardando Ativação";

	String DESCRICAO = "PONTO_CONSUMO_DESCRICAO";

	String NUMERO_SEQUENCIA_LEITURA = "PONTO_CONSUMO_NUMERO_SEQUENCIA_LEITURA";

	String SEGMENTO = "PONTO_CONSUMO_SEGMENTO";

	String RAMO_ATIVIDADE = "PONTO_CONSUMO_RAMO_ATIVIDADE";

	String MODALIDADE_USO = "PONTO_CONSUMO_MODALIDADE_USO";

	String CEP = "PONTO_CONSUMO_CEP";

	String NUMERO = "PONTO_CONSUMO_NUMERO";

	String COMPLEMENTO = "PONTO_CONSUMO_COMPLEMENTO";

	String ENDERECO_REFERENCIA = "PONTO_CONSUMO_ENDERECO_REFERENCIA";

	String FACE_QUADRA = "PONTO_CONSUMO_FACE_QUADRA";

	String CITY_GATES_PERCENTUAIS = "PONTO_CONSUMO_CITY_GATES_PERCENTUAIS";

	String PONTO_CONSUMO = "ROTULO_PONTO_CONSUMO";

	String PONTO_CONSUMO_ROTA = "PONTO_CONSUMO_ROTA";
	
	String ATRIBUTO_DESCRICAO = "descricao";
	
	/**
	 * @return the imovel
	 */
	Imovel getImovel();

	/**
	 * @param imovel
	 *            the imovel to set
	 */
	void setImovel(Imovel imovel);

	/**
	 * @return the situacaoConsumo
	 */
	SituacaoConsumo getSituacaoConsumo();

	/**
	 * @param situacaoConsumo
	 *            the situacaoConsumo to set
	 */
	void setSituacaoConsumo(SituacaoConsumo situacaoConsumo);

	/**
	 * @return the numeroSequenciaLeitura
	 */
	Integer getNumeroSequenciaLeitura();

	/**
	 * @param numeroSequenciaLeitura
	 *            the numeroSequenciaLeitura to
	 *            set
	 */
	void setNumeroSequenciaLeitura(Integer numeroSequenciaLeitura);

	/**
	 * @return the modalidadeUso
	 */
	EntidadeConteudo getModalidadeUso();

	/**
	 * @param modalidadeUso
	 *            the modalidadeUso to set
	 */
	void setModalidadeUso(EntidadeConteudo modalidadeUso);

	/**
	 * @return the cep
	 */
	Cep getCep();

	/**
	 * @param cep
	 *            the cep to set
	 */
	void setCep(Cep cep);

	/**
	 * @return the numeroImovel
	 */
	java.lang.String getNumeroImovel();

	/**
	 * @param numeroImovel
	 *            the numeroImovel to set
	 */
	void setNumeroImovel(String numeroImovel);

	/**
	 * @return the descricaoComplemento
	 */
	String getDescricaoComplemento();

	/**
	 * @param descricaoComplemento
	 *            the descricaoComplemento to set
	 */
	void setDescricaoComplemento(String descricaoComplemento);

	/**
	 * @return the enderecoReferencia
	 */
	String getEnderecoReferencia();

	/**
	 * @param enderecoReferencia
	 *            the enderecoReferencia to set
	 */
	void setEnderecoReferencia(String enderecoReferencia);

	/**
	 * @return the segmento
	 */
	Segmento getSegmento();

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	void setSegmento(Segmento segmento);

	/**
	 * @return the ramoAtividade
	 */
	RamoAtividade getRamoAtividade();

	/**
	 * @param ramoAtividade
	 *            the ramoAtividade to set
	 */
	void setRamoAtividade(RamoAtividade ramoAtividade);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the rota
	 */
	Rota getRota();

	/**
	 * @param rota
	 *            the rota to set
	 */
	void setRota(Rota rota);

	/**
	 * @return the instalacaoMedidor
	 */
	InstalacaoMedidor getInstalacaoMedidor();

	/**
	 * @param instalacaoMedidor
	 *            the instalacaoMedidor to set
	 */
	void setInstalacaoMedidor(InstalacaoMedidor instalacaoMedidor);

	/**
	 * Método responsável por retornar as
	 * informações do ponto de consumo
	 * formatadas para exibição.
	 * 
	 * @return String com informações de ponto de
	 *         consumo formatadas.
	 */
	String getDescricaoFormatada();

	/**
	 * Método responsável por retornar as
	 * informações do endereço do ponto de consumo
	 * formatadas para exibição.
	 * 
	 * @return String com informações do endereço
	 *         de ponto de consumo formatadas.
	 */
	String getEnderecoFormatado();

	/**
	 * @return the comentarios
	 */
	Collection<ComentarioPontoConsumo> getComentarios();

	/**
	 * @param comentarios
	 *            the comentarios to set
	 */
	void setComentarios(Collection<ComentarioPontoConsumo> comentarios);

	/**
	 * @return the listaPontoConsumoCityGate
	 */
	Collection<PontoConsumoCityGate> getListaPontoConsumoCityGate();

	/**
	 * @param listaPontoConsumoCityGate
	 *            the listaPontoConsumoCityGate to
	 *            set
	 */
	void setListaPontoConsumoCityGate(Collection<PontoConsumoCityGate> listaPontoConsumoCityGate);

	/**
	 * @return
	 */
	Collection<PontoConsumoTributoAliquota> getListaPontoConsumoTributoAliquota();

	/**
	 * @param listaPontoConsumoTributoAliquota
	 */
	void setListaPontoConsumoTributoAliquota(Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota);

	/**
	 * @return the quadraFace
	 */
	QuadraFace getQuadraFace();

	/**
	 * @param quadraFace
	 *            the quadraFace to set
	 */
	void setQuadraFace(QuadraFace quadraFace);

	/**
	 * @return the codigoPontoConsumoSupervisorio
	 */
	String getCodigoPontoConsumoSupervisorio();

	/**
	 * @param latitudeGrau
	 */
	void setLatitudeGrau(BigDecimal latitudeGrau);

	/**
	 * @return the latitudeGrau
	 */
	BigDecimal getLatitudeGrau();

	/**
	 * @param longitudeGrau
	 */
	void setLongitudeGrau(BigDecimal longitudeGrau);

	/**
	 * @return the longitudeGrau
	 */
	BigDecimal getLongitudeGrau();

	/**
	 * @return the codigoLegado
	 */
	String getCodigoLegado();

	/**
	 * @param codigoLegado
	 *            the codigoLegado to set
	 */
	void setCodigoLegado(String codigoLegado);

	/**
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigoPontoConsumoSupervisorio to set
	 */
	void setCodigoPontoConsumoSupervisorio(String codigoPontoConsumoSupervisorio);

	/**
	 * @return Retorna Código Ponto de Cusmo
	 */
	String getCodigoPontoConsumo();

	/**
	 * @return Retorna Coleção Historico Consumo
	 */
	Collection<HistoricoConsumo> getListaConsumoHistorico();

	/**
	 * @param listaConsumoHistorico - Set lista consumo histórico.	
	 */
	void setListaConsumoHistorico(Collection<HistoricoConsumo> listaConsumoHistorico);

	/**
	 * @return Retorna Coleção Historico de medição
	 */
	Collection<HistoricoMedicao> getListaMedicaoHistorico();

	/**
	 * @param listaMedicaoHistorico - Set lista medição histórico.
	 */
	void setListaMedicaoHistorico(Collection<HistoricoMedicao> listaMedicaoHistorico);

	/**
	 * @return Retorna Logradouro Formatado
	 */
	String getLogradouroFormatado();

	/**
	 * @return Retorna Habilitado
	 */
	Boolean getHabilitado();

	/**
	 * @return Retorna Endereço Formatado Rua/Número/Complemento
	 */
	String getEnderecoFormatadoRuaNumeroComplemento();

	/**
	 * @return Retorna Endereço Formatado Bairro
	 */
	String getEnderecoFormatadoBairro();

	/**
	 * @return Retorna Endereço Formatado Municipior UF-Cep
	 */
	String getEnderecoFormatadoMunicipioUFCEP();

	/**
	 * @return Retorna Endereço Logradouro
	 */
	String getEnderecoLogradouro();

	/**
	 * @return Retorna Endereço Formatado Municipio UF
	 */
	String getEnderecoFormatadoMunicipioUF();

	/**
	 * @return Endereço do ponto de consumo concatenado
	 *         na sequência descrita na assinatura do método.
	 */
	String getEnderecoFormatadoCepMunicipioUF();

	/**
	 * @return Endereço do ponto de consumo para
	 *         os relatórios Notificação de corte
	 *         e Aviso de corte da Sergás
	 */
	String getEnderecoFormatadoRelatorioCorteSergas();
	
	/**
	 * Gets the dados resumo ponto consumo.
	 *
	 * @return the dados resumo ponto consumo
	 */
	DadosResumoPontoConsumo getDadosResumoPontoConsumo();
	
	/**
	 * Sets the dados resumo ponto consumo.
	 *
	 * @param dadosResumoPontoConsumo the new dados resumo ponto consumo
	 */
	void setDadosResumoPontoConsumo(DadosResumoPontoConsumo dadosResumoPontoConsumo);
	
	/**
	 * Gets the lista ponto consumo equipamento.
	 *
	 * @return the lista ponto consumo equipamento
	 */
	public Collection<PontoConsumoEquipamento> getListaPontoConsumoEquipamento();
	
	/**
	 * Sets the lista ponto consumo equipamento.
	 *
	 * @param listaPontoConsumoEquipamento the new lista ponto consumo equipamento
	 */
	public void setListaPontoConsumoEquipamento(Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento);

	/**
	 * Gets o indicador classifical fiscal
	 *
	 * @return o valor do indicador classifical fiscal
	 */
	public Boolean getIndicadorClassificacaoFiscal() ;
	
	/**
	 * Sets o indicador classifical fiscal.
	 *
	 * @param indicadorClassificacaoFiscal
	 */
	public void setIndicadorClassificacaoFiscal(Boolean indicadorClassificacaoFiscal);

	/**
	 * Informa se possui grupo de faturamento
	 * @return se possui grupo de faturamento
	 */
	public boolean temGrupoFaturamento();
	
	
	/**
	 * Lista com os pontos de consumo agrupados
	 * @return Lista agrupada com os imoveis filhos
	 */
	public List<PontoConsumo> getListaPontosConsumoImoveisFilho();
	
	/**
	 * Lista com os pontos de consumo agrupados
	 * @param listaPontosConsumoImoveisFilho Lista com os novos valores
	 */
	public void setListaPontosConsumoImoveisFilho(List<PontoConsumo> listaPontosConsumoImoveisFilho);

	String getObservacao();

	void setObservacao(String observacao);
	
	/**
	 * Gets the indicadorMensagemAdicional
	 * @return the indicadorMensagemAdicional
	 */
	boolean isIndicadorMensagemAdicional();
	
	/**
	 * Sets the indicadorMensagemAdicional
	 * @param indicadorMensagemAdicional the new indicadorMensagemAdicional
	 */
	void setIndicadorMensagemAdicional(boolean indicadorMensagemAdicional);
	
	/**
	 * Gets the descricaoMensagemAdicional
	 * @return descricaoMensagemAdicional
	 */
	String getDescricaoMensagemAdicional();
	
	/**
	 * Sets the descricaoMensagemAdicional
	 * @param descricaoMensagemAdicional the new descricaoMensagemAdicional
	 */
	void setDescricaoMensagemAdicional(String descricaoMensagemAdicional);

	Double getDistanciaEntrePontos();

	void setDistanciaEntrePontos(Double distanciaEntrePontos);

}
