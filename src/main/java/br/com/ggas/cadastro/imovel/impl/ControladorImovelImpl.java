/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.cadastro.imovel.impl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.routines.BigDecimalValidator;
import org.apache.log4j.Logger;
import org.displaytag.properties.SortOrderEnum;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.AreaConstruidaFaixa;
import br.com.ggas.cadastro.imovel.AtributoImovel;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.ContatoImovel;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ImovelRamoAtividade;
import br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao;
import br.com.ggas.cadastro.imovel.ImovelValidador;
import br.com.ggas.cadastro.imovel.ModalidadeAquecimento;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.MotivoFimRelacionamentoClienteImovel;
import br.com.ggas.cadastro.imovel.PadraoConstrucao;
import br.com.ggas.cadastro.imovel.PavimentoCalcada;
import br.com.ggas.cadastro.imovel.PavimentoRua;
import br.com.ggas.cadastro.imovel.PerfilImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.imovel.PontoConsumoEquipamento;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.cadastro.imovel.RedeInternaImovel;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoImovel;
import br.com.ggas.cadastro.imovel.TipoBotijao;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.cadastro.imovel.TipoRelacionamentoClienteImovel;
import br.com.ggas.cadastro.levantamentomercado.agente.dominio.Agente;
import br.com.ggas.cadastro.levantamentomercado.agente.negocio.ControladorAgente;
import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.localidade.RedeIndicador;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.dominio.EntidadeDominio;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.BooleanUtil;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.OrdenacaoEspecial;
import br.com.ggas.util.Pair;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.cadastro.imovel.ImovelAction;
import br.com.ggas.web.faturamento.leitura.dto.ImovelDTO;
import br.com.ggas.web.faturamento.leitura.dto.PesquisaPontoConsumo;
import br.com.ggas.web.faturamento.leitura.dto.PontoConsumoDTO;
import br.com.ggas.web.parametrosistema.ParametroSistemaAction;
import br.com.ggas.webservice.ContatoImovelTO;
import br.com.ggas.webservice.ImovelRamoAtividadeTO;
import br.com.ggas.webservice.ImovelTO;
import br.com.ggas.webservice.RedeInternaImovelTO;
import br.com.ggas.webservice.TipoComboBoxTO;

class ControladorImovelImpl extends ControladorNegocioImpl implements ControladorImovel {

	private static final String SITUACAO_CONTRATO = "situacaoContrato";

	private static final String SELECT_DISTINCT = " select distinct";

	private static final String CONSTANTE_PONTO_CONSUMO = " pontoConsumo ";

	private static final String CONSTANTE_FROM = " from ";

	private static final String IMOVEL_PONTO_CONSUMO = CONSTANTE_PONTO_CONSUMO;

	private static final String IMOVEL_DESCRICAO = "descricao";

	private static final String ID_IMOVEL = "idImovel";

	private static final String CONDOMINIO = "condominio";

	private static final String IMOVEL_CONDOMINIO = "imovelCondominio";

	private static final String IMOVEL = "imovel";

	private static final String NUMERO_SUBLOTE = "numeroSublote";

	private static final String NUMERO_LOTE = "numeroLote";

	private static final String QUADRA_FACE = "quadraFace";

	private static final String CLIENTE = "cliente";

	private static final String PONTO_CONSUMO = "ponto_consumo";

	private static final String AMBOS = "ambos";

	private static final String UPPER = "UPPER";

	private static final String LIKE = "LIKE";

	private static final int DIAS_SEMANA = 7;

	private static final int TAMANHO_IDEAL_NOMES = 3;

	private static final int PORCENTAGEM = 100;

	private static final int CONSTANTE_NUMERO_DOIS = 2;

	private static final int QTD_COMPLEMENTO = 3;

	private static final int LIMITE_CAMPO = 2;

	private static final double LONGITUDE_MAX_BA = 680.69238168969;
	private static final double LATITUDE_MAX_BA = 90.567199392667;

	private static final double LATITUDE_MIN_BA = 79.725923462342;
	private static final double LONGITUDE_MIN_BA = 323.57205797324;

	private static final double LONGITUDE_MAX = 180;
	private static final double LATITUDE_MAX = 90;

	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	private static final String ENDERECO = "endereco";
	private static final String CEP = "cep";
	private static final String DESCRICAO_COMPLEMENTO = "descricaoComplemento";
	private static final String IMOVEL_CHAVE_PRIMARIA = "imovel.chavePrimaria";
	private static final String NUMERO_IMOVEL = "numeroImovel";
	private static final String WHERE = " where ";
	private static final String FROM = CONSTANTE_FROM;

	private static final Logger LOG = Logger.getLogger(ControladorImovelImpl.class);
	public static final String FALSE = "false";

	private Fachada fachada = Fachada.getInstancia();

	private ControladorParametroSistema parametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
			.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

	private ControladorUsuario controladorUsuario =
			(ControladorUsuario) ServiceLocator.getInstancia().getControladorNegocio(ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO);

	@Autowired
	@Qualifier("imovelValidador")
	private ImovelValidador validador;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Imovel.BEAN_ID_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarAtributoImovel()
	 */
	@Override
	public EntidadeNegocio criarAtributoImovel() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AtributoImovel.BEAN_ID_ATRIBUTO_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarRedeInternaImovel()
	 */
	@Override
	public EntidadeNegocio criarRedeInternaImovel() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(RedeInternaImovel.BEAN_ID_REDE_INTERNA_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarPontoConsumo()
	 */
	@Override
	public EntidadeNegocio criarPontoConsumo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel
	 * #criarPontoConsumoEquipamento
	 * ()
	 */
	@Override
	public EntidadeNegocio criarPontoConsumoTributoAliquota() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						PontoConsumoTributoAliquota.BEAN_ID_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel
	 * #criarPontoConsumoTributoAliquota
	 * ()
	 */
	@Override
	public EntidadeNegocio criarPontoConsumoEquipamento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						PontoConsumoEquipamento.BEAN_ID_PONTO_CONSUMO_EQUIPAMENTO);
	}


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel
	 * #criarModalidadeAquecimento()
	 */
	@Override
	public EntidadeNegocio criarModalidadeAquecimento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ModalidadeAquecimento.BEAN_ID_MODALIDADE_AQUECIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarClienteImovel()
	 */
	@Override
	public EntidadeNegocio criarClienteImovel() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ClienteImovel.BEAN_ID_CLIENTE_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel
	 * #criarTipoRelacionamentoClienteImovel
	 * ()
	 */
	@Override
	public EntidadeNegocio criarTipoRelacionamentoClienteImovel() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						TipoRelacionamentoClienteImovel.BEAN_ID_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#
	 * criarMotivoFimRelacionamentoClienteImovel()
	 */
	@Override
	public EntidadeNegocio criarMotivoFimRelacionamentoClienteImovel() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(
						MotivoFimRelacionamentoClienteImovel.BEAN_ID_MOTIVO_FIM_RELACIONAMENTO_CLIENTE_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarContatoImovel()
	 */
	@Override
	public EntidadeNegocio criarContatoImovel() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ContatoImovel.BEAN_ID_CONTATO_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarSituacaoConsumo()
	 */
	@Override
	public EntidadeNegocio criarSituacaoConsumo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(SituacaoConsumo.BEAN_ID_SITUACAO_CONSUMO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarPadraoConstrucao()
	 */
	@Override
	public EntidadeNegocio criarPadraoConstrucao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PadraoConstrucao.BEAN_ID_PADRAO_CONSTRUCAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarPavimentoRua()
	 */
	@Override
	public EntidadeNegocio criarPavimentoRua() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PavimentoRua.BEAN_ID_PAVIMENTO_RUA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarPavimentoCalcada()
	 */
	@Override
	public EntidadeNegocio criarPavimentoCalcada() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PavimentoCalcada.BEAN_ID_PAVIMENTO_CALCADA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarSituacaoImovel()
	 */
	@Override
	public EntidadeNegocio criarSituacaoImovel() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(SituacaoImovel.BEAN_ID_SITUACAO_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarPerfilImovel()
	 */
	@Override
	public EntidadeNegocio criarPerfilImovel() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(PerfilImovel.BEAN_ID_PERFIL_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarTipoBotijao()
	 */
	@Override
	public EntidadeNegocio criarTipoBotijao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(TipoBotijao.BEAN_ID_TIPO_BOTIJAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarGrupoFaturamento()
	 */
	@Override
	public EntidadeNegocio criarGrupoFaturamento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(GrupoFaturamento.BEAN_ID_GRUPO_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * ControladorImovel#criarTipoLeitura()
	 */
	@Override
	public EntidadeNegocio criarTipoLeitura() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(TipoLeitura.BEAN_ID_TIPO_LEITURA);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #criarImovelRamoAtividade()
	 */
	@Override
	public EntidadeNegocio criarImovelRamoAtividade() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ImovelRamoAtividade.BEAN_ID_IMOVEL_RAMO_ATIVIDADE);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel #criarImovelRamoAtividade()
	 */
	@Override
	public EntidadeNegocio criarImovelServicoTipoRestricao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia()
				.getBeanPorID(ImovelServicoTipoRestricao.BEAN_ID_IMOVEL_SERVICO_TIPO_RESTRICAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Imovel.BEAN_ID_IMOVEL);
	}

	public Class<?> getClasseEntidadeMedidorInstalacao() {

		return ServiceLocator.getInstancia().getClassPorID(InstalacaoMedidor.BEAN_ID_INSTALACAO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeAtributoImovel() {

		return ServiceLocator.getInstancia().getClassPorID(AtributoImovel.BEAN_ID_ATRIBUTO_IMOVEL);
	}

	public Class<?> getClasseEntidadeRedeInternaImovel() {

		return ServiceLocator.getInstancia().getClassPorID(RedeInternaImovel.BEAN_ID_REDE_INTERNA_IMOVEL);
	}

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadePontoConsumoTributoAliquota() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumoTributoAliquota.BEAN_ID_PONTO_CONSUMO_TRIBUTO_ALIQUOTA);
	}

	public Class<?> getClasseEntidadeModalidadeAquecimento() {

		return ServiceLocator.getInstancia().getClassPorID(ModalidadeAquecimento.BEAN_ID_MODALIDADE_AQUECIMENTO);
	}

	public Class<?> getClasseEntidadeClienteImovel() {

		return ServiceLocator.getInstancia().getClassPorID(ClienteImovel.BEAN_ID_CLIENTE_IMOVEL);
	}

	public Class<?> getClasseEntidadeTipoRelacionamentoClienteImovel() {

		return ServiceLocator.getInstancia().getClassPorID(TipoRelacionamentoClienteImovel.BEAN_ID_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL);
	}

	public Class<?> getClasseEntidadeMotivoFimRelacionamentoClienteImovel() {

		return ServiceLocator.getInstancia().getClassPorID(
						MotivoFimRelacionamentoClienteImovel.BEAN_ID_MOTIVO_FIM_RELACIONAMENTO_CLIENTE_IMOVEL);
	}

	public Class<?> getClasseEntidadeContatoImovel() {

		return ServiceLocator.getInstancia().getClassPorID(ContatoImovel.BEAN_ID_CONTATO_IMOVEL);
	}

	public Class<?> getClasseEntidadeSituacaoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(SituacaoConsumo.BEAN_ID_SITUACAO_CONSUMO);
	}

	public Class<?> getClasseEntidadePadraoConstrucao() {

		return ServiceLocator.getInstancia().getClassPorID(PadraoConstrucao.BEAN_ID_PADRAO_CONSTRUCAO);
	}

	public Class<?> getClasseEntidadePavimentoRua() {

		return ServiceLocator.getInstancia().getClassPorID(PavimentoRua.BEAN_ID_PAVIMENTO_RUA);
	}

	public Class<?> getClasseEntidadePavimentoCalcada() {

		return ServiceLocator.getInstancia().getClassPorID(PavimentoCalcada.BEAN_ID_PAVIMENTO_CALCADA);
	}

	public Class<?> getClasseEntidadeSituacaoImovel() {

		return ServiceLocator.getInstancia().getClassPorID(SituacaoImovel.BEAN_ID_SITUACAO_IMOVEL);
	}

	public Class<?> getClasseEntidadePerfilImovel() {

		return ServiceLocator.getInstancia().getClassPorID(PerfilImovel.BEAN_ID_PERFIL_IMOVEL);
	}

	public Class<?> getClasseEntidadeTipoBotijao() {

		return ServiceLocator.getInstancia().getClassPorID(TipoBotijao.BEAN_ID_TIPO_BOTIJAO);
	}

	public Class<?> getClasseEntidadeGrupoFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(GrupoFaturamento.BEAN_ID_GRUPO_FATURAMENTO);
	}

	public Class<?> getClasseEntidadeTipoLeitura() {

		return ServiceLocator.getInstancia().getClassPorID(TipoLeitura.BEAN_ID_TIPO_LEITURA);
	}

	public Class<?> getClasseEntidadeImovelRamoAtividade() {

		return ServiceLocator.getInstancia().getClassPorID(ImovelRamoAtividade.BEAN_ID_IMOVEL_RAMO_ATIVIDADE);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadePontoConsumoCityGate() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumoCityGate.BEAN_ID_PONTO_CONSUMO_CITY_GATE);
	}

	public Class<?> getClasseEntidadeAreaConstruidaFaixa() {

		return ServiceLocator.getInstancia().getClassPorID(AreaConstruidaFaixa.BEAN_ID_AREA_CONSTRUIDA);
	}

	private ControladorPontoConsumo getControladorPontoConsumo(){
		return ServiceLocator.getInstancia().getControladorPontoConsumo();
	}

	/**
	 * Classe privada reponsável por criar uma
	 * ordenação especial por endereço.
	 *
	 * @author gmatos
	 */
	private static class OrdenacaoEspecialEndereco implements OrdenacaoEspecial {

		private static final String DESCRICAO_COMPLEMENTO = "descricaoComplemento";


		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#createAlias(org.hibernate.Criteria)
		 */
		@Override
		public void createAlias(Criteria criteria) {

			criteria.createAlias("quadraFace.endereco.cep", "cep");
		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(org.hibernate.Criteria, org.displaytag.properties.SortOrderEnum)
		 */
		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {
				criteria.addOrder(Order.asc("cep.logradouro"));
				criteria.addOrder(Order.asc("cep.tipoLogradouro"));
				criteria.addOrder(Order.asc(NUMERO_IMOVEL));
				criteria.addOrder(Order.asc(DESCRICAO_COMPLEMENTO));
			} else {
				criteria.addOrder(Order.desc("cep.logradouro"));
				criteria.addOrder(Order.desc("cep.tipoLogradouro"));
				criteria.addOrder(Order.desc(NUMERO_IMOVEL));
				criteria.addOrder(Order.desc(DESCRICAO_COMPLEMENTO));
			}

		}

		/*
		 * (non-Javadoc)
		 * @see br.com.ggas.util.OrdenacaoEspecial#addOrder(java.lang.String, org.displaytag.properties.SortOrderEnum, java.lang.Object)
		 */
		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}
	}

	/**
	 * Metodo criado para adicionar o criteria por causa do sonar.
	 * @param criteria  Criteria Principal da consulta
	 * @param value Valor Adionado na consulta
	 * @param criterion Argumento do criteria
	 */
	private void addCriteriaString(Criteria criteria, String value, Criterion criterion) {
		if (!StringUtils.isEmpty(value)) {
			criteria.add(criterion);
		}
	}

	/**
	 * Metodo criado para adicionar o criteria por causa do sonar.
	 * @param criteria  Criteria Principal da consulta
	 * @param value Valor Adionado na consulta
	 * @param criterion Argumento do criteria
	 */
	private void addCriteriaLong(Criteria criteria, Long longValue, Criterion criterion) {
		if ((longValue != null) && (longValue > 0)) {
			criteria.add(criterion);
		}
	}


	/**
	 * Metodo criado para adicionar o criteria por causa do sonar.
	 * @param criteria  Criteria Principal da consulta
	 * @param value Valor Adionado na consulta
	 * @param criterion Argumento do criteria
	 */
	private void addCriteriaInteger(Criteria criteria, Integer longValue, Criterion criterion) {
		if ((longValue != null) && (longValue > 0)) {
			criteria.add(criterion);
		}
	}


	/**
	 * Metodo criado para adicionar o criteria por causa do sonar.
	 * @param criteria  criteria  Criteria Principal da consulta
	 * @param longArray Array de longs com os filtros
	 * @param criterion Argumento do criteria
	 */
	private void addCriteriaLongArray (Criteria criteria, Long[] longArray, Criterion criterion) {
		if (longArray != null && longArray.length > 0) {
			criteria.add(criterion);
		}
	}

	/**
	 * Metodo criado para adicionar o criteria por causa do sonar.
	 * @param criteria  criteria  Criteria Principal da consulta
	 * @param booleanValue Array de longs com os filtros
	 * @param criterion Argumento do criteria
	 */
	private void addCriteriaBoolean (Criteria criteria, Boolean booleanValue, Criterion criterion) {
		if (booleanValue != null) {
			criteria.add(criterion);
		}
	}

	/**
	 *  Metodo criado para adicionar o criteria por causa do sonar.
	 * @param criteria Criteria Principal da consulta
	 * @param nomeImovel Nome do imovel
	 */
	private void addCriteriaNomeImovel(Criteria criteria, String nomeImovel) {
		if (!StringUtils.isEmpty(nomeImovel)) {
			addCriteriaString(criteria, nomeImovel, Restrictions.sqlRestriction(UPPER + "("
				+ Util.removerAcentuacaoQuerySql("this_.IMOV_NM") + ") " + LIKE + " '"
				+ Util.removerAcentuacao(Util.formatarTextoConsulta(nomeImovel)).toUpperCase() + "'"));
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #consultarImoveis(java.util.Map)
	 */
	@Override
	public Collection<Imovel> consultarImoveis(Map<String, Object> filtro) throws NegocioException {
		Criteria criteria = getCriteria();
		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS);
			addCriteriaLongArray(criteria, chavesPrimarias, Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			String nome = (String) filtro.get("nome");
			addCriteriaNomeImovel(criteria, nome);
			Long idCliente = (Long) filtro.get("idCliente");
			if ((idCliente != null) && (idCliente > 0)) {
				criteria.createCriteria("listaClienteImovel").createCriteria(CLIENTE).add(Restrictions.eq(CHAVE_PRIMARIA, idCliente));
			}
			aplicaCriteriaCepImovel(criteria, filtro);
			String numeroImovel = (String) filtro.get(NUMERO_IMOVEL);
			addCriteriaString(criteria, numeroImovel, Restrictions.eq(NUMERO_IMOVEL, numeroImovel));
			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			addCriteriaBoolean(criteria, habilitado, Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			String habilitadoAgencia = (String) filtro.get("habilitadoAgencia");
			if ("true".equalsIgnoreCase(habilitadoAgencia) || FALSE.equalsIgnoreCase(habilitadoAgencia)) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.valueOf(habilitadoAgencia)));
			}
			Long idSituacaoImovel = (Long) filtro.get("idSituacaoImovel");
			addCriteriaLong(criteria, idSituacaoImovel, Restrictions.eq("situacaoImovel.chavePrimaria", idSituacaoImovel));
			Integer numeroLote = (Integer) filtro.get(NUMERO_LOTE);
			addCriteriaInteger(criteria, numeroLote, Restrictions.eq(NUMERO_LOTE, numeroLote));
			Integer numeroSublote = (Integer) filtro.get(NUMERO_SUBLOTE);
			addCriteriaInteger(criteria, numeroSublote, Restrictions.eq(NUMERO_SUBLOTE, numeroSublote));
			aplicarCriteriaQuadraFace(criteria, filtro);
			Long pavimentoCalcada = (Long) filtro.get("idPavimentoCalcada");
			addCriteriaLong(criteria, pavimentoCalcada, Restrictions.eq("pavimentoCalcada.chavePrimaria", pavimentoCalcada));
			Long pavimentoRua = (Long) filtro.get("idPavimentoRua");
			addCriteriaLong(criteria, pavimentoRua, Restrictions.eq("pavimentoRua.chavePrimaria", pavimentoRua));
			Long padraoConstrucao = (Long) filtro.get("idPadraoConstrucao");
			addCriteriaLong(criteria, padraoConstrucao, Restrictions.eq("padraoConstrucao.chavePrimaria", padraoConstrucao));
			Long tipoBotijao = (Long) filtro.get("idTipoBotijao");
			addCriteriaLong(criteria, tipoBotijao, Restrictions.eq("tipoBotijao.chavePrimaria", tipoBotijao));
			Long idPerfilImovel = (Long) filtro.get("idPerfilImovel");
			addCriteriaLong(criteria, idPerfilImovel, Restrictions.eq("perfilImovel.chavePrimaria", idPerfilImovel));
			Long[] idAreaConstruidaFaixa = (Long[]) filtro.get("idAreaConstruidaFaixa");
			addCriteriaLongArray(criteria, idAreaConstruidaFaixa, Restrictions.in("areaConstruidaFaixa.chavePrimaria", idAreaConstruidaFaixa));
			String bloco = (String) filtro.get("bloco");
			addCriteriaString(criteria, bloco, Restrictions.like("nome", "%," + bloco + ",%"));
			addCriteriaPontoConsumo(criteria, filtro);
			String complementoImovel = (String) filtro.get("complementoImovel");
			addCriteriaComplementoImovel(criteria, complementoImovel);
			adicionaCriteriasCompostos(criteria, filtro);
		}
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		Collection<Imovel> retorno = criteria.list();
		if (retorno != null && !retorno.isEmpty()) {
			for (final Imovel imovel : retorno) {
				if (imovel.getImovelCondominio() != null) {
					Hibernate.initialize(imovel.getImovelCondominio());
				}
			}
		}
		return retorno;
	}

	/**
	 * Metodo criado para satisfazer o sonar
	 * @param criteria Criteria da consulta base
	 * @param complementoImovel Complemento do imovel
	 */
	private void addCriteriaComplementoImovel(Criteria criteria, String complementoImovel) {
		if (!StringUtils.isEmpty(complementoImovel)) {
			addCriteriaString(criteria, complementoImovel, Restrictions.ilike(DESCRICAO_COMPLEMENTO,
				Util.formatarTextoConsulta(complementoImovel)));
		}

	}

	/**
	 * Metodo criado para satisfazer o sonar
	 * @param criteria Criteria da consulta base
	 * @param filtro Parametros passados para rodar o criteria.
	 */
	private void addCriteriaPontoConsumo(Criteria criteria, Map<String, Object> filtro) {
		String nomeCliente = (String) filtro.get("nomeCliente");
		if (!StringUtils.isEmpty(nomeCliente)) {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(getClasseEntidadeContratoPontoConsumo(),
							"contratoPontoConsumo");
			detachedCriteria.createCriteria("contratoPontoConsumo.contrato", "contrato");
			detachedCriteria.createCriteria("contratoPontoConsumo.pontoConsumo", "pontoConsumo");
			detachedCriteria.createCriteria("contrato.clienteAssinatura").add(
							Restrictions.ilike("nome", Util.formatarTextoConsulta(nomeCliente)));
			detachedCriteria.createAlias("pontoConsumo.imovel", IMOVEL);
			detachedCriteria.setProjection(Property.forName(IMOVEL_CHAVE_PRIMARIA));
			criteria.add(Subqueries.propertyIn(CHAVE_PRIMARIA, detachedCriteria));
		}

	}

	/**
	 * Metodo Criado para satisfazer o sonar
	 * @param criteria Criteria de consutla base
	 * @param filtro Parametros passados de tela para consulta
	 */
	private void aplicaCriteriaCepImovel(Criteria criteria, Map<String, Object> filtro) {
		String idCepImovel = (String) filtro.get("idCepImovel");
		if (idCepImovel != null && !"".equals(idCepImovel)) {
			criteria.createCriteria(QUADRA_FACE).createCriteria(ENDERECO).createCriteria(CEP)
			.add(Restrictions.eq(CHAVE_PRIMARIA, Long.parseLong(idCepImovel)));
		}else {
			String cep = (String) filtro.get("cepImovel");
			if (!StringUtils.isEmpty(cep)) {
				criteria.createCriteria(QUADRA_FACE).createCriteria(ENDERECO).createCriteria(CEP).add(Restrictions.eq(CEP, cep));
			} else {
				criteria.setFetchMode(QUADRA_FACE, FetchMode.JOIN);
			}
		}

	}

	/**
	 * Metodo criado para satisfazer o sonar
	 * @param criteria Consulta Base
	 * @param filtro Parametros da query passados da tela.
	 */
	private void aplicarCriteriaQuadraFace(Criteria criteria, Map<String, Object> filtro) {
		QuadraFace quadraFace = (QuadraFace) filtro.get(QUADRA_FACE);
		if (quadraFace != null) {
			criteria.createAlias(QUADRA_FACE, QUADRA_FACE);
			criteria.add(Restrictions.eq("quadraFace.chavePrimaria", quadraFace.getChavePrimaria()));
		} else {
			criteria.setFetchMode(QUADRA_FACE, FetchMode.JOIN);
		}
		if (filtro.get("quadraFaceChavePrimaria") != null) {
			criteria.createAlias(QUADRA_FACE, QUADRA_FACE);
			criteria.add(Restrictions.eq("quadraFace.chavePrimaria", filtro.get("quadraFaceChavePrimaria")));
		}

	}

	/**
	 * Metodo criado para verificar se o Long esta preenchido
	 * @param number numero a ser verificado
	 * @return Se o numero esta preenchido ou nao
	 */
	private boolean verificaLongMaiorZero(Long number) {
		return (number != null && number > 0);
	}

	/**
	 * Metodo separado por causa do sonar
	 * @param criteria Criteria Base da consulta
	 * @param filtro Filtro com os valores para serem iltrados no criteria
	 */
	private void adicionaCriteriasCompostos(Criteria criteria, Map<String, Object> filtro) {
		Long idRota = (Long) filtro.get("idRota");
		Long idGrupoFaturamento = (Long) filtro.get("idGrupoFaturamento");
		boolean listaPontoConsumoBool = false;
		if (verificaLongMaiorZero(idRota)) {
			listaPontoConsumoBool = true;
			criteria.createAlias(Imovel.LISTA_PONTO_CONSUMO, PONTO_CONSUMO);
			criteria.add(Restrictions.eq("ponto_consumo.rota.chavePrimaria", idRota));
		} else if (verificaLongMaiorZero(idGrupoFaturamento)) {
			listaPontoConsumoBool = true;
			criteria.createAlias(Imovel.LISTA_PONTO_CONSUMO, PONTO_CONSUMO);
			criteria.createAlias("ponto_consumo.rota", "rota");
			criteria.add(Restrictions.eq("rota.grupoFaturamento.chavePrimaria", idGrupoFaturamento));
		}
		boolean relacaoImovelCondominio = false;
		String indicadorCondominioAmbos = (String) filtro.get("indicadorCondominioAmbos");
		if (indicadorCondominioAmbos != null) {
			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (AMBOS.equalsIgnoreCase(indicadorCondominioAmbos)) {
				this.adicionarRestricaoPorChavePrimaria(chavePrimaria, criteria);
				relacaoImovelCondominio = true;
				criteria.setFetchMode(IMOVEL_CONDOMINIO, FetchMode.JOIN);
			} else if ("true".equalsIgnoreCase(indicadorCondominioAmbos)) {
				this.adicionarRestricaoPorChavePrimaria(chavePrimaria, criteria);
				criteria.add(Restrictions.eq(CONDOMINIO, Boolean.TRUE));
			} else if (FALSE.equalsIgnoreCase(indicadorCondominioAmbos)) {
				this.adicionarRestricaoPorChavePrimaria(chavePrimaria, criteria);
				criteria.setFetchMode(IMOVEL_CONDOMINIO, FetchMode.JOIN);
				relacaoImovelCondominio = true;
				criteria.add(Restrictions.eq(CONDOMINIO, Boolean.FALSE));
			}
		} else {
			Boolean indicadorCondominio = (Boolean) filtro.get("indicadorCondominio");
			if (indicadorCondominio != null) {
				criteria.add(Restrictions.eq(CONDOMINIO, indicadorCondominio));
			}
			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			addCriteriaLong(criteria, chavePrimaria, Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			Long matriculaCondominio = (Long) filtro.get("matriculaCondominio");
			if (verificaLongMaiorZero(matriculaCondominio)) {
				criteria.setFetchMode(IMOVEL_CONDOMINIO, FetchMode.JOIN);
				criteria.add(Restrictions.eq("imovelCondominio.chavePrimaria", matriculaCondominio));
			} else {
				criteria.setFetchMode(IMOVEL_CONDOMINIO, FetchMode.JOIN);
			}
		}

		addCriteriaMedicaoAmbos(criteria, filtro, relacaoImovelCondominio);
		listaPontoConsumoBool = addCriteriaPontoConsumoLegado(criteria, filtro, listaPontoConsumoBool);
		listaPontoConsumoBool = addCriteriaPontosConsumo(criteria, filtro, listaPontoConsumoBool);
		listaPontoConsumoBool = addCriteriaSegmentoPontoConsumo(criteria, filtro, listaPontoConsumoBool);
		listaPontoConsumoBool = addCriteriaSituacaoPontoConsumo(criteria, filtro, listaPontoConsumoBool);
		listaPontoConsumoBool = addCriteriaOrdenacaoSituacaoPontoConsumo(criteria, filtro, listaPontoConsumoBool);
		addCriteriaRamoAtividadePontoConsumo(criteria, filtro, listaPontoConsumoBool);
		addCriteriaPaginacaoPadrao(criteria, filtro);
	}

	private boolean addCriteriaOrdenacaoSituacaoPontoConsumo(Criteria criteria, Map<String, Object> filtro,
			boolean listaPontoConsumoBool) {
		ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
		if (colecaoPaginada != null && colecaoPaginada.getSortCriterion() != null && colecaoPaginada.getSortCriterion().equals("situacaoC.descricao")) {
			if (!listaPontoConsumoBool) {
				criteria.createAlias(Imovel.LISTA_PONTO_CONSUMO, PONTO_CONSUMO);
			}
			
			criteria.createAlias("ponto_consumo.situacaoConsumo", "situacaoC");
			return true;
		}
		return listaPontoConsumoBool;
	}
	
	private boolean addCriteriaSituacaoPontoConsumo(Criteria criteria, Map<String, Object> filtro,
			boolean listaPontoConsumoBool) {
		
		Long idSituacaoPontoConsumo = (Long) filtro.get("idSituacaoPontoConsumo");
		if (idSituacaoPontoConsumo != null) {
			if (listaPontoConsumoBool) {
				criteria.add(Restrictions.eq("ponto_consumo.situacaoConsumo.chavePrimaria", idSituacaoPontoConsumo));
				return listaPontoConsumoBool;
			} else {
				criteria.createAlias(Imovel.LISTA_PONTO_CONSUMO, PONTO_CONSUMO);
				criteria.add(Restrictions.eq("ponto_consumo.situacaoConsumo.chavePrimaria", idSituacaoPontoConsumo));
				return true;
			}
		}
		return listaPontoConsumoBool;
		
	}

	/**
	 *
	 * @param criteria
	 * @param filtro
	 * @param relacaoImovelCondominio
	 */
	private void addCriteriaMedicaoAmbos(Criteria criteria, Map<String, Object> filtro,boolean relacaoImovelCondominio) {
		String tipoMedicaoAmbos = (String) filtro.get("tipoMedicaoAmbos");
		if (StringUtils.isNotEmpty(tipoMedicaoAmbos) && !tipoMedicaoAmbos.equals(AMBOS)) {
			if (!relacaoImovelCondominio) {
				criteria.setFetchMode(IMOVEL_CONDOMINIO, FetchMode.JOIN);
			}
			criteria.createAlias(IMOVEL_CONDOMINIO, "ic", Criteria.LEFT_JOIN);
			Junction dj = Restrictions.disjunction();
			dj.add(Restrictions.eq("modalidadeMedicaoImovel.codigo", Integer.valueOf(tipoMedicaoAmbos)));
			dj.add(Restrictions.eq("ic.modalidadeMedicaoImovel.codigo", Integer.valueOf(tipoMedicaoAmbos)));
			criteria.add(dj);
		}
	}

	/**
	 * Metodo para adicionar o criterio do ponto de consumo legado
	 * @param criteria  Criteria Base da consulta
	 * @param filtro Filtro com os valores da tela
	 * @param listaPontoConsumoBool  Se a consulta inclui os pontos de consumo ou nao
	 * @return  Se a consulta inclui os pontos de consumo ou nao
	 */
	private boolean addCriteriaPontoConsumoLegado(Criteria criteria, Map<String, Object> filtro, boolean listaPontoConsumoBool ) {
		String idPontoConsumoLegado = (String) filtro.get("pontoConsumoLegado");
		if (StringUtils.isNotEmpty(idPontoConsumoLegado)) {
			if (listaPontoConsumoBool) {
				Restrictions.eq("ponto_consumo.codigoLegado", idPontoConsumoLegado);
				return listaPontoConsumoBool;
			} else {
				criteria.createAlias(Imovel.LISTA_PONTO_CONSUMO, PONTO_CONSUMO);
				criteria.add(Restrictions.eq("ponto_consumo.codigoLegado", idPontoConsumoLegado));
				return true;
			}
		}
		return listaPontoConsumoBool;
	}

	/**
	 * Metodo para adicionar o criteria dos pontos de consumo
	 * @param criteria Criteria Base da consulta
	 * @param filtro Filtro com os valores da tela
	 * @param listaPontoConsumoBool Se a consulta inclui os pontos de consumo ou nao
	 * @return Se a consulta inclui os pontos de consumo ou nao.
	 */
	private boolean addCriteriaPontosConsumo(Criteria criteria, Map<String, Object> filtro, boolean listaPontoConsumoBool) {
		String descricaoPontoConsumo = (String) filtro.get("descricaoPontoConsumo");
		if (StringUtils.isNotEmpty(descricaoPontoConsumo)) {
			if (listaPontoConsumoBool) {
				criteria.add(
						Restrictions.sqlRestriction(UPPER + "(" + Util.removerAcentuacaoQuerySql("POCN_DS") + ") " + LIKE + " '"
						+ Util.removerAcentuacao(Util.formatarTextoConsulta(descricaoPontoConsumo)).toUpperCase() + "'"));
				return listaPontoConsumoBool;
			} else {
				criteria.createAlias(Imovel.LISTA_PONTO_CONSUMO, PONTO_CONSUMO);
				criteria.add(
						Restrictions.sqlRestriction(UPPER + "(" + Util.removerAcentuacaoQuerySql("POCN_DS") + ") " + LIKE + " '"
								+ Util.removerAcentuacao(Util.formatarTextoConsulta(descricaoPontoConsumo)).toUpperCase() + "'"));
				return true;
			}
		}
		return listaPontoConsumoBool;
	}

	/**
	 *
	 * @param criteria
	 * @param filtro
	 * @param listaPontoConsumoBool
	 */
	private boolean addCriteriaSegmentoPontoConsumo(Criteria criteria, Map<String, Object> filtro, boolean listaPontoConsumoBool) {
		Long idSegmentoPontoConsumo = (Long) filtro.get("idSegmentoPontoConsumo");
		if (verificaLongMaiorZero(idSegmentoPontoConsumo)) {
			if (listaPontoConsumoBool) {
				criteria.add(Restrictions.eq("ponto_consumo.segmento.chavePrimaria", idSegmentoPontoConsumo));
				return listaPontoConsumoBool;
			} else {
				criteria.createAlias(Imovel.LISTA_PONTO_CONSUMO, PONTO_CONSUMO);
				criteria.add(Restrictions.eq("ponto_consumo.segmento.chavePrimaria", idSegmentoPontoConsumo));
				listaPontoConsumoBool = true;
				return true;
			}
		}
		return listaPontoConsumoBool;
	}

	/**
	 * Metodo para adicionar o ramo de atividade ponto consumo no criteria
	 * @param criteria Criteria padrao da busca
	 * @param filtro Objeto da tela com os filtros
	 * @param listaPontoConsumoBool se é para incluir os pontos de consumo ou nao
	 */
	private void  addCriteriaRamoAtividadePontoConsumo(Criteria criteria, Map<String, Object> filtro, boolean listaPontoConsumoBool) {
		Long idRamoAtividadePontoConsumo = (Long) filtro.get("idRamoAtividadePontoConsumo");
		if (verificaLongMaiorZero(idRamoAtividadePontoConsumo)) {
			if (listaPontoConsumoBool) {
				criteria.add(Restrictions.eq("ponto_consumo.ramoAtividade.chavePrimaria", idRamoAtividadePontoConsumo));
			} else {
				criteria.createAlias(Imovel.LISTA_PONTO_CONSUMO, PONTO_CONSUMO);
				criteria.add(Restrictions.eq("ponto_consumo.ramoAtividade.chavePrimaria", idRamoAtividadePontoConsumo));
			}
		}
	}


	/**
	 * Metodo para adicionar a paginacao no criteria
	 * @param criteria Criteria padrao da busca
	 * @param filtro Objeto da tela com os filtros
	 */
	private void addCriteriaPaginacaoPadrao(Criteria criteria, Map<String, Object>  filtro) {
		boolean paginacaoPadrao = false;
		if (filtro.containsKey("colecaoPaginada")) {
			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
			colecaoPaginada.adicionarOrdenacaoEspecial("enderecoFormatado", new OrdenacaoEspecialEndereco());
			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);
			if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}
		} else {
			paginacaoPadrao = true;
		}
		if (paginacaoPadrao) {
			criteria.addOrder(Order.asc("nome"));
			criteria.addOrder(Order.asc(DESCRICAO_COMPLEMENTO));
		}
	}

	/**
	 * Metodo para Adicionar a restricao por chave primaria
	 * @param chavePrimaria Identificador unico
	 * @param criteria Criteria base da consulta
	 */
	private void adicionarRestricaoPorChavePrimaria(Long chavePrimaria, Criteria criteria){
		if ((chavePrimaria != null) && (chavePrimaria > 0)) {
			criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#consultarImovelRamoAtividade(java.util.Map)
	 */
	@Override
	public Collection<ImovelRamoAtividade> consultarImovelRamoAtividade(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeImovelRamoAtividade());

		if (filtro != null) {
			Long idRamoAtividade = (Long) filtro.get("idRamoAtividade");
			if (idRamoAtividade != null && idRamoAtividade > 0) {
				criteria.add(Restrictions.eq("ramoAtividade.chavePrimaria", idRamoAtividade));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

		}

		return criteria.list();

	}

	/**
	 * Método responsável por validar os dados do
	 * contato do imóvel.
	 *
	 * @param contato
	 *            O contato do imóvel.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	@Override
	public void validarDadosContatoImovel(ContatoImovel contato) throws NegocioException {

		Map<String, Object> errosContato = contato.validarDados();

		if (errosContato != null && !errosContato.isEmpty()) {
			throw new NegocioException(errosContato);
		}

		if (!StringUtils.isEmpty(contato.getEmail()) && !Util.validarDominio(contato.getEmail(), Constantes.EXPRESSAO_REGULAR_EMAIL)) {
			throw new NegocioException(ERRO_NEGOCIO_EMAIL_INVALIDO, ContatoCliente.EMAIL);
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel #verificarEmailContatoExistente(java .lang.Integer, java.util.Collection,
	 * br.com.ggas.cadastro.imovel.ContatoImovel)
	 */
	@Override
	public void verificarEmailContatoExistente(Integer indexLista, Collection<ContatoImovel> listaContatoImovel,
			ContatoImovel contatoImovel) throws NegocioException {

		for (int i = 0; i < listaContatoImovel.size(); i++) {

			ContatoImovel contatoImovelExistente = new ArrayList<>(listaContatoImovel).get(i);

			String email = contatoImovelExistente.getEmail();
			if (email != null && email.equals(contatoImovel.getEmail()) && indexLista != i) {
				throw new NegocioException(ERRO_NEGOCIO_EMAIL_CONTATO_EXISTENTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if (entidadeNegocio.getClass().equals(getClasseEntidade())) {
			Imovel imovel = (Imovel) entidadeNegocio;
			validarImovelJaExistente(imovel);

			if (imovel.getCondominio() && imovel.getQuantidadeBloco() != null && imovel.getQuantidadeBloco() == 0) {
				throw new NegocioException(ERRO_IMOVEL_LOTE_BLOCO_IGUAL_ZERO, true);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#inserirImovel(br.com.ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public Long inserirImovel(Imovel imovel) throws GGASException {

		return this.inserirImovel(imovel, false);
	}

	/**
	 * Insere imovel
	 *
	 * @param imovel
	 * @param veioDoChamado
	 * @return imovel id
	 * @throws GGASException
	 */
	public Long inserirImovel(Imovel imovel, Boolean veioDoChamado) throws GGASException {

		this.validador.validar(imovel);

		long codigoImovel = this.inserir(imovel);
		imovel.setChavePrimaria(codigoImovel);

		if (veioDoChamado != null && veioDoChamado) {
			this.enviarEmailCadastroChamado(imovel);
		}

		return codigoImovel;
	}

	/**
	 * Envia email para imoveis cadastrados a partir de chamado
	 *
	 * @param imovel
	 * @throws GGASException
	 */
	private void enviarEmailCadastroChamado(Imovel imovel) throws GGASException {
		ParametroSistema servidorEmailConfigurado =
				parametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_EMAIL_CONFIGURADO);

		ParametroSistema emailRemetentePadrao = parametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
		JavaMailUtil mailUtil = (JavaMailUtil) ServiceLocator.getInstancia().getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

		if (servidorEmailConfigurado != null && BooleanUtil.converterStringCharParaBooleano(servidorEmailConfigurado.getValor())) {
			String assunto = "Imóvel Cadastrado a partir de Chamado";
			String conteudo = montarEmail(imovel);

			Collection<Usuario> administradoresAtendimento = controladorUsuario.listarAdministradoresAtendimento();
			for (Usuario usuario : administradoresAtendimento) {
				String emailDestino = usuario.getFuncionario().getEmail();
				if (StringUtils.isNotBlank(emailDestino)) {
					mailUtil.enviar(emailRemetentePadrao.getValor(), emailDestino, assunto, conteudo, true, false);
				}
			}
		}
	}

	/**
	 * Monta a mensagem do email
	 *
	 * @param imovel
	 * @return conteudo do email
	 */
	private String montarEmail(Imovel imovel) {
		return "<h3>O seguinte Imóvel foi cadastrado a partir do menu de chamado</h3><br /><br />"
				.concat("<p><strong>Imóvel: </strong>").concat(imovel.getNome()).concat("</p> ")
				.concat("<p><strong>Código do imóvel: </strong>").concat(String.valueOf(imovel.getChavePrimaria())).concat("</p> ")
				.concat("<p><strong>Endereço do imóvel: </strong>").concat(imovel.getEnderecoFormatado()).concat("</p>");
	}

	/**
	 * Atualizar imovel.
	 *
	 * @param imovel the imovel
	 * @throws NegocioException the negocio exception
	 */
	@Override
	@Transactional
	public void atualizarImovel(Imovel imovel) throws NegocioException {

		try {
			this.validador.validar(imovel);

			for (PontoConsumo p : imovel.getListaPontoConsumo()) {
				if (p.getChavePrimaria() == 0) {
					this.inserir(p);
				} else {
					ControladorIntegracao controladorIntegracao = (ControladorIntegracao) ServiceLocator.getInstancia()
							.getBeanPorID(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
					controladorIntegracao.atualizarPontoConsumoIntegracaoContrato(p);
					super.atualizar(p, p.getClass());
				}
			}

			this.atualizar(imovel, ImovelImpl.class);
		} catch (ConcorrenciaException e) {
			LOG.error(e.getStackTrace(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#removerImovel(java.lang.Long[], br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerImovel(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Inserir ponto consumo tributo aliquota.
	 *
	 * @param pontoConsumoTributoAliquota
	 *            the ponto consumo tributo aliquota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void inserirPontoConsumoTributoAliquota(PontoConsumoTributoAliquota pontoConsumoTributoAliquota) throws NegocioException {

		this.inserir(pontoConsumoTributoAliquota);
	}

	/**
	 * Atualizar ponto consumo tributo aliquota.
	 *
	 * @param pontoConsumoTributoAliquota
	 *            the ponto consumo tributo aliquota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void atualizarPontoConsumoTributoAliquota(PontoConsumoTributoAliquota pontoConsumoTributoAliquota) throws NegocioException {

		try {
			atualizar(pontoConsumoTributoAliquota, getClasseEntidadePontoConsumoTributoAliquota());
		} catch (ConcorrenciaException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Consultar pontos consumo tributo aliquota.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings({"squid:S1192", "unchecked"})
	@Override
	public Collection<PontoConsumoTributoAliquota> consultarPontosConsumoTributoAliquota(Map<String, Object> filtro)
					throws NegocioException {
		if (filtro == null) {
			return Collections.emptyList();
		}

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadePontoConsumoTributoAliquota().getSimpleName());
		hql.append(" pctl ");
		hql.append(" inner join fetch pctl.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel imovel ");
		hql.append(WHERE);

		Boolean flagWhere = false;
		Long idImovel = (Long) filtro.get(ID_IMOVEL);
		if (idImovel != null && idImovel > 0) {
			hql.append(" pctl.pontoConsumo.imovel.chavePrimaria = :idImovel ");
			flagWhere = true;
		}

		Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
		if (habilitado != null && habilitado.equals(true)) {
			if (flagWhere) {
				hql.append(" and pctl.habilitado = :habilitado");
			} else {
				hql.append(" pctl.habilitado = :habilitado");
			}
		}

		Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
		if (chavePrimaria != null && chavePrimaria > 0) {
			hql.append(" and pctl.chavePrimaria = :chave");
		}

		Long idPontoConsumo = (Long) filtro.get("idPontoConsumo");
		if (idPontoConsumo != null && idPontoConsumo > 0) {
			hql.append(" and pctl.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (habilitado != null && habilitado.equals(true)) {
			query.setBoolean(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado);
		}

		if (chavePrimaria != null && chavePrimaria > 0) {
			query.setLong("chave", chavePrimaria);
		}

		if (idPontoConsumo != null && idPontoConsumo > 0) {
			query.setLong("idPontoConsumo", idPontoConsumo);
		}

		if (idImovel != null && idImovel > 0) {
			query.setLong(ID_IMOVEL, idImovel);
		}
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if (entidadeNegocio.getClass().equals(getClasseEntidade())) {
			Imovel imovel = (Imovel) entidadeNegocio;

			validarAlteracaoImovelCondominio(imovel);
			validarImovelJaExistente(imovel);
			if (imovel.getCondominio()) {
				validarAlteracaoNumeroBlocos(imovel);
			}

			validarAlteracaoIndicadorUso(imovel);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#posAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void posAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if (entidadeNegocio.getClass().equals(getClasseEntidade())) {
			propagarAlteracaoImovelCondominio((Imovel) entidadeNegocio);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preRemocao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if (entidadeNegocio.getClass().equals(getClasseEntidade())) {
			Imovel imovel = (Imovel) entidadeNegocio;
			validarRestricaoIntegridadeImovel(imovel);
		}

	}

	/**
	 * Validar alteracao imovel condominio.
	 *
	 * @param imovelAlterado
	 *            the imovel alterado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarAlteracaoImovelCondominio(Imovel imovelAlterado) throws NegocioException {

		Imovel imovelOriginal = (Imovel) obter(imovelAlterado.getChavePrimaria());
		if ((imovelAlterado.getCondominio() != null) && (!imovelAlterado.getCondominio().equals(imovelOriginal.getCondominio()))
						&& (imovelAlterado.getCondominio().equals(Boolean.FALSE))) {

			int totalFilhos = obterQtdFilhosImovel(imovelAlterado);
			if (totalFilhos > 0) {
				throw new NegocioException(ERRO_ALTERACAO_IMOVEL_CONDOMINIO, new Object[] {});
			}
		}
		getHibernateTemplate().getSessionFactory().getCurrentSession().evict(imovelOriginal);
	}

	/**
	 * Validar restricao integridade imovel.
	 *
	 * @param imovel
	 *            the imovel
	 */
	private void validarRestricaoIntegridadeImovel(Imovel imovel) {
		if (imovel  != null) {
			return;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #validarImovelJaExistente(br.com.procenge
	 * .ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public void validarImovelJaExistente(Imovel imovel) throws NegocioException {

		Integer numeroLote = imovel.getNumeroLote();
		Integer numeroSubLote = imovel.getNumeroSublote();
		QuadraFace quadraFace = imovel.getQuadraFace();

		if ((numeroLote != null && numeroLote.intValue() > 0) && (numeroSubLote != null && numeroSubLote.intValue() > 0)
						&& (quadraFace != null)) {
			Map<String, Object> filtro = new HashMap<>();
			filtro.put(QUADRA_FACE, quadraFace);
			filtro.put(NUMERO_LOTE, numeroLote);
			filtro.put(NUMERO_SUBLOTE, numeroSubLote);

			List<Imovel> listaImovel = (List<Imovel>) consultarImoveis(filtro);
			if (!listaImovel.isEmpty()) {
				Imovel imovelExistente = listaImovel.get(0);

				Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
				sessao.evict(imovelExistente);

				if (imovelExistente.getChavePrimaria() != imovel.getChavePrimaria() && imovel.getChavePrimaria() != 0
						&& imovel.getIdImovelCondominio() == null) {
					throw new NegocioException(ERRO_IMOVEL_JA_EXISTENTE, true);
				}
			}
		}
	}

	/**
	 * Com a retirada da Obrigatoriedade dos campos: quantidadeBloco, Quantidade Andar e Quantidade de apartamento
	 * por andar, caso esses campos nao forem infamdos nao eh necessario validar o valor informado
	 * @param imovel Imovel que esta sendo alterado
	 * @return Se os campos estao preenchido ou nao.
	 */
	private boolean validaCamposCondominioPreenchido(Imovel imovel) {
		return (imovel.getQuantidadeBloco() !=null && imovel.getQuantidadeAndar() != null
				&& imovel.getQuantidadeApartamentoAndar()!=null);
	}
    /**
	 * Validar alteracao numero blocos.
	 *
	 * @param imovel
	 *            the imovel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarAlteracaoNumeroBlocos(Imovel imovel) throws NegocioException {

		if (validaCamposCondominioPreenchido(imovel)) {
			Integer totalBlocosCadastrados = this.obterQtdBlocosCadastrados(imovel);

			if (imovel.getQuantidadeBloco() != null && imovel.getQuantidadeBloco() == 0) {
				throw new NegocioException(ERRO_IMOVEL_LOTE_BLOCO_IGUAL_ZERO, true);
			}

			if (imovel.getQuantidadeBloco() < totalBlocosCadastrados) {
				throw new NegocioException(ControladorImovel.ERRO_NEGOCIO_NUMERO_BLOCO_INVALIDO, true);
			}
		}

	}

	/**
	 * Validar alteracao indicador uso.
	 *
	 * @param imovel
	 *            the imovel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarAlteracaoIndicadorUso(Imovel imovel) throws NegocioException {

		if (!imovel.isHabilitado()) {

			Query query = null;

			StringBuilder hql = new StringBuilder();
			hql.append(" select count(contratoPontoConsumo) from ");
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(" contratoPontoConsumo ");
			hql.append(WHERE);
			hql.append(" contratoPontoConsumo.pontoConsumo.imovel.chavePrimaria = :idImovel ");
			hql.append(" and contratoPontoConsumo.contrato.situacao.chavePrimaria = :situacaoContrato");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong(ID_IMOVEL, imovel.getChavePrimaria());
			query.setLong(SITUACAO_CONTRATO, SituacaoContrato.ATIVO);

			Long resultado = (Long) query.uniqueResult();

			if ((resultado != null) && (resultado > 0)) {
				throw new NegocioException(ControladorImovel.ERRO_NEGOCIO_CONTRATO_ATIVO, true);
			}

			Collection<PontoConsumo> listaPontos = imovel.getListaPontoConsumo();
			for (PontoConsumo pontoConsumo : listaPontos) {
				if (getControladorPontoConsumo().isPontoConsumoAtivo(pontoConsumo)
						|| getControladorPontoConsumo().isPontoConsumoSuprimidoDefinitivo(pontoConsumo)) {
					throw new NegocioException(ControladorImovel.ERRO_NEGOCIO_PONTO_CONSUMO_ATIVO, true);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel #validarClientesRelacionados(java.util .List)
	 */
	@Override
	public void validarClientesRelacionados(ClienteImovel clienteImovelValidar, Collection<ClienteImovel> listaClienteImovel,
			Boolean atualizar) throws NegocioException {

		if (atualizar) {
			for (ClienteImovel clienteImovel : listaClienteImovel) {

				if ((clienteImovel.isHabilitado()) && (clienteImovel.getTipoRelacionamentoClienteImovel()
						.getChavePrimaria() == clienteImovelValidar.getTipoRelacionamentoClienteImovel().getChavePrimaria())) {

					this.validarClienteImovelExistentePeloCpf(clienteImovel, clienteImovelValidar);
					this.validarClienteImovelExistentePeloCnpj(clienteImovel, clienteImovelValidar);

				}
			}
		} else {
			if (listaClienteImovel != null && !listaClienteImovel.isEmpty()) {
				for (ClienteImovel clienteImovel : listaClienteImovel) {
					this.verificarTipoRelacionamentoJaExistente(clienteImovel, clienteImovelValidar);

				}
			}
		}
	}

	private void verificarTipoRelacionamentoJaExistente(ClienteImovel clienteImovel, ClienteImovel clienteImovelValidar)
			throws NegocioException {

		if (clienteImovel.isHabilitado()) {

			if (clienteImovel.getTipoRelacionamentoClienteImovel().getChavePrimaria() == clienteImovelValidar
					.getTipoRelacionamentoClienteImovel().getChavePrimaria()) {
				throw new NegocioException(ERRO_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL_TIPO_JA_EXISTENTE, true);
			}

			if ((!StringUtils.isEmpty(clienteImovel.getCliente().getCpf())
					&& !StringUtils.isEmpty(clienteImovelValidar.getCliente().getCpf()))
					&& (clienteImovel.getCliente().getCpf().equals(clienteImovelValidar.getCliente().getCpf()))) {
				throw new NegocioException(ERRO_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL_JA_EXISTENTE, true);
			}
		}

		if (!StringUtils.isEmpty(clienteImovel.getCliente().getCnpj()) && !StringUtils.isEmpty(clienteImovelValidar.getCliente().getCnpj())
				&& clienteImovel.getCliente().getCnpj().equals(clienteImovelValidar.getCliente().getCnpj())) {
			throw new NegocioException(ERRO_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL_JA_EXISTENTE, true);
		}
	}

	private void validarClienteImovelExistentePeloCpf(ClienteImovel clienteImovel, ClienteImovel clienteImovelValidar) throws NegocioException{
		if ((!StringUtils.isEmpty(clienteImovel.getCliente().getCpf()))
				&& (!StringUtils.isEmpty(clienteImovelValidar.getCliente().getCpf())
				&& !clienteImovel.getCliente().getCpf().equals(clienteImovelValidar.getCliente().getCpf()))) {
			throw new NegocioException(ERRO_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL_TIPO_JA_EXISTENTE, true);
		}

	}

	private void validarClienteImovelExistentePeloCnpj(ClienteImovel clienteImovel, ClienteImovel clienteImovelValidar)
			throws NegocioException {

		if ((!StringUtils.isEmpty(clienteImovel.getCliente().getCnpj())
				&& !StringUtils.isEmpty(clienteImovelValidar.getCliente().getCnpj()))
				&& !clienteImovel.getCliente().getCnpj().equals(clienteImovelValidar.getCliente().getCnpj())) {
			throw new NegocioException(ERRO_TIPO_RELACIONAMENTO_CLIENTE_IMOVEL_TIPO_JA_EXISTENTE, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #validarExcluirClientesRelacionados
	 * (java.util.List)
	 */
	@Override
	public void validarExcluirClientesRelacionados(ClienteImovel clienteImovel) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (clienteImovel.getMotivoFimRelacionamentoClienteImovel() == null) {
			stringBuilder.append(ClienteImovel.MOTIVO_FIM_RELACIONAMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (clienteImovel.getRelacaoFim() == null) {
			stringBuilder.append(ClienteImovel.RELACAO_FIM);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();
		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		if (!erros.isEmpty()) {
			throw new NegocioException(erros);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#obterClienteImovel(long)
	 */
	@Override
	public ClienteImovel obterClienteImovel(long chavePrimaria) throws NegocioException {

		return (ClienteImovel) super.obter(chavePrimaria, getClasseEntidadeClienteImovel());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterModalidadeAquecimento(long)
	 */
	@Override
	public ModalidadeAquecimento obterModalidadeAquecimento(long chavePrimaria) throws NegocioException {

		return (ModalidadeAquecimento) super.obter(chavePrimaria, getClasseEntidadeModalidadeAquecimento());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarImovelSituacao()
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<SituacaoImovel> listarImovelSituacao() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeSituacaoImovel().getSimpleName());
		hql.append(WHERE);
		hql.append(" habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarTipoRelacionamentoClienteImovel
	 * ()
	 */
	@Override
	public Collection<TipoRelacionamentoClienteImovel> listarTipoRelacionamentoClienteImovel() throws NegocioException {

		Query query = criarQueryParaCombosImovel(getClasseEntidadeTipoRelacionamentoClienteImovel().getSimpleName());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #validarRemoverImoveis(java.lang.Long
	 * [])
	 */
	@Override
	public void validarRemoverImoveis(Long[] chavesPrimarias) throws NegocioException {

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

		Map<String, Object> map = new HashMap<>();
		map.put(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, chavesPrimarias);
		Collection<Imovel> imoveis = consultarImoveis(map);
		if (imoveis != null) {
			for (Imovel imov : imoveis) {
				if (imov.getSituacaoImovel().getDescricao().equals(Constantes.IMOVEL_INTERLIGADO)) {
					throw new NegocioException(Constantes.ERRO_NAO_E_POSSIVEL_REMOVER_IMOVEL_INTERLIGADO, true);
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarClienteImovelPorChaveImovel(
	 * java.lang.Long,
	 * boolean)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<ClienteImovel> listarClienteImovelPorChaveImovel(Long chavePrimariaImovel)
					throws NegocioException {
		return queryListarCliente(chavePrimariaImovel, false);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarClienteImovelPorChaveImovel(
	 * java.lang.Long,
	 * boolean)
	 */
	@Override
	public Collection<ClienteImovel> listarCliImovelPorChaveImovel(Long chavePrimariaImovel)
					throws NegocioException {
		return queryListarCliente(chavePrimariaImovel, true);
	}

	/*
	 * Listar clientes
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #queryList(
	 * java.lang.Long,
	 * boolean,
	 * boolean)
	 */
	private Collection<ClienteImovel> queryListarCliente(Long chavePrimariaImovel, boolean distinct){
		
		Query query = null;

		StringBuilder hql = new StringBuilder();
		if (distinct){
			hql.append(" select distinct clienteImovel ");
		}
		hql.append(FROM);
		hql.append(getClasseEntidadeClienteImovel().getSimpleName());
		hql.append(" clienteImovel ");
		hql.append(" inner join fetch clienteImovel.imovel imovel");
		hql.append(" left join fetch clienteImovel.cliente cliente");
		hql.append(" left join fetch cliente.enderecos enderecos");
		hql.append(WHERE);
		hql.append(" clienteImovel.imovel.chavePrimaria = ? ");
		hql.append(" order by clienteImovel.relacaoFim desc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaImovel);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#listarClienteImovelPorChaveCliente(java.lang.Long, boolean)
	 */
	@Override
	public Collection<ClienteImovel> listarClienteImovelPorChaveCliente(Long chavePrimariaCliente, boolean listarInativos)
					throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeClienteImovel());
		criteria.createAlias(CLIENTE, CLIENTE);
		criteria.add(Restrictions.eq("cliente.chavePrimaria", chavePrimariaCliente));

		if (!listarInativos) {
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
		}
		criteria.setFetchMode(IMOVEL, FetchMode.JOIN);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarContatoImovelPorChaveImovel(
	 * java.lang.Long)
	 */
	@Override
	public Collection<ContatoImovel> listarContatoImovelPorChaveImovel(Long chavePrimariaImovel) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContatoImovel().getSimpleName());
		hql.append(" WHERE habilitado = true AND ");
		hql.append(" imovel.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaImovel);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarPontoConsumo(java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumo(Long chavePrimariaImovel) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" where habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #validarDadosClienteImovel(br.com.procenge
	 * .ggas.cadastro.imovel.ClienteImovel)
	 */
	@Override
	public void validarDadosClienteImovel(ClienteImovel clienteImovel) throws NegocioException {

		Map<String, Object> errosClienteImovel = clienteImovel.validarDados();

		if (errosClienteImovel != null && !errosClienteImovel.isEmpty()) {
			throw new NegocioException(errosClienteImovel);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #buscarTipoRelacionamentoClienteImovel
	 * (java.lang.Long)
	 */
	@Override
	public TipoRelacionamentoClienteImovel buscarTipoRelacionamentoClienteImovel(Long chavePrimaria) throws NegocioException {

		return (TipoRelacionamentoClienteImovel) obter(chavePrimaria, getClasseEntidadeTipoRelacionamentoClienteImovel());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarUnidadeConsumidoraPorChaveImovel
	 * (java.lang.Long)
	 */
	@Override
	public Collection<ImovelRamoAtividade> listarUnidadeConsumidoraPorChaveImovel(Long chavePrimariaImovel) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeImovelRamoAtividade());
		criteria.createAlias(IMOVEL, IMOVEL);
		criteria.add(Restrictions.eq(IMOVEL_CHAVE_PRIMARIA, chavePrimariaImovel));
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
		criteria.setFetchMode("ramoAtividade", FetchMode.JOIN);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #consultarImovelRamoAtividadePorSegmento
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<ImovelRamoAtividade> consultarImovelRamoAtividadePorSegmento(Long idImovel, Long idSegmento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeImovelRamoAtividade().getSimpleName());
		hql.append(WHERE);
		hql.append(" habilitado = true ");
		hql.append(" AND imovel.chavePrimaria = ? ");
		hql.append(" AND ramoAtividade.segmento.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idImovel);
		query.setLong(1, idSegmento);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarPontoConsumoPorChaveImovel(java
	 * .lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoPorChaveImovel(Long chavePrimariaImovel) throws NegocioException {

		return queryListarPontoConsumoPorChave(chavePrimariaImovel, true);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarPontoConsumoPorChaveImovel(java
	 * .lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumoPorChaveImovelAtivosInativos(Long chavePrimariaImovel) throws NegocioException {

		return queryListarPontoConsumoPorChave(chavePrimariaImovel, false);

	}
	
	/**
	 * Lista Ponto Consumo por chave primaria
	 * @param chavePrimariaImovel - {@link Long}
	 * @param habilitado - {@link boolean}
	 * @return Coleção de pontos de consumo
	 * @throws NegocioException - {@link NegocioException}
	 */
	private Collection<PontoConsumo> queryListarPontoConsumoPorChave(Long chavePrimariaImovel, boolean habilitado)throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(SELECT_DISTINCT);
		hql.append(CONSTANTE_PONTO_CONSUMO);
		hql.append(CONSTANTE_FROM);
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(IMOVEL_PONTO_CONSUMO);
		hql.append(" left join fetch pontoConsumo.listaPontoConsumoCityGate pontoCityGate");
		hql.append(" left join fetch pontoCityGate.cityGate cityGate");
		hql.append(WHERE);
		if (habilitado){
			hql.append(" pontoConsumo.habilitado = true ");
			hql.append(" and ");
		}
		hql.append(" pontoConsumo.imovel.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaImovel);

		return new HashSet<>(query.list());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#carregarPontoConsumoPorChaveImovel(java.lang.Long)
	 */
	@Override
	public Collection<PontoConsumo> carregarPontoConsumoPorChaveImovel(Long chavePrimariaImovel, Boolean isPontoAtivo) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(SELECT_DISTINCT);
		hql.append(CONSTANTE_PONTO_CONSUMO);
		hql.append(CONSTANTE_FROM);
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(IMOVEL_PONTO_CONSUMO);
		hql.append(" inner join fetch pontoConsumo.segmento segmento ");
		hql.append(" inner join fetch pontoConsumo.situacaoConsumo situacaoConsumo ");
		hql.append(" inner join fetch pontoConsumo.imovel imovel ");
		hql.append(WHERE);
		hql.append(" 1 = 1 ");
		if(isPontoAtivo) {
			hql.append(" and pontoConsumo.habilitado = true ");
		}
		hql.append(" and pontoConsumo.imovel.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaImovel);

		return new HashSet<>(query.list());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #validarDadosImovelRamoAtividade(java
	 * .lang.Long,
	 * java.lang.Long, java.lang.Integer)
	 */
	@Override
	public void validarDadosImovelRamoAtividade(Long chavePrimariaSegmento, Long chavePrimariaRamoAtividade, Integer quantidadeEconomia)
					throws NegocioException {

		Map<String, Object> errosImovelRamoAtividade = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (chavePrimariaSegmento == null || chavePrimariaSegmento <= 0) {
			stringBuilder.append(ImovelRamoAtividade.SEGMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (chavePrimariaRamoAtividade == null || chavePrimariaRamoAtividade <= 0) {
			stringBuilder.append(ImovelRamoAtividade.RAMO_ATIVIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (quantidadeEconomia == null) {
			stringBuilder.append(ImovelRamoAtividade.QUANTIDADE_ECONOMIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			errosImovelRamoAtividade.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
							camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		if (!errosImovelRamoAtividade.isEmpty()) {
			throw new NegocioException(errosImovelRamoAtividade);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #validarDadosPontoConsumo(br.com.procenge
	 * .ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void validarDadosPontoConsumo(PontoConsumo pontoConsumo) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		if (StringUtils.isEmpty(pontoConsumo.getDescricao())) {
			stringBuilder.append(PontoConsumo.DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (pontoConsumo.getChavePrimaria() > 0) {
			PontoConsumo pontoConsumoPersistido = (PontoConsumo) this.obter(pontoConsumo.getChavePrimaria(), IMOVEL);

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(pontoConsumoPersistido);
			if (exigeSequenciaLeituraAlteracaoPontoConsumo(pontoConsumoPersistido.getImovel().getChavePrimaria())
							&& ((pontoConsumo.getNumeroSequenciaLeitura() == null) || (pontoConsumo.getNumeroSequenciaLeitura() == 0))) {
				stringBuilder.append(PontoConsumo.NUMERO_SEQUENCIA_LEITURA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}
		if (pontoConsumo.getSegmento() == null) {
			stringBuilder.append(PontoConsumo.SEGMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (pontoConsumo.getRamoAtividade() == null) {
			stringBuilder.append(PontoConsumo.RAMO_ATIVIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (pontoConsumo.getCep() == null) {
			stringBuilder.append(PontoConsumo.CEP);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (pontoConsumo.getQuadraFace() == null) {
			stringBuilder.append(PontoConsumo.FACE_QUADRA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		Boolean indicadorRotaObrigatoria = Boolean.valueOf((String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_ACEITA_INCLUSAO_PONTO_CONSUMO_NA_ROTA));

		if (indicadorRotaObrigatoria && pontoConsumo.getRota() == null) {
			stringBuilder.append(PontoConsumo.PONTO_CONSUMO_ROTA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		validarLimites(pontoConsumo, erros);

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		if (!erros.isEmpty()) {
			throw new NegocioException(erros);
		}
	}


	private void validarLimites (PontoConsumo pontoConsumo, Map<String, Object> erros) throws NegocioException{

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		String unidadeFederacao = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_UNIDADE_FEDERACAO_IMPLANTACAO);

		Empresa empresa = fachada.obterEmpresaPrincipal();
		if(empresa != null){
			if(empresa.isPrincipal() && unidadeFederacao != null && unidadeFederacao.equals("5")){
				validarLimitesCoordenadasBa(pontoConsumo,erros);
			}else{
				validarLimitesCoordenadas(pontoConsumo,erros);
			}
		}else{
				validarLimitesCoordenadas(pontoConsumo,erros);
		}
	}


	/**
	 * Valida se os valores da latitude e longitude respeitam
	 * os limites da Bahia.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param erros the erros
	 */
	private void validarLimitesCoordenadasBa(PontoConsumo pontoConsumo, Map<String, Object> erros) {
		BigDecimalValidator verificadorCoordenadas = BigDecimalValidator.getInstance();
		BigDecimal latitudeGrau = pontoConsumo.getLatitudeGrau();
		BigDecimal longitudeGrau = pontoConsumo.getLongitudeGrau();

		if(latitudeGrau != null) {
			if(!verificadorCoordenadas.isInRange(latitudeGrau, LATITUDE_MIN_BA, LATITUDE_MAX_BA)) {
				erros.put(Constantes.ERRO_NUMERO_INVALIDO_LATITUDE_LONGITUDE_BA, StringUtils.EMPTY);
			}  else if (longitudeGrau != null && !verificadorCoordenadas.isInRange(longitudeGrau, LONGITUDE_MIN_BA, LONGITUDE_MAX_BA)){
				erros.put(Constantes.ERRO_NUMERO_INVALIDO_LATITUDE_LONGITUDE_BA, StringUtils.EMPTY);
			}
		}
	}

	/**
	 * Valida se os valores da latitude e longitude respeitam
	 * os limites demais estados.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param erros the erros
	 */
	private void validarLimitesCoordenadas(PontoConsumo pontoConsumo, Map<String, Object> erros) {
		BigDecimalValidator verificadorCoordenadas = BigDecimalValidator.getInstance();
		BigDecimal latitudeGrau = pontoConsumo.getLatitudeGrau();
		BigDecimal longitudeGrau = pontoConsumo.getLongitudeGrau();
		if(latitudeGrau != null) {
			if(!verificadorCoordenadas.isInRange(latitudeGrau, -LATITUDE_MAX, LATITUDE_MAX)) {
				erros.put(Constantes.ERRO_NUMERO_INVALIDO_LATITUDE_LONGITUDE, StringUtils.EMPTY);
			} else if(longitudeGrau != null &&
							!verificadorCoordenadas.isInRange(longitudeGrau, -LONGITUDE_MAX, LONGITUDE_MAX)) {
				erros.put(Constantes.ERRO_NUMERO_INVALIDO_LATITUDE_LONGITUDE, StringUtils.EMPTY);
			}
		}
	}


	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #validarComplementoImclusaoEmLote(java
	 * .lang.String[])
	 */
	@Override
	public void validarInclusaoImoveisEmLote(Imovel imovel, String[] apartamento, String[] bloco, String numeroBlocos, String tipoUnidade,
					String[] numeroSequenciaLeitura) throws NegocioException {

		validarBlocosJaCadastradosImoveisEmLote(imovel, bloco);
		validarDadosInclusaoImoveisEmLote(apartamento, tipoUnidade);
		validarBlocosInclusaoEmLote(bloco, numeroBlocos);
		validarApartamentosInclusaoEmLote(apartamento, bloco);
		validarNumerosSequenciaInclusaoEmLote(numeroSequenciaLeitura);
	}

	/**
	 * Validar numeros sequencia inclusao em lote.
	 *
	 * @param numerosSequenciaLeitura
	 *            the numeros sequencia leitura
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarNumerosSequenciaInclusaoEmLote(String[] numerosSequenciaLeitura) throws NegocioException {

		int numerosSequenciaLeituraRepetidos = 0;
		for (int i = 0; i < numerosSequenciaLeitura.length; i++) {
			String numeroSequencia = numerosSequenciaLeitura[i].trim();
			numerosSequenciaLeituraRepetidos = 0;

			for (int j = 0; j < numerosSequenciaLeitura.length; j++) {
				if (numeroSequencia.equals(numerosSequenciaLeitura[j].trim())) {
					numerosSequenciaLeituraRepetidos++;
				}
			}

			if (numerosSequenciaLeituraRepetidos > 1) {
				throw new NegocioException(ERRO_NEGOCIO_NUMERO_LEITURA_REPETIDOS, true);
			}
		}
	}

	/**
	 * Validar dados inclusao imoveis em lote.
	 *
	 * @param apartamento
	 *            the apartamento
	 *
	 * @param imovel
	 *            the imovel
	 * @param tipoUnidade
	 *            the tipo unidade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosInclusaoImoveisEmLote(String[] apartamento, String tipoUnidade)
					throws NegocioException {

		boolean apartamentoFoiPreenchido = true;
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (apartamento.length == 0) {
			apartamentoFoiPreenchido = false;
		}

		for (int i = 0; i < apartamento.length; i++) {
			if (StringUtils.isEmpty(apartamento[i].trim())) {
				apartamentoFoiPreenchido = false;
				break;
			}
		}

		if (!apartamentoFoiPreenchido) {
			stringBuilder.append("Apartamento");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (StringUtils.isEmpty(tipoUnidade)) {
			stringBuilder.append("Tipo da Unidade");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder
							.toString().length() - LIMITE_CAMPO));
		}
	}

	/**
	 * Validar blocos ja cadastrados imoveis em lote.
	 *
	 * @param imovel
	 *            the imovel
	 * @param blocos
	 *            the blocos
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarBlocosJaCadastradosImoveisEmLote(Imovel imovel, String[] blocos) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" imovelCondominio = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, imovel.getChavePrimaria());

		Collection<Imovel> imoveisCadastrados = query.list();

		List<String> blocosCadastrados = new ArrayList<>();

		for (Imovel imovelCadastrado : imoveisCadastrados) {
			if (imovelCadastrado.getDescricaoComplemento() != null) {
				String[] descricaoComplemento = imovelCadastrado.getDescricaoComplemento().split(" ");
				if (descricaoComplemento.length == QTD_COMPLEMENTO) {
					String bloco = descricaoComplemento[CONSTANTE_NUMERO_DOIS];
					this.adicionarBlocoCadastrados(blocos, bloco, blocosCadastrados);

				}
			}
		}

		if (!blocosCadastrados.isEmpty()) {
			StringBuilder blocosExistentes = new StringBuilder();
			for (String string : blocosCadastrados) {
				blocosExistentes.append(string);
				blocosExistentes.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			throw new NegocioException(ERRO_NEGOCIO_BLOCO_JA_CADASTRADO, blocosExistentes.toString());
		}
	}

	private void adicionarBlocoCadastrados(String[] blocos, String bloco, List<String> blocosCadastrados){
		for (int i = 0; i < blocos.length; i++) {
			if (bloco.equals(blocos[i]) && !blocosCadastrados.contains(bloco)) {
				blocosCadastrados.add(bloco);
			}
		}
	}

	/**
	 * Validar apartamentos inclusao em lote.
	 *
	 * @param apartamentos
	 *            the apartamentos
	 * @param blocos
	 *            the blocos
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarApartamentosInclusaoEmLote(String[] apartamentos, String[] blocos) throws NegocioException {

		int apartamentosRepetidos;
		for (int i = 0; i < apartamentos.length; i++) {
			apartamentosRepetidos = 0;
			String apartamento = apartamentos[i];

			if (blocos.length > 0) {
				String bloco = blocos[i];

				for (int j = 0; j < blocos.length; j++) {
					if (apartamento.equals(apartamentos[j]) && bloco.equals(blocos[j])) {
						apartamentosRepetidos++;
					}
				}

				if (apartamentosRepetidos > 1) {
					throw new NegocioException(ERRO_NEGOCIO_APARTAMENTOS_REPETIDOS, true);
				}
			}
		}
	}

	/**
	 * Validar blocos inclusao em lote.
	 *
	 * @param bloco
	 *            the bloco
	 * @param numeroBlocos
	 *            the numero blocos
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarBlocosInclusaoEmLote(String[] bloco, String numeroBlocos) throws NegocioException {

		Integer qtdBlocos = Integer.valueOf(numeroBlocos);
		Set<String> blocosNaoRepetidos = new HashSet<>();

		for (int i = 0; i < bloco.length; i++) {
			blocosNaoRepetidos.add(bloco[i]);
		}

		if (blocosNaoRepetidos.size() > qtdBlocos) {
			throw new NegocioException(ERRO_NEGOCIO_QUANTIDADE_BLOCOS_EXCEDIDA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarPavimentoCalcada()
	 */
	@Override
	public Collection<PavimentoCalcada> listarPavimentoCalcada() throws NegocioException {

		Query query = criarQueryParaCombosImovel(getClasseEntidadePavimentoCalcada().getSimpleName());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarPavimentoRua()
	 */
	@Override
	public Collection<PavimentoRua> listarPavimentoRua() throws NegocioException {

		Query query = criarQueryParaCombosImovel(getClasseEntidadePavimentoRua().getSimpleName());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarPadraoConstrucao()
	 */
	@Override
	public Collection<PadraoConstrucao> listarPadraoConstrucao() throws NegocioException {

		Query query = criarQueryParaCombosImovel(getClasseEntidadePadraoConstrucao().getSimpleName());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarPerfilImovel()
	 */
	@Override
	public Collection<PerfilImovel> listarPerfilImovel() throws NegocioException {

		Query query = criarQueryParaCombosImovel(getClasseEntidadePerfilImovel().getSimpleName());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarTipoBotijao()
	 */
	@Override
	public Collection<TipoBotijao> listarTipoBotijao() throws NegocioException {

		Query query = criarQueryParaCombosImovel(getClasseEntidadeTipoBotijao().getSimpleName());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarModalidadeUso()
	 */
	@Override
	public Collection<EntidadeConteudo> listarModalidadeUso() throws NegocioException {

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		return controladorEntidadeConteudo.obterListaModalidadesUso();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarModalidadeUso()
	 */
	@Override
	public Collection<EntidadeConteudo> listarTipoCombustivel() throws NegocioException {

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		return controladorEntidadeConteudo.obterListaTiposCombustiveis();
	}


	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterSituacaoConsumo(java.lang.String)
	 */
	@Override
	public SituacaoConsumo obterSituacaoConsumo(long idSituacaoConsumo) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeSituacaoConsumo().getSimpleName());
		hql.append(WHERE);
		hql.append(" habilitado = true AND ");
		hql.append(" chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idSituacaoConsumo);

		return (SituacaoConsumo) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #gerarImoveisEmLote(br.com.procenge
	 * .ggas.cadastro.imovel.Imovel,
	 * java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public List<Imovel> gerarImoveisEmLote(Imovel imovel, String numeroBlocos, String numeroAndares, String numeroAptAndar)
					throws NegocioException {

		StringBuilder campos = new StringBuilder();
		String camposObrigatorios = null;

		if (StringUtils.isEmpty(numeroBlocos)) {
			campos.append("N° de Blocos");
			campos.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (StringUtils.isEmpty(numeroAndares)) {
			campos.append("N° de Andares");
			campos.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (StringUtils.isEmpty(numeroAptAndar)) {
			campos.append("N° de Apartamentos por Andar");
			campos.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = campos.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, campos.toString()
							.length() - LIMITE_CAMPO));
		}

		int numeroBlocosTemp = Integer.parseInt(numeroBlocos);
		int numeroAndaresTemp = Integer.parseInt(numeroAndares);
		int numeroAptAndarTemp = Integer.parseInt(numeroAptAndar);
		int totalImoveis = numeroBlocosTemp * numeroAndaresTemp * numeroAptAndarTemp;

		List<Imovel> retorno = new ArrayList<>();
		for (int i = 0; i < totalImoveis; i++) {
			Imovel imovelNovo = (Imovel) criar();
			preencherImovelEmLote(imovelNovo, imovel);
			retorno.add(imovelNovo);
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #criarModalidadeMedicaoImovel()
	 */
	@Override
	public EntidadeDominio criarModalidadeMedicaoImovel() {

		return (EntidadeDominio) ServiceLocator.getInstancia().getBeanPorID(ModalidadeMedicaoImovel.BEAN_ID_MODALIDADE_MEDICAO_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarTodasModalidadeMedicaoImovel()
	 */
	@Override
	public Collection<ModalidadeMedicaoImovel> listarModalidadeMedicaoImovel() {

		List<ModalidadeMedicaoImovel> modalidades = new ArrayList<>();
		ModalidadeMedicaoImovel modalidadeMedicaoImovel = null;
		Set<?> mapa = ModalidadeMedicaoImovel.MODALIDADES_MEDICAO_IMOVEL.keySet();

		for (Iterator<?> iterator = mapa.iterator(); iterator.hasNext();) {
			Integer codigo = (Integer) iterator.next();
			modalidadeMedicaoImovel = (ModalidadeMedicaoImovel) this.criarModalidadeMedicaoImovel();
			modalidadeMedicaoImovel.setCodigo(codigo);
			modalidades.add(modalidadeMedicaoImovel);
		}

		Collections.sort(modalidades, new Comparator<ModalidadeMedicaoImovel>(){

			@Override
			public int compare(ModalidadeMedicaoImovel o1, ModalidadeMedicaoImovel o2) {

				return Integer.valueOf(o1.getCodigo()).compareTo(Integer.valueOf(o2.getCodigo()));
			}
		});

		return modalidades;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterModalidadeMedicaoImovel(java.
	 * lang.Integer)
	 */
	@Override
	public ModalidadeMedicaoImovel obterModalidadeMedicaoImovel(Integer codigo) {

		ModalidadeMedicaoImovel modalidadeMedicaoImovel = null;
		if (codigo != null && codigo > 0) {
			modalidadeMedicaoImovel = (ModalidadeMedicaoImovel) this.criarModalidadeMedicaoImovel();
			modalidadeMedicaoImovel.setCodigo(codigo);

		}

		return modalidadeMedicaoImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterSituacaoImovel(long)
	 */
	@Override
	public SituacaoImovel obterSituacaoImovel(long chavePrimaria) throws NegocioException {

		return (SituacaoImovel) super.obter(chavePrimaria, getClasseEntidadeSituacaoImovel());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterPadraoConstrucao(long)
	 */
	@Override
	public PadraoConstrucao obterPadraoConstrucao(long chavePrimaria) throws NegocioException {

		return (PadraoConstrucao) super.obter(chavePrimaria, getClasseEntidadePadraoConstrucao());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterPavimentoCalcada(long)
	 */
	@Override
	public PavimentoCalcada obterPavimentoCalcada(long chavePrimaria) throws NegocioException {

		return (PavimentoCalcada) super.obter(chavePrimaria, getClasseEntidadePavimentoCalcada());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterPavimentoRua(long)
	 */
	@Override
	public PavimentoRua obterPavimentoRua(long chavePrimaria) throws NegocioException {

		return (PavimentoRua) super.obter(chavePrimaria, getClasseEntidadePavimentoRua());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterPerfilImovel(long)
	 */
	@Override
	public PerfilImovel obterPerfilImovel(long chavePrimaria) throws NegocioException {

		return (PerfilImovel) super.obter(chavePrimaria, getClasseEntidadePerfilImovel());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterTipoBotijao(long)
	 */
	@Override
	public TipoBotijao obterTipoBotijao(long chavePrimaria) throws NegocioException {

		return (TipoBotijao) super.obter(chavePrimaria, getClasseEntidadeTipoBotijao());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel #obterTipoBotijao(long)
	 */
	@Override
	public ContatoImovel obterContatoImovel(long chavePrimaria) throws NegocioException {

		return (ContatoImovel) super.obter(chavePrimaria, getClasseEntidadeContatoImovel());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterMotivoFimRelacionamentoClienteImovel
	 * (long)
	 */
	@Override
	public MotivoFimRelacionamentoClienteImovel obterMotivoFimRelacionamentoClienteImovel(long chavePrimaria) throws NegocioException {

		return (MotivoFimRelacionamentoClienteImovel) super.obter(chavePrimaria, getClasseEntidadeMotivoFimRelacionamentoClienteImovel());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarMotivoFimRelacionamentoClienteImovel
	 * ()
	 */
	@Override
	public Collection<MotivoFimRelacionamentoClienteImovel> listarMotivoFimRelacionamentoClienteImovel() {

		Query query = criarQueryParaCombosImovel(getClasseEntidadeMotivoFimRelacionamentoClienteImovel().getSimpleName());
		return query.list();
	}

	/**
	 * Criar query para combos imovel.
	 *
	 * @param classe
	 *            the classe
	 * @return the query
	 */
	private Query criarQueryParaCombosImovel(String classe) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(classe);
		hql.append(" order by descricao ");

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
	}

	/**
	 * Obter qtd filhos imovel.
	 *
	 * @param imovel
	 *            the imovel
	 * @return the int
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private int obterQtdFilhosImovel(Imovel imovel) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT count(IM.chavePrimaria) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" IM ");
		hql.append(WHERE);
		hql.append(" IM.imovelCondominio.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, imovel.getChavePrimaria());

		Long total = (Long) query.uniqueResult();
		return total.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterImoveisFilhosDoCondominio(br.
	 * com.procenge.ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public Collection<Imovel> obterImoveisFilhosDoCondominio(Imovel imovel) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" imovelCondominio.chavePrimaria = ?");
		hql.append(" ORDER BY chavePrimaria ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, imovel.getChavePrimaria());
		return query.list();
	}

	/**
	 * Preencher imovel em lote.
	 *
	 * @param imovelNovo
	 *            the imovel novo
	 * @param imovelOriginal
	 *            the imovel original
	 */
	private void preencherImovelEmLote(Imovel imovelNovo, Imovel imovelOriginal) {

		imovelNovo.setImovelCondominio(imovelOriginal);

		imovelNovo.setVersao(imovelOriginal.getVersao());
		imovelNovo.setQuadra(imovelOriginal.getQuadraFace().getQuadra());
		imovelNovo.setQuadraFace(imovelOriginal.getQuadraFace());
		imovelNovo.setEnderecoReferencia(imovelOriginal.getEnderecoReferencia());
		imovelNovo.setPavimentoCalcada(imovelOriginal.getPavimentoCalcada());
		imovelNovo.setPavimentoRua(imovelOriginal.getPavimentoRua());
		imovelNovo.setSituacaoImovel(imovelOriginal.getSituacaoImovel());
		imovelNovo.setPerfilImovel(imovelOriginal.getPerfilImovel());
		imovelNovo.setPadraoConstrucao(imovelOriginal.getPadraoConstrucao());
		imovelNovo.setNumeroLote(imovelOriginal.getNumeroLote());
		imovelNovo.setNumeroTestada(imovelOriginal.getNumeroTestada());
		imovelNovo.setQuantidadePontoConsumo(imovelOriginal.getQuantidadePontoConsumo());
		imovelNovo.setNumeroImovel(imovelOriginal.getNumeroImovel());
		imovelNovo.setNome(imovelOriginal.getNome());
		imovelNovo.setCep(imovelOriginal.getCep());
		imovelNovo.setDataPrevisaoEncerramentoObra(imovelOriginal.getDataPrevisaoEncerramentoObra());
		imovelNovo.setRota(imovelOriginal.getRota());
		imovelNovo.setTipoCombustivel(imovelOriginal.getTipoCombustivel());
		if(imovelOriginal.getAgente() != null){
			imovelNovo.setAgente(imovelOriginal.getAgente());
		}
	}

	/**
	 * Propagar alteracao imovel condominio.
	 *
	 * @param imovelCondominio the imovel condominio
	 * @throws NegocioException the negocio exception
	 */
	private void propagarAlteracaoImovelCondominio(Imovel imovelCondominio) throws NegocioException {

		if ((imovelCondominio.getCondominio() != null) && (imovelCondominio.getCondominio().equals(Boolean.TRUE))
				&& (imovelCondominio.getIndicadorAlterarImovelCondominioFilho()!=null &&
				imovelCondominio.getIndicadorAlterarImovelCondominioFilho().equals(Boolean.TRUE))) {
			Collection<Imovel> listaImovel = obterImoveisFilhosDoCondominio(imovelCondominio);
			for (Imovel imovel : listaImovel) {
				imovel.setQuadra(imovelCondominio.getQuadraFace().getQuadra());
				imovel.setNumeroImovel(imovelCondominio.getNumeroImovel());
				imovel.setEnderecoReferencia(imovelCondominio.getEnderecoReferencia());
				imovel.setQuadraFace(imovelCondominio.getQuadraFace());
				imovel.setNumeroLote(imovelCondominio.getNumeroLote());
				imovel.setPavimentoCalcada(imovelCondominio.getPavimentoCalcada());
				imovel.setPavimentoRua(imovelCondominio.getPavimentoRua());
				imovel.setDadosAuditoria(imovelCondominio.getDadosAuditoria());
				imovel.setDataPrevisaoEncerramentoObra(imovelCondominio.getDataPrevisaoEncerramentoObra());
				imovel.setRota(imovelCondominio.getRota());
				imovel.setAgente(imovelCondominio.getAgente());
				imovel.setDescricaoComplemento(imovelCondominio.getDescricaoComplemento());

				imovel.setPadraoConstrucao(imovelCondominio.getPadraoConstrucao());
				imovel.setTipoCombustivel(imovelCondominio.getTipoCombustivel());
				imovel.setAreaConstruidaFaixa(imovelCondominio.getAreaConstruidaFaixa());
				imovel.setPerfilImovel(imovelCondominio.getPerfilImovel());
				imovel.setQuantidadeBanheiro(imovelCondominio.getQuantidadeBanheiro());
				imovel.setIndicadorObraTubulacao(imovelCondominio.getIndicadorObraTubulacao());
				imovel.setIndicadorRestricaoServico(imovelCondominio.getIndicadorRestricaoServico());
				imovel.setNumeroSublote(imovelCondominio.getNumeroSublote());
				imovel.setTipoCombustivel(imovelCondominio.getTipoCombustivel());
				imovel.setQuantidadeBloco(imovelCondominio.getQuantidadeBloco());
				imovel.setNumeroTestada(imovelCondominio.getNumeroTestada());

				(imovel).incrementarVersao();
				(imovel).setUltimaAlteracao(Calendar.getInstance().getTime());

				getHibernateTemplate().getSessionFactory().getCurrentSession().evict(imovelCondominio);
				try {
					this.atualizar(imovel);
				} catch (ConcorrenciaException e) {
					throw new NegocioException("Alteração imovel condominio não pode ser propagada",  e);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterPontoConsumoPorNumeroSerieMedidor
	 * (java.lang.String)
	 */
	@Override
	public PontoConsumo obterPontoConsumoPorNumeroSerieMedidor(String numeroSerieMedidor) throws NegocioException {

		return queryChaveprimariaSerie(0L, numeroSerieMedidor);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterPontoConsumoPorChaveMedidor(java
	 * .lang.Long)
	 */
	@Override
	public PontoConsumo obterPontoConsumoPorChaveMedidor(Long chavePrimaria) throws NegocioException {

		return queryChaveprimariaSerie(chavePrimaria, "");

	}
	/**
	 * Listar ponto de consumo pela chave e numero serie medidor
	 * @param chavePrimaria - {@link Long}
	 * @param numeroSerieMedidor - {@link String}
	 * @return Ponto de Consumo - {@link PontoConsumo}
	 * @throws NegocioException - {@link NegocioException}
	 */
	private PontoConsumo queryChaveprimariaSerie(Long chavePrimaria, String numeroSerieMedidor) throws NegocioException {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select ponto ");
		hql.append(FROM);
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" ponto ");
		hql.append(WHERE);
		if (chavePrimaria != 0){
			hql.append(" ponto.instalacaoMedidor.medidor.chavePrimaria = ? ");
		}
		if(!"".equals(numeroSerieMedidor)){
			hql.append(" ponto.instalacaoMedidor.medidor.numeroSerie = ? ");
		}
		hql.append(" and ponto.instalacaoMedidor.data = ");
		hql.append(" 	(select max(instalacao.data) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeMedidorInstalacao().getSimpleName());
		hql.append("    instalacao");
		hql.append(" 	where ");
		if (chavePrimaria != 0){
			hql.append(" 	instalacao.medidor.chavePrimaria = ?) ");
		}
		if(!"".equals(numeroSerieMedidor)){
			hql.append(" 	instalacao.medidor.numeroSerie = ?) ");
		}
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (chavePrimaria != 0){
			query.setLong(0, chavePrimaria);
			query.setLong(1, chavePrimaria);
		}
		if(!"".equals(numeroSerieMedidor)){
			query.setString(0, numeroSerieMedidor);
			query.setString(1, numeroSerieMedidor);
		}

		return (PontoConsumo) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #existeImovelPorMatricula(java.lang
	 * .Long)
	 */
	@Override
	public boolean existeImovelPorMatricula(Long matricula) throws NegocioException {

		boolean imovelInexistente = true;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, matricula);

		Imovel imovel = (Imovel) query.uniqueResult();

		if (imovel == null || !imovel.isHabilitado()) {
			throw new NegocioException(ControladorImovel.ERRO_NEGOCIO_IMOVEL_NAO_EXISTENTE, matricula);
		} else {
			imovelInexistente = false;
		}

		return imovelInexistente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #validarImovelSemPontoConsumo(java.
	 * util.Collection)
	 */
	@Override
	public void validarImovelSemPontoConsumo(Collection<PontoConsumo> listaPontosConsumo) throws NegocioException {

		if (listaPontosConsumo == null || listaPontosConsumo.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_IMOVEL_SEM_PONTO_CONSUMO, true);
		}
	}

	/**
	 * Validar caqrregar imovel em lote.
	 *
	 * @param imovel the imovel
	 * @param numeroBlocos the numero blocos
	 * @param numeroAndares the numero andares
	 * @param numeroAptAndar the numero Apt andar
	 * @param incrementoApartamento the incremento apartamento
	 * @param tipoUnidade the tipo unidade
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	@Override
	public void validarCarregarImoveisEmLote(Imovel imovel, String numeroBlocos, String numeroAndares, String numeroAptAndar,
					String inicioApartamento, String incrementoApartamento, String tipoUnidade) throws NegocioException {

		validarGeracaoApartamentosImoveisEmLote(numeroBlocos, numeroAndares, numeroAptAndar, inicioApartamento, incrementoApartamento,
				tipoUnidade);

		if ("0".equals(numeroBlocos)) {
			throw new NegocioException(ERRO_IMOVEL_LOTE_BLOCO_IGUAL_ZERO, true);
		}

		validarNumeroBlocosImoveisEmLote(imovel, Integer.valueOf(numeroBlocos));
		validarIncrementoImoveisEmLote(numeroAptAndar, incrementoApartamento);
	}

	/**
	 * Validar incremento imoveis em lote.
	 *
	 * @param numeroAptAndar
	 *            the numero apt andar
	 * @param incrementoApartamento
	 *            the incremento apartamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarIncrementoImoveisEmLote(String numeroAptAndar, String incrementoApartamento) throws NegocioException {

		Integer apartamentoPorAndar;
		Integer incremento;

		if ((!StringUtils.isEmpty(numeroAptAndar)) && (!StringUtils.isEmpty(incrementoApartamento))) {
			apartamentoPorAndar = Integer.valueOf(numeroAptAndar);
			incremento = Integer.valueOf(incrementoApartamento);

			if (incremento < apartamentoPorAndar) {
				throw new NegocioException(ControladorImovel.ERRO_NEGOCIO_INCREMENTO_MENOR, true);
			}
		}
	}

	/**
	 * Validar numero blocos imoveis em lote.
	 *
	 * @param imovel
	 *            the imovel
	 * @param numeroBlocos
	 *            the numero blocos
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarNumeroBlocosImoveisEmLote(Imovel imovel, Integer numeroBlocos) throws NegocioException {

		Integer blocosRestantes = obterQtdBlocosRestantesImoveisEmLote(imovel);

		if (numeroBlocos > blocosRestantes) {
			throw new NegocioException(ControladorImovel.ERRO_NEGOCIO_QUANTIDADE_BLOCO_INVALIDA, blocosRestantes);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #gerarApartamentosImoveisEmLote(java
	 * .lang.Integer, java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer,
	 * java.lang.Integer)
	 */
	@Override
	public List<String> gerarApartamentosImoveisEmLote(String numeroBlocos, String numeroAndares, String numeroAptAndar,
					String inicioApartamento, String incrementoApartamento) throws NegocioException {

		int qtdBlocos = Integer.parseInt(numeroBlocos);
		int qtdAndares = Integer.parseInt(numeroAndares);
		int qtdAptAndar = Integer.parseInt(numeroAptAndar);
		List<String> listaApartamentos = new ArrayList<>();

		if (qtdAndares != 1 || qtdAptAndar != 1) {
			int inicio = Integer.parseInt(inicioApartamento);
			int incremento = Integer.parseInt(incrementoApartamento);

			int nDigits = String.valueOf((qtdAndares-1)*incremento + inicio + qtdAptAndar - 1).length();
			String format = String.format("%%0%dd", nDigits);
			// Valor que será incrementado ao
			// apartamento de acordo com o seu andar.
			int incrementoAndar = 0;
			int apt;
			for (int i = 0; i < qtdBlocos; i++) {
				incrementoAndar = 0;
				for (int j = 0; j < qtdAndares; j++) {
					for (int k = 0; k < qtdAptAndar; k++) {
						apt = inicio + incrementoAndar + k;
						listaApartamentos.add(String.format(format, apt));
					}
					incrementoAndar += incremento;
				}
			}
		} else {
			for (int i = 0; i < qtdBlocos; i++) {
				listaApartamentos.add(String.valueOf(i + 1));
			}
		}

		return listaApartamentos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#gerarNumeroLeituraImoveisEmLote(java.lang.Integer)
	 */
	@Override
	public List<String> gerarNumeroLeituraImoveisEmLote(Integer qtdApartamentos) throws NegocioException {

		List<String> listaNumeroLeitura = new ArrayList<>();

		for (int i = qtdApartamentos; i > 0; i--) {
			listaNumeroLeitura.add(String.valueOf(i));
		}

		return listaNumeroLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#gerarBlocosImoveisEmLote(java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String[])
	 */
	@Override
	public List<String> gerarBlocosImoveisEmLote(String numeroBlocos, String numeroAndares, String numeroAptAndar, String[] nomeBlocos)
					throws NegocioException {

		int qtdAndares = Integer.parseInt(numeroAndares);
		int qtdAptAndar = Integer.parseInt(numeroAptAndar);
		int qtdApartamentoBloco = qtdAndares * qtdAptAndar;

		List<String> blocosAp = new ArrayList<>();

		if (nomeBlocos != null && (nomeBlocos.length > 0)) {
			for (int i = 0; i < nomeBlocos.length; i++) {

				for (int j = 0; j < qtdApartamentoBloco; j++) {
					blocosAp.add(nomeBlocos[i]);
				}
			}
		}

		return blocosAp;
	}

	/**
	 * Validar geracao apartamentos imoveis em lote.
	 *
	 * @param numeroBlocos
	 *            the numero blocos
	 * @param numeroAndares
	 *            the numero andares
	 * @param numeroAptAndar
	 *            the numero apt andar
	 * @param inicioApartamento
	 *            the inicio apartamento
	 * @param incrementoApartamento
	 *            the incremento apartamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarGeracaoApartamentosImoveisEmLote(String numeroBlocos, String numeroAndares, String numeroAptAndar,
					String inicioApartamento, String incrementoApartamento, String tipoUnidade) throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (StringUtils.isEmpty(numeroBlocos)) {
			stringBuilder.append(Imovel.QUANTIDADE_BLOCO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (StringUtils.isEmpty(numeroAndares)) {
			stringBuilder.append(Imovel.QUANTIDADE_ANDAR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (StringUtils.isEmpty(numeroAptAndar)) {
			stringBuilder.append(Imovel.QUANTIDADE_APARTAMENTO_ANDAR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (StringUtils.isEmpty(inicioApartamento)) {
			stringBuilder.append("Nº Inicial do Apartamento");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (StringUtils.isEmpty(incrementoApartamento)) {
			stringBuilder.append("Nº de Incremento");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (StringUtils.isEmpty(tipoUnidade)) {
			stringBuilder.append("Nº Tipo da Unidade");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder
							.toString().length() - LIMITE_CAMPO));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterQtdBlocosCadastrados(br.com.procenge
	 * .ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public Integer obterQtdBlocosCadastrados(Imovel imovel) throws NegocioException {

		Query query = null;
		Long quantidade = 0L;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(imovel.chavePrimaria) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" imovel where");
		hql.append(" imovel.imovelCondominio.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, imovel.getChavePrimaria());

		quantidade = (Long) query.uniqueResult();

		int totalBlocosCadastrados = 0;

		int qtdApto = imovel.getQuantidadeBloco() * imovel.getQuantidadeAndar() * imovel.getQuantidadeApartamentoAndar();
		int qdtAptBloco = imovel.getQuantidadeAndar() * imovel.getQuantidadeApartamentoAndar();
		int total2 = 0;

		if (quantidade < qtdApto) {
			MathContext mc = new MathContext(0, RoundingMode.CEILING);
			BigDecimal total = BigDecimal.valueOf(quantidade / qdtAptBloco);
			if (total.compareTo(BigDecimal.ZERO) == 0) {
				total2 = 0;
			} else {
				total = total.round(mc);
				total2 = (total.intValue() + 1);
			}
		} else {
			total2 = imovel.getQuantidadeBloco();
		}

		totalBlocosCadastrados = total2;

		return totalBlocosCadastrados;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterQtdBlocosRestantesImoveisEmLote
	 * (br.com.ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public Integer obterQtdBlocosRestantesImoveisEmLote(Imovel imovel) throws NegocioException {

		Integer totalBlocosCadastrados = this.obterQtdBlocosCadastrados(imovel);

		return imovel.getQuantidadeBloco() - totalBlocosCadastrados;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #exigeSequenciaLeituraAlteracaoPontoConsumo
	 * (java.lang.Long)
	 */
	@Override
	public boolean exigeSequenciaLeituraAlteracaoPontoConsumo(Long chavePrimariaImovel) throws NegocioException {

		boolean retorno = false;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valor = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_INTERLIGADO);

		Long codigoSituacaoInterligada = Long.valueOf(valor);

		Imovel imovel = (Imovel) this.obter(chavePrimariaImovel);
		Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
		sessao.evict(imovel);

		if (imovel.getSituacaoImovel().getChavePrimaria() == codigoSituacaoInterligada) {
			retorno = true;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#verificarSitucaoImovelPorRedeIndicador(java.lang.String, java.lang.String)
	 */
	@Override
	public Collection<SituacaoImovel> verificarSitucaoImovelPorRedeIndicador(String chaveQuadraFace, String selectChavePrimaria)
					throws GGASException {

		Imovel imovel = null;
		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorQuadra controladorQuadra = ServiceLocator.getInstancia().getControladorQuadra();

		if (selectChavePrimaria != null) {

			imovel = (Imovel) obter(Long.parseLong(selectChavePrimaria));
		}
		int codigoSitucaoImovel = 0;
		Collection<SituacaoImovel> listaSituacoesImovel = null;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		codigoSitucaoImovel = Integer.parseInt(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_INTERLIGADO));

		String idEmProspeccao = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_EM_PROSPECCAO);
		String idProspectado = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_STATUS_PROSPECTADO);

		if (chaveQuadraFace != null) {

			if (imovel == null || imovel.getSituacaoImovel().getChavePrimaria() != codigoSitucaoImovel) {

				if (!StringUtils.isEmpty(chaveQuadraFace)) {
					QuadraFace quadraFace = controladorQuadra.obterQuadraFace(Long.parseLong(chaveQuadraFace));

					listaSituacoesImovel = this.listarSituacoesImovelPorRedeIndicador(quadraFace, listaSituacoesImovel, codigoSitucaoImovel);

				}
			} else {
				listaSituacoesImovel = listarSituacaoImovelInterligado();
				listaSituacoesImovel.add(Fachada.getInstancia().buscarSituacaoImovelPorChave(Long.parseLong(idEmProspeccao)));
				listaSituacoesImovel.add(Fachada.getInstancia().buscarSituacaoImovelPorChave(Long.parseLong(idProspectado)));
			}
		}

		return listaSituacoesImovel;
	}

	private Collection<SituacaoImovel> listarSituacoesImovelPorRedeIndicador(QuadraFace quadraFace,
			Collection<SituacaoImovel> listaSituacoesImovelAux, int codigoSitucaoImovel) {
		Collection<SituacaoImovel> listaSituacoesImovel = listaSituacoesImovelAux;
		if (quadraFace != null) {
			RedeIndicador redeIndicador = quadraFace.getRedeIndicador();
			int codigoRedeIndicador = redeIndicador.getCodigo();
			listaSituacoesImovel = listarSituacoesImovelPorRedeIndicador(codigoRedeIndicador, codigoSitucaoImovel);
		}
		return listaSituacoesImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarSituacoesManterImovel()
	 */
	@Override
	public Collection<SituacaoImovel> listarSituacaoImovelInterligado() throws NegocioException {

		Query query = null;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ServiceLocator.getInstancia().getControladorParametroSistema();
		String valor = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_INTERLIGADO);

		Long idSituacaoImovelInterligado = Long.valueOf(valor);

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeSituacaoImovel().getSimpleName());
		hql.append(WHERE);
		hql.append(" habilitado = true ");
		hql.append(" and chavePrimaria = :idSituacao ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idSituacao", idSituacaoImovelInterligado);

		return query.list();
	}

	/**
	 * Listar situacoes imovel por rede indicador.
	 *
	 * @param codigoRedeIndicador the codigo rede indicador
	 * @param codigoSitucaoImovel the codigo situcao imovel
	 * @return the collection
	 */
	public Collection<SituacaoImovel> listarSituacoesImovelPorRedeIndicador(int codigoRedeIndicador, int codigoSitucaoImovel) {

		Query query = null;
		Long indicadorRede = null;
		Long indicadorRedeAmbos = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeSituacaoImovel().getSimpleName());
		hql.append(WHERE);
		hql.append(" habilitado = true ");
		if (codigoSitucaoImovel > 0) {
			hql.append(" and chavePrimaria <> :codigoSitucaoImovel ");
		}
		if (codigoRedeIndicador > 0) {

			if (codigoRedeIndicador == RedeIndicador.TEM || codigoRedeIndicador == RedeIndicador.TEM_PARCIALMENTE) {
				indicadorRede = Long.valueOf(RedeIndicador.TEM);
				indicadorRedeAmbos = Long.valueOf(RedeIndicador.AMBOS);
			} else if (codigoRedeIndicador == RedeIndicador.NAO_TEM) {
				indicadorRede = Long.valueOf(RedeIndicador.NAO_TEM);
				indicadorRedeAmbos = Long.valueOf(RedeIndicador.AMBOS);
			}

			if (codigoRedeIndicador == RedeIndicador.TEM || codigoRedeIndicador == RedeIndicador.TEM_PARCIALMENTE
					|| codigoRedeIndicador == RedeIndicador.NAO_TEM) {
				hql.append(" and redeIndicador in (:indicadorRede, :indicadorRedeAmbos) ");
			}
		}

		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (codigoRedeIndicador > 0 && (codigoRedeIndicador == RedeIndicador.TEM || codigoRedeIndicador == RedeIndicador.TEM_PARCIALMENTE
				|| codigoRedeIndicador == RedeIndicador.NAO_TEM)) {
			query.setLong("indicadorRede", indicadorRede);
			query.setLong("indicadorRedeAmbos", indicadorRedeAmbos);
		}
		if (codigoSitucaoImovel > 0) {
			query.setLong("codigoSitucaoImovel", codigoSitucaoImovel);
		}
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterClienteImovelPrincipal(java.lang
	 * .Long)
	 */
	@Override
	public ClienteImovel obterClienteImovelPrincipal(Long idImovel) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select clienteImovel from ");
		hql.append(getClasseEntidadeClienteImovel().getSimpleName());
		hql.append(" clienteImovel ");
		hql.append(" inner join fetch clienteImovel.cliente cliente");
		hql.append(WHERE);
		hql.append(" clienteImovel.habilitado = true ");
		hql.append(" and clienteImovel.tipoRelacionamentoClienteImovel.principal = true ");
		hql.append(" and clienteImovel.imovel.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idImovel);

		return (ClienteImovel) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterQuadraPorFaceQuadra(java.lang
	 * .Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Quadra obterQuadraPorFaceQuadra(Long idQuadraFace) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select imovel.quadraFace.quadra from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" imovel ");
		hql.append(WHERE);
		hql.append(" imovel.quadraFace = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idQuadraFace);

		return (Quadra) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#obterQuadraPorFaceQuadraPontoConsumo(java.lang.Long)
	 */
	@Override
	public Quadra obterQuadraPorFaceQuadraPontoConsumo(Long idQuadraFace) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select pontoConsumo.quadraFace.quadra from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo where ");
		hql.append(" pontoConsumo.quadraFace = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idQuadraFace);

		return (Quadra) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #listarPontoConsumo()
	 */
	@Override
	public Collection<PontoConsumo> listarPontoConsumo() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #validarRemocaoPontoConsumoImovel(long)
	 */
	@Override
	public void validarRemocaoPontoConsumoImovel(long chavePontoConsumo) throws NegocioException {

		if (chavePontoConsumo > 0) {
			validarPontoConsumoAssociadoContrato(chavePontoConsumo);
		}
	}

	/**
	 * Validar ponto consumo associado contrato.
	 *
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarPontoConsumoAssociadoContrato(long chavePontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(contratoPonto) from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPonto ");
		hql.append(WHERE);
		hql.append(" contratoPonto.pontoConsumo.chavePrimaria = :chavePontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePontoConsumo", chavePontoConsumo);

		Long resultado = (Long) query.uniqueResult();

		if (resultado != null && resultado > 0) {
			throw new NegocioException(ERRO_NEGOCIO_ASSOCIADO_PONTO_CONSUMO, false);
		}
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.cadastro.imovel.
	 * ControladorImovel#
	 * validarDatasAbaRelacionamento(java.util.Date
	 * , java.util.Date)
	 */
	@Override
	public void validarDatasAbaRelacionamento(Date dataRelacaoInicio, Date dataRelacaoFim) throws NegocioException {

		Date dataHoje = new Date(System.currentTimeMillis());
		if (dataRelacaoInicio != null && dataRelacaoInicio.compareTo(dataHoje) > 0) {
			throw new NegocioException(ERRO_DATA_RELACIONAMENTO_INICIO_INVALIDA, true);
		}

		if (dataRelacaoInicio != null && dataRelacaoFim != null && dataRelacaoInicio.compareTo(dataRelacaoFim) > 0) {
			throw new NegocioException(ERRO_DATA_RELACIONAMENTO_FIM_ANTERIOR_INICIO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#quantidadeImoveisFilhos(java.lang.Long)
	 */
	@Override
	public int quantidadeImoveisFilhos(Long chavePrimariaImovelCondominio) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select count(imovel) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" imovel ");
		hql.append(WHERE);
		hql.append(" imovel.imovelCondominio.chavePrimaria = :chavePrimariaImovelCondominio");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaImovelCondominio", chavePrimariaImovelCondominio);

		Long total = (Long) query.uniqueResult();

		return total.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterClienteImovelPrincipal(java.lang
	 * .Long)
	 */
	@Override
	public Collection<ClienteImovel> obterClienteImovelPorChave(Long idImovel) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select clienteImovel from ");
		hql.append(getClasseEntidadeClienteImovel().getSimpleName());
		hql.append(" clienteImovel ");
		hql.append(" inner join fetch clienteImovel.cliente cliente");
		hql.append(" inner join fetch cliente.enderecos enderecos");
		hql.append(WHERE);
		hql.append(" clienteImovel.imovel.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idImovel);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#validarPontoConsumoTributoAliquota(java.util.Collection,
	 * br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota, java.lang.Integer)
	 */
	@Override
	public void validarPontoConsumoTributoAliquota(Collection<PontoConsumoTributoAliquota> listPontosConsumoTributoAliquota,
			PontoConsumoTributoAliquota pontoConsumoTributoAliquota, Integer indexPontoConsumoTributoAliquota) throws NegocioException {

		Map<String, Object> validarPontoConsumoTributoAliquota = pontoConsumoTributoAliquota.validarDados();
		List<PontoConsumoTributoAliquota> pontosConsumoTributoAliquota = new ArrayList<>();

		if (!validarPontoConsumoTributoAliquota.isEmpty()) {
			throw new NegocioException(validarPontoConsumoTributoAliquota);
		}

		if (listPontosConsumoTributoAliquota != null) {
			pontosConsumoTributoAliquota.addAll(listPontosConsumoTributoAliquota);
		}

		if (listPontosConsumoTributoAliquota != null && !listPontosConsumoTributoAliquota.isEmpty()
				&& indexPontoConsumoTributoAliquota > -1) {
			pontosConsumoTributoAliquota.remove(indexPontoConsumoTributoAliquota.intValue());
		}

		if ((pontoConsumoTributoAliquota.getPorcentagemAliquota() != null)
				&& (pontoConsumoTributoAliquota.getPorcentagemAliquota().compareTo(BigDecimal.ZERO)) <= 0) {
			throw new NegocioException(ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_ZERO, true);
		} else {
			if (pontoConsumoTributoAliquota.getPorcentagemAliquota() != null
					&& pontoConsumoTributoAliquota.getPorcentagemAliquota().longValue() > PORCENTAGEM) {
				throw new NegocioException(ERRO_PONTO_CONSUMO_TRIBUTO_PORCENTAGEM_MAXIMO_100, true);
			}
		}

		if (pontoConsumoTributoAliquota.getDataInicioVigencia() != null && pontoConsumoTributoAliquota.getDataFimVigencia() != null
				&& !Util.converterDataParaStringSemHora(pontoConsumoTributoAliquota.getDataFimVigencia(), Constantes.FORMATO_DATA_BR)
						.equals(Util.converterDataParaStringSemHora(pontoConsumoTributoAliquota.getDataInicioVigencia(),
								Constantes.FORMATO_DATA_BR))
				&& pontoConsumoTributoAliquota.getDataFimVigencia().before(pontoConsumoTributoAliquota.getDataInicioVigencia())) {
			throw new NegocioException(ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_DATA_INICIO_VIGENCIA_MENOR_ATUAL, true);
		}

		if (!pontosConsumoTributoAliquota.isEmpty()) {
			for (PontoConsumoTributoAliquota pontoConsumoTributoAliquotaAtual : pontosConsumoTributoAliquota) {

				if (pontoConsumoTributoAliquota.getTributo().getChavePrimaria() == pontoConsumoTributoAliquotaAtual.getTributo()
						.getChavePrimaria()) {

					this.verificarPontoConsumoTributoAliquotaDuplicado(pontoConsumoTributoAliquota, pontoConsumoTributoAliquotaAtual);
				}
			}
		}
	}

	private void verificarPontoConsumoTributoAliquotaDuplicado(PontoConsumoTributoAliquota pontoConsumoTributoAliquota,
			PontoConsumoTributoAliquota pontoConsumoTributoAliquotaAtual) throws NegocioException {
		if (pontoConsumoTributoAliquota.getDataInicioVigencia().after(pontoConsumoTributoAliquotaAtual.getDataInicioVigencia())
				|| pontoConsumoTributoAliquota.getDataInicioVigencia().equals(pontoConsumoTributoAliquotaAtual.getDataInicioVigencia())) {
			if (pontoConsumoTributoAliquotaAtual.getDataFimVigencia() == null) {
				throw new NegocioException(ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_TRIBUTO_DUPLICADO, true);
			} else {
				if (pontoConsumoTributoAliquota.getDataInicioVigencia().before(pontoConsumoTributoAliquotaAtual.getDataFimVigencia())
						|| pontoConsumoTributoAliquota.getDataInicioVigencia()
								.equals(pontoConsumoTributoAliquotaAtual.getDataFimVigencia())) {
					throw new NegocioException(ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_TRIBUTO_DUPLICADO, true);
				}
			}
		} else {
			if (pontoConsumoTributoAliquota.getDataInicioVigencia().before(pontoConsumoTributoAliquotaAtual.getDataInicioVigencia())) {
				if (pontoConsumoTributoAliquota.getDataFimVigencia() == null) {
					throw new NegocioException(ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_TRIBUTO_DUPLICADO, true);
				} else {

					if (pontoConsumoTributoAliquotaAtual.getDataFimVigencia() != null) {
						if ((pontoConsumoTributoAliquota.getDataInicioVigencia()
								.after(pontoConsumoTributoAliquotaAtual.getDataFimVigencia())
								|| pontoConsumoTributoAliquota.getDataInicioVigencia()
										.equals(pontoConsumoTributoAliquotaAtual.getDataFimVigencia()))
								&& pontoConsumoTributoAliquotaAtual.getDataFimVigencia() != null) {
							throw new NegocioException(ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_TRIBUTO_DUPLICADO, true);
						} else {
							if ((pontoConsumoTributoAliquota.getDataFimVigencia()
									.after(pontoConsumoTributoAliquotaAtual.getDataInicioVigencia())
									|| pontoConsumoTributoAliquota.getDataFimVigencia()
											.equals(pontoConsumoTributoAliquotaAtual.getDataInicioVigencia()))
									&& pontoConsumoTributoAliquotaAtual.getDataFimVigencia() != null) {
								throw new NegocioException(ERRO_PONTO_CONSUMO_TRIBUTO_ALIQUOTA_TRIBUTO_DUPLICADO, true);
							}
						}
					}
				}
			}
		}
	}



	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorImovel
	 * #obterUltimoAnoMesFaturamento()
	 */
	@Override
	public Integer obterUltimoAnoMesFaturamento() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select max(grupoFaturamento.anoMesReferencia)");
		hql.append(FROM);
		hql.append(getClasseEntidadeGrupoFaturamento().getSimpleName());
		hql.append(" grupoFaturamento ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return (Integer) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#consultarContatoImovel(java.util.Map)
	 */
	@Override
	public Collection<ContatoImovel> consultarContatoImovel(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(ContatoImovel.class);

		if (filtro != null) {

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			Long idProfissao = (Long) filtro.get("idProfissao");
			if (idProfissao != null && idProfissao > 0) {
				criteria.add(Restrictions.eq("profissao.chavePrimaria", idProfissao));
			}

			Long idContatoTipo = (Long) filtro.get("idContatoTipo");
			if (idContatoTipo != null && idContatoTipo > 0) {
				criteria.add(Restrictions.eq("tipoContato.chavePrimaria", idContatoTipo));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#validarEnderecoRemoto(java.lang.String)
	 */
	@Override
	public void validarEnderecoRemoto(String enderecoRemotoPC) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ParametroSistema parametro = controladorParametroSistema.obterParametroPorCodigo("TIPO_ATRIBUTO_ID_PONTO_CONSUMO_SUPERVISORIOS");

		Criteria criteria = createCriteria(PontoConsumoImpl.class);
		if (parametro != null) {
			criteria = createCriteria(EntidadeConteudoImpl.class);
			criteria.setProjection(Projections.property(IMOVEL_DESCRICAO));
			criteria.add(Restrictions.eq(CHAVE_PRIMARIA, Long.valueOf(parametro.getValor())));
			criteria.setMaxResults(1);

			String descricao = (String) criteria.uniqueResult();
			if (descricao != null && "Numérico".equals(descricao)) {
				for (char c : enderecoRemotoPC.toCharArray()) {
					this.validarEnderecoRemoto(c);
				}
			}
		} else {
			throw new NegocioException("ERRO_NEGOCIO_PARAMETRO_SISTEMA_NAO_ENCONTRADO", "Tipo de Atributo de Endereço Remoto");
		}
	}

	private void validarEnderecoRemoto(char c) throws NegocioException{
		if (!Character.isDigit(c)) {
			throw new NegocioException(ERRO_NEGOCIO_ENDERECO_REMOTO_INVALIDO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#verificarDuplicidadeEnderecoRemoto(java.lang.Integer, java.lang.String,
	 * java.util.List)
	 */
	@Override
	public void verificarDuplicidadeEnderecoRemoto(Integer indexLista, String codigoPontoConsumoSupervisorio,
					List<PontoConsumo> listaPontoConsumo) throws NegocioException {

		ControladorMedidor controladorMedidor = (ControladorMedidor) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);

		int i = 0;
		for (PontoConsumo pc : listaPontoConsumo) {
			if (i != indexLista && pc.getCodigoPontoConsumoSupervisorio() != null
							&& pc.getCodigoPontoConsumoSupervisorio().equals(codigoPontoConsumoSupervisorio)
							&& !"".equals(codigoPontoConsumoSupervisorio)) {
				throw new NegocioException("ERRO_NEGOCIO_ENDERECO_REMOTO_DUPLICADO", true);
			}
			i = i + 1;
		}

		Criteria criteria = createCriteria(PontoConsumoImpl.class);
		criteria.createAlias(IMOVEL, IMOVEL);
		criteria.add(Restrictions.eq("codigoPontoConsumoSupervisorio", codigoPontoConsumoSupervisorio));
		criteria.setMaxResults(1);

		PontoConsumo pcLista = null;
		if (indexLista != null && indexLista >= 0) {
			pcLista = listaPontoConsumo.get(indexLista);
		}

		PontoConsumo pcBanco = (PontoConsumo) criteria.uniqueResult();
		if (pcBanco != null) {
			// só verifica se o resultado duplicado for igual ao item alterado se estiver alterando.
			// Na inclusao é desnecessario ja que esse item nao existe no banco
			if (pcLista != null && pcBanco.getChavePrimaria() == pcLista.getChavePrimaria()) {
				return;
			}
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ENDERECO_REMOTO_EXISTENTE, new Object[] {pcBanco.getImovel()
							.getChavePrimaria(), pcBanco.getDescricao()});
		}

		if(listaPontoConsumo.isEmpty()){
			controladorMedidor.validarEnderecoRemoto(codigoPontoConsumoSupervisorio, null, 0L);
		}
		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			controladorMedidor.validarEnderecoRemoto(codigoPontoConsumoSupervisorio, null, pontoConsumo.getChavePrimaria());
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#alterarSituacaoImoveisFactivel(java.util.Collection)
	 */
	@Override
	public void alterarSituacaoImoveisFactivel(Collection<Imovel> imoveis) {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		for (Imovel imovel : imoveis) {
			ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_IMOVEL_FACTIVEL);
			try {
				configSituacaoImovel(imovel, constanteSituacaoImovel);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
			}
		}

	}

	/**
	 * Config situacao imovel.
	 *
	 * @param imovel
	 *            the imovel
	 * @param constanteSituacaoImovel
	 *            the constante situacao imovel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void configSituacaoImovel(Imovel imovel, ConstanteSistema constanteSituacaoImovel) throws NegocioException {

		ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();
		SituacaoImovel situacaoImovel = controladorImovel.obterSituacaoImovel(Long.parseLong(constanteSituacaoImovel.getValor()));
		imovel.setSituacaoImovel(situacaoImovel);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#atualizarColecao(java.util.Collection, java.lang.Class)
	 */
	@Override
	public void atualizarColecao(Collection<Imovel> listaImoveis, Class<Imovel> classe) throws NegocioException {

		if (listaImoveis != null && !listaImoveis.isEmpty()) {
			for (Imovel imovel : listaImoveis) {
				try {
					this.atualizar(imovel);
				} catch (ConcorrenciaException e) {
					LOG.error(e.getMessage(), e);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#validarCamaposImovelEmLote(java.lang.String[], java.lang.String[],
	 * java.lang.String[])
	 */
	@Override
	public void validarCamaposImovelEmLote(String[] apartamento, String[] bloco, String[] quantidadeBanheiro) throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();

		if (apartamento != null && bloco != null && quantidadeBanheiro != null) {
			for (String registro : apartamento) {
				if (registro == null || registro.isEmpty()) {
					stringBuilder.append(Imovel.IMOVEL_APARTAMENTO);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
					break;
				}
			}

			if (stringBuilder.length() > 0) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, stringBuilder);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#listarImovelPorChaveRota(long)
	 */
	@Override
	public Collection<Imovel> listarImovelPorChaveRota(long chavePrimariaRota) {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String constanteValor = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_NAO_LIBERADO);

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" imovel ");
		hql.append(" inner join fetch imovel.rota rota");
		hql.append(" inner join fetch imovel.situacaoImovel situacaoImovel");
		hql.append(WHERE);
		hql.append(" rota.chavePrimaria = ?");
		hql.append(" and situacaoImovel.chavePrimaria = ?");
		hql.append(" order by imovel.nome asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, chavePrimariaRota);

		if (constanteValor != null && !constanteValor.isEmpty()) {
			query.setLong(1, Long.parseLong(constanteValor));
		}

		return query.list();
	}


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#listarPontoConsumoSemRota(java.util.Map, java.util.List)
	 */
	@Override
	@SuppressWarnings({"squid:S1192", "unchecked"})
	public Collection<PontoConsumo> listarPontoConsumoSemRota(Map<String, Object> filtro, List<Long> chavesPontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(IMOVEL_PONTO_CONSUMO);
		hql.append(" inner join fetch pontoConsumo.imovel imovel");
		hql.append(" left join fetch pontoConsumo.imovel.imovelCondominio imovelCondominio");
		hql.append(" left join fetch pontoConsumo.rota rota");
		hql.append(" inner join fetch pontoConsumo.segmento segmento");
		hql.append(" inner join fetch pontoConsumo.ramoAtividade ramoAtividade");
		hql.append(WHERE);
		hql.append(" rota.chavePrimaria is null");

		if (chavesPontoConsumo != null && !chavesPontoConsumo.isEmpty()) {
			hql.append(" and pontoConsumo.chavePrimaria not in (:chavesPontoConsumo)");
		}

		String descricao = (String) filtro.get("descricaoPontoConsumo");
		if (!StringUtils.isEmpty(descricao)) {
			hql.append(" and upper(pontoConsumo.descricao) like upper(:descricao)");
		}

		Long idSegmento = (Long) filtro.get("idSegmentoPontoConsumo");
		if (idSegmento != null) {
			hql.append(" and segmento.chavePrimaria = :idSegmento");
		}

		Long idRamoAtividade = (Long) filtro.get("idRamoAtividadePontoConsumo");
		if (idRamoAtividade != null) {
			hql.append(" and ramoAtividade.chavePrimaria = :idRamoAtividade");
		}
		String nomeImovel = (String) filtro.get("nome");
		if (!StringUtils.isEmpty(nomeImovel)) {
			hql.append(" and upper(imovel.nome) like upper(:nomeImovel)");
		}
		Long matriculaImovel = (Long) filtro.get("matricula");
		if (matriculaImovel != null) {
			hql.append(" and imovel.chavePrimaria = :matriculaImovel ");
		}
		String nomeImovelCondominio = (String) filtro.get("nomeImovelCondominio");
		if (!StringUtils.isEmpty(nomeImovelCondominio)) {
			hql.append("  and ( (upper(imovelCondominio.nome) like upper(:nomeImovelCondominio))"
					+ " or upper(imovel.nome)"
					+ " like upper(:nomeImovelCondominio))");
		}
		Long situacaoContrato = (Long) filtro.get(ImovelAction.SITUACAO_CONTRATO);
		Date dataInicioAssinatura = (Date) filtro.get(ImovelAction.DATA_INICIO_ASSINATURA_CONTRATO);
		Date dataFimAssinatura = (Date) filtro.get(ImovelAction.DATA_FIM_ASSINATURA_CONTRATO);
		if (dataInicioAssinatura != null || dataFimAssinatura != null || situacaoContrato != null) {
			hql.append(" and exists ( from  ");
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(" cpc inner join cpc.contrato c ");
			hql.append(" where cpc.pontoConsumo.chavePrimaria = pontoConsumo.chavePrimaria and c.habilitado = true ");
			if (dataInicioAssinatura != null) {
				hql.append(" and c.dataAssinatura >= :dataInicioAssinatura ");
			}
			if (dataFimAssinatura != null) {
				hql.append(" and c.dataAssinatura <= :dataFimAssinatura ");
			}
			if (situacaoContrato != null) {
				hql.append(" and c.situacao.chavePrimaria = :situacaoContrato");
			}
			hql.append(" ) ");
		}

		Long situacaoMedidor = (Long) filtro.get(ImovelAction.SITUACAO_MEDIDOR);
		if (situacaoMedidor != null) {
			hql.append(" and pontoConsumo.situacaoConsumo.chavePrimaria = :situacaoMedidor");
		}

		Date dataInicioAtivacao = (Date) filtro.get(ImovelAction.DATA_INICIO_ATIVACAO_MEDIDOR);
		Date dataFimAtivacao = (Date) filtro.get(ImovelAction.DATA_FIM_ATIVACAO_MEDIDOR);
		if (dataInicioAtivacao != null || dataFimAtivacao != null) {
			hql.append(" and exists ( from  ");
			hql.append(getClasseEntidadeMedidorInstalacao().getSimpleName());
			hql.append(" instalacao ");
			hql.append(" where instalacao.pontoConsumo.chavePrimaria = pontoConsumo.chavePrimaria ");
			if (dataInicioAtivacao != null) {
				hql.append(" and instalacao.data >= :dataInicioAtivacao ");
			}
			if (dataFimAtivacao != null) {
				hql.append(" and instalacao.data <= :dataFimAtivacao ");
			}
			hql.append(" ) ");
		}

		hql.append(" order by pontoConsumo.descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (chavesPontoConsumo != null && !chavesPontoConsumo.isEmpty()) {
			query.setParameterList("chavesPontoConsumo", chavesPontoConsumo);
		}

		if (!StringUtils.isEmpty(descricao)) {
			query.setParameter(IMOVEL_DESCRICAO, Util.formatarTextoConsulta(descricao));

		}

		if (idSegmento != null) {
			query.setParameter("idSegmento", idSegmento);
		}

		if (idRamoAtividade != null) {
			query.setParameter("idRamoAtividade", idRamoAtividade);
		}
		if (!StringUtils.isEmpty(nomeImovel)) {
			query.setParameter("nomeImovel", Util.formatarTextoConsulta(nomeImovel));
		}
		if (matriculaImovel != null) {
			query.setParameter("matriculaImovel", matriculaImovel);
		}
		if (!StringUtils.isEmpty(nomeImovelCondominio)) {
			query.setParameter("nomeImovelCondominio", Util.formatarTextoConsulta(nomeImovelCondominio));
		}
		if (situacaoContrato != null) {
			query.setParameter(SITUACAO_CONTRATO, situacaoContrato);
		}
		if (situacaoMedidor != null) {
			query.setParameter("situacaoMedidor", situacaoMedidor);
		}
		if (dataInicioAssinatura != null) {
			query.setParameter("dataInicioAssinatura", dataInicioAssinatura);
		}
		if (dataFimAssinatura != null) {
			query.setParameter("dataFimAssinatura", dataFimAssinatura);
		}
		if (dataInicioAtivacao != null) {
			query.setParameter("dataInicioAtivacao", dataInicioAtivacao);
		}
		if (dataFimAtivacao != null) {
			query.setParameter("dataFimAtivacao", dataFimAtivacao);
		}

		Collection<PontoConsumo> retorno = query.list();
		Collection<PontoConsumo> listaFinal = new ArrayList<PontoConsumo>();
		if (retorno != null && !retorno.isEmpty()) {
			for (PontoConsumo pontoConsumo : retorno) {
				listaFinal.add(pontoConsumo);
				if (pontoConsumo.getImovel().getCondominio()) {
					listaFinal.addAll(recuperaPontoConsumoImoveisFilhos(pontoConsumo.getImovel().getChavePrimaria(), IMOVEL_CHAVE_PRIMARIA));
					listaFinal.addAll(recuperaPontoConsumoImoveisFilhos(pontoConsumo.getImovel().getChavePrimaria(), "imovel.idImovelCondominio"));
				}else {
					Hibernate.initialize(pontoConsumo.getImovel().getImovelCondominio());
				}
			}

		}
		return retiraItensDuplicados(listaFinal);
	}

	@Override
	public Pair<List<ImovelDTO>, Long> listarImovelComPontosDeConsumoFilho(PesquisaPontoConsumo pesquisa) {
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct(imov_cd), imov_nm, count(*) over () as Count ");
		sql.append("from imovel where imov_cd in ( ");
		sql.append("select case i.IMOV_CD_CONDOMINIO when null then i.imov_cd else i.imov_cd_condominio end as id ");
		sql.append("from imovel i ");
		sql.append("inner join ponto_consumo pc on pc.imov_cd = i.imov_cd ");
		sql.append("where UPPER(pc.pocn_ds) like UPPER(:descricao) and pc.ROTA_CD in (:rotas) ");
		sql.append(") order by imov_nm");

		SQLQuery query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString());
		query.setMaxResults(pesquisa.getQtdRegistros());
		query.setFirstResult(pesquisa.getOffset());
		query.setParameter(IMOVEL_DESCRICAO, '%' + pesquisa.getDescricao() + '%');
		query.setParameterList("rotas", pesquisa.getRotas());
		List<Objects[]> results = query.list();

		List<ImovelDTO> imoveisDTO = new ArrayList<>();
		Long totalRegistros = 0L;
		for (Object[] row: results) {
			final ImovelDTO imovelDTO = new ImovelDTO();
			Long id = Long.valueOf(row[0].toString());
			imovelDTO.setChavePrimaria(id);
			imovelDTO.setDescricao(row[1].toString());
			imoveisDTO.add(imovelDTO);
			totalRegistros = Long.valueOf(row[CONSTANTE_NUMERO_DOIS].toString());
		}
		for (ImovelDTO imovel : imoveisDTO) {
			buscarPontoConsumoPorImovel(pesquisa, imovel);
		}
		return new Pair<>(imoveisDTO, totalRegistros);
	}

	/**
	 * Pesquisa todos os pontos de consumo filhos do imóvel informado que atendem ao critério de pesquisa
	 * @param pesquisa O filtro de pesquisa do ponto de consumo
	 * @param imovel O imóvel pai para referencia do ponto de consumo
	 */
	public void buscarPontoConsumoPorImovel(PesquisaPontoConsumo pesquisa, ImovelDTO imovel) {
		Criteria criteriaPontoFilho = createCriteria(getClasseEntidadePontoConsumo());
		criteriaPontoFilho.createAlias(IMOVEL, "i");
		criteriaPontoFilho.add(Restrictions.like(PontoConsumo.ATRIBUTO_DESCRICAO, "%"+pesquisa.getDescricao()+"%").ignoreCase());
		criteriaPontoFilho.add(Restrictions.in("rota.chavePrimaria", pesquisa.getRotas()));
		SimpleExpression condicao1 = Restrictions.eq("i.chavePrimaria", imovel.getChavePrimaria());
		SimpleExpression condicao2 = Restrictions.eq("i.idImovelCondominio",imovel.getChavePrimaria());
		criteriaPontoFilho.add(Restrictions.or(condicao1, condicao2));
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("chavePrimaria"), "chavePrimaria");
		projList.add(Projections.property(IMOVEL_DESCRICAO), IMOVEL_DESCRICAO);
		projList.add(Projections.property("i.nome"), "imovel");
		criteriaPontoFilho.setProjection(projList);
		criteriaPontoFilho.addOrder(Order.asc(IMOVEL_DESCRICAO));
		criteriaPontoFilho.setResultTransformer(Transformers.aliasToBean(PontoConsumoDTO.class));
		List<PontoConsumoDTO> pontosFilhos = criteriaPontoFilho.list();
		imovel.setPontosConsumo(pontosFilhos);
	}

	@SuppressWarnings("squid:S1192")
	@Override
	public Pair<List<ImovelDTO>, Long> listarImovelRotaComPontosDeConsumoFilho(PesquisaPontoConsumo pesquisa) {
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		HashMap<String, Object> params = new HashMap<>();
		ArrayList<String> whereStm = new ArrayList<>();
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct(imov_cd), imov_nm, count(*) over () as Count ");
		sql.append("from imovel where imov_cd in ( ");
		sql.append("select case when i.IMOV_CD_CONDOMINIO is null then i.imov_cd else i.imov_cd_condominio end as id ");
		sql.append("from imovel i ");
		sql.append("inner join PONTO_CONSUMO pc on pc.imov_cd = i.imov_cd and pc.ROTA_CD is null ");
		sql.append("left join PONTO_CONSUMO_SITUACAO pcs on pc.POCS_CD = pcs.POCS_CD ");
		sql.append("left join CONTRATO_PONTO_CONSUMO cpc on cpc.POCN_CD = pc.POCN_CD ");
		sql.append("left join CONTRATO c on c.CONT_CD = cpc.CONT_CD and c.CONT_IN_USO = 1 ");
		sql.append("left join MEDIDOR_INSTALACAO mi on mi.POCN_CD = pc.POCN_CD ");

		if (pesquisa.getSituacaoContrato() != null) {
			if (pesquisa.getSituacaoContrato() == 1) {
				long valorSituacaoContratoAtivo =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO));
				pesquisa.setSituacaoContrato(valorSituacaoContratoAtivo);
				whereStm.add(" c.COSI_CD in (:situacaoContrato) ");
				params.put(SITUACAO_CONTRATO, pesquisa.getSituacaoContrato());
			}
			if (pesquisa.getSituacaoContrato() == 0) {
				long valorSituacaoContratoEncerrado =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ENCERRADO));
				pesquisa.setSituacaoContrato(valorSituacaoContratoEncerrado);
				whereStm.add(" c.COSI_CD in (:situacaoContrato) ");
				params.put(SITUACAO_CONTRATO, pesquisa.getSituacaoContrato());
			}
		}
		if (pesquisa.getSituacaoMedidor() != null) {
			if (pesquisa.getSituacaoMedidor() == 1) {
				long situacaoConsumoAtivo = Long.parseLong(
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_ATIVO));
				pesquisa.setSituacaoMedidor(situacaoConsumoAtivo);
				whereStm.add(" pc.POCS_CD in (:situacaoConsumo) ");
				params.put("situacaoConsumo", pesquisa.getSituacaoMedidor());
			}
			if (pesquisa.getSituacaoMedidor() == 0) {
				long situacaoConsumoAguardandoAtivacao = Long.parseLong(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO));
				pesquisa.setSituacaoMedidor(situacaoConsumoAguardandoAtivacao);
				whereStm.add(" pc.POCS_CD in (:situacaoConsumo) ");
				params.put("situacaoConsumo", pesquisa.getSituacaoMedidor());
			}
		}
		if (pesquisa.getDataInicioAssinaturaContrato() != null) {
			whereStm.add(" c.CONT_DT_ASSINATURA >= (:dataInicioAssinaturaContrato) ");
			params.put("dataInicioAssinaturaContrato", DataUtil.toDate(pesquisa.getDataInicioAssinaturaContrato()));
		}
		if (pesquisa.getDataFimAssinaturaContrato() != null) {
			whereStm.add(" c.CONT_DT_ASSINATURA <= (:dataFimAssinaturaContrato) ");
			params.put("dataFimAssinaturaContrato", DataUtil.toDate(pesquisa.getDataFimAssinaturaContrato()));
		}
		if (pesquisa.getDataInicioAtivacaoMedidor() != null) {
			whereStm.add(" mi.MEIN_DT >= (:dataInicioAtivacaoMedidor) ");
			params.put("dataInicioAtivacaoMedidor", DataUtil.toDate(pesquisa.getDataInicioAtivacaoMedidor()));
		}
		if (pesquisa.getDataFimAtivacaoMedidor() != null) {
			whereStm.add(" mi.MEIN_DT <= (:dataFimAtivacaoMedidor) ");
			params.put("dataFimAtivacaoMedidor", DataUtil.toDate(pesquisa.getDataFimAtivacaoMedidor()));
		}

		if (!whereStm.isEmpty()) {
			sql.append(" where ").append(String.join(" and ", whereStm));
		}
		sql.append(") order by imov_nm asc");

		SQLQuery query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString());
		query.setMaxResults(pesquisa.getQtdRegistros());
		query.setFirstResult(pesquisa.getOffset());
		for (Map.Entry<String, Object> entrada : params.entrySet()) {
			String chave = entrada.getKey();
			Object info = entrada.getValue();
			if (info instanceof Collection) {
				query.setParameterList(chave, (List) info);
			} else {
				query.setParameter(chave, info);
			}
		}
		List<Objects[]> results = query.list();

		List<ImovelDTO> imoveisDTO = new ArrayList<>();
		Long totalRegistros = 0L;
		for (Object[] row: results) {
			final ImovelDTO imovelDTO = new ImovelDTO();
			Long id = Long.valueOf(row[0].toString());
			imovelDTO.setChavePrimaria(id);
			imovelDTO.setDescricao(row[1].toString());
			imoveisDTO.add(imovelDTO);
			totalRegistros = Long.valueOf(row[CONSTANTE_NUMERO_DOIS].toString());
		}
		for (ImovelDTO imovel : imoveisDTO) {
			List<PontoConsumoDTO> pontos = buscarPontoConsumoRotaPorImovel(pesquisa, imovel);
			imovel.setPontosConsumo(pontos);
		}
		return new Pair<>(imoveisDTO, totalRegistros);
	}

	/**
	 * Pesquisa todos os pontos de consumo filhos do imóvel informado que atendem ao critério de pesquisa
	 * @param pesquisa O filtro de pesquisa do ponto de consumo
	 * @param imovel O imóvel pai para referencia do ponto de consumo
	 * @return A lista dos pontos de consumo filhos do imóvel e que atendem aos filtros
	 */
	@SuppressWarnings("squid:S1192")
	public List<PontoConsumoDTO> buscarPontoConsumoRotaPorImovel(PesquisaPontoConsumo pesquisa, ImovelDTO imovel) {
		SimpleExpression condicao1 = Restrictions.eq("i.chavePrimaria", imovel.getChavePrimaria());
		SimpleExpression condicao2 = Restrictions.eq("i.idImovelCondominio",imovel.getChavePrimaria());
		Criteria criteriaPontoFilho = createCriteria(getClasseEntidadePontoConsumo(), "p")
				.createAlias("p.imovel", "i", Criteria.INNER_JOIN)
				.createAlias("p.segmento", "s", Criteria.LEFT_JOIN)
				.createAlias("p.ramoAtividade", "ramo", Criteria.LEFT_JOIN)
				.createAlias("p.situacaoConsumo", "psc", Criteria.LEFT_JOIN);

		criteriaPontoFilho.add(Restrictions.or(condicao1, condicao2));
		criteriaPontoFilho.add(Restrictions.isNull("p.rota.chavePrimaria"));

		if (pesquisa.getDataInicioAssinaturaContrato() != null || pesquisa.getDataFimAssinaturaContrato() != null
				|| (pesquisa.getSituacaoContrato() != null && pesquisa.getSituacaoContrato() >= 0)) {
			DetachedCriteria subCriteria = DetachedCriteria.forClass(getClasseEntidadeContratoPontoConsumo(),"cpc")
					.createAlias("contrato", "c", Criteria.INNER_JOIN, Restrictions.eq("c.habilitado", true))
					.add(Property.forName("cpc.pontoConsumo.chavePrimaria").eqProperty("p.chavePrimaria"));
			if (pesquisa.getDataInicioAssinaturaContrato() != null) {
				subCriteria.add(Restrictions.ge("c.dataAssinatura", DataUtil.toDate(pesquisa.getDataInicioAssinaturaContrato())));
			}
			if (pesquisa.getDataFimAssinaturaContrato() != null) {
				subCriteria.add(Restrictions.le("c.dataAssinatura", DataUtil.toDate(pesquisa.getDataFimAssinaturaContrato())));
			}
			if (pesquisa.getSituacaoContrato() != null && pesquisa.getSituacaoContrato() >= 0) {
				subCriteria.add(Restrictions.eq("c.situacao.chavePrimaria", pesquisa.getSituacaoContrato()));
			}
			criteriaPontoFilho.add(Subqueries.exists(subCriteria.setProjection(Projections.property("cpc.pontoConsumo.chavePrimaria"))));
		}
		if (pesquisa.getDataInicioAtivacaoMedidor() != null || pesquisa.getDataFimAtivacaoMedidor() != null) {
			DetachedCriteria subCriteria = DetachedCriteria.forClass(getClasseEntidadeMedidorInstalacao(),"mi")
					.add(Property.forName("mi.pontoConsumo.chavePrimaria").eqProperty("p.chavePrimaria"));
			if (pesquisa.getDataInicioAtivacaoMedidor() != null) {
				subCriteria.add(Restrictions.ge("mi.data", DataUtil.toDate(pesquisa.getDataInicioAtivacaoMedidor())));
			}
			if (pesquisa.getDataFimAtivacaoMedidor() != null) {
				subCriteria.add(Restrictions.le("mi.data", DataUtil.toDate(pesquisa.getDataInicioAtivacaoMedidor())));
			}
			criteriaPontoFilho.add(Subqueries.exists(subCriteria.setProjection(Projections.property("mi.pontoConsumo.chavePrimaria"))));
		}
		if (pesquisa.getSituacaoMedidor() != null && pesquisa.getSituacaoMedidor() >= 0) {
			criteriaPontoFilho.add(Restrictions.eq("p.situacaoConsumo.chavePrimaria", pesquisa.getSituacaoMedidor()));
		}
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.property("p.chavePrimaria"), "chavePrimaria");
		projList.add(Projections.property("p.descricao"), IMOVEL_DESCRICAO);
		projList.add(Projections.property("i.nome"), "imovel");
		projList.add(Projections.property("s.descricao"), "segmento");
		projList.add(Projections.property("ramo.descricao"), "ramoAtividade");
		projList.add(Projections.property("psc.descricao"), "situacaoConsumo");
		criteriaPontoFilho.setProjection(projList);
		criteriaPontoFilho.addOrder(Order.asc("p.descricao"));
		criteriaPontoFilho.setResultTransformer(Transformers.aliasToBean(PontoConsumoDTO.class));

		return criteriaPontoFilho.list();
	}


	/**
	 * Retira os Pontos de consumo duplicados
	 * @param retorno Lista com os pontos de consumo que podem estar duplicados
	 * @return Lista unificada
	 */
	private Collection<PontoConsumo> retiraItensDuplicados(Collection<PontoConsumo> retorno) {
		return new ArrayList<PontoConsumo>(new HashSet<PontoConsumo>(retorno));
	}

	/**
	 * Metodo para prencher os imoveis filhos da pesquisa de ponto de consumo dos imoveis
	 * @param chavePrimaria Identidicador do imovel
	 * @param campoFiltro Campo Fultro para a busvca
	 * @return Lista com os pontos de consumo dos imoveis filhos
	 */
	@SuppressWarnings("squid:S1192")
	private List<PontoConsumo> recuperaPontoConsumoImoveisFilhos(long chavePrimaria, String campoFiltro) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(IMOVEL_PONTO_CONSUMO);
		hql.append(" inner join fetch pontoConsumo.imovel imovel");
		hql.append(" left join fetch pontoConsumo.rota rota");
		hql.append(" inner join fetch pontoConsumo.segmento segmento");
		hql.append(" inner join fetch pontoConsumo.ramoAtividade ramoAtividade");
		hql.append(WHERE);
		hql.append(" rota.chavePrimaria is null");
		hql.append(" and ");
		hql.append(campoFiltro);
		hql.append(" = :chavePrimaria ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(CHAVE_PRIMARIA, chavePrimaria);
		return query.list();
	}
	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#atualizarLogradouroImovel(java.util.Collection)
	 */
	@Override
	public void atualizarLogradouroImovel(Collection<Imovel> listaImovel, DadosAuditoria dadosAuditoria) throws GGASException {

		Collection<Imovel> listaImovelPai = new ArrayList<>();
		for (Imovel imovel : listaImovel) {
			imovel.setDadosAuditoria(dadosAuditoria);
			boolean isImovelPai = imovel.getCondominio();
			if (isImovelPai) {
				listaImovelPai.add(imovel);
			} else {
				this.atualizar(imovel);
			}
		}

		this.atualizarColecao(listaImovelPai, Imovel.class);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#alterarSituacaoImovelPorIndicadorRede(java.util.Collection,
	 * br.com.ggas.cadastro.localidade.QuadraFace)
	 */
	@Override
	public void alterarSituacaoImovelPorIndicadorRede(Collection<Imovel> listaImovel, QuadraFace novaQuadraFace) throws NegocioException {

		int novoIndicadorRede = novaQuadraFace.getRedeIndicador().getCodigo();

		for (Imovel imovel : listaImovel) {

			SituacaoImovel situacaoImovel = imovel.getSituacaoImovel();
			long chaveSituacaoImovel = situacaoImovel.getChavePrimaria();
			QuadraFace quadraFace = imovel.getQuadraFace();
			RedeIndicador antigoRedeIndicador = quadraFace.getRedeIndicador();
			int antigoIndicadorRede = antigoRedeIndicador.getCodigo();

			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			if (antigoIndicadorRede == RedeIndicador.NAO_TEM && novoIndicadorRede == RedeIndicador.TEM) {
				ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema
								.obterConstantePorCodigo(Constantes.C_IMOVEL_FACTIVEL);
				this.configSituacaoImovel(imovel, constanteSituacaoImovel);
			} else if (antigoIndicadorRede == RedeIndicador.TEM_PARCIALMENTE && novoIndicadorRede == RedeIndicador.NAO_TEM) {
				String valorConstanteInterligado = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_INTERLIGADO);
				long codigoInterligado = Long.parseLong(valorConstanteInterligado);

				if (chaveSituacaoImovel == codigoInterligado) {
					throw new NegocioException("Existe Imóvel que não pode ser alterado, pois possui situação de Interligado");
				} else {
					ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema
									.obterConstantePorCodigo(Constantes.C_IMOVEL_POTENCIAL);
					this.configSituacaoImovel(imovel, constanteSituacaoImovel);
				}

			} else if (antigoIndicadorRede == RedeIndicador.TEM_PARCIALMENTE && novoIndicadorRede == RedeIndicador.TEM) {
				if (chaveSituacaoImovel == SituacaoImovel.SITUACAO_POTENCIAL) {
					ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema
									.obterConstantePorCodigo(Constantes.C_IMOVEL_FACTIVEL);
					this.configSituacaoImovel(imovel, constanteSituacaoImovel);
				}
			} else if (antigoIndicadorRede == RedeIndicador.TEM && novoIndicadorRede == RedeIndicador.NAO_TEM) {
				String valorConstanteInterligado = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_INTERLIGADO);
				long codigoInterligado = Long.parseLong(valorConstanteInterligado);

				if (chaveSituacaoImovel == codigoInterligado) {
					throw new NegocioException("Existe Imóvel que não pode ser alterado, pois possui situação de Interligado");
				} else {
					ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema
									.obterConstantePorCodigo(Constantes.C_IMOVEL_POTENCIAL);
					this.configSituacaoImovel(imovel, constanteSituacaoImovel);
				}
			} else if (antigoIndicadorRede == RedeIndicador.NAO_TEM && novoIndicadorRede == RedeIndicador.TEM_PARCIALMENTE) {
				ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema
								.obterConstantePorCodigo(Constantes.C_IMOVEL_FACTIVEL);
				this.configSituacaoImovel(imovel, constanteSituacaoImovel);
			} else if (antigoIndicadorRede == RedeIndicador.TEM && novoIndicadorRede == RedeIndicador.TEM_PARCIALMENTE) {
				ConstanteSistema constanteSituacaoImovel = controladorConstanteSistema
								.obterConstantePorCodigo(Constantes.C_IMOVEL_POTENCIAL);
				this.configSituacaoImovel(imovel, constanteSituacaoImovel);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#listarNomeImoveis(java.lang.Long[])
	 */
	@Override
	public Collection<String> listarNomeImoveis(Long[] chavesPrimarias) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct nome ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" chavePrimaria in (:chavesPrimarias) ");
		hql.append(" order by nome asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList(EntidadeConteudo.ATRIBUTO_CHAVES_PRIMARIAS, chavesPrimarias);

		Collection<String> nomes = query.list();

		Collection<String> blocos = new ArrayList<>();
		for (String nome : nomes) {

			String[] arrayNome = nome.split(",");
			if (arrayNome.length == TAMANHO_IDEAL_NOMES && !blocos.contains(arrayNome[1])) {
				blocos.add(arrayNome[1]);
			}

		}

		return blocos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#listarImoveisEmProspeccao(java.util.Map, java.lang.Long)
	 */
	@Override
	public Collection<Imovel> listarImoveisEmProspeccao(Map<String, Object> filtro, Long idFuncionario) throws NegocioException {

		Criteria criteria = createCriteria(Imovel.class);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String emProspeccao = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_IMOVEL_EM_PROSPECCAO);

		if (idFuncionario != null && idFuncionario > 0) {
			criteria.createAlias("agente", "agente");
			criteria.add(Restrictions.eq("agente.funcionario.chavePrimaria", idFuncionario));
			criteria.add(Restrictions.eq("situacaoImovel.chavePrimaria", Long.valueOf(emProspeccao)));
		}

		if (filtro != null) {
			String numeroImovel = (String) filtro.get(NUMERO_IMOVEL);
			if (!StringUtils.isEmpty(numeroImovel)) {
				criteria.add(Restrictions.eq(NUMERO_IMOVEL, numeroImovel));
			}

			String nome = (String) filtro.get("nome");
			if (!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.ilike("nome", Util.formatarTextoConsulta(nome)));
			}

			String complementoImovel = (String) filtro.get("complementoImovel");
			if (!StringUtils.isEmpty(complementoImovel)) {
				criteria.add(Restrictions.ilike(DESCRICAO_COMPLEMENTO, Util.formatarTextoConsulta(complementoImovel)));
			}

			String indicadorCondominioAmbos = (String) filtro.get("indicadorCondominioAmbos");
			if (indicadorCondominioAmbos != null) {
				if (AMBOS.equalsIgnoreCase(indicadorCondominioAmbos)) {
					Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);

					this.adicionarImovelCondominio(chavePrimaria, criteria);

				} else if ("true".equalsIgnoreCase(indicadorCondominioAmbos)) {
					Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
					this.adicionarRestricaoChavePrimaria(chavePrimaria, criteria);
					criteria.add(Restrictions.eq(CONDOMINIO, Boolean.TRUE));
				} else if (FALSE.equalsIgnoreCase(indicadorCondominioAmbos)) {
					Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
					if (chavePrimaria != null && chavePrimaria > 0) {
						criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
					}
					criteria.add(Restrictions.eq(CONDOMINIO, Boolean.FALSE));
				}
			} else {
				Boolean indicadorCondominio = (Boolean) filtro.get("indicadorCondominio");
				if (indicadorCondominio != null) {
					criteria.add(Restrictions.eq(CONDOMINIO, indicadorCondominio));
				}

				Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
				this.adicionarRestricaoChavePrimaria(chavePrimaria, criteria);

				Long matriculaCondominio = (Long) filtro.get("matriculaCondominio");
				if ((matriculaCondominio != null) && (matriculaCondominio > 0)) {
					criteria.createAlias(IMOVEL_CONDOMINIO, IMOVEL_CONDOMINIO);
					criteria.add(Restrictions.eq("imovelCondominio.chavePrimaria", matriculaCondominio));
				} else {
					criteria.setFetchMode(IMOVEL_CONDOMINIO, FetchMode.JOIN);
				}
			}

			String habilitado = (String) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (!"todos".equals(habilitado) && habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.valueOf(habilitado)));
			}

			String cep = (String) filtro.get("cepImovel");
			if (!StringUtils.isEmpty(cep)) {
				criteria.createCriteria(QUADRA_FACE).createCriteria(ENDERECO).createCriteria(CEP).add(Restrictions.eq(CEP, cep));
			} else {
				criteria.setFetchMode(QUADRA_FACE, FetchMode.JOIN);
			}

			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));

			criteria.addOrder(Order.asc("nome"));
		}

		return criteria.list();
	}

	private void adicionarImovelCondominio(Long chavePrimaria, Criteria criteria) {
		if ((chavePrimaria != null) && (chavePrimaria > 0)) {
			criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
		} else {
			criteria.setFetchMode(IMOVEL_CONDOMINIO, FetchMode.JOIN);
		}
	}

	private void adicionarRestricaoChavePrimaria(Long chavePrimaria, Criteria criteria){
		if (chavePrimaria != null && chavePrimaria > 0) {
			criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
		}
	}



	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#obterSituacaoImovelPorDescricao(java.lang.String)
	 */
	@Override
	public SituacaoImovel obterSituacaoImovelPorDescricao(String descricao) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeSituacaoImovel().getSimpleName());
		hql.append(" situacaoImovel ");
		hql.append(WHERE);
		hql.append(" upper(descricao) = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, descricao);

		return (SituacaoImovel) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#obterRedeInterDoImovel(java.lang.Long)
	 */
	@Override
	public RedeInternaImovel obterRedeInterDoImovel(Long chavePrimaria) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeRedeInternaImovel().getSimpleName());
		hql.append(" redeInterna ");
		hql.append(WHERE);
		hql.append(" imovel.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimaria);

		return (RedeInternaImovel) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#converterImovelTOEmImovel(br.com.ggas.webservice.ImovelTO)
	 */
	@Override
	public Imovel converterImovelTOEmImovel(ImovelTO imovelTO) throws GGASException {
		Imovel imovel = null;
		if (imovelTO.getChavePrimaria() != null && imovelTO.getChavePrimaria() > 0) {
			imovel = fachada.buscarImovelPorChave(imovelTO.getChavePrimaria(), "redeInternaImovel", "listaClienteImovel",
					"unidadesConsumidoras", "contatos", Imovel.LISTA_PONTO_CONSUMO, QUADRA_FACE, "quadraFace.endereco",
							"quadraFace.endereco.cep");
		} else {
			imovel = fachada.criarImovel();
		}

		if (imovelTO.getChavePrimaria() != null && imovelTO.getChavePrimaria() > 0) {
			imovel.setChavePrimaria(imovelTO.getChavePrimaria());
		}

		if (imovelTO.getQuadra() != null) {
			Map<String, Object> filtro = new HashMap<>();
			filtro.put(CHAVE_PRIMARIA, imovelTO.getQuadra().getChavePrimaria());
			Collection<Quadra> quadras = fachada.consultarQuadras(filtro);
			if (!CollectionUtils.isEmpty(quadras)) {
				Quadra quadra = quadras.iterator().next();
				imovel.setQuadra(quadra);
			}
		}

		if (imovelTO.getQuadraFace() != null) {
			QuadraFace face = fachada.buscarQuadraFacePorChave(imovelTO.getQuadraFace().getChavePrimaria());
			if (face != null) {
				imovel.setQuadraFace(face);
			}
		}

		imovel.setEnderecoReferencia(imovelTO.getEnderecoReferencia());

		if (imovelTO.getAreaConstruidaFaixa() != null) {
			AreaConstruidaFaixa area = fachada.buscarAreaConstruidaFaixaPorChave(imovelTO.getAreaConstruidaFaixa().getChavePrimaria());
			imovel.setAreaConstruidaFaixa(area);
		}

		if (imovelTO.getPavimentoCalcada() != null) {
			PavimentoCalcada calcada = fachada.buscarPavimentoCalcadaPorChave(imovelTO.getPavimentoCalcada().getChavePrimaria());
			imovel.setPavimentoCalcada(calcada);
		}

		if (imovelTO.getPavimentoRua() != null) {
			PavimentoRua rua = fachada.buscarPavimentoRuaPorChave(imovelTO.getPavimentoRua().getChavePrimaria());
			imovel.setPavimentoRua(rua);
		}

		if (imovelTO.getSituacaoImovel() != null) {
			SituacaoImovel situacao = fachada.buscarSituacaoImovelPorChave(imovelTO.getSituacaoImovel().getChavePrimaria());
			imovel.setSituacaoImovel(situacao);

		}

		if (imovelTO.getPerfilImovel() != null) {
			PerfilImovel perfil = fachada.buscarPerfilImovelPorChave(imovelTO.getPerfilImovel().getChavePrimaria());
			imovel.setPerfilImovel(perfil);
		}

		if (imovelTO.getPadraoConstrucao() != null) {
			PadraoConstrucao padrao = fachada.buscarPadraoConstrucaoPorChave(imovelTO.getPadraoConstrucao().getChavePrimaria());
			imovel.setPadraoConstrucao(padrao);
		}

		if (imovelTO.getTipoBotijao() != null) {
			TipoBotijao botijao = fachada.buscarTipoBotijaoPorChave(imovelTO.getTipoBotijao().getChavePrimaria());
			imovel.setTipoBotijao(botijao);
		}

		if (imovelTO.getModalidadeMedicaoImovel() != null) {
			ModalidadeMedicaoImovel modalidade = fachada.buscarModalidadeMedicaoImovelPorCodigo(imovelTO.getModalidadeMedicaoImovel()
							.getChavePrimaria().intValue());
			imovel.setModalidadeMedicaoImovel(modalidade);
		}

		imovel.setNumeroLote(imovelTO.getNumeroLote());

		if (imovelTO.getNumeroSublote() != null && imovelTO.getNumeroSublote() > 0) {
			imovel.setNumeroSublote(imovelTO.getNumeroSublote());
		}

		Collection<ImovelRamoAtividade> unidades = null;
		if (!CollectionUtils.isEmpty(imovelTO.getUnidadesConsumidoras())) {
			unidades = new HashSet<>();
			for (ImovelRamoAtividadeTO ramoTO : imovelTO.getUnidadesConsumidoras()) {
				Map<String, Object> filtro = new HashMap<>();

				ImovelRamoAtividade ramo = null;
				if (ramoTO.getChavePrimaria() != null && ramoTO.getChavePrimaria() > 0) {
					filtro.put(CHAVE_PRIMARIA, ramoTO.getChavePrimaria());
					ramo = (ImovelRamoAtividade) obter(ramoTO.getChavePrimaria(), getClasseEntidadeImovelRamoAtividade());
				} else {
					ramo = (ImovelRamoAtividade) criarImovelRamoAtividade();
				}

				if (ramoTO.getRamoAtividadeTO() != null) {
					ramo.setRamoAtividade(fachada.buscarRamoAtividadeChave(ramoTO.getRamoAtividadeTO().getChavePrimaria()));
				}

				if (!StringUtils.isEmpty(ramoTO.getQuantidadeUnidadesConsumidoras())) {
					ramo.setQuantidadeEconomia(Integer.valueOf(ramoTO.getQuantidadeUnidadesConsumidoras()));
				}

				ramo.setImovel(imovel);
				ramo.setHabilitado(Boolean.TRUE);
				ramo.setUltimaAlteracao(new Date());
				unidades.add(ramo);
			}
		} else {
			imovel.getUnidadesConsumidoras().clear();
		}

		converterImovelTOEmImovelPasso2(imovelTO, imovel, unidades);

		return imovel;
	}

	/**
	 * Converter imovel to em imovel passo2.
	 *
	 * @param imovelTO
	 *            the imovel to
	 * @param imovel
	 *            the imovel
	 * @param unidades
	 *            the unidades
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void converterImovelTOEmImovelPasso2(ImovelTO imovelTO, Imovel imovel, Collection<ImovelRamoAtividade> unidades)
					throws GGASException {

		imovel.setRedePreexistente(imovelTO.getRedePreexistente());

		imovel.setNumeroTestada(imovelTO.getNumeroTestada());

		imovel.setQuantidadeUnidadeConsumidora(imovelTO.getQuantidadeUnidadeConsumidora());

		imovel.setQuantidadePontoConsumo(imovelTO.getQuantidadePontoConsumo());

		imovel.setNumeroSequenciaLeitura(imovelTO.getNumeroSequenciaLeitura());

		imovel.setQuantidadeBloco(imovelTO.getQuantidadeBloco());

		imovel.setQuantidadeAndar(imovelTO.getQuantidadeAndar());

		imovel.setQuantidadeBanheiro(imovelTO.getQuantidadeBanheiro());

		imovel.setQuantidadeApartamentoAndar(imovelTO.getQuantidadeApartamentoAndar());

		imovel.setNumeroImovel(imovelTO.getNumeroImovel());

		imovel.setDescricaoComplemento(imovelTO.getDescricaoComplemento());

		imovel.setNome(imovelTO.getNome());

		imovel.setCep(imovelTO.getCep());

		if (!StringUtils.isEmpty(imovelTO.getDataEntrega())) {
			imovel.setDataEntrega(Util.converterCampoStringParaData("Previsão Entrega", imovelTO.getDataEntrega(),
							Constantes.FORMATO_DATA_BR));
		}

		if (!StringUtils.isEmpty(imovelTO.getDataPrevisaoEncerramentoObra())) {
			imovel.setDataPrevisaoEncerramentoObra(Util.converterCampoStringParaData("Previsão Entrega",
							imovelTO.getDataPrevisaoEncerramentoObra(), Constantes.FORMATO_DATA_BR));
		}

		imovel.setIndicadorObraTubulacao(imovelTO.getIndicadorObraTubulacao());

		imovel.setCondominio(imovelTO.getCondominio());

		imovel.setValvulaBloqueio(imovelTO.getValvulaBloqueio());

		if (!CollectionUtils.isEmpty(imovelTO.getContatos())) {
			Collection<ContatoImovel> contatosImovel = new HashSet<>();
			for (ContatoImovelTO contatoTO : imovelTO.getContatos()) {

				ContatoImovel contato = null;

				if (contatoTO.getChavePrimaria() != null && contatoTO.getChavePrimaria() > 0) {
					Map<String, Object> filtro = new HashMap<>();
					filtro.put(CHAVE_PRIMARIA, contatoTO.getChavePrimaria());
					contato = (ContatoImovel) obter(contatoTO.getChavePrimaria(), getClasseEntidadeContatoImovel());
				} else {
					contato = fachada.criarContatoImovel();
				}

				contato.setNome(contatoTO.getNome());
				contato.setCodigoDDD(contatoTO.getDDD());
				contato.setFone(contatoTO.getFone());
				contato.setEmail(contatoTO.getEmail());
				contato.setPrincipal(contatoTO.isPrincipal());
				contato.setDescricaoArea(contatoTO.getArea());
				contato.setRamal(contatoTO.getRamal());
				if (contatoTO.getTipoContato() != null) {
					TipoContato tipoContato = Fachada.getInstancia().buscarTipoContato(contatoTO.getTipoContato().getChavePrimaria());
					contato.setTipoContato(tipoContato);
				}

				if (contatoTO.getProfissao() != null) {
					Profissao profissao = Fachada.getInstancia().obterProfissao(contatoTO.getProfissao().getChavePrimaria());
					contato.setProfissao(profissao);
				}

				contato.setImovel(imovel);
				contato.setUltimaAlteracao(new Date());
				contato.setHabilitado(true);
				contatosImovel.add(contato);
			}
			imovel.getContatos().clear();
			imovel.getContatos().addAll(contatosImovel);
		} else {
			imovel.getContatos().clear();
		}

		if (imovelTO.getRedePreexistente()) {

			if (imovelTO.getRedeInternaImovelTO() != null) {

				RedeInternaImovel redeInternaImovel = null;
				if (imovelTO.getChavePrimaria() != null && imovelTO.getChavePrimaria() > 0) {
					redeInternaImovel = obterRedeInterDoImovel(imovelTO.getChavePrimaria());
				}

				redeInternaImovel = converterRedeInternaImovelTO(imovelTO.getRedeInternaImovelTO(), redeInternaImovel);
				imovel.setRedeInternaImovel(redeInternaImovel);
				redeInternaImovel.setImovel(imovel);
			} else {
				imovel.setRedeInternaImovel(null);
			}
		} else {
			imovel.setRedeInternaImovel(null);
		}

		if (imovelTO.getRota() != null) {
			Rota rota = fachada.buscarRota(imovelTO.getRota().getChavePrimaria());
			imovel.setRota(rota);
		}

		if (imovelTO.getAgente() != null) {
			ControladorAgente controladorAgente = (ControladorAgente) ServiceLocator.getInstancia().getBeanPorID("controladorAgenteImpl");
			Agente agente = controladorAgente.obterAgente(imovelTO.getAgente().getChavePrimaria());
			imovel.setAgente(agente);
		}

		if (unidades != null) {
			imovel.getUnidadesConsumidoras().clear();
			imovel.getUnidadesConsumidoras().addAll(unidades);
		}

		imovel.setUltimaAlteracao(new Date());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#converterImovelEmImovelTO(br.com.ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public ImovelTO converterImovelEmImovelTO(Imovel imovel) throws GGASException {

		ImovelTO imovelTO = new ImovelTO();

		imovelTO.setChavePrimaria(imovel.getChavePrimaria());

		imovelTO.setHabilitado(imovel.isHabilitado());

		verificarSetarCamposImovelTO(imovel, imovelTO);

		if (!CollectionUtils.isEmpty(imovel.getUnidadesConsumidoras())) {
			List<ImovelRamoAtividadeTO> listaAtividadeTOs = new ArrayList<>();
			for (ImovelRamoAtividade ramo : imovel.getUnidadesConsumidoras()) {
				ImovelRamoAtividadeTO imovelRamoAtividadeTO = new ImovelRamoAtividadeTO();
				if (ramo.getChavePrimaria() > 0) {
					imovelRamoAtividadeTO.setChavePrimaria(ramo.getChavePrimaria());
				}

				if (ramo.getRamoAtividade() != null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(ramo.getRamoAtividade().getChavePrimaria());
					combo.setDescricao(ramo.getRamoAtividade().getDescricao());
					imovelRamoAtividadeTO.setRamoAtividadeTO(combo);
				}

				if (ramo.getQuantidadeEconomia() != null && ramo.getQuantidadeEconomia() > 0) {
					imovelRamoAtividadeTO.setQuantidadeUnidadesConsumidoras(ramo.getQuantidadeEconomia().toString());
				}

				if (ramo.getRamoAtividade().getSegmento() != null) {
					TipoComboBoxTO combo = new TipoComboBoxTO();
					combo.setChavePrimaria(ramo.getRamoAtividade().getSegmento().getChavePrimaria());
					combo.setDescricao(ramo.getRamoAtividade().getSegmento().getDescricao());
					imovelRamoAtividadeTO.setSegmento(combo);
				}
				listaAtividadeTOs.add(imovelRamoAtividadeTO);
			}
			imovelTO.setUnidadesConsumidoras(listaAtividadeTOs);
		}

		converterImovelEmImovelTOPasso2(imovelTO, imovel);

		return imovelTO;
	}

	private void verificarSetarCamposImovelTO(Imovel imovel, ImovelTO imovelTO) throws NegocioException {
		if (imovel.getValvulaBloqueio() == null) {
			imovelTO.setValvulaBloqueio(false);
		} else {
			imovelTO.setValvulaBloqueio(imovel.getValvulaBloqueio());

		}

		if (imovel.getQuadraFace() != null) {
			TipoComboBoxTO quadraTO = new TipoComboBoxTO();
			quadraTO.setChavePrimaria(imovel.getQuadraFace().getQuadra().getChavePrimaria());
			quadraTO.setDescricao(imovel.getQuadraFace().getQuadra().getNumeroQuadra());
			imovelTO.setQuadra(quadraTO);
		}

		if (imovel.getQuadraFace() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(imovel.getQuadraFace().getChavePrimaria());
			combo.setDescricao(imovel.getQuadraFace().getQuadraFaceFormatada());
			imovelTO.setQuadraFace(combo);
		}

		imovelTO.setEnderecoReferencia(imovel.getEnderecoReferencia());

		if (imovel.getAreaConstruidaFaixa() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(imovel.getAreaConstruidaFaixa().getChavePrimaria());
			combo.setDescricao(String.valueOf(imovel.getAreaConstruidaFaixa().getMenorFaixa()));
			combo.setDescricao1(String.valueOf(imovel.getAreaConstruidaFaixa().getMaiorFaixa()));
			imovelTO.setAreaConstruidaFaixa(combo);
		}

		if (imovel.getPavimentoCalcada() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(imovel.getPavimentoCalcada().getChavePrimaria());
			combo.setDescricao(imovel.getPavimentoCalcada().getDescricao());
			imovelTO.setPavimentoCalcada(combo);
		}

		if (imovel.getPavimentoRua() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(imovel.getPavimentoRua().getChavePrimaria());
			combo.setDescricao(imovel.getPavimentoRua().getDescricao());
			imovelTO.setPavimentoRua(combo);
		}

		if (imovel.getSituacaoImovel() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(imovel.getSituacaoImovel().getChavePrimaria());
			combo.setDescricao(imovel.getSituacaoImovel().getDescricao());
			imovelTO.setSituacaoImovel(combo);

		}

		if (imovel.getPerfilImovel() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(imovel.getPerfilImovel().getChavePrimaria());
			combo.setDescricao(imovel.getPerfilImovel().getDescricao());
			imovelTO.setPerfilImovel(combo);
		}

		if (imovel.getPadraoConstrucao() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(imovel.getPadraoConstrucao().getChavePrimaria());
			combo.setDescricao(imovel.getPadraoConstrucao().getDescricao());
			imovelTO.setPadraoConstrucao(combo);
		}

		if (imovel.getTipoBotijao() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(imovel.getTipoBotijao().getChavePrimaria());
			combo.setDescricao(imovel.getTipoBotijao().getDescricao());
			imovelTO.setTipoBotijao(combo);
		}

		if (imovel.getModalidadeMedicaoImovel() != null) {
			TipoComboBoxTO modalidade = new TipoComboBoxTO();
			modalidade.setChavePrimaria(Long.valueOf(imovel.getModalidadeMedicaoImovel().getCodigo()));
			modalidade.setDescricao(imovel.getModalidadeMedicaoImovel().getDescricao());
			imovelTO.setModalidadeMedicaoImovel(modalidade);
		}

		imovelTO.setNumeroLote(imovel.getNumeroLote());

		if (imovel.getNumeroSublote() != null && imovel.getNumeroSublote() > 0) {
			imovelTO.setNumeroSublote(imovel.getNumeroSublote());
		}

		if (imovel.getChavePrimaria() > 0) {
			Collection<ImovelRamoAtividade> lista = fachada.listarUnidadeConsumidoraPorChaveImovel(imovel.getChavePrimaria());
			if (!CollectionUtils.isEmpty(lista)) {
				Collection<ImovelRamoAtividade> listaHash = new HashSet<>();
				for (ImovelRamoAtividade ramo : lista) {
					listaHash.add(ramo);
				}
				imovel.setUnidadesConsumidoras(listaHash);
			}
		}
	}

	/**
	 * Converter imovel em imovel to passo2.
	 *
	 * @param imovelTO
	 *            the imovel to
	 * @param imovel
	 *            the imovel
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void converterImovelEmImovelTOPasso2(ImovelTO imovelTO, Imovel imovel) throws GGASException {

		imovelTO.setNumeroTestada(imovel.getNumeroTestada());

		imovelTO.setQuantidadeUnidadeConsumidora(imovel.getQuantidadeUnidadeConsumidora());

		imovelTO.setQuantidadePontoConsumo(imovel.getQuantidadePontoConsumo());

		imovelTO.setNumeroSequenciaLeitura(imovel.getNumeroSequenciaLeitura());

		imovelTO.setQuantidadeBloco(imovel.getQuantidadeBloco());

		imovelTO.setQuantidadeAndar(imovel.getQuantidadeAndar());

		imovelTO.setQuantidadeBanheiro(imovel.getQuantidadeBanheiro());

		imovelTO.setQuantidadeApartamentoAndar(imovel.getQuantidadeApartamentoAndar());

		imovelTO.setNumeroImovel(imovel.getNumeroImovel());

		imovelTO.setDescricaoComplemento(imovel.getDescricaoComplemento());

		imovelTO.setNome(imovel.getNome());

		if (imovel.getQuadraFace() != null) {
			imovelTO.setCep(imovel.getQuadraFace().getEndereco().getCep().getCep());
		}

		if (imovel.getEnderecoFormatado() != null) {
			imovelTO.setEndereco(imovel.getEnderecoFormatado());
		}

		if (imovel.getRedePreexistente() != null) {
			imovelTO.setRedePreexistente(imovel.getRedePreexistente());
		} else {
			imovelTO.setRedePreexistente(false);
		}

		if (imovel.getDataEntrega() != null) {
			imovelTO.setDataEntrega(DataUtil.converterDataParaString(imovel.getDataEntrega()));
		}

		if (imovel.getDataPrevisaoEncerramentoObra() != null) {
			imovelTO.setDataPrevisaoEncerramentoObra(DataUtil.converterDataParaString(imovel.getDataPrevisaoEncerramentoObra()));
		}

		if (imovel.getIndicadorObraTubulacao() != null) {
			imovelTO.setIndicadorObraTubulacao(imovel.getIndicadorObraTubulacao());
		} else {
			imovelTO.setIndicadorObraTubulacao(false);
		}

		if (imovel.getCondominio() != null) {
			imovelTO.setCondominio(imovel.getCondominio());
		} else {
			imovelTO.setCondominio(false);
		}

		if (!CollectionUtils.isEmpty(imovel.getContatos())) {
			Collection<ContatoImovelTO> contatosImovelTO = new ArrayList<>();
			for (ContatoImovel contato : imovel.getContatos()) {
				ContatoImovelTO contatoTO = new ContatoImovelTO();
				if (contato.getChavePrimaria() > 0) {
					contatoTO.setChavePrimaria(contato.getChavePrimaria());
				}
				contatoTO.setNome(contato.getNome());
				contatoTO.setDDD(contato.getCodigoDDD());
				contatoTO.setFone(contato.getFone());
				contatoTO.setEmail(contato.getEmail());
				contatoTO.setPrincipal(contato.isPrincipal());
				contatoTO.setArea(contato.getDescricaoArea());
				contatoTO.setRamal(contato.getRamal());
				if (contato.isPrincipal() && contato.getCodigoDDD() != null && contato.getFone() != null) {
					imovelTO.setFoneContato("(" + contato.getCodigoDDD() + ")" + contato.getFone());
				}
				if (contato.getTipoContato() != null) {
					TipoComboBoxTO tipoContatoTO = new TipoComboBoxTO();
					tipoContatoTO.setChavePrimaria(contato.getTipoContato().getChavePrimaria());
					tipoContatoTO.setDescricao(contato.getTipoContato().getDescricao());
					contatoTO.setTipoContato(tipoContatoTO);
				}

				if (contato.getProfissao() != null) {
					TipoComboBoxTO profissaoTO = new TipoComboBoxTO();
					profissaoTO.setChavePrimaria(contato.getProfissao().getChavePrimaria());
					profissaoTO.setDescricao(contato.getProfissao().getDescricao());
					contatoTO.setProfissao(profissaoTO);
				}
				contatosImovelTO.add(contatoTO);
			}
			imovelTO.getContatos().addAll(contatosImovelTO);
		}

		if (imovel.getRedeInternaImovel() != null) {
			RedeInternaImovelTO redeInternaImovelTO = converterReImoInternaImovel(imovel.getRedeInternaImovel());
			imovelTO.setRedeInternaImovelTO(redeInternaImovelTO);
		} else {
			imovelTO.setRedeInternaImovelTO(null);
		}

		if (imovel.getRota() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(imovel.getRota().getChavePrimaria());
			combo.setDescricao(imovel.getRota().getNumeroRota());
			imovelTO.setRota(combo);
		}

		if (imovel.getAgente() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(imovel.getAgente().getChavePrimaria());
			combo.setDescricao(imovel.getAgente().getFuncionario().getNome());
			imovelTO.setAgente(combo);
		}

	}

	/**
	 * Converter rede interna imovel to.
	 *
	 * @param redeInternaImovelTO the rede interna imovel to
	 * @param redeImovel the rede imovel
	 * @return the rede interna imovel
	 * @throws GGASException the GGAS exception
	 */
	private RedeInternaImovel converterRedeInternaImovelTO(RedeInternaImovelTO redeInternaImovelTO, RedeInternaImovel redeImovel)
			throws GGASException {

		RedeInternaImovel redeInternaImovel = null;
		if (redeImovel == null) {
			redeInternaImovel = Fachada.getInstancia().criarRedeInternaImovel();
		} else {
			redeInternaImovel = redeImovel;
		}

		if (redeInternaImovelTO.getChavePrimaria() != null && redeInternaImovelTO.getChavePrimaria() > 0) {
			redeInternaImovel.setChavePrimaria(redeInternaImovelTO.getChavePrimaria());
		}

		redeInternaImovel.setHabilitado(Boolean.TRUE);

		if (redeInternaImovelTO.getQuantidadePrumada() != null) {
			redeInternaImovel.setQuantidadePrumada(redeInternaImovelTO.getQuantidadePrumada());
		} else {
			redeInternaImovel.setQuantidadePrumada(0);
		}

		if (redeInternaImovelTO.getQuantidadeReguladorHall() != null) {
			redeInternaImovel.setQuantidadeReguladorHall(redeInternaImovelTO.getQuantidadeReguladorHall());
		} else {
			redeInternaImovel.setQuantidadeReguladorHall(0);
		}

		redeInternaImovel.setVentilacaoApartamento(redeInternaImovelTO.getVentilacaoApartamento());

		redeInternaImovel.setVentilacaoHall(redeInternaImovelTO.getVentilacaoHall());

		if (redeInternaImovelTO.getRedeDiametroCentral() != null) {
			redeInternaImovel.setRedeDiametroCentral(
					fachada.buscarRedeDiametroPorChave(redeInternaImovelTO.getRedeDiametroCentral().getChavePrimaria()));
		}

		if (redeInternaImovelTO.getRedeDiametroPrumada() != null) {
			redeInternaImovel.setRedeDiametroPrumada(
					fachada.buscarRedeDiametroPorChave(redeInternaImovelTO.getRedeDiametroPrumada().getChavePrimaria()));
		}

		if (redeInternaImovelTO.getRedeDiametroPrumadaApartamento() != null) {
			redeInternaImovel.setRedeDiametroPrumadaApartamento(
					fachada.buscarRedeDiametroPorChave(redeInternaImovelTO.getRedeDiametroPrumadaApartamento().getChavePrimaria()));
		}

		if (redeInternaImovelTO.getRedeMaterial() != null) {
			redeInternaImovel.setRedeMaterial(fachada.buscarRedeMaterialPorChave(redeInternaImovelTO.getRedeMaterial().getChavePrimaria()));
		}

		redeInternaImovel.setUltimaAlteracao(new Date());

		return redeInternaImovel;
	}

	/**
	 * Converter re imo interna imovel.
	 *
	 * @param redeInternaImovel
	 *            the rede interna imovel
	 * @return the rede interna imovel to
	 */
	private RedeInternaImovelTO converterReImoInternaImovel(RedeInternaImovel redeInternaImovel) {

		RedeInternaImovelTO redeInternaImovelTO = new RedeInternaImovelTO();

		if (redeInternaImovel.getChavePrimaria() > 0) {
			redeInternaImovelTO.setChavePrimaria(redeInternaImovel.getChavePrimaria());
		}

		if (redeInternaImovel.getQuantidadePrumada() != null) {
			redeInternaImovelTO.setQuantidadePrumada(redeInternaImovel.getQuantidadePrumada());
		}

		if (redeInternaImovel.getQuantidadeReguladorHall() != null) {
			redeInternaImovelTO.setQuantidadeReguladorHall(redeInternaImovel.getQuantidadeReguladorHall());
		}

		if (redeInternaImovel.getVentilacaoApartamento() != null) {
			redeInternaImovelTO.setVentilacaoApartamento(redeInternaImovel.getVentilacaoApartamento());
		} else {
			redeInternaImovelTO.setVentilacaoApartamento(false);
		}

		if (redeInternaImovel.getVentilacaoHall() != null) {
			redeInternaImovelTO.setVentilacaoHall(redeInternaImovel.getVentilacaoHall());
		} else {
			redeInternaImovelTO.setVentilacaoHall(false);
		}

		if (redeInternaImovel.getRedeDiametroCentral() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(redeInternaImovel.getRedeDiametroCentral().getChavePrimaria());
			combo.setDescricao(redeInternaImovel.getRedeDiametroCentral().getDescricao());
			redeInternaImovelTO.setRedeDiametroCentral(combo);
		}

		if (redeInternaImovel.getRedeDiametroPrumada() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(redeInternaImovel.getRedeDiametroPrumada().getChavePrimaria());
			combo.setDescricao(redeInternaImovel.getRedeDiametroPrumada().getDescricao());
			redeInternaImovelTO.setRedeDiametroPrumada(combo);
		}

		if (redeInternaImovel.getRedeDiametroPrumadaApartamento() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(redeInternaImovel.getRedeDiametroPrumadaApartamento().getChavePrimaria());
			combo.setDescricao(redeInternaImovel.getRedeDiametroPrumadaApartamento().getDescricao());
			redeInternaImovelTO.setRedeDiametroPrumadaApartamento(combo);
		}

		if (redeInternaImovel.getRedeMaterial() != null) {
			TipoComboBoxTO combo = new TipoComboBoxTO();
			combo.setChavePrimaria(redeInternaImovel.getRedeMaterial().getChavePrimaria());
			combo.setDescricao(redeInternaImovel.getRedeMaterial().getDescricao());
			redeInternaImovelTO.setRedeMaterial(combo);
		}

		return redeInternaImovelTO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#validarPontoConsumoEquipamento(java.util.Collection,
	 * br.com.ggas.cadastro.imovel.PontoConsumoEquipamento, java.lang.Integer)
	 */
	@Override
	public void validarPontoConsumoEquipamento(Collection<PontoConsumoEquipamento> listPontosConsumoEquipamento,
			PontoConsumoEquipamento pontoConsumoEquipamento, Integer indexPontoConsumoEquipamento)
					throws NegocioException {

		Map<String, Object> validarPontoConsumoEquipamento = pontoConsumoEquipamento.validarDados();
		List<PontoConsumoEquipamento> pontosConsumoEquipamento = new ArrayList<>();

		if (!validarPontoConsumoEquipamento.isEmpty()) {
			throw new NegocioException(validarPontoConsumoEquipamento);
		}

		if (listPontosConsumoEquipamento != null) {
			pontosConsumoEquipamento.addAll(listPontosConsumoEquipamento);
		}

		if (listPontosConsumoEquipamento != null && !listPontosConsumoEquipamento.isEmpty()
						&& indexPontoConsumoEquipamento > -1) {
			pontosConsumoEquipamento.remove(indexPontoConsumoEquipamento.intValue());
		}

		if ((pontoConsumoEquipamento.getPotencia() != null)
						&& (pontoConsumoEquipamento.getPotencia().compareTo(BigDecimal.ZERO)) <= 0) {
			throw new NegocioException(ERRO_PONTO_CONSUMO_EQUIPAMENTO_VAZA0_ZERO, true);
		}

		if ((pontoConsumoEquipamento.getDiasPorSemana() != null)
				&& (pontoConsumoEquipamento.getDiasPorSemana().intValue() < 0) ) {
			throw new NegocioException(ERRO_PONTO_CONSUMO_EQUIPAMENTO_DIAS_0, true);
		} else {
			if (pontoConsumoEquipamento.getDiasPorSemana() != null
							&& pontoConsumoEquipamento.getDiasPorSemana().intValue() > DIAS_SEMANA) {
				throw new NegocioException(ERRO_PONTO_CONSUMO_EQUIPAMENTO_DIAS_MAXIMOS_7, true);
			}
		}

		if ((pontoConsumoEquipamento.getHorasPorDia() != null)
				&& (pontoConsumoEquipamento.getHorasPorDia().intValue() < 0) ) {
			throw new NegocioException(ERRO_PONTO_CONSUMO_EQUIPAMENTO_HORAS_ZERO, true);
		} else {
			if (pontoConsumoEquipamento.getDiasPorSemana() != null
							&& pontoConsumoEquipamento.getDiasPorSemana().intValue() > DIAS_SEMANA) {
				throw new NegocioException(ERRO_PONTO_CONSUMO_EQUIPAMENTO_HORAS_MAXIMAS_24, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#validarPontoConsumoEEquipamento(java.util.Collection,
	 * br.com.ggas.cadastro.imovel.PontoConsumoEquipamento, java.lang.Integer)
	 */
	@Override
	public void validarPontoConsumoEEquipamento(PontoConsumo pontoConsumo, PontoConsumoEquipamento pontoConsumoEquipamento)
			throws NegocioException {

		if (pontoConsumo.getSegmento().getChavePrimaria() != pontoConsumoEquipamento.getEquipamento().getSegmento().getChavePrimaria()) {
			throw new NegocioException(ERRO_PONTO_CONSUMO_EQUIPAMENTO_DIFERENTES_SEGMENTOS, true);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional#consultarUnidadeOrganizacionalOrdenada()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<ServicoTipo> consultarServicoTipoOrdenados() {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(ServicoTipo.class.getSimpleName());
		hql.append(" servicoTipo ");
		hql.append(" order by servicoTipo.descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/**
	 * Consultar servico tipo.
	 *
	 * @param chavePrimaria
	 * 			the chave primaria
	 * @return the servico tipo
	 */
	@Override
	public ServicoTipo consultarServicoTipo(Long chavePrimaria) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(ServicoTipo.class.getSimpleName());
		hql.append(" servico ");
		hql.append(WHERE);
		hql.append(" servico.chavePrimaria = :chavePrimaria ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter(CHAVE_PRIMARIA, chavePrimaria);

		return (ServicoTipo) query.uniqueResult();
	}

	/**
	 * Consultar servico tipo.
	 *
	 * @param chavePrimaria the chave primaria
	 * @return the servico tipo
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<ImovelServicoTipoRestricao> consultarImovelServicoTipoRestricaoPorImovel(Long chavePrimariaImovel) {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(ImovelServicoTipoRestricaoImpl.class.getSimpleName());
		hql.append(" imovelServicoTipoRestricao ");
		hql.append(WHERE);
		hql.append(" imovelServicoTipoRestricao.imovel.chavePrimaria = :chavePrimariaImovel ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("chavePrimariaImovel", chavePrimariaImovel);

		return query.list();
	}
	
	@Override
	public Collection<Imovel> consultarImovelCondominio(String descricaoCondominio, Integer offset) {
		StringBuilder hql = new StringBuilder("SELECT imovel ");
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" imovel ");
		hql.append(" WHERE 1=1 ");

		hql.append(" and imovel.habilitado = true ");
		hql.append(" and imovel.condominio = true ");
		hql.append(" and upper(imovel.nome) like :descricaoCondominio ");

		hql.append(" ORDER BY imovel.nome ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("descricaoCondominio", Util.removerAcentuacao(Util.formatarTextoConsulta(descricaoCondominio).toUpperCase()));
		

		query.setMaxResults(50);

		if (offset != 0) {
			offset = (50 * offset) + 1;
		}

		query.setFirstResult(offset);

		return (Collection<Imovel>) query.list().stream().distinct().collect(Collectors.toList());
	}
}
