/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Map;

import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.RedeInternaImovel;
import br.com.ggas.cadastro.operacional.RedeDiametro;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pelos métodos relacionados 
 * a identificação da rede interna do imóvel
 *
 */
public class RedeInternaImovelImpl extends EntidadeNegocioImpl implements RedeInternaImovel {

	private static final long serialVersionUID = -4430839627676812555L;

	private Imovel imovel;

	private RedeMaterial redeMaterial;

	private Integer quantidadePrumada;

	private Integer quantidadeReguladorHall;

	private Boolean ventilacaoHall;

	private Boolean ventilacaoApartamento;

	private RedeDiametro redeDiametroCentral;

	private RedeDiametro redeDiametroPrumada;

	private RedeDiametro redeDiametroPrumadaApartamento;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel#getImovel()
	 */
	@Override
	public Imovel getImovel() {

		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel
	 * #setImovel(br.com.ggas.cadastro
	 * .imovel.Imovel)
	 */
	@Override
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel#getRedeMaterial()
	 */
	@Override
	public RedeMaterial getRedeMaterial() {

		return redeMaterial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel
	 * #setRedeMaterial(br.com.ggas
	 * .cadastro.operacional.RedeMaterial)
	 */
	@Override
	public void setRedeMaterial(RedeMaterial redeMaterial) {

		this.redeMaterial = redeMaterial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel#getQuantidadePrumada()
	 */
	@Override
	public Integer getQuantidadePrumada() {

		return quantidadePrumada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel
	 * #setQuantidadePrumada(java.lang.Integer)
	 */
	@Override
	public void setQuantidadePrumada(Integer quantidadePrumada) {

		this.quantidadePrumada = quantidadePrumada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel
	 * #getQuantidadeReguladorHall()
	 */
	@Override
	public Integer getQuantidadeReguladorHall() {

		return quantidadeReguladorHall;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel
	 * #setQuantidadeReguladorHall
	 * (java.lang.Integer)
	 */
	@Override
	public void setQuantidadeReguladorHall(Integer quantidadeReguladorHall) {

		this.quantidadeReguladorHall = quantidadeReguladorHall;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel#getVentilacaoHall()
	 */
	@Override
	public Boolean getVentilacaoHall() {

		return ventilacaoHall;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel
	 * #setVentilacaoHall(java.lang.Boolean)
	 */
	@Override
	public void setVentilacaoHall(Boolean ventilacaoHall) {

		this.ventilacaoHall = ventilacaoHall;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel
	 * #getVentilacaoApartamento()
	 */
	@Override
	public Boolean getVentilacaoApartamento() {

		return ventilacaoApartamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.impl.
	 * RedeInternaImovel
	 * #setVentilacaoApartamento(java
	 * .lang.Boolean)
	 */
	@Override
	public void setVentilacaoApartamento(Boolean ventilacaoApartamento) {

		this.ventilacaoApartamento = ventilacaoApartamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.RedeInternaImovel
	 * #getRedeDiametroCentral()
	 */
	@Override
	public RedeDiametro getRedeDiametroCentral() {

		return redeDiametroCentral;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.RedeInternaImovel
	 * #
	 * setRedeDiametroCentral(br.com.ggas.cadastro
	 * .operacional.RedeDiametro)
	 */
	@Override
	public void setRedeDiametroCentral(RedeDiametro redeDiametroCentral) {

		this.redeDiametroCentral = redeDiametroCentral;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.RedeInternaImovel
	 * #getRedeDiametroPrumada()
	 */
	@Override
	public RedeDiametro getRedeDiametroPrumada() {

		return redeDiametroPrumada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.RedeInternaImovel
	 * #
	 * setRedeDiametroPrumada(br.com.ggas.cadastro
	 * .operacional.RedeDiametro)
	 */
	@Override
	public void setRedeDiametroPrumada(RedeDiametro redeDiametroPrumada) {

		this.redeDiametroPrumada = redeDiametroPrumada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.RedeInternaImovel
	 * #getRedeDiametroPrumadaApartamento()
	 */
	@Override
	public RedeDiametro getRedeDiametroPrumadaApartamento() {

		return redeDiametroPrumadaApartamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.RedeInternaImovel
	 * #
	 * setRedeDiametroPrumadaApartamento(br.com.ggas
	 * .cadastro.operacional.RedeDiametro)
	 */
	@Override
	public void setRedeDiametroPrumadaApartamento(RedeDiametro redeDiametroPrumadaApartamento) {

		this.redeDiametroPrumadaApartamento = redeDiametroPrumadaApartamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}
}
