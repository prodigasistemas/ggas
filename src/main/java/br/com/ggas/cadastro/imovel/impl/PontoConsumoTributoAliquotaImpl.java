/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.imovel.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * 
 *
 */
class PontoConsumoTributoAliquotaImpl extends EntidadeNegocioImpl implements PontoConsumoTributoAliquota {

	private static final int LIMITE_CAMPO = 2;

	/**
     * 
     */
	private static final long serialVersionUID = 4813563869270229414L;

	/**
	 * serialVersionUID
	 */

	private PontoConsumo pontoConsumo;

	private Tributo tributo;

	private Date dataInicioVigencia;

	private Date dataFimVigencia;

	private BigDecimal porcentagemAliquota;

	private Boolean indicadorIsencao;

	private Boolean destaqueIndicadorNota;

	private Boolean indicadorCreditoIcms;
	
	private Boolean indicadorIcmsSubstitutoTarifa;
	
	private Boolean indicadorDrawback;
	
	private BigDecimal valorDrawback;

	/**
	 * @return the tributo
	 */
	@Override
	public Tributo getTributo() {

		return tributo;
	}

	/**
	 * @param tributo
	 *            the tributo to set
	 */
	@Override
	public void setTributo(Tributo tributo) {

		this.tributo = tributo;
	}

	/**
	 * @return the dataInicioVigencia
	 */
	@Override
	public Date getDataInicioVigencia() {

		return dataInicioVigencia;
	}

	/**
	 * @param dataInicioVigencia
	 *            the dataInicioVigencia to set
	 */
	@Override
	public void setDataInicioVigencia(Date dataInicioVigencia) {

		this.dataInicioVigencia = dataInicioVigencia;
	}

	/**
	 * @return the dataFimVigencia
	 */
	@Override
	public Date getDataFimVigencia() {

		return dataFimVigencia;
	}

	/**
	 * @param dataFimVigencia
	 *            the dataFimVigencia to set
	 */
	@Override
	public void setDataFimVigencia(Date dataFimVigencia) {

		this.dataFimVigencia = dataFimVigencia;
	}

	/**
	 * @return the pontoConsumo
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return the porcentagemAliquota
	 */
	@Override
	public BigDecimal getPorcentagemAliquota() {

		return porcentagemAliquota;
	}

	/**
	 * @param porcentagemAliquota
	 *            the porcentagemAliquota to set
	 */
	@Override
	public void setPorcentagemAliquota(BigDecimal porcentagemAliquota) {

		this.porcentagemAliquota = porcentagemAliquota;
	}

	/**
	 * @return the indicadorIsencao
	 */
	@Override
	public Boolean getIndicadorIsencao() {

		return indicadorIsencao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(tributo == null) {
			stringBuilder.append(PONTO_CONSUMO_TRIBUTO_ALIQUOTA_TRIBUTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataInicioVigencia == null) {
			stringBuilder.append(PONTO_CONSUMO_TRIBUTO_DATA_INICIO_VIGENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (indicadorIsencao == null) {
			stringBuilder.append(PONTO_CONSUMO_TRIBUTO_INDICADOR_ISENCAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if (!indicadorIsencao && porcentagemAliquota == null) {
				stringBuilder.append(PONTO_CONSUMO_TRIBUTO_PORCENTAGEM_ALIQUOTA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	/**
	 * @param indicadorIsencao
	 *            the indicadorIsencao to set
	 */
	@Override
	public void setIndicadorIsencao(Boolean indicadorIsencao) {

		this.indicadorIsencao = indicadorIsencao;
	}

	@Override
	public Boolean getDestaqueIndicadorNota() {

		return destaqueIndicadorNota;
	}

	@Override
	public void setDestaqueIndicadorNota(Boolean destaqueIndicadorNota) {

		this.destaqueIndicadorNota = destaqueIndicadorNota;
	}

	@Override
	public Boolean getIndicadorCreditoIcms() {

		return indicadorCreditoIcms;
	}

	@Override
	public void setIndicadorCreditoIcms(Boolean indicadorCreditoIcms) {

		this.indicadorCreditoIcms = indicadorCreditoIcms;
	}

	@Override
	public Boolean getIndicadorIcmsSubstitutoTarifa() {

		return indicadorIcmsSubstitutoTarifa;
	}

	@Override
	public void setIndicadorIcmsSubstitutoTarifa(Boolean indicadorIcmsSubstitutoTarifa) {

		this.indicadorIcmsSubstitutoTarifa = indicadorIcmsSubstitutoTarifa;
	}

	@Override
	public Boolean getIndicadorDrawback() {

		return indicadorDrawback;
	}

	@Override
	public void setIndicadorDrawback(Boolean indicadorDrawback) {

		this.indicadorDrawback = indicadorDrawback;
	}

	@Override
	public BigDecimal getValorDrawback() {

		return valorDrawback;
	}

	@Override
	public void setValorDrawback(BigDecimal valorDrawback) {

		this.valorDrawback = valorDrawback;
	}

}
