package br.com.ggas.cadastro.imovel.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pela representação de uma restrição de servico para o imovel
 *
 */
public class ImovelServicoTipoRestricaoImpl extends EntidadeNegocioImpl implements ImovelServicoTipoRestricao {

	private static final long serialVersionUID = -8967421639563651552L;

	private Imovel imovel;

	private ServicoTipo servicoTipo;

	/**
	 * Nova instancia da classe com valores default para versão, ultima alteração e habilitado
	 */
	public ImovelServicoTipoRestricaoImpl() {
		this.setUltimaAlteracao(new Date());
		this.setHabilitado(true);
		this.setVersao(1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao#getImovel()
	 */
	@Override
	public Imovel getImovel() {
		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao#setImovel(br.com.ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao#getServicoTipo()
	 */
	@Override
	public ServicoTipo getServicoTipo() {
		return servicoTipo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel.ImovelServicoTipoRestricao#setServicoTipo(br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo)
	 */
	@Override
	public void setServicoTipo(ServicoTipo servicoTipo) {
		this.servicoTipo = servicoTipo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#setUltimaAlteracao(java.util.Date)
	 */
	@Override
	public final void setUltimaAlteracao(Date ultimaAlteracao) {
		super.setUltimaAlteracao(ultimaAlteracao);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#setHabilitado(boolean)
	 */
	@Override
	public final void setHabilitado(boolean habilitado) {
		super.setHabilitado(habilitado);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#setVersao(int)
	 */
	@Override
	public final void setVersao(int versao) {
		super.setVersao(versao);
	}

}
