/*

 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorGrupoFaturamentoImpl representa uma ControladorGrupoFaturamentoImpl no sistema.
 *
 * @since 21/06/2019
 *
 */
package br.com.ggas.cadastro.imovel.impl;

import br.com.ggas.cadastro.imovel.ControladorGrupoFaturamento;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe responsável pela implementação dos métodos relacionados ao Controlador de GrupoFaturamento.
 *
 */
public class ControladorGrupoFaturamentoImpl extends ControladorNegocioImpl implements ControladorGrupoFaturamento {


	@Override
	public EntidadeNegocio criar() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(GrupoFaturamento.BEAN_ID_GRUPO_FATURAMENTO);
	}

	@Override
	public Class<?> getClasseEntidade() {
		return ServiceLocator.getInstancia().getClassPorID(GrupoFaturamento.BEAN_ID_GRUPO_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorGrupoFaturamento#isPeriodicidadeGrupoMultiplosCiclos(br.com.ggas.cadastro.imovel.GrupoFaturamento)
	 */
	@Override
	public boolean isPeriodicidadeGrupoMultiplosCiclos(GrupoFaturamento grupo) throws NegocioException{
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		if (grupo == null || grupo.getPeriodicidade() == null) {
			return false;
		}
		return controladorParametroSistema.obterValorParametroUtilizacaoMultiplosCiclos() && grupo.getPeriodicidade().isMultiplusCiclos();
	}
}
