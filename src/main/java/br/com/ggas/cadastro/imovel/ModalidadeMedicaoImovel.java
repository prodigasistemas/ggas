/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.geral.dominio.EntidadeDominio;

/**
 * Interface responsável pela assinatura dos métodos relacionados a modalidade de medição do imóvel 
 *
 */
public interface ModalidadeMedicaoImovel extends EntidadeDominio {

	String BEAN_ID_MODALIDADE_MEDICAO_IMOVEL = "modalidadeMedicaoImovel";

	// FIXME: Substituir por constante
	/**
	 * @deprecated
	 */
	@Deprecated
	public static int INDIVIDUAL = 1;
	
	// FIXME: Substituir por constante
	/**
	 * @deprecated
	 */
	@Deprecated
	public static int COLETIVA = 2;

	public static String DESCRICAO_INDIVIDUAL = "Individual";

	public static String DESCRICAO_COLETIVA = "Coletiva";

	Map<Integer, String> MODALIDADES_MEDICAO_IMOVEL = new HashMap<Integer, String>(){

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			put(INDIVIDUAL, DESCRICAO_INDIVIDUAL);
			put(COLETIVA, DESCRICAO_COLETIVA);
		}
	};

}
