/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável por implementar
 * métodos relacionados as tabelas
 *
 */
public class TabelaImpl extends EntidadeNegocioImpl implements Tabela {

	private static final long serialVersionUID = 7050658896945321531L;

	private String nome;

	private String descricao;

	private String mnemonico;

	private Boolean atributoDinamico;

	private Boolean consultaDinamica;

	private String atributosConsultaDinamica;

	private String nomeClasse;

	private Collection<Coluna> colunas;

	private Collection<Coluna> listaColunaTabela;

	private Boolean auditavel;

	private Menu menu;

	private Boolean alcada;

	private Long numeroMesesDescarte;

	private Boolean integracao;

	private Boolean mapeamento;

	private Boolean constante;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Tabela
	 * #getNome()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Tabela
	 * #setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Tabela
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Tabela
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Tabela
	 * #getMnemonico()
	 */
	@Override
	public String getMnemonico() {

		return mnemonico;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Tabela
	 * #setMnemonico(java.lang.String)
	 */
	@Override
	public void setMnemonico(String mnemonico) {

		this.mnemonico = mnemonico;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Tabela#
	 * getAtributoDinamico()
	 */
	@Override
	public Boolean getAtributoDinamico() {

		return atributoDinamico;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Tabela#
	 * setAtributoDinamico(java.lang.Boolean)
	 */
	@Override
	public void setAtributoDinamico(Boolean atributoDinamico) {

		this.atributoDinamico = atributoDinamico;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Tabela#
	 * getListaColunaTabela()
	 */
	@Override
	public Collection<Coluna> getListaColunaTabela() {

		return listaColunaTabela;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Tabela#
	 * setListaColunaTabela(java.util.Collection)
	 */
	@Override
	public void setListaColunaTabela(Collection<Coluna> listaColunaTabela) {

		this.listaColunaTabela = listaColunaTabela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Tabela#isAuditavel
	 * ()
	 */
	@Override
	public Boolean getAuditavel() {

		return auditavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Tabela#setAuditavel
	 * (java.lang.Boolean)
	 */
	@Override
	public void setAuditavel(Boolean auditavel) {

		this.auditavel = auditavel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Tabela#isConsultaDinamica()
	 */
	@Override
	public Boolean isConsultaDinamica() {

		return consultaDinamica;
	}

	@Override
	public void setConsultaDinamica(Boolean consultaDinamica) {

		this.consultaDinamica = consultaDinamica;
	}

	@Override
	public String getAtributosConsultaDinamica() {

		return atributosConsultaDinamica;
	}

	@Override
	public void setAtributosConsultaDinamica(String atributosConsultaDinamica) {

		this.atributosConsultaDinamica = atributosConsultaDinamica;
	}

	@Override
	public String getNomeClasse() {

		return nomeClasse;
	}

	@Override
	public void setNomeClasse(String nomeClasse) {

		this.nomeClasse = nomeClasse;
	}

	@Override
	public Collection<Coluna> getColunas() {

		return colunas;
	}

	@Override
	public void setColunas(Collection<Coluna> colunas) {

		this.colunas = colunas;
	}

	@Override
	public Boolean getConsultaDinamica() {

		return consultaDinamica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Tabela#getMenu
	 * ()
	 */
	@Override
	public Menu getMenu() {

		return menu;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Tabela#setMenu
	 * (br.com.ggas.geral.Menu)
	 */
	@Override
	public void setMenu(Menu menu) {

		this.menu = menu;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Tabela#isAlcada
	 * ()
	 */
	@Override
	public Boolean isAlcada() {

		return alcada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.Tabela#setAlcada
	 * (java.lang.Boolean)
	 */
	@Override
	public void setAlcada(Boolean alcada) {

		this.alcada = alcada;
	}

	/**
	 * @return the numeroMesesDescarte
	 */
	@Override
	public Long getNumeroMesesDescarte() {

		return numeroMesesDescarte;
	}

	/**
	 * @param numeroMesesDescarte
	 *            the numeroMesesDescarte to set
	 */
	@Override
	public void setNumeroMesesDescarte(Long numeroMesesDescarte) {

		this.numeroMesesDescarte = numeroMesesDescarte;
	}

	@Override
	public Boolean getIntegracao() {

		return integracao;
	}

	@Override
	public void setIntegracao(Boolean integracao) {

		this.integracao = integracao;
	}

	@Override
	public Boolean getMapeamento() {

		return mapeamento;
	}

	@Override
	public void setMapeamento(Boolean mapeamento) {

		this.mapeamento = mapeamento;
	}

	@Override
	public Boolean getConstante() {

		return constante;
	}

	@Override
	public void setConstante(Boolean constante) {

		this.constante = constante;
	}

}
