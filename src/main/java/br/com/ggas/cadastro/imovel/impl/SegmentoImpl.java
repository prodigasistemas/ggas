/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SegmentoAmostragemPCS;
import br.com.ggas.cadastro.imovel.SegmentoIntervaloPCS;
import br.com.ggas.cadastro.imovel.TipoSegmento;
import br.com.ggas.contabil.ContaContabil;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.util.Constantes;
/**
 * 
 * Classe responsável pelos métodos
 * relacionados ao Segmento
 *
 */
public class SegmentoImpl extends EntidadeNegocioImpl implements Segmento {

	private static final int LIMITE_DESCRICAO_ABREVIADA = 3;

	private static final int LIMITE_CAMPO = 2;

	private static final int LIMITE_DESCRICAO = 15;

	private static final long serialVersionUID = 3952220153884247862L;

	private String descricao;

	private String descricaoAbreviada;

	private TipoSegmento tipoSegmento;

	private BigDecimal consumoEstouro;

	private Integer numeroCiclos;

	private Integer numeroDiasFaturamento;

	private Collection<RamoAtividade> ramosAtividades = new HashSet<>();

	private ContaContabil contaContabil;

	private Collection<SegmentoAmostragemPCS> segmentoAmostragemPCSs = new HashSet<>();

	private Collection<SegmentoIntervaloPCS> segmentoIntervaloPCSs = new HashSet<>();

	private Integer tamanhoReducao;

	private Integer volumeMedioEstouro;

	private Integer previsaoJaneiro;

	private Integer previsaoFevereiro;

	private Integer previsaoMarco;

	private Integer previsaoAbril;

	private Integer previsaoMaio;

	private Integer previsaoJunho;

	private Integer previsaoJulho;

	private Integer previsaoAgosto;

	private Integer previsaoSetembro;

	private Integer previsaoOutubro;

	private Integer previsaoNovembro;

	private Integer previsaoDezembro;

	private Boolean usaProgramacaoConsumo;
	
	private Boolean indicadorEquipamentoPotenciaFixa;
	
	private Periodicidade periodicidade;
	
	private EntidadeConteudo tipoConsumoFaturamento;
	
	public static final long RESIDENCIAL = 4;

	/**
	 * @return the ramosAtividades
	 */
	@Override
	public Collection<RamoAtividade> getRamosAtividades() {

		return ramosAtividades;
	}

	/**
	 * @param ramosAtividades
	 *            the ramosAtividades to set
	 */
	@Override
	public void setRamosAtividades(Collection<RamoAtividade> ramosAtividades) {

		this.ramosAtividades = ramosAtividades;
	}

	/**
	 * @return the numeroDiasFaturamento
	 */
	@Override
	public Integer getNumeroDiasFaturamento() {

		return numeroDiasFaturamento;
	}

	/**
	 * @param numeroDiasFaturamento
	 *            the numeroDiasFaturamento to set
	 */
	@Override
	public void setNumeroDiasFaturamento(Integer numeroDiasFaturamento) {

		this.numeroDiasFaturamento = numeroDiasFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * getPeriodicidade()
	 */
	@Override
	public Periodicidade getPeriodicidade() {

		return periodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * setPeriodicidade
	 * (br.com.ggas.medicao.rota.Periodicidade)
	 */
	@Override
	public void setPeriodicidade(Periodicidade periodicidade) {

		this.periodicidade = periodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * getDescricaoAbreviada()
	 */
	@Override
	public String getDescricaoAbreviada() {

		return descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * setDescricaoAbreviada(java.lang.String)
	 */
	@Override
	public void setDescricaoAbreviada(String descricaoAbreviada) {

		this.descricaoAbreviada = descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * getConsumoEstouro()
	 */
	@Override
	public BigDecimal getConsumoEstouro() {

		return consumoEstouro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * setConsumoEstouro(java.math.BigDecimal)
	 */
	@Override
	public void setConsumoEstouro(BigDecimal consumoEstouro) {

		this.consumoEstouro = consumoEstouro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * getTipoSegmento()
	 */
	@Override
	public TipoSegmento getTipoSegmento() {

		return tipoSegmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * setTipoSegmento
	 * (br.com.ggas.cadastro.imovel.TipoSegmento)
	 */
	@Override
	public void setTipoSegmento(TipoSegmento tipoSegmento) {

		this.tipoSegmento = tipoSegmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * getNumeroMeses()
	 */
	@Override
	public Integer getNumeroCiclos() {

		return numeroCiclos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.Segmento#
	 * setNumeroMeses(java.lang.Integer)
	 */
	@Override
	public void setNumeroCiclos(Integer numeroCiclos) {

		this.numeroCiclos = numeroCiclos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder camposObrigatoriosAcumulados = new StringBuilder();
		StringBuilder tamanhoCamposAcumulados = new StringBuilder();
		String camposObrigatorios = null;
		String camposInvalidos = null;

		verificaDescricoes(camposObrigatoriosAcumulados, tamanhoCamposAcumulados);
		
		if(tipoSegmento == null) {
			camposObrigatoriosAcumulados.append(TIPO_SEGMENTO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(numeroCiclos == null) {
			camposObrigatoriosAcumulados.append(ERRO_NUMERO_CICLOS);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(periodicidade == null) {
			camposObrigatoriosAcumulados.append(ERRO_PERIODICIDA);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(tipoConsumoFaturamento == null) {
			camposObrigatoriosAcumulados.append(ERRO_TIPO_CONSUMO_FATURAMENTO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(numeroDiasFaturamento == null) {
			camposObrigatoriosAcumulados.append(ERRO_NUMERO_DIAS_FATURAMENTO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = camposObrigatoriosAcumulados.toString();
		camposInvalidos = tamanhoCamposAcumulados.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
							camposObrigatorios.substring(0, camposObrigatoriosAcumulados.toString().length() - LIMITE_CAMPO));
		}

		if(camposInvalidos.length() > 0) {
			erros.put(Constantes.ERRO_TAMANHO_LIMITE, camposInvalidos.substring(0, tamanhoCamposAcumulados.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

	/**
	 * Verifica campo Descrição e descrição abreviada.
	 *  
	 * @param camposObrigatoriosAcumulados
	 * @param tamanhoCamposAcumulados
	 */
	private void verificaDescricoes(StringBuilder camposObrigatoriosAcumulados,
			StringBuilder tamanhoCamposAcumulados) {
		if(StringUtils.isEmpty(descricao)) {
			camposObrigatoriosAcumulados.append(DESCRICAO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if(descricao.length() > LIMITE_DESCRICAO) {
				tamanhoCamposAcumulados.append(DESCRICAO);
				tamanhoCamposAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}
		if(StringUtils.isEmpty(descricaoAbreviada)) {
			camposObrigatoriosAcumulados.append(DESCRICAO_ABREVIADA);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if(descricaoAbreviada.length() > LIMITE_DESCRICAO_ABREVIADA) {
				tamanhoCamposAcumulados.append(descricaoAbreviada);
				tamanhoCamposAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}
	}

	@Override
	public ContaContabil getContaContabil() {

		return contaContabil;
	}

	@Override
	public void setContaContabil(ContaContabil contaContabil) {

		this.contaContabil = contaContabil;
	}

	@Override
	public Collection<SegmentoAmostragemPCS> getSegmentoAmostragemPCSs() {

		return segmentoAmostragemPCSs;
	}

	@Override
	public void setSegmentoAmostragemPCSs(Collection<SegmentoAmostragemPCS> segmentoAmostragemPCSs) {

		this.segmentoAmostragemPCSs = segmentoAmostragemPCSs;

	}

	@Override
	public Collection<SegmentoIntervaloPCS> getSegmentoIntervaloPCSs() {

		return segmentoIntervaloPCSs;
	}

	@Override
	public void setSegmentoIntervaloPCSs(Collection<SegmentoIntervaloPCS> segmentoIntervaloPCSs) {

		this.segmentoIntervaloPCSs = segmentoIntervaloPCSs;
	}

	@Override
	public Integer getTamanhoReducao() {

		return tamanhoReducao;
	}

	@Override
	public void setTamanhoReducao(Integer tamanhoReducao) {

		this.tamanhoReducao = tamanhoReducao;
	}

	@Override
	public Integer getVolumeMedioEstouro() {

		return volumeMedioEstouro;
	}

	@Override
	public void setVolumeMedioEstouro(Integer volumeMedioEstouro) {

		this.volumeMedioEstouro = volumeMedioEstouro;
	}

	@Override
	public Integer getPrevisaoJaneiro() {

		return previsaoJaneiro;
	}

	@Override
	public void setPrevisaoJaneiro(Integer previsaoJaneiro) {

		this.previsaoJaneiro = previsaoJaneiro;
	}

	@Override
	public Integer getPrevisaoFevereiro() {

		return previsaoFevereiro;
	}

	@Override
	public void setPrevisaoFevereiro(Integer previsaoFevereiro) {

		this.previsaoFevereiro = previsaoFevereiro;
	}

	@Override
	public Integer getPrevisaoMarco() {

		return previsaoMarco;
	}

	@Override
	public void setPrevisaoMarco(Integer previsaoMarco) {

		this.previsaoMarco = previsaoMarco;
	}

	@Override
	public Integer getPrevisaoAbril() {

		return previsaoAbril;
	}

	@Override
	public void setPrevisaoAbril(Integer previsaoAbril) {

		this.previsaoAbril = previsaoAbril;
	}

	@Override
	public Integer getPrevisaoMaio() {

		return previsaoMaio;
	}

	@Override
	public void setPrevisaoMaio(Integer previsaoMaio) {

		this.previsaoMaio = previsaoMaio;
	}

	@Override
	public Integer getPrevisaoJunho() {

		return previsaoJunho;
	}

	@Override
	public void setPrevisaoJunho(Integer previsaoJunho) {

		this.previsaoJunho = previsaoJunho;
	}

	@Override
	public Integer getPrevisaoJulho() {

		return previsaoJulho;
	}

	@Override
	public void setPrevisaoJulho(Integer previsaoJulho) {

		this.previsaoJulho = previsaoJulho;
	}

	@Override
	public Integer getPrevisaoAgosto() {

		return previsaoAgosto;
	}

	@Override
	public void setPrevisaoAgosto(Integer previsaoAgosto) {

		this.previsaoAgosto = previsaoAgosto;
	}

	@Override
	public Integer getPrevisaoSetembro() {

		return previsaoSetembro;
	}

	@Override
	public void setPrevisaoSetembro(Integer previsaoSetembro) {

		this.previsaoSetembro = previsaoSetembro;
	}

	@Override
	public Integer getPrevisaoOutubro() {

		return previsaoOutubro;
	}

	@Override
	public void setPrevisaoOutubro(Integer previsaoOutubro) {

		this.previsaoOutubro = previsaoOutubro;
	}

	@Override
	public Integer getPrevisaoNovembro() {

		return previsaoNovembro;
	}

	@Override
	public void setPrevisaoNovembro(Integer previsaoNovembro) {

		this.previsaoNovembro = previsaoNovembro;
	}

	@Override
	public Integer getPrevisaoDezembro() {

		return previsaoDezembro;
	}

	@Override
	public void setPrevisaoDezembro(Integer previsaoDezembro) {

		this.previsaoDezembro = previsaoDezembro;
	}

	@Override
	public Boolean getUsaProgramacaoConsumo() {

		return usaProgramacaoConsumo;
	}

	@Override
	public void setUsaProgramacaoConsumo(Boolean usaProgramacaoConsumo) {

		this.usaProgramacaoConsumo = usaProgramacaoConsumo;
	}

	/**
	 * @return the indicadorEquipamentoPotenciaFixa
	 */
	@Override
	public Boolean getindicadorEquipamentoPotenciaFixa() {
		return indicadorEquipamentoPotenciaFixa;
	}

	/**
	 * @param indicadorEquipamentoPotenciaFixa the indicadorEquipamentoPotenciaFixa to set
	 */
	@Override
	public void setindicadorEquipamentoPotenciaFixa(Boolean indicadorEquipamentoPotenciaFixa) {
		this.indicadorEquipamentoPotenciaFixa = indicadorEquipamentoPotenciaFixa;
	}

	/**
	 * @return the tipoConsumoFaturamento
	 */
	public EntidadeConteudo getTipoConsumoFaturamento() {
		return tipoConsumoFaturamento;
	}

	/**
	 * @param itipoConsumoFaturamento the tipoConsumoFaturamento to set
	 */
	public void setTipoConsumoFaturamento(EntidadeConteudo tipoConsumoFaturamento) {
		this.tipoConsumoFaturamento = tipoConsumoFaturamento;
	}
	
}
