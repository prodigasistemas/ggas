/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.RamoAtividadeAmostragemPCS;
import br.com.ggas.cadastro.imovel.RamoAtividadeIntervaloPCS;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 *
 */
class ControladorRamoAtividadeImpl extends ControladorNegocioImpl implements ControladorRamoAtividade {

	private static final String WHERE = " where ";
	private static final String DELETE = " delete ";
	private static final String INTERVALO_PCS = "intervaloPCS";
	private static final String PERIODICIDADE = "periodicidade";
	private static final String SEGMENTO = "segmento";
	private static final String FROM = " from ";


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(RamoAtividade.BEAN_ID_RAMO_ATIVIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(RamoAtividade.BEAN_ID_RAMO_ATIVIDADE);
	}

	public Class<?> getClasseEntidadeRamoAtividadeAmostragemPCS() {

		return ServiceLocator.getInstancia().getClassPorID(RamoAtividadeAmostragemPCS.BEAN_ID_RAMO_ATIVIDADE_AMOSTRAGEM_PCS);
	}

	public Class<?> getClasseEntidadeRamoAtividadeIntervaloPCS() {

		return ServiceLocator.getInstancia().getClassPorID(RamoAtividadeIntervaloPCS.BEAN_ID_RAMO_ATIVIDADE_INTERVALO_PCS);
	}

	public Class<?> getClasseEntidadeRamoAtividadeSubstituicaoTributaria() {

		return ServiceLocator.getInstancia().getClassPorID(RamoAtividadeSubstituicaoTributaria.BEAN_ID_SUBST_TRIBUTARIA);
	}

	public Class<?> getClasseEntidadeConteudo() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}

	public Class<?> getClasseIntervaloPCS() {

		return ServiceLocator.getInstancia().getClassPorID(IntervaloPCS.BEAN_ID_INTERVALO_PCS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorRamoAtividade
	 * #consultarRamoAtividade(java.util.Map)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<RamoAtividade> consultarRamoAtividade(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String descricao = (String) filtro.get(EntidadeConteudo.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(EntidadeConteudo.ATRIBUTO_DESCRICAO, descricao, MatchMode.ANYWHERE));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeConteudo.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeConteudo.ATRIBUTO_HABILITADO, habilitado));
			}

			Long idSegmento = (Long) filtro.get("idSegmento");
			if(idSegmento != null && idSegmento > 0) {
				criteria.createAlias(SEGMENTO, SEGMENTO);
				criteria.add(Restrictions.eq("segmento.chavePrimaria", idSegmento));
			} else {
				criteria.setFetchMode(SEGMENTO, FetchMode.JOIN);
			}

			Long idPeriodicidade = (Long) filtro.get("idPeriodicidade");
			if(idPeriodicidade != null && idPeriodicidade > 0) {
				criteria.createAlias(PERIODICIDADE, PERIODICIDADE);
				criteria.add(Restrictions.eq("periodicidade.chavePrimaria", idPeriodicidade));
			} else {
				criteria.setFetchMode(PERIODICIDADE, FetchMode.JOIN);
			}
		}
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorRamoAtividade
	 * #validarRemoverRamoAtividade
	 * (java.lang.Long[])
	 */
	@Override
	public void validarRemoverRamoAtividade(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorRamoAtividade
	 * #listarRamoAtividadePorSegmento
	 * (java.lang.Long)
	 */
	@SuppressWarnings({"squid:S1192", "unchecked"})
	@Override
	public Collection<RamoAtividade> listarRamoAtividadePorSegmento(Long chavePrimariaSegmento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where segmento.chavePrimaria = ? ");
		hql.append(" AND habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaSegmento);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel. ControladorRamoAtividade #listarRamoAtividadePorSegmentoImovel (java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<RamoAtividade> listarRamoAtividadePorSegmentoImovel(Long idRamoAtividadePontoConsumo) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where segmento.chavePrimaria = ? ");
		hql.append(" AND habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idRamoAtividadePontoConsumo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorRamoAtividade
	 * #listarAmostragensPCSPorRamoAtividade
	 * (br.com.procenge
	 * .ggas.cadastro.imovel.RamoAtividade)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<EntidadeConteudo> listarAmostragensPCSPorRamoAtividade(RamoAtividade ramoAtividade) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select intervalo.localAmostragemPCS");
		hql.append(FROM);
		hql.append(getClasseEntidadeRamoAtividadeAmostragemPCS().getSimpleName());
		hql.append(" intervalo ");
		hql.append(" where intervalo.ramoAtividade.chavePrimaria = :chaveRamoAtividade ");
		hql.append(" order by intervalo.prioridade ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveRamoAtividade", ramoAtividade.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.
	 * ControladorRamoAtividade
	 * #listarIntervalosPCSPorRamoAtividade
	 * (br.com.procenge
	 * .ggas.cadastro.imovel.RamoAtividade)
	 */
	@SuppressWarnings({"unchecked", "squid:S1192"})
	@Override
	public Collection<IntervaloPCS> listarIntervalosPCSPorRamoAtividade(RamoAtividade ramoAtividade) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeRamoAtividadeIntervaloPCS());

		Long idRamoAtividade = ramoAtividade.getChavePrimaria();
		criteria.add(Restrictions.eq("ramoAtividade.chavePrimaria", idRamoAtividade));
		criteria.createCriteria(INTERVALO_PCS, INTERVALO_PCS, Criteria.INNER_JOIN);

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorRamoAtividade
	 * #consultarRamoAtividadePorSegmentos
	 * (java.lang.Long[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<RamoAtividade> consultarRamoAtividadePorSegmentos(Long[] chavesSegmentos) throws NegocioException {

		Criteria criteria = getCriteria();

		criteria.add(Restrictions.eq(EntidadeConteudo.ATRIBUTO_HABILITADO, Boolean.TRUE));

		if(chavesSegmentos != null && chavesSegmentos.length > 0) {
			criteria.createAlias(SEGMENTO, SEGMENTO);
			criteria.add(Restrictions.in("segmento.chavePrimaria", chavesSegmentos));
		} else {
			criteria.setFetchMode(SEGMENTO, FetchMode.JOIN);
		}
		criteria.addOrder(Order.asc(EntidadeConteudo.ATRIBUTO_DESCRICAO));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorRamoAtividade
	 * #validarDadosRamoAtividade
	 * (br.com.ggas.cadastro.imovel.RamoAtividade)
	 */
	@Override
	public void validarDadosRamoAtividade(RamoAtividade ramoAtividade) throws NegocioException {

		ControladorSegmento controladorSegmento = (ControladorSegmento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);

		Segmento segmento = (Segmento) controladorSegmento.criar();

		if(ramoAtividade.getSegmento() == null) {
			ramoAtividade.setSegmento(segmento);
		}

		this.validarDadosEntidade(ramoAtividade);

		ramoAtividade.setSegmento(null);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorRamoAtividade#validarRamoAtividade(java.lang.Long)
	 */
	@Override
	public void validarRamoAtividade(Long ramoAtividade) throws NegocioException {

		if(ramoAtividade == null || ramoAtividade == -1) {
			throw new NegocioException(ControladorRamoAtividade.ERRO_TIPO_RAMO_ATIVIDADE, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorRamoAtividade#listarRamoAtividadeAmostragemPCSPorRamoAtividade(br.com.ggas.cadastro.imovel
	 * .RamoAtividade)
	 */
	@Override
	public Collection<RamoAtividadeAmostragemPCS> listarRamoAtividadeAmostragemPCSPorRamoAtividade(RamoAtividade ramoAtividade)
					throws GGASException {

		Criteria criteria = createCriteria(getClasseEntidadeRamoAtividadeAmostragemPCS());

		Long idRamoAtividade = ramoAtividade.getChavePrimaria();
		criteria.add(Restrictions.eq("ramoAtividade.chavePrimaria", idRamoAtividade));
		criteria.createCriteria("localAmostragemPCS", "localAmostragemPCS", Criteria.INNER_JOIN);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.ControladorRamoAtividade#listarRamoAtividadeIntervaloPCSPorRamoAtividade(br.com.ggas.cadastro.imovel.
	 * RamoAtividade)
	 */
	@Override
	public Collection<RamoAtividadeIntervaloPCS> listarRamoAtividadeIntervaloPCSPorRamoAtividade(RamoAtividade ramoAtividade)
					throws GGASException {

		Criteria criteria = createCriteria(getClasseEntidadeRamoAtividadeIntervaloPCS());

		Long idRamoAtividade = ramoAtividade.getChavePrimaria();
		criteria.add(Restrictions.eq("ramoAtividade.chavePrimaria", idRamoAtividade));
		criteria.createCriteria(INTERVALO_PCS, INTERVALO_PCS, Criteria.INNER_JOIN);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorRamoAtividade#listarRamoAtividadeAmostragemPCSDisponiveis(java.lang.Long)
	 */
	@Override
	public Collection<EntidadeConteudo> listarRamoAtividadeAmostragemPCSDisponiveis(Long idRamoAtividade) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeConteudo().getSimpleName());
		hql.append(" as enco ");
		hql.append(" inner join fetch enco.entidadeClasse encl ");
		hql.append(" where encl.descricao = 'Local de amostragem do PCS' ");
		hql.append("and enco.chavePrimaria not in(");
		hql.append(" select localAmostragemPCS.chavePrimaria ");
		hql.append(FROM);
		hql.append(getClasseEntidadeRamoAtividadeAmostragemPCS().getSimpleName());
		hql.append(WHERE);
		hql.append(" ramoAtividade.chavePrimaria = ?)");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, idRamoAtividade);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorRamoAtividade#listarRamoAtividadeIntervaloPCSDisponiveis(java.lang.Long)
	 */
	@Override
	public Collection<IntervaloPCS> listarRamoAtividadeIntervaloPCSDisponiveis(Long idRamoAtividade) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseIntervaloPCS().getSimpleName());
		hql.append(" as pcin ");
		hql.append(WHERE);
		hql.append(" pcin.chavePrimaria not in ( ");
		hql.append(" select intervaloPCS.chavePrimaria ");
		hql.append(FROM);
		hql.append(getClasseEntidadeRamoAtividadeIntervaloPCS().getSimpleName());
		hql.append(WHERE);
		hql.append(" ramoAtividade.chavePrimaria = ? ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, idRamoAtividade);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorRamoAtividade#criarRamoAtividadeAmostragemPCS()
	 */
	@Override
	public RamoAtividadeAmostragemPCS criarRamoAtividadeAmostragemPCS() throws NegocioException {

		return (RamoAtividadeAmostragemPCS) ServiceLocator.getInstancia().getBeanPorID(
						RamoAtividadeAmostragemPCS.BEAN_ID_RAMO_ATIVIDADE_AMOSTRAGEM_PCS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorRamoAtividade#criarRamoAtividadeIntervaloPCS()
	 */
	@Override
	public RamoAtividadeIntervaloPCS criarRamoAtividadeIntervaloPCS() throws NegocioException {

		return (RamoAtividadeIntervaloPCS) ServiceLocator.getInstancia().getBeanPorID(
						RamoAtividadeIntervaloPCS.BEAN_ID_RAMO_ATIVIDADE_INTERVALO_PCS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorRamoAtividade#obterIntervaloPCSParaRamoAtividade(java.lang.Long)
	 */
	@Override
	public IntervaloPCS obterIntervaloPCSParaRamoAtividade(Long chavePrimaria) throws NegocioException {

		return (IntervaloPCS) this.obter(chavePrimaria, this.getClasseIntervaloPCS());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorRamoAtividade#removerRamoAtividade(java.util.Collection)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public void removerRamoAtividade(Collection<RamoAtividade> ramosAtividades) throws NegocioException {

		Iterator<RamoAtividade> iterator = ramosAtividades.iterator();

		while(iterator.hasNext()) {

			RamoAtividade ra = (RamoAtividade) this.obter(iterator.next().getChavePrimaria());

			Query query = null;

			StringBuilder hql = new StringBuilder();
			hql.append(DELETE);
			hql.append(FROM);
			hql.append(getClasseEntidadeRamoAtividadeAmostragemPCS().getSimpleName());
			hql.append(WHERE);
			hql.append("ramoAtividade = ? ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong(0, ra.getChavePrimaria());
			query.executeUpdate();

			hql = new StringBuilder();
			hql.append(DELETE);
			hql.append(FROM);
			hql.append(getClasseEntidadeRamoAtividadeIntervaloPCS().getSimpleName());
			hql.append(WHERE);
			hql.append(" ramoAtividade = ? ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong(0, ra.getChavePrimaria());
			query.executeUpdate();

			hql = new StringBuilder();
			hql.append(DELETE);
			hql.append(FROM);
			hql.append(getClasseEntidadeRamoAtividadeSubstituicaoTributaria().getSimpleName());
			hql.append(WHERE);
			hql.append(" ramoAtividade = ? ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong(0, ra.getChavePrimaria());
			query.executeUpdate();

			this.remover(ra);

		}
	}
	
	
	/**
	 * Agrupa os ramos de atividade dos pontos de consumo
	 * 
	 * @param pontosConsumo
	 * @return ramos de atividade distintos dos pontos de consumo
	 */
	@Override
	public Collection<RamoAtividade> agruparRamoAtividadeDePontosConsumo(Collection<PontoConsumo> pontosConsumo) {
		Collection<RamoAtividade> listaRamoAtividade = new ArrayList<>();
		for (PontoConsumo pontoConsumo : pontosConsumo) {
			RamoAtividade ramoAtividade = pontoConsumo.getRamoAtividade();
			if (!listaRamoAtividade.contains(ramoAtividade)) {
				listaRamoAtividade.add(ramoAtividade);
			}
		}
		return listaRamoAtividade;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorRamoAtividade
	 * #listarRamoAtividadePorSegmento
	 * (java.lang.Long)
	 */
	@SuppressWarnings({"squid:S1192", "unchecked"})
	@Override
	public Collection<RamoAtividade> listarRamoAtividade() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where 1 = 1 ");
		hql.append(" AND habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}
	
}
