package br.com.ggas.cadastro.imovel.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.equipamento.Equipamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoEquipamento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pelos métodos relacionados
 * aos equipamentos do ponto de consumo
 * 
 *
 */
public class PontoConsumoEquipamentoImpl extends EntidadeNegocioImpl implements PontoConsumoEquipamento {


	private static final int LIMITE_CAMPO = 2;

	private static final int QUANTIDADE_CASAS_DECIMAIS = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = -5465959443183831606L;
	
	/**
	 * serialVersionUID
	 */
	
	private PontoConsumo pontoConsumo;

	private Equipamento equipamento;

	private BigDecimal potencia;

	private Integer horasPorDia;

	private Integer diasPorSemana;
		
	private Long potenciaFixa;
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;
		
		if(equipamento == null) {
			stringBuilder.append(PONTO_CONSUMO_EQUIPAMENTO_EQ);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		
		} else {
			if (equipamento.getSegmento().getindicadorEquipamentoPotenciaFixa() == null
					|| !equipamento.getSegmento().getindicadorEquipamentoPotenciaFixa()) {
				if(potencia == null) {
					stringBuilder.append(PONTO_CONSUMO_EQUIPAMENTO_POTENCIA);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
	
				if (horasPorDia == null) {
					stringBuilder.append(PONTO_CONSUMO_EQUIPAMENTO_HORAS_DIA);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				} 
				
				if (diasPorSemana == null) {
					stringBuilder.append(PONTO_CONSUMO_EQUIPAMENTO_DIAS_SEMANA);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			} else {
				if(potenciaFixa== null) {
					stringBuilder.append(PONTO_CONSUMO_EQUIPAMENTO_POTENCIA_FIXA);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}
	
	@Override
	public PontoConsumo getPontoConsumo() {
		return pontoConsumo;
	}

	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}

	@Override
	public Equipamento getEquipamento() {
		return equipamento;
	}

	@Override
	public void setEquipamento(Equipamento equipamento) {
		this.equipamento = equipamento;
	}

	@Override
	public BigDecimal getPotencia() {
		return potencia;
	}

	@Override
	public void setPotencia(BigDecimal potencia) {
		this.potencia = potencia;
	}

	@Override
	public Integer getHorasPorDia() {
		return horasPorDia;
	}

	@Override
	public void setHorasPorDia(Integer horasPorDia) {
		this.horasPorDia = horasPorDia;
	}

	@Override
	public Integer getDiasPorSemana() {
		return diasPorSemana;
	}

	@Override
	public void setDiasPorSemana(Integer diasPorSemana) {
		this.diasPorSemana = diasPorSemana;
	}

	/**
	 * @return the potenciaFixa
	 */
	@Override
	public Long getPotenciaFixa() {
		return potenciaFixa;
	}

	/**
	 * @param potenciaFixa the potenciaFixa to set
	 */
	@Override
	public void setPotenciaFixa(Long potenciaFixa) {
		this.potenciaFixa = potenciaFixa;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.equipamento.Equipamento#getPotenciaPadraoFormatada()
	 */
	@Override
	public String getPotenciaFormatada() {
		String retorno = "";
		if (this.potencia != null) {
			retorno = Util.converterCampoValorDecimalParaString(PontoConsumoEquipamento.ATRIBUTO_POTENCIA, this.potencia,
					Constantes.LOCALE_PADRAO, QUANTIDADE_CASAS_DECIMAIS);
		}
		return retorno;
	}
		
}
