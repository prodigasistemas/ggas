/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.imovel.PontoConsumoEquipamento;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.faturamento.fatura.DadosResumoPontoConsumo;
import br.com.ggas.faturamento.fatura.impl.DadosResumoPontoConsumoImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável pela representação de um ponto de consumo
 *
 */
public class PontoConsumoImpl extends EntidadeNegocioImpl implements PontoConsumo {

	private static final String ROTULO_CEP = "CEP";

	private static final long serialVersionUID = 1070503960254707509L;

	private Imovel imovel;

	private SituacaoConsumo situacaoConsumo;

	private Segmento segmento;

	private RamoAtividade ramoAtividade;

	private EntidadeConteudo modalidadeUso;

	private String enderecoReferencia;

	private Cep cep;

	private String descricao;

	private Integer numeroSequenciaLeitura;

	private String numeroImovel;

	private String descricaoComplemento;

	private BigDecimal longitudeGrau;

	private BigDecimal latitudeGrau;

	private String codigoLegado;

	private Rota rota;

	private QuadraFace quadraFace;

	private InstalacaoMedidor instalacaoMedidor;

	private Collection<ComentarioPontoConsumo> comentarios = new HashSet<>();

	private Collection<PontoConsumoCityGate> listaPontoConsumoCityGate = new HashSet<>();

	private Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota = new HashSet<>();
	
	private Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento = new HashSet<>();

	private Collection<HistoricoConsumo> listaConsumoHistorico = new HashSet<>();

	private Collection<HistoricoMedicao> listaMedicaoHistorico = new HashSet<>();
	
	private String codigoPontoConsumoSupervisorio;
	
	private DadosResumoPontoConsumo dadosResumoPontoConsumo = new DadosResumoPontoConsumoImpl();
	
	private Boolean indicadorClassificacaoFiscal;
	
	private List<PontoConsumo> listaPontosConsumoImoveisFilho;

	private String observacao;
	
	private String descricaoMensagemAdicional;

	private boolean indicadorMensagemAdicional;
	
	private Double distanciaEntrePontos;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getCodigoLegado()
	 */
	@Override
	public String getCodigoLegado() {

		return codigoLegado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #setCodigoLegado(Long)
	 */
	@Override
	public void setCodigoLegado(String codigoLegado) {

		this.codigoLegado = codigoLegado;
	}


	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getRota()
	 */
	@Override
	public Rota getRota() {

		return rota;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #setRota(br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public void setRota(Rota rota) {

		this.rota = rota;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #getImovel()
	 */
	@Override
	public Imovel getImovel() {

		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #
	 * setImovel(br.com.ggas.cadastro.imovel.Imovel
	 * )
	 */
	@Override
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #getSituacaoConsumo()
	 */
	@Override
	public SituacaoConsumo getSituacaoConsumo() {

		return situacaoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #
	 * setSituacaoConsumo(br.com.ggas.cadastro.imovel
	 * .SituacaoConsumo)
	 */
	@Override
	public void setSituacaoConsumo(SituacaoConsumo situacaoConsumo) {

		this.situacaoConsumo = situacaoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #getNumeroSequenciaLeitura()
	 */
	@Override
	public Integer getNumeroSequenciaLeitura() {

		return numeroSequenciaLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #
	 * setNumeroSequenciaLeitura(java.lang.Integer
	 * )
	 */
	@Override
	public void setNumeroSequenciaLeitura(Integer numeroSequenciaLeitura) {

		this.numeroSequenciaLeitura = numeroSequenciaLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getModalidadeUso()
	 */
	@Override
	public EntidadeConteudo getModalidadeUso() {

		return modalidadeUso;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #setModalidadeUso
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setModalidadeUso(EntidadeConteudo modalidadeUso) {

		this.modalidadeUso = modalidadeUso;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #getCep()
	 */
	@Override
	public Cep getCep() {

		return cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #setCep(br.com.ggas.cadastro.endereco.Cep)
	 */
	@Override
	public void setCep(Cep cep) {

		this.cep = cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #getNumeroImovel()
	 */
	@Override
	public String getNumeroImovel() {

		return numeroImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #setNumeroImovel(java.lang.String)
	 */
	@Override
	public void setNumeroImovel(String numeroImovel) {

		this.numeroImovel = numeroImovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #getDescricaoComplemento()
	 */
	@Override
	public String getDescricaoComplemento() {

		return descricaoComplemento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #setDescricaoComplemento(java.lang.String)
	 */
	@Override
	public void setDescricaoComplemento(String descricaoComplemento) {

		this.descricaoComplemento = descricaoComplemento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #getEnderecoReferencia()
	 */
	@Override
	public String getEnderecoReferencia() {

		return enderecoReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.PontoConsumo
	 * #setEnderecoReferencia(java.lang.String)
	 */
	@Override
	public void setEnderecoReferencia(String enderecoReferencia) {

		this.enderecoReferencia = enderecoReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getSegmento()
	 */
	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #setSegmento
	 * (br.com.ggas.cadastro.imovel.Segmento)
	 */
	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getRamoAtividade()
	 */
	@Override
	public RamoAtividade getRamoAtividade() {

		return ramoAtividade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #setRamoAtividade
	 * (br.com.ggas.cadastro.imovel.RamoAtividade)
	 */
	@Override
	public void setRamoAtividade(RamoAtividade ramoAtividade) {

		this.ramoAtividade = ramoAtividade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getInstalacaoMedidor()
	 */
	@Override
	public InstalacaoMedidor getInstalacaoMedidor() {

		return instalacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #setInstalacaoMedidor
	 * (br.com.ggas.medicao.leitura
	 * .InstalacaoMedidor)
	 */
	@Override
	public void setInstalacaoMedidor(InstalacaoMedidor instalacaoMedidor) {

		this.instalacaoMedidor = instalacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getDescricaoFormatada()
	 */
	@Override
	public String getDescricaoFormatada() {

		StringBuilder descricaoFormatada = new StringBuilder();
		String separador = "";
		if (!StringUtils.isEmpty(getImovel().getNome())) {
			descricaoFormatada.append(getImovel().getNome());
			separador = " - ";
		}
		
		if (!StringUtils.isEmpty(getDescricao())) {
			descricaoFormatada.append(separador);
			descricaoFormatada.append(getDescricao());
			separador = " - ";
		}

		Cep cepEndereco = getCep();
		if (cepEndereco == null) {
			cepEndereco = getImovel().getQuadraFace().getEndereco().getCep();
		}
		
		if (!StringUtils.isEmpty(cepEndereco.getTipoLogradouro())) {
			descricaoFormatada.append(separador);
			descricaoFormatada.append(cepEndereco.getTipoLogradouro());
			if (StringUtils.isEmpty(cepEndereco.getLogradouro())) {
				separador = ", ";
			} else {
				separador = "";
			}
		}
		
		if (!StringUtils.isEmpty(cepEndereco.getLogradouro())) {
			descricaoFormatada.append(separador);
			descricaoFormatada.append(cepEndereco.getLogradouro());
			separador = ", ";
		}

		if (!StringUtils.isEmpty(getNumeroImovel())) {
			descricaoFormatada.append(separador);
			descricaoFormatada.append(getNumeroImovel());
			separador = ", ";
		} else {
			descricaoFormatada.append(separador);
			descricaoFormatada.append(getImovel().getNumeroImovel());
			separador = ", ";
		}
		
		if (!StringUtils.isEmpty(getDescricaoComplemento())) {
			descricaoFormatada.append(separador);
			descricaoFormatada.append(getDescricaoComplemento());
			separador = ", ";
		}
		
		if (!StringUtils.isEmpty(cepEndereco.getBairro())) {
			descricaoFormatada.append(separador);
			descricaoFormatada.append(cepEndereco.getBairro());
		}

		return descricaoFormatada.toString();
	}

	@Override
	public String getEnderecoFormatado() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";
		Cep cepLocal = null;
		Endereco endereco = null;
		if (this.getQuadraFace() != null) {
			endereco = this.getQuadraFace().getEndereco();
			if (endereco != null) {
				cepLocal = endereco.getCep();
				if (cepLocal != null) {
					if (cepLocal.getTipoLogradouro() != null) {
						enderecoFormatado.append(cepLocal.getTipoLogradouro());
					}
					if (cepLocal.getLogradouro() != null) {
						if (enderecoFormatado.length() > 0) {
							separador = ", ";
						} else {
							separador = "";
						}
						enderecoFormatado.append(separador);
						enderecoFormatado.append(cepLocal.getLogradouro());
					}
					if (this.numeroImovel != null) {
						if (enderecoFormatado.length() > 0) {
							separador = ", ";
						} else {
							separador = "";
						}
						enderecoFormatado.append(separador);
						enderecoFormatado.append(this.numeroImovel);
					}
					if (this.descricaoComplemento != null) {
						enderecoFormatado.append(separador);
						enderecoFormatado.append(this.descricaoComplemento);
					}
					if (endereco.getComplemento() != null) {
						if (enderecoFormatado.length() > 0) {
							separador = ", ";
						} else {
							separador = "";
						}
						enderecoFormatado.append(separador);
						enderecoFormatado.append(endereco.getComplemento());
					}
					if (cepLocal.getBairro() != null) {
						if (enderecoFormatado.length() > 0) {
							separador = ", ";
						} else {
							separador = "";
						}
						enderecoFormatado.append(separador);
						enderecoFormatado.append(cepLocal.getBairro());
					}
					if (cepLocal.getMunicipio() != null) {
						if (enderecoFormatado.length() > 0) {
							separador = ", ";
						} else {
							separador = "";
						}
						enderecoFormatado.append(separador);
						enderecoFormatado.append(cepLocal.getNomeMunicipio());
					}
					if (cepLocal.getUf() != null) {
						if (enderecoFormatado.length() > 0) {
							separador = ", ";
						} else {
							separador = "";
						}
						enderecoFormatado.append(separador);
						enderecoFormatado.append(cepLocal.getUf());
					}
				}
			}

		}

		return enderecoFormatado.toString();
	}

	@Override
	public String getLogradouroFormatado() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";
		Cep cepLocal = null;
		Endereco endereco = null;
		if(this.getQuadraFace() != null) {
			endereco = this.getQuadraFace().getEndereco();
			if(endereco != null) {
				cepLocal = endereco.getCep();
				if(cepLocal != null) {
					if(cepLocal.getTipoLogradouro() != null) {
						enderecoFormatado.append(cepLocal.getTipoLogradouro());
					}
					
					if (cepLocal.getLogradouro() != null) {
						if (enderecoFormatado.length() > 0) {
							separador = " - ";
						} else {
							separador = "";
						}
						
						enderecoFormatado.append(separador);
						enderecoFormatado.append(cepLocal.getLogradouro());
					}
					
					if (this.numeroImovel != null) {
						
						if (enderecoFormatado.length() > 0) {
							separador = ", ";
						} else {
							separador = "";
						}
						
						enderecoFormatado.append(separador);
						enderecoFormatado.append(this.numeroImovel);
					}
					
					if(this.descricaoComplemento != null) {
						enderecoFormatado.append(separador);
						enderecoFormatado.append(this.descricaoComplemento);
					}
					
					if (endereco.getComplemento() != null) {
						
						if (enderecoFormatado.length() > 0) {
							separador = ", ";
						} else {
							separador = "";
						}
						
						enderecoFormatado.append(separador);
						enderecoFormatado.append(endereco.getComplemento());
					}
				}
			}

		}
		return enderecoFormatado.toString();
	}

	@Override
	public String getEnderecoFormatadoRuaNumeroComplemento() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";
		Endereco endereco = null;

		String logradouro = this.getEnderecoLogradouro();

		if (logradouro != null && !logradouro.isEmpty()) {
			enderecoFormatado.append(logradouro);
		}

		if (this.numeroImovel != null) {
			if (enderecoFormatado.length() > 0) {
				separador = ", ";
			} else {
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.numeroImovel);
		}
		
		if (this.getQuadraFace() != null) {
			endereco = this.getQuadraFace().getEndereco();
			if (endereco != null && endereco.getComplemento() != null) {
				if (enderecoFormatado.length() > 0) {
					separador = ", ";
				} else {
					separador = "";
				}
				enderecoFormatado.append(separador);
				enderecoFormatado.append(endereco.getComplemento());
			}
		}

		return enderecoFormatado.toString();
	}

	@Override
	public String getEnderecoFormatadoBairro() {

		StringBuilder enderecoFormatado = new StringBuilder();
		if(this.getCep().getBairro() != null) {
			enderecoFormatado.append(this.getCep().getBairro());
		}

		return enderecoFormatado.toString();
	}

	@Override
	public String getEnderecoFormatadoMunicipioUFCEP() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		if(this.getCep().getNomeMunicipio() != null) {
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getNomeMunicipio());
		} else if(this.getCep().getMunicipio() != null) {
			enderecoFormatado.append(this.getCep().getMunicipio().getDescricao());
		}

		if (this.getCep().getUf() != null) {
			if (enderecoFormatado.length() > 0) {
				separador = " - ";
			} else {
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getUf());
		}
		if (this.getCep().getCep() != null) {
			if (enderecoFormatado.length() > 0) {
				separador = " CEP ";
			} else {
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getCep());
		}

		return enderecoFormatado.toString();
	}

	@Override
	public String getEnderecoLogradouro() {

		final StringBuilder enderecoLogradouro = new StringBuilder();
		if(this.getCep() != null && this.getCep().getTipoLogradouro() != null) {
			enderecoLogradouro.append(this.getCep().getTipoLogradouro());
		}
		if (this.getCep() != null && this.getCep().getLogradouro() != null) {
			final String separador;
			if (enderecoLogradouro.length() > 0) {
				separador = " - ";
			} else {
				separador = "";
			}
			enderecoLogradouro.append(separador);
			enderecoLogradouro.append(this.getCep().getLogradouro());
		}

		return enderecoLogradouro.toString();
	}

	@Override
	public String getEnderecoFormatadoMunicipioUF() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		if (this.getCep().getNomeMunicipio() != null) {
			enderecoFormatado.append(this.getCep().getNomeMunicipio());

		} else if (this.getCep().getMunicipio() != null) {
			enderecoFormatado.append(this.getCep().getMunicipio().getDescricao());
		}

		if (this.getCep().getUf() != null) {
			
			if (enderecoFormatado.length() > 0) {
				separador = " - ";
			} else {
				separador = "";
			}
			
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getUf());
		}

		return enderecoFormatado.toString();
	}

	/**
	 * @return the comentarios
	 */
	@Override
	public Collection<ComentarioPontoConsumo> getComentarios() {

		return comentarios;
	}

	/**
	 * @param comentarios
	 *            the comentarios to set
	 */
	@Override
	public void setComentarios(Collection<ComentarioPontoConsumo> comentarios) {

		this.comentarios = comentarios;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getListaPontoConsumoCityGate()
	 */
	@Override
	public Collection<PontoConsumoCityGate> getListaPontoConsumoCityGate() {

		return listaPontoConsumoCityGate;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #setListaPontoConsumoCityGate
	 * (java.util.Collection)
	 */
	@Override
	public void setListaPontoConsumoCityGate(Collection<PontoConsumoCityGate> listaPontoConsumoCityGate) {

		this.listaPontoConsumoCityGate = listaPontoConsumoCityGate;
	}

	/**
	 * @return
	 */
	@Override
	public Collection<PontoConsumoTributoAliquota> getListaPontoConsumoTributoAliquota() {

		return listaPontoConsumoTributoAliquota;
	}

	/**
	 * @param listaPontoConsumoTributoAliquota
	 */
	@Override
	public void setListaPontoConsumoTributoAliquota(Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquota) {

		this.listaPontoConsumoTributoAliquota = listaPontoConsumoTributoAliquota;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getQuadraFace()
	 */
	@Override
	public QuadraFace getQuadraFace() {

		return quadraFace;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #setQuadraFace
	 * (br.com.ggas.cadastro.localidade
	 * .QuadraFace)
	 */
	@Override
	public void setQuadraFace(QuadraFace quadraFace) {

		this.quadraFace = quadraFace;
	}

	/**
	 * @return the codigoPontoConsumoSupervisorio
	 */
	@Override
	public String getCodigoPontoConsumoSupervisorio() {

		return codigoPontoConsumoSupervisorio;
	}

	/**
	 * @param codigoPontoConsumoSupervisorio
	 *            the codigoPontoConsumoSupervisorio to set
	 */
	@Override
	public void setCodigoPontoConsumoSupervisorio(String codigoPontoConsumoSupervisorio) {

		this.codigoPontoConsumoSupervisorio = codigoPontoConsumoSupervisorio;
	}

	@Override
	public BigDecimal getLongitudeGrau() {

		return longitudeGrau;
	}

	@Override
	public void setLongitudeGrau(BigDecimal longitudeGrau) {

		this.longitudeGrau = longitudeGrau;
	}

	@Override
	public BigDecimal getLatitudeGrau() {

		return latitudeGrau;
	}

	@Override
	public void setLatitudeGrau(BigDecimal latitudeGrau) {

		this.latitudeGrau = latitudeGrau;
	}

	@Override
	public String getCodigoPontoConsumo() {

		if(this.codigoLegado != null) {
			return this.codigoLegado;
		} else {
			return String.valueOf(getChavePrimaria());
		}
	}

	@Override
	public Collection<HistoricoConsumo> getListaConsumoHistorico() {

		return listaConsumoHistorico;
	}

	@Override
	public void setListaConsumoHistorico(Collection<HistoricoConsumo> listaConsumoHistorico) {

		this.listaConsumoHistorico = listaConsumoHistorico;
	}

	@Override
	public Collection<HistoricoMedicao> getListaMedicaoHistorico() {

		return listaMedicaoHistorico;
	}

	@Override
	public void setListaMedicaoHistorico(Collection<HistoricoMedicao> listaMedicaoHistorico) {

		this.listaMedicaoHistorico = listaMedicaoHistorico;
	}

	@Override
	public Boolean getHabilitado() {

		return this.isHabilitado();
	}

	@Override
	public String getEnderecoFormatadoCepMunicipioUF() {

		StringBuilder enderecoFormatado = new StringBuilder();
		String separador = "";

		if(this.getCep().getCep() != null) {
			enderecoFormatado.append("CEP: ");
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getCep());
		}

		if(this.getCep().getMunicipio() != null) {
			enderecoFormatado.append(" - ");
			enderecoFormatado.append(this.getCep().getMunicipio().getDescricao());
		}

		if (this.getCep().getUf() != null) {
			if (enderecoFormatado.length() > 0) {
				separador = " - ";
			} else {
				separador = "";
			}
			enderecoFormatado.append(separador);
			enderecoFormatado.append(this.getCep().getUf());
		}

		return enderecoFormatado.toString();
	}

	@Override
	public String getEnderecoFormatadoRelatorioCorteSergas() {
		Cep cepPontoConsumo = this.getCep();
		String strNumeroImovel = this.getNumeroImovel();
		String bairro = cepPontoConsumo.getBairro();
		String nomeMunicipio = cepPontoConsumo.getNomeMunicipio();
		String uf = cepPontoConsumo.getUf();
		
		String strLogradouro = Util.montarStringSemNulos(Constantes.STRING_ESPACO, 
						cepPontoConsumo.getTipoLogradouro(), cepPontoConsumo.getLogradouro());
		
		String strNumeroCep = Util.montarStringSemNulos(Constantes.STRING_DOIS_PONTOS_ESPACO, 
						ROTULO_CEP, cepPontoConsumo.getCep());
		
		return Util.montarStringSemNulos(
						Constantes.STRING_VIRGULA_ESPACO, strLogradouro, 
						strNumeroImovel,bairro,nomeMunicipio,uf, strNumeroCep);
	}
	
	/**
	 * @return
	 */
	@Override
	public DadosResumoPontoConsumo getDadosResumoPontoConsumo() {

		return dadosResumoPontoConsumo;
	}

	/**
	 * @param dadosResumoPontoConsumo
	 */
	@Override
	public void setDadosResumoPontoConsumo(DadosResumoPontoConsumo dadosResumoPontoConsumo) {

		this.dadosResumoPontoConsumo = dadosResumoPontoConsumo;
	}
	
	/**
	 * @return indicadorClassificacaoFiscal
	 */
	@Override
	public Boolean getIndicadorClassificacaoFiscal() {

		return indicadorClassificacaoFiscal;
	}

	/**
	 * @param indicadorClassificacaoFiscal
	 */
	@Override
	public void setIndicadorClassificacaoFiscal(Boolean indicadorClassificacaoFiscal) {

		this.indicadorClassificacaoFiscal = indicadorClassificacaoFiscal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getListaPontoConsumoEquipamento()
	 */
	public Collection<PontoConsumoEquipamento> getListaPontoConsumoEquipamento() {
		return listaPontoConsumoEquipamento;
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getListaPontoConsumoEquipamento()
	 */
	public void setListaPontoConsumoEquipamento(Collection<PontoConsumoEquipamento> listaPontoConsumoEquipamento) {
		this.listaPontoConsumoEquipamento = listaPontoConsumoEquipamento;
	}
	
	public boolean temGrupoFaturamento() {
		return this.rota != null && this.rota.getGrupoFaturamento() != null;
	}

	/**
	 * Lista para exibicao agrupada de pontos de consumo de imoveis
	 * @return lista agrupada para exibicao
	 */
	@Override
	public List<PontoConsumo> getListaPontosConsumoImoveisFilho() {
		return listaPontosConsumoImoveisFilho;
	}

	/**
	 * Lista para exibicao agrupada de pontos de consumo de imoveis
	 * @param listaPontosConsumoImoveisFilho Lista agrupada
	 */
	@Override
	public void setListaPontosConsumoImoveisFilho(List<PontoConsumo>  listaPontosConsumoImoveisFilho) {
		this.listaPontosConsumoImoveisFilho = listaPontosConsumoImoveisFilho;
	}

	@Override
	public String getObservacao() {
		return observacao;
	}

	@Override
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	
	@Override
	public boolean isIndicadorMensagemAdicional() {
		return indicadorMensagemAdicional;
	}

	@Override
	public void setIndicadorMensagemAdicional(boolean indicadorMensagemAdicional) {
		this.indicadorMensagemAdicional = indicadorMensagemAdicional	;	
	}

	@Override
	public String getDescricaoMensagemAdicional() {
		return descricaoMensagemAdicional;
	}

	@Override
	public void setDescricaoMensagemAdicional(String descricaoMensagemAdicional) {
		this.descricaoMensagemAdicional = descricaoMensagemAdicional;	
	}

	@Override
	public Double getDistanciaEntrePontos() {
		return distanciaEntrePontos;
	}

	@Override
	public void setDistanciaEntrePontos(Double distanciaEntrePontos) {
		this.distanciaEntrePontos = distanciaEntrePontos;
	}

}
