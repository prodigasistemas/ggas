package br.com.ggas.cadastro.imovel.impl;

import br.com.ggas.cadastro.imovel.PontoConsumo;

/**
 * Classe responsável pela representação de valores de um Imóvel.
 * 
 * @author esantana
 *
 */
public class ImovelVO {

	private Long chavePrimaria;

	private Boolean veioDoChamado;

	private Integer versao;

	private String nome;

	private String chaveCep;

	private String cepImovel;
	
	private String idCepImovel;

	private String numeroImovel;

	private String complementoImovel;

	private String enderecoReferencia;

	private String dataPrevisaoEncerramentoObra;

	private Boolean indicadorAlterarImovelCondominioFilho;

	private String habilitado = "true";

	private Long idQuadraImovel;

	private Long idQuadraFace;

	private String numeroLote;

	private String numeroSublote;

	private String testadaLote;

	private Long idSituacao;

	private Long idRotaPrevista;

	private Long idAgente;

	private String codigoSitucaoImovel;

	private String descricaoPontoConsumo;

	private Integer indexLista = -1;

	private Integer indexListaPontoConsumoTributo;

	private Long idQuadraFacePontoConsumo;

	private String[] percentualCityGate;

	private String dataInicioVigenciaCityGate;

	private Long idPavimentoCalcada;

	private Long idPavimentoRua;

	private Long idPadraoConstrucao;

	private Long idAreaConstruidaFaixa;

	private Long idPerfilImovel;

	private String condominio;

	private Integer codigoModalidadeMedicao;

	private Long idTipoCombustivel;

	private Long idTipoBotijao;

	private String valvulaBloqueio;

	private String dataEntrega;

	private Long idEmpresa;

	private String portaria;

	private String redePreexistente;

	private String quantidadeBloco;

	private String quantidadeAndar;

	private String quantidadeBanheiro;

	private String quantidadeApartamentoAndar;

	private String indicadorObraTubulacao;

	private Long idRedeMaterial;

	private String quantidadePrumada;

	private Long idRedeDiametroCentral;

	private Long idRedeDiametroPrumada;

	private Long idRedeDiametroPrumadaApartamento;

	private String quantidadeReguladorHall;

	private String ventilacaoHall;

	private String ventilacaoApartamento;

	private Long[] idsServicoTipoRestricao;

	private String indicadorRestricaoServico;

	private String quadraFaceImovel;

	private String descricaoSituacaoImovel;

	private String descricaoPavimentoCalcadaImovel;

	private String descricaoPavimentoRuaImovel;

	private String descricaoPadraoConstrucaoImovel;

	private String faixaAreaConstruidaImovel;

	private String descricaoPerfilImovel;

	private String descricaoModalidadeMedicaoImovel;

	private String descricaoTipoBotijaoImovel;

	private String descricaoTipoCombustivelImovel;

	private String nomeConstrutora;

	private String descricaoRedeMaterial;

	private String diametroRedeTrechoPrumada;

	private String diametroRedeNasPrumada;

	private String diametroRedeSecundaria;

	private String descricaoRotaPrevista;

	private String nomeAgente;

	private Long idModalidadeUso; 

	private Long[] chavesPrimarias;

	private String indicadorCondominio;

	private String imovelPai;

	private String nomeFantasiaImovelPai;

	private String matriculaImovelPai;

	private String numeroImovelPai;

	private String cidadeImovelPai;

	private String condominioPai;

	private String indicadorCondominioImovelPai;

	private Long idImovelCondominio;
	
	private PontoConsumo pontoConsumo;

	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	public Boolean getVeioDoChamado() {
		return veioDoChamado;
	}

	public void setVeioDoChamado(Boolean veioDoChamado) {
		this.veioDoChamado = veioDoChamado;
	}

	public Integer getVersao() {
		return versao;
	}

	public void setVersao(Integer versao) {
		this.versao = versao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getChaveCep() {
		return chaveCep;
	}

	public void setChaveCep(String chaveCep) {
		this.chaveCep = chaveCep;
	}

	public String getCepImovel() {
		return cepImovel;
	}

	public void setCepImovel(String cepImovel) {
		this.cepImovel = cepImovel;
	}

	public String getNumeroImovel() {
		return numeroImovel;
	}

	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}

	public String getComplementoImovel() {
		return complementoImovel;
	}

	public void setComplementoImovel(String complementoImovel) {
		this.complementoImovel = complementoImovel;
	}

	public String getEnderecoReferencia() {
		return enderecoReferencia;
	}

	public void setEnderecoReferencia(String enderecoReferencia) {
		this.enderecoReferencia = enderecoReferencia;
	}

	public String getDataPrevisaoEncerramentoObra() {
		return dataPrevisaoEncerramentoObra;
	}

	public void setDataPrevisaoEncerramentoObra(String dataPrevisaoEncerramentoObra) {
		this.dataPrevisaoEncerramentoObra = dataPrevisaoEncerramentoObra;
	}

	public Boolean getIndicadorAlterarImovelCondominioFilho() {
		return indicadorAlterarImovelCondominioFilho;
	}

	public void setIndicadorAlterarImovelCondominioFilho(Boolean indicadorAlterarImovelCondominioFilho) {
		this.indicadorAlterarImovelCondominioFilho = indicadorAlterarImovelCondominioFilho;
	}

	public String getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}

	public Long getIdQuadraImovel() {
		return idQuadraImovel;
	}

	public void setIdQuadraImovel(Long idQuadraImovel) {
		this.idQuadraImovel = idQuadraImovel;
	}

	public Long getIdQuadraFace() {
		return idQuadraFace;
	}

	public void setIdQuadraFace(Long idQuadraFace) {
		this.idQuadraFace = idQuadraFace;
	}

	public String getNumeroLote() {
		return numeroLote;
	}

	public void setNumeroLote(String numeroLote) {
		this.numeroLote = numeroLote;
	}

	public String getNumeroSublote() {
		return numeroSublote;
	}

	public void setNumeroSublote(String numeroSublote) {
		this.numeroSublote = numeroSublote;
	}

	public String getTestadaLote() {
		return testadaLote;
	}

	public void setTestadaLote(String testadaLote) {
		this.testadaLote = testadaLote;
	}

	public Long getIdSituacao() {
		return idSituacao;
	}

	public void setIdSituacao(Long idSituacao) {
		this.idSituacao = idSituacao;
	}

	public Long getIdRotaPrevista() {
		return idRotaPrevista;
	}

	public void setIdRotaPrevista(Long idRotaPrevista) {
		this.idRotaPrevista = idRotaPrevista;
	}

	public Long getIdAgente() {
		return idAgente;
	}

	public void setIdAgente(Long idAgente) {
		this.idAgente = idAgente;
	}

	public String getCodigoSitucaoImovel() {
		return codigoSitucaoImovel;
	}

	public void setCodigoSitucaoImovel(String codigoSitucaoImovel) {
		this.codigoSitucaoImovel = codigoSitucaoImovel;
	}

	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public Integer getIndexLista() {
		return indexLista;
	}

	public void setIndexLista(Integer indexLista) {
		this.indexLista = indexLista;
	}

	public Integer getIndexListaPontoConsumoTributo() {
		return indexListaPontoConsumoTributo;
	}

	public void setIndexListaPontoConsumoTributo(Integer indexListaPontoConsumoTributo) {
		this.indexListaPontoConsumoTributo = indexListaPontoConsumoTributo;
	}

	public Long getIdQuadraFacePontoConsumo() {
		return idQuadraFacePontoConsumo;
	}

	public void setIdQuadraFacePontoConsumo(Long idQuadraFacePontoConsumo) {
		this.idQuadraFacePontoConsumo = idQuadraFacePontoConsumo;
	}

	public String[] getPercentualCityGate() {
		String[] retorno = null;
		if(this.percentualCityGate != null) {
			retorno = this.percentualCityGate.clone();
		}
		return retorno;
	}

	public void setPercentualCityGate(String[] percentualCityGate) {
		if(percentualCityGate != null) {
			this.percentualCityGate = percentualCityGate.clone();
		}

	}

	public String getDataInicioVigenciaCityGate() {
		return dataInicioVigenciaCityGate;
	}

	public void setDataInicioVigenciaCityGate(String dataInicioVigenciaCityGate) {
		this.dataInicioVigenciaCityGate = dataInicioVigenciaCityGate;
	}

	public Long getIdPavimentoCalcada() {
		return idPavimentoCalcada;
	}

	public void setIdPavimentoCalcada(Long idPavimentoCalcada) {
		this.idPavimentoCalcada = idPavimentoCalcada;
	}

	public Long getIdPavimentoRua() {
		return idPavimentoRua;
	}

	public void setIdPavimentoRua(Long idPavimentoRua) {
		this.idPavimentoRua = idPavimentoRua;
	}

	public Long getIdPadraoConstrucao() {
		return idPadraoConstrucao;
	}

	public void setIdPadraoConstrucao(Long idPadraoConstrucao) {
		this.idPadraoConstrucao = idPadraoConstrucao;
	}

	public Long getIdAreaConstruidaFaixa() {
		return idAreaConstruidaFaixa;
	}

	public void setIdAreaConstruidaFaixa(Long idAreaConstruidaFaixa) {
		this.idAreaConstruidaFaixa = idAreaConstruidaFaixa;
	}

	public Long getIdPerfilImovel() {
		return idPerfilImovel;
	}

	public void setIdPerfilImovel(Long idPerfilImovel) {
		this.idPerfilImovel = idPerfilImovel;
	}

	public String getCondominio() {
		return condominio;
	}

	public void setCondominio(String condominio) {
		this.condominio = condominio;
	}

	public Integer getCodigoModalidadeMedicao() {
		return codigoModalidadeMedicao;
	}

	public void setCodigoModalidadeMedicao(Integer codigoModalidadeMedicao) {
		this.codigoModalidadeMedicao = codigoModalidadeMedicao;
	}

	public Long getIdTipoCombustivel() {
		return idTipoCombustivel;
	}

	public void setIdTipoCombustivel(Long idTipoCombustivel) {
		this.idTipoCombustivel = idTipoCombustivel;
	}

	public Long getIdTipoBotijao() {
		return idTipoBotijao;
	}

	public void setIdTipoBotijao(Long idTipoBotijao) {
		this.idTipoBotijao = idTipoBotijao;
	}

	public String getValvulaBloqueio() {
		return valvulaBloqueio;
	}

	public void setValvulaBloqueio(String valvulaBloqueio) {
		this.valvulaBloqueio = valvulaBloqueio;
	}

	public String getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(String dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getPortaria() {
		return portaria;
	}

	public void setPortaria(String portaria) {
		this.portaria = portaria;
	}

	public String getRedePreexistente() {
		return redePreexistente;
	}

	public void setRedePreexistente(String redePreexistente) {
		this.redePreexistente = redePreexistente;
	}

	public String getQuantidadeBloco() {
		return quantidadeBloco;
	}

	public void setQuantidadeBloco(String quantidadeBloco) {
		this.quantidadeBloco = quantidadeBloco;
	}

	public String getQuantidadeAndar() {
		return quantidadeAndar;
	}

	public void setQuantidadeAndar(String quantidadeAndar) {
		this.quantidadeAndar = quantidadeAndar;
	}

	public String getQuantidadeBanheiro() {
		return quantidadeBanheiro;
	}

	public void setQuantidadeBanheiro(String quantidadeBanheiro) {
		this.quantidadeBanheiro = quantidadeBanheiro;
	}

	public String getQuantidadeApartamentoAndar() {
		return quantidadeApartamentoAndar;
	}

	public void setQuantidadeApartamentoAndar(String quantidadeApartamentoAndar) {
		this.quantidadeApartamentoAndar = quantidadeApartamentoAndar;
	}

	public String getIndicadorObraTubulacao() {
		return indicadorObraTubulacao;
	}

	public void setIndicadorObraTubulacao(String indicadorObraTubulacao) {
		this.indicadorObraTubulacao = indicadorObraTubulacao;
	}

	public Long getIdRedeMaterial() {
		return idRedeMaterial;
	}

	public void setIdRedeMaterial(Long idRedeMaterial) {
		this.idRedeMaterial = idRedeMaterial;
	}

	public String getQuantidadePrumada() {
		return quantidadePrumada;
	}

	public void setQuantidadePrumada(String quantidadePrumada) {
		this.quantidadePrumada = quantidadePrumada;
	}

	public Long getIdRedeDiametroCentral() {
		return idRedeDiametroCentral;
	}

	public void setIdRedeDiametroCentral(Long idRedeDiametroCentral) {
		this.idRedeDiametroCentral = idRedeDiametroCentral;
	}

	public Long getIdRedeDiametroPrumadaApartamento() {
		return idRedeDiametroPrumadaApartamento;
	}

	public void setIdRedeDiametroPrumadaApartamento(Long idRedeDiametroPrumadaApartamento) {
		this.idRedeDiametroPrumadaApartamento = idRedeDiametroPrumadaApartamento;
	}

	public Long getIdRedeDiametroPrumada() {
		return idRedeDiametroPrumada;
	}

	public void setIdRedeDiametroPrumada(Long idRedeDiametroPrumada) {
		this.idRedeDiametroPrumada = idRedeDiametroPrumada;
	}

	public String getQuantidadeReguladorHall() {
		return quantidadeReguladorHall;
	}

	public void setQuantidadeReguladorHall(String quantidadeReguladorHall) {
		this.quantidadeReguladorHall = quantidadeReguladorHall;
	}

	public String getVentilacaoHall() {
		return ventilacaoHall;
	}

	public void setVentilacaoHall(String ventilacaoHall) {
		this.ventilacaoHall = ventilacaoHall;
	}

	public String getVentilacaoApartamento() {
		return ventilacaoApartamento;
	}

	public void setVentilacaoApartamento(String ventilacaoApartamento) {
		this.ventilacaoApartamento = ventilacaoApartamento;
	}

	public Long[] getIdsServicoTipoRestricao() {
		Long[] retorno = null;
		if(this.idsServicoTipoRestricao != null) {
			retorno = this.idsServicoTipoRestricao.clone();
		}
		return retorno;
	}

	public void setIdsServicoTipoRestricao(Long[] idsServicoTipoRestricao) {
		if(idsServicoTipoRestricao != null) {
			this.idsServicoTipoRestricao = idsServicoTipoRestricao.clone();
		}
	}

	public String getIndicadorRestricaoServico() {
		return indicadorRestricaoServico;
	}

	public void setIndicadorRestricaoServico(String indicadorRestricaoServico) {
		this.indicadorRestricaoServico = indicadorRestricaoServico;
	}

	public String getQuadraFaceImovel() {
		return quadraFaceImovel;
	}

	public void setQuadraFaceImovel(String quadraFaceImovel) {
		this.quadraFaceImovel = quadraFaceImovel;
	}

	public String getDescricaoSituacaoImovel() {
		return descricaoSituacaoImovel;
	}

	public void setDescricaoSituacaoImovel(String descricaoSituacaoImovel) {
		this.descricaoSituacaoImovel = descricaoSituacaoImovel;
	}

	public String getDescricaoPavimentoCalcadaImovel() {
		return descricaoPavimentoCalcadaImovel;
	}

	public void setDescricaoPavimentoCalcadaImovel(String descricaoPavimentoCalcadaImovel) {
		this.descricaoPavimentoCalcadaImovel = descricaoPavimentoCalcadaImovel;
	}

	public String getDescricaoPavimentoRuaImovel() {
		return descricaoPavimentoRuaImovel;
	}

	public void setDescricaoPavimentoRuaImovel(String descricaoPavimentoRuaImovel) {
		this.descricaoPavimentoRuaImovel = descricaoPavimentoRuaImovel;
	}

	public String getDescricaoPadraoConstrucaoImovel() {
		return descricaoPadraoConstrucaoImovel;
	}

	public void setDescricaoPadraoConstrucaoImovel(String descricaoPadraoConstrucaoImovel) {
		this.descricaoPadraoConstrucaoImovel = descricaoPadraoConstrucaoImovel;
	}

	public String getFaixaAreaConstruidaImovel() {
		return faixaAreaConstruidaImovel;
	}

	public void setFaixaAreaConstruidaImovel(String faixaAreaConstruidaImovel) {
		this.faixaAreaConstruidaImovel = faixaAreaConstruidaImovel;
	}

	public String getDescricaoPerfilImovel() {
		return descricaoPerfilImovel;
	}

	public void setDescricaoPerfilImovel(String descricaoPerfilImovel) {
		this.descricaoPerfilImovel = descricaoPerfilImovel;
	}

	public String getEhCondominio() {
		String ehCondominio = "Não";
		if("true".equals(this.condominio)) {
			ehCondominio = "Sim";
		}
		return ehCondominio;
	}

	public String getExisteValvulaBloqueio() {
		String existeValvulaBloqueio = "Não";
		if("true".equals(this.valvulaBloqueio)) {
			existeValvulaBloqueio = "Sim";
		}
		return existeValvulaBloqueio;
	}

	public String getDescricaoModalidadeMedicaoImovel() {
		return descricaoModalidadeMedicaoImovel;
	}

	public void setDescricaoModalidadeMedicaoImovel(String descricaoModalidadeMedicaoImovel) {
		this.descricaoModalidadeMedicaoImovel = descricaoModalidadeMedicaoImovel;
	}

	public String getDescricaoTipoBotijaoImovel() {
		return descricaoTipoBotijaoImovel;
	}

	public void setDescricaoTipoBotijaoImovel(String descricaoTipoBotijaoImovel) {
		this.descricaoTipoBotijaoImovel = descricaoTipoBotijaoImovel;
	}

	public String getDescricaoTipoCombustivelImovel() {
		return descricaoTipoCombustivelImovel;
	}

	public void setDescricaoTipoCombustivelImovel(String descricaoTipoCombustivelImovel) {
		this.descricaoTipoCombustivelImovel = descricaoTipoCombustivelImovel;
	}

	public String getNomeConstrutora() {
		return nomeConstrutora;
	}

	public void setNomeConstrutora(String nomeConstrutora) {
		this.nomeConstrutora = nomeConstrutora;
	}

	public String getPossuiPortaria() {
		String possuiPortaria = "Não";
		if("true".equals(this.portaria)) {
			possuiPortaria = "Sim";
		}
		return possuiPortaria;
	}

	public String getPossuiRedeInterna() {
		String possuiRedeInterna = "Não";
		if("true".equals(this.redePreexistente)) {
			possuiRedeInterna = "Sim";
		}
		return possuiRedeInterna;
	}

	public String getPossuiVentilacaoHall() {
		String possuiVentilacaoHall = "Não";
		if("true".equals(this.ventilacaoHall)) {
			possuiVentilacaoHall = "Sim";
		}
		return possuiVentilacaoHall;
	}

	public String getDescricaoRedeMaterial() {
		return descricaoRedeMaterial;
	}

	public void setDescricaoRedeMaterial(String descricaoRedeMaterial) {
		this.descricaoRedeMaterial = descricaoRedeMaterial;
	}

	public String getDiametroRedeTrechoPrumada() {
		return diametroRedeTrechoPrumada;
	}

	public void setDiametroRedeTrechoPrumada(String diametroRedeTrechoPrumada) {
		this.diametroRedeTrechoPrumada = diametroRedeTrechoPrumada;
	}

	public String getDiametroRedeNasPrumada() {
		return diametroRedeNasPrumada;
	}

	public void setDiametroRedeNasPrumada(String diametroRedeNasPrumada) {
		this.diametroRedeNasPrumada = diametroRedeNasPrumada;
	}

	public String getDiametroRedeSecundaria() {
		return diametroRedeSecundaria;
	}

	public void setDiametroRedeSecundaria(String diametroRedeSecundaria) {
		this.diametroRedeSecundaria = diametroRedeSecundaria;
	}

	public String getPossuiVentilacaoApartamento() {
		String possuiVentilacaoApartamento = "Não";
		if("true".equals(this.ventilacaoApartamento)) {
			possuiVentilacaoApartamento = "Sim";
		}
		return possuiVentilacaoApartamento;
	}

	public String getDescricaoRotaPrevista() {
		return descricaoRotaPrevista;
	}

	public void setDescricaoRotaPrevista(String descricaoRotaPrevista) {
		this.descricaoRotaPrevista = descricaoRotaPrevista;
	}

	public String getNomeAgente() {
		return nomeAgente;
	}

	public void setNomeAgente(String nomeAgente) {
		this.nomeAgente = nomeAgente;
	}

	public Long getIdModalidadeUso() {
		return idModalidadeUso;
	}

	public void setIdModalidadeUso(Long idModalidadeUso) {
		this.idModalidadeUso = idModalidadeUso;
	}

	public Long[] getChavesPrimarias() {
		Long[] retorno = null;
		if(this.chavesPrimarias != null) {
			retorno = this.chavesPrimarias.clone();
		}
		return retorno;
	}

	public void setChavesPrimarias(Long[] chavesPrimarias) {
		if(chavesPrimarias != null) {
			this.chavesPrimarias = chavesPrimarias.clone();
		}
	}

	public String getIndicadorCondominio() {
		return indicadorCondominio;
	}

	public void setIndicadorCondominio(String indicadorCondominio) {
		this.indicadorCondominio = indicadorCondominio;
	}

	public String getImovelPai() {
		return imovelPai;
	}

	public void setImovelPai(String imovelPai) {
		this.imovelPai = imovelPai;
	}

	public String getNomeFantasiaImovelPai() {
		return nomeFantasiaImovelPai;
	}

	public void setNomeFantasiaImovelPai(String nomeFantasiaImovelPai) {
		this.nomeFantasiaImovelPai = nomeFantasiaImovelPai;
	}

	public String getMatriculaImovelPai() {
		return matriculaImovelPai;
	}

	public void setMatriculaImovelPai(String matriculaImovelPai) {
		this.matriculaImovelPai = matriculaImovelPai;
	}

	public String getNumeroImovelPai() {
		return numeroImovelPai;
	}

	public void setNumeroImovelPai(String numeroImovelPai) {
		this.numeroImovelPai = numeroImovelPai;
	}

	public String getCidadeImovelPai() {
		return cidadeImovelPai;
	}

	public void setCidadeImovelPai(String cidadeImovelPai) {
		this.cidadeImovelPai = cidadeImovelPai;
	}

	public String getCondominioPai() {
		return condominioPai;
	}

	public void setCondominioPai(String condominioPai) {
		this.condominioPai = condominioPai;
	}

	public String getIndicadorCondominioImovelPai() {
		return indicadorCondominioImovelPai;
	}

	public void setIndicadorCondominioImovelPai(String indicadorCondominioImovelPai) {
		this.indicadorCondominioImovelPai = indicadorCondominioImovelPai;
	}

	public Long getIdImovelCondominio() {
		return idImovelCondominio;
	}

	public void setIdImovelCondominio(Long idImovelCondominio) {
		this.idImovelCondominio = idImovelCondominio;
	}

	public String getIdCepImovel() {
		return idCepImovel;
	}

	public void setIdCepImovel(String idCepImovel) {
		this.idCepImovel = idCepImovel;
	}

	public PontoConsumo getPontoConsumo() {
		return pontoConsumo;
	}

	public void setPontoConsumo(PontoConsumo pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}	
}
