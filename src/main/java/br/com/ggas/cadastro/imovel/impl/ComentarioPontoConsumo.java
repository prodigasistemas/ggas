/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos
 * da classe ComentarioPontoConsumoImpl
 * 
 */
public interface ComentarioPontoConsumo extends EntidadeNegocio {

	String BEAN_ID_COMENTARIO_PONTO_CONSUMO = "comentarioPontoConsumo";

	String NUMERO = "COMENTARIO_PONTO_CONSUMO_NUMERO";

	String DESCRICAO = "COMENTARIO_PONTO_CONSUMO_DESCRICAO";

	String PONTO_CONSUMO = "COMENTARIO_PONTO_CONSUMO_PONTO_CONSUMO";

	String USUARIO = "COMENTARIO_PONTO_CONSUMO_USUARIO";

	String OPERACAO = "COMENTARIO_PONTO_CONSUMO_OPERACAO";

	/**
	 * @return the numero
	 */
	public Integer getNumero();

	/**
	 * @param numero
	 *            the numero to set
	 */
	public void setNumero(Integer numero);

	/**
	 * @return the descricao
	 */
	public String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	public void setDescricao(String descricao);

	/**
	 * @return the pontoConsumo
	 */
	public PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the usuario
	 */
	public Usuario getUsuario();

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	public void setUsuario(Usuario usuario);

	/**
	 * @return the operacao
	 */
	public Operacao getOperacao();

	/**
	 * @param operacao
	 *            the operacao to set
	 */
	public void setOperacao(Operacao operacao);

}
