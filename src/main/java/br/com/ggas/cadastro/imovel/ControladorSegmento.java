/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.consumo.IntervaloPCS;

/**
 * Interface ControladorSegmento
 * 
 * @author arthur
 *
 */
public interface ControladorSegmento extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_SEGMENTO = "controladorSegmento";

	String ERRO_NEGOCIO_SEGMENTO_EXISTENTE = "ERRO_NEGOCIO_SEGMENTO_EXISTENTE";

	String ERRO_NUMERO_CICLOS = "ERRO_NUMERO_CICLOS";

	String ERRO_NUMERO_DIAS_FATURAMENTO = "ERRO_NUMERO_DIAS_FATURAMENTO";

	String ERRO_PERIODICIDA = "ERRO_PERIODICIDA";

	String ERRO_DESCRICAO = "ERRO_DESCRICAO";

	String ERRO_DESCRICAO_ABREVIADA = "ERRO_DESCRICAO_ABREVIADA";

	String ERRO_TIPO_SEGMENTO = "ERRO_TIPO_SEGMENTO";

	String ERRO_INTEGRIDADE_RELACIONAL = "ERRO_INTEGRIDADE_RELACIONAL";

	String ERRO_NEGOCIO_REMOVER_TIPO_SEGMENTO = "ERRO_NEGOCIO_REMOVER_TIPO_SEGMENTO";

	/**
	 * Método responsável por consultar os
	 * segmentos pelo filtro informado.
	 * 
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de segmentos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Segmento> consultarSegmento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar segmento inclusao faixa.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Segmento> consultarSegmentoInclusaoFaixa(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método que cria a entidade TipoSegmento.
	 * 
	 * @return Instância de TipoSegmento
	 */
	EntidadeNegocio criarTipoSegmento();

	/**
	 * Recupera a tipo segmento com a chave
	 * primária informada.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do tipo de
	 *            segmento.
	 * @return TipoSegmento]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	TipoSegmento obterTipoSegmento(Long chavePrimaria) throws NegocioException;

	/**
	 * Recupera a segmento com a chave primária
	 * informada.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do segmento.
	 * @return Segmento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Segmento obterSegmento(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover segmentos.
	 * 
	 * @param chavesPrimarias
	 *            Chaves dos segmentos.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarRemoverSegmentos(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por listar todos os
	 * tipos de segmentos.
	 * 
	 * @return Coleção de tipos de segmentos.
	 */
	Collection<TipoSegmento> listarTodosTipoSegmento();

	/**
	 * Método responsável por listas os segmentos.
	 * 
	 * @return coleção de tipos segmentos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Segmento> listarSegmento() throws NegocioException;

	/**
	 * Método responsável por listar as
	 * amostragens de PCS de um segmento
	 * informado.
	 * 
	 * @param segmento
	 *            O segmento.
	 * @return Coleção de amostragens de PCS.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> listarAmostragensPCSPorSegmento(Segmento segmento) throws NegocioException;

	/**
	 * Método responsável por listar os intervalos
	 * de PCS de um segmento informado.
	 * 
	 * @param segmento
	 *            O segmento.
	 * @return Coleção de intervalos de PCS.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<IntervaloPCS> listarIntervalosPCSPorSegmento(Segmento segmento) throws NegocioException;

	/**
	 * Atualizar segmento.
	 * 
	 * @param segmento
	 *            the segmento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws GGASException
	 *             the exception
	 */
	void atualizarSegmento(Segmento segmento, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Inserir segmento ramo atividade.
	 * 
	 * @param segmento
	 *            the segmento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	long inserirSegmentoRamoAtividade(Segmento segmento, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Validar segmento.
	 * 
	 * @param segmento
	 *            the segmento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarSegmento(Long segmento) throws NegocioException;

	/**
	 * Listar segmento amostragem pcs.
	 * 
	 * @param segmento
	 *            the segmento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SegmentoAmostragemPCS> listarSegmentoAmostragemPCS(Long segmento) throws NegocioException;

	/**
	 * Listar segmento intervalo pcs.
	 * 
	 * @param segmento
	 *            the segmento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SegmentoIntervaloPCS> listarSegmentoIntervaloPCS(Long segmento) throws NegocioException;

	/**
	 * Listar segmento amostragem pcs disponiveis.
	 * 
	 * @param segmento
	 *            the segmento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarSegmentoAmostragemPCSDisponiveis(Long segmento) throws NegocioException;

	/**
	 * Listar segmento intervalo pcs disponiveis.
	 * 
	 * @param segmento
	 *            the segmento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntervaloPCS> listarSegmentoIntervaloPCSDisponiveis(Long segmento) throws NegocioException;

	/**
	 * Obter segmento intervalo pcs.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the segmento intervalo pcs
	 * @throws NegocioException
	 *             the negocio exception
	 */
	SegmentoIntervaloPCS obterSegmentoIntervaloPCS(Long chavePrimaria) throws NegocioException;

	/**
	 * Obter segmento amostragem pcs.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the segmento amostragem pcs
	 * @throws NegocioException
	 *             the negocio exception
	 */
	SegmentoAmostragemPCS obterSegmentoAmostragemPCS(Long chavePrimaria) throws NegocioException;

	/**
	 * Criar segmento amostragem pcs.
	 * 
	 * @return the segmento amostragem pcs
	 */
	SegmentoAmostragemPCS criarSegmentoAmostragemPCS();

	/**
	 * Criar segmento intervalo pcs.
	 * 
	 * @return the segmento intervalo pcs
	 */
	SegmentoIntervaloPCS criarSegmentoIntervaloPCS();

	/**
	 * Obter intervalo pcs para segmento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the intervalo pcs
	 * @throws NegocioException
	 *             the negocio exception
	 */
	IntervaloPCS obterIntervaloPCSParaSegmento(Long chavePrimaria) throws NegocioException;

	/**
	 * Remover segmentos.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerSegmentos(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Obtém as chaves dos Segmentos dos pontos de consumo
	 * especificados no parâmetro.
	 * 
	 * @param pontosConsumo {@link Collection}
	 * @return segmentos dos pontos de consumo
	 **/
	Long[] obterChavesDeSegmentosDePontosConsumo(Collection<PontoConsumo> pontosConsumo);

}
