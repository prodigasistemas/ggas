/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.consumo.IntervaloPCS;

/**
 * Interface ControladorRamoAtividade
 * 
 * @author arthur
 *
 */
public interface ControladorRamoAtividade extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_RAMO_ATIVIDADE = "controladorRamoAtividade";

	String ERRO_TIPO_RAMO_ATIVIDADE = "ERRO_TIPO_RAMO_ATIVIDADE";

	String ERRO_NEGOCIO_REMOVER_RAMO_ATIVIDADE = "ERRO_NEGOCIO_REMOVER_RAMO_ATIVIDADE";

	/**
	 * Metodo responsavel por validar a remocao de
	 * um Ramo Atividade.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarRemoverRamoAtividade(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Metodo responsavel por consultar Ramo
	 * Atividade a partir de um filtro informado.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<RamoAtividade> consultarRamoAtividade(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar os ramos de
	 * atividades por segmento.
	 * 
	 * @param chavePrimariaSegmento
	 *            Chave primária do segmento
	 * @return coleção de ramos de atividade.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<RamoAtividade> listarRamoAtividadePorSegmento(Long chavePrimariaSegmento) throws NegocioException;

	/**
	 * Método responsável por listar os ramos de atividades por segmento imóvel.
	 * 
	 * @param idRamoAtividadePontoConsumo id ramo atividade por segmento
	 * @return coleção de ramos de atividade.
	 * @throws NegocioException the negocio exception
	 */
	Collection<RamoAtividade> listarRamoAtividadePorSegmentoImovel(Long idRamoAtividadePontoConsumo) throws NegocioException;

	/**
	 * Método responsável por listar as
	 * amostragens de PCS de um ramo de atividade
	 * informado.
	 * 
	 * @param ramoAtividade
	 *            O ramo de atividade.
	 * @return Coleção de amostragens de PCS.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> listarAmostragensPCSPorRamoAtividade(RamoAtividade ramoAtividade) throws NegocioException;

	/**
	 * Método responsável por listar os intervalos
	 * de PCS de um ramo de atividade informado.
	 * 
	 * @param ramoAtividade
	 *            O ramo de atividade.
	 * @return Coleção de intervalos de PCS.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<IntervaloPCS> listarIntervalosPCSPorRamoAtividade(RamoAtividade ramoAtividade) throws NegocioException;

	/**
	 * Método responsável por listar os Ramos de
	 * Atividade de um grupo de segmentos.
	 * 
	 * @param chavesSegmentos
	 *            chaves dos segmentos
	 * @return a coleção de ramos de atividades
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método
	 */
	Collection<RamoAtividade> consultarRamoAtividadePorSegmentos(Long[] chavesSegmentos) throws NegocioException;

	/**
	 * Método responsável por validar os dados do
	 * ramo de atividade.
	 * 
	 * @param ramoAtividade
	 *            the ramo atividade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosRamoAtividade(RamoAtividade ramoAtividade) throws NegocioException;

	/**
	 * Validar ramo atividade.
	 * 
	 * @param ramoAtividade
	 *            the ramo atividade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarRamoAtividade(Long ramoAtividade) throws NegocioException;

	/**
	 * Listar ramo atividade amostragem pcs por ramo atividade.
	 * 
	 * @param ramoAtividade
	 *            the ramo atividade
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<RamoAtividadeAmostragemPCS> listarRamoAtividadeAmostragemPCSPorRamoAtividade(RamoAtividade ramoAtividade)
					throws GGASException;

	/**
	 * Listar ramo atividade intervalo pcs por ramo atividade.
	 * 
	 * @param ramoAtividade
	 *            the ramo atividade
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<RamoAtividadeIntervaloPCS> listarRamoAtividadeIntervaloPCSPorRamoAtividade(RamoAtividade ramoAtividade) throws GGASException;

	/**
	 * Listar ramo atividade amostragem pcs disponiveis.
	 * 
	 * @param segmento
	 *            the segmento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarRamoAtividadeAmostragemPCSDisponiveis(Long segmento) throws NegocioException;

	/**
	 * Listar ramo atividade intervalo pcs disponiveis.
	 * 
	 * @param segmento
	 *            the segmento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<IntervaloPCS> listarRamoAtividadeIntervaloPCSDisponiveis(Long segmento) throws NegocioException;

	/**
	 * Criar ramo atividade amostragem pcs.
	 * 
	 * @return the ramo atividade amostragem pcs
	 * @throws NegocioException
	 *             the negocio exception
	 */
	RamoAtividadeAmostragemPCS criarRamoAtividadeAmostragemPCS() throws NegocioException;

	/**
	 * Criar ramo atividade intervalo pcs.
	 * 
	 * @return the ramo atividade intervalo pcs
	 * @throws NegocioException
	 *             the negocio exception
	 */
	RamoAtividadeIntervaloPCS criarRamoAtividadeIntervaloPCS() throws NegocioException;

	/**
	 * Obter intervalo pcs para ramo atividade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the intervalo pcs
	 * @throws NegocioException
	 *             the negocio exception
	 */
	IntervaloPCS obterIntervaloPCSParaRamoAtividade(Long chavePrimaria) throws NegocioException;

	/**
	 * Remover ramo atividade.
	 * 
	 * @param ramosAtividades
	 *            the ramos atividades
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerRamoAtividade(Collection<RamoAtividade> ramosAtividades) throws NegocioException;

	/**
	 * Agrupa os ramos de atividade dos pontos de consumo
	 * 
	 * @param pontosConsumo {@link Collection}
	 * @return ramos de atividade distintos dos pontos de consumo
	 */
	Collection<RamoAtividade> agruparRamoAtividadeDePontosConsumo(Collection<PontoConsumo> pontosConsumo);

	Collection<RamoAtividade> listarRamoAtividade() throws NegocioException;
}
