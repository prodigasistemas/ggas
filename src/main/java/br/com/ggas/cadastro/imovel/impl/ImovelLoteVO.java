package br.com.ggas.cadastro.imovel.impl;

/**
 * Classe responsável pela representação dos atributos relativos aos lotes de um imóvel.
 * 
 * @author esantana
 *
 */
public class ImovelLoteVO {

	private Integer indexListaLote;

	private String[] blocoImovelLote;

	private String tipoUnidade;

	private String[] numeroSubLoteImovelLote;

	private String[] numeroSequenciaLeituraImovelLote;

	private String[] apartamentoImovelLote;

	private String[] complementoImovelLote;

	private String[] quantidadeBanheiroLote;

	private Long[] modalidadeUsoLote;

	private Long[] faixaAreaConstruidaImovelLote;

	private String numeroBlocos;

	private Long idSegmentoPC;

	private Long idRamoAtividadePC;

	private Long idRotaPC;

	private String[] nomeBlocos;

	private String inicioApartamento;

	private String incrementoApartamento;

	private String indicadorBotaoAcionado = "false";

	private String nomeAntigoBloco;

	private String nomeNovoBloco;

	private Boolean existeImoveisFilhos;

	public String[] getBlocoImovelLote() {
		String[] retorno = null;
		if (this.blocoImovelLote != null) {
			retorno = this.blocoImovelLote.clone();
		}
		return retorno;
	}

	public void setBlocoImovelLote(String[] blocoImovelLote) {
		if (blocoImovelLote != null) {
			this.blocoImovelLote = blocoImovelLote.clone();
		}
	}

	public String getTipoUnidade() {
		return tipoUnidade;
	}

	public void setTipoUnidade(String tipoUnidade) {
		this.tipoUnidade = tipoUnidade;
	}

	public String[] getNumeroSubLoteImovelLote() {
		String[] retorno = null;
		if (this.numeroSubLoteImovelLote != null) {
			retorno = this.numeroSubLoteImovelLote.clone();
		}
		return retorno;
	}

	public void setNumeroSubLoteImovelLote(String[] numeroSubLoteImovelLote) {
		if (numeroSubLoteImovelLote != null) {
			this.numeroSubLoteImovelLote = numeroSubLoteImovelLote.clone();
		}
	}

	public String[] getNumeroSequenciaLeituraImovelLote() {
		String[] retorno = null;
		if (this.numeroSequenciaLeituraImovelLote != null) {
			retorno = this.numeroSequenciaLeituraImovelLote.clone();
		}
		return retorno;
	}

	public void setNumeroSequenciaLeituraImovelLote(String[] numeroSequenciaLeituraImovelLote) {
		if (numeroSequenciaLeituraImovelLote != null) {
			this.numeroSequenciaLeituraImovelLote = numeroSequenciaLeituraImovelLote.clone();
		}
	}

	public String[] getApartamentoImovelLote() {
		String[] retorno = {};
		if (this.apartamentoImovelLote != null) {
			retorno = this.apartamentoImovelLote.clone();
		}
		return retorno;
	}

	public void setApartamentoImovelLote(String[] apartamentoImovelLote) {
		if (apartamentoImovelLote != null) {
			this.apartamentoImovelLote = apartamentoImovelLote.clone();
		}
	}

	public String[] getComplementoImovelLote() {
		String[] retorno = null;
		if (this.complementoImovelLote != null) {
			retorno = this.complementoImovelLote.clone();
		}
		return retorno;
	}

	public void setComplementoImovelLote(String[] complementoImovelLote) {
		if (complementoImovelLote != null) {
			this.complementoImovelLote = complementoImovelLote.clone();
		}

	}

	public String[] getQuantidadeBanheiroLote() {
		String[] retorno = null;
		if (this.quantidadeBanheiroLote != null) {
			retorno = this.quantidadeBanheiroLote.clone();
		}
		return retorno;
	}

	public void setQuantidadeBanheiroLote(String[] quantidadeBanheiroLote) {
		if (quantidadeBanheiroLote != null) {
			this.quantidadeBanheiroLote = quantidadeBanheiroLote.clone();
		}
	}

	public Long[] getModalidadeUsoLote() {
		Long[] retorno = null;
		if (this.modalidadeUsoLote != null) {
			retorno = this.modalidadeUsoLote.clone();
		}
		return retorno;
	}

	public void setModalidadeUsoLote(Long[] modalidadeUsoLote) {
		if (modalidadeUsoLote != null) {
			this.modalidadeUsoLote = modalidadeUsoLote.clone();
		}
	}

	public Long[] getFaixaAreaConstruidaImovelLote() {
		Long[] retorno = null;
		if (this.faixaAreaConstruidaImovelLote != null) {
			retorno = this.faixaAreaConstruidaImovelLote.clone();
		}
		return retorno;
	}

	public void setFaixaAreaConstruidaImovelLote(Long[] faixaAreaConstruidaImovelLote) {
		if (faixaAreaConstruidaImovelLote != null) {
			this.faixaAreaConstruidaImovelLote = faixaAreaConstruidaImovelLote.clone();
		}
	}

	public String getNumeroBlocos() {
		return numeroBlocos;
	}

	public void setNumeroBlocos(String numeroBlocos) {
		this.numeroBlocos = numeroBlocos;
	}

	public Long getIdSegmentoPC() {
		return idSegmentoPC;
	}

	public void setIdSegmentoPC(Long idSegmentoPC) {
		this.idSegmentoPC = idSegmentoPC;
	}

	public Long getIdRamoAtividadePC() {
		return idRamoAtividadePC;
	}

	public void setIdRamoAtividadePC(Long idRamoAtividadePC) {
		this.idRamoAtividadePC = idRamoAtividadePC;
	}

	public Long getIdRotaPC() {
		return idRotaPC;
	}

	public void setIdRotaPC(Long idRotaPC) {
		this.idRotaPC = idRotaPC;
	}

	public String[] getNomeBlocos() {
		String[] retorno = null;
		if (this.nomeBlocos != null) {
			retorno = this.nomeBlocos.clone();
		}
		return retorno;
	}

	public void setNomeBlocos(String[] nomeBlocos) {
		if (nomeBlocos != null) {
			this.nomeBlocos = nomeBlocos.clone();
		}
	}

	public String getInicioApartamento() {
		return inicioApartamento;
	}

	public void setInicioApartamento(String inicioApartamento) {
		this.inicioApartamento = inicioApartamento;
	}

	public String getIncrementoApartamento() {
		return incrementoApartamento;
	}

	public void setIncrementoApartamento(String incrementoApartamento) {
		this.incrementoApartamento = incrementoApartamento;
	}

	public Integer getIndexListaLote() {
		return indexListaLote;
	}

	public void setIndexListaLote(Integer indexListaLote) {
		this.indexListaLote = indexListaLote;
	}

	public String getIndicadorBotaoAcionado() {
		return indicadorBotaoAcionado;
	}

	public void setIndicadorBotaoAcionado(String indicadorBotaoAcionado) {
		this.indicadorBotaoAcionado = indicadorBotaoAcionado;
	}

	public String getNomeAntigoBloco() {
		return nomeAntigoBloco;
	}

	public void setNomeAntigoBloco(String nomeAntigoBloco) {
		this.nomeAntigoBloco = nomeAntigoBloco;
	}

	public String getNomeNovoBloco() {
		return nomeNovoBloco;
	}

	public void setNomeNovoBloco(String nomeNovoBloco) {
		this.nomeNovoBloco = nomeNovoBloco;
	}

	public Boolean getExisteImoveisFilhos() {
		return existeImoveisFilhos;
	}

	public void setExisteImoveisFilhos(Boolean existeImoveisFilhos) {
		this.existeImoveisFilhos = existeImoveisFilhos;
	}

}
