/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * interface PontoConsumoTributoAliquota
 * 
 * @author arthur
 *
 */
public interface PontoConsumoTributoAliquota extends EntidadeNegocio {

	String BEAN_ID_PONTO_CONSUMO_TRIBUTO_ALIQUOTA = "pontoConsumoTributoAliquota";

	String PONTO_CONSUMO_TRIBUTO_PONTO_CONSUMO = "PONTO_CONSUMO_TRIBUTO_PONTO_CONSUMO";

	String PONTO_CONSUMO_TRIBUTO_ALIQUOTA_TRIBUTO = "PONTO_CONSUMO_TRIBUTO_ALIQUOTA_TRIBUTO";

	String PONTO_CONSUMO_TRIBUTO_INDICADOR_ISENCAO = "PONTO_CONSUMO_TRIBUTO_INDICADOR_ISENCAO";

	String PONTO_CONSUMO_TRIBUTO_DATA_INICIO_VIGENCIA = "PONTO_CONSUMO_TRIBUTO_DATA_INICIO_VIGENCIA";

	String PONTO_CONSUMO_TRIBUTO_PORCENTAGEM_ALIQUOTA = "PONTO_CONSUMO_TRIBUTO_PORCENTAGEM_ALIQUOTA";

	String PONTO_CONSUMO_TRIBUTO_DATA_FIM_VIGENCIA = "PONTO_CONSUMO_TRIBUTO_DATA_FIM_VIGENCIA";

	/**
	 * @return the tributo
	 */
	Tributo getTributo();

	/**
	 * @param tributo
	 *            the tributo to set
	 */
	void setTributo(Tributo tributo);

	/**
	 * @return the dataInicioVigencia
	 */
	Date getDataInicioVigencia();

	/**
	 * @param dataInicioVigencia
	 *            the dataInicioVigencia to set
	 */
	void setDataInicioVigencia(Date dataInicioVigencia);

	/**
	 * @return the dataFimVigencia
	 */
	Date getDataFimVigencia();

	/**
	 * @param dataFimVigencia
	 *            the dataFimVigencia to set
	 */
	void setDataFimVigencia(Date dataFimVigencia);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the porcentagemAliquota
	 */
	BigDecimal getPorcentagemAliquota();

	/**
	 * @param porcentagemAliquota
	 *            the porcentagemAliquota to set
	 */
	void setPorcentagemAliquota(BigDecimal porcentagemAliquota);

	/**
	 * @return the indicadorIsencao
	 */
	Boolean getIndicadorIsencao();

	/**
	 * @param indicadorIsencao
	 *            the indicadorIsencao to set
	 */
	void setIndicadorIsencao(Boolean indicadorIsencao);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.EntidadeNegocio#validarDados()
	 */
	@Override
	Map<String, Object> validarDados();

	/**
	 * @return Boolean - Destaque indicador nota.
	 */
	Boolean getDestaqueIndicadorNota();

	/**
	 * @param destaqueIndicadorNota - Set destaque indicador de nota.
	 */
	void setDestaqueIndicadorNota(Boolean destaqueIndicadorNota);

	/**
	 * @return Boolean - Indicador credito icms.
	 */
	Boolean getIndicadorCreditoIcms();

	/**
	 * @param indicadorCreditoIcms - Set Indicador credito Icms.
	 */
	void setIndicadorCreditoIcms(Boolean indicadorCreditoIcms);

	/**
	 * @return Boolean - Indicador Icms substituto tarfa.
	 */
	Boolean getIndicadorIcmsSubstitutoTarifa();

	/**
	 * @param indicadorIcmsSubstitutoTarifa - Set indicador icms substituto tarifa.
	 */
	void setIndicadorIcmsSubstitutoTarifa(Boolean indicadorIcmsSubstitutoTarifa);

	/**
	 * @return Boolean - Indicador DrawBack.
	 */
	Boolean getIndicadorDrawback();

	/**
	 * @param indicadorDrawback - Set Indicador DrawBack. 
	 */
	void setIndicadorDrawback(Boolean indicadorDrawback);

	/**
	 * @return BigDecimal - Retorna Valor drawBack.
	 */
	BigDecimal getValorDrawback();

	/**
	 * @param valorDrawback - Set valor drawback. 
	 */
	void setValorDrawback(BigDecimal valorDrawback);

}
