/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Map;

import br.com.ggas.cadastro.imovel.Atributo;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.cadastro.imovel.TipoDadoAtributo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * 
 *
 */
class AtributoImpl extends EntidadeNegocioImpl implements Atributo {

	private static final long serialVersionUID = -8588077525006391782L;

	private String nome;

	private String observacao;

	private Tabela tabela;

	private TipoDadoAtributo tipoDadoAtributo;

	private String mascara;

	private Boolean obrigatorio;

	private Boolean multivalorado;

	private Boolean escolhaFechada;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #getNome()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #getObservacao()
	 */
	@Override
	public String getObservacao() {

		return observacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #setObservacao(java.lang.String)
	 */
	@Override
	public void setObservacao(String observacao) {

		this.observacao = observacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #getTabela()
	 */
	@Override
	public Tabela getTabela() {

		return tabela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #setTabela
	 * (br.com.ggas.cadastro.imovel.Tabela)
	 */
	@Override
	public void setTabela(Tabela tabela) {

		this.tabela = tabela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #getTipoDadoAtributo()
	 */
	@Override
	public TipoDadoAtributo getTipoDadoAtributo() {

		return tipoDadoAtributo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #setTipoDadoAtributo
	 * (br.com.ggas.cadastro.imovel
	 * .TipoDadoAtributo)
	 */
	@Override
	public void setTipoDadoAtributo(TipoDadoAtributo tipoDadoAtributo) {

		this.tipoDadoAtributo = tipoDadoAtributo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #getMascara()
	 */
	@Override
	public String getMascara() {

		return mascara;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #setMascara(java.lang.String)
	 */
	@Override
	public void setMascara(String mascara) {

		this.mascara = mascara;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #getObrigatorio()
	 */
	@Override
	public Boolean getObrigatorio() {

		return obrigatorio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #setObrigatorio(java.lang.Boolean)
	 */
	@Override
	public void setObrigatorio(Boolean obrigatorio) {

		this.obrigatorio = obrigatorio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #getMultivalorado()
	 */
	@Override
	public Boolean getMultivalorado() {

		return multivalorado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #setMultivalorado(java.lang.Boolean)
	 */
	@Override
	public void setMultivalorado(Boolean multivalorado) {

		this.multivalorado = multivalorado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #getEscolhaFechada()
	 */
	@Override
	public Boolean getEscolhaFechada() {

		return escolhaFechada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.impl.Atributo
	 * #setEscolhaFechada(java.lang.Boolean)
	 */
	@Override
	public void setEscolhaFechada(Boolean escolhaFechada) {

		this.escolhaFechada = escolhaFechada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
