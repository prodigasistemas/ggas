package br.com.ggas.cadastro.imovel;

import java.math.BigDecimal;
import java.util.Map;

import br.com.ggas.cadastro.equipamento.Equipamento;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * interface PontoConsumoEquipamento
 * 
 * @author arthur
 *
 */
public interface PontoConsumoEquipamento extends EntidadeNegocio {
	
	String BEAN_ID_PONTO_CONSUMO_EQUIPAMENTO = "pontoConsumoEquipamento";
	
	String PONTO_CONSUMO_EQUIPAMENTO_EQ = "PONTO_CONSUMO_EQUIPAMENTO_EQ";
	
	String PONTO_CONSUMO_EQUIPAMENTO_PC = "PONTO_CONSUMO_EQUIPAMENTO_PC";

	String PONTO_CONSUMO_EQUIPAMENTO_POTENCIA = "PONTO_CONSUMO_EQUIPAMENTO_POTENCIA";

	String PONTO_CONSUMO_EQUIPAMENTO_HORAS_DIA = "PONTO_CONSUMO_EQUIPAMENTO_HORAS_DIA";

	String PONTO_CONSUMO_EQUIPAMENTO_DIAS_SEMANA = "PONTO_CONSUMO_EQUIPAMENTO_DIAS_SEMANA";
	
	String PONTO_CONSUMO_EQUIPAMENTO_POTENCIA_FIXA = "PONTO_CONSUMO_EQUIPAMENTO_POTENCIA_FIXA";
	
	String ATRIBUTO_POTENCIA = "potencia";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.EntidadeNegocio#validarDados()
	 */
	@Override
	Map<String, Object> validarDados();

	/**
	 * @return pontoConsumo
	 */
	public PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo 
	 * 
	 * ponto de consumo
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return equipamento
	 */
	public Equipamento getEquipamento();

	/**
	 * @param equipamento 
	 * 
	 * equipamento
	 */
	public void setEquipamento(Equipamento equipamento);

	/**
	 * @return vazao por hora
	 */
	public BigDecimal getPotencia();

	/**
	 * @param potencia 
	 * 
	 * vazao por hora
	 */
	public void setPotencia(BigDecimal potencia);

	/**
	 * @return horas por dia
	 */
	public Integer getHorasPorDia();

	/**
	 * @param horasPorDia 
	 * 
	 * horas por dia
	 */
	public void setHorasPorDia(Integer horasPorDia);

	/**
	 * @return dias por semana
	 */
	public Integer getDiasPorSemana();

	/**
	 * @param diasPorSemana 
	 * 
	 * dias por semana
	 */
	public void setDiasPorSemana(Integer diasPorSemana);
	

	/**
	 * @return potencia fixa
	 */
	public Long getPotenciaFixa();

	/**
	 * @param potenciaFixa 
	 * 
	 * potenciaFixa the potenciaFixa to set
	 */
	public void setPotenciaFixa(Long potenciaFixa);

	/**
	 * @return potencia formatada
	 */
	String getPotenciaFormatada();
	
}
