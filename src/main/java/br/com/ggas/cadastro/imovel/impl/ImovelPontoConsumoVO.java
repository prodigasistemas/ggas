package br.com.ggas.cadastro.imovel.impl;

/**
 * Classe responsável pela representação dos atributos 
 * relativos aos Pontos de Consumo de um Imóvel com um Cliente.
 * 
 * @author esantana
 *
 */
public class ImovelPontoConsumoVO {

	private Long idSegmentoPontoConsumo;
	
	private Long idRamoAtividadePontoConsumo;
	
	private Long idModalidadeUso;
	
	private String enderecoReferenciaPontoConsumo;
	
	private String enderecoRemotoPontoConsumo;
	
	private String descricaoPontoConsumo;
	
	private String descricaoComplementoPontoConsumo;
	
	private String cepPontosConsumo;
	
	private String numeroImovelPontoConsumo;
	
	private String numeroSequenciaLeitura;
	
	private Long idQuadraFacePontoConsumo;
	
	private Long idSituacaoPontoConsumo;
	
	private String latitudeGrau;
	
	private String longitudeGrau;
	
	private Long idRota;
	
	private String pontoConsumoLegado;
	
	private Long idPontoConsumo = 0L;
	
	private String indicadorClassificacaoFiscal;
	
	private String habilitadoPontoConsumo = "true";
	
	private Long idPontoConsumoTributo;
	
	private String indicadorIsento = "true";
	
	private String indicadorDestaqueNota = "false";
	
	private String percentualAliquota;
	
	private String dataInicioVigencia;
	
	private String dataFimVigencia;
	
	private String creditoIcms;
	
	private String indicadorIcmsSubstitutoTarifa;
	
	private String indicadorDrawback = "false";
	
	private String valorDrawback;
	
	private Integer indexListaPontoConsumoTributo = -1;
	
	private Long idQuadraPontoConsumo;
	
	private Integer indexListaPontoConsumoEquipamento = -1;
	
	private String qtdeEquipamentos;
	
	private Long idEquipamento;
	
	private String potencia;
	
	private String horasPorDia;
	
	private String diasPorSemana;
	
	private Long idPotenciaFixa;
	
	private String descricaoEquipamento;
	
	private String descricaoSegmento;

	private String observacaoPontoConsumo;
	
	private String descricaoMensagemAdicional;

	private boolean indicadorMensagemAdicional;

	public Long getIdPontoConsumoTributo() {
		return idPontoConsumoTributo;
	}

	public void setIdPontoConsumoTributo(Long idPontoConsumoTributo) {
		this.idPontoConsumoTributo = idPontoConsumoTributo;
	}

	public String getIndicadorIsento() {
		return indicadorIsento;
	}

	public void setIndicadorIsento(String indicadorIsento) {
		this.indicadorIsento = indicadorIsento;
	}

	public String getIndicadorDestaqueNota() {
		return indicadorDestaqueNota;
	}

	public void setIndicadorDestaqueNota(String indicadorDestaqueNota) {
		this.indicadorDestaqueNota = indicadorDestaqueNota;
	}

	public String getPercentualAliquota() {
		return percentualAliquota;
	}

	public void setPercentualAliquota(String percentualAliquota) {
		this.percentualAliquota = percentualAliquota;
	}

	public String getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	public void setDataInicioVigencia(String dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	public String getDataFimVigencia() {
		return dataFimVigencia;
	}

	public void setDataFimVigencia(String dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	public String getCreditoIcms() {
		return creditoIcms;
	}

	public void setCreditoIcms(String creditoIcms) {
		this.creditoIcms = creditoIcms;
	}

	public String getIndicadorIcmsSubstitutoTarifa() {
		return indicadorIcmsSubstitutoTarifa;
	}

	public void setIndicadorIcmsSubstitutoTarifa(String indicadorIcmsSubstitutoTarifa) {
		this.indicadorIcmsSubstitutoTarifa = indicadorIcmsSubstitutoTarifa;
	}

	public String getIndicadorDrawback() {
		return indicadorDrawback;
	}

	public void setIndicadorDrawback(String indicadorDrawback) {
		this.indicadorDrawback = indicadorDrawback;
	}

	public String getValorDrawback() {
		return valorDrawback;
	}

	public void setValorDrawback(String valorDrawback) {
		this.valorDrawback = valorDrawback;
	}

	public Long getIdSegmentoPontoConsumo() {
		return idSegmentoPontoConsumo;
	}

	public void setIdSegmentoPontoConsumo(Long idSegmentoPontoConsumo) {
		this.idSegmentoPontoConsumo = idSegmentoPontoConsumo;
	}

	public Long getIdRamoAtividadePontoConsumo() {
		return idRamoAtividadePontoConsumo;
	}

	public void setIdRamoAtividadePontoConsumo(Long idRamoAtividadePontoConsumo) {
		this.idRamoAtividadePontoConsumo = idRamoAtividadePontoConsumo;
	}

	public Long getIdModalidadeUso() {
		return idModalidadeUso;
	}

	public void setIdModalidadeUso(Long idModalidadeUso) {
		this.idModalidadeUso = idModalidadeUso;
	}

	public String getEnderecoReferenciaPontoConsumo() {
		return enderecoReferenciaPontoConsumo;
	}

	public void setEnderecoReferenciaPontoConsumo(String enderecoReferenciaPontoConsumo) {
		this.enderecoReferenciaPontoConsumo = enderecoReferenciaPontoConsumo;
	}

	public String getEnderecoRemotoPontoConsumo() {
		return enderecoRemotoPontoConsumo;
	}

	public void setEnderecoRemotoPontoConsumo(String enderecoRemotoPontoConsumo) {
		this.enderecoRemotoPontoConsumo = enderecoRemotoPontoConsumo;
	}

	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	public String getDescricaoComplementoPontoConsumo() {
		return descricaoComplementoPontoConsumo;
	}

	public void setDescricaoComplementoPontoConsumo(String descricaoComplementoPontoConsumo) {
		this.descricaoComplementoPontoConsumo = descricaoComplementoPontoConsumo;
	}

	public String getCepPontosConsumo() {
		return cepPontosConsumo;
	}

	public void setCepPontosConsumo(String cepPontosConsumo) {
		this.cepPontosConsumo = cepPontosConsumo;
	}

	public String getNumeroImovelPontoConsumo() {
		return numeroImovelPontoConsumo;
	}

	public void setNumeroImovelPontoConsumo(String numeroImovelPontoConsumo) {
		this.numeroImovelPontoConsumo = numeroImovelPontoConsumo;
	}

	public String getNumeroSequenciaLeitura() {
		return numeroSequenciaLeitura;
	}

	public void setNumeroSequenciaLeitura(String numeroSequenciaLeitura) {
		this.numeroSequenciaLeitura = numeroSequenciaLeitura;
	}

	public Long getIdQuadraFacePontoConsumo() {
		return idQuadraFacePontoConsumo;
	}

	public void setIdQuadraFacePontoConsumo(Long idQuadraFacePontoConsumo) {
		this.idQuadraFacePontoConsumo = idQuadraFacePontoConsumo;
	}

	public Long getIdSituacaoPontoConsumo() {
		return idSituacaoPontoConsumo;
	}

	public void setIdSituacaoPontoConsumo(Long idSituacaoPontoConsumo) {
		this.idSituacaoPontoConsumo = idSituacaoPontoConsumo;
	}

	public String getLatitudeGrau() {
		return latitudeGrau;
	}

	public void setLatitudeGrau(String latitudeGrau) {
		this.latitudeGrau = latitudeGrau;
	}

	public String getLongitudeGrau() {
		return longitudeGrau;
	}

	public void setLongitudeGrau(String longitudeGrau) {
		this.longitudeGrau = longitudeGrau;
	}

	public Long getIdRota() {
		return idRota;
	}

	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}

	public String getPontoConsumoLegado() {
		return pontoConsumoLegado;
	}

	public void setPontoConsumoLegado(String pontoConsumoLegado) {
		this.pontoConsumoLegado = pontoConsumoLegado;
	}

	public Long getIdPontoConsumo() {
		return idPontoConsumo;
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {
		this.idPontoConsumo = idPontoConsumo;
	}

	public String getIndicadorClassificacaoFiscal() {
		return indicadorClassificacaoFiscal;
	}

	public void setIndicadorClassificacaoFiscal(String indicadorClassificacaoFiscal) {
		this.indicadorClassificacaoFiscal = indicadorClassificacaoFiscal;
	}

	public String getHabilitadoPontoConsumo() {
		return habilitadoPontoConsumo;
	}

	public void setHabilitadoPontoConsumo(String habilitadoPontoConsumo) {
		this.habilitadoPontoConsumo = habilitadoPontoConsumo;
	}

	public Integer getIndexListaPontoConsumoTributo() {
		return indexListaPontoConsumoTributo;
	}

	public void setIndexListaPontoConsumoTributo(Integer indexListaPontoConsumoTributo) {
		this.indexListaPontoConsumoTributo = indexListaPontoConsumoTributo;
	}

	public Long getIdQuadraPontoConsumo() {
		return idQuadraPontoConsumo;
	}

	public void setIdQuadraPontoConsumo(Long idQuadraPontoConsumo) {
		this.idQuadraPontoConsumo = idQuadraPontoConsumo;
	}

	public Integer getIndexListaPontoConsumoEquipamento() {
		return indexListaPontoConsumoEquipamento;
	}

	public void setIndexListaPontoConsumoEquipamento(Integer indexListaPontoConsumoEquipamento) {
		this.indexListaPontoConsumoEquipamento = indexListaPontoConsumoEquipamento;
	}

	public String getQtdeEquipamentos() {
		return qtdeEquipamentos;
	}

	public void setQtdeEquipamentos(String qtdeEquipamentos) {
		this.qtdeEquipamentos = qtdeEquipamentos;
	}

	public Long getIdPotenciaFixa() {
		return idPotenciaFixa;
	}

	public void setIdPotenciaFixa(Long idPotenciaFixa) {
		this.idPotenciaFixa = idPotenciaFixa;
	}

	public String getDiasPorSemana() {
		return diasPorSemana;
	}

	public void setDiasPorSemana(String diasPorSemana) {
		this.diasPorSemana = diasPorSemana;
	}

	public String getHorasPorDia() {
		return horasPorDia;
	}

	public void setHorasPorDia(String horasPorDia) {
		this.horasPorDia = horasPorDia;
	}

	public String getPotencia() {
		return potencia;
	}

	public void setPotencia(String potencia) {
		this.potencia = potencia;
	}

	public Long getIdEquipamento() {
		return idEquipamento;
	}

	public void setIdEquipamento(Long idEquipamento) {
		this.idEquipamento = idEquipamento;
	}

	public String getDescricaoEquipamento() {
		return descricaoEquipamento;
	}

	public void setDescricaoEquipamento(String descricaoEquipamento) {
		this.descricaoEquipamento = descricaoEquipamento;
	}

	public String getDescricaoSegmento() {
		return descricaoSegmento;
	}

	public void setDescricaoSegmento(String descricaoSegmento) {
		this.descricaoSegmento = descricaoSegmento;
	}

	public String getObservacaoPontoConsumo() {
		return observacaoPontoConsumo;
	}

	public void setObservacaoPontoConsumo(String observacaoPontoConsumo) {
		this.observacaoPontoConsumo = observacaoPontoConsumo;
	}
	
	public boolean isIndicadorMensagemAdicional() {
		return indicadorMensagemAdicional;
	}

	public void setIndicadorMensagemAdicional(boolean indicadorMensagemAdicional) {
		this.indicadorMensagemAdicional = indicadorMensagemAdicional	;	
	}

	public String getDescricaoMensagemAdicional() {
		return descricaoMensagemAdicional;
	}

	public void setDescricaoMensagemAdicional(String descricaoMensagemAdicional) {
		this.descricaoMensagemAdicional = descricaoMensagemAdicional;	
	}
}
