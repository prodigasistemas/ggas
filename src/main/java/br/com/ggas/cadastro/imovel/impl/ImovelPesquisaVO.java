package br.com.ggas.cadastro.imovel.impl;

import java.io.Serializable;

import br.com.ggas.cadastro.cliente.Cliente;

/**
 * 
 * Classe responsável pela representação de valores de um Imóvel.
 * 
 * @author pedro
 *
 */
public class ImovelPesquisaVO implements Serializable {

	private static final long serialVersionUID = -8103701897076807999L;
	private Cliente cliente;
	private String nomeCliente;
	private String matriculaImovel;
	private String matriculaCondominio;
	private String nomeMedicao;
	private String cepImovel;
	private String idCepImovel;
	private Boolean indicadorCondominio;
	private Boolean tipoMedicao;
	private String indicadorCondominioAmbos;
	private String tipoMedicaoAmbos;
	private String nome;
	private String descricaoPontoConsumo;
	private String complementoImovel;
	private String numeroImovel;
	private Boolean habilitado;
	private Long idSegmento;
	private Long idGrupoFaturamento;
	private Long rota;
	private String pontoConsumoLegado;
	private Long idSegmentoPontoConsumo;
	private Long idRamoAtividadePontoConsumo;
	private String nomeFantasia;
	private Long[] chavesPrimarias;
	private String pesquisaImovelPai;
	private Long idImovelCondominio;
	private String nomeImovelCondominio;
	private Integer situacaoContrato;
	private Integer situacaoMedidor;
	private String dataInicioAssinaturaContrato;
	private String dataFimAssinaturaContrato;
	private String dataInicioAtivacaoMedidor;
	private String dataFimAtivacaoMedidor;
	private Long idSituacaoPontoConsumo;

	/**
	 * Metodo para recuperar se a pesquisa pertence a um imovel pai
	 * @return Se e uma pesquisa de imovel pai
	 */
	public String getPesquisaImovelPai() {
		return pesquisaImovelPai;
	}

	/**
	 * Metodo para preencher se a pesquisa eh de um imovel pai
	 * @param pesquisaImovelPai Novo valor para indicar se pesquisa eh uma pesquisa de movel pai
	 */
	public void setPesquisaImovelPai(final String pesquisaImovelPai) {
		this.pesquisaImovelPai = pesquisaImovelPai;
	}

	/**
	 * Metodo para recuperar o cliente da pesquisa
	 * @return cliente Cliente da pesquisa
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * Metodo para preencher o cliente da pesquisa
	 * @param cliente Cliente que sera preenchido
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * Metodo para recueprar a matricula de um imovel
	 * @return matricula Matricula do imovel para a pesquisa
	 */
	public String getMatriculaImovel() {
		return matriculaImovel;
	}


	/**
	 * Metodo para preencher a matricula do imovel para a pesquisa
	 * @param matricula Matricula do imovel para pesquisa
	 */
	public void setMatriculaImovel(String matriculaImovel) {
		this.matriculaImovel = matriculaImovel;
	}

	/**
	 * Metodo para recuperar a matricula do condomino para a pesquisa
	 * @return matriculaCondominio Matricula do condominio para a pesquisa
	 */
	public String getMatriculaCondominio() {
		return matriculaCondominio;
	}

	/**
	 *  Metodo para preencher a matricula da medicao da pesquisa
	 * @param matriculaCondominio Matricula da medicao da pesquisa
	 */
	public void setMatriculaCondominio(String matriculaCondominio) {
		this.matriculaCondominio = matriculaCondominio;
	}

	/**
	 * Metodo para preencher o nome da medicao da pesquisa
	 * @return nomeMedicao nome da medicao da pesquisa
	 */
	public String getNomeMedicao() {
		return nomeMedicao;
	}

	/**
	 * Metodo para alterar o nome da medicao da pesquisa
	 * @param nomeMedicao nome da medicao da pesquisa
	 */
	public void setNomeMedicao(String nomeMedicao) {
		this.nomeMedicao = nomeMedicao;
	}

	/**
	 * Metodo para alterar o cepimovel da medicao da pesquisa
	 * @return cepImovel cepimovel da medicao da pesquisa
	 */
	public String getCepImovel() {
		return cepImovel;
	}

	/**
	 * Metodo para alterar o cepimovel da medicao da pesquisa
	 * @param cepImovel Cepimovel da medicao da pesquisa
	 */
	public void setCepImovel(String cepImovel) {
		this.cepImovel = cepImovel;
	}

	/**
	 * Metodo para recuperar o indicador da medicao da pesquisa
	 * @return indicadorCondominio
	 */
	public Boolean getIndicadorCondominio() {
		return indicadorCondominio;
	}

	/**
	 * Metodo para alterar o indicador de condominio da medicao da pesquisa
	 * @param indicadorCondominio
	 */
	public void setIndicadorCondominio(Boolean indicadorCondominio) {
		this.indicadorCondominio = indicadorCondominio;
	}

	/**
	 *  Metodo para Recuperar o tipo da medicao da pesquisa
	 * @return tipoMedicao
	 */
	public Boolean getTipoMedicao() {
		return tipoMedicao;
	}

	/**
	 * Metodo para alterar o tipo da medicao da pesquisa
	 * @param tipoMedicao Novo tipo de medicao
	 */
	public void setTipoMedicao(Boolean tipoMedicao) {
		this.tipoMedicao = tipoMedicao;
	}

	/**
	 * Metodo para alterar o IndicadorCondominioAmbos da medicao da pesquisa
	 * @return indicadorCondominioAmbos IndicadorCondominioAmbos da medicao da pesquisa
	 */
	public String getIndicadorCondominioAmbos() {
		return indicadorCondominioAmbos;
	}

	/**
	 * Metodo para alterar o IndicadorCondominioAmbos da medicao da pesquisa
	 * @param indicadorCondominioAmbos IndicadorCondominioAmbos da medicao da pesquisa
	 */
	public void setIndicadorCondominioAmbos(String indicadorCondominioAmbos) {
		this.indicadorCondominioAmbos = indicadorCondominioAmbos;
	}

	/**
	 * Metodo para alterar o tipoMedicaoAmbos da medicao da pesquisa
	 * @return tipoMedicaoAmbos tipoMedicaoAmbos da medicao da pesquisa
	 */
	public String getTipoMedicaoAmbos() {
		return tipoMedicaoAmbos;
	}

	/**
	 * Metodo para alterar o tipoMedicaoAmbos da medicao da pesquisa
	 * @param tipoMedicaoAmbos tipoMedicaoAmbos da medicao da pesquisa
	 */
	public void setTipoMedicaoAmbos(String tipoMedicaoAmbos) {
		this.tipoMedicaoAmbos = tipoMedicaoAmbos;
	}

	/**
	 * Metodo para alterar o nome da medicao da pesquisa
	 * @return nome Nome da medicao da pesquisa
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Metodo para alterar o nome da medicao da pesquisa
	 * @param nome nome da medicao da pesquisa
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Metodo para alterar a DescricaoPontoConsumo da medicao da pesquisa
	 * @return descricaoPontoConsumo DescricaoPontoConsumo da medicao da pesquisa
	 */
	public String getDescricaoPontoConsumo() {
		return descricaoPontoConsumo;
	}

	/**
	 * Metodo para alterar a DescricaoPontoConsumo da medicao da pesquisa
	 * @param descricaoPontoConsumo DescricaoPontoConsumo da medicao da pesquisa
	 */
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {
		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	/**
	 * Metodo para alterar a ComplementoImovel da medicao da pesquisa
	 * @return complementoImovel ComplementoImovel da medicao da pesquisa
	 */
	public String getComplementoImovel() {
		return complementoImovel;
	}

	/**
	 * Metodo para alterar a ComplementoImovel da medicao da pesquisa
	 * @param complementoImovel ComplementoImovel da medicao da pesquisa
	 */
	public void setComplementoImovel(String complementoImovel) {
		this.complementoImovel = complementoImovel;
	}

	/**
	 * Metodo para alterar a NumeroImovel da medicao da pesquisa
	 * @return numeroImovel NumeroImovel da medicao da pesquisa
	 */
	public String getNumeroImovel() {
		return numeroImovel;
	}

	/**
	 * Metodo para alterar a NumeroImovel da medicao da pesquisa
	 * @param numeroImovel NumeroImovel da medicao da pesquisa
	 */
	public void setNumeroImovel(String numeroImovel) {
		this.numeroImovel = numeroImovel;
	}

	/**
	 * Metodo para alterar a Habilitado da medicao da pesquisa
	 * @return habilitado Habilitado da medicao da pesquisa
	 */
	public Boolean getHabilitado() {
		return habilitado;
	}

	/**
	 * Metodo para alterar a Habilitado da medicao da pesquisa
	 * @param habilitado Habilitado da medicao da pesquisa
	 */
	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	/**
	 * Metodo para alterar a IdSegmento da medicao da pesquisa
	 * @return Habilitado da IdSegmento da pesquisa
	 */
	public Long getIdSegmento() {
		return idSegmento;
	}

	/**
	 * Metodo para alterar a IdSegmento da medicao da pesquisa
	 * @param idSegmento IdSegmento da medicao da pesquisa
	 */
	public void setIdSegmento(Long idSegmento) {
		this.idSegmento = idSegmento;
	}

	/**
	 * Metodo para alterar a IdGrupoFaturamento da medicao da pesquisa
	 * @return Habilitado da medicao da pesquisa
	 */
	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}

	/**
	 * Metodo para alterar a idGrupoFaturamento da medicao da pesquisa
	 * @param idGrupoFaturamento
	 */
	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}

	/**
	 * Metodo para alterar a Rota da medicao da pesquisa
	 * @return Rota da medicao da pesquisa
	 */
	public Long getRota() {
		return rota;
	}

	/**
	 * Metodo para alterar a Rota da medicao da pesquisa
	 * @param rota Rota da medicao da pesquisa
	 */
	public void setRota(Long rota) {
		this.rota = rota;
	}

	/**
	 * Metodo para alterar a PontoConsumoLegado da medicao da pesquisa
	 * @return PontoConsumoLegado da medicao da pesquisa
	 */
	public String getPontoConsumoLegado() {
		return pontoConsumoLegado;
	}

	/**
	 * Metodo para alterar a PontoConsumoLegado da medicao da pesquisa
	 * @param pontoConsumoLegado PontoConsumoLegado da medicao da pesquisa
	 */
	public void setPontoConsumoLegado(String pontoConsumoLegado) {
		this.pontoConsumoLegado = pontoConsumoLegado;
	}

	/**
	 * Metodo para alterar a IdSegmentoPontoConsumo da medicao da pesquisa
	 * @return IdSegmentoPontoConsumo da medicao da pesquisa
	 */
	public Long getIdSegmentoPontoConsumo() {
		return idSegmentoPontoConsumo;
	}

	/**
	 * Metodo para alterar a IdSegmentoPontoConsumo da medicao da pesquisa
	 * @param idSegmentoPontoConsumo IdSegmentoPontoConsumo da medicao da pesquisa
	 */
	public void setIdSegmentoPontoConsumo(Long idSegmentoPontoConsumo) {
		this.idSegmentoPontoConsumo = idSegmentoPontoConsumo;
	}

	/**
	 * Metodo para alterar a IdRamoAtividadePontoConsumo da medicao da pesquisa
	 * @return IdRamoAtividadePontoConsumo da medicao da pesquisa
	 */
	public Long getIdRamoAtividadePontoConsumo() {
		return idRamoAtividadePontoConsumo;
	}

	/**
	 * Metodo para alterar a IdRamoAtividadePontoConsumo da medicao da pesquisa
	 * @param idRamoAtividadePontoConsumo IdRamoAtividadePontoConsumo da medicao da pesquisa
	 */
	public void setIdRamoAtividadePontoConsumo(Long idRamoAtividadePontoConsumo) {
		this.idRamoAtividadePontoConsumo = idRamoAtividadePontoConsumo;
	}

	/**
	 * Metodo para alterar a NomeFantasia da medicao da pesquisa
	 * @return NomeFantasia da medicao da pesquisa
	 */
	public String getNomeFantasia() {
		return nomeFantasia;
	}

	/**
	 *  Metodo para alterar a NomeFantasia da medicao da pesquisa
	 * @param nomeFantasia NomeFantasia da medicao da pesquisa
	 */
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	/**
	 * NomeFantasia da NomeCliente da pesquisa
	 * @return NomeCliente da pesquisa
	 */
	public String getNomeCliente() {
		return nomeCliente;
	}

	/**
	 * NomeCliente da NomeCliente da pesquisa
	 * @param nomeCliente NomeFantasia da medicao da pesquisa
	 */
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	/**
	 * NomeCliente da ChavesPrimarias da pesquisa
	 * @return ChavesPrimarias da NomeCliente da pesquisa
	 */
	public Long[] getChavesPrimarias() {
		Long[] retorno = null;
		if (this.chavesPrimarias != null) {
			retorno = this.chavesPrimarias.clone();
		}
		return retorno;
	}

	/**
	 * NomeCliente da ChavesPrimarias da pesquisa
	 * @param chavesPrimarias NomeCliente da ChavesPrimarias da pesquisa
	 */
	public void setChavesPrimarias(Long[] chavesPrimarias) {
		if (chavesPrimarias != null) {
			this.chavesPrimarias = chavesPrimarias.clone();
		}
	}

	/**
	 * NomeCliente da IdCepImovel da pesquisa
	 * @return IdCepImovel da pesquisa
	 */
	public String getIdCepImovel() {
		return idCepImovel;
	}

	/**
	 * NomeCliente da IdCepImovel da pesquisa
	 * @param idCepImovel NomeCliente da IdCepImovel da pesquisa
	 */
	public void setIdCepImovel(String idCepImovel) {
		this.idCepImovel = idCepImovel;
	}

	/**
	 * NomeCliente da IdImovelCondominio da pesquisa
	 * @return IdImovelCondominio da pesquisa
	 */
	public Long getIdImovelCondominio() {
		return idImovelCondominio;
	}

	/**
	 * NomeCliente da IdImovelCondominio da pesquisa
	 * @param idImovelCondominio da pesquisa
	 */
	public void setIdImovelCondominio(Long idImovelCondominio) {
		this.idImovelCondominio = idImovelCondominio;
	}

	/**
	 * NomeImovelCondominio da IdImovelCondominio da pesquisa
	 * @return NomeImovelCondominio da pesquisa
	 */
	public String getNomeImovelCondominio() {
		return nomeImovelCondominio;
	}

	/**
	 * NomeImovelCondominio da IdImovelCondominio da pesquisa
	 * @param nomeImovelCondominio IdImovelCondominio da pesquisa
	 */
	public void setNomeImovelCondominio(String nomeImovelCondominio) {
		this.nomeImovelCondominio = nomeImovelCondominio;
	}

	public Integer getSituacaoContrato() {
		return situacaoContrato;
	}

	public void setSituacaoContrato(Integer situacaoContrato) {
		this.situacaoContrato = situacaoContrato;
	}

	public Integer getSituacaoMedidor() {
		return situacaoMedidor;
	}

	public void setSituacaoMedidor(Integer situacaoMedidor) {
		this.situacaoMedidor = situacaoMedidor;
	}

	public String getDataInicioAssinaturaContrato() {
		return dataInicioAssinaturaContrato;
	}

	public void setDataInicioAssinaturaContrato(String dataInicioAssinaturaContrato) {
		this.dataInicioAssinaturaContrato = dataInicioAssinaturaContrato;
	}

	public String getDataFimAssinaturaContrato() {
		return dataFimAssinaturaContrato;
	}

	public void setDataFimAssinaturaContrato(String dataFimAssinaturaContrato) {
		this.dataFimAssinaturaContrato = dataFimAssinaturaContrato;
	}

	public String getDataInicioAtivacaoMedidor() {
		return dataInicioAtivacaoMedidor;
	}

	public void setDataInicioAtivacaoMedidor(String dataInicioAtivacaoMedidor) {
		this.dataInicioAtivacaoMedidor = dataInicioAtivacaoMedidor;
	}

	public String getDataFimAtivacaoMedidor() {
		return dataFimAtivacaoMedidor;
	}

	public void setDataFimAtivacaoMedidor(String dataFimAtivacaoMedidor) {
		this.dataFimAtivacaoMedidor = dataFimAtivacaoMedidor;
	}

	public Long getIdSituacaoPontoConsumo() {
		return idSituacaoPontoConsumo;
	}

	public void setIdSituacaoPontoConsumo(Long idSituacaoPontoConsumo) {
		this.idSituacaoPontoConsumo = idSituacaoPontoConsumo;
	}
}
