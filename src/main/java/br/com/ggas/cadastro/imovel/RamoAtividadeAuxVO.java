package br.com.ggas.cadastro.imovel;

import br.com.ggas.medicao.rota.Periodicidade;

/**
 * Classe VO utilizada para transferência de dados entre os métodos de Segmento e RamoAtividade
 * 
 * @author Pedro
 */
public class RamoAtividadeAuxVO {

	private Long chavePrimaria;
	private String contaContabil;
	private String operacao;
	private String operacaoSubstituicao;
	private boolean retornou;
	private boolean executado;
	private Long idRamoAtividade;
	private Long idSubsTributaria;
	private String ramoDescricao;
	private Long[] listaSegmentoAmostragemPCSSelecionados;
	private Long[] listaSegmentoIntervaloPCSSelecionados;
	private Long[] ramoAtividadeAmostragemPCS;
	private Long[] ramoAtividadeIntervaloPCS;
	private Long[] listaSubsTributaria;
	private Integer volumeMedioEstouroRamo;
	private Integer tamanhoReducaoRamo;
	private Periodicidade ramoPeriodicidade;
	
	/**
	 * 
	 * @return chavePrimaria
	 */
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	
	/**
	 * 
	 * @param chavePrimaria
	 */
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	
	/**
	 * 
	 * @return contaContabil
	 */
	public String getContaContabil() {
		return contaContabil;
	}
	
	/**
	 * 
	 * @param contaContabil
	 */
	public void setContaContabil(String contaContabil) {
		this.contaContabil = contaContabil;
	}
	
	/**
	 * 
	 * @return operacao
	 */
	public String getOperacao() {
		return operacao;
	}
	
	/**
	 * 
	 * @param operacao
	 */
	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}
	
	/**
	 * 
	 * @return operacaoSubstituicao
	 */
	public String getOperacaoSubstituicao() {
		return operacaoSubstituicao;
	}
	
	/**
	 * 
	 * @param operacaoSubstituicao
	 */
	public void setOperacaoSubstituicao(String operacaoSubstituicao) {
		this.operacaoSubstituicao = operacaoSubstituicao;
	}
	
	/**
	 * 
	 * @return retornou
	 */
	public boolean isRetornou() {
		return retornou;
	}
	
	/**
	 * 
	 * @param retornou
	 */
	public void setRetornou(boolean retornou) {
		this.retornou = retornou;
	}
	
	/**
	 * 
	 * @return executado
	 */
	public boolean isExecutado() {
		return executado;
	}
	
	/**
	 * 
	 * @param executado
	 */
	public void setExecutado(boolean executado) {
		this.executado = executado;
	}
	
	/**
	 * 
	 * @return idRamoAtividade
	 */
	public Long getIdRamoAtividade() {
		return idRamoAtividade;
	}
	
	/**
	 * 
	 * @param idRamoAtividade
	 */
	public void setIdRamoAtividade(Long idRamoAtividade) {
		this.idRamoAtividade = idRamoAtividade;
	}
	
	/**
	 * 
	 * @return idSubsTributaria
	 */
	public Long getIdSubsTributaria() {
		return idSubsTributaria;
	}
	
	/**
	 * 
	 * @param idSubsTributaria
	 */
	public void setIdSubsTributaria(Long idSubsTributaria) {
		this.idSubsTributaria = idSubsTributaria;
	}
	
	/**
	 * 
	 * @return ramoDescricao
	 */
	public String getRamoDescricao() {
		return ramoDescricao;
	}
	/**
	 * 
	 * @param ramoDescricao
	 */
	public void setRamoDescricao(String ramoDescricao) {
		this.ramoDescricao = ramoDescricao;
	}
	
	/**
	 * 
	 * @return listaSegmentoAmostragemPCSSelecionados
	 */
	public Long[] getListaSegmentoAmostragemPCSSelecionados() {
		Long[] listaSegmentoAmostragem = null;
		if(this.listaSegmentoAmostragemPCSSelecionados != null) {
			listaSegmentoAmostragem = this.listaSegmentoAmostragemPCSSelecionados.clone();
		}
		return listaSegmentoAmostragem;
	}
	
	/**
	 * 
	 * @param listaSegmentoAmostragemPCSSelecionados
	 */
	public void setListaSegmentoAmostragemPCSSelecionados(Long[] listaSegmentoAmostragemPCSSelecionados) {
		if(listaSegmentoAmostragemPCSSelecionados != null) {
			this.listaSegmentoAmostragemPCSSelecionados = listaSegmentoAmostragemPCSSelecionados.clone();
		} else {
			this.listaSegmentoAmostragemPCSSelecionados = null;
		}
	}
	
	/**
	 * 
	 * @return listaSegmentoIntervaloPCSSelecionados
	 */
	public Long[] getListaSegmentoIntervaloPCSSelecionados() {
		Long[] listaSegmentoIntervalo = null;
		if(this.listaSegmentoIntervaloPCSSelecionados != null) {
			listaSegmentoIntervalo = this.listaSegmentoIntervaloPCSSelecionados.clone();
		}
		return listaSegmentoIntervalo;
	}
	
	/**
	 * 
	 * @param listaSegmentoIntervaloPCSSelecionados
	 */
	public void setListaSegmentoIntervaloPCSSelecionados(Long[] listaSegmentoIntervaloPCSSelecionados) {
		if(listaSegmentoIntervaloPCSSelecionados != null) {
			this.listaSegmentoIntervaloPCSSelecionados = listaSegmentoIntervaloPCSSelecionados.clone();
		} else {
			this.listaSegmentoIntervaloPCSSelecionados = null;
		}
	}
	
	/**
	 * 
	 * @return ramoAtividadeAmostragemPCS
	 */
	public Long[] getRamoAtividadeAmostragemPCS() {
		Long[] ramoAtividadeAmostragem = null;
		if(this.ramoAtividadeAmostragemPCS != null) {
			ramoAtividadeAmostragem = ramoAtividadeAmostragemPCS.clone();
		}
		return ramoAtividadeAmostragem;
	}
	
	/**
	 * 
	 * @param ramoAtividadeAmostragemPCS
	 */
	public void setRamoAtividadeAmostragemPCS(Long[] ramoAtividadeAmostragemPCS) {
		if(ramoAtividadeAmostragemPCS != null) {
			this.ramoAtividadeAmostragemPCS = ramoAtividadeAmostragemPCS.clone();
		} else {
			this.ramoAtividadeAmostragemPCS = null;
		}
	}
	
	/**
	 * 
	 * @return ramoAtividadeIntervaloPCS
	 */
	public Long[] getRamoAtividadeIntervaloPCS() {
		Long[] ramoAtividadeIntervalo = null;
		if(this.ramoAtividadeIntervaloPCS != null) {
			ramoAtividadeIntervalo = ramoAtividadeIntervaloPCS.clone();
		}
		return ramoAtividadeIntervalo;
	}
	
	/**
	 * 
	 * @param ramoAtividadeIntervaloPCS
	 */
	public void setRamoAtividadeIntervaloPCS(Long[] ramoAtividadeIntervaloPCS) {
		if(ramoAtividadeIntervaloPCS != null) {
			this.ramoAtividadeIntervaloPCS = ramoAtividadeIntervaloPCS.clone();
		} else {
			this.ramoAtividadeIntervaloPCS = null;
		}
			
	}
	
	/**
	 * 
	 * @return listaSubsTributaria
	 */
	public Long[] getListaSubsTributaria() {
		Long[] listaSubstituicao = null;
		if(this.listaSubsTributaria != null) {
			listaSubstituicao = listaSubsTributaria.clone();
		}
		return listaSubstituicao;
	}
	
	/**
	 * 
	 * @param listaSubsTributaria
	 */
	public void setListaSubsTributaria(Long[] listaSubsTributaria) {
		if(listaSubsTributaria != null) {
			this.listaSubsTributaria = listaSubsTributaria.clone();
		} else {
			this.listaSubsTributaria = null;
		}
	}
	
	/**
	 * 
	 * @return volumeMedioEstouroRamo
	 */
	public Integer getVolumeMedioEstouroRamo() {
		return volumeMedioEstouroRamo;
	}
	
	/**
	 * 
	 * @param volumeMedioEstouroRamo
	 */
	public void setVolumeMedioEstouroRamo(Integer volumeMedioEstouroRamo) {
		this.volumeMedioEstouroRamo = volumeMedioEstouroRamo;
	}
	
	/**
	 * 
	 * @return tamanhoReducaoRamo
	 */
	public Integer getTamanhoReducaoRamo() {
		return tamanhoReducaoRamo;
	}
	
	/**
	 * 
	 * @param tamanhoReducaoRamo
	 */
	public void setTamanhoReducaoRamo(Integer tamanhoReducaoRamo) {
		this.tamanhoReducaoRamo = tamanhoReducaoRamo;
	}
	
	/**
	 * 
	 * @return ramoPeriodicidade
	 */
	public Periodicidade getRamoPeriodicidade() {
		return ramoPeriodicidade;
	}
	
	/**
	 * 
	 * @param ramoPeriodicidade
	 */
	public void setRamoPeriodicidade(Periodicidade ramoPeriodicidade) {
		this.ramoPeriodicidade = ramoPeriodicidade;
	}
}
