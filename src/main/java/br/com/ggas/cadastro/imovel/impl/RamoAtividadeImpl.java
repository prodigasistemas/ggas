/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.RamoAtividadeAmostragemPCS;
import br.com.ggas.cadastro.imovel.RamoAtividadeIntervaloPCS;
import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.tabelaAuxiliar.impl.TabelaAuxiliarImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelos métodos relacionados
 * a identificação do ramo de ativiadade
 * 
 */
public class RamoAtividadeImpl extends TabelaAuxiliarImpl implements RamoAtividade {

	private static final int LIMITE_CAMPO = 2;

	private static final int LIMITE_DESCRICAO = 100;

	private EntidadeConteudo tipoConsumoFaturamento;

	private Collection<RamoAtividadeSubstituicaoTributaria> ramoAtividadeSubstituicaoTributaria = 
					new HashSet<>();

	private Collection<RamoAtividadeAmostragemPCS> ramoAtividadeAmostragemPCS = new HashSet<>();

	private Collection<RamoAtividadeIntervaloPCS> ramoAtividadeIntervaloPCS = new HashSet<>();

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 6984422700460919024L;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.tabelaAuxiliar.impl.TabelaAuxiliarImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder camposObrigatoriosAcumulados = new StringBuilder();
		StringBuilder tamanhoCamposAcumulados = new StringBuilder();
		String camposObrigatorios = null;
		String camposInvalidos = null;

		if(super.getDescricao() == null || super.getDescricao().length() == 0) {
			camposObrigatoriosAcumulados.append(DESCRICAO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if(super.getDescricao().length() > LIMITE_DESCRICAO) {
				tamanhoCamposAcumulados.append(DESCRICAO);
				tamanhoCamposAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}
		if(this.getSegmento() == null) {
			camposObrigatoriosAcumulados.append(SEGMENTO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(tipoConsumoFaturamento == null) {
			camposObrigatoriosAcumulados.append(ERRO_TIPO_CONSUMO_FATURAMENTO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = camposObrigatoriosAcumulados.toString();
		camposInvalidos = tamanhoCamposAcumulados.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
							camposObrigatorios.substring(0, camposObrigatoriosAcumulados.toString().length() - LIMITE_CAMPO));
		}

		if(camposInvalidos.length() > 0) {
			erros.put(Constantes.ERRO_TAMANHO_LIMITE, camposInvalidos.substring(0, tamanhoCamposAcumulados.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	@Override
	public Collection<RamoAtividadeSubstituicaoTributaria> getRamoAtividadeSubstituicaoTributaria() {

		return this.ramoAtividadeSubstituicaoTributaria;
	}

	@Override
	public void setRamoAtividadeSubstituicaoTributaria(Collection<RamoAtividadeSubstituicaoTributaria> ramosAtividadeSubstituicaoTributaria) {

		this.ramoAtividadeSubstituicaoTributaria = ramosAtividadeSubstituicaoTributaria;

	}

	@Override
	public Collection<RamoAtividadeAmostragemPCS> getRamoAtividadeAmostragemPCS() {

		return ramoAtividadeAmostragemPCS;
	}

	@Override
	public Collection<RamoAtividadeIntervaloPCS> getRamoAtividadeIntervaloPCS() {

		return ramoAtividadeIntervaloPCS;
	}

	@Override
	public void setRamoAtividadeIntervaloPCS(Collection<RamoAtividadeIntervaloPCS> ramoAtividadeIntervaloPCS) {

		this.ramoAtividadeIntervaloPCS = ramoAtividadeIntervaloPCS;

	}

	@Override
	public void setRamoAtividadeAmostragemPCS(Collection<RamoAtividadeAmostragemPCS> ramoAtividadeAmostragemPCS) {

		this.ramoAtividadeAmostragemPCS = ramoAtividadeAmostragemPCS;

	}

	@Override
	public EntidadeConteudo getTipoConsumoFaturamento() {
		return tipoConsumoFaturamento;
	}

	@Override
	public void setTipoConsumoFaturamento(EntidadeConteudo tipoConsumoFaturamento) {
		this.tipoConsumoFaturamento = tipoConsumoFaturamento;
	}
}
