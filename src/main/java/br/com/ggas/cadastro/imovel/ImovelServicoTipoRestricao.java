package br.com.ggas.cadastro.imovel;

import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Classe responsável pela representação de uma restrição de servico para o imovel
 *
 */
public interface ImovelServicoTipoRestricao extends EntidadeNegocio {

	String BEAN_ID_IMOVEL_SERVICO_TIPO_RESTRICAO = "imovelServicoTipoRestricao";

	/**
	 * @return the imovel
	 */
	public Imovel getImovel();

	/**
	 * @param imovel the imovel to set
	 */
	public void setImovel(Imovel imovel);

	/**
	 * @return the servicoTipo
	 */
	public ServicoTipo getServicoTipo();

	/**
	 * @param servicoTipo the servicoTipo to set
	 */
	public void setServicoTipo(ServicoTipo servicoTipo);

}
