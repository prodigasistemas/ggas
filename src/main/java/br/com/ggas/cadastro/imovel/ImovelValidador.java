/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import br.com.ggas.geral.exception.GGASRuntimeException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Constantes;
import com.google.common.base.Strings;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Contém a lógica de validação de um {@link Imovel}, a fim de garantir que a entidade esteja com sua regra de negócio
 * válida antes de ser persistida no banco
 *
 * @author jose.victor@logiquesistemas.com.br
 */
public class ImovelValidador {

	public static final int QTD_CHAR_PARA_REMOVER = 2;
	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	/**
	 * Valida as propriedades do imóvel
	 * @param imovel imovel a ser validado
	 *
	 * @throws NegocioException exceção lançada caso ocorra falha na validação
	 */
	public void validar(Imovel imovel) throws NegocioException {
		StringBuilder camposObrigatorios = new StringBuilder();

		if (StringUtils.isEmpty(imovel.getCep())) {
			camposObrigatorios.append(Imovel.CEP_IMOVEL);
			camposObrigatorios.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (imovel.getQuadra() == null) {
			camposObrigatorios.append(Imovel.QUADRA);
			camposObrigatorios.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (camposObrigatorios.length() > 0) {
			camposObrigatorios.insert(0, "da aba Identificação e Localização: ");
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, camposObrigatorios
					.toString().length() - QTD_CHAR_PARA_REMOVER));
		}

		if (CollectionUtils.isNotEmpty(imovel.getListaPontoConsumo())) {
			validarQtdMaximaPontosConsumoPorRota(imovel.getListaPontoConsumo());
		}
	}

	/**
	 * Verifica se os pontos de consumo do imóvel não irão extrapolar o número máximo de
	 * @param pontoConsumos lista de pontos de consumo
	 * @throws NegocioException exceção lançada caso haja erro de validação
	 */
	private void validarQtdMaximaPontosConsumoPorRota(Collection<PontoConsumo> pontoConsumos) throws NegocioException {
		final Collection<Rota> rotasDosPontosConsumo = pontoConsumos.stream().map(PontoConsumo::getRota)
				.filter(Objects::nonNull)
				.filter(rota -> Objects.nonNull(rota.getNumeroMaximoPontosConsumo()))
				.collect(Collectors.toMap(Rota::getChavePrimaria, rota -> rota, (r1, r2) -> r1))
				.values();

		final String rotasComMaximoPontosConsumoExtrapolado = rotasDosPontosConsumo.stream().filter(rota -> {

			final int numeroMaximoPontosConsumo = rota.getNumeroMaximoPontosConsumo();
			try {
				Collection<PontoConsumo> pontoConsumosRota = controladorPontoConsumo.consultarPontoConsumoPorRota(rota);
				final int qtdAtualPontosConsumoNaRota = pontoConsumosRota.size();
				final long qtdAdicional = pontoConsumos.stream().map(PontoConsumo::getRota).filter(Objects::nonNull)
						.filter(r -> Objects.equals(r.getChavePrimaria(), rota.getChavePrimaria())).count();
				return qtdAtualPontosConsumoNaRota + qtdAdicional > numeroMaximoPontosConsumo;
			} catch (NegocioException e) {
				throw new GGASRuntimeException(e);
			}

		}).map(Rota::getNumeroRota).collect(Collectors.joining(", "));


		if (!Strings.isNullOrEmpty(rotasComMaximoPontosConsumoExtrapolado)) {
			throw new NegocioException("ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO_NAS_ROTAS", rotasComMaximoPontosConsumoExtrapolado);
		}
	}

}
