/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.imovel.AreaConstruidaFaixa;
import br.com.ggas.cadastro.imovel.ControladorAreaConstruidaFaixa;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 *
 */
class ControladorAreaConstruidaFaixaImpl extends ControladorNegocioImpl implements ControladorAreaConstruidaFaixa {

	private static final String MAIOR_FAIXA = "maiorFaixa";
	private static final String MENOR_FAIXA = "menorFaixa";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AreaConstruidaFaixa.BEAN_ID_AREA_CONSTRUIDA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(AreaConstruidaFaixa.BEAN_ID_AREA_CONSTRUIDA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorAreaConstruidaFaixa
	 * #consultarAreaConstruidaFaixas
	 * (java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AreaConstruidaFaixa> consultarAreaConstruidaFaixas(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			Integer menorFaixa = (Integer) filtro.get(MENOR_FAIXA);
			if(menorFaixa != null) {
				criteria.add(Restrictions.eq(MENOR_FAIXA, menorFaixa));
			}
			Integer maiorFaixa = (Integer) filtro.get(MAIOR_FAIXA);
			if(maiorFaixa != null) {
				criteria.add(Restrictions.eq(MAIOR_FAIXA, maiorFaixa));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		validarIntervaloFaixa((AreaConstruidaFaixa) entidadeNegocio);
		validarFaixaExistente((AreaConstruidaFaixa) entidadeNegocio);
	}

	/**
	 * Validar intervalo faixa.
	 * 
	 * @param areaConstruidaFaixa
	 *            the area construida faixa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarIntervaloFaixa(AreaConstruidaFaixa areaConstruidaFaixa) throws NegocioException {

		if(areaConstruidaFaixa.getMenorFaixa() > areaConstruidaFaixa.getMaiorFaixa()) {
			throw new NegocioException(ControladorAreaConstruidaFaixa.ERRO_NEGOCIO_INTERVALO_FAIXA, true);
		}
	}

	/**
	 * Validar faixa existente.
	 * 
	 * @param areaConstruidaFaixa
	 *            the area construida faixa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarFaixaExistente(AreaConstruidaFaixa areaConstruidaFaixa) throws NegocioException {

		Map<String, Object> filtro = new HashMap<>();
		filtro.put(MENOR_FAIXA, areaConstruidaFaixa.getMenorFaixa());
		filtro.put(MAIOR_FAIXA, areaConstruidaFaixa.getMaiorFaixa());
		List<AreaConstruidaFaixa> areaConstruidaFaixas = (List<AreaConstruidaFaixa>) this.consultarAreaConstruidaFaixas(filtro);

		if(!areaConstruidaFaixas.isEmpty()) {
			AreaConstruidaFaixa areaConstruidaExistente = areaConstruidaFaixas.get(0);

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(areaConstruidaExistente);

			if(areaConstruidaExistente.getChavePrimaria() != areaConstruidaFaixa.getChavePrimaria()) {
				throw new NegocioException(ControladorAreaConstruidaFaixa.ERRO_NEGOCIO_FAIXA_EXISTENTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		validarIntervaloFaixa((AreaConstruidaFaixa) entidadeNegocio);
		validarFaixaExistente((AreaConstruidaFaixa) entidadeNegocio);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorAreaConstruidaFaixa
	 * #validarRemoverAreaConstruidaFaixas
	 * (java.lang.Long[])
	 */
	@Override
	public void validarRemoverAreaConstruidaFaixas(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorAreaConstruidaFaixa
	 * #listarAreaConstruidaFaixa()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AreaConstruidaFaixa> listarAreaConstruidaFaixa() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by menorFaixa, maiorFaixa ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();

	}
}
