package br.com.ggas.cadastro.operacional.impl;

import java.math.BigDecimal;
import java.util.Map;

import br.com.ggas.cadastro.operacional.RedeComprimento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class RedeComprimentoImpl extends EntidadeNegocioImpl implements RedeComprimento{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3561679598139500216L;
	
	private BigDecimal comprimento;
	private Integer referencia;

	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

	@Override
	public BigDecimal getComprimento() {
		return comprimento;
	}

	@Override
	public void setComprimento(BigDecimal comprimento) {
		this.comprimento = comprimento;
	}

	@Override
	public Integer getReferencia() {
		return referencia;
	}

	@Override
	public void setReferencia(Integer referencia) {
		this.referencia = referencia;
	}

}
