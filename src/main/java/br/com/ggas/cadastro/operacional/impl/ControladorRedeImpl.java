/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.cadastro.operacional.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.RedeInternaImovel;
import br.com.ggas.cadastro.localidade.Rede;
import br.com.ggas.cadastro.localidade.RedeDistribuicaoTronco;
import br.com.ggas.cadastro.localidade.RedeIndicador;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.ControladorRede;
import br.com.ggas.cadastro.operacional.RedeComprimento;
import br.com.ggas.cadastro.operacional.RedeDiametro;
import br.com.ggas.cadastro.operacional.RedeMaterial;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.geral.dominio.EntidadeDominio;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 *
 *
 */
class ControladorRedeImpl extends ControladorNegocioImpl implements ControladorRede {

	private static final String FROM = " from ";
	private static final int INDICE_DESCRICAO = 3;
	private static final int INDICE_CHAVE_PRIMARIA = 2;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Rede.BEAN_ID_REDE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRede
	 * #criarRedeDistribuicaoTronco()
	 */
	@Override
	public EntidadeNegocio criarRedeDistribuicaoTronco() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(RedeDistribuicaoTronco.BEAN_ID_REDE_DISTRIBUICAO_TRONCO);
	}

	@Override
	public Class<?> getClasseEntidadeRedeDistribuicaoTronco() {

		return ServiceLocator.getInstancia().getClassPorID(RedeDistribuicaoTronco.BEAN_ID_REDE_DISTRIBUICAO_TRONCO);
	}

	public Class<?> getClasseEntidadeRedeInternaImovel() {

		return ServiceLocator.getInstancia().getClassPorID(RedeInternaImovel.BEAN_ID_REDE_INTERNA_IMOVEL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Rede.BEAN_ID_REDE);
	}

	public Class<?> getClasseEntidadeUnidadeMedicao() {

		return ServiceLocator.getInstancia().getClassPorID(Unidade.BEAN_ID_UNIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRede#criarRedeIndicador()
	 */
	@Override
	public EntidadeDominio criarRedeIndicador() {

		return (EntidadeDominio) ServiceLocator.getInstancia().getBeanPorID(RedeIndicador.BEAN_ID_REDE_INDICADOR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRede#criarRedeMaterial()
	 */
	@Override
	public EntidadeNegocio criarRedeMaterial() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(RedeMaterial.BEAN_ID_REDE_MATERIAL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRede#criarRedeDiametro()
	 */
	@Override
	public EntidadeNegocio criarRedeDiametro() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(RedeDiametro.BEAN_ID_REDE_DIAMETRO);
	}

	public Class<?> getClasseEntidadeRedeMaterial() {

		return ServiceLocator.getInstancia().getClassPorID(RedeMaterial.BEAN_ID_REDE_MATERIAL);
	}

	public Class<?> getClasseEntidadeRedeDiametro() {

		return ServiceLocator.getInstancia().getClassPorID(RedeDiametro.BEAN_ID_REDE_DIAMETRO);
	}
	
	public Class<?> getClasseEntidadeRedeComprimento() {

		return ServiceLocator.getInstancia().getClassPorID(RedeComprimento.BEAN_ID_REDE_COMPRIMENTO);
	}
	
	
	@Override
	public EntidadeNegocio criarRedeComprimento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(RedeComprimento.BEAN_ID_REDE_COMPRIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.
	 * ControladorRede
	 * #validarRemoverRede(java.lang.Long
	 * [])
	 */
	@Override
	public void validarRemoverRede(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if(entidadeNegocio.getClass().getSimpleName().equals(getClasseEntidade().getSimpleName())) {
			Rede rede = (Rede) entidadeNegocio;
			this.validarRedeSemTronco(rede.getListaRedeTronco());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if(entidadeNegocio.getClass().getSimpleName().equals(getClasseEntidade().getSimpleName())) {
			Rede rede = (Rede) entidadeNegocio;
			this.validarRedeSemTronco(rede.getListaRedeTronco());
		}
	}

	/**
	 * Validar rede sem tronco.
	 * 
	 * @param listaRedeTronco
	 *            the lista rede tronco
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarRedeSemTronco(Collection<RedeDistribuicaoTronco> listaRedeTronco) throws NegocioException {

		if(listaRedeTronco == null || listaRedeTronco.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_REDE_SEM_TRONCO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorQuadra
	 * #listarTodosIndicadoresRede()
	 */
	@Override
	public Collection<RedeIndicador> listarTodosIndicadoresRede() {

		List<RedeIndicador> indicadores = new ArrayList<>();
		RedeIndicador redeIndicador = null;
		Set<?> mapa = RedeIndicador.INDICADORES_REDE.keySet();

		for (Iterator<?> iterator = mapa.iterator(); iterator.hasNext();) {
			Integer codigo = (Integer) iterator.next();
			redeIndicador = (RedeIndicador) this.criarRedeIndicador();
			redeIndicador.setCodigo(codigo);
			indicadores.add(redeIndicador);
		}

		Collections.sort(indicadores, new Comparator<RedeIndicador>(){

			@Override
			public int compare(RedeIndicador o1, RedeIndicador o2) {

				return Integer.valueOf(o1.getCodigo()).compareTo(Integer.valueOf(o2.getCodigo()));
			}
		});

		return indicadores;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.localidade.
	 * ControladorQuadra
	 * #obterRedeIndicador(java.lang.Integer
	 * )
	 */
	@Override
	public RedeIndicador obterRedeIndicador(Integer codigo) {

		RedeIndicador redeIndicador = null;
		if(codigo != null && codigo > 0) {
			redeIndicador = (RedeIndicador) this.criarRedeIndicador();
			redeIndicador.setCodigo(codigo);

		}

		return redeIndicador;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRede
	 * #consultarRedes(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Rede> consultarRedes(Map<String, Object> filtro) throws NegocioException {


		Criteria criteria = getCriteria();
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			String descricao = (String) filtro.get("descricao");
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike("descricao", Util.formatarTextoConsulta(descricao)));
			}
			criteria.createAlias("localidade", "localidade");
			Long idLocalidade = (Long) filtro.get("idLocalidade");
			if(idLocalidade != null && idLocalidade > 0) {
				criteria.add(Restrictions.eq("localidade.chavePrimaria", idLocalidade));
			}
			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}
			Long idRedeMaterial = (Long) filtro.get("idRedeMaterial");
			if(idRedeMaterial != null && (idRedeMaterial > 0)) {
				criteria.add(Restrictions.eq("redeMaterial.chavePrimaria", idRedeMaterial));
			}
			Long idRedeDiametro = (Long) filtro.get("idRedeDiametro");
			if(idRedeDiametro != null && (idRedeDiametro > 0)) {
				criteria.add(Restrictions.eq("redeDiametro.chavePrimaria", idRedeDiametro));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRede#listarRedeDiametro()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<RedeDiametro> listarRedeDiametro() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeRedeDiametro().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRede#listarRedeMaterial()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<RedeMaterial> listarRedeMaterial() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeRedeMaterial().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRede#obterRedeDiametro(long)
	 */
	@Override
	public RedeDiametro obterRedeDiametro(long chavePrimaria) throws NegocioException {

		return (RedeDiametro) super.obter(chavePrimaria, getClasseEntidadeRedeDiametro());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRede#obterRedeMaterial(long)
	 */
	@Override
	public RedeMaterial obterRedeMaterial(long chavePrimaria) throws NegocioException {

		return (RedeMaterial) super.obter(chavePrimaria, getClasseEntidadeRedeMaterial());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.
	 * ControladorRede
	 * #validarDadosRedeDistribuicaoTronco
	 * (br.com.ggas.cadastro.localidade.
	 * RedeDistribuicaoTronco)
	 */
	@Override
	public void validarDadosRedeDistribuicaoTronco(RedeDistribuicaoTronco redeTronco) throws NegocioException {

		Map<String, Object> errosRedeDistribuicaoTronco = redeTronco.validarDados();

		if(errosRedeDistribuicaoTronco != null && !errosRedeDistribuicaoTronco.isEmpty()) {
			throw new NegocioException(errosRedeDistribuicaoTronco);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional. ControladorRede
	 * #validarTroncoPorCityGate(br.com .procenge.ggas.cadastro.localidade.
	 * RedeDistribuicaoTronco, java.util.Collection)
	 */
	@Override
	public void validarTroncoPorCityGate(RedeDistribuicaoTronco redeTronco, Collection<RedeDistribuicaoTronco> listaRedeTronco)
					throws NegocioException {

		if(redeTronco != null || (listaRedeTronco != null && !listaRedeTronco.isEmpty())) {
			for (Iterator<?> iterator = listaRedeTronco.iterator(); iterator.hasNext();) {
				RedeDistribuicaoTronco redeDistribuicaoTronco = (RedeDistribuicaoTronco) iterator.next();
				if(redeTronco != null && redeTronco.getTronco().getCityGate()
						.getChavePrimaria() == redeDistribuicaoTronco.getTronco().getCityGate().getChavePrimaria()) {
					throw new NegocioException(ERRO_NEGOCIO_TRONCO_CADASTRADO_NESTE_CITYGATE, true);
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.
	 * ControladorRede
	 * #listarRedeTroncoPorRede(br.com.
	 * procenge.ggas.cadastro.localidade.Rede)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<RedeDistribuicaoTronco> listarRedeTroncoPorRede(Rede rede) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select redeTronco");
		hql.append(FROM);
		hql.append(getClasseEntidadeRedeDistribuicaoTronco().getSimpleName());
		hql.append(" redeTronco ");
		hql.append(" inner join fetch redeTronco.tronco tronco ");
		hql.append(" inner join fetch tronco.cityGate cityGate ");
		hql.append(" where ");
		hql.append(" redeTronco.rede.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, rede.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.ControladorRede#consultarRedeDistribuicaoTronco(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Rede> consultarRedeDistribuicaoTronco(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeRedeDistribuicaoTronco());
		if(filtro != null) {
			Long idTronco = (Long) filtro.get("idTronco");
			if(idTronco != null && idTronco > 0) {
				criteria.add(Restrictions.eq("tronco.chavePrimaria", idTronco));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.ControladorRede#consultarRedeInternaImovel(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<RedeInternaImovel> consultarRedeInternaImovel(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeRedeInternaImovel());
		if(filtro != null) {
			Long idRedeMaterial = (Long) filtro.get("idRedeMaterial");
			if(idRedeMaterial != null && idRedeMaterial > 0) {
				criteria.add(Restrictions.eq("redeMaterial.chavePrimaria", idRedeMaterial));
			}
		}

		return criteria.list();
	}
	
	/**
	 * Obtém uma lista de Troncos por Rede, consultando  os 
	 * seguintes atributos: {@code chavePrimariaTronco}, 
	 * {@code chavePrimariaRede}, {@code chavePrimariaCityGate}, 
	 * {@code descricaoCityGate}.
	 * @param chavesPrimariasRede
	 * @return mapa de Troncos por Rede
	 */
	@Override
	public Map<Rede, List<Tronco>> consultarTroncosDaRede(Long[] chavesPrimariasRede) {
	
		StringBuilder hql = new StringBuilder();
		hql.append(" select tronco.chavePrimaria, rede.chavePrimaria, ");
		hql.append(" cityGate.chavePrimaria, cityGate.descricao ");
		hql.append(FROM);
		hql.append(getClasseEntidadeRedeDistribuicaoTronco().getSimpleName());
		hql.append(" redeDistribuicao ");
		hql.append(" inner join redeDistribuicao.rede rede ");
		hql.append(" inner join redeDistribuicao.tronco tronco ");
		hql.append(" left join tronco.cityGate cityGate ");
		hql.append(" where rede.chavePrimaria IN (:chavesPrimarias) ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("chavesPrimarias", chavesPrimariasRede);
		
		return this.construirMapaTroncoPorRede( query.list() );
	}
	
	/**
	 * Constrói o mapa de lista de Troncos por Rede, a partir da 
	 * lista de {@code atributos} passados como parâmetro.
	 * @param listaAtributos
	 * @return mapa de Troncos por Rede
	 */
	private Map<Rede, List<Tronco>>  construirMapaTroncoPorRede(List<Object[]> listaAtributos) {
		Map<Rede, List<Tronco>> mapaTroncoPorRede = new HashMap<>();
		List<Tronco> listaTronco;
		for ( Object[] atributos : listaAtributos) {
			Tronco tronco = new TroncoImpl();
			tronco.setChavePrimaria((long) atributos[0]);
			Rede rede = (Rede) criar();
			rede.setChavePrimaria((long) atributos[1]);
			CityGate cityGate = new CityGateImpl();
			cityGate.setChavePrimaria((long) atributos[INDICE_CHAVE_PRIMARIA]);
			cityGate.setDescricao((String) atributos[INDICE_DESCRICAO]);
			tronco.setCityGate(cityGate);
			if ( !mapaTroncoPorRede.containsKey(rede) ) {
				listaTronco = new ArrayList<>();
				mapaTroncoPorRede.put(rede, listaTronco);
			} else {
				listaTronco = mapaTroncoPorRede.get(rede);
			}
			listaTronco.add(tronco);

		}
		return mapaTroncoPorRede;
		
	}

	/**
	 * Obtém as chaves das Redes dos pontos de consumo
	 * especificados no parâmetro.
	 * 
	 * @param pontosConsumo
	 * @return redes dos pontos de consumo
	 **/
	@Override
	public Long[] obterChavesDeRedesDePontosDeConsumo(Collection<PontoConsumo> pontosConsumo) {

		List<Long> novoArray = new ArrayList<>();
		for (PontoConsumo pontoConsumo : pontosConsumo) {
			Imovel imovel = pontoConsumo.getImovel();
			if (imovel != null && imovel.getQuadraFace() != null) {
				Rede rede = imovel.getQuadraFace().getRede();
				if (rede != null && !novoArray.contains(rede.getChavePrimaria())) {
					novoArray.add(rede.getChavePrimaria());
				}
			}
		}

		Long[] chaves = new Long[novoArray.size()];
		novoArray.toArray(chaves);

		return chaves;
	}

	@Override
	public Collection<RedeComprimento> consultarRedeComprimentoPorAno(String ano) {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select redeComprimento ");
		hql.append(FROM);
		hql.append(getClasseEntidadeRedeComprimento().getSimpleName());
		hql.append(" redeComprimento ");
		hql.append(" where ");
		hql.append(" redeComprimento.referencia like ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, "%"+ano+"%");

		return query.list();
	}
	
	@Override
	public void incluirRedeComprimento(List<String> comprimentos, String ano) throws NegocioException {
		this.deletarRedeComprimentoAno(ano);

		int i = 1;
		for (String comprimento : comprimentos) {
			String referenciaStr = ano + String.format("%02d", i);
			Integer referencia = Integer.valueOf(referenciaStr);
			RedeComprimento redeComprimento = Fachada.getInstancia().criarRedeComprimento();
			redeComprimento.setReferencia(referencia);
			
			if(StringUtils.isNotEmpty(comprimento)) {
				redeComprimento.setComprimento(new BigDecimal(comprimento.replace(".", "").replace(",", ".")));
			}
			
			this.inserir(redeComprimento);

			i++;
		}
	}

	private void deletarRedeComprimentoAno(String ano) {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" delete ");
		hql.append(FROM);
		hql.append(getClasseEntidadeRedeComprimento().getSimpleName());
		hql.append(" redeComprimento ");
		hql.append(" where ");
		hql.append(" redeComprimento.referencia like ?");
		
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, "%"+ano+"%");
		query.executeUpdate();
	}
	
}
