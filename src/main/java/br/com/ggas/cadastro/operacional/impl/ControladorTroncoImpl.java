/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.operacional.impl;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.ControladorRamal;
import br.com.ggas.cadastro.operacional.ControladorTronco;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 *
 */
class ControladorTroncoImpl extends ControladorNegocioImpl implements ControladorTronco {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Tronco.BEAN_ID_TRONCO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Tronco.BEAN_ID_TRONCO);
	}

	public Class<?> getClasseEntidadeCityGate() {

		return ServiceLocator.getInstancia().getClassPorID(CityGate.BEAN_ID_CITY_GATE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorTronco
	 * #consultarTronco(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Tronco> consultarTronco(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String nome = (String) filtro.get("nome");
			if (!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.ilike("nome", nome, MatchMode.ANYWHERE));
			}

			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if (habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}

			Long idCityGate = (Long) filtro.get("idCityGate");
			if ((idCityGate) != null && (idCityGate > 0)) {
				criteria.createCriteria("cityGate").add(Restrictions.eq("chavePrimaria", idCityGate));
			}
		}

		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preRemocao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Tronco tronco = (Tronco) entidadeNegocio;
		ControladorRamal controladorRamal = ServiceLocator.getInstancia().getControladorRamal();
		Integer quantidade = controladorRamal.consultarQuantidadeTroncoPeloRamal(tronco.getChavePrimaria());
		if (quantidade > 0) {
			throw new NegocioException(ERRO_INTEGRIDADE_RELACIONAL, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.ControladorTronco#listarCityGateExistente()
	 */
	@Override
	public Collection<CityGate> listarCityGateExistente() {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCityGate().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return (Collection<CityGate>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorTronco#obterCityGate(long)
	 */
	@Override
	public CityGate obterCityGate(long chavePrimaria) throws NegocioException {

		return (CityGate) this.obter(chavePrimaria, getClasseEntidadeCityGate());
	}

}
