package br.com.ggas.cadastro.operacional;

import java.math.BigDecimal;

import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface RedeComprimento extends EntidadeNegocio{
	String BEAN_ID_REDE_COMPRIMENTO = "redeComprimento";

	BigDecimal getComprimento();

	void setComprimento(BigDecimal comprimento);

	Integer getReferencia();

	void setReferencia(Integer referencia);

}
