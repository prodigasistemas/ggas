/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.operacional.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.operacional.ControladorErp;
import br.com.ggas.cadastro.operacional.Erp;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 *
 */
class ControladorErpImpl extends ControladorNegocioImpl implements ControladorErp {

	private static final String ATRIBUTO_LOCALIDADE = "localidade";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Erp.BEAN_ID_ERP);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Erp.BEAN_ID_ERP);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Erp erp = (Erp) entidadeNegocio;
		validarErpExistente(erp);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.ControladorErp#consultarLocalidadesDasERP()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Localidade> consultarLocalidadesDasERP() throws NegocioException {


		Criteria criteria = getCriteria();
		criteria.setProjection(Projections.property(ATRIBUTO_LOCALIDADE));
		criteria.add(Restrictions.eq(EntidadeClasse.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.setProjection(Projections.groupProperty(ATRIBUTO_LOCALIDADE).as("loca")).addOrder(Order.asc("loca"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.ControladorErp
	 * #consultarERP(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Erp> consultarERP(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
			}

			String nome = (String) filtro.get("nome");
			if(!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.ilike("nome", nome, MatchMode.ANYWHERE));
			}

			Long idLocalidade = (Long) filtro.get("idLocalidade");
			if(idLocalidade != null && idLocalidade > 0) {
				criteria.createAlias(ATRIBUTO_LOCALIDADE, ATRIBUTO_LOCALIDADE);
				criteria.add(Restrictions.eq("localidade.chavePrimaria", idLocalidade));
			} else {
				criteria.setFetchMode(ATRIBUTO_LOCALIDADE, FetchMode.JOIN);
			}

			Boolean medicao = (Boolean) filtro.get("medicao");
			if(medicao != null) {
				criteria.add(Restrictions.eq("medicao", medicao));
			}

			Boolean pressao = (Boolean) filtro.get("pressao");
			if(pressao != null) {
				criteria.add(Restrictions.eq("pressao", pressao));
			}
			Boolean habilitado = (Boolean) filtro.get(EntidadeClasse.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeClasse.ATRIBUTO_HABILITADO, habilitado));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.ControladorErp
	 * #validarERPsSelecionadas(java.lang.Long[])
	 */
	@Override
	public void validarERPsSelecionadas(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/**
	 * Método responsável por validar Erp para nao
	 * haver insercao repetida.
	 * 
	 * @param erp
	 *            the erp
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	@SuppressWarnings("unchecked")
	private void validarErpExistente(Erp erp) throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.add(Restrictions.ilike("nome", erp.getNome(), MatchMode.EXACT));
		List<Erp> listaErp = criteria.list();

		if(!listaErp.isEmpty()) {
			Erp erpExistente = listaErp.get(0);

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(erpExistente);

			if(erpExistente.getChavePrimaria() != erp.getChavePrimaria()) {
				throw new NegocioException(ControladorErp.ERRO_NEGOCIO_ESTACAO_EXISTENTE, true);
			}
		}
	}

}
