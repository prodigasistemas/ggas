/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.operacional;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador de Ramal
 *
 */
public interface ControladorRamal extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_RAMAL = "controladorRamal";

	String ERRO_NEGOCIO_RAMAL_EXISTENTE = "ERRO_NEGOCIO_RAMAL_EXISTENTE";

	String ERRO_INTEGRIDADE_RELACIONAL = "ERRO_INTEGRIDADE_RELACIONAL";

	/**
	 * Metodo responsavel por consultar Ramal a
	 * partir de um filtro informado.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Ramal> consultarRamal(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se foi
	 * selecionada apenas um ramal para alteração.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos ramais
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarSelecaoDeAtualizacao(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por consultar a
	 * quantidade de Tronco a partir de Ramal.
	 * 
	 * @param chaveRamal
	 *            the chave ramal
	 * @return the integer
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Integer consultarQuantidadeTroncoPeloRamal(Long chaveRamal) throws NegocioException;

	/**
	 * Método responsável por verificar se foi
	 * selecionada algum ramal para exclusão.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias dos ramais
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRamaisSelecionados(Long[] chavesPrimarias) throws NegocioException;
}
