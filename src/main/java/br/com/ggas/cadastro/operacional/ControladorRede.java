/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.operacional;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.RedeInternaImovel;
import br.com.ggas.cadastro.localidade.Rede;
import br.com.ggas.cadastro.localidade.RedeDistribuicaoTronco;
import br.com.ggas.cadastro.localidade.RedeIndicador;
import br.com.ggas.geral.dominio.EntidadeDominio;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador de Rede.
 * 
 *
 */
public interface ControladorRede extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_REDE = "controladorRede";

	String ERRO_NEGOCIO_REDE_SEM_TRONCO = "ERRO_NEGOCIO_REDE_SEM_TRONCO";

	String ERRO_NEGOCIO_TRONCO_CADASTRADO_NESTE_CITYGATE = "ERRO_NEGOCIO_TRONCO_CADASTRADO_NESTE_CITYGATE";

	String ERRO_NEGOCIO_REMOVER_REDE = "ERRO_NEGOCIO_REMOVER_REDE";

	String ERRO_NEGOCIO_REMOVER_REDE_MATERIAL = "ERRO_NEGOCIO_REMOVER_REDE_MATERIAL";

	String ERRO_NEGOCIO_REMOVER_REDE_DIAMETRO = "ERRO_NEGOCIO_REMOVER_REDE_DIAMETRO";

	/**
	 * Metodo responsável por criar um entidade de
	 * negocio do tipo Rede Diametro.
	 * 
	 * @return Uma Entidade de Negócio
	 */
	EntidadeNegocio criarRedeDiametro();

	/**
	 * Metodo responsável por criar uma entidade
	 * de negocio do tipo Rede Material.
	 * 
	 * @return Uma Entidade de Negócio
	 */
	EntidadeNegocio criarRedeMaterial();

	/**
	 * Método responsável por criar uma entidade
	 * RedeIndicador.
	 * 
	 * @return Uma EntidadeDominio de
	 *         RedeIndicador.
	 */
	EntidadeDominio criarRedeIndicador();

	/**
	 * Método responsável por listar todos os
	 * indicadores de rede.
	 * 
	 * @return coleção de indicadores de rede.
	 */
	Collection<RedeIndicador> listarTodosIndicadoresRede();

	/**
	 * Método responsável por obter um indicador
	 * de rede pelo código informado.
	 * 
	 * @param codigo
	 *            O código do indicador de rede.
	 * @return Um indicador de rede.
	 */
	RedeIndicador obterRedeIndicador(Integer codigo);

	/**
	 * Método responsável por consultar as redes.
	 * 
	 * @param filtro
	 *            Um filtro de consulta
	 * @return Uma coleção de redes
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	Collection<Rede> consultarRedes(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listas os diâmetros
	 * de redes do sistema.
	 * 
	 * @return coleção de diâmetros de redes.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<RedeDiametro> listarRedeDiametro() throws NegocioException;

	/**
	 * Método responsável por listas os materiais
	 * de rede do sistema.
	 * 
	 * @return coleção de materiais de rede.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<RedeMaterial> listarRedeMaterial() throws NegocioException;

	/**
	 * Método responsável por retornar um diâmetro
	 * de rede pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do diâmetro de
	 *            rede.
	 * @return Um diâmetro de rede.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	RedeDiametro obterRedeDiametro(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por retornar um material
	 * de rede pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do material de
	 *            rede.
	 * @return Um material de rede.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	RedeMaterial obterRedeMaterial(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para remover rede.
	 * 
	 * @param chavesPrimarias
	 *            Array de Chaves das localidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverRede(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por criar uma entidade
	 * RedeDistribuicaoTronco.
	 * 
	 * @return Uma EntidadeNegócio de
	 *         RedeDistribuicaoTronco.
	 */
	EntidadeNegocio criarRedeDistribuicaoTronco();

	/**
	 * Método responsável por validar o tronco da
	 * rede de distribuição.
	 * 
	 * @param redeTronco
	 *            O tronco da rede
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarDadosRedeDistribuicaoTronco(RedeDistribuicaoTronco redeTronco) throws NegocioException;

	/**
	 * Método responsável por validar um unico
	 * tronco por cada city gate.
	 * 
	 * @param redeTronco
	 *            O tronco da rede,
	 *            listaRedeTronco uma colecao de
	 *            RedeDistribuicaoTronco
	 * @param listaRedeTronco
	 *            the lista rede tronco
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarTroncoPorCityGate(RedeDistribuicaoTronco redeTronco, Collection<RedeDistribuicaoTronco> listaRedeTronco)
					throws NegocioException;

	/**
	 * Método responsável por listar
	 * RedeDistribuicaoTronco por uma rede
	 * informada.
	 * 
	 * @param rede
	 *            Uma Rede do sistema
	 * @return Colecao de RedeDistribuicaoTroncos
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<RedeDistribuicaoTronco> listarRedeTroncoPorRede(Rede rede) throws NegocioException;

	/**
	 * Método responsável por obter a Classe da
	 * Entidade Rede Distribuicao Tronco
	 * 
	 * @return Class RedeDistribuicaoTronco
	 */
	@SuppressWarnings("rawtypes")
	Class getClasseEntidadeRedeDistribuicaoTronco();

	/**
	 * Consultar rede distribuicao tronco.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Rede> consultarRedeDistribuicaoTronco(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar rede interna imovel.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<RedeInternaImovel> consultarRedeInternaImovel(Map<String, Object> filtro) throws NegocioException;
	
	/**
	 * Obtém uma lista de Troncos por Rede, consultando  os 
	 * seguintes atributos: {@code chavePrimariaTronco}, {@code chavePrimariaRede},
	 * {@code chavePrimariaCityGate}, {@code descricaoCityGate}.
	 * @param chavesPrimariasRede {@link Long}
	 * @return mapa de Troncos por Rede
	 */
	Map<Rede, List<Tronco>> consultarTroncosDaRede(Long[] chavesPrimariasRede);

	/**
	 * Obtém as chaves das Redes dos pontos de consumo
	 * especificados no parâmetro.
	 * 
	 * @param pontosConsumo {@link Collection}
	 * @return redes dos pontos de consumo
	 **/
	Long[] obterChavesDeRedesDePontosDeConsumo(Collection<PontoConsumo> pontosConsumo);

	Collection<RedeComprimento> consultarRedeComprimentoPorAno(String ano);

	EntidadeNegocio criarRedeComprimento();

	void incluirRedeComprimento(List<String> comprimentos, String ano) throws NegocioException;

}
