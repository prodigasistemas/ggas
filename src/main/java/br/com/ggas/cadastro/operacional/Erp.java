/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.operacional;

import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Erp
 * 
 */
public interface Erp extends EntidadeNegocio {

	String BEAN_ID_ERP = "erp";

	String ESTACAO = "ESTACAO";

	String ERP_CAMPO_STRING = "ERP";

	String NOME = "ERP_NOME";

	String ENDERECO = "ERP_ENDERECO";

	String LOCALIDADE = "ERP_LOCALIDADE";

	String MEDICAO = "ERP_MEDICAO";

	/**
	 * @return the nome
	 */
	String getNome();

	/**
	 * @param nome
	 *            the nome to set
	 */
	void setNome(String nome);

	/**
	 * @return the endereco
	 */
	Endereco getEndereco();

	/**
	 * @param endereco
	 *            the endereco to set
	 */
	void setEndereco(Endereco endereco);

	/**
	 * @return the localidade
	 */
	Localidade getLocalidade();

	/**
	 * @param localidade
	 *            the localidade to set
	 */
	void setLocalidade(Localidade localidade);

	/**
	 * @return the medicao
	 */
	boolean isMedicao();

	/**
	 * @param medicao
	 *            the medicao to set
	 */
	void setMedicao(boolean medicao);

	/**
	 * @return the pressao
	 */
	public boolean isPressao();

	/**
	 * @param pressao
	 *            the pressao to set
	 */
	public void setPressao(boolean pressao);

	/**
	 * @return the nomeAbreviado
	 */
	String getNomeAbreviado();

	/**
	 * @param nomeAbreviado
	 *            the nomeAbreviado to set
	 */
	void setNomeAbreviado(String nomeAbreviado);

	/**
	 * @return the troncoAnterior
	 */
	Tronco getTroncoAnterior();

	/**
	 * @param troncoAnterior
	 *            the troncoAnterior to set
	 */
	void setTroncoAnterior(Tronco troncoAnterior);

	/**
	 * @return the troncoPosterior
	 */
	Tronco getTroncoPosterior();

	/**
	 * @param troncoPosterior
	 *            the troncoPosterior to set
	 */
	void setTroncoPosterior(Tronco troncoPosterior);
	
	/**
	 * @return the imovel
	 */
	Imovel getImovel();

	/**
	 * @param imovel
	 *            the imovel to set
	 */
	void setImovel(Imovel imovel);

}
