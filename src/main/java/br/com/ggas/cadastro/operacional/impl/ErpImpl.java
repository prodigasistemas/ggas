/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.operacional.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.operacional.Erp;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * A classe ErpImpl representa uma Estação no sistema.
 *
 */
public class ErpImpl extends EntidadeNegocioImpl implements Erp {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 7050167614416903321L;

	private String nome;

	private String nomeAbreviado;

	private Tronco troncoAnterior;

	private Tronco troncoPosterior;

	private Endereco endereco;

	private Localidade localidade;
	
	private Imovel imovel;

	private boolean medicao;

	private boolean pressao;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.impl.ERP
	 * #validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(this.nome)) {
			stringBuilder.append(NOME);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(localidade == null) {
			stringBuilder.append(LOCALIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.Erp#isPressao
	 * ()
	 */
	@Override
	public boolean isPressao() {

		return pressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.Erp#setPressao
	 * (boolean)
	 */
	@Override
	public void setPressao(boolean pressao) {

		this.pressao = pressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.impl.ERP
	 * #getNome()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.impl.ERP
	 * #setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.impl.ERP
	 * #getEndereco()
	 */
	@Override
	public Endereco getEndereco() {

		return endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.impl.ERP
	 * #setEndereco
	 * (br.com.ggas.cadastro.endereco.Endereco)
	 */
	@Override
	public void setEndereco(Endereco endereco) {

		this.endereco = endereco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.impl.ERP
	 * #getLocalidade()
	 */
	@Override
	public Localidade getLocalidade() {

		return localidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.impl.ERP
	 * #setLocalidade
	 * (br.com.ggas.cadastro.localidade
	 * .Localidade)
	 */
	@Override
	public void setLocalidade(Localidade localidade) {

		this.localidade = localidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.impl.ERP
	 * #isMedicao()
	 */
	@Override
	public boolean isMedicao() {

		return medicao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.operacional.impl.ERP
	 * #setMedicao(boolean)
	 */
	@Override
	public void setMedicao(boolean medicao) {

		this.medicao = medicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.Erp#
	 * getNomeAbreviado()
	 */
	@Override
	public String getNomeAbreviado() {

		return nomeAbreviado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.Erp#
	 * setNomeAbreviado(java.lang.String)
	 */
	@Override
	public void setNomeAbreviado(String nomeAbreviado) {

		this.nomeAbreviado = nomeAbreviado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.Erp#
	 * getTroncoAnterior()
	 */
	@Override
	public Tronco getTroncoAnterior() {

		return troncoAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.Erp#
	 * setTroncoAnterior
	 * (br.com.ggas.cadastro.operacional.Tronco)
	 */
	@Override
	public void setTroncoAnterior(Tronco troncoAnterior) {

		this.troncoAnterior = troncoAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.Erp#
	 * getTroncoPosterior()
	 */
	@Override
	public Tronco getTroncoPosterior() {

		return troncoPosterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.Erp#
	 * setTroncoPosterior
	 * (br.com.ggas.cadastro.operacional.Tronco)
	 */
	@Override
	public void setTroncoPosterior(Tronco troncoPosterior) {

		this.troncoPosterior = troncoPosterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.Erp#
	 * getImovel
	 */
	@Override
	public Imovel getImovel() {
		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.Erp#
	 * setImovel
	 * (br.com.ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public void setImovel(Imovel imovel) {
		this.imovel = imovel;
	}
	
	

}
