/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.operacional.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.operacional.ControladorRamal;
import br.com.ggas.cadastro.operacional.Ramal;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

class ControladorRamalImpl extends ControladorNegocioImpl implements ControladorRamal {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Ramal.BEAN_ID_RAMAL);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Ramal.BEAN_ID_RAMAL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Ramal ramal = (Ramal) entidadeNegocio;
		validarRamalExistente(ramal);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Ramal ramal = (Ramal) entidadeNegocio;
		validarRamalExistente(ramal);
	}

	/**
	 * Validar ramal existente.
	 * 
	 * @param ramal
	 *            the ramal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private void validarRamalExistente(Ramal ramal) throws NegocioException {


		Criteria criteria = getCriteria();
		criteria.add(Restrictions.ilike("nome", ramal.getNome(), MatchMode.EXACT));
		List<Ramal> listaRamais = criteria.list();

		if(!listaRamais.isEmpty()) {
			Ramal ramalExistente = listaRamais.get(0);

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(ramalExistente);

			if(ramalExistente.getChavePrimaria() != ramal.getChavePrimaria()) {
				throw new NegocioException(ControladorRamal.ERRO_NEGOCIO_RAMAL_EXISTENTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRamal
	 * #consultarRamal(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Ramal> consultarRamal(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
				criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
			}

			String nome = (String) filtro.get("nome");
			if(!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.ilike("nome", nome, MatchMode.ANYWHERE));
			}

			Long idRede = (Long) filtro.get("idRede");
			if(idRede != null && idRede > 0) {
				criteria.createAlias("rede", "rede");
				criteria.add(Restrictions.eq("rede.chavePrimaria", idRede));
			} else {
				criteria.setFetchMode("rede", FetchMode.JOIN);
			}

			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRamal
	 * #validarSelecaoDeAtualizacao
	 * (java.lang.Long[])
	 */
	@Override
	public void validarSelecaoDeAtualizacao(Long[] chavesPrimarias) throws NegocioException {

		if((chavesPrimarias == null) || (chavesPrimarias.length != 1)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRamal
	 * #consultarQuantidadeTroncoPeloRamal
	 * (java.lang.Long)
	 */
	@Override
	public Integer consultarQuantidadeTroncoPeloRamal(Long chaveRamal) throws NegocioException {


		Long quantidade = 0L;
		if(chaveRamal != null && chaveRamal > 0) {
			Criteria criteria = getCriteria();
			criteria.setProjection(Projections.rowCount());
			criteria.createAlias("tronco", "tronco");
			criteria.add(Restrictions.eq("tronco.chavePrimaria", chaveRamal));
			quantidade = (Long) criteria.uniqueResult();
		}
		return quantidade.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.operacional.
	 * ControladorRamal
	 * #validarRamaisSelecionados(java
	 * .lang.Long[])
	 */
	@Override
	public void validarRamaisSelecionados(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}
}
