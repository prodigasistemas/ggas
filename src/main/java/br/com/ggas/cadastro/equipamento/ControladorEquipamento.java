package br.com.ggas.cadastro.equipamento;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PotenciaFixaEquipamentoVO;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface ControladorEquipamento
 * 
 * @author Procenge
 */
public interface ControladorEquipamento extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_EQUIPAMENTO = "controladorEquipamento";

	/**
	 * consultar Equipamentos
	 * 
	 * @param filtro - {@link}
	 * @return coleção de equipamentos - {@link Collection}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Collection<Equipamento> consultarEquipamentos(Map<String, Object> filtro) throws NegocioException;

	/**
	 * obter Equipamento
	 * 
	 * @param chavePrimaria - {@link Long}
	 * @return equipamento - {@link Equipamento}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public Equipamento obterEquipamento(long chavePrimaria) throws NegocioException;

	/**
	 * @return coleção de vazão equipamentos residenciais
	 * @throws NegocioException {@link NegocioException}
	 */
	Collection<PotenciaFixaEquipamentoVO> listarPotenciasFixas()
			throws NegocioException;
	
}
