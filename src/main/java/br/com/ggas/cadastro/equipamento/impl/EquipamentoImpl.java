package br.com.ggas.cadastro.equipamento.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.equipamento.Equipamento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe EquipamentoImpl
 * 
 * @author Procenge
 */
/**
 * @author ary.consenso
 *
 */
public class EquipamentoImpl extends EntidadeNegocioImpl implements Equipamento {

	private static final int LIMITE_CAMPO = 2;

	private static final int QUANTIDADE_CASAS_DECIMAIS = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = 5339267893921814473L;

	private String descricao;
	private Segmento segmento;
	
	private BigDecimal potenciaFixaAlta;
	private BigDecimal potenciaFixaMedia;
	private BigDecimal potenciaFixaBaixa;
	
	private BigDecimal potenciaPadrao;
		
	@Override
	public Map<String, Object> validarDados() {
		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (this.descricao == null || this.descricao.length() == 0) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (this.segmento == null) {
			stringBuilder.append(SEGMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if(segmento.getindicadorEquipamentoPotenciaFixa() != null && segmento.getindicadorEquipamentoPotenciaFixa()) {
				if(potenciaFixaAlta == null) {
					stringBuilder.append(POTENCIA_FIXA_ALTA);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
				
				if(potenciaFixaMedia == null) {
					stringBuilder.append(POTENCIA_FIXA_MEDIA);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
				
				if(potenciaFixaBaixa == null) {
					stringBuilder.append(POTENCIA_FIXA_BAIXA);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			} else {
				if(potenciaPadrao == null) {
					stringBuilder.append(POTENCIA_PADRAO);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

	@Override
	public String getDescricao() {
		return descricao;
	}

	@Override
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public Segmento getSegmento() {
		return segmento;
	}

	@Override
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}

	/**
	 * @return the potenciaFixaBaixa
	 */
	@Override
	public BigDecimal getPotenciaFixaBaixa() {
		return potenciaFixaBaixa;
	}

	/**
	 * @param potenciaFixaBaixa the potenciaFixaBaixa to set
	 */
	@Override
	public void setPotenciaFixaBaixa(BigDecimal potenciaFixaBaixa) {
		this.potenciaFixaBaixa = potenciaFixaBaixa;
	}

	/**
	 * @return the potenciaFixaMedia
	 */
	@Override
	public BigDecimal getPotenciaFixaMedia() {
		return potenciaFixaMedia;
	}

	/**
	 * @param potenciaFixaMedia the potenciaFixaMedia to set
	 */
	@Override
	public void setPotenciaFixaMedia(BigDecimal potenciaFixaMedia) {
		this.potenciaFixaMedia = potenciaFixaMedia;
	}

	/**
	 * @return the potenciaFixaAlta
	 */
	@Override
	public BigDecimal getPotenciaFixaAlta() {
		return potenciaFixaAlta;
	}

	/**
	 * @param potenciaFixaAlta the potenciaFixaAlta to set
	 */
	@Override
	public void setPotenciaFixaAlta(BigDecimal potenciaFixaAlta) {
		this.potenciaFixaAlta = potenciaFixaAlta;
	}

	/**
	 * @return the potenciaPadrao
	 */
	@Override
	public BigDecimal getPotenciaPadrao() {
		return potenciaPadrao;
	}

	/**
	 * @param potenciaPadrao the potenciaPadrao to set
	 */
	@Override
	public void setPotenciaPadrao(BigDecimal potenciaPadrao) {
		this.potenciaPadrao = potenciaPadrao;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.equipamento.Equipamento#getPotenciaPadraoFormatada()
	 */
	@Override
	public String getPotenciaPadraoFormatada() {
		return Util.converterCampoValorDecimalParaString(Equipamento.ATRIBUTO_POTENCIA_PADRAO, 
				this.potenciaPadrao, Constantes.LOCALE_PADRAO, QUANTIDADE_CASAS_DECIMAIS);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.equipamento.Equipamento#getPotenciaPadraoFormatada()
	 */
	@Override
	public String getPotenciaFixaAltaFormatada() {
		String retorno = "";
		if (this.potenciaFixaAlta != null) {
			retorno = Util.converterCampoValorDecimalParaString(Equipamento.ATRIBUTO_POTENCIA_FIXA_ALTA, this.potenciaFixaAlta,
					Constantes.LOCALE_PADRAO, QUANTIDADE_CASAS_DECIMAIS);
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.equipamento.Equipamento#getPotenciaPadraoFormatada()
	 */
	@Override
	public String getPotenciaFixaMediaFormatada() {
		String retorno = "";
		if (this.potenciaFixaMedia != null) {
			retorno = Util.converterCampoValorDecimalParaString(Equipamento.ATRIBUTO_POTENCIA_FIXA_MEDIA, this.potenciaFixaMedia,
					Constantes.LOCALE_PADRAO, QUANTIDADE_CASAS_DECIMAIS);
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.equipamento.Equipamento#getPotenciaPadraoFormatada()
	 */
	@Override
	public String getPotenciaFixaBaixaFormatada() {
		String retorno = "";
		if (this.potenciaFixaBaixa != null) {
			retorno = Util.converterCampoValorDecimalParaString(Equipamento.ATRIBUTO_POTENCIA_FIXA_BAIXA, this.potenciaFixaBaixa,
					Constantes.LOCALE_PADRAO, QUANTIDADE_CASAS_DECIMAIS);
		}
		return retorno;
	}
	
}
