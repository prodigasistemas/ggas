/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.equipamento;

import java.math.BigDecimal;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface Equipamento
 * 
 * @author Procenge
 */
public interface Equipamento extends EntidadeNegocio {

	String BEAN_ID_EQUIPAMENTO = "equipamento";

	String EQUIPAMENTO_CAMPO_STRING = "Equipamento";

	String DESCRICAO = "Descricao";

	String SEGMENTO = "Segmento";
	
	String POTENCIA_FIXA_ALTA = "Potencia Alta";
	
	String POTENCIA_FIXA_MEDIA = "Potencia Media";
	
	String POTENCIA_FIXA_BAIXA = "Potencia Baixa";
	
	String POTENCIA_PADRAO = "Potencia Padrao";
	
	String ATRIBUTO_POTENCIA_FIXA_ALTA = "potenciaFixaAlta";
	
	String ATRIBUTO_POTENCIA_FIXA_MEDIA = "potenciaFixaMedia";
	
	String ATRIBUTO_POTENCIA_FIXA_BAIXA = "potenciaFixaBaixa";
	
	String ATRIBUTO_POTENCIA_PADRAO = "potenciaPadrao";
	
	/**
	 * @return descricao
	 */
	public String getDescricao();

	/**
	 * Seta o parâmetro descrição
	 * @param descricao
	 */
	public void setDescricao(String descricao);

	/**
	 * @return segmento
	 */
	public Segmento getSegmento();

	/**
	 * @param segmento
	 */
	public void setSegmento(Segmento segmento);

	/**
	 * @return the potenciaFixaBaixa
	 */
	public BigDecimal getPotenciaFixaBaixa();

	/**
	 * @param potenciaFixaBaixa the potenciaFixaBaixa to set
	 */
	public void setPotenciaFixaBaixa(BigDecimal potenciaFixaBaixa);
	
	/**
	 * @return the potenciaFixaMedia
	 */
	public BigDecimal getPotenciaFixaMedia() ;

	/**
	 * @param potenciaFixaMedia the potenciaFixaMedia to set
	 */
	public void setPotenciaFixaMedia(BigDecimal potenciaFixaMedia);

	/**
	 * @return the potenciaFixaAlta
	 */
	public BigDecimal getPotenciaFixaAlta();

	/**
	 * @param potenciaFixaAlta the potenciaFixaAlta to set
	 */
	public void setPotenciaFixaAlta(BigDecimal potenciaFixaAlta);

	/**
	 * @return the potenciaPadrao
	 */
	public BigDecimal getPotenciaPadrao();

	/**
	 * @param potenciaPadrao the potenciaPadrao to set
	 */
	public void setPotenciaPadrao(BigDecimal potenciaPadrao);

	/**
	 * @return potenciaPadrao formatada
	 */
	String getPotenciaPadraoFormatada();

	/**
	 * @return potenciaFixaAlta formatada
	 */
	String getPotenciaFixaAltaFormatada();

	/**
	 * @return potenciaFixaMedia formatada
	 */
	String getPotenciaFixaMediaFormatada();

	/**
	 * @return potenciaFixaBaixa formatada
	 */
	String getPotenciaFixaBaixaFormatada();

}
