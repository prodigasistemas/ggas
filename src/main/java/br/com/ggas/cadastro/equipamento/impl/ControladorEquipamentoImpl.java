package br.com.ggas.cadastro.equipamento.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.equipamento.ControladorEquipamento;
import br.com.ggas.cadastro.equipamento.Equipamento;
import br.com.ggas.cadastro.imovel.PotenciaFixaEquipamentoVO;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe ControladorEquipamentoImpl
 * 
 * @author Procenge
 */
public class ControladorEquipamentoImpl extends ControladorNegocioImpl implements ControladorEquipamento {

	private static final String SEGMENTO = "segmento";
	private static final String DESCRICAO = "descricao";


	public Class<?> getClasseEntidadeEquipamento() {

		return ServiceLocator.getInstancia().getClassPorID(Equipamento.BEAN_ID_EQUIPAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Equipamento.BEAN_ID_EQUIPAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {
		return ServiceLocator.getInstancia().getClassPorID(Equipamento.BEAN_ID_EQUIPAMENTO);
	}

	
	/*consultarEquipamentos
	 * 
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.equipamento. ControladorEquipamento #consultarEquipamentos(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Equipamento> consultarEquipamentos(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");

			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String descricao = (String) filtro.get(DESCRICAO);
			if (descricao != null && !descricao.isEmpty()) {
				criteria.add(Restrictions.ilike(DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			Long idSegmento = (Long) filtro.get("idSegmento");
			if (idSegmento != null && idSegmento > 0) {
				criteria.createAlias(SEGMENTO, SEGMENTO);
				criteria.add(Restrictions.eq("segmento.chavePrimaria", idSegmento));
			} else {
				criteria.setFetchMode(SEGMENTO, FetchMode.JOIN);
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			criteria.addOrder(Order.asc(DESCRICAO));

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.equipamento. ControladorEquipamento #consultarVazoesEquipamentoResidencial()
	 */
	@Override
	public Collection<PotenciaFixaEquipamentoVO> listarPotenciasFixas() throws NegocioException {
		
		Collection<PotenciaFixaEquipamentoVO> listaPotenciasFixas = new ArrayList<>();
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		
		PotenciaFixaEquipamentoVO potenciaAlta = new PotenciaFixaEquipamentoVO();
		potenciaAlta.setDescricao("ALTA");
		potenciaAlta.setCodPotenciaFixa(
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_PCEQ_POTENCIA_FIXA_ALTA).getValorLong());
		listaPotenciasFixas.add(potenciaAlta);

		PotenciaFixaEquipamentoVO potenciaMedia = new PotenciaFixaEquipamentoVO();
		potenciaMedia.setDescricao("MÉDIA");
		potenciaMedia.setCodPotenciaFixa(
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_PCEQ_POTENCIA_FIXA_MEDIA).getValorLong());
		listaPotenciasFixas.add(potenciaMedia);
				
		PotenciaFixaEquipamentoVO potenciaBaixa = new PotenciaFixaEquipamentoVO();
		potenciaBaixa.setDescricao("BAIXA");
		potenciaBaixa.setCodPotenciaFixa(
				controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_PCEQ_POTENCIA_FIXA_BAIXA).getValorLong());
		listaPotenciasFixas.add(potenciaBaixa);
		
		return listaPotenciasFixas;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.cadastro.imovel.ControladorImovel#obterEquipamento(long)
	 */
	@Override
	public Equipamento obterEquipamento(long chavePrimaria) throws NegocioException {

		return (Equipamento) super.obter(chavePrimaria, getClasseEntidadeEquipamento());
	}


}
