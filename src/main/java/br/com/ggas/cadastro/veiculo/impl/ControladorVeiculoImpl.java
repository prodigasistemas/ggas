package br.com.ggas.cadastro.veiculo.impl;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import br.com.ggas.cadastro.veiculo.ControladorVeiculo;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ServiceLocator;

public class ControladorVeiculoImpl extends ControladorNegocioImpl implements ControladorVeiculo{

	@Override
	public EntidadeNegocio criar() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Veiculo.BEAN_ID_VEICULO);
	}

	@Override
	public Class<?> getClasseEntidade() {
		return ServiceLocator.getInstancia().getClassPorID(Veiculo.BEAN_ID_VEICULO);
	}
	
	
	@Override
	public Collection<Veiculo> consultarVeiculo(Veiculo veiculo, String habilitado) {
		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" veiculo ");
		hql.append(" WHERE ");
		hql.append(" 1 = 1 ");

		if(!StringUtils.isEmpty(veiculo.getPlaca())) {
			hql.append(" AND placa = :placa ");
		}
		
		if(veiculo.getUf() != null && veiculo.getUf().getChavePrimaria() > 0) {
			hql.append(" AND uf.chavePrimaria = :uf ");
		}
		
		if(!StringUtils.isEmpty(veiculo.getModelo())) {
			hql.append(" AND modelo LIKE :modelo");
		}
		
		if(habilitado != null) {
			if(habilitado.equals("true") || habilitado.equals("false")) {
				hql.append(" AND habilitado = :habilitado");
			}
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(!StringUtils.isEmpty(veiculo.getPlaca())) {
			query.setParameter("placa", veiculo.getPlaca());
		}
		
		if(veiculo.getUf() != null && veiculo.getUf().getChavePrimaria() > 0) {
			query.setParameter("uf", veiculo.getUf().getChavePrimaria());
		}
		
		if(!StringUtils.isEmpty(veiculo.getModelo())) {
			query.setParameter("modelo", "%"+veiculo.getModelo()+"%");
		}
		
		if(habilitado != null) {
			if(habilitado.equals("true") || habilitado.equals("false")) {
				query.setParameter("habilitado", veiculo.isHabilitado());
			}
		}

		return query.list();
	}
	
	@Override
	public Collection<Veiculo> listaVeiculos() {
		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" veiculo ");
		hql.append(" WHERE veiculo.habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

}
