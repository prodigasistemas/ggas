package br.com.ggas.cadastro.veiculo;

import java.util.Collection;

import br.com.ggas.geral.negocio.ControladorNegocio;

public interface ControladorVeiculo extends ControladorNegocio{
	String BEAN_ID_CONTROLADOR_VEICULO = "controladorVeiculo";

	Collection<Veiculo> consultarVeiculo(Veiculo veiculo,String habilitado);
	
	Collection<Veiculo> listaVeiculos();
}
