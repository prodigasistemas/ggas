
package br.com.ggas.cadastro.veiculo.impl;

/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/06/2014 11:06:01
 @author rfilho
 */

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelos atributos de Veículo
 *
 */
public class VeiculoImpl extends EntidadeNegocioImpl implements Veiculo{

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */

	private static final long serialVersionUID = -673635539707959639L;

	private static final String PLACA = "Placa do Veículo";

	private static final String UF = "Uf";
	
	private static final String MODELO = "Modelo";

	private String placa;

	private UnidadeFederacao uf;
	
	private String modelo;
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(placa == null || placa.isEmpty()) {
			stringBuilder.append(PLACA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(uf == null) {
			stringBuilder.append(UF);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(modelo == null || modelo.isEmpty()) {
			stringBuilder.append(MODELO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

	@Override
	public String getPlaca() {

		return this.placa;
	}

	@Override
	public void setPlaca(String placa) {

		this.placa = placa;

	}

	@Override
	public UnidadeFederacao getUf() {

		return uf;
	}

	@Override
	public void setUf(UnidadeFederacao uf) {

		this.uf = uf;
	}

	@Override
	public String getModelo() {
		return modelo;
	}

	@Override
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

}
