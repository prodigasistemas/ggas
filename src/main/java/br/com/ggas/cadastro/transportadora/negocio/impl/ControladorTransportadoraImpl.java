/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 16:24:02
 @author rfilho
 */

package br.com.ggas.cadastro.transportadora.negocio.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.motorista.Motorista;
import br.com.ggas.cadastro.transportadora.dominio.Transportadora;
import br.com.ggas.cadastro.transportadora.dominio.TransportadoraVO;
import br.com.ggas.cadastro.transportadora.dominio.TransportadoraVeiculo;
import br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora;
import br.com.ggas.cadastro.transportadora.repositorio.RepositorioTransportadora;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
/**
 * Classe responsável pelos métodos de inclusão, alteração, consulta e validação da classe Transportadora. 
 *
 */
@Service("controladorTransportadora")
@Transactional
public class ControladorTransportadoraImpl implements ControladorTransportadora {

	@Autowired
	private RepositorioTransportadora repositorioTransportadora;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#consultarTransportadora(br.com.ggas.cadastro.transportadora
	 * .dominio.TransportadoraVO, java.lang.Boolean)
	 */
	@Override
	public Collection<Transportadora> consultarTransportadora(TransportadoraVO transportadoraVO, Boolean habilitado) {

		return repositorioTransportadora.consultarRepositorioTransportadora(transportadoraVO, habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#incluirTransportadora(br.com.ggas.cadastro.transportadora.dominio
	 * .Transportadora)
	 */
	@Override
	public void incluirTransportadora(Transportadora transportadora) throws NegocioException {

		repositorioTransportadora.validarDadosEntidade(transportadora);
		validarListaMotoristaVeiculo(transportadora);
		repositorioTransportadora.inserir(transportadora);

	}

	/**
	 * Validar lista motorista veiculo.
	 * 
	 * @param transportadora
	 *            the transportadora
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarListaMotoristaVeiculo(Transportadora transportadora) throws NegocioException {

		if(transportadora.getListaMotorista() == null || transportadora.getListaMotorista().isEmpty()
						|| transportadora.getListaVeiculo() == null || transportadora.getListaVeiculo().isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_AOMENOS_UM_REGISTO_NA_LISTA_TRANSPORTADORA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#alterarTransportadora(br.com.ggas.cadastro.transportadora.dominio
	 * .Transportadora, java.util.List, java.util.List)
	 */
	@Override
	public void alterarTransportadora(Transportadora transportadora, List<Long> listaRemover, List<Long> listaRemoverMotorista)
					throws ConcorrenciaException, NegocioException {

		validarListaMotoristaVeiculo(transportadora);

		if(listaRemover != null && !listaRemover.isEmpty()) {
			for (Long long1 : listaRemover) {
				repositorioTransportadora.remover(repositorioTransportadora.obter(long1, Veiculo.class), Veiculo.class);
			}
		}

		if(listaRemoverMotorista != null && !listaRemoverMotorista.isEmpty()) {
			for (Long long1 : listaRemoverMotorista) {
				repositorioTransportadora.remover(repositorioTransportadora.obter(long1, Motorista.class), Motorista.class);
			}
		}

		repositorioTransportadora.atualizar(transportadora);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#obterTransportadora(java.lang.Long)
	 */
	@Override
	public Transportadora obterTransportadora(Long chavePrimaria) throws NegocioException {

		return (Transportadora) repositorioTransportadora.obter(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#
	 * verificarSeExisteMotorista(br.com.ggas.cadastro.motorista.Motorista
	 * , java.util.Collection, java.lang.Integer)
	 */
	@Override
	public void verificarSeExisteMotorista(Motorista motorista, Collection<Motorista> listaMotoristas, Integer indexList) {
		//Método vazio.
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#verificarSeExisteVeiculo(br.com.ggas.cadastro.veiculo.Veiculo,
	 * java.util.Collection, java.lang.Integer)
	 */
	@Override
	public void verificarSeExisteVeiculo(Veiculo veiculo, Collection<Veiculo> listaVeiculos, Integer indexList) {
		//Método vazio.
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#removerTransportadora(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerTransportadora(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.validarRemoverTransportadora(chavesPrimarias);

		this.repositorioTransportadora.remover(chavesPrimarias, dadosAuditoria);

	}

	/**
	 * Validar remover transportadora.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarRemoverTransportadora(Long[] chavesPrimarias) throws NegocioException {

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		for (Long idTransportadora : chavesPrimarias) {
			if(idTransportadora != null && idTransportadora > 0) {
				Map<String, Object> filtro = new HashMap<>();
				filtro.put("idTransportadora", idTransportadora);
				Collection<Fatura> faturas = controladorFatura.consultarFatura(filtro);
				if(faturas != null && !faturas.isEmpty()) {
					throw new NegocioException(Constantes.ERRO_NAO_E_POSSIVEL_REMOVER_TRANSPORTADORA, true);
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#consultarVeiculosPorTransportadora(java.lang.Long)
	 */
	@Override
	public Collection<Veiculo> consultarVeiculosPorTransportadora(Long chavePrimariaTransportadora) {

		return repositorioTransportadora.consultarVeiculosPorTransportadora(chavePrimariaTransportadora);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#consultarMotoristasPorTransportadora(java.lang.Long)
	 */
	@Override
	public Collection<Motorista> consultarMotoristasPorTransportadora(Long chavePrimariaTransportadora) {

		return repositorioTransportadora.consultarMotoristasPorTransportadora(chavePrimariaTransportadora);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#listarTransportadoras()
	 */
	@Override
	public Collection<Transportadora> listarTransportadoras() {

		return repositorioTransportadora.listarTransportadoras();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#obterVeiculo(java.lang.Long)
	 */
	@Override
	public Veiculo obterVeiculo(Long idVeiculo) throws NegocioException {

		return (Veiculo) repositorioTransportadora.obter(idVeiculo, Veiculo.class);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#obterMotorista(java.lang.Long)
	 */
	@Override
	public Motorista obterMotorista(Long idMotorista) throws NegocioException {

		return (Motorista) repositorioTransportadora.obter(idMotorista, Motorista.class);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#validarRemocaoVeiculo(java.lang.Long)
	 */
	@Override
	public void validarRemocaoVeiculo(Long chavePrimaria) throws NegocioException {

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("idVeiculo", chavePrimaria);
		Collection<Fatura> faturas = controladorFatura.consultarFatura(filtro);
		if(faturas != null && !faturas.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NAO_E_POSSIVEL_REMOVER_VEICULO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#validarRemocaoMotorista(java.lang.Long)
	 */
	@Override
	public void validarRemocaoMotorista(Long chavePrimaria) throws NegocioException {

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("idMotorista", chavePrimaria);
		Collection<Fatura> faturas = controladorFatura.consultarFatura(filtro);
		if(faturas != null && !faturas.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NAO_E_POSSIVEL_REMOVER_MOTORISTA, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#validarExisteTransportadora(java.lang.Long)
	 */
	@Override
	public void validarExisteTransportadora(Long idCliente) throws NegocioException {

		TransportadoraVO transportadoraVO = new TransportadoraVO();
		transportadoraVO.setIdCliente(idCliente);
		Collection<Transportadora> transportadoras = repositorioTransportadora.consultarRepositorioTransportadora(transportadoraVO, true);
		if(transportadoras != null && !transportadoras.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_JA_EXISTE_TRANSPORTADORA, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora#incluirTransportadoraVeiculo(br.com.ggas.cadastro.transportadoraVeiculo.dominio
	 * .Transportadora)
	 */
	@Override
	public void incluirTransportadoraVeiculo(List<TransportadoraVeiculo> veiculos) throws NegocioException {

		for (TransportadoraVeiculo veiculo : veiculos) {
			repositorioTransportadora.inserir(veiculo);
		}

	}

}
