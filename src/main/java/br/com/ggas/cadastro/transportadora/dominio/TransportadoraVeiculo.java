package br.com.ggas.cadastro.transportadora.dominio;

import java.util.Map;

import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

public class TransportadoraVeiculo extends EntidadeNegocioImpl {

	private static final long serialVersionUID = -8572008401198727837L;

	private Veiculo veiculo;
	
	private Transportadora transportadora;

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public Transportadora getTransportadora() {
		return transportadora;
	}

	public void setTransportadora(Transportadora transportadora) {
		this.transportadora = transportadora;
	}

	@Override
	public Map<String, Object> validarDados() {
		// TODO Auto-generated method stub
		return null;
	}

}
