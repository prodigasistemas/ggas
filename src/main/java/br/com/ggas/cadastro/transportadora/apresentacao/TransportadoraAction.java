/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/06/2014 19:02:22
 @author rfilho
 */

package br.com.ggas.cadastro.transportadora.apresentacao;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.motorista.Motorista;
import br.com.ggas.cadastro.transportadora.dominio.Transportadora;
import br.com.ggas.cadastro.transportadora.dominio.TransportadoraVO;
import br.com.ggas.cadastro.transportadora.dominio.TransportadoraVeiculo;
import br.com.ggas.cadastro.transportadora.negocio.ControladorTransportadora;
import br.com.ggas.cadastro.veiculo.ControladorVeiculo;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Controller responsável pelas telas relacionadas a Transportadora 
 *
 */
@Controller
@SessionAttributes("transportadoraVO")
public class TransportadoraAction extends GenericAction {

	private static final String TELA_AJAX_ERRO = "ajaxErro";

	private static final String LISTA_REMOVER_MOTORISTA = "listaRemoverMotorista";

	private static final String ALTERAR = "alterar";

	private static final String UNIDADES_FEDERACAO = "unidadesFederacao";

	private static final String LISTA_REMOVIDA_VEICULO = "listaRemovidaVeiculo";

	private static final String LISTA_VEICULOS = "listaVeiculos";

	private static final String LISTA_MOTORISTAS = "listaMotoristas";

	private static final String TELA_EXIBIR_PESQUISA_TRANSPORTADORA = "exibirPesquisaTransportadora";

	private static final Logger LOG = Logger.getLogger(TransportadoraAction.class);

	private static final String TRANSPORTADORA = "Transportadora";

	@Autowired
	private ControladorTransportadora controladorTransportadora;

	@Autowired
	@Qualifier("controladorEndereco")
	private ControladorEndereco controladorEndereco;

	@Autowired
	@Qualifier("controladorCliente")
	private ControladorCliente controladorCliente;

	
	@Autowired
	@Qualifier("controladorVeiculo")
	private ControladorVeiculo controladorVeiculo;
	
	/**
	 * Exibir pesquisa transportadora.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_TRANSPORTADORA)
	public ModelAndView exibirPesquisaTransportadora(HttpSession sessao) throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_TRANSPORTADORA);

		sessao.removeAttribute(LISTA_MOTORISTAS);
		sessao.removeAttribute(LISTA_VEICULOS);
		sessao.removeAttribute("transportadoraVO");
		sessao.removeAttribute(LISTA_REMOVIDA_VEICULO);
		sessao.removeAttribute("listaRemovidaMotorista");

		return model;

	}

	/**
	 * Pesquisar transportadora.
	 * 
	 * @param transportadoraVO
	 *            the transportadora vo
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("pesquisarTransportadora")
	public ModelAndView pesquisarTransportadora(@ModelAttribute("TransportadoraVO") TransportadoraVO transportadoraVO)
					throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_TRANSPORTADORA);
		model.addObject("transportadoraVO", transportadoraVO);

		transportadoraVO.setNomeMotorista(transportadoraVO.getNomeMotorista());
		transportadoraVO.setPlacaVeiculo(transportadoraVO.getPlacaVeiculo());
		transportadoraVO.setIdCliente(transportadoraVO.getIdCliente());

		Collection<Transportadora> listaTransportadoras = controladorTransportadora.consultarTransportadora(transportadoraVO,
						transportadoraVO.getHabilitado());

		model.addObject("listaTransportadoras", listaTransportadoras);

		return model;

	}

	/**
	 * Exibir inclusao transportadora.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("exibirInclusaoTransportadora")
	public ModelAndView exibirInclusaoTransportadora(HttpSession sessao) {

		ModelAndView model = new ModelAndView("exibirInclusaoTransportadora");

		Transportadora transportadora = new Transportadora();

		model.addObject(TRANSPORTADORA, transportadora);
		model.addObject(UNIDADES_FEDERACAO, controladorEndereco.listarTodasUnidadeFederacao());
		sessao.setAttribute(UNIDADES_FEDERACAO, controladorEndereco.listarTodasUnidadeFederacao());
		sessao.setAttribute(LISTA_VEICULOS, null);
		sessao.setAttribute(LISTA_MOTORISTAS, null);
		model.addObject("acao", "incluir");

		return model;

	}

	/**
	 * Incluir transportadora.
	 * 
	 * @param transportadora
	 *            the transportadora
	 * @param result           
	 * 			  the BindingResult
	 * @param idCliente
	 *            the id cliente
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("incluirTransportadora")
	public ModelAndView incluirTransportadora(@ModelAttribute(TRANSPORTADORA) Transportadora transportadora, BindingResult result,
					@RequestParam("idCliente") Long idCliente, HttpSession sessao) throws NegocioException {

		ModelAndView model = null;
		Cliente cliente = null;
		if(idCliente != null) {
			cliente = controladorCliente.obterCliente(idCliente, new String[] {"enderecos"});
		}

		Collection<TransportadoraVeiculo> listaVeiculos = null;
		if(sessao.getAttribute(LISTA_VEICULOS) != null) {
			listaVeiculos = new HashSet<>((Collection<TransportadoraVeiculo>) sessao.getAttribute(LISTA_VEICULOS));
		}

		Collection<Motorista> listaMotoristas = null;
		if(sessao.getAttribute(LISTA_MOTORISTAS) != null) {
			listaMotoristas = new HashSet<>((Collection<Motorista>) sessao.getAttribute(LISTA_MOTORISTAS));
		}

		if(listaVeiculos != null) {
			for (TransportadoraVeiculo transportadoraVeiculo : listaVeiculos) {
				transportadoraVeiculo.setTransportadora(transportadora);
				transportadoraVeiculo.setUltimaAlteracao(new Date());
				transportadoraVeiculo.setHabilitado(true);

			}
		}

		if(listaMotoristas != null) {
			for (Motorista motorista : listaMotoristas) {
				motorista.setTransportadora(transportadora);
				motorista.setUltimaAlteracao(new Date());
				motorista.setHabilitado(true);
			}
		}

		transportadora.setListaMotorista(listaMotoristas);
		transportadora.setCliente(cliente);

		try {
			controladorTransportadora.validarExisteTransportadora(idCliente);
			controladorTransportadora.incluirTransportadora(transportadora);
			TransportadoraVO t = new TransportadoraVO();
			t.setIdCliente(idCliente);			
			model = this.pesquisarTransportadora(t);
			sessao.removeAttribute(LISTA_VEICULOS);
			sessao.removeAttribute(LISTA_MOTORISTAS);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, Constantes.TRANSPORTADORA);
		} catch(NegocioException e) {
			model = new ModelAndView("exibirInclusaoTransportadora");
			model.addObject(TRANSPORTADORA, transportadora);
			sessao.setAttribute(LISTA_VEICULOS, listaVeiculos);
			sessao.setAttribute(LISTA_MOTORISTAS, listaMotoristas);
			model.addObject("acao", "incluir");
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Exibir alteracao transportadora.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("exibirAlteracaoTransportadora")
	public ModelAndView exibirAlteracaoTransportadora(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao) {

		ModelAndView model = new ModelAndView("exibirAlteracaoTransportadora");

		Transportadora transportadora = null;
		Collection<Veiculo> listaVeiculos = null;
		Collection<Motorista> listaMotoristas = null;
		try {
			transportadora = controladorTransportadora.obterTransportadora(chavePrimaria);
			listaVeiculos = controladorTransportadora.consultarVeiculosPorTransportadora(chavePrimaria);
			listaMotoristas = controladorTransportadora.consultarMotoristasPorTransportadora(chavePrimaria);

		} catch(NegocioException e) {
			LOG.error(e.getMessage(), e);
		}

		Veiculo veiculo = (Veiculo) controladorVeiculo.criar();
		Motorista motorista = new Motorista();

		model.addObject(TRANSPORTADORA, transportadora);

		motorista.setHabilitado(true);
		veiculo.setHabilitado(true);

		model.addObject("veiculo", veiculo);
		model.addObject("motorista", motorista);

		Collection<UnidadeFederacao> ufs = controladorEndereco.listarTodasUnidadeFederacao();
		sessao.setAttribute(LISTA_VEICULOS, listaVeiculos);
		sessao.setAttribute(LISTA_MOTORISTAS, listaMotoristas);
		model.addObject(UNIDADES_FEDERACAO, ufs);
		sessao.setAttribute(UNIDADES_FEDERACAO, ufs);
		model.addObject("acao", ALTERAR);

		return model;
	}

	/**
	 * Alterar transportadora.
	 * 
	 * @param transportadora
	 *            the transportadora
	 * @param result           
	 * 			  the BindingResult            
	 * @param idCliente
	 *            the id cliente
	 * @param session
	 *            the session
	 * @return the model and view
	 */
	@RequestMapping("alterarTransportadora")
	public ModelAndView alterarTransportadora(@ModelAttribute(TRANSPORTADORA) Transportadora transportadora, BindingResult result,
					@RequestParam("idCliente") Long idCliente, HttpSession session) {

		Collection<Veiculo> listaVeiculos = new HashSet<>((Collection<Veiculo>) session.getAttribute(LISTA_VEICULOS));

		Collection<Motorista> listaMotoristas = new HashSet<>((Collection<Motorista>) session.getAttribute(LISTA_MOTORISTAS));
		Cliente cliente = null;
		if(idCliente != null) {	
			try {
				cliente = controladorCliente.obterCliente(idCliente, new String[] {"enderecos"});
			} catch(NegocioException e1) {
				LOG.error(e1.getMessage(), e1);
			}
		}

		for (Motorista motorista : listaMotoristas) {
			motorista.setUltimaAlteracao(new Date());
			motorista.setTransportadora(transportadora);
		}

//		for (Veiculo veiculo : listaVeiculos) {
//			veiculo.setUltimaAlteracao(new Date());
//			veiculo.setTransportadora(transportadora);
//		}

		transportadora.setListaMotorista(listaMotoristas);
		transportadora.setListaVeiculo(listaVeiculos);
		transportadora.setCliente(cliente);

		List<Long> listaRemoverVeiculo = (List<Long>) session.getAttribute(LISTA_REMOVIDA_VEICULO);
		List<Long> listaRemoverMotorista = (List<Long>) session.getAttribute(LISTA_REMOVER_MOTORISTA);

		session.getAttribute("listaRemovida");
		ModelAndView model = new ModelAndView("exibirAlteracaoTransportadora");

		try {
			controladorTransportadora.alterarTransportadora(transportadora, listaRemoverVeiculo, listaRemoverMotorista);
			model = this.exibirAlteracaoTransportadora(transportadora.getChavePrimaria(), session);
			TransportadoraVO t = new TransportadoraVO();
			t.setIdCliente(transportadora.getCliente().getChavePrimaria());
			
			model = this.pesquisarTransportadora(t);
			
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, GenericAction.obterMensagem(Constantes.TRANSPORTADORA));
		} catch(ConcorrenciaException e) {
			model.addObject(TRANSPORTADORA, transportadora);
			mensagemErroParametrizado(model, e);
			model.addObject("acao", ALTERAR);
		} catch(NegocioException e) {
			model.addObject(TRANSPORTADORA, transportadora);
			mensagemErroParametrizado(model, e);
			model.addObject("acao", ALTERAR);
		}

		return model;
	}

	/**
	 * Exibir detalhar transportadora.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("exibirDetalharTransportadora")
	public ModelAndView exibirDetalharTransportadora(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao) {

		ModelAndView model = new ModelAndView("exibirDetalharTransportadora");

		try {
			Transportadora transportadora = controladorTransportadora.obterTransportadora(chavePrimaria);
			Collection<Veiculo> listaVeiculos = controladorTransportadora.consultarVeiculosPorTransportadora(chavePrimaria);
			Collection<Motorista> listaMotoristas = controladorTransportadora.consultarMotoristasPorTransportadora(chavePrimaria);

			model.addObject(TRANSPORTADORA, transportadora);
			model.addObject("acao", "detalhar");
			sessao.setAttribute(LISTA_VEICULOS, listaVeiculos);
			sessao.setAttribute(LISTA_MOTORISTAS, listaMotoristas);
		} catch(NegocioException e) {
			LOG.error(e.getMessage(), e);
		}

		return model;
	}

	/**
	 * Carregar adicionar motorista.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param nomeMotorista
	 *            the nome motorista
	 * @param cpf
	 *            the cpf
	 * @param indexList
	 *            the index list
	 * @param habilitado
	 *            the habilitado
	 * @param sessao
	 *            the sessao
	 * @param request
	 *            the request
	 * @param transportadora
	 *            the transportadora
	 * @return the model and view
	 */
	@RequestMapping("carregarAdicionarMotorista")
	public ModelAndView carregarAdicionarMotorista(@RequestParam("chavePrimariaTransportadora") Long chavePrimaria,
					@RequestParam("nomeMotorista") String nomeMotorista, @RequestParam("cpf") String cpf,
					@RequestParam("indexList") Integer indexList, @RequestParam(value = "habilitado", required = false) boolean habilitado,
					HttpSession sessao, HttpServletRequest request, @ModelAttribute("Questionario") Transportadora transportadora) {

		ModelAndView model = new ModelAndView("gridMotoristas");

		Motorista motorista = new Motorista();
		motorista.setNome(nomeMotorista);
		String cpfSemFormatacao = Util.removerCaracteresEspeciais(cpf);
		motorista.setCpf(cpfSemFormatacao);
		motorista.setHabilitado(habilitado);

		Collection<Motorista> listaMotoristas = (Collection<Motorista>) sessao.getAttribute(LISTA_MOTORISTAS);
		try {
			Util.validarCPF(cpfSemFormatacao);
			if(indexList != null) {
				List<Motorista> lista = new ArrayList<>(listaMotoristas);
				lista.get(indexList).setNome(nomeMotorista);
				lista.get(indexList).setCpf(cpfSemFormatacao);
				lista.get(indexList).setHabilitado(habilitado);
				validarEntidade(motorista);
				listaMotoristas.clear();
				listaMotoristas.addAll(lista);
			} else {
				if(listaMotoristas != null && !listaMotoristas.isEmpty()) {
					validarEntidade(motorista);
					listaMotoristas.add(motorista);
				} else {
					validarEntidade(motorista);
					listaMotoristas = new HashSet<>();
					listaMotoristas.add(motorista);
				}
			}
		} catch(NegocioException e) {
			model = new ModelAndView(TELA_AJAX_ERRO);
			mensagemErroParametrizado(model, e);
		} catch(FormatoInvalidoException e) {
			model = new ModelAndView(TELA_AJAX_ERRO);
			mensagemErroParametrizado(model, e);
		}

		if(listaMotoristas != null) {
			sessao.setAttribute(LISTA_MOTORISTAS, listaMotoristas);
			model.addObject(LISTA_MOTORISTAS, listaMotoristas);
		}

		return model;
	}

	/**
	 * Carregar adicionar veiculo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param placa
	 *            the placa
	 * @param idUf
	 *            the id uf
	 * @param indexList
	 *            the index list
	 * @param habilitado
	 *            the habilitado
	 * @param sessao
	 *            the sessao
	 * @param request
	 *            the request
	 * @param transportadora
	 *            the transportadora
	 * @return the model and view
	 */
	@RequestMapping("carregarAdicionarVeiculo")
	public ModelAndView carregarAdicionarVeiculo(@RequestParam("chavePrimariaTransportadora") Long chavePrimaria,
					@RequestParam("placaVeiculo") String placa, @RequestParam("idOrgaoEmissorUF") Long idUf,
					@RequestParam("indexList") Integer indexList, @RequestParam(value = "habilitado", required = false) boolean habilitado,
					HttpSession sessao, HttpServletRequest request, @ModelAttribute("Questionario") Transportadora transportadora) {

		ModelAndView model = new ModelAndView("gridVeiculos");

		UnidadeFederacao unidade = null;
		try {
			unidade = controladorEndereco.obterUnidadeFederacao(idUf);
		} catch(NegocioException e1) {
			LOG.error(e1.getMessage(), e1);
		}

		Veiculo veiculo = (Veiculo) controladorVeiculo.criar();
		veiculo.setPlaca(placa);
		veiculo.setUf(unidade);
		veiculo.setHabilitado(habilitado);

		Collection<Veiculo> listaVeiculos = (Collection<Veiculo>) sessao.getAttribute(LISTA_VEICULOS);
		try {
			if(indexList != null) {
				List<Veiculo> lista = new ArrayList<>(listaVeiculos);
				lista.get(indexList).setPlaca(placa);
				lista.get(indexList).setUf(unidade);
				lista.get(indexList).setHabilitado(habilitado);
				validarEntidade(veiculo);
				listaVeiculos.clear();
				listaVeiculos.addAll(lista);
			} else {
				if(listaVeiculos != null && !listaVeiculos.isEmpty()) {
					validarEntidade(veiculo);
					listaVeiculos.add(veiculo);
				} else {
					validarEntidade(veiculo);
					listaVeiculos = new HashSet<>();
					listaVeiculos.add(veiculo);
				}
			}
		} catch(NegocioException e) {
			model = new ModelAndView(TELA_AJAX_ERRO);
			mensagemErroParametrizado(model, e);
		}
		if(listaVeiculos != null) {
			sessao.setAttribute(LISTA_VEICULOS, listaVeiculos);
			model.addObject(LISTA_VEICULOS, listaVeiculos);
		}

		return model;
	}

	/**
	 * Remover veiculo.
	 * 
	 * @param indexVeiculo
	 *            the index veiculo
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("removerVeiculo")
	public ModelAndView removerVeiculo(@RequestParam("indexVeiculo") int indexVeiculo, HttpSession sessao) throws NegocioException {

		ModelAndView model = new ModelAndView("gridVeiculos");

		List<Long> removida = (List<Long>) sessao.getAttribute(LISTA_REMOVIDA_VEICULO);
		if(removida == null) {
			removida = new ArrayList<>();
		}

		Collection<Veiculo> lista = (Collection<Veiculo>) sessao.getAttribute(LISTA_VEICULOS);
		if(lista != null && !lista.isEmpty()) {
			List<Veiculo> lista1 = new ArrayList<>(lista);
			try {
				controladorTransportadora.validarRemocaoVeiculo(lista1.get(indexVeiculo).getChavePrimaria());
			} catch(NegocioException e) {
				mensagemErroParametrizado(model, e);
			}
			removida.add(lista1.get(indexVeiculo).getChavePrimaria());
			lista1.get(indexVeiculo);
			lista1.remove(indexVeiculo);
			sessao.setAttribute(LISTA_VEICULOS, lista1);
		}
		sessao.setAttribute(LISTA_REMOVIDA_VEICULO, removida);
		return model;
	}

	/**
	 * Remover motorista.
	 * 
	 * @param indexMotorista
	 *            the index motorista
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("removerMotorista")
	public ModelAndView removerMotorista(@RequestParam("indexMotorista") int indexMotorista, HttpSession sessao) {

		ModelAndView model = new ModelAndView("gridMotoristas");

		List<Long> removida = (List<Long>) sessao.getAttribute(LISTA_REMOVER_MOTORISTA);
		if(removida == null) {
			removida = new ArrayList<>();
		}

		Collection<Motorista> lista = (Collection<Motorista>) sessao.getAttribute(LISTA_MOTORISTAS);
		if(lista != null && !lista.isEmpty()) {
			List<Motorista> lista1 = new ArrayList<>(lista);
			try {
				controladorTransportadora.validarRemocaoMotorista(lista1.get(indexMotorista).getChavePrimaria());
			} catch(NegocioException e) {
				mensagemErroParametrizado(model, e);
			}
			removida.add(lista1.get(indexMotorista).getChavePrimaria());
			lista1.remove(indexMotorista);

			sessao.setAttribute(LISTA_MOTORISTAS, lista1);
		}
		sessao.setAttribute(LISTA_REMOVER_MOTORISTA, removida);

		return model;

	}

	/**
	 * Remover transportadora.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	@RequestMapping("removerTransportadora")
	public ModelAndView removerTransportadora(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request) {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_TRANSPORTADORA);

		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());
			
			controladorTransportadora.removerTransportadora(chavesPrimarias, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, TRANSPORTADORA);
		} catch(NegocioException e) {

			mensagemErroParametrizado(model, e);
		}

		return model;
	}

}
