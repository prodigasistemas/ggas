/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/06/2014 11:45:30
 @author rfilho
 */

package br.com.ggas.cadastro.transportadora.dominio;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.motorista.Motorista;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pelos atributos de transportadora 
 *
 */
public class Transportadora extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2264097467161269675L;

	private static final String CLIENTE = "Pessoa Jurídica";

	private static final String MOTORISTA = "Motorista";

	private static final String VEICULO = "Veículo";

	private Collection<Motorista> listaMotorista = new HashSet<>();

	private Collection<Veiculo> listaVeiculo = new HashSet<>();

	private Cliente cliente;

	public Collection<Veiculo> getListaVeiculo() {

		return listaVeiculo;
	}

	public void setListaVeiculo(Collection<Veiculo> listaVeiculo) {

		this.listaVeiculo = listaVeiculo;
	}

	public Cliente getCliente() {

		return cliente;
	}

	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	public Collection<Motorista> getListaMotorista() {

		return listaMotorista;
	}

	public void setListaMotorista(Collection<Motorista> listaMotorista) {

		this.listaMotorista = listaMotorista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(cliente == null) {
			stringBuilder.append(CLIENTE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(listaMotorista == null || listaMotorista.isEmpty()) {
			stringBuilder.append(MOTORISTA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(listaVeiculo == null || listaVeiculo.isEmpty()) {
			stringBuilder.append(VEICULO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

}
