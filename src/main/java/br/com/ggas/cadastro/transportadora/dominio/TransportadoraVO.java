/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 15:26:24
 @author rfilho
 */

package br.com.ggas.cadastro.transportadora.dominio;

import java.io.Serializable;
/**
 * Classe responsável pela representação de uma entidade 
 *
 */
public class TransportadoraVO implements Serializable {

	private String nomeMotorista;

	private String placaVeiculo;

	private String nomeTransportadora;

	private String cnpjTransportadora;

	private Long idCliente;

	private Boolean habilitado;

	public String getNomeMotorista() {

		return nomeMotorista;
	}

	public void setNomeMotorista(String nomeMotorista) {

		this.nomeMotorista = nomeMotorista;
	}

	public String getPlacaVeiculo() {

		return placaVeiculo;
	}

	public void setPlacaVeiculo(String placaVeiculo) {

		this.placaVeiculo = placaVeiculo;
	}

	public String getNomeTransportadora() {

		return nomeTransportadora;
	}

	public void setNomeTransportadora(String nomeTransportadora) {

		this.nomeTransportadora = nomeTransportadora;
	}

	public String getCnpjTransportadora() {

		return cnpjTransportadora;
	}

	public void setCnpjTransportadora(String cnpjTransportadora) {

		this.cnpjTransportadora = cnpjTransportadora;
	}

	public Boolean getHabilitado() {

		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {

		this.habilitado = habilitado;
	}

	public Long getIdCliente() {

		return idCliente;
	}

	public void setIdCliente(Long idCliente) {

		this.idCliente = idCliente;
	}

}
