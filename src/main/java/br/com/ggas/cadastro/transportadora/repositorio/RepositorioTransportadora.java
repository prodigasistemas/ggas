/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 16:32:49
 @author rfilho
 */

package br.com.ggas.cadastro.transportadora.repositorio;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.cadastro.motorista.Motorista;
import br.com.ggas.cadastro.transportadora.dominio.Transportadora;
import br.com.ggas.cadastro.transportadora.dominio.TransportadoraVO;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Classe responsável pelos métodos de consulta de Transportadora
 *
 */
@Repository
public class RepositorioTransportadora extends RepositorioGenerico {

	private static final String ALIAS_TRANSPORTADORA = "transportadora";
	private static final String ALIAS_CLIENTE = "cliente";

	/**
	 * Instantiates a new repositorio transportadora.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioTransportadora(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new Transportadora();
	}

	@Override
	public Class<?> getClasseEntidade() {

		return Transportadora.class;
	}

	private Class<?> getClasseEntidadeVeiculo() {

		return Veiculo.class;
	}

	private Class<?> getClasseEntidadeMotorista() {

		return Motorista.class;
	}

	/**
	 * Consultar repositorio transportadora.
	 * 
	 * @param transportadoraVO
	 *            the transportadora vo
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 */
	public Collection<Transportadora> consultarRepositorioTransportadora(TransportadoraVO transportadoraVO, Boolean habilitado) {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode(ALIAS_CLIENTE, FetchMode.JOIN);
		criteria.setFetchMode("listaMotorista", FetchMode.JOIN);
		criteria.setFetchMode("listaVeiculo", FetchMode.JOIN);

		criteria.createAlias(ALIAS_CLIENTE, ALIAS_CLIENTE, Criteria.LEFT_JOIN);
		criteria.createAlias("listaMotorista", "motorista", Criteria.LEFT_JOIN);
		criteria.createAlias("listaVeiculo", "veiculo", Criteria.LEFT_JOIN);

		if(!StringUtils.isEmpty(transportadoraVO.getNomeMotorista())) {
			criteria.add(Restrictions.ilike("motorista.nome", transportadoraVO.getNomeMotorista().trim()));
		}

		if(!StringUtils.isEmpty(transportadoraVO.getPlacaVeiculo())) {
			criteria.add(Restrictions.like("veiculo.placa", transportadoraVO.getPlacaVeiculo()));
		}
		if(transportadoraVO.getIdCliente() != null && transportadoraVO.getIdCliente() > 0) {
			criteria.add(Restrictions.eq("cliente.chavePrimaria", transportadoraVO.getIdCliente()));
		}
		if(habilitado != null) {
			criteria.add(Restrictions.eq("habilitado", habilitado));
		}

		return criteria.list();
	}

	/**
	 * Consultar veiculos por transportadora.
	 * 
	 * @param chavePrimariaTransportadora
	 *            the chave primaria transportadora
	 * @return the collection
	 */
	public Collection<Veiculo> consultarVeiculosPorTransportadora(Long chavePrimariaTransportadora) {

		Criteria criteria = createCriteria(getClasseEntidadeVeiculo());

		criteria.createAlias(ALIAS_TRANSPORTADORA, ALIAS_TRANSPORTADORA, Criteria.LEFT_JOIN);

		if(chavePrimariaTransportadora != null) {
			criteria.add(Restrictions.eq("transportadora.chavePrimaria", chavePrimariaTransportadora));
		}

		return criteria.list();
	}

	/**
	 * Consultar motoristas por transportadora.
	 * 
	 * @param chavePrimariaTransportadora
	 *            the chave primaria transportadora
	 * @return the collection
	 */
	public Collection<Motorista> consultarMotoristasPorTransportadora(Long chavePrimariaTransportadora) {

		Criteria criteria = createCriteria(getClasseEntidadeMotorista());

		criteria.createAlias(ALIAS_TRANSPORTADORA, ALIAS_TRANSPORTADORA, Criteria.LEFT_JOIN);

		if(chavePrimariaTransportadora != null) {
			criteria.add(Restrictions.eq("transportadora.chavePrimaria", chavePrimariaTransportadora));
		}

		return criteria.list();
	}

	/**
	 * Listar transportadoras.
	 * 
	 * @return the collection
	 */
	public Collection<Transportadora> listarTransportadoras() {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.add(Restrictions.eq("habilitado", true));
		return criteria.list();
	}

}
