/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 16:23:31
 @author rfilho
 */

package br.com.ggas.cadastro.transportadora.negocio;

import java.util.Collection;
import java.util.List;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.motorista.Motorista;
import br.com.ggas.cadastro.transportadora.dominio.Transportadora;
import br.com.ggas.cadastro.transportadora.dominio.TransportadoraVO;
import br.com.ggas.cadastro.transportadora.dominio.TransportadoraVeiculo;
import br.com.ggas.cadastro.veiculo.Veiculo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;

/**
 * ControladorTransportadora
 * 
 * @author rfilho
 * 
 * Classe reponsável por controlar dados por entre 
 * das transportadoras.
 * 
 */
public interface ControladorTransportadora {

	/**
	 * Consultar transportadora.
	 * 
	 * @param transportadoraVO
	 *            the transportadora vo
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 */
	Collection<Transportadora> consultarTransportadora(TransportadoraVO transportadoraVO, Boolean habilitado);

	/**
	 * Incluir transportadora.
	 * 
	 * @param transportadora
	 *            the transportadora
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void incluirTransportadora(Transportadora transportadora) throws NegocioException;

	/**
	 * Alterar transportadora.
	 * 
	 * @param transportadora
	 *            the transportadora
	 * @param listaRemover
	 *            the lista remover
	 * @param listaRemoverMotorista
	 *            the lista remover motorista
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void alterarTransportadora(Transportadora transportadora, List<Long> listaRemover, List<Long> listaRemoverMotorista)
					throws ConcorrenciaException, NegocioException;

	/**
	 * Obter transportadora.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the transportadora
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Transportadora obterTransportadora(Long chavePrimaria) throws NegocioException;

	/**
	 * Verificar se existe motorista.
	 * 
	 * @param motorista
	 *            the motorista
	 * @param listaMotoristas
	 *            the lista motoristas
	 * @param indexList
	 *            the index list
	 */
	void verificarSeExisteMotorista(Motorista motorista, Collection<Motorista> listaMotoristas, Integer indexList);

	/**
	 * Verificar se existe veiculo.
	 * 
	 * @param veiculo
	 *            the veiculo
	 * @param listaVeiculos
	 *            the lista veiculos
	 * @param indexList
	 *            the index list
	 */
	void verificarSeExisteVeiculo(Veiculo veiculo, Collection<Veiculo> listaVeiculos, Integer indexList);

	/**
	 * Remover transportadora.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerTransportadora(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Consultar veiculos por transportadora.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	Collection<Veiculo> consultarVeiculosPorTransportadora(Long chavePrimaria);

	/**
	 * Consultar motoristas por transportadora.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	Collection<Motorista> consultarMotoristasPorTransportadora(Long chavePrimaria);

	/**
	 * Listar transportadoras.
	 * 
	 * @return the collection
	 */
	Collection<Transportadora> listarTransportadoras();

	/**
	 * Obter veiculo.
	 * 
	 * @param idVeiculo
	 *            the id veiculo
	 * @return the veiculo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Veiculo obterVeiculo(Long idVeiculo) throws NegocioException;

	/**
	 * Obter motorista.
	 * 
	 * @param idMotorista
	 *            the id motorista
	 * @return the motorista
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Motorista obterMotorista(Long idMotorista) throws NegocioException;

	/**
	 * Validar remocao veiculo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarRemocaoVeiculo(Long chavePrimaria) throws NegocioException;

	/**
	 * Validar remocao motorista.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarRemocaoMotorista(Long chavePrimaria) throws NegocioException;

	/**
	 * Validar existe transportadora.
	 * 
	 * @param idCliente
	 *            the id cliente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarExisteTransportadora(Long idCliente) throws NegocioException;

	/**
	 * Incluir os veiculos da transportadora.
	 * 
	 * @param veiculos
	 *            the List<TransportadoraVeiculo> 
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void incluirTransportadoraVeiculo(List<TransportadoraVeiculo> veiculos) throws NegocioException;

}
