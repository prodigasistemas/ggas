/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.unidade;

import java.util.Date;

import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados a Unidade Organizacional
 * 
 *
 */
public interface UnidadeOrganizacional extends EntidadeNegocio {

	String BEAN_ID_UNIDADE_ORGANIZACIONAL = "unidadeOrganizacional";

	String DESCRICAO = "UNIDADE_ORGANIZACIONAL_DESCRICAO";

	String EMPRESA = "UNIDADE_ORGANIZACIONAL_EMPRESA";

	String LOCALIDADE = "UNIDADE_ORGANIZACIONAL_LOCALIDADE";

	String UNIDADE_ORGANIZACIONAL = "UNIDADE_ORGANIZACIONAL";

	String SIGLA = "UNIDADE_ORGANIZACIONAL_SIGLA";

	String TIPO = "UNIDADE_ORGANIZACIONAL_TIPO";

	String UNIDADE_SUPERIOR = "UNIDADE_ORGANIZAZIONAL_SUPERIOR";

	String UNIDADE_CENTRALIZADORA = "UNIDADE_ORGANIZAZIONAL_CENTRALIZADORA";

	String UNIDADE_REPAVIMENTADORA = "UNIDADE_ORGANIZAZIONAL_REPAVIMENTADORA";

	String MUNICIPIO = "UNIDADE_ORGANIZACIONAL_MUNICIPIO";

	String GERENCIA_REGIONAL = "UNIDADE_ORGANIZACIONAL_GERENCIA_REGIONAL";

	String UNIDADE_TIPO = "";

	String CANAL_ATENDIMENTO = "";

	String HORA_INICIO_EXPEDIENTE = "UNIDADE_ORGANIZACIONAL_HORA_INICIO_EXPEDIENTE";

	String HORA_FIM_EXPEDIENTE = "UNIDADE_ORGANIZACIONAL_HORA_FIM_EXPEDIENTE";
	
	String EMAIL_CONTATO = "EMAIL_CONTATO";
	
	
	/**
	 * @return the indicadorTramite
	 */
	boolean isIndicadorTramite();

	/**
	 * @param indicadorTramite
	 *            the indicadorTramite to set
	 */
	void setIndicadorTramite(boolean indicadorTramite);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the sigla
	 */
	String getSigla();

	/**
	 * @param sigla
	 *            the sigla to set
	 */
	void setSigla(String sigla);

	/**
	 * @return the registroAtendimento
	 */
	boolean isIndicadorAbreChamado();

	/**
	 * @param indicadorRegistroAtendimento - Set indicador registro atendimento.
	 */
	void setIndicadorAbreChamado(boolean indicadorRegistroAtendimento);

	/**
	 * @return the centralAtendimento
	 */
	boolean isIndicadorCentralAtendimento();

	/**
	 * @param indicadorCentralAtendimento - Set indicador central de atendimento.
	 */
	void setIndicadorCentralAtendimento(boolean indicadorCentralAtendimento);

	/**
	 * @return the empresa
	 */
	Empresa getEmpresa();

	/**
	 * @param empresa
	 */
	void setEmpresa(Empresa empresa);

	/**
	 * @return the localidade
	 */
	Localidade getLocalidade();

	/**
	 * @param localidade
	 *            the localidade to set
	 */
	void setLocalidade(Localidade localidade);

	/**
	 * @return the unidadeSuperior
	 */
	UnidadeOrganizacional getUnidadeSuperior();

	/**
	 * @param unidadeSuperior
	 *            the unidadeSuperior to set
	 */
	void setUnidadeSuperior(UnidadeOrganizacional unidadeSuperior);

	/**
	 * @return the unidadeCentralizadora
	 */
	UnidadeOrganizacional getUnidadeCentralizadora();

	/**
	 * @param unidadeCentralizadora
	 *            the unidadeCentralizadora to set
	 */
	void setUnidadeCentralizadora(UnidadeOrganizacional unidadeCentralizadora);

	/**
	 * @return the unidadeRepavimentadora
	 */
	UnidadeOrganizacional getUnidadeRepavimentadora();

	/**
	 * @param unidadeRepavimentadora
	 *            the unidadeRepavimentadora to
	 *            set
	 */
	void setUnidadeRepavimentadora(UnidadeOrganizacional unidadeRepavimentadora);

	/**
	 * @return the inicioExpediente
	 */
	Date getInicioExpediente();

	/**
	 * @param inicioExpediente
	 *            the inicioExpediente to set
	 */
	void setInicioExpediente(Date inicioExpediente);

	/**
	 * @return the fimExpediente
	 */
	Date getFimExpediente();

	/**
	 * @param fimExpediente
	 *            the fimExpediente to set
	 */
	void setFimExpediente(Date fimExpediente);

	/**
	 * @return the indicadorChat
	 */
	boolean isIndicadorChat();

	/**
	 * @param indicadorChat
	 *            the indicadorChat to set
	 */
	void setIndicadorChat(boolean indicadorChat);

	/**
	 * @return the indicadorEmail
	 */
	boolean isIndicadorEmail();

	/**
	 * @param indicadorEmail
	 *            the indicadorEmail to set
	 */
	void setIndicadorEmail(boolean indicadorEmail);

	/**
	 * @return the municipio
	 */
	Municipio getMunicipio();

	/**
	 * @param municipio
	 *            the municipio to set
	 */
	void setMunicipio(Municipio municipio);

	/**
	 * @return the gerenciaRegional
	 */
	GerenciaRegional getGerenciaRegional();

	/**
	 * @param gerenciaRegional
	 *            the gerenciaRegional to set
	 */
	void setGerenciaRegional(GerenciaRegional gerenciaRegional);

	/**
	 * @return the canalAtendimento
	 */
	CanalAtendimento getCanalAtendimento();

	/**
	 * @param canalAtendimento
	 *            the canalAtendimento to set
	 */
	void setCanalAtendimento(CanalAtendimento canalAtendimento);

	/**
	 * @return the unidadeTipo
	 */
	UnidadeTipo getUnidadeTipo();

	/**
	 * @param unidadeTipo
	 *            the unidadeTipo to set
	 */
	void setUnidadeTipo(UnidadeTipo unidadeTipo);

	/**
	 * @return the emailContato
	 */
	public String getEmailContato();

	/**
	 * @param emailContato the emailContato to set
	 */
	public void setEmailContato(String emailContato);
	
}
