/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.cadastro.unidade.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeTipo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 *	Classe responsável pela implementação de Unidade Organizacional e métodos para seus atributos.  
 *
 */
public class UnidadeOrganizacionalImpl extends EntidadeNegocioImpl implements UnidadeOrganizacional {

	private static final int LIMITE_CAMPO = 2;

	private static final int TAMANHO_LIMITE_DESCRICAO = 250;

	private static final long serialVersionUID = -5966270924315627354L;

	private String descricao;

	private String sigla;

	private boolean indicadorTramite;

	private boolean indicadorAbreChamado;

	private boolean indicadorCentralAtendimento;

	private Empresa empresa;

	private Localidade localidade;

	private UnidadeOrganizacional unidadeSuperior;

	private UnidadeOrganizacional unidadeCentralizadora;

	private UnidadeOrganizacional unidadeRepavimentadora;

	private Municipio municipio;

	private GerenciaRegional gerenciaRegional;

	private CanalAtendimento canalAtendimento;

	private UnidadeTipo unidadeTipo;

	private Date inicioExpediente;

	private Date fimExpediente;

	private boolean indicadorChat;

	private boolean indicadorEmail;
	
	private String emailContato;

	public UnidadeOrganizacionalImpl() {
	}

	/**
	 * Unidade Organizacional.
	 *
	 * @param chavePrimaria the chave Primaria
	 * @param descricao the descricao
	 */
	public UnidadeOrganizacionalImpl(Long chavePrimaria, String descricao) {
		this.setChavePrimaria(chavePrimaria);
		this.descricao = descricao;
	}

	/**
	 * Construtor da unidade organizacional
	 * @param chavePrimaria A chave primária
	 * @param descricao A descrição
	 * @param emailContato O email
	 * @param unidade A unidade
	 * @param indicadorEmail O indicadorEmail
	 */
	public UnidadeOrganizacionalImpl(Long chavePrimaria, String descricao,String emailContato,UnidadeOrganizacional unidade
			,boolean indicadorEmail) {
		this.setChavePrimaria(chavePrimaria);
		this.descricao = descricao;
		this.emailContato = emailContato;
		this.unidadeSuperior = unidade;
		this.indicadorEmail = indicadorEmail;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#isIndicadorEmail()
	 */
	@Override
	public boolean isIndicadorEmail() {

		return indicadorEmail;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setIndicadorEmail(boolean)
	 */
	@Override
	public void setIndicadorEmail(boolean indicadorEmail) {

		this.indicadorEmail = indicadorEmail;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#isIndicadorChat()
	 */
	@Override
	public boolean isIndicadorChat() {

		return indicadorChat;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setIndicadorChat(boolean)
	 */
	@Override
	public void setIndicadorChat(boolean indicadorChat) {

		this.indicadorChat = indicadorChat;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#getInicioExpediente()
	 */
	@Override
	public Date getInicioExpediente() {
		Date data = null;
		if(this.inicioExpediente != null) {
			data = (Date) inicioExpediente.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setInicioExpediente(java.util.Date)
	 */
	@Override
	public void setInicioExpediente(Date inicioExpediente) {
		if(inicioExpediente != null){
			this.inicioExpediente = (Date) inicioExpediente.clone();
		} else {
			this.inicioExpediente = null;
		}
	}

	/**
	 * @return the fimExpediente
	 */
	@Override
	public Date getFimExpediente() {

		return fimExpediente;
	}

	/**
	 * @param fimExpediente
	 *            the fimExpediente to set
	 */
	@Override
	public void setFimExpediente(Date fimExpediente) {
		if (fimExpediente != null) {
			this.fimExpediente = (Date) fimExpediente.clone();
		} else {
			this.fimExpediente = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#isIndicadorTramite()
	 */
	@Override
	public boolean isIndicadorTramite() {

		return indicadorTramite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setIndicadorTramite(boolean)
	 */
	@Override
	public void setIndicadorTramite(boolean indicadorTramite) {

		this.indicadorTramite = indicadorTramite;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#getSigla()
	 */
	@Override
	public String getSigla() {

		return sigla;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setSigla(java.lang.String)
	 */
	@Override
	public void setSigla(String sigla) {

		this.sigla = sigla;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #isIndicadorCentralAtendimento()
	 */
	@Override
	public boolean isIndicadorCentralAtendimento() {

		return indicadorCentralAtendimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #isIndicadorAbreChamado()
	 */
	@Override
	public boolean isIndicadorAbreChamado() {

		return indicadorAbreChamado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setIndicadorCentralAtendimento(boolean)
	 */
	@Override
	public void setIndicadorCentralAtendimento(boolean indicadorCentralAtendimento) {

		this.indicadorCentralAtendimento = indicadorCentralAtendimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setIndicadorRegistroAtendimento(boolean)
	 */
	@Override
	public void setIndicadorAbreChamado(boolean indicadorAbreChamado) {

		this.indicadorAbreChamado = indicadorAbreChamado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#getEmpresa()
	 */
	@Override
	public Empresa getEmpresa() {

		return empresa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setEmpresa(br.com.ggas
	 * .cadastro.empresa.Empresa)
	 */
	@Override
	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#getLocalidade()
	 */
	@Override
	public Localidade getLocalidade() {

		return localidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setLocalidade(br.com.ggas
	 * .cadastro.localidade.Localidade)
	 */
	@Override
	public void setLocalidade(Localidade localidade) {

		this.localidade = localidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#getUnidadeSuperior()
	 */
	@Override
	public UnidadeOrganizacional getUnidadeSuperior() {

		return unidadeSuperior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setUnidadeSuperior(br.
	 * com.ggas.cadastro.unidade
	 * .UnidadeOrganizacional)
	 */
	@Override
	public void setUnidadeSuperior(UnidadeOrganizacional unidadeSuperior) {

		this.unidadeSuperior = unidadeSuperior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #getUnidadeCentralizadora()
	 */
	@Override
	public UnidadeOrganizacional getUnidadeCentralizadora() {

		return unidadeCentralizadora;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setUnidadeCentralizadora
	 * (br.com.ggas.cadastro
	 * .unidade.UnidadeOrganizacional)
	 */
	@Override
	public void setUnidadeCentralizadora(UnidadeOrganizacional unidadeCentralizadora) {

		this.unidadeCentralizadora = unidadeCentralizadora;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #getUnidadeRepavimentadora()
	 */
	@Override
	public UnidadeOrganizacional getUnidadeRepavimentadora() {

		return unidadeRepavimentadora;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setUnidadeRepavimentadora
	 * (br.com.ggas.cadastro
	 * .unidade.UnidadeOrganizacional)
	 */
	@Override
	public void setUnidadeRepavimentadora(UnidadeOrganizacional unidadeRepavimentadora) {

		this.unidadeRepavimentadora = unidadeRepavimentadora;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#getMunicipio()
	 */
	@Override
	public Municipio getMunicipio() {

		return municipio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setMunicipio(br.com.ggas
	 * .cadastro.geografico.Municipio)
	 */
	@Override
	public void setMunicipio(Municipio municipio) {

		this.municipio = municipio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#getGerenciaRegional()
	 */
	@Override
	public GerenciaRegional getGerenciaRegional() {

		return gerenciaRegional;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setGerenciaRegional(br
	 * .com.ggas.cadastro.localidade
	 * .GerenciaRegional)
	 */
	@Override
	public void setGerenciaRegional(GerenciaRegional gerenciaRegional) {

		this.gerenciaRegional = gerenciaRegional;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#getMeioSolicitacao()
	 */
	@Override
	public CanalAtendimento getCanalAtendimento() {

		return canalAtendimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setMeioSolicitacao(br.
	 * com.ggas.atendimento.
	 * registroatendimento.MeioSolicitacao)
	 */
	@Override
	public void setCanalAtendimento(CanalAtendimento canalAtendimento) {

		this.canalAtendimento = canalAtendimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional#getUnidadeTipo()
	 */
	@Override
	public UnidadeTipo getUnidadeTipo() {

		return unidadeTipo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * UnidadeOrganizacional
	 * #setUnidadeTipo(br.com.
	 * ggas.cadastro.unidade.UnidadeTipo)
	 */
	@Override
	public void setUnidadeTipo(UnidadeTipo unidadeTipo) {

		this.unidadeTipo = unidadeTipo;
	}

	/**
	 * @return the emailContato
	 */
	public String getEmailContato() {
		return emailContato;
	}

	/**
	 * @param emailContato the emailContato to set
	 */
	public void setEmailContato(String emailContato) {
		this.emailContato = emailContato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder camposObrigatoriosAcumulados = new StringBuilder();
		StringBuilder tamanhoCamposAcumulados = new StringBuilder();
		String camposObrigatorios = null;
		String camposInvalidos = null;

		if(unidadeTipo == null) {
			camposObrigatoriosAcumulados.append(TIPO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(empresa == null) {
			camposObrigatoriosAcumulados.append(EMPRESA);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(descricao)) {
			camposObrigatoriosAcumulados.append(DESCRICAO);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		} else if(this.descricao.length() > TAMANHO_LIMITE_DESCRICAO) {
			tamanhoCamposAcumulados.append(DESCRICAO);
			tamanhoCamposAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(inicioExpediente == null) {
			camposObrigatoriosAcumulados.append(HORA_INICIO_EXPEDIENTE);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(fimExpediente == null) {
			camposObrigatoriosAcumulados.append(HORA_FIM_EXPEDIENTE);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = camposObrigatoriosAcumulados.toString();
		camposInvalidos = tamanhoCamposAcumulados.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
							camposObrigatorios.substring(0, camposObrigatoriosAcumulados.toString().length() - LIMITE_CAMPO));
		}

		if(camposInvalidos.length() > 0) {
			erros.put(Constantes.ERRO_TAMANHO_LIMITE, camposInvalidos.substring(0, tamanhoCamposAcumulados.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

}
