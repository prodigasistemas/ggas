/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.cadastro.unidade.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeTipo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
/**
 *	Classe responsável pelos métodos de consulta validação de Unidade Organizacional
 *
 */
public class ControladorUnidadeOrganizacionalImpl extends ControladorNegocioImpl implements ControladorUnidadeOrganizacional {

	private static final String WHERE = " where ";
	private static final String FROM = " from ";

	// TODO override o pre-remocao para verificar se a unidade organizacional possui vinculoscom outra entidade
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(UnidadeOrganizacional.BEAN_ID_UNIDADE_ORGANIZACIONAL);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(UnidadeOrganizacional.BEAN_ID_UNIDADE_ORGANIZACIONAL);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.unidade.
	 * ControladorUnidadeOrganizacional
	 * #criarUnidadeTipo()
	 */
	@Override
	public EntidadeNegocio criarUnidadeTipo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(UnidadeTipo.BEAN_ID_UNIDADE_TIPO);
	}

	public Class<?> getClasseEntidadeUnidadeTipo() {

		return ServiceLocator.getInstancia().getClassPorID(UnidadeTipo.BEAN_ID_UNIDADE_TIPO);
	}

	public Class<?> getClasseEntidadeCanalAtendimento() {

		return ServiceLocator.getInstancia().getClassPorID(CanalAtendimento.BEAN_ID_CANAL_ATENDIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		UnidadeOrganizacional unidadeOrganizacional = (UnidadeOrganizacional) entidadeNegocio;
		verificarUnidadeOrganizacionalExistenteDaMesmaEmpresa(unidadeOrganizacional);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		UnidadeOrganizacional unidadeOrganizacional = (UnidadeOrganizacional) entidadeNegocio;
		verificarUnidadeOrganizacionalExistenteDaMesmaEmpresa(unidadeOrganizacional);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * ControladorUnidadeOrganizacional#
	 * consultarUnidadeOrganizacional(java.util.Map
	 * )
	 */
	@SuppressWarnings({"unchecked", "squid:S1192"})
	@Override
	public Collection<UnidadeOrganizacional> consultarUnidadeOrganizacional(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
			}

			criteria.createAlias("unidadeTipo", "unidadeTipo");

			Long idUnidadeTipo = (Long) filtro.get("idUnidadeTipo");
			if(idUnidadeTipo != null && idUnidadeTipo > 0) {
				criteria.add(Restrictions.eq("unidadeTipo.chavePrimaria", idUnidadeTipo));
			} else {
				criteria.setFetchMode("unidadeTipo", FetchMode.JOIN);
			}

			Integer nivel = (Integer) filtro.get("nivelTipoUnidade");
			if(nivel != null && nivel > 0) {
				criteria.add(Restrictions.eq("unidadeTipo.nivel", nivel));
			} else {
				criteria.setFetchMode("unidadeTipo", FetchMode.JOIN);
			}

			String tipoUnidadeTipo = (String) filtro.get("tipoUnidadeTipo");
			if(!StringUtils.isEmpty(tipoUnidadeTipo)) {
				criteria.add(Restrictions.ilike("unidadeTipo.tipo", tipoUnidadeTipo, MatchMode.ANYWHERE));
			}

			String descricao = (String) filtro.get("descricaoUnidade");
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricao, MatchMode.ANYWHERE));
			}

			String sigla = (String) filtro.get("siglaUnidade");
			if(!StringUtils.isEmpty(sigla)) {
				criteria.add(Restrictions.ilike("sigla", sigla, MatchMode.ANYWHERE));
			}

			Long idEmpresa = (Long) filtro.get("idEmpresa");
			if(idEmpresa != null && idEmpresa > 0) {
				criteria.createAlias("empresa", "empresa");
				criteria.add(Restrictions.eq("empresa.chavePrimaria", idEmpresa));
			} else {
				criteria.setFetchMode("empresa", FetchMode.JOIN);
			}

			Long idGerenciaRegional = (Long) filtro.get("idGerenciaRegional");
			if(idGerenciaRegional != null && idGerenciaRegional > 0) {
				criteria.createAlias("gerenciaRegional", "gerenciaRegional");
				criteria.add(Restrictions.eq("gerenciaRegional.chavePrimaria", idGerenciaRegional));
			} else {
				criteria.setFetchMode("gerenciaRegional", FetchMode.JOIN);
			}

			Long idLocalidade = (Long) filtro.get("idLocalidade");
			if(idLocalidade != null && idLocalidade > 0) {
				criteria.createAlias("localidade", "localidade");
				criteria.add(Restrictions.eq("localidade.chavePrimaria", idLocalidade));
			} else {
				criteria.setFetchMode("localidade", FetchMode.JOIN);
			}

			Long idUnidadeSuperior = (Long) filtro.get("idUnidadeSuperior");
			if(idUnidadeSuperior != null && idUnidadeSuperior > 0) {
				criteria.createAlias("unidadeSuperior", "unidadeSuperior");
				criteria.add(Restrictions.eq("unidadeSuperior.chavePrimaria", idUnidadeSuperior));
			} else {
				criteria.setFetchMode("unidadeSuperior", FetchMode.JOIN);
			}

			Long idUnidadeCentralizadora = (Long) filtro.get("idUnidadeCentralizadora");
			if(idUnidadeCentralizadora != null && idUnidadeCentralizadora > 0) {
				criteria.createAlias("unidadeCentralizadora", "unidadeCentralizadora");
				criteria.add(Restrictions.eq("unidadeCentralizadora.chavePrimaria", idUnidadeCentralizadora));
			} else {
				criteria.setFetchMode("unidadeCentralizadora", FetchMode.JOIN);
			}

			Long idCanalAtendimento = (Long) filtro.get("idCanalAtendimento");
			if(idCanalAtendimento != null && idCanalAtendimento > 0) {
				criteria.createAlias("canalAtendimento", "canalAtendimento");
				criteria.add(Restrictions.eq("canalAtendimento.chavePrimaria", idCanalAtendimento));
			} else {
				criteria.setFetchMode("canalAtendimento", FetchMode.JOIN);
			}

			Boolean indicadorTramite = (Boolean) filtro.get("indicadorTramite");
			if(indicadorTramite != null) {
				criteria.add(Restrictions.eq("indicadorTramite", indicadorTramite));
			}

			Boolean indicadorAbreChamado = (Boolean) filtro.get("indicadorAbreChamado");
			if(indicadorAbreChamado != null) {
				criteria.add(Restrictions.eq("indicadorAbreChamado", indicadorAbreChamado));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			Long idMunicipio = (Long) filtro.get("idMunicipio");
			if(idMunicipio != null && idMunicipio > 0) {
				criteria.add(Restrictions.eq("municipio.chavePrimaria", idMunicipio));
			}
			
			String emailContato = (String) filtro.get("emailContato");
			if(!StringUtils.isEmpty(emailContato)) {
				criteria.add(Restrictions.ilike("emailContato", emailContato, MatchMode.ANYWHERE));
			}
		}

		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * ControladorUnidadeOrganizacional#
	 * listarUnidadesCentralizadoras()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<UnidadeOrganizacional> listarUnidadesCentralizadoras() throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.createAlias("unidadeTipo", "unidadeTipo");
		// FIXME: Substituir por constante
		criteria.add(Restrictions.eq("unidadeTipo.tipo", UnidadeTipo.CENTRALIZADORA));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.unidade.
	 * ControladorUnidadeOrganizacional
	 * #listarUnidadesSuperiores
	 * ()
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public Collection<UnidadeOrganizacional> listarUnidadesSuperiores() throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.add(Restrictions.isNotNull("unidadeSuperior"));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
		List<UnidadeOrganizacional> unidades = criteria.list();
		List<UnidadeOrganizacional> unidadesSuperiores = new ArrayList<>();

		if(!unidades.isEmpty()) {
			for (UnidadeOrganizacional unidadeOrganizacional : unidades) {
				unidadesSuperiores.add(unidadeOrganizacional.getUnidadeSuperior());
			}

			Collections.sort(unidadesSuperiores, new Comparator(){

				@Override
				public int compare(Object o1, Object o2) {

					UnidadeOrganizacional p1 = (UnidadeOrganizacional) o1;
					UnidadeOrganizacional p2 = (UnidadeOrganizacional) o2;
					return p1.getDescricao().compareToIgnoreCase(p2.getDescricao());
				}
			});
		}

		return unidadesSuperiores;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * ControladorUnidadeOrganizacional#
	 * consultarQuantidadeUnidadesOrganizacionaisPelaEmpresa
	 * (java.lang.Long)
	 */
	@Override
	public Integer consultarQuantidadeUnidadesOrganizacionaisPelaEmpresa(Long chaveEmpresa) throws NegocioException {


		Long quantidade = 0L;
		if(chaveEmpresa != null && chaveEmpresa > 0) {
			Criteria criteria = getCriteria();
			criteria.setProjection(Projections.rowCount());
			criteria.createAlias("empresa", "empresa");
			criteria.add(Restrictions.eq("empresa.chavePrimaria", chaveEmpresa));
			quantidade = (Long) criteria.uniqueResult();
		}
		return quantidade.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.unidade.
	 * ControladorUnidadeOrganizacional
	 * #listarcanalAtendimento
	 * ()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<CanalAtendimento> listarCanalAtendimento() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeCanalAtendimento());
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.unidade.
	 * ControladorUnidadeOrganizacional
	 * #listarUnidadeTipo()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<UnidadeTipo> listarUnidadeTipo() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeUnidadeTipo());
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		return criteria.list();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<UnidadeOrganizacional> listarUnidadesOrganizacionaisInferiores(Long idUnidadeSuperior) {
		return createCriteria(getClasseEntidade()).add(
				Restrictions.eq("unidadeSuperior.id", idUnidadeSuperior)
		).list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional#listarNivelUnidadeTipo()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<String> listarNivelUnidadeTipo() throws NegocioException {

		Collection<String> listarNivelUnidadeTipoString = new ArrayList<>();
		Collection<Integer> listarNivelUnidadeTipoInteger = null;

		Criteria criteria = createCriteria(getClasseEntidadeUnidadeTipo());
		criteria.addOrder(Order.asc("nivel"));
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.setProjection(Projections.distinct(Projections.property("nivel")));
		listarNivelUnidadeTipoInteger = criteria.list();

		if(listarNivelUnidadeTipoInteger != null && !listarNivelUnidadeTipoInteger.isEmpty()) {
			for (Integer nivel : listarNivelUnidadeTipoInteger) {
				if(nivel != null) {
					listarNivelUnidadeTipoString.add(nivel.toString());
				}

			}

		}

		return listarNivelUnidadeTipoString;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional#listarTipoUnidadeTipo()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<String> listarTipoUnidadeTipo() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeUnidadeTipo());
		criteria.addOrder(Order.asc("tipo"));
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.setProjection(Projections.distinct(Projections.property("tipo")));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.unidade.
	 * ControladorUnidadeOrganizacional
	 * #obterUnidadeTipo(java
	 * .lang.Long)
	 */
	@Override
	public UnidadeTipo obterUnidadeTipo(Long chavePrimaria) throws NegocioException {

		return (UnidadeTipo) super.obter(chavePrimaria, getClasseEntidadeUnidadeTipo());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.unidade.
	 * ControladorUnidadeOrganizacional
	 * #obtercanalAtendimento
	 * (java.lang.Long)
	 */
	@Override
	public CanalAtendimento obterCanalAtendimento(Long chavePrimaria) throws NegocioException {

		return (CanalAtendimento) super.obter(chavePrimaria, getClasseEntidadeCanalAtendimento());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.
	 * ControladorUnidadeOrganizacional#
	 * validarRemoverUnidadeOrgazinacional(java.lang
	 * .Long[])
	 */
	@Override
	public void validarRemoverUnidadeOrgazinacional(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/**
	 * Verificar unidade organizacional existente da mesma empresa.
	 * 
	 * @param unidadeOrganizacional
	 *            the unidade organizacional
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings({"unchecked", "squid:S1192"})
	private void verificarUnidadeOrganizacionalExistenteDaMesmaEmpresa(UnidadeOrganizacional unidadeOrganizacional) throws NegocioException {

		Collection<UnidadeOrganizacional> listaUnidadeOrganizacional = null;
		Long chavePrimaria = unidadeOrganizacional.getChavePrimaria();

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append("upper (descricao) like upper(:desc)  ");

		if(unidadeOrganizacional.getSigla() != null) {
			hql.append(" and upper (sigla) like upper(:sig) ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("desc", unidadeOrganizacional.getDescricao());

		if(unidadeOrganizacional.getSigla() != null) {
			query.setString("sig", unidadeOrganizacional.getSigla());
		}

		listaUnidadeOrganizacional = query.list();

		if (listaUnidadeOrganizacional != null && !listaUnidadeOrganizacional.isEmpty()) {

			for (UnidadeOrganizacional unidadeOrg : listaUnidadeOrganizacional) {

				if (chavePrimaria != null && chavePrimaria > 0) {

					// se já existir no banco irá verificar se o registro que se quer alterar não é o
					// mesmo retornado pela consulta

					Long chave = unidadeOrg.getChavePrimaria();

					if (!chave.equals(chavePrimaria)
									&& unidadeOrg.getEmpresa().getChavePrimaria() == unidadeOrganizacional.getEmpresa().getChavePrimaria()) {
						if (unidadeOrg.getMunicipio() == null && unidadeOrganizacional.getMunicipio() == null) {
							throw new NegocioException(ControladorUnidadeOrganizacional.ERRO_NEGOCIO_EMPRESA_IGUAL_UNIDADE_ORGANIZACINAL,
											true);
						} else if (unidadeOrg.getMunicipio() != null
										&& unidadeOrganizacional.getMunicipio() != null
										&& unidadeOrg.getMunicipio().getChavePrimaria() == unidadeOrganizacional.getMunicipio()
														.getChavePrimaria()) {
							throw new NegocioException(ControladorUnidadeOrganizacional.ERRO_NEGOCIO_EMPRESA_IGUAL_UNIDADE_ORGANIZACINAL,
											true);
						}
					}
				} else {
					// se não existir no banco entrará nesse fluxo
					if (unidadeOrg.getEmpresa().getChavePrimaria() == unidadeOrganizacional.getEmpresa().getChavePrimaria()) {
						if (unidadeOrg.getMunicipio() == null && unidadeOrganizacional.getMunicipio() == null) {
							throw new NegocioException(ControladorUnidadeOrganizacional.ERRO_NEGOCIO_EMPRESA_IGUAL_UNIDADE_ORGANIZACINAL,
											true);
						} else if (unidadeOrg.getMunicipio() != null
										&& unidadeOrganizacional.getMunicipio() != null
										&& unidadeOrg.getMunicipio().getChavePrimaria() == unidadeOrganizacional.getMunicipio()
														.getChavePrimaria()) {
							throw new NegocioException(ControladorUnidadeOrganizacional.ERRO_NEGOCIO_EMPRESA_IGUAL_UNIDADE_ORGANIZACINAL,
											true);
						}
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional#consultarUnidadeOrganizacionalOrdenada()
	 */
	@Override
	public Collection<UnidadeOrganizacional> consultarUnidadeOrganizacionalOrdenada() {

		StringBuilder hql = getQueryUnidadeOrganizacional();
		hql.append(" order by unidadeOrganizacional.descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();

	}

	@SuppressWarnings("squid:S1192")
	private StringBuilder getQueryUnidadeOrganizacional() {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT new UnidadeOrganizacionalImpl(unidadeOrganizacional.chavePrimaria, unidadeOrganizacional.descricao) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" unidadeOrganizacional ");
		return hql;
	}

	@Override
	public UnidadeOrganizacional consultarUnidadeOrganizacionalPorChamadoAssunto(Long chavePrimariaChamadoAssunto) {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT new UnidadeOrganizacionalImpl(unidadeOrganizacional.chavePrimaria, unidadeOrganizacional.descricao) ");
		hql.append(FROM);
		hql.append(" ChamadoAssunto ");
		hql.append(" chamado ");
		hql.append(" inner join chamado.unidadeOrganizacional unidadeOrganizacional");
		hql.append(WHERE);
		hql.append(" chamado.chavePrimaria= :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chavePrimaria", chavePrimariaChamadoAssunto);
		query.setMaxResults(1);

		try {
			return (UnidadeOrganizacional) query.uniqueResult();
		} catch (NonUniqueResultException ex) {
			return null;
		}
	}

	public Collection<UnidadeOrganizacional> obterUnidadesOrganizacionalPorChavesPrimarias(Collection<Long> chaves) {

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT new UnidadeOrganizacionalImpl(unidadeOrganizacional.chavePrimaria, unidadeOrganizacional.descricao) ");
		hql.append(" FROM ");
		hql.append(" UnidadeOrganizacionalImpl unidadeOrganizacional ");
		hql.append(" WHERE unidadeOrganizacional.chavePrimaria in (:chaves) ");
		hql.append(" ORDER BY unidadeOrganizacional.descricao ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if (chaves == null || chaves.isEmpty()) {
			chaves = Arrays.asList(0L);
		}
		query.setParameterList("chaves", chaves);

		@SuppressWarnings("unchecked")
		Collection<UnidadeOrganizacional> unidades = query.list();

		return unidades;
	}
}
