package br.com.ggas.cadastro.unidade.conversor;

import org.apache.log4j.Logger;

import br.com.ggas.atendimento.material.apresentacao.conversor.UnidadeConverter;
import br.com.ggas.cadastro.unidade.UnidadeTipo;
import br.com.ggas.cadastro.unidade.impl.UnidadeTipoImpl;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;

import org.springframework.core.convert.converter.Converter;
/**
 * Classe responsável por conveter uma String
 * que representa uma chave primária em uma entidade da classe UnidadeTipo.
 *
 */
public class UnidadeTipoConverter implements Converter<String, UnidadeTipo> {

	private static final Logger LOG = Logger.getLogger(UnidadeConverter.class);

	@Override
	public UnidadeTipo convert(String chave) {

		UnidadeTipo unidadeTipo = null;
		if (chave != null && !"".equals(chave) && Long.parseLong(chave) > 0) {
			try {
				long chaveBuscar = Long.parseLong(chave);
				unidadeTipo =
						(UnidadeTipo) ServiceLocator.getInstancia().getControladorTabelaAuxiliar()
								.obter(chaveBuscar, UnidadeTipoImpl.class);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
			}
		}

		return unidadeTipo;
	}

}
