package br.com.ggas.cadastro.unidade.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;
/**
 * LocalidadeConverter
 * 
 * @author pedro
 *  
 * Classe responsável por converter uma String que representa uma chave primária
 * em uma entidade do tipo Localidade.
 * 
 */
public class LocalidadeConverter implements Converter<String, Localidade> {

	private static final Logger LOG = Logger.getLogger(Localidade.class);
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	@Override
	public Localidade convert(String chave) {

		Localidade localidade = null;
		if (chave != null && !"".equals(chave) && Long.parseLong(chave) > 0) {
			try {
				Long chaveLong = Long.parseLong(chave);
				localidade = (Localidade) ServiceLocator.getInstancia().getControladorLocalidade().obter(chaveLong);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return localidade;
	}

}
