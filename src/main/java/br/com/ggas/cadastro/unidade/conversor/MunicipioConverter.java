package br.com.ggas.cadastro.unidade.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;
/**
 * Classe responsável por conveter uma String
 * que representa uma chave primária em uma entidade da classe Municipio.
 *
 */
public class MunicipioConverter implements Converter<String, Municipio>{

	private static final Logger LOG = Logger.getLogger(Municipio.class);

	@Override
	public Municipio convert(String chave) {

		Municipio municipio= null;
		if (chave != null && !"".equals(chave) && Long.parseLong(chave) > 0) {
			try {
				Long chaveLong = Long.parseLong(chave);
				municipio = (Municipio) ServiceLocator.getInstancia().getControladorMunicipio().obter(chaveLong);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return municipio;
	}


}
