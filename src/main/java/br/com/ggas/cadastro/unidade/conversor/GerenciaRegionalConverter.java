package br.com.ggas.cadastro.unidade.conversor;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.impl.GerenciaRegionalImpl;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe responsável por conveter uma String
 * que representa uma chave primária em uma entidade da classe GerenciaRegional.
 *
 */
public class GerenciaRegionalConverter implements Converter<String, GerenciaRegional> {

	private static final Logger LOG = Logger.getLogger(GerenciaRegional.class);

	@Override
	public GerenciaRegional convert(String chave) {

		GerenciaRegional gerenciaRegional = null;
		if (chave != null && !"".equals(chave) && Long.parseLong(chave) > 0) {
			try {
				Long chaveLong = Long.parseLong(chave);
				gerenciaRegional =
						(GerenciaRegional) ServiceLocator.getInstancia().getControladorEntidadeConteudo()
								.obter(chaveLong, GerenciaRegionalImpl.class);
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
			}
		}

		return gerenciaRegional;
	}

}
