/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.unidade;

import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

import java.util.Collection;
import java.util.Map;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador de Unidade Organizacional
 *
 */
public interface ControladorUnidadeOrganizacional extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_UNIDADE_ORGANIZACIONAL = "controladorUnidadeOrganizacional";

	String ERRO_NEGOCIO_DESCRICAO_EXISTENTE = "ERRO_NEGOCIO_DESCRICAO_EXISTENTE";

	String ERRO_NEGOCIO_SIGLA_EXISTENTE = "ERRO_NEGOCIO_SIGLA_EXISTENTE";

	String ERRO_NEGOCIO_EMPRESA_PRINCIPAL_UNIDADE_TERCEIRIZADA = "ERRO_NEGOCIO_EMPRESA_PRINCIPAL_UNIDADE_TERCEIRIZADA";

	String ERRO_NEGOCIO_REMOVER_UNIDADE_TIPO = "ERRO_NEGOCIO_REMOVER_UNIDADE_TIPO";

	String ERRO_NEGOCIO_REMOVER_AREA_TIPO = "ERRO_NEGOCIO_REMOVER_AREA_TIPO";

	String ERRO_NEGOCIO_REMOVER_ZONA_BLOQUEIO = "ERRO_NEGOCIO_REMOVER_ZONA_BLOQUEIO";

	String ERRO_NEGOCIO_REMOVER_SETOR_CENSITARIO = "ERRO_NEGOCIO_REMOVER_SETOR_CENSITARIO";

	String ERRO_NEGOCIO_REMOVER_ZEIS = "ERRO_NEGOCIO_REMOVER_ZEIS";

	String ERRO_NEGOCIO_REMOVER_CANAL_ATENDIMENTO = "ERRO_NEGOCIO_REMOVER_CANAL_ATENDIMENTO";

	String ERRO_NEGOCIO_INSERIR_UNIDADE_ORGANIZACINAL = "ERRO_NEGOCIO_INSERIR_UNIDADE_ORGANIZACINAL";

	String ERRO_NEGOCIO_EMPRESA_IGUAL_UNIDADE_ORGANIZACINAL = "ERRO_NEGOCIO_EMPRESA_IGUAL_UNIDADE_ORGANIZACINAL";

	/**
	 * Método que cria a entidade UnidadeTipo.
	 * 
	 * @return Instância de UnidadeTipo
	 */
	EntidadeNegocio criarUnidadeTipo();

	/**
	 * Método responsável por consultar as
	 * unidades organizacionais pelo filtro
	 * informado.
	 * 
	 * @param filtro
	 *            contendo os parametros da
	 *            pesquisa.
	 * @return coleção de unidade organizacional.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<UnidadeOrganizacional> consultarUnidadeOrganizacional(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * unidades organizacionais centralizadoras.
	 * 
	 * @return coleção de unidade organizacional.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<UnidadeOrganizacional> listarUnidadesCentralizadoras() throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * unidades organizacionais superiores.
	 * 
	 * @return coleção de unidades organizacionais
	 *         superiores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<UnidadeOrganizacional> listarUnidadesSuperiores() throws NegocioException;

	/**
	 * Método responsável por consultar a
	 * quantidade de unidades organizacionais a
	 * partir da empresa.
	 * 
	 * @param chaveEmpresa
	 *            chave primária da empresa.
	 * @return the integer
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Integer consultarQuantidadeUnidadesOrganizacionaisPelaEmpresa(Long chaveEmpresa) throws NegocioException;

	/**
	 * Método responsável por validar a remocao de
	 * uma unidade organizacional.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverUnidadeOrgazinacional(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por listar os meios de
	 * solicitação disponível no sistema.
	 * 
	 * @return coleção de meios de solicitação.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CanalAtendimento> listarCanalAtendimento() throws NegocioException;

	/**
	 * Método responsável por listar os tipos de
	 * unidades organizacionais.
	 * 
	 * @return coleção dos tipos das unidades
	 *         organizacionais.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<UnidadeTipo> listarUnidadeTipo() throws NegocioException;

	/**
	 * Lista as unidades organizacionais inferiores a uma determinada unidade organizacional
	 * @param idUnidadeSuperior id da unidade organizacional superior
	 * @return retorna uma lista de unidades organizacionais inferiores a unidade informada
	 */
	Collection<UnidadeOrganizacional> listarUnidadesOrganizacionaisInferiores(Long idUnidadeSuperior);

	/**
	 * Método responsável por obter o tipo da
	 * unidade organizacional.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria do tipo da
	 *            unidade.
	 * @return tipo da unidade organizacional.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	UnidadeTipo obterUnidadeTipo(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o meio de
	 * solicitação da unidade organizacional.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria do meio de
	 *            solicitação.
	 * @return meio de solicitação.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	CanalAtendimento obterCanalAtendimento(Long chavePrimaria) throws NegocioException;

	/**
	 * Listar nivel unidade tipo.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<String> listarNivelUnidadeTipo() throws NegocioException;

	/**
	 * Listar tipo unidade tipo.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<String> listarTipoUnidadeTipo() throws NegocioException;

	/**
	 * Consultar unidade organizacional ordenada.
	 * 
	 * @return the collection
	 */
	Collection<UnidadeOrganizacional> consultarUnidadeOrganizacionalOrdenada();

	/**
	 * Consultar Unidade Organizacional Por Chamado Assunto.
	 * @param chavePrimariaChamadoAssunto the ChavePrimaria Chamado Assunto
	 * @return the collection
	 */
	UnidadeOrganizacional consultarUnidadeOrganizacionalPorChamadoAssunto(Long chavePrimariaChamadoAssunto);

	/**
	 * Obter Unidades Organizacional Por Chaves Primarias.
	 * @param chaves the chaves
	 * @return the collection
	 */
	Collection<UnidadeOrganizacional> obterUnidadesOrganizacionalPorChavesPrimarias(Collection<Long> chaves);

}
