/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorEmpresa representa um controladorEmpresa no sistema.
 *
 * @since 20/07/2009
 * 
 */

package br.com.ggas.cadastro.empresa;

import java.util.Collection;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Controlador responsável pela classe Empresa 
 *
 */
public interface ControladorEmpresa extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_EMPRESA = "controladorEmpresa";

	String ERRO_NEGOCIO_EMPRESA_EXISTENTE = "ERRO_NEGOCIO_EMPRESA_EXISTENTE";

	String ERRO_NEGOCIO_VALOR = "ERRO_NEGOCIO_VALOR";

	String ERRO_NEGOCIO_ARQUIVO_NAO_ENCONTRADO = "ERRO_NEGOCIO_ARQUIVO_NAO_ENCONTRADO";

	String ERRO_NEGOCIO_ARQUIVO = "ERRO_NEGOCIO_ARQUIVO";

	String ERRO_NEGOCIO_EMPRESA_VINCULADA = "ERRO_NEGOCIO_EMPRESA_VINCULADA";

	String ERRO_NEGOCIO_EMPRESA_PRINCIPAL_JA_EXISTENTE = "ERRO_NEGOCIO_EMPRESA_PRINCIPAL_JA_EXISTENTE";


	/**
	 * Método responsável por consultar as
	 * empresas a partir do filtro informado.
	 * 
	 * @param filtro
	 *            O filtro para consulta.
	 * @return coleção de empresas cadastradas no
	 *         sistema.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Empresa> consultarEmpresas(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se foi
	 * selecionada alguma empresa para exclusão.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias das empresas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarEmpresasSelecionadas(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por verificar se foi
	 * selecionada apenas uma empresa para
	 * alteração.
	 * 
	 * @param chavesPrimarias
	 *            Chaves primárias das empresas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarSelecaoDeAtualizacao(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por listas as empresas
	 * construtoras.
	 * 
	 * @return coleção de empresas construtoras.
	 */
	Collection<Empresa> listarEmpresasConstrutoras();

	/**
	 * Método responsável por listas as empresas
	 * de medição.
	 * 
	 * @return coleção de empresas de medição.
	 */
	Collection<Empresa> listarEmpresasMedicao();

	/**
	 * Método responsável por obter a empresa
	 * principal.
	 * 
	 * @return Empresa principal
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Empresa obterEmpresaPrincipal() throws NegocioException;

	/**
	 * Obtém o logo da empresa e a chave primária da
	 * empresa principal.
	 * @return empresa
	 */
	Empresa obterLogoEmpresaPrincipal();

	/**
	 * Retorna <true> se a imagem for válida e <false> caso contrário.
	 * 
	 * @param conteudoFoto {@link byte}
	 * @param nomeArquivo {@link String}
	 * @param extensao {@link String}
	 * @param tamanho {@link int}
	 * @param erroTipoArquivo {@link String}
	 * @param erroTamanhoMaximoArquivo {@link String}
	 * @param isArquivoClienteAnexo {@link Boolean}
	 * @return boolean  {@link boolean}
	 * @throws NegocioException {@link NegocioException}
	 */
	boolean validarImagemDoArquivo(byte[] conteudoFoto, String nomeArquivo, String[] extensao, int tamanho, String erroTipoArquivo,
			String erroTamanhoMaximoArquivo, Boolean isArquivoClienteAnexo) throws NegocioException;

	/**
	 * Retorna <true> se a imagem for válida e <false> caso contrário.
	 * 
	 * @param foto {@link MultipartFile}
	 * @param extensao {@link String}
	 * @param tamanho {@link int}
	 * @param erroTipoArquivo {@link String}
	 * @param erroTamanhoMaximoArquivo {@link String}
	 * @param isArquivoClienteAnexo {@link String}
	 * @return boolean {@link boolean}
	 * @throws NegocioException {@link NegocioException}
	 */
	boolean validarImagemDoArquivo(MultipartFile foto, String[] extensao, int tamanho, String erroTipoArquivo,
			String erroTamanhoMaximoArquivo, Boolean isArquivoClienteAnexo) throws NegocioException;

	/**
	 * Método responsável por trazer uma coleção de Empresas com nome e cnpj relacionado a um cliente.
	 * 
	 * @param filtro {@link Map}
	 * @return Collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	public Collection<Empresa> consultarDadosEmpresas(Map<String, Object> filtro) throws NegocioException;
}
