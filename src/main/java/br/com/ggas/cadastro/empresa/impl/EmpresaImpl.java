/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe EmpresaImpl representa uma empresa no sistema.
 *
 * @since 20/07/2009
 * 
 */

package br.com.ggas.cadastro.empresa.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.empresa.ServicoPrestado;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe EmpresaImpl
 * 
 * @author Procenge
 */
public class EmpresaImpl extends EntidadeNegocioImpl implements Empresa {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7224845114403234517L;

	private byte[] logoEmpresa;

	private boolean principal;

	private Cliente cliente;

	private Collection<ServicoPrestado> servicosPrestados = new HashSet<>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.empresa.impl.Empresa
	 * #getLogoEmpresa()
	 */
	@Override
	public byte[] getLogoEmpresa() {
		byte[] logoEmpresaTmp = null;
		if(logoEmpresa != null){
			logoEmpresaTmp = logoEmpresa.clone();
		}
		return logoEmpresaTmp;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.empresa.impl.Empresa
	 * #setLogoEmpresa(byte[])
	 */
	@Override
	public void setLogoEmpresa(byte[] logoEmpresa) {
		if(logoEmpresa != null){
			this.logoEmpresa = logoEmpresa.clone();
		} else {
			this.logoEmpresa = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.empresa.Empresa#
	 * isPrincipal()
	 */
	@Override
	public boolean isPrincipal() {

		return principal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.empresa.Empresa#
	 * setPrincipal(boolean)
	 */
	@Override
	public void setPrincipal(boolean principal) {

		this.principal = principal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.empresa.Empresa#getCliente
	 * ()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.empresa.Empresa#setCliente
	 * (br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.empresa.Empresa#
	 * getServicosPrestados()
	 */
	@Override
	public Collection<ServicoPrestado> getServicosPrestados() {

		return servicosPrestados;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.empresa.Empresa#
	 * setServicosPrestados(java.util.Collection)
	 */
	@Override
	public void setServicosPrestados(Collection<ServicoPrestado> servicosPrestados) {

		this.servicosPrestados = servicosPrestados;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(cliente == null) {
			stringBuilder.append(CLIENTE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(servicosPrestados == null || servicosPrestados.isEmpty()) {
			stringBuilder.append(SERVICOS_PRESTADOS);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

	@Override
	public String[] getExtensoesArquivoLogoEmpresa() {
		
		return EXTENSOES_ARQUIVO_LOGO_EMPRESA;
	}
	
	
}
