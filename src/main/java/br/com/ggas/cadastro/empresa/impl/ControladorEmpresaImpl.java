/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorEmpresaImpl representa um controladorEmpresaImpl no sistema.
 *
 * @since 20/07/2009
 * 
 */

package br.com.ggas.cadastro.empresa.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.web.multipart.MultipartFile;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * 
 * 
 */
class ControladorEmpresaImpl extends ControladorNegocioImpl implements ControladorEmpresa {

	private static final int TAMANHO_MAXIMO_ARQUIVO = 1024;

	private static final Logger LOG = Logger.getLogger(ControladorEmpresaImpl.class);

	private static final String UNCHECKED = "unchecked";
	public static final String PRINCIPAL = "principal";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.empresa.impl.
	 * ControladorEmpresa#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Empresa.BEAN_ID_EMPRESA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Empresa.BEAN_ID_EMPRESA);
	}
	
	@Override
	public boolean validarImagemDoArquivo(MultipartFile foto, String[] extensao, int tamanho, String erroTipoArquivo,
			String erroTamanhoMaximoArquivo, Boolean isArquivoClienteAnexo) throws NegocioException {

		boolean retorno = false;

		try {
			retorno = validarImagemDoArquivo(foto.getBytes(), foto.getOriginalFilename(), extensao, tamanho, erroTipoArquivo,
							erroTamanhoMaximoArquivo, isArquivoClienteAnexo);
		} catch (FileNotFoundException e) {
			throw new NegocioException(ControladorEmpresa.ERRO_NEGOCIO_ARQUIVO_NAO_ENCONTRADO, e);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(ControladorEmpresa.ERRO_NEGOCIO_ARQUIVO);
		}

		return retorno;
	}
	
	@Override
	public boolean validarImagemDoArquivo(byte[] conteudoFoto, String nomeArquivo, String[] extensao, int tamanho, String erroTipoArquivo,
			String erroTamanhoMaximoArquivo, Boolean isArquivoClienteAnexo) throws NegocioException {
		boolean retorno = false;

		byte[] fotoByte = conteudoFoto;

		if (fotoByte.length != 0) {
			
			boolean isInvalido = this.verificarArquivoInvalido(nomeArquivo, extensao);
			
			if (isInvalido) {
				throw new NegocioException(erroTipoArquivo, true);
			}

			if (fotoByte.length < (tamanho * TAMANHO_MAXIMO_ARQUIVO)) {
				retorno = true;
			} else {

				if (isArquivoClienteAnexo != null && isArquivoClienteAnexo) {
					throw new NegocioException(erroTamanhoMaximoArquivo, tamanho);
				}

				throw new NegocioException(erroTamanhoMaximoArquivo, true);
			}

		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Empresa empresa = (Empresa) entidadeNegocio;
		verificarEntidadeExistente(empresa);

		if(empresa.isPrincipal()) {
			this.verificarExistenciaEmpresaPrincipal(empresa.getChavePrimaria());
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Empresa empresa = (Empresa) entidadeNegocio;

		if(empresa.isPrincipal()) {
			this.verificarExistenciaEmpresaPrincipal(empresa.getChavePrimaria());
		}

	}
	
	private boolean verificarArquivoInvalido(String nomeArquivo, String[] extensao){
		boolean isInvalido = true;
		if (extensao != null && extensao.length > 0) {
			for (String ext : extensao) {
				if (nomeArquivo.toUpperCase().endsWith(ext)) {
					isInvalido = false;
					break;
				}
			}
		}
		return isInvalido;
	}

	/**
	 * Verificar existencia empresa principal.
	 * 
	 * @param idEmpresa
	 *            the id empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarExistenciaEmpresaPrincipal(Long idEmpresa) throws NegocioException {

		Criteria criteria = this.createCriteria(Empresa.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.add(Restrictions.eq(PRINCIPAL, Boolean.TRUE));

		if(idEmpresa != null && idEmpresa > 0) {
			criteria.add(Restrictions.ne(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idEmpresa));
		}

		if(!criteria.list().isEmpty()) {

			throw new NegocioException(ControladorEmpresa.ERRO_NEGOCIO_EMPRESA_PRINCIPAL_JA_EXISTENTE, true);

		}

	}

	/**
	 * Verificar entidade existente.
	 * 
	 * @param empresa
	 *            the empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings({"squid:S1192", UNCHECKED})
	private void verificarEntidadeExistente(Empresa empresa) throws NegocioException {

		Collection<Empresa> empresas = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" cliente.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, empresa.getCliente().getChavePrimaria());

		empresas = query.list();

		if(empresas != null && !empresas.isEmpty()) {
			Empresa empresaExistente = empresas.iterator().next();

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(empresaExistente);

			if(empresaExistente.getChavePrimaria() != empresa.getChavePrimaria()) {
				throw new NegocioException(ControladorEmpresa.ERRO_NEGOCIO_EMPRESA_EXISTENTE, new String[] {});
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.empresa.ControladorEmpresa
	 * #consultarEmpresas(java.util.Map)
	 */
	@Override
	@SuppressWarnings(UNCHECKED)
	public Collection<Empresa> consultarEmpresas(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		filtroConsultaEmpresas(filtro, criteria);

		criteria.createAlias("cliente", "cliente");
		criteria.addOrder(Order.asc("cliente.nome"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.empresa.ControladorEmpresa
	 * #
	 * validarEmpresasSelecionadas(java.lang.Long[
	 * ])
	 */
	@Override
	public void validarEmpresasSelecionadas(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.empresa.ControladorEmpresa
	 * #
	 * validarSelecaoDeAtualizacao(java.lang.Long[
	 * ])
	 */
	@Override
	public void validarSelecaoDeAtualizacao(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length != 1) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preRemocao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		ControladorUnidadeOrganizacional controladorUnidadeOrganizacional = ServiceLocator.getInstancia()
						.getControladorUnidadeOrganizacional();
		Empresa empresa = (Empresa) entidadeNegocio;
		Integer quantidade = controladorUnidadeOrganizacional.consultarQuantidadeUnidadesOrganizacionaisPelaEmpresa(empresa
						.getChavePrimaria());
		if(quantidade > 0) {
			throw new NegocioException(ERRO_NEGOCIO_EMPRESA_VINCULADA, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.empresa.ControladorEmpresa
	 * #listarEmpresasConstrutoras()
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<Empresa> listarEmpresasConstrutoras() {

		String aliasEmpresa = "empresa";

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select ");
		hql.append(aliasEmpresa);
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" ");
		hql.append(aliasEmpresa);
		hql.append(" inner join ");
		hql.append(aliasEmpresa);
		hql.append(".servicosPrestados servico");
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema)
						ServiceLocator.getInstancia().getBeanPorID(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String servicoPrestadoConstrutora = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
						Constantes.C_SERVICO_PRESTADO_CONSTRUTORA);
		hql.append(" where servico.chavePrimaria = ");
		hql.append(servicoPrestadoConstrutora);
		hql.append(" order by ");
		hql.append(aliasEmpresa);
		hql.append(".cliente.nome ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.empresa.ControladorEmpresa#listarEmpresasMedicao()
	 */
	@Override
	public Collection<Empresa> listarEmpresasMedicao() {

		String aliasEmpresa = "empresa";

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select ");
		hql.append(aliasEmpresa);
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" ");
		hql.append(aliasEmpresa);
		hql.append(" inner join ");
		hql.append(aliasEmpresa);
		hql.append(".servicosPrestados servico");
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema)
						ServiceLocator.getInstancia().getBeanPorID(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String servicoPrestadoMedicao = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
						Constantes.C_SERVICO_PRESTADO_MEDICAO);		
		hql.append(" where servico.chavePrimaria = ");
		hql.append(servicoPrestadoMedicao);
		hql.append(" order by ");
		hql.append(aliasEmpresa);
		hql.append(".cliente.nome ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.empresa.ControladorEmpresa
	 * #obterEmpresaPrincipal()
	 */
	@Override
	public Empresa obterEmpresaPrincipal() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" empresa ");
		hql.append(" where empresa.principal = true");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (Empresa) query.uniqueResult();
	}
	
	
	
	/**
	 * Obtém o logo da empresa e a chave primária da
	 * empresa principal.
	 * @return empresa
	 */
	@Override
	public Empresa obterLogoEmpresaPrincipal() {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select ");
		hql.append(" empresa.logoEmpresa as logoEmpresa, ");
		hql.append(" empresa.chavePrimaria as chavePrimaria ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" empresa ");
		hql.append(" where empresa.principal = true");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidade()));
		
		return (Empresa) query.uniqueResult();
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.empresa.ControladorEmpresa
	 * #consultarEmpresas(java.util.Map)
	 */
	@Override
	@SuppressWarnings(UNCHECKED)
	public Collection<Empresa> consultarDadosEmpresas(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.createAlias("cliente", "cliente");
		criteria.setProjection(Projections.projectionList()
				.add(Projections.distinct(Projections.property("chavePrimaria")), "chavePrimaria")
				.add(Projections.property("cliente.chavePrimaria"), "cliente_chavePrimaria")
				.add(Projections.property("cliente.nome"), "cliente_nome")
				.add(Projections.property("cliente.cnpj"), "cliente_cnpj"));
		
		filtroConsultaEmpresas(filtro, criteria);

		criteria.addOrder(Order.asc("cliente.nome"));
		criteria.setResultTransformer(new GGASTransformer(getClasseEntidade(), super.getSessionFactory().getAllClassMetadata()));
		
		return criteria.list();
	}

	private void filtroConsultaEmpresas(Map<String, Object> filtro, Criteria criteria) {
		
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Boolean principal = (Boolean) filtro.get(PRINCIPAL);
			if(principal != null) {
				criteria.add(Restrictions.eq(PRINCIPAL, principal));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			filtroConsultaEmpresasDois(filtro, criteria);
		}
	}

	private void filtroConsultaEmpresasDois(Map<String, Object> filtro, Criteria criteria) {
		
		Long idCliente = (Long) filtro.get("idCliente");
		if(idCliente != null && idCliente > 0) {
			criteria.add(Restrictions.eq("cliente.chavePrimaria", idCliente));
		}

		String nomeCliente = (String) filtro.get("nomeCliente");
		if(!StringUtils.isEmpty(nomeCliente)) {
			criteria.add(Restrictions.ilike("cliente.nome", Util.formatarTextoConsulta(nomeCliente)));
		}

		String cnpjCliente = (String) filtro.get("cnpjCliente");
		if(!StringUtils.isEmpty(cnpjCliente)) {
			criteria.add(Restrictions.eq("cliente.cnpj", cnpjCliente));
		}

		Long idServicoPrestado = (Long) filtro.get("idServicoPrestado");
		if(idServicoPrestado != null && idServicoPrestado > 0) {
			criteria.createCriteria("servicosPrestados").add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idServicoPrestado));
		}
	}

}
