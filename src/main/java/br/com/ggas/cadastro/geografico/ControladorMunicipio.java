/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.geografico;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface ControladorMunicipio
 * 
 * @author Procenge
 */
public interface ControladorMunicipio extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_MUNICIPIO = "controladorMunicipio";

	String ERRO_NEGOCIO_REMOVER_UNIDADE_FEDERACAO = "ERRO_NEGOCIO_REMOVER_UNIDADE_FEDERACAO";

	String ERRO_NEGOCIO_REMOVER_MUNICIPIO = "ERRO_NEGOCIO_REMOVER_MUNICIPIO";

	/**
	 * Método responsável por consultar os municípios pelo filtro informado.
	 * 
	 * @param filtro contendo os parametros da pesquisa.
	 * @return coleção de Municipio.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Municipio> consultarMunicipios(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar os municípios cadastrados no sistema.
	 * 
	 * @return Coleção de municípios.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Municipio> listarMunicipios() throws NegocioException;

	/**
	 * Método responsável por listar as unidades da federação.
	 * 
	 * @return lista de unidades federativas
	 * @throws NegocioException the negocio exception
	 */
	Collection<UnidadeFederacao> listarUnidadeFederacao() throws NegocioException;

	/**
	 * Método responsável por obter a chave primaria da unidade federacao pela chave primaria do municipio.
	 * 
	 * @param idMunicipio chave primaria do municipio
	 * @return Long chave primaria da unidade federacao
	 * @throws NegocioException the negocio exception
	 */
	Long obterChavePrimariaUnidadeFederacaoPorChaveMunicipio(Long idMunicipio) throws NegocioException;

	/**
	 * Obter o municipio a partir do setor comercial.
	 * 
	 * @param idSetorComercial the id setor comercial
	 * @return the municipio
	 * @throws NegocioException the negocio exception
	 */
	Municipio obterMunicipioPorSetorComercial(Long idSetorComercial) throws NegocioException;

	/**
	 * Obter o municipio a partir do nome e uf.
	 * 
	 * @param nome the nome
	 * @param uf the uf
	 * @return the municipio
	 * @throws NegocioException the negocio exception
	 */
	Municipio obterMunicipioPorNomeUF(String nome, String uf) throws NegocioException;

	/**
	 * Criar unidade federacao.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarUnidadeFederacao();

}
