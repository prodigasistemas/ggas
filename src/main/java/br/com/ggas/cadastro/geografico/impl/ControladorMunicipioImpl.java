/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorMunicipioImpl representa um controlador do municipio no sistema.
 *
 * @since 30/07/2009
 * 
 */

package br.com.ggas.cadastro.geografico.impl;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 *
 */
class ControladorMunicipioImpl extends ControladorNegocioImpl implements ControladorMunicipio {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Municipio.BEAN_ID_MUNICIPIO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Municipio.BEAN_ID_MUNICIPIO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.ControladorMunicipio#criarUnidadeFederacao()
	 */
	@Override
	public EntidadeNegocio criarUnidadeFederacao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(UnidadeFederacao.BEAN_ID_UNIDADE_FEDERACAO);
	}

	public Class<?> getClasseEntidadeUnidadeFederacao() {

		return ServiceLocator.getInstancia().getClassPorID(UnidadeFederacao.BEAN_ID_UNIDADE_FEDERACAO);
	}

	public Class<?> getClasseEntidadeSetorComercial() {

		return ServiceLocator.getInstancia().getClassPorID(SetorComercial.BEAN_ID_SETOR_COMERCIAL);
	}

	public Class<?> getClasseEntidadeMunicipio() {

		return ServiceLocator.getInstancia().getClassPorID(Municipio.BEAN_ID_MUNICIPIO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.geografico.
	 * ControladorMunicipio
	 * #consultarMunicipios(java.util
	 * .Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Municipio> consultarMunicipios(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			String nome = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, nome, MatchMode.ANYWHERE));
			}
			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}
			
			criteria.createAlias("unidadeFederacao", "unidadeFederacao");
			Long idUnidadeFederacao = (Long) filtro.get("idUnidadeFederacao");
			if(idUnidadeFederacao != null && idUnidadeFederacao > 0) {
				criteria.add(Restrictions.eq("unidadeFederacao.chavePrimaria", idUnidadeFederacao));
			}
			
			String siglaUnidadeFederacao = (String) filtro.get("siglaUnidadeFederacao");
			if(StringUtils.isNotBlank(siglaUnidadeFederacao)) {
				criteria.add(Restrictions.eq("unidadeFederacao.sigla", siglaUnidadeFederacao));
			}
			
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.
	 * ControladorMunicipio#listarMunicipios()
	 */
	@Override
	@SuppressWarnings({"squid:S1192", "unchecked"})
	public Collection<Municipio> listarMunicipios() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (Collection<Municipio>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.
	 * ControladorMunicipio
	 * #listarUnidadeFederacao()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<UnidadeFederacao> listarUnidadeFederacao() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeUnidadeFederacao().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (Collection<UnidadeFederacao>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.ControladorMunicipio#obterChavePrimariaUnidadeFederacaoPorChaveMunicipio(java.lang.Long)
	 */
	@Override
	public Long obterChavePrimariaUnidadeFederacaoPorChaveMunicipio(Long idMunicipio) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select municipio.unidadeFederacao.chavePrimaria ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" municipio ");
		hql.append(" where ");
		hql.append(" municipio.chavePrimaria = :idMunicipio ");
		hql.append(" and municipio.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idMunicipio", idMunicipio);

		return (Long) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.geografico.
	 * ControladorMunicipio
	 * #obterMunicipioPorSetorComercial
	 * (java.lang.Long)
	 */
	@Override
	public Municipio obterMunicipioPorSetorComercial(Long idSetorComercial) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select setorComercial.municipio ");
		hql.append(" from ");
		hql.append(getClasseEntidadeSetorComercial().getSimpleName());
		hql.append(" setorComercial ");
		hql.append(" where setorComercial.chavePrimaria = :idSetorComercial ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idSetorComercial", idSetorComercial);

		return (Municipio) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.geografico.
	 * ControladorMunicipio
	 * #obterMunicipioPorSetorComercial
	 * (java.lang.Long)
	 */
	@Override
	public Municipio obterMunicipioPorNomeUF(String nome, String uf) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select municipio");
		hql.append(" from ");
		hql.append(getClasseEntidadeMunicipio().getSimpleName());
		hql.append(" municipio ");
		hql.append(" inner join fetch municipio.unidadeFederacao uf");
		hql.append(" where (lower(municipio.descricao) = :nome");
		hql.append(" or lower(translate(municipio.descricao,'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç','AAAAAAAAEEEEIIOOOOOOUUUUCC')) = :nome)");
		hql.append(" and uf.sigla = :uf");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("nome", nome.toLowerCase());
		query.setString("uf", uf);

		return (Municipio) query.uniqueResult();
	}

}
