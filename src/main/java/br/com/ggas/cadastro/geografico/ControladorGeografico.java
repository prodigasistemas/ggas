/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.geografico;

import java.util.Collection;
import java.util.List;

import br.com.ggas.cadastro.geografico.impl.Pais;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface ControladorGeografico
 * 
 * @author Procenge
 */
public interface ControladorGeografico extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_GEOGRAFICO = "controladorGeografico";

	/**
	 * Método responsável por listar as regiões cadastradas no sistema.
	 * 
	 * @return Coleção de regiões.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Regiao> listarRegioes() throws NegocioException;

	/**
	 * Método responsável por listar as microrregiões cadastradas no sistema.
	 * 
	 * @return Coleção de microrregiões.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Collection<Microrregiao> listarMicrorregioes() throws NegocioException;

	/**
	 * Método responsável por recuperar uma região pela chave primária.
	 * 
	 * @param chavePrimaria A chave primária.
	 * @return Uma região.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Regiao obterRegiao(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por recuperar uma microrregião pela chave primária.
	 * 
	 * @param chavePrimaria A chave primária.
	 * @return Uma microrregião.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	Microrregiao obterMicrorregiao(long chavePrimaria) throws NegocioException;

	/**
	 * Criar entidade regiao.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarEntidadeRegiao();

	/**
	 * Listar paises.
	 * 
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Pais> listarPaises() throws NegocioException;

	/**
	 * Obter microrregiões por região.
	 * 
	 * @param idRegiao the id regiao
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	List<Microrregiao> obterMicrorregioesPorRegiao(Long idRegiao) throws NegocioException;

	/**
	 * Obter microrregiões por unidade federação.
	 * 
	 * @param idUnidadeFederacao the id unidade federação
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	List<Microrregiao> obterMicrorregioesPorUnidadeFederacao(Long idUnidadeFederacao) throws NegocioException;

	/**
	 * Obter regiões por unidade federação.
	 * 
	 * @param idUnidadeFederacao the id unidade federacao
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	List<Regiao> obterRegioesPorUnidadeFederacao(Long idUnidadeFederacao) throws NegocioException;

	/**
	 * Criar entidade pais.
	 * 
	 * @return the pais
	 */
	Pais criarEntidadePais();

	/**
	 * Criar entidade microrregiao.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarEntidadeMicrorregiao();
}
