/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.geografico.impl;

import java.util.Collection;
import java.util.List;

import org.hibernate.Query;

import br.com.ggas.cadastro.geografico.ControladorGeografico;
import br.com.ggas.cadastro.geografico.Microrregiao;
import br.com.ggas.cadastro.geografico.Regiao;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 * 
 */
class ControladorGeograficoImpl extends ControladorNegocioImpl implements ControladorGeografico {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	/**
	 * @deprecated
	 */
	@Override
	@Deprecated
	public EntidadeNegocio criar() {

		return null;
	}

	/**
	 * @deprecated
	 */
	@Override
	@Deprecated
	public Class<?> getClasseEntidade() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.ControladorGeografico#criarEntidadePais()
	 */
	@Override
	public Pais criarEntidadePais() {

		return (Pais) ServiceLocator.getInstancia().getBeanPorID(Pais.BEAN_ID_PAIS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.ControladorGeografico#criarEntidadeRegiao()
	 */
	@Override
	public EntidadeNegocio criarEntidadeRegiao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Regiao.BEAN_ID_REGIAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.ControladorGeografico#criarEntidadeMicrorregiao()
	 */
	@Override
	public EntidadeNegocio criarEntidadeMicrorregiao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Microrregiao.BEAN_ID_MICRORREGIAO);
	}

	public Class<?> getClasseEntidadeRegiao() {

		return ServiceLocator.getInstancia().getClassPorID(Regiao.BEAN_ID_REGIAO);
	}

	public Class<?> getClasseEntidadeMicrorregiao() {

		return ServiceLocator.getInstancia().getClassPorID(Microrregiao.BEAN_ID_MICRORREGIAO);
	}

	public Class<?> getClasseEntidadePais() {

		return ServiceLocator.getInstancia().getClassPorID(Pais.BEAN_ID_PAIS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.
	 * ControladorGeografico#listarMicrorregioes()
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<Microrregiao> listarMicrorregioes() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeMicrorregiao().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (Collection<Microrregiao>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.
	 * ControladorGeografico#listarRegioes()
	 */
	@Override
	public Collection<Regiao> listarRegioes() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeRegiao().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (Collection<Regiao>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.
	 * ControladorGeografico
	 * #obterMicrorregiao(long)
	 */
	@Override
	public Microrregiao obterMicrorregiao(long chavePrimaria) throws NegocioException {

		return (Microrregiao) this.obter(chavePrimaria, getClasseEntidadeMicrorregiao());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.
	 * ControladorGeografico#obterRegiao(long)
	 */
	@Override
	public Regiao obterRegiao(long chavePrimaria) throws NegocioException {

		return (Regiao) this.obter(chavePrimaria, getClasseEntidadeRegiao());
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.ControladorGeografico#obterMicrorregioesPorRegiao(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public List<Microrregiao> obterMicrorregioesPorRegiao(Long idRegiao) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append("select microrregiao ");
		hql.append(" from ");
		hql.append(getClasseEntidadeMicrorregiao().getSimpleName());
		hql.append(" microrregiao ");
		hql.append(" inner join microrregiao.regiao regiao ");
		hql.append(" where ");
		hql.append(" regiao.chavePrimaria = ? and microrregiao.habilitado = true ");
		hql.append(" order by microrregiao.descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idRegiao);

		return query.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.ControladorGeografico#obterMicrorregioesPorUnidadeFederacao(java.lang.Long)
	 */
	@Override
	public List<Microrregiao> obterMicrorregioesPorUnidadeFederacao(Long idUnidadeFederacao) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append("select microrregiao ");
		hql.append(" from ");
		hql.append(getClasseEntidadeMicrorregiao().getSimpleName());
		hql.append(" microrregiao ");
		hql.append(" inner join microrregiao.regiao regiao ");
		hql.append(" inner join regiao.unidadeFederacao unidadeFederacao ");
		hql.append(" where ");
		hql.append(" unidadeFederacao.chavePrimaria = ? and microrregiao.habilitado = true ");
		hql.append(" order by microrregiao.descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idUnidadeFederacao);

		return query.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.ControladorGeografico#obterRegioesPorUnidadeFederacao(java.lang.Long)
	 */
	@Override
	public List<Regiao> obterRegioesPorUnidadeFederacao(Long idUnidadeFederacao) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append("select regiao ");
		hql.append(" from ");
		hql.append(getClasseEntidadeRegiao().getSimpleName());
		hql.append(" regiao ");
		hql.append(" inner join regiao.unidadeFederacao unidadeFederacao ");
		hql.append(" where ");
		hql.append(" unidadeFederacao.chavePrimaria = ? and regiao.habilitado = true ");
		hql.append(" order by regiao.descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idUnidadeFederacao);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.geografico.ControladorGeografico#listarPaises()
	 */
	@Override
	public Collection<Pais> listarPaises() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadePais().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by nome ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (Collection<Pais>) query.list();
	}

}
