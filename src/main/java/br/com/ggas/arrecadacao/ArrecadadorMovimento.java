/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pelo movimentação do arrecadador
 */
public interface ArrecadadorMovimento extends EntidadeNegocio {

	String BEAN_ID_ARRECADADOR_MOVIMENTO = "arrecadadorMovimento";

	/**
	 * @return the formaArrecadacao
	 */
	EntidadeConteudo getFormaArrecadacao();

	/**
	 * @param formaArrecadacao
	 *            the formaArrecadacao to set
	 */
	void setFormaArrecadacao(EntidadeConteudo formaArrecadacao);

	/**
	 * @return the itens
	 */
	Collection<ArrecadadorMovimentoItem> getItens();

	/**
	 * @param itens
	 *            the itens to set
	 */
	void setItens(Collection<ArrecadadorMovimentoItem> itens);

	/**
	 * @return the dataGeracao
	 */
	Date getDataGeracao();

	/**
	 * @param dataGeracao
	 *            the dataGeracao to set
	 */
	void setDataGeracao(Date dataGeracao);

	/**
	 * @return the numeroNSA
	 */
	Integer getNumeroNSA();

	/**
	 * @param numeroNSA
	 *            the numeroNSA to set
	 */
	void setNumeroNSA(Integer numeroNSA);

	/**
	 * @return the codigoConvenio
	 */
	String getCodigoConvenio();

	/**
	 * @param codigoConvenio
	 *            the codigoConvenio to set
	 */
	void setCodigoConvenio(String codigoConvenio);

	/**
	 * @return the nomeEmpresa
	 */
	String getNomeEmpresa();

	/**
	 * @param nomeEmpresa
	 *            the nomeEmpresa to set
	 */
	void setNomeEmpresa(String nomeEmpresa);

	/**
	 * @return the codigoEmpresa
	 */
	Integer getCodigoEmpresa();

	/**
	 * @param codigoEmpresa
	 *            the codigoEmpresa to set
	 */
	void setCodigoEmpresa(Integer codigoEmpresa);

	/**
	 * @return the codigoBanco
	 */
	Integer getCodigoBanco();

	/**
	 * @param codigoBanco
	 *            the codigoBanco to set
	 */
	void setCodigoBanco(Integer codigoBanco);

	/**
	 * @return the codigoAgencia
	 */
	String getCodigoAgencia();

	/**
	 * @param codigoAgencia
	 *            the codigoAgencia to set
	 */
	void setCodigoAgencia(String codigoAgencia);

	/**
	 * @return the codigoDigitoAgencia
	 */
	String getCodigoDigitoAgencia();

	/**
	 * @param codigoDigitoAgencia
	 *            the codigoDigitoAgencia to set
	 */
	void setCodigoDigitoAgencia(String codigoDigitoAgencia);

	/**
	 * @return the codigoContaBancaria
	 */
	String getCodigoContaBancaria();

	/**
	 * @param codigoContaBancaria
	 *            the codigoContaBancaria to set
	 */
	void setCodigoContaBancaria(String codigoContaBancaria);

	/**
	 * @return the codigoDigitoContaBancaria
	 */
	String getCodigoDigitoContaBancaria();

	/**
	 * @param codigoDigitoContaBancaria
	 *            the codigoDigitoContaBancaria to
	 *            set
	 */
	void setCodigoDigitoContaBancaria(String codigoDigitoContaBancaria);

	/**
	 * @return the numeroVersaoLayout
	 */
	String getNumeroVersaoLayout();

	/**
	 * @param numeroVersaoLayout
	 *            the numeroVersaoLayout to set
	 */
	void setNumeroVersaoLayout(String numeroVersaoLayout);

	/**
	 * @return the codigoRemessa
	 */
	Integer getCodigoRemessa();

	/**
	 * @param codigoRemessa
	 *            the codigoRemessa to set
	 */
	void setCodigoRemessa(Integer codigoRemessa);

	/**
	 * @return the quantidadeTotalRegistro
	 */
	Integer getQuantidadeTotalRegistro();

	/**
	 * @param quantidadeTotalRegistro
	 *            the quantidadeTotalRegistro to
	 *            set
	 */
	void setQuantidadeTotalRegistro(Integer quantidadeTotalRegistro);

	/**
	 * @return the valorTotal
	 */
	BigDecimal getValorTotal();

	/**
	 * @param valorTotal
	 *            the valorTotal to set
	 */
	void setValorTotal(BigDecimal valorTotal);
	
	/**
	 * @return the nomeArquivo
	 */
	String getNomeArquivo();

	/**
	 * @param nomeArquivo
	 *            the nomeArquivo to set
	 */
	void setNomeArquivo(String nomeArquivo);
}
