/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.devolucao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pelos métodos de devolução
 */
public interface Devolucao extends EntidadeNegocio {

	String BEAN_ID_DEVOLUCAO = "devolucao";

	String DEVOLUCAO_CAMPO_STRING = "DEVOLUCAO";

	String VALOR_DEVOLUCAO = "VALOR_DEVOLUCAO";

	String DATA_DEVOLUCAO = "DATA_DEVOLUCAO";

	String TIPO_DEVOLUCAO = "TIPO_DEVOLUCAO";

	String DOCUMENTO = "DOCUMENTO";

	/**
	 * @return the cliente
	 */
	Cliente getCliente();

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	void setCliente(Cliente cliente);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the tipoDevolucao
	 */
	EntidadeConteudo getTipoDevolucao();

	/**
	 * @param tipoDevolucao
	 *            the tipoDevolucao to set
	 */
	void setTipoDevolucao(EntidadeConteudo tipoDevolucao);

	/**
	 * @return the valorDevolucao
	 */
	BigDecimal getValorDevolucao();

	/**
	 * @param valorDevolucao
	 *            the valorDevolucao to set
	 */
	void setValorDevolucao(BigDecimal valorDevolucao);

	/**
	 * @return the dataDevolucao
	 */
	Date getDataDevolucao();

	/**
	 * @param dataDevolucao
	 *            the dataDevolucao to set
	 */
	void setDataDevolucao(Date dataDevolucao);

	/**
	 * @return the observacao
	 */
	String getObservacao();

	/**
	 * @param observacao
	 *            the observacao to set
	 */
	void setObservacao(String observacao);

	/**
	 * @return the listaNotaCredito
	 */
	Collection<Fatura> getListaNotaCredito();

	/**
	 * @param listaNotaCredito
	 *            the listaNotaCredito to set
	 */
	void setListaNotaCredito(Collection<Fatura> listaNotaCredito);

	/**
	 * @return the listaCreditoDebitoARealizar
	 */
	Collection<CreditoDebitoARealizar> getListaCreditoDebitoARealizar();

	/**
	 * @param listaCreditoDebitoARealizar
	 *            the listaCreditoDebitoARealizar
	 *            to set
	 */
	void setListaCreditoDebitoARealizar(Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar);

	/**
	 * @return the listaRecebimento
	 */
	Collection<Recebimento> getListaRecebimento();

	/**
	 * @param listaRecebimento
	 *            the listaRecebimento to set
	 */
	void setListaRecebimento(Collection<Recebimento> listaRecebimento);
}
