/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.devolucao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.arrecadacao.devolucao.ControladorDevolucao;
import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoDetalhamento;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

class ControladorDevolucaoImpl extends ControladorNegocioImpl implements ControladorDevolucao {

	private static final String TIPO_DEVOLUCAO = "tipoDevolucao";

	private static final String PONTO_CONSUMO = "pontoConsumo";

	private static final String CLIENTE = "cliente";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String ID_CLIENTE = "idCliente";

	private static final Logger LOG = Logger.getLogger(ControladorDevolucaoImpl.class);
	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";
	private static final String LISTA_NOTA_CREDITO = "listaNotaCredito";
	private static final String FROM = " FROM ";
	private static final String WHERE = " WHERE ";

	private ControladorConstanteSistema controladorConstanteSistema;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Devolucao.BEAN_ID_DEVOLUCAO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Devolucao.BEAN_ID_DEVOLUCAO);
	}

	public Class<?> getClasseEntidadeConteudo() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}

	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeCreditoDebitoARealizar() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoARealizar.BEAN_ID_CREDITO_DEBITO_A_REALIZAR);
	}

	public Class<?> getClasseEntidadeCreditoDebitoDetalhamento() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoDetalhamento.BEAN_ID_CREDITO_DEBITO_DETALHAMENTO);
	}

	public Class<?> getClasseEntidadeRecebimento() {

		return ServiceLocator.getInstancia().getClassPorID(Recebimento.BEAN_ID_RECEBIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.devolucao.
	 * ControladorDevolucao
	 * #consultarDevolucoes(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Devolucao> consultarDevolucoes(Map<String, Object> filtro) {

		Criteria criteria = getCriteria();

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Long idCliente = (Long) filtro.get(ID_CLIENTE);
			if((idCliente != null) && (idCliente > 0)) {
				criteria.createAlias(CLIENTE, CLIENTE);
				criteria.add(Restrictions.eq("cliente.chavePrimaria", idCliente));
			} else {
				criteria.setFetchMode(CLIENTE, FetchMode.JOIN);
			}

			Long idPontoConsumo = (Long) filtro.get(ID_PONTO_CONSUMO);
			if((idPontoConsumo != null) && (idPontoConsumo > 0)) {
				criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO);
				criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", idPontoConsumo));
			} else {
				criteria.setFetchMode(PONTO_CONSUMO, FetchMode.JOIN);
			}

			Long idTipoDevolucao = (Long) filtro.get("idTipoDevolucao");
			if((idTipoDevolucao != null) && (idTipoDevolucao > 0)) {
				criteria.createAlias(TIPO_DEVOLUCAO, TIPO_DEVOLUCAO);
				criteria.add(Restrictions.eq("tipoDevolucao.chavePrimaria", idTipoDevolucao));
			} else {
				criteria.setFetchMode(TIPO_DEVOLUCAO, FetchMode.JOIN);
			}

			Date dataDevolucaoInicial = (Date) filtro.get("dataDevolucaoInicial");
			Date dataDevolucaoFinal = (Date) filtro.get("dataDevolucaoFinal");
			if((dataDevolucaoInicial != null) && (dataDevolucaoFinal != null)) {
				criteria.add(Restrictions.between("dataDevolucao", dataDevolucaoInicial, dataDevolucaoFinal));
			}

			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}
		}

		criteria.createAlias(LISTA_NOTA_CREDITO, LISTA_NOTA_CREDITO, Criteria.LEFT_JOIN);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.devolucao.ControladorDevolucao#estornarDevolucao(java.util.Map, br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void estornarDevolucao(Map<String, Object> filtro, DadosAuditoria dadosAuditoria) throws NegocioException {

		Collection<Devolucao> listaDevolucao = this.consultarDevolucoes(filtro);

		if((listaDevolucao != null) && (!listaDevolucao.isEmpty())) {
			for (Devolucao devolucao : listaDevolucao) {
				devolucao.setDadosAuditoria(dadosAuditoria);
				this.remover(devolucao);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.devolucao.ControladorDevolucao#consultarFaturasManterDevolucao(java.lang.Long, java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Fatura> consultarFaturasManterDevolucao(Long idCliente, Long idPontoConsumo) throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeFatura().getSimpleName()).append(" fatura ");
		hql.append(" inner join fetch fatura.tipoDocumento tipoDocumento ");
		hql.append(" inner join fetch fatura.situacaoPagamento situacaoPagamento ");

		hql.append(WHERE);
		if((idCliente != null) && (idCliente > 0)) {
			hql.append(" fatura.cliente.chavePrimaria = :idCliente ");
		} else if((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			hql.append(" fatura.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		}

		hql.append(" and fatura.situacaoPagamento.chavePrimaria in (:arrayPagamento) ");
		hql.append(" and fatura.tipoDocumento.chavePrimaria = :paramTipoDocumento ");
		hql.append(" and fatura.devolucao is null ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if((idCliente != null) && (idCliente > 0)) {
			query.setLong(ID_CLIENTE, idCliente);
		} else if((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			query.setLong(ID_PONTO_CONSUMO, idPontoConsumo);
		}

		String situacaoPagamentoPaga = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

		Long[] arrayPagamento = {Long.valueOf(situacaoPagamentoPaga)};
		query.setParameterList("arrayPagamento", arrayPagamento);

		String tipoDocumento = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO);
		query.setParameter("paramTipoDocumento", Long.parseLong(tipoDocumento));

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.devolucao.
	 * ControladorDevolucao
	 * #consultarCreditosDebitosManterDevolucao
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CreditoDebitoARealizar> consultarCreditosDebitosManterDevolucao(Long idCliente, Long idPontoConsumo)
					throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeCreditoDebitoARealizar().getSimpleName()).append(" creditoDebito ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoNegociado.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoNegociado.rubrica rubrica ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoSituacao creditoDebitoSituacao ");
		hql.append(WHERE);

		hql.append(" creditoDebito.chavePrimaria in ( ");
		hql.append(" select CDDetalhamento.creditoDebitoARealizar.chavePrimaria ");
		hql.append(FROM);
		hql.append(getClasseEntidadeCreditoDebitoDetalhamento().getSimpleName()).append(" CDDetalhamento ");
		hql.append(WHERE);
		if((idCliente != null) && (idCliente > 0)) {
			hql.append(" CDDetalhamento.creditoDebitoARealizar.creditoDebitoNegociado.cliente.chavePrimaria = :idCliente ");
		} else if((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			hql.append(" CDDetalhamento.creditoDebitoARealizar.creditoDebitoNegociado.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		}
		hql.append(" and CDDetalhamento.lancamentoItemContabil.tipoCreditoDebito.chavePrimaria = :paramTipoCreditoDebito ");
		hql.append(" and CDDetalhamento.creditoDebitoARealizar.anoMesCobranca is null ");
		hql.append(" and CDDetalhamento.creditoDebitoARealizar.devolucao is null ");
		hql.append(" ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if((idCliente != null) && (idCliente > 0)) {
			query.setLong(ID_CLIENTE, idCliente);
		} else if((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			query.setLong(ID_PONTO_CONSUMO, idPontoConsumo);
		}

		String tipoCredito = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_DOCUMENTO_CREDITO_A_REALIZAR);
		query.setParameter("paramTipoCreditoDebito", Long.parseLong(tipoCredito));

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.devolucao.ControladorDevolucao#listarFaturasPorChaveDevolucao(long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Fatura> listarFaturasPorChaveDevolucao(long idDevolucao) throws GGASException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeFatura().getSimpleName()).append(" fatura ");
		hql.append(" inner join fetch fatura.tipoDocumento tipoDocumento ");
		hql.append(" inner join fetch fatura.situacaoPagamento situacaoPagamento ");

		hql.append(WHERE);
		hql.append(" fatura.devolucao.chavePrimaria = :idDevolucao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idDevolucao", idDevolucao);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.devolucao.ControladorDevolucao#listarCreditosPorChaveDevolucao(long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CreditoDebitoARealizar> listarCreditosPorChaveDevolucao(long idDevolucao) throws GGASException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeCreditoDebitoARealizar().getSimpleName()).append(" creditoDebito ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoNegociado.rubrica rubrica ");
		hql.append(" inner join fetch creditoDebito.creditoDebitoSituacao creditoDebitoSituacao ");

		hql.append(WHERE);
		hql.append(" creditoDebito.devolucao.chavePrimaria = :idDevolucao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idDevolucao", idDevolucao);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.devolucao.
	 * ControladorDevolucao
	 * #validarDataDevolucaoInicialFinal
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public void validarDataDevolucaoInicialFinal(String dataInicial, String dataFinal) throws GGASException {

		if((!StringUtils.isEmpty(dataInicial)) && (StringUtils.isEmpty(dataFinal))) {

			throw new NegocioException(ERRO_DATA_FINAL_OBRIGATORIA, true);
		} else if((StringUtils.isEmpty(dataInicial)) && (!StringUtils.isEmpty(dataFinal))) {

			throw new NegocioException(ERRO_DATA_INICIAL_OBRIGATORIA, true);
		} else {
			SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");

			try {
				if(formatoData.parse(dataFinal).before(formatoData.parse(dataInicial))) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_AUTORIZACAO_DATA_FINAL_MENOR, true);
				}
			} catch(ParseException e) {
				LOG.error(e.getMessage());
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.devolucao.
	 * ControladorDevolucao
	 * #validarRemoverDevolucao(java.lang.Long[])
	 */
	@Override
	public void validarRemoverDevolucao(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.devolucao.ControladorDevolucao#validarValorDevolucao(java.lang.String)
	 */
	@Override
	public void validarValorDevolucao(String valorDevolucao) throws NegocioException {

		if("0.00".equals(valorDevolucao)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_VALOR_ZERADO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#posInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void posInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		classificarDevolucao((Devolucao) entidadeNegocio, false);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preRemocao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		classificarDevolucao((Devolucao) entidadeNegocio, true);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.devolucao.
	 * ControladorDevolucao
	 * #classificarDevolucao(br
	 * .com.ggas.arrecadacao.devolucao.Devolucao,
	 * boolean)
	 */
	@Override
	public void classificarDevolucao(Devolucao devolucao, boolean estorno) throws NegocioException {

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();

		Collection<Fatura> listaNotaCredito = devolucao.getListaNotaCredito();
		Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar = devolucao.getListaCreditoDebitoARealizar();
		Collection<Recebimento> listaRecebimento = new HashSet<>();

		String valorParametro = null;
		CreditoDebitoSituacao creditoDebitoSituacao = null;

		if(estorno) {
			// (Devolvido) Não válida
			valorParametro = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_DEVOLVIDO);
		} else {
			// (Normal)
			valorParametro = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_NORMAL);
		}

		creditoDebitoSituacao = controladorCobranca.obterCreditoDebitoSituacao(Long.valueOf(valorParametro));

		if(listaNotaCredito != null && !listaNotaCredito.isEmpty()) {
			for (Fatura fatura : listaNotaCredito) {
				fatura.setDevolucao(null);
				fatura.setCreditoDebitoSituacao(creditoDebitoSituacao);
				fatura.setDadosAuditoria(devolucao.getDadosAuditoria());
				try {
					controladorCobranca.atualizarFatura(fatura);
				} catch(ConcorrenciaException e) {
					throw new NegocioException(e);
				}
			}
		}

		if(listaCreditoDebitoARealizar != null && !listaCreditoDebitoARealizar.isEmpty()) {
			for (CreditoDebitoARealizar creditoDebitoARealizar : listaCreditoDebitoARealizar) {
				creditoDebitoARealizar.setDevolucao(null);
				creditoDebitoARealizar.setCreditoDebitoSituacao(creditoDebitoSituacao);
				creditoDebitoARealizar.setDadosAuditoria(devolucao.getDadosAuditoria());
				try {
					controladorCobranca.atualizarCreditoDebitoARealizar(creditoDebitoARealizar);
				} catch(ConcorrenciaException e) {
					throw new NegocioException(e);
				}
			}
		}

		if(CollectionUtils.isNotEmpty(listaRecebimento)) {
			for (Recebimento recebimento : listaRecebimento) {
				recebimento.setDevolucao(null);
				recebimento.setDadosAuditoria(devolucao.getDadosAuditoria());
				recebimento.setBaixado(!estorno);
				try {
					this.atualizar(recebimento);
				} catch(ConcorrenciaException e) {
					throw new NegocioException(e);
				}
			}
		}

	}
}
