/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.devolucao;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface responsável por ações relacionadas a devoluções
 *
 */
public interface ControladorDevolucao extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_DEVOLUCAO = "controladorDevolucao";

	String ERRO_DATA_FINAL_OBRIGATORIA = "ERRO_DATA_FINAL_OBRIGATORIA";

	String ERRO_DATA_INICIAL_OBRIGATORIA = "ERRO_DATA_INICIAL_OBRIGATORIA";

	String ERRO_DATA_INICIAL_MAIOR_DATA_FINAL = "ERRO_DATA_INICIAL_MAIOR_DATA_FINAL";

	/**
	 * Método responsável por consultar as
	 * devoluções.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return lista de devoluções
	 */
	Collection<Devolucao> consultarDevolucoes(Map<String, Object> filtro);

	/**
	 * Método que valida se a data inicial é menor
	 * que a data final.
	 * 
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void validarDataDevolucaoInicialFinal(String dataInicial, String dataFinal) throws GGASException;

	/**
	 * Método que valida a seleção das devoluções
	 * para a exclusão;.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void validarRemoverDevolucao(Long[] chavesPrimarias) throws GGASException;

	/**
	 * Método que retorna uma lista de faturas
	 * para a inclusão de devoluções.
	 * 
	 * @param idCliente
	 *            the id cliente
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return lista de faturas
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Fatura> consultarFaturasManterDevolucao(Long idCliente, Long idPontoConsumo) throws NegocioException;

	/**
	 * Retorna a uma lista de crédito débtio a
	 * realizar.
	 * 
	 * @param idCliente
	 *            the id cliente
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return lista de credito débito a realizar
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CreditoDebitoARealizar> consultarCreditosDebitosManterDevolucao(Long idCliente, Long idPontoConsumo) throws NegocioException;

	/**
	 * Listar faturas por chave devolucao.
	 * 
	 * @param idDevolucao
	 *            the id devolucao
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<Fatura> listarFaturasPorChaveDevolucao(long idDevolucao) throws GGASException;

	/**
	 * Listar creditos por chave devolucao.
	 * 
	 * @param idDevolucao
	 *            the id devolucao
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<CreditoDebitoARealizar> listarCreditosPorChaveDevolucao(long idDevolucao) throws GGASException;

	/**
	 * Validar valor devolucao.
	 * 
	 * @param valorDevolucao
	 *            the valor devolucao
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void validarValorDevolucao(String valorDevolucao) throws GGASException;

	/**
	 * Estornar devolucao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void estornarDevolucao(Map<String, Object> filtro, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Método responsável por classificar uma
	 * devolução.
	 * 
	 * @param devolucao
	 *            A devolução
	 * @param estorno
	 *            Se é um estorno
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void classificarDevolucao(Devolucao devolucao, boolean estorno) throws NegocioException;

}
