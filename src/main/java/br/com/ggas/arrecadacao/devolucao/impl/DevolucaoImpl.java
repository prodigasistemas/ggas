/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.devolucao.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pela representação de uma devolução
 */
public class DevolucaoImpl extends EntidadeNegocioImpl implements Devolucao {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 6010565133630258211L;

	private Cliente cliente;

	private PontoConsumo pontoConsumo;

	private EntidadeConteudo tipoDevolucao;

	private BigDecimal valorDevolucao;

	private Date dataDevolucao;

	private String observacao;

	private Collection<Fatura> listaNotaCredito = new HashSet<>();

	private Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar = new HashSet<>();

	private Collection<Recebimento> listaRecebimento = new HashSet<>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao#getCliente()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao
	 * #setCliente(br.com.ggas.cadastro.cliente
	 * .Cliente)
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao#getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao
	 * #setPontoConsumo(br.com.ggas.cadastro
	 * .imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao#getTipoDevolucao()
	 */
	@Override
	public EntidadeConteudo getTipoDevolucao() {

		return tipoDevolucao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao
	 * #setTipoDevolucao(br.com.ggas.geral
	 * .EntidadeConteudo)
	 */
	@Override
	public void setTipoDevolucao(EntidadeConteudo tipoDevolucao) {

		this.tipoDevolucao = tipoDevolucao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao#getValorDevolucao()
	 */
	@Override
	public BigDecimal getValorDevolucao() {

		return valorDevolucao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao
	 * #setValorDevolucao(java.math.BigDecimal)
	 */
	@Override
	public void setValorDevolucao(BigDecimal valorDevolucao) {

		this.valorDevolucao = valorDevolucao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao#getDataDevolucao()
	 */
	@Override
	public Date getDataDevolucao() {
		Date dataDevolucaoTmp = null;
		if(dataDevolucao != null){
			dataDevolucaoTmp = (Date) dataDevolucao.clone();
		}
		return dataDevolucaoTmp;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao#setDataDevolucao(java.util.Date)
	 */
	@Override
	public void setDataDevolucao(Date dataDevolucao) {
		if(dataDevolucao != null){
			this.dataDevolucao = (Date) dataDevolucao.clone();
		} else {
			this.dataDevolucao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao#getObservacao()
	 */
	@Override
	public String getObservacao() {

		return observacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.impl.
	 * Devolucao#setObservacao(java.lang.String)
	 */
	@Override
	public void setObservacao(String observacao) {

		this.observacao = observacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.Devolucao
	 * #getListaNotaCredito()
	 */
	@Override
	public Collection<Fatura> getListaNotaCredito() {

		return listaNotaCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.Devolucao
	 * #setListaNotaCredito(java.util.Collection)
	 */
	@Override
	public void setListaNotaCredito(Collection<Fatura> listaNotaCredito) {

		this.listaNotaCredito = listaNotaCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.Devolucao
	 * #getListaCreditoDebitoARealizar()
	 */
	@Override
	public Collection<CreditoDebitoARealizar> getListaCreditoDebitoARealizar() {

		return listaCreditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.Devolucao
	 * #setListaCreditoDebitoARealizar(java.util.
	 * Collection)
	 */
	@Override
	public void setListaCreditoDebitoARealizar(Collection<CreditoDebitoARealizar> listaCreditoDebitoARealizar) {

		this.listaCreditoDebitoARealizar = listaCreditoDebitoARealizar;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.Devolucao
	 * #getListaRecebimento()
	 */
	@Override
	public Collection<Recebimento> getListaRecebimento() {

		return listaRecebimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.devolucao.Devolucao
	 * #setListaRecebimento(java.util.Collection)
	 */
	@Override
	public void setListaRecebimento(Collection<Recebimento> listaRecebimento) {

		this.listaRecebimento = listaRecebimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(valorDevolucao == null) {
			stringBuilder.append(VALOR_DEVOLUCAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataDevolucao == null) {
			stringBuilder.append(DATA_DEVOLUCAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tipoDevolucao == null) {
			stringBuilder.append(TIPO_DEVOLUCAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if((listaNotaCredito.isEmpty()) && (listaCreditoDebitoARealizar.isEmpty()) && (listaRecebimento.isEmpty())) {
			stringBuilder.append(DOCUMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

}
