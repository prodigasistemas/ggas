/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.batch;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.ArrecadadorMovimento;
import br.com.ggas.arrecadacao.ArrecadadorMovimentoItem;
import br.com.ggas.arrecadacao.ClienteDebitoAutomatico;
import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.DebitoAutomatico;
import br.com.ggas.arrecadacao.DebitoNaoProcessado;
import br.com.ggas.arrecadacao.avisobancario.AvisoBancario;
import br.com.ggas.arrecadacao.documento.ControladorDocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutCampo;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutErroOcorrencia;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRetornoOcorrencia;
import br.com.ggas.arrecadacao.impl.DadosArquivoRetorno;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.arrecadacao.recebimento.RecebimentoSituacao;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.Batch;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.ControladorDocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.impl.FaturaGeralImpl;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.InfraestruturaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.integracao.titulos.IntegracaoTitulo;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.ejb.criteria.predicate.IsEmptyPredicate;
import org.springframework.stereotype.Component;

import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.Set;

/**
 * Classe responsável por processar arquivo de retorno
 */
@Component
public class ProcessarArquivoRetornoBatch implements Batch {

	private static final String CHARSET_UTF_8 = "UTF-8";

	private static final int INDICADOR_REGISTRO_MEDIO = 3;

	private static final String LINHA = "Linha ";

	private static final String DATA_INVALIDA_SEIS_DIGITOS = "000000";

	private static final String DATA_INVALIDA_OITO_DIGITOS = "00000000";

	private static final String CODIGO_DOCUMENTO_DE_COBRANCA_NAO_INFORMADO = "O código do documento de cobrança não foi informado.";

	private static final String VALOR_RECEBIMENTO_NAO_INFORMADO = "O valor do recebimento não foi informado.";

	private static final String STRING_VALOR_RECEBIDO = " Valor Recebido:";

	private static final int POSICAO_0 = 0;

	private static final int POSICAO_2 = 2;

	private static final int POSICAO_4 = 4;

	private static final int POSICAO_6 = 6;

	private static final int POSICAO_26 = 26;

	private static final int POSICAO_12 = 12;

	private static final int POSICAO_156 = 156;

	private static final int POSICAO_150 = 150;

	private static final int POSICAO_79 = 79;

	private static final int POSICAO_77 = 77;

	private static final int POSICAO_48 = 48;

	private static final int POSICAO_33 = 33;

	private static final int POSICAO_22 = 22;

	private static final int POSICAO_3 = 3;

	private static final int POSICAO_45 = 45;

	private static final int POSICAO_43 = 43;

	private static final int POSICAO_44 = 44;

	private static final int POSICAO_5 = 5;

	private static final int POSICAO_15 = 15;

	private static final int POSICAO_1 = 1;

	private static final int POSICAO_8 = 8;

	private static final int POSICAO_30 = 30;

	private static final int POSICAO_37 = 37;

	private static final String PROCESSAR_CADASTRO_DEBITO_AUTOMATICO = "processarCadastroDebitoAutomatico";

	private static final String PROCESSAR_RECEBIMENTO_VALORES = "processarRecebimentoValores";

	private static final String PROCESSAR_RECEBIMENTO = "processarRecebimento";

	private static final String PROCESSAR_CABECALHO = "processarCabecalho";

	private static final String PARAMETRO_REFERENCIA_CONTABIL = "REFERENCIA_CONTABIL";

	private static final String CODIGO_MOVIMENTO_INCLUSAO = "2";

	private static final String CODIGO_MOVIMENTO_EXCLUSAO = "1";

	private static final String CODIGO_RETORNO = "2";

	private static final String PROCESSO = "processo";
	public static final int PRECISION = 4;

	private final ControladorDocumentoLayout controladorDocumentoLayout;

	private final ControladorArrecadacao controladorArrecadacao;

	private final ControladorRecebimento controladorRecebimentos;

	private final ControladorParametroSistema controladorParametroSistema;

	private final ControladorConstanteSistema controladorConstanteSistema;

	private final ControladorDocumentoCobranca controladorDocumentoCobranca;

	private final ControladorEmpresa controladorEmpresa;

	private final ControladorCliente controladorCliente;

	private final ControladorPontoConsumo controladorPontoConsumo;

	private final ControladorEntidadeConteudo controladorEntidadeConteudo;

	private static final Logger LOG = Logger.getLogger(ProcessarArquivoRetornoBatch.class);

	private static final int REGISTRO_MEDIO = 240;

	private static final int REGISTRO_GRANDE = 400;

	private static final int QTD_CASAS_DECIMAIS = 2;

	private static final int REGISTRO_PEQUENO = 150;

	/**
	 * Instantiates a new processar arquivo retorno batch.
	 */
	public ProcessarArquivoRetornoBatch() {

		controladorDocumentoLayout = (ControladorDocumentoLayout) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorDocumentoLayout.BEAN_ID_CONTROLADOR_DOCUMENTO_LAYOUT);

		controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);

		controladorRecebimentos = (ControladorRecebimento) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);

		controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		controladorDocumentoCobranca = (ControladorDocumentoCobranca) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorDocumentoCobranca.BEAN_ID_CONTROLADOR_DOCUMENTO_COBRANCA);

		controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		controladorCliente = (ControladorCliente) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

		controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
	}

	/**
	 * Processa os parametros, retornando o log do processamento
	 * 
	 * @param parametros Os parametros do processo.
	 * @return retorna o resultado do processamento
	 * @throws GGASException exceção lançada caso ocorra falha na operação
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();

		ControladorProcesso controladorProcesso = ServiceLocator.getInstancia().getControladorProcesso();

		String arquivoRetorno = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(ControladorParametroSistema.DIRETORIO_ARQUIVOS_UPLOAD);

		if (StringUtils.isEmpty(arquivoRetorno)) {
			throw new GGASException(Constantes.ERRO_NEGOCIO_ARQUIVO_NAO_ENCONTRADO, true);
		}
		Scanner scanner = null;
		String linha = null;
		int tamanhoRegistro = 0;
		String identificadorRegistro = null;
		String codigoBanco = null;
		int numeroLinha = 0;

		String metodoProcessamento = null;
		Map<EntidadeConteudo, DocumentoLayoutCampo> metodoArgumentos = null;
		boolean moverArquivoComErro = false;

		ArrecadadorMovimento arrecadadorMovimento = null;
		Collection<DebitoNaoProcessado> colecaoDebitoNaoProcessado = new ArrayList<>();
		DebitoAutomatico debitoAutomatico = null;
		Arrecadador arrecadador = null;
		Collection<RelatorioProcessamentoArquivoRetornoVO> dadosRelatorio = new ArrayList<>();
		Collection<Recebimento> recebimentos = new HashSet<>();
		DocumentoLayout documentoLayout = null;

		DadosArquivoRetorno dadosArquivoRetorno = controladorArrecadacao.criarDadosArquivoRetorno();
		Map<Date, AvisoBancario> mapAvisosBancarios = new HashMap<>();

		String codigoTipoConvenio = null;
		EntidadeConteudo tipoConvenio = null;

		ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		DadosAuditoria dadosAuditoria = Util.getDadosAuditoria(processo.getUsuario(), processo.getOperacao(),
				(String) controladorParametrosSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_IP));

		logProcessamento.append("\r\n Processando no diretório ");
		logProcessamento.append(arquivoRetorno);
		logProcessamento.append(" \r\n");

		File arquivo = Util.getFile(arquivoRetorno);

		File arquivoDestino = null;
		Exception exception = null;
		File[] files = arquivo.listFiles();
		Boolean semNenhumArquivo = Boolean.TRUE;
		String nomeArquivo;
		
		try {

			if (files == null || files.length == 0 || arquivo == null
					|| !arquivo.isFile() && !arquivo.exists() && arquivo.isDirectory()) {
				logProcessamento.append("\r\n");
				logProcessamento.append(" Nenhum arquivo encontrado para processar.");
				semNenhumArquivo = Boolean.FALSE;
			} else {
				for (File arqs : files) {
					nomeArquivo = arqs.getName();

					logProcessamento.append("\r\n Processando o arquivo: ");
					logProcessamento.append(nomeArquivo);
					logProcessamento.append(" \r\n");

					scanner = new Scanner(arqs, CHARSET_UTF_8);
					Collection<DocumentoLayoutRegistro> registros = null;
					do {
						linha = scanner.nextLine();

						if (!StringUtils.isEmpty(linha)) {

							tamanhoRegistro = linha.length();

							identificadorRegistro = verificaTamanhoRegistroMedio(linha, tamanhoRegistro);

							if (numeroLinha == 0) {
								logProcessamento.append(" \r\n Leiaute no padrão CNAB");
								logProcessamento.append(tamanhoRegistro);

								switch (tamanhoRegistro) {
								case REGISTRO_PEQUENO:
									codigoBanco = Util.obterValorPosicao(linha, new int[] { POSICAO_43, POSICAO_45 });
									codigoTipoConvenio = Util.obterValorPosicao(linha,
											new int[] { POSICAO_3, POSICAO_22 });
									break;
								case REGISTRO_MEDIO:
									codigoBanco = Util.obterValorPosicao(linha, new int[] { POSICAO_1, POSICAO_3 });
									codigoTipoConvenio = Util.obterValorPosicao(linha,
											new int[] { POSICAO_33, POSICAO_48 });
									break;
								case REGISTRO_GRANDE:
									codigoBanco = Util.obterValorPosicao(linha, new int[] { POSICAO_77, POSICAO_79 });
									switch (codigoBanco) {
									case "001":
										codigoTipoConvenio = Util.obterValorPosicao(linha,
												new int[] { POSICAO_150, POSICAO_156 });
										break;
									case "707":
										codigoTipoConvenio = Util.obterValorPosicao(linha,
												new int[] { POSICAO_30, POSICAO_37 });
										break;
									default:
										codigoTipoConvenio = Util.obterValorPosicao(linha,
												new int[] { POSICAO_12, POSICAO_26 });
									}
									break;
								default:
									moverArquivoComErro = true;
									throw new GGASException("Arquivo com layout inválido: " + arquivoRetorno);
								}
								if (arrecadador == null) {
									arrecadador = controladorArrecadacao
											.obterArrecadadorPorBanco(Long.valueOf(codigoBanco));
								}
								// Busca o tipoArrecacao de acordo com o codigo obtido do arquivo
								tipoConvenio = obterTipoArrecacaoEntidadeConteudo(codigoTipoConvenio);

								if (tipoConvenio == null) {
									throw new GGASException(
											"O tipo de convênio não foi encontrado: " + codigoTipoConvenio);
								}
								dadosArquivoRetorno.setCodigoBanco(codigoBanco);
								dadosArquivoRetorno.setArrecadador(arrecadador);

								DocumentoLayout filtro = controladorDocumentoLayout.criarLeiauteFiltro();
								filtro.getRegistros().iterator().next().setIdentificadorRegistro(identificadorRegistro);
								filtro.getConvenios().iterator().next().getArrecadadorContrato().getArrecadador()
										.getBanco().setCodigoBanco(codigoBanco);
								filtro.setTamanhoRegistro(tamanhoRegistro);

								for (DocumentoLayout leiaute : controladorDocumentoLayout
										.obterDocumentosLayout(filtro)) {
									if (verificarIgualdadeComChavePrimariaOuCodigoDoTipoArrecadacao(leiaute,
											tipoConvenio)) {
										documentoLayout = leiaute;
										registros = leiaute.getRegistros();
										break;
									}
								}
							}

							DocumentoLayoutRegistro registro = this.obterDocumentoLayoutRegistro(registros,
									identificadorRegistro);

							if (registro != null && !StringUtils.isEmpty(registro.getMetodoProcessamento())) {

								metodoProcessamento = registro.getMetodoProcessamento();
								metodoArgumentos = construirMapaCampos(registro, null);

								if (metodoArgumentos == null || metodoArgumentos.isEmpty()) {
									throw new GGASException(
											"Os argumentos que definem as posições no layout não estão cadastrado no banco.");
								}

								if (tamanhoRegistro == REGISTRO_MEDIO
										&& PROCESSAR_RECEBIMENTO.equalsIgnoreCase(metodoProcessamento)) {
									for (DocumentoLayoutRegistro dlr : registros) {
										if (PROCESSAR_RECEBIMENTO_VALORES
												.equalsIgnoreCase(dlr.getMetodoProcessamento())) {
											construirMapaCampos(dlr, metodoArgumentos);
											break;
										}
									}
								}

								if (PROCESSAR_CABECALHO.equalsIgnoreCase(metodoProcessamento)) {

									logProcessamento.append("\r\n\nLinha 1 (HEADER): ");
									logProcessamento.append(codigoBanco);
									logProcessamento.append("- ");
									logProcessamento.append(arrecadador.getBanco().getNome());

									arrecadadorMovimento = (ArrecadadorMovimento) controladorArrecadacao
											.criarArrecadadorMovimento();

									arrecadadorMovimento = this.processarCabecalho(dadosArquivoRetorno, linha,
											tipoConvenio, logProcessamento, arrecadadorMovimento, metodoArgumentos);
									
									arrecadadorMovimento.setNomeArquivo(nomeArquivo);		
									arrecadadorMovimento.setDadosAuditoria(dadosAuditoria);
									arrecadadorMovimento.setVersao(1);

									dadosArquivoRetorno.setArrecadadorMovimento(arrecadadorMovimento);
									controladorArrecadacao.inserirArrecadacaoMovimento(arrecadadorMovimento);

								} else if (PROCESSAR_CADASTRO_DEBITO_AUTOMATICO.equalsIgnoreCase(metodoProcessamento)) {

									debitoAutomatico = (DebitoAutomatico) controladorArrecadacao
											.criarDebitoAutomatico();

									colecaoDebitoNaoProcessado = this.processarCadastroDebitoAutomatico(processo,
											registro, colecaoDebitoNaoProcessado, numeroLinha + 1, linha,
											logProcessamento, arrecadador, debitoAutomatico, parametros,
											metodoArgumentos);

								} else if (PROCESSAR_RECEBIMENTO.equalsIgnoreCase(metodoProcessamento)) {

									String linhaValores = null;

									if (tamanhoRegistro == REGISTRO_MEDIO
											&& Integer.parseInt(identificadorRegistro) == INDICADOR_REGISTRO_MEDIO) {
										linhaValores = scanner.nextLine();
									}

									recebimentos.addAll(this.processarRecebimento(tipoConvenio, dadosAuditoria,
											mapAvisosBancarios, numeroLinha + 1, linha, registro, logProcessamento,
											dadosArquivoRetorno, dadosRelatorio, tamanhoRegistro, linhaValores,
											metodoArgumentos));
								}
							}
						}
						numeroLinha++;
					} while (scanner.hasNextLine());
					// Verificar se existem débitos que não foram processados, se existir, montar um
					// arquivo de remessa para banco.
					if (!colecaoDebitoNaoProcessado.isEmpty()) {
						controladorArrecadacao.montarArquivoDebitosNaoProcessados(colecaoDebitoNaoProcessado,
								arrecadador, arrecadadorMovimento, documentoLayout);
					}
					// Setar avisos bancários nos recebimentos e persistir.
					if (recebimentos != null && !recebimentos.isEmpty()) {

						ControladorApuracaoPenalidade controladorApuracaoPenalidade = (ControladorApuracaoPenalidade) ServiceLocator
								.getInstancia().getControladorNegocio(
										ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);

						ControladorRecebimento controladorRecebimento = (ControladorRecebimento) ServiceLocator
								.getInstancia()
								.getControladorNegocio(ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);

						ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia()
								.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

						ControladorIntegracao controladorIntegracao = (ControladorIntegracao) ServiceLocator
								.getInstancia()
								.getControladorNegocio(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

						ConstanteSistema recebimentoDocumentoInexistente = controladorConstanteSistema
								.obterConstantePorCodigo(Constantes.C_RECEBIMENTO_DOCUMENTO_INEXISTENTE);

						ConstanteSistema recebimentoDocumentoClassificado = controladorConstanteSistema
								.obterConstantePorCodigo(Constantes.C_RECEBIMENTO_CLASSIFICADO);

						// Persistir avisos bancarios.
						controladorArrecadacao.inserirAvisosBancarios(mapAvisosBancarios);

						BigDecimal somaPagosParcial = BigDecimal.ZERO;

						for (Recebimento recebimentoAdicionado : recebimentos) {

							if (recebimentoAdicionado != null) {

								AvisoBancario avisoBancario = mapAvisosBancarios
										.get(recebimentoAdicionado.getDataRecebimento());
								recebimentoAdicionado.setAvisoBancario(avisoBancario);

								// se o recebimento for C_RECEBIMENTO_CLASSIFICADO o sistema deve gerar a baixa
								// de titulo
								if (recebimentoAdicionado.getRecebimentoSituacao() != null
										&& recebimentoAdicionado.getRecebimentoSituacao().getChavePrimaria() == Long
												.parseLong(recebimentoDocumentoClassificado.getValor())) {

									recebimentoAdicionado.setDadosAuditoria(dadosAuditoria);

									gerarBaixaTitulo(logProcessamento, controladorIntegracao, somaPagosParcial,
											recebimentoAdicionado);

									// atualiza a QPNR do contrato
									if (recebimentoAdicionado.getPontoConsumo() != null) {
										Contrato contrato = controladorContrato
												.obterContratoAtivoPontoConsumo(recebimentoAdicionado.getPontoConsumo())
												.getContrato();
										if (contrato != null) {
											if (contrato.getChavePrimariaPai() != null) {
												contrato = (Contrato) controladorContrato
														.obter(contrato.getChavePrimariaPai());
											}
											controladorApuracaoPenalidade.atualizarQuantidadePagaNaoRecuperada(
													recebimentoAdicionado, dadosAuditoria, contrato);
										}
									}
								}

								// se o recebimento não for identificado
								// C_RECEBIMENTO_DOCUMENTO_INEXISTENTE, é necessário registrar um titulo de
								// baixa para o ERP
								if (recebimentoAdicionado.getRecebimentoSituacao() != null
										&& recebimentoAdicionado.getRecebimentoSituacao().getChavePrimaria() == Long
												.parseLong(recebimentoDocumentoInexistente.getValor())) {

									ConstanteSistema constanteTitulo = controladorConstanteSistema
											.obterConstantePorCodigo(Constantes.C_INTEGRACAO_TITULOS);

									ConstanteSistema constanteOperacaoInclusao = controladorConstanteSistema
											.obterConstantePorCodigo(Constantes.C_INTEGRACAO_OPERACAO_INCLUSAO);

									ConstanteSistema situacaoNaoProcessado = controladorConstanteSistema
											.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_NAO_PROCESSADO);

									ConstanteSistema constanteSistemaIntegracaoSistemaGGAS = controladorConstanteSistema
											.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SISTEMA_GGAS);

									ConstanteSistema baixaNaoIdentificada = controladorConstanteSistema
											.obterConstantePorCodigo(
													Constantes.C_INTEGRACAO_TITULO_BAIXA_NAO_IDENTIFICADA);

									Map<String, Object> filtro = new HashMap<>();
									filtro.put("idIntegracaoFuncao", Long.parseLong(constanteTitulo.getValor()));
									List<String> listaIntegracaoSistemaFuncao = controladorIntegracao
											.gerarListaSistemasIntegrantesPorFuncao(
													Long.parseLong(constanteTitulo.getValor()));

									for (String sistemaIntegrante : listaIntegracaoSistemaFuncao) {

										IntegracaoTitulo titulo = controladorIntegracao.criarIntegracaoTitulo();
										titulo.setRecebimento(
												Util.converteLongParaInteger(recebimentoAdicionado.getChavePrimaria()));

										ContaBancaria contaBancaria = controladorArrecadacao
												.obterContaBancaria(recebimentoAdicionado.getAvisoBancario()
														.getContaBancaria().getChavePrimaria());

										titulo.setBanco(contaBancaria.getAgencia().getBanco().getCodigoBanco());

										titulo.setNumeroConta(contaBancaria.getNumeroConta());
										String agencia = contaBancaria.getAgencia().getCodigo();
										titulo.setNumeroAgencia(agencia.substring(0, agencia.length() - 1));
										titulo.setNumeroAgenciaDigito(
												agencia.substring(agencia.length() - 1, agencia.length()));
										titulo.setNumeroContaDigito(contaBancaria.getNumeroDigito());

										titulo.setOperacao(constanteOperacaoInclusao.getValor());

										titulo.setSequencial(1);
										titulo.setData(recebimentoAdicionado.getDataRecebimento());
										titulo.setTipoBaixa(baixaNaoIdentificada.getValor());
										titulo.setValor(recebimentoAdicionado.getValorRecebimento());
										titulo.setValorAbatimento(recebimentoAdicionado.getValorAbatimento());
										titulo.setValorCredito(recebimentoAdicionado.getValorCredito());
										titulo.setValorDescontos(recebimentoAdicionado.getValorDescontos());
										titulo.setValorJurosMulta(recebimentoAdicionado.getValorJurosMulta());
										titulo.setValorPrincipal(recebimentoAdicionado.getValorPrincipal());

										titulo.setSistemaOrigem(constanteSistemaIntegracaoSistemaGGAS.getValor());
										titulo.setSistemaDestino(sistemaIntegrante);
										titulo.setSituacao(situacaoNaoProcessado.getValor());
										titulo.setDadosAuditoria(dadosAuditoria);
										titulo.setUsuario(
												String.valueOf(dadosAuditoria.getUsuario().getChavePrimaria()));

										controladorIntegracao.inserir(titulo);
									}
								}
								// fim do bloco de inserção de titulo de baixa
								controladorRecebimento.inserir(recebimentoAdicionado);
							}
						}
						if (somaPagosParcial.compareTo(BigDecimal.ZERO) > 0) {
							logProcessamento.append(somaPagosParcial.toString());
							logProcessamento.append(" título(s) pago(s) parcialmente");
						}
					}

					if (dadosRelatorio != null && !dadosRelatorio.isEmpty()) {

						// Gerar o relatorio de processamento e enviar por email
						Map<String, Object> parametrosJasper = new HashMap<>();
						Empresa cdlEmpresa = controladorEmpresa.obterEmpresaPrincipal();

						if (cdlEmpresa.getLogoEmpresa() != null) {
							parametrosJasper.put("imagem",
									Constantes.URL_LOGOMARCA_EMPRESA + cdlEmpresa.getChavePrimaria());
						}
						parametrosJasper.put("codigoBanco", codigoBanco);
						parametrosJasper.put("nomeBanco", arrecadador.getBanco().getNome());
						parametrosJasper.put("descricaoConvenio", tipoConvenio.getDescricao());
						parametrosJasper.put("nomeArquivo", arqs.getName());
						BigDecimal valorTotalNaoBaixado = BigDecimal.valueOf(0.0);
						BigDecimal valorTotalBaixado = BigDecimal.valueOf(0.0);
						BigDecimal valorTotalTarifaNaoBaixado = BigDecimal.valueOf(0.0);
						BigDecimal valorTotalTarifaBaixado = BigDecimal.valueOf(0.0);
						Collection<RelatorioProcessamentoArquivoRetornoVO> listaNaoBaixados = new ArrayList<>();

						for (RelatorioProcessamentoArquivoRetornoVO dadoRelatorio : dadosRelatorio) {

							if (dadoRelatorio.getOcorrencia()) {
								valorTotalNaoBaixado = valorTotalNaoBaixado.add(dadoRelatorio.getValorRecebimento());
								valorTotalTarifaNaoBaixado = valorTotalTarifaNaoBaixado.add(dadoRelatorio.getMulta());
								listaNaoBaixados.add(dadoRelatorio);

							} else {
								valorTotalBaixado = valorTotalBaixado.add(dadoRelatorio.getValorRecebimento());
								valorTotalTarifaBaixado = valorTotalTarifaBaixado.add(dadoRelatorio.getMulta());
							}
						}

						dadosRelatorio.removeAll(listaNaoBaixados);
						parametrosJasper.put("listaBaixado", dadosRelatorio);
						parametrosJasper.put("listaNaoBaixado", listaNaoBaixados);
						parametrosJasper.put("valorTotalBaixado", valorTotalBaixado);
						parametrosJasper.put("valorTotalNaoBaixado", valorTotalNaoBaixado);
						parametrosJasper.put("valorTotalTarifaNaoBaixado", valorTotalTarifaNaoBaixado);
						parametrosJasper.put("valorTotalTarifaBaixado", valorTotalTarifaBaixado);

						Collection<RelatorioProcessamentoArquivoRetornoVO> dados = new ArrayList<>();
						if (!dadosRelatorio.isEmpty()) {
							dados.add(dadosRelatorio.iterator().next());
						} else if (!listaNaoBaixados.isEmpty()) {
							dados.add(listaNaoBaixados.iterator().next());
						}

						byte[] bytes = RelatorioUtil.gerarRelatorioPDF(dados, parametrosJasper,
								"relatorioProcessamentoArquivo.jasper");

						if (bytes != null) {
							ControladorProcessoDocumento controladorProcessoDocumento = ServiceLocator.getInstancia()
									.getControladorProcessoDocumento();

							ByteArrayInputStream byteArrayInputStream = null;
							salvaRelatorio(byteArrayInputStream, processo, logProcessamento, controladorProcesso,
									controladorParametrosSistema, bytes, controladorProcessoDocumento);

							if (!StringUtils.isEmpty(processo.getEmailResponsavel())) {

								JavaMailUtil javaMailUtil = (JavaMailUtil) ServiceLocator.getInstancia()
										.getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);

								ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(bytes,
										"application/pdf");

								StringBuilder conteudoEmail = new StringBuilder();
								conteudoEmail.append("Código do Processo: ").append(processo.getChavePrimaria())
										.append("\r\n");
								conteudoEmail.append("Descrição do Processo: ")
										.append(processo.getOperacao().getDescricao()).append("\r\n");

								AtividadeSistema atividadeSistema = controladorProcesso
										.obterAtividadeSistemaPorChaveOperacao(
												processo.getOperacao().getChavePrimaria());

								javaMailUtil.enviar(atividadeSistema.getDescricaoEmailRemetente(),
										processo.getEmailResponsavel(), "Relatório de Processamento de Arquivo",
										conteudoEmail.toString(), "Relatorio_processamento_arquivo"
												+ processo.getChavePrimaria() + Constantes.EXTENSAO_ARQUIVO_PDF,
										byteArrayDataSource);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			moverArquivoComErro = true;
			Logger.getLogger("ERROR").debug(e.getStackTrace());
			exception = e;
		} finally {

			logProcessamento.append("\r\n");

			if (scanner != null) {
				scanner.close();
			}

			if (files != null && semNenhumArquivo) {
				// monta o diretorio principal de destino
				String pathDiretorioDestino = null;
				String diretorio = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(ControladorParametroSistema.DIRETORIO_ARQUIVOS_RETORNO);

				if (moverArquivoComErro) {
					// se houve algum erro, move arquivo para a pasta de erro
					pathDiretorioDestino = diretorio + Constantes.DIRETORIO_COMPLEMENTAR_ERROS;
				} else {
					// se completou com sucesso, move arquivo para a pasta de processados
					pathDiretorioDestino = diretorio + Constantes.DIRETORIO_COMPLEMENTAR_PROCESSADOS;
				}

				// os arquivos devem ser separados por data
				Date dataHoje = new Date(System.currentTimeMillis());
				SimpleDateFormat formatadorData = new SimpleDateFormat("yyyyMMdd");
				pathDiretorioDestino += formatadorData.format(dataHoje) + File.separator;

				// verifica se o diretorio ja existe, se nao existe cria.
				File diretorioDestino = Util.getFile(pathDiretorioDestino);
				if (!diretorioDestino.exists() && !diretorioDestino.mkdirs()) {
					exception = Util.suprimirExcecao(new GGASException(
							ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, Constantes.LOCALE_PADRAO),
							Constantes.PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_CRIAR_DIRETORIO), exception);
				} else {
					// move arquivo p/ pasta de destino
					arquivoDestino = Util.getFileChild(pathDiretorioDestino, arquivo.getName());
					if (arquivoDestino.exists()) {
						arquivoDestino.delete();
					}
					boolean isMoveuSucesso = false;
					for (File f : files) {
						isMoveuSucesso = f.renameTo(Util.getFileChild(pathDiretorioDestino, f.getName()));
					}
					if (!isMoveuSucesso) {
						exception = Util.suprimirExcecao(new GGASException(
								ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, Constantes.LOCALE_PADRAO),
								Constantes.PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_MOVER_ARQUIVO), exception);
					}
				}
			}
		}

		if (exception != null) {
			throw new GGASException(exception);
		}

		return logProcessamento.toString();
	}

	private void gerarBaixaTitulo(StringBuilder logProcessamento, ControladorIntegracao controladorIntegracao,
			BigDecimal somaPagosParcial, Recebimento recebimentoAdicionado) {
		try {
			controladorIntegracao.gerarBaixaTitulo(recebimentoAdicionado, Boolean.FALSE, somaPagosParcial);
		} catch (GGASException e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			logProcessamento.append(" \r\n Erro ao processar a baixa do titulo na integração. Fatura: ");
			logProcessamento.append(recebimentoAdicionado.getFaturaGeral().getFaturaAtual().getChavePrimaria());
		}
	}

	private void salvaRelatorio(ByteArrayInputStream tmpByteArrayInputStream, Processo processo,
			StringBuilder logProcessamento, ControladorProcesso controladorProcesso,
			ControladorParametroSistema controladorParametrosSistema, byte[] bytes,
			ControladorProcessoDocumento controladorProcessoDocumento) {
		ByteArrayInputStream byteArrayInputStream = tmpByteArrayInputStream;
		try {
			byteArrayInputStream = new ByteArrayInputStream(bytes);

			String pathDiretorio = (String) controladorParametrosSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_RELATORIO_PDF);
			File diretorio = Util.getFile(pathDiretorio);

			if (!diretorio.exists() && !diretorio.mkdirs()) {
				throw new NegocioException(
						ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, Constantes.LOCALE_PADRAO),
						Constantes.PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_CRIAR_DIRETORIO);
			}

			// recuperar o diretorio de parametro_sistema
			final String nomeDocumento = String.valueOf(processo.getChavePrimaria())
					+ Calendar.getInstance().getTime().getTime();

			String diretorioDocumento = pathDiretorio + nomeDocumento + Constantes.EXTENSAO_ARQUIVO_PDF;

			logProcessamento.append("\r\n\n Diretorio e nome do relatorio PDF gerado:");
			logProcessamento.append(diretorioDocumento);

			controladorProcesso.salvarRelatorio(processo, controladorProcessoDocumento, byteArrayInputStream,
					nomeDocumento, diretorioDocumento);
		} catch (Exception e) {
			throw new InfraestruturaException(e.getMessage(), e);
		}
	}

	/**
	 * Obtem o documento de layout do registro
	 * 
	 * @param registros             lista de registros
	 * @param identificadorRegistro identificador do registro
	 */
	private DocumentoLayoutRegistro obterDocumentoLayoutRegistro(Collection<DocumentoLayoutRegistro> registros,
			String identificadorRegistro) {
		DocumentoLayoutRegistro registro = null;

		for (DocumentoLayoutRegistro dlr : registros) {
			if (dlr.getIdentificadorRegistro().equalsIgnoreCase(identificadorRegistro)
					&& dlr.getMetodoProcessamento().contains("processar")) {
				registro = dlr;
				break;
			}
		}

		return registro;
	}

	private String verificaTamanhoRegistroMedio(String linha, int tamanhoRegistro) {
		String identificadorRegistro;
		if (tamanhoRegistro == REGISTRO_MEDIO) {
			identificadorRegistro = Util.obterValorPosicao(linha, new int[] { POSICAO_8, POSICAO_8 });
		} else {
			identificadorRegistro = Util.obterValorPosicao(linha, new int[] { POSICAO_1, POSICAO_1 });
		}
		return identificadorRegistro;
	}

	/**
	 * Construir mapa campos.
	 *
	 * @param documentoLayout the documento layout
	 * @param mapa            the mapa
	 * @return the map
	 */
	private Map<EntidadeConteudo, DocumentoLayoutCampo> construirMapaCampos(DocumentoLayoutRegistro documentoLayout,
			Map<EntidadeConteudo, DocumentoLayoutCampo> mapa) {

		Map<EntidadeConteudo, DocumentoLayoutCampo> campos = null;

		if (mapa != null) {
			campos = mapa;
		} else {
			campos = new HashMap<>();
		}

		for (DocumentoLayoutCampo campo : documentoLayout.getCampos()) {
			campos.put(campo.getCampo(), campo);
		}
		return campos;
	}

	/**
	 * Processar cabecalho.
	 *
	 * @param dadosArquivoRetorno  the dados arquivo retorno
	 * @param linha                the linha
	 * @param formaArrecadacao     the forma arrecadacao
	 * @param arrecadadorMovimento the arrecadador movimento
	 * @param argumentos           the argumentos
	 * @param logProcessamento     {@link StringBuilder}
	 * @return the arrecadador movimento
	 * @throws GGASException the GGAS exception
	 */
	public ArrecadadorMovimento processarCabecalho(DadosArquivoRetorno dadosArquivoRetorno, String linha,
			EntidadeConteudo formaArrecadacao, StringBuilder logProcessamento,
			ArrecadadorMovimento arrecadadorMovimento, Map<EntidadeConteudo, DocumentoLayoutCampo> argumentos)
			throws GGASException {

		DocumentoLayoutCampo campo;

		String dataGeracao = null;
		try {
			campo = argumentos
					.get(controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.DATA_GERACAO));
			dataGeracao = Util.obterValorPosicao(linha,
					new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
			String formatoData = campo.getFormato();
			dadosArquivoRetorno.setDataGeracao(Util.converterCampoStringParaData("Data", dataGeracao, formatoData));
			dadosArquivoRetorno.setDataMovimento(dataGeracao);
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException("A data de geração não foi informada.");
		}

		String sequencialArquivo = null;
		try {
			campo = argumentos.get(
					controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.SEQUENCIAL_NSA));
			sequencialArquivo = Util.obterValorPosicao(linha,
					new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException("O sequencial do arquivo não foi informado.");
		}

		String codigoConvenio = null;
		try {
			campo = argumentos.get(
					controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.CODIGO_CONVENIO));
			codigoConvenio = Util.obterValorPosicao(linha,
					new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
			codigoConvenio = codigoConvenio.trim();
			dadosArquivoRetorno.setCodigoConvenio(codigoConvenio);
			logProcessamento.append("\r\n	Código Convenio: ");
			logProcessamento.append(codigoConvenio);
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException("O código do convênio não foi informado.");
		}

		String nomeEmpresa = null;
		try {
			campo = argumentos
					.get(controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.NOME_EMPRESA));
			nomeEmpresa = Util.obterValorPosicao(linha,
					new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
			logProcessamento.append("\r\n	Nome da Empresa: ");
			logProcessamento.append(nomeEmpresa);
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException("O nome da empresa não foi informado.");
		}

		String codigoBanco = null;
		try {
			campo = argumentos
					.get(controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.CODIGO_BANCO));
			codigoBanco = Util.obterValorPosicao(linha,
					new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException("O código do banco não foi informado.");
		}

		String codigoRemessa = null;
		try {
			campo = argumentos.get(
					controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.CODIGO_REMESSA));
			codigoRemessa = Util.obterValorPosicao(linha,
					new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException("O código de remessa não foi informado.");
		}

		String versaoLayout = null;
		try {
			campo = argumentos.get(controladorEntidadeConteudo
					.obterEntidadeConteudoPorConstanteSistema(Constantes.VERSAO_LAYOUT_ARQUIVO));
			if (campo != null) {
				versaoLayout = Util.obterValorPosicao(linha,
						new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error("A versão do layout não foi informada.", e);
		}

		if (!CODIGO_RETORNO.equals(codigoRemessa)) {
			throw new GGASException("Código de Remessa não corresponde a Retorno.");
		}

		campo = argumentos
				.get(controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.DATA_GERACAO));

		arrecadadorMovimento.setDataGeracao(Util.converterCampoStringParaData("Data", dataGeracao, campo.getFormato()));
		arrecadadorMovimento.setNumeroNSA(Integer.valueOf(sequencialArquivo));
		arrecadadorMovimento.setCodigoConvenio(codigoConvenio);
		arrecadadorMovimento.setFormaArrecadacao(formaArrecadacao);
		arrecadadorMovimento.setNomeEmpresa(nomeEmpresa);
		arrecadadorMovimento.setCodigoBanco(Integer.valueOf(codigoBanco));
		arrecadadorMovimento.setNumeroVersaoLayout(versaoLayout);
		arrecadadorMovimento.setCodigoRemessa(Integer.valueOf(codigoRemessa));

		return arrecadadorMovimento;

	}

	/**
	 * Processar cadastro debito automatico.
	 *
	 * @param processo                   the processo
	 * @param documentoLayout            the documento layout
	 * @param colecaoDebitoNaoProcessado the colecao debito nao processado
	 * @param numeroLinha                the numero linha
	 * @param linha                      the linha
	 * @param logProcessamento           the log processamento
	 * @param arrecadador                the arrecadador
	 * @param debitoAutomatico           the debito automatico
	 * @param argumentos                 the argumentos
	 * @param parametros                 the parametros
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<DebitoNaoProcessado> processarCadastroDebitoAutomatico(Processo processo,
			DocumentoLayoutRegistro documentoLayout, Collection<DebitoNaoProcessado> colecaoDebitoNaoProcessado,
			int numeroLinha, String linha, StringBuilder logProcessamento, Arrecadador arrecadador,
			DebitoAutomatico debitoAutomatico, Map<String, Object> parametros,
			Map<EntidadeConteudo, DocumentoLayoutCampo> argumentos) throws GGASException {

		Date objDataOpcao = null;
		DebitoNaoProcessado debitoNaoProcessado = null;
		boolean flagErro = false;
		DocumentoLayoutRetornoOcorrencia documentoLayoutRetornoOcorrencia = null;
		String codigoOcorrencia = null;
		String descricaoOcorrencia = null;
		String formatoDataOpcao = null;

		// FIXME: Criar cadastro para os campos
		// 0 Identificação do cliente na Empresa
		// 1 Agencia para Debito
		// 2 Identificacao do cliente no Banco
		// 3 Data da Opcao
		// 4 Reservado para Futuro
		// 5 Codigo do Movimento

		String identificacaoClienteEmpresa = null;
		DocumentoLayoutCampo campo;
		try {
			campo = argumentos.get(controladorEntidadeConteudo
					.obterEntidadeConteudoPorConstanteSistema(Constantes.IDENT_CLIENTE_EMPRESA));
			identificacaoClienteEmpresa = Util.obterValorPosicao(linha,
					new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException(". A identificação do cliente na empresa não foi informada.");
		}

		String agenciaDebito = null;
		try {
			campo = argumentos.get(
					controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.AGENCIA_DEBITO));
			agenciaDebito = Util.obterValorPosicao(linha,
					new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException(". A agência para débito não foi informada.");
		}

		String identificacaoClienteBanco = null;
		try {
			campo = argumentos.get(controladorEntidadeConteudo
					.obterEntidadeConteudoPorConstanteSistema(Constantes.IDENT_CLIENTE_BANCO));
			identificacaoClienteBanco = Util.obterValorPosicao(linha,
					new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException(". A identificação do cliente no banco não foi informada.");
		}

		String dataOpcao = null;
		try {
			campo = argumentos
					.get(controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.DATA_OPCAO));
			dataOpcao = Util.obterValorPosicao(linha, new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
			formatoDataOpcao = campo.getFormato();
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException(". A data de opção não foi informada.");
		}

		String codigoMovimento = null;
		try {
			campo = argumentos.get(
					controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.CODIGO_MOVIMENTO));
			codigoMovimento = Util.obterValorPosicao(linha,
					new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() });
		} catch (ArrayIndexOutOfBoundsException e) {
			LOG.error(e.getMessage(), e);
			throw new GGASException(". O código do movimento não foi informado.");
		}

		// Validação de código de movimento
		if (StringUtils.isEmpty(codigoMovimento) || (!codigoMovimento.equals(CODIGO_MOVIMENTO_EXCLUSAO)
				&& !codigoMovimento.equals(CODIGO_MOVIMENTO_INCLUSAO))) {

			logProcessamento.append(LINHA);
			logProcessamento.append(numeroLinha);
			logProcessamento.append(". O código de movimento não corresponde ao código de inclusão ou exclusão\r\n");
			codigoOcorrencia = "IR";
			descricaoOcorrencia = "CODIGO DE MOVIMENTO INVALIDO";
			flagErro = true;
		}

		// Validação da identificação do cliente no banco
		if (StringUtils.isEmpty(identificacaoClienteBanco)) {
			logProcessamento.append(LINHA);
			logProcessamento.append(numeroLinha);
			logProcessamento.append(". A Identificação do Cliente no Banco não é válida\r\n");
			flagErro = true;
		}

		// Validação da identificação do cliente na empresa
		if (StringUtils.isEmpty(identificacaoClienteEmpresa)) {
			logProcessamento.append(LINHA);
			logProcessamento.append(numeroLinha);
			logProcessamento.append(". A Identificação do Cliente na Empresa não é válida\r\n");
			flagErro = true;
		}

		// Validação da Agência para débito
		if (StringUtils.isEmpty(agenciaDebito)) {
			logProcessamento.append(LINHA);
			logProcessamento.append(numeroLinha);
			logProcessamento.append(". A Agência para débito não é válida\r\n");
			flagErro = true;
		}

		// Validação de data de Opção
		objDataOpcao = null;
		if (!StringUtils.isEmpty(dataOpcao)) {
			try {
				objDataOpcao = Util.converterCampoStringParaData("Data de Opção", dataOpcao, formatoDataOpcao);
				debitoAutomatico.setDataOpcao(objDataOpcao);
			} catch (FormatoInvalidoException e) {
				LOG.error(e.getLocalizedMessage(), e);
				codigoOcorrencia = "IR";
				descricaoOcorrencia = "DATA DE OPCAO/EXCLUSAO INVALIDA";
				logProcessamento.append(LINHA);
				logProcessamento.append(numeroLinha);
				logProcessamento.append(". A data de Opção/Exclusão é inválida.\r\n");
				flagErro = true;
			}

		} else {
			codigoOcorrencia = "IR";
			descricaoOcorrencia = "DATA DE OPCAO/EXCLUSAO INVALIDA";
			logProcessamento.append(LINHA);
			logProcessamento.append(numeroLinha);
			logProcessamento.append(". A data de Opção não foi informada\r\n");
			flagErro = true;
		}

		ClienteDebitoAutomatico clienteDebitoAutomatico = null;
		identificacaoClienteEmpresa = identificacaoClienteEmpresa.trim();
		Long idClienteDebitoAutomatico = Long
				.parseLong(identificacaoClienteEmpresa.substring(0, identificacaoClienteEmpresa.length() - 1));

		clienteDebitoAutomatico = controladorArrecadacao.obterClienteDebitoAutomatico(idClienteDebitoAutomatico);

		if (clienteDebitoAutomatico != null) {

			if (!StringUtils.isEmpty(codigoMovimento)) {

				documentoLayoutRetornoOcorrencia = documentoLayout.obterRetornoOcorrencia(codigoMovimento);

				if (documentoLayoutRetornoOcorrencia != null) {

					if (documentoLayoutRetornoOcorrencia.isProcessaCadastro()) {

						DebitoAutomatico debitoAutomaticoCadastrado = null;
						debitoAutomaticoCadastrado = controladorArrecadacao
								.obterDebitoAutomaticoAtivo(idClienteDebitoAutomatico);

						debitoAutomatico.setAgencia(agenciaDebito);
						debitoAutomatico.setConta(identificacaoClienteBanco);
						debitoAutomatico.setArrecadador(arrecadador);
						debitoAutomatico.setDataInclusao(Calendar.getInstance().getTime());
						debitoAutomatico.setClienteDebitoAutomatico(clienteDebitoAutomatico);
						debitoAutomatico.setHabilitado(true);

						if (debitoAutomaticoCadastrado != null) {

							// achou um ja cadastrado, verifica se é mesmo arrecadador, agencia e conta.
							if (debitoAutomaticoCadastrado.getArrecadador().getCodigoAgente()
									.equals(arrecadador.getCodigoAgente())
									&& debitoAutomaticoCadastrado.getAgencia().equals(agenciaDebito)
									&& debitoAutomaticoCadastrado.getConta().equals(identificacaoClienteBanco)) {

								// débito automatico ja cadastrado
								flagErro = true;
								logProcessamento.append(LINHA);
								logProcessamento.append(numeroLinha);
								logProcessamento.append(". Débito automático já cadastrado. ");

								codigoOcorrencia = "IR";
								descricaoOcorrencia = "CONTRATO DE DEBITO JA CADASTRADO";
								processo.setEnviarLogExecucao(true);

							} else {

								// exclui o antigo e cadastra um novo.
								debitoAutomaticoCadastrado.setDataExclusao(objDataOpcao);
								controladorArrecadacao.alterarDebitoAutomatico(debitoAutomaticoCadastrado);
								controladorArrecadacao.inserirDebitoAutomatico(debitoAutomatico);
							}

						} else {

							// prossegue cadastramento
							controladorArrecadacao.inserirDebitoAutomatico(debitoAutomatico);
						}

					} else {

						// exclusao de um cadastro de debito automático
						DebitoAutomatico debitoAutomaticoCadastrado = null;
						debitoAutomaticoCadastrado = controladorArrecadacao
								.obterDebitoAutomaticoAtivo(idClienteDebitoAutomatico);

						if (debitoAutomaticoCadastrado != null) {
							debitoAutomaticoCadastrado.setDataExclusao(objDataOpcao);
							controladorArrecadacao.alterarDebitoAutomatico(debitoAutomaticoCadastrado);
						} else {
							flagErro = true;
							logProcessamento.append(LINHA);
							logProcessamento.append(numeroLinha);
							logProcessamento.append(". Exclusão para cadastro de débito automático inexistente. ");

							codigoOcorrencia = "ID";
							descricaoOcorrencia = "IDENTIFICACAO DO CLIENTE NAO LOCALIZADA / INCONSISTENTE";
							processo.setEnviarLogExecucao(true);
						}
					}
				} else {

					throw new GGASException("O código do movimento não está cadastrado como ocorrência do retorno.");
				}
			}

		} else {
			flagErro = true;
			logProcessamento.append(LINHA);
			logProcessamento.append(numeroLinha);
			logProcessamento
					.append(". Não foi possível encontrar um Cliente de Débito Automático com essa identificação: ");
			logProcessamento.append(idClienteDebitoAutomatico);

			codigoOcorrencia = "ID";
			descricaoOcorrencia = "IDENTIFICACAO DO CLIENTE NAO LOCALIZADA / INCONSISTENTE";
			processo.setEnviarLogExecucao(true);
		}

		if (flagErro) {
			debitoNaoProcessado = controladorArrecadacao.criarDebitoNaoProcessado();
			debitoNaoProcessado.setAgenciaDebito(agenciaDebito);
			debitoNaoProcessado.setCodigoMovimento(codigoMovimento);
			debitoNaoProcessado.setIdentificacaoClienteBanco(identificacaoClienteBanco);
			debitoNaoProcessado.setIdentificacaoClienteEmpresa(identificacaoClienteEmpresa);
			debitoNaoProcessado.setOcorrencia1(codigoOcorrencia);
			debitoNaoProcessado.setOcorrencia2(descricaoOcorrencia);
			colecaoDebitoNaoProcessado.add(debitoNaoProcessado);
		}

		return colecaoDebitoNaoProcessado;
	}

	/**
	 * Processar recebimento.
	 *
	 * @param tipoConvenio        the tipo convenio
	 * @param dadosAuditoria      the dados auditoria
	 * @param mapAvisoBancario    the map aviso bancario
	 * @param numeroLinha         the numero linha
	 * @param linha               the linha
	 * @param documentoLayout     the documento layout
	 * @param logProcessamento    the log processamento
	 * @param dadosArquivoRetorno the dados arquivo retorno
	 * @param dadosRelatorio      the dados relatorio
	 * @param tamanhoRegistro     the tamanho registro
	 * @param linhaValores        the linha valores
	 * @param argumentos          the argumentos
	 * @return the collection
	 * @throws GGASException the GGAS exception
	 */
	public Collection<Recebimento> processarRecebimento(EntidadeConteudo tipoConvenio, DadosAuditoria dadosAuditoria,
			Map<Date, AvisoBancario> mapAvisoBancario, int numeroLinha, String linha,
			DocumentoLayoutRegistro documentoLayout, StringBuilder logProcessamento,
			DadosArquivoRetorno dadosArquivoRetorno, Collection<RelatorioProcessamentoArquivoRetornoVO> dadosRelatorio,
			int tamanhoRegistro, String linhaValores, Map<EntidadeConteudo, DocumentoLayoutCampo> argumentos)
			throws GGASException {

		boolean ocorrenciaProcessaRecebimento = false;
		DocumentoCobranca documentoCobranca = null;
		Collection<DocumentoCobrancaItem> documentoCobrancaItens = null;
		Recebimento recebimento = null;
		Collection<Recebimento> recebimentos = new HashSet<>();
		RecebimentoSituacao recebimentoSituacao = null;
		AvisoBancario avisoBancario = null;
		DocumentoLayoutRetornoOcorrencia documentoLayoutRetornoOcorrencia = null;
		boolean tipoConvenioSemRetornoOcorrencia = false;
		StringBuilder ocorrencia = new StringBuilder();
		RelatorioProcessamentoArquivoRetornoVO relatorioProcessamentoArquivo = null;
		DebitoAutomatico debitoAutomatico = null;

		String valorPrincipal = null;
		String dataRecebimento = null;
		String codigoDocumentoCobranca = null;
		String nossoNumero = null;
		String dataVencimento = null;
		String dataCredito = null;
		String codigoRetornoOcorrencia = null;
		String nomePagador = null;
		String primeiroCodigoErro = null;
		String segundoCodigoErro = null;
		String terceiroCodigoErro = null;

		String valorJurosMulta = null;
		String valorDescontos = null;
		String valorAbatimento = null;
		String valorCreditos = null;
		String valorIOF = null;
		String valorCobranca = null;
		String valorCodigoBarras = null;

		Date objDataRecebimento = null;
		Date objDataVencimento = null;
		Date objDataCredito = null;

		DocumentoLayoutCampo campo;

		String convenioArrecadacao = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_FORMA_COBRANCA_CODIGO_BARRAS);

		logProcessamento.append("\r\n\n");
		logProcessamento.append(LINHA);
		logProcessamento.append(numeroLinha);
		logProcessamento.append(" (MOVIMENTO):");

		if (tamanhoRegistro == REGISTRO_PEQUENO) {

			tipoConvenioSemRetornoOcorrencia = true;
			ocorrenciaProcessaRecebimento = true;

			if (Long.valueOf(convenioArrecadacao).equals(tipoConvenio.getChavePrimaria())) {

				try {
					campo = argumentos.get(controladorEntidadeConteudo
							.obterEntidadeConteudoPorConstanteSistema(Constantes.VALOR_RECEBIMENTO));
					valorPrincipal = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				} catch (ArrayIndexOutOfBoundsException e) {
					LOG.error(e.getMessage(), e);
					throw new GGASException(VALOR_RECEBIMENTO_NAO_INFORMADO);
				}

				try {
					campo = argumentos.get(controladorEntidadeConteudo
							.obterEntidadeConteudoPorConstanteSistema(Constantes.CODIGO_BARRAS));
					String codigoBarras = null;
					if (campo != null) {
						codigoBarras = Util.obterValorPosicao(linha,
								new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
						codigoDocumentoCobranca = codigoBarras.substring(POSICAO_33, POSICAO_44);
						valorCodigoBarras = codigoBarras.substring(POSICAO_5, POSICAO_15);
					} else {
						campo = argumentos.get(controladorEntidadeConteudo
								.obterEntidadeConteudoPorConstanteSistema(Constantes.NUMERO_AUT_CODIGO_TRANSMISSAO));
						if (campo != null) {
							codigoDocumentoCobranca = Util.obterValorPosicao(linha,
									new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
						}
					}

				} catch (ArrayIndexOutOfBoundsException e) {
					LOG.error(e.getMessage(), e);
					throw new GGASException(CODIGO_DOCUMENTO_DE_COBRANCA_NAO_INFORMADO);
				}

				try {
					campo = argumentos.get(controladorEntidadeConteudo
							.obterEntidadeConteudoPorConstanteSistema(Constantes.DATA_CREDITO));
					if (campo != null) {
						dataCredito = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
								new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
						dataRecebimento = dataCredito;
						objDataRecebimento = Util.converterCampoStringParaData("Data", dataRecebimento,
								campo.getFormato());
						objDataCredito = objDataRecebimento;
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					LOG.error(e);
					throw new GGASException("A data do crédito não foi informada.");
				}

			} else {

				try {
					campo = argumentos.get(controladorEntidadeConteudo
							.obterEntidadeConteudoPorConstanteSistema(Constantes.VALOR_DEBITO));
					valorPrincipal = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				} catch (ArrayIndexOutOfBoundsException e) {
					LOG.error(e.getMessage(), e);
					throw new GGASException(VALOR_RECEBIMENTO_NAO_INFORMADO);
				}

				try {
					campo = argumentos.get(controladorEntidadeConteudo
							.obterEntidadeConteudoPorConstanteSistema(Constantes.USO_EMPRESA));
					codigoDocumentoCobranca = Util
							.obterValorPosicao(linha, new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() })
							.trim();
				} catch (ArrayIndexOutOfBoundsException e) {
					LOG.error(e.getMessage(), e);
					throw new GGASException(CODIGO_DOCUMENTO_DE_COBRANCA_NAO_INFORMADO);
				}

				try {
					campo = argumentos.get(controladorEntidadeConteudo
							.obterEntidadeConteudoPorConstanteSistema(Constantes.DATA_VENCIMENTO));
					if (campo != null) {
						dataCredito = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
								new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
						dataRecebimento = dataCredito;
						objDataRecebimento = Util.converterCampoStringParaData("Data", dataRecebimento,
								campo.getFormato());
						objDataCredito = objDataRecebimento;
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					LOG.error(e);
					throw new GGASException("A data do crédito não foi informada.");
				}

			}

		} else {

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.VALOR_RECEBIMENTO));
				valorPrincipal = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
						new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				String valorPrincipalFormatado = String.valueOf(Long.parseLong(valorPrincipal));
				if (!"0".equals(valorPrincipalFormatado)) {
					logProcessamento.append(STRING_VALOR_RECEBIDO);
					logProcessamento
							.append(valorPrincipalFormatado.substring(0, valorPrincipalFormatado.length() - POSICAO_2));
					logProcessamento.append(",");
					logProcessamento.append(valorPrincipalFormatado
							.substring(valorPrincipalFormatado.length() - POSICAO_2, valorPrincipalFormatado.length()));
				} else {
					logProcessamento.append(" Valor Recebido: 0");
				}

			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
				throw new GGASException(VALOR_RECEBIMENTO_NAO_INFORMADO);
			}

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.DATA_RECEBIMENTO));
				dataRecebimento = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
						new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				dataCredito = dataRecebimento;
				logProcessamento.append("\r\n");
				logProcessamento.append("	Data Recebimento:");
				logProcessamento.append(dataRecebimento.substring(POSICAO_0, POSICAO_2));
				logProcessamento.append(dataRecebimento.substring(POSICAO_2, POSICAO_4));
				logProcessamento.append(dataRecebimento.substring(POSICAO_4, POSICAO_6));
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
				throw new GGASException("A data do crédito/recebimento não foi informada.");
			}

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.NUMERO_DOCUMENTO));
				codigoDocumentoCobranca = Util
						.obterValorPosicao(linha, new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() })
						.trim();
				logProcessamento.append("\r\n");
				logProcessamento.append("	Documento de Cobrança:");

				if ("".equals(codigoDocumentoCobranca)) {
					logProcessamento.append(" Sem Documento de Cobrança!");
				} else {
					logProcessamento.append(codigoDocumentoCobranca);
				}

			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(CODIGO_DOCUMENTO_DE_COBRANCA_NAO_INFORMADO, e);
			}

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.CODIGO_RETORNO_OCORRENCIA));
				codigoRetornoOcorrencia = Util
						.obterValorPosicao(linha, new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() })
						.trim();
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
				// O convênio código de barra não possui código de ocorrência
				tipoConvenioSemRetornoOcorrencia = true;
				ocorrenciaProcessaRecebimento = true;
			}

			try {
				campo = argumentos.get(
						controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.NOSSO_NUMERO));
				nossoNumero = Util
						.obterValorPosicao(linha, new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() })
						.trim();
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(CODIGO_DOCUMENTO_DE_COBRANCA_NAO_INFORMADO, e);
			}

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.JUROS_MORA_MULTA));
				if (campo != null) {
					valorJurosMulta = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
			}

			try {
				campo = argumentos.get(
						controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.DESCONTOS));
				if (campo != null) {
					valorDescontos = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
			}

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.VALOR_ABATIMENTO));
				if (campo != null) {
					valorAbatimento = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
			}

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.OUTROS_CREDITOS));
				if (campo != null) {
					valorCreditos = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
			}

			try {
				campo = argumentos
						.get(controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.IOF));
				if (campo != null) {
					valorIOF = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
			}

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.TARIFA_COBRANCA));
				if (campo != null) {
					valorCobranca = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
			}

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.DATA_VENCIMENTO));
				dataVencimento = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
						new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();

				if (dataVencimento == null || DATA_INVALIDA_SEIS_DIGITOS.equals(dataVencimento)
						|| DATA_INVALIDA_OITO_DIGITOS.equals(dataVencimento) || dataVencimento.isEmpty()) {

					List<DocumentoCobranca> documentoCobranca1 = (List<DocumentoCobranca>) controladorDocumentoCobranca
							.buscarDocumentoCobrancaNossoNumero(Long.valueOf(nossoNumero));

					if (documentoCobranca1 != null && !documentoCobranca1.isEmpty()) {
						dataVencimento = Util.converterDataParaStringDiaMesAnoSemCaracteresEspeciais(
								documentoCobranca1.get(0).getDataVencimento());
					}
				}

				logProcessamento.append("\r\n");
				logProcessamento.append("	Data Vencimento:");
				logProcessamento.append(dataVencimento.substring(POSICAO_0, POSICAO_2));
				logProcessamento.append(dataVencimento.substring(POSICAO_2, POSICAO_4));
				logProcessamento.append(dataVencimento.substring(POSICAO_4, POSICAO_6));

			} catch (ArrayIndexOutOfBoundsException e) {

				LOG.error(e);
				throw new GGASException("A data do vencimento não foi informada.");
			}

			try {
				campo = argumentos.get(
						controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.NOME_PAGADOR));
				if (campo != null) {
					nomePagador = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
					logProcessamento.append("\r\n");
					logProcessamento.append("	Nome Pagador:");
					logProcessamento.append(nomePagador);
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
			}

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.PRIMEIRO_CODIGO_ERRO));
				if (campo != null) {
					primeiroCodigoErro = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
			}

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.SEGUNDO_CODIGO_ERRO));
				if (campo != null) {
					segundoCodigoErro = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
			}

			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.TERCEIRO_CODIGO_ERRO));
				if (campo != null) {
					terceiroCodigoErro = Util.obterValorPosicao(processarLinha(tamanhoRegistro, linha, linhaValores),
							new int[] { campo.getPosicaoInicial(), campo.getPosicaoFinal() }).trim();
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				LOG.error(e);
			}

		}

		String identificacaoClienteEmpresa = null;

		Integer anoMesContabilParametro = Integer.valueOf(
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(PARAMETRO_REFERENCIA_CONTABIL));

		if (anoMesContabilParametro == null) {
			throw new GGASException(
					"O ano mês contábil não foi cadastrado no parâmetro: " + PARAMETRO_REFERENCIA_CONTABIL);
		}

		if (!StringUtils.isEmpty(convenioArrecadacao)
				&& Long.valueOf(convenioArrecadacao).equals(tipoConvenio.getChavePrimaria())
				&& !Long.valueOf(valorPrincipal).equals(Long.valueOf(valorCodigoBarras))) {

			String erroValorCodigoBarras = "	(ATENÇÃO)Valor pago diferente do valor existente no código de barras.";
			String erroDocumentoCobranca = "Para o documento cobrança: " + Long.valueOf(codigoDocumentoCobranca);
			ocorrenciaProcessaRecebimento = false;
			ocorrencia.setLength(0);
			ocorrencia.append(erroValorCodigoBarras).append(erroDocumentoCobranca);
			logProcessamento.append(erroValorCodigoBarras);
			logProcessamento.append(erroDocumentoCobranca);
		}

		try {

			Collection<DocumentoCobranca> listaDocumentoCobranca;

			if (!StringUtils.isEmpty(codigoDocumentoCobranca)) {

				listaDocumentoCobranca = controladorDocumentoCobranca.buscarDocumentoCobranca(
						Util.converterCampoStringParaValorLong("documentoCobranca", codigoDocumentoCobranca));

				if (listaDocumentoCobranca != null && !listaDocumentoCobranca.isEmpty()) {
					documentoCobranca = listaDocumentoCobranca.iterator().next();
				} else {
					logProcessamento.append("\r\n");
					logProcessamento.append("	(ATENÇÃO)O documento de cobrança não foi encontrado.");
				}

			} else if (!StringUtils.isEmpty(nossoNumero)) {

				listaDocumentoCobranca = controladorDocumentoCobranca.buscarDocumentoCobrancaNossoNumero(
						Util.converterCampoStringParaValorLong("nossoNumero", nossoNumero));

				if (listaDocumentoCobranca != null && !listaDocumentoCobranca.isEmpty()) {
					if (listaDocumentoCobranca.size() > 1) {
						ocorrencia.setLength(0);
						ocorrencia.append("A consulta retornou mais de um documento de cobrança.");
						logProcessamento.append("A consulta retornou mais de um documento de cobrança. ");
						documentoCobranca = null;
					} else {
						documentoCobranca = listaDocumentoCobranca.iterator().next();
					}
				} else {
					logProcessamento.append("	(ATENÇÃO)O documento de cobrança não foi encontrado. ");
				}

			} else {
				ocorrencia.setLength(0);
				ocorrencia.append("O documento de cobrança não foi informado.");
				logProcessamento.append("\r\n");
				logProcessamento.append("	(ATENÇÃO)O documento de cobrança não foi informado.");
			}

		} catch (FormatoInvalidoException e) {
			LOG.error(e);
			throw new GGASException(e);
		}

		if (objDataRecebimento == null && !StringUtils.isEmpty(dataRecebimento)
				&& !DATA_INVALIDA_SEIS_DIGITOS.equals(dataRecebimento)
				&& !DATA_INVALIDA_OITO_DIGITOS.equals(dataRecebimento)) {
			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.DATA_RECEBIMENTO));
				objDataRecebimento = Util.converterCampoStringParaData("Data", dataRecebimento, campo.getFormato());
			} catch (FormatoInvalidoException e) {
				LOG.error(e);
				throw new GGASException(e);
			}
		} else if (objDataRecebimento == null) {
			ocorrencia.setLength(0);
			ocorrencia.append("A data de recebimento não foi informada.");
			logProcessamento.append("\r\n");
			logProcessamento.append("	(ATENÇÃO)A data de recebimento não foi informada.");
		}

		if (objDataVencimento == null && !StringUtils.isEmpty(dataVencimento)
				&& !DATA_INVALIDA_SEIS_DIGITOS.equals(dataVencimento)
				&& !DATA_INVALIDA_OITO_DIGITOS.equals(dataVencimento)) {
			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.DATA_VENCIMENTO));
				objDataVencimento = Util.converterCampoStringParaData("Data", dataVencimento, campo.getFormato());
			} catch (FormatoInvalidoException e) {
				LOG.error(e);
				throw new GGASException(e);
			}
		} else if (objDataVencimento == null) {

			if (documentoCobranca == null || documentoCobranca.getDataVencimento() == null) {
				ocorrencia.setLength(0);
				ocorrencia.append("A data de vencimento não foi informada.");
				logProcessamento.append("\r\n");
				logProcessamento.append("	(ATENÇÃO)A data de vencimento não foi informada.");
			} else {
				objDataVencimento = documentoCobranca.getDataVencimento();
			}
		}

		if (objDataCredito == null && !StringUtils.isEmpty(dataCredito)
				&& !DATA_INVALIDA_SEIS_DIGITOS.equals(dataCredito) && !DATA_INVALIDA_OITO_DIGITOS.equals(dataCredito)) {
			try {
				campo = argumentos.get(controladorEntidadeConteudo
						.obterEntidadeConteudoPorConstanteSistema(Constantes.DATA_RECEBIMENTO));
				objDataCredito = Util.converterCampoStringParaData("Data", dataCredito, campo.getFormato());
			} catch (FormatoInvalidoException e) {
				LOG.error(e);
				throw new GGASException(e);
			}
		} else if (objDataCredito == null) {
			ocorrencia.setLength(0);
			ocorrencia.append("A data do credito não foi informada.");
			logProcessamento.append("\r\n");
			logProcessamento.append("	(ATENÇÃO)A data do credito não foi informada.");
		}

		BigDecimal objValorPrincipal = BigDecimal.ZERO;
		if (!StringUtils.isEmpty(valorPrincipal)) {
			objValorPrincipal = new BigDecimal(
					Util.formatarValorMonetarioString(valorPrincipal, valorPrincipal.length(), QTD_CASAS_DECIMAIS));

		} else {
			ocorrencia.setLength(0);
			ocorrencia.append("O valor principal não foi informado.");
			logProcessamento.append("\r\n");
			logProcessamento.append("	(ATENÇÃO)O valor principal não foi informado.");
		}

		BigDecimal objValorJurosMulta = BigDecimal.ZERO;
		if (valorJurosMulta != null && !StringUtils.isEmpty(valorJurosMulta)) {
			objValorJurosMulta = new BigDecimal(
					Util.formatarValorMonetarioString(valorJurosMulta, valorJurosMulta.length(), QTD_CASAS_DECIMAIS));
		}

		BigDecimal objValorDescontos = BigDecimal.ZERO;
		if (valorDescontos != null && !StringUtils.isEmpty(valorDescontos)) {
			objValorDescontos = new BigDecimal(
					Util.formatarValorMonetarioString(valorDescontos, valorDescontos.length(), QTD_CASAS_DECIMAIS));
		}

		BigDecimal objValorAbatimento = BigDecimal.ZERO;
		if (valorAbatimento != null && !StringUtils.isEmpty(valorAbatimento)) {
			objValorAbatimento = new BigDecimal(
					Util.formatarValorMonetarioString(valorAbatimento, valorAbatimento.length(), QTD_CASAS_DECIMAIS));
		}

		BigDecimal objValorCredito = BigDecimal.ZERO;
		if (valorCreditos != null && !StringUtils.isEmpty(valorCreditos)) {
			objValorCredito = new BigDecimal(
					Util.formatarValorMonetarioString(valorCreditos, valorCreditos.length(), QTD_CASAS_DECIMAIS));
		}

		BigDecimal objValorIOF = BigDecimal.ZERO;
		if (valorIOF != null && !StringUtils.isEmpty(valorIOF)) {
			objValorIOF = new BigDecimal(
					Util.formatarValorMonetarioString(valorIOF, valorIOF.length(), QTD_CASAS_DECIMAIS));
		}

		BigDecimal objValorCobranca = BigDecimal.ZERO;
		if (valorCobranca != null && !StringUtils.isEmpty(valorCobranca)) {
			objValorCobranca = new BigDecimal(
					Util.formatarValorMonetarioString(valorCobranca, valorCobranca.length(), QTD_CASAS_DECIMAIS));
		}

		BigDecimal objValorRecebimento;
		objValorRecebimento = objValorPrincipal.add(objValorJurosMulta).add(objValorIOF).subtract(objValorDescontos)
				.subtract(objValorAbatimento).add(objValorCredito);

		if (objValorRecebimento.compareTo(BigDecimal.ZERO) <= 0) {
			ocorrencia.setLength(0);
			ocorrencia.append("O valor do recebimento é inválido.");
			logProcessamento.append("\r\n");
			logProcessamento.append("	(ATENÇÂO)O valor do recebimento é inválido.");
		}

		// Verificando o código de retorno para poder processar o recebimento
		if (!tipoConvenioSemRetornoOcorrencia) {

			if (!StringUtils.isEmpty(codigoRetornoOcorrencia)) {

				documentoLayoutRetornoOcorrencia = documentoLayout.obterRetornoOcorrencia(codigoRetornoOcorrencia);

				logProcessamento.append("\r\n");
				logProcessamento.append("	Movimento de ocorrência:");
				logProcessamento.append(codigoRetornoOcorrencia);
				logProcessamento.append(" - ");

				if (documentoLayoutRetornoOcorrencia != null) {
					logProcessamento.append(documentoLayoutRetornoOcorrencia.getDescricao());
				} else {
					logProcessamento.append(" Codigo de Ocorrência não cadastrado! ");
				}

				popularLogComCodigoOcorrenciaErro(primeiroCodigoErro, "	Primeiro código de erro:", logProcessamento,
						documentoLayout);
				popularLogComCodigoOcorrenciaErro(segundoCodigoErro, "	Segundo código de erro:", logProcessamento,
						documentoLayout);
				popularLogComCodigoOcorrenciaErro(terceiroCodigoErro, "	Terceiro código de erro:", logProcessamento,
						documentoLayout);

				if (documentoLayoutRetornoOcorrencia != null) {

					if (documentoLayoutRetornoOcorrencia.isProcessaRecebimento()) {

						ocorrenciaProcessaRecebimento = true;

					} else if (!documentoLayoutRetornoOcorrencia.isProcessaCadastro()) {

						// exclusao de um cadastro de debito automático, ocorrências 02, 10, 14, 15, 30,
						// 49 e 50.
						// caso for tratar as ocorrências restantes (ex 01, 04, 12...), verificar
						// processamento.

						// FIXME: Mudar para pegar dos argumentos.
						identificacaoClienteEmpresa = Util.obterValorPosicao(linha, new int[] { POSICAO_2, POSICAO_26 })
								.trim();
						debitoAutomatico = controladorArrecadacao
								.obterDebitoAutomaticoAtivo(Long.parseLong(identificacaoClienteEmpresa.trim()));

						if (debitoAutomatico != null) {
							debitoAutomatico.setDataExclusao(Calendar.getInstance().getTime());
							controladorArrecadacao.alterarDebitoAutomatico(debitoAutomatico);
						}

						ocorrenciaProcessaRecebimento = false;
						ocorrencia.setLength(0);
						ocorrencia.append(documentoLayoutRetornoOcorrencia.getDescricao());

					} else {
						ocorrenciaProcessaRecebimento = false;
						ocorrencia.setLength(0);
						ocorrencia.append(documentoLayoutRetornoOcorrencia.getDescricao());
					}
				}

			} else {
				ocorrenciaProcessaRecebimento = false;
			}
		}

		if (objDataRecebimento != null || ocorrenciaProcessaRecebimento) {

			if (mapAvisoBancario.containsKey(objDataRecebimento)) {

				avisoBancario = mapAvisoBancario.get(objDataRecebimento);
				avisoBancario.setValorArrecadacaoInformado(
						avisoBancario.getValorArrecadacaoInformado().add(objValorRecebimento));
				avisoBancario.setValorArrecadacaoCalculado(
						avisoBancario.getValorArrecadacaoCalculado().add(objValorRecebimento));
				avisoBancario.setValorRealizado(avisoBancario.getValorRealizado().add(objValorRecebimento));

			} else {

				Date dataLancamento = dadosArquivoRetorno.getDataGeracao();
				String codigoConvenio = dadosArquivoRetorno.getCodigoConvenio();
				Arrecadador arrecadador = dadosArquivoRetorno.getArrecadador();
				ArrecadadorContratoConvenio arrecadadorContratoConvenio = null;
				ParametroSistema parametroCampoArquivoCodigoConvenio = controladorParametroSistema
						.obterParametroPorCodigo(Constantes.ARRECADACAO_CAMPO_ARQUIVO_CODIGO_CONVENIO);

				if (StringUtils.isNumeric(codigoConvenio)) {
					arrecadadorContratoConvenio = controladorArrecadacao
							.obterArrecadadorContratoConvenio(codigoConvenio, arrecadador.getChavePrimaria());
				} else if (parametroCampoArquivoCodigoConvenio.getValor().equals(codigoConvenio.trim())) {
					arrecadadorContratoConvenio = controladorArrecadacao
							.obterArrecadadorContratoConvenio(
									controladorParametroSistema
											.obterParametroPorCodigo(Constantes.ARRECADACAO_CODIGO_CONVENIO).getValor(),
									arrecadador.getChavePrimaria());
				}

				if (arrecadadorContratoConvenio == null) {
					throw new GGASException(
							"Não foi possível recuperar o Arrecadador Contrato Convênio. Código Convênio: "
									+ codigoConvenio + " Arrecadador: " + arrecadador.getChavePrimaria());
				}

				ContaBancaria contaConvenio = arrecadadorContratoConvenio.getContaCredito();

				if (contaConvenio == null) {
					throw new GGASException("A conta de crédito não foi encontrada.");
				}

				Date dataPrevistaDebito = this.obterDataPrevistaParaDebito(objDataRecebimento,
						arrecadadorContratoConvenio);

				// criar um novo e botar no mapa
				avisoBancario = (AvisoBancario) controladorArrecadacao.criarAvisoBancario();
				avisoBancario.setArrecadador(arrecadador);
				avisoBancario.setArrecadadorMovimento(dadosArquivoRetorno.getArrecadadorMovimento());
				avisoBancario.setContaBancaria(contaConvenio);
				avisoBancario.setAnoMesContabil(
						controladorArrecadacao.obterAnoMesContabil(anoMesContabilParametro, dataLancamento));
				avisoBancario.setIndicadorCreditoDebito(true);
				avisoBancario.setNumeroDocumento(null);
				avisoBancario.setNumeroSequencial(
						this.obterSequencialAvisoBancario(dadosArquivoRetorno.getArrecadador(), dataLancamento));
				avisoBancario.setDataLancamento(dataLancamento);
				avisoBancario.setValorContabilizado(BigDecimal.ZERO);
				avisoBancario.setValorRealizado(objValorRecebimento);
				avisoBancario.setDataPrevista(dataPrevistaDebito);
				avisoBancario.setDataRealizada(dataPrevistaDebito);
				avisoBancario.setValorArrecadacaoCalculado(objValorRecebimento);
				avisoBancario.setValorDevolucaoCalculado(BigDecimal.ZERO);
				avisoBancario.setValorArrecadacaoInformado(objValorRecebimento);
				avisoBancario.setValorDevolucaoInformado(BigDecimal.ZERO);
				avisoBancario.setHabilitado(true);
				mapAvisoBancario.put(objDataRecebimento, avisoBancario);
			}

		}

		ConstanteSistema recebimentoClassificado = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_RECEBIMENTO_CLASSIFICADO);
		recebimentoSituacao = controladorRecebimentos
				.obterRecebimentoSituacao(Long.valueOf(recebimentoClassificado.getValor()));

		// Classificar verificar se o arrecadador Movimento Item deverá ser gerado
		// (Aceite)
		boolean aceitacao = true;
		ArrecadadorMovimentoItem arrecadadorMovimentoItem = null;

		if (ocorrenciaProcessaRecebimento && objDataRecebimento != null && tipoConvenio != null) {

			if (aceitacao) {
				arrecadadorMovimentoItem = (ArrecadadorMovimentoItem) controladorArrecadacao
						.criarArrecadadorMovimentoItem();
				arrecadadorMovimentoItem.setLinhaRegistro(linha);
				arrecadadorMovimentoItem.setDescricaoOcorrencia("Inclusão de Novo Registro");

				if (recebimentos != null && !recebimentos.isEmpty()) {
					arrecadadorMovimentoItem.setAceitacao(true);
				} else {
					arrecadadorMovimentoItem.setAceitacao(false);
				}

				arrecadadorMovimentoItem.setDadosAuditoria(dadosAuditoria);
				arrecadadorMovimentoItem.setVersao(1);
				arrecadadorMovimentoItem.setUltimaAlteracao(Calendar.getInstance().getTime());

				arrecadadorMovimentoItem.setArrecadadorMovimento(dadosArquivoRetorno.getArrecadadorMovimento());
				arrecadadorMovimentoItem.setVersao(1);
				arrecadadorMovimentoItem.setDadosAuditoria(dadosAuditoria);
				dadosArquivoRetorno.getArrecadadorMovimento().getItens().add(arrecadadorMovimentoItem);

				controladorArrecadacao.alterarArrecadacaoMovimento(dadosArquivoRetorno.getArrecadadorMovimento());

			}
			// FIXME: CORRIGIR esse trecho
			if (codigoRetornoOcorrencia != null && "BD".equals(codigoRetornoOcorrencia)) {

				ocorrencia.setLength(0);
				ocorrencia.append(documentoLayoutRetornoOcorrencia.getDescricao());
				logProcessamento.append("\r\n");
				logProcessamento.append(LINHA);
				logProcessamento.append(numeroLinha);
				logProcessamento.append(": ");
				logProcessamento.append(documentoLayoutRetornoOcorrencia.getDescricao());

			} else {

				if (documentoCobranca != null) {

					documentoCobrancaItens = documentoCobranca.getItens();
					BigDecimal valorRecebimentoAux = objValorRecebimento;

					Iterator<DocumentoCobrancaItem> documentoCobrancaItensIterator = documentoCobrancaItens.iterator();

					while (documentoCobrancaItensIterator.hasNext()) {

						DocumentoCobrancaItem documentoCobrancaItem = documentoCobrancaItensIterator.next();

						if (valorRecebimentoAux.compareTo(BigDecimal.ZERO) > 0) {
							recebimento = (Recebimento) controladorRecebimentos.criar();
							recebimento.setCliente((Cliente) controladorCliente
									.obter(documentoCobranca.getCliente().getChavePrimaria()));
							recebimento.setRecebimentoSituacao(recebimentoSituacao);
							recebimento.setAnoMesContabil(anoMesContabilParametro);
							recebimento.setArrecadadorMovimentoItem(arrecadadorMovimentoItem);
							recebimento.setAvisoBancario(avisoBancario);
							recebimento.setDocumentoCobrancaItem(documentoCobrancaItem);
							recebimento.setDadosAuditoria(dadosAuditoria);

							if (documentoCobrancaItem.getPontoConsumo() != null) {
								PontoConsumo pc = (PontoConsumo) controladorPontoConsumo.obter(
										documentoCobrancaItem.getPontoConsumo().getChavePrimaria(), "quadraFace",
										"quadraFace.quadra", "quadraFace.quadra.setorComecial",
										"quadraFace.quadra.setorComecial.localidade");
								recebimento.setLocalidade(
										pc.getQuadraFace().getQuadra().getSetorComercial().getLocalidade());
								recebimento.setPontoConsumo(pc);
							}

							recebimento.setFormaArrecadacao(tipoConvenio);
							recebimento.setDataRecebimento(objDataRecebimento);
							recebimento.setValorJurosMulta(objValorJurosMulta);
							recebimento.setValorDescontos(objValorDescontos);
							recebimento.setValorAbatimento(objValorAbatimento);
							recebimento.setValorCredito(objValorCredito);
							recebimento.setValorIOF(objValorIOF);
							recebimento.setValorCobranca(objValorCobranca);

							if (valorRecebimentoAux.compareTo(documentoCobrancaItem.getValor()) < 0) {

								recebimento.setValorPrincipal(documentoCobrancaItem.getValor());
								recebimento.setValorRecebimento(valorRecebimentoAux);
								valorRecebimentoAux = BigDecimal.ZERO;

							} else {

								if (documentoCobrancaItensIterator.hasNext()) {
									recebimento.setValorPrincipal(documentoCobrancaItem.getValor());
									recebimento.setValorRecebimento(documentoCobrancaItem.getValor());
									valorRecebimentoAux = valorRecebimentoAux
											.subtract(documentoCobrancaItem.getValor());
								} else {
									recebimento.setValorPrincipal(documentoCobrancaItem.getValor());
									recebimento.setValorRecebimento(valorRecebimentoAux);
									valorRecebimentoAux = BigDecimal.ZERO;
								}

							}

							controladorRecebimentos.classificarRecebimento(recebimento, false);
							recebimentos.add(recebimento);
						}
					}
				} else {
					recebimento = (Recebimento) controladorRecebimentos.criar();
					recebimento.setRecebimentoSituacao(recebimentoSituacao);
					recebimento.setAnoMesContabil(anoMesContabilParametro);
					recebimento.setArrecadadorMovimentoItem(arrecadadorMovimentoItem);
					recebimento.setAvisoBancario(avisoBancario);
					recebimento.setFormaArrecadacao(tipoConvenio);
					recebimento.setDataRecebimento(objDataRecebimento);
					recebimento.setDadosAuditoria(dadosAuditoria);

					recebimento.setValorPrincipal(objValorPrincipal);
					recebimento.setValorJurosMulta(objValorJurosMulta);
					recebimento.setValorDescontos(objValorDescontos);
					recebimento.setValorAbatimento(objValorAbatimento);
					recebimento.setValorCredito(objValorCredito);
					recebimento.setValorIOF(objValorIOF);
					recebimento.setValorCobranca(objValorCobranca);
					recebimento.setValorRecebimento(objValorRecebimento);

					controladorRecebimentos.classificarRecebimento(recebimento, false);
					recebimentos.add(recebimento);
				}
			}

		}

		if (recebimento != null) {

			Map<String, Object> filtro = criarFiltroRecebimentoAnterior(recebimento);
			String nomeArquivo = recebimento.getArrecadadorMovimentoItem().getArrecadadorMovimento().getNomeArquivo();
			ocorrencia.append(criarMensagemOcorrencia(recebimento.getRecebimentoSituacao(), nomeArquivo));

			relatorioProcessamentoArquivo = new RelatorioProcessamentoArquivoRetornoVO();
			if (ocorrencia != null && !StringUtils.isEmpty(ocorrencia.toString()) && !"Título baixado".equals(ocorrencia.toString())) {
				relatorioProcessamentoArquivo.setOcorrencia(true);
				relatorioProcessamentoArquivo.setDescricaoOcorrencia(ocorrencia.toString());
			} else {
				relatorioProcessamentoArquivo.setDescricaoOcorrencia("Título baixado");
			}
			BigDecimal tarifa = new BigDecimal("0.00");
			if (documentoCobranca != null && documentoCobranca.getItens() != null) {

				ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

				FaturaGeral faturaGeral = (FaturaGeral) controladorFatura.obter(
						documentoCobranca.getItens().iterator().next().getFaturaGeral().getChavePrimaria(),
						FaturaGeralImpl.class, "faturaAtual");
				Fatura fatura = (Fatura) controladorFatura.obter(faturaGeral.getFaturaAtual().getChavePrimaria());

				if (fatura.getPontoConsumo() != null) {
					relatorioProcessamentoArquivo.setPontoConsumo(fatura.getPontoConsumo().getDescricao());
				}
				relatorioProcessamentoArquivo.setDocumentoCobranca(String.valueOf(fatura.getChavePrimaria()));

				fatura.setListaFaturaItem(controladorFatura.listarFaturaItemPorChaveFatura(fatura.getChavePrimaria()));

				Collection<FaturaItem> listaFaturaItem = fatura.getListaFaturaItem();

				if (listaFaturaItem != null) {
					BigDecimal medidaTotal = new BigDecimal("0.00");
					for (FaturaItem faturaItem : listaFaturaItem) {
						if (faturaItem.getMedidaConsumo() != null) {
							medidaTotal = medidaTotal.add(faturaItem.getMedidaConsumo());
						}
					}
					if (medidaTotal.compareTo(BigDecimal.ZERO) > 0) {
						BigDecimal total = fatura.getValorTotal();
						tarifa = tarifa.add(total.divide(medidaTotal, new MathContext(PRECISION)));
					}

				}
			} else {
				relatorioProcessamentoArquivo.setDocumentoCobranca(nossoNumero);
			}
			if (relatorioProcessamentoArquivo.getPontoConsumo() != null) {
				relatorioProcessamentoArquivo.setNumeroLinha(numeroLinha);
			}
			if (tipoConvenio != null) {
				relatorioProcessamentoArquivo.setDescricaoConvenio(tipoConvenio.getDescricao());
			}
			relatorioProcessamentoArquivo
					.setCodigoBanco(String.valueOf(dadosArquivoRetorno.getArrecadadorMovimento().getCodigoBanco()));
			relatorioProcessamentoArquivo.setNomeBanco(dadosArquivoRetorno.getArrecadador().getBanco().getNome());

			relatorioProcessamentoArquivo.setDataRecebimento(objDataRecebimento);
			relatorioProcessamentoArquivo.setDataVencimento(objDataVencimento);
			relatorioProcessamentoArquivo.setDataCredito(objDataCredito);
			relatorioProcessamentoArquivo.setValorRecebimento(objValorRecebimento);
			relatorioProcessamentoArquivo.setMulta(objValorJurosMulta);
			relatorioProcessamentoArquivo.setTarifa(tarifa);

			dadosRelatorio.add(relatorioProcessamentoArquivo);

		}

		Collection<CobrancaBancariaMovimento> listaCobrancaBancariaMovimento;

		if (documentoLayoutRetornoOcorrencia != null) {
			if (documentoCobranca != null) {
				listaCobrancaBancariaMovimento = controladorArrecadacao
						.consultarCobrancaBancariaMovimentoHabilitado(documentoCobranca);
				if (listaCobrancaBancariaMovimento != null && !listaCobrancaBancariaMovimento.isEmpty()) {
					atualizarCobrancaMovimento(listaCobrancaBancariaMovimento.iterator().next(),
							documentoLayoutRetornoOcorrencia);
					logProcessamento.append("\r\n");
					logProcessamento.append(
							"	Foi atualizada com sucesso o movimento de cobrança com número de documento de cobrança : ");
					logProcessamento.append(documentoCobranca.getChavePrimaria());

				} else {
					logProcessamento.append("\r\n");
					logProcessamento.append(
							"	(ATENÇÂO) Não foi possivel atualizar o movimento de cobrança. Cobrança Movimento não foi encontrado");
				}
			} else {
				logProcessamento.append("\r\n");
				logProcessamento.append(
						"	(ATENÇÂO) Não foi possivel atualizar o movimento de cobrança. Documento de Cobrança não foi encontrado");
			}
		} else {
			logProcessamento.append("\r\n");
			logProcessamento.append(
					"	(ATENÇÂO) Não foi possivel atualizar o movimento de cobrança. Movimento de ocorrência não foi encontrado");
		}

		return recebimentos;

	}

	/**
	 * Obter nsa recebimento anterior.
	 *
	 * @param filtro the filtro
	 * @return the integer
	 * @throws NegocioException the negocio exception
	 */
	private Integer obterNSARecebimentoAnterior(Map<String, Object> filtro) throws NegocioException {

		if (filtro != null) {
			Collection<Recebimento> recebimentos = controladorRecebimentos.consultarRecebimento(filtro);
			if (!recebimentos.isEmpty() && recebimentos.iterator().next().getArrecadadorMovimentoItem() != null
					&& recebimentos.iterator().next().getArrecadadorMovimentoItem().getArrecadadorMovimento() != null) {
				return recebimentos.iterator().next().getArrecadadorMovimentoItem().getArrecadadorMovimento()
						.getNumeroNSA();
			}
		}

		return null;
	}

	/**
	 * Criar filtro recebimento anterior.
	 *
	 * @param recebimento the recebimento
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	private Map<String, Object> criarFiltroRecebimentoAnterior(Recebimento recebimento) throws NegocioException {

		Fatura faturaAtual = null;

		if (recebimento.getDocumentoCobrancaItem() != null) {
			faturaAtual = controladorDocumentoCobranca
					.consultarFaturaPorDocumentoCobrancaItem(recebimento.getDocumentoCobrancaItem().getChavePrimaria());
		} else if (recebimento.getFaturaGeral() != null) {
			faturaAtual = recebimento.getFaturaGeral().getFaturaAtual();
		}

		if (faturaAtual != null) {
			Map<String, Object> filtro = new HashMap<>();
			filtro.put("chavesFaturas", new Long[] { faturaAtual.getChavePrimaria() });
			EntidadeConteudo situacaoPaga = controladorEntidadeConteudo
					.obterEntidadeConteudoPorConstanteSistema(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);
			filtro.put("situacaoPagamentoFaturaAtual", situacaoPaga);
			return filtro;
		}

		return null;
	}

	/**
	 * Criar mensagem ocorrencia.
	 *
	 * @param recebimentoSituacao the recebimento situacao
	 * @param nsa                 the nsa
	 * @return the string
	 * @throws NegocioException the negocio exception
	 */
	private String criarMensagemOcorrencia(RecebimentoSituacao recebimentoSituacao, String nomeArquivo)
			throws NegocioException {

		Map<RecebimentoSituacao, String> mensagensRelatorio = montarMapaMensagensRelatorio(nomeArquivo);
		return mensagensRelatorio.get(recebimentoSituacao);
	}

	/**
	 * Montar mapa mensagens relatorio.
	 *
	 * @param nomeArquivo the nomeArquivo
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	private Map<RecebimentoSituacao, String> montarMapaMensagensRelatorio(String nomeArquivo) throws NegocioException {

		Map<String, String> mapaNomeConstantes = criarMapaNomeConstantes();
		return transformarConstantes(mapaNomeConstantes, nomeArquivo);
	}

	/**
	 * Transformar constantes.
	 *
	 * @param mapaNomeConstantes the mapa nome constantes
	 * @param nomeArquivo                the nomeArquivo
	 * @return the map
	 * @throws NegocioException the negocio exception
	 */
	private Map<RecebimentoSituacao, String> transformarConstantes(Map<String, String> mapaNomeConstantes, String nomeArquivo)
			throws NegocioException {

		Map<RecebimentoSituacao, String> mapaMensagensRelatorio = new HashMap<>();
		Set<Entry<String, String>> entradas = mapaNomeConstantes.entrySet();
		String mensagem;
		for (Entry<String, String> entrada : entradas) {
			mensagem = criarMensagemRelatorio(nomeArquivo, entrada);
			mapaMensagensRelatorio.put(obterRecebimentoSituacao(entrada.getKey()), mensagem);
		}
		return mapaMensagensRelatorio;
	}

	/**
	 * Criar mensagem relatorio.
	 *
	 * @param nomeArquivo     the nomeArquivo
	 * @param entrada the entrada
	 * @return the string
	 */
	private String criarMensagemRelatorio(String nomeArquivo, Entry<String, String> entrada) {

		String mensagem;
		if (Constantes.C_RECEBIMENTO_DUPLICIDADE_EXCESSO.equals(entrada.getKey())) {
			mensagem = MensagemUtil.obterMensagem(entrada.getValue(), nomeArquivo);
		} else {
			mensagem = MensagemUtil.obterMensagem(entrada.getValue());
		}
		return mensagem;
	}

	/**
	 * Criar mapa nome constantes.
	 *
	 * @return the map
	 */
	private Map<String, String> criarMapaNomeConstantes() {

		Map<String, String> mapaNomeConstantes = new HashMap<>();
		mapaNomeConstantes.put(Constantes.C_RECEBIMENTO_CLASSIFICADO, Constantes.MENSAGEM_RECEBIMENTO_CLASSIFICADO);
		mapaNomeConstantes.put(Constantes.C_RECEBIMENTO_VALOR_NAO_CONFERE,
				Constantes.MENSAGEM_RECEBIMENTO_VALOR_NAO_CONFERE);
		mapaNomeConstantes.put(Constantes.C_RECEBIMENTO_DUPLICIDADE_EXCESSO,
				Constantes.MENSAGEM_RECEBIMENTO_DUPLICIDADE_EXCESSO);
		mapaNomeConstantes.put(Constantes.C_RECEBIMENTO_DOCUMENTO_INEXISTENTE,
				Constantes.MENSAGEM_RECEBIMENTO_DOCUMENTO_INEXISTENTE);
		return mapaNomeConstantes;
	}

	/**
	 * Obter recebimento situacao.
	 *
	 * @param situacao the situacao
	 * @return the recebimento situacao
	 * @throws NegocioException the negocio exception
	 */
	private RecebimentoSituacao obterRecebimentoSituacao(String situacao) throws NegocioException {

		ConstanteSistema constanteSituacaoRecebimento = controladorConstanteSistema.obterConstantePorCodigo(situacao);
		Long valorConstante = Long.valueOf(constanteSituacaoRecebimento.getValor());
		return controladorRecebimentos.obterRecebimentoSituacao(valorConstante);
	}

	/**
	 * Obter data prevista para debito.
	 * 
	 * @param dataDebito                  the data debito
	 * @param arrecadadorContratoConvenio the arrecadador contrato convenio
	 * @return the date
	 */
	private Date obterDataPrevistaParaDebito(Date dataDebito, ArrecadadorContratoConvenio arrecadadorContratoConvenio) {

		/**
		 * Determina a data prevista para o crédito = Data do Débito + quantidade de
		 * dias de float (ACTF_NNDIAFLOAT da tabela ARRECADADOR_CONTRATO_TARIFA com
		 * ARCT_ID=ARCT_ID da tabela ARRECADADOR_CONTRATO com ARRC_ID=Campo A05 e
		 * ARCT_DTCONTRATOENCERRAMENTO com o valor nulo e a forma de arrecadação
		 * (ARFM_ID) com o valor correspondente a Débito Automático da tabela
		 * ARRECADACAO_FORMA);
		 */
		Integer diasFloat = arrecadadorContratoConvenio.getNumeroDiasFloat();
		Date dataPrevistaDebito = dataDebito;

		if (diasFloat != null) {
			Calendar calendar = GregorianCalendar.getInstance();
			calendar.setTime(dataPrevistaDebito);
			calendar.add(Calendar.DAY_OF_MONTH, diasFloat);
			dataPrevistaDebito = calendar.getTime();
		}

		return dataPrevistaDebito;
	}

	/**
	 * Obter sequencial aviso bancario.
	 * 
	 * @param arrecadador    the arrecadador
	 * @param dataLancamento the data lancamento
	 * @return the integer
	 * @throws NegocioException the negocio exception
	 */
	private Integer obterSequencialAvisoBancario(Arrecadador arrecadador, Date dataLancamento) throws NegocioException {

		Integer retorno = 0;

		Integer sequencialAvisoBancario = controladorArrecadacao.obterUltimoSequencialAvisoBancario(arrecadador,
				dataLancamento);
		if (sequencialAvisoBancario != null) {
			retorno = sequencialAvisoBancario + 1;
		}

		return retorno;
	}

	/**
	 * Processar linha.
	 * 
	 * @param tamanhoRegistro the tamanho registro
	 * @param linha           the linha
	 * @param linhaAuxiliar   the linha auxiliar
	 * @return the string
	 */
	private String processarLinha(int tamanhoRegistro, String linha, String linhaAuxiliar) {

		if (tamanhoRegistro == REGISTRO_MEDIO) {
			return linhaAuxiliar;
		}
		return linha;

	}

	/**
	 * Metodo para buscar a {@link EntidadeConteudo} relacionado ao tipo arrecadacao
	 * de acordo com a informacao vinda no arquivo carregado.
	 * 
	 * @param codigo - {@code String} que vem do arquivo
	 * @return {@code EntidadeConteudo} tipoArrecadacao
	 * @throws NegocioException
	 */
	private EntidadeConteudo obterTipoArrecacaoEntidadeConteudo(String codigo) throws NegocioException {
		ParametroSistema parametroCampoArquivoCodigoConvenio = controladorParametroSistema
				.obterParametroPorCodigo(Constantes.ARRECADACAO_CAMPO_ARQUIVO_CODIGO_CONVENIO);
		if (StringUtils.isNumeric(codigo.trim())) {
			return controladorDocumentoLayout.obterTipoConvenioPorCodigoConvenio(codigo);
		} else if (codigo.trim().equals(parametroCampoArquivoCodigoConvenio.getValor())) {
			return controladorDocumentoLayout.obterTipoConvenioPorCodigoConvenio(controladorParametroSistema
					.obterParametroPorCodigo(Constantes.ARRECADACAO_CODIGO_CONVENIO).getValor());
		}
		return null;
	}

	/**
	 * Metodo auxiliar para algumas validacoes de igualdade entre a
	 * chavePrimaria/Codigo do tipoArrecadacao com o {@code DocumentoLayout}.
	 * 
	 * 
	 * @param leiaute         {@code DocumentoLayout}
	 * @param tipoArrecadacao {@code EntidadeConteudo}
	 * @return boolean {@code true} se achou alguma igualdade
	 */
	private boolean verificarIgualdadeComChavePrimariaOuCodigoDoTipoArrecadacao(DocumentoLayout leiaute,
			EntidadeConteudo tipoArrecadacao) {
		boolean flagChavePrimariaIgual = leiaute.getFormaArrecadacao().getChavePrimaria() == tipoArrecadacao
				.getChavePrimaria();
		boolean flagCodigoIgual = !StringUtils.isBlank(tipoArrecadacao.getCodigo())
				&& (leiaute.getFormaArrecadacao().getChavePrimaria() == Long.parseLong(tipoArrecadacao.getCodigo()));
		return flagChavePrimariaIgual || flagCodigoIgual;
	}

	/**
	 * Método para popular o log de processamento com a descrição dos códigod de
	 * ocorrencia de erro
	 * 
	 * @param codigoErro       - codigo do erro que vem do leiaute
	 * @param ordemCodigoErro  - qual a ordem do codigo de erro (primeiro, segundo
	 *                         ou terceiro)
	 * @param logProcessamento - log do processamento
	 * @param documentoLayout  - {@code DocumentoLayoutRegistro}
	 */
	private void popularLogComCodigoOcorrenciaErro(String codigoErro, String ordemCodigoErro,
			StringBuilder logProcessamento, DocumentoLayoutRegistro documentoLayout) {
		DocumentoLayoutErroOcorrencia documentoLayoutErroOcorrencia = documentoLayout.obterErroOcorrencia(codigoErro);
		if (documentoLayoutErroOcorrencia != null) {
			logProcessamento.append("\r\n	");
			logProcessamento.append(ordemCodigoErro);
			logProcessamento.append(documentoLayoutErroOcorrencia.getCodigo());
			logProcessamento.append(" - ");
			logProcessamento.append(documentoLayoutErroOcorrencia.getDescricao());
		}
	}

	/**
	 * Atualizar Cobranca Movimento
	 * 
	 * @param movimentoCobranca                - {@link CobrancaBancariaMovimento}
	 * @param documentoLayoutRetornoOcorrencia -
	 *                                         {@link DocumentoLayoutRetornoOcorrencia}
	 * @throws ConcorrenciaException - the Concorrencia Exception
	 * @throws NegocioException      - the Negocio Exception
	 */
	private void atualizarCobrancaMovimento(CobrancaBancariaMovimento movimentoCobranca,
			DocumentoLayoutRetornoOcorrencia documentoLayoutRetornoOcorrencia)
			throws ConcorrenciaException, NegocioException {
		if (movimentoCobranca.getDataRetorno() == null) {
			movimentoCobranca.setDocumentoLayoutRetornoOcorrencia(documentoLayoutRetornoOcorrencia);
			movimentoCobranca.setDataRetorno(Calendar.getInstance().getTime());
			movimentoCobranca.setVersao(movimentoCobranca.getVersao() - 1);
			controladorArrecadacao.atualizar(movimentoCobranca);
		} else {
			movimentoCobranca.setVersao(movimentoCobranca.getVersao() - 1);
			movimentoCobranca.setHabilitado(false);
			controladorArrecadacao.atualizar(movimentoCobranca);

			CobrancaBancariaMovimento cobrancaAtualizada = (CobrancaBancariaMovimento) controladorArrecadacao
					.criarCobrancaBancariaMovimento();
			cobrancaAtualizada.setDocumentoCobranca(movimentoCobranca.getDocumentoCobranca());
			cobrancaAtualizada.setDocumentoLayoutRetornoOcorrencia(documentoLayoutRetornoOcorrencia);
			cobrancaAtualizada.setDataEnvio(movimentoCobranca.getDataEnvio());
			cobrancaAtualizada.setDataRetorno(Calendar.getInstance().getTime());
			cobrancaAtualizada.setDataProcessamento(movimentoCobranca.getDataProcessamento());
			cobrancaAtualizada.setVersao(movimentoCobranca.getVersao() + 1);
			cobrancaAtualizada.setArrecadadorContratoConvenio(movimentoCobranca.getArrecadadorContratoConvenio());
			cobrancaAtualizada.setDocumentoLayoutEnvioOcorrencia(movimentoCobranca.getDocumentoLayoutEnvioOcorrencia());
			controladorArrecadacao.inserir(cobrancaAtualizada);
		}

	}
}
