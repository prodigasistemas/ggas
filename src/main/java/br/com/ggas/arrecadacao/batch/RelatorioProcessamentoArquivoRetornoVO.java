/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.batch;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * Classe responsável pela representação do relatório do processamento do arquivo de retorno
 */
public class RelatorioProcessamentoArquivoRetornoVO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 8819329710855719145L;

	private String codigoBanco;

	private String nomeBanco;

	private String descricaoConvenio;

	private String documentoCobranca;

	private BigDecimal valorRecebimento;

	private Date dataRecebimento;

	private String descricaoOcorrencia;

	private int numeroLinha;

	private BigDecimal tarifa;

	private BigDecimal juros;

	private BigDecimal multa;

	private Date dataVencimento;

	private Date dataCredito;

	private boolean ocorrencia;

	private String pontoConsumo;

	/**
	 * Retorna codigoBanco
	 * 
	 * @return the codigoBanco
	 */
	public String getCodigoBanco() {

		return codigoBanco;
	}

	/**
	 * @param codigoBanco
	 *            the codigoBanco to set
	 */
	public void setCodigoBanco(String codigoBanco) {

		this.codigoBanco = codigoBanco;
	}

	/**
	 * Retorna nomeBanco
	 * 
	 * @return the nomeBanco
	 */
	public String getNomeBanco() {

		return nomeBanco;
	}

	/**
	 * @param nomeBanco
	 *            the nomeBanco to set
	 */
	public void setNomeBanco(String nomeBanco) {

		this.nomeBanco = nomeBanco;
	}

	/**
	 * Retorna descricaoConvenio
	 * 
	 * @return the descricaoConvenio
	 */
	public String getDescricaoConvenio() {

		return descricaoConvenio;
	}

	/**
	 * Atribui descricaoConvenio
	 * 
	 * @param descricaoConvenio the descricaoConvenio to set
	 */
	public void setDescricaoConvenio(String descricaoConvenio) {

		this.descricaoConvenio = descricaoConvenio;
	}

	/**
	 * Retorna documento cobrança
	 * 
	 * @return the documentoCobranca
	 */
	public String getDocumentoCobranca() {

		return documentoCobranca;
	}

	/**
	 * @param documentoCobranca
	 *            the documentoCobranca to set
	 */
	public void setDocumentoCobranca(String documentoCobranca) {

		this.documentoCobranca = documentoCobranca;
	}

	/**
	 * Retorna valorRecebimento
	 * 
	 * @return the valorRecebimento
	 */
	public BigDecimal getValorRecebimento() {

		return valorRecebimento;
	}

	/**
	 * @param valorRecebimento
	 *            the valorRecebimento to set
	 */
	public void setValorRecebimento(BigDecimal valorRecebimento) {

		this.valorRecebimento = valorRecebimento;
	}

	/**
	 * Retorna dataRecebimento
	 * 
	 * @return the dataRecebimento
	 */
	public Date getDataRecebimento() {
		Date data = null;
		if (this.dataRecebimento != null) {
			data = (Date) this.dataRecebimento.clone();
		}
		return data;
	}

	/**
	 * @param dataRecebimento
	 *            the dataRecebimento to set
	 */
	public void setDataRecebimento(Date dataRecebimento) {
		if(dataRecebimento != null){
			this.dataRecebimento = (Date) dataRecebimento.clone();
		} else {
			this.dataRecebimento = null;
		}
	}

	/**
	 * Retorna descricaoOcorrencia
	 * 
	 * @return the descricaoOcorrencia
	 */
	public String getDescricaoOcorrencia() {

		return descricaoOcorrencia;
	}

	/**
	 * @param descricaoOcorrencia
	 *            the descricaoOcorrencia to set
	 */
	public void setDescricaoOcorrencia(String descricaoOcorrencia) {

		this.descricaoOcorrencia = descricaoOcorrencia;
	}

	/**
	 * Retorna numerolinha
	 * 
	 * @return the numeroLinha
	 */
	public int getNumeroLinha() {

		return numeroLinha;
	}

	/**
	 * @param numeroLinha
	 *            the numeroLinha to set
	 */
	public void setNumeroLinha(int numeroLinha) {

		this.numeroLinha = numeroLinha;
	}
	
	/**
	 * Retorna valor da tarifa
	 * 
	 * @return o valor da tarifa
	 */
	public BigDecimal getTarifa() {

		return tarifa;
	}
	/**
	 * define o valor da tarifa
	 */
	public void setTarifa(BigDecimal tarifa) {

		this.tarifa = tarifa;
	}
	
	/**
	 * Retorna valor do juros
	 * 
	 * @return o valor do juros
	 */
	public BigDecimal getJuros() {

		return juros;
	}
	/**
	 * @param juros
	 * define os juros
	 */
	public void setJuros(BigDecimal juros) {

		this.juros = juros;
	}
	
	/**
	 * Retorna valor da multa
	 * 
	 * @return o valor da multa
	 */
	public BigDecimal getMulta() {

		return multa;
	}
	
	/**
	 * Atribui multa
	 */
	public void setMulta(BigDecimal multa) {

		this.multa = multa;
	}

	/**
	 * @return a data de vencimento
	 */
	public Date getDataVencimento() {
		Date data = null;
		if (dataVencimento != null) {
			data = (Date) dataVencimento.clone();
		}
		return data;
	}
	
	/**
	 * @param dataVencimento define a data de vencimento
	 */
	public void setDataVencimento(Date dataVencimento) {
		if (dataVencimento != null) {
			this.dataVencimento = (Date) dataVencimento.clone();
		} else {
			this.dataVencimento = null;
		}
	}
	
	/**
	 * @return a ocorrência
	 */
	public boolean getOcorrencia() {

		return ocorrencia;
	}
	
	/**
	 * Atribui ocorrencia
	 * 
	 * @param ocorrencia
	 */
	public void setOcorrencia(boolean ocorrencia) {

		this.ocorrencia = ocorrencia;
	}
	
	/**
	 * Retorna data de credito
	 * 
	 * @return data de crédito
	 */
	public Date getDataCredito() {
		Date data = null;
		if (this.dataCredito != null) {
			data = (Date) this.dataCredito.clone();
		}
		return data;
	}
	
	/**
	 * @param dataCredito
	 * define a data de crédito
	 */
	public void setDataCredito(Date dataCredito) {
		if(dataCredito != null){
			this.dataCredito = new Date(dataCredito.getTime());
		} else {
			this.dataCredito = null;
		}
	}
	
	/**
	 * Retorna ponto de consumo
	 * 
	 * @return o ponto de consumo
	 */
	public String getPontoConsumo() {

		return pontoConsumo;
	}
	/**
	 * @param pontoConsumo
	 * define o ponto de consumo
	 */
	public void setPontoConsumo(String pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

}
