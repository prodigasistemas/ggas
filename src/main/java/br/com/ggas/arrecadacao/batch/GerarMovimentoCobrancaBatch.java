/* 
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.batch;

import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;

/**
 * Classe responsável por gerar movimento de cobrança em batch
 */
@Component
public class GerarMovimentoCobrancaBatch implements Batch {

	private static final String PROCESSO = "processo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java. util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		StringBuilder logProcessamento = new StringBuilder();
		Processo processo = (Processo) parametros.get(PROCESSO);

		try {

			StringBuilder logProcessamentoCobrancaNaoRegistrada = null;
			StringBuilder logProcessamentoCobrancaRegistrada = null;

			ControladorArrecadacao controladorArrecadacao =
							(ControladorArrecadacao) ServiceLocator.getInstancia().getControladorNegocio(
											ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);

			ControladorConstanteSistema controladorConstanteSistema =
							(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
											ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			ControladorEntidadeConteudo controladorEntidadeConteudo =
							(ControladorEntidadeConteudo) ServiceLocator.getInstancia().getControladorNegocio(
											ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

			String codigoCobrancaNaoRegistrada =
							controladorConstanteSistema
											.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_NAO_COBRANCA_REGISTRADA);
			String codigoCobrancaRegistrada =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_COBRANCA_REGISTRADA);
			String codigoCobrancaRegistradaEscritural =
							controladorConstanteSistema
											.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_COBRANCA_REGISTRADA_ESCRITURAL);

			EntidadeConteudo cobrancaNaoRegistrada = controladorEntidadeConteudo.obter(Long.valueOf(codigoCobrancaNaoRegistrada));
			EntidadeConteudo cobrancaRegistrada = controladorEntidadeConteudo.obter(Long.valueOf(codigoCobrancaRegistrada));
			EntidadeConteudo cobrancaRegistradaEscritural =
							controladorEntidadeConteudo.obter(Long.valueOf(codigoCobrancaRegistradaEscritural));

			Collection<CobrancaBancariaMovimento> listaCobrancaNaoRegistradaMovimento =
							controladorArrecadacao.listarCobrancaBancariaMovimento(cobrancaNaoRegistrada);

			Collection<CobrancaBancariaMovimento> listaCobrancaRegistradaMovimento =
							controladorArrecadacao.listarCobrancaBancariaMovimento(cobrancaRegistrada);

			if (listaCobrancaRegistradaMovimento != null && !listaCobrancaRegistradaMovimento.isEmpty()) {
				listaCobrancaRegistradaMovimento.addAll(controladorArrecadacao
								.listarCobrancaBancariaMovimento(cobrancaRegistradaEscritural));
			} else {
				listaCobrancaRegistradaMovimento = controladorArrecadacao.listarCobrancaBancariaMovimento(cobrancaRegistradaEscritural);
			}

			if (listaCobrancaNaoRegistradaMovimento != null && !listaCobrancaNaoRegistradaMovimento.isEmpty()) {
				logProcessamento.append("\r\n");
				logProcessamento.append("Gerando Cobrança não registrada para ");
				logProcessamento.append(listaCobrancaNaoRegistradaMovimento.size());
				logProcessamento.append("\r\n");
				logProcessamentoCobrancaNaoRegistrada =
								controladorArrecadacao.gerarMovimentoCobrancaNaoRegistrada(listaCobrancaNaoRegistradaMovimento);
				logProcessamento.append(logProcessamentoCobrancaNaoRegistrada);
			} else {
				logProcessamento.append("\r\n");
				logProcessamento.append("Nenhum movimento de cobrança não registrada foi encontrado. ");
				logProcessamento.append("\r\n");
			}

			if (listaCobrancaRegistradaMovimento != null && !listaCobrancaRegistradaMovimento.isEmpty()) {
				logProcessamento.append("Gerando Cobrança registrada para ");
				logProcessamento.append(listaCobrancaRegistradaMovimento.size());
				logProcessamento.append("\r\n");
				logProcessamentoCobrancaRegistrada =
								controladorArrecadacao.gerarMovimentoCobrancaRegistrada(listaCobrancaRegistradaMovimento);
				logProcessamento.append(logProcessamentoCobrancaRegistrada);
			} else {
				logProcessamento.append("Nenhum movimento de cobrança registrada foi encontrado. ");
				logProcessamento.append("\r\n");
			}

		} catch (HibernateException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErroBytes(e));
			throw new HibernateException(e);
		} catch (NegocioException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErroBytes(e));
			throw new NegocioException(e);
		} catch (Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErroBytes(e));
			throw new GGASException(e);
		}

		return logProcessamento.toString();
	}

}
