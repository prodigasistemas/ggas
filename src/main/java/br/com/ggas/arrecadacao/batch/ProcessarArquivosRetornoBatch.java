/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.batch;

import br.com.ggas.arrecadacao.documento.ControladorDocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutCampo;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.Batch;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.PeriodicidadeProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Classe responsável por processar arquivos em um processo batch
 */
@Component
public class ProcessarArquivosRetornoBatch implements Batch {

	private static final int MEIO_MINUTO = 30;

	private static final String PROCESSO = "processo";

	private static final Logger LOG = Logger.getLogger(ProcessarArquivosRetornoBatch.class);

	private static final ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia()
					.getControladorEntidadeConteudo();

	private static final ControladorDocumentoLayout controladorDocumentoLayout = ServiceLocator.getInstancia()
					.getControladorDocumentoLayout();

	private static final ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia()
					.getControladorParametroSistema();

	private static final ControladorModulo controladorModulo = ServiceLocator.getInstancia().getControladorModulo();

	private static final ControladorProcesso controladorProcesso = (ControladorProcesso) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java. util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();

		try {

			class ArquivoOrdenado {

				private String codigoNsa;

				private File arquivo;
			}

			Processo processoPai = null;
			Processo processoFilho = null;
			Map<String, String> parametrosFilho = null;
			Operacao operacaoFilho = null;
			String diretorio = null;
			ArquivoOrdenado arquivoOrdenado = null;
			List<ArquivoOrdenado> arquivosProcessamento = new LinkedList<>();
			Scanner scanner = null;
			String linha = null;
			DateTime dataAgendamento = null;

			processoPai = (Processo) parametros.get(PROCESSO);
			diretorio =
							(String) controladorParametroSistema
											.obterValorDoParametroPorCodigo(ControladorParametroSistema.DIRETORIO_ARQUIVOS_RETORNO);

			if (StringUtils.isEmpty(diretorio)) {
				throw new GGASException("O diretório não foi informado.");
			}

			logProcessamento.append("Lendo os arquivos de retorno no diretório ").append(diretorio);
			logProcessamento.append("\r\n");

			File file = Util.getFile(diretorio);

			// Verifica se o diretório existe
			if (file.exists() && file.isDirectory()) {

				operacaoFilho = controladorModulo.buscarOperacaoPorRecurso(ProcessarArquivoRetornoBatch.class.getName()).iterator().next();

				if (operacaoFilho == null) {
					throw new GGASException("A operação não foi encontrada, recurso: " + ProcessarArquivoRetornoBatch.class.getName());
				}

				DadosAuditoria dadosAuditoria =
								Util.getDadosAuditoria(processoPai.getUsuario(), operacaoFilho, (String) controladorParametroSistema
												.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_IP));

				// Arquivos encontrados no diretório
				File[] arquivos = file.listFiles();
				if (arquivos != null) {
					// varrer array de arquivos
					for (File file_i : arquivos) {
						if (!file_i.isFile() && file_i.getName().contains("upload")) {
							File[] listaArquivos = file_i.listFiles();
							if (listaArquivos != null) {
								for (File arq : listaArquivos) {
									if (arq.isFile()) {

										arquivoOrdenado = new ArquivoOrdenado();
										arquivoOrdenado.arquivo = arq;

										logProcessamento.append("\r\n");
										logProcessamento.append("Verificando NSA do arquivo: ");
										logProcessamento.append(arquivoOrdenado.arquivo.getName());
										logProcessamento.append("\r\n");

										try {
											scanner = new Scanner(arquivoOrdenado.arquivo);
											if (scanner.hasNext()) {
												linha = scanner.nextLine();
												String codigoNsa = deduzirNSA(linha, logProcessamento);
												if (codigoNsa != null && !StringUtils.isEmpty(codigoNsa)) {
													arquivoOrdenado.codigoNsa = codigoNsa;
													arquivosProcessamento.add(arquivoOrdenado);
												}
											} else {
												logProcessamento.append("Arquivo inválido.");
												logProcessamento.append("\r\n");
											}
										} catch (FileNotFoundException e) {
											throw new GGASException(e);
										} finally {
											scanner.close();
										}
									}
								}
							}
						}
					}
				}
				// Ordenado os processos de acordo com o NSA
				Collections.sort(arquivosProcessamento, new Comparator<ArquivoOrdenado>(){

					@Override
					public int compare(ArquivoOrdenado arquivoOrdenado1, ArquivoOrdenado arquivoOrdenado2) {

						int retorno = 0;
						if (!StringUtils.isEmpty(arquivoOrdenado1.codigoNsa) && !StringUtils.isEmpty(arquivoOrdenado2.codigoNsa)) {
							retorno = Integer.valueOf(arquivoOrdenado1.codigoNsa).compareTo(Integer.valueOf(arquivoOrdenado2.codigoNsa));
						}
						return retorno;
					}
				});

				if (arquivosProcessamento != null && !arquivosProcessamento.isEmpty()) {
					logProcessamento.append("\r\n");
					logProcessamento.append("Ordenando arquivos por NSA. \r\n");
				}

				// Criando os processos dos arquivos
				dataAgendamento = new DateTime(Calendar.getInstance().getTime());
				dataAgendamento = dataAgendamento.plusSeconds(MEIO_MINUTO);

				for (ArquivoOrdenado arquivoOrdenadoProcesso : arquivosProcessamento) {

					processoFilho = (Processo) controladorProcesso.criar();
					processoFilho.setDescricao(operacaoFilho.getDescricao() + " - " + arquivoOrdenadoProcesso.arquivo.getName() + " NSA: "
									+ arquivoOrdenadoProcesso.codigoNsa);
					processoFilho.setDataInicioAgendamento(dataAgendamento.toDate());
					processoFilho.setDiaNaoUtil(processo.isDiaNaoUtil());
					processoFilho.setEmailResponsavel(processo.getEmailResponsavel());
					processoFilho.setHabilitado(true);
					processoFilho.setOperacao(operacaoFilho);
					processoFilho.setPeriodicidade(PeriodicidadeProcesso.SEM_PERIODICIDADE);
					processoFilho.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
					processoFilho.setUsuario(processo.getUsuario());

					parametrosFilho = new HashMap<>();
					String nomeArquivoProcesso = arquivoOrdenadoProcesso.arquivo.getPath();
					parametrosFilho.put("arquivoRetorno", nomeArquivoProcesso);

					processoFilho.setParametros(parametrosFilho);
					processoFilho.setDadosAuditoria(dadosAuditoria);
					processoFilho.setVersao(0);

					adicionarDescricaoChaveProcesso(logProcessamento, controladorProcesso, processoFilho, operacaoFilho);

					logProcessamento.append("Enviando arquivo ");
					logProcessamento.append(arquivoOrdenadoProcesso.arquivo.getName());
					logProcessamento.append(" NSA: ");
					logProcessamento.append(arquivoOrdenadoProcesso.codigoNsa);
					logProcessamento.append(" para ser processado.");
					logProcessamento.append("\r\n");

				}

			} else {
				throw new GGASException("O diretório " + diretorio + " não foi encontrado.");
			}

		} catch (HibernateException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErroBytes(e));
			throw new HibernateException(e);
		} catch (NegocioException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErroBytes(e));
			throw new NegocioException(e);
		} catch (GGASException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErroBytes(e));
			throw new GGASException(e);
		} catch (Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErroBytes(e));
			throw new GGASException(e);
		}

		return logProcessamento.toString();
	}

	/**
	 * Adicionar descricao chave processo.
	 *
	 * @param logProcessamento the log processamento
	 * @param controladorProcesso the controlador processo
	 * @param processoFilho the processo filho
	 * @param operacaoFilho the operacao filho
	 */
	private void adicionarDescricaoChaveProcesso(StringBuilder logProcessamento, ControladorProcesso controladorProcesso,
					Processo processoFilho, Operacao operacaoFilho) {

		try {
			Long chave = controladorProcesso.inserir(processoFilho);
			logProcessamento.append(operacaoFilho.getDescricao());
			logProcessamento.append(" - Chave do processo: ");
			logProcessamento.append(chave);
			logProcessamento.append("\r\n");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Deduzir nsa.
	 *
	 * @param linha the linha
	 * @param logProcessamento the log processamento
	 * @return the string
	 * @throws NegocioException the negocio exception
	 */
	private String deduzirNSA(String linha, StringBuilder logProcessamento) throws NegocioException {

		DocumentoLayout filtro = controladorDocumentoLayout.criarLeiauteFiltro();

		filtro.setTamanhoRegistro(linha.length());
		filtro.getConvenios().iterator().next().getArrecadadorContrato().getArrecadador().getBanco()
						.setCodigoBanco(deduzirCodigoBanco(linha, logProcessamento));
		filtro.getRegistros().iterator().next().setMetodoProcessamento("processarCabecalho");

		if (filtro.getConvenios().iterator().next().getArrecadadorContrato().getArrecadador().getBanco().getCodigoBanco() != null) {
			logProcessamento.append("Código do banco localizado no arquivo: ");
			logProcessamento.append(filtro.getConvenios().iterator().next().getArrecadadorContrato().getArrecadador().getBanco()
							.getCodigoBanco());
			logProcessamento.append("\r\n");
		}

		DocumentoLayoutCampo campoNSA =
						filtrarCampoPorEntidade(controladorDocumentoLayout.obterDocumentosLayout(filtro),
										controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.SEQUENCIAL_NSA),
										logProcessamento);

		if (campoNSA != null) {
			logProcessamento.append("NSA do arquivo localizado.");
			logProcessamento.append("\r\n");

			return Util.obterValorPosicao(linha, new int[] {
															campoNSA.getPosicaoInicial(), campoNSA.getPosicaoFinal()});
		} else {
			logProcessamento.append("NSA do arquivo não localizado.");
			logProcessamento.append("\r\n");
		}

		return null;

	}

	/**
	 * Deduzir codigo banco.
	 *
	 * @param linha the linha
	 * @param logProcessamento the log processamento
	 * @return the string
	 * @throws NegocioException the negocio exception
	 */
	private String deduzirCodigoBanco(String linha, StringBuilder logProcessamento) throws NegocioException {

		DocumentoLayout filtro = controladorDocumentoLayout.criarLeiauteFiltro();

		filtro.setTamanhoRegistro(linha.length());
		EntidadeConteudo entidadeConteudo;
		if ("707".equals(Constantes.CODIGO_BANCO)) {
			entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema("341");
		} else {
			entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.CODIGO_BANCO);
		}
		DocumentoLayoutCampo campoCodigoBanco =
						filtrarCampoPorEntidade(controladorDocumentoLayout.obterDocumentosLayout(filtro), entidadeConteudo, logProcessamento);

		if (campoCodigoBanco != null) {
			return Util.obterValorPosicao(linha, new int[] {
															campoCodigoBanco.getPosicaoInicial(), campoCodigoBanco.getPosicaoFinal()});
		}

		return null;

	}

	/**
	 * Filtrar campo por entidade.
	 *
	 * @param leiautes the leiautes
	 * @param entidade the entidade
	 * @param logProcessamento the log processamento
	 * @return the documento layout campo
	 */
	private DocumentoLayoutCampo filtrarCampoPorEntidade(Collection<DocumentoLayout> leiautes, EntidadeConteudo entidade,
					StringBuilder logProcessamento) {

		DocumentoLayoutCampo campoEntidade = null;

		for (DocumentoLayout leiaute : leiautes) {
			campoEntidade = filtrarCampoPorEntidade(leiaute, entidade);
			if (campoEntidade != null) {
				break;
			}
		}

		if (campoEntidade == null) {
			logProcessamento.append("Leiaute não localizado para processamento do campo: ");
			logProcessamento.append(entidade.getDescricao());
			logProcessamento.append("\r\n");
		}

		return campoEntidade;
	}

	/**
	 * Filtrar campo por entidade.
	 *
	 * @param leiaute the leiaute
	 * @param entidade the entidade
	 * @return the documento layout campo
	 */
	private DocumentoLayoutCampo filtrarCampoPorEntidade(DocumentoLayout leiaute, EntidadeConteudo entidade) {

		DocumentoLayoutCampo campoEntidade = null;

		for (DocumentoLayoutRegistro registro : leiaute.getRegistros()) {
			for (DocumentoLayoutCampo campo : registro.getCampos()) {
				if (entidade.equals(campo.getCampo())) {
					campoEntidade = campo;
					break;
				}
			}
		}

		return campoEntidade;

	}
}
