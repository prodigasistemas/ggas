/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * The Interface ControladorBanco.
 */
public interface ControladorBanco extends ControladorNegocio {

	/** The bean id controlador banco. */
	String BEAN_ID_CONTROLADOR_BANCO = "controladorBanco";

	/** The erro negocio banco existente. */
	String ERRO_NEGOCIO_BANCO_EXISTENTE = "ERRO_NEGOCIO_BANCO_EXISTENTE";

	/** The erro negocio arquivo nao encontrado. */
	String ERRO_NEGOCIO_ARQUIVO_NAO_ENCONTRADO = "ERRO_NEGOCIO_ARQUIVO_NAO_ENCONTRADO";

	/** The erro negocio arquivo. */
	String ERRO_NEGOCIO_ARQUIVO = "ERRO_NEGOCIO_ARQUIVO";

	/** The erro negocio tamanho maximo arquivo. */
	String ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO = "ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO";

	/** The erro negocio tipo arquivo. */
	String ERRO_NEGOCIO_TIPO_ARQUIVO = "ERRO_NEGOCIO_TIPO_ARQUIVO";

	/** The erro negocio nome banco existente. */
	String ERRO_NEGOCIO_NOME_BANCO_EXISTENTE = "ERRO_NEGOCIO_NOME_BANCO_EXISTENTE";

	/** The erro negocio nome banco vazio. */
	String ERRO_NEGOCIO_NOME_BANCO_VAZIO = "ERRO_NEGOCIO_NOME_BANCO_VAZIO";

	/** The erro negocio codigo banco existente. */
	String ERRO_NEGOCIO_CODIGO_BANCO_EXISTENTE = "ERRO_NEGOCIO_CODIGO_BANCO_EXISTENTE";

	/** The erro negocio codigo banco vazio. */
	String ERRO_NEGOCIO_CODIGO_BANCO_VAZIO = "ERRO_NEGOCIO_CODIGO_BANCO_VAZIO";
	
	/** Erro negocio banco nao suportado. */
	String ERRO_NEGOCIO_BANCO_NAO_SUPORTADO = "ERRO_NEGOCIO_BANCO_NAO_SUPORTADO";

	/**
	 * Consultar banco.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	public Collection<Banco> consultarBanco(Map<String, Object> filtro);

	/**
	 * Listar banco.
	 * 
	 * @return the collection
	 */
	public Collection<Banco> listarBanco();

	/**
	 * Validar existente.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void validarExistente(Map<String, Object> filtro) throws NegocioException;

}
