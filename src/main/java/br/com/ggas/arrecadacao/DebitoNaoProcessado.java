/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.io.Serializable;

/**
 * Interface responsável pela definição de débito não processado.
 */
public interface DebitoNaoProcessado extends Serializable {

	String BEAN_ID_DEBITO_NAO_PROCESSADO = "debitoNaoProcessado";

	/**
	 * @return the identificacaoClienteEmpresa
	 */
	String getIdentificacaoClienteEmpresa();

	/**
	 * @param identificacaoClienteEmpresa
	 *            the identificacaoClienteEmpresa
	 *            to set
	 */
	void setIdentificacaoClienteEmpresa(String identificacaoClienteEmpresa);

	/**
	 * @return the agenciaDebito
	 */
	String getAgenciaDebito();

	/**
	 * @param agenciaDebito
	 *            the agenciaDebito to set
	 */
	void setAgenciaDebito(String agenciaDebito);

	/**
	 * @return the identificacaoClienteBanco
	 */
	String getIdentificacaoClienteBanco();

	/**
	 * @param identificacaoClienteBanco
	 *            the identificacaoClienteBanco to
	 *            set
	 */
	void setIdentificacaoClienteBanco(String identificacaoClienteBanco);

	/**
	 * @return the ocorrencia1
	 */
	String getOcorrencia1();

	/**
	 * @param ocorrencia1
	 *            the ocorrencia1 to set
	 */
	void setOcorrencia1(String ocorrencia1);

	/**
	 * @return the ocorrencia2
	 */
	String getOcorrencia2();

	/**
	 * @param ocorrencia2
	 *            the ocorrencia2 to set
	 */
	void setOcorrencia2(String ocorrencia2);

	/**
	 * @return the reservadoFuturo
	 */
	String getReservadoFuturo();

	/**
	 * @param reservadoFuturo
	 *            the reservadoFuturo to set
	 */
	void setReservadoFuturo(String reservadoFuturo);

	/**
	 * @return the codigoMovimento
	 */
	String getCodigoMovimento();

	/**
	 * @param codigoMovimento
	 *            the codigoMovimento to set
	 */
	void setCodigoMovimento(String codigoMovimento);

}
