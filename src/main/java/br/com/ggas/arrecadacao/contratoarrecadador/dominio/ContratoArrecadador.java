/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.contratoarrecadador.dominio;

/**
 @since 15/07/2014 09:51:00
 @author rfilho
 */

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responśavel pela representação do contrato arrecadador
 */
public class ContratoArrecadador extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = -7531002399806955376L;

	private static final String ARRECADADOR = "Arrecadador";

	private static final String NUMERO_CONTRATO = "Número Contrato";

	private static final String DATA_INICIO = "Data Início";

	private Arrecadador arrecadador;

	private String numeroContrato;

	private Date dataInicio;

	private Date dataFim;

	public Arrecadador getArrecadador() {

		return arrecadador;
	}

	public void setArrecadador(Arrecadador arrecadador) {

		this.arrecadador = arrecadador;
	}

	public String getNumeroContrato() {

		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	public Date getDataInicio() {
		Date data = null;
		if(dataInicio != null){
			data = (Date) dataInicio.clone();
		} 
		return data;
	}

	public void setDataInicio(Date dataInicio) {
		if(dataInicio != null){
			this.dataInicio = (Date) dataInicio.clone();
		} else {
			this.dataInicio = null;
		}
	}

	public Date getDataFim() {
		Date data = null;
		if(dataFim != null){
			data = (Date) dataFim.clone();
		} 
		return data;
	}

	public void setDataFim(Date dataFim) {
		if(dataFim != null){
			this.dataFim = (Date) dataFim.clone();
		} else {
			this.dataFim = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();

		String camposObrigatorios = null;
		if(arrecadador == null) {
			stringBuilder.append(ARRECADADOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(numeroContrato == null || numeroContrato.isEmpty()) {
			stringBuilder.append(NUMERO_CONTRATO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(dataInicio == null) {
			stringBuilder.append(DATA_INICIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

}
