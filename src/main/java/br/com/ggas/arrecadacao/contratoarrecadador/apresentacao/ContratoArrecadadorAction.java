/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/06/2014 19:02:22
 @author rfilho
 */

package br.com.ggas.arrecadacao.contratoarrecadador.apresentacao;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ControladorArrecadador;
import br.com.ggas.arrecadacao.contratoarrecadador.dominio.ContratoArrecadador;
import br.com.ggas.arrecadacao.contratoarrecadador.dominio.ContratoArrecadadorVO;
import br.com.ggas.arrecadacao.contratoarrecadador.negocio.ControladorContratoArrecadador;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Controller relacionado as telas de contrato arrecadador
 */
@Controller
public class ContratoArrecadadorAction extends GenericAction {

	private static final String CONTRATO_ARRECADADOR_ATRIBUTO = "contratoArrecadador";

	private static final String LISTA_ARRECADADOR = "listaArrecadador";

	private static final String TELA_EXIBIR_PESQUISA_CONTRATO_ARRECADADOR = "exibirPesquisaContratoArrecadador";

	private static final Logger LOG = Logger.getLogger(ContratoArrecadadorAction.class);

	private static final String CONTRATO_ARRECADADOR = "Contrato Arecadador";

	@Autowired
	@Qualifier("controladorArrecadador")
	private ControladorArrecadador controladorArrecadador;

	@Autowired
	private ControladorContratoArrecadador controladorContratoArrecadador;

	/**
	 * Exibir pesquisa contrato arrecadador.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_CONTRATO_ARRECADADOR)
	public ModelAndView exibirPesquisaContratoArrecadador(HttpSession sessao) throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_CONTRATO_ARRECADADOR);

		carregarCampos(model, sessao);

		return model;

	}

	/**
	 * Carregar campos.
	 * 
	 * @param model
	 *            the model
	 * @param sessao
	 *            the sessao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void carregarCampos(ModelAndView model, HttpSession sessao) throws NegocioException {

		Collection<Arrecadador> lista = (Collection<Arrecadador>) controladorArrecadador.obterTodas(true);
		model.addObject(LISTA_ARRECADADOR, lista);
		sessao.setAttribute(LISTA_ARRECADADOR, lista);

	}

	/**
	 * Pesquisar contrato arrecadador.
	 * 
	 * @param contratoArrecadadorVO
	 *            the contrato arrecadador vo
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("pesquisarContratoArrecadador")
	public ModelAndView pesquisarContratoArrecadador(@ModelAttribute("ContratoArrecadadorVO") ContratoArrecadadorVO contratoArrecadadorVO,
					HttpSession sessao) throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_CONTRATO_ARRECADADOR);
		carregarCampos(model, sessao);
		Collection<ContratoArrecadador> listaContratoArrecadadores = null;
		try {
			listaContratoArrecadadores = controladorContratoArrecadador.consultarControladorArrecadadores(contratoArrecadadorVO);
		} catch(GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		model.addObject("listaContratoArrecadadores", listaContratoArrecadadores);

		return model;

	}

	/**
	 * Incluir contrato arrecadador.
	 * 
	 * @param contratoArrecadador
	 *            the contrato arrecadador
	 * @param chaveArrecadador
	 *            the chave arrecadador
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFim
	 *            the data fim
	 * @param result
	 *            the result
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("incluirContratoArrecadador")
	public ModelAndView incluirContratoArrecadador(@ModelAttribute("ContratoArrecadador") ContratoArrecadador contratoArrecadador,
					@RequestParam("chaveArrecadador") Long chaveArrecadador, @RequestParam("dataInicial") String dataInicial,
					@RequestParam("dataFinal") String dataFim, BindingResult result, HttpSession sessao) {

		ModelAndView model = new ModelAndView("forward:/pesquisarContratoArrecadador");

		try {
			popularContratoArrecadador(contratoArrecadador, chaveArrecadador, dataInicial, dataFim);

			controladorContratoArrecadador.inserirContratoArrecadador(contratoArrecadador);
			ContratoArrecadadorVO c = new ContratoArrecadadorVO();
			c.setNumeroContrato(contratoArrecadador.getNumeroContrato());
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, CONTRATO_ARRECADADOR);
			model.addObject("contratoArrecadadorVO", c);
		} catch(GGASException e) {
			model = new ModelAndView("exibirInclusaoContratoArrecadador");
			model.addObject(CONTRATO_ARRECADADOR_ATRIBUTO, contratoArrecadador);
			model.addObject("acao", "incluir");
			mensagemErroParametrizado(model, e);
		}
		return model;
	}

	/**
	 * Popular contrato arrecadador.
	 * 
	 * @param contratoArrecadador
	 *            the contrato arrecadador
	 * @param chaveArrecadador
	 *            the chave arrecadador
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFim
	 *            the data fim
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularContratoArrecadador(ContratoArrecadador contratoArrecadador, Long chaveArrecadador, String dataInicial,
					String dataFim) throws GGASException {

		Arrecadador arrecadador = null;
		Date dataInicio = null;
		Date dataFinal = null;
		if(chaveArrecadador != null) {
			arrecadador = (Arrecadador) controladorArrecadador.obter(chaveArrecadador);
		}
		contratoArrecadador.setArrecadador(arrecadador);

		if(dataInicial != null && !dataInicial.isEmpty()) {
			dataInicio = Util.converterCampoStringParaData("Data Início", dataInicial, Constantes.FORMATO_DATA_BR);
		}
		if(dataFim != null && !dataFim.isEmpty()) {
			dataFinal = Util.converterCampoStringParaData("Data Fim", dataFim, Constantes.FORMATO_DATA_BR);
		}

		contratoArrecadador.setDataInicio(dataInicio);
		contratoArrecadador.setDataFim(dataFinal);
	}

	/**
	 * Exibir inclusao contrato arrecadador.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirInclusaoContratoArrecadador")
	public ModelAndView exibirInclusaoContratoArrecadador(HttpSession sessao) throws NegocioException {

		ModelAndView model = new ModelAndView("exibirInclusaoContratoArrecadador");

		ContratoArrecadador contratoArrecadador = new ContratoArrecadador();

		model.addObject(CONTRATO_ARRECADADOR_ATRIBUTO, contratoArrecadador);
		model.addObject(LISTA_ARRECADADOR, controladorArrecadador.obterTodas(true));
		sessao.setAttribute(LISTA_ARRECADADOR, controladorArrecadador.obterTodas(true));
		model.addObject("acao", "incluir");

		return model;
	}

	/**
	 * Exibir alteracao contrato arrecadador.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirAlteracaoContratoArrecadador")
	public ModelAndView exibirAlteracaoContratoArrecadador(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao)
					throws NegocioException {

		ContratoArrecadador contratoArrecadador = null;

		ModelAndView model = new ModelAndView("exibirAlteracaoContratoArrecadador");

		contratoArrecadador = controladorContratoArrecadador.obterContratoArrecadador(chavePrimaria);

		model.addObject(CONTRATO_ARRECADADOR_ATRIBUTO, contratoArrecadador);
		carregarCampos(model, sessao);
		model.addObject("acao", "alterar");

		return model;
	}

	/**
	 * Alterar contrato arrecadador.
	 * 
	 * @param contratoArrecadador
	 *            the contrato arrecadador
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param chaveArrecadador
	 *            the chave arrecadador
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFim
	 *            the data fim
	 * @param session
	 *            the session
	 * @return the model and view
	 */
	@RequestMapping("alterarContratoArrecadador")
	public ModelAndView alterarContratoArrecadador(@ModelAttribute("ContratoArrecadador") ContratoArrecadador contratoArrecadador,
					@RequestParam("chavePrimaria") Long chavePrimaria, @RequestParam("chaveArrecadador") Long chaveArrecadador,
					@RequestParam("dataInicial") String dataInicial, @RequestParam("dataFinal") String dataFim, HttpSession session) {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_CONTRATO_ARRECADADOR);

		try {
			ContratoArrecadador contratoArrec = controladorContratoArrecadador.obterContratoArrecadador(chavePrimaria);
			popularContratoArrecadador(contratoArrecadador, chaveArrecadador, dataInicial, dataFim);
			contratoArrecadador.setVersao(contratoArrec.getVersao());
			controladorContratoArrecadador.alterarContratoArrecadador(contratoArrecadador);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, CONTRATO_ARRECADADOR);
		} catch(GGASException e) {
			model = new ModelAndView("exibirAlteracaoContratoArrecadador");
			model.addObject(CONTRATO_ARRECADADOR_ATRIBUTO, contratoArrecadador);
			mensagemErroParametrizado(model, e);
			model.addObject("acao", "alterar");
		}

		return model;

	}

	/**
	 * Exibir detalhar contrato arrecadador.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("exibirDetalharContratoArrecadador")
	public ModelAndView exibirDetalharContratoArrecadador(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao) {

		ModelAndView model = new ModelAndView("exibirDetalharContratoArrecadador");
		ContratoArrecadador contratoArrecadador = null;
		try {
			contratoArrecadador = controladorContratoArrecadador.obterContratoArrecadador(chavePrimaria);

			model.addObject(CONTRATO_ARRECADADOR_ATRIBUTO, contratoArrecadador);
			model.addObject("acao", "detalhar");
		} catch(NegocioException e) {
			LOG.error(e.getMessage(), e);
		}

		return model;
	}

	/**
	 * Remover contrato arrecadador.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	@RequestMapping("removerContratoArrecadador")
	public ModelAndView removerContratoArrecadador(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request) {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_CONTRATO_ARRECADADOR);

		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());
			controladorContratoArrecadador.removerContratoArrecadador(chavesPrimarias, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, CONTRATO_ARRECADADOR);
		} catch(NegocioException e) {
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

}

