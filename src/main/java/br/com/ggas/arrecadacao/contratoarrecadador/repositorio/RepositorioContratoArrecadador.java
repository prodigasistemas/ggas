/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 16:32:49
 @author rfilho
 */

package br.com.ggas.arrecadacao.contratoarrecadador.repositorio;

import java.util.Collection;
import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.arrecadacao.contratoarrecadador.dominio.ContratoArrecadador;
import br.com.ggas.arrecadacao.contratoarrecadador.dominio.ContratoArrecadadorVO;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Repositório responsável pelo contrato arrecadador
 */
@Repository
public class RepositorioContratoArrecadador extends RepositorioGenerico {

	private static final String ARRECADADOR = "arrecadador";
	private static final Integer ZERO = 00;
	/**
	 * Instantiates a new repositorio contrato arrecadador.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioContratoArrecadador(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new ContratoArrecadador();
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ContratoArrecadador.class;
	}

	/**
	 * Consultar controlador arrecadadores.
	 * 
	 * @param contratoArrecadadorVO
	 *            the contrato arrecadador vo
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Collection<ContratoArrecadador> consultarControladorArrecadadores(ContratoArrecadadorVO contratoArrecadadorVO)
					throws GGASException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode(ARRECADADOR, FetchMode.JOIN);

		criteria.createAlias(ARRECADADOR, ARRECADADOR, Criteria.LEFT_JOIN);

		if(contratoArrecadadorVO.getChaveArrecadador() != null && contratoArrecadadorVO.getChaveArrecadador() > 0) {
			criteria.add(Restrictions.eq("arrecadador.chavePrimaria", contratoArrecadadorVO.getChaveArrecadador()));
		}

		if(contratoArrecadadorVO.getNumeroContrato() != null && !contratoArrecadadorVO.getNumeroContrato().isEmpty()) {
			criteria.add(Restrictions.like("numeroContrato", contratoArrecadadorVO.getNumeroContrato()));
		}

		DateTime dataInicioNova;
		DateTime dataFimNova;
		if(contratoArrecadadorVO.getDataInicio() != null 
						&& Util.isDataValida(contratoArrecadadorVO.getDataInicio(), Constantes.FORMATO_DATA_BR)) {
			Date dataInicio = Util.converterCampoStringParaData("", contratoArrecadadorVO.getDataInicio(), Constantes.FORMATO_DATA_BR);
			Date data = Util.decrementarDataComQuantidadeDias(dataInicio, 1);
			dataInicioNova = new DateTime(data);
			dataInicioNova = dataInicioNova.withHourOfDay(ZERO);
			dataInicioNova = dataInicioNova.withMinuteOfHour(ZERO);
			dataInicioNova = dataInicioNova.withSecondOfMinute(ZERO);

			criteria.add(Restrictions.gt("dataInicio", dataInicioNova.toDate()));

		}

		if(contratoArrecadadorVO.getDataFim() != null && Util.isDataValida(contratoArrecadadorVO.getDataFim(), Constantes.FORMATO_DATA_BR)) {
			Date dataFim = Util.converterCampoStringParaData("", contratoArrecadadorVO.getDataFim(), Constantes.FORMATO_DATA_BR);
			Date data = Util.incrementarDataComQuantidadeDias(dataFim, 1);

			dataFimNova = new DateTime(data);
			dataFimNova = dataFimNova.withHourOfDay(ZERO);
			dataFimNova = dataFimNova.withMinuteOfHour(ZERO);
			dataFimNova = dataFimNova.withSecondOfMinute(ZERO);

			criteria.add(Restrictions.lt("dataFim", dataFimNova.toDate()));

		}
		if(contratoArrecadadorVO.getHabilitado() != null && !contratoArrecadadorVO.getHabilitado().isEmpty()) {

			criteria.add(Restrictions.eq("habilitado", Boolean.valueOf(contratoArrecadadorVO.getHabilitado())));
		}

		return criteria.list();
	}
}
