/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 16:24:02
 @author rfilho
 */

package br.com.ggas.arrecadacao.contratoarrecadador.negocio.impl;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.arrecadacao.contratoarrecadador.dominio.ContratoArrecadador;
import br.com.ggas.arrecadacao.contratoarrecadador.dominio.ContratoArrecadadorVO;
import br.com.ggas.arrecadacao.contratoarrecadador.negocio.ControladorContratoArrecadador;
import br.com.ggas.arrecadacao.contratoarrecadador.repositorio.RepositorioContratoArrecadador;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pelas ações relacionadas ao contrato arrecadador
 */
@Service("controladorContratoArrecadadorImpl")
@Transactional
public class ControladorContratoArrecadadorImpl implements ControladorContratoArrecadador {

	@Autowired
	private RepositorioContratoArrecadador repositorioContratoArrecadador;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.contratoarrecadador.negocio.ControladorContratoArrecadador#consultarControladorArrecadadores(br.com.ggas.
	 * arrecadacao
	 * .contratoarrecadador.dominio.ContratoArrecadadorVO)
	 */
	@Override
	public Collection<ContratoArrecadador> consultarControladorArrecadadores(ContratoArrecadadorVO contratoArrecadadorVO)
					throws GGASException {

		if(contratoArrecadadorVO.getDataInicio() != null && !contratoArrecadadorVO.getDataInicio().isEmpty() && 
						contratoArrecadadorVO.getDataFim() != null && !contratoArrecadadorVO.getDataFim().isEmpty()) {
			Date dataInicio = Util.converterCampoStringParaData("Data Inicio", contratoArrecadadorVO.getDataInicio(),
							Constantes.FORMATO_DATA_BR);
			Date dataFinal = Util.converterCampoStringParaData("Data Final", contratoArrecadadorVO.getDataFim(),
							Constantes.FORMATO_DATA_BR);
			validarDataContratoArrecadador(dataInicio, dataFinal);
		}
		return repositorioContratoArrecadador.consultarControladorArrecadadores(contratoArrecadadorVO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.contratoarrecadador.negocio.ControladorContratoArrecadador#inserirContratoArrecadador(br.com.ggas.arrecadacao
	 * .
	 * contratoarrecadador.dominio.ContratoArrecadador)
	 */
	@Override
	public void inserirContratoArrecadador(ContratoArrecadador contratoArrecadador) throws GGASException {

		validarInserirAtualizar(contratoArrecadador);
		repositorioContratoArrecadador.inserir(contratoArrecadador);
	}

	/**
	 * Validar inserir atualizar.
	 * 
	 * @param contratoArrecadador
	 *            the contrato arrecadador
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void validarInserirAtualizar(ContratoArrecadador contratoArrecadador) throws GGASException {

		Collection<ContratoArrecadador> contratosArrecadadores = consultarControladorArrecadadorIgual(contratoArrecadador);
		if(!CollectionUtils.isEmpty(contratosArrecadadores)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CONTRATO_ARRECADADOR_NUMERO_CONTRATO_EXISTENTE_ARRECADADOR, true);
		}
		validarDataContratoArrecadador(contratoArrecadador.getDataInicio(), contratoArrecadador.getDataFim());
	}

	/**
	 * Consultar controlador arrecadador igual.
	 * 
	 * @param contratoArrecadador
	 *            the contrato arrecadador
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<ContratoArrecadador> consultarControladorArrecadadorIgual(ContratoArrecadador contratoArrecadador)
					throws GGASException {

		ContratoArrecadadorVO contratoArrecadadorVO = new ContratoArrecadadorVO();
		contratoArrecadadorVO.setNumeroContrato(contratoArrecadador.getNumeroContrato());
		contratoArrecadadorVO.setChaveArrecadador(contratoArrecadador.getArrecadador().getChavePrimaria());
		return consultarControladorArrecadadores(contratoArrecadadorVO);
	}

	/**
	 * Validar data contrato arrecadador.
	 * 
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDataContratoArrecadador(Date dataInicio, Date dataFim) throws NegocioException {

		if(dataInicio != null && dataFim != null && dataInicio.after(dataFim)) {
			throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.contratoarrecadador.negocio.ControladorContratoArrecadador#obterContratoArrecadador(java.lang.Long)
	 */
	@Override
	public ContratoArrecadador obterContratoArrecadador(Long chavePrimaria) throws NegocioException {

		return (ContratoArrecadador) repositorioContratoArrecadador.obter(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.contratoarrecadador.negocio.ControladorContratoArrecadador#alterarContratoArrecadador(br.com.ggas.arrecadacao
	 * .
	 * contratoarrecadador.dominio.ContratoArrecadador)
	 */
	@Override
	public void alterarContratoArrecadador(ContratoArrecadador contratoArrecadador) throws GGASException {

		validarInserirAtualizar(contratoArrecadador);
		repositorioContratoArrecadador.atualizar(contratoArrecadador);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.contratoarrecadador.negocio.ControladorContratoArrecadador#removerContratoArrecadador(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerContratoArrecadador(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		repositorioContratoArrecadador.remover(chavesPrimarias, dadosAuditoria);

	}

}
