/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 16:23:31
 @author rfilho
 */

package br.com.ggas.arrecadacao.contratoarrecadador.negocio;

import java.util.Collection;

import br.com.ggas.arrecadacao.contratoarrecadador.dominio.ContratoArrecadador;
import br.com.ggas.arrecadacao.contratoarrecadador.dominio.ContratoArrecadadorVO;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;

/**
 * Interface responsável pelas ações 
 * relacionadas ao contrato arrecadador. 
 */
public interface ControladorContratoArrecadador {

	/**
	 * Consultar controlador arrecadadores.
	 * 
	 * @param contratoArrecadadorVO
	 *            the contrato arrecadador vo
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<ContratoArrecadador> consultarControladorArrecadadores(ContratoArrecadadorVO contratoArrecadadorVO) throws GGASException;

	/**
	 * Inserir contrato arrecadador.
	 * 
	 * @param contratoArrecadador
	 *            the contrato arrecadador
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void inserirContratoArrecadador(ContratoArrecadador contratoArrecadador) throws GGASException;

	/**
	 * Obter contrato arrecadador.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the contrato arrecadador
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ContratoArrecadador obterContratoArrecadador(Long chavePrimaria) throws NegocioException;

	/**
	 * Alterar contrato arrecadador.
	 * 
	 * @param contratoArrecadador
	 *            the contrato arrecadador
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void alterarContratoArrecadador(ContratoArrecadador contratoArrecadador) throws GGASException;

	/**
	 * Remover contrato arrecadador.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerContratoArrecadador(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

}
