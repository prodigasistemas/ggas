/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 15/07/2014 14:41:30
 @author rfilho
 */

package br.com.ggas.arrecadacao.contratoarrecadador.dominio;

import java.io.Serializable;
/**
 * Classe que representa um objeto de valor de contrato arrecadador
 */
public class ContratoArrecadadorVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9198017886172018228L;

	private String numeroContrato;

	private String dataInicio;

	private String dataFim;

	private Long chaveArrecadador;

	private String habilitado;

	public String getHabilitado() {

		return habilitado;
	}
	/**
	 * @param habilitado
	 * define se o contrato arrecador está habilitado 
	 */
	public void setHabilitado(String habilitado) {

		this.habilitado = habilitado;
	}
	/**
	 * @return chave primária do arrecadador
	 */
	public Long getChaveArrecadador() {

		return chaveArrecadador;
	}
	/**
	 * @param chaveArrecadador
	 * define chave primária do arrecadador
	 */
	public void setChaveArrecadador(Long chaveArrecadador) {

		this.chaveArrecadador = chaveArrecadador;
	}

	public String getNumeroContrato() {

		return numeroContrato;
	}
	/**
	 * @param numeroContrato
	 * define o número do caontrato
	 */
	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	public String getDataInicio() {

		return dataInicio;
	}
	/**
	 * @param dataInicio
	 * define o início do contrato arrecadador
	 */
	public void setDataInicio(String dataInicio) {

		this.dataInicio = dataInicio;
	}

	public String getDataFim() {

		return dataFim;
	}
	/**
	 * @param dataFim
	 * define data final para o contrato arrecadador
	 */
	public void setDataFim(String dataFim) {

		this.dataFim = dataFim;
	}

}
