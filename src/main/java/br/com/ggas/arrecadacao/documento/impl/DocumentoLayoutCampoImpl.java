/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.documento.impl;

import java.util.Map;

import br.com.ggas.arrecadacao.documento.DocumentoLayoutCampo;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

class DocumentoLayoutCampoImpl extends EntidadeNegocioImpl implements DocumentoLayoutCampo {

	private static final long serialVersionUID = 9217271087589039646L;

	private String descricao;

	private EntidadeConteudo campo;

	private String valorPadrao;

	private String formato;

	private int posicaoInicial;

	private int posicaoFinal;

	private DocumentoLayoutRegistro documentoLayout;

	private EntidadeConteudo tipo;

	private boolean remessa;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo#isRemessa()
	 */
	@Override
	public boolean isRemessa() {

		return remessa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo
	 * #setRemessa(boolean)
	 */
	@Override
	public void setRemessa(boolean remessa) {

		this.remessa = remessa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo#getCampo()
	 */
	@Override
	public EntidadeConteudo getCampo() {

		return campo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo
	 * #setCampo(java.lang.String)
	 */
	@Override
	public void setCampo(EntidadeConteudo campo) {

		this.campo = campo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo#getValorPadrao()
	 */
	@Override
	public String getValorPadrao() {

		return valorPadrao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo
	 * #setValorPadrao(java.lang.String)
	 */
	@Override
	public void setValorPadrao(String valorPadrao) {

		this.valorPadrao = valorPadrao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo
	 * #getPosicaoInicial()
	 */
	@Override
	public int getPosicaoInicial() {

		return posicaoInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo
	 * #setPosicaoInicial(int)
	 */
	@Override
	public void setPosicaoInicial(int posicaoInicial) {

		this.posicaoInicial = posicaoInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo#getPosicaoFinal()
	 */
	@Override
	public int getPosicaoFinal() {

		return posicaoFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo
	 * #setPosicaoFinal(int)
	 */
	@Override
	public void setPosicaoFinal(int posicaoFinal) {

		this.posicaoFinal = posicaoFinal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo
	 * #getDocumentoRemessa()
	 */
	@Override
	public DocumentoLayoutRegistro getDocumentoLayout() {

		return documentoLayout;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo
	 * #setDocumentoRemessa
	 * (br.com.ggas.arrecadacao
	 * .remessa.DocumentoRemessa)
	 */
	@Override
	public void setDocumentoLayout(DocumentoLayoutRegistro documentoLayout) {

		this.documentoLayout = documentoLayout;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo#getTipo()
	 */
	@Override
	public EntidadeConteudo getTipo() {

		return tipo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.
	 * DocumentoRemessaAtributo
	 * #setTipo(br.com.ggas
	 * .geral.EntidadeConteudo)
	 */
	@Override
	public void setTipo(EntidadeConteudo tipo) {

		this.tipo = tipo;
	}

	/**
	 * @return the formato
	 */
	@Override
	public String getFormato() {

		return formato;
	}

	/**
	 * @param formato
	 *            the formato to set
	 */
	@Override
	public void setFormato(String formato) {

		this.formato = formato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
