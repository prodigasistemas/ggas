package br.com.ggas.arrecadacao.documento;

import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface DocumentoLayoutEnvioOcorrencia.
 */
public interface DocumentoLayoutErroOcorrencia extends EntidadeNegocio{
	String BEAN_ID_DOCUMENTO_LAYOUT_ERRO_OCORRENCIA = "documentoLayoutErroOcorrencia";
	
	/**
	 * @return the codigo
	 */
	String getCodigo();

	/**
	 * @param codigo the codigo to set
	 */
	void setCodigo(String codigo);

	/**
	 * @return the documentoLayout
	 */
	DocumentoLayoutRegistro getDocumentoLayoutRegistro();

	/**
	 * @param documentoLayout the documentoLayout to set
	 */
	void setDocumentoLayoutRegistro(DocumentoLayoutRegistro documentoLayoutRegistro);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao the descricao to set
	 */
	void setDescricao(String descricao);
}
