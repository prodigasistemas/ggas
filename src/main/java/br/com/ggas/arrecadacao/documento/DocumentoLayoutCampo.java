/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.documento;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface DocumentoLayoutCampo.
 */
public interface DocumentoLayoutCampo extends EntidadeNegocio {

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the campo
	 */
	EntidadeConteudo getCampo();

	/**
	 * @param campo
	 *            the campo to set
	 */
	void setCampo(EntidadeConteudo campo);

	/**
	 * @return the valorPadrao
	 */
	String getValorPadrao();

	/**
	 * @param valorPadrao
	 *            the valorPadrao to set
	 */
	void setValorPadrao(String valorPadrao);

	/**
	 * @return the formato
	 */
	String getFormato();

	/**
	 * @param formato
	 *            the formato to set
	 */
	void setFormato(String formato);

	/**
	 * @return the posicaoInicial
	 */
	int getPosicaoInicial();

	/**
	 * @param posicaoInicial
	 *            the posicaoInicial to set
	 */
	void setPosicaoInicial(int posicaoInicial);

	/**
	 * @return the posicaoFinal
	 */
	int getPosicaoFinal();

	/**
	 * @param posicaoFinal
	 *            the posicaoFinal to set
	 */
	void setPosicaoFinal(int posicaoFinal);

	/**
	 * @return the documentoLayout
	 */
	DocumentoLayoutRegistro getDocumentoLayout();

	/**
	 * @param documentoLayout
	 *            the documentoLayout to set
	 */
	void setDocumentoLayout(DocumentoLayoutRegistro documentoLayout);

	/**
	 * @return the tipo
	 */
	EntidadeConteudo getTipo();

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	void setTipo(EntidadeConteudo tipo);

	/**
	 * @return the remessa
	 */
	boolean isRemessa();

	/**
	 * @param remessa
	 *            the remessa to set
	 */
	void setRemessa(boolean remessa);
}
