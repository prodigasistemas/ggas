package br.com.ggas.arrecadacao.documento.impl;

import java.util.Map;

import br.com.ggas.arrecadacao.documento.DocumentoLayoutErroOcorrencia;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável pela representação dos códigos de erro de ocorrência do leiaute de retorno
 */
public class DocumentoLayoutErroOcorrenciaImpl extends EntidadeNegocioImpl implements DocumentoLayoutErroOcorrencia{

	private static final long serialVersionUID = -7179708249840449937L;

	private String codigo;

	private String descricao;

	private DocumentoLayoutRegistro documentoLayoutRegistro;
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento.DocumentoLayoutEnvioOcorrencia#getCodigo()
	 */
	@Override
	public String getCodigo() {
		return codigo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento.DocumentoLayoutEnvioOcorrencia#setCodigo()
	 */
	@Override
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento.DocumentoLayoutEnvioOcorrencia#getDescricao()
	 */
	@Override
	public String getDescricao() {
		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento.DocumentoLayoutEnvioOcorrencia#setDescricao()
	 */
	@Override
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro#getDocumentoLayoutRegistro()
	 */
	@Override
	public DocumentoLayoutRegistro getDocumentoLayoutRegistro() {
		return documentoLayoutRegistro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro#setDocumentoLayoutRegistro()
	 */
	@Override
	public void setDocumentoLayoutRegistro(DocumentoLayoutRegistro documentoLayoutRegistro) {
		this.documentoLayoutRegistro = documentoLayoutRegistro;
	}

}
