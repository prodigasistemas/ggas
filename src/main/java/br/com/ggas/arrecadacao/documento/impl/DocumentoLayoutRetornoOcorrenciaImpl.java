/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.documento.impl;

import java.util.Map;

import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRetornoOcorrencia;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * 
 *
 */
class DocumentoLayoutRetornoOcorrenciaImpl extends EntidadeNegocioImpl implements DocumentoLayoutRetornoOcorrencia {

	private static final long serialVersionUID = 7989260483180391387L;

	private String codigo;

	private String descricao;

	private boolean processaRecebimento;

	private boolean processaCadastro;

	private DocumentoLayoutRegistro documentoLayoutRegistro;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento.
	 * DocumentoLayoutRetornoOcorrencia
	 * #isProcessaCadastro()
	 */
	@Override
	public boolean isProcessaCadastro() {

		return processaCadastro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento.
	 * DocumentoLayoutRetornoOcorrencia
	 * #setProcessaCadastro(boolean)
	 */
	@Override
	public void setProcessaCadastro(boolean processaCadastro) {

		this.processaCadastro = processaCadastro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento.
	 * DocumentoLayoutRetornoOcorrencia
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento.
	 * DocumentoLayoutRetornoOcorrencia
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.documento.impl.
	 * DocumentoLayoutRetornoOcorrencia
	 * #getCodigo()
	 */
	@Override
	public String getCodigo() {

		return codigo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.documento.impl.
	 * DocumentoLayoutRetornoOcorrencia
	 * #setCodigo(java.lang.String)
	 */
	@Override
	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.documento.impl.
	 * DocumentoLayoutRetornoOcorrencia
	 * #isProcessaRecebimento()
	 */
	@Override
	public boolean isProcessaRecebimento() {

		return processaRecebimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.documento.impl.
	 * DocumentoLayoutRetornoOcorrencia
	 * #setProcessaRecebimento(boolean)
	 */
	@Override
	public void setProcessaRecebimento(boolean processaRecebimento) {

		this.processaRecebimento = processaRecebimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.documento.impl.
	 * DocumentoLayoutRetornoOcorrencia
	 * #getDocumentoLayoutRegistro()
	 */
	@Override
	public DocumentoLayoutRegistro getDocumentoLayoutRegistro() {

		return documentoLayoutRegistro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.documento.impl.
	 * DocumentoLayoutRetornoOcorrencia
	 * #setDocumentoLayoutRegistro
	 * (br.com.ggas.arrecadacao.
	 * documento.DocumentoLayout)
	 */
	@Override
	public void setDocumentoLayoutRegistro(DocumentoLayoutRegistro documentoLayoutRegistro) {

		this.documentoLayoutRegistro = documentoLayoutRegistro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
