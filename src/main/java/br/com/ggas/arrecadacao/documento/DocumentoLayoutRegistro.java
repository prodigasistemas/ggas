/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.documento;

import java.util.Collection;

import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * A Interface DocumentoLayoutRegistro.
 */
public interface DocumentoLayoutRegistro extends EntidadeNegocio {

	String BEAN_ID_DOCUMENTO_LAYOUT_REGISTRO = "documentoLayoutRegistro";

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the identificadorRegistro
	 */
	String getIdentificadorRegistro();

	/**
	 * @param identificadorRegistro the identificadorRegistro to set
	 */
	void setIdentificadorRegistro(String identificadorRegistro);

	/**
	 * @return the metodoProcessamento
	 */
	String getMetodoProcessamento();

	/**
	 * @param metodoProcessamento the metodoProcessamento to set
	 */
	void setMetodoProcessamento(String metodoProcessamento);

	/**
	 * @return the retornoOcorrencias
	 */
	Collection<DocumentoLayoutRetornoOcorrencia> getRetornoOcorrencias();

	/**
	 * @param retornoOcorrencias the retornoOcorrencias to set
	 */
	void setRetornoOcorrencias(Collection<DocumentoLayoutRetornoOcorrencia> retornoOcorrencias);

	/**
	 * Método responsável pot obter o retorno da ocorrência.
	 * 
	 * @param codigo O código da ocorrência
	 * @return Um retorno de ocorrência
	 */
	DocumentoLayoutRetornoOcorrencia obterRetornoOcorrencia(String codigo);

	/**
	 * @param leiaute o novo leiaute
	 */
	void setLeiaute(DocumentoLayout leiaute);

	/**
	 * @return o leiaute
	 */
	DocumentoLayout getLeiaute();

	/**
	 * @param campos os campos
	 */
	void setCampos(Collection<DocumentoLayoutCampo> campos);

	/**
	 * @return os campos
	 */
	Collection<DocumentoLayoutCampo> getCampos();

	/**
	 * @return the envioOcorrencias
	 */
	Collection<DocumentoLayoutEnvioOcorrencia> getEnvioOcorrencias();

	/**
	 * @param envioOcorrencias the envioOcorrencias to set
	 */
	void setEnvioOcorrencias(Collection<DocumentoLayoutEnvioOcorrencia> envioOcorrencias);
	
	/**
	 * @return the envioOcorrencias
	 */
	Collection<DocumentoLayoutErroOcorrencia> getErroOcorrencias();

	/**
	 * @param envioOcorrencias the envioOcorrencias to set
	 */
	void setErroOcorrencias(Collection<DocumentoLayoutErroOcorrencia> erroOcorrencias);

	/**
	 * Método responsável por obter um específico código de erro de ocorrência na coleção do objeto
	 * 
	 * @param codigo the codigo
	 * @return the erroOcorrencia
	 */
	DocumentoLayoutErroOcorrencia obterErroOcorrencia(String codigo);
}
