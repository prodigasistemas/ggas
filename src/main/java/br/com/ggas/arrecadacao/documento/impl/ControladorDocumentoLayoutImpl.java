/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.documento.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorContrato;
import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.ArrecadadorMovimento;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.DebitoAutomaticoMovimento;
import br.com.ggas.arrecadacao.DebitoNaoProcessado;
import br.com.ggas.arrecadacao.documento.ControladorDocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutCampo;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.arrecadacao.impl.ArrecadadorContratoConvenioImpl;
import br.com.ggas.arrecadacao.impl.ArrecadadorContratoImpl;
import br.com.ggas.arrecadacao.impl.ArrecadadorImpl;
import br.com.ggas.arrecadacao.impl.BancoImpl;
import br.com.ggas.atendimento.documentoimpressaolayout.dominio.DocumentoImpressaoLayout;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorDocumentoLayoutImpl extends ControladorNegocioImpl implements ControladorDocumentoLayout {

	private static final String METODO_NAO_ENCONTRADO_DOCUMENTO = "### Não foi encontrado o método para o documento: ";

	private static final int LIMITE_NUMERO = 3;

	private static final String PREFIXO_METODO_CONVERSOR = "get";	
	
	private static final Logger LOG = Logger.getLogger(ControladorDocumentoLayoutImpl.class);

	public Class<?> getClasseEntidadeDocumentoImpressaoLayout() {

		return DocumentoImpressaoLayout.class;
	}

	/**
	 * Consultar documento remessa.
	 * 
	 * @param banco the banco
	 * @param identificadorRegistro the identificador registro
	 * @param tamanhoRegistro the tamanho registro
	 * @return the documento layout
	 */
	public DocumentoLayout consultarDocumentoRemessa(EntidadeConteudo banco, String identificadorRegistro, int tamanhoRegistro) {

		return null;
	}

	/**
	 * Consultar documento remessa.
	 * 
	 * @param banco the banco
	 * @param identificadorRegistro the identificador registro
	 * @return the documento layout
	 */
	public DocumentoLayout consultarDocumentoRemessa(EntidadeConteudo banco, String identificadorRegistro) {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoLayout.BEAN_ID_DOCUMENTO_LAYOUT);
	}

	public Class<?> getClasseEntidadeArrecadorContratoConvenio() {

		return ServiceLocator.getInstancia().getClassPorID(ArrecadadorContratoConvenio.BEAN_ID_ARRECADADOR_CONTRATO_CONVENIO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.remessa. ControladorDocumentoRemessa #gerarDadosDocumentoRemessaDebitoAutomatico (int,
	 * java.math.BigDecimal, br.com.ggas.arrecadacao .DebitoAutomaticoMovimento, br.com.ggas.arrecadacao .remessa.DocumentoRemessa,
	 * br.com.ggas.util.ServiceLocator)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> gerarDadosDocumentoRemessaDebitosNaoProcessados(int totalRegistros, BigDecimal somatorio,
			DebitoNaoProcessado debitoNaoProcessado, DocumentoLayoutRegistro dlr, ArrecadadorMovimento arrecadadorMovimento,
			Arrecadador arrecadador, ServiceLocator serviceLocator) throws NegocioException {

		Class<?> clazz = null;
		Map<String, Object> retorno = null;
		Method metodo = null;

		try {

			clazz = Class.forName(dlr.getLeiaute().getClasseConversorCampo());
			metodo = Util.getMethodRecursive(clazz, PREFIXO_METODO_CONVERSOR + dlr.getIdentificadorRegistro(), int.class, BigDecimal.class,
					DebitoNaoProcessado.class, DocumentoLayoutRegistro.class, ArrecadadorMovimento.class, Arrecadador.class,
					ServiceLocator.class);
			retorno = (Map<String, Object>) metodo.invoke(null, totalRegistros, somatorio, debitoNaoProcessado, dlr, arrecadadorMovimento,
					arrecadador, serviceLocator);

		} catch (SecurityException e) {
			throw new NegocioException(e);
		} catch (NoSuchMethodException e) {
			LOG.info(METODO_NAO_ENCONTRADO_DOCUMENTO + dlr.getIdentificadorRegistro() + " : " + e.getMessage(), e);
		} catch (IllegalArgumentException e) {
			throw new NegocioException(e);
		} catch (IllegalAccessException e) {
			throw new NegocioException(e);
		} catch (InvocationTargetException e) {
			throw new NegocioException(e);
		} catch (ClassNotFoundException e) {
			throw new NegocioException(e);
		}

		return retorno;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.remessa. ControladorDocumentoRemessa #gerarDadosDocumentoRemessaDebitoAutomatico (int,
	 * java.math.BigDecimal, br.com.ggas.arrecadacao .DebitoAutomaticoMovimento, br.com.ggas.arrecadacao .remessa.DocumentoRemessa,
	 * br.com.ggas.util.ServiceLocator)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> gerarDadosDocumentoRemessaDebitoAutomatico(int totalRegistros, BigDecimal somatorio,
			DebitoAutomaticoMovimento debitoAutomaticoMovimento, DocumentoLayoutRegistro dlr, ServiceLocator serviceLocator)
			throws NegocioException {

		Class<?> clazz = null;
		Map<String, Object> retorno = null;
		Method metodo = null;

		try {

			clazz = Class.forName(dlr.getLeiaute().getClasseConversorCampo());
			metodo = Util.getMethodRecursive(clazz, PREFIXO_METODO_CONVERSOR + dlr.getIdentificadorRegistro(), int.class, BigDecimal.class,
					DebitoAutomaticoMovimento.class, DocumentoLayoutRegistro.class, ServiceLocator.class);
			retorno = (Map<String, Object>) metodo.invoke(null, totalRegistros, somatorio, debitoAutomaticoMovimento, dlr, serviceLocator);

		} catch (SecurityException e) {
			throw new NegocioException(e);
		} catch (NoSuchMethodException e) {
			LOG.info(METODO_NAO_ENCONTRADO_DOCUMENTO + dlr.getIdentificadorRegistro() + " : " + e.getMessage(), e);
		} catch (IllegalArgumentException e) {
			throw new NegocioException(e);
		} catch (IllegalAccessException e) {
			throw new NegocioException(e);
		} catch (InvocationTargetException e) {
			throw new NegocioException(e);
		} catch (ClassNotFoundException e) {
			throw new NegocioException(e);
		}

		return retorno;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.remessa. ControladorDocumentoRemessa #gerarDadosDocumentoRemessaCobranca(int, java.math.BigDecimal,
	 * br.com.ggas.arrecadacao .CobrancaRegistradaMovimento, br.com.ggas.arrecadacao .remessa.DocumentoRemessa,
	 * br.com.ggas.util.ServiceLocator)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> gerarDadosDocumentoRemessaCobranca(int totalRegistros, BigDecimal somatorio,
			CobrancaBancariaMovimento cobrancaBancariaMovimento, DocumentoLayoutRegistro dlr, ServiceLocator serviceLocator)
			throws NegocioException {

		Class<?> clazz = null;
		Map<String, Object> retorno = null;
		Method metodo = null;

		try {

			clazz = Class.forName(dlr.getLeiaute().getClasseConversorCampo());

			metodo = Util.getMethodRecursive(clazz, PREFIXO_METODO_CONVERSOR + dlr.getIdentificadorRegistro(), int.class, BigDecimal.class,
					CobrancaBancariaMovimento.class, DocumentoLayoutRegistro.class, ServiceLocator.class);

			retorno = (Map<String, Object>) metodo.invoke(null, totalRegistros, somatorio, cobrancaBancariaMovimento, dlr, serviceLocator);

		} catch (SecurityException e) {
			throw new NegocioException(e);
		} catch (NoSuchMethodException e) {
			LOG.info(METODO_NAO_ENCONTRADO_DOCUMENTO + dlr.getIdentificadorRegistro() + " : " + e.getMessage(), e);
		} catch (IllegalArgumentException e) {
			throw new NegocioException(e);
		} catch (IllegalAccessException e) {
			throw new NegocioException(e);
		} catch (InvocationTargetException e) {
			throw new NegocioException(e);
		} catch (ClassNotFoundException e) {
			throw new NegocioException(e);
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.remessa. ControladorDocumentoRemessa #gerarLinhaRemessa(java.util.Map, br.com.ggas
	 * .arrecadacao.remessa.DocumentoRemessa)
	 */
	@Override
	public String gerarLinhaRemessa(Map<String, Object> dados, DocumentoLayoutRegistro documentoRemessa) throws NegocioException {

		char[] arrLinha = new char[documentoRemessa.getLeiaute().getTamanhoRegistro()];		

		Util.inicializarArray(arrLinha);

		if (dados != null && !dados.isEmpty()) {
			String campo = null;
			String valor = null;
			String formato = null;
			String valorPadrao = null;
			int tamanho = 0;
			Collection<DocumentoLayoutCampo> atributos = null;
			atributos = documentoRemessa.getCampos();

			for (DocumentoLayoutCampo documentoRemessaAtributo : atributos) {
				if (documentoRemessaAtributo.isHabilitado() && documentoRemessaAtributo.isRemessa()) {
					campo = documentoRemessaAtributo.getCampo().getDescricao();
					formato = documentoRemessaAtributo.getFormato();
					valorPadrao = documentoRemessaAtributo.getValorPadrao();

					tamanho = validaTamanhoDocumentoRemessaAtributo(tamanho, documentoRemessaAtributo);

					valor = verificaValorPadrao(dados, campo, formato, valorPadrao, tamanho, documentoRemessaAtributo);

					verificaSeValorVazio(arrLinha, valor, documentoRemessaAtributo);
				}
				tamanho = 0;
			}
		}

		return Util.removerAcentuacao(String.valueOf(arrLinha));
	}

	private void verificaSeValorVazio(char[] arrLinha, String valor, DocumentoLayoutCampo documentoRemessaAtributo) {
		if (!StringUtils.isEmpty(valor)) {
			Util.preencherArray(new int[] { documentoRemessaAtributo.getPosicaoInicial(), documentoRemessaAtributo.getPosicaoFinal() },
					valor, arrLinha);
		}
	}
	
	private String verificaValorPadrao(Map<String, Object> dados, String campo, String formato, String valorPadrao, int tamanho,
			DocumentoLayoutCampo documentoRemessaAtributo) {
		String valor = null;
		if (!StringUtils.isEmpty(valorPadrao)) {
			valor = valorPadrao;
		} else {
			if (dados.containsKey(campo)) {
				Object data;
				Object numero;
				if (DOCUMENTO_LAYOUT_TIPO_NUMERICO.equals(documentoRemessaAtributo.getTipo().getCodigo())) {
					numero = dados.get(campo);

					valor = this.colocarZerosAEsquerdaEmDoumentoLayoutTipoNumerico(numero, dados, campo, tamanho);

				} else if (DOCUMENTO_LAYOUT_TIPO_DATA.equals(documentoRemessaAtributo.getTipo().getCodigo())) {
					data = dados.get(campo);

					valor = this.formatarDataEmDocumentoLayoutTipoData(data, formato);

				} else {
					valor = String.valueOf(dados.get(campo));
				}
			} 
		}
		return valor;
	}
	
	private String colocarZerosAEsquerdaEmDoumentoLayoutTipoNumerico(Object numero, Map<String, Object> dados, String campo, int tamanho){
		NumberFormat numberFormat;
		String numeroFormatado;
		String valor;
		if (numero instanceof Number) {
			if (numero instanceof BigDecimal) {
				numberFormat = NumberFormat.getCurrencyInstance(Constantes.LOCALE_PADRAO);
				numeroFormatado = numberFormat.format(numero).substring(LIMITE_NUMERO);
			} else {
				numeroFormatado = Util.formatarValorNumerico(numero);
			}
			numeroFormatado = numeroFormatado.replaceAll(Constantes.EXPRESSAO_REGULAR_PONTO, StringUtils.EMPTY);
			numeroFormatado = numeroFormatado.replaceAll(Constantes.EXPRESSAO_REGULAR_VIRGULA, StringUtils.EMPTY);
			valor = Util.adicionarZerosEsquerdaNumero(numeroFormatado, tamanho);
		} else {
			valor = String.valueOf(dados.get(campo));
			valor = Util.adicionarZerosEsquerdaNumero(valor, tamanho);
		}
		
		return valor;
	}
	
	private String formatarDataEmDocumentoLayoutTipoData(Object data, String formato){
		
		String valor;
		SimpleDateFormat simpleDateFormat;
		
		if (data instanceof Date && !StringUtils.isEmpty(formato)) {
			simpleDateFormat = new SimpleDateFormat(formato);
			valor = simpleDateFormat.format((Date) data);
		} else {
			valor = null;
		}
		
		return valor;
	}

	private int validaTamanhoDocumentoRemessaAtributo(int tamanhoDocumento, DocumentoLayoutCampo documentoRemessaAtributo) {
		int tamanho = tamanhoDocumento;
		if (documentoRemessaAtributo.getPosicaoFinal() > documentoRemessaAtributo.getPosicaoInicial()) {
			tamanho = documentoRemessaAtributo.getPosicaoFinal() - (documentoRemessaAtributo.getPosicaoInicial() - 1);
		}
		return tamanho;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.remessa. ControladorDocumentoRemessa #obterNsaRetornoEsperado(java.lang.String, java.lang.String)
	 */
	@Override
	public Long obterNsaRetornoEsperado(String codigoConvenio) {

		Long nsaRetornoEsperado = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select  nsaRetorno ");
		hql.append(" from ");
		hql.append(getClasseEntidadeArrecadorContratoConvenio().getSimpleName());
		hql.append(" arrecadadorContratoConvenio ");
		hql.append(" where ");
		hql.append(" arrecadadorContratoConvenio.codigoConvenio like :codigoConvenio");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("codigoConvenio", codigoConvenio.trim());

		Long nsaRetorno = (Long) query.uniqueResult();

		if (nsaRetorno != null) {
			nsaRetornoEsperado = nsaRetorno + 1;
		}

		return nsaRetornoEsperado;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<DocumentoLayout> obterDocumentosLayout() {

		Criteria criteria = this.createCriteria(DocumentoLayout.class);
		criteria.createAlias("formaArrecadacao", "formaArrecadacao");
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	@Override
	public DocumentoLayout obterDocumentoLayout(DocumentoLayout filtro) {

		return (DocumentoLayout) montarCriterio(filtro).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<DocumentoLayout> obterDocumentosLayout(DocumentoLayout filtro) {

		return montarCriterio(filtro).list();
	}

	private Criteria montarCriterio(DocumentoLayout filtro) {

		Criteria criterioLeiaute = getSession().createCriteria(filtro.getClass()).add(Example.create(filtro).excludeZeroes());
		adicionarCriterioCampo(filtro, criterioLeiaute);
		adicionarCriterioBanco(filtro, criterioLeiaute);
		return criterioLeiaute;
	}

	private void adicionarCriterioCampo(DocumentoLayout filtro, Criteria criterioLeiaute) {

		DocumentoLayoutRegistro registro = filtro.getRegistros().iterator().next();
		criterioLeiaute.createCriteria("registros").add(Example.create(registro).excludeZeroes())
				.addOrder(Order.asc("identificadorRegistro"));
	}

	private void adicionarCriterioBanco(DocumentoLayout filtro, Criteria criterioLeiaute) {

		ArrecadadorContratoConvenio convenio = filtro.getConvenios().iterator().next();
		criterioLeiaute.createCriteria("convenios").add(Example.create(convenio).excludeZeroes()).createCriteria("arrecadadorContrato")
				.add(Example.create(convenio.getArrecadadorContrato()).excludeZeroes()).createCriteria("arrecadador")
				.add(Example.create(convenio.getArrecadadorContrato().getArrecadador()).excludeZeroes()).createCriteria("banco")
				.add(Example.create(convenio.getArrecadadorContrato().getArrecadador().getBanco()).excludeZeroes());
	}

	@Override
	public DocumentoLayout criarLeiauteFiltro() {

		DocumentoLayout leiaute = new DocumentoLayoutImpl();
		Collection<DocumentoLayoutRegistro> registros = new HashSet<>();
		DocumentoLayoutRegistro registro = new DocumentoLayoutRegistroImpl();
		registro.setHabilitado(true);
		registros.add(registro);
		Collection<ArrecadadorContratoConvenio> convenios = new HashSet<>();
		ArrecadadorContratoConvenio convenio = new ArrecadadorContratoConvenioImpl();
		convenio.setHabilitado(true);
		ArrecadadorContrato arrecadadorContrato = new ArrecadadorContratoImpl();
		arrecadadorContrato.setHabilitado(true);
		Arrecadador arrecadador = new ArrecadadorImpl();
		arrecadador.setHabilitado(true);
		Banco banco = new BancoImpl();
		banco.setHabilitado(true);
		arrecadador.setBanco(banco);
		arrecadadorContrato.setArrecadador(arrecadador);
		convenio.setArrecadadorContrato(arrecadadorContrato);
		convenios.add(convenio);
		leiaute.setRegistros(registros);
		leiaute.setConvenios(convenios);
		leiaute.setHabilitado(true);
		return leiaute;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.remessa. ControladorDocumentoRemessa #obterTipoMovimentoPorCodigoConvenio (java.lang.String)
	 */
	@Override
	public EntidadeConteudo obterTipoConvenioPorCodigoConvenio(String codigoConvenio) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select  tipoConvenio ");
		hql.append(" from ");
		hql.append(getClasseEntidadeArrecadorContratoConvenio().getSimpleName());
		hql.append(" arrecadadorContratoConvenio ");
		hql.append(" where ");
		hql.append(" arrecadadorContratoConvenio.codigoConvenio like :codigoConvenio");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("codigoConvenio", codigoConvenio.trim());

		return (EntidadeConteudo) query.uniqueResult();
	}
	
	@Override
	@Cacheable("documentoLayout")
	public DocumentoLayout obterDocumentoLayoutPorConvenio(Long chaveArrecadadorConvenio) {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select documentoLayout ");
		hql.append(" from ");
		hql.append(DocumentoLayoutImpl.class.getSimpleName());
		hql.append(" documentoLayout ");
		hql.append(" inner join fetch documentoLayout.formaArrecadacao ");
		hql.append(" where ");
		hql.append(" documentoLayout.chavePrimaria IN (select leiaute.chavePrimaria ");
		hql.append(" from ");
		hql.append(getClasseEntidadeArrecadorContratoConvenio().getSimpleName());
		hql.append(" convenio ");
		hql.append(" inner join convenio.leiaute leiaute ");
		hql.append(" where ");
		hql.append(" convenio.chavePrimaria = :chavePrimaria )");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, chaveArrecadadorConvenio);

		return (DocumentoLayout) query.uniqueResult();
	}

}
