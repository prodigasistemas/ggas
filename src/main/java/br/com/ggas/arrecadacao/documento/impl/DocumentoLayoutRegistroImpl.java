/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.documento.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.arrecadacao.documento.DocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutCampo;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutEnvioOcorrencia;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutErroOcorrencia;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRetornoOcorrencia;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * A Class DocumentoLayoutRegistroImpl.
 */
public class DocumentoLayoutRegistroImpl extends EntidadeNegocioImpl implements DocumentoLayoutRegistro {

	private static final long serialVersionUID = 7285044714672525671L;

	private String descricao;

	private String identificadorRegistro;

	private Collection<DocumentoLayoutCampo> campos = new HashSet<>();

	private String metodoProcessamento;

	private Collection<DocumentoLayoutRetornoOcorrencia> retornoOcorrencias = new HashSet<>();

	private DocumentoLayout leiaute;

	private Collection<DocumentoLayoutEnvioOcorrencia> envioOcorrencias = new HashSet<>();
	
	private Collection<DocumentoLayoutErroOcorrencia> erroOcorrencias = new HashSet<>();

	/**
	 * @return the retornoOcorrencias
	 */
	@Override
	public Collection<DocumentoLayoutRetornoOcorrencia> getRetornoOcorrencias() {
		return retornoOcorrencias;
	}

	/**
	 * @param retornoOcorrencias the retornoOcorrencias to set
	 */
	@Override
	public void setRetornoOcorrencias(Collection<DocumentoLayoutRetornoOcorrencia> retornoOcorrencias) {
		this.retornoOcorrencias = retornoOcorrencias;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.remessa.DocumentoRemessa#getMetodoProcessamento()
	 */
	@Override
	public String getMetodoProcessamento() {
		return metodoProcessamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.DocumentoRemessa#setMetodoProcessamento(java.lang.String)
	 */
	@Override
	public void setMetodoProcessamento(String metodoProcessamento) {
		this.metodoProcessamento = metodoProcessamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.DocumentoRemessa#getDescricao()
	 */
	@Override
	public String getDescricao() {
		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.remessa. DocumentoRemessa#setDescricao
	 * (java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.remessa.DocumentoRemessa#getIdentificadorRegistro()
	 */
	@Override
	public String getIdentificadorRegistro() {
		return identificadorRegistro;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.remessa.DocumentoRemessa#setIdentificadorRegistro(java.lang.String)
	 */
	@Override
	public void setIdentificadorRegistro(String identificadorRegistro) {

		this.identificadorRegistro = identificadorRegistro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento. ocumentoLayoutRegistro#getLeiaute()
	 */
	@Override
	public DocumentoLayout getLeiaute() {
		return leiaute;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro#setLeiaute
	 * (br.com.ggas.arrecadacao.documento.DocumentoLayout)
	 */
	@Override
	public void setLeiaute(DocumentoLayout leiaute) {
		this.leiaute = leiaute;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.documento. DocumentoLayout
	 * #obterRetornoOcorrencia(java.lang.String)
	 */
	@Override
	public DocumentoLayoutRetornoOcorrencia obterRetornoOcorrencia(String codigo) {

		DocumentoLayoutRetornoOcorrencia retorno = null;
		for (DocumentoLayoutRetornoOcorrencia documentoLayoutRetornoOcorrencia : retornoOcorrencias) {
			if (documentoLayoutRetornoOcorrencia.getCodigo().equals(codigo)) {
				retorno = documentoLayoutRetornoOcorrencia;
				break;
			}
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.documento. DocumentoLayoutRegistro#getCampos()
	 */
	@Override
	public Collection<DocumentoLayoutCampo> getCampos() {
		return campos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.documento. DocumentoLayoutRegistro#setCampos
	 * (java.util.Collection)
	 */
	@Override
	public void setCampos(Collection<DocumentoLayoutCampo> campos) {
		this.campos = campos;
	}

	@Override
	public Collection<DocumentoLayoutEnvioOcorrencia> getEnvioOcorrencias() {
		return envioOcorrencias;
	}

	@Override
	public void setEnvioOcorrencias(Collection<DocumentoLayoutEnvioOcorrencia> envioOcorrencias) {
		this.envioOcorrencias = envioOcorrencias;
	}

	@Override
	public Collection<DocumentoLayoutErroOcorrencia> getErroOcorrencias() {
		return this.erroOcorrencias;
	}

	@Override
	public void setErroOcorrencias(Collection<DocumentoLayoutErroOcorrencia> erroOcorrencias) {
		this.erroOcorrencias = erroOcorrencias;		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.documento. DocumentoLayout
	 * #obterRetornoOcorrencia(java.lang.String)
	 */
	@Override
	public DocumentoLayoutErroOcorrencia obterErroOcorrencia(String codigo) {

		DocumentoLayoutErroOcorrencia retorno = null;
		for (DocumentoLayoutErroOcorrencia documentoLayoutEnvioOcorrencia : erroOcorrencias) {
			if (documentoLayoutEnvioOcorrencia.getCodigo().equals(codigo)) {
				retorno = documentoLayoutEnvioOcorrencia;
				break;
			}
		}
		return retorno;
	}
}
