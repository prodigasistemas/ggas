/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.documento;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorMovimento;
import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.DebitoAutomaticoMovimento;
import br.com.ggas.arrecadacao.DebitoNaoProcessado;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.util.ServiceLocator;

/**
 * Interface responsável pelos métodos do controlador de layout de documento
 */
public interface ControladorDocumentoLayout extends ControladorNegocio {

	public static final String DOCUMENTO_LAYOUT_TIPO_DATA = "D";

	public static final String DOCUMENTO_LAYOUT_TIPO_NUMERICO = "N";

	public static final String DOCUMENTO_LAYOUT_TIPO_ALFA_NUMERICO = "A";
	
	public static final String FROM = " from ";

	public static final String WHERE = " where ";

	String BEAN_ID_CONTROLADOR_DOCUMENTO_LAYOUT = "controladorDocumentoLayout";

	/**
	 * Método responsável por gerar uma linha de
	 * remessa para compor o arquivo.
	 * 
	 * @param dados
	 *            O dados necessários
	 * @param documentoRemessa
	 *            O documento remessa
	 * @return Uma linha com os dados
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	String gerarLinhaRemessa(Map<String, Object> dados, DocumentoLayoutRegistro documentoRemessa) throws NegocioException;

	/**
	 * Método responsável por gerar um mapa com os
	 * dados do documento de remessa para débito
	 * automatico.
	 * 
	 * @param totalRegistros
	 *            O total de registros
	 * @param somatorio
	 *            O somatório dos valores
	 * @param debitoAutomaticoMovimento
	 *            O movimento
	 * @param documentoRemessa
	 *            O documento
	 * @param serviceLocator
	 *            O service locator
	 * @return Um mapa
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Map<String, Object> gerarDadosDocumentoRemessaDebitoAutomatico(int totalRegistros, BigDecimal somatorio,
					DebitoAutomaticoMovimento debitoAutomaticoMovimento, DocumentoLayoutRegistro documentoRemessa,
					ServiceLocator serviceLocator) throws NegocioException;

	/**
	 * Método responsável por gerar um mapa com os
	 * dados do documento de remessa para cobrança.
	 * 
	 * @param totalRegistros
	 *            O total de registros
	 * @param somatorio
	 *            O somatório dos valores
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            O documento
	 * @param serviceLocator
	 *            O service locator
	 * @return Um mapa
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Map<String, Object> gerarDadosDocumentoRemessaCobranca(int totalRegistros, BigDecimal somatorio,
					CobrancaBancariaMovimento cobrancaBancariaMovimento, DocumentoLayoutRegistro documentoRemessa,
					ServiceLocator serviceLocator) throws NegocioException;

	/**
	 * Método responsável por gerar um mapa com os
	 * dados do documento de remessa para os
	 * débitos não processados.
	 * 
	 * @param totalRegistros
	 *            O total de registros
	 * @param somatorio
	 *            O somatório dos valores
	 * @param debitoNaoProcessadoVO
	 *            O débito não processado
	 * @param documentoLayout
	 *            O documento layout
	 * @param arrecadadorMovimento
	 *            the arrecadador movimento
	 * @param serviceLocator
	 *            O service locator
	 * @param arrecadador {@link Arrecadador}
	 * @return Um mapa
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Map<String, Object> gerarDadosDocumentoRemessaDebitosNaoProcessados(int totalRegistros, BigDecimal somatorio,
					DebitoNaoProcessado debitoNaoProcessadoVO, DocumentoLayoutRegistro documentoLayout,
					ArrecadadorMovimento arrecadadorMovimento, Arrecadador arrecadador, ServiceLocator serviceLocator)
					throws NegocioException;

	/**
	 * Método responsável por obter o Número
	 * Sequencial do Arquivo de Retorno esperado
	 * pelo sistema.
	 * 
	 * @param codigoConvenio
	 *            the codigo convenio
	 * @return Número Sequencial do Arquivo de
	 *         Retorno esperado.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Long obterNsaRetornoEsperado(String codigoConvenio) throws NegocioException;

	/**
	 * Método responsável por obter o tipo de
	 * movimento de acordo com o convênio.
	 * 
	 * @param codigoConvenio
	 *            the codigo convenio
	 * @return Tipo de Movimento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	EntidadeConteudo obterTipoConvenioPorCodigoConvenio(String codigoConvenio) throws NegocioException;

	/**
	 * Obter documentos layout.
	 *
	 * @return the collection
	 */
	Collection<DocumentoLayout> obterDocumentosLayout();

	/**
	 * Obter documento layout.
	 *
	 * @param filtro the filtro
	 * @return the documento layout
	 */
	DocumentoLayout obterDocumentoLayout(DocumentoLayout filtro);

	/**
	 * Obter documentos layout.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 */
	Collection<DocumentoLayout> obterDocumentosLayout(DocumentoLayout filtro);

	/**
	 * Criar leiaute filtro.
	 *
	 * @return the documento layout
	 */
	DocumentoLayout criarLeiauteFiltro();

	/**
	 * Obtém o layout do documento a partir convênio do arrecadador.
	 * @param chaveArrecadadorConvenio - {@link Long}
	 * @return {@link DocumentoLayout}
	 */
	DocumentoLayout obterDocumentoLayoutPorConvenio(Long chaveArrecadadorConvenio);

}
