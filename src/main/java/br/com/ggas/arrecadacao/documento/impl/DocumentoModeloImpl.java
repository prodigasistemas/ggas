/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 02/12/2013 10:24:55
 @author wcosta
 */

package br.com.ggas.arrecadacao.documento.impl;

import java.util.Map;

import br.com.ggas.arrecadacao.documento.DocumentoModelo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pela representação dos modelos de documentos
 */
public class DocumentoModeloImpl extends EntidadeNegocioImpl implements DocumentoModelo {

	private static final long serialVersionUID = -6717381342694837683L;

	private String descricao;

	private String arquivo;

	private byte[] modelo;
	
	/**
	 * Retorna descricao
	 * 
	 * @return descricao
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}
	
	/**
	 * Atribui descricao
	 * 
	 * @param descricao
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}
	
	/**
	 * Retorna arquivo
	 * 
	 * @return arquivo
	 */
	@Override
	public String getArquivo() {

		return arquivo;
	}
	
	/**
	 * Atribui arquivo
	 * 
	 * @param arquivo
	 */
	@Override
	public void setArquivo(String arquivo) {

		this.arquivo = arquivo;
	}
	
	/**
	 * Retorna modelo
	 * 
	 * @return o modelo de documento
	 */
	@Override
	public byte[] getModelo() {
		byte[] modeloTmp = null;
		if(modelo != null){
			modeloTmp = modelo.clone();
		}
		return modeloTmp;
	}
	/**
	 * @param modelo
	 * define o modelo de documento
	 */
	@Override
	public void setModelo(byte[] modelo) {
		if(modelo != null){
			this.modelo = modelo.clone();
		} else {
			this.modelo = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
