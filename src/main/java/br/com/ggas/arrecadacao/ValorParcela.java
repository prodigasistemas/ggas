/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * Interface responsavel pelo valor da parcela
 */
public interface ValorParcela extends Serializable {

	String BEAN_ID_VALOR_PARCELA = "valorParcela";

	/**
	 * @return the dataVencimento
	 */
	Date getDataVencimento();

	/**
	 * @param dataVencimento
	 *            the dataVencimento to set
	 */
	void setDataVencimento(Date dataVencimento);

	/**
	 * @return the periodo
	 */
	int getPeriodo();

	/**
	 * @param periodo
	 *            the periodo to set
	 */
	void setPeriodo(int periodo);

	/**
	 * @return the saldo
	 */
	BigDecimal getSaldo();

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	void setSaldo(BigDecimal saldo);

	/**
	 * @return the juros
	 */
	BigDecimal getJuros();

	/**
	 * @param juros
	 *            the juros to set
	 */
	void setJuros(BigDecimal juros);

	/**
	 * @return the amortizacao
	 */
	BigDecimal getAmortizacao();

	/**
	 * @param amortizacao
	 *            the amortizacao to set
	 */
	void setAmortizacao(BigDecimal amortizacao);

	/**
	 * @return the valorPagar
	 */
	BigDecimal getValorPagar();

	/**
	 * @param valorPagar
	 *            the valorPagar to set
	 */
	void setValorPagar(BigDecimal valorPagar);

}
