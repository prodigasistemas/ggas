/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.remessa.conversor;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.DebitoAutomaticoMovimento;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe DACNAB150
 * 
 * @author Procenge
 */
public class DACNAB150Caixa extends DACNAB150 {

	private static final String RESERVADO_FUTURO2 = "reservadoFuturo2";
	private static final String RESERVADO_FUTURO1 = "reservadoFuturo1";
	private static final String CODIGO_OPERADOR = "003";

	/**
	 * Gets the a.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the a
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getA(int totalRegistros, BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB150.getA(somatorio, debitoAutomaticoMovimento, documentoRemessa, serviceLocator);

		dados.put("identificacaoServico", "DEB AUTOMAT");

		setarInformacoesContaCompromisso(debitoAutomaticoMovimento, documentoRemessa, serviceLocator, dados);

		dados.put("idAmbienteCliente", "P");
		dados.put("idAmbienteCaixa", "P");

		dados.put(RESERVADO_FUTURO1, StringUtils.EMPTY);
		// numeroSequencialRegistro já tem um
		// valor padrão = 000000
		dados.put(RESERVADO_FUTURO2, StringUtils.EMPTY);

		return dados;
	}

	/**
	 * Setar informacoes conta compromisso.
	 * 
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @param dados
	 *            the dados
	 * @throws GGASException 
	 */
	private static void setarInformacoesContaCompromisso(DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator, Map<String, Object> dados) throws GGASException {

		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) serviceLocator
						.getBeanPorID(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);

		ArrecadadorContratoConvenio arrecadadorContratoConvenio = controladorArrecadacao.obterACConvenioPorArrecadador(
						debitoAutomaticoMovimento.getDebitoAutomatico().getArrecadador().getChavePrimaria(), documentoRemessa
										.getLeiaute().getFormaArrecadacao().getChavePrimaria());
		
		if(arrecadadorContratoConvenio != null && arrecadadorContratoConvenio.getContaConvenio() != null) {

			if(arrecadadorContratoConvenio.getContaConvenio().getAgencia() != null) {
				String agencia = arrecadadorContratoConvenio.getContaConvenio().getAgencia().getCodigo();
				dados.put("codigoAgenciaConta", agencia);
			}
			dados.put("codigoOperacaoConta", CODIGO_OPERADOR);
			dados.put("numeroConta", Integer.valueOf(arrecadadorContratoConvenio.getContaConvenio().getNumeroConta()));
					
		}
	}

	/**
	 * Gets the e.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the e
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getE(int totalRegistros, BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB150.getE(totalRegistros, somatorio, debitoAutomaticoMovimento, documentoRemessa, serviceLocator);

		if(debitoAutomaticoMovimento.getDebitoAutomatico() != null
						&& debitoAutomaticoMovimento.getDebitoAutomatico().getNumeroAgendamento() != null) {
			dados.put("numeroAgendamentoCliente", Integer.valueOf(debitoAutomaticoMovimento.getDebitoAutomatico().getNumeroAgendamento()));
		}
		dados.put("reservadoFuturo", StringUtils.EMPTY);
		dados.put("numeroSequencialRegistro", totalRegistros);

		return dados;

	}

	/**
	 * Gets the z.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the z
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getZ(int totalRegistros, BigDecimal somatorio,
			DebitoAutomaticoMovimento debitoAutomaticoMovimento, DocumentoLayoutRegistro documentoRemessa,
			ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB150.getZ(totalRegistros, somatorio, documentoRemessa, serviceLocator);

		dados.put(RESERVADO_FUTURO1, StringUtils.EMPTY);
		dados.put("numeroSequencialRegistro", totalRegistros - 1);
		dados.put(RESERVADO_FUTURO2, StringUtils.EMPTY);

		return dados;
	}

	/**
	 * Gets the b.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the b
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getB(int totalRegistros, BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB150.getB(somatorio, debitoAutomaticoMovimento, documentoRemessa, serviceLocator);

		dados.put(RESERVADO_FUTURO1, StringUtils.EMPTY);
		dados.put(RESERVADO_FUTURO2, StringUtils.EMPTY);

		return dados;
	}
}
