/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.remessa.conversor;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe DACNAB240
 * 
 * @author Procenge
 */
public class DACNAB240 {

	private static final String LOTE_SERVICO = "loteServico";
	private static final String NUMERO_INSCRICAO_EMPRESA = "numeroInscricaoEmpresa";
	private static final String NOME_EMPRESA = "nomeEmpresa";
	private static final String IDENTIFICACAO_REGISTRO = "identificacaoRegistro";
	private static final int DIVISOR = 2;
	private static final int SEGUNDO_LIMITADOR_REGISTROS = 4;
	private static final int LIMITADOR_REGISTROS = 3;

	/**
	 * Gets the 0.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 0
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get0(BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = new HashMap<>();
		Map<String, Object> dadosEmpresa = obterNomeEmpresa(serviceLocator);

		dados.put(IDENTIFICACAO_REGISTRO, Integer.valueOf("0"));
		dados.put("operacao", Integer.valueOf("1"));
		dados.put("literalRemessa", "REMESSA");
		dados.put("codigoServico", Integer.valueOf("01"));
		dados.put("literalServico", "COBRANCA");
		dados.put(NOME_EMPRESA, dadosEmpresa.get(NOME_EMPRESA));
		dados.put(NUMERO_INSCRICAO_EMPRESA, dadosEmpresa.get(NUMERO_INSCRICAO_EMPRESA));
		dados.put("codigoBanco",
						Integer.valueOf(cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getArrecadadorContrato()
										.getArrecadador().getCodigoAgente()));
		if(cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getArrecadadorContrato().getArrecadador().getBanco() != null) {
			dados.put("nomeBanco", cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getArrecadadorContrato().getArrecadador()
							.getBanco().getNome());
		} else {
			dados.put("nomeBanco", cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getArrecadadorContrato().getArrecadador()
							.getCliente().getNome());
		}
		dados.put("numeroSequencialRegistro", Integer.valueOf("1"));

		dados.put("dataArquivo", new Date());
		dados.put("horaArquivo", new Date());

		return dados;
	}

	/**
	 * Obter nome empresa.
	 * 
	 * @param serviceLocator
	 *            the service locator
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private static Map<String, Object> obterNomeEmpresa(ServiceLocator serviceLocator) throws GGASException {

		String nomeEmpresa = null;
		Empresa empresa = null;
		String cnpj = null;

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) serviceLocator
						.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("principal", true);

		Collection<Empresa> empresas = controladorEmpresa.consultarEmpresas(filtro);

		if(empresas != null && !empresas.isEmpty()) {
			if(empresas.size() > 1) {
				throw new GGASException("Erro ao tentar recuperar empresa principal. Mais de uma encontrada.");
			} else {
				empresa = empresas.iterator().next();
				nomeEmpresa = empresa.getCliente().getNome();
				cnpj = empresa.getCliente().getCnpj();
				filtro.clear();
				filtro.put(NOME_EMPRESA, nomeEmpresa);
				filtro.put(NUMERO_INSCRICAO_EMPRESA, cnpj);
			}

		} else {
			throw new GGASException("Não foi possível recuperar a empresa principal.");
		}

		return filtro;
	}

	/**
	 * Gets the 1.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 1
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get1(BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = new HashMap<>();
		Map<String, Object> dadosEmpresa = obterNomeEmpresa(serviceLocator);

		dados.put(IDENTIFICACAO_REGISTRO, Integer.valueOf("1"));
		dados.put(NOME_EMPRESA, dadosEmpresa.get(NOME_EMPRESA));
		dados.put(NUMERO_INSCRICAO_EMPRESA, dadosEmpresa.get(NUMERO_INSCRICAO_EMPRESA));
		dados.put("tipoServico", getCodigoTipoConvenio(cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getTipoConvenio()));

		dados.put("dataArquivo", new Date());
		dados.put("dataCredito", new Date());

		return dados;
	}

	/**
	 * Gets the 3.
	 * 
	 * @param somatorio {@link BigDecimal}
	 * @param cobrancaBancariaMovimento {@link CobrancaBancariaMovimento}
	 * @param documentoRemessa {@link DocumentoLayoutRegistro}
	 * @param serviceLocator {@link ServiceLocator}
	 * @return dados {@link Map}
	 */
	public static Map<String, Object> get3(BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) {

		Map<String, Object> dados = new HashMap<>();

		DocumentoCobranca documentoCobranca = cobrancaBancariaMovimento.getDocumentoCobranca();
		Cliente cliente = documentoCobranca.getCliente();
		ClienteEndereco clienteEndereco = cliente.getEnderecoPrincipal();
		Cep cep = clienteEndereco.getCep();
		String[] arrayCep = cep.getCep().split("-");

		/**
		 * SEGMENTO P
		 */
		dados.put("modalidadeCarteira", "14");
		dados.put(LOTE_SERVICO, "0001");
		dados.put("nossoNumero", documentoCobranca.getNossoNumero());
		dados.put("codigoCarteira", "1");
		dados.put("emissaoBloqueto", "2");
		dados.put("entregaBloqueto", "2");
		dados.put("numeroDocumento", documentoCobranca.getChavePrimaria());
		dados.put("formaCadastroTitulo",
						getCodigoTipoConvenio(cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getTipoConvenio()));

		dados.put("vencimento", documentoCobranca.getDataVencimento());
		dados.put("valorTitulo", documentoCobranca.getValorTotal());
		dados.put("especieTitulo", "18");
		dados.put("tituloAceitoNaoAceito", "A");
		dados.put("dataEmissaoTitulo", documentoCobranca.getDataEmissao());
		dados.put("codigoJurosMora", "3");
		dados.put("codigoDesconto", "0");
		dados.put("identificacaoTituloEmpresa", documentoCobranca.getChavePrimaria());
		dados.put("codigoProtesto", "3");
		dados.put("numeroDiasProtesto", "00");
		dados.put("numeroDiasBaixaDevolucao", "090");
		dados.put("codigoBaixaDevolucao", "1");
		/**
		 * SEGMENTO P
		 */

		/**
		 * DADOS SEGMENTO Q
		 */
		dados.put("codigoMovimento", "01");
		dados.put("nome", cliente.getNome());
		dados.put("endereco", cep.getTipoLogradouro() + " " + cep.getLogradouro());
		dados.put("cidade", cep.getNomeMunicipio());
		dados.put("bairro", cep.getBairro());
		dados.put("cep", arrayCep[0]);
		dados.put("sufixoCep", arrayCep[1]);
		dados.put("uf", cep.getUf());

		if(cliente.getCnpj() != null) {
			dados.put("numeroInscricao", cliente.getCnpj());
			dados.put("tipoInscricaoSacado", "2");
			dados.put("numeroInscricaoSacador", cliente.getCnpj());
			dados.put("tipoInscricaoSacador", "2");
		} else {
			dados.put("numeroInscricao", cliente.getCpf());
			dados.put("tipoInscricaoSacado", "1");
			dados.put("numeroInscricaoSacador", cliente.getCpf());
			dados.put("tipoInscricaoSacador", "1");
		}

		dados.put("nomeSacador", cliente.getNome());
		/**
		 * DADOS SEGMENTO Q
		 */

		return dados;
	}

	/**
	 * Gets the 5.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 5
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get5(int totalRegistros, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator){

		Map<String, Object> dados = new HashMap<>();

		dados.put(IDENTIFICACAO_REGISTRO, Integer.valueOf("5"));
		dados.put(LOTE_SERVICO, "0001");
		dados.put("quantidadeLote", totalRegistros - 1);
		dados.put("quantidadeCobranca", totalRegistros - LIMITADOR_REGISTROS);

		return dados;
	}

	/**
	 * Gets the 9.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 9
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get9(int totalRegistros, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = new HashMap<>();
		dados.put(IDENTIFICACAO_REGISTRO, Integer.valueOf("9"));
		dados.put("complementoRegistro", StringUtils.EMPTY);
		dados.put("numeroSequencial", Integer.valueOf("1"));
		dados.put(LOTE_SERVICO, "9999");
		dados.put("quantidadeLote", (totalRegistros - SEGUNDO_LIMITADOR_REGISTROS) / DIVISOR);
		dados.put("quantidadeRegistros", totalRegistros);

		return dados;
	}

	/**
	 * Gets the codigo tipo convenio.
	 * 
	 * @param tipoConvenio
	 *            the tipo convenio
	 * @return the codigo tipo convenio
	 */
	private static String getCodigoTipoConvenio(EntidadeConteudo tipoConvenio) {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		StringBuilder stringBuilder = new StringBuilder();

		String codigoCobrancaRegistrada = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_COBRANCA_REGISTRADA);

		String codigoCobrancaNaoRegistrada = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_NAO_COBRANCA_REGISTRADA);

		if(codigoCobrancaRegistrada != null && tipoConvenio.getChavePrimaria() == Long.parseLong(codigoCobrancaRegistrada)) {
			stringBuilder.append("1");
		}

		if(codigoCobrancaNaoRegistrada != null && tipoConvenio.getChavePrimaria() == Long.parseLong(codigoCobrancaNaoRegistrada)) {
			stringBuilder.append("2");
		}

		return stringBuilder.toString();
	}

}
