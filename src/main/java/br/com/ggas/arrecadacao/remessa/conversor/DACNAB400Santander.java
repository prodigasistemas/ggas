/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.remessa.conversor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.boletobancario.codigobarras.DigitoAutoConferencia;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.BigDecimalUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe DACNAB400Santander
 *
 * @author ProdigaSistemas
 */
public class DACNAB400Santander extends DACNAB400 {
	
	private static final String CAMPO_MENSAGEM = "mensagem";
	private static final String CAMPO_BRANCOS = "brancos";
	
	private static final int TAMANHO_CEP = 5;
	private static final int TAMANHO_CONTA_COBRANCA_PARTE_UM = 8;
	private static final int TAMANHO_AGENCIA = 4;
	
	private static final int CODIGO_CNPJ = 2;
	private static final int CODIGO_MULTA = 4;
	private static final int ESPECIE_TITULO = 1;
	private static final String IDENTIFICACAO_ACEITE = "N";
	private static final String IDENTIFICADOR_DO_COMPLEMENTO = "I";
	private static final int DIVISOR_MORA = 30;
	private static final int MULTIPLICADOR_MORA = 100;

	/**
	 * Gets the 0.
	 *
	 * @param totalRegistros            the total registros
	 * @param somatorio                 the somatorio
	 * @param cobrancaBancariaMovimento the cobranca bancaria movimento
	 * @param documentoRemessa          the documento remessa
	 * @param serviceLocator            the service locator
	 * @return the 0
	 * @throws GGASException the GGAS exception
	 */
	public static Map<String, Object> get0(int totalRegistros, BigDecimal somatorio,
			CobrancaBancariaMovimento cobrancaBancariaMovimento, DocumentoLayoutRegistro documentoRemessa,
			ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB400.get0(totalRegistros, somatorio, cobrancaBancariaMovimento,
				documentoRemessa, serviceLocator);

		dados.put("numeroAutCodTransmissao", obterCodigoTransmissao(cobrancaBancariaMovimento));

		if (cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getArrecadadorContrato().getArrecadador().getBanco() != null) {
			dados.put("nomeBanco", cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getArrecadadorContrato()
					.getArrecadador().getBanco().getNomeAbreviado());
		}

		dados.put("dataGravacao", Calendar.getInstance().getTime());
		dados.put("zeros", BigDecimal.ZERO);
		dados.put(CAMPO_MENSAGEM, StringUtils.EMPTY);
		dados.put(CAMPO_MENSAGEM, StringUtils.EMPTY);
		dados.put(CAMPO_MENSAGEM, StringUtils.EMPTY);
		dados.put(CAMPO_MENSAGEM, StringUtils.EMPTY);
		dados.put(CAMPO_MENSAGEM, StringUtils.EMPTY);
		dados.put(CAMPO_BRANCOS, StringUtils.EMPTY);
		dados.put(CAMPO_BRANCOS, StringUtils.EMPTY);
		dados.put("versaoLeiaute", BigDecimal.ZERO);

		return dados;
	}

	/**
	 * Gets the 1.
	 *
	 * @param totalRegistros            the total registros
	 * @param somatorio                 the somatorio
	 * @param cobrancaBancariaMovimento the cobranca bancaria movimento
	 * @param documentoRemessa          the documento remessa
	 * @param serviceLocator            the service locator
	 * @return the 1
	 * @throws GGASException the GGAS exception
	 */
	public static Map<String, Object> get1(int totalRegistros, BigDecimal somatorio,
			CobrancaBancariaMovimento cobrancaBancariaMovimento, DocumentoLayoutRegistro documentoRemessa,
			ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB400.get1(
				totalRegistros, somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);

		DocumentoCobranca documentoCobranca = obterDocumentoCobranca(cobrancaBancariaMovimento, serviceLocator);
		String agencia = obterAgencia(cobrancaBancariaMovimento);
		Cliente cliente = documentoCobranca.getCliente();
		Contrato contrato = obterContrato(documentoCobranca); 
				
		dados.put("tipoInscricaoEmpresa", CODIGO_CNPJ);
		dados.put("numeroInscricaoEmpresa", obterInscricaoEmpresa(serviceLocator));
		dados.put("agenciaMantenedoraConta", agencia);
		dados.put("contaCorrenteEmpresa", obterContaMovimento(cobrancaBancariaMovimento));
		dados.put("contaCobranca", obterContaCobrancaParte1(cobrancaBancariaMovimento));
		dados.put("numeroControleParticipante", StringUtils.EMPTY);
		dados.put("nossoNumero", obterNossoNumeroDigitoVerificador(documentoCobranca.getNossoNumero()));
		dados.put("dataSegundoDesconto", BigDecimal.ZERO);
		dados.put("branco", StringUtils.EMPTY);
		dados.put("codigoMulta", obterIndicadorMulta(contrato));
		dados.put("percentualMulta", obterPercentualMulta(contrato));
		dados.put("valorTituloOutraUnidade", BigDecimal.ZERO);
		dados.put(CAMPO_BRANCOS, StringUtils.EMPTY);
		dados.put("dataMulta", BigDecimal.ZERO);
		dados.put("codigoCarteira", cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getArrecadadorCarteiraCobranca()
				.getCodigoCarteira().getCodigo());
		dados.put("identificacaoOcorrencia", cobrancaBancariaMovimento.getDocumentoLayoutEnvioOcorrencia().getCodigo());
		dados.put("seuNumero", cobrancaBancariaMovimento.getDocumentoCobranca().getChavePrimaria());
		dados.put("dataVencimentoTitulo", documentoCobranca.getDataVencimento());
		dados.put("valorTitulo", documentoCobranca.getValorTotal());
		dados.put("numeroBanco", obterNumeroBanco(cobrancaBancariaMovimento));
		dados.put("codigoAgenciaCobradora", obterAgenciaCobradora(agencia));
		dados.put("especieTitulo", ESPECIE_TITULO);
		dados.put("identificacaoAceite", IDENTIFICACAO_ACEITE);
		dados.put("dataEmissaoTitulo", documentoCobranca.getDataEmissao());
		dados.put("primeiraInstrucaoCobranca", BigDecimal.ZERO);
		dados.put("segundaInstrucaoCobranca", BigDecimal.ZERO);
		dados.put("valorMoraDiaAtraso", obterValorMora(contrato, documentoCobranca.getValorTotal()));
		dados.put("dataLimiteConcessaoDesconto", BigDecimal.ZERO);
		dados.put("valorDescontoConcedido", BigDecimal.ZERO);
		dados.put("valorIOF", BigDecimal.ZERO);
		dados.put("valorAbatimentoConcedido", BigDecimal.ZERO);
		
		dados.put("identificacaoTipoInscricao", documentoCobranca.getCliente().getTipoCliente().getTipoPessoa().getCodigo());
		dados.put("numeroInscricaoSacado", obterNumeroInscricaoSacado(cliente));
		dados.put("nomeSacado", cliente.getNome());
		
		ClienteEndereco endereco = cliente.getEnderecoPrincipal();
		if (endereco != null) {
			dados.put("ruaNumeroComplementoSacado", endereco.getEnderecoFormatadoRuaNumeroComplemento());
			dados.put("bairroSacado", endereco.getCep().getBairro());
			dados.put("cepSacado", Util.removerCaracteresEspeciais(endereco.getCep().getCep()).substring(0, TAMANHO_CEP));
			dados.put("sufixoCep", Util.removerCaracteresEspeciais(endereco.getCep().getCep()).substring(TAMANHO_CEP));
			dados.put("cidadeSacado", endereco.getCep().getNomeMunicipio());
			dados.put("ufSacado", endereco.getCep().getUf());
		} else {
			dados.put("ruaNumeroComplementoSacado", StringUtils.EMPTY);
			dados.put("bairroSacado", StringUtils.EMPTY);
			dados.put("cepSacado", BigDecimal.ZERO);
			dados.put("sufixoCep", BigDecimal.ZERO);
			dados.put("cidadeSacado", StringUtils.EMPTY);
			dados.put("ufSacado", StringUtils.EMPTY);
		}
		
		dados.put("nomeSacador", StringUtils.EMPTY);
		dados.put(CAMPO_BRANCOS, StringUtils.EMPTY);
		dados.put("identificadorComplemento", IDENTIFICADOR_DO_COMPLEMENTO);
		dados.put("complementoRegistro", obterContaCobrancaParte2(cobrancaBancariaMovimento));
		dados.put(CAMPO_BRANCOS, StringUtils.EMPTY);
		dados.put("numeroDiasProtesto", BigDecimal.ZERO);
		dados.put(CAMPO_BRANCOS, StringUtils.EMPTY);
		dados.put("sequencialRegistro", totalRegistros);

		return dados;
	}

	private static DocumentoCobranca obterDocumentoCobranca(CobrancaBancariaMovimento cobrancaBancariaMovimento,
			ServiceLocator serviceLocator) throws NegocioException {
		
		ControladorCobranca controladorCobranca = (ControladorCobranca) serviceLocator.getBeanPorID(
				ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
		
		return controladorCobranca.obterDocumentoCobranca(cobrancaBancariaMovimento.getDocumentoCobranca().getChavePrimaria());
	}
	
	/**
	 * Gets the 9.
	 *
	 * @param totalRegistros            the total registros
	 * @param somatorio                 the somatorio
	 * @param cobrancaBancariaMovimento the cobranca bancaria movimento
	 * @param documentoRemessa          the documento remessa
	 * @param serviceLocator            the service locator
	 * @return the 9
	 * @throws GGASException the GGAS exception
	 */
	public static Map<String, Object> get9(int totalRegistros,BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
				    DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {	
		
		Map<String, Object> dados = DACNAB400.get9(totalRegistros, somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);
		
		dados.put("valorTotalRegistrosArquivo", somatorio);
		dados.put("zeros", BigDecimal.ZERO);

		return dados;
	}

	private static String obterCodigoTransmissao(CobrancaBancariaMovimento cobrancaBancariaMovimento) {
		return obterAgencia(cobrancaBancariaMovimento) +
			   obterContaMovimento(cobrancaBancariaMovimento) + 
			   obterContaCobrancaParte1(cobrancaBancariaMovimento);
	}
	
	private static String obterInscricaoEmpresa(ServiceLocator serviceLocator) throws GGASException {
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) serviceLocator.getBeanPorID(
				ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("principal", true);

		Collection<Empresa> empresas = controladorEmpresa.consultarEmpresas(filtro);
		Empresa empresa = null;
		if (empresas != null && !empresas.isEmpty()) {
			if (empresas.size() > 1) {
				throw new GGASException("Erro ao tentar recuperar empresa principal. Mais de uma encontrada.");
			} else {
				empresa = empresas.iterator().next();
				return empresa.getCliente().getCnpj();
			}
		} else {
			throw new GGASException("Não foi possível recuperar a empresa principal.");
		}
	}
	
	private static String obterAgencia(CobrancaBancariaMovimento cobrancaBancariaMovimento) {
		return cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getContaConvenio().getAgencia().getCodigo();
	}
	
	private static String obterContaMovimento(CobrancaBancariaMovimento cobrancaBancariaMovimento) {
		ContaBancaria contaBancaria = cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getContaConvenio();
		return contaBancaria.getNumeroConta() + contaBancaria.getNumeroDigito();
	}
	
	private static String obterContaCobrancaParte1(CobrancaBancariaMovimento cobrancaBancariaMovimento) {
		String contaCobranca = cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getContaConvenio().getNumeroContaCobranca();
		
		if (contaCobranca != null && !contaCobranca.equals(StringUtils.EMPTY)) {
			if (contaCobranca.length() >= TAMANHO_CONTA_COBRANCA_PARTE_UM) {
				return contaCobranca.substring(0, TAMANHO_CONTA_COBRANCA_PARTE_UM);
			} else {
				return contaCobranca.substring(0, contaCobranca.length());
			}
		} else {
			return "0";
		}
	}
	
	private static String obterContaCobrancaParte2(CobrancaBancariaMovimento cobrancaBancariaMovimento) {
		String contaCobranca = cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getContaConvenio().getNumeroContaCobranca();
		
		if (contaCobranca != null && !"".equals(contaCobranca)) {
			if (contaCobranca.length() >= TAMANHO_CONTA_COBRANCA_PARTE_UM) {
				return contaCobranca.substring(TAMANHO_CONTA_COBRANCA_PARTE_UM, contaCobranca.length());
			} else {
				return "0";
			}
		} else {
			return "0";
		}
	}

	private static String obterNumeroBanco(CobrancaBancariaMovimento cobrancaBancariaMovimento) {
		Banco banco = cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getArrecadadorContrato().getArrecadador().getBanco();
		if (banco != null) {
			return banco.getCodigoBanco();
		} else {
			return StringUtils.EMPTY;
		}
	}
	
	private static String obterAgenciaCobradora(String agencia) {
		if (agencia.length() == TAMANHO_AGENCIA) {
			agencia += BigDecimal.ZERO;
		}
		
		return agencia;
	}
	
	private static String obterNumeroInscricaoSacado(Cliente cliente) {
		if (cliente.isPessoaFisica()) {
			return cliente.getCpf();
		} else if (cliente.isPessoaJuridica()) {
			return cliente.getCnpj();
		} else {
			return String.valueOf(BigDecimal.ZERO);
		}
	}
	
	private static Contrato obterContrato(DocumentoCobranca documentoCobranca) {
		if (!Util.isNullOrEmpty(documentoCobranca.getItens())) {
			FaturaGeral faturaGeral = documentoCobranca.getItens().iterator().next().getFaturaGeral();

			if (faturaGeral != null) {
				return documentoCobranca.getItens().iterator().next().getFaturaGeral().getFaturaAtual().getContratoAtual();
			}
		}

		return null;
	}
	
	private static int obterIndicadorMulta(Contrato contrato) {
		if (contrato != null && contrato.getIndicadorMulta()) {
			return CODIGO_MULTA; 
		} else {
			return 0;
		}
	}
	
	private static BigDecimal obterPercentualMulta(Contrato contrato) {
		if (contrato != null && contrato.getIndicadorMulta()) {
			return contrato.getPercentualMulta().multiply(BigDecimalUtil.CEM);
		} else {
			return BigDecimal.ZERO;
		}
	}
	
	private static String obterNossoNumeroDigitoVerificador(Long nossoNumero) {
		String nossoNumeroConvertido = String.valueOf(nossoNumero);
		String dacNossoNumero = String.valueOf(DigitoAutoConferencia.modulo11(nossoNumero.toString()));
		return nossoNumeroConvertido + dacNossoNumero;
	}
	
	private static int obterValorMora(Contrato contrato, BigDecimal valorTitulo) {
		DecimalFormat df = new DecimalFormat("0.##");
		df.setRoundingMode(RoundingMode.DOWN);
		
		if(contrato != null && contrato.getPercentualJurosMora() != null) {
			Double valorMora = contrato.getPercentualJurosMora().doubleValue();
			Double valorTotalTitulo = valorTitulo.doubleValue();
			Double valorTotalMora = (valorTotalTitulo * valorMora) / DIVISOR_MORA;
			
			return (int) Math.round(Double.valueOf(df.format(valorTotalMora)) * MULTIPLICADOR_MORA);
		}
		
		return 0;
	}
}
