/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.remessa.conversor;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.DebitoAutomaticoMovimento;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe DACNAB150HSBC
 * 
 * @author Procenge
 */
public class DACNAB150HSBC extends DACNAB150 {

	/**
	 * Gets the a.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the a
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getA(BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB150.getA(somatorio, debitoAutomaticoMovimento, documentoRemessa, serviceLocator);
		dados.put("branco", StringUtils.EMPTY);

		return dados;
	}

	/**
	 * Gets the e.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the e
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getE(int totalRegistros, BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB150.getE(totalRegistros, somatorio, debitoAutomaticoMovimento, documentoRemessa, serviceLocator);

		/*
		 * E.05 - Identificação do cliente
		 * consumidor no Banco.
		 * Identificação utilizada pelo Banco para
		 * efetuar o débito (número da conta
		 * corrente)
		 * - Posição 031 a 033 - Conteúdo: 399
		 * fixo
		 * - Posição 034 a 037 - Conteúdo: Código
		 * da agência, sem os dígitos
		 * verificadores,
		 * mantendo apenas as quatro primeiras
		 * posições identificadoras da agência
		 * - Posição 038 a 044 - Conteúdo: Nr. da
		 * conta corrente (com DV).
		 */
		dados.put("idClienteConsumidorCredor", debitoAutomaticoMovimento.getDebitoAutomatico().getClienteDebitoAutomatico().getCliente()
						.getChavePrimaria());

		dados.put("branco1", StringUtils.EMPTY);
		dados.put("usoClienteCredor", StringUtils.EMPTY);
		dados.put("CPFCNPJDevedor", StringUtils.EMPTY);
		dados.put("valorBaseCalculoIOF", dados.get("valorDebito"));
		dados.put("tipoDebito", "43");
		// "Anexo 1 - 43 Gás"
		dados.put("aliquotaIOF", Integer.valueOf(0));
		dados.put("branco2", StringUtils.EMPTY);

		return dados;

	}

	/**
	 * Gets the z.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the z
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getZ(int totalRegistros, BigDecimal somatorio,
			DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB150.getZ(totalRegistros, somatorio, documentoRemessa, serviceLocator);
		dados.put("valorTotalIOFRecolhido", StringUtils.EMPTY);
		// somente
		// no
		// retorno
		dados.put("branco", StringUtils.EMPTY);

		return dados;
	}

	/**
	 * Gets the b.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the b
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getB(BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		return DACNAB150.getB(somatorio, debitoAutomaticoMovimento, documentoRemessa, serviceLocator);
	}
}
