package br.com.ggas.arrecadacao.remessa.conversor;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.DebitoAutomaticoMovimento;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe DACNAB400BancoBrasil
 * 
 * @author Procenge
 */
public class DACNAB400BancoBrasil extends DACNAB400{
	
	/**
	 * Gets the e.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the e
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getE(int totalRegistros, BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB150.getE(totalRegistros, somatorio, debitoAutomaticoMovimento, documentoRemessa, serviceLocator);
		dados.put("reservadoFuturo", StringUtils.EMPTY);

		return dados;
	}

	/**
	 * Gets the b.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the b
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getB(BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		return DACNAB150.getB(somatorio, debitoAutomaticoMovimento, documentoRemessa, serviceLocator);
	}

}
