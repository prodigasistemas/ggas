/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.remessa.conversor;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorMovimento;
import br.com.ggas.arrecadacao.DebitoAutomaticoMovimento;
import br.com.ggas.arrecadacao.DebitoNaoProcessado;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe DACNAB150Itau
 * 
 * @author Procenge
 */
public class DACNAB150Itau extends DACNAB150 {

	private static final String COMPLEMENTO_REGISTRO_1 = "complementoRegistro1";

	private static final String COMPLEMENTO_REGISTRO_2 = "complementoRegistro2";

	/**
	 * Gets the a.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the a
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getA(BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB150.getA(somatorio, debitoAutomaticoMovimento, documentoRemessa, serviceLocator);

		dados.put(COMPLEMENTO_REGISTRO_1, StringUtils.EMPTY);
		dados.put(COMPLEMENTO_REGISTRO_2, StringUtils.EMPTY);

		return dados;
	}

	// Só será usado ao gerar um registro do tipo
	/**
	 * Gets the a.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param debitoNaoProcessado
	 *            the debito nao processado
	 * @param documentoLayout
	 *            the documento layout
	 * @param arrecadadorMovimento
	 *            the arrecadador movimento
	 * @param arrecadador - {@link Arrecadador}
	 * @param serviceLocator
	 *            the service locator
	 * @return the a
	 * @throws GGASException
	 *             the GGAS exception
	 */
	// C
	public static Map<String, Object> getA(BigDecimal somatorio, DebitoNaoProcessado debitoNaoProcessado,
					DocumentoLayoutRegistro documentoLayout, ArrecadadorMovimento arrecadadorMovimento, Arrecadador arrecadador,
					ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados =
						DACNAB150.getA(somatorio, debitoNaoProcessado, documentoLayout, arrecadadorMovimento, arrecadador,
										serviceLocator);

		dados.put(COMPLEMENTO_REGISTRO_1, StringUtils.EMPTY);
		dados.put(COMPLEMENTO_REGISTRO_2, StringUtils.EMPTY);
		dados.put("complementoRegistro3", StringUtils.EMPTY);

		return dados;

	}

	/**
	 * Gets the e.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @param debitoAutomaticoMovimento - {@link DebitoAutomaticoMovimento}
	 * 
	 * @return the e
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getE(int totalRegistros, BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB150.getE(totalRegistros, somatorio, debitoAutomaticoMovimento, documentoRemessa, serviceLocator);

		dados.put(COMPLEMENTO_REGISTRO_1, StringUtils.EMPTY);

		dados.put("numeroContaDebitada", debitoAutomaticoMovimento.getDebitoAutomatico().getConta());
		dados.put("usoReservadoEmpresa", StringUtils.EMPTY);
		dados.put("valorEncargoDiaAtraso", StringUtils.EMPTY);
		dados.put("informacaoComplementarCC", StringUtils.EMPTY);
		dados.put(COMPLEMENTO_REGISTRO_2, StringUtils.EMPTY);

		if (debitoAutomaticoMovimento.getDebitoAutomatico().getClienteDebitoAutomatico().getCliente().getCpf() != null) {
			dados.put("numeroInscricaoDebitado", Long.valueOf(debitoAutomaticoMovimento.getDebitoAutomatico().getClienteDebitoAutomatico()
							.getCliente().getCpf()));
		} else if (debitoAutomaticoMovimento.getDebitoAutomatico().getClienteDebitoAutomatico().getCliente().getCnpj() != null) {
			dados.put("numeroInscricaoDebitado",
							Long.valueOf(debitoAutomaticoMovimento.getDebitoAutomatico().getClienteDebitoAutomatico().getCliente()
											.getCnpj()));
		}

		return dados;
	}

	/**
	 * Gets the b.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the b
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getB(BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB150.getB(somatorio, debitoAutomaticoMovimento, documentoRemessa, serviceLocator);

		dados.put("numeroAgenciaDebitada", StringUtils.EMPTY);
		dados.put(COMPLEMENTO_REGISTRO_1, StringUtils.EMPTY);
		dados.put("numeroContaDebitada", StringUtils.EMPTY);
		dados.put("digitoVerificadorAgConta", StringUtils.EMPTY);
		dados.put(COMPLEMENTO_REGISTRO_2, StringUtils.EMPTY);

		return dados;
	}
}
