/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.remessa.conversor;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorMovimento;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.DebitoAutomaticoMovimento;
import br.com.ggas.arrecadacao.DebitoNaoProcessado;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cobranca.boletobancario.codigobarras.DigitoAutoConferencia;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe DACNAB150
 * 
 * @author Procenge
 */
public class DACNAB150 {

	private static final int TIPO_PESSOA_FISICA = 2;
	private static final String CODIGO_MOVIMENTO = "codigoMovimento";
	private static final String ID_CLIENTE_BANCO = "idClienteBanco";
	private static final String AGENCIA_DEBITO = "agenciaDebito";
	private static final String ID_CLIENTE_EMPRESA = "idClienteEmpresa";
	private static final String RESERVADO_FUTURO = "reservadoFuturo";
	private static final String NOME_BANCO = "nomeBanco";
	private static final String CODIGO_REGISTRO = "codigoRegistro";
	private static final int VERSAO_LEIAUTE = 4;

	/**
	 * Gets the a.
	 * 
	 * @param totalRegistros the total registros
	 * @param somatorio the somatorio
	 * @param debitoAutomaticoMovimento the debito automatico movimento
	 * @param documentoRemessa the documento remessa
	 * @param serviceLocator the service locator
	 * @return the a
	 * @throws GGASException the GGAS exception
	 */
	public static Map<String, Object> getA(BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = new HashMap<>();

		dados.put(CODIGO_REGISTRO, documentoRemessa.getIdentificadorRegistro());
		dados.put("codigoRemessa", Integer.valueOf(1));
		setarCodigoConvenioNoMapa(debitoAutomaticoMovimento, documentoRemessa, serviceLocator, dados);
		setarNomeEmpresaNoMapa(serviceLocator, dados);
		dados.put("codigoBanco", Integer.valueOf(debitoAutomaticoMovimento.getDebitoAutomatico().getArrecadador().getCodigoAgente()));

		if (debitoAutomaticoMovimento.getDebitoAutomatico().getArrecadador().getBanco() != null) {
			dados.put(NOME_BANCO, debitoAutomaticoMovimento.getDebitoAutomatico().getArrecadador().getBanco().getNome());
		} else {
			dados.put(NOME_BANCO, debitoAutomaticoMovimento.getDebitoAutomatico().getArrecadador().getCliente().getNome());
		}

		dados.put("dataArquivo", Calendar.getInstance().getTime());
		setarNSAArrecadadorNoMapa(debitoAutomaticoMovimento.getDebitoAutomatico().getArrecadador().getChavePrimaria(), documentoRemessa,
						serviceLocator, dados);

		// Esse campo pode ser especificado como
		// padrão no atributo de remessa
		dados.put("versaoLeiaute", Integer.valueOf(VERSAO_LEIAUTE));

		// Esse campo pode ser especificado como
		// padrão no atributo de remessa
		dados.put("identificacaoServico", "DEBITO AUTOMATICO");

		// Esse campo não será utilizado por
		// enquanto
		dados.put(RESERVADO_FUTURO, StringUtils.EMPTY);

		return dados;
	}

	/**
	 * Setar codigo convenio no mapa.
	 * 
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @param dados
	 *            the dados
	 * @throws GGASException 
	 */
	private static void setarCodigoConvenioNoMapa(DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator, Map<String, Object> dados)
					throws GGASException {

		ControladorArrecadacao controladorArrecadacao =
						(ControladorArrecadacao) serviceLocator.getBeanPorID(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);

		String codigoConvenio =
						controladorArrecadacao.obterCodigoConvenio(debitoAutomaticoMovimento.getDebitoAutomatico().getArrecadador()
										.getChavePrimaria(), documentoRemessa.getLeiaute().getFormaArrecadacao().getChavePrimaria());

		dados.put("codigoConvenio", codigoConvenio);

	}

	/**
	 * Setar nsa arrecadador no mapa.
	 * 
	 * @param chaveArrecadador
	 *            the chave arrecadador
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private static void setarNSAArrecadadorNoMapa(long chaveArrecadador, DocumentoLayoutRegistro documentoRemessa,
					ServiceLocator serviceLocator, Map<String, Object> dados) throws GGASException {

		ControladorArrecadacao controladorArrecadacao =
						(ControladorArrecadacao) serviceLocator.getBeanPorID(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);

		Long nsaRemessa =
						controladorArrecadacao.gerarNSAArrecadador(chaveArrecadador, documentoRemessa.getLeiaute().getFormaArrecadacao()
										.getChavePrimaria());

		dados.put("sequencialArquivo", nsaRemessa);
	}

	/**
	 * Setar nome empresa no mapa.
	 * 
	 * @param serviceLocator
	 *            the service locator
	 * @param dados
	 *            the dados
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private static void setarNomeEmpresaNoMapa(ServiceLocator serviceLocator, Map<String, Object> dados) throws GGASException {

		ControladorEmpresa controladorEmpresa =
						(ControladorEmpresa) serviceLocator.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("principal", true);

		Collection<Empresa> empresas = controladorEmpresa.consultarEmpresas(filtro);
		Empresa empresa = null;

		if (empresas != null && !empresas.isEmpty()) {
			if (empresas.size() > 1) {
				throw new GGASException("Erro ao tentar recuperar empresa principal. Mais de uma encontrada.");
			} else {
				empresa = empresas.iterator().next();
				dados.put("nomeEmpresa", empresa.getCliente().getNome());
			}

		} else {
			throw new GGASException("Não foi possível recuperar a empresa principal.");
		}
	}

	/**
	 * Gets the e.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the e
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getE(int totalRegistros, BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		ControladorCliente controladorCliente =
						(ControladorCliente) serviceLocator.getBeanPorID(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

		Map<String, Object> dados = new HashMap<>();

		Long identificadorClienteDebito = debitoAutomaticoMovimento.getDebitoAutomatico().getClienteDebitoAutomatico().getChavePrimaria();
		String codigoCliente =
						String.valueOf(identificadorClienteDebito).concat(
										String.valueOf(DigitoAutoConferencia.modulo10(String.valueOf(identificadorClienteDebito))));

		Cliente cliente =
						(Cliente) controladorCliente.obter(debitoAutomaticoMovimento.getDebitoAutomatico().getClienteDebitoAutomatico()
										.getCliente().getChavePrimaria());

		dados.put(CODIGO_REGISTRO, documentoRemessa.getIdentificadorRegistro());
		dados.put(ID_CLIENTE_EMPRESA, codigoCliente);
		dados.put(AGENCIA_DEBITO, debitoAutomaticoMovimento.getDebitoAutomatico().getAgencia());
		dados.put(ID_CLIENTE_BANCO, debitoAutomaticoMovimento.getDebitoAutomatico().getConta());
		dados.put("valorDebito", debitoAutomaticoMovimento.getValorDebito());
		dados.put("dataVencimento", debitoAutomaticoMovimento.getDataDebito());

		dados.put("identificacao", totalRegistros);


		// Esse campo pode ser especificado como
		// padrão no atributo de remessa
		dados.put("codigoMoeda", "03");

		// Código do documento cobrança, ele será
		// utilizado na identificação do pagamento
		// no retorno do arquivo (not null)
		dados.put("usoEmpresa", debitoAutomaticoMovimento.getDocumentoCobranca().getChavePrimaria());

		// Esse campo não será utilizado por
		// enquanto
		dados.put(RESERVADO_FUTURO, StringUtils.EMPTY);

		dados.put(CODIGO_MOVIMENTO, debitoAutomaticoMovimento.getCodigoMovimento());

		return dados;
	}

	/**
	 * Gets the z.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the z
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getZ(int totalRegistros, BigDecimal somatorio, DocumentoLayoutRegistro documentoRemessa,
			ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = new HashMap<>();

		dados.put(CODIGO_REGISTRO, "Z");
		dados.put("totalRegistrosArquivo", Integer.valueOf(totalRegistros));
		dados.put("valorTotalRegistrosArquivo", somatorio);
		dados.put(RESERVADO_FUTURO, StringUtils.EMPTY);

		return dados;
	}

	/**
	 * Gets the b.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param debitoAutomaticoMovimento
	 *            the debito automatico movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the b
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getB(BigDecimal somatorio, DebitoAutomaticoMovimento debitoAutomaticoMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = new HashMap<>();

		dados.put(CODIGO_REGISTRO, "B");
		dados.put(ID_CLIENTE_EMPRESA, StringUtils.EMPTY);
		dados.put(AGENCIA_DEBITO, StringUtils.EMPTY);
		dados.put(ID_CLIENTE_BANCO, StringUtils.EMPTY);
		dados.put("dataOpcao", Calendar.getInstance().getTime());
		dados.put(RESERVADO_FUTURO, StringUtils.EMPTY);
		dados.put(CODIGO_MOVIMENTO, Integer.valueOf(0));

		return dados;
	}

	// Cabeçalho que será usado ao retornar para o
	/**
	 * Gets the a.
	 *
	 * @param somatorio            the somatorio
	 * @param debitoNaoProcessado            the debito nao processado
	 * @param documentoLayoutRegistro the documento layout registro
	 * @param arrecadadorMovimento            the arrecadador movimento
	 * @param arrecadador the arrecadador
	 * @param serviceLocator            the service locator
	 * @return the a
	 * @throws GGASException             the GGAS exception
	 */
	// banco um tipo 'C'
	public static Map<String, Object> getA(BigDecimal somatorio, DebitoNaoProcessado debitoNaoProcessado,
					DocumentoLayoutRegistro documentoLayoutRegistro, ArrecadadorMovimento arrecadadorMovimento, Arrecadador arrecadador,
					ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = new HashMap<>();

		dados.put(CODIGO_REGISTRO, documentoLayoutRegistro.getIdentificadorRegistro());
		dados.put("codigoRemessa", Integer.valueOf(1));
		dados.put("codigoConvenio", arrecadadorMovimento.getCodigoConvenio());
		dados.put("nomeEmpresa", arrecadadorMovimento.getNomeEmpresa());
		dados.put("codigoBanco", arrecadadorMovimento.getCodigoBanco());
		dados.put(NOME_BANCO, arrecadador.getBanco().getNome());
		dados.put("dataArquivo", Calendar.getInstance().getTime());
		setarNSAArrecadadorNoMapa(arrecadador.getChavePrimaria(), documentoLayoutRegistro, serviceLocator, dados);
		dados.put("versaoLeiaute", Integer.valueOf(VERSAO_LEIAUTE));
		dados.put("identificacaoServico", "DEBITO AUTOMATICO");

		return dados;

	}

	/**
	 * Gets the c.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param debitoNaoProcessado
	 *            the debito nao processado
	 * @param documentoLayout
	 *            the documento layout
	 * @param arrecadadorMovimento
	 *            the arrecadador movimento
	 * @param serviceLocator
	 *            the service locator
	 * @param arrecadador - {@link ArrecadadorMovimento}
	 * 
	 * @return the c
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> getC(BigDecimal somatorio, DebitoNaoProcessado debitoNaoProcessado,
					DocumentoLayoutRegistro documentoLayout, ArrecadadorMovimento arrecadadorMovimento, Arrecadador arrecadador,
					ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = new HashMap<>();

		dados.put(CODIGO_REGISTRO, "C");
		dados.put(ID_CLIENTE_EMPRESA, debitoNaoProcessado.getIdentificacaoClienteEmpresa());
		dados.put(AGENCIA_DEBITO, debitoNaoProcessado.getAgenciaDebito());
		dados.put(ID_CLIENTE_BANCO, debitoNaoProcessado.getIdentificacaoClienteBanco());

		if (!StringUtils.isEmpty(debitoNaoProcessado.getOcorrencia1())) {
			dados.put("ocorrencia1", debitoNaoProcessado.getOcorrencia1());
		} else {
			dados.put("ocorrencia1", StringUtils.EMPTY);
		}

		if (!StringUtils.isEmpty(debitoNaoProcessado.getOcorrencia2())) {
			dados.put("ocorrencia2", debitoNaoProcessado.getOcorrencia2());
		} else {
			dados.put("ocorrencia2", StringUtils.EMPTY);
		}

		dados.put(RESERVADO_FUTURO, StringUtils.EMPTY);
		dados.put(CODIGO_MOVIMENTO, debitoNaoProcessado.getCodigoMovimento());

		return dados;
	}

}
