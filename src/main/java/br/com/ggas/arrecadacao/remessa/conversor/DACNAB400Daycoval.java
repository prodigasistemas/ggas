/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.remessa.conversor;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.util.Constantes;
import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe DACNAB400Daycoval
 * 
 * @author Topos
 */
public class DACNAB400Daycoval extends DACNAB400 {

	private static final String NUMERO_INSCRICAO_SACADO = "numeroInscricaoSacado";
	private static final int NUMERO_BANCO_CAMARA_COMPENSACAO = 341;
	private static final int TIPO_INSCRICAO_EMPRESA = 7;

	/**
	 * Gets the 0.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 0
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get0(BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB400.get0(0, somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);

		if(cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getContaConvenio() != null) {
			dados.put("agencia", cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getContaConvenio().getAgencia()
							.getChavePrimaria());
			dados.put("contaCorrenteEmpresa",
							Integer.valueOf(cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getContaConvenio().getNumeroConta()
											+ cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getContaConvenio()
															.getNumeroDigito()));
		}
		dados.put("complementoRegistro1", BigDecimal.ZERO);
		dados.put("complementoRegistro2", StringUtils.EMPTY);
		dados.put("dataGeracaoArquivo", Calendar.getInstance().getTime());
		dados.put("complementoRegistro3", StringUtils.EMPTY);

		return dados;
	}

	/**
	 * Gets the 1.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 1
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get1(int totalRegistros, BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB400.get1(totalRegistros, somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);

		DocumentoCobranca documentoCobranca = cobrancaBancariaMovimento.getDocumentoCobranca();

		dados.put("tipoInscricaoEmpresa", Integer.valueOf(TIPO_INSCRICAO_EMPRESA));
		setarInscricaoEmpresaNoMapa(serviceLocator, dados);

		String agencia = cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getContaConvenio().getAgencia().getCodigo();
		agencia = agencia.substring(0, agencia.length() - 1);

		dados.put("agenciaMantenedoraConta", Integer.valueOf(agencia));
		dados.put("contaCorrenteEmpresa",
						Integer.valueOf(cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getContaConvenio().getNumeroConta()));
		dados.put("dac", Integer.valueOf(cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getContaConvenio().getNumeroDigito()));
		dados.put("codigoInstrAlegacaoCancelada", Integer.valueOf(0));
		dados.put("idTituloEmpresa", String.valueOf(documentoCobranca.getChavePrimaria()));
		dados.put("idTituloBanco", documentoCobranca.getNossoNumero());
		dados.put("quantidadeMoedaVariavel", Integer.valueOf(0));
		dados.put("numeroCarteiraBanco", cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getArrecadadorCarteiraCobranca()
						.getNumero());
		dados.put("codigoCarteria", cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getArrecadadorCarteiraCobranca()
						.getCodigoCarteira().getDescricao());
		dados.put("idOperacaoBanco", StringUtils.EMPTY);
		dados.put("idOcorrencia", Integer.valueOf(0));
		dados.put("numeroDocCobranca", String.valueOf(documentoCobranca.getChavePrimaria()));
		dados.put("dataVencimentoTitulo", documentoCobranca.getDataVencimento());
		dados.put("valorNominalTitulo", documentoCobranca.getValorTotal());
		dados.put("numeroBancoCamaraCompensacao", Integer.valueOf(NUMERO_BANCO_CAMARA_COMPENSACAO));
		dados.put("agenciaCobradora", BigDecimal.ZERO);
		dados.put("especieTitulo", "17");
		dados.put("idtituloAceitoNaoAceito", "N");
		dados.put("dataEmissaoTitulo", documentoCobranca.getDataEmissao());
		dados.put("primeiraInstrucaoCobranca", StringUtils.EMPTY);
		dados.put("segundaInstrucaoCobranca", StringUtils.EMPTY);
		dados.put("valorMoraDiaAtraso", BigDecimal.ZERO);
		dados.put("dataLimiteConcessaoDesconto", BigDecimal.ZERO);
		dados.put("valorDescontoConcedido", BigDecimal.ZERO);
		dados.put("valorIOFRecolhidoNotasSeguro", BigDecimal.ZERO);
		dados.put("valorAbatimentoConcedido", BigDecimal.ZERO);
		dados.put("identificacaoTipoInscricao", documentoCobranca.getCliente().getTipoCliente().getTipoPessoa().getCodigo());

		if(documentoCobranca.getCliente().getTipoCliente().getTipoPessoa().getCodigo() == Constantes.PESSOA_FISICA
						&& (documentoCobranca.getCliente().getCpf() != null)) {
			dados.put(NUMERO_INSCRICAO_SACADO, Long.valueOf(documentoCobranca.getCliente().getCpf()));
		} else if(documentoCobranca.getCliente().getTipoCliente().getTipoPessoa().getCodigo() == Constantes.PESSOA_JURIDICA
						&& (documentoCobranca.getCliente().getCnpj() != null)) {
			dados.put(NUMERO_INSCRICAO_SACADO, Long.valueOf(documentoCobranca.getCliente().getCnpj()));
		} else {
			dados.put(NUMERO_INSCRICAO_SACADO, BigDecimal.ZERO);
		}

		dados.put("nomeSacado", documentoCobranca.getCliente().getNome());

		ClienteEndereco endereco = documentoCobranca.getCliente().getEnderecoPrincipal();
		if(endereco != null) {
			dados.put("ruaNumeroComplementoSacado", endereco.getEnderecoFormatado());
			dados.put("bairroSacado", endereco.getCep().getBairro());
			dados.put("cepSacado", Integer.valueOf(Util.removerCaracteresEspeciais(endereco.getCep().getCep())));
			dados.put("cidadeSacado", endereco.getCep().getMunicipio());
			dados.put("ufSacado", endereco.getCep().getUf());
		} else {
			dados.put("ruaNumeroComplementoSacado", StringUtils.EMPTY);
			dados.put("bairroSacado", StringUtils.EMPTY);
			dados.put("cepSacado", BigDecimal.ZERO);
			dados.put("cidadeSacado", StringUtils.EMPTY);
			dados.put("ufSacado", StringUtils.EMPTY);
		}

		dados.put("dataMora", Integer.valueOf(0));
		dados.put("quantidadeDias", Integer.valueOf(0));
		dados.put("numeroSequencial", Integer.valueOf(totalRegistros));

		dados.put("complementoRegistro1", BigDecimal.ZERO);
		dados.put("complementoRegistro2", StringUtils.EMPTY);
		dados.put("complementoRegistro3", StringUtils.EMPTY);
		dados.put("complementoRegistro4", StringUtils.EMPTY);
		dados.put("complementoRegistro5", StringUtils.EMPTY);

		return dados;
	}

	/**
	 * Setar inscricao empresa no mapa.
	 * 
	 * @param serviceLocator
	 *            the service locator
	 * @param dados
	 *            the dados
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private static void setarInscricaoEmpresaNoMapa(ServiceLocator serviceLocator, Map<String, Object> dados) throws GGASException {

		// criar metodo que
		// retorna a empresa principal pois ela é
		// usada em diversos lugares. ex: 150.
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) serviceLocator
						.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("principal", true);

		Collection<Empresa> empresas = controladorEmpresa.consultarEmpresas(filtro);
		Empresa empresa = null;

		if(empresas != null && !empresas.isEmpty()) {
			if(empresas.size() > 1) {
				throw new GGASException("Erro ao tentar recuperar empresa principal. Mais de uma encontrada.");
			} else {
				empresa = empresas.iterator().next();
				dados.put("numeroInscricaoEmpresa", Long.valueOf(empresa.getCliente().getCnpj()));
			}

		} else {
			throw new GGASException("Não foi possível recuperar a empresa principal.");
		}
	}

	/**
	 * Gets the 9.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 9
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get9(int totalRegistros, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		return DACNAB400.get9(totalRegistros, null, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);
	}

	/**
	 * Gets the 7.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 7
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get7(int totalRegistros,BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
			DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {
		
		ControladorCobranca controladorCobranca = (ControladorCobranca) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
		
		Map<String, Object> dados = DACNAB400.get7(totalRegistros, somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);		
		DocumentoCobranca documentoCobranca =
				controladorCobranca.obterDocumentoCobranca(cobrancaBancariaMovimento.getDocumentoCobranca().getChavePrimaria());
			
		dados.put("usoEmpresa", documentoCobranca.getChavePrimaria());
		dados.put("jurosMoraDaycoval", BigDecimal.ZERO);
		
		dados.put("codigoAgenciaCobradora", dados.get("codigoAgencia"));
		dados.put("dvAgenciaCobradora", dados.get("dvAgencia"));
		
		return dados;
	}
	
}
