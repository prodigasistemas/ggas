/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.remessa.conversor;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe DACNAB400Bradesco
 * 
 * @author Procenge
 */
public class DACNAB400Bradesco extends DACNAB400 {

	private static final int NUMERO_INSTRUCAO = 2;
	private static final int ID_TIPO_ISCRICAO_SACADO = 98;
	private static final int VALOR_ABATIMENTO = 10;
	private static final int VALOR_IOF = 121;
	private static final int VALOR_DESCONTO = 121;
	private static final double VALOR_COBRADO_DIA_ATRASO = 1.20;
	private static final int ESPECIE_TITULO = 99;
	private static final int VALOR_TITULO = 121;
	private static final int NUMERO_DOCUMENTO = 111;
	private static final int IDENTIFICADOR_OCORRENCIA = 111;
	private static final int COND_EMISSAO_PAPEL_COBRANCA = 2;
	private static final double DESCONTO_BONIFICACAO = 0.5;
	private static final int IDENTIFICADOR_TITULO_BANCO = 5;
	private static final double PERCENTUAL_MULTA = 0.5;
	private static final double CAMPO_MULTA = 1.1;
	private static final int CODIGO_BANCO = 444;
	private static final int CODIGO_CONTA_CORRENTE = 333;
	private static final int CODIGO_RAZAO_CONTA_CORRENTE = 222;
	private static final int CODIGO_DEBITO = 111;

	/**
	 * Gets the 0.
	 * 
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 0
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get0(BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB400.get0(0,somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);
		dados.put("codigoEmpresa", Integer.valueOf("12345"));
		dados.put("dataGravacaoArquivo", Calendar.getInstance().getTime());
		dados.put("branco1", StringUtils.EMPTY);
		dados.put("identificacaoSistema", "MX");
		dados.put("numeroSequencialRemessa", Integer.valueOf("15"));
		dados.put("branco2", StringUtils.EMPTY);

		return dados;
	}

	/**
	 * Gets the 1.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 1
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get1(int totalRegistros, BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB400.get1(totalRegistros, somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);

		dados.put("agenciaDebito", Integer.valueOf(CODIGO_DEBITO));
		dados.put("razaoContaCorrente", Integer.valueOf(CODIGO_RAZAO_CONTA_CORRENTE));
		dados.put("contaCorrente", Integer.valueOf(CODIGO_CONTA_CORRENTE));
		dados.put("identEmpresaCedenteBanco", "IDENTIFICACAOEMPRESA");
		dados.put("numeroControleParticipante", "USO EMPRESA");

		dados.put("codigoBanco", Integer.valueOf(CODIGO_BANCO));
		dados.put("campoMulta", BigDecimal.valueOf(CAMPO_MULTA));
		dados.put("percentualMulta", BigDecimal.valueOf(PERCENTUAL_MULTA));
		dados.put("identTituloBanco", Integer.valueOf(IDENTIFICADOR_TITULO_BANCO));
		dados.put("digitoAutoConfNumBancario", "2");
		dados.put("descBonificacaoDia", BigDecimal.valueOf(DESCONTO_BONIFICACAO));
		dados.put("condEmissaoPapCobranca", Integer.valueOf(COND_EMISSAO_PAPEL_COBRANCA));
		dados.put("identEmiteBoletoAutomatico", "N");
		dados.put("identOperacaoBanco", StringUtils.EMPTY);
		dados.put("indicadorRateioCred", "R");
		dados.put("endAvisoDebAutCC", "ENDERECO");
		dados.put("branco", StringUtils.EMPTY);
		dados.put("identOcorrencia", Integer.valueOf(IDENTIFICADOR_OCORRENCIA));
		dados.put("numeroDocumento", Integer.valueOf(NUMERO_DOCUMENTO));
		dados.put("dataVencimento", Calendar.getInstance().getTime());
		dados.put("valorTitulo", BigDecimal.valueOf(VALOR_TITULO));
		dados.put("bancoEncCobranca", Integer.valueOf(0));
		dados.put("agenciaDepositaria", Integer.valueOf(0));
		dados.put("especieTitulo", Integer.valueOf(ESPECIE_TITULO));
		dados.put("identificacao", "N");
		dados.put("dataEmissaoTitulo", Calendar.getInstance().getTime());
		dados.put("primeiraInstrucao", Integer.valueOf(1));
		dados.put("segundaInstrucao", Integer.valueOf(NUMERO_INSTRUCAO));
		dados.put("valorCobradoDiaAtraso", BigDecimal.valueOf(VALOR_COBRADO_DIA_ATRASO));
		dados.put("dataLimiteConcessao", Calendar.getInstance().getTime());
		dados.put("valorDesconto", BigDecimal.valueOf(VALOR_DESCONTO));
		dados.put("valorIOF", BigDecimal.valueOf(VALOR_IOF));
		dados.put("valorAbatimento", BigDecimal.valueOf(VALOR_ABATIMENTO));
		dados.put("identTipoInscSacado", Integer.valueOf(ID_TIPO_ISCRICAO_SACADO));
		dados.put("numeroInscricaoSacado", "05082052221");
		dados.put("nomeSacado", "Nome Sacado");
		dados.put("enderecoCompleto", "Endereco");
		dados.put("primeiraMensagem", "Mensagem");
		dados.put("cep", "05082051422");
		dados.put("sufixoCep", "232");
		dados.put("sacadorAvalista2Mensagem", "sacador");
		dados.put("numeroSequencialRegistro", Integer.valueOf(1));

		return dados;
	}

}
