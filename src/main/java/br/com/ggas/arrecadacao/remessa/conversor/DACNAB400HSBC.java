/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.remessa.conversor;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe DACNAB400HSBC
 * 
 * @author Procenge
 */
public class DACNAB400HSBC extends DACNAB400 {

	private static final int IDENTIFICACAO_TIPO_INSCRICAO = 98;
	private static final int VALOR_PARCELA_UNICA = 2;
	private static final int ESPECIE_TITULO = 99;
	private static final int VALOR_PARCELA = 352;
	private static final int ID_TITULO_EMPRESA = 1111;
	private static final int TIPO_INSCRICAO_EMPRESA = 99;
	private static final int INDICADOR_TIPO_MOEDA_COBRANCA = 9;
	private static final int PERIODICIDADE_VENCIMENTO_PARCELAS = 4;
	private static final int UNIDADE_DENSIDADE = 01600;
	private static final int NUMERO_CODIGO_CEDENTE = 111;

	/**
	 * Gets the 0.
	 * 
	 *
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 0
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get0(BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		// EM ANDAMENTO
		Map<String, Object> dados = DACNAB400.get0(0, somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);

		dados.put("numeroCodigoCedente", Integer.valueOf(NUMERO_CODIGO_CEDENTE));
		dados.put("complementoRegistro1", StringUtils.EMPTY);
		dados.put("dataGeracaoArquivo", Calendar.getInstance().getTime());
		dados.put("densidadeGravacao", Integer.valueOf(0));
		dados.put("unidadeDensidade", Integer.valueOf(UNIDADE_DENSIDADE));
		dados.put("horaGravacaoArquivo", Calendar.getInstance().getTime());
		dados.put("tipoFormularioImpressao", Integer.valueOf(0));
		dados.put("periodicidadeVencParcelas", Integer.valueOf(PERIODICIDADE_VENCIMENTO_PARCELAS));
		dados.put("complementoTamRegistro1", StringUtils.EMPTY);
		dados.put("indicadorTipoMoedaCobranca", Integer.valueOf(INDICADOR_TIPO_MOEDA_COBRANCA));
		dados.put("indicadorValorParcela", Integer.valueOf(0));
		dados.put("indicadorRemetenteDocumento", Integer.valueOf(0));
		dados.put("indicadorTipoMontagemCarnes", Integer.valueOf(1));
		dados.put("complementoTamanhoRegistro2", StringUtils.EMPTY);
		dados.put("literalComunicCredorDevedor1", StringUtils.EMPTY);
		dados.put("literalComunicCredorDevedor2", StringUtils.EMPTY);
		dados.put("literalComunicCredorDevedor3", StringUtils.EMPTY);
		dados.put("literalArquivoDatas", "Y2K");
		dados.put("complementoTamanhoRegistro3", StringUtils.EMPTY);

		return dados;
	}

	/**
	 * Gets the 1.
	 * 
	 *
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 1
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get1(BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		Map<String, Object> dados = DACNAB400.get0(0, somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);

		dados.put("tipoInscricaoEmpresa", Integer.valueOf(TIPO_INSCRICAO_EMPRESA));
		dados.put("numeroCodigoCedente", Integer.valueOf(NUMERO_CODIGO_CEDENTE));
		dados.put("complementoRegistro1", StringUtils.EMPTY);
		dados.put("complementoRegistro2", Integer.valueOf(0));
		dados.put("idTituloEmpresa", Integer.valueOf(ID_TITULO_EMPRESA));
		dados.put("complementoRegistro3", StringUtils.EMPTY);
		dados.put("tipoCarteiraCobranca", Integer.valueOf(0));
		dados.put("identificacaoOcorrencia", Integer.valueOf(01));
		dados.put("indicadorNumeroPrimeiraParcela", Integer.valueOf(0));
		dados.put("numeroParcelasSeremEmitidas", Integer.valueOf(0));
		dados.put("indicadorNumeroUltimaParcela", Integer.valueOf(0));
		dados.put("complementoRegistro4", StringUtils.EMPTY);
		dados.put("dataPrimeiroVencimentoParcelas", Calendar.getInstance().getTime());
		dados.put("valorParcela", new BigDecimal(VALOR_PARCELA));
		dados.put("bancoEncarregadoCobranca", Integer.valueOf(0));
		dados.put("complementoRegistro5", StringUtils.EMPTY);
		dados.put("especieTitulo", Integer.valueOf(ESPECIE_TITULO));
		dados.put("identificacaoAceite", "N");
		dados.put("complementoRegistro6", StringUtils.EMPTY);
		dados.put("valorParcelaUnica", Integer.valueOf(VALOR_PARCELA_UNICA));
		dados.put("dataVencimentoParcelaUnica", Calendar.getInstance().getTime());
		dados.put("complementoRegistro7", StringUtils.EMPTY);
		dados.put("identificacaoTipoInscricao", Integer.valueOf(IDENTIFICACAO_TIPO_INSCRICAO));
		dados.put("complementoRegistro8", StringUtils.EMPTY);
		dados.put("cepSacado", "53150250");
		dados.put("nomeSacado", "NOME DO SACADO");
		dados.put("ruaNumeroComplementoSacado", "RUA, NUMERO E COMPLEMENTO DO ENDERECO DO SACADO");
		dados.put("bairroSacado", "BAIRRO");
		dados.put("complementoRegistro9", StringUtils.EMPTY);
		dados.put("cidadeSacado", "CIDADE");
		dados.put("ufSacado", "UF");
		dados.put("observacaoSacado", "OBSERVACAO");
		dados.put("indicadorFormaRemessa", Integer.valueOf(1));
		dados.put("numeroSequencial", Integer.valueOf(1));

		return dados;
	}

	/**
	 * Gets the 9.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 9
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get9(int totalRegistros, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		return DACNAB400.get9(totalRegistros, null, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);
	}
}
