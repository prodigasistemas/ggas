/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.remessa.conversor;

import java.math.BigDecimal;
import java.util.Map;

import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 * Classe responsável pela parametrização da intervenção bancaria.
 *
 */
public class DACNAB240Itau extends DACNAB240 {

	/**
	 * Gets the 0.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 0
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get0(BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		return DACNAB240.get0(somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);
	}

	/**
	 * Gets the 1.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 1
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get1(BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		return DACNAB240.get1(somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);
	}

	/**
	 * Gets the 3.
	 * 
	 * @param somatorio {@link BigDecimal}
	 * @param cobrancaBancariaMovimento {@link CobrancaBancariaMovimento}
	 * @param documentoRemessa {@link DocumentoLayoutRegistro}
	 * @param serviceLocator {@link ServiceLocator}
	 * @return the 3 {@link Map}
	 */
	public static Map<String, Object> get3(BigDecimal somatorio, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) {

		return DACNAB240.get3(somatorio, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);
	}

	/**
	 * Gets the 5.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param somatorio
	 *            the somatorio
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 5
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get5(int totalRegistros, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) {

		return DACNAB240.get5(totalRegistros, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);
	}

	/**
	 * Gets the 9.
	 * 
	 * @param totalRegistros
	 *            the total registros
	 * @param cobrancaBancariaMovimento
	 *            the cobranca bancaria movimento
	 * @param documentoRemessa
	 *            the documento remessa
	 * @param serviceLocator
	 *            the service locator
	 * @return the 9
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public static Map<String, Object> get9(int totalRegistros, CobrancaBancariaMovimento cobrancaBancariaMovimento,
					DocumentoLayoutRegistro documentoRemessa, ServiceLocator serviceLocator) throws GGASException {

		return DACNAB240.get9(totalRegistros, cobrancaBancariaMovimento, documentoRemessa, serviceLocator);
	}

}
