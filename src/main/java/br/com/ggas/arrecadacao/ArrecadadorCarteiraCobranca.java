/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela carteira de cobrança do arrecadador
 */
public interface ArrecadadorCarteiraCobranca extends EntidadeNegocio {

	/** The bean id arrecadador carteira cobranca. */
	String BEAN_ID_ARRECADADOR_CARTEIRA_COBRANCA = "arrecadadorCarteiraCobranca";

	/** The arrecadador. */
	String ARRECADADOR = "Arrecadador";

	/** The codigo carteira. */
	String CODIGO_CARTEIRA = "Código Carteira";

	/** The tipo carteira. */
	String TIPO_CARTEIRA = "Tipo Carteira";

	/** The descricao. */
	String DESCRICAO = "Descrição";

	/** The numero. */
	String NUMERO = "Número";

	/**
	 * Gets the arrecadador.
	 *
	 * @return the arrecadador
	 */
	Arrecadador getArrecadador();

	/**
	 * Sets the arrecadador.
	 *
	 * @param arrecadador            the arrecadador to set
	 */
	void setArrecadador(Arrecadador arrecadador);

	/**
	 * Gets the codigo carteira.
	 *
	 * @return the codigoCarteira
	 */
	EntidadeConteudo getCodigoCarteira();

	/**
	 * Sets the codigo carteira.
	 *
	 * @param codigoCarteira            the codigoCarteira to set
	 */
	void setCodigoCarteira(EntidadeConteudo codigoCarteira);

	/**
	 * Gets the tipo carteira.
	 *
	 * @return the tipoCarteira
	 */
	EntidadeConteudo getTipoCarteira();

	/**
	 * Sets the tipo carteira.
	 *
	 * @param tipoCarteira            the tipoCarteira to set
	 */
	void setTipoCarteira(EntidadeConteudo tipoCarteira);

	/**
	 * Gets the descricao.
	 *
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * Sets the descricao.
	 *
	 * @param descricao            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * Gets the numero.
	 *
	 * @return the numero
	 */
	Integer getNumero();

	/**
	 * Sets the numero.
	 *
	 * @param numero            the numero to set
	 */
	void setNumero(Integer numero);

	/**
	 * Checks if is indicador faixa nosso numero.
	 *
	 * @return the indicadorFaixaNossoNumero
	 */
	boolean isIndicadorFaixaNossoNumero();

	/**
	 * Sets the indicador faixa nosso numero.
	 *
	 * @param indicadorFaixaNossoNumero            the indicadorFaixaNossoNumero to
	 *            set
	 */
	void setIndicadorFaixaNossoNumero(boolean indicadorFaixaNossoNumero);

	/**
	 * Checks if is indicador nosso numero livre.
	 *
	 * @return the indicadorNossoNumeroLivre
	 */
	boolean isIndicadorNossoNumeroLivre();

	/**
	 * Sets the indicador nosso numero livre.
	 *
	 * @param indicadorNossoNumeroLivre            the indicadorNossoNumeroLivre to
	 *            set
	 */
	void setIndicadorNossoNumeroLivre(boolean indicadorNossoNumeroLivre);

	/**
	 * Checks if is indicador emissao.
	 *
	 * @return the indicadorEmissao
	 */
	boolean isIndicadorEmissao();

	/**
	 * Sets the indicador emissao.
	 *
	 * @param indicadorEmissao            the indicadorEmissao to set
	 */
	void setIndicadorEmissao(boolean indicadorEmissao);

	/**
	 * Checks if is indicador protesto.
	 *
	 * @return the indicadorProtesto
	 */
	boolean isIndicadorProtesto();

	/**
	 * Sets the indicador protesto.
	 *
	 * @param indicadorProtesto            the indicadorProtesto to set
	 */
	void setIndicadorProtesto(boolean indicadorProtesto);

	/**
	 * Checks if is indicador entrega.
	 *
	 * @return the indicadorEntrega
	 */
	boolean isIndicadorEntrega();

	/**
	 * Sets the indicador entrega.
	 *
	 * @param indicadorEntrega            the indicadorEntrega to set
	 */
	void setIndicadorEntrega(boolean indicadorEntrega);

	/**
	 * Retorna o valor do arquivo de Layout da Fatura impressa, caso exista
	 *
	 * @return o nome do arquivo do layout da fatura. Caso não exista, retorna null
	 */
	String getArquivoLayoutFatura();

	/**
	 * Define o vaor do nome do arquivo de layout para a fatura
	 * @param arquivoLayoutFatura o nome arquivo
	 */
	void setArquivoLayoutFatura(String arquivoLayoutFatura);

}
