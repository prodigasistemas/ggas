/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/06/2014 10:54:08
 @author vtavares
 */

package br.com.ggas.arrecadacao.carteiracobranca.negocio;

import java.util.Collection;

import br.com.ggas.arrecadacao.ArrecadadorCarteiraCobranca;
import br.com.ggas.arrecadacao.carteiracobranca.dominio.CarteiraCobrancaVO;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
/**
 * Interface responsável por ações relacionadas a carteira de cobrança
 */
public interface ControladorCarteiraCobranca {

	/**
	 * Consultar arrecadador carteira cobranca.
	 * 
	 * @param carteiraCobrancaVO
	 *            the carteira cobranca vo
	 * @return the collection
	 */
	Collection<ArrecadadorCarteiraCobranca> consultarArrecadadorCarteiraCobranca(CarteiraCobrancaVO carteiraCobrancaVO);

	/**
	 * Inserir carteira cobranca.
	 * 
	 * @param carteiraCobrancaVO
	 *            the carteira cobranca vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirCarteiraCobranca(CarteiraCobrancaVO carteiraCobrancaVO) throws NegocioException;

	/**
	 * Remover carteira cobranca.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerCarteiraCobranca(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Obter carteira cobranca.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the arrecadador carteira cobranca
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ArrecadadorCarteiraCobranca obterCarteiraCobranca(Long chavePrimaria) throws NegocioException;

	/**
	 * Atualizar carteira cobranca.
	 * 
	 * @param carteiraCobrancaVO
	 *            the carteira cobranca vo
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException {@link GGASException}
	 */
	void atualizarCarteiraCobranca(CarteiraCobrancaVO carteiraCobrancaVO) throws GGASException;

	/**
	 * Popular carteira cobranca vo.
	 * 
	 * @param arrecadadorCarteiraCobranca
	 *            the arrecadador carteira cobranca
	 * @return the carteira cobranca vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CarteiraCobrancaVO popularCarteiraCobrancaVO(ArrecadadorCarteiraCobranca arrecadadorCarteiraCobranca) throws NegocioException;

}
