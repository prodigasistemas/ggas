/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/06/2014 10:52:25
 @author vtavares
 */

package br.com.ggas.arrecadacao.carteiracobranca.repositorio;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.arrecadacao.ArrecadadorCarteiraCobranca;
import br.com.ggas.arrecadacao.carteiracobranca.dominio.CarteiraCobrancaVO;
import br.com.ggas.arrecadacao.impl.ArrecadadorCarteiraCobrancaImpl;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Util;
/**
 * Repositório responsável pela carteira de cobrança
 */
@Repository
public class RepositorioCarteiraCobranca extends RepositorioGenerico {

	/**
	 * Instantiates a new repositorio carteira cobranca.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioCarteiraCobranca(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new ArrecadadorCarteiraCobrancaImpl();
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ArrecadadorCarteiraCobrancaImpl.class;
	}

	/**
	 * Consultar arrecadador carteira cobranca.
	 * 
	 * @param carteiraCobrancaVO
	 *            the carteira cobranca vo
	 * @return the collection
	 */
	public Collection<ArrecadadorCarteiraCobranca> consultarArrecadadorCarteiraCobranca(CarteiraCobrancaVO carteiraCobrancaVO) {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode("arrecadador", FetchMode.JOIN);
		criteria.setFetchMode("codigoCarteira", FetchMode.JOIN);
		criteria.setFetchMode("tipoCarteira", FetchMode.JOIN);

		Long arrecadador = carteiraCobrancaVO.getChaveArrecadador();
		if(arrecadador != null) {
			criteria.add(Restrictions.eq("arrecadador.chavePrimaria", arrecadador));
		}
		Long codigoCarteira = carteiraCobrancaVO.getChaveCodigoCarteira();
		if(codigoCarteira != null) {
			criteria.add(Restrictions.eq("codigoCarteira.chavePrimaria", codigoCarteira));
		}
		Long tipoCarteira = carteiraCobrancaVO.getChaveTipoCarteira();
		if(tipoCarteira != null) {
			criteria.add(Restrictions.eq("tipoCarteira.chavePrimaria", tipoCarteira));
		}

		String descricao = carteiraCobrancaVO.getDescricao();
		if(descricao != null && !descricao.isEmpty()) {
			criteria.add(Restrictions.ilike("descricao", "%" + Util.formatarTextoConsulta(descricao) + "%"));
		}

		Integer numero = carteiraCobrancaVO.getNumero();
		if(numero != null) {
			criteria.add(Restrictions.eq("numero", numero));
		}

		Boolean indicadorFaixaNossoNumero = carteiraCobrancaVO.getIndicadorFaixaNossoNumero();
		if(indicadorFaixaNossoNumero != null) {
			criteria.add(Restrictions.eq("indicadorFaixaNossoNumero", indicadorFaixaNossoNumero));
		}

		Boolean indicadorNossoNumeroLivre = carteiraCobrancaVO.getIndicadorNossoNumeroLivre();
		if(indicadorNossoNumeroLivre != null) {
			criteria.add(Restrictions.eq("indicadorNossoNumeroLivre", indicadorNossoNumeroLivre));
		}

		Boolean indicadorEmissao = carteiraCobrancaVO.getIndicadorEmissao();
		if(indicadorEmissao != null) {
			criteria.add(Restrictions.eq("indicadorEmissao", indicadorEmissao));
		}

		Boolean indicadorProtesto = carteiraCobrancaVO.getIndicadorProtesto();
		if(indicadorProtesto != null) {
			criteria.add(Restrictions.eq("indicadorProtesto", indicadorProtesto));
		}

		Boolean indicadorEntrega = carteiraCobrancaVO.getIndicadorEntrega();
		if(indicadorEntrega != null) {
			criteria.add(Restrictions.eq("indicadorEntrega", indicadorEntrega));
		}

		Boolean habilitado = carteiraCobrancaVO.getHabilitado();
		if(habilitado != null) {
			criteria.add(Restrictions.eq("habilitado", habilitado));
		}

		String arquivoLayoutImpresso = carteiraCobrancaVO.getArquivoLayoutFatura();
		if(arquivoLayoutImpresso != null) {
			criteria.add(Restrictions.eq("arquivoLayoutFatura", arquivoLayoutImpresso));
		}

		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}
}
