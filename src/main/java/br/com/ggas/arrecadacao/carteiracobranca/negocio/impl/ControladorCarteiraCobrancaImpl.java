/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/06/2014 10:55:42
 @author vtavares
 */

package br.com.ggas.arrecadacao.carteiracobranca.negocio.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorCarteiraCobranca;
import br.com.ggas.arrecadacao.ControladorArrecadador;
import br.com.ggas.arrecadacao.carteiracobranca.dominio.CarteiraCobrancaVO;
import br.com.ggas.arrecadacao.carteiracobranca.negocio.ControladorCarteiraCobranca;
import br.com.ggas.arrecadacao.carteiracobranca.repositorio.RepositorioCarteiraCobranca;
import br.com.ggas.arrecadacao.impl.ArrecadadorCarteiraCobrancaImpl;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável por implementar ações relacionadas a carteira de cobrança.
 */
@Service("controladorCarteiraCobranca")
@Transactional
public class ControladorCarteiraCobrancaImpl implements ControladorCarteiraCobranca {

	@Autowired
	private RepositorioCarteiraCobranca repositorioCarteiraCobranca;

	@Autowired
	@Qualifier("controladorArrecadador")
	private ControladorArrecadador controladorArrecadador;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	private static final String[] PROPRIEDADESLAZYCARTEIRACOBRANCA = {"tipoCarteira", "codigoCarteira", "arrecadador"};

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.carteiracobranca.negocio.ControladorCarteiraCobranca#
	 * consultarArrecadadorCarteiraCobranca(br.com.ggas.arrecadacao
	 * .carteiracobranca.dominio.CarteiraCobrancaVO)
	 */
	@Override
	public Collection<ArrecadadorCarteiraCobranca> consultarArrecadadorCarteiraCobranca(CarteiraCobrancaVO carteiraCobrancaVO) {

		return repositorioCarteiraCobranca.consultarArrecadadorCarteiraCobranca(carteiraCobrancaVO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.carteiracobranca.negocio.ControladorCarteiraCobranca#inserirCarteiraCobranca(br.com.ggas.arrecadacao.
	 * carteiracobranca.dominio.CarteiraCobrancaVO)
	 */
	@Override
	public void inserirCarteiraCobranca(CarteiraCobrancaVO carteiraCobrancaVO) throws NegocioException {

		CarteiraCobrancaVO carteiraCobrancaVOConsulta = new CarteiraCobrancaVO();
		carteiraCobrancaVOConsulta.setChaveArrecadador(carteiraCobrancaVO.getChaveArrecadador());
		carteiraCobrancaVOConsulta.setChaveTipoCarteira(carteiraCobrancaVO.getChaveTipoCarteira());
		carteiraCobrancaVOConsulta.setChaveCodigoCarteira(carteiraCobrancaVO.getChaveCodigoCarteira());
		carteiraCobrancaVOConsulta.setNumero(carteiraCobrancaVO.getNumero());

		Collection<ArrecadadorCarteiraCobranca> carteiraCobrancas = repositorioCarteiraCobranca
						.consultarArrecadadorCarteiraCobranca(carteiraCobrancaVOConsulta);

		if(carteiraCobrancas != null && !carteiraCobrancas.isEmpty() && !(carteiraCobrancas.size() > 1)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CARTEIRA_COBRANCA_EXISTENTE);
		}

		ArrecadadorCarteiraCobranca carteiraCobranca = popularArrecadadorCarteiraCobranca(carteiraCobrancaVO);

		repositorioCarteiraCobranca.inserir(carteiraCobranca);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.carteiracobranca.negocio.ControladorCarteiraCobranca#removerCarteiraCobranca(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerCarteiraCobranca(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		repositorioCarteiraCobranca.remover(chavesPrimarias, dadosAuditoria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.carteiracobranca.negocio.ControladorCarteiraCobranca#atualizarCarteiraCobranca(br.com.ggas.arrecadacao.
	 * carteiracobranca.dominio.CarteiraCobrancaVO)
	 */
	@Override
	public void atualizarCarteiraCobranca(CarteiraCobrancaVO carteiraCobrancaVO) throws NegocioException, ConcorrenciaException {

		CarteiraCobrancaVO carteiraCobrancaVOConsulta = new CarteiraCobrancaVO();
		carteiraCobrancaVOConsulta.setChaveArrecadador(carteiraCobrancaVO.getChaveArrecadador());
		carteiraCobrancaVOConsulta.setChaveTipoCarteira(carteiraCobrancaVO.getChaveTipoCarteira());
		carteiraCobrancaVOConsulta.setChaveCodigoCarteira(carteiraCobrancaVO.getChaveCodigoCarteira());
		carteiraCobrancaVOConsulta.setNumero(carteiraCobrancaVO.getNumero());

		Collection<ArrecadadorCarteiraCobranca> carteiraCobrancas = repositorioCarteiraCobranca
						.consultarArrecadadorCarteiraCobranca(carteiraCobrancaVOConsulta);

		if(carteiraCobrancas != null && !carteiraCobrancas.isEmpty()) {
			ArrecadadorCarteiraCobranca existente = carteiraCobrancas.iterator().next();
			if(existente.getChavePrimaria() != carteiraCobrancaVO.getChavePrimaria()) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CARTEIRA_COBRANCA_EXISTENTE);
			}
		}

		ArrecadadorCarteiraCobranca arrecadadorCarteiraCobranca = popularArrecadadorCarteiraCobranca(carteiraCobrancaVO);

		ArrecadadorCarteiraCobranca arrecadadorCarteiraCobrancaConsulta = (ArrecadadorCarteiraCobranca) repositorioCarteiraCobranca
						.obter(carteiraCobrancaVO.getChavePrimaria());

		arrecadadorCarteiraCobranca.setChavePrimaria(carteiraCobrancaVO.getChavePrimaria());
		arrecadadorCarteiraCobranca.setVersao(arrecadadorCarteiraCobrancaConsulta.getVersao());

		repositorioCarteiraCobranca.atualizar(arrecadadorCarteiraCobranca);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.carteiracobranca.negocio.ControladorCarteiraCobranca#obterCarteiraCobranca(java.lang.Long)
	 */
	@Override
	public ArrecadadorCarteiraCobranca obterCarteiraCobranca(Long chavePrimaria) throws NegocioException {

		return (ArrecadadorCarteiraCobranca) repositorioCarteiraCobranca.obter(chavePrimaria, PROPRIEDADESLAZYCARTEIRACOBRANCA);
	}

	/**
	 * Popular arrecadador carteira cobranca.
	 * 
	 * @param carteiraCobrancaVO
	 *            the carteira cobranca vo
	 * @return the arrecadador carteira cobranca
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private ArrecadadorCarteiraCobranca popularArrecadadorCarteiraCobranca(CarteiraCobrancaVO carteiraCobrancaVO) throws NegocioException {

		ArrecadadorCarteiraCobranca arrecadadorCarteiraCobranca = new ArrecadadorCarteiraCobrancaImpl();

		arrecadadorCarteiraCobranca.setDescricao(carteiraCobrancaVO.getDescricao());
		arrecadadorCarteiraCobranca.setNumero(carteiraCobrancaVO.getNumero());
		arrecadadorCarteiraCobranca.setIndicadorEmissao(carteiraCobrancaVO.getIndicadorEmissao());
		arrecadadorCarteiraCobranca.setIndicadorEntrega(carteiraCobrancaVO.getIndicadorEntrega());
		arrecadadorCarteiraCobranca.setIndicadorFaixaNossoNumero(carteiraCobrancaVO.getIndicadorFaixaNossoNumero());
		arrecadadorCarteiraCobranca.setIndicadorNossoNumeroLivre(carteiraCobrancaVO.getIndicadorNossoNumeroLivre());
		arrecadadorCarteiraCobranca.setIndicadorProtesto(carteiraCobrancaVO.getIndicadorProtesto());

		if(carteiraCobrancaVO.getChaveArrecadador() != null) {
			Arrecadador arrecadador = (Arrecadador) controladorArrecadador.obter(carteiraCobrancaVO.getChaveArrecadador());
			arrecadadorCarteiraCobranca.setArrecadador(arrecadador);
		}

		if(carteiraCobrancaVO.getChaveCodigoCarteira() != null) {
			EntidadeConteudo entidadeConteudo = controladorEntidadeConteudo.obter(carteiraCobrancaVO
							.getChaveCodigoCarteira());
			arrecadadorCarteiraCobranca.setCodigoCarteira(entidadeConteudo);
		}

		if(carteiraCobrancaVO.getChaveTipoCarteira() != null) {
			EntidadeConteudo entidadeConteudo = controladorEntidadeConteudo.obter(carteiraCobrancaVO
							.getChaveTipoCarteira());
			arrecadadorCarteiraCobranca.setTipoCarteira(entidadeConteudo);
		}

		if(carteiraCobrancaVO.getHabilitado() == null) {
			arrecadadorCarteiraCobranca.setHabilitado(true);
		} else {
			arrecadadorCarteiraCobranca.setHabilitado(carteiraCobrancaVO.getHabilitado());
		}

		arrecadadorCarteiraCobranca.setArquivoLayoutFatura(carteiraCobrancaVO.getArquivoLayoutFatura());

		return arrecadadorCarteiraCobranca;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.carteiracobranca.negocio.ControladorCarteiraCobranca#popularCarteiraCobrancaVO(br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca)
	 */
	@Override
	public CarteiraCobrancaVO popularCarteiraCobrancaVO(ArrecadadorCarteiraCobranca arrecadadorCarteiraCobranca) throws NegocioException {

		CarteiraCobrancaVO carteiraCobrancaVO = new CarteiraCobrancaVO();

		carteiraCobrancaVO.setChavePrimaria(arrecadadorCarteiraCobranca.getChavePrimaria());
		carteiraCobrancaVO.setDescricao(arrecadadorCarteiraCobranca.getDescricao());
		carteiraCobrancaVO.setNumero(arrecadadorCarteiraCobranca.getNumero());
		carteiraCobrancaVO.setIndicadorEmissao(arrecadadorCarteiraCobranca.isIndicadorEmissao());
		carteiraCobrancaVO.setIndicadorEntrega(arrecadadorCarteiraCobranca.isIndicadorEntrega());
		carteiraCobrancaVO.setIndicadorFaixaNossoNumero(arrecadadorCarteiraCobranca.isIndicadorFaixaNossoNumero());
		carteiraCobrancaVO.setIndicadorNossoNumeroLivre(arrecadadorCarteiraCobranca.isIndicadorNossoNumeroLivre());
		carteiraCobrancaVO.setIndicadorProtesto(arrecadadorCarteiraCobranca.isIndicadorProtesto());
		carteiraCobrancaVO.setHabilitado(arrecadadorCarteiraCobranca.isHabilitado());

		carteiraCobrancaVO.setChaveArrecadador(arrecadadorCarteiraCobranca.getArrecadador().getChavePrimaria());
		carteiraCobrancaVO.setChaveCodigoCarteira(arrecadadorCarteiraCobranca.getCodigoCarteira().getChavePrimaria());
		carteiraCobrancaVO.setChaveTipoCarteira(arrecadadorCarteiraCobranca.getTipoCarteira().getChavePrimaria());
		carteiraCobrancaVO.setArquivoLayoutFatura(arrecadadorCarteiraCobranca.getArquivoLayoutFatura());

		return carteiraCobrancaVO;

	}

}
