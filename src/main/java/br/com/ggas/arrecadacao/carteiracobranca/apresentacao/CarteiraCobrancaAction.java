/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/06/2014 10:57:03
 @author vtavares
 */

package br.com.ggas.arrecadacao.carteiracobranca.apresentacao;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.arrecadacao.ArrecadadorCarteiraCobranca;
import br.com.ggas.arrecadacao.ControladorArrecadador;
import br.com.ggas.arrecadacao.carteiracobranca.dominio.CarteiraCobrancaVO;
import br.com.ggas.arrecadacao.carteiracobranca.negocio.ControladorCarteiraCobranca;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
/**
 * Action responsável pelas telas relacionadas a carteira de cobrança
 */
@Controller
public class CarteiraCobrancaAction extends GenericAction {

	private static final String TELA_FORWARD_PESQUISAR_CARTEIRA_COBRANCA = "forward:/pesquisarCarteiraCobranca";

	@Autowired
	private ControladorCarteiraCobranca controladorCarteiraCobranca;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier("controladorArrecadador")
	private ControladorArrecadador controladorArrecadador;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	private static final String CARTEIRA_COBRANCA_VO = "carteiraCobrancaVO";

	private static final String FLUXO_INCLUSAO = "fluxoInclusao";

	private static final String FLUXO_ALTERACAO = "fluxoAlteracao";

	private static final String TITULO_CARTEIRA_COBRANCA = "Carteira Cobrança";

	private static final String CARTEIRA_COBRANCA = "carteiraCobranca";

	private static final Logger LOG = Logger.getLogger(CarteiraCobrancaAction.class);

	/**
	 * Exibir pesquisa carteira cobranca.
	 * 
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirPesquisaCarteiraCobranca")
	public ModelAndView exibirPesquisaCarteiraCobranca() throws NegocioException {

		ModelAndView model = new ModelAndView("exibirPesquisaCarteiraCobranca");

		CarteiraCobrancaVO carteiraCobrancaVO = new CarteiraCobrancaVO();
		carteiraCobrancaVO.setHabilitado(Boolean.TRUE);

		model.addObject(CARTEIRA_COBRANCA_VO, carteiraCobrancaVO);

		carregarCampos(model);

		return model;
	}

	/**
	 * Pesquisar carteira cobranca.
	 * 
	 * @param carteiraCobrancaVO
	 *            the carteira cobranca vo
	 * @param result
	 *            the result
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("pesquisarCarteiraCobranca")
	public ModelAndView pesquisarCarteiraCobranca(@ModelAttribute("CarteiraCobrancaVO") CarteiraCobrancaVO carteiraCobrancaVO,
					BindingResult result) throws NegocioException {

		ModelAndView model = new ModelAndView("exibirPesquisaCarteiraCobranca");

		model.addObject("listaCarteiraCobranca", controladorCarteiraCobranca.consultarArrecadadorCarteiraCobranca(carteiraCobrancaVO));

		model.addObject(CARTEIRA_COBRANCA_VO, carteiraCobrancaVO);
		carregarCampos(model);

		return model;
	}

	/**
	 * Exibir alteracao carteira cobranca.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirAlteracaoCarteiraCobranca")
	public ModelAndView exibirAlteracaoCarteiraCobranca(@RequestParam("chavePrimaria") Long chavePrimaria) throws NegocioException {

		ModelAndView model = new ModelAndView("exibirAlteracaoCarteiraCobranca");

		ArrecadadorCarteiraCobranca arrecadadorCarteiraCobranca = controladorCarteiraCobranca.obterCarteiraCobranca(chavePrimaria);
		CarteiraCobrancaVO carteiraCobrancaVO = controladorCarteiraCobranca.popularCarteiraCobrancaVO(arrecadadorCarteiraCobranca);

		model.addObject(CARTEIRA_COBRANCA_VO, carteiraCobrancaVO);

		model.addObject(FLUXO_ALTERACAO, true);

		carregarCampos(model);

		return model;
	}

	/**
	 * Alterar carteira cobranca.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param carteiraCobrancaVO
	 *            the carteira cobranca vo
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("alterarCarteiraCobranca")
	public ModelAndView alterarCarteiraCobranca(@RequestParam("chavePrimaria") Long chavePrimaria,
					@ModelAttribute("CarteiraCobrancaVO") CarteiraCobrancaVO carteiraCobrancaVO) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_CARTEIRA_COBRANCA);

		try {
			controladorCarteiraCobranca.atualizarCarteiraCobranca(carteiraCobrancaVO);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, TITULO_CARTEIRA_COBRANCA);
		} catch(NegocioException e) {
			model = exibirAlteracaoCarteiraCobranca(chavePrimaria);
			model.addObject(CARTEIRA_COBRANCA_VO, carteiraCobrancaVO);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Exibir inclusao carteira cobranca.
	 * 
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirInclusaoCarteiraCobranca")
	public ModelAndView exibirInclusaoCarteiraCobranca() throws NegocioException {

		ModelAndView model = new ModelAndView("exibirInclusaoCarteiraCobranca");

		CarteiraCobrancaVO carteiraCobrancaVO = new CarteiraCobrancaVO();
		carteiraCobrancaVO.setIndicadorEmissao(Boolean.TRUE);
		carteiraCobrancaVO.setIndicadorEntrega(Boolean.TRUE);
		carteiraCobrancaVO.setIndicadorFaixaNossoNumero(Boolean.TRUE);
		carteiraCobrancaVO.setIndicadorNossoNumeroLivre(Boolean.TRUE);
		carteiraCobrancaVO.setIndicadorProtesto(Boolean.TRUE);
		carteiraCobrancaVO.setHabilitado(Boolean.TRUE);

		model.addObject(CARTEIRA_COBRANCA_VO, carteiraCobrancaVO);
		model.addObject(FLUXO_INCLUSAO, true);
		carregarCampos(model);

		return model;
	}

	/**
	 * Incluir carteira cobranca.
	 * 
	 * @param carteiraCobrancaVO
	 *            the carteira cobranca vo
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("incluirCarteiraCobranca")
	public ModelAndView incluirCarteiraCobranca(@ModelAttribute("CarteiraCobrancaVO") CarteiraCobrancaVO carteiraCobrancaVO)
					throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_CARTEIRA_COBRANCA);

		try {
			controladorCarteiraCobranca.inserirCarteiraCobranca(carteiraCobrancaVO);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, TITULO_CARTEIRA_COBRANCA);
		} catch(NegocioException e) {
			model = exibirInclusaoCarteiraCobranca();
			model.addObject(CARTEIRA_COBRANCA_VO, carteiraCobrancaVO);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Remover carteira cobranca.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("removerCarteiraCobranca")
	public ModelAndView removerCarteiraCobranca(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_CARTEIRA_COBRANCA);

		try {
			controladorCarteiraCobranca.removerCarteiraCobranca(chavesPrimarias, getDadosAuditoria(request));
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, TITULO_CARTEIRA_COBRANCA);
		} catch (DataIntegrityViolationException ex) {
			LOG.error(ex.getMessage(), ex);
			try {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CARTEIRA_COBRANCA_RELACIONADA, true);
			} catch(NegocioException e) {
				mensagemErroParametrizado(model, e);
			}
		}

		return model;
	}

	/**
	 * Exibir detalhamento carteira cobranca.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirDetalhamentoCarteiraCobranca")
	public ModelAndView exibirDetalhamentoCarteiraCobranca(@RequestParam("chavePrimaria") Long chavePrimaria) throws NegocioException {

		ModelAndView model = new ModelAndView("exibirDetalhamentoCarteiraCobranca");

		model.addObject(CARTEIRA_COBRANCA, controladorCarteiraCobranca.obterCarteiraCobranca(chavePrimaria));

		return model;
	}

	/**
	 * Carregar campos.
	 * 
	 * @param model
	 *            the model
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void carregarCampos(ModelAndView model) throws NegocioException {

		ConstanteSistema constanateSistema = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_CODIGO_CARTEIRA_COBRANCA);
		model.addObject("listaCodigoCarteira",
						controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanateSistema.getValor())));

		constanateSistema = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_ENT_CLASSE_TIPO_CARTEIRA_COBRANCA);
		model.addObject("listaTipoCarteira", controladorEntidadeConteudo.listarEntidadeConteudo(Long.valueOf(constanateSistema.getValor())));

		model.addObject("listaArrecadador", controladorArrecadador.obterTodas(true));

	}

}
