/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 19/06/2014 10:58:05
 @author vtavares
 */

package br.com.ggas.arrecadacao.carteiracobranca.dominio;

/**
 * Classe responsável pela representação de uma carteira de cobrança
 */
public class CarteiraCobrancaVO {

	private Long chavePrimaria;

	private Long chaveArrecadador;

	private Long chaveCodigoCarteira;

	private Long chaveTipoCarteira;

	private String descricao;

	private Integer numero;

	private Boolean indicadorFaixaNossoNumero;

	private Boolean indicadorNossoNumeroLivre;

	private Boolean indicadorEmissao;

	private Boolean indicadorProtesto;

	private Boolean indicadorEntrega;

	private Boolean habilitado;

	private String arquivoLayoutFatura;

	/**
	 * @return chaveArrecadador
	 */
	public Long getChaveArrecadador() {

		return chaveArrecadador;
	}

	/**
	 * @param chaveArrecadador
	 */
	public void setChaveArrecadador(Long chaveArrecadador) {

		this.chaveArrecadador = chaveArrecadador;
	}

	/**
	 * @return chaveCodigoCarteira
	 */
	public Long getChaveCodigoCarteira() {

		return chaveCodigoCarteira;
	}

	/**
	 * @param chaveCodigoCarteira
	 */
	public void setChaveCodigoCarteira(Long chaveCodigoCarteira) {

		this.chaveCodigoCarteira = chaveCodigoCarteira;
	}

	/**
	 * @return chaveTipoCarteira
	 */
	public Long getChaveTipoCarteira() {

		return chaveTipoCarteira;
	}

	/**
	 * @param chaveTipoCarteira
	 */
	public void setChaveTipoCarteira(Long chaveTipoCarteira) {

		this.chaveTipoCarteira = chaveTipoCarteira;
	}

	/**
	 * @return descrição da carteira de cobrança
	 */
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 * define descrição da carteira de cobrança
	 */
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return número da carteira de cobrança
	 */
	public Integer getNumero() {

		return numero;
	}

	/**
	 * @param numero
	 * define número da carteira de cobrança
	 */
	public void setNumero(Integer numero) {

		this.numero = numero;
	}

	/**
	 * @return indicadorFaixaNossoNumero
	 */
	public Boolean getIndicadorFaixaNossoNumero() {

		return indicadorFaixaNossoNumero;
	}

	/**
	 * @param indicadorFaixaNossoNumero
	 */
	public void setIndicadorFaixaNossoNumero(Boolean indicadorFaixaNossoNumero) {

		this.indicadorFaixaNossoNumero = indicadorFaixaNossoNumero;
	}

	/**
	 * @return indicadorNossoNumeroLivre
	 */
	public Boolean getIndicadorNossoNumeroLivre() {

		return indicadorNossoNumeroLivre;
	}

	/**
	 * @param indicadorNossoNumeroLivre
	 */
	public void setIndicadorNossoNumeroLivre(Boolean indicadorNossoNumeroLivre) {

		this.indicadorNossoNumeroLivre = indicadorNossoNumeroLivre;
	}

	/**
	 * @return se foi emitida a carteira de cobrança
	 */
	public Boolean getIndicadorEmissao() {

		return indicadorEmissao;
	}

	/**
	 * @param indicadorEmissao
	 * define se foi emitida a carteira de cobrança
	 */
	public void setIndicadorEmissao(Boolean indicadorEmissao) {

		this.indicadorEmissao = indicadorEmissao;
	}

	/**
	 * @return se ocasiona protesto
	 */
	public Boolean getIndicadorProtesto() {

		return indicadorProtesto;
	}

	/**
	 * @param indicadorProtesto
	 * define se ocasiona protesto
	 */
	public void setIndicadorProtesto(Boolean indicadorProtesto) {

		this.indicadorProtesto = indicadorProtesto;
	}

	/**
	 * @return se a de cobraça entrega foi realizada
	 */
	public Boolean getIndicadorEntrega() {

		return indicadorEntrega;
	}

	/**
	 * @param indicadorEntrega
	 * indica entrega de cobrança
	 */
	public void setIndicadorEntrega(Boolean indicadorEntrega) {

		this.indicadorEntrega = indicadorEntrega;
	}

	/**
	 * @return status da carteira de cobrança
	 */
	public Boolean getHabilitado() {

		return habilitado;
	}

	/**
	 * @param habilitado
	 * define se a carteira de cobrança está habilitada.
	 */
	public void setHabilitado(Boolean habilitado) {

		this.habilitado = habilitado;
	}

	/**
	 * @return a chave primária 
	 * de uma carteira de cobrança
	 */
	public Long getChavePrimaria() {

		return chavePrimaria;
	}

	/**
	 * @param chavePrimaria
	 * define a chave primária da carteira de cobrança
	 */
	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * Retorna o nome do arquivo de layout da fatura impressa
	 *
	 * @return o nome do arquivo
	 */
	public String getArquivoLayoutFatura() {
		return arquivoLayoutFatura;
	}

	/**
	 * Define o nomr do arquivo de layout da fatura impressa
	 *
	 * @param arquivoLayoutFatura o nome do arquivo
	 */
	public void setArquivoLayoutFatura(String arquivoLayoutFatura) {
		this.arquivoLayoutFatura = arquivoLayoutFatura;
	}


}
