/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.util.Collection;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela agencia
 */
public interface Agencia extends EntidadeNegocio {

	/** The bean id agencia. */
	String BEAN_ID_AGENCIA = "agencia";

	/** The nome. */
	String NOME = "Nome da Agência";

	/** The banco. */
	String BANCO = "Banco";

	/** The codigo. */
	String CODIGO = "Código da Agência";

	/**
	 * Gets the banco.
	 *
	 * @return the banco
	 */
	Banco getBanco();

	/**
	 * Sets the banco.
	 *
	 * @param banco            the banco to set
	 */
	void setBanco(Banco banco);

	/**
	 * Gets the codigo.
	 *
	 * @return the codigo
	 */
	String getCodigo();

	/**
	 * Sets the codigo.
	 *
	 * @param codigo            the codigo to set
	 */
	void setCodigo(String codigo);

	/**
	 * Gets the nome.
	 *
	 * @return the nome
	 */
	String getNome();

	/**
	 * Sets the nome.
	 *
	 * @param nome            the nome to set
	 */
	void setNome(String nome);

	/**
	 * Gets the telefone.
	 *
	 * @return the telefone
	 */
	String getTelefone();

	/**
	 * Sets the telefone.
	 *
	 * @param telefone            the telefone to set
	 */
	void setTelefone(String telefone);

	/**
	 * Gets the ramal.
	 *
	 * @return the ramal
	 */
	String getRamal();

	/**
	 * Sets the ramal.
	 *
	 * @param ramal            the ramal to set
	 */
	void setRamal(String ramal);

	/**
	 * Gets the fax.
	 *
	 * @return the fax
	 */
	String getFax();

	/**
	 * Sets the fax.
	 *
	 * @param fax            the fax to set
	 */
	void setFax(String fax);

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	String getEmail();

	/**
	 * Sets the email.
	 *
	 * @param email            the email to set
	 */
	void setEmail(String email);

	/**
	 * Gets the cep.
	 *
	 * @return the cep
	 */
	Cep getCep();

	/**
	 * Sets the cep.
	 *
	 * @param cep            the cep to set
	 */
	void setCep(Cep cep);

	/**
	 * Gets the lista conta bancaria.
	 *
	 * @return the lista conta bancaria
	 */
	public Collection<ContaBancaria> getListaContaBancaria();

	/**
	 * Sets the lista conta bancaria.
	 *
	 * @param listaContaBancaria the new lista conta bancaria
	 */
	public void setListaContaBancaria(Collection<ContaBancaria> listaContaBancaria);

}
