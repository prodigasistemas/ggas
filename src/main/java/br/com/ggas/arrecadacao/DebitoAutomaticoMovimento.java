/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pelas ações de débito automático
 */
public interface DebitoAutomaticoMovimento extends EntidadeNegocio {

	String BEAN_ID_DEBITO_AUTOMATICO_MOVIMENTO = "debitoAutomaticoMovimento";

	int CODIGO_MOVIMENTO_INCLUSAO = 0;

	int CODIGO_MOVIMENTO_CANCELAMENTO = 1;

	/**
	 * @return the debitoAutomatico
	 */
	DebitoAutomatico getDebitoAutomatico();

	/**
	 * @param debitoAutomatico
	 *            the debitoAutomatico to set
	 */
	void setDebitoAutomatico(DebitoAutomatico debitoAutomatico);

	/**
	 * @return the documentoCobranca
	 */
	DocumentoCobranca getDocumentoCobranca();

	/**
	 * @param documentoCobranca
	 *            the documentoCobranca to set
	 */
	void setDocumentoCobranca(DocumentoCobranca documentoCobranca);

	/**
	 * @return the dataDebito
	 */
	Date getDataDebito();

	/**
	 * @param dataDebito
	 *            the dataDebito to set
	 */
	void setDataDebito(Date dataDebito);

	/**
	 * @return the valorDebito
	 */
	BigDecimal getValorDebito();

	/**
	 * @param valorDebito
	 *            the valorDebito to set
	 */
	void setValorDebito(BigDecimal valorDebito);

	/**
	 * @return the ocorrencia
	 */
	String getOcorrencia();

	/**
	 * @param ocorrencia
	 *            the ocorrencia to set
	 */
	void setOcorrencia(String ocorrencia);

	/**
	 * @return the dataProcessamento
	 */
	Date getDataProcessamento();

	/**
	 * @param dataProcessamento
	 *            the dataProcessamento to set
	 */
	void setDataProcessamento(Date dataProcessamento);

	/**
	 * @return the dataEnvio
	 */
	Date getDataEnvio();

	/**
	 * @param dataEnvio
	 *            the dataEnvio to set
	 */
	void setDataEnvio(Date dataEnvio);

	/**
	 * @return the dataRetorno
	 */
	Date getDataRetorno();

	/**
	 * @param dataRetorno
	 *            the dataRetorno to set
	 */
	void setDataRetorno(Date dataRetorno);

	/**
	 * @return
	 */
	Integer getCodigoMovimento();

	/**
	 * @param codigoMovimento
	 */
	void setCodigoMovimento(Integer codigoMovimento);

}
