/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * The Interface ControladorArrecadadorConvenio.
 */
public interface ControladorArrecadadorConvenio extends ControladorNegocio {

	/** The bean id controlador arrecadador convenio. */
	String BEAN_ID_CONTROLADOR_ARRECADADOR_CONVENIO = "controladorArrecadadorConvenio";

	/**
	 * Obter arrecadador convenio.
	 * 
	 * @param idArrecadadorConvenio
	 *            the id arrecadador convenio
	 * @return the arrecadador contrato convenio
	 * @throws NegocioException the negocio exception
	 */
	public ArrecadadorContratoConvenio obterArrecadadorConvenio(Long idArrecadadorConvenio) throws NegocioException;

	/**
	 * Obter arrecadador convenio.
	 * 
	 * @param idArrecadadorConvenio chavePrimaria para consultar o arrecadador
	 * @param propiedadesLazy propridades que serão carregadas na consulta
	 * @return the arrecadador contrato convenio
	 * @throws NegocioException exceção que será lançada caso algum erro ocorra
	 */
	public ArrecadadorContratoConvenio obterArrecadadorConvenio(Long idArrecadadorConvenio, String... propiedadesLazy)
			throws NegocioException;

	/**
	 * Obter arrecadador contrato.
	 * 
	 * @param idArrecadadorContrato the id arrecadador contrato
	 * @return the arrecadador contrato
	 * @throws NegocioException the negocio exception
	 */
	public ArrecadadorContrato obterArrecadadorContrato(Long idArrecadadorContrato) throws NegocioException;

	/**
	 * Obter arrecadador carteira cobranca.
	 * 
	 * @param idArrecadadorCarteiraCobranca
	 *            the id arrecadador carteira cobranca
	 * @return the arrecadador carteira cobranca
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public ArrecadadorCarteiraCobranca obterArrecadadorCarteiraCobranca(Long idArrecadadorCarteiraCobranca) throws GGASException;

	/**
	 * Consultar arrecadador convenio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ArrecadadorContratoConvenio> consultarArrecadadorConvenio(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Alterar arrecadador contrato convenio.
	 * 
	 * @param arrecadadorContratoConvenio
	 *            the arrecadador contrato convenio {@link ArrecadadorContratoConvenio}
	 * @throws GGASException
	 *             the GGAS exception {@link GGASException}
	 */
	public void alterarArrecadadorContratoConvenio(ArrecadadorContratoConvenio arrecadadorContratoConvenio) throws GGASException;

	/**
	 * Tornar padrao arrecadador contrato convenio.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria {@link Long}
	 * @throws GGASException
	 *             the GGAS exception {@link GGASException}
	 */
	public void tornarPadraoArrecadadorContratoConvenio(Long chavePrimaria) throws GGASException;

	/**
	 * Listar arrecadador contrato.
	 *
	 * @return the collection
	 */
	public Collection<ArrecadadorContrato> listarArrecadadorContrato();

	/**
	 * Listar conta bancaria.
	 * 
	 * @return the collection
	 */
	public Collection<ContaBancaria> listarContaBancaria();

	/**
	 * Listar carteira cobranca.
	 * 
	 * @return the collection
	 */
	public Collection<ArrecadadorCarteiraCobranca> listarCarteiraCobranca();

	/**
	 * Obter conta bancaria.
	 * 
	 * @param idContaBancaria
	 *            the id conta bancaria
	 * @return the conta bancaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public ContaBancaria obterContaBancaria(Long idContaBancaria) throws NegocioException;

	/**
	 * Inserir arrecadador contrato convenio.
	 * 
	 * @param arrecadadorContratoConvenio
	 *            the arrecadador contrato convenio
	 * @return the long
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public Long inserirArrecadadorContratoConvenio(ArrecadadorContratoConvenio arrecadadorContratoConvenio) throws GGASException;

	/**
	 * Obter arrecadador contrato convenio padrao.
	 *
	 * @return the arrecadador contrato convenio
	 */
	ArrecadadorContratoConvenio obterArrecadadorContratoConvenioPadrao();
	
	/**
	 * Obtem arrecadador contrato convenio por documento de cobrança e contrato com situação diferente de "Encerrado".
	 * 
	 * @param documentoCobranca Documento de cobrança
	 * 
	 * @return the arrecadador contrato convenio
	 */
	public ArrecadadorContratoConvenio obterArrecadadorContratoConvenioPorDocumentoCobranca(DocumentoCobranca documentoCobranca);

	/**
	 * Obtém uma carteira de cobrança de um convênio.
	 * 
	 * @param chaveArrecadadorConvenio - {@link Long}
	 * @return {@link ArrecadadorCarteiraCobranca}
	 */
	ArrecadadorCarteiraCobranca obterCarteiraCobrancaDeConvenio(Long chaveArrecadadorConvenio);

}
