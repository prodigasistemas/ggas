/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela atualização do indice financeiro
 */
public interface IndiceFinanceiroValorNominal extends EntidadeNegocio {

	String BEAN_ID_INDICE_FINANCEIRO_VALOR_NOMINAL = "indiceFinanceiroValorNominal";

	String VALOR_NOMINAL = "ROTULO_INDICE_FINANCEIRO_VALOR_NOMINAL";

	String DATA = "ROTULO_INDICE_FINANCEIRO_DATA";

	String UNIDADE_MONETARIA = "ROTULO_INDICE_FINANCEIRO_UNIDADE_MONETARIA";

	String TIPO_VALOR = "ROTULO_INDICE_FINANCEIRO_TIPO_VALOR";

	/**
	 * @return the indiceFinanceiro
	 */
	IndiceFinanceiro getIndiceFinanceiro();

	/**
	 * @param indiceFinanceiro
	 *            the indiceFinanceiro to set
	 */
	void setIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro);

	/**
	 * @return the dataReferencia
	 */
	Date getDataReferencia();

	/**
	 * @param dataReferencia
	 *            the dataReferencia to set
	 */
	void setDataReferencia(Date dataReferencia);

	/**
	 * @return the valorNominal
	 */
	BigDecimal getValorNominal();

	/**
	 * @param valorNominal
	 *            the valorNominal to set
	 */
	void setValorNominal(BigDecimal valorNominal);

	/**
	 * @return the indicadorUtilizado
	 */
	boolean isIndicadorUtilizado();

	/**
	 * @param indicadorUtilizado
	 *            the indicadorUtilizado to set
	 */
	void setIndicadorUtilizado(boolean indicadorUtilizado);

}
