/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela entidade Banco
 */
public interface Banco extends EntidadeNegocio {

	String ATTR_NOME = "nome";

	String ATTR_CODIGO_BANCO = "codigoBanco";

	String ATTR_NOME_ABREVIADO = "nomeAbreviado";

	String ATTR_HABILITADO = "habilitado";

	String BEAN_ID_BANCO = "banco";

	String NOME_DO_BANCO = "Nome do Banco";
	
	String CODIGO_DO_BANCO = "Código do Banco";

	String BANCO_CAMPO_STRING = "Banco";

	String BANCOS = "Banco(s)";

	String BANCO_LOGO = "BANCO_LOGO";

	String ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO = "ERRO_NEGOCIO_TAMANHO_MAXIMO_ARQUIVO";

	String ERRO_NEGOCIO_TIPO_ARQUIVO = "ERRO_NEGOCIO_TIPO_ARQUIVO";

	/**
	 * Extensoes de arquivo aceitas para a logo da empresa
	 */
	final String[] EXTENSOES_ARQUIVO_LOGO_EMPRESA = {".JPG", ".JPEG"};

	/**
	 * Tamanho do arquivo maximo para a logo da empresa
	 */
	final int TAMANHO_MAXIMO_ARQUIVO_LOGO_EMPRESA = 204800;

	/**
	 * @return the nome
	 */
	String getNome();

	/**
	 * @param nome
	 *            the nome to set
	 */
	void setNome(String nome);

	/**
	 * @return the nomeAbreviado
	 */
	String getNomeAbreviado();

	/**
	 * @param nomeAbreviado
	 *            the nomeAbreviado to set
	 */
	void setNomeAbreviado(String nomeAbreviado);

	/**
	 * @return the codigoBanco
	 */
	String getCodigoBanco();

	/**
	 * @param codigoBanco
	 *            the codigoBanco to set
	 */
	void setCodigoBanco(String codigoBanco);

	/**
	 * @return the logoBanco
	 */
	byte[] getLogoBanco();

	/**
	 * @param logoBanco
	 *            the logoBanco to set
	 */
	void setLogoBanco(byte[] logoBanco);

	/**
	 * @return the EXTENSOES_ARQUIVO_LOGO_EMPRESA
	 */
	public String[] getExtensoesArquivoLogoEmpresa();
}
