/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.util.Date;

import br.com.ggas.arrecadacao.documento.DocumentoLayoutEnvioOcorrencia;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRetornoOcorrencia;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela movimentação da cobrança bancária
 */
public interface CobrancaBancariaMovimento extends EntidadeNegocio {

	String BEAN_ID_COBRANCA_BANCARIA_MOVIMENTO = "cobrancaBancariaMovimento";

	/**
	 * @return the documentoCobranca
	 */
	DocumentoCobranca getDocumentoCobranca();

	/**
	 * @param documentoCobranca
	 *            the documentoCobranca to set
	 */
	void setDocumentoCobranca(DocumentoCobranca documentoCobranca);

	/**
	 * @return the
	 *         documentoLayoutRetornoOcorrencia
	 */
	DocumentoLayoutRetornoOcorrencia getDocumentoLayoutRetornoOcorrencia();

	/**
	 * @param documentoLayoutRetornoOcorrencia
	 *            the
	 *            documentoLayoutRetornoOcorrencia
	 *            to set
	 */
	void setDocumentoLayoutRetornoOcorrencia(DocumentoLayoutRetornoOcorrencia documentoLayoutRetornoOcorrencia);

	/**
	 * @return the arrecadadorContratoConvenio
	 */
	ArrecadadorContratoConvenio getArrecadadorContratoConvenio();

	/**
	 * @param arrecadadorContratoConvenio
	 *            the arrecadadorContratoConvenio
	 *            to set
	 */
	void setArrecadadorContratoConvenio(ArrecadadorContratoConvenio arrecadadorContratoConvenio);

	/**
	 * @return the dataEnvio
	 */
	Date getDataEnvio();

	/**
	 * @param dataEnvio
	 *            the dataEnvio to set
	 */
	void setDataEnvio(Date dataEnvio);

	/**
	 * @return the dataRetorno
	 */
	Date getDataRetorno();

	/**
	 * @param dataRetorno
	 *            the dataRetorno to set
	 */
	void setDataRetorno(Date dataRetorno);

	/**
	 * @return the dataProcessamento
	 */
	Date getDataProcessamento();

	/**
	 * @param dataProcessamento
	 *            the dataProcessamento to set
	 */
	void setDataProcessamento(Date dataProcessamento);
	
	/**
	 * @return the
	 *         documentoLayoutEnvioOcorrencia
	 */
	DocumentoLayoutEnvioOcorrencia getDocumentoLayoutEnvioOcorrencia();

	/**
	 * @param documentoLayoutEnvioOcorrencia
	 *            the
	 *            documentoLayoutEnvioOcorrencia
	 *            to set
	 */
	void setDocumentoLayoutEnvioOcorrencia(DocumentoLayoutEnvioOcorrencia documentoLayoutEnvioOcorrencia);

}
