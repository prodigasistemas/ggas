/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 10:08:45
 @author vtavares
 */

package br.com.ggas.arrecadacao.agenciabancaria.negocio.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.ControladorBanco;
import br.com.ggas.arrecadacao.agenciabancaria.dominio.AgenciaBancariaVO;
import br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria;
import br.com.ggas.arrecadacao.agenciabancaria.repositorio.RepositorioAgenciaBancaria;
import br.com.ggas.arrecadacao.impl.AgenciaImpl;
import br.com.ggas.arrecadacao.impl.ContaBancariaImpl;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * 
 * Classe que implementa ControladorAgenciaBancaria
 * Controlador responsável pelos métodos 
 * que manipulam AgenciaBancariaImpl
 * 
 */
@Service("controladorAgenciaBancariaImpl")
@Transactional
public class ControladorAgenciaBancariaImpl implements ControladorAgenciaBancaria {

	@Autowired
	private RepositorioAgenciaBancaria repositorioAgenciaBancaria;

	@Autowired
	@Qualifier("controladorBanco")
	private ControladorBanco controladorBanco;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria#consultarAgenciaBancaria(br.com.ggas.arrecadacao.
	 * agenciabancaria
	 * .dominio.AgenciaBancariaVO)
	 */
	@Override
	public Collection<Agencia> consultarAgenciaBancaria(AgenciaBancariaVO agenciaBancariaVO) throws NegocioException,
					IllegalAccessException, InvocationTargetException {

		return repositorioAgenciaBancaria.consultarAgenciaBancaria(agenciaBancariaVO);
	}

	@Override
	public Collection<Agencia> consultarAgenciaBancaria(Map<String, Object> filtro)
					throws NegocioException {

		return repositorioAgenciaBancaria.consultarAgenciaBancaria(filtro);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria#removerAgenciaBancaria(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerAgenciaBancaria(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		repositorioAgenciaBancaria.remover(chavesPrimarias, dadosAuditoria);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria#
	 * inserirAgenciaBancaria(br.com.ggas.arrecadacao.agenciabancaria
	 * .dominio
	 * .AgenciaBancariaVO)
	 */
	@Override
	public void inserirAgenciaBancaria(AgenciaBancariaVO agenciaBancariaVO) throws NegocioException,
	IllegalAccessException, InvocationTargetException {

		Agencia agencia = converterAgenciaBancariaVOParaAgencia(agenciaBancariaVO);

		for (ContaBancaria contaBancaria : agenciaBancariaVO.getListaContaBancaria()) {
			contaBancaria.setAgencia(agencia);
			contaBancaria.setHabilitado(true);
			contaBancaria.setUltimaAlteracao(Calendar.getInstance().getTime());
		}

		if(agencia.getEmail() != null && !agencia.getEmail().isEmpty()) {
			Util.validarEmail(agencia.getEmail());
		}
		repositorioAgenciaBancaria.inserir(agencia);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria#adicionarContaBancariaLista(java.util.List,
	 * br.com.ggas.arrecadacao.ContaBancaria)
	 */
	@Override
	public List<ContaBancaria> adicionarContaBancariaLista(
					List<ContaBancaria> listaContaBancaria, ContaBancaria contaBancaria)
					throws NegocioException {
		List<ContaBancaria> listaContaBancariaRecebida = listaContaBancaria;
		Map<String, Object> erros = contaBancaria.validarDados();
		if(erros != null && !erros.isEmpty()) {
			throw new NegocioException(erros);
		}

		if(listaContaBancariaRecebida == null || listaContaBancariaRecebida.isEmpty()) {
			listaContaBancariaRecebida = new ArrayList<>();
			listaContaBancariaRecebida.add(contaBancaria);
		} else {
			if(!verificarContasDuplicadas(listaContaBancariaRecebida, contaBancaria)) {
				listaContaBancariaRecebida.add(contaBancaria);
			} else {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CONTA_CORRENTE_EXISTENTE, true);
			}
		}

		return listaContaBancariaRecebida;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria#alterarContaBancariaLista(java.util.List,
	 * br.com.ggas.arrecadacao.ContaBancaria, java.lang.Integer)
	 */
	@Override
	public List<ContaBancaria> alterarContaBancariaLista(List<ContaBancaria> listaContaBancaria, ContaBancaria contaBancaria,
					Integer indexLista) throws NegocioException {

		Map<String, Object> erros = contaBancaria.validarDados();
		if(erros != null && !erros.isEmpty()) {
			throw new NegocioException(erros);
		}

		List<ContaBancaria> listaContaBancariaAlterada = new ArrayList<>();
		listaContaBancariaAlterada.addAll(listaContaBancaria);

		if(indexLista != null) {
			ContaBancaria contaAlterada = ((ArrayList<ContaBancaria>) listaContaBancariaAlterada).get(indexLista);
			contaAlterada.setDescricao(contaBancaria.getDescricao());
			contaAlterada.setNumeroConta(contaBancaria.getNumeroConta());
			contaAlterada.setNumeroDigito(contaBancaria.getNumeroDigito());

			for (int i = 0; i < listaContaBancariaAlterada.size(); i++) {
				ContaBancaria contaExistente = ((ArrayList<ContaBancaria>) listaContaBancariaAlterada).get(i);
				verificaContaExistente(indexLista, contaAlterada, i, contaExistente);
			}
		}

		return listaContaBancariaAlterada;
	}
	/**
	 * 
	 * @param indexLista
	 * @param contaAlterada
	 * @param i
	 * @param contaExistente
	 * @throws NegocioException
	 */
	private void verificaContaExistente(Integer indexLista, ContaBancaria contaAlterada, int i, ContaBancaria contaExistente)
			throws NegocioException {
		if(i != indexLista && contaExistente != null && contaExistente.getNumeroConta().equals(contaAlterada.getNumeroConta())) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CONTA_CORRENTE_EXISTENTE, true);
		}
	}

	/**
	 * Verificar contas duplicadas.
	 * 
	 * @param listaContaBancaria the lista conta bancaria
	 * @param contaBancaria the conta bancaria
	 * @return true, se existerem contas duplicadas, false se não houver.
	 */
	private boolean verificarContasDuplicadas(List<ContaBancaria> listaContaBancaria, ContaBancaria contaBancaria) {

		for (ContaBancaria conta : listaContaBancaria) {
			if(conta.getNumeroConta().equals(contaBancaria.getNumeroConta())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Converter agencia bancaria vo para agencia.
	 * 
	 * @param agenciaBancariaVO
	 *            the agencia bancaria vo
	 * @return the agencia
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Agencia converterAgenciaBancariaVOParaAgencia(AgenciaBancariaVO agenciaBancariaVO)
			throws NegocioException {

		Agencia agencia = new AgenciaImpl();
		Util.copyProperties(agencia, agenciaBancariaVO);

		String chaveBanco = agenciaBancariaVO.getChaveBanco();
		if (chaveBanco != null && !chaveBanco.isEmpty()) {
			agencia.setBanco((Banco) controladorBanco.obter(Long.parseLong(chaveBanco)));
		}

		return agencia;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria#obterAgencia(java.lang.Long)
	 */
	@Override
	public Agencia obterAgencia(Long chavePrimaria) throws NegocioException {

		return (Agencia) repositorioAgenciaBancaria.obter(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria#obterAgeciaBancaria(java.lang.Long)
	 */
	@Override
	public AgenciaBancariaVO obterAgeciaBancaria(Long chavePrimaria) throws NegocioException {

		AgenciaBancariaVO agenciaBancariaVO = new AgenciaBancariaVO();

		Agencia agencia = (Agencia) repositorioAgenciaBancaria.obter(chavePrimaria);

		agenciaBancariaVO.setChaveBanco(String.valueOf(agencia.getBanco().getChavePrimaria()));
		agenciaBancariaVO.setChavePrimaria(agencia.getChavePrimaria());
		agenciaBancariaVO.setNome(agencia.getNome());
		agenciaBancariaVO.setFax(agencia.getFax());
		agenciaBancariaVO.setTelefone(agencia.getTelefone());
		agenciaBancariaVO.setEmail(agencia.getEmail());
		agenciaBancariaVO.setCodigo(agencia.getCodigo());
		agenciaBancariaVO.setHabilitado(String.valueOf(agencia.isHabilitado()));
		agenciaBancariaVO.setListaContaBancaria(agencia.getListaContaBancaria());

		return agenciaBancariaVO;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria#alterarrAgenciaBancaria(br.com.ggas.arrecadacao.
	 * agenciabancaria.
	 * dominio.AgenciaBancariaVO)
	 */
	@Override
	public void alterarrAgenciaBancaria(AgenciaBancariaVO agenciaBancariaVO) throws NegocioException,
					IllegalAccessException, InvocationTargetException, ConcorrenciaException {

		Agencia agencia = (Agencia) repositorioAgenciaBancaria.obter(agenciaBancariaVO.getChavePrimaria());

		agencia.setChavePrimaria(agenciaBancariaVO.getChavePrimaria());
		agencia.setNome(agenciaBancariaVO.getNome());
		agencia.setFax(agenciaBancariaVO.getFax());
		agencia.setTelefone(agenciaBancariaVO.getTelefone());
		agencia.setEmail(agenciaBancariaVO.getEmail());
		agencia.setCodigo(agenciaBancariaVO.getCodigo());
		agencia.setHabilitado(Boolean.parseBoolean(agenciaBancariaVO.getHabilitado()));

		String chaveBanco = agenciaBancariaVO.getChaveBanco();
		if(chaveBanco != null && !chaveBanco.isEmpty()) {
			agencia.setBanco((Banco) controladorBanco.obter(Long.parseLong(chaveBanco)));
		} else {
			agencia.setBanco(null);
		}

		if(agencia.getEmail() != null && !agencia.getEmail().isEmpty()) {
			Util.validarEmail(agencia.getEmail());
		}

		repositorioAgenciaBancaria.atualizar(agencia);

	}

	/**
	 * Remover contas bancarias.
	 * 
	 * @param listaContaBancaria
	 *            the lista conta bancaria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void removerContasBancarias(List<ContaBancaria> listaContaBancaria) throws NegocioException {

		for (ContaBancaria contaBancaria : listaContaBancaria) {
			repositorioAgenciaBancaria.remover(contaBancaria, ContaBancariaImpl.class, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria#listarContaBancariaPorAgencia(java.lang.Long)
	 */
	@Override
	public List<ContaBancaria> listarContaBancariaPorAgencia(Long chaveAgencia) throws NegocioException {

		return repositorioAgenciaBancaria.listarContaBancariaPorAgencia(chaveAgencia);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria#inserirContaBancaria(br.com.ggas.arrecadacao.agenciabancaria
	 * .dominio
	 * .AgenciaBancariaVO)
	 */
	@Override
	public void inserirContaBancaria(AgenciaBancariaVO agenciaBancariaVO) throws NegocioException, ConcorrenciaException {

		List<ContaBancaria> listaRemoverContaBancaria = agenciaBancariaVO.getListaRemoverContaBancaria();
		if(listaRemoverContaBancaria != null) {
			removerContasBancarias(listaRemoverContaBancaria);
		}

		Collection<ContaBancaria> listaInserirAlterarContaBancaria = agenciaBancariaVO.getListaContaBancaria();
		for (ContaBancaria contaBancaria : listaInserirAlterarContaBancaria) {
			if(contaBancaria.getChavePrimaria() != 0) {
				repositorioAgenciaBancaria.atualizar(contaBancaria, ContaBancariaImpl.class);
			} else {
				Agencia agencia = obterAgencia(agenciaBancariaVO.getChavePrimaria());
				contaBancaria.setAgencia(agencia);
				contaBancaria.setUltimaAlteracao(Calendar.getInstance().getTime());
				repositorioAgenciaBancaria.inserir(contaBancaria);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria#verificarAgenciaExistente(br.com.ggas.arrecadacao.
	 * agenciabancaria
	 * .dominio.AgenciaBancariaVO)
	 */
	@Override
	public void verificarAgenciaExistente(AgenciaBancariaVO agenciaBancariaVO) throws NegocioException {

		if(agenciaBancariaVO.getChavePrimaria() == null) {
			agenciaBancariaVO.setChavePrimaria(0L);
		}

		Agencia agenciaConsulta = repositorioAgenciaBancaria.existeAgencia(agenciaBancariaVO);

		if (agenciaConsulta != null && agenciaConsulta.getChavePrimaria() != agenciaBancariaVO.getChavePrimaria()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_AGENCIA_BANCARIA_EXISTENTE);
		}

	}
}
