/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 10:08:21
 @author vtavares
 */

package br.com.ggas.arrecadacao.agenciabancaria.negocio;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.agenciabancaria.dominio.AgenciaBancariaVO;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
/**
 * Controlador responsável pela agencia bancaria
 */
public interface ControladorAgenciaBancaria {

	/**
	 * Consultar agencia bancaria.
	 * 
	 * @param agenciaBancariaVO
	 *            the agencia bancaria vo
	 * @return the collection
	 * @throws GGASException - {@link GGASException} lançada caso ocorra falha de banco
	 * @throws ReflectiveOperationException - {@link ReflectiveOperationException} lançada caso ocorra falha ao utiliza o reflection
	 */
	Collection<Agencia> consultarAgenciaBancaria(AgenciaBancariaVO agenciaBancariaVO) throws GGASException, ReflectiveOperationException;
	/**
	 * Método responsável pela consulta de agencia bancária
	 * 
	 * @param filtro para pesquisar agências
	 * @throws NegocioException {@link NegocioException}
	 * @return coleção de agências bancárias
	 */
	Collection<Agencia> consultarAgenciaBancaria(Map<String, Object> filtro)
					throws NegocioException;

	/**
	 * Remover agencia bancaria.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerAgenciaBancaria(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Inserir agencia bancaria.
	 * 
	 * @param agenciaBancariaVO
	 *            the agencia bancaria vo {@link AgenciaBancariaVO}
	 * @throws NegocioException
	 *             the negocio exception {@link NegocioException}
	 * @throws ReflectiveOperationException {@link ReflectiveOperationException}
	 */
	void inserirAgenciaBancaria(AgenciaBancariaVO agenciaBancariaVO) throws NegocioException, ReflectiveOperationException;

	/**
	 * Alterarr agencia bancaria.
	 * 
	 * @param agenciaBancariaVO
	 *            the agencia bancaria vo
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws ReflectiveOperationException - {@link ReflectiveOperationException}
	 * @throws GGASException - {@link GGASException}
	 * 
	 */
	void alterarrAgenciaBancaria(AgenciaBancariaVO agenciaBancariaVO) throws ReflectiveOperationException, GGASException;

	/**
	 * Adicionar conta bancaria lista.
	 * 
	 * @param listaContaBancaria
	 *            the lista conta bancaria {@link List}
	 * @param contaBancaria
	 *            the conta bancaria {@link ContaBancaria}
	 * @return the list {@link List}
	 * @throws NegocioException
	 *             the negocio exception {@link NegocioException}
	 */
	List<ContaBancaria> adicionarContaBancariaLista(List<ContaBancaria> listaContaBancaria, ContaBancaria contaBancaria)
					throws NegocioException;

	/**
	 * Alterar conta bancaria lista.
	 * 
	 * @param listaContaBancaria
	 *            the lista conta bancaria
	 * @param contaBancaria
	 *            the conta bancaria
	 * @param indexLista
	 *            the index lista
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<ContaBancaria> alterarContaBancariaLista(List<ContaBancaria> listaContaBancaria, ContaBancaria contaBancaria, Integer indexLista)
					throws NegocioException;

	/**
	 * Obter agecia bancaria.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the agencia bancaria vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	AgenciaBancariaVO obterAgeciaBancaria(Long chavePrimaria) throws NegocioException;

	/**
	 * Obter agencia.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the agencia
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Agencia obterAgencia(Long chavePrimaria) throws NegocioException;

	/**
	 * Inserir conta bancaria.
	 * 
	 * @param agenciaBancariaVO
	 *            the agencia bancaria vo
	 * @throws GGASException
	 *             the GGASException
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void inserirContaBancaria(AgenciaBancariaVO agenciaBancariaVO) throws GGASException;

	/**
	 * Listar conta bancaria por agencia.
	 * 
	 * @param chaveAgencia
	 *            the chave agencia
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<ContaBancaria> listarContaBancariaPorAgencia(Long chaveAgencia) throws NegocioException;

	/**
	 * Verificar agencia existente.
	 * 
	 * @param agenciaBancariaVO
	 *            the agencia bancaria vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarAgenciaExistente(AgenciaBancariaVO agenciaBancariaVO) throws NegocioException;

}
