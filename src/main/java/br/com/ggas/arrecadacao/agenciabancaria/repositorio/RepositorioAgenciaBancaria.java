/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 10:07:41
 @author vtavares
 */

package br.com.ggas.arrecadacao.agenciabancaria.repositorio;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.agenciabancaria.dominio.AgenciaBancariaVO;
import br.com.ggas.arrecadacao.impl.AgenciaImpl;
import br.com.ggas.arrecadacao.impl.ContaBancariaImpl;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Util;
/**
 * Repositorio de agencia bancaria
 */
@Repository
public class RepositorioAgenciaBancaria extends RepositorioGenerico {

	private static final String BANCO = "banco";
	private static final String CODIGO = "codigo";
	private static final String BANCO_CHAVE_PRIMARIA = "banco.chavePrimaria";

	/**
	 * Instantiates a new repositorio agencia bancaria.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioAgenciaBancaria(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {
		return new AgenciaImpl();
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {
		return AgenciaImpl.class;
	}

	/**
	 * Consultar agencia bancaria.
	 * 
	 * @param agencia
	 *            the agencia
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<Agencia> consultarAgenciaBancaria(AgenciaBancariaVO agencia) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.setFetchMode(BANCO, FetchMode.JOIN);

		if (agencia != null) {
			
			String banco = agencia.getChaveBanco();
			if(banco != null && !banco.isEmpty()) {
				criteria.add(Restrictions.eq(BANCO_CHAVE_PRIMARIA, Long.parseLong(banco)));
			}

			String codigoAgencia = agencia.getCodigo();
			if(codigoAgencia != null && !codigoAgencia.isEmpty()) {
				criteria.add(Restrictions.eq(CODIGO, codigoAgencia));
			}

			String nomeAgencia = agencia.getNome();
			if(nomeAgencia != null && !nomeAgencia.isEmpty()) {
				criteria.add(Restrictions.ilike("nome", Util.formatarTextoConsulta(nomeAgencia)));
			}

			String habilitado = agencia.getHabilitado();
			if(agencia.getHabilitado() != null && !agencia.getHabilitado().isEmpty()) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.valueOf(habilitado)));
			}
		}
		return criteria.list();
	}
	
	/**
	 * Método responsável por consultar uma coleção de agências atráves de um filtro
	 * 
	 * @param filtro - {@link Map}
	 * @return coleção de agência - {@link Collection}
	 * @throws NegocioException - {@link NegocioException}
	 */
	public Collection<Agencia> consultarAgenciaBancaria(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.setFetchMode(BANCO, FetchMode.JOIN);

		if (filtro != null) {
			
			String banco = (String) filtro.get("chavePrimaria");
			if(banco != null && !banco.isEmpty()) {
				criteria.add(Restrictions.eq(BANCO_CHAVE_PRIMARIA, Long.parseLong(banco)));
			}

			String codigoAgencia = (String) filtro.get(CODIGO);
			if(codigoAgencia != null && !codigoAgencia.isEmpty()) {
				criteria.add(Restrictions.eq(CODIGO, codigoAgencia));
			}

			String nomeAgencia = (String) filtro.get("nome");
			if(nomeAgencia != null && !nomeAgencia.isEmpty()) {
				criteria.add(Restrictions.ilike("nome", Util.formatarTextoConsulta(nomeAgencia)));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}
		}
		return criteria.list();
	}

	/**
	 * Listar conta bancaria por agencia.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the list
	 */
	public List<ContaBancaria> listarContaBancariaPorAgencia(Long chavePrimaria) {

		Criteria criteria = createCriteria(ContaBancariaImpl.class);
		criteria.setFetchMode("agencia", FetchMode.JOIN);
		criteria.add(Restrictions.eq("agencia.chavePrimaria", chavePrimaria));
		return criteria.list();
	}

	/**
	 * Verifica se existe agencia.
	 * 
	 * @param agenciaBancariaVO
	 *            the agencia bancaria vo
	 * @return the agencia
	 */
	public Agencia existeAgencia(AgenciaBancariaVO agenciaBancariaVO) {

		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.setFetchMode(BANCO, FetchMode.JOIN);
		criteria.add(Restrictions.eq(CODIGO, agenciaBancariaVO.getCodigo()));

		String chaveBanco = agenciaBancariaVO.getChaveBanco();
		if(chaveBanco != null && !chaveBanco.isEmpty()) {
			criteria.add(Restrictions.eq(BANCO_CHAVE_PRIMARIA, Long.parseLong(chaveBanco)));
		}
		return (Agencia) criteria.uniqueResult();
	}
}
