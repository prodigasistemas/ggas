/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 10:10:53
 @author vtavares
 */

package br.com.ggas.arrecadacao.agenciabancaria.apresentacao;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.ControladorBanco;
import br.com.ggas.arrecadacao.agenciabancaria.dominio.AgenciaBancariaVO;
import br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria;
import br.com.ggas.arrecadacao.impl.BancoImpl;
import br.com.ggas.arrecadacao.impl.ContaBancariaImpl;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;
/**
 * classe responsável pela agencia bancaria
 */
@Controller
public class AgenciaBancariaAction extends GenericAction {

	private static final String GRID_CONTA_BANCARIA = "gridContaBancaria";

	private static final String TELA_FORWARD_PESQUISAR_AGENCIA_BANCARIA = "forward:/pesquisarAgenciaBancaria";

	private static final Logger LOG = Logger.getLogger(AgenciaBancariaAction.class);

	private static final String AGENCIA_BANCARIA = "Agência Bancária";

	private static final String FLUXO_INCLUSAO = "fluxoInclusao";

	private static final String FLUXO_ALTERACAO = "fluxoAlteracao";

	private static final String LISTA_CONTA_BANCARIA = "listaContaBancaria";

	private static final String LISTA_REMOVER_CONTA_BANCARIA = "listaRemoverContaBancaria";

	private static final String AJAX_ERRO = "ajaxErro";

	private static final String AGENCIA_BANCARIA_VO = "agenciaBancariaVO";

	private static final String AGENCIA = "agencia";

	/**
	  * Auto injeção do controlador
	 * para uso na classe.
	 */
	@Autowired
	@Qualifier("controladorBanco")
	public ControladorBanco controladorBanco;

	/**
	 * Auto injeção do controlador
	 * para uso na classe.
	 */
	@Autowired
	public ControladorAgenciaBancaria controladorAgenciaBancaria;

	/**
	 * Exibir pesquisa agencia bancaria.
	 * 
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirPesquisaAgenciaBancaria")
	public ModelAndView exibirPesquisaAgenciaBancaria(HttpSession sessao) throws NegocioException {

		ModelAndView model = new ModelAndView("exibirPesquisaAgenciaBancaria");

		carregarCampos(model);
		model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, "true");
		removerDadosSessao(sessao);

		return model;
	}

	/**
	 * Remover dados sessao.
	 * 
	 * @param sessao
	 *            the sessao
	 */
	private void removerDadosSessao(HttpSession sessao) {

		sessao.removeAttribute(LISTA_CONTA_BANCARIA);
		sessao.removeAttribute(LISTA_REMOVER_CONTA_BANCARIA);

	}

	/**
	 * Pesquisar agencia bancaria.
	 * 
	 * @param agenciaBancariaVO
	 *            the agencia bancaria vo
	 * @param request
	 *            the request
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * 
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("pesquisarAgenciaBancaria")
	public ModelAndView pesquisarAgenciaBancaria(@ModelAttribute(AGENCIA_BANCARIA_VO) AgenciaBancariaVO agenciaBancariaVO,
					HttpServletRequest request, HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaAgenciaBancaria");

		carregarCampos(model);

		try {
			model.addObject("listaAgenciaBancaria", controladorAgenciaBancaria.consultarAgenciaBancaria(agenciaBancariaVO));
		} catch (ReflectiveOperationException e) {
			throw new GGASException("Falha ao consultar agencia bancaria", e);
		}

		model.addObject(AGENCIA_BANCARIA_VO, agenciaBancariaVO);

		String habilitado = request.getParameter(EntidadeNegocio.ATRIBUTO_HABILITADO);
		if (habilitado == null) {
			model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, "true");
		} else {
			model.addObject(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado);
		}
		removerDadosSessao(sessao);
		return model;
	}

	/**
	 * Remover agencia bancaria.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("removerAgenciaBancaria")
	public ModelAndView removerAgenciaBancaria(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws NegocioException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_AGENCIA_BANCARIA);

		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());

			controladorAgenciaBancaria.removerAgenciaBancaria(chavesPrimarias, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDA, AGENCIA_BANCARIA);
		} catch (DataIntegrityViolationException ex) {
			LOG.error(ex.getMessage(), ex);
			try {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_AGENCIA_RELACIONADA, true);
			} catch (NegocioException e) {
				mensagemErroParametrizado(model, e);
			}
		}

		return model;
	}

	/**
	 * Exibir inclusao agencia bancaria.
	 * 
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirInclusaoAgenciaBancaria")
	public ModelAndView exibirInclusaoAgenciaBancaria() throws NegocioException {

		ModelAndView model = new ModelAndView("exibirInclusaoAgenciaBancaria");

		model.addObject(FLUXO_INCLUSAO, true);

		carregarCampos(model);

		return model;
	}

	/**
	 * Incluir agencia bancaria.
	 * 
	 * @param agenciaBancariaVO
	 *            the agencia bancaria vo
	 * @param sessao
	 *            the sessao
	 * @param result
	 *            the result
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 */
	@RequestMapping("incluirAgenciaBancaria")
	public ModelAndView incluirAgenciaBancaria(@ModelAttribute(AGENCIA_BANCARIA_VO) AgenciaBancariaVO agenciaBancariaVO,
			HttpSession sessao, BindingResult result) {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_AGENCIA_BANCARIA);

		try {

			List<ContaBancaria> listaContaBancaria = (List<ContaBancaria>) sessao.getAttribute(LISTA_CONTA_BANCARIA);
			if (listaContaBancaria != null) {
				agenciaBancariaVO.getListaContaBancaria().addAll(listaContaBancaria);
			}
			controladorAgenciaBancaria.verificarAgenciaExistente(agenciaBancariaVO);
			controladorAgenciaBancaria.inserirAgenciaBancaria(agenciaBancariaVO);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, AGENCIA_BANCARIA);

		} catch (NegocioException e) {
			model = new ModelAndView("exibirInclusaoAgenciaBancaria");
			try {
				carregarCampos(model);
			} catch (NegocioException ne) {
				LOG.error(ne);
			}
			model.addObject(AGENCIA_BANCARIA_VO, agenciaBancariaVO);
			mensagemErroParametrizado(model, e);
		} catch (ReflectiveOperationException ex){
			LOG.error(ex);
		} finally {
			model.addObject(FLUXO_INCLUSAO, true);
			sessao.removeAttribute(LISTA_REMOVER_CONTA_BANCARIA);
		}

		return model;
	}

	/**
	 * Exibir alteracao agencia bancaria.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirAlteracaoAgenciaBancaria")
	public ModelAndView exibirAlteracaoAgenciaBancaria(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao)
					throws NegocioException {

		ModelAndView model = new ModelAndView("exibirAlteracaoAgenciaBancaria");

		AgenciaBancariaVO agenciaBancariaVO = controladorAgenciaBancaria.obterAgeciaBancaria(chavePrimaria);

		model.addObject(AGENCIA_BANCARIA_VO, agenciaBancariaVO);

		sessao.setAttribute(LISTA_CONTA_BANCARIA, controladorAgenciaBancaria.listarContaBancariaPorAgencia(chavePrimaria));
		sessao.setAttribute(LISTA_REMOVER_CONTA_BANCARIA, new ArrayList<ContaBancaria>());

		model.addObject(FLUXO_ALTERACAO, true);

		carregarCampos(model);

		return model;
	}

	/**
	 * Alterar agencia bancaria.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param agenciaBancariaVO
	 *            the agencia bancaria vo
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException GGASException
	 */
	@RequestMapping("alterarAgenciaBancaria")
	public ModelAndView alterarAgenciaBancaria(@RequestParam("chavePrimaria") Long chavePrimaria,
			@ModelAttribute(AGENCIA_BANCARIA_VO) AgenciaBancariaVO agenciaBancariaVO, HttpSession sessao) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_FORWARD_PESQUISAR_AGENCIA_BANCARIA);

		try {
			agenciaBancariaVO.setChavePrimaria(chavePrimaria);
			agenciaBancariaVO.getListaContaBancaria().addAll((List<ContaBancaria>) sessao.getAttribute(LISTA_CONTA_BANCARIA));
			agenciaBancariaVO.getListaRemoverContaBancaria()
							.addAll((List<ContaBancaria>) sessao.getAttribute(LISTA_REMOVER_CONTA_BANCARIA));
			controladorAgenciaBancaria.verificarAgenciaExistente(agenciaBancariaVO);
			controladorAgenciaBancaria.alterarrAgenciaBancaria(agenciaBancariaVO);
			controladorAgenciaBancaria.inserirContaBancaria(agenciaBancariaVO);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, AGENCIA_BANCARIA);

		} catch (DataIntegrityViolationException ex) {
			LOG.error(ex.getMessage(), ex);
			try {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CONTA_BANCARIA_RELACIONADA, true);
			} catch (NegocioException e) {
				model = exibirAlteracaoAgenciaBancaria(chavePrimaria, sessao);
				model.addObject(AGENCIA_BANCARIA_VO, agenciaBancariaVO);
				carregarCampos(model);
				mensagemErroParametrizado(model, e);
			}
		} catch (NegocioException e) {
			model = exibirAlteracaoAgenciaBancaria(chavePrimaria, sessao);
			model.addObject(AGENCIA_BANCARIA_VO, agenciaBancariaVO);
			carregarCampos(model);
			mensagemErroParametrizado(model, e);
		} catch (ReflectiveOperationException e) {
			LOG.error(e.getMessage(), e);
		} finally {
			model.addObject(FLUXO_ALTERACAO, true);
		}

		return model;
	}

	/**
	 * Exibir detalhamento agencia bancaria.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirDetalhamentoAgenciaBancaria")
	public ModelAndView exibirDetalhamentoAgenciaBancaria(@RequestParam("chavePrimaria") Long chavePrimaria, HttpSession sessao)
					throws NegocioException {

		ModelAndView model = new ModelAndView("exibirDetalhamentoAgenciaBancaria");

		Agencia agencia = controladorAgenciaBancaria.obterAgencia(chavePrimaria);

		model.addObject(AGENCIA, agencia);
		model.addObject(LISTA_CONTA_BANCARIA, controladorAgenciaBancaria.listarContaBancariaPorAgencia(chavePrimaria));

		return model;
	}

	/**
	 * Adicionar conta bancaria.
	 * 
	 * @param descricao
	 *            the descricao
	 * @param numeroConta
	 *            the numero conta
	 * @param numeroDigito
	 *            the numero digito
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("adicionarContaBancaria")
	public ModelAndView adicionarContaBancaria(@RequestParam("descricao") String descricao,
					@RequestParam("numeroConta") String numeroConta, @RequestParam("numeroDigito") String numeroDigito, HttpSession sessao)
					throws GGASException {

		ModelAndView model = new ModelAndView(GRID_CONTA_BANCARIA);

		@SuppressWarnings("unchecked")
		List<ContaBancaria> listaContaBancaria = (List<ContaBancaria>) sessao.getAttribute(LISTA_CONTA_BANCARIA);

		try {
			ContaBancaria contaBancaria = new ContaBancariaImpl();
			contaBancaria.setDescricao(descricao);
			contaBancaria.setNumeroConta(numeroConta);
			contaBancaria.setNumeroDigito(numeroDigito);
			listaContaBancaria = controladorAgenciaBancaria.adicionarContaBancariaLista(listaContaBancaria, contaBancaria);
		} catch (GGASException e) {
			model = new ModelAndView(AJAX_ERRO);
			mensagemErroParametrizado(model, e);
		} finally {
			sessao.setAttribute(LISTA_CONTA_BANCARIA, listaContaBancaria);
		}

		return model;
	}

	/**
	 * Alterar conta bancaria.
	 * 
	 * @param descricao
	 *            the descricao
	 * @param numeroConta
	 *            the numero conta
	 * @param numeroDigito
	 *            the numero digito
	 * @param indexLista
	 *            the index lista
	 * @param sessao
	 *            the sessao
	 * @return the model and view
	 */
	@RequestMapping("alterarContaBancaria")
	public ModelAndView alterarContaBancaria(@RequestParam("descricao") String descricao, @RequestParam("numeroConta") String numeroConta,
					@RequestParam("numeroDigito") String numeroDigito, @RequestParam("indexListaContaBancaria") int indexLista,
					HttpSession sessao) {

		ModelAndView model = new ModelAndView(GRID_CONTA_BANCARIA);

		@SuppressWarnings("unchecked")
		List<ContaBancaria> listaContaBancaria = (List<ContaBancaria>) sessao.getAttribute(LISTA_CONTA_BANCARIA);

		try {
			ContaBancaria contaBancaria = new ContaBancariaImpl();
			contaBancaria.setDescricao(descricao);
			contaBancaria.setNumeroConta(numeroConta);
			contaBancaria.setNumeroDigito(numeroDigito);
			listaContaBancaria = controladorAgenciaBancaria.alterarContaBancariaLista(listaContaBancaria, contaBancaria, indexLista);
		} catch (GGASException e) {
			model = new ModelAndView(AJAX_ERRO);
			mensagemErroParametrizado(model, e);
		} finally {
			sessao.setAttribute(LISTA_CONTA_BANCARIA, listaContaBancaria);
		}

		return model;
	}

	/**
	 * Remover conta bancaria.
	 * Remove a conta bancária a partir da lista que
	 * está na sessão
	 * 
	 * @param indexLista
	 *            the index lista
	 * @param sessao
	 *            the sessao
	 * @return the model
	 * 			and view
	 */
	@RequestMapping("removerContaBancaria")
	public ModelAndView removerContaBancaria(@RequestParam("indexListaContaBancaria") int indexLista, HttpSession sessao) {

		ModelAndView model = new ModelAndView(GRID_CONTA_BANCARIA);

		List<ContaBancaria> listaContaBancaria = (List<ContaBancaria>) sessao.getAttribute(LISTA_CONTA_BANCARIA);
		List<ContaBancaria> listaRemoverContaBancaria = (List<ContaBancaria>) sessao.getAttribute(LISTA_REMOVER_CONTA_BANCARIA);

		if (listaContaBancaria != null) {
			ContaBancaria contaBancaria = listaContaBancaria.get(indexLista);
			if (listaRemoverContaBancaria == null) {
				listaRemoverContaBancaria = new ArrayList<ContaBancaria>();
			}
			listaRemoverContaBancaria.add(contaBancaria);
			listaContaBancaria.remove(indexLista);
			sessao.setAttribute(LISTA_CONTA_BANCARIA, listaContaBancaria);
			sessao.setAttribute(LISTA_REMOVER_CONTA_BANCARIA, listaRemoverContaBancaria);
		}

		return model;
	}

	/**
	 * Carregar campos.
	 * 
	 * @param model
	 *            the model
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void carregarCampos(ModelAndView model) throws NegocioException {

		model.addObject("listaBanco", controladorBanco.obterTodas(BancoImpl.class, true));
	}

}
