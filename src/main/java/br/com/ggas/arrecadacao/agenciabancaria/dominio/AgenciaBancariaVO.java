/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 11/06/2014 10:49:05
 @author vtavares
 */

package br.com.ggas.arrecadacao.agenciabancaria.dominio;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import br.com.ggas.arrecadacao.ContaBancaria;
/**
 * Classe que representa uma agência bancária
 */
public class AgenciaBancariaVO {

	private Long chavePrimaria;

	private String chaveBanco;

	private String codigo;

	private String nome;

	private String habilitado;

	private String telefone;

	private String fax;

	private String email;

	private Collection<ContaBancaria> listaContaBancaria = new HashSet<>();

	private List<ContaBancaria> listaRemoverContaBancaria = new ArrayList<>();

	/**
	 * @return chaveBanco
	 */
	public String getChaveBanco() {

		return chaveBanco;
	}
	/**
	 * @param chaveBanco
	 * define alteração da chave primária da agencia bancária
	 */
	public void setChaveBanco(String chaveBanco) {

		this.chaveBanco = chaveBanco;
	}

	/**
	 * recupera o código de uma agencia bancária 
         * @return codigo
	 */
	public String getCodigo() {

		return codigo;
	}
	/**
	 * @param codigo
	 * define código da agencia bancária
	 */
	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	/**
	 * recupera o nome de uma agencia bancária 
	 * @return nome
	 */
	public String getNome() {

		return nome;
	}
	/**
	 * @param nome
	 * define o nome da agencia bancária
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * recupera a informação se uma agencia bancária
	 * está  habilitada.
	 * @return habilitado
	 */
	public String getHabilitado() {

		return habilitado;
	}
	/**
	 * @param habilitado
	 * define se a agencia bancária está habilitada.
	 */
	public void setHabilitado(String habilitado) {

		this.habilitado = habilitado;
	}

	/**
	 * recupera o telefone de uma agencia bancária
	 * @return telefone 
	 */
	public String getTelefone() {

		return telefone;
	}
	/**
	 * @param telefone
	 * define o numero de telefone da agência bancária
	 */
	public void setTelefone(String telefone) {

		this.telefone = telefone;
	}

	/**
	 * recupera o numero de fax de uma agencia bancária.
	 * @return fax
	 */
	public String getFax() {

		return fax;
	}
	/**
	 * @param fax
	 * define número de fax da agência bancária
	 */
	public void setFax(String fax) {

		this.fax = fax;
	}

	/**
	 * recupera o email de uma agencia bancária.
	 * @return email
	 */
	public String getEmail() {

		return email;
	}
	/**
	 * @param email
	 * define o email da agência bancária
	 */
	public void setEmail(String email) {

		this.email = email;
	}
	
	/**
	 * recupera a chave primária de uma agencia bancária.
	 * @return chavePrimaria
	 */
	public Long getChavePrimaria() {

		return chavePrimaria;
	}
	/**
	 * @param chavePrimaria
	 * define a chave primária da agência bancária
	 */
	public void setChavePrimaria(Long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * @return listaContaBancaria
	 */
	public Collection<ContaBancaria> getListaContaBancaria() {

		return listaContaBancaria;
	}
	/**
	 * @param listaContaBancaria
	 * define uma lista de contas bancárias.
	 */
	public void setListaContaBancaria(Collection<ContaBancaria> listaContaBancaria) {

		this.listaContaBancaria = listaContaBancaria;
	}

	/**
	 * 
	 * recupera uma lista de contas a bancárias.
	 * @return listaContaBancaria
	 */
	public List<ContaBancaria> getListaRemoverContaBancaria() {

		return listaRemoverContaBancaria;
	}
	/**
	 * @param listaRemoverContaBancaria
	 * 
	 */
	public void setListaRemoverContaBancaria(List<ContaBancaria> listaRemoverContaBancaria) {

		this.listaRemoverContaBancaria = listaRemoverContaBancaria;
	}

}
