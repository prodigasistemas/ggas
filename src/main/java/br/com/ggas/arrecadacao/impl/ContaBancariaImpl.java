/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * 
 * Classe responsável pela representação da conta bancária
 */
public class ContaBancariaImpl extends EntidadeNegocioImpl implements ContaBancaria {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 909586395856965595L;

	private Agencia agencia;

	private String descricao;

	private String numeroConta;

	private String numeroDigito;

	private String numeroContaCobranca;
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.ContaBancaria
	 * #getAgencia()
	 */
	@Override
	public Agencia getAgencia() {

		return agencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.ContaBancaria
	 * #setAgencia
	 * (br.com.ggas.arrecadacao.impl.Agencia)
	 */
	@Override
	public void setAgencia(Agencia agencia) {

		this.agencia = agencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.ContaBancaria
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.ContaBancaria
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.ContaBancaria
	 * #getNumeroConta()
	 */
	@Override
	public String getNumeroConta() {

		return numeroConta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.ContaBancaria
	 * #setNumeroConta(java.lang.String)
	 */
	@Override
	public void setNumeroConta(String numeroConta) {

		this.numeroConta = numeroConta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.ContaBancaria
	 * #getNumeroDigito()
	 */
	@Override
	public String getNumeroDigito() {

		return numeroDigito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.ContaBancaria
	 * #setNumeroDigito(java.lang.String)
	 */
	@Override
	public void setNumeroDigito(String numeroDigito) {

		this.numeroDigito = numeroDigito;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(descricao == null || descricao.isEmpty()) {
			stringBuilder.append(Constantes.DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty("numeroDigito") || numeroDigito.trim().length() == 0) {
			stringBuilder.append("Dígito Verificador");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(numeroConta) || numeroConta.trim().length() == 0) {
			stringBuilder.append("Número da Conta");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	@Override
	public String getNumeroContaCobranca() {
		return numeroContaCobranca;
	}

	@Override
	public void setNumeroContaCobranca(String numeroContaCobranca) {
		this.numeroContaCobranca = numeroContaCobranca;
	}

}
