/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.arrecadacao.ArrecadadorMovimento;
import br.com.ggas.arrecadacao.ArrecadadorMovimentoItem;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
/**
 * Classe responsável pela representação do movimentação do arrecadador
 */
public class ArrecadadorMovimentoImpl extends EntidadeNegocioImpl implements ArrecadadorMovimento {

	private static final long serialVersionUID = 6466173490689330258L;

	private EntidadeConteudo formaArrecadacao;

	private Date dataGeracao;

	private Integer numeroNSA;

	private String codigoConvenio;

	private String nomeEmpresa;

	private Integer codigoEmpresa;

	private Integer codigoBanco;

	private String codigoAgencia;

	private String codigoDigitoAgencia;

	private String codigoContaBancaria;

	private String codigoDigitoContaBancaria;

	private String numeroVersaoLayout;

	private Integer codigoRemessa;

	private Integer quantidadeTotalRegistro;

	private BigDecimal valorTotal;
	
	private String nomeArquivo;

	private Collection<ArrecadadorMovimentoItem> itens = new HashSet<>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ArrecadadorMovimento
	 * #getItens()
	 */
	@Override
	public Collection<ArrecadadorMovimentoItem> getItens() {

		return itens;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ArrecadadorMovimento
	 * #setItens(java.util.Collection)
	 */
	@Override
	public void setItens(Collection<ArrecadadorMovimentoItem> itens) {

		this.itens = itens;
	}

	/**
	 * @return the formaArrecadacao
	 */
	@Override
	public EntidadeConteudo getFormaArrecadacao() {

		return formaArrecadacao;
	}

	/**
	 * @param formaArrecadacao
	 *            the formaArrecadacao to set
	 */
	@Override
	public void setFormaArrecadacao(EntidadeConteudo formaArrecadacao) {

		this.formaArrecadacao = formaArrecadacao;
	}

	/**
	 * @return the dataGeracao
	 */
	@Override
	public Date getDataGeracao() {
		Date dataGeracaoTmp = null;
		if(dataGeracao != null){
			dataGeracaoTmp = (Date) dataGeracao.clone();
		} 
		return dataGeracaoTmp;
	}

	/**
	 * @param dataGeracao
	 *            the dataGeracao to set
	 */
	@Override
	public void setDataGeracao(Date dataGeracao) {
		if(dataGeracao != null){
			this.dataGeracao = (Date) dataGeracao.clone();
		} else {
			this.dataGeracao = null;
		}
	}

	/**
	 * @return the numeroNSA
	 */
	@Override
	public Integer getNumeroNSA() {

		return numeroNSA;
	}

	/**
	 * @param numeroNSA
	 *            the numeroNSA to set
	 */
	@Override
	public void setNumeroNSA(Integer numeroNSA) {

		this.numeroNSA = numeroNSA;
	}

	/**
	 * @return the codigoConvenio
	 */
	@Override
	public String getCodigoConvenio() {

		return codigoConvenio;
	}

	/**
	 * @param codigoConvenio
	 *            the codigoConvenio to set
	 */
	@Override
	public void setCodigoConvenio(String codigoConvenio) {

		this.codigoConvenio = codigoConvenio;
	}

	/**
	 * @return the nomeEmpresa
	 */
	@Override
	public String getNomeEmpresa() {

		return nomeEmpresa;
	}

	/**
	 * @param nomeEmpresa
	 *            the nomeEmpresa to set
	 */
	@Override
	public void setNomeEmpresa(String nomeEmpresa) {

		this.nomeEmpresa = nomeEmpresa;
	}

	/**
	 * @return the codigoEmpresa
	 */
	@Override
	public Integer getCodigoEmpresa() {

		return codigoEmpresa;
	}

	/**
	 * @param codigoEmpresa
	 *            the codigoEmpresa to set
	 */
	@Override
	public void setCodigoEmpresa(Integer codigoEmpresa) {

		this.codigoEmpresa = codigoEmpresa;
	}

	/**
	 * @return the codigoBanco
	 */
	@Override
	public Integer getCodigoBanco() {

		return codigoBanco;
	}

	/**
	 * @param codigoBanco
	 *            the codigoBanco to set
	 */
	@Override
	public void setCodigoBanco(Integer codigoBanco) {

		this.codigoBanco = codigoBanco;
	}

	/**
	 * @return the codigoAgencia
	 */
	@Override
	public String getCodigoAgencia() {

		return codigoAgencia;
	}

	/**
	 * @param codigoAgencia
	 *            the codigoAgencia to set
	 */
	@Override
	public void setCodigoAgencia(String codigoAgencia) {

		this.codigoAgencia = codigoAgencia;
	}

	/**
	 * @return the codigoDigitoAgencia
	 */
	@Override
	public String getCodigoDigitoAgencia() {

		return codigoDigitoAgencia;
	}

	/**
	 * @param codigoDigitoAgencia
	 *            the codigoDigitoAgencia to set
	 */
	@Override
	public void setCodigoDigitoAgencia(String codigoDigitoAgencia) {

		this.codigoDigitoAgencia = codigoDigitoAgencia;
	}

	/**
	 * @return the codigoContaBancaria
	 */
	@Override
	public String getCodigoContaBancaria() {

		return codigoContaBancaria;
	}

	/**
	 * @param codigoContaBancaria
	 *            the codigoContaBancaria to set
	 */
	@Override
	public void setCodigoContaBancaria(String codigoContaBancaria) {

		this.codigoContaBancaria = codigoContaBancaria;
	}

	/**
	 * @return the codigoDigitoContaBancaria
	 */
	@Override
	public String getCodigoDigitoContaBancaria() {

		return codigoDigitoContaBancaria;
	}

	/**
	 * @param codigoDigitoContaBancaria
	 *            the codigoDigitoContaBancaria to
	 *            set
	 */
	@Override
	public void setCodigoDigitoContaBancaria(String codigoDigitoContaBancaria) {

		this.codigoDigitoContaBancaria = codigoDigitoContaBancaria;
	}

	/**
	 * @return the numeroVersaoLayout
	 */
	@Override
	public String getNumeroVersaoLayout() {

		return numeroVersaoLayout;
	}

	/**
	 * @param numeroVersaoLayout
	 *            the numeroVersaoLayout to set
	 */
	@Override
	public void setNumeroVersaoLayout(String numeroVersaoLayout) {

		this.numeroVersaoLayout = numeroVersaoLayout;
	}

	/**
	 * @return the codigoRemessa
	 */
	@Override
	public Integer getCodigoRemessa() {

		return codigoRemessa;
	}

	/**
	 * @param codigoRemessa
	 *            the codigoRemessa to set
	 */
	@Override
	public void setCodigoRemessa(Integer codigoRemessa) {

		this.codigoRemessa = codigoRemessa;
	}

	/**
	 * @return the quantidadeTotalRegistro
	 */
	@Override
	public Integer getQuantidadeTotalRegistro() {

		return quantidadeTotalRegistro;
	}

	/**
	 * @param quantidadeTotalRegistro
	 *            the quantidadeTotalRegistro to
	 *            set
	 */
	@Override
	public void setQuantidadeTotalRegistro(Integer quantidadeTotalRegistro) {

		this.quantidadeTotalRegistro = quantidadeTotalRegistro;
	}

	/**
	 * @return the valorTotal
	 */
	@Override
	public BigDecimal getValorTotal() {

		return valorTotal;
	}

	/**
	 * @param valorTotal
	 *            the valorTotal to set
	 */
	@Override
	public void setValorTotal(BigDecimal valorTotal) {

		this.valorTotal = valorTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public String getNomeArquivo() {
		return nomeArquivo;
	}

	@Override
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
}
