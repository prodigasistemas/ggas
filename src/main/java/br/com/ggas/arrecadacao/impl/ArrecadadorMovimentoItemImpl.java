/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Map;

import br.com.ggas.arrecadacao.ArrecadadorMovimento;
import br.com.ggas.arrecadacao.ArrecadadorMovimentoItem;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsável por representar o item da movimentação do arrecadador
 *
 */
public class ArrecadadorMovimentoItemImpl extends EntidadeNegocioImpl implements ArrecadadorMovimentoItem {

	/**
	 * serialVersionUID
	 * 
	 */

	private static final long serialVersionUID = -2121982398188753795L;

	private ArrecadadorMovimento arrecadadorMovimento;

	private String linhaRegistro;

	private String descricaoOcorrencia;

	private Boolean aceitacao;

	/**
	 * @return the arrecadadorMovimento
	 */
	@Override
	public ArrecadadorMovimento getArrecadadorMovimento() {

		return arrecadadorMovimento;
	}

	/**
	 * @param arrecadadorMovimento
	 *            the arrecadadorMovimento to set
	 */
	@Override
	public void setArrecadadorMovimento(ArrecadadorMovimento arrecadadorMovimento) {

		this.arrecadadorMovimento = arrecadadorMovimento;
	}

	/**
	 * @return the linhaRegistro
	 */
	@Override
	public String getLinhaRegistro() {

		return linhaRegistro;
	}

	/**
	 * @param linhaRegistro
	 *            the linhaRegistro to set
	 */
	@Override
	public void setLinhaRegistro(String linhaRegistro) {

		this.linhaRegistro = linhaRegistro;
	}

	/**
	 * @return the descricaoOcorrencia
	 */
	@Override
	public String getDescricaoOcorrencia() {

		return descricaoOcorrencia;
	}

	/**
	 * @param descricaoOcorrencia
	 *            the descricaoOcorrencia to set
	 */
	@Override
	public void setDescricaoOcorrencia(String descricaoOcorrencia) {

		this.descricaoOcorrencia = descricaoOcorrencia;
	}

	/**
	 * @return the aceitacao
	 */
	@Override
	public Boolean getAceitacao() {

		return aceitacao;
	}

	/**
	 * @param aceitacao
	 *            the aceitacao to set
	 */
	@Override
	public void setAceitacao(Boolean aceitacao) {

		this.aceitacao = aceitacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
