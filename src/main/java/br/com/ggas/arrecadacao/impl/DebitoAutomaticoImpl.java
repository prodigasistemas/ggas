/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ClienteDebitoAutomatico;
import br.com.ggas.arrecadacao.DebitoAutomatico;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

class DebitoAutomaticoImpl extends EntidadeNegocioImpl implements DebitoAutomatico {

	private static final long serialVersionUID = 5611360362666229652L;

	private PontoConsumo pontoConsumo;

	private Cliente cliente;

	private Arrecadador arrecadador;

	private String agencia;

	private String conta;

	private String numeroAgendamento;

	private Date dataOpcao;

	private Date dataInclusao;

	private Date dataExclusao;

	private ClienteDebitoAutomatico clienteDebitoAutomatico;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoAutomatico
	 * #getClienteDebitoAutomatico()
	 */
	@Override
	public ClienteDebitoAutomatico getClienteDebitoAutomatico() {

		return clienteDebitoAutomatico;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoAutomatico
	 * #setClienteDebitoAutomatico
	 * (br.com.ggas.arrecadacao
	 * .impl.ClienteDebitoAutomatico)
	 */
	@Override
	public void setClienteDebitoAutomatico(ClienteDebitoAutomatico clienteDebitoAutomatico) {

		this.clienteDebitoAutomatico = clienteDebitoAutomatico;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoAutomatico
	 * #getNumeroAgendamento()
	 */
	@Override
	public String getNumeroAgendamento() {

		return numeroAgendamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoAutomatico
	 * #setNumeroAgendamento(java.lang.String)
	 */
	@Override
	public void setNumeroAgendamento(String numeroAgendamento) {

		this.numeroAgendamento = numeroAgendamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #getPontoConsumo()
	 */
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #setPontoConsumo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #getCliente()
	 */
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #setCliente
	 * (br.com.ggas.cadastro.cliente.Cliente)
	 */
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoAutomatico
	 * #getArrecadador()
	 */
	@Override
	public Arrecadador getArrecadador() {

		return arrecadador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoAutomatico
	 * #setArrecadador
	 * (br.com.ggas.arrecadacao.Arrecadador)
	 */
	@Override
	public void setArrecadador(Arrecadador arrecadador) {

		this.arrecadador = arrecadador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #getAgencia()
	 */
	@Override
	public String getAgencia() {

		return agencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #setAgencia(java.lang.String)
	 */
	@Override
	public void setAgencia(String agencia) {

		this.agencia = agencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #getConta()
	 */
	@Override
	public String getConta() {

		return conta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #setConta(java.lang.String)
	 */
	@Override
	public void setConta(String conta) {

		this.conta = conta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #getDataOpcao()
	 */
	@Override
	public Date getDataOpcao() {

		return dataOpcao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #setDataOpcao(java.sql.Date)
	 */
	@Override
	public void setDataOpcao(Date dataOpcao) {

		this.dataOpcao = dataOpcao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #getDataInclusao()
	 */
	@Override
	public Date getDataInclusao() {

		return dataInclusao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #setDataInclusao(java.sql.Date)
	 */
	@Override
	public void setDataInclusao(Date dataInclusao) {

		this.dataInclusao = dataInclusao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #getDataExclusao()
	 */
	@Override
	public Date getDataExclusao() {

		return dataExclusao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.DebitoAutomatico
	 * #setDataExclusao(java.sql.Date)
	 */
	@Override
	public void setDataExclusao(Date dataExclusao) {

		this.dataExclusao = dataExclusao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
