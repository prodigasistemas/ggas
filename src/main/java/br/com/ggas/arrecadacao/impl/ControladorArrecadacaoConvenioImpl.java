/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorArrecadacaoImpl representa uma ControladorArrecadacaoImpl no sistema.
 *
 * @since 12/02/2010
 * 
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.arrecadacao.ArrecadadorCarteiraCobranca;
import br.com.ggas.arrecadacao.ArrecadadorContrato;
import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.ControladorArrecadadorConvenio;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.impl.DocumentoCobrancaItemImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * 
 * 
 */
class ControladorArrecadacaoConvenioImpl extends ControladorNegocioImpl implements ControladorArrecadadorConvenio {

	private static final String ARRECADADOR_CONVENIO = "arrecadadorContrato";

	private static final String TIPO_CONVENIO = "tipoConvenio";

	private static final String CONTA_CONVENIO = "contaConvenio";

	private static final String CONTA_CREDITO = "contaCredito";

	private static final String CODIGO_CONVENIO = "codigoConvenio";

	private static final String LEIAUTE = "leiaute";

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(ArrecadadorContratoConvenio.BEAN_ID_ARRECADADOR_CONTRATO_CONVENIO);
	}

	public Class<?> getClasseContratoArrecadador() {

		return ServiceLocator.getInstancia().getClassPorID(ArrecadadorContrato.BEAN_ID_ARRECADADOR_CONTRATO);
	}

	public Class<?> getClasseEntidadeConteudo() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}

	public Class<?> getClasseContaBancaria() {

		return ServiceLocator.getInstancia().getClassPorID(ContaBancaria.BEAN_ID_CONTA_BANCARIA);
	}

	public Class<?> getClasseCarteiraCobranca() {

		return ServiceLocator.getInstancia().getClassPorID(ArrecadadorCarteiraCobranca.BEAN_ID_ARRECADADOR_CARTEIRA_COBRANCA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#listarArrecadadorContrato()
	 */
	@Override
	public Collection<ArrecadadorContrato> listarArrecadadorContrato() {

		Criteria criteria = this.createCriteria(ArrecadadorContrato.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc("numeroContrato"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#listarContaBancaria()
	 */
	@Override
	public Collection<ContaBancaria> listarContaBancaria() {

		Criteria criteria = this.createCriteria(ContaBancaria.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#listarCarteiraCobranca()
	 */
	@Override
	public Collection<ArrecadadorCarteiraCobranca> listarCarteiraCobranca() {

		Criteria criteria = this.createCriteria(ArrecadadorCarteiraCobranca.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#consultarArrecadadorConvenio(java.util.Map)
	 */
	@Override
	public Collection<ArrecadadorContratoConvenio> consultarArrecadadorConvenio(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(ArrecadadorContratoConvenio.class);

		criteria.createAlias(ARRECADADOR_CONVENIO, ARRECADADOR_CONVENIO);
		criteria.createAlias(TIPO_CONVENIO, TIPO_CONVENIO);
		criteria.createAlias(CONTA_CONVENIO, CONTA_CONVENIO, CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias(CONTA_CREDITO, CONTA_CREDITO, CriteriaSpecification.LEFT_JOIN);
		criteria.createAlias(LEIAUTE, LEIAUTE, CriteriaSpecification.LEFT_JOIN);

		if (filtro != null) {

			Long idArrecadadorConvenio = (Long) filtro.get("idArrecadadorConvenio");
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");

			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			if (idArrecadadorConvenio != null && idArrecadadorConvenio > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idArrecadadorConvenio));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String habilitado = String.valueOf(filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO));
			if(habilitado != null && !habilitado.isEmpty() && "true".equalsIgnoreCase(habilitado)) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
			} else if("false".equalsIgnoreCase(habilitado)) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.FALSE));
			}

			Long arrecadadorContrato = (Long) filtro.get(ARRECADADOR_CONVENIO);
			if (arrecadadorContrato != null && arrecadadorContrato > 0) {
				criteria.add(Restrictions.eq("arrecadadorContrato.chavePrimaria", arrecadadorContrato));
			}

			Long tipoConvenio = (Long) filtro.get(TIPO_CONVENIO);
			if (tipoConvenio != null && tipoConvenio > 0) {
				criteria.add(Restrictions.eq("tipoConvenio.chavePrimaria", tipoConvenio));
			}

			Long contaConvenio = (Long) filtro.get(CONTA_CONVENIO);
			if (contaConvenio != null && contaConvenio > 0) {
				criteria.add(Restrictions.eq("contaConvenio.chavePrimaria", contaConvenio));
			}

			Long contaCredito = (Long) filtro.get(CONTA_CREDITO);
			if (contaCredito != null && contaCredito > 0) {
				criteria.add(Restrictions.eq("contaCredito.chavePrimaria", contaCredito));
			}

			Long leiaute = (Long) filtro.get(LEIAUTE);
			if (leiaute != null && leiaute > 0) {
				criteria.add(Restrictions.eq("leiaute.chavePrimaria", leiaute));
			}

			Long arrecadadorCarteiraCobranca = (Long) filtro.get("arrecadadorCarteiraCobranca");
			if (arrecadadorCarteiraCobranca != null && arrecadadorCarteiraCobranca > 0) {
				criteria.add(Restrictions.eq("arrecadadorCarteiraCobranca.chavePrimaria", arrecadadorCarteiraCobranca));
			}

			String codigoConvenio = (String) filtro.get(CODIGO_CONVENIO);
			if (!StringUtils.isEmpty(codigoConvenio)) {
				criteria.add(Restrictions.ilike(CODIGO_CONVENIO, codigoConvenio, MatchMode.ANYWHERE));
			}
		}

		// Paginação em banco dados
		boolean paginacaoPadrao = false;
		if (filtro != null && filtro.containsKey("colecaoPaginada")) {
			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if (paginacaoPadrao) {
			criteria.addOrder(Order.asc("arrecadadorContrato.numeroContrato"));
			criteria.addOrder(Order.asc(CODIGO_CONVENIO));
			criteria.addOrder(Order.asc("contaConvenio.numeroConta"));
			criteria.addOrder(Order.asc("contaCredito.numeroConta"));
			criteria.addOrder(Order.asc("tipoConvenio.descricao"));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#obterArrecadadorConvenio(java.lang.Long)
	 */
	@Override
	public ArrecadadorContratoConvenio obterArrecadadorConvenio(Long idArrecadadorConvenio) throws NegocioException {

		return (ArrecadadorContratoConvenio) super.obter(idArrecadadorConvenio, getClasseEntidade(), LEIAUTE, "leiaute.formaArrecadacao");
	}
	
	/**
	 * 
	 * @param idArrecadadorConvenio - {@link - Long}
	 * @param propriedadesLazy - {@link - String}
	 * @return ArrecadorContratoConvenio - {@link - ArrecadadorContratoConvenio}
	 * @throws NegocioException - {@link - NegocioException}
	 */
	public ArrecadadorContratoConvenio obterArrecadadorConvenio(Long idArrecadadorConvenio, String... propriedadesLazy)
			throws NegocioException {

		return (ArrecadadorContratoConvenio) obter(idArrecadadorConvenio, ArrecadadorContratoConvenioImpl.class, propriedadesLazy);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.arrecadacao
	 * .ControladorArrecadadorConvenio
	 * #obterArrecadadorContratoConvenioPadrao()
	 */
	@Override
	public ArrecadadorContratoConvenio obterArrecadadorContratoConvenioPadrao() {

		Criteria criteria = createCriteria(ArrecadadorContratoConvenio.class);
		criteria.add(Restrictions.eq("indicadorPadrao", Boolean.TRUE));

		return (ArrecadadorContratoConvenio) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#obterArrecadadorContrato(java.lang.Long)
	 */
	@Override
	public ArrecadadorContrato obterArrecadadorContrato(Long idArrecadadorContrato) throws NegocioException {

		return (ArrecadadorContrato) super.obter(idArrecadadorContrato, getClasseContratoArrecadador());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#obterArrecadadorCarteiraCobranca(java.lang.Long)
	 */
	@Override
	public ArrecadadorCarteiraCobranca obterArrecadadorCarteiraCobranca(Long idArrecadadorCarteiraCobranca) throws GGASException {

		return (ArrecadadorCarteiraCobranca) super.obter(idArrecadadorCarteiraCobranca, getClasseCarteiraCobranca());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#obterContaBancaria(java.lang.Long)
	 */
	@Override
	public ContaBancaria obterContaBancaria(Long idContaBancaria) throws NegocioException {

		return (ContaBancaria) super.obter(idContaBancaria, getClasseContaBancaria());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#inserirArrecadadorContratoConvenio(br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio)
	 */
	@Override
	public Long inserirArrecadadorContratoConvenio(ArrecadadorContratoConvenio arrecadadorContratoConvenio) throws GGASException {

		validarExiste(arrecadadorContratoConvenio);
		arrecadadorContratoConvenio.setIndicadorPadrao(false);
		return this.inserir(arrecadadorContratoConvenio);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#alterarArrecadadorContratoConvenio(br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio)
	 */
	@Override
	public void alterarArrecadadorContratoConvenio(ArrecadadorContratoConvenio arrecadadorContratoConvenio) throws ConcorrenciaException,
					NegocioException {

		validarExiste(arrecadadorContratoConvenio);
		if (!arrecadadorContratoConvenio.isHabilitado()) {
			validarInatividade(arrecadadorContratoConvenio);
		}
		super.atualizar(arrecadadorContratoConvenio, getClasseEntidade());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#tornarPadraoArrecadadorContratoConvenio(java.lang.Long)
	 */
	@Override
	public void tornarPadraoArrecadadorContratoConvenio(Long chavePrimaria) throws ConcorrenciaException, NegocioException {

		Collection<ArrecadadorContratoConvenio> arrecadadorContratoConvenios = consultarArrecadadorConvenio(null);

		if (arrecadadorContratoConvenios != null) {
			for (ArrecadadorContratoConvenio arrecadadorContratoConvenio : arrecadadorContratoConvenios) {
				arrecadadorContratoConvenio.setIndicadorPadrao(arrecadadorContratoConvenio.getChavePrimaria() == chavePrimaria);
				alterarArrecadadorContratoConvenio(arrecadadorContratoConvenio);
			}
		}

	}

	/**
	 * Validar existe.
	 * 
	 * @param arrecadadorContratoConvenio
	 *            the arrecadador contrato convenio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarExiste(ArrecadadorContratoConvenio arrecadadorContratoConvenio) throws NegocioException {

		String codigoConvenio = arrecadadorContratoConvenio.getCodigoConvenio();
		ArrecadadorContrato arrecadadorContrato = arrecadadorContratoConvenio.getArrecadadorContrato();

		if (!StringUtils.isEmpty(codigoConvenio) && arrecadadorContrato != null) {

			Criteria criteria = createCriteria(ArrecadadorContratoConvenio.class);
			criteria.add(Restrictions.eq(CODIGO_CONVENIO, codigoConvenio));
			criteria.add(Restrictions.eq("arrecadadorContrato.chavePrimaria", arrecadadorContrato.getChavePrimaria()));

			Collection<ArrecadadorContratoConvenio> listaArrecadadorContratoConvenio = criteria.list();

			validadorArrecadadorContrato(arrecadadorContratoConvenio, listaArrecadadorContratoConvenio);
		}

	}

	private void validadorArrecadadorContrato(ArrecadadorContratoConvenio arrecadadorContratoConvenio,
					Collection<ArrecadadorContratoConvenio> listaArrecadadorContratoConvenio) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String convenioBoleto =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_FORMA_COBRANCA_BOLETO_BANCARIO);

		if (!listaArrecadadorContratoConvenio.isEmpty()) {
			for (ArrecadadorContratoConvenio arrecadadorConvenio : listaArrecadadorContratoConvenio) {
				if (arrecadadorConvenio.getChavePrimaria() != arrecadadorContratoConvenio.getChavePrimaria()) {
					throw new NegocioException(Constantes.ERRO_NEGOCIO_ARRECADADOR_CONVENIO_EXISTENTE_CONTRATO, true);
				}
			}
		}

		if (arrecadadorContratoConvenio.getTipoConvenio().getChavePrimaria() == Long.parseLong(convenioBoleto)
						&& arrecadadorContratoConvenio.getArrecadadorCarteiraCobranca() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ARRECADADOR_CONVENIO_BOLETO_SEM_CARTEIRA, true);
		}

	}

	/**
	 * Validar inatividade.
	 * 
	 * @param arrecadadorContratoConvenio
	 *            the arrecadador contrato convenio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarInatividade(ArrecadadorContratoConvenio arrecadadorContratoConvenio) throws NegocioException {

		if (!arrecadadorContratoConvenio.isIndicadorPadrao()) {

			ControladorContrato controladorContrato =
							(ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(
											ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
			Collection<Contrato> contratos =
							controladorContrato.consultarContratoPorArrecadadorConvenioContrato(arrecadadorContratoConvenio
											.getChavePrimaria());

			if (!Util.isNullOrEmpty(contratos)) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_ARRECADADOR_CONVENIO_INATIVO_CONTRATO_ATIVO, true);
			}

		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ARRECADADOR_CONVENIO_PADRAO_INATIVO, true);
		}

	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadadorConvenio#obterArrecadadorContratoConvenioPorDocumentoCobranca(
	 * br.com.ggas.faturamento.DocumentoCobranca)
	 */
	public ArrecadadorContratoConvenio obterArrecadadorContratoConvenioPorDocumentoCobranca(DocumentoCobranca documentoCobranca){
		Criteria criteria = createCriteria(getClasseEntidade(), "arcc");
		criteria.setFetchMode("tipoConvenio", FetchMode.JOIN);
		
		DetachedCriteria subQuery = DetachedCriteria.forClass(DocumentoCobrancaItemImpl.class, "doc_cob_item");
		subQuery.createAlias("doc_cob_item.faturaGeral", "fat_geral");
		subQuery.createAlias("fat_geral.faturaAtual", "fat_atual");
		subQuery.createAlias("fat_atual.contratoAtual", "cont_atual");
		subQuery.createAlias("cont_atual.situacao", "sit_cont");
		
		subQuery.add(Restrictions.not(Restrictions.ilike("sit_cont.descricao", "Encerrado", MatchMode.EXACT)));
		subQuery.add(Restrictions.eq("doc_cob_item.documentoCobranca.chavePrimaria", documentoCobranca.getChavePrimaria()));
		
		subQuery.setProjection(Projections.property("cont_atual.arrecadadorContratoConvenio.chavePrimaria"));
		
		criteria.add(Subqueries.propertyIn("arcc.chavePrimaria", subQuery));
		
		return (ArrecadadorContratoConvenio) criteria.uniqueResult();
	}
	
	@Override
	@Cacheable("arrecadadorCarteiraCobranca")
	public ArrecadadorCarteiraCobranca obterCarteiraCobrancaDeConvenio(Long chaveArrecadadorConvenio) {
		
		StringBuilder hql = new StringBuilder();
		
		hql.append(" select convenio.arrecadadorCarteiraCobranca ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" convenio ");
		hql.append(" where ");
		hql.append(" convenio.chavePrimaria = :chavePrimaria ");
		
		Query query = getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(EntidadeConteudo.ATRIBUTO_CHAVE_PRIMARIA, chaveArrecadadorConvenio);
		
		return (ArrecadadorCarteiraCobranca) query.uniqueResult();
	}

	
}
