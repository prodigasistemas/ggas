/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Map;

import br.com.ggas.arrecadacao.ClienteDebitoAutomatico;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

class ClienteDebitoAutomaticoImpl extends EntidadeNegocioImpl implements ClienteDebitoAutomatico {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5480752948551834412L;

	private Cliente cliente;

	private Imovel imovel;

	private Contrato contrato;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ClienteDebitoAutomatico#getCliente()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ClienteDebitoAutomatico
	 * #setCliente(br.com.ggas
	 * .cadastro.cliente.Cliente)
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ClienteDebitoAutomatico#getImovel()
	 */
	@Override
	public Imovel getImovel() {

		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ClienteDebitoAutomatico
	 * #setImovel(br.com.ggas
	 * .cadastro.imovel.Imovel)
	 */
	@Override
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ClienteDebitoAutomatico#getContrato()
	 */
	@Override
	public Contrato getContrato() {

		return contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ClienteDebitoAutomatico
	 * #setContrato(br.com.ggas
	 * .contrato.contrato.Contrato)
	 */
	@Override
	public void setContrato(Contrato contrato) {

		this.contrato = contrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
