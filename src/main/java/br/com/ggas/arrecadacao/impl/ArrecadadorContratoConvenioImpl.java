/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.ArrecadadorCarteiraCobranca;
import br.com.ggas.arrecadacao.ArrecadadorContrato;
import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.documento.DocumentoLayout;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável por representar o convênio com o contrato e o arrecadador
 */
public class ArrecadadorContratoConvenioImpl extends EntidadeNegocioImpl implements ArrecadadorContratoConvenio {

	private static final int LIMITE_CAMPO = 2;

	public static final String INDICADOR_PADRAO_STRING_SIM = "Sim";

	public static final String INDICADOR_PADRAO_STRING_NAO = "Não";

	private static final long serialVersionUID = -1832912129551453511L;

	private ArrecadadorContrato arrecadadorContrato;

	private EntidadeConteudo tipoConvenio;

	private String codigoConvenio;

	private Integer numeroDiasFloat;

	private Long nsaRemessa;

	private Long nsaRetorno;

	private Long sequencialCobrancaInicio;

	private Long sequencialCobrancaFim;

	private Integer tentativasReenvio;

	private ContaBancaria contaConvenio;

	private ContaBancaria contaCredito;

	private ArrecadadorCarteiraCobranca arrecadadorCarteiraCobranca;

	private Long sequenciaCobrancaGerado;

	private Boolean indicadorPadrao;

	private DocumentoLayout leiaute;

	private String arquivoLayoutFatura;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #getContaConvenio()
	 */
	@Override
	public ContaBancaria getContaConvenio() {

		return contaConvenio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #setContaConvenio
	 * (br.com.ggas.arrecadacao.impl
	 * .ContaBancaria)
	 */
	@Override
	public void setContaConvenio(ContaBancaria contaConvenio) {

		this.contaConvenio = contaConvenio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #getContaCredito()
	 */
	@Override
	public ContaBancaria getContaCredito() {

		return contaCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #setContaCredito(
	 * br.com.ggas.arrecadacao.impl.ContaBancaria)
	 */
	@Override
	public void setContaCredito(ContaBancaria contaCredito) {

		this.contaCredito = contaCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #getArrecadadorContrato()
	 */
	@Override
	public ArrecadadorContrato getArrecadadorContrato() {

		return arrecadadorContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #setArrecadadorContrato
	 * (br.com.ggas.arrecadacao
	 * .ArrecadadorContrato)
	 */
	@Override
	public void setArrecadadorContrato(ArrecadadorContrato arrecadadorContrato) {

		this.arrecadadorContrato = arrecadadorContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #getTipoConvenio()
	 */
	@Override
	public EntidadeConteudo getTipoConvenio() {

		return tipoConvenio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #setTipoConvenio(
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setTipoConvenio(EntidadeConteudo tipoConvenio) {

		this.tipoConvenio = tipoConvenio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #getCodigoConvenio()
	 */
	@Override
	public String getCodigoConvenio() {

		return codigoConvenio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #setCodigoConvenio(java.lang.String)
	 */
	@Override
	public void setCodigoConvenio(String codigoConvenio) {

		this.codigoConvenio = codigoConvenio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #getNumeroDiasFloat()
	 */
	@Override
	public Integer getNumeroDiasFloat() {

		return numeroDiasFloat;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #setNumeroDiasFloat(java.lang.Integer)
	 */
	@Override
	public void setNumeroDiasFloat(Integer numeroDiasFloat) {

		this.numeroDiasFloat = numeroDiasFloat;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio#getNsaRemessa()
	 */
	@Override
	public Long getNsaRemessa() {

		return nsaRemessa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #setNsaRemessa(java.lang.Long)
	 */
	@Override
	public void setNsaRemessa(Long nsaRemessa) {

		this.nsaRemessa = nsaRemessa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio#getNsaRetorno()
	 */
	@Override
	public Long getNsaRetorno() {

		return nsaRetorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #setNsaRetorno(java.lang.Long)
	 */
	@Override
	public void setNsaRetorno(Long nsaRetorno) {

		this.nsaRetorno = nsaRetorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #getSequencialCobrancaInicio()
	 */
	@Override
	public Long getSequencialCobrancaInicio() {

		return sequencialCobrancaInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #setSequencialCobrancaInicio
	 * (java.lang.Long)
	 */
	@Override
	public void setSequencialCobrancaInicio(Long sequencialCobrancaInicio) {

		this.sequencialCobrancaInicio = sequencialCobrancaInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #getSequencialCobrancaFim()
	 */
	@Override
	public Long getSequencialCobrancaFim() {

		return sequencialCobrancaFim;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #setSequencialCobrancaFim(java.lang.Long)
	 */
	@Override
	public void setSequencialCobrancaFim(Long sequencialCobrancaFim) {

		this.sequencialCobrancaFim = sequencialCobrancaFim;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #getTentativasReenvio()
	 */
	@Override
	public Integer getTentativasReenvio() {

		return tentativasReenvio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * ArrecadadorContratoConvenio
	 * #setTentativasReenvio(java.lang.Integer)
	 */
	@Override
	public void setTentativasReenvio(Integer tentativasReenvio) {

		this.tentativasReenvio = tentativasReenvio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio
	 * #getArrecadadorCarteiraCobranca()
	 */
	@Override
	public ArrecadadorCarteiraCobranca getArrecadadorCarteiraCobranca() {

		return arrecadadorCarteiraCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio
	 * #setArrecadadorCarteiraCobranca
	 * (br.com.ggas.
	 * arrecadacao.ArrecadadorCarteiraCobranca)
	 */
	@Override
	public void setArrecadadorCarteiraCobranca(ArrecadadorCarteiraCobranca arrecadadorCarteiraCobranca) {

		this.arrecadadorCarteiraCobranca = arrecadadorCarteiraCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio
	 * #getSequenciaCobrancaGerado()
	 */
	@Override
	public Long getSequenciaCobrancaGerado() {

		return sequenciaCobrancaGerado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio
	 * #setSequenciaCobrancaGerado(java.lang.Long)
	 */
	@Override
	public void setSequenciaCobrancaGerado(Long sequenciaCobrancaGerado) {

		this.sequenciaCobrancaGerado = sequenciaCobrancaGerado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio
	 * #isIndicadorPadrao()
	 */
	@Override
	public Boolean isIndicadorPadrao() {

		return indicadorPadrao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio
	 * #getIndicadorPadraoString()
	 */
	@Override
	public String getIndicadorPadraoString() {

		if (indicadorPadrao) {
			return INDICADOR_PADRAO_STRING_SIM;
		}
		return INDICADOR_PADRAO_STRING_NAO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio
	 * #setIndicadorPadrao(boolean)
	 */
	@Override
	public void setIndicadorPadrao(Boolean indicadorPadrao) {

		this.indicadorPadrao = indicadorPadrao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (StringUtils.isEmpty(codigoConvenio)) {
			stringBuilder.append(CODIGO_CONVENIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (tipoConvenio == null) {
			stringBuilder.append(TIPO_CONVENIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (arrecadadorContrato == null) {
			stringBuilder.append(CONTRATO_ARRECADADOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (leiaute == null) {
			stringBuilder.append(LEIAUTE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
							camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		} else if ((leiaute != null && tipoConvenio != null)
						&& (leiaute.getFormaArrecadacao().getChavePrimaria() != tipoConvenio.getChavePrimaria())) {
			erros.put(Constantes.ERRO_NEGOCIO_ARRECADADOR_CONVENIO_LEIAUTE_TIPO_CONVENIO, "");
		} else if ((sequencialCobrancaInicio != null && sequencialCobrancaFim != null)
						&& (sequencialCobrancaFim <= sequencialCobrancaInicio)) {
			erros.put(Constantes.ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA, "");
		} else if ((sequenciaCobrancaGerado != null && sequencialCobrancaInicio != null && sequencialCobrancaFim != null)
						&& (sequenciaCobrancaGerado < sequencialCobrancaInicio || sequenciaCobrancaGerado > sequencialCobrancaFim)) {
			erros.put(Constantes.ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_GERADO, "");
		} else if (sequencialCobrancaInicio == null && sequencialCobrancaFim != null) {
			erros.put(Constantes.ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_INICIO, "");
		} else if (sequencialCobrancaInicio != null && sequencialCobrancaFim == null) {
			erros.put(Constantes.ERRO_ARRECADADOR_CONVENIO_SEQUENCIA_COBRANCA_FIM, "");
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio#getLeiaute()
	 */
	@Override
	public DocumentoLayout getLeiaute() {

		return leiaute;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio#setLeiaute
	 * (br.com.ggas.arrecadacao.documento.DocumentoLayout)
	 */
	@Override
	public void setLeiaute(DocumentoLayout leiaute) {

		this.leiaute = leiaute;
	}

	@Override
	public void validacaoGeracaoNossoNumero() throws NegocioException {

		if (getSequenciaCobrancaGerado() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_NOSSO_NUMERO, getCodigoConvenio());
		}
		if (getSequencialCobrancaInicio() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_SEQUENCIA_INICIAL, getCodigoConvenio());
		}
		if (getSequencialCobrancaFim() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ARRECADADO_CONVENIO_SEM_SEQUENCIA_FIM, getCodigoConvenio());
		}
	}

	public String getArquivoLayoutFatura() {
		return arquivoLayoutFatura;
	}

	public void setArquivoLayoutFatura(String arquivoLayoutFatura) {
		this.arquivoLayoutFatura = arquivoLayoutFatura;
	}
}
