/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorContrato;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * A Class ArrecadadorContratoImpl.
 */
public class ArrecadadorContratoImpl extends EntidadeNegocioImpl implements ArrecadadorContrato {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5226819115425777950L;

	private Arrecadador arrecadador;

	private String numeroContrato;

	private Date dataInicioContrato;

	private Date dataFimContrato;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ArrecadadorContrato
	 * #getArrecadador()
	 */
	@Override
	public Arrecadador getArrecadador() {

		return arrecadador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ArrecadadorContrato
	 * #setArrecadador(br.com.ggas.arrecadacao.
	 * Arrecadador)
	 */
	@Override
	public void setArrecadador(Arrecadador arrecadador) {

		this.arrecadador = arrecadador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ArrecadadorContrato
	 * #getNumeroContrato()
	 */
	@Override
	public String getNumeroContrato() {

		return numeroContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ArrecadadorContrato
	 * #setNumeroContrato(java.lang.String)
	 */
	@Override
	public void setNumeroContrato(String numeroContrato) {

		this.numeroContrato = numeroContrato;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ArrecadadorContrato
	 * #getDataInicioContrato()
	 */
	@Override
	public Date getDataInicioContrato() {
		Date dataInicio = null;
		if(this.dataInicioContrato != null) {
			dataInicio = (Date) this.dataInicioContrato.clone();
		}
		
		return dataInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ArrecadadorContrato
	 * #setDataInicioContrato(java.util.Date)
	 */
	@Override
	public void setDataInicioContrato(Date dataInicioContrato) {

		if(dataInicioContrato != null) {
			this.dataInicioContrato = (Date) dataInicioContrato.clone();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ArrecadadorContrato
	 * #getDataFimContrato()
	 */
	@Override
	public Date getDataFimContrato() {
		Date dataFim = null; 
		if(this.dataFimContrato != null) {
			dataFim = (Date) this.dataFimContrato.clone();
		}
		return dataFim;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ArrecadadorContrato
	 * #setDataFimContrato(java.util.Date)
	 */
	@Override
	public void setDataFimContrato(Date dataFimContrato) {
		if(dataFimContrato != null) {
			this.dataFimContrato = (Date) dataFimContrato.clone();
		} else {
			this.dataFimContrato = null;
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(arrecadador == null) {
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}
	
}
