/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Collection;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ControladorArrecadador;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 * 
 */
class ControladorArrecadadorImpl extends ControladorNegocioImpl implements ControladorArrecadador {

	private static final String CODIGO_AGENTE = "codigoAgente";
	private static final String BANCO = "banco";
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Arrecadador.BEAN_ID_ARRECADADOR);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Arrecadador.BEAN_ID_ARRECADADOR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadador#consultarArrecadador(java.util.Map)
	 */
	@Override
	public Collection<Arrecadador> consultarArrecadador(Map<String, Object> filtro) {

		Criteria criteria = getCriteria();
		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			String codigoAgente = (String) filtro.get(CODIGO_AGENTE);
			if(codigoAgente != null && !codigoAgente.isEmpty()) {
				criteria.add(Restrictions.like(CODIGO_AGENTE, codigoAgente));
			}
			Long banco = (Long) filtro.get(BANCO);
			if(banco != null && banco > 0) {
				criteria.add(Restrictions.like("banco.chavePrimaria", banco));
			}
			Long cliente = (Long) filtro.get("idCliente");
			if(cliente != null && cliente > 0) {
				criteria.add(Restrictions.like("cliente.chavePrimaria", cliente));
			}
			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}
		}
		criteria.addOrder(Order.asc(CODIGO_AGENTE));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadador#listarArrecadador()
	 */
	@Override
	public Collection<Arrecadador> listarArrecadador() {

		return createCriteria(getClass()).addOrder(Order.asc(CODIGO_AGENTE)).list();

	}

	/**
	 * Inserir arrecadador.
	 * 
	 * @param arrecadador
	 *            the arrecadador
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public Long inserirArrecadador(Arrecadador arrecadador, DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorArrecadador.BEAN_ID_CONTROLADOR_ARRECADADOR);

		return this.inserir(arrecadador);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadador#validarExistente(java.util.Map)
	 */
	@Override
	public void validarExistente(Map<String, Object> filtro) throws NegocioException {

		if(filtro != null) {

			Query query = null;

			StringBuilder hql = new StringBuilder();

			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where 1 = 1 ");

			String codigoAgente = (String) filtro.get(CODIGO_AGENTE);
			if(codigoAgente != null && !codigoAgente.isEmpty()) {
				hql.append(" and codigoAgente = :codigoAgente ");
			} else {
				throw new NegocioException(ControladorArrecadador.ERRO_NEGOCIO_CODIGO_ARRECADADOR_VAZIO, true);
			}
			Long banco = (Long) filtro.get(BANCO);
			if(banco != null && banco > 0) {
				hql.append(" and banco.chavePrimaria = :banco ");
			} else {
				throw new NegocioException(ControladorArrecadador.ERRO_NEGOCIO_BANCO_VAZIO, true);
			}

			if(filtro.containsKey(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA)) {
				hql.append(" and chavePrimaria != :chavePrimaria");
			}

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setString(CODIGO_AGENTE, codigoAgente);
			query.setLong(BANCO, banco);

			if(filtro.containsKey(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA)) {
				query.setLong(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA));
			}

			Collection<Arrecadador> listaArrecadador = query.list();
			if(listaArrecadador != null && !listaArrecadador.isEmpty()) {
				throw new NegocioException(ControladorArrecadador.ERRO_NEGOCIO_CODIGO_ARRECADADOR_EXISTENTE, true);
			}

		}
	}

}

