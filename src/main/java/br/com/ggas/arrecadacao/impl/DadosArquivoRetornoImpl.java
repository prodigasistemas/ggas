/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Date;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorMovimento;

/**
 * DadosArquivoRetornoImpl
 *
 */
class DadosArquivoRetornoImpl implements DadosArquivoRetorno {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -9198917311348232661L;

	private String codigoBanco;

	private String codigoConvenio;

	private Date dataGeracao;

	private String dataMovimento;

	private Arrecadador arrecadador;

	private ArrecadadorMovimento arrecadadorMovimento;

	/**
	 * @return the codigoConvenio
	 */
	@Override
	public String getCodigoConvenio() {

		return codigoConvenio;
	}

	/**
	 * @param codigoConvenio
	 *            the codigoConvenio to set
	 */
	@Override
	public void setCodigoConvenio(String codigoConvenio) {

		this.codigoConvenio = codigoConvenio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * DadosArquivoRetorno#getCodigoBanco()
	 */
	@Override
	public String getCodigoBanco() {

		return codigoBanco;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * DadosArquivoRetorno
	 * #setCodigoBanco(java.lang.String)
	 */
	@Override
	public void setCodigoBanco(String codigoBanco) {

		this.codigoBanco = codigoBanco;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * DadosArquivoRetorno#getDataGeracao()
	 */
	@Override
	public Date getDataGeracao() {

		return dataGeracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * DadosArquivoRetorno
	 * #setDataGeracao(java.lang.String)
	 */
	@Override
	public void setDataGeracao(Date dataGeracao) {

		this.dataGeracao = dataGeracao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * DadosArquivoRetorno#getArrecadador()
	 */
	@Override
	public Arrecadador getArrecadador() {

		return arrecadador;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * DadosArquivoRetorno
	 * #setArrecadador(br.com.ggas
	 * .arrecadacao.Arrecadador)
	 */
	@Override
	public void setArrecadador(Arrecadador arrecadador) {

		this.arrecadador = arrecadador;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * DadosArquivoRetorno
	 * #getArrecadadorMovimento()
	 */
	@Override
	public ArrecadadorMovimento getArrecadadorMovimento() {

		return arrecadadorMovimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * DadosArquivoRetorno
	 * #setArrecadadorMovimento(
	 * br.com.ggas.arrecadacao
	 * .ArrecadadorMovimento)
	 */
	@Override
	public void setArrecadadorMovimento(ArrecadadorMovimento arrecadadorMovimento) {

		this.arrecadadorMovimento = arrecadadorMovimento;
	}

	@Override
	public String getDataMovimento() {

		return dataMovimento;
	}

	@Override
	public void setDataMovimento(String dataMovimento) {

		this.dataMovimento = dataMovimento;
	}

}
