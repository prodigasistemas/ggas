/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.impl;

import java.util.HashMap;
import java.util.Map;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável por representar o arrecadador
 *
 */
public class ArrecadadorImpl extends EntidadeNegocioImpl implements Arrecadador {

	private static final int LIMITE_CAMPO = 2;

	private static final int TAMANHO_INCORRETO = 5;

	private static final int QTD_ZEROS = 3;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2125742237854456299L;

	private Banco banco;

	private Cliente cliente;

	private String codigoAgente;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Arrecadador
	 * #getCodigoAgenteFormatado()
	 */
	@Override
	public String getCodigoAgenteFormatado() {

		return Util.adicionarZerosEsquerdaNumero(codigoAgente.toString(), QTD_ZEROS);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Arrecadador
	 * #getBanco()
	 */
	@Override
	public Banco getBanco() {

		return banco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Arrecadador
	 * #setBanco(br.com.ggas.arrecadacao.Banco)
	 */
	@Override
	public void setBanco(Banco banco) {

		this.banco = banco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Arrecadador
	 * #getCliente()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Arrecadador
	 * #setCliente
	 * (br.com.ggas.cadastro.cliente.Cliente)
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Arrecadador
	 * #getCodigoAgente()
	 */
	@Override
	public String getCodigoAgente() {

		return codigoAgente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Arrecadador
	 * #setCodigoAgente(java.lang.Integer)
	 */
	@Override
	public void setCodigoAgente(String codigoAgente) {

		this.codigoAgente = codigoAgente;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder camposObrigatoriosAcumulados = new StringBuilder();
		StringBuilder tamanhoCamposAcumulados = new StringBuilder();
		String camposObrigatorios = null;
		String camposInvalidos = null;

		if(codigoAgente == null || "".equals(codigoAgente)) {
			camposObrigatoriosAcumulados.append(CODIGO_AGENTE);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if(codigoAgente.toString().length() > TAMANHO_INCORRETO) {
				tamanhoCamposAcumulados.append(CODIGO_AGENTE);
				tamanhoCamposAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}
		if(banco == null) {
			camposObrigatoriosAcumulados.append(BANCO_ARRECADADOR);
			camposObrigatoriosAcumulados.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = camposObrigatoriosAcumulados.toString();
		camposInvalidos = tamanhoCamposAcumulados.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
							camposObrigatorios.substring(0, camposObrigatoriosAcumulados.toString().length() - LIMITE_CAMPO));
		}

		if(camposInvalidos.length() > 0) {
			erros.put(Constantes.ERRO_TAMANHO_LIMITE, camposInvalidos.substring(0, tamanhoCamposAcumulados.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

}
