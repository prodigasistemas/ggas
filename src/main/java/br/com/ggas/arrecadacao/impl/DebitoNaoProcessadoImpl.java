/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import br.com.ggas.arrecadacao.DebitoNaoProcessado;

class DebitoNaoProcessadoImpl implements DebitoNaoProcessado {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1412139999758966245L;

	// FEBRABAN
	// 1,1
	private String identificacaoClienteEmpresa;

	// 2,26

	private String agenciaDebito;

	// 27,30

	private String identificacaoClienteBanco;

	// 31,
	// 44

	private String ocorrencia1;

	// 45,84

	private String ocorrencia2;

	// 85, 124

	private String reservadoFuturo;

	// 125, 149

	private String codigoMovimento;

	// 150,150

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #getIdentificacaoClienteEmpresa()
	 */
	@Override
	public String getIdentificacaoClienteEmpresa() {

		return identificacaoClienteEmpresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #
	 * setIdentificacaoClienteEmpresa(java.lang.String
	 * )
	 */
	@Override
	public void setIdentificacaoClienteEmpresa(String identificacaoClienteEmpresa) {

		this.identificacaoClienteEmpresa = identificacaoClienteEmpresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #getAgenciaDebito()
	 */
	@Override
	public String getAgenciaDebito() {

		return agenciaDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #setAgenciaDebito(java.lang.String)
	 */
	@Override
	public void setAgenciaDebito(String agenciaDebito) {

		this.agenciaDebito = agenciaDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #getIdentificacaoClienteBanco()
	 */
	@Override
	public String getIdentificacaoClienteBanco() {

		return identificacaoClienteBanco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #
	 * setIdentificacaoClienteBanco(java.lang.String
	 * )
	 */
	@Override
	public void setIdentificacaoClienteBanco(String identificacaoClienteBanco) {

		this.identificacaoClienteBanco = identificacaoClienteBanco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #getOcorrencia1()
	 */
	@Override
	public String getOcorrencia1() {

		return ocorrencia1;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #setOcorrencia1(java.lang.String)
	 */
	@Override
	public void setOcorrencia1(String ocorrencia1) {

		this.ocorrencia1 = ocorrencia1;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #getOcorrencia2()
	 */
	@Override
	public String getOcorrencia2() {

		return ocorrencia2;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #setOcorrencia2(java.lang.String)
	 */
	@Override
	public void setOcorrencia2(String ocorrencia2) {

		this.ocorrencia2 = ocorrencia2;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #getReservadoFuturo()
	 */
	@Override
	public String getReservadoFuturo() {

		return reservadoFuturo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #setReservadoFuturo(java.lang.String)
	 */
	@Override
	public void setReservadoFuturo(String reservadoFuturo) {

		this.reservadoFuturo = reservadoFuturo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #getCodigoMovimento()
	 */
	@Override
	public String getCodigoMovimento() {

		return codigoMovimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.DebitoNaoProcessado
	 * #setCodigoMovimento(java.lang.String)
	 */
	@Override
	public void setCodigoMovimento(String codigoMovimento) {

		this.codigoMovimento = codigoMovimento;
	}
}
