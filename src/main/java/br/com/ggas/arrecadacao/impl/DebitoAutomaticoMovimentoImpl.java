/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.arrecadacao.DebitoAutomatico;
import br.com.ggas.arrecadacao.DebitoAutomaticoMovimento;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

class DebitoAutomaticoMovimentoImpl extends EntidadeNegocioImpl implements DebitoAutomaticoMovimento {

	private static final long serialVersionUID = 5392130895058365654L;

	private DebitoAutomatico debitoAutomatico;

	private DocumentoCobranca documentoCobranca;

	private Date dataDebito;

	private BigDecimal valorDebito;

	private String ocorrencia;

	private Date dataProcessamento;

	private Date dataEnvio;

	private Date dataRetorno;

	private Integer codigoMovimento;

	/**
	 * @return
	 */
	@Override
	public Integer getCodigoMovimento() {

		return codigoMovimento;
	}

	/**
	 * @param codigoMovimento
	 */
	@Override
	public void setCodigoMovimento(Integer codigoMovimento) {

		this.codigoMovimento = codigoMovimento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento
	 * #getDebitoAutomatico()
	 */
	@Override
	public DebitoAutomatico getDebitoAutomatico() {

		return debitoAutomatico;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento
	 * #setDebitoAutomatico
	 * (br.com.ggas.cobranca.DebitoAutomatico)
	 */
	@Override
	public void setDebitoAutomatico(DebitoAutomatico debitoAutomatico) {

		this.debitoAutomatico = debitoAutomatico;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento
	 * #getDocumentoCobranca()
	 */
	@Override
	public DocumentoCobranca getDocumentoCobranca() {

		return documentoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento
	 * #setDocumentoCobranca
	 * (br.com.ggas.cobranca.DocumentoCobranca)
	 */
	@Override
	public void setDocumentoCobranca(DocumentoCobranca documentoCobranca) {

		this.documentoCobranca = documentoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento#getDataDebito()
	 */
	@Override
	public Date getDataDebito() {

		return dataDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento
	 * #setDataDebito(java.sql.Date)
	 */
	@Override
	public void setDataDebito(Date dataDebito) {

		this.dataDebito = dataDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento#getValorDebito()
	 */
	@Override
	public BigDecimal getValorDebito() {

		return valorDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento
	 * #setValorDebito(java.math.BigDecimal)
	 */
	@Override
	public void setValorDebito(BigDecimal valorDebito) {

		this.valorDebito = valorDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento#getOcorrencia()
	 */
	@Override
	public String getOcorrencia() {

		return ocorrencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento
	 * #setOcorrencia(String)
	 */
	@Override
	public void setOcorrencia(String ocorrencia) {

		this.ocorrencia = ocorrencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento
	 * #getDataProcessamento()
	 */
	@Override
	public Date getDataProcessamento() {

		return dataProcessamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento
	 * #setDataProcessamento(java.sql.Date)
	 */
	@Override
	public void setDataProcessamento(Date dataProcessamento) {

		this.dataProcessamento = dataProcessamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento#getDataEnvio()
	 */
	@Override
	public Date getDataEnvio() {

		return dataEnvio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento
	 * #setDataEnvio(java.sql.Date)
	 */
	@Override
	public void setDataEnvio(Date dataEnvio) {

		this.dataEnvio = dataEnvio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento#getDataRetorno()
	 */
	@Override
	public Date getDataRetorno() {

		return dataRetorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cobranca.impl.
	 * DebitoAutomaticoMovimento
	 * #setDataRetorno(java.sql.Date)
	 */
	@Override
	public void setDataRetorno(Date dataRetorno) {

		this.dataRetorno = dataRetorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
