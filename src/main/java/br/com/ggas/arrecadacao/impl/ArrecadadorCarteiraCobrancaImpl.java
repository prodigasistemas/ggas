/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorCarteiraCobranca;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável por representar a carteira de cobrança do arrecadador
 */
public class ArrecadadorCarteiraCobrancaImpl extends EntidadeNegocioImpl implements ArrecadadorCarteiraCobranca {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = 4574024096800430634L;

	private Arrecadador arrecadador;

	private EntidadeConteudo codigoCarteira;

	private EntidadeConteudo tipoCarteira;

	private String descricao;

	private Integer numero;

	private boolean indicadorFaixaNossoNumero;

	private boolean indicadorNossoNumeroLivre;

	private boolean indicadorEmissao;

	private boolean indicadorProtesto;

	private boolean indicadorEntrega;

	private String arquivoLayoutFatura;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #getArrecadador()
	 */
	@Override
	public Arrecadador getArrecadador() {

		return arrecadador;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #setArrecadador(br
	 * .com.ggas.arrecadacao.Arrecadador)
	 */
	@Override
	public void setArrecadador(Arrecadador arrecadador) {

		this.arrecadador = arrecadador;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #getCodigoCarteira()
	 */
	@Override
	public EntidadeConteudo getCodigoCarteira() {

		return codigoCarteira;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #setCodigoCarteira
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setCodigoCarteira(EntidadeConteudo codigoCarteira) {

		this.codigoCarteira = codigoCarteira;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #getTipoCarteira()
	 */
	@Override
	public EntidadeConteudo getTipoCarteira() {

		return tipoCarteira;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #setTipoCarteira(
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setTipoCarteira(EntidadeConteudo tipoCarteira) {

		this.tipoCarteira = tipoCarteira;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca#getNumero()
	 */
	@Override
	public Integer getNumero() {

		return numero;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #setNumero(java.lang.Integer)
	 */
	@Override
	public void setNumero(Integer numero) {

		this.numero = numero;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #isIndicadorFaixaNossoNumero()
	 */
	@Override
	public boolean isIndicadorFaixaNossoNumero() {

		return indicadorFaixaNossoNumero;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #setIndicadorFaixaNossoNumero(boolean)
	 */
	@Override
	public void setIndicadorFaixaNossoNumero(boolean indicadorFaixaNossoNumero) {

		this.indicadorFaixaNossoNumero = indicadorFaixaNossoNumero;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #isIndicadorNossoNumeroLivre()
	 */
	@Override
	public boolean isIndicadorNossoNumeroLivre() {

		return indicadorNossoNumeroLivre;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #setIndicadorNossoNumeroLivre(boolean)
	 */
	@Override
	public void setIndicadorNossoNumeroLivre(boolean indicadorNossoNumeroLivre) {

		this.indicadorNossoNumeroLivre = indicadorNossoNumeroLivre;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #isIndicadorEmissao()
	 */
	@Override
	public boolean isIndicadorEmissao() {

		return indicadorEmissao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #setIndicadorEmissao(boolean)
	 */
	@Override
	public void setIndicadorEmissao(boolean indicadorEmissao) {

		this.indicadorEmissao = indicadorEmissao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #isIndicadorProtesto()
	 */
	@Override
	public boolean isIndicadorProtesto() {

		return indicadorProtesto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #setIndicadorProtesto(boolean)
	 */
	@Override
	public void setIndicadorProtesto(boolean indicadorProtesto) {

		this.indicadorProtesto = indicadorProtesto;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #isIndicadorEntrega()
	 */
	@Override
	public boolean isIndicadorEntrega() {

		return indicadorEntrega;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * ArrecadadorCarteiraCobranca
	 * #setIndicadorEntrega(boolean)
	 */
	@Override
	public void setIndicadorEntrega(boolean indicadorEntrega) {

		this.indicadorEntrega = indicadorEntrega;
	}

	@Override
	public String getArquivoLayoutFatura() {
		return arquivoLayoutFatura;
	}

	@Override
	public void setArquivoLayoutFatura(String arquivoLayoutFatura) {
		this.arquivoLayoutFatura = arquivoLayoutFatura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao) || descricao.trim().length() == 0) {
			stringBuilder.append(ArrecadadorCarteiraCobranca.DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(numero == null) {
			stringBuilder.append(ArrecadadorCarteiraCobranca.NUMERO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(arrecadador == null) {
			stringBuilder.append(ArrecadadorCarteiraCobranca.ARRECADADOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(codigoCarteira == null) {
			stringBuilder.append(ArrecadadorCarteiraCobranca.CODIGO_CARTEIRA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(tipoCarteira == null) {
			stringBuilder.append(ArrecadadorCarteiraCobranca.TIPO_CARTEIRA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

}
