/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.math.BigDecimal;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.CreditoOrigem;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.rubrica.Rubrica;
/**
 * Classe responsável pela representação de um objeto de valor dos dados de debito e credito
 */
public class DadosEstruturaCreditoDebitoVO {

	private PontoConsumo pontoConsumo;

	private Cliente cliente;

	private Rubrica rubrica;

	private BigDecimal valor;

	private BigDecimal quantidade;

	private DadosAuditoria dadosAuditoria;

	private DocumentoCobrancaItem documentoCobrancaItem;

	private FaturaGeral faturaGeral;

	private Integer anoMesFaturamento;

	private Integer numeroCiclo;

	private CreditoOrigem creditoOrigem;
	
	/**
	 * Retorna ponto de consumo
	 * 
	 * @return ponto de consumo
	 */
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}
	
	/**
	 * Atribui ponto de consumo
	 * 
	 * @param pontoConsumo
	 */
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}
	
	/**
	 * Retorna cliente
	 * 
	 * @return cliente
	 */
	public Cliente getCliente() {

		return cliente;
	}
	
	/**
	 * Atribui cliente
	 * 
	 * @param cliente
	 */
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}
	
	/**
	 * Retorna rubrica
	 * 
	 * @return rubrica
	 */
	public Rubrica getRubrica() {

		return rubrica;
	}
	
	/**
	 * Atribui rubrica
	 * 
	 * @param rubrica
	 */
	public void setRubrica(Rubrica rubrica) {

		this.rubrica = rubrica;
	}
	
	/**
	 * Retorna valor
	 * 
	 * @return valor
	 */
	public BigDecimal getValor() {

		return valor;
	}
	
	/**
	 * Atribui valor
	 * 
	 * @param valor
	 */
	public void setValor(BigDecimal valor) {

		this.valor = valor;
	}
	
	/**
	 * Retorna dados de auditoria
	 * 
	 * @return dados de auditoria
	 */
	public DadosAuditoria getDadosAuditoria() {

		return dadosAuditoria;
	}
	
	/**
	 * Atribui dadosAuditoria
	 * 
	 * @param dadosAuditoria
	 */
	public void setDadosAuditoria(DadosAuditoria dadosAuditoria) {

		this.dadosAuditoria = dadosAuditoria;
	}
	
	/**
	 * Retorna documento de cobranca
	 * 
	 * @return item do documento de cobranca
	 */
	public DocumentoCobrancaItem getDocumentoCobrancaItem() {

		return documentoCobrancaItem;
	}
	
	/**
	 * Atribui documentoCobranca
	 * 
	 * @param documentoCobrancaItem
	 */
	public void setDocumentoCobrancaItem(DocumentoCobrancaItem documentoCobrancaItem) {

		this.documentoCobrancaItem = documentoCobrancaItem;
	}
	
	/**
	 * Retorna fatura geral
	 * 
	 * @return Fatura geral
	 */
	public FaturaGeral getFaturaGeral() {

		return faturaGeral;
	}
	
	/**
	 * Atribui fatura geral
	 * 
	 * @param faturaGeral
	 */
	public void setFaturaGeral(FaturaGeral faturaGeral) {

		this.faturaGeral = faturaGeral;
	}
	
	/**
	 * Atribui quantidade
	 * 
	 * @param quantidade
	 */
	public void setQuantidade(BigDecimal quantidade) {

		this.quantidade = quantidade;
	}
	
	/**
	 * Retorna quantidade
	 * 
	 * @return quantidade
	 */
	public BigDecimal getQuantidade() {

		return quantidade;
	}
	
	/**
	 * Atribui anoMesFaturamento
	 * 
	 * @param anoMesFaturamento
	 */
	public void setAnoMesFaturamento(Integer anoMesFaturamento) {

		this.anoMesFaturamento = anoMesFaturamento;
	}
	
	/**
	 * Retorna ano e mes de faturamento
	 * 
	 * @return ano e mês de faturamento
	 */
	public Integer getAnoMesFaturamento() {

		return anoMesFaturamento;
	}
	
	/**
	 * Atribui numeroCiclo
	 * 
	 * @param numeroCiclo
	 */
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;
	}
	
	/**
	 * Retorna numero do ciclo
	 * 
	 * @return numero do ciclo
	 */
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}
	
	/**
	 * Retorna origem de creditos
	 * 
	 * @return origem de créditos
	 */
	public CreditoOrigem getCreditoOrigem() {

		return creditoOrigem;
	}
	
	/**
	 * Atribui creditoOrigem
	 * 
	 * @param creditoOrigem
	 */
	public void setCreditoOrigem(CreditoOrigem creditoOrigem) {

		this.creditoOrigem = creditoOrigem;
	}
}
