/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe <nome> representa uma <nome> no sistema.
 *
 * @since data atual
 * 
 */

package br.com.ggas.arrecadacao.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.arrecadacao.IndiceFinanceiroValorNominal;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;

/**
 * Classe responsável pela representação do valor nominal do índice financeiro
 *
 */
public class IndiceFinanceiroValorNominalImpl extends EntidadeNegocioImpl implements IndiceFinanceiroValorNominal {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3627775653694492838L;

	private IndiceFinanceiro indiceFinanceiro;

	private Date dataReferencia;

	private BigDecimal valorNominal;

	private boolean indicadorUtilizado;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * IndiceFinanceiroValorNominal
	 * #getIndiceFinanceiro()
	 */
	@Override
	public IndiceFinanceiro getIndiceFinanceiro() {

		return indiceFinanceiro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * IndiceFinanceiroValorNominal
	 * #setIndiceFinanceiro
	 * (br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void setIndiceFinanceiro(IndiceFinanceiro indiceFinanceiro) {

		this.indiceFinanceiro = indiceFinanceiro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * IndiceFinanceiroValorNominal
	 * #getDataReferencia()
	 */
	@Override
	public Date getDataReferencia() {
		Date dataReferenciaTmp = null;
		if(dataReferencia != null){
			dataReferenciaTmp = (Date) dataReferencia.clone();
		}
		return dataReferenciaTmp;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * IndiceFinanceiroValorNominal
	 * #setDataReferencia(java.util.Date)
	 */
	@Override
	public void setDataReferencia(Date dataReferencia) {
		if(dataReferencia != null){
			this.dataReferencia = (Date) dataReferencia.clone();
		} else {
			this.dataReferencia = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * IndiceFinanceiroValorNominal
	 * #getValorNominal()
	 */
	@Override
	public BigDecimal getValorNominal() {

		return valorNominal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * IndiceFinanceiroValorNominal
	 * #setValorNominal(java.math.BigDecimal)
	 */
	@Override
	public void setValorNominal(BigDecimal valorNominal) {

		this.valorNominal = valorNominal;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * IndiceFinanceiroValorNominal
	 * #isIndicadorUtilizado()
	 */
	@Override
	public boolean isIndicadorUtilizado() {

		return indicadorUtilizado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.
	 * IndiceFinanceiroValorNominal
	 * #setIndicadorUtilizado(boolean)
	 */
	@Override
	public void setIndicadorUtilizado(boolean indicadorUtilizado) {

		this.indicadorUtilizado = indicadorUtilizado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(dataReferencia == null) {
			stringBuilder.append(DATA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(valorNominal == null) {
			stringBuilder.append(VALOR_NOMINAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(indiceFinanceiro == null) {
			stringBuilder.append(UNIDADE_MONETARIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	@Override
	public String hashString() {
		return DataUtil.converterDataParaString(
						this.getDataReferencia());
	}

}
