/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável por representar uma agência bancária
 */
public class AgenciaImpl extends EntidadeNegocioImpl implements Agencia {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = -2459187058254865196L;

	private Banco banco;

	private String codigo;

	private String nome;

	private String telefone;

	private String ramal;

	private String fax;

	private String email;

	private Cep cep;

	private Collection<ContaBancaria> listaContaBancaria = new HashSet<>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#getBanco
	 * ()
	 */
	@Override
	public Banco getBanco() {

		return banco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#setBanco
	 * (br.com.ggas.arrecadacao.Banco)
	 */
	@Override
	public void setBanco(Banco banco) {

		this.banco = banco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#getCodigo
	 * ()
	 */
	@Override
	public String getCodigo() {

		return codigo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#setCodigo
	 * (java.lang.String)
	 */
	@Override
	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#getNome
	 * ()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#setNome
	 * (java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.Agencia#
	 * getTelefone()
	 */
	@Override
	public String getTelefone() {

		return telefone;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.Agencia#
	 * setTelefone(java.lang.String)
	 */
	@Override
	public void setTelefone(String telefone) {

		this.telefone = telefone;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#getRamal
	 * ()
	 */
	@Override
	public String getRamal() {

		return ramal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#setRamal
	 * (java.lang.String)
	 */
	@Override
	public void setRamal(String ramal) {

		this.ramal = ramal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#getFax
	 * ()
	 */
	@Override
	public String getFax() {

		return fax;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#setFax
	 * (java.lang.String)
	 */
	@Override
	public void setFax(String fax) {

		this.fax = fax;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#getEmail
	 * ()
	 */
	@Override
	public String getEmail() {

		return email;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#setEmail
	 * (java.lang.String)
	 */
	@Override
	public void setEmail(String email) {

		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#getCep
	 * ()
	 */
	@Override
	public Cep getCep() {

		return cep;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.impl.Agencia#setCep
	 * (br.com.ggas.cadastro.endereco.Cep)
	 */
	@Override
	public void setCep(Cep cep) {

		this.cep = cep;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(banco == null) {
			stringBuilder.append(BANCO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(nome) || nome.trim().length() == 0) {
			stringBuilder.append(NOME);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(codigo) || codigo.trim().length() == 0) {
			stringBuilder.append(CODIGO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	@Override
	public Collection<ContaBancaria> getListaContaBancaria() {

		return listaContaBancaria;
	}

	@Override
	public void setListaContaBancaria(Collection<ContaBancaria> listaContaBancaria) {

		this.listaContaBancaria = listaContaBancaria;
	}

}
