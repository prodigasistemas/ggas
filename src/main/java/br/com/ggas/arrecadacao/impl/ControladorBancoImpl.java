/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Collection;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.arrecadacao.ControladorBanco;
import br.com.ggas.cobranca.boletobancario.codigobarras.FabricaCodigoBarras;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 * 
 */
class ControladorBancoImpl extends ControladorNegocioImpl implements ControladorBanco {

	private static final String CODIGO_BANCO = "codigoBanco";
	private static final String HABILITADO = "habilitado";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Banco.BEAN_ID_BANCO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Banco.BEAN_ID_BANCO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorBanco#consultarBanco(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Banco> consultarBanco(Map<String, Object> filtro) {

		Criteria criteria = getCriteria();
		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			String codigoBanco = (String) filtro.get(CODIGO_BANCO);
			if (codigoBanco != null && !codigoBanco.isEmpty()) {
				criteria.add(Restrictions.like(CODIGO_BANCO, "%" + codigoBanco + "%"));
			}
			String nome = (String) filtro.get("nome");
			if (nome != null && !nome.isEmpty()) {
				criteria.add(Restrictions.like("nome", "%" + nome + "%"));
			}
			String nomeAbreviado = (String) filtro.get("nomeAbreviado");
			if (nomeAbreviado != null && !nomeAbreviado.isEmpty()) {
				criteria.add(Restrictions.like("nomeAbreviado", "%" + nomeAbreviado + "%"));
			}
			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}
		}
		criteria.addOrder(Order.asc("nome"));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorBanco#listarBanco()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Banco> listarBanco() {

		Criteria criteria = getCriteria();
		criteria.addOrder(Order.asc("nome"));
		criteria.add(Restrictions.eq(HABILITADO, true));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorBanco#validarExistente(java.util.Map)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void validarExistente(Map<String, Object> filtro) throws NegocioException {

		if (filtro != null) {

			Query query = null;

			StringBuilder hql = new StringBuilder();

			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where 1 = 1 ");

			Long chavaPrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			String codigoBanco = (String) filtro.get(CODIGO_BANCO);
			if (codigoBanco != null && !codigoBanco.isEmpty() && habilitado != null && habilitado) {
				String codigoBancosSuportados = FabricaCodigoBarras.obterCodigoBancosSuportados();
				if (!codigoBancosSuportados.contains(codigoBanco)) {
					Object[] param = {
										codigoBanco, codigoBancosSuportados};
					throw new NegocioException(ControladorBanco.ERRO_NEGOCIO_BANCO_NAO_SUPORTADO, param);
				}
				hql.append(" and codigoBanco = :codigoBanco ");
			} else {
				throw new NegocioException(ControladorBanco.ERRO_NEGOCIO_CODIGO_BANCO_VAZIO, true);
			}
			String nome = (String) filtro.get("nome");
			if (nome != null && !nome.isEmpty()) {
				hql.append(" or nome = :nome ");
			} else {
				throw new NegocioException(ControladorBanco.ERRO_NEGOCIO_NOME_BANCO_VAZIO, true);
			}
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setString(CODIGO_BANCO, codigoBanco);
			query.setString("nome", nome);

			Collection<Banco> listaBanco = query.list();

			for (Banco banco : listaBanco) {
				verificaNomeChavePrimariaBanco(chavaPrimaria, nome, banco);
				verificaCodigoChavePrimariaBanco(chavaPrimaria, codigoBanco, banco);
			}
		}
	}

	private void verificaCodigoChavePrimariaBanco(Long chavaPrimaria, String codigoBanco, Banco banco)
			throws NegocioException {
		if ((banco.getCodigoBanco().equalsIgnoreCase(codigoBanco))
				&& ((chavaPrimaria != null && banco.getChavePrimaria() != chavaPrimaria) || (chavaPrimaria == null))) {
			throw new NegocioException(ControladorBanco.ERRO_NEGOCIO_CODIGO_BANCO_EXISTENTE, true);
		}
	}

	private void verificaNomeChavePrimariaBanco(Long chavaPrimaria, String nome, Banco banco) throws NegocioException {
		if ((banco.getNome().equalsIgnoreCase(nome))
				&& ((chavaPrimaria != null && banco.getChavePrimaria() != chavaPrimaria) || (chavaPrimaria == null))) {
			throw new NegocioException(ControladorBanco.ERRO_NEGOCIO_NOME_BANCO_EXISTENTE, true);
		}
	}

}
