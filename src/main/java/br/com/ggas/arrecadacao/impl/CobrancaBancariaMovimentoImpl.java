/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutEnvioOcorrencia;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRetornoOcorrencia;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

class CobrancaBancariaMovimentoImpl extends EntidadeNegocioImpl implements CobrancaBancariaMovimento {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 495770823002307394L;

	private DocumentoCobranca documentoCobranca;

	private DocumentoLayoutRetornoOcorrencia documentoLayoutRetornoOcorrencia;
	
	private DocumentoLayoutEnvioOcorrencia documentoLayoutEnvioOcorrencia;

	private ArrecadadorContratoConvenio arrecadadorContratoConvenio;

	private Date dataEnvio;

	private Date dataRetorno;

	private Date dataProcessamento;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #getDocumentoCobranca()
	 */
	@Override
	public DocumentoCobranca getDocumentoCobranca() {

		return documentoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #setDocumentoCobranca
	 * (br.com.ggas.faturamento.DocumentoCobranca)
	 */
	@Override
	public void setDocumentoCobranca(DocumentoCobranca documentoCobranca) {

		this.documentoCobranca = documentoCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #getDocumentoLayoutRetornoOcorrencia()
	 */
	@Override
	public DocumentoLayoutRetornoOcorrencia getDocumentoLayoutRetornoOcorrencia() {

		return documentoLayoutRetornoOcorrencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #setDocumentoLayoutRetornoOcorrencia
	 * (br.com.ggas.arrecadacao.documento.
	 * DocumentoLayoutRetornoOcorrencia)
	 */
	@Override
	public void setDocumentoLayoutRetornoOcorrencia(DocumentoLayoutRetornoOcorrencia documentoLayoutRetornoOcorrencia) {

		this.documentoLayoutRetornoOcorrencia = documentoLayoutRetornoOcorrencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #getArrecadadorContratoConvenio()
	 */
	@Override
	public ArrecadadorContratoConvenio getArrecadadorContratoConvenio() {

		return arrecadadorContratoConvenio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #setArrecadadorContratoConvenio
	 * (br.com.ggas.
	 * arrecadacao.ArrecadadorContratoConvenio)
	 */
	@Override
	public void setArrecadadorContratoConvenio(ArrecadadorContratoConvenio arrecadadorContratoConvenio) {

		this.arrecadadorContratoConvenio = arrecadadorContratoConvenio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento#getDataEnvio()
	 */
	@Override
	public Date getDataEnvio() {

		return dataEnvio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #setDataEnvio(java.util.Date)
	 */
	@Override
	public void setDataEnvio(Date dataEnvio) {

		this.dataEnvio = dataEnvio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento#getDataRetorno()
	 */
	@Override
	public Date getDataRetorno() {

		return dataRetorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #setDataRetorno(java.util.Date)
	 */
	@Override
	public void setDataRetorno(Date dataRetorno) {

		this.dataRetorno = dataRetorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #getDataProcessamento()
	 */
	@Override
	public Date getDataProcessamento() {

		return dataProcessamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #setDataProcessamento(java.util.Date)
	 */
	@Override
	public void setDataProcessamento(Date dataProcessamento) {

		this.dataProcessamento = dataProcessamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #getDocumentoLayoutEnvioOcorrencia()
	 */
	@Override
	public DocumentoLayoutEnvioOcorrencia getDocumentoLayoutEnvioOcorrencia() {
		return documentoLayoutEnvioOcorrencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.impl.
	 * CobrancaBancariaMovimento
	 * #setDocumentoLayoutEnvioOcorrencia
	 * (br.com.ggas.arrecadacao.documento.
	 * DocumentoLayoutEnvioOcorrencia)
	 */
	@Override
	public void setDocumentoLayoutEnvioOcorrencia(DocumentoLayoutEnvioOcorrencia documentoLayoutEnvioOcorrencia) {
		this.documentoLayoutEnvioOcorrencia = documentoLayoutEnvioOcorrencia;
		
	}

}
