/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.arrecadacao.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.Banco;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável representar um Banco
 *
 */
public class BancoImpl extends EntidadeNegocioImpl implements Banco {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7295301942995957492L;

	private String nome;

	private String nomeAbreviado;

	private String codigoBanco;

	private byte[] logoBanco;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.Banco#getCodigoBanco
	 * ()
	 */
	@Override
	public String getCodigoBanco() {

		return codigoBanco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.Banco#setCodigoBanco
	 * (java.lang.String)
	 */
	@Override
	public void setCodigoBanco(String codigoBanco) {

		this.codigoBanco = codigoBanco;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.Banco#getNome()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.Banco#setNome(java
	 * .lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.Banco#getNomeAbreviado
	 * ()
	 */
	@Override
	public String getNomeAbreviado() {

		return nomeAbreviado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.Banco#setNomeAbreviado
	 * (java.lang.String)
	 */
	@Override
	public void setNomeAbreviado(String nomeAbreviado) {

		this.nomeAbreviado = nomeAbreviado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.Banco#getLogoBanco
	 * ()
	 */
	@Override
	public byte[] getLogoBanco() {
		byte[] logoBancoTmp = null;
		if(logoBanco != null){
			logoBancoTmp = logoBanco.clone();
		}
		return logoBancoTmp;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.Banco#setLogoBanco
	 * (byte[])
	 */
	@Override
	public void setLogoBanco(byte[] logoBanco) {
		if(logoBanco != null){
			this.logoBanco = logoBanco.clone();
		} else {
			this.logoBanco = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(nome)) {
			stringBuilder.append(NOME_DO_BANCO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(StringUtils.isEmpty(codigoBanco)){
			stringBuilder.append(CODIGO_DO_BANCO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	@Override
	public String[] getExtensoesArquivoLogoEmpresa() {
		return EXTENSOES_ARQUIVO_LOGO_EMPRESA;
	}

}
