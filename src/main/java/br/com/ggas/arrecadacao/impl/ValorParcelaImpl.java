/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.arrecadacao.ValorParcela;
/**
 * Classe responsável pela representação do valor da parcela
 */
class ValorParcelaImpl implements ValorParcela {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5182312789141421015L;

	private int periodo;
	private BigDecimal saldo;
	private BigDecimal juros;
	private BigDecimal amortizacao;
	private BigDecimal valorPagar;
	private Date dataVencimento;

	/**
	 * Retorna dataVencimento
	 * 
	 * @return the dataVencimento
	 */
	@Override
	public Date getDataVencimento() {

		return dataVencimento;
	}

	/**
	 * @param dataVencimento
	 *            the dataVencimento to set
	 */
	@Override
	public void setDataVencimento(Date dataVencimento) {

		this.dataVencimento = dataVencimento;
	}

	/**
	 * Retorna Periodo
	 * 
	 * @return the periodo
	 */
	@Override
	public int getPeriodo() {

		return periodo;
	}

	/**
	 * @param periodo
	 *            the periodo to set
	 */
	@Override
	public void setPeriodo(int periodo) {

		this.periodo = periodo;
	}

	/**
	 * Retorna saldo
	 * 
	 * @return the saldo
	 */
	@Override
	public BigDecimal getSaldo() {

		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	@Override
	public void setSaldo(BigDecimal saldo) {

		this.saldo = saldo;
	}

	/**
	 * Retorna juros
	 * 
	 * @return the juros
	 */
	@Override
	public BigDecimal getJuros() {

		return juros;
	}

	/**
	 * @param juros
	 *            the juros to set
	 */
	@Override
	public void setJuros(BigDecimal juros) {

		this.juros = juros;
	}

	/**
	 * Retorna amortizacao
	 * 
	 * @return the amortizacao
	 */
	@Override
	public BigDecimal getAmortizacao() {

		return amortizacao;
	}

	/**
	 * @param amortizacao
	 *            the amortizacao to set
	 */
	@Override
	public void setAmortizacao(BigDecimal amortizacao) {

		this.amortizacao = amortizacao;
	}

	/**
	 * Retorna valorpagar
	 * 
	 * @return the valorPagar
	 */
	@Override
	public BigDecimal getValorPagar() {

		return valorPagar;
	}

	/**
	 * @param valorPagar
	 *            the valorPagar to set
	 */
	@Override
	public void setValorPagar(BigDecimal valorPagar) {

		this.valorPagar = valorPagar;
	}
}
