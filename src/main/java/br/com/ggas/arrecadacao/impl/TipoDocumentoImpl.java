/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.documento.DocumentoModelo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável pela representação do tipo de documento.
 */
public class TipoDocumentoImpl extends EntidadeNegocioImpl implements TipoDocumento {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 2456087373866176801L;

	private String descricao;

	private String descricaoAbreviada;

	private String indicadorPagavel;

	private String indicadorCodigoBarras;

	private Collection<DocumentoModelo> modelos = new HashSet<>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.TipoDocumento
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.TipoDocumento
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.TipoDocumento
	 * #getDescricaoAbreviada()
	 */
	@Override
	public String getDescricaoAbreviada() {

		return descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.TipoDocumento
	 * #setDescricaoAbreviada(java.lang.String)
	 */
	@Override
	public void setDescricaoAbreviada(String descricaoAbreviada) {

		this.descricaoAbreviada = descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.TipoDocumento
	 * #getIndicadorPagavel()
	 */
	@Override
	public String getIndicadorPagavel() {

		return indicadorPagavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.TipoDocumento
	 * #setIndicadorPagavel(java.lang.String)
	 */
	@Override
	public void setIndicadorPagavel(String indicadorPagavel) {

		this.indicadorPagavel = indicadorPagavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.TipoDocumento
	 * #getIndicadorCodigoBarras()
	 */
	@Override
	public String getIndicadorCodigoBarras() {

		return indicadorCodigoBarras;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cobranca.impl.TipoDocumento
	 * #setIndicadorCodigoBarras(java.lang.String)
	 */
	@Override
	public void setIndicadorCodigoBarras(String indicadorCodigoBarras) {

		this.indicadorCodigoBarras = indicadorCodigoBarras;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao) || "".equals(StringUtils.trim(descricao))) {
			stringBuilder.append("Descrição");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(descricaoAbreviada) || "".equals(StringUtils.trim(descricaoAbreviada))) {
			stringBuilder.append("Descrição Abreviada");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;

	}

	@Override
	public Collection<DocumentoModelo> getModelos() {

		return modelos;
	}

	@Override
	public void setModelos(Collection<DocumentoModelo> modelos) {

		this.modelos = modelos;
	}

	@Override
	public String getIndicadorPagavelFormatado() {

		if("1".equalsIgnoreCase(getIndicadorPagavel()) || "true".equalsIgnoreCase(getIndicadorPagavel())) {
			return "Sim";
		}
		return "Não";
	}

	@Override
	public String getIndicadorCodigoBarrasFormatado() {

		if("1".equalsIgnoreCase(getIndicadorCodigoBarras()) || "true".equalsIgnoreCase(getIndicadorCodigoBarras())) {
			return "Sim";
		}
		return "Não";
	}

}
