/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorArrecadacaoImpl representa uma ControladorArrecadacaoImpl no sistema.
 *
 * @since 12/02/2010
 *
 */

package br.com.ggas.arrecadacao.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.io.output.FileWriterWithEncoding;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorCarteiraCobranca;
import br.com.ggas.arrecadacao.ArrecadadorContrato;
import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.ArrecadadorMovimento;
import br.com.ggas.arrecadacao.ArrecadadorMovimentoItem;
import br.com.ggas.arrecadacao.ClienteDebitoAutomatico;
import br.com.ggas.arrecadacao.CobrancaBancariaMovimento;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.ControladorArrecadadorConvenio;
import br.com.ggas.arrecadacao.DebitoAutomatico;
import br.com.ggas.arrecadacao.DebitoAutomaticoMovimento;
import br.com.ggas.arrecadacao.DebitoNaoProcessado;
import br.com.ggas.arrecadacao.DebitosACobrar;
import br.com.ggas.arrecadacao.IndiceFinanceiroValorNominal;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.avisobancario.AvisoBancario;
import br.com.ggas.arrecadacao.documento.ControladorDocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutEnvioOcorrencia;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.parcelamento.Parcelamento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.impl.OperacaoContabil;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.EncargoTributario;
import br.com.ggas.faturamento.ControladorDocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoDetalhamento;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.impl.FaturaGeralImpl;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.BigDecimalUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela implementação dos métodos relacionados as regras de negócio do GGAS.
 *
 */
public class ControladorArrecadacaoImpl extends ControladorNegocioImpl implements ControladorArrecadacao {

	private static final String COB_BANCARIA_MOVIMENTO = " cobBancariaMovimento ";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CLIENTE_DEBITO_AUTOMATICO = " clienteDebitoAutomatico ";

	private static final String ARRECADADOR_CONTRATO_CONVENIO = " arrecadadorContratoConvenio ";

	private static final String SEPARATOR = File.separator;

	private static final String ENCODING_ISO_8859_1 = "ISO-8859-1";

	private static final String FILTRO_POR_CLIENTE = "cliente";

	private static final String FILTRO_POR_IMOVEL = "imovel";

	private static final String FILTRO_POR_CONTRATO = "contrato";

	private static final int PERCENTUAL = 100;

	private static final int DIVISOR = 6;

	private static final int DUAS_CASAS_DECIMAIS = 2;

	private static final int DIVISOR_DIAS = 6;

	private static final Logger LOG = Logger.getLogger(ControladorArrecadacaoImpl.class);

	private static final String PREFIXO_ARQUIVO_REMESSA_DEBITO_AUTOMATICO = "DA";

	private static final String PREFIXO_ARQUIVO_DEBITO_AUTOMATICO_NAO_PROCESSADO = "NP";

	private static final String DIRETORIO_ARQUIVOS_REMESSA = "DIRETORIO_ARQUIVOS_REMESSA";

	private static final String SUFIXO_ARQUIVO_REMESSA = "txt";

	private static final String ACCONVENIO = " acConvenio ";

	private static final String CHAVE_ARRECADADOR = "CHAVE_ARRECADADOR";

	private static final String DEBITO_AUTOMATICO = " debitoAutomatico ";

	private static final String DEBITO_AUTOMATICO_MOVIMENTO = " debitoAutomaticoMovimento ";

	private static final String CHAVE_ARRECADADOR_UM = "chaveArrecadador";

	private static final String CODIGO_BANCO_DAYCOVAL = "707";

	private static final String SIGLA_DAYCOVAL = "WMJ";

	public static final String FROM = " from ";

	public static final String WHERE = " where ";
	
	private static final String ERRO_NEGOCIO_INSERIR_MOVIMENTO_OCORRENCIA_ENVIO = "ERRO_NEGOCIO_INSERIR_MOVIMENTO_OCORRENCIA_ENVIO";

	private Map<Long, TipoDocumento> tipoDocumentoCache = new HashMap<>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	/**
	 * Método responsável por obter uma Entidade
	 * Arrecadacao Movimento.
	 *
	 * @return the entidade negocio
	 */
	@Override
	public EntidadeNegocio criarArrecadadorMovimento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ArrecadadorMovimento.BEAN_ID_ARRECADADOR_MOVIMENTO);
	}

	/**
	 * Método responsável por obter uma Entidade
	 * Arrecadacao Movimento Item.
	 *
	 * @return the entidade negocio
	 */
	@Override
	public EntidadeNegocio criarArrecadadorMovimentoItem() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ArrecadadorMovimentoItem.BEAN_ID_ARRECADADOR_MOVIMENTO_ITEM);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#criarCreditoDebitoARealizar()
	 */
	@Override
	public EntidadeNegocio criarCreditoDebitoARealizar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(CreditoDebitoARealizar.BEAN_ID_CREDITO_DEBITO_A_REALIZAR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#criarDebitoAutomaticoMovimento()
	 */
	@Override
	public EntidadeNegocio criarDebitoAutomaticoMovimento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(DebitoAutomaticoMovimento.BEAN_ID_DEBITO_AUTOMATICO_MOVIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#criarCobrancaBancariaMovimento()
	 */
	@Override
	public EntidadeNegocio criarCobrancaBancariaMovimento() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(CobrancaBancariaMovimento.BEAN_ID_COBRANCA_BANCARIA_MOVIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#criarClienteDebitoAutomatico()
	 */
	@Override
	public EntidadeNegocio criarClienteDebitoAutomatico() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ClienteDebitoAutomatico.BEAN_ID_CLIENTE_DEBITO_AUTOMATICO);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#
	 * getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	public Class<?> getClasseEntidadeIndiceFinaceiroValorNominal() {

		return ServiceLocator.getInstancia().getClassPorID(IndiceFinanceiroValorNominal.BEAN_ID_INDICE_FINANCEIRO_VALOR_NOMINAL);
	}

	@Override
	public Class<?> getClasseEntidadeEncargoTributario() {

		return ServiceLocator.getInstancia().getClassPorID(EncargoTributario.BEAN_ID_ENCARGO_TRIBUTARIO);
	}

	public Class<?> getClasseEntidadeArrecadador() {

		return ServiceLocator.getInstancia().getClassPorID(Arrecadador.BEAN_ID_ARRECADADOR);
	}

	public Class<?> getClasseEntidadeDebitoAutomaticoMovimento() {

		return ServiceLocator.getInstancia().getClassPorID(DebitoAutomaticoMovimento.BEAN_ID_DEBITO_AUTOMATICO_MOVIMENTO);
	}

	public Class<?> getClasseEntidadeDebitoAutomatico() {

		return ServiceLocator.getInstancia().getClassPorID(DebitoAutomatico.BEAN_ID_DEBITO_AUTOMATICO);
	}

	public Class<?> getClasseEntidadeArrecadadorContratoConvenio() {

		return ServiceLocator.getInstancia().getClassPorID(ArrecadadorContratoConvenio.BEAN_ID_ARRECADADOR_CONTRATO_CONVENIO);
	}

	public Class<?> getClasseEntidadeTipoDocumento() {

		return ServiceLocator.getInstancia().getClassPorID(TipoDocumento.BEAN_ID_TIPO_DOCUMENTO);
	}

	public Class<?> getClasseEntidadeAvisoBancario() {

		return ServiceLocator.getInstancia().getClassPorID(AvisoBancario.BEAN_ID_AVISO_BANCARIO);
	}

	public Class<?> getClasseEntidadeCobrancaBancariaMovimento() {

		return ServiceLocator.getInstancia().getClassPorID(CobrancaBancariaMovimento.BEAN_ID_COBRANCA_BANCARIA_MOVIMENTO);
	}

	public Class<?> getClasseEntidadeFaturaGeral() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaGeral.BEAN_ID_FATURA_GERAL);
	}

	public Class<?> getClasseEntidadeDocumentoCobrancaItem() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoCobrancaItem.BEAN_ID_DOCUMENTO_COBRANCA_ITEM);
	}

	public Class<?> getClasseEntidadeContaBancaria() {

		return ServiceLocator.getInstancia().getClassPorID(ContaBancaria.BEAN_ID_CONTA_BANCARIA);
	}

	public Class<?> getClasseEntidadeAgencia() {

		return ServiceLocator.getInstancia().getClassPorID(Agencia.BEAN_ID_AGENCIA);
	}

	public Class<?> getClasseEntidadeClienteDebitoAutomatico() {

		return ServiceLocator.getInstancia().getClassPorID(ClienteDebitoAutomatico.BEAN_ID_CLIENTE_DEBITO_AUTOMATICO);
	}
	
	public Class<?> getClasseEntidadeDocumentoLayoutEnvioOcorrencia() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoLayoutEnvioOcorrencia.BEAN_ID_DOCUMENTO_LAYOUT_ENVIO_OCORRENCIA);
	}

	private ControladorContrato getControladorContrato() {

		return ServiceLocator.getInstancia().getControladorContrato();
	}

	private ControladorCobranca getControladorCobranca() {

		return ServiceLocator.getInstancia().getControladorCobranca();
	}

	private ControladorFatura getControladorFatura() {

		return ServiceLocator.getInstancia().getControladorFatura();
	}

	private ControladorPontoConsumo getControladorPontoConsumo() {

		return ServiceLocator.getInstancia().getControladorPontoConsumo();
	}

	private ControladorFeriado getControladorFeriado() {

		return ServiceLocator.getInstancia().getControladorFeriado();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao#gerarDebitosACobrarPorAcrescimentoImpontualidade(br.com.ggas.arrecadacao.recebimento
	 * .Recebimento, boolean, boolean, java.util.Collection)
	 */
	@Override
	public void gerarDebitosACobrarPorAcrescimentoImpontualidade(Recebimento recebimento, boolean indicadorMulta,
					boolean indicadorJurosMora, Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado) throws GGASException {

		this.gerarDebitosACobrarPorAcrescimentoImpontualidade(recebimento, indicadorMulta, indicadorJurosMora, false,
						listaCreditoDebitoNegociado);
	}

	@Override
	public void gerarDebitosACobrarPorAcrescimentoImpontualidade(Recebimento recebimento, boolean indicadorMulta,
					boolean indicadorJurosMora, boolean indicadorCorrecaoMonetaria,
					Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();

		if (recebimento != null) {

			FaturaGeral faturaGeral = null;
			if (recebimento.getDocumentoCobrancaItem() != null) {
				faturaGeral = recebimento.getDocumentoCobrancaItem().getFaturaGeral();
			} else {
				faturaGeral = recebimento.getFaturaGeral();
			}

			faturaGeral = (FaturaGeral) controladorFatura.obter(faturaGeral.getChavePrimaria(), FaturaGeralImpl.class, "faturaAtual");
			recebimento.setFaturaGeral(faturaGeral);

			DebitosACobrar debitosACobrarVO =
							this.calcularAcrescimentoImpontualidade(faturaGeral.getFaturaAtual(), recebimento.getDataRecebimento(),
											recebimento.getValorRecebimento(), indicadorMulta, indicadorJurosMora,
											indicadorCorrecaoMonetaria);

			if (debitosACobrarVO != null) {

				String codigoRubricaJuros =
								controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_JUROS_MORA);
				String codigoRubricaMulta =
								controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_MULTA_ATRASO);

				if ((debitosACobrarVO.getJurosMora() != null) && (debitosACobrarVO.getJurosMora().compareTo(BigDecimal.ZERO) > 0)) {

					this.inserirEstruturaCreditoDebito(recebimento, debitosACobrarVO.getJurosMora(), this.obterRubrica(codigoRubricaJuros),
									null, listaCreditoDebitoNegociado);
				}

				if ((debitosACobrarVO.getMulta() != null) && (debitosACobrarVO.getMulta().compareTo(BigDecimal.ZERO) > 0)) {

					this.inserirEstruturaCreditoDebito(recebimento, debitosACobrarVO.getMulta(), this.obterRubrica(codigoRubricaMulta),
									null, listaCreditoDebitoNegociado);

				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#inserirEstruturaCreditoDebito(br.com.ggas.arrecadacao.recebimento.Recebimento,
	 * java.math.BigDecimal, br.com.ggas.faturamento.rubrica.Rubrica, java.lang.String, java.util.Collection)
	 */
	@Override
	public void inserirEstruturaCreditoDebito(Recebimento recebimento, BigDecimal valor, Rubrica rubrica, String creditoOrigem,
					Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado) throws NegocioException, ConcorrenciaException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Integer referenciaContabil =
						Integer.valueOf((String) controladorParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_CONTABIL));

		ControladorCreditoDebito controladorCreditoDebito =
						(ControladorCreditoDebito) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorCreditoDebito.BEAN_ID_CONTROLADOR_CREDITO_DEBITO);

		CreditoDebitoNegociado creditoDebitoNegociado =
						controladorCreditoDebito.popularCreditoDebitoNegociado(recebimento.getPontoConsumo(), recebimento.getCliente(),
										rubrica, valor, recebimento.getDadosAuditoria(), creditoOrigem);
		controladorCreditoDebito.inserirCreditoDebitoNegociado(creditoDebitoNegociado);

		if (listaCreditoDebitoNegociado != null) {
			listaCreditoDebitoNegociado.add(creditoDebitoNegociado);
		}

		CreditoDebitoARealizar creditoDebitoARealizar =
						controladorCreditoDebito.popularCreditoDebitoARealizar(recebimento.getDocumentoCobrancaItem(),
										recebimento.getFaturaGeral(), valor, creditoDebitoNegociado, recebimento.getDadosAuditoria());
		controladorCreditoDebito.inserirCreditoDebitoARealizar(creditoDebitoARealizar);

		CreditoDebitoDetalhamento creditoDebitoDetalhamento =
						controladorCreditoDebito.popularCreditoDebitoDetalhamento(creditoDebitoARealizar, rubrica,
										recebimento.getDadosAuditoria());
		controladorCreditoDebito.inserirCreditoDebitoDetalhamento(creditoDebitoDetalhamento);

		if ((Util.obterAnoMes(recebimento.getDataRecebimento())).compareTo(referenciaContabil) == 0) {
			controladorCreditoDebito.registrarLancamentoContabil(creditoDebitoNegociado, OperacaoContabil.INCLUIR_CREDITO_DEBITO,
							recebimento.getDataRecebimento());
		} else {
			controladorCreditoDebito.registrarLancamentoContabil(creditoDebitoNegociado, OperacaoContabil.INCLUIR_CREDITO_DEBITO, null);
		}

	}

	/**
	 * Obter rubrica.
	 *
	 * @param chaveRubrica
	 *            the chave rubrica
	 * @return the rubrica
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Rubrica obterRubrica(String chaveRubrica) throws NegocioException {

		ControladorRubrica controladorRubrica = ServiceLocator.getInstancia().getControladorRubrica();

		return (Rubrica) controladorRubrica.obter(Long.parseLong(chaveRubrica));
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * calcularAcrescimentoImpontualidade
	 * (br.com.ggas.faturamento.Fatura,
	 * java.util.Date, boolean,
	 * boolean)
	 */
	@Override
	public DebitosACobrar calcularAcrescimentoImpontualidade(Fatura fatura, Date dataPagamento, BigDecimal valorPagamento,
					boolean indicadorMulta, boolean indicadorJurosMora) throws GGASException {

		return this.calcularAcrescimentoImpontualidade(fatura, dataPagamento, valorPagamento, indicadorMulta, indicadorJurosMora, false);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * calcularAcrescimentoImpontualidade
	 * (br.com.ggas.faturamento.Fatura,
	 * java.util.Date, boolean,
	 * boolean)
	 */
	@Override
	public DebitosACobrar calcularAcrescimentoImpontualidade(Fatura fatura, Date dataPagamento, BigDecimal valorPagamento,
					boolean indicadorMulta, boolean indicadorJurosMora, boolean indicadorCorrecaoMonetaria) throws GGASException {

		BigDecimal valorTotalRubricasIncidemJuros = BigDecimal.ZERO;
		BigDecimal valorTotalRubricasIncidemMulta = BigDecimal.ZERO;

		Collection<FaturaItem> itensFatura = getControladorFatura().listarFaturaItemPorChaveFatura(fatura.getChavePrimaria());

		for (FaturaItem faturaItem : itensFatura) {
			if (faturaItem.getRubrica() != null) {
				if (faturaItem.getRubrica().getIndicadorCobrancaJuros()) {
					valorTotalRubricasIncidemJuros = valorTotalRubricasIncidemJuros.add(faturaItem.getValorTotal());
				}
				if (faturaItem.getRubrica().getIndicadorCobrancaMulta()) {
					valorTotalRubricasIncidemMulta = valorTotalRubricasIncidemMulta.add(faturaItem.getValorTotal());
				}
			} else {
				if (faturaItem.getCreditoDebitoARealizar() != null) {
					Rubrica rubrica =
									this.obterRubrica(String.valueOf(faturaItem.getCreditoDebitoARealizar().getCreditoDebitoNegociado()
													.getRubrica().getChavePrimaria()));

					valorTotalRubricasIncidemJuros = verificaIndicadorJuros(valorTotalRubricasIncidemJuros, faturaItem, rubrica);

					valorTotalRubricasIncidemMulta = verificaIndicadorMulta(valorTotalRubricasIncidemMulta, faturaItem, rubrica);
				}
			}
		}

		DateTime dataVencimentoUtil = getDataVencimentoUtil(fatura);

		Date dataPagto = getDataPagamentoOuHojeSemHora(dataPagamento);

		if (indicadorCorrecaoMonetaria) {

			BigDecimal fatorCorrecaoIndiceFinanceiro = BigDecimal.ONE;
			Parcelamento parcelamento = fatura.getParcelamento();
			if (parcelamento != null) {
				IndiceFinanceiro indiceFinanceiro = parcelamento.getParcelamentoPerfil().getIndiceFinanceiro();
				if (indiceFinanceiro != null) {
					fatorCorrecaoIndiceFinanceiro =
									getControladorContrato().fatorCorrecaoIndiceFinanceiro(indiceFinanceiro, fatura.getDataVencimento());
				}
			} else {
				IndiceFinanceiro indiceFinanceiro = fatura.getContratoAtual().getIndiceFinanceiro();
				if (indiceFinanceiro != null) {
					fatorCorrecaoIndiceFinanceiro =
									getControladorContrato().fatorCorrecaoIndiceFinanceiro(indiceFinanceiro, fatura.getDataVencimento());
				}
			}

			valorTotalRubricasIncidemMulta = valorTotalRubricasIncidemMulta.multiply(fatorCorrecaoIndiceFinanceiro);

		}

		if (dataPagto.after(dataVencimentoUtil.toDate())) {
			return this.calcularJurosEMulta(valorTotalRubricasIncidemJuros, valorTotalRubricasIncidemMulta, indicadorMulta,
							indicadorJurosMora, dataPagto, fatura, valorPagamento);
		}

		return null;
	}

	private BigDecimal verificaIndicadorMulta(BigDecimal valorTotalRubricasIncidemMulta, FaturaItem faturaItem, Rubrica rubrica) {

		if (rubrica.getIndicadorCobrancaMulta()) {
			return valorTotalRubricasIncidemMulta.add(faturaItem.getValorTotal());
		}
		return valorTotalRubricasIncidemMulta;
	}

	private BigDecimal verificaIndicadorJuros(BigDecimal valorTotalRubricasIncidemJuros, FaturaItem faturaItem, Rubrica rubrica) {

		if (rubrica.getIndicadorCobrancaJuros()) {
			return valorTotalRubricasIncidemJuros.add(faturaItem.getValorTotal());
		}
		return valorTotalRubricasIncidemJuros;
	}

	@Override
	public boolean isDocumentoVencido(Fatura fatura, Date dataPagamento) throws NegocioException {

		DateTime dataVencimentoUtil = getDataVencimentoUtil(fatura);

		Date dataPagto = getDataPagamentoOuHojeSemHora(dataPagamento);

		return dataPagto.after(dataVencimentoUtil.toDate());

	}

	private DebitosACobrar calcularJurosEMulta(BigDecimal valorTotalRubricasIncidemJuros, BigDecimal valorTotalRubricasIncidemMulta,
					boolean indicadorMulta, boolean indicadorJurosMora, Date dataPagamento, Fatura fatura, BigDecimal valorPagamento)
					throws GGASException {

		BigDecimal valorTotalPagoParcialmente = calcularValorPagoParcialmente(fatura);

		Contrato contrato = getControladorContrato().consultarContratoPontoConsumoPorPontoConsumoAtivoOuMaisRecente(fatura);

		BigDecimal jurosMora = BigDecimal.ZERO;
		BigDecimal multa = BigDecimal.ZERO;

		if (contrato != null) {
			jurosMora =
							calcularJurosMora(fatura, contrato, indicadorJurosMora, dataPagamento, valorPagamento,
											valorTotalPagoParcialmente, valorTotalRubricasIncidemJuros);

			multa = calcularMulta(fatura, contrato, indicadorMulta, valorTotalRubricasIncidemMulta, valorTotalPagoParcialmente);
		}

		DebitosACobrar debitosACobrarVO = getDebitosACobrar();
		debitosACobrarVO.setJurosMora(jurosMora);
		debitosACobrarVO.setMulta(multa);
		return debitosACobrarVO;

	}

	/**
	 * calcula juros mora
	 *
	 * @param contrato
	 * @param indicadorJurosMora
	 * @param baseCalculoJuros
	 * @param datasVencimento
	 * @param dataPagamento
	 * @param valorPagamento
	 * @param valorTotalPagoParcialmente
	 * @param valorTotalRubricasIncidemJuros
	 * @return
	 * @throws GGASException
	 */
	private BigDecimal calcularJurosMora(Fatura fatura, Contrato contrato, boolean indicadorJurosMora, Date dataPagamento,
					BigDecimal valorPagamento, BigDecimal valorTotalPagoParcialmente, BigDecimal valorTotalRubricasIncidemJuros) {

		BigDecimal baseCalculoJuros =
						gerarBaseCalculoJuros(fatura, valorPagamento, valorTotalPagoParcialmente, valorTotalRubricasIncidemJuros);

		if ((indicadorJurosMora) && BigDecimalUtil.maiorQueZero(baseCalculoJuros)) {

			BigDecimal percentualJurosMora = contrato.getPercentualJurosMora();

			if (percentualJurosMora != null) {

				BigDecimal expoente = gerarDiasPorMesDeItervaloDatas(fatura.getDataVencimento(), dataPagamento);

				percentualJurosMora =
						BigDecimal.valueOf(Math.pow(percentualJurosMora.add(BigDecimal.ONE).doubleValue(), expoente.doubleValue()))
								.subtract(BigDecimal.ONE);

				return baseCalculoJuros.multiply(percentualJurosMora);
			}
		}

		return null;
	}

	private BigDecimal gerarDiasPorMesDeItervaloDatas(Date dataVencimento, Date dataPagamento) {

		Collection<Date> datas = Util.gerarIntervaloDatas(dataVencimento, dataPagamento);
		List<Integer> meses = new ArrayList<>();
		List<Integer> diasPorMes = new ArrayList<>();
		List<Integer> totalDiasPorMes = new ArrayList<>();

		for (Date date : datas) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			if (!meses.isEmpty()) {
				boolean adicionaMes = true;
				for (int i = 0; i < meses.size(); i++) {
					adicionaMes = defineSeAdicionaDia(meses, diasPorMes, cal, adicionaMes, i);
				}
				if (adicionaMes) {
					meses.add(cal.get(Calendar.MONTH) + 1);
					totalDiasPorMes.add(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
					diasPorMes.add(1);
				}
			} else {
				meses.add(cal.get(Calendar.MONTH) + 1);
				totalDiasPorMes.add(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
				diasPorMes.add(1);
			}
		}

		BigDecimal expoente = BigDecimal.ZERO;

		for (int i = 0; i < diasPorMes.size(); i++) {
			expoente =
							expoente.add(BigDecimal.valueOf(diasPorMes.get(i)).divide(BigDecimal.valueOf(totalDiasPorMes.get(i)),
											DIVISOR_DIAS, RoundingMode.HALF_UP));
		}

		return expoente;
	}

	private boolean defineSeAdicionaDia(List<Integer> meses, List<Integer> diasPorMes, Calendar cal, boolean adicionaMes, int i) {

		if (meses.get(i) == cal.get(Calendar.MONTH) + 1) {
			int somaDia = diasPorMes.get(i) + 1;
			diasPorMes.set(i, somaDia);
			return false;
		}
		return adicionaMes;
	}

	/**
	 * calcula multa
	 *
	 * @param contrato
	 * @param indicadorMulta
	 * @param valorTotalRubricasIncidemMulta
	 * @param valorTotalPagoParcialmente
	 * @param baseCalculoMulta
	 * @return
	 * @throws NegocioException
	 */
	private BigDecimal calcularMulta(Fatura fatura, Contrato contrato, boolean indicadorMulta, BigDecimal valorTotalRubricasIncidemMulta,
					BigDecimal valorTotalPagoParcialmente) throws NegocioException {

		BigDecimal baseCalculoMulta = gerarBaseCalculoMulta(fatura, valorTotalRubricasIncidemMulta, valorTotalPagoParcialmente);

		if ((indicadorMulta) && BigDecimalUtil.maiorQueZero(baseCalculoMulta)) {
			BigDecimal percentualMulta = contrato.getPercentualMulta();
			if (percentualMulta != null) {
				return baseCalculoMulta.multiply(percentualMulta).setScale(DUAS_CASAS_DECIMAIS, RoundingMode.HALF_UP);
			}
		}
		return null;
	}

	private BigDecimal gerarBaseCalculoJuros(Fatura fatura, BigDecimal valorPagamento, BigDecimal valorTotalPagoParcialmente,
					BigDecimal valorTotalRubricasIncidemJuros) {

		BigDecimal baseCalculoJuros = valorTotalRubricasIncidemJuros;
		// FIXME: Comparar o valor pago com o saldo da fatura
		if (valorPagamento != null) {
			if (valorPagamento.compareTo(fatura.getValorTotal()) < 0) {
				baseCalculoJuros = baseCalculoJuros.subtract(valorTotalPagoParcialmente);
				if (valorPagamento.compareTo(baseCalculoJuros) < 0) {

					baseCalculoJuros = valorPagamento;
				}
			} else {
				if (BigDecimalUtil.menorQue(valorTotalPagoParcialmente, fatura.getValorTotal())) {
					baseCalculoJuros = baseCalculoJuros.subtract(valorTotalPagoParcialmente);
				}
			}
		}
		return baseCalculoJuros;
	}

	private BigDecimal
					gerarBaseCalculoMulta(Fatura fatura, BigDecimal valorTotalRubricasIncidemMulta, BigDecimal valorTotalPagoParcialmente)
									throws NegocioException {

		if (valorTotalPagoParcialmente.compareTo(BigDecimal.ZERO) != 0) {
			// Maior Data de Recebimento ou Maior Data de Conciliação
			// deverá ser menor ou igual a data de vecimento
			Date dataRecebimentoPelaFatura = getControladorCobranca().obterUltimaDataRecebimentoPelaFatura(fatura.getChavePrimaria());
			Date dataConciliacaoPelaFatura = getControladorCobranca().obterUltimaDataConciliacaoPelaFatura(fatura.getChavePrimaria());

			if (DataUtil.menorQueUmaDasDatas(fatura.getDataVencimento(), dataRecebimentoPelaFatura, dataConciliacaoPelaFatura)) {
				return BigDecimal.ZERO;
			} else {
				return valorTotalRubricasIncidemMulta.subtract(valorTotalPagoParcialmente);
			}
		} else {
			return valorTotalRubricasIncidemMulta;
		}
	}

	/**
	 * calcula a soma dos recebimentos e o valor conciliacao da fatura
	 *
	 * @param fatura
	 * @param controladorCobranca
	 * @return
	 * @throws NegocioException
	 */
	private BigDecimal calcularValorPagoParcialmente(Fatura fatura) throws NegocioException {

		BigDecimal valorTotalPagoParcialmente = BigDecimal.ZERO;

		BigDecimal valorRecebimentos = getControladorCobranca().obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());

		if ((valorRecebimentos != null) && BigDecimalUtil.maiorQueZero(valorRecebimentos)) {
			valorTotalPagoParcialmente = valorTotalPagoParcialmente.add(valorRecebimentos);
		}

		BigDecimal valorConciliacoes = fatura.getValorConciliado();
		if ((valorConciliacoes != null) && BigDecimalUtil.maiorQueZero(valorConciliacoes)) {
			valorTotalPagoParcialmente = valorTotalPagoParcialmente.add(valorConciliacoes);
		}

		return valorTotalPagoParcialmente;
	}

	/**
	 * se a entrada for nula, retorna a data de hoje e zera os atributos hora, minuto, segundo e milisegundo da saida
	 *
	 * @param dataPagamento
	 * @return
	 */
	private Date getDataPagamentoOuHojeSemHora(Date dataPagamento) {

		Date retorno;
		if (dataPagamento == null) {
			retorno = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		} else {
			Calendar data = Calendar.getInstance();
			data.setTime(dataPagamento);
			data.set(Calendar.HOUR, 0);
			data.set(Calendar.MINUTE, 0);
			data.set(Calendar.SECOND, 0);
			data.set(Calendar.MILLISECOND, 0);
			retorno = data.getTime();
		}
		return retorno;
	}

	/**
	 * verifica se a data de pagamento é um dia útil,
	 * caso nao seja, retorna o proximo dia util
	 *
	 * @param fatura
	 * @param datasVencimento
	 * @return
	 * @throws NegocioException
	 */
	private DateTime getDataVencimentoUtil(Fatura fatura) throws NegocioException {

		Date dataVencimento = fatura.getDataVencimento();
		Long idMunicipio = getMunicipioId(fatura);
		if (getControladorFeriado().isDiaUtil(dataVencimento, idMunicipio, Boolean.TRUE)) {
			return new DateTime(getDataPagamentoOuHojeSemHora(dataVencimento));
		} else {
			Date proximaDataUtil = getControladorFeriado().obterProximoDiaUtil(dataVencimento, idMunicipio, Boolean.TRUE);
			return new DateTime(getDataPagamentoOuHojeSemHora(proximaDataUtil));
		}
	}

	/**
	 * pega o id do municipio do ponto de consumo,
	 * se o ponto de consumo estiver nulo retorna o municipio
	 * do cliente da fatura
	 *
	 * @param fatura
	 * @return
	 * @throws NegocioException
	 */
	private Long getMunicipioId(Fatura fatura) throws NegocioException {

		if (fatura.getPontoConsumo() != null) {
			PontoConsumo pontoConsumo =
							(PontoConsumo) getControladorPontoConsumo().obter(fatura.getPontoConsumo().getChavePrimaria(), "quadraFace");
			return pontoConsumo.getQuadraFace().getQuadra().getSetorComercial().getMunicipio().getChavePrimaria();
		} else {
			return fatura.getIdMunicipioCliente();
		}
	}

	private DebitosACobrar getDebitosACobrar() {

		return (DebitosACobrar) ServiceLocator.getInstancia().getBeanPorID(DebitosACobrar.BEAN_ID_DEBITOS_A_COBRAR);
	}

	@Override
	public BigDecimal obterValorPercentual(String chaveIndice) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		BigDecimal valorPercentual = BigDecimal.ZERO;

		EncargoTributario encargo = this.obterValorNominalEncargoTributario(Long.valueOf(chaveIndice));
		if (encargo != null) {
			BigDecimal percentual;

			if (encargo.getPercentualNominal() != null) {
				percentual = encargo.getPercentualNominal();
			} else {
				percentual = BigDecimal.ZERO;
			}

			if (encargo.getPeriodicidade() != null) {
				EntidadeConteudo periodicidade =
								(EntidadeConteudo) controladorEntidadeConteudo.obter(encargo.getPeriodicidade().getChavePrimaria());
				BigDecimal tamanhoPeriodicidade = new BigDecimal(periodicidade.getCodigo());
				valorPercentual = percentual.divide(tamanhoPeriodicidade, DIVISOR, RoundingMode.HALF_UP);
			} else {
				valorPercentual = percentual;
			}
		}

		return valorPercentual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#obterValorNominalEncargoTributario(java.lang.Long)
	 */
	@Override
	public EncargoTributario obterValorNominalEncargoTributario(Long chaveEncargo) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeEncargoTributario().getSimpleName());
		hql.append(" encargo ");
		hql.append(WHERE);
		hql.append(" encargo.encargoTributario.chavePrimaria = :CHAVE_ENCARGO ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_ENCARGO", chaveEncargo);

		return (EncargoTributario) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#inserirArrecadacaoMovimento(br.com.ggas.arrecadacao.ArrecadadorMovimento)
	 */
	@Override
	public void inserirArrecadacaoMovimento(ArrecadadorMovimento arrecadadorMovimento) throws NegocioException {

		arrecadadorMovimento.setUltimaAlteracao(Calendar.getInstance().getTime());
		super.inserir(arrecadadorMovimento);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#alterarArrecadacaoMovimento(br.com.ggas.arrecadacao.ArrecadadorMovimento)
	 */
	@Override
	public void alterarArrecadacaoMovimento(ArrecadadorMovimento arrecadadorMovimento) throws ConcorrenciaException, NegocioException {

		arrecadadorMovimento.setUltimaAlteracao(Calendar.getInstance().getTime());
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
		getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(arrecadadorMovimento);
		getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
		getSession().flush();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#inserirArrecadacaoMovimentoItem(br.com.ggas.arrecadacao.ArrecadadorMovimentoItem)
	 */
	@Override
	public void inserirArrecadacaoMovimentoItem(ArrecadadorMovimentoItem arrecadadorMovimentoItem) throws NegocioException {

		super.inserir(arrecadadorMovimentoItem);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * inserirAvisosBancarios(java.util.Map)
	 */
	@Override
	public void inserirAvisosBancarios(Map<Date, AvisoBancario> mapAvisosBancarios) {

		Iterator iterator = mapAvisosBancarios.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry avisoBancario = (Map.Entry) iterator.next();
			this.inserirAvisoBancario((AvisoBancario) avisoBancario.getValue());
		}
	}

	/**
	 * Inserir aviso bancario.
	 *
	 * @param avisoBancario
	 *            the aviso bancario
	 */
	public void inserirAvisoBancario(AvisoBancario avisoBancario) {

		avisoBancario.setUltimaAlteracao(Calendar.getInstance().getTime());
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(avisoBancario);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * gerarMovimentoCobrancaRegistrada(java.util.
	 * Collection)
	 */
	@Override
	public StringBuilder gerarMovimentoCobrancaRegistrada(Collection<CobrancaBancariaMovimento> listaCobrancaBancariaMovimento)
					throws GGASException {

		return this.gerarMovimentoCobranca(listaCobrancaBancariaMovimento);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * gerarMovimentoCobrancaNaoRegistrada(java.util
	 * .Collection)
	 */
	@Override
	public StringBuilder gerarMovimentoCobrancaNaoRegistrada(Collection<CobrancaBancariaMovimento> listaCobrancaBancariaMovimento)
					throws GGASException {

		return this.gerarMovimentoCobranca(listaCobrancaBancariaMovimento);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#montarArquivoDebitosNaoProcessados(java.util.Collection,
	 * br.com.ggas.arrecadacao.Arrecadador, br.com.ggas.arrecadacao.ArrecadadorMovimento)
	 */
	@Override
	public StringBuilder montarArquivoDebitosNaoProcessados(Collection<DebitoNaoProcessado> colecaoDebitoNaoProcessado,
					Arrecadador arrecadador, ArrecadadorMovimento arrecadadorMovimento, DocumentoLayout documentoLayout)
					throws NegocioException, ConcorrenciaException {

		class ArrecadadorWriter {

			private BufferedWriter bufferedWriter;

			private boolean cabecalhoGerado;

			private int totalLinhas;

			private BigDecimal somatorio;

			private String caminhoArquivo;
		}

		ServiceLocator serviceLocator = ServiceLocator.getInstancia();
		ControladorParametroSistema controladorParametroSistema = serviceLocator.getControladorParametroSistema();
		Map<String, Object> dados = null;
		String linha = null;
		EntidadeConteudo tipoConvenio = null;
		ArrecadadorWriter arrecadadorWriter = null;
		String diretorioArquivosRemessa = null;
		StringBuilder logProcessamento = new StringBuilder();
		DocumentoLayoutRegistro documentoLayoutRegistro = null;
		Map<String, DocumentoLayoutRegistro> mapaDocumentoLayoutRegistro = null;

		try {

			ControladorDocumentoLayout controladorDocumentoLayout = ServiceLocator.getInstancia().getControladorDocumentoLayout();
			ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

			ControladorConstanteSistema controladorConstanteSistema =
							(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
											ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

			diretorioArquivosRemessa = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(DIRETORIO_ARQUIVOS_REMESSA);

			tipoConvenio =
							controladorEntidadeConteudo.obter(Long.valueOf(controladorConstanteSistema
											.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_DEBITO_AUTOMATICO)));

			ArrecadadorContratoConvenio arrecadadorContratoConvenio =
							obterArrecadadorContratoConvenioDebitoAutomatico(arrecadador.getChavePrimaria());

			mapaDocumentoLayoutRegistro =
							criarMapaDocumentoLayoutRegistroDebitoAutomatico(arrecadadorContratoConvenio.getLeiaute().getRegistros());

			logProcessamento.append("Tipo de convênio: ");
			logProcessamento.append(tipoConvenio.getDescricao());
			logProcessamento.append("\r\n");

			if (arrecadadorWriter == null) {

				arrecadadorWriter = new ArrecadadorWriter();

				String nomeArrecadador = defineNomeArrecadador(arrecadador);

				String pathDiretorioDestino = diretorioArquivosRemessa + SEPARATOR + tipoConvenio.getDescricao() +
						SEPARATOR + nomeArrecadador + SEPARATOR;
				verificaDiretorio(pathDiretorioDestino);

				Long nsaRemessaNovo = null;
				ArrecadadorContratoConvenio acConvenio =
								this.obterACConvenioPorArrecadador(arrecadador.getChavePrimaria(), tipoConvenio.getChavePrimaria());
				if (acConvenio != null && acConvenio.getNsaRemessa() != null) {
					nsaRemessaNovo = acConvenio.getNsaRemessa() + 1;
				} else {
					nsaRemessaNovo = 0L;
				}

				arrecadadorWriter.caminhoArquivo =
								pathDiretorioDestino + PREFIXO_ARQUIVO_DEBITO_AUTOMATICO_NAO_PROCESSADO + nsaRemessaNovo + "."
												+ SUFIXO_ARQUIVO_REMESSA;

				// verifica se arquivo já existe para não sobrescrever
				File arquivoGerado = Util.getFile(arrecadadorWriter.caminhoArquivo);
				int sequencial = 1;

				while (arquivoGerado.exists()) {
					arrecadadorWriter.caminhoArquivo =
									pathDiretorioDestino + PREFIXO_ARQUIVO_DEBITO_AUTOMATICO_NAO_PROCESSADO + nsaRemessaNovo + "_"
													+ sequencial + "." + SUFIXO_ARQUIVO_REMESSA;
					arquivoGerado = Util.getFile(arrecadadorWriter.caminhoArquivo);
					sequencial++;
				}

				arrecadadorWriter.bufferedWriter = new BufferedWriter(Util.getFileWriter(arrecadadorWriter.caminhoArquivo));

				arrecadadorWriter.somatorio = BigDecimal.ZERO;
				arrecadadorWriter.totalLinhas = 0;

			}

			for (DebitoNaoProcessado debitoNaoProcessado : colecaoDebitoNaoProcessado) {

				// Montar Cabeçalho
				if (!arrecadadorWriter.cabecalhoGerado) {

					documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("A");

					dados =
									controladorDocumentoLayout.gerarDadosDocumentoRemessaDebitosNaoProcessados(
													arrecadadorWriter.totalLinhas, arrecadadorWriter.somatorio, debitoNaoProcessado,
													documentoLayoutRegistro, arrecadadorMovimento, arrecadador, serviceLocator);

					arrecadadorWriter.totalLinhas++;
					linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);
					arrecadadorWriter.bufferedWriter.write(linha);
					arrecadadorWriter.bufferedWriter.newLine();
					arrecadadorWriter.cabecalhoGerado = true;

				}

				// Montar Tipo 'C'
				documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("C");

				dados =
								controladorDocumentoLayout.gerarDadosDocumentoRemessaDebitosNaoProcessados(arrecadadorWriter.totalLinhas,
												arrecadadorWriter.somatorio, debitoNaoProcessado, documentoLayoutRegistro,
												arrecadadorMovimento, arrecadador, serviceLocator);

				arrecadadorWriter.totalLinhas++;
				linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);

				arrecadadorWriter.bufferedWriter.write(linha);
				arrecadadorWriter.bufferedWriter.newLine();

			}

			// Montar Trilha
			if (arrecadadorWriter.cabecalhoGerado) {

				arrecadadorWriter.totalLinhas++;

				documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("Z");

				dados =
								controladorDocumentoLayout.gerarDadosDocumentoRemessaDebitoAutomatico(arrecadadorWriter.totalLinhas,
												BigDecimal.ZERO, null, documentoLayoutRegistro, serviceLocator);

				// Gerar a linha
				linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);

				// Gravar linha no arquivo
				arrecadadorWriter.bufferedWriter.write(linha);

			}

		} catch (IOException e) {
			LOG.error(e.getStackTrace(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		} finally {
			// [Fechando e salvando os arquivos no
			// disco]
			// **********************************************************************************

			try {
				arrecadadorWriter.bufferedWriter.close();
			} catch (Exception e) {
				LOG.error(e.getStackTrace(), e);
				Logger.getLogger("ERROR").debug(e.getStackTrace());
			}

		}

		return logProcessamento;

	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * gerarMovimentoDebitoAutomatico(java.util.
	 * Collection)
	 */
	@Override
	public StringBuilder gerarMovimentoDebitoAutomatico(Collection<DebitoAutomaticoMovimento> listaDebitoAutomaticoMovimento)
					throws NegocioException, ConcorrenciaException {

		class ArrecadadorWriter {

			private BufferedWriter bufferedWriter;

			private boolean cabecalhoGerado;

			private int totalLinhas;

			private BigDecimal somatorio;

			private String caminhoArquivo;
		}

		Map<Long, ArrecadadorWriter> mapaArrecadadorWrite = new HashMap<>();
		ArrecadadorWriter arrecadadorWriter = null;
		Arrecadador arrecadador = null;
		EntidadeConteudo tipoConvenio = null;
		Map<String, Object> dados = null;
		String linha = null;
		ServiceLocator serviceLocator = ServiceLocator.getInstancia();
		String codigoTipoConvenio = null;
		String diretorioArquivosRemessa = null;
		StringBuilder logProcessamento = new StringBuilder();
		DocumentoLayoutRegistro documentoLayoutRegistro = null;
		Map<String, DocumentoLayoutRegistro> mapaDocumentoLayoutRegistro = null;

		try {

			ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
			ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
			ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
			ControladorDocumentoLayout controladorDocumentoLayout = ServiceLocator.getInstancia().getControladorDocumentoLayout();

			codigoTipoConvenio =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_DEBITO_AUTOMATICO);

			tipoConvenio = controladorEntidadeConteudo.obter(Long.valueOf(codigoTipoConvenio));

			logProcessamento.append("Tipo de convênio: ");
			logProcessamento.append(tipoConvenio.getDescricao());
			logProcessamento.append("\r\n");

			// Obter o diretorio dos arquivos de remessa
			diretorioArquivosRemessa = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(DIRETORIO_ARQUIVOS_REMESSA);
			logProcessamento.append("Diretório para armazenamento dos arquivos: ");
			logProcessamento.append(diretorioArquivosRemessa);
			logProcessamento.append("\r\n");

			// Data de envio
			Date dataEnvio = new Date(System.currentTimeMillis());

			// Iterar sobre os dados do debito automático
			for (DebitoAutomaticoMovimento debitoAutomaticoMovimento : listaDebitoAutomaticoMovimento) {

				arrecadador = obterArrecadador(debitoAutomaticoMovimento.getDebitoAutomatico().getArrecadador().getChavePrimaria());
				ArrecadadorContratoConvenio arrecadadorContratoConvenio =
								obterArrecadadorContratoConvenioDebitoAutomatico(arrecadador.getChavePrimaria());

				mapaDocumentoLayoutRegistro =
								criarMapaDocumentoLayoutRegistroDebitoAutomatico(arrecadadorContratoConvenio.getLeiaute().getRegistros());

				// Para cada débito, processar e gerar arquivo
				arrecadadorWriter = mapaArrecadadorWrite.get(arrecadador.getChavePrimaria());

				if (arrecadadorWriter == null) {

					arrecadadorWriter = new ArrecadadorWriter();

					String nomeArrecadador = defineNomeArrecadador(arrecadador);

					String pathDiretorioDestino = diretorioArquivosRemessa + SEPARATOR + tipoConvenio.getDescricao() + SEPARATOR + nomeArrecadador
									+ SEPARATOR;
					verificaDiretorio(pathDiretorioDestino);

					Long nsaRemessaNovo = defineNsaRemessaNovo(arrecadador, codigoTipoConvenio);


					arrecadadorWriter.caminhoArquivo =
									pathDiretorioDestino + PREFIXO_ARQUIVO_REMESSA_DEBITO_AUTOMATICO + nsaRemessaNovo + "."
													+ SUFIXO_ARQUIVO_REMESSA;

					// verifica se arquivo já
					// existe para não
					// sobrescrever
					File arquivoGerado = Util.getFile(arrecadadorWriter.caminhoArquivo);
					int sequencial = 1;

					while (arquivoGerado.exists()) {
						arrecadadorWriter.caminhoArquivo =
										pathDiretorioDestino + PREFIXO_ARQUIVO_REMESSA_DEBITO_AUTOMATICO + nsaRemessaNovo + "_"
														+ sequencial + "." + SUFIXO_ARQUIVO_REMESSA;
						arquivoGerado = Util.getFile(arrecadadorWriter.caminhoArquivo);
						sequencial++;
					}

					arrecadadorWriter.bufferedWriter = new BufferedWriter(Util.getFileWriter(arrecadadorWriter.caminhoArquivo));

					arrecadadorWriter.somatorio = BigDecimal.ZERO;
					arrecadadorWriter.totalLinhas = 0;
					mapaArrecadadorWrite.put(arrecadador.getChavePrimaria(), arrecadadorWriter);
				}

				// [Gerando o cabecalho]
				// **********************************************************************************
				if (!arrecadadorWriter.cabecalhoGerado) {

					documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("A");

					// Montar os dados no mapa
					dados =
									controladorDocumentoLayout.gerarDadosDocumentoRemessaDebitoAutomatico(arrecadadorWriter.totalLinhas,
													arrecadadorWriter.somatorio, debitoAutomaticoMovimento, documentoLayoutRegistro,
													serviceLocator);

					arrecadadorWriter.totalLinhas++;
					linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);
					arrecadadorWriter.bufferedWriter.write(linha);
					arrecadadorWriter.bufferedWriter.newLine();
					arrecadadorWriter.cabecalhoGerado = true;

				}

				// [Gerando o detalhe]
				// **********************************************************************************

				documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("E");

				// Montar os dados no mapa
				dados =
								controladorDocumentoLayout.gerarDadosDocumentoRemessaDebitoAutomatico(arrecadadorWriter.totalLinhas,
												arrecadadorWriter.somatorio, debitoAutomaticoMovimento, documentoLayoutRegistro,
												serviceLocator);

				arrecadadorWriter.totalLinhas++;

				// Somar o campo do débito, pois ele será usado no Z
				if (debitoAutomaticoMovimento.getValorDebito() != null) {
					arrecadadorWriter.somatorio = arrecadadorWriter.somatorio.add(debitoAutomaticoMovimento.getValorDebito());
				}

				// Gerar a linha
				linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);

				// Gravar linha no arquivo
				arrecadadorWriter.bufferedWriter.write(linha);
				arrecadadorWriter.bufferedWriter.newLine();

				// Atualizando a data de envio
				debitoAutomaticoMovimento.setDataEnvio(dataEnvio);
				debitoAutomaticoMovimento.setUltimaAlteracao(dataEnvio);
				// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
				getHibernateTemplate().getSessionFactory().getCurrentSession().update(debitoAutomaticoMovimento);

			}

			// [Gerar a trilha Z para todos os arquivos]
			// **********************************************************************************

			for (Iterator<ArrecadadorWriter> iterator = mapaArrecadadorWrite.values().iterator(); iterator.hasNext();) {

				arrecadadorWriter = iterator.next();
				arrecadadorWriter.totalLinhas++;

				documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("Z");

				// Montar os dados no mapa
				dados =
								controladorDocumentoLayout.gerarDadosDocumentoRemessaDebitoAutomatico(arrecadadorWriter.totalLinhas,
												arrecadadorWriter.somatorio, null, documentoLayoutRegistro, serviceLocator);

				// Gerar a linha
				linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);

				// Gravar linha no arquivo
				arrecadadorWriter.bufferedWriter.write(linha);
			}

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		} finally {
			// [Fechando e salvando os arquivos no disco]
			// **********************************************************************************
			for (Iterator<ArrecadadorWriter> iterator = mapaArrecadadorWrite.values().iterator(); iterator.hasNext();) {

				arrecadadorWriter = iterator.next();

				try {
					arrecadadorWriter.bufferedWriter.newLine();
					arrecadadorWriter.bufferedWriter.close();
				} catch (IOException e) {
					LOG.error(e.getMessage(), e);
				}
			}
			logProcessamento.append("Foram gerados ").append(mapaArrecadadorWrite.size()).append(" arquivos.");
		}

		return logProcessamento;

	}

	private String defineNomeArrecadador(Arrecadador arrecadador) {
		String nomeArrecadador = "";
		if (arrecadador.getBanco() != null) {
			nomeArrecadador = arrecadador.getBanco().getNomeAbreviado();
		} else {
			nomeArrecadador = arrecadador.getCliente().getNomeAbreviado();
		}
		return nomeArrecadador;
	}

	private Long defineNsaRemessaNovo(Arrecadador arrecadador, String codigoTipoConvenio) throws NegocioException {
		Long nsaRemessaNovo = null;
		ArrecadadorContratoConvenio acConvenio = this.obterACConvenioPorArrecadador(arrecadador.getChavePrimaria(),
						Long.valueOf(codigoTipoConvenio));
		if (acConvenio != null && acConvenio.getNsaRemessa() != null) {
			nsaRemessaNovo = acConvenio.getNsaRemessa() + 1;
		} else {
			nsaRemessaNovo = 0L;
		}
		return nsaRemessaNovo;
	}

	private void verificaDiretorio(String pathDiretorioDestino) throws NegocioException {
		File diretorioDestino = Util.getFile(pathDiretorioDestino);
		if (!diretorioDestino.exists() && !diretorioDestino.mkdirs()) {

			throw new NegocioException(ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, Constantes.LOCALE_PADRAO),
							Constantes.PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_CRIAR_DIRETORIO);
		}
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * listarDebitoAutomaticoMovimento()
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Collection<DebitoAutomaticoMovimento> listarDebitoAutomaticoMovimento() {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeDebitoAutomaticoMovimento().getSimpleName());
		hql.append(DEBITO_AUTOMATICO_MOVIMENTO);
		hql.append(" inner join fetch debitoAutomaticoMovimento.debitoAutomatico debitoAutomatico");
		hql.append(" inner join fetch debitoAutomaticoMovimento.debitoAutomatico.clienteDebitoAutomatico clienteDebitoAutomatico");
		hql.append(" inner join fetch debitoAutomaticoMovimento.debitoAutomatico.clienteDebitoAutomatico.cliente cliente");
		hql.append(" inner join fetch debitoAutomaticoMovimento.debitoAutomatico.arrecadador ");
		hql.append(WHERE);
		hql.append(" debitoAutomaticoMovimento.dataEnvio is null ");
		hql.append(" and debitoAutomatico.dataExclusao is null ");
		hql.append(" and debitoAutomaticoMovimento.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/**
	 * Listar debito automatico movimento.
	 *
	 * @param debitoAutomatico the debito automatico
	 * @return the collection
	 */
	public Collection<DebitoAutomaticoMovimento> listarDebitoAutomaticoMovimento(DebitoAutomatico debitoAutomatico) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeDebitoAutomaticoMovimento().getSimpleName());
		hql.append(DEBITO_AUTOMATICO_MOVIMENTO);
		hql.append(" inner join fetch debitoAutomaticoMovimento.debitoAutomatico debitoAutomatico");
		hql.append(" inner join fetch debitoAutomaticoMovimento.debitoAutomatico.arrecadador ");
		hql.append(WHERE);
		hql.append(" debitoAutomaticoMovimento.dataEnvio is null ");
		hql.append(" and debitoAutomatico.dataExclusao is null ");
		hql.append(" and debitoAutomaticoMovimento.habilitado = true ");
		hql.append(" and debitoAutomaticoMovimento.debitoAutomatico.chavePrimaria = ?");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, debitoAutomatico.getChavePrimaria());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * listarArrecadadorExistenteDescricao()
	 */
	@Override
	public Collection<Arrecadador> listarArrecadadorExistenteDescricao() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append("from ");
		hql.append(getClasseEntidadeArrecadador().getSimpleName()).append(" arrecadador");
		hql.append(" left join fetch arrecadador.banco");
		hql.append(" left join fetch arrecadador.cliente");
		hql.append(" where arrecadador.habilitado = true");
		hql.append(" order by arrecadador.codigoAgente");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * listarTipoDocumentoExistente()
	 */
	@Override
	public Collection<TipoDocumento> listarTipoDocumentoExistente() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTipoDocumento());
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * listarTipoDocumentoExistente()
	 */
	@Override
	public Collection<TipoDocumento> listarTipoDocumentoRecebimento() throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Collection<Long> colecaoChaves = new ArrayList<>();
		String parametroFatura = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);
		String parametroNotasDebito =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO);

		Criteria criteria = createCriteria(getClasseEntidadeTipoDocumento());

		if (parametroFatura != null && parametroNotasDebito != null) {
			colecaoChaves.add(Long.valueOf(parametroFatura));
			// fatura
			colecaoChaves.add(Long.valueOf(parametroNotasDebito));
			// nota
			// de
			// débito
			criteria.add(Restrictions.in(CHAVE_PRIMARIA, colecaoChaves));
		}

		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #obterArrecadador
	 * (java.lang.Long)
	 */
	@Override
	public Arrecadador obterArrecadador(Long idArrecadador) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeArrecadador().getSimpleName());
		hql.append(" arrecadador where arrecadador.chavePrimaria = :idArrecadador ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idArrecadador", idArrecadador);

		return (Arrecadador) query.uniqueResult();
	}

	@Override
	public Arrecadador obterArrecadador(Long idArrecadador, String... propriedadesLazy) throws GGASException {

		return (Arrecadador) obter(idArrecadador, ArrecadadorImpl.class, propriedadesLazy);
	}

	/**
	 * Atualiza objeto arrecadador
	 * @param arrecadador é o objeto para ser atualizado
	 * @throws GGASException é lançado se ocorrer algum erro
	 */
	public void atualizarArrecadador(Arrecadador arrecadador) throws GGASException {

		atualizar(arrecadador,ArrecadadorImpl.class);
	}


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#obterACConvenioPorArrecadador(java.lang.Long, java.lang.Long)
	 */
	@Override
	public ArrecadadorContratoConvenio obterACConvenioPorArrecadador(Long chaveArrecadador, Long chaveTipoConvenio) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeArrecadadorContratoConvenio().getSimpleName());
		hql.append(ACCONVENIO);
		hql.append(WHERE);
		hql.append(" acConvenio.arrecadadorContrato.arrecadador.chavePrimaria = :CHAVE_ARRECADADOR ");
		hql.append(" and acConvenio.tipoConvenio.chavePrimaria = :CHAVE_TIPO_CONVENIO ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(CHAVE_ARRECADADOR, chaveArrecadador);
		query.setParameter("CHAVE_TIPO_CONVENIO", chaveTipoConvenio);

		return (ArrecadadorContratoConvenio) query.setMaxResults(1).uniqueResult();

	}

	/**
	 * Consultar ac convenio por arrecadador.
	 *
	 * @param chaveArrecadador
	 *            the chave arrecadador
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<ArrecadadorContratoConvenio> consultarACConvenioPorArrecadador(Long chaveArrecadador) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeArrecadadorContratoConvenio().getSimpleName());
		hql.append(ACCONVENIO);
		hql.append(" inner join fetch acConvenio.arrecadadorContrato arrecadadorContrato ");
		hql.append(" inner join fetch arrecadadorContrato.arrecadador arrecadador ");
		hql.append(" left join fetch  acConvenio.contaCredito");
		hql.append(WHERE);
		hql.append(" arrecadador.chavePrimaria = :CHAVE_ARRECADADOR ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(CHAVE_ARRECADADOR, chaveArrecadador);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #
	 * obterACConvenioPorTipoConvenio(java.lang.Long
	 * )
	 */
	@Override
	public ArrecadadorContratoConvenio obterACConvenioPorTipoConvenio(Long chaveTipoConvenio) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeArrecadadorContratoConvenio().getSimpleName());
		hql.append(ACCONVENIO);
		hql.append(WHERE);
		hql.append(" acConvenio.tipoConvenio.chavePrimaria = :CHAVE_TIPO_CONVENIO ");
		hql.append(" and acConvenio.habilitado = :HABILITADO ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_TIPO_CONVENIO", chaveTipoConvenio);
		query.setParameter("HABILITADO", Boolean.TRUE);

		query.setMaxResults(1);
		return (ArrecadadorContratoConvenio) query.uniqueResult();
	}

	/**
	 * Atualizar nsa remessa do arrecadador.
	 *
	 * @param arrecadadorContratoConvenio
	 *            the arrecadador contrato convenio
	 * @param nsaRemessaNovo
	 *            the nsa remessa novo
	 */
	private void atualizarNSARemessaDoArrecadador(ArrecadadorContratoConvenio arrecadadorContratoConvenio, Long nsaRemessaNovo) {

		arrecadadorContratoConvenio.setNsaRemessa(nsaRemessaNovo);
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(arrecadadorContratoConvenio);

	}

	/**
	 * Atualizar nsa retorno do arrecadador.
	 *
	 * @param arrecadadorContratoConvenio
	 *            the arrecadador contrato convenio
	 * @param nsaRetornoNovo
	 *            the nsa retorno novo
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void atualizarNSARetornoDoArrecadador(ArrecadadorContratoConvenio arrecadadorContratoConvenio, Long nsaRetornoNovo)
					throws ConcorrenciaException, NegocioException {

		arrecadadorContratoConvenio.setNsaRetorno(nsaRetornoNovo);
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(arrecadadorContratoConvenio);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#gerarNSAArrecadador(java.lang.Long, java.lang.Long)
	 */
	@Override
	public Long gerarNSAArrecadador(Long chaveArrecadador, Long chaveTipoConvenio) throws NegocioException, ConcorrenciaException {

		Long nsaRemessaNovo = null;
		ArrecadadorContratoConvenio acConvenio = this.obterACConvenioPorArrecadador(chaveArrecadador, chaveTipoConvenio);

		if (acConvenio != null && acConvenio.getNsaRemessa() != null) {
			nsaRemessaNovo = acConvenio.getNsaRemessa() + 1;
			this.atualizarNSARemessaDoArrecadador(acConvenio, nsaRemessaNovo);
		} else {
			throw new NegocioException(ControladorArrecadacao.ERRO_NEGOCIO_GERACAO_NSA_REMESSA, true);
		}
		return nsaRemessaNovo;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * incrementarNSARetornoArrecadador(java.lang.Long
	 * , java.lang.Long)
	 */
	@Override
	public void incrementarNSARetornoArrecadador(Long chaveArrecadador, Long chaveTipoConvenio) throws NegocioException,
					ConcorrenciaException {

		Long nsaRetornoNovo = null;
		ArrecadadorContratoConvenio acConvenio = this.obterACConvenioPorArrecadador(chaveArrecadador, chaveTipoConvenio);
		if (acConvenio != null && acConvenio.getNsaRetorno() != null) {
			nsaRetornoNovo = acConvenio.getNsaRetorno() + 1;
			this.atualizarNSARetornoDoArrecadador(acConvenio, nsaRetornoNovo);
		} else {
			throw new NegocioException(ControladorArrecadacao.ERRO_NEGOCIO_GERACAO_NSA_RETORNO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #obterCodigoConvenio
	 * (java.lang.Long, java.lang.Long)
	 */
	@Override
	public String obterCodigoConvenio(Long chaveArrecadador, Long chaveTipoConvenio) throws NegocioException {

		ArrecadadorContratoConvenio acConvenio = this.obterACConvenioPorArrecadador(chaveArrecadador, chaveTipoConvenio);
		return acConvenio.getCodigoConvenio();

	}

	/**
	 * Criar recebimento.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarRecebimento() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#listarCobrancaBancariaMovimento(br.com.ggas.geral.EntidadeConteudo)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<CobrancaBancariaMovimento> listarCobrancaBancariaMovimento(EntidadeConteudo tipoCarteira) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeCobrancaBancariaMovimento().getSimpleName());
		hql.append(COB_BANCARIA_MOVIMENTO);
		hql.append(" where cobBancariaMovimento.arrecadadorContratoConvenio.arrecadadorCarteiraCobranca.tipoCarteira.chavePrimaria = ");
		hql.append(" :chaveTipoCarteira ");
		hql.append(" and cobBancariaMovimento.dataEnvio is null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveTipoCarteira", tipoCarteira.getChavePrimaria());

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #
	 * consultarACConvenioPorArrecadador(java.lang
	 * .Long, java.lang.Long)
	 */
	@Override
	public Collection<ArrecadadorContratoConvenio> consultarACConvenioPorArrecadador(Long chaveArrecadador, Long chaveFormaArrecadacao)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeArrecadadorContratoConvenio().getSimpleName());
		hql.append(ACCONVENIO);
		hql.append(" inner join fetch acConvenio.arrecadadorContrato arrecadadorContrato ");
		hql.append(" inner join fetch arrecadadorContrato.arrecadador arrecadador ");
		hql.append(" inner join fetch acConvenio.tipoConvenio tipoConvenio ");
		hql.append(" left join fetch  acConvenio.contaCredito ");
		hql.append(WHERE);
		hql.append(" arrecadador.chavePrimaria = :CHAVE_ARRECADADOR ");
		hql.append(" and (arrecadadorContrato.dataFimContrato is NULL or arrecadadorContrato.dataFimContrato <= :DATA_FIM2)");

		if (chaveFormaArrecadacao != null && chaveFormaArrecadacao > 0) {
			hql.append(" and acConvenio.tipoConvenio.chavePrimaria = :CHAVE_FORMA_ARRRECADACAO ");
		}

		hql.append(" order by tipoConvenio.descricao asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(CHAVE_ARRECADADOR, chaveArrecadador);
		query.setParameter("DATA_FIM2", Calendar.getInstance().getTime());

		if (chaveFormaArrecadacao != null && chaveFormaArrecadacao > 0) {
			query.setParameter("CHAVE_FORMA_ARRRECADACAO", chaveFormaArrecadacao);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * obterArrecadadorPorBanco(java.lang.Long)
	 */
	@Override
	public Arrecadador obterArrecadadorPorBanco(Long idBanco) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeArrecadador().getSimpleName());
		hql.append(" arrecadador where arrecadador.banco.codigoBanco = :idBanco ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idBanco", idBanco);

		return (Arrecadador) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * obterArrecadadorContratoConvenio(java.lang.
	 * String, long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public ArrecadadorContratoConvenio obterArrecadadorContratoConvenio(String codigoConvenio, long chaveArrecadador)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeArrecadadorContratoConvenio().getSimpleName());
		hql.append(ARRECADADOR_CONTRATO_CONVENIO);
		hql.append(" inner join fetch arrecadadorContratoConvenio.arrecadadorContrato ");
		hql.append(" inner join fetch arrecadadorContratoConvenio.arrecadadorContrato.arrecadador arrecadador ");
		hql.append(WHERE);
		hql.append(" arrecadadorContratoConvenio.codigoConvenio like :codigoConvenio ");
		hql.append(" and arrecadadorContratoConvenio.arrecadadorContrato.dataFimContrato is null ");
		hql.append(" and arrecadador.chavePrimaria = :chaveArrecadador ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("codigoConvenio", codigoConvenio);
		query.setLong(CHAVE_ARRECADADOR_UM, chaveArrecadador);

		return (ArrecadadorContratoConvenio) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * obterArrecadadorContratoConvenio(java.lang.
	 * String, long)
	 */
	@Override
	public ArrecadadorContratoConvenio obterArrecadadorContratoConvenio(long chavePrimaria) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeArrecadadorContratoConvenio().getSimpleName());
		hql.append(ARRECADADOR_CONTRATO_CONVENIO);
		hql.append(" inner join fetch arrecadadorContratoConvenio.arrecadadorContrato ");
		hql.append(" inner join fetch arrecadadorContratoConvenio.arrecadadorContrato.arrecadador arrecadador ");
		hql.append(WHERE);
		hql.append(" arrecadadorContratoConvenio.chavePrimaria = :chavePrimaria ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(CHAVE_PRIMARIA, chavePrimaria);

		return (ArrecadadorContratoConvenio) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #obterDebitoAutomaticoAtivo(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public DebitoAutomaticoMovimento obterDebitoAutomaticoMovimento(DocumentoCobranca documentoCobranca) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeDebitoAutomaticoMovimento().getSimpleName());
		hql.append(DEBITO_AUTOMATICO_MOVIMENTO);
		hql.append(WHERE);
		hql.append(" debitoAutomaticoMovimento.documentoCobranca.chavePrimaria = :CHAVE_DOCUMENTO ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_DOCUMENTO", documentoCobranca.getChavePrimaria());

		return (DebitoAutomaticoMovimento) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #obterDebitoAutomaticoAtivo(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public DebitoAutomatico obterDebitoAutomaticoAtivo(Long idCliente, Long idContrato) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeDebitoAutomatico().getSimpleName());
		hql.append(DEBITO_AUTOMATICO);
		hql.append(WHERE);
		hql.append(" debitoAutomatico.clienteDebitoAutomatico.cliente.chavePrimaria = :CHAVE_CLIENTE ");
		if (idContrato != null && idContrato > 0) {
			hql.append(" and debitoAutomatico.clienteDebitoAutomatico.contrato.chavePrimaria = :CHAVE_CONTRATO ");
		}
		hql.append(" and debitoAutomatico.dataExclusao is null");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_CLIENTE", idCliente);

		if (idContrato != null && idContrato > 0) {
			query.setParameter("CHAVE_CONTRATO", idContrato);
		}

		return (DebitoAutomatico) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#obterDebitoAutomaticoAtivo(java.lang.Long)
	 */
	@Override
	public DebitoAutomatico obterDebitoAutomaticoAtivo(Long idClienteDebitoAutomatico) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeDebitoAutomatico().getSimpleName());
		hql.append(DEBITO_AUTOMATICO);
		hql.append(" inner join fetch debitoAutomatico.arrecadador arrecadador ");
		hql.append(WHERE);
		hql.append(" debitoAutomatico.clienteDebitoAutomatico.chavePrimaria = :idClienteDebitoAutomatico ");
		hql.append(" and debitoAutomatico.dataExclusao is null");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idClienteDebitoAutomatico", idClienteDebitoAutomatico);

		return (DebitoAutomatico) query.uniqueResult();

	}

	/**
	 * Método responsável por obter uma Entidade
	 * Debito Automatico.
	 *
	 * @return the entidade negocio
	 */
	@Override
	public EntidadeNegocio criarDebitoAutomatico() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(DebitoAutomatico.BEAN_ID_DEBITO_AUTOMATICO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #criarAvisoBancario
	 * ()
	 */
	@Override
	public EntidadeNegocio criarAvisoBancario() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AvisoBancario.BEAN_ID_AVISO_BANCARIO);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * criarDebitoNaoProcessado()
	 */
	@Override
	public DebitoNaoProcessado criarDebitoNaoProcessado() {

		return (DebitoNaoProcessado) ServiceLocator.getInstancia().getBeanPorID(DebitoNaoProcessado.BEAN_ID_DEBITO_NAO_PROCESSADO);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * criarDadosArquivoRetorno()
	 */
	@Override
	public DadosArquivoRetorno criarDadosArquivoRetorno() {

		return (DadosArquivoRetorno) ServiceLocator.getInstancia().getBeanPorID(DadosArquivoRetorno.BEAN_ID_DADOS_ARQUIVO_RETORNO);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * inserirDebitoAutomatico
	 * (br.com.ggas.arrecadacao.DebitoAutomatico)
	 */
	@Override
	public void inserirDebitoAutomatico(DebitoAutomatico debitoAutomatico) {

		debitoAutomatico.setUltimaAlteracao(Calendar.getInstance().getTime());
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(debitoAutomatico);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #obterTipoDocumento
	 * (java.lang.Long)
	 */
	@Override
	public TipoDocumento obterTipoDocumento(Long chavePrimaria) throws NegocioException {

		TipoDocumento tipoDocumento = null;
		if (!tipoDocumentoCache.containsKey(chavePrimaria)) {
			tipoDocumento = (TipoDocumento) obter(chavePrimaria, getClasseEntidadeTipoDocumento());
			tipoDocumentoCache.put(chavePrimaria, tipoDocumento);
		} else {
			tipoDocumento = tipoDocumentoCache.get(chavePrimaria);
		}
		return tipoDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#alterarDebitoAutomatico(br.com.ggas.arrecadacao.DebitoAutomatico)
	 */
	@Override
	public void alterarDebitoAutomatico(DebitoAutomatico debitoAutomatico) {

		debitoAutomatico.setUltimaAlteracao(Calendar.getInstance().getTime());
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(debitoAutomatico);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#listarTipoDocumento()
	 */
	@Override
	public Collection<TipoDocumento> listarTipoDocumento() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeTipoDocumento().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * obterUltimoSequencialAvisoBancario
	 * (br.com.ggas.arrecadacao.Arrecadador,
	 * java.util.Date)
	 */
	@Override
	public Integer obterUltimoSequencialAvisoBancario(Arrecadador arrecadador, Date dataLancamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select max(numeroSequencial) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeAvisoBancario().getSimpleName());
		hql.append(" avisoBancario ");
		hql.append(" where avisoBancario.arrecadador.chavePrimaria = :chaveArrecadador ");
		hql.append(" and avisoBancario.dataLancamento = :dataLancamento ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setDate("dataLancamento", dataLancamento);
		query.setLong(CHAVE_ARRECADADOR_UM, arrecadador.getChavePrimaria());

		return (Integer) query.uniqueResult();
	}

	/**
	 * Gerar movimento cobranca.
	 *
	 * @param listaCobrancaBancariaMovimento lista cobranca bancaria movimento
	 * @return o log do processamento string builder
	 */
	@SuppressWarnings("squid:S1943")
	private StringBuilder gerarMovimentoCobranca(Collection<CobrancaBancariaMovimento> listaCobrancaBancariaMovimento) {

		class ArrecadadorWriter {

			private BufferedWriter bufferedWriter;

			private boolean cabecalhoGerado;

			private int totalLinhas;

			private String caminhoArquivo;
		}

		StringBuilder logProcessamento = new StringBuilder();
		Map<Long, ArrecadadorWriter> mapaArrecadadorWrite = new HashMap<>();
		ArrecadadorWriter arrecadadorWriter = null;
		Arrecadador arrecadador = null;
		DocumentoLayoutRegistro documentoLayoutRegistro = null;
		Map<String, Object> dados = null;
		String linha = null;
		String diretorioArquivosRemessa = null;
		EntidadeConteudo tipoConvenio = null;
		boolean erro = false;
		int numeroSequencialRegistro = 1;
		BigDecimal valorTotalTitulos = BigDecimal.ZERO;
		Map<String, DocumentoLayoutRegistro> mapaDocumentoLayoutRegistro = null;

		try {

			ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
			ControladorDocumentoLayout controladorDocumentoLayout = ServiceLocator.getInstancia().getControladorDocumentoLayout();
			ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
			ControladorArrecadadorConvenio controladorArrecadadorConvenio =
					ServiceLocator.getInstancia().getControladorArrecadadorConvenio();

			// Obter o diretorio dos arquivos de remessa para cobrança registrada
			diretorioArquivosRemessa = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(DIRETORIO_ARQUIVOS_REMESSA);

			logProcessamento.append("Diretório para armazenamento dos arquivos: ");
			logProcessamento.append(diretorioArquivosRemessa);
			logProcessamento.append("\r\n");

			Date dataEnvio = new Date(System.currentTimeMillis());

			for (CobrancaBancariaMovimento cobrancaBancariaMovimento : listaCobrancaBancariaMovimento) {

				ArrecadadorContratoConvenio arrecadadorContratoConvenio = this
						.obterArrecadadorContratoConvenio(cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getChavePrimaria());
				tipoConvenio = (EntidadeConteudo) controladorEntidadeConteudo
						.obter(arrecadadorContratoConvenio.getTipoConvenio().getChavePrimaria());

				ArrecadadorContrato arrecadadorContrato = controladorArrecadadorConvenio
						.obterArrecadadorContrato(arrecadadorContratoConvenio.getArrecadadorContrato().getChavePrimaria());

				arrecadador = this.obterArrecadador(arrecadadorContrato.getArrecadador().getChavePrimaria());
				cobrancaBancariaMovimento.setArrecadadorContratoConvenio(arrecadadorContratoConvenio);

				mapaDocumentoLayoutRegistro = criarMapaDocumentoLayoutRegistro(arrecadadorContratoConvenio.getLeiaute().getRegistros());

				// Para cada registro, processar e gerar arquivo
				arrecadadorWriter = mapaArrecadadorWrite.get(arrecadador.getChavePrimaria());
				Long nsaRemessaNovo = null;

				if (arrecadadorWriter == null) {

					arrecadadorWriter = new ArrecadadorWriter();

					String nomeArrecadador = defineNomeArrecadador(arrecadador);

					String pathDiretorioDestino =
							diretorioArquivosRemessa + SEPARATOR + tipoConvenio.getDescricao() + SEPARATOR + nomeArrecadador + SEPARATOR;
					verificaDiretorio(pathDiretorioDestino);

					nsaRemessaNovo = this.gerarNSAArrecadador(arrecadador.getChavePrimaria(), tipoConvenio.getChavePrimaria());

					DateFormat dataUm = new SimpleDateFormat("ddMMyyyy");
					DateFormat dataDois = new SimpleDateFormat("ddMM");

					String prefArqRemessa = (arrecadador.getBanco().getCodigoBanco().equals(CODIGO_BANCO_DAYCOVAL))
							? SIGLA_DAYCOVAL + dataDois.format(new Date()).toString()
							: dataUm.format(new Date()).toString();

					arrecadadorWriter.caminhoArquivo =
							pathDiretorioDestino + prefArqRemessa + nsaRemessaNovo + "." + SUFIXO_ARQUIVO_REMESSA;

					// verifica se arquivo já existe para não sobrescrever
					File arquivoGerado = Util.getFile(arrecadadorWriter.caminhoArquivo);
					int sequencial = 1;

					while (arquivoGerado.exists()) {
						arrecadadorWriter.caminhoArquivo =
								pathDiretorioDestino + prefArqRemessa + nsaRemessaNovo + "_" + sequencial + "." + SUFIXO_ARQUIVO_REMESSA;
						arquivoGerado = Util.getFile(arrecadadorWriter.caminhoArquivo);
						sequencial++;
					}

					arrecadadorWriter.bufferedWriter =
							new BufferedWriter(new FileWriterWithEncoding(arrecadadorWriter.caminhoArquivo, ENCODING_ISO_8859_1, false));

					arrecadadorWriter.totalLinhas = 0;
					mapaArrecadadorWrite.put(arrecadador.getChavePrimaria(), arrecadadorWriter);
				}

				DocumentoCobranca documentoCobranca = this.getControladorCobranca()
						.obterDocumentoCobranca(cobrancaBancariaMovimento.getDocumentoCobranca().getChavePrimaria());

				// [Gerando o cabecalho]
				// **********************************************************************************
				if (!arrecadadorWriter.cabecalhoGerado) {

					documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("cabecalhoRemessa");

					if (documentoLayoutRegistro != null) {

						// Montar os dados no mapa
						dados = controladorDocumentoLayout.gerarDadosDocumentoRemessaCobranca(arrecadadorWriter.totalLinhas, null,
								cobrancaBancariaMovimento, documentoLayoutRegistro, ServiceLocator.getInstancia());

						dados.put("sequencialArquivo", nsaRemessaNovo);

						arrecadadorWriter.totalLinhas++;
						linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);
						arrecadadorWriter.bufferedWriter.write(linha);
						arrecadadorWriter.bufferedWriter.newLine();
						arrecadadorWriter.cabecalhoGerado = true;
					} else {
						erro = true;
					}

				}

				// [Gerando o detalhe]
				// **********************************************************************************

				// Verificar se deve gerar o detalhe - Erro caso não tenha 'nosso número'.
				erro = verificaSeGerarDetalhe(logProcessamento, erro, cobrancaBancariaMovimento, documentoCobranca);

				// Verificar se deve gerar o detalhe - Erro caso não tenha 'endereço principal'.
				if (documentoCobranca.getCliente() != null && documentoCobranca.getCliente().getEnderecoPrincipal() == null) {
					erro = true;
					logProcessamento.append("O cliente ");
					logProcessamento.append(documentoCobranca.getCliente().getNome());
					logProcessamento.append(" não possui Endereço Principal. \r\n");
					throw new GGASException();
				}

				// - Erro caso não tenha 'ArrecadadorCarteiraCobranca'.
				erro = verificaExistenciaArrecadadorCarteiraCobranca(logProcessamento, erro, cobrancaBancariaMovimento);

				documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("detalheRemessa");

				if (documentoLayoutRegistro != null) {

					// Montar os dados no mapa
					dados = controladorDocumentoLayout.gerarDadosDocumentoRemessaCobranca(arrecadadorWriter.totalLinhas, null,
							cobrancaBancariaMovimento, documentoLayoutRegistro, ServiceLocator.getInstancia());

					arrecadadorWriter.totalLinhas++;

					verificaSePossuiFaturaGeral(dados, documentoCobranca);
					valorTotalTitulos = valorTotalTitulos.add(documentoCobranca.getValorTotal());

					dados.put("sequencialRegistro", arrecadadorWriter.totalLinhas);

					// Gerar a linha
					linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);

					// Gravar linha no arquivo
					arrecadadorWriter.bufferedWriter.write(linha);
					arrecadadorWriter.bufferedWriter.newLine();

					// Atualizando a data de envio
					cobrancaBancariaMovimento.setDataEnvio(dataEnvio);
					cobrancaBancariaMovimento.setUltimaAlteracao(dataEnvio);

					// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
					getHibernateTemplate().getSessionFactory().getCurrentSession().update(cobrancaBancariaMovimento);

				}

				documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("detalheRemessaP");

				if (documentoLayoutRegistro != null) {

					dados = controladorDocumentoLayout.gerarDadosDocumentoRemessaCobranca(arrecadadorWriter.totalLinhas, null,
							cobrancaBancariaMovimento, documentoLayoutRegistro, ServiceLocator.getInstancia());

					dados.put("agencia", arrecadadorContratoConvenio.getContaCredito().getAgencia().getCodigo());
					dados.put("agenciaCobradora", arrecadadorContratoConvenio.getContaCredito().getAgencia().getCodigo());
					dados.put("contaCorrente", arrecadadorContratoConvenio.getCodigoConvenio());
					dados.put("sequenciaRegistro", numeroSequencialRegistro);

					linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);
					numeroSequencialRegistro++;
					arrecadadorWriter.totalLinhas++;

					// Gravar linha no arquivo
					arrecadadorWriter.bufferedWriter.write(linha);
					arrecadadorWriter.bufferedWriter.newLine();

				}

				documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("detalheRemessaQ");

				if (documentoLayoutRegistro != null) {

					dados = controladorDocumentoLayout.gerarDadosDocumentoRemessaCobranca(arrecadadorWriter.totalLinhas, null,
							cobrancaBancariaMovimento, documentoLayoutRegistro, ServiceLocator.getInstancia());
					dados.put("sequenciaRegistro", numeroSequencialRegistro);

					linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);
					numeroSequencialRegistro++;
					arrecadadorWriter.totalLinhas++;

					// Gravar linha no arquivo
					arrecadadorWriter.bufferedWriter.write(linha);
					arrecadadorWriter.bufferedWriter.newLine();

				}

				documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("detalheMulta");

				if (documentoLayoutRegistro != null) {

					// Montar os dados no mapa
					dados = controladorDocumentoLayout.gerarDadosDocumentoRemessaCobranca(arrecadadorWriter.totalLinhas, null, null,
							documentoLayoutRegistro, ServiceLocator.getInstancia());

					arrecadadorWriter.totalLinhas++;

					dados.put("valorTotalCarteira", valorTotalTitulos);
					dados.put("sequencialRegistro", arrecadadorWriter.totalLinhas);
					dados.put("dataMulta", Util.adicionarDiasData(documentoCobranca.getDataVencimento(), 1));

					this.verificacaoDeMulta(documentoCobranca, dados);

					// Gerar a linha
					linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);

					// Gravar linha no arquivo
					arrecadadorWriter.bufferedWriter.write(linha);
					arrecadadorWriter.bufferedWriter.newLine();

					valorTotalTitulos = valorTotalTitulos.add(documentoCobranca.getValorTotal());

				}

			}

			// [Gerar a trilha 9 para todos os arquivos]
			// **********************************************************************************
			for (Iterator<ArrecadadorWriter> iterator = mapaArrecadadorWrite.values().iterator(); iterator.hasNext();) {

				arrecadadorWriter = iterator.next();
				arrecadadorWriter.totalLinhas++;

				documentoLayoutRegistro = mapaDocumentoLayoutRegistro.get("trilhaRemessa");

				if (documentoLayoutRegistro != null) {

					// Montar os dados no mapa
					dados = controladorDocumentoLayout.gerarDadosDocumentoRemessaCobranca(arrecadadorWriter.totalLinhas, valorTotalTitulos,
							null, documentoLayoutRegistro, ServiceLocator.getInstancia());

					// Gerar a linha
					linha = controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);

					// Gravar linha no arquivo
					arrecadadorWriter.bufferedWriter.write(linha);
				}
			}

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		} finally {

			// [Fechando e salvando os arquivos no disco]
			// **********************************************************************************
			logProcessamento.append("Fechando e salvando arquivos. \r\n");
			for (Iterator<ArrecadadorWriter> iterator = mapaArrecadadorWrite.values().iterator(); iterator.hasNext();) {
				arrecadadorWriter = iterator.next();
				try {
					arrecadadorWriter.bufferedWriter.newLine();
					arrecadadorWriter.bufferedWriter.close();
					if (erro) {
						File arquivoGerado = Util.getFile(arrecadadorWriter.caminhoArquivo);

						this.deletarArquivoGerado(arquivoGerado);

					}
				} catch (IOException e) {
					LOG.error(e.getMessage(), e);
				}
			}

			logProcessamento.append("Foram gerados ").append(mapaArrecadadorWrite.size()).append(" arquivos.");

		}

		return logProcessamento;
	}

	private void deletarArquivoGerado(File arquivoGerado){

		if (arquivoGerado.exists()) {
			arquivoGerado.delete();
		}
	}

	private void verificacaoDeMulta(DocumentoCobranca documentoCobranca,Map<String, Object> dados ){


		if (!Util.isNullOrEmpty(documentoCobranca.getItens()) && documentoCobranca.getItens().iterator().next().getFaturaGeral() != null) {


			Contrato contrato = documentoCobranca.getItens().iterator().next().getFaturaGeral().getFaturaAtual().getContratoAtual();
			if (contrato != null && contrato.getPercentualMulta() != null) {
				dados.put("valorMulta", contrato.getPercentualMulta().multiply(new BigDecimal(PERCENTUAL)));
			}
		}


	}

	private void verificaSePossuiFaturaGeral(Map<String, Object> dados, DocumentoCobranca documentoCobranca) {
		if (!Util.isNullOrEmpty(documentoCobranca.getItens()) && documentoCobranca.getItens().iterator().next().getFaturaGeral() != null) {
			Contrato contrato =
							documentoCobranca.getItens().iterator().next().getFaturaGeral().getFaturaAtual().getContratoAtual();
			if (contrato != null && contrato.getPercentualJurosMora() != null) {
				dados.put("jurosMora", contrato.getPercentualJurosMora().multiply(new BigDecimal(PERCENTUAL)));
			}
		}
	}

	private boolean verificaExistenciaArrecadadorCarteiraCobranca(StringBuilder logProcessamento, boolean erroCarteira,
			CobrancaBancariaMovimento cobrancaBancariaMovimento) throws GGASException {
		boolean erro = erroCarteira;
		if (cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getArrecadadorCarteiraCobranca() == null) {
			erro = true;
			logProcessamento.append("Não foi encontrada a carteira de cobrança para o convênio: ");
			logProcessamento.append(cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getChavePrimaria());
			logProcessamento.append("\r\n");
			throw new GGASException();
		}
		return erro;
	}

	private boolean verificaSeGerarDetalhe(StringBuilder logProcessamento, boolean erroNossoNumero,
			CobrancaBancariaMovimento cobrancaBancariaMovimento, DocumentoCobranca documentoCobranca)
			throws GGASException {
		boolean erro = erroNossoNumero;
		if (documentoCobranca == null || documentoCobranca.getNossoNumero() == null) {
			erro = true;
			logProcessamento.append("O documento de cobrança ");
			logProcessamento.append(cobrancaBancariaMovimento.getDocumentoCobranca().getChavePrimaria());
			logProcessamento.append(" não possui Nosso Número. \r\n");
			throw new GGASException();
		}
		return erro;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * verificarExistenciaDebitoAutomaticoAtivo
	 * (br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public boolean verificarExistenciaDebitoAutomaticoAtivo(Long idClienteDebitoAutomatico, String conta, String agencia)
					throws NegocioException {

		boolean existe = false;
		Long quantidade = 0L;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(debitoAutomatico.chavePrimaria) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeDebitoAutomatico().getSimpleName());
		hql.append(DEBITO_AUTOMATICO);
		hql.append(" inner join fetch debitoAutomatico.clienteDebitoAutomatico ");
		hql.append(WHERE);
		hql.append(" debitoAutomatico.clienteDebitoAutomatico.chavePrimaria = :idClienteDebitoAutomatico ");
		hql.append(" and debitoAutomatico.agencia = :agencia ");
		hql.append(" and debitoAutomatico.conta = :conta ");
		hql.append(" and debitoAutomatico.dataExclusao is null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idClienteDebitoAutomatico", idClienteDebitoAutomatico);
		query.setParameter("agencia", agencia);
		query.setParameter("conta", conta);

		quantidade = (Long) query.uniqueResult();

		if (quantidade > 0) {
			existe = true;
		}

		return existe;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #obterArrecadador
	 * (java.lang.Long)
	 */
	@Override
	public AvisoBancario obterAvisoBancario(Long idAvisoBancario) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeAvisoBancario().getSimpleName());
		hql.append(" avisoBancario where avisoBancario.chavePrimaria = :idAvisoBancario ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idAvisoBancario", idAvisoBancario);

		return (AvisoBancario) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #obterArrecadador
	 * (java.lang.Long)
	 */
	@Override
	public FaturaGeral obterFaturaGeral(Long idFaturaGeral) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeFaturaGeral().getSimpleName());
		hql.append(" faturaGeral ");
		hql.append(" inner join fetch faturaGeral.faturaAtual ");
		hql.append(" where faturaGeral.chavePrimaria = :idFaturaGeral ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idFaturaGeral", idFaturaGeral);

		return (FaturaGeral) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #obterArrecadador
	 * (java.lang.Long)
	 */
	@Override
	public DocumentoCobrancaItem obterDocumentoCobrancaItem(Long idDocumentoCobrancaItem) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" documentoCobrancaItem ");
		hql.append(" inner join fetch documentoCobrancaItem.documentoCobranca documentoCobranca ");
		hql.append(" left join fetch documentoCobrancaItem.faturaGeral faturaGeral ");
		hql.append(" left join fetch faturaGeral.faturaAtual faturaAtual ");
		hql.append(" where documentoCobrancaItem.chavePrimaria = :idDocumentoCobrancaItem ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idDocumentoCobrancaItem", idDocumentoCobrancaItem);

		return (DocumentoCobrancaItem) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #
	 * obterDataVencimentoDocumentoCobrancaPeloCreditoDebitoARealizar
	 * (java.lang.Long)
	 */
	@Override
	public Date obterDataVencimentoDocumentoCobrancaPeloCreditoDebitoARealizar(Long idCreditoDebitoARealizar) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append("select max(documentoCobrancaItem.documentoCobranca.dataVencimento)");
		hql.append(FROM);
		hql.append(getClasseEntidadeDocumentoCobrancaItem().getSimpleName());
		hql.append(" documentoCobrancaItem ");
		hql.append(" where documentoCobrancaItem.creditoDebitoARealizar.chavePrimaria = :idCreditoDebitoARealizar");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idCreditoDebitoARealizar", idCreditoDebitoARealizar);

		return (Date) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#obterAnoMesContabil(java.lang.Integer, java.util.Date)
	 */
	@Override
	public Integer obterAnoMesContabil(Integer anoMesContabilParametro, Date dataLancamento) {

		/**
		 * 'anoMes da data do lançamento' caso
		 * seja maior que
		 * PARAMETRO_REFERENCIA_CONTABIL, caso
		 * contrario,
		 * PARAMETRO_REFERENCIA_CONTABIL
		 */
		Integer retorno = anoMesContabilParametro;

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dataLancamento);

		String ano = String.valueOf(calendar.get(Calendar.YEAR));
		String mes = String.valueOf(calendar.get(Calendar.MONTH) + 1);

		Integer anoMesDataLancamento = Integer.valueOf(ano + mes);

		if (anoMesDataLancamento > anoMesContabilParametro) {
			retorno = anoMesDataLancamento;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#obterAgenciaPorChave(java.lang.Long)
	 */
	@Override
	public Agencia obterAgenciaPorChave(Long idAgencia) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeAgencia().getSimpleName());
		hql.append(" agencia ");
		hql.append(" left join fetch agencia.banco ");
		hql.append(WHERE);
		hql.append(" agencia.chavePrimaria = :idAgencia");

		Query query = null;

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idAgencia", idAgencia);

		return (Agencia) query.uniqueResult();
	}

	@Override
	public Collection<Agencia> obterListaAgencia(Map<String, Object> filtro) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeAgencia().getSimpleName());
		hql.append(" agencia ");

		Query query = null;

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#obterClienteDebitoAutomatico(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public ClienteDebitoAutomatico obterClienteDebitoAutomatico(Long identificacaoClienteEmpresa) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeClienteDebitoAutomatico().getSimpleName());
		hql.append(CLIENTE_DEBITO_AUTOMATICO);
		hql.append(" left join fetch clienteDebitoAutomatico.cliente ");
		hql.append(WHERE);
		hql.append(" clienteDebitoAutomatico.chavePrimaria = :identificacaoClienteEmpresa ");

		Query query = null;

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("identificacaoClienteEmpresa", identificacaoClienteEmpresa);

		return (ClienteDebitoAutomatico) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#obterClienteDebitoAutomatico(java.lang.Long, java.lang.Long)
	 */
	@Override
	public ClienteDebitoAutomatico obterClienteDebitoAutomatico(Long identificacaoClienteEmpresa, Long identificacaoContratoPai)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeClienteDebitoAutomatico().getSimpleName());
		hql.append(CLIENTE_DEBITO_AUTOMATICO);
		hql.append(" left join fetch clienteDebitoAutomatico.cliente ");
		hql.append(WHERE);
		hql.append(" clienteDebitoAutomatico.cliente.chavePrimaria = :identificacaoClienteEmpresa ");
		hql.append(" and ");
		hql.append(" clienteDebitoAutomatico.contrato.chavePrimaria = :identificacaoContratoPai ");

		Query query = null;

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("identificacaoClienteEmpresa", identificacaoClienteEmpresa);
		query.setLong("identificacaoContratoPai", identificacaoContratoPai);

		return (ClienteDebitoAutomatico) query.uniqueResult();
	}

	/**
	 * Obter cliente debito automatico por cliente imovel.
	 *
	 * @param identificacaoClienteEmpresa
	 *            the identificacao cliente empresa
	 * @param identificacaoImovel
	 *            the identificacao imovel
	 * @return the cliente debito automatico
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public ClienteDebitoAutomatico obterClienteDebitoAutomaticoPorClienteImovel(Long identificacaoClienteEmpresa, Long identificacaoImovel)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeClienteDebitoAutomatico().getSimpleName());
		hql.append(CLIENTE_DEBITO_AUTOMATICO);
		hql.append(" left join fetch clienteDebitoAutomatico.cliente ");
		hql.append(WHERE);
		hql.append(" clienteDebitoAutomatico.cliente.chavePrimaria = :identificacaoClienteEmpresa ");
		hql.append(" and ");
		hql.append(" clienteDebitoAutomatico.imovel.chavePrimaria = :identificacaoImovel ");

		Query query = null;

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("identificacaoClienteEmpresa", identificacaoClienteEmpresa);
		query.setLong("identificacaoImovel", identificacaoImovel);

		return (ClienteDebitoAutomatico) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.
	 * ControladorArrecadacao#
	 * obterArrecadadorContratoConvenioParaBoletoBancario
	 * ()
	 */
	@Override
	public ArrecadadorContratoConvenio obterArrecadadorContratoConvenioParaBoletoBancario() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeArrecadadorContratoConvenio().getSimpleName()).append(ARRECADADOR_CONTRATO_CONVENIO);
		hql.append(" inner join fetch arrecadadorContratoConvenio.arrecadadorContrato ");
		hql.append(" inner join fetch arrecadadorContratoConvenio.arrecadadorContrato.arrecadador ");
		hql.append(" inner join fetch arrecadadorContratoConvenio.arrecadadorContrato.arrecadador.banco ");
		hql.append(" inner join fetch arrecadadorContratoConvenio.contaConvenio ");
		hql.append(" inner join fetch arrecadadorContratoConvenio.contaConvenio.agencia ");
		hql.append(" left join fetch arrecadadorContratoConvenio.arrecadadorCarteiraCobranca ");
		hql.append(WHERE);
		hql.append(" arrecadadorContratoConvenio.habilitado = true ");
		hql.append(" and arrecadadorContratoConvenio.arrecadadorContrato.dataFimContrato is null ");
		hql.append(" and arrecadadorContratoConvenio.indicadorPadrao = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (ArrecadadorContratoConvenio) query.uniqueResult();
	}

	@Override
	public Long calcularNossoNumero(ArrecadadorContratoConvenio arrecadadorContratoConvenio, DadosAuditoria dadosAuditoria)
					throws NegocioException {

		Long inicioSequencia = arrecadadorContratoConvenio.getSequencialCobrancaInicio();
		Long fimSequencia = arrecadadorContratoConvenio.getSequencialCobrancaFim();

		Long ultimoNumero = arrecadadorContratoConvenio.getSequenciaCobrancaGerado() + 1;

		ArrecadadorCarteiraCobranca arrecadadorCarteiraCobranca = arrecadadorContratoConvenio.getArrecadadorCarteiraCobranca();

		if ((arrecadadorCarteiraCobranca != null) && (arrecadadorCarteiraCobranca.isIndicadorFaixaNossoNumero()) &&
						(ultimoNumero > fimSequencia)) {

			ultimoNumero = inicioSequencia;
		}

		arrecadadorContratoConvenio.setSequenciaCobrancaGerado(ultimoNumero);
		arrecadadorContratoConvenio.setDadosAuditoria(dadosAuditoria);

		try {
			atualizar(arrecadadorContratoConvenio, getClasseEntidadeArrecadadorContratoConvenio());
		} catch (ConcorrenciaException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.ERRO_ENTIDADE_VERSAO_DESATUALIZADA, getClasseEntidadeArrecadadorContratoConvenio()
							.getName());
		}

		return ultimoNumero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #calcularNossoNumeroComValidacao
	 * (br.com.ggas.arrecadacao.
	 * ArrecadadorContratoConvenio,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public synchronized Long calcularNossoNumeroComValidacao(ArrecadadorContratoConvenio arrecadadorContratoConvenio,
			DadosAuditoria dadosAuditoria) throws NegocioException {
		
		ArrecadadorCarteiraCobranca arrecadadorCarteiraCobranca = ServiceLocator.getInstancia()
				.getControladorArrecadadorConvenio()
				.obterCarteiraCobrancaDeConvenio(arrecadadorContratoConvenio.getChavePrimaria());
		
		arrecadadorContratoConvenio.setLeiaute(ServiceLocator.getInstancia().getControladorDocumentoLayout()
				.obterDocumentoLayoutPorConvenio(arrecadadorContratoConvenio.getChavePrimaria()));

		//O "Nosso Numero" sera zerado quando nao for da responsabilidade da empresa gera-lo.
		// Isso sera definido atraves da coluna "ARCA_IN_NOSSO_NUMERO_LIVRE" da tabela "ARRECADADOR_CARTEIRA_COBRANCA"
		if (arrecadadorCarteiraCobranca != null && !arrecadadorCarteiraCobranca.isIndicadorNossoNumeroLivre()) {
			return 0L;
		}

		Long inicioSequencia = arrecadadorContratoConvenio.getSequencialCobrancaInicio();
		Long fimSequencia = arrecadadorContratoConvenio.getSequencialCobrancaFim();

		if(inicioSequencia == null){
			inicioSequencia = 0L;
		}

		if(fimSequencia == null){
			fimSequencia = 0L;
		}

		Long ultimoNumero = null;
		if (arrecadadorContratoConvenio.getSequenciaCobrancaGerado() != null) {
			ultimoNumero = arrecadadorContratoConvenio.getSequenciaCobrancaGerado() + 1;
		} else {
			throw new NegocioException(ERRO_NEGOCIO_CONVENIO_SEM_NOSSO_NUMERO, true);
		}

		if ((arrecadadorCarteiraCobranca != null) && (arrecadadorCarteiraCobranca.isIndicadorFaixaNossoNumero())
						&& (ultimoNumero > fimSequencia)) {
			ultimoNumero = inicioSequencia;
		}

		arrecadadorContratoConvenio.setSequenciaCobrancaGerado(ultimoNumero);
		arrecadadorContratoConvenio.setDadosAuditoria(dadosAuditoria);
	
		try {
			atualizarEmBatch(arrecadadorContratoConvenio, getClasseEntidadeArrecadadorContratoConvenio());
		} catch (ConcorrenciaException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.ERRO_ENTIDADE_VERSAO_DESATUALIZADA, getClasseEntidadeArrecadadorContratoConvenio()
							.getName());
		}

		return ultimoNumero;
	}
	

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #
	 * consultarClienteDebitoAutomatico(java.util.
	 * Map, java.lang.Boolean)
	 */
	@Override
	public ClienteDebitoAutomatico consultarClienteDebitoAutomatico(Map<String, Object> filtro, Boolean isApenasDebitoAutomatico) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" debitoAutomatico.clienteDebitoAutomatico ");
		hql.append(FROM);
		hql.append(getClasseEntidadeDebitoAutomatico().getSimpleName());
		hql.append(DEBITO_AUTOMATICO);
		hql.append(WHERE);
		if (filtro.containsKey(FILTRO_POR_CLIENTE)) {
			hql.append(" debitoAutomatico.clienteDebitoAutomatico.cliente.chavePrimaria = :chaveCliente ");
		}
		if (filtro.containsKey(FILTRO_POR_CONTRATO)) {
			hql.append(" and debitoAutomatico.clienteDebitoAutomatico.contrato.chavePrimaria = :chaveContrato ");
		}
		if (filtro.containsKey(FILTRO_POR_IMOVEL)) {
			hql.append(" and debitoAutomatico.clienteDebitoAutomatico.imovel.chavePrimaria = :chaveImovel ");
		}
		if (isApenasDebitoAutomatico != null && isApenasDebitoAutomatico) {
			hql.append(" and debitoAutomatico.dataExclusao is null");
		}

		Query query = null;

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if (filtro.containsKey(FILTRO_POR_CLIENTE)) {
			query.setLong("chaveCliente", (Long) filtro.get(FILTRO_POR_CLIENTE));
		}
		if (filtro.containsKey(FILTRO_POR_CONTRATO)) {
			query.setLong("chaveContrato", (Long) filtro.get(FILTRO_POR_CONTRATO));
		}
		if (filtro.containsKey(FILTRO_POR_IMOVEL)) {
			query.setLong("chaveImovel", (Long) filtro.get(FILTRO_POR_IMOVEL));
		}

		return (ClienteDebitoAutomatico) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#inserirClienteDebitoAutomatico(br.com.ggas.faturamento.Fatura,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public Long inserirClienteDebitoAutomatico(Fatura fatura, DadosAuditoria dadosAuditoria) throws NegocioException {

		Long identificadorClienteDebito = 0L;
		ClienteDebitoAutomatico clienteDebitoAutomatico = null;
		if (fatura.getContrato() != null) {
			clienteDebitoAutomatico =
							this.obterClienteDebitoAutomatico(fatura.getCliente().getChavePrimaria(), fatura.getContrato()
											.getChavePrimaria());

			if (clienteDebitoAutomatico == null) {
				clienteDebitoAutomatico = (ClienteDebitoAutomatico) this.criarClienteDebitoAutomatico();
				clienteDebitoAutomatico.setDadosAuditoria(dadosAuditoria);
				clienteDebitoAutomatico.setCliente(fatura.getCliente());
				clienteDebitoAutomatico.setContrato(fatura.getContrato());
				clienteDebitoAutomatico.setHabilitado(Boolean.TRUE);
				clienteDebitoAutomatico.setUltimaAlteracao(Calendar.getInstance().getTime());
				clienteDebitoAutomatico.setVersao(0);
				super.inserir(clienteDebitoAutomatico);
			}
			identificadorClienteDebito = clienteDebitoAutomatico.getChavePrimaria();
		}

		return identificadorClienteDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.ControladorArrecadacao
	 * #obterContaBancaria
	 * (java.lang.Long)
	 */
	@Override
	public ContaBancaria obterContaBancaria(Long idContaBancaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeContaBancaria().getSimpleName());
		hql.append(" contaBancaria where contaBancaria.chavePrimaria = :idContaBancaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idContaBancaria", idContaBancaria);

		return (ContaBancaria) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#listarCobrancaBancariaMovimento(br.com.ggas.faturamento.DocumentoCobranca)
	 */
	@Override
	public Collection<CobrancaBancariaMovimento> listarCobrancaBancariaMovimento(DocumentoCobranca documentoCobranca)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeCobrancaBancariaMovimento().getSimpleName());
		hql.append(COB_BANCARIA_MOVIMENTO);
		hql.append(" where cobBancariaMovimento.documentoCobranca.chavePrimaria = :chaveDocumentoCobranca");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveDocumentoCobranca", documentoCobranca.getChavePrimaria());

		return query.list();
	}

	/**
	 * Criar mapa documento layout registro.
	 *
	 * @param registros the registros
	 * @return the map
	 */
	public Map<String, DocumentoLayoutRegistro> criarMapaDocumentoLayoutRegistro(Collection<DocumentoLayoutRegistro> registros) {

		Map<String, DocumentoLayoutRegistro> mapaDocumentoLayoutRegistro = new HashMap<>();

		for (Iterator<DocumentoLayoutRegistro> iterator = registros.iterator(); iterator.hasNext();) {
			DocumentoLayoutRegistro dlr = iterator.next();
			mapaDocumentoLayoutRegistro.put(dlr.getMetodoProcessamento(), dlr);
		}

		return mapaDocumentoLayoutRegistro;

	}

	/**
	 * Criar mapa documento layout registro debito automatico.
	 *
	 * @param registros the registros
	 * @return the map
	 */
	private Map<String, DocumentoLayoutRegistro> criarMapaDocumentoLayoutRegistroDebitoAutomatico(
					Collection<DocumentoLayoutRegistro> registros) {

		Map<String, DocumentoLayoutRegistro> mapaDocumentoLayoutRegistro = new HashMap<>();

		for (Iterator<DocumentoLayoutRegistro> iterator = registros.iterator(); iterator.hasNext();) {
			DocumentoLayoutRegistro dlr = iterator.next();
			mapaDocumentoLayoutRegistro.put(dlr.getIdentificadorRegistro(), dlr);
		}

		return mapaDocumentoLayoutRegistro;

	}

	/**
	 * Obter arrecadador contrato convenio debito automatico.
	 *
	 * @param chaveArrecadador the chave arrecadador
	 * @return the arrecadador contrato convenio
	 * @throws NegocioException the negocio exception
	 */
	public ArrecadadorContratoConvenio obterArrecadadorContratoConvenioDebitoAutomatico(long chaveArrecadador) {

		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();

		Long codigoTipoConvenio =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONVENIO_DEBITO_AUTOMATICO));

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeArrecadadorContratoConvenio().getSimpleName()).append(ARRECADADOR_CONTRATO_CONVENIO);
		hql.append(" inner join fetch arrecadadorContratoConvenio.arrecadadorContrato ");
		hql.append(" inner join fetch arrecadadorContratoConvenio.arrecadadorContrato.arrecadador ");
		hql.append(" inner join fetch arrecadadorContratoConvenio.arrecadadorContrato.arrecadador.banco ");
		hql.append(" inner join fetch arrecadadorContratoConvenio.contaConvenio ");
		hql.append(" inner join fetch arrecadadorContratoConvenio.contaConvenio.agencia ");
		hql.append(" left join fetch arrecadadorContratoConvenio.arrecadadorCarteiraCobranca ");
		hql.append(WHERE);
		hql.append(" arrecadadorContratoConvenio.habilitado = true ");
		hql.append(" and arrecadadorContratoConvenio.arrecadadorContrato.dataFimContrato is null ");
		hql.append(" and arrecadadorContratoConvenio.tipoConvenio.chavePrimaria = :codigoTipoConvenio ");
		hql.append(" and arrecadadorContratoConvenio.arrecadadorContrato.arrecadador.chavePrimaria = :chaveArrecadador ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("codigoTipoConvenio", codigoTipoConvenio);
		query.setLong(CHAVE_ARRECADADOR_UM, chaveArrecadador);

		return (ArrecadadorContratoConvenio) query.uniqueResult();
	}

	/**
	 * Insere um movimento de cobrança bancária (COBRANCA_BANCARIA_MOVIMENTO)
	 * por ocorrência de envio do arquivo de remessa (DLEO_CD) conforme constante 
	 * configurada por banco.
	 * 
	 * @param fatura A fatura que está sendo alterada
	 * @param dadosAuditoria Os dados para auditoria
	 * @param constanteOcorrenciaEnvio A constante de ocorrência de envio
	 * @param documentoCobranca O Documento de Cobrança da Fatura
	 * @return Retorna a chave primária do Movimento de Cobrança Bancária inserido
	 * @throws NegocioException é lançado em caso de erro 
	 */
	public long inserirCobrancaBancariaMovimentoPorOcorrencia(Fatura fatura, DadosAuditoria dadosAuditoria,
			String constanteOcorrenciaEnvio, DocumentoCobranca documentoCobranca) throws NegocioException {
		
		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
		
		ControladorArrecadadorConvenio controladorArrecadacaoConvenio = ServiceLocator.getInstancia()
				.getControladorArrecadadorConvenio();
		
		ArrecadadorContratoConvenio arrecadadorContratoConvenio = null;

		if (fatura.getContratoAtual() != null) {
			arrecadadorContratoConvenio = controladorArrecadacao.obterArrecadadorContratoConvenio(
					fatura.getContratoAtual().getArrecadadorContratoConvenio().getChavePrimaria());
		} else {
			arrecadadorContratoConvenio = controladorArrecadacaoConvenio.obterArrecadadorContratoConvenioPadrao();
		}
		
		DocumentoLayoutEnvioOcorrencia ocorrencia = obterDocumentoLayoutEnvioOcorrencia(constanteOcorrenciaEnvio);
		
		if (ocorrencia != null && documentoCobranca != null && arrecadadorContratoConvenio != null) {
			CobrancaBancariaMovimento movimento = (CobrancaBancariaMovimento) controladorArrecadacao.criarCobrancaBancariaMovimento();
			movimento.setDocumentoLayoutEnvioOcorrencia(ocorrencia);
			movimento.setDocumentoCobranca(documentoCobranca);
			movimento.setDadosAuditoria(dadosAuditoria);
			movimento.setArrecadadorContratoConvenio(arrecadadorContratoConvenio);
			movimento.setHabilitado(Boolean.TRUE);
			movimento.setUltimaAlteracao(Calendar.getInstance().getTime());
			
			return super.inserir(movimento);
		} else {
			throw new NegocioException(ERRO_NEGOCIO_INSERIR_MOVIMENTO_OCORRENCIA_ENVIO, true);
		}
	}
	
	/**
	 * Insere um movimento de cobrança bancário com ocorrência de envio de CANCELAMENTO
	 * 
	 * @param fatura A Fatura que está sendo cancelada
	 * @param dadosAuditoria Os dados para auditoria
	 * @return Retorna a chave primária do Movimento de Cobrança Bancária inserido
	 * @throws NegocioException é lançado em caso de erro
	 */
	public long inserirMovimentoDeCancelamento(Fatura fatura, DadosAuditoria dadosAuditoria) throws NegocioException {
		
		ControladorDocumentoCobranca controladorDocumentoCobranca = (ControladorDocumentoCobranca) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorDocumentoCobranca.BEAN_ID_CONTROLADOR_DOCUMENTO_COBRANCA);
		
		Collection<DocumentoCobranca> listaDocumentoCobranca = controladorDocumentoCobranca
				.consultarDocumentoCobrancaPelaFatura(fatura.getChavePrimaria(), fatura.getTipoDocumento());
		
		if (listaDocumentoCobranca.iterator().hasNext()) {
			DocumentoCobranca documentoCobranca = listaDocumentoCobranca.iterator().next();
			
			return this.inserirCobrancaBancariaMovimentoPorOcorrencia(fatura, dadosAuditoria, 
					Constantes.C_OCORRENCIA_ENVIO_CANCELAMENTO_TITULO_SANTANDER, documentoCobranca);
		} else {
			throw new NegocioException(ERRO_NEGOCIO_INSERIR_MOVIMENTO_OCORRENCIA_ENVIO, true);
		}
	}
	
	/**
	 * Obtém o ocorrência de envio de arquivo de remessa através da constante configurada por banco.
	 * 
	 * @param constante A código da constante
	 * @return Retorna a ocorrência de envio pesquisada
	 * @throws NegocioException é lançado em caso de erro
	 */
	private DocumentoLayoutEnvioOcorrencia obterDocumentoLayoutEnvioOcorrencia(String constante) throws NegocioException {
		
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		
		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
		
		String codigoOcorrenciaCancelamento = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(constante);
		
		if (codigoOcorrenciaCancelamento != null) {
			return controladorArrecadacao.obterDocumentoLayoutEnvioOcorrencia(Long.valueOf(codigoOcorrenciaCancelamento));
		} else {
			return null;
		}
	}
	
	/**
	 * Obtém a ocorrência de envio através da chave primária
	 * 
	 * @param chavePrimaria A chave primária da ocorrência
	 * @return Retorna a ocorrência de envio pesquisada
	 * @throws NegocioException é lançado em caso de erro
	 */
	@Cacheable("documentoLayoutEnvioOcorrencia")
	@Override
	public DocumentoLayoutEnvioOcorrencia obterDocumentoLayoutEnvioOcorrencia(Long chavePrimaria) throws NegocioException {
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeDocumentoLayoutEnvioOcorrencia().getSimpleName());
		hql.append(" documentoLayoutEnvioOcorrencia ");
		hql.append(" where documentoLayoutEnvioOcorrencia.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA, chavePrimaria);

		return (DocumentoLayoutEnvioOcorrencia) query.uniqueResult();
	}
	
	/**
	 * Obtém o movimento de cobrança bancária por chave primária
	 * 
	 * @param chavePrimaria A chave primária do Movimento de Cobrança Bancária
	 * @return Retorna o Movimento Cobrança Bancária pesquisado 
	 * @throws NegocioException é lançado em caso de erro
	 */
	@Override
	public CobrancaBancariaMovimento obterCobrancaBancariaMovimento(Long chavePrimaria) throws NegocioException {
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeCobrancaBancariaMovimento().getSimpleName());
		hql.append(" cobrancaBancariaMovimento ");
		hql.append(" where cobrancaBancariaMovimento.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CHAVE_PRIMARIA, chavePrimaria);

		return (CobrancaBancariaMovimento) query.uniqueResult();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#consultarCobrancaBancariaMovimentoHabilitado(br.com.ggas.faturamento.DocumentoCobranca)
	 */
	@Override
	public Collection<CobrancaBancariaMovimento> consultarCobrancaBancariaMovimentoHabilitado(DocumentoCobranca documentoCobranca)
			throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeCobrancaBancariaMovimento().getSimpleName());
		hql.append(COB_BANCARIA_MOVIMENTO);
		hql.append(" where cobBancariaMovimento.documentoCobranca.chavePrimaria = :chaveDocumentoCobranca");
		hql.append(" and cobBancariaMovimento.habilitado = true ");
		hql.append(" order by cobBancariaMovimento.chavePrimaria desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveDocumentoCobranca", documentoCobranca.getChavePrimaria());
		query.setMaxResults(1);

		return query.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorArrecadacao#obterClienteDebitoAutomatico(java.lang.Long, java.lang.Long)
	 */
	@Override
	public ClienteDebitoAutomatico obterChaveClienteDebitoAutomatico(Long identificacaoClienteEmpresa, Long identificacaoContratoPai)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select clienteDebitoAutomatico.chavePrimaria as chavePrimaria ");
		hql.append(FROM);
		hql.append(getClasseEntidadeClienteDebitoAutomatico().getSimpleName());
		hql.append(CLIENTE_DEBITO_AUTOMATICO);
		hql.append(" left join clienteDebitoAutomatico.cliente ");
		hql.append(WHERE);
		hql.append(" clienteDebitoAutomatico.cliente.chavePrimaria = :identificacaoClienteEmpresa ");
		hql.append(" and ");
		hql.append(" clienteDebitoAutomatico.contrato.chavePrimaria = :identificacaoContratoPai ");

		Query query = null;

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(new GGASTransformer(getClasseEntidadeClienteDebitoAutomatico(),
				super.getSessionFactory().getAllClassMetadata()));
		query.setLong("identificacaoClienteEmpresa", identificacaoClienteEmpresa);
		query.setLong("identificacaoContratoPai", identificacaoContratoPai);
		return (ClienteDebitoAutomatico) query.uniqueResult();
	}
}
