/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 02/12/2013 14:14:11
 @author wcosta
 */

package br.com.ggas.arrecadacao.impl;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.arrecadacao.ControladorTipoDocumento;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
/**
 * Classe responsável pelas ações relacionadas ao tipo de documento
 */
public class ControladorTipoDocumentoImpl extends ControladorNegocioImpl implements ControladorTipoDocumento {

	private static final String DESCRICAO_ABREVIADA = "descricaoAbreviada";
	private static final String INDICADOR_PAGAVEL = "indicadorPagavel";
	private static final String INDICADOR_CODIGO_BARRAS = "indicadorCodigoBarras";
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(TipoDocumento.BEAN_ID_TIPO_DOCUMENTO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(TipoDocumento.BEAN_ID_TIPO_DOCUMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorTipoDocumento#listarTipoDocumento(java.util.Map)
	 */
	@Override
	public Collection<TipoDocumento> listarTipoDocumento(Map<String, Object> filtro) {

		Criteria criteria = getCriteria();

		if (filtro != null) {

			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);

			String descricaoAbreviada = (String) filtro.get(DESCRICAO_ABREVIADA);

			Boolean indicadorPagavel = null;
			if (filtro.get(INDICADOR_PAGAVEL) != null) {
				indicadorPagavel = (Boolean) filtro.get(INDICADOR_PAGAVEL);
			}

			Boolean indicadorCodigoBarras = null;
			if (filtro.get(INDICADOR_CODIGO_BARRAS) != null) {
				indicadorCodigoBarras = (Boolean) filtro.get(INDICADOR_CODIGO_BARRAS);
			}

			Boolean habilitado = null;
			if (filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO) != null) {
				habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			}

			if (descricao != null && !"".equals(descricao.trim())) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}

			if (descricaoAbreviada != null && !"".equals(descricaoAbreviada.trim())) {
				criteria.add(Restrictions.ilike(DESCRICAO_ABREVIADA, Util.formatarTextoConsulta(descricaoAbreviada)));
			}

			if(indicadorPagavel != null) {
				if(indicadorPagavel) {
					criteria.add(Restrictions.eq(INDICADOR_PAGAVEL, "1"));
				} else if(!indicadorPagavel) {
					criteria.add(Restrictions.eq(INDICADOR_PAGAVEL, "0"));
				}
			}

			if(indicadorCodigoBarras != null) {
				if(indicadorCodigoBarras) {
					criteria.add(Restrictions.eq(INDICADOR_CODIGO_BARRAS, "1"));
				} else if(!indicadorCodigoBarras) {
					criteria.add(Restrictions.eq(INDICADOR_CODIGO_BARRAS, "0"));
				}
			}

			if(habilitado != null) {
				if(habilitado) {
					criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
				} else if(!habilitado) {
					criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.FALSE));
				}
			}

		}

		// Paginação em banco dados
		boolean paginacaoPadrao = false;
		if(filtro != null && filtro.containsKey("colecaoPaginada")) {
			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if (paginacaoPadrao) {
			criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
			criteria.addOrder(Order.asc(DESCRICAO_ABREVIADA));
		}

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.ControladorTipoDocumento#validarExistenciaTipoDocumento(br.com.ggas.arrecadacao.TipoDocumento)
	 */
	@Override
	public void validarExistenciaTipoDocumento(TipoDocumento tipoDocumento) throws NegocioException {

		Criteria criteria = getCriteria();

		if (tipoDocumento.getChavePrimaria() > 0) {
			criteria.add(Restrictions.ne("chavePrimaria", tipoDocumento.getChavePrimaria()));
		}

		if (tipoDocumento.getDescricao() != null && !"".equals(tipoDocumento.getDescricao().trim())
						&& tipoDocumento.getDescricaoAbreviada() != null && !"".equals(tipoDocumento.getDescricaoAbreviada().trim())) {

			Criterion restrictionOR = Restrictions.or(
							Restrictions.like(TabelaAuxiliar.ATRIBUTO_DESCRICAO, tipoDocumento.getDescricao(), MatchMode.EXACT).ignoreCase(),
					Restrictions.like(DESCRICAO_ABREVIADA, tipoDocumento.getDescricaoAbreviada(), MatchMode.EXACT).ignoreCase());
			criteria.add(restrictionOR);

		} else {

			if (tipoDocumento.getDescricao() != null && !"".equals(tipoDocumento.getDescricao().trim())) {
				criteria.add(Restrictions.like(TabelaAuxiliar.ATRIBUTO_DESCRICAO, tipoDocumento.getDescricao(), MatchMode.EXACT).ignoreCase());
			}
			if (tipoDocumento.getDescricaoAbreviada() != null && !"".equals(tipoDocumento.getDescricaoAbreviada().trim())) {
				criteria.add(Restrictions.like(DESCRICAO_ABREVIADA, tipoDocumento.getDescricaoAbreviada(), MatchMode.EXACT).ignoreCase());
			}

		}

		Collection<TipoDocumento> lista = criteria.list();
		if (lista != null && !lista.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_TIPO_DOCUMENTO_EXISTENTE, false);
		}

	}

	@Override
	public void remover(Long chavePrimaria, DadosAuditoria dadosAuditoria) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" chavePrimaria = :chavePrimaria ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);
		Collection<EntidadeNegocio> entidades = query.list();
		if ((entidades != null) && (!entidades.isEmpty())) {
			for (EntidadeNegocio entidade : entidades) {
				entidade.setDadosAuditoria(dadosAuditoria);
				this.remover(entidade);
			}
		}

	}
	
	@Override
	public TipoDocumento obterTipoDocumentoDescricao(String descricao) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" descricao = :descricao ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		
		query.setString("descricao", descricao);
		
		return (TipoDocumento) query.uniqueResult();


	}

}

