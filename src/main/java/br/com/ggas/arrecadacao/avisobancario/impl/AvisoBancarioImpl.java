/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe AvisoBancarioImpl representa uma AvisoBancarioImpl no sistema.
 *
 * @since 27/02/2010
 * 
 */

package br.com.ggas.arrecadacao.avisobancario.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorMovimento;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.avisobancario.AvisoBancario;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Implementação da interface AvisoBancario
 */
public class AvisoBancarioImpl extends EntidadeNegocioImpl implements AvisoBancario {

	private static final int FINAL_CAMPO = 2;

	private static final long serialVersionUID = 3990818152805493476L;

	private Arrecadador arrecadador;

	private ArrecadadorMovimento arrecadadorMovimento;

	private ContaBancaria contaBancaria;

	private Integer anoMesContabil;

	private boolean indicadorCreditoDebito;

	private Integer numeroDocumento;

	private Integer numeroSequencial;

	private Date dataLancamento;

	private BigDecimal valorContabilizado;

	private BigDecimal valorRealizado;

	private Date dataPrevista;

	private Date dataRealizada;

	private BigDecimal valorArrecadacaoCalculado;

	private BigDecimal valorDevolucaoCalculado;

	private BigDecimal valorArrecadacaoInformado;

	private BigDecimal valorDevolucaoInformado;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getArrecadador()
	 */
	@Override
	public Arrecadador getArrecadador() {

		return arrecadador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setArrecadador(br.com.ggas.arrecadacao
	 * .Arrecadador)
	 */
	@Override
	public void setArrecadador(Arrecadador arrecadador) {

		this.arrecadador = arrecadador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getArrecadadorMovimento()
	 */
	@Override
	public ArrecadadorMovimento getArrecadadorMovimento() {

		return arrecadadorMovimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setArrecadadorMovimento(br.com
	 * .ggas.arrecadacao.ArrecadadorMovimento)
	 */
	@Override
	public void setArrecadadorMovimento(ArrecadadorMovimento arrecadadorMovimento) {

		this.arrecadadorMovimento = arrecadadorMovimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getContaBancaria()
	 */
	@Override
	public ContaBancaria getContaBancaria() {

		return contaBancaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setContaBancaria(br.com.ggas.
	 * arrecadacao.ContaBancaria)
	 */
	@Override
	public void setContaBancaria(ContaBancaria contaBancaria) {

		this.contaBancaria = contaBancaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getAnoMesContabil()
	 */
	@Override
	public Integer getAnoMesContabil() {

		return anoMesContabil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setAnoMesContabil(java.lang.Integer)
	 */
	@Override
	public void setAnoMesContabil(Integer anoMesContabil) {

		this.anoMesContabil = anoMesContabil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#isIndicadorCreditoDebito()
	 */
	@Override
	public boolean isIndicadorCreditoDebito() {

		return indicadorCreditoDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setIndicadorCreditoDebito(boolean)
	 */
	@Override
	public void setIndicadorCreditoDebito(boolean indicadorCreditoDebito) {

		this.indicadorCreditoDebito = indicadorCreditoDebito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getNumeroDocumento()
	 */
	@Override
	public Integer getNumeroDocumento() {

		return numeroDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setNumeroDocumento(java.lang.Integer)
	 */
	@Override
	public void setNumeroDocumento(Integer numeroDocumento) {

		this.numeroDocumento = numeroDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getNumeroSequencial()
	 */
	@Override
	public Integer getNumeroSequencial() {

		return numeroSequencial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setNumeroSequencial(java.lang.Integer)
	 */
	@Override
	public void setNumeroSequencial(Integer numeroSequencial) {

		this.numeroSequencial = numeroSequencial;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getDataLancamento()
	 */
	@Override
	public Date getDataLancamento() {
		Date data = null;
		if(this.dataLancamento != null){
			data = (Date) this.dataLancamento.clone();
		}		
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setDataLancamento(java.util.Date)
	 */
	@Override
	public void setDataLancamento(Date dataLancamento) {
		if(dataLancamento != null){
			this.dataLancamento = (Date) dataLancamento.clone();
		} else {
			this.dataLancamento = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getValorContabilizado()
	 */
	@Override
	public BigDecimal getValorContabilizado() {

		return valorContabilizado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setValorContabilizado(java.math
	 * .BigDecimal)
	 */
	@Override
	public void setValorContabilizado(BigDecimal valorContabilizado) {

		this.valorContabilizado = valorContabilizado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getValorRealizado()
	 */
	@Override
	public BigDecimal getValorRealizado() {

		return valorRealizado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setValorRealizado(java.math.BigDecimal)
	 */
	@Override
	public void setValorRealizado(BigDecimal valorRealizado) {

		this.valorRealizado = valorRealizado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getDataPrevista()
	 */
	@Override
	public Date getDataPrevista() {
		Date data = null;
		if (this.dataPrevista != null) {
			data = (Date) this.dataPrevista.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setDataPrevista(java.util.Date)
	 */
	@Override
	public void setDataPrevista(Date dataPrevista) {
		if(dataPrevista != null) {
			this.dataPrevista = (Date) dataPrevista.clone();
		} else {
			this.dataPrevista = null;
		}
		
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getDataRealizadaFormatada()
	 */
	@Override
	public String getDataRealizadaFormatada() {

		String dataRealizadaFormatada = "";
		if(this.dataRealizada != null) {
			dataRealizadaFormatada = Util.converterDataParaStringSemHora(this.dataRealizada, Constantes.FORMATO_DATA_BR);
		}
		return dataRealizadaFormatada;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getDataRealizada()
	 */
	@Override
	public Date getDataRealizada() {
		Date data = null;
		if (this.dataRealizada != null) {
			data = (Date) this.dataRealizada.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setDataRealizada(java.util.Date)
	 */
	@Override
	public void setDataRealizada(Date dataRealizada) {
		if(dataRealizada != null){
			this.dataRealizada = (Date) dataRealizada.clone();
		} else {
			this.dataRealizada = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #getValorArrecadacaoCalculado()
	 */
	@Override
	public BigDecimal getValorArrecadacaoCalculado() {

		return valorArrecadacaoCalculado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setValorArrecadacaoCalculado(
	 * java.math.BigDecimal)
	 */
	@Override
	public void setValorArrecadacaoCalculado(BigDecimal valorArrecadacaoCalculado) {

		this.valorArrecadacaoCalculado = valorArrecadacaoCalculado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getValorDevolucaoCalculado()
	 */
	@Override
	public BigDecimal getValorDevolucaoCalculado() {

		return valorDevolucaoCalculado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setValorDevolucaoCalculado(java
	 * .math.BigDecimal)
	 */
	@Override
	public void setValorDevolucaoCalculado(BigDecimal valorDevolucaoCalculado) {

		this.valorDevolucaoCalculado = valorDevolucaoCalculado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #getValorArrecadacaoInformado()
	 */
	@Override
	public BigDecimal getValorArrecadacaoInformado() {

		return valorArrecadacaoInformado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setValorArrecadacaoInformado(
	 * java.math.BigDecimal)
	 */
	@Override
	public void setValorArrecadacaoInformado(BigDecimal valorArrecadacaoInformado) {

		this.valorArrecadacaoInformado = valorArrecadacaoInformado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario#getValorDevolucaoInformado()
	 */
	@Override
	public BigDecimal getValorDevolucaoInformado() {

		return valorDevolucaoInformado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.avisobancario.impl
	 * .AvisoBancario
	 * #setValorDevolucaoInformado(java
	 * .math.BigDecimal)
	 */
	@Override
	public void setValorDevolucaoInformado(BigDecimal valorDevolucaoInformado) {

		this.valorDevolucaoInformado = valorDevolucaoInformado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(arrecadador == null) {
			stringBuilder.append(ARRECADADOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(dataRealizada == null) {
			stringBuilder.append(DATA_RECEBIMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder.toString().length() - FINAL_CAMPO));
		}
		return erros;

	}

}
