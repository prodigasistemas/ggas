/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.avisobancario;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorMovimento;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface AvisoBancario.
 */
public interface AvisoBancario extends EntidadeNegocio {

	/** The bean id aviso bancario. */
	String BEAN_ID_AVISO_BANCARIO = "avisoBancario";

	/** The ano mes contabil. */
	String ANO_MES_CONTABIL = "ANO_MES_CONTABIL";

	/** The arrecadador. */
	String ARRECADADOR = "ARRECADADOR";

	/** The conta bancaria. */
	String CONTA_BANCARIA = "CONTA_BANCARIA";

	/** The data recebimento. */
	String DATA_RECEBIMENTO = "DATA_RECEBIMENTO";

	/**
	 * Gets the arrecadador.
	 *
	 * @return the arrecadador
	 */
	Arrecadador getArrecadador();

	/**
	 * Sets the arrecadador.
	 *
	 * @param arrecadador the new arrecadador
	 */
	void setArrecadador(Arrecadador arrecadador);

	/**
	 * Gets the arrecadador movimento.
	 *
	 * @return the arrecadador movimento
	 */
	ArrecadadorMovimento getArrecadadorMovimento();

	/**
	 * Sets the arrecadador movimento.
	 *
	 * @param arrecadadorMovimento the new arrecadador movimento
	 */
	void setArrecadadorMovimento(ArrecadadorMovimento arrecadadorMovimento);

	/**
	 * Gets the conta bancaria.
	 *
	 * @return the conta bancaria
	 */
	ContaBancaria getContaBancaria();

	/**
	 * Sets the conta bancaria.
	 *
	 * @param contaBancaria the new conta bancaria
	 */
	void setContaBancaria(ContaBancaria contaBancaria);

	/**
	 * Gets the ano mes contabil.
	 *
	 * @return the ano mes contabil
	 */
	Integer getAnoMesContabil();

	/**
	 * Sets the ano mes contabil.
	 *
	 * @param anoMesContabil the new ano mes contabil
	 */
	void setAnoMesContabil(Integer anoMesContabil);

	/**
	 * Checks if is indicador credito debito.
	 *
	 * @return true, if is indicador credito debito
	 */
	boolean isIndicadorCreditoDebito();

	/**
	 * Sets the indicador credito debito.
	 *
	 * @param indicadorCreditoDebito the new indicador credito debito
	 */
	void setIndicadorCreditoDebito(boolean indicadorCreditoDebito);

	/**
	 * Gets the numero documento.
	 *
	 * @return the numero documento
	 */
	Integer getNumeroDocumento();

	/**
	 * Sets the numero documento.
	 *
	 * @param numeroDocumento the new numero documento
	 */
	void setNumeroDocumento(Integer numeroDocumento);

	/**
	 * Gets the numero sequencial.
	 *
	 * @return the numero sequencial
	 */
	Integer getNumeroSequencial();

	/**
	 * Sets the numero sequencial.
	 *
	 * @param numeroSequencial the new numero sequencial
	 */
	void setNumeroSequencial(Integer numeroSequencial);

	/**
	 * Gets the data lancamento.
	 *
	 * @return the data lancamento
	 */
	Date getDataLancamento();

	/**
	 * Sets the data lancamento.
	 *
	 * @param dataLancamento the new data lancamento
	 */
	void setDataLancamento(Date dataLancamento);

	/**
	 * Gets the valor contabilizado.
	 *
	 * @return the valor contabilizado
	 */
	BigDecimal getValorContabilizado();

	/**
	 * Sets the valor contabilizado.
	 *
	 * @param valorContabilizado the new valor contabilizado
	 */
	void setValorContabilizado(BigDecimal valorContabilizado);

	/**
	 * Gets the valor realizado.
	 *
	 * @return the valor realizado
	 */
	BigDecimal getValorRealizado();

	/**
	 * Sets the valor realizado.
	 *
	 * @param valorRealizado the new valor realizado
	 */
	void setValorRealizado(BigDecimal valorRealizado);

	/**
	 * Gets the data prevista.
	 *
	 * @return the data prevista
	 */
	Date getDataPrevista();

	/**
	 * Sets the data prevista.
	 *
	 * @param dataPrevista the new data prevista
	 */
	void setDataPrevista(Date dataPrevista);

	/**
	 * Gets the data realizada formatada.
	 *
	 * @return the data realizada formatada
	 */
	String getDataRealizadaFormatada();

	/**
	 * Gets the data realizada.
	 *
	 * @return the data realizada
	 */
	Date getDataRealizada();

	/**
	 * Sets the data realizada.
	 *
	 * @param dataRealizada the new data realizada
	 */
	void setDataRealizada(Date dataRealizada);

	/**
	 * Gets the valor arrecadacao calculado.
	 *
	 * @return the valor arrecadacao calculado
	 */
	BigDecimal getValorArrecadacaoCalculado();

	/**
	 * Sets the valor arrecadacao calculado.
	 *
	 * @param valorArrecadacaoCalculado the new valor arrecadacao calculado
	 */
	void setValorArrecadacaoCalculado(BigDecimal valorArrecadacaoCalculado);

	/**
	 * Gets the valor devolucao calculado.
	 *
	 * @return the valor devolucao calculado
	 */
	BigDecimal getValorDevolucaoCalculado();

	/**
	 * Sets the valor devolucao calculado.
	 *
	 * @param valorDevolucaoCalculado the new valor devolucao calculado
	 */
	void setValorDevolucaoCalculado(BigDecimal valorDevolucaoCalculado);

	/**
	 * Gets the valor arrecadacao informado.
	 *
	 * @return the valor arrecadacao informado
	 */
	BigDecimal getValorArrecadacaoInformado();

	/**
	 * Sets the valor arrecadacao informado.
	 *
	 * @param valorArrecadacaoInformado the new valor arrecadacao informado
	 */
	void setValorArrecadacaoInformado(BigDecimal valorArrecadacaoInformado);

	/**
	 * Gets the valor devolucao informado.
	 *
	 * @return the valor devolucao informado
	 */
	BigDecimal getValorDevolucaoInformado();

	/**
	 * Sets the valor devolucao informado.
	 *
	 * @param valorDevolucaoInformado the new valor devolucao informado
	 */
	void setValorDevolucaoInformado(BigDecimal valorDevolucaoInformado);

}
