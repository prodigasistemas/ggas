/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.util.Map;

import br.com.ggas.arrecadacao.documento.DocumentoLayout;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface que relaciona o convênio com o contrato e o arrecadador
 */
public interface ArrecadadorContratoConvenio extends EntidadeNegocio {

	String BEAN_ID_ARRECADADOR_CONTRATO_CONVENIO = "arrecadadorContratoConvenio";

	String ARRECADADOR_CONTRATO_CONVENIO = "ARRECADADOR_CONTRATO_CONVENIO";

	String CONTRATO_ARRECADADOR = "Contrato do Arrecadador";

	String TIPO_CONVENIO = "Tipo de Convênio";

	String CODIGO_CONVENIO = "Código do Convênio";
	
	String LEIAUTE = "Leiaute";

	/**
	 * @return the contaConvenio
	 */
	ContaBancaria getContaConvenio();

	/**
	 * @param contaConvenio
	 *            the contaConvenio to set
	 */
	void setContaConvenio(ContaBancaria contaConvenio);

	/**
	 * @return the contaCredito
	 */
	ContaBancaria getContaCredito();

	/**
	 * @param contaCredito
	 *            the contaCredito to set
	 */
	void setContaCredito(ContaBancaria contaCredito);

	/**
	 * @return the arrecadadorContrato
	 */
	ArrecadadorContrato getArrecadadorContrato();

	/**
	 * @param arrecadadorContrato
	 *            the arrecadadorContrato to set
	 */
	void setArrecadadorContrato(ArrecadadorContrato arrecadadorContrato);

	/**
	 * @return the tipoConvenio
	 */
	EntidadeConteudo getTipoConvenio();

	/**
	 * @param tipoConvenio
	 *            the tipoConvenio to set
	 */
	void setTipoConvenio(EntidadeConteudo tipoConvenio);

	/**
	 * @return the codigoConvenio
	 */
	String getCodigoConvenio();

	/**
	 * @param codigoConvenio
	 *            the codigoConvenio to set
	 */
	void setCodigoConvenio(String codigoConvenio);

	/**
	 * @return the numeroDiasFloat
	 */
	Integer getNumeroDiasFloat();

	/**
	 * @param numeroDiasFloat
	 *            the numeroDiasFloat to set
	 */
	void setNumeroDiasFloat(Integer numeroDiasFloat);

	/**
	 * @return the nsaRemessa
	 */
	Long getNsaRemessa();

	/**
	 * @param nsaRemessa
	 *            the nsaRemessa to set
	 */
	void setNsaRemessa(Long nsaRemessa);

	/**
	 * @return the nsaRetorno
	 */
	Long getNsaRetorno();

	/**
	 * @param nsaRetorno
	 *            the nsaRetorno to set
	 */
	void setNsaRetorno(Long nsaRetorno);

	/**
	 * @return the sequencialCobrancaInicio
	 */
	Long getSequencialCobrancaInicio();

	/**
	 * @param sequencialCobrancaInicio
	 *            the sequencialCobrancaInicio to
	 *            set
	 */
	void setSequencialCobrancaInicio(Long sequencialCobrancaInicio);

	/**
	 * @return the sequencialCobrancaFim
	 */
	Long getSequencialCobrancaFim();

	/**
	 * @param sequencialCobrancaFim
	 *            the sequencialCobrancaFim to set
	 */
	void setSequencialCobrancaFim(Long sequencialCobrancaFim);

	/**
	 * @return the tentativasReenvio
	 */
	Integer getTentativasReenvio();

	/**
	 * @param tentativasReenvio
	 *            the tentativasReenvio to set
	 */
	void setTentativasReenvio(Integer tentativasReenvio);

	/**
	 * @return the arrecadadorCarteiraCobranca
	 */
	ArrecadadorCarteiraCobranca getArrecadadorCarteiraCobranca();

	/**
	 * @param arrecadadorCarteiraCobranca
	 *            the arrecadadorCarteiraCobranca
	 *            to set
	 */
	void setArrecadadorCarteiraCobranca(ArrecadadorCarteiraCobranca arrecadadorCarteiraCobranca);

	/**
	 * @return the sequenciaCobrancaGerado
	 */
	Long getSequenciaCobrancaGerado();

	/**
	 * @param sequenciaCobrancaGerado
	 *            the sequenciaCobrancaGerado to
	 *            set
	 */
	void setSequenciaCobrancaGerado(Long sequenciaCobrancaGerado);

	/**
	 * Checks if is indicador padrao.
	 * 
	 * @return the indicadorPadrao
	 */
	Boolean isIndicadorPadrao();

	/**
	 * @return
	 *         "Sim" if indicadorPadrao is true and
	 *         "Não" otherwise
	 */
	String getIndicadorPadraoString();

	/**
	 * @param indicadorResponsavel
	 *            the indicadorResponsavel to
	 *            set
	 */
	void setIndicadorPadrao(Boolean indicadorResponsavel);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.EntidadeNegocio#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados();

	/**
	 * @param leiaute o novo leiaute
	 */
	void setLeiaute(DocumentoLayout leiaute);

	/**
	 * @return o leiaute
	 */
	DocumentoLayout getLeiaute();
	
	/**
	 * Metodo que valida a geracao do proximo nosso numero
	 * caso nao seja possivel uma excecao com a mesagem apropriada sera lancada
	 * 
	 * @throws NegocioException excecao lancada caso nao seja 
	 * 			possivel gerar proximo nosso numero 
	 */
	void validacaoGeracaoNossoNumero() throws NegocioException;

	void setArquivoLayoutFatura(String arquivo);

	String getArquivoLayoutFatura();
	
}
