/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.recebimento.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.arrecadacao.ArrecadadorMovimentoItem;
import br.com.ggas.arrecadacao.avisobancario.AvisoBancario;
import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.arrecadacao.recebimento.RecebimentoSituacao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela representação de um recebimento
 */
public class RecebimentoImpl extends EntidadeNegocioImpl implements Recebimento {

	private static final int LIMITE_CAMPO = 2;

	private static final int NUMEROS_DECIMAIS = 2;

	private static final long serialVersionUID = -4531616389748488175L;

	private RecebimentoSituacao recebimentoSituacao;

	private Cliente cliente;

	private PontoConsumo pontoConsumo;

	private ArrecadadorMovimentoItem arrecadadorMovimentoItem;

	private AvisoBancario avisoBancario;

	private FaturaGeral faturaGeral;

	private DocumentoCobrancaItem documentoCobrancaItem;

	private EntidadeConteudo formaArrecadacao;

	private Localidade localidade;

	private Integer anoMesContabil;

	private BigDecimal valorRecebimento;

	private Date dataRecebimento;

	private boolean baixado;

	private Devolucao devolucao;

	private BigDecimal valorExcedente;

	private String observacao;

	private BigDecimal valorPrincipal;

	private BigDecimal valorJurosMulta;

	private BigDecimal valorDescontos;

	private BigDecimal valorAbatimento;

	private BigDecimal valorCredito;

	private BigDecimal valorIOF;

	private BigDecimal valorCobranca;

	private Integer sequencial;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #getValorPrincipal()
	 */
	@Override
	public BigDecimal getValorPrincipal() {

		return valorPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #setValorPrincipal(java.math.BigDecimal)
	 */
	@Override
	public void setValorPrincipal(BigDecimal valorPrincipal) {

		this.valorPrincipal = valorPrincipal;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #getValorJurosMulta()
	 */
	@Override
	public BigDecimal getValorJurosMulta() {

		return valorJurosMulta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #setValorJurosMulta(java.math.BigDecimal)
	 */
	@Override
	public void setValorJurosMulta(BigDecimal valorJurosMulta) {

		this.valorJurosMulta = valorJurosMulta;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #getValorDescontos()
	 */
	@Override
	public BigDecimal getValorDescontos() {

		return valorDescontos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #setValorDescontos(java.math.BigDecimal)
	 */
	@Override
	public void setValorDescontos(BigDecimal valorDescontos) {

		this.valorDescontos = valorDescontos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #getValorAbatimento()
	 */
	@Override
	public BigDecimal getValorAbatimento() {

		return valorAbatimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #setValorAbatimento(java.math.BigDecimal)
	 */
	@Override
	public void setValorAbatimento(BigDecimal valorAbatimento) {

		this.valorAbatimento = valorAbatimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #getValorCredito()
	 */
	@Override
	public BigDecimal getValorCredito() {

		return valorCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #setValorCredito(java.math.BigDecimal)
	 */
	@Override
	public void setValorCredito(BigDecimal valorCredito) {

		this.valorCredito = valorCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #getValorIOF()
	 */
	@Override
	public BigDecimal getValorIOF() {

		return valorIOF;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #setValorIOF(java.math.BigDecimal)
	 */
	@Override
	public void setValorIOF(BigDecimal valorIOF) {

		this.valorIOF = valorIOF;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #getValorCobranca()
	 */
	@Override
	public BigDecimal getValorCobranca() {

		return valorCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #setValorCobranca(java.math.BigDecimal)
	 */
	@Override
	public void setValorCobranca(BigDecimal valorCobranca) {

		this.valorCobranca = valorCobranca;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #isBaixado()
	 */
	@Override
	public boolean isBaixado() {

		return baixado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #setBaixado(java.lang.Boolean)
	 */
	@Override
	public void setBaixado(boolean baixado) {

		this.baixado = baixado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getRecebimentoSituacao()
	 */
	@Override
	public RecebimentoSituacao getRecebimentoSituacao() {

		return recebimentoSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setRecebimentoSituacao(br.com.ggas
	 * .arrecadacao
	 * .recebimento.RecebimentoSituacao)
	 */
	@Override
	public void setRecebimentoSituacao(RecebimentoSituacao recebimentoSituacao) {

		this.recebimentoSituacao = recebimentoSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getCliente()
	 */
	@Override
	public Cliente getCliente() {

		return cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setCliente(br.com.ggas.cadastro
	 * .cliente.Cliente)
	 */
	@Override
	public void setCliente(Cliente cliente) {

		this.cliente = cliente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setPontoConsumo(br.com.ggas.cadastro
	 * .imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getArrecadadorMovimentoItem()
	 */
	@Override
	public ArrecadadorMovimentoItem getArrecadadorMovimentoItem() {

		return arrecadadorMovimentoItem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setArrecadadorMovimentoItem(br.
	 * com.ggas.arrecadacao
	 * .ArrecadadorMovimentoItem)
	 */
	@Override
	public void setArrecadadorMovimentoItem(ArrecadadorMovimentoItem arrecadadorMovimentoItem) {

		this.arrecadadorMovimentoItem = arrecadadorMovimentoItem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getAvisoBancario()
	 */
	@Override
	public AvisoBancario getAvisoBancario() {

		return avisoBancario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setAvisoBancario(br.com.ggas.arrecadacao
	 * .avisobancario.AvisoBancario)
	 */
	@Override
	public void setAvisoBancario(AvisoBancario avisoBancario) {

		this.avisoBancario = avisoBancario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #getFaturaGeral()
	 */
	@Override
	public FaturaGeral getFaturaGeral() {

		return faturaGeral;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #
	 * setFatura(br.com.ggas.faturamento.FaturaGeral
	 * )
	 */
	@Override
	public void setFaturaGeral(FaturaGeral faturaGeral) {

		this.faturaGeral = faturaGeral;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getDocumentoCobrancaItem()
	 */
	@Override
	public DocumentoCobrancaItem getDocumentoCobrancaItem() {

		return documentoCobrancaItem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setDocumentoCobrancaItem(br.com
	 * .ggas.faturamento.DocumentoCobrancaItem)
	 */
	@Override
	public void setDocumentoCobrancaItem(DocumentoCobrancaItem documentoCobrancaItem) {

		this.documentoCobrancaItem = documentoCobrancaItem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getFormaArrecadacao()
	 */
	@Override
	public EntidadeConteudo getFormaArrecadacao() {

		return formaArrecadacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setFormaArrecadacao(br.com.ggas
	 * .geral.EntidadeConteudo)
	 */
	@Override
	public void setFormaArrecadacao(EntidadeConteudo formaArrecadacao) {

		this.formaArrecadacao = formaArrecadacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getLocalidade()
	 */
	@Override
	public Localidade getLocalidade() {

		return localidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setLocalidade(br.com.ggas.cadastro
	 * .localidade.Localidade)
	 */
	@Override
	public void setLocalidade(Localidade localidade) {

		this.localidade = localidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getAnoMesContabil()
	 */
	@Override
	public Integer getAnoMesContabil() {

		return anoMesContabil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setAnoMesContabil(java.lang.Integer)
	 */
	@Override
	public void setAnoMesContabil(Integer anoMesContabil) {

		this.anoMesContabil = anoMesContabil;
	}

	@Override
	public String getAnoMesContabilFormatado() {

		return Util.formatarAnoMes(this.anoMesContabil);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getValorRecebimento()
	 */
	@Override
	public BigDecimal getValorRecebimento() {

		return valorRecebimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setValorRecebimento(java.math.BigDecimal)
	 */
	@Override
	public void setValorRecebimento(BigDecimal valorRecebimento) {

		this.valorRecebimento = valorRecebimento;
	}

	@Override
	public String getValorRecebimentoFormatado() {

		return Util.converterCampoValorDecimalParaString("", this.valorRecebimento, Constantes.LOCALE_PADRAO, NUMEROS_DECIMAIS);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getDataRecebimento()
	 */
	@Override
	public Date getDataRecebimento() {
		Date dataRecebimentoTmp = null;
		if(dataRecebimento != null){
			dataRecebimentoTmp = (Date) dataRecebimento.clone();
		}
		return dataRecebimentoTmp;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setDataRecebimento(java.util.Date)
	 */
	@Override
	public void setDataRecebimento(Date dataRecebimento) {
		if(dataRecebimento != null){
			this.dataRecebimento = (Date) dataRecebimento.clone();
		} else {
			this.dataRecebimento = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #getDataRecebimentoFormatada()
	 */
	@Override
	public String getDataRecebimentoFormatada() {

		return Util.converterDataParaStringSemHora(this.dataRecebimento, Constantes.FORMATO_DATA_BR);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #getDevolucao()
	 */
	@Override
	public Devolucao getDevolucao() {

		return devolucao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.Recebimento
	 * #
	 * setDevolucao(br.com.ggas.arrecadacao.devolucao
	 * .Devolucao)
	 */
	@Override
	public void setDevolucao(Devolucao devolucao) {

		this.devolucao = devolucao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getValorExcedente()
	 */
	@Override
	public BigDecimal getValorExcedente() {

		return valorExcedente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setValorExcedente(java.math.BigDecimal)
	 */
	@Override
	public void setValorExcedente(BigDecimal valorExcedente) {

		this.valorExcedente = valorExcedente;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento#getObservacao()
	 */
	@Override
	public String getObservacao() {

		return observacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.impl
	 * .Recebimento
	 * #setObservacao(java.lang.String)
	 */
	@Override
	public void setObservacao(String observacao) {

		this.observacao = observacao;
	}

	/**
	 * @return the sequencial
	 */
	@Override
	public Integer getSequencial() {

		return sequencial;
	}

	/**
	 * @param sequencial
	 *            the sequencial to set
	 */
	@Override
	public void setSequencial(Integer sequencial) {

		this.sequencial = sequencial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(avisoBancario == null) {
			stringBuilder.append(ARRECADADOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(formaArrecadacao == null) {
			stringBuilder.append(FORMA_ARRECADACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(dataRecebimento == null) {
			stringBuilder.append(DATA_RECEBIMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(valorRecebimento == null || valorRecebimento.compareTo(BigDecimal.ZERO) == 0) {
			stringBuilder.append(VALOR_RECEBIMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}
		return erros;
	}

}
