/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.recebimento;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.arrecadacao.recebimento.impl.ResultadoClassificacao;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.web.relatorio.posicaocontasreceber.SubRelatorioPosicaoContasReceberVO;
import br.com.ggas.web.relatorio.tituloemabertopordatavencimento.SubRelatorioTitulosAbertoPorDataVencimentoVO;

/**
 * Interface responsável pelos métodos relacionados aos recebimentos de valores
 *
 */
public interface ControladorRecebimento extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_RECEBIMENTO = "controladorRecebimento";

	String RELATORIO_RECIBO_QUITACAO = "relatorioReciboQuitacao.jasper";

	String CODIGO_ACAO_PADRAO_RECEBIMENTO_A_MENOR = "CODIGO_ACAO_PADRAO_RECEBIMENTO_A_MENOR";

	String GERA_CREDITO_POR_RECEBIMENTO_MAIOR_DUPLICIDADE = "GERA_CREDITO_POR_RECEBIMENTO_MAIOR_DUPLICIDADE";

	String VALOR_MIN_DOCUMENTO_COBRANCA_POR_RECEBIMENTO_MENOR = "VALOR_MIN_DOCUMENTO_COBRANCA_POR_RECEBIMENTO_MENOR";

	String GERA_DOCUMENTO_COBRANCA_POR_RECEBIMENTO_MENOR = "GERA_DOCUMENTO_COBRANCA_POR_RECEBIMENTO_MENOR";

	String ERRO_NEGOCIO_RECEBIMENTO_SEM_FATURA = "ERRO_NEGOCIO_RECEBIMENTO_SEM_FATURA";

	String ERRO_RECEBIMENTO_NAO_BAIXADO = "ERRO_RECEBIMENTO_NAO_BAIXADO";

	String ERRO_CAMPOS_OBRIGATORIOS_PESQUISA_DATA_RECEBIMENTOS = "ERRO_CAMPOS_OBRIGATORIOS_PESQUISA_DATA_RECEBIMENTOS";

	String ERRO_PESQUISA_DATA_FINAL_MAIOR_DOCUMENTOS = "ERRO_PESQUISA_DATA_FINAL_MAIOR_DOCUMENTOS";

	String ERRO_PESQUISA_DATA_FINAL_MAIOR_RECEBIMENTOS = "ERRO_PESQUISA_DATA_FINAL_MAIOR_RECEBIMENTOS";

	String ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA_CONJUNTAMENTE = "ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA_CONJUNTAMENTE";

	String ERRO_PESQUISA_REFERENCIA_INICIAL_MAIOR = "ERRO_PESQUISA_REFERENCIA_INICIAL_MAIOR";

	String ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL = "ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL";

	String ERRO_NEGOCIO_VALOR_RECEBIMENTO_ZERO = "ERRO_NEGOCIO_VALOR_RECEBIMENTO_ZERO";

	String ERRO_NEGOCIO_CONTRATO_ENCERRADO_VALOR_DIFERENTE = "ERRO_NEGOCIO_CONTRATO_ENCERRADO_VALOR_DIFERENTE";

	String ERRO_NEGOCIO_DATA_RECEBIMENTO_INICIO_INVALIDA = "ERRO_NEGOCIO_DATA_RECEBIMENTO_INICIO_INVALIDA";

	/**
	 * Metodo responsável por listar as Entidades
	 * 'RecebimentoSituacao' existentes ativas no
	 * sistema.
	 * 
	 * @return Coleção de RecebimentoSituacao do
	 *         sistema
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<RecebimentoSituacao> listarRecebimentoSituacaoExistente() throws NegocioException;

	/**
	 * Método responsável por obter uma situação
	 * de recebimento.
	 * 
	 * @param chavePrimaria
	 *            A chavePrimaria
	 * @return Uma situação
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	RecebimentoSituacao obterRecebimentoSituacao(Long chavePrimaria) throws NegocioException;

	/**
	 * Metodo responsável por listar as Entidades
	 * 'Recebimento' filtradas pela tela.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return Coleção de Recebimento do sistema
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Recebimento> consultarRecebimento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Metodo responsável por inserir um
	 * recebimento.
	 *
	 * @param recebimento
	 *            the recebimento
	 * @return void
	 * @throws GGASException the GGAS exception
	 */
	void inserirRecebimento(Recebimento recebimento) throws GGASException;

	/**
	 * Metodo responsável por inserir um
	 * recebimento.
	 * 
	 * @param recebimento
	 *            the recebimento
	 * @param chavesPrimariasFaturas
	 *            the chaves primarias faturas
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return Chaves primárias dos recebimento
	 *         inseridos.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Long[] inserirRecebimento(Recebimento recebimento, Long[] chavesPrimariasFaturas, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Metodo responsável por restornar uma lista
	 * de recebimentos.
	 * 
	 * @param chavesPrimarias {@link Long}
	 *            the chaves primarias
	 * @param dadosAuditoria {@link DadosAuditoria}
	 *            the dados auditoria
	 * @throws NegocioException {@link NegocioException}
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException {@link ConcorrenciaException}
	 * @throws GGASException {@link GGASException}
	 */
	void estornarRecebimento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Metodo responsável por restornar uma lista
	 * de tipos de convênio.
	 * 
	 * @param chaveArrecadador
	 *            A chave do arrecadador.
	 * @return lista de tipos de convênio
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeConteudo> obterTiposConvenioPorArrecadador(Long chaveArrecadador) throws NegocioException;

	/**
	 * Validar dados recebimento.
	 * 
	 * @param idArrecadador
	 *            the id arrecadador
	 * @param idFormaArrecadacao
	 *            the id forma arrecadacao
	 * @param dataRecebimentoString
	 *            the data recebimento string
	 * @param valorRecebimentoString
	 *            the valor recebimento string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosRecebimento(Long idArrecadador, Long idFormaArrecadacao, String dataRecebimentoString, String valorRecebimentoString)
					throws NegocioException;

	/**
	 * Método responsável por classificar um
	 * recebimento.
	 * 
	 * @param recebimento
	 *            O recebimento
	 * @param baixaForcada
	 *            baixaForcada
	 * @param indicadorCorrecaoMonetaria
	 *            indicador de correcao monetaria.
	 * @return o resultado da classificacao
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	ResultadoClassificacao classificarRecebimento(Recebimento recebimento, boolean baixaForcada,
					boolean indicadorCorrecaoMonetaria) throws GGASException;

	/**
	 * Método responsável por classificar um
	 * recebimento.
	 * 
	 * @param recebimento
	 *            O recebimento
	 * @param baixaForcada
	 *            baixaForcada
	 * @return o resultado da classificacao
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	ResultadoClassificacao classificarRecebimento(Recebimento recebimento, boolean baixaForcada) throws GGASException;

	/**
	 * Metodo responsável por atualizar um
	 * recebimento.
	 * 
	 * @param recebimento
	 *            the recebimento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void atualizarRecebimento(Recebimento recebimento) throws GGASException;

	/**
	 * Metodo responsável por atualizar um
	 * recebimento.
	 * 
	 * @param recebimento
	 *            the recebimento
	 * @param chavesPrimariasFaturas
	 *            the chaves primarias faturas
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void atualizarRecebimentos(Recebimento recebimento, Long[] chavesPrimariasFaturas) throws GGASException;

	/**
	 * Método responsável por gerar o relatório de
	 * recibo de quitação para cada fatura.
	 * 
	 * @param listaRecebimentos
	 *            Lista de recebimentos.
	 * @param chavesFaturas
	 *            Chaves primárias das faturas.
	 * @return Um array bytes do relatório.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	byte[] gerarRelatorioReciboQuitacao(Collection<Recebimento> listaRecebimentos, Long[] chavesFaturas) throws NegocioException;

	/**
	 * Método responsavel por verificar os
	 * parametros obrigatorios passados na
	 * pesquisa de recebimentos.
	 * 
	 * @param filtro {@link Map}
	 *            O filtro da tela de pesquisa
	 * @throws NegocioException {@link NegocioException}
	 *             the negocio exception
	 * @throws FormatoInvalidoException {@link FormatoInvalidoException}
	 *             the formato invalido exception
	 * @throws GGASException {@link GGASException}
	 */
	void validarFiltroPesquisaRecebimento(Map<String, Object> filtro) throws GGASException;

	/**
	 * Método que lista os recebimentos da fatura
	 * passada por parâmetro.
	 * 
	 * @param faturaGeral
	 *            the fatura geral
	 * @return lista de recebimentos
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Recebimento> consultarRecebimento(FaturaGeral faturaGeral) throws NegocioException;

	/**
	 * Método responsável por retornar o somatorio
	 * dos recebimentos feitos para um credito
	 * debito a realizar.
	 * 
	 * @param idCreditoDebitoARealizar
	 *            the id credito debito a realizar
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	BigDecimal obterValorRecebimentoPelaCreditoDebitoARealizar(Long idCreditoDebitoARealizar) throws NegocioException;

	/**
	 * Método responsável por retornar a data de
	 * pagamento de recebimento de um credito
	 * debito a realizar pelo documento de
	 * cobranca.
	 * 
	 * @param idCreditoDebitoARealizar
	 *            the id credito debito a realizar
	 * @return the date
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Date obterDataPagamentoPeloDocumentoCobranca(Long idCreditoDebitoARealizar) throws NegocioException;

	/**
	 * Método responsável por retornar a data de
	 * pagamento de recebimento de um credito
	 * debito a realizar pela fatura.
	 * 
	 * @param idCreditoDebitoARealizar
	 *            the id credito debito a realizar
	 * @return the date
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Date obterDataPagamentoPelaFatura(Long idCreditoDebitoARealizar) throws NegocioException;

	/**
	 * Método responsável por retornar a data de recebimento mais recente de um Recebimento que pertence a uma FaturaGeral.
	 * 
	 * @param faturasGerais - {@link Collection}
	 * @return Data de Recebimento por código da Fatura Geral - {@link Map}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Map<Long, Collection<Date>> consultarDataPagamentoRecebimentoPorCodigoFaturaGeral(Collection<FaturaGeral> faturasGerais)
			throws NegocioException;
	
	/**
	 * Método responsável por consultar
	 * Recebimento que pertence a uma FaturaGeral
	 * por situação.
	 * 
	 * @param faturaGeral
	 *            the fatura geral
	 * @param codigoClassificado
	 *            the codigo classificado
	 * @param codigoValorNaoConfere
	 *            the codigo valor nao confere
	 * @return coleção de recebimentos
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<Recebimento> consultarRecebimentoPorSituacao(FaturaGeral faturaGeral, String codigoClassificado, String codigoValorNaoConfere)
					throws NegocioException;

	/**
	 * Método responsável por consultar
	 * Recebimento que pertence a uma FaturaGeral
	 * por situação.
	 * 
	 * @param faturaGeral
	 *            the fatura geral
	 * @param conciliacao
	 *            the conciliacao
	 * @param chavesFaturasConciliadas
	 *            the chaves faturas conciliadas
	 * @return coleção de recebimentos
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	boolean recebimentosConciliacoesNoPrazo(FaturaGeral faturaGeral, boolean conciliacao, Collection<Long> chavesFaturasConciliadas)
					throws NegocioException;

	/**
	 * Prepara os dados necessários e chama a
	 * rotina de registro dos lançamentos
	 * contábeis sintéticos e analíticos.
	 * 
	 * @param recebimento {@link Recebimento}
	 *            Objeto de referência do evento
	 *            comercial.
	 * @param isPosterior {@link boolean}
	 *            the is posterior
	 * @param segmentoEstorno {@link Segmento}
	 *            the segmento estorno
	 * @throws NegocioException {@link NegocioException}
	 *             the negocio exception
	 * @throws ConcorrenciaException {@link ConcorrenciaException}
	 *             the concorrencia exception
	 * @throws GGASException  {@link GGASException}
	 */
	void registrarLancamentoContabil(Recebimento recebimento, boolean isPosterior, Segmento segmentoEstorno) throws GGASException;

	/**
	 * Consultar qtd dias atraso recebimento.
	 * 
	 * @param idFaturaGeral
	 *            the id fatura geral
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SubRelatorioTitulosAbertoPorDataVencimentoVO> consultarQtdDiasAtrasoRecebimento(Long idFaturaGeral) throws NegocioException;

	/**
	 * Consultar qtd dias atraso recebimento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param classe
	 *            the classe
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SubRelatorioPosicaoContasReceberVO> consultarQtdDiasAtrasoRecebimento(Map<String, Object> filtro, String classe)
					throws NegocioException;

	/**
	 * Obter recebimentos.
	 * 
	 * @param campoDiasAtraso
	 *            the campo dias atraso
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @param idsFaturaGeral
	 *            the ids fatura geral
	 * @return the list
	 */
	List<Recebimento> obterRecebimentos(Integer campoDiasAtraso, Date campoDataReferencia, Long[] idsFaturaGeral);

	/**
	 * Registrar lancamento contabil despesa.
	 *
	 * @param recebimento o recebimento
	 * @param fatura a fatura
	 * @param valor o valor
	 * @throws GGASException caso ocorra algum erro
	 */
	void registrarLancamentoContabilDespesa(Recebimento recebimento, Fatura fatura, BigDecimal valor) throws GGASException;

}
