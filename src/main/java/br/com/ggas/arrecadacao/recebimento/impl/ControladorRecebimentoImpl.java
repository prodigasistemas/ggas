/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.recebimento.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.arrecadacao.recebimento.RecebimentoSituacao;
import br.com.ggas.arrecadacao.recebimento.RegistradorLancamentoContabil;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.EventoComercialLancamento;
import br.com.ggas.contabil.LancamentoContabilAnaliticoVO;
import br.com.ggas.contabil.LancamentoContabilVO;
import br.com.ggas.contabil.impl.OperacaoContabil;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoDetalhamento;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.FaturaConciliacao;
import br.com.ggas.faturamento.impl.FaturaGeralImpl;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Extenso;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.posicaocontasreceber.SubRelatorioPosicaoContasReceberVO;
import br.com.ggas.web.relatorio.tituloemabertopordatavencimento.SubRelatorioTitulosAbertoPorDataVencimentoVO;

class ControladorRecebimentoImpl extends ControladorNegocioImpl implements ControladorRecebimento {

	private static final String ALIAS_AVISO_BANCARIO = "avisoBancario";

	private static final String ALIAS_DOCUMENTO_COBRANCA_ITEM = "documentoCobrancaItem";

	private static final String IDS_FATURA_GERAL = "idsFaturaGeral";

	private static final String RECE = " rece ";

	private static final String ID_CREDIDO_DEBITO_REALIZAR = "idCredidoDebitoRealizar";

	private static final String ID_FATURA_GERAL = "idFaturaGeral";

	private static final String BAIXADO = "baixado";

	private static final String DATA_RECEBIMENTO = "dataRecebimento";

	private static final String WHERE = " where ";

	private static final String FROM = " from ";

	private static final int POSICAO = 2;

	private static final int DUAS_CASAS_DECIMAIS = 2;

	private static final int DECIMO_DIA = 10;

	private static final int LIMITE_CAMPO = 2;

	private static final int LIMITE_OBSERVACAO = 300;

	private static final String PERIODO_REFERENCIA_CONTABIL = "Período de Referência Contabil";

	private static final String ANO_MES_CONTABIL_FINAL = "anoMesContabilFinal";

	private static final String ANO_MES_CONTABIL_INICIAL = "anoMesContabilInicial";

	private static final String DATA_RECEBIMENTO_FINAL = "dataRecebimentoFinal";

	private static final String DATA_RECEBIMENTO_INICIAL = "dataRecebimentoInicial";

	private static final String DATA_DOCUMENTO_FINAL = "dataDocumentoFinal";

	private static final String DATA_DOCUMENTO_INICIAL = "dataDocumentoInicial";

	private static final String BOOLEAN_VERDADE_1 = "1";

	private static final String CLIENTE = "cliente";

	private static final String FATURA_GERAL = "faturaGeral";

	private ControladorFatura getControladorFatura() {

		return ServiceLocator.getInstancia().getControladorFatura();
	}

	private ControladorConstanteSistema getControladorConstanteSistema() {

		return ServiceLocator.getInstancia().getControladorConstanteSistema();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Recebimento.BEAN_ID_RECEBIMENTO);
	}

	/**
	 * Criar recebimento situacao.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarRecebimentoSituacao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(RecebimentoSituacao.BEAN_ID_RECEBIMENTO_SITUACAO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Recebimento.BEAN_ID_RECEBIMENTO);
	}

	public Class<?> getClasseEntidadeRecebimentoSituacao() {

		return ServiceLocator.getInstancia().getClassPorID(RecebimentoSituacao.BEAN_ID_RECEBIMENTO_SITUACAO);
	}

	public Class<?> getClasseEntidadeConteudo() {

		return ServiceLocator.getInstancia().getClassPorID(EntidadeConteudo.BEAN_ID_ENTIDADE_CONTEUDO);
	}

	public Class<?> getClasseEntidadeArrecadador() {

		return ServiceLocator.getInstancia().getClassPorID(Arrecadador.BEAN_ID_ARRECADADOR);
	}

	public Class<?> getClasseEntidadeContaBancaria() {

		return ServiceLocator.getInstancia().getClassPorID(ContaBancaria.BEAN_ID_CONTA_BANCARIA);
	}

	/**
	 * Listar recebimento.
	 *
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<Recebimento> listarRecebimento() throws NegocioException {

		return montarConsulta(getClasseEntidade()).list();
	}

	public Class<?> getClasseEntidadeCreditoDebitoNegociado() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoNegociado.BEAN_ID_CREDITO_DEBITO_NEGOCIADO);
	}

	public Class<?> getClasseEntidadeCreditoDebitoARealizar() {

		return ServiceLocator.getInstancia().getClassPorID(CreditoDebitoARealizar.BEAN_ID_CREDITO_DEBITO_A_REALIZAR);
	}

	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.recebimento.ControladorRecebimento#listarRecebimentoSituacaoExistente()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<RecebimentoSituacao> listarRecebimentoSituacaoExistente() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeRecebimentoSituacao().getSimpleName());
		hql.append(WHERE);
		hql.append(" habilitado = true");
		hql.append(" order by descricao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento#
	 * obterRecebimentoSituacao(java.lang.Long)
	 */
	@Override
	public RecebimentoSituacao obterRecebimentoSituacao(Long chavePrimaria) throws NegocioException {

		return (RecebimentoSituacao) obter(chavePrimaria, getClasseEntidadeRecebimentoSituacao());
	}

	/**
	 * Montar consulta.
	 *
	 * @param classe
	 *            the classe
	 * @return the criteria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Criteria montarConsulta(Class<?> classe) throws NegocioException {

		Criteria criteria = createCriteria(classe);
		criteria.setFetchMode(ALIAS_DOCUMENTO_COBRANCA_ITEM, FetchMode.JOIN);
		criteria.setFetchMode(CLIENTE, FetchMode.JOIN);
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
		return criteria;
	}

	/**
	 * Método responsavel por verificar os parametros obrigatorios passados na
	 * pesquisa de recebimentos.
	 *
	 * @param filtro
	 *            O filtro da tela de pesquisa
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	@Override
	public void validarFiltroPesquisaRecebimento(Map<String, Object> filtro) throws NegocioException, FormatoInvalidoException {

		Date dataRecebimentoInicial = null;
		if (filtro == null) {
			throw new NegocioException(ControladorRecebimento.ERRO_CAMPOS_OBRIGATORIOS_PESQUISA_DATA_RECEBIMENTOS, true);
		}
		dataRecebimentoInicial = (Date) filtro.get(DATA_DOCUMENTO_INICIAL);

		Date dataRecebimentoFinal = (Date) filtro.get(DATA_DOCUMENTO_FINAL);

		if (dataRecebimentoInicial != null && dataRecebimentoFinal != null && dataRecebimentoFinal.before(dataRecebimentoInicial)) {
			throw new NegocioException(ControladorRecebimento.ERRO_PESQUISA_DATA_FINAL_MAIOR_DOCUMENTOS, true);
		}

		Date dataCadastroInicial = (Date) filtro.get(DATA_RECEBIMENTO_INICIAL);

		Date dataCadastroFinal = (Date) filtro.get(DATA_RECEBIMENTO_FINAL);

		if (dataCadastroInicial == null || dataCadastroFinal == null) {
			throw new NegocioException(ControladorRecebimento.ERRO_CAMPOS_OBRIGATORIOS_PESQUISA_DATA_RECEBIMENTOS, true);
		}

		if (dataCadastroFinal.before(dataCadastroInicial)) {
			throw new NegocioException(ControladorRecebimento.ERRO_PESQUISA_DATA_FINAL_MAIOR_RECEBIMENTOS, true);
		}

		String anoMesContabilInicial = (String) filtro.get(ANO_MES_CONTABIL_INICIAL);

		String anoMesContabilFinal = (String) filtro.get(ANO_MES_CONTABIL_FINAL);

		if (anoMesContabilInicial != null && anoMesContabilFinal == null || anoMesContabilFinal != null && anoMesContabilInicial == null) {

			throw new NegocioException(ControladorRecebimento.ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA_CONJUNTAMENTE, true);

		} else if (anoMesContabilInicial != null) {

			Integer anoMesInicial = Util.converterCampoStringParaValorAnoMes(PERIODO_REFERENCIA_CONTABIL, anoMesContabilInicial);
			Integer anoMesFinal = Util.converterCampoStringParaValorAnoMes(PERIODO_REFERENCIA_CONTABIL, anoMesContabilFinal);

			if (anoMesInicial > anoMesFinal) {
				throw new NegocioException(ControladorRecebimento.ERRO_PESQUISA_REFERENCIA_INICIAL_MAIOR, true);
			}
		}

	}

	/**
	 * Metodo responsavel por listar as Entidades 'Recebimento' filtradas pela tela.
	 *
	 * @param filtro the filtro
	 * @return Coleção de Recebimento do sistema
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Recebimento> consultarRecebimento(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if (filtro != null) {

			criteria.createAlias(FATURA_GERAL, FATURA_GERAL);
			criteria.setFetchMode(FATURA_GERAL, FetchMode.JOIN);
			criteria.createAlias("faturaGeral.faturaAtual", "faturaAtual");

			criteria.createAlias(CLIENTE, CLIENTE);
			criteria.setFetchMode(CLIENTE, FetchMode.JOIN);

			criteria.createAlias(ALIAS_DOCUMENTO_COBRANCA_ITEM, ALIAS_DOCUMENTO_COBRANCA_ITEM, Criteria.LEFT_JOIN);

			criteria.createAlias("arrecadadorMovimentoItem", "arrecadadorMovimentoItem", Criteria.LEFT_JOIN);
			criteria.createAlias("arrecadadorMovimentoItem.arrecadadorMovimento", "arrecadadorMovimento", Criteria.LEFT_JOIN);

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long idArrecadador = (Long) filtro.get("idArrecadador");
			if (idArrecadador != null && idArrecadador > 0) {
				criteria.createAlias(ALIAS_AVISO_BANCARIO, ALIAS_AVISO_BANCARIO);
				criteria.add(Restrictions.eq("avisoBancario.arrecadador.chavePrimaria", idArrecadador));
			} else {
				criteria.createAlias(ALIAS_AVISO_BANCARIO, ALIAS_AVISO_BANCARIO, Criteria.LEFT_JOIN);
			}

			criteria.createAlias("avisoBancario.contaBancaria", "contaBancaria", Criteria.LEFT_JOIN);

			Long idRecebimentoSituacao = (Long) filtro.get("idRecebimentoSituacao");
			if (idRecebimentoSituacao != null && idRecebimentoSituacao > 0) {
				criteria.add(Restrictions.eq("recebimentoSituacao.chavePrimaria", idRecebimentoSituacao));
			}

			Long idCliente = (Long) filtro.get("idCliente");
			if ((idCliente != null) && (idCliente > 0)) {
				criteria.add(Restrictions.eq("cliente.chavePrimaria", idCliente));
			}

			Long idPontoConsumo = (Long) filtro.get("idPontoConsumo");
			if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
				criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", idPontoConsumo));
			}

			Long idDevolucao = (Long) filtro.get("idDevolucao");
			if ((idDevolucao != null) && (idDevolucao > 0)) {
				criteria.add(Restrictions.eq("devolucao.chavePrimaria", idDevolucao));
			}

			String anoMesContabilInicial = (String) filtro.get(ANO_MES_CONTABIL_INICIAL);
			if (anoMesContabilInicial != null) {
				criteria.add(Restrictions.ge("anoMesContabil", Integer.valueOf(anoMesContabilInicial)));
			}

			String anoMesContabilFinal = (String) filtro.get(ANO_MES_CONTABIL_FINAL);
			if (anoMesContabilFinal != null) {
				criteria.add(Restrictions.le("anoMesContabil", Integer.valueOf(anoMesContabilFinal)));
			}

			Date dataRecebimentoInicial = (Date) filtro.get(DATA_RECEBIMENTO_INICIAL);
			if (dataRecebimentoInicial != null) {
				criteria.add(Restrictions.ge(DATA_RECEBIMENTO, dataRecebimentoInicial));
			}

			Date dataRecebimentoFinal = (Date) filtro.get(DATA_RECEBIMENTO_FINAL);
			if (dataRecebimentoFinal != null) {
				criteria.add(Restrictions.le(DATA_RECEBIMENTO, dataRecebimentoFinal));
			}

			Long idTipoDocumento = (Long) filtro.get("idTipoDocumento");
			if (idTipoDocumento != null && idTipoDocumento > 0) {
				criteria.add(Restrictions.eq("faturaAtual.tipoDocumento.chavePrimaria", idTipoDocumento));
			}

			Date dataDocumentoInicial = (Date) filtro.get(DATA_DOCUMENTO_INICIAL);
			Date dataDocumentoFinal = (Date) filtro.get(DATA_DOCUMENTO_FINAL);
			if ((dataDocumentoInicial != null) && (dataDocumentoFinal != null)) {
				criteria.add(Restrictions.between("faturaAtual.dataEmissao", dataDocumentoInicial, dataDocumentoFinal));
			}

			EntidadeConteudo situacaoPagamentoFaturaAtual = (EntidadeConteudo) filtro.get("situacaoPagamentoFaturaAtual");
			if (situacaoPagamentoFaturaAtual != null) {
				criteria.add(Restrictions.eq("faturaAtual.situacaoPagamento.chavePrimaria", situacaoPagamentoFaturaAtual.getChavePrimaria()));
			}

			Long[] chavesFaturas = (Long[]) filtro.get("chavesFaturas");
			if ((chavesFaturas != null) && (chavesFaturas.length > 0)) {
				criteria.add(Restrictions.in("faturaAtual.chavePrimaria", chavesFaturas));
			}

			Boolean baixado = (Boolean) filtro.get(BAIXADO);
			if (baixado != null) {
				criteria.add(Restrictions.eq(BAIXADO, baixado));
			}

			Boolean devolvido = (Boolean) filtro.get("devolvido");
			if (devolvido != null) {
				criteria.add(Restrictions.isNull("devolucao.chavePrimaria"));
			}

			criteria.setFetchMode("recebimentoSituacao", FetchMode.JOIN);
			criteria.addOrder(Order.desc(DATA_RECEBIMENTO));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento#
	 * classificarRecebimento
	 * (br.com.ggas.arrecadacao.recebimento.
	 * Recebimento, boolean)
	 */
	@Override
	public ResultadoClassificacao classificarRecebimento(Recebimento recebimento, boolean baixaForcada) throws GGASException {

		return this.classificarRecebimento(recebimento, baixaForcada, false);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento#
	 * classificarRecebimento
	 * (br.com.ggas.arrecadacao.recebimento.
	 * Recebimento, boolean, boolean)
	 */
	@Override
	public ResultadoClassificacao classificarRecebimento(Recebimento recebimento, boolean baixaForcada,
					boolean indicadorCorrecaoMonetaria) throws GGASException {

		RegistradorLancamentoContabil registradorLancamentoContabil = new RegistradorLancamentoContabilVazio();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		DocumentoCobrancaItem documentoCobrancaItem = null;

		Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado = new ArrayList<>();

		if (recebimento.getDocumentoCobrancaItem() != null) {
			documentoCobrancaItem = recebimento.getDocumentoCobrancaItem();
		}

		BigDecimal somatorioRecebimentosNaoBaixados = BigDecimal.ZERO;
		BigDecimal somatorioRecebimentosGeral = BigDecimal.ZERO;
		BigDecimal valorAntecipadoDebito = BigDecimal.ZERO;
		String valorParametro = null;

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ConstanteSistema recebimentoClassificado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_RECEBIMENTO_CLASSIFICADO);
		RecebimentoSituacao recebimentoSituacaoClassificado =
						this.obterRecebimentoSituacao(Long.valueOf(recebimentoClassificado.getValor()));

		ConstanteSistema recebimentoMaiorMenor =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_RECEBIMENTO_VALOR_NAO_CONFERE);
		RecebimentoSituacao recebimentoSituacaoMenorMaior = this.obterRecebimentoSituacao(Long.valueOf(recebimentoMaiorMenor.getValor()));

		ConstanteSistema recebimentoDuplicidade =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_RECEBIMENTO_DUPLICIDADE_EXCESSO);
		RecebimentoSituacao recebimentoSituacaoDuplicidade = this.obterRecebimentoSituacao(Long.valueOf(recebimentoDuplicidade.getValor()));

		ConstanteSistema recebimentoDocumentoInexistente =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_RECEBIMENTO_DOCUMENTO_INEXISTENTE);
		RecebimentoSituacao recebimentoSituacaoInexistente =
						this.obterRecebimentoSituacao(Long.valueOf(recebimentoDocumentoInexistente.getValor()));

		ConstanteSistema constanteRecebimentoSituacaoBaixaPorDacao =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_RECEBIMENTO_BAIXA_POR_DACAO);
		RecebimentoSituacao recebimentoSituacaoBaixaPorDacao =
						this.obterRecebimentoSituacao(Long.valueOf(constanteRecebimentoSituacaoBaixaPorDacao.getValor()));

		ConstanteSistema pago = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);
		EntidadeConteudo situacaoPagamentoPaga = controladorEntidadeConteudo.obter(Long.valueOf(pago.getValor()));

		ConstanteSistema parcialmentePago =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);
		EntidadeConteudo situacaoParcialmentePago = controladorEntidadeConteudo.obter(Long.valueOf(parcialmentePago.getValor()));

		valorParametro =
						(String) controladorParametroSistema.obterValorDoParametroPorCodigo(GERA_CREDITO_POR_RECEBIMENTO_MAIOR_DUPLICIDADE);

		boolean geracaoCreditoALancar = false;
		if (BOOLEAN_VERDADE_1.equals(valorParametro)) {
			geracaoCreditoALancar = true;
		}

		RecebimentoSituacao recebimentoSituacaoPrincipal = recebimento.getRecebimentoSituacao();
		FaturaGeral faturaGeral = null;
		CreditoDebitoARealizar creditoDebitoARealizar = null;
		Collection<Recebimento> recebimentos = null;

		if (documentoCobrancaItem != null) {
			faturaGeral = documentoCobrancaItem.getFaturaGeral();
			creditoDebitoARealizar = documentoCobrancaItem.getCreditoDebitoARealizar();
		} else {
			faturaGeral = recebimento.getFaturaGeral();
		}

		// Buscar outros recebimentos anteriores ordenados pela data de pagamento (crescente) para essa fatura
		Map<String, Object> filtroRecebimento = new HashMap<>();

		// Monta a ordenação da consulta
		String ordenacao = " dataRecebimento asc";

		if (faturaGeral != null) {
			recebimento.setFaturaGeral(faturaGeral);
			filtroRecebimento.put(ID_FATURA_GERAL, faturaGeral.getChavePrimaria());
			recebimentos = this.consultarRecebimento(filtroRecebimento, ordenacao);
		}

		if (creditoDebitoARealizar != null) {
			filtroRecebimento.put(ID_CREDIDO_DEBITO_REALIZAR, creditoDebitoARealizar.getChavePrimaria());
			recebimentos = this.consultarRecebimento(filtroRecebimento, ordenacao);
		}

		if (faturaGeral != null || creditoDebitoARealizar != null) {

			// Verifica se o contrato referente ao item que está sendo recebito apresenta a situação ENCERRADO.
			boolean isContratoEncerrado = false;
			Contrato contrato = null;
			if (faturaGeral != null) {
				faturaGeral =
								(FaturaGeral) controladorFatura.obter(faturaGeral.getChavePrimaria(), FaturaGeralImpl.class, "faturaAtual",
												"faturaAtual.contratoAtual");
			}
			if (faturaGeral != null && faturaGeral.getFaturaAtual() != null && faturaGeral.getFaturaAtual().getContratoAtual() != null) {
				contrato =
								(Contrato) controladorContrato.obter(faturaGeral.getFaturaAtual().getContratoAtual().getChavePrimaria(),
												"situacao");
			} else if (creditoDebitoARealizar != null && creditoDebitoARealizar.getCreditoDebitoNegociado() != null
							&& creditoDebitoARealizar.getCreditoDebitoNegociado().getPontoConsumo() != null) {
				ContratoPontoConsumo contratoPonto =
								controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(creditoDebitoARealizar
												.getCreditoDebitoNegociado().getPontoConsumo().getChavePrimaria());
				if (contratoPonto != null) {
					contrato = (Contrato) controladorContrato.obter(contratoPonto.getContrato().getChavePrimaria(), "situacao");
				}
			}

			Long sitEncerradaContrato = Long.parseLong(controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ENCERRADO));
						if (contrato != null && sitEncerradaContrato != null && contrato.getSituacao().getChavePrimaria() == sitEncerradaContrato) {
				isContratoEncerrado = true;
			}

			// Tratamento de recebimentos relativos a faturas e notas de débito
			if (faturaGeral != null && creditoDebitoARealizar == null) {
				Fatura faturaAtual = faturaGeral.getFaturaAtual();
				CreditoDebitoSituacao creditoDebitoSituacao = faturaAtual.getCreditoDebitoSituacao();

				if (creditoDebitoSituacao.isValido()) {

					EntidadeConteudo situacaoPagamento = faturaAtual.getSituacaoPagamento();
					if (situacaoPagamento.getChavePrimaria() != situacaoPagamentoPaga.getChavePrimaria()) {

						// Buscar outros recebimentos anteriores ordenados pela data de pagamento (crescente) para essa fatura

						/*
						 * Somar os valores dos recebimentos encontrados e não baixados (indicador de baixado (Sim,Não))
						 * com o recebimento que está sendo classificado
						 */
						List<Long> chavesRecebimentosEncontrados;
						chavesRecebimentosEncontrados = verificarQuantidadeRecebimentos(recebimentos);
						somatorioRecebimentosNaoBaixados =
										this.calcularSomatorioRecebimentosNaoBaixados(somatorioRecebimentosNaoBaixados, recebimentos,
														chavesRecebimentosEncontrados);

						if (recebimento.getChavePrimaria() == 0) {
							somatorioRecebimentosGeral = somatorioRecebimentosNaoBaixados.add(recebimento.getValorRecebimento());
						} else {
							somatorioRecebimentosGeral = somatorioRecebimentosNaoBaixados;
						}

						// [Se o resultado da soma for igual] ao da fatura atual, o sistema deve atualizar a situação dos recebimentos
						// para normal(classificado) atualizar a situação do pagamento da fatura atual para paga e a situacao de
						// recebimento baixado dos recebimentos para sim

						BigDecimal valorFatura = faturaAtual.getValorSaldoConciliado();

						if (somatorioRecebimentosGeral.compareTo(valorFatura) == 0) {

							this.classificarRecebimentoValorIgual(recebimento, situacaoPagamentoPaga, faturaAtual, null,
											chavesRecebimentosEncontrados, recebimentoSituacaoClassificado);
							// [Se o resultado for menor]
						} else if (somatorioRecebimentosGeral.compareTo(valorFatura) < 0) {
							if (!isContratoEncerrado) {
								registradorLancamentoContabil = this.classificarRecebimentoValorMenor(recebimento, somatorioRecebimentosNaoBaixados,
												recebimentoSituacaoClassificado, recebimentoSituacaoMenorMaior, situacaoPagamentoPaga,
												situacaoParcialmentePago, faturaAtual, valorFatura, null, chavesRecebimentosEncontrados,
												listaCreditoDebitoNegociado);
							} else {
								throw new NegocioException(ERRO_NEGOCIO_CONTRATO_ENCERRADO_VALOR_DIFERENTE, true);
							}
							// [Se o resultado for maior]
						} else if (somatorioRecebimentosGeral.compareTo(valorFatura) > 0) {
							if (!isContratoEncerrado) {
								this.classificarRecebimentoValorMaior(recebimento, recebimentoSituacaoClassificado,
												recebimentoSituacaoMenorMaior, situacaoPagamentoPaga, geracaoCreditoALancar, faturaAtual,
												valorFatura, null, recebimentos, chavesRecebimentosEncontrados,
												listaCreditoDebitoNegociado);
							} else {
								throw new NegocioException(ERRO_NEGOCIO_CONTRATO_ENCERRADO_VALOR_DIFERENTE, true);
							}
						}

						// Buscar os indicadores de multa e juros do contrato atual do ponto de consumo,

						Boolean indicadorMulta = false;
						if (contrato != null && contrato.getIndicadorMulta() != null) {
							if (recebimentos == null || recebimentos.isEmpty()) {
								indicadorMulta = contrato.getIndicadorMulta();
							} else {
								if (!recebimentosConciliacoesNoPrazo(faturaGeral, false, null)) {
									indicadorMulta = true;
								}
							}

							controladorArrecadacao.gerarDebitosACobrarPorAcrescimentoImpontualidade(recebimento, indicadorMulta,
											contrato.getIndicadorJuros(), indicadorCorrecaoMonetaria, listaCreditoDebitoNegociado);
						} else {
							if (recebimentos == null || recebimentos.isEmpty()) {
								indicadorMulta = true;
							} else {
								if (this.recebimentosConciliacoesNoPrazo(faturaGeral, false, null)) {
									indicadorMulta = true;
								}
							}
							// [Se não existir contrato ativo] assumir sim para os dois indicadores
							controladorArrecadacao.gerarDebitosACobrarPorAcrescimentoImpontualidade(recebimento, indicadorMulta, true,
											listaCreditoDebitoNegociado);
						}
					} else {
						this.classificarRecebimentoDuplicidade(recebimento, recebimentoSituacaoDuplicidade, geracaoCreditoALancar,
										listaCreditoDebitoNegociado);
					}
				} else {
					this.classificarRecebimentoDocumentoInexistente(recebimento, recebimentoSituacaoInexistente);
				}

				// Tratamento de recebimentos relativos a extratos de débito
			} else if (creditoDebitoARealizar != null && faturaGeral == null) {
				CreditoDebitoSituacao creditoDebitoSituacao = creditoDebitoARealizar.getCreditoDebitoSituacao();

				if (creditoDebitoSituacao.isValido()) {

					EntidadeConteudo situacaoPagamento = creditoDebitoARealizar.getSituacaoPagamento();
					if (situacaoPagamento.getChavePrimaria() != situacaoPagamentoPaga.getChavePrimaria()) {

						// Buscar outros recebimentos anteriores ordenados pela data de pagamento (crescente) para esse débito
						valorAntecipadoDebito = controladorCobranca.calcularValorPresente(creditoDebitoARealizar, 0, 1);

						/*
						 * Somar os valores dos recebimentos encontrados e não baixados (indicador de baixado(Sim,Não))
						 * com o recebimento que está sendo classificado
						 */
						List<Long> chavesRecebimentosEncontrados = new ArrayList<>(recebimentos.size());

						somatorioRecebimentosNaoBaixados =
										this.calcularSomatorioRecebimentosNaoBaixados(somatorioRecebimentosNaoBaixados, recebimentos,
														chavesRecebimentosEncontrados);

						somatorioRecebimentosGeral = somatorioRecebimentosNaoBaixados.add(recebimento.getValorRecebimento());

						// [Se o resultado da soma for igual] ao do débito, o sistema deve atualizar a situação dos recebimentos para
						// normal(classificado) atualizar a situação do pagamento do debito para paga e a situacao de recebimento
						// baixado dos recebimentos para sim
						if (somatorioRecebimentosGeral.compareTo(valorAntecipadoDebito) == 0) {
							this.classificarRecebimentoValorIgual(recebimento, situacaoPagamentoPaga, null, creditoDebitoARealizar,
											chavesRecebimentosEncontrados, recebimentoSituacaoClassificado);
						} else if (somatorioRecebimentosGeral.compareTo(valorAntecipadoDebito) < 0) {
							// [Se o resultado for menor]
							if (!isContratoEncerrado) {
								registradorLancamentoContabil = this.classificarRecebimentoValorMenor(recebimento, somatorioRecebimentosNaoBaixados,
												recebimentoSituacaoClassificado, recebimentoSituacaoMenorMaior, situacaoPagamentoPaga,
												situacaoParcialmentePago, null, null, creditoDebitoARealizar,
												chavesRecebimentosEncontrados, listaCreditoDebitoNegociado);
							} else {
								throw new NegocioException(ERRO_NEGOCIO_CONTRATO_ENCERRADO_VALOR_DIFERENTE, true);
							}
						} else if (somatorioRecebimentosGeral.compareTo(valorAntecipadoDebito) > 0) {
							// [Se o resultado for maior]
							if (!isContratoEncerrado) {
								this.classificarRecebimentoValorMaior(recebimento, recebimentoSituacaoClassificado,
												recebimentoSituacaoMenorMaior, situacaoPagamentoPaga, geracaoCreditoALancar, null, null,
												creditoDebitoARealizar, recebimentos, chavesRecebimentosEncontrados,
												listaCreditoDebitoNegociado);
							} else {
								throw new NegocioException(ERRO_NEGOCIO_CONTRATO_ENCERRADO_VALOR_DIFERENTE, true);
							}
						}
					} else {
						this.classificarRecebimentoDuplicidade(recebimento, recebimentoSituacaoDuplicidade, geracaoCreditoALancar,
										listaCreditoDebitoNegociado);
					}
				} else {
					this.classificarRecebimentoDocumentoInexistente(recebimento, recebimentoSituacaoInexistente);
				}

			} else {
				// atualizar o valor excedente do recebimento com o valor do recebimento
				recebimento.setValorExcedente(recebimento.getValorRecebimento());

				// atualizar a situacao do recebimento para documento inexistente
				recebimento.setRecebimentoSituacao(recebimentoSituacaoInexistente);

				// atualizar a situacao de recebimento baixado dos recebimentos para não
				recebimento.setBaixado(false);
			}

		} else {
			// atualizar o valor excedente do recebimento com o valor do recebimento
			recebimento.setValorExcedente(recebimento.getValorRecebimento());

			// atualizar a situacao do recebimento para documento inexistente
			recebimento.setRecebimentoSituacao(recebimentoSituacaoInexistente);

			// atualizar a situacao de recebimento baixado dos recebimentos para não
			recebimento.setBaixado(false);

			filtroRecebimento.put("documentoInexistente", Boolean.TRUE);
			recebimentos = this.consultarRecebimento(filtroRecebimento, ordenacao);

		}

		if (recebimento.getRecebimentoSituacao().getChavePrimaria() == recebimentoSituacaoInexistente.getChavePrimaria()) {
			recebimento.setSequencial(1);
		} else {
			recebimento.setSequencial(this.gerarSequencial(recebimentos));
		}

		// baixa por dacao
		if (recebimentoSituacaoPrincipal.getChavePrimaria() == recebimentoSituacaoBaixaPorDacao.getChavePrimaria()) {
			recebimento.setRecebimentoSituacao(recebimentoSituacaoBaixaPorDacao);
			recebimento.setBaixado(true);
		}

		return new ResultadoClassificacao(listaCreditoDebitoNegociado, registradorLancamentoContabil);
	}

	private List<Long> verificarQuantidadeRecebimentos(Collection<Recebimento> recebimentos) {

		List<Long> chavesRecebimentosEncontrados;
		if (recebimentos != null) {
			chavesRecebimentosEncontrados = new ArrayList<>(recebimentos.size());
		} else {
			chavesRecebimentosEncontrados = new ArrayList<>();
		}
		return chavesRecebimentosEncontrados;
	}

	/**
	 * Gerar sequencial.
	 *
	 * @param lRecebimentos
	 *            the l recebimentos
	 * @return the integer
	 */
	private Integer gerarSequencial(Collection<Recebimento> lRecebimentos) {

		Integer sequencial = Integer.valueOf(1);
		for (Recebimento recebimento : lRecebimentos) {
			if (recebimento.getSequencial() != null) {
				if (recebimento.getSequencial().compareTo(sequencial) >= 0) {
					sequencial += recebimento.getSequencial();
				}
			} else {
				sequencial++;
			}
		}

		return sequencial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.recebimento.ControladorRecebimento#recebimentosConciliacoesNoPrazo(br.com.ggas.faturamento.FaturaGeral,
	 * boolean, java.util.Collection)
	 */
	@Override
	@SuppressWarnings(value={"unchecked"})
	public boolean recebimentosConciliacoesNoPrazo(FaturaGeral faturaGeral, boolean conciliacao, Collection<Long> chavesFaturasConciliadas)
					throws NegocioException {

		boolean flagResultado = true;
		Long idMunicipio = null;

		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();

		Collection<Recebimento> recebimentos = this.consultarRecebimento(faturaGeral);
		Fatura faturaAtual = faturaGeral.getFaturaAtual();

		if (faturaAtual.getPontoConsumo() != null) {
			PontoConsumo pontoConsumo =
							(PontoConsumo) controladorPontoConsumo.obter(faturaAtual.getPontoConsumo().getChavePrimaria(), "quadraFace");
			idMunicipio = pontoConsumo.getQuadraFace().getQuadra().getSetorComercial().getMunicipio().getChavePrimaria();
		} else {
			// FIXME: [ebfu] Atenção (Urgente)tratar municipio do cliente
			if (faturaAtual.getCliente().getEnderecoPrincipal() != null
							&& faturaAtual.getCliente().getEnderecoPrincipal().getMunicipio() != null) {
				idMunicipio = faturaAtual.getCliente().getEnderecoPrincipal().getMunicipio().getChavePrimaria();
			}
		}

		ControladorFeriado controladorFeriado =
						(ControladorFeriado) ServiceLocator.getInstancia().getBeanPorID(ControladorFeriado.BEAN_ID_CONTROLADOR_FERIADO);

		Date dataVecimentoFatura = faturaAtual.getDataVencimento();
		if (!controladorFeriado.isDiaUtil(faturaAtual.getDataVencimento())) {
			dataVecimentoFatura = controladorFeriado.obterProximoDiaUtil(faturaAtual.getDataVencimento(), idMunicipio, Boolean.TRUE);
		}

		for (Recebimento recebimento : recebimentos) {
			if (recebimento.getDataRecebimento().after(dataVecimentoFatura)) {
				flagResultado = false;
			}
		}

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		Collection<FaturaConciliacao> listaFaturasConciliacao =
						controladorFatura.consultarFaturaConciliacao(faturaAtual.getChavePrimaria());

		if (chavesFaturasConciliadas == null) {
			for (FaturaConciliacao faturaConciliacao : listaFaturasConciliacao) {
				if (faturaConciliacao.getDataConciliacao().after(dataVecimentoFatura)) {
					flagResultado = false;
				}
			}
		} else {
			for (FaturaConciliacao faturaConciliacao : listaFaturasConciliacao) {
				if ((faturaConciliacao.getDataConciliacao().after(dataVecimentoFatura))
								&& !(chavesFaturasConciliadas.contains(faturaConciliacao.getChavePrimaria()))) {
					flagResultado = false;
				}
			}
		}

		return flagResultado;
	}

	/**
	 * Calcular somatorio recebimentos nao baixados.
	 *
	 * @param somatorioRecebimentos
	 *            the somatorio recebimentos nao baixados
	 * @param recebimentos
	 *            the recebimentos
	 * @param chavesRecebimentosEncontrados
	 *            the chaves recebimentos encontrados
	 * @return the big decimal
	 */
	private BigDecimal calcularSomatorioRecebimentosNaoBaixados(BigDecimal somatorioRecebimentos,
	// by gmatos on 15/10/09 10:22
					Collection<Recebimento> recebimentos, List<Long> chavesRecebimentosEncontrados) {
		BigDecimal somatorioRecebimentosNaoBaixados = somatorioRecebimentos;
		if (recebimentos != null) {
			for (Recebimento recebimentoEncontrado : recebimentos) {
				chavesRecebimentosEncontrados.add(recebimentoEncontrado.getChavePrimaria());
				if (!recebimentoEncontrado.isBaixado()) {
					somatorioRecebimentosNaoBaixados = somatorioRecebimentosNaoBaixados.add(recebimentoEncontrado.getValorRecebimento());
				}
			}
		}
		return somatorioRecebimentosNaoBaixados;
	}

	/**
	 * Calcular somatorio recebimentos.
	 *
	 * @param somatorio
	 *            the somatorio
	 * @param recebimentos
	 *            the recebimentos
	 * @param parametroRecebimento
	 *            the parametro recebimento
	 * @return the big decimal
	 */
	private BigDecimal calcularSomatorioRecebimentos(BigDecimal somatorio, Collection<Recebimento> recebimentos, Long parametroRecebimento) {

		BigDecimal somatorioRecebimentos = somatorio;
		for (Recebimento recebimentoEncontrado : recebimentos) {
			if (recebimentoEncontrado.getChavePrimaria() != parametroRecebimento) {
				somatorioRecebimentos = somatorioRecebimentos.add(recebimentoEncontrado.getValorRecebimento());
			}
		}
		return somatorioRecebimentos;
	}

	/**
	 * Classificar recebimento documento inexistente.
	 *
	 * @param recebimento
	 *            the recebimento
	 * @param recebimentoSituacaoInexistente
	 *            the recebimento situacao inexistente
	 */
	private void classificarRecebimentoDocumentoInexistente(Recebimento recebimento, RecebimentoSituacao recebimentoSituacaoInexistente) {

		// atualizar o valor excedente do
		// recebimento com o valor do recebimento
		recebimento.setValorExcedente(recebimento.getValorRecebimento());

		// e a situacao do recebimento para
		// documento inexistente
		recebimento.setRecebimentoSituacao(recebimentoSituacaoInexistente);

		// e a situacao de recebimento baixado dos
		// recebimentos para não
		recebimento.setBaixado(false);
	}

	/**
	 * Classificar recebimento duplicidade.
	 *
	 * @param recebimento
	 *            the recebimento
	 * @param recebimentoSituacaoDuplicidade
	 *            the recebimento situacao duplicidade
	 * @param geracaoCreditoALancar
	 *            the geracao credito a lancar
	 * @param listaCreditoDebitoNegociado
	 *            the lista credito debito negociado
	 * @throws GGASException
	 */
	private void classificarRecebimentoDuplicidade(Recebimento recebimento, RecebimentoSituacao recebimentoSituacaoDuplicidade,
					boolean geracaoCreditoALancar, Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorRubrica controladorRubrica = ServiceLocator.getInstancia().getControladorRubrica();
		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();

		// P4=geracao de crédito a lancar
		// [SIM,NAO],

		// atualizar o valor excedente do
		// recebimento com o valor do recebimento
		recebimento.setValorExcedente(recebimento.getValorRecebimento());

		// atualizar a situacao do recebimento
		// para duplicidade/excesso
		recebimento.setRecebimentoSituacao(recebimentoSituacaoDuplicidade);

		// [Se P4 for igual a sim]
		if (geracaoCreditoALancar) {

			// atualizar a situacao de recebimento
			// baixado dos recebimentos para sim
			recebimento.setBaixado(true);

			// Gerar crédito a realizar (Rubrica
			// de crédito)

			String codigoRubricaCredito =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_CREDITO_PAGTO_MAIOR);

			Rubrica rubrica = (Rubrica) controladorRubrica.obter(Long.parseLong(codigoRubricaCredito));

			String origem =
							controladorConstanteSistema
											.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_ORIGEM_DOCUMENTOS_PAGOS_DUPLICIDADE);

			controladorArrecadacao.inserirEstruturaCreditoDebito(recebimento, recebimento.getValorRecebimento(), rubrica, origem,
							listaCreditoDebitoNegociado);

		} else {
			// [Se P4 for não]
			recebimento.setBaixado(false);
		}
	}

	/**
	 * Classificar recebimento valor maior.
	 *
	 * @param recebimento
	 *            the recebimento
	 * @param recebimentoSituacaoClassificado
	 *            the recebimento situacao classificado
	 * @param recebimentoSituacaoDuplicidade
	 *            the recebimento situacao duplicidade
	 * @param situacaoPagamentoPaga
	 *            the situacao pagamento paga
	 * @param geracaoCreditoALancar
	 *            the geracao credito a lancar
	 * @param faturaAtual
	 *            the fatura atual
	 * @param valorFatura
	 *            the valor fatura
	 * @param creditoDebitoARealizar
	 *            the credito debito a realizar
	 * @param recebimentos
	 *            the recebimentos
	 * @param chavesRecebimentosEncontrados
	 *            the chaves recebimentos encontrados
	 * @param listaCreditoDebitoNegociado
	 *            the lista credito debito negociado
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void classificarRecebimentoValorMaior(Recebimento recebimento, RecebimentoSituacao recebimentoSituacaoClassificado,
					RecebimentoSituacao recebimentoSituacaoDuplicidade, EntidadeConteudo situacaoPagamentoPaga,
					boolean geracaoCreditoALancar, Fatura faturaAtual, BigDecimal valorFatura,
					CreditoDebitoARealizar creditoDebitoARealizar, Collection<Recebimento> recebimentos,
					List<Long> chavesRecebimentosEncontrados, Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado)
					throws GGASException {

		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();
		ControladorRubrica controladorRubrica = ServiceLocator.getInstancia().getControladorRubrica();
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();

		BigDecimal valorTotalExcedente = BigDecimal.ZERO;
		BigDecimal valorCalculado = BigDecimal.ZERO;

		// Verificar o valor excedente por recebimento
		BigDecimal acumulador = BigDecimal.ZERO;
		boolean excedeu = false;
		Collection<Recebimento> todosOsRecebimentos = new HashSet<>();
		todosOsRecebimentos.addAll(recebimentos);
		todosOsRecebimentos.add(recebimento);

		if (faturaAtual != null) {
			valorCalculado = valorFatura;
		} else {
			valorCalculado = creditoDebitoARealizar.getValor();
		}

		for (Recebimento recebimentoEncontrado : todosOsRecebimentos) {
			acumulador = acumulador.add(recebimentoEncontrado.getValorRecebimento());

			if (acumulador.compareTo(valorCalculado) < 1) {
				// atualizar a situacao do recebimento para classificado e zerar o valor excedente
				recebimentoEncontrado.setValorExcedente(BigDecimal.ZERO);
				recebimentoEncontrado.setRecebimentoSituacao(recebimentoSituacaoClassificado);
				atualizaRecebimento(recebimento, recebimentoEncontrado);
			} else {

				if (!excedeu) {

					// Atualizar o recebimento(Valor excedido) com a diferença entre o valor acumulado e o valor da fatura
					recebimentoEncontrado.setRecebimentoSituacao(recebimentoSituacaoClassificado);
					recebimentoEncontrado.setValorExcedente(acumulador.subtract(valorCalculado).abs());
					valorTotalExcedente = valorTotalExcedente.add(acumulador.subtract(valorCalculado).abs());

					recebimento.setRecebimentoSituacao(recebimentoSituacaoClassificado);

					atualizaRecebimento(recebimento, recebimentoEncontrado);
					excedeu = true;
				} else {
					// Atualizar o recebimento(Valor excedido) com o valor do recebimento
					recebimentoEncontrado.setRecebimentoSituacao(recebimentoSituacaoDuplicidade);
					recebimentoEncontrado.setValorExcedente(valorTotalExcedente.add(recebimentoEncontrado.getValorRecebimento()));
					recebimento.setRecebimentoSituacao(recebimentoSituacaoDuplicidade);
					atualizaValorExcedido(recebimento, recebimentoEncontrado);
				}

			}

		}

		// atualizar a situação da fatura para
		// paga
		if (faturaAtual != null) {
			atualizarSituacaoAtualFatura(recebimento, situacaoPagamentoPaga, faturaAtual, controladorCobranca);
		} else {
			try {

				creditoDebitoARealizar.setSituacaoPagamento(situacaoPagamentoPaga);
				creditoDebitoARealizar.setDadosAuditoria(recebimento.getDadosAuditoria());
				controladorCobranca.atualizarCreditoDebitoARealizar(creditoDebitoARealizar);
			} catch (ConcorrenciaException e) {
				throw new NegocioException(e);
			}
		}

		// atualizar a situacao de recebimento baixado dos recebimentos para não
		if (chavesRecebimentosEncontrados != null && !chavesRecebimentosEncontrados.isEmpty()) {
			// FIXME: Substituir pelo atualizar do controladorNegocio
			this.atualizarBaixadoRecebimentos(chavesRecebimentosEncontrados, false);
		}

		// Gerar crédito a realizar (Rubrica de crédito) P4=geracao de crédito a lancar
		// [SIM,NAO], [Se P4 for igual a sim]
		if (geracaoCreditoALancar) {
			if (valorTotalExcedente.compareTo(BigDecimal.ZERO) != 0) {
				recebimento.setValorExcedente(valorTotalExcedente);

				String codigoRubricaCredito =
								controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_CREDITO_PAGTO_MAIOR);

				Rubrica rubrica = (Rubrica) controladorRubrica.obter(Long.parseLong(codigoRubricaCredito));

				String origem =
								controladorConstanteSistema
												.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_ORIGEM_DOCUMENTOS_PAGOS_DUPLICIDADE);

				controladorArrecadacao.inserirEstruturaCreditoDebito(recebimento, recebimento.getValorExcedente(), rubrica, origem,
								listaCreditoDebitoNegociado);

				recebimento.setBaixado(true);
			}
		} else {
			recebimento.setRecebimentoSituacao(recebimentoSituacaoDuplicidade);
		}
	}

	private void atualizaValorExcedido(Recebimento recebimento, Recebimento recebimentoEncontrado) throws NegocioException {

		try {
			recebimentoEncontrado.setDadosAuditoria(recebimento.getDadosAuditoria());
			if (recebimentoEncontrado.getChavePrimaria() > 0) {
				atualizaRecebimento(recebimentoEncontrado);
			}
		} catch (ConcorrenciaException e) {
			throw new NegocioException(e);
		}
	}

	private void atualizaRecebimento(Recebimento recebimento, Recebimento recebimentoEncontrado) throws NegocioException {

		try {
			recebimentoEncontrado.setDadosAuditoria(recebimento.getDadosAuditoria());
			if (recebimentoEncontrado.getChavePrimaria() > 0) {
				this.atualizaRecebimento(recebimentoEncontrado);
			}
		} catch (ConcorrenciaException e) {
			throw new NegocioException(e);
		}
	}

	private void atualizaRecebimento(Recebimento recebimentoEncontrado) throws ConcorrenciaException, NegocioException {

		getSession().flush();
		getSession().clear();
		this.atualizar(recebimentoEncontrado);
	}

	/**
	 * Classificar recebimento valor menor.
	 *
	 * @param recebimento
	 *            the recebimento
	 * @param somatorioRecebimentos
	 *            the somatorio recebimentos nao baixados
	 * @param recebimentoSituacaoClassificado
	 *            the recebimento situacao classificado
	 * @param recebimentoSituacaoMenorMaior
	 *            the recebimento situacao menor maior
	 * @param situacaoPagamentoPaga
	 *            the situacao pagamento paga
	 * @param situacaoParcialmentePago
	 *            the situacao parcialmente pago
	 * @param faturaAtual
	 *            the fatura atual
	 * @param valorFatura
	 *            the valor fatura
	 * @param creditoDebitoARealizar
	 *            the credito debito a realizar
	 * @param chavesRecebimentosEncontrados
	 *            the chaves recebimentos encontrados
	 * @param listaCreditoDebitoNegociado
	 *            the lista credito debito negociado
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private RegistradorLancamentoContabil classificarRecebimentoValorMenor(Recebimento recebimento,
			BigDecimal somatorioRecebimentos,
			RecebimentoSituacao recebimentoSituacaoClassificado, RecebimentoSituacao recebimentoSituacaoMenorMaior,
					EntidadeConteudo situacaoPagamentoPaga, EntidadeConteudo situacaoParcialmentePago, Fatura faturaAtual,
					BigDecimal valorFatura, CreditoDebitoARealizar creditoDebitoARealizar, List<Long> chavesRecebimentosEncontrados,
					Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado) throws GGASException {

		BigDecimal somatorioRecebimentosNaoBaixados = somatorioRecebimentos;
		RegistradorLancamentoContabil registradorLancamentosContabeis = new RegistradorLancamentoContabilVazio();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorRubrica controladorRubrica = ServiceLocator.getInstancia().getControladorRubrica();
		ControladorCreditoDebito controladorCreditoDebito = ServiceLocator.getInstancia().getControladorCreditoDebito();

		String valorParametro =
						(String) controladorParametroSistema.obterValorDoParametroPorCodigo(GERA_DOCUMENTO_COBRANCA_POR_RECEBIMENTO_MENOR);

		boolean gerDocComplementar = false;
		if (BOOLEAN_VERDADE_1.equals(valorParametro)) {
			gerDocComplementar = true;
		}

		EntidadeConteudo acaoBaixaRecebimentoMenorDespesa =
						controladorEntidadeConteudo.obter(Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_BAIXA_RECEBIMENTO_MENOR_DESPESA)));

		EntidadeConteudo acaoBaixaRecebimentoMenorGeraDebito =
						controladorEntidadeConteudo.obter(Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_BAIXA_RECEBIMENTO_MENOR_GERA_DEBITO)));

		EntidadeConteudo acaoBaixaRecebimentoMenor =
						controladorEntidadeConteudo.obter(Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_NAO_BAIXA_RECEBIMENTO_MENOR)));

		EntidadeConteudo acaoPadraoRecebimentoMenor =
						controladorEntidadeConteudo.obter(Long.valueOf((String) controladorParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_ACAO_TRATAR_RECEBIMENTO_MENOR)));

		// calcular a diferença
		BigDecimal diferencaRecebimentos = BigDecimal.ZERO;
		somatorioRecebimentosNaoBaixados = somatorioRecebimentosNaoBaixados.add(recebimento.getValorRecebimento());
		if (faturaAtual != null) {
			diferencaRecebimentos = somatorioRecebimentosNaoBaixados.subtract(valorFatura).abs();
		} else {
			diferencaRecebimentos = somatorioRecebimentosNaoBaixados.subtract(creditoDebitoARealizar.getValor()).abs();
		}

		// Gera Nota de Débito
		if (gerDocComplementar) {

			// atualizar a situação do pagamento da fatura atual para parcialmente paga.
			if (faturaAtual != null) {
				// Inserindo o documento de cobrança com a diferença
				this.inserirDocumentoCobrancaDiferenca(recebimento, faturaAtual, diferencaRecebimentos);
				atualizarSituacaoAtualFatura(recebimento, situacaoPagamentoPaga, faturaAtual, controladorCobranca);
			} else {
				atualizaSituacaoPagamentoCreditoDebito(situacaoPagamentoPaga, creditoDebitoARealizar, controladorCobranca);
			}

			recebimento.setRecebimentoSituacao(recebimentoSituacaoMenorMaior);
			recebimento.setBaixado(false);

			// o sistema deve atualizar a situação dos recebimentos para normal(classificado)
			if (chavesRecebimentosEncontrados != null && !chavesRecebimentosEncontrados.isEmpty()) {
				// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
				this.atualizarSituacaoRecebimentos(chavesRecebimentosEncontrados, recebimentoSituacaoMenorMaior);
				this.atualizarBaixadoRecebimentos(chavesRecebimentosEncontrados, false);
			}

		} else if (acaoPadraoRecebimentoMenor.equals(acaoBaixaRecebimentoMenorDespesa)) {
			// Se não gera Nota de Débito e a ação padrão para recebimento a menor for baixar como despesa.


			if (faturaAtual != null) {
				registradorLancamentosContabeis =
						new RegistradorLancamentoContabilDespesa(recebimento, faturaAtual, diferencaRecebimentos);
				atualizarSituacaoAtualFatura(recebimento, situacaoPagamentoPaga, faturaAtual, controladorCobranca);
			} else {
				atualizaSituacaoPagamentoCreditoDebito(situacaoPagamentoPaga, creditoDebitoARealizar, controladorCobranca);
			}

			recebimento.setRecebimentoSituacao(recebimentoSituacaoClassificado);
			recebimento.setBaixado(true);

			if (chavesRecebimentosEncontrados != null && !chavesRecebimentosEncontrados.isEmpty()) {
				// FIXME: Substituir pelo atualizar do controladorNegocio
				// o sistema deve atualizar a situação dos recebimentos para normal(classificado)
				this.atualizarSituacaoRecebimentos(chavesRecebimentosEncontrados, recebimentoSituacaoClassificado);
				// atualizar a situacao de recebimento baixado dos recebimentos para sim
				this.atualizarBaixadoRecebimentos(chavesRecebimentosEncontrados, true);
			}

		} else if (acaoPadraoRecebimentoMenor.equals(acaoBaixaRecebimentoMenorGeraDebito)) {
			// Se não gera Nota de Débito e a ação padrão para recebimento a menor for gerar débito a cobrar.

			// atualizar a situação do pagamento da fatura atual para paga.
			if (faturaAtual != null) {
				atualizarSituacaoAtualFatura(recebimento, situacaoPagamentoPaga, faturaAtual, controladorCobranca);
			} else {
				atualizaSituacaoPagamentoCreditoDebito(situacaoPagamentoPaga, creditoDebitoARealizar, controladorCobranca);
			}

			recebimento.setRecebimentoSituacao(recebimentoSituacaoMenorMaior);
			recebimento.setBaixado(true);

			if (chavesRecebimentosEncontrados != null && !chavesRecebimentosEncontrados.isEmpty()) {
				// FIXME: Substituir pelo atualizar do controladorNegocio
				// o sistema deve atualizar a situação dos recebimentos para normal(classificado)
				this.atualizarSituacaoRecebimentos(chavesRecebimentosEncontrados, recebimentoSituacaoClassificado);

				// atualizar a situacao de recebimento baixado dos recebimentos para sim
				this.atualizarBaixadoRecebimentos(chavesRecebimentosEncontrados, true);
			}

			Rubrica rubrica =
							(Rubrica) controladorRubrica.obter(Long.parseLong(controladorConstanteSistema
											.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_DEBITO_PAGTO_MENOR)));

			CreditoDebitoNegociado creditoDebitoNegociado =
							controladorCreditoDebito.popularCreditoDebitoNegociado(recebimento.getPontoConsumo(), recebimento.getCliente(),
											rubrica, diferencaRecebimentos, recebimento.getDadosAuditoria(), null);

			controladorCreditoDebito.inserirCreditoDebitoNegociado(creditoDebitoNegociado);

			if (listaCreditoDebitoNegociado != null) {
				listaCreditoDebitoNegociado.add(creditoDebitoNegociado);
			}

			CreditoDebitoARealizar debitoARealizar =
							controladorCreditoDebito.popularCreditoDebitoARealizar(recebimento.getDocumentoCobrancaItem(),
											recebimento.getFaturaGeral(), creditoDebitoNegociado.getValorTotal(), creditoDebitoNegociado,
											recebimento.getDadosAuditoria());
			// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
			getHibernateTemplate().getSessionFactory().getCurrentSession().save(debitoARealizar);

			CreditoDebitoDetalhamento creditoDebitoDetalhamento =
							controladorCreditoDebito.popularCreditoDebitoDetalhamento(debitoARealizar, rubrica,
											recebimento.getDadosAuditoria());
			// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
			getHibernateTemplate().getSessionFactory().getCurrentSession().save(creditoDebitoDetalhamento);

			Integer referenciaContabil =
							Integer.valueOf((String) controladorParametroSistema
											.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_CONTABIL));
			if ((Util.obterAnoMes(recebimento.getDataRecebimento())).compareTo(referenciaContabil) == 0) {
				controladorCreditoDebito.registrarLancamentoContabil(creditoDebitoNegociado, OperacaoContabil.INCLUIR_CREDITO_DEBITO,
								recebimento.getDataRecebimento());
			} else {
				controladorCreditoDebito.registrarLancamentoContabil(creditoDebitoNegociado, OperacaoContabil.INCLUIR_CREDITO_DEBITO, null);
			}

		} else if (acaoPadraoRecebimentoMenor.equals(acaoBaixaRecebimentoMenor)) {
			// Se não gera Nota de Débito e a ação padrão para recebimento a menor baixa a menor.

			// atualizar a situação da fatura para paga parcial e a situação dos recebimentos para recebimento a menor
			if (faturaAtual != null) {
				atualizarSituacaoAtualFatura(recebimento, situacaoParcialmentePago, faturaAtual, controladorCobranca);
			} else {
				atualizaSituacaoPagamentoCreditoDebito(situacaoParcialmentePago, creditoDebitoARealizar, controladorCobranca);
			}

			recebimento.setRecebimentoSituacao(recebimentoSituacaoMenorMaior);
			recebimento.setBaixado(false);

			if (chavesRecebimentosEncontrados != null && !chavesRecebimentosEncontrados.isEmpty()) {
				// FIXME: Substituir pelo atualizar do controladorNegocio
				// o sistema deve atualizar a situação dos recebimentos para recebimento a menor
				this.atualizarSituacaoRecebimentos(chavesRecebimentosEncontrados, recebimentoSituacaoMenorMaior);

				// atualizar a situacao de recebimento baixado dos recebimentos para nao
				this.atualizarBaixadoRecebimentos(chavesRecebimentosEncontrados, false);
			}

		}
		return registradorLancamentosContabeis;
	}

	private void atualizaSituacaoPagamentoCreditoDebito(EntidadeConteudo situacaoPagamentoPaga,
					CreditoDebitoARealizar creditoDebitoARealizar, ControladorCobranca controladorCobranca) throws NegocioException {

		try {
			creditoDebitoARealizar.setSituacaoPagamento(situacaoPagamentoPaga);
			controladorCobranca.atualizarCreditoDebitoARealizar(creditoDebitoARealizar);
		} catch (ConcorrenciaException e) {
			throw new NegocioException(e);
		}
	}

	private void atualizarSituacaoAtualFatura(Recebimento recebimento, EntidadeConteudo situacaoPagamentoPaga, Fatura faturaAtual,
					ControladorCobranca controladorCobranca) throws NegocioException {

		try {
			faturaAtual.setSituacaoPagamento(situacaoPagamentoPaga);
			faturaAtual.setDadosAuditoria(recebimento.getDadosAuditoria());
			controladorCobranca.atualizarFatura(faturaAtual);
		} catch (ConcorrenciaException e) {
			throw new NegocioException(e);
		}
	}

	/**
	 * Inserir documento cobranca diferenca.
	 *
	 * @param recebimento
	 *            the recebimento
	 * @param faturaAtual
	 *            the fatura atual
	 * @param valorDiferenca
	 *            the diferenca recebimentos
	 * @throws GGASException
	 */
	private void inserirDocumentoCobrancaDiferenca(Recebimento recebimento, Fatura faturaAtual,
			BigDecimal valorDiferenca) throws GGASException {

		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();

		Fatura notaDebitoCredito =
						this.gerarNotaDebitoDiferenca(faturaAtual.getContratoAtual(), faturaAtual.getPontoConsumo(), valorDiferenca,
										recebimento.getDataRecebimento());

		notaDebitoCredito.setProvisaoDevedoresDuvidosos(faturaAtual.getProvisaoDevedoresDuvidosos());

		controladorFatura.inserirNotaDebitoCredito(notaDebitoCredito, new Long[] {}, null);

	}

	/**
	 * Gerar nota debito diferenca.
	 *
	 * @param contrato the contrato
	 * @param pontoConsumo the ponto consumo
	 * @param valorDiferenca the valor diferenca
	 * @param dataVencimento the data vencimento
	 * @return the fatura
	 * @throws GGASException the GGAS exception
	 */
	public Fatura gerarNotaDebitoDiferenca(Contrato contrato, PontoConsumo pontoConsumo, BigDecimal valorDiferenca, Date dataVencimento)
					throws GGASException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorRubrica controladorRubrica = ServiceLocator.getInstancia().getControladorRubrica();
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();

		Fatura notaDebitoCredito = (Fatura) controladorFatura.criar();

		notaDebitoCredito.setCliente(contrato.getClienteAssinatura());
		notaDebitoCredito.setValorConciliado(BigDecimal.ZERO);
		notaDebitoCredito.setValorTotal(valorDiferenca);
		notaDebitoCredito.setContratoAtual(contrato);
		notaDebitoCredito.setCobrada(Boolean.TRUE);
		notaDebitoCredito.setDataEmissao(Util.getDataCorrente(false));

		notaDebitoCredito.setDadosAuditoria(contrato.getDadosAuditoria());
		notaDebitoCredito.setHabilitado(Boolean.TRUE);
		notaDebitoCredito.setUltimaAlteracao(Calendar.getInstance().getTime());

		notaDebitoCredito.setPontoConsumo(pontoConsumo);
		notaDebitoCredito.setContratoAtual(contrato);

		if (contrato.getChavePrimariaPai() != null) {
			notaDebitoCredito.setContrato((Contrato) controladorContrato.obter(contrato.getChavePrimariaPai()));
		} else {
			notaDebitoCredito.setContrato(contrato);
		}

		if (pontoConsumo != null) {
			notaDebitoCredito.setSegmento(pontoConsumo.getSegmento());
		}

		notaDebitoCredito.setDataVencimento(dataVencimento);

		Long idSituacaoNormal =
						Long.valueOf((String) controladorParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_NORMAL));
		notaDebitoCredito.setCreditoDebitoSituacao(controladorCobranca.obterCreditoDebitoSituacao(idSituacaoNormal));

		Long idSituacaoPendente =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE));
		notaDebitoCredito.setSituacaoPagamento(controladorEntidadeConteudo.obterEntidadeConteudo(idSituacaoPendente));

		Long idTipoDocumento =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO));
		notaDebitoCredito.setTipoDocumento(controladorArrecadacao.obterTipoDocumento(idTipoDocumento));

		FaturaItem faturaItem = (FaturaItem) controladorFatura.criarFaturaItem();

		faturaItem.setFatura(notaDebitoCredito);
		faturaItem.setQuantidade(BigDecimal.ONE);
		faturaItem.setValorUnitario(valorDiferenca);
		faturaItem.setValorTotal(valorDiferenca);
		faturaItem.setUltimaAlteracao(Calendar.getInstance().getTime());
		faturaItem.setDadosAuditoria(contrato.getDadosAuditoria());
		faturaItem.setNumeroSequencial(1);

		Rubrica rubrica =
						(Rubrica) controladorRubrica.obter(Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_DEBITO_PAGTO_MENOR)));
		faturaItem.setRubrica(rubrica);

		notaDebitoCredito.getListaFaturaItem().add(faturaItem);

		return notaDebitoCredito;

	}

	/**
	 * Classificar recebimento valor igual.
	 *
	 * @param recebimento
	 *            the recebimento
	 * @param situacaoPagamentoPaga
	 *            the situacao pagamento paga
	 * @param faturaAtual
	 *            the fatura atual
	 * @param creditoDebitoARealizar
	 *            the credito debito a realizar
	 * @param chavesRecebimentosEncontrados
	 *            the chaves recebimentos encontrados
	 * @param recebimentoSituacaoClassificado
	 *            the recebimento situacao classificado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void classificarRecebimentoValorIgual(Recebimento recebimento, EntidadeConteudo situacaoPagamentoPaga, Fatura faturaAtual,
					CreditoDebitoARealizar creditoDebitoARealizar, List<Long> chavesRecebimentosEncontrados,
					RecebimentoSituacao recebimentoSituacaoClassificado) throws NegocioException {

		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();

		if (faturaAtual != null) {
			atualizarSituacaoAtualFatura(recebimento, situacaoPagamentoPaga, faturaAtual, controladorCobranca);
		} else {
			try {
				creditoDebitoARealizar.setSituacaoPagamento(situacaoPagamentoPaga);
				creditoDebitoARealizar.setDadosAuditoria(recebimento.getDadosAuditoria());
				controladorCobranca.atualizarCreditoDebitoARealizar(creditoDebitoARealizar);
			} catch (ConcorrenciaException e) {
				throw new NegocioException(e);
			}
		}

		if (chavesRecebimentosEncontrados != null && !chavesRecebimentosEncontrados.isEmpty()) {
			// FIXME: Substituir pelo atualizar do controladorNegocio
			this.atualizarBaixadoRecebimentos(chavesRecebimentosEncontrados, true);
			this.atualizarSituacaoRecebimentos(chavesRecebimentosEncontrados, recebimentoSituacaoClassificado);
		}

		recebimento.setRecebimentoSituacao(recebimentoSituacaoClassificado);
		recebimento.setBaixado(true);

	}

	/**
	 * Atualizar situacao recebimentos.
	 *
	 * @deprecated
	 * @param chavesRecebimentos
	 *            the chaves recebimentos
	 * @param situacao
	 *            the situacao
	 */
	@Deprecated
	private void atualizarSituacaoRecebimentos(List<Long> chavesRecebimentos, RecebimentoSituacao situacao) {

		StringBuilder hql = new StringBuilder();

		hql.append(" update ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" set recebimentoSituacao.chavePrimaria = ? ");
		hql.append(WHERE);
		hql.append(" chavePrimaria in (:chavesPrimarias)");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS, chavesRecebimentos);
		query.setParameter(0, situacao.getChavePrimaria());
		query.executeUpdate();

	}

	/**
	 * Atualizar baixado recebimentos.
	 *
	 * @param chavesRecebimentos
	 *            the chaves recebimentos
	 * @param baixado
	 *            the baixado
	 * @deprecated
	 */

	@Deprecated
	private void atualizarBaixadoRecebimentos(List<Long> chavesRecebimentos, boolean baixado) {

		StringBuilder hql = new StringBuilder();

		hql.append(" update ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" set baixado = :baixado ");
		hql.append(WHERE);
		hql.append(" chavePrimaria in (:chavesPrimarias)");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(BAIXADO, baixado);
		query.setParameterList(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS, chavesRecebimentos);

		query.executeUpdate();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento
	 * #consultarRecebimento(
	 * br.com.ggas.faturamento.FaturaGeral)
	 */
	/**
	 * @deprecated
	 */
	@Override
	@SuppressWarnings("unchecked")
	@Deprecated
	public Collection<Recebimento> consultarRecebimento(FaturaGeral faturaGeral) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" faturaGeral.chavePrimaria = ? ");
		hql.append(" order by dataRecebimento asc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(0, faturaGeral.getChavePrimaria());

		return query.list();
	}

	/**
	 * Consultar recebimento.
	 *
	 * @param filtro
	 *            the filtro
	 * @param ordenacao
	 *            the ordenacao
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento
	 * #consultarRecebimento(
	 * br.com.ggas.faturamento.FaturaGeral)
	 */
	@SuppressWarnings("unchecked")
	public Collection<Recebimento> consultarRecebimento(Map<String, Object> filtro, String ordenacao, String... propriedadesLazy)
					throws NegocioException {

		Query query = null;
		Long idFaturaGeral = null;
		Long idCredidoDebitoRealizar = null;
		Boolean documentoInexistente = null;
		Long[] listaRecebimentoSituacao = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(RECE);
		hql.append(" where 1 = 1 ");

		if (filtro != null && !filtro.isEmpty()) {
			idFaturaGeral = (Long) filtro.get(ID_FATURA_GERAL);
			idCredidoDebitoRealizar = (Long) filtro.get(ID_CREDIDO_DEBITO_REALIZAR);
			listaRecebimentoSituacao = (Long[]) filtro.get("listaRecebimentoSituacao");
			documentoInexistente = (Boolean) filtro.get("documentoInexistente");

			if (idFaturaGeral != null) {
				hql.append(" and rece.faturaGeral.chavePrimaria = :idFaturaGeral ");
			}

			if (idCredidoDebitoRealizar != null) {
				hql.append(" and rece.documentoCobrancaItem.creditoDebitoARealizar.chavePrimaria = :idCredidoDebitoRealizar ");
			}

			if (documentoInexistente != null && documentoInexistente) {
				hql.append(" and rece.faturaGeral is null ");
				hql.append(" and rece.documentoCobrancaItem is null ");
			}

			if (listaRecebimentoSituacao != null) {
				hql.append(" and rece.recebimentoSituacao.chavePrimaria in (:listaRecebimentoSituacao) ");
			}
		}

		if (ordenacao != null) {

			hql.append(" order by  ");
			hql.append(ordenacao);

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (filtro != null && !filtro.isEmpty()) {

			if (idFaturaGeral != null) {
				query.setLong(ID_FATURA_GERAL, idFaturaGeral);
			}

			if (idCredidoDebitoRealizar != null) {
				query.setLong(ID_CREDIDO_DEBITO_REALIZAR, idCredidoDebitoRealizar);
			}

			if (listaRecebimentoSituacao != null) {
				query.setParameterList("listaRecebimentoSituacao", listaRecebimentoSituacao);
			}
		}

		Collection<Object> lista = query.list();

		if (propriedadesLazy != null && propriedadesLazy.length > 0) {

			lista = inicializarLazyColecao(lista, propriedadesLazy);
		}

		return (Collection<Recebimento>) (Collection<?>) lista;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento
	 * #consultarRecebimento(
	 * br.com.ggas.faturamento.FaturaGeral)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Recebimento> consultarRecebimentoPorSituacao(FaturaGeral faturaGeral, String codigoClassificado,
					String codigoValorNaoConfere) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" faturaGeral.chavePrimaria = ? and ");
		hql.append(" recebimentoSituacao.chavePrimaria in (:codigoSituacao1, :codigoSituacao2) ");
		hql.append(" order by dataRecebimento asc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(0, faturaGeral.getChavePrimaria());
		query.setLong("codigoSituacao1", Long.parseLong(codigoClassificado));
		query.setLong("codigoSituacao2", Long.parseLong(codigoValorNaoConfere));

		return query.list();
	}

	@Override
	public void inserirRecebimento(Recebimento recebimento)
			throws GGASException {
		this.inserir(recebimento.getAvisoBancario());
		this.atualizar(recebimento.getFaturaGeral().getFaturaAtual());
		this.inserir(recebimento);
	}

	/**
	 * Metodo responsável por inserir um
	 * recebimento.
	 *
	 * @param recebimento
	 *            the recebimento
	 * @param chavesPrimariasFaturas
	 *            the chaves primarias faturas
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the long[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Override
	public Long[] inserirRecebimento(Recebimento recebimento, Long[] chavesPrimariasFaturas, DadosAuditoria dadosAuditoria)
					throws GGASException {

		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ControladorIntegracao controladorIntegracao =
						(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(
										ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
		Long[] chavesRecebimentos = null;
		// Verifica se há itens selecionados
		if (chavesPrimariasFaturas == null || chavesPrimariasFaturas.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

		if (recebimento.getDataRecebimento() != null) {
			validarDataRecebimento(recebimento.getDataRecebimento());
		}

		BigDecimal valorRecebimentoRestante = recebimento.getValorRecebimento();
		Collection<Fatura> faturas = controladorCobranca.consultarFaturas(chavesPrimariasFaturas);

		// Verifica se há itens selecionados
		if (faturas == null || faturas.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

		verificaItensSelecionados(recebimento);

		ConstanteSistema recebimentoValorNaoConfere =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_RECEBIMENTO_VALOR_NAO_CONFERE);
		RecebimentoSituacao situacaoValorNaoConfere = this.obterRecebimentoSituacao(Long.valueOf(recebimentoValorNaoConfere.getValor()));
		ConstanteSistema constanteRecebimentoSituacaoBaixaPorDacao =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_RECEBIMENTO_BAIXA_POR_DACAO);
		RecebimentoSituacao recebimentoSituacaoBaixaPorDacao =
						this.obterRecebimentoSituacao(Long.valueOf(constanteRecebimentoSituacaoBaixaPorDacao.getValor()));

		controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		this.inserir(recebimento.getAvisoBancario());

		Iterator<Fatura> faturaIterator = faturas.iterator();
		Recebimento novoRecebimento = null;

		while (faturaIterator.hasNext()) {

			Fatura fatura = faturaIterator.next();
			BigDecimal valorTotal =
							getControladorFatura().calcularSaldoFatura(fatura, recebimento.getDataRecebimento(), true, false).getSecond();

			if (valorRecebimentoRestante.compareTo(BigDecimal.ZERO) > 0) {

				novoRecebimento = (Recebimento) this.criar();
				ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);
				Util.copyProperties(novoRecebimento, recebimento);

				novoRecebimento.setFaturaGeral(fatura.getFaturaGeral());
				novoRecebimento.setUltimaAlteracao(Calendar.getInstance().getTime());
				if (recebimento.getRecebimentoSituacao() == null) {
					novoRecebimento.setRecebimentoSituacao(situacaoValorNaoConfere);
				} else {
					novoRecebimento.setRecebimentoSituacao(recebimentoSituacaoBaixaPorDacao);
				}
				novoRecebimento.setValorPrincipal(fatura.getValorSaldoConciliado());

				if (valorRecebimentoRestante.compareTo(valorTotal) < 0) {

					novoRecebimento.setValorRecebimento(valorRecebimentoRestante);
					valorRecebimentoRestante = BigDecimal.ZERO;

				} else {

					valorRecebimentoRestante = verificaExistenciaFaturas(valorRecebimentoRestante, faturaIterator,
							novoRecebimento, valorTotal);

				}

				novoRecebimento.setCliente(fatura.getCliente());
				recebimento.setCliente(fatura.getCliente());

				novoRecebimento.setPontoConsumo(fatura.getPontoConsumo());
				recebimento.setPontoConsumo(fatura.getPontoConsumo());
				novoRecebimento.setDadosAuditoria(dadosAuditoria);

				ResultadoClassificacao resultadoClassificacao = this.classificarRecebimento(novoRecebimento, false, true);
				Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado = resultadoClassificacao.getListaCreditoDebitoNegociado();

				novoRecebimento.setChavePrimaria(this.inserir(novoRecebimento));

				for (CreditoDebitoNegociado creditoDebitoNegociado : listaCreditoDebitoNegociado) {
					creditoDebitoNegociado.setRecebimento(novoRecebimento);
					this.atualizar(creditoDebitoNegociado, getClasseEntidadeCreditoDebitoNegociado());
				}

				this.registrarLancamentoContabil(novoRecebimento, false, null);
				resultadoClassificacao.getRegistradorLancamentoContabil().registrar();

				ControladorApuracaoPenalidade controladorApuracaoPenalidade =
								(ControladorApuracaoPenalidade) ServiceLocator.getInstancia().getControladorNegocio(
												ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);
				ControladorRecebimento controladorRecebimento =
								(ControladorRecebimento) ServiceLocator.getInstancia().getControladorNegocio(
												ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);
				ConstanteSistema recebimentoClassificado =
								controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_RECEBIMENTO_CLASSIFICADO);

				RecebimentoSituacao recebimentoSituacaoClassificado =
								controladorRecebimento.obterRecebimentoSituacao(Long.valueOf(recebimentoClassificado.getValor()));

				verificaExistenciaContrato(dadosAuditoria, novoRecebimento, fatura, controladorApuracaoPenalidade,
						recebimentoSituacaoClassificado);
			}
			controladorIntegracao.gerarBaixaTitulo(novoRecebimento, Boolean.FALSE, null);
		}

		return chavesRecebimentos;
	}

	private void verificaExistenciaContrato(DadosAuditoria dadosAuditoria, Recebimento novoRecebimento, Fatura fatura,
			ControladorApuracaoPenalidade controladorApuracaoPenalidade,
			RecebimentoSituacao recebimentoSituacaoClassificado) throws NegocioException, ConcorrenciaException {
		if (fatura.getContrato() != null) {

			Contrato contrato = fatura.getContrato();

			if (novoRecebimento.getRecebimentoSituacao().equals(recebimentoSituacaoClassificado)) {
				controladorApuracaoPenalidade.atualizarQuantidadePagaNaoRecuperada(novoRecebimento, dadosAuditoria, contrato);
			}

		}
	}

	private BigDecimal verificaExistenciaFaturas(BigDecimal valorRecebimento, Iterator<Fatura> faturaIterator,
			Recebimento novoRecebimento, BigDecimal valorTotal) {
		BigDecimal valorRecebimentoRestante = valorRecebimento;

		if (faturaIterator.hasNext()) {
			novoRecebimento.setValorRecebimento(valorTotal);
			valorRecebimentoRestante = valorRecebimentoRestante.subtract(valorTotal);
		} else {
			novoRecebimento.setValorRecebimento(valorRecebimentoRestante);
			valorRecebimentoRestante = BigDecimal.ZERO;
		}
		return valorRecebimentoRestante;
	}

	private void verificaItensSelecionados(Recebimento recebimento) throws NegocioException {
		if (recebimento.getValorExcedente() == null) {
			recebimento.setValorExcedente(BigDecimal.ZERO);
		}

		if (recebimento.getAvisoBancario().getContaBancaria() == null) {
			throw new NegocioException(Constantes.ARRECADADOR_SEM_CONTRATO, true);
		}

		if (recebimento.getObservacao().length() > LIMITE_OBSERVACAO) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ESTOURO_LIMITE_SUPERIOR_VALOR_PERCENTUAL, true);
		}
	}

	/**
	 * Metodo responsável por restornar uma lista
	 * de recebimentos.
	 *
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	@Override
	public void estornarRecebimento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws GGASException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		ControladorIntegracao controladorIntegracao =
						(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(
										ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

		// Verifica se há itens selecionados
		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

		//TODO: Refactory nesse método, fazer um metodo que consulte uma unica vez, passando as chaves primárias
		Collection<Recebimento> colecaoRecebimento = new ArrayList<>();
		for (int i = 0; i < chavesPrimarias.length; i++) {
			if (chavesPrimarias[i] > 0) {
				colecaoRecebimento.add((Recebimento) this.obter(chavesPrimarias[i]));
			}
		}

		if (colecaoRecebimento.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

		// Para cada recebimento selecionado atualizada a "Situação do Documento" e o "Indicador" do recebimento
		for (Recebimento recebimento : colecaoRecebimento) {

			if (!recebimento.isBaixado()) {
				throw new NegocioException(ERRO_RECEBIMENTO_NAO_BAIXADO, true);
			}

			ConstanteSistema recebimentoPendente =
							getControladorConstanteSistema().obterConstantePorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);

			ConstanteSistema recebimentoParcialmentePago =
							getControladorConstanteSistema().obterConstantePorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);
			ConstanteSistema recebimentoPago =
							getControladorConstanteSistema().obterConstantePorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);
			String codigoRecebimentoValorNaoConfere =
							getControladorConstanteSistema()
											.obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_VALOR_NAO_CONFERE);
			String codigoRecebimentoClassificado =
							getControladorConstanteSistema().obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_CLASSIFICADO);

			boolean recebimentoAtualizado = false;

			if (recebimento.getFaturaGeral() != null) {

				Fatura fatura = (Fatura) controladorCobranca.obterFatura(recebimento.getFaturaGeral().getFaturaAtual().getChavePrimaria());

				// buscar outros recebimentos para a fatura: rest_cd in (1,6) e não for o atual
				// somaRecebimentos = consultarRecebimento()
				BigDecimal somatorioRecebimentos = BigDecimal.ZERO;
				Long parametroRecebimento = recebimento.getChavePrimaria();
				Collection<Recebimento> recebimentos =
								consultarRecebimentoPorSituacao(recebimento.getFaturaGeral(), codigoRecebimentoClassificado,
												codigoRecebimentoValorNaoConfere);
				somatorioRecebimentos = calcularSomatorioRecebimentos(somatorioRecebimentos, recebimentos, parametroRecebimento);

				// se fatura.getValorConciliado() == 0 && somaRecebimentos == 0 => pendente
				// senao, se fatura.getValorSaldoConciliado() <= somaRecebimentos => quitado
				// senao parcialmente quitado

				if ((fatura.getValorConciliado().compareTo(BigDecimal.ZERO) == 0)
								&& (somatorioRecebimentos.compareTo(BigDecimal.ZERO) == 0)) {
					EntidadeConteudo situacaoPagamentoPendente =
									controladorEntidadeConteudo.obter(Long.valueOf(recebimentoPendente.getValor()));
					fatura.setSituacaoPagamento(situacaoPagamentoPendente);
				} else if (fatura.getValorSaldoConciliado().compareTo(somatorioRecebimentos) <= 0) {
					EntidadeConteudo situacaoPago = controladorEntidadeConteudo.obter(Long.valueOf(recebimentoPago.getValor()));
					fatura.setSituacaoPagamento(situacaoPago);
				} else {
					EntidadeConteudo situacaoPagamentoPagoParcial =
									controladorEntidadeConteudo.obter(Long.valueOf(recebimentoParcialmentePago.getValor()));
					fatura.setSituacaoPagamento(situacaoPagamentoPagoParcial);
				}
				fatura.setDadosAuditoria(dadosAuditoria);
				Recebimento recebimentoFatura = Util.primeiroElemento(fatura.getFaturaGeral().getListaRecebimento());

				recebimentoAtualizado = true;
				setAtributosRecebimentoEstornado(dadosAuditoria, recebimentoFatura);
				atualizar(fatura, getClasseEntidadeFatura());

			} else if (recebimento.getDocumentoCobrancaItem() != null) {

				DocumentoCobrancaItem documentoCobrancaItem =
								controladorCobranca.obterDocumentoCobrancaItem(recebimento.getDocumentoCobrancaItem().getChavePrimaria());

				FaturaGeral faturaGeral = documentoCobrancaItem.getFaturaGeral();
				CreditoDebitoARealizar creditoDebitoARealizar = documentoCobrancaItem.getCreditoDebitoARealizar();

				// FIXME: [jgfm] Fazer refactory, colocar essa situação no classificar
				if (faturaGeral != null && creditoDebitoARealizar == null) {
					Fatura fatura = faturaGeral.getFaturaAtual();

					verificaValorConciliadoRecebimento(controladorEntidadeConteudo, recebimento, recebimentoPendente,
							recebimentoParcialmentePago, fatura);

					fatura.setDadosAuditoria(dadosAuditoria);
					atualizar(fatura, getClasseEntidadeFatura());
				} else if (creditoDebitoARealizar != null && faturaGeral == null) {

					verificaValorRecebidoComDevido(controladorEntidadeConteudo, recebimento, recebimentoPendente,
							recebimentoParcialmentePago, creditoDebitoARealizar);

					creditoDebitoARealizar.setUltimaAlteracao(Calendar.getInstance().getTime());
					atualizar(creditoDebitoARealizar, getClasseEntidadeCreditoDebitoARealizar());

				}

			}

			RecebimentoSituacao recebimentoSituacaoAntes = recebimento.getRecebimentoSituacao();

			Segmento segmentoEstorno = null;
			if (recebimento.getFaturaGeral() != null) {
				segmentoEstorno = recebimento.getFaturaGeral().getFaturaAtual().getSegmento();
			}

			setAtributosRecebimentoEstornado(dadosAuditoria, recebimento);
			if (!recebimentoAtualizado) {
				this.atualizar(recebimento);
			}

			Long codigoSituacaoRecebimentoDocumentoInexistente =
							controladorParametroSistema.obterParametroCodigoSituacaoRecebimentoDocumentoInexistente().getValorLong();

			if (recebimentoSituacaoAntes.getChavePrimaria() == codigoSituacaoRecebimentoDocumentoInexistente) {
				this.registrarLancamentoContabil(recebimento, Boolean.TRUE, segmentoEstorno);
			} else {
				this.registrarLancamentoContabil(recebimento, Boolean.FALSE, segmentoEstorno);
			}

			controladorIntegracao.gerarBaixaTitulo(recebimento, Boolean.TRUE, null);

		}
	}

	private void verificaValorRecebidoComDevido(ControladorEntidadeConteudo controladorEntidadeConteudo,
			Recebimento recebimento, ConstanteSistema recebimentoPendente, ConstanteSistema recebimentoParcialmentePago,
			CreditoDebitoARealizar creditoDebitoARealizar) {
		if (creditoDebitoARealizar.getValor().compareTo(recebimento.getValorRecebimento()) == 0) {
			EntidadeConteudo situacaoPagamentoPendente =
							controladorEntidadeConteudo.obter(Long.valueOf(recebimentoPendente.getValor()));
			creditoDebitoARealizar.setSituacaoPagamento(situacaoPagamentoPendente);
		} else {
			EntidadeConteudo situacaoPagamentoPagoParcial =
							controladorEntidadeConteudo.obter(Long.valueOf(recebimentoParcialmentePago.getValor()));
			creditoDebitoARealizar.setSituacaoPagamento(situacaoPagamentoPagoParcial);
		}
	}

	private void verificaValorConciliadoRecebimento(ControladorEntidadeConteudo controladorEntidadeConteudo,
			Recebimento recebimento, ConstanteSistema recebimentoPendente, ConstanteSistema recebimentoParcialmentePago,
			Fatura fatura) {
		if (fatura.getValorSaldoConciliado().compareTo(recebimento.getValorRecebimento()) == 0) {
			EntidadeConteudo situacaoPagamentoPendente =
							controladorEntidadeConteudo.obter(Long.valueOf(recebimentoPendente.getValor()));
			fatura.setSituacaoPagamento(situacaoPagamentoPendente);
		} else {
			EntidadeConteudo situacaoPagamentoPagoParcial =
							controladorEntidadeConteudo.obter(Long.valueOf(recebimentoParcialmentePago.getValor()));
			fatura.setSituacaoPagamento(situacaoPagamentoPagoParcial);
		}
	}

	private void setAtributosRecebimentoEstornado(DadosAuditoria dadosAuditoria, Recebimento recebimento) throws NegocioException {

		String valorParametro = getControladorConstanteSistema().obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_ESTORNADO);

		RecebimentoSituacao recebimentoSituacaoEstornado = obterRecebimentoSituacao(Long.valueOf(valorParametro));

		recebimento.setBaixado(false);
		recebimento.setRecebimentoSituacao(recebimentoSituacaoEstornado);

		recebimento.setDocumentoCobrancaItem(null);

		if (recebimento.getDevolucao() != null) {
			if ((recebimento.getValorRecebimento() != null) && (recebimento.getValorExcedente() != null)) {
				recebimento.setValorExcedente(recebimento.getValorRecebimento().subtract(recebimento.getValorExcedente()));
			}
		} else {
			recebimento.setValorExcedente(recebimento.getValorRecebimento());
		}
		recebimento.setDadosAuditoria(dadosAuditoria);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento#
	 * obterTiposConvenioPorArrecadador(java.lang.Long
	 * )
	 */
	@Override
	public Collection<EntidadeConteudo> obterTiposConvenioPorArrecadador(Long chaveArrecadador) throws NegocioException {

		Collection<EntidadeConteudo> tiposConvenio = null;
		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();

		Arrecadador arrecadador = (Arrecadador) controladorArrecadacao.obter(chaveArrecadador, getClasseEntidadeArrecadador());
		if (arrecadador.getBanco() == null) {
			tiposConvenio = this.consultarTiposConvenio(EntidadeConteudo.CODIGO_TIPO_CONVENIO_CLIENTE.toUpperCase());
		} else {
			tiposConvenio = this.consultarTiposConvenio(EntidadeConteudo.CODIGO_TIPO_CONVENIO_BANCO.toUpperCase());
		}
		return tiposConvenio;

	}

	/**
	 * Consultar tipos convenio.
	 *
	 * @param codigo
	 *            the codigo
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<EntidadeConteudo> consultarTiposConvenio(String codigo) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeConteudo().getSimpleName());
		hql.append(WHERE);
		hql.append(" upper(codigo) like :CODIGO");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CODIGO", codigo);
		return query.list();
	}

	/**
	 * Validar data recebimento.
	 *
	 * @param dataRecebimento
	 *            the data recebimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void validarDataRecebimento(Date dataRecebimento) throws NegocioException {

		Date dataHoje = new Date(System.currentTimeMillis());
		if (dataRecebimento.compareTo(dataHoje) > 0) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_RECEBIMENTO_INICIO_INVALIDA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.recebimento.ControladorRecebimento#validarDadosRecebimento(java.lang.Long, java.lang.Long,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void validarDadosRecebimento(Long idArrecadador, Long idFormaArrecadacao, String dataRecebimentoString,
					String valorRecebimentoString) throws NegocioException {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (idArrecadador == null || idArrecadador < 0) {
			stringBuilder.append("Arrecadador");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (idFormaArrecadacao == null || idFormaArrecadacao < 0) {
			stringBuilder.append("Tipo de Convênio");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (dataRecebimentoString == null || "".equals(dataRecebimentoString)) {
			stringBuilder.append("Data do Recebimento");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (valorRecebimentoString == null || "".equals(valorRecebimentoString)) {
			stringBuilder.append("Valor do Recebimento");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();
		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		if (!erros.isEmpty()) {
			throw new NegocioException(erros);
		}
	}

	/**
	 * Metodo responsável por atualizar um
	 * recebimento.
	 *
	 * @param recebimento
	 *            the recebimento
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Override
	public void atualizarRecebimento(Recebimento recebimento) throws GGASException {

		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(recebimento);

		this.classificarRecebimento(recebimento, false);

		ControladorIntegracao controladorIntegracao =
						(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(
										ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
		controladorIntegracao.gerarBaixaTitulo(recebimento, Boolean.FALSE, null);

	}

	/**
	 * Metodo responsável por atualizar um
	 * recebimento.
	 *
	 * @param recebimento
	 * @param chavesPrimariasFaturas
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	@Override
	public void atualizarRecebimentos(Recebimento recebimentoAtual, Long[] colecaoChavesFatura) throws GGASException {

		//

		// by
		// gmatos
		// on
		// 15/10/09
		// 10:22
		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();
		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Recebimento recebimento = recebimentoAtual;
		BigDecimal valorRecebimentoRestante = recebimento.getValorRecebimento();
		String codigoSituacaoRecebimentoValorNaoConfere =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_VALOR_NAO_CONFERE);
		RecebimentoSituacao situacaoValorNaoConfere = this.obterRecebimentoSituacao(Long.valueOf(codigoSituacaoRecebimentoValorNaoConfere));
		List<Fatura> faturas = new ArrayList<>();

		if (colecaoChavesFatura == null || colecaoChavesFatura.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_FATURA_NAO_SELECIONADA_RECEBIMENTO, true);
		}

		if (recebimento.getObservacao() != null && recebimento.getObservacao().length() > LIMITE_OBSERVACAO) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ESTOURO_VALOR_CAMPO_TAMANHO_MAXIMO, new Object[] {
																													"Observação", "300"});
		}

		if (recebimento.getValorRecebimento().compareTo(BigDecimal.ZERO) == 0) {
			throw new NegocioException(ControladorRecebimento.ERRO_NEGOCIO_VALOR_RECEBIMENTO_ZERO, true);
		}

		if (recebimento.getDataRecebimento() != null) {
			validarDataRecebimento(recebimento.getDataRecebimento());
		}

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		faturas.addAll(controladorFatura.consultarFaturasPorChaves(colecaoChavesFatura));

		Collections.sort(faturas, new Comparator<Fatura>(){

			@Override
			public int compare(Fatura fatura1, Fatura fatura2) {

				if (fatura1.getValorSaldoConciliado() != null && fatura2.getValorSaldoConciliado() != null) {
					return fatura1.getValorSaldoConciliado().compareTo(fatura2.getValorSaldoConciliado());
				} else if (fatura1.getValorSaldoConciliado() != null) {
					return 1;
				} else {
					return -1;
				}
			}
		});

		// Consulta todos os recebimentos da fatura antiga
		if (recebimento.getFaturaGeral() != null) {
			Collection<Recebimento> recebimentos = this.consultarRecebimento(recebimento.getFaturaGeral());

			// Soma o valor de todos os recebimentos da fatura antiga, exceto o atual
			BigDecimal somatorioRecebimentos = BigDecimal.ZERO;
			somatorioRecebimentos = somaValorRecebimentosFaturaAntiga(recebimento, recebimentos, somatorioRecebimentos);

			// se fatura.getValorConciliado() == 0 && somaRecebimentos == 0 => pendente
			// senao, se fatura.getValorSaldoConciliado() <= somaRecebimentos => quitado
			// senao parcialmente quitado
			FaturaGeral faturaGeral = controladorArrecadacao.obterFaturaGeral(recebimento.getFaturaGeral().getChavePrimaria());
			Fatura faturaAtual = faturaGeral.getFaturaAtual();

			if (faturaAtual != null) {
				if ((faturaAtual.getValorSaldoConciliado().compareTo(BigDecimal.ZERO) == 0)
								&& (somatorioRecebimentos.compareTo(BigDecimal.ZERO) == 0)) {
					String codigoSituacaoPagamentoPendente =
									controladorConstanteSistema
													.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
					EntidadeConteudo situcaoPagamentoPendente =
									controladorEntidadeConteudo.obter(Long.valueOf(codigoSituacaoPagamentoPendente));
					faturaAtual.setSituacaoPagamento(situcaoPagamentoPendente);

				} else if (faturaAtual.getValorSaldoConciliado().compareTo(somatorioRecebimentos) <= 0) {
					String codigoSituacaoPagamentoQuitado =
									controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);
					EntidadeConteudo situcaoPagamentoQuitado =
									controladorEntidadeConteudo.obter(Long.valueOf(codigoSituacaoPagamentoQuitado));
					faturaAtual.setSituacaoPagamento(situcaoPagamentoQuitado);

				} else {
					String codigoSituacaoPagamentoParcialmentePago =
									controladorConstanteSistema
													.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);
					EntidadeConteudo situcaoPagamentoPagoParcial =
									controladorEntidadeConteudo.obter(Long.valueOf(codigoSituacaoPagamentoParcialmentePago));
					faturaAtual.setSituacaoPagamento(situcaoPagamentoPagoParcial);

				}

				// Atualiza a fatura anterior
				try {
					faturaAtual.setDadosAuditoria(recebimento.getDadosAuditoria());
					this.getSession().evict(faturaAtual);
					controladorCobranca.atualizarFatura(faturaAtual);
				} catch (ConcorrenciaException e) {
					throw new NegocioException(e);
				}
			}
		}
		/* *************************************************************************************** */

		Iterator<Fatura> faturaIterator = faturas.iterator();
		boolean isPrimeiro = true;
		while (faturaIterator.hasNext()) {

			Fatura fatura = faturaIterator.next();
			// FIXME: Substituir trecho abaixo pela chamada ao método controladorFatura.calcularSaldoFatura(fatura)
			BigDecimal valorTotal = fatura.getValorSaldoConciliado();
			BigDecimal valorRecebimento = controladorCobranca.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
			if (valorRecebimento != null) {
				valorTotal = valorTotal.subtract(valorRecebimento);
			}

			if (valorRecebimentoRestante.compareTo(BigDecimal.ZERO) > 0) {

				ConvertUtils.register(new BigDecimalConverter(null), java.math.BigDecimal.class);

				if (!isPrimeiro) {

					Recebimento novoRecebimento = (Recebimento) this.criar();

					Util.copyProperties(novoRecebimento, recebimento);
					recebimento = novoRecebimento;
					recebimento.setChavePrimaria(0);

				}

				recebimento.setFaturaGeral(fatura.getFaturaGeral());
				recebimento.setUltimaAlteracao(Calendar.getInstance().getTime());
				recebimento.setRecebimentoSituacao(situacaoValorNaoConfere);

				comparaValorTotalrecebimentoRestante(recebimento, valorRecebimentoRestante, faturaIterator, valorTotal);
				valorRecebimentoRestante = valorRecebimentoRestante.subtract(valorTotal);

				if (isPrimeiro) {
					this.atualizar(recebimento);
					isPrimeiro = false;
				} else {
					// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
					getHibernateTemplate().getSessionFactory().getCurrentSession().save(recebimento);
				}

				RecebimentoSituacao recebimentoSituacaoAntes = recebimento.getRecebimentoSituacao();

				this.classificarRecebimento(recebimento, false);

				RecebimentoSituacao recebimentoSituacaoDepois = recebimento.getRecebimentoSituacao();

				// ainda falta verificar o antes e o depois
				if (recebimentoSituacaoAntes.getChavePrimaria() == Long.parseLong(controladorParametroSistema
								.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_SITUACAO_RECEBIMENTO_DOCTO_INEXISTENTE)
								.toString())
								&& recebimentoSituacaoDepois.getChavePrimaria() != Long.parseLong(controladorParametroSistema
												.obterValorDoParametroPorCodigo(
																Constantes.PARAMETRO_CODIGO_SITUACAO_RECEBIMENTO_DOCTO_INEXISTENTE)
												.toString())) {
					this.registrarLancamentoContabil(recebimento, true, null);
				}
			}
		}

		ControladorIntegracao controladorIntegracao =
						(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(
										ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
		controladorIntegracao.gerarBaixaTitulo(recebimento, Boolean.FALSE, null);
	}

	private void comparaValorTotalrecebimentoRestante(Recebimento recebimento, BigDecimal valorRecebimentoRestante,
			Iterator<Fatura> faturaIterator, BigDecimal valorTotal) {
		if (valorTotal.compareTo(valorRecebimentoRestante) < 0) {
			recebimento.setValorRecebimento(valorRecebimentoRestante);
		} else {
			if (faturaIterator.hasNext()) {
				recebimento.setValorRecebimento(valorTotal);
			} else {
				recebimento.setValorRecebimento(valorRecebimentoRestante);
			}
		}
	}

	private BigDecimal somaValorRecebimentosFaturaAntiga(Recebimento recebimento, Collection<Recebimento> recebimentos,
			BigDecimal somatorioValorRecebimentos) {

		BigDecimal somatorioRecebimentos = somatorioValorRecebimentos;
		if (recebimentos != null) {
			for (Recebimento recebimentoFatura : recebimentos) {
				if (recebimentoFatura.getChavePrimaria() == recebimento.getChavePrimaria()) {
					getHibernateTemplate().getSessionFactory().getCurrentSession().evict(recebimentoFatura);
				} else {
					somatorioRecebimentos = somatorioRecebimentos.add(recebimentoFatura.getValorRecebimento());
				}
			}
		}
		return somatorioRecebimentos;
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento#
	 * gerarRelatorioReciboQuitacao
	 * (br.com.ggas.arrecadacao.recebimento.
	 * Recebimento,
	 * java.lang.Long[])
	 */
	@Override
	public byte[] gerarRelatorioReciboQuitacao(Collection<Recebimento> listaRecebimentos, Long[] chavesFaturas) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();

		if ((chavesFaturas == null) || (chavesFaturas.length == 0)) {
			throw new NegocioException(ERRO_NEGOCIO_RECEBIMENTO_SEM_FATURA, true);
		}

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long idValorNaoConfere =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_VALOR_NAO_CONFERE));

		byte[] relatoriosReceboQuitacao = null;
		List<byte[]> listaRelatorioRecibo = new ArrayList<>();

		for (Recebimento recebimento : listaRecebimentos) {
			Map<String, Object> parametros = new HashMap<>();
			verificaRecebimento(controladorCliente, recebimento, parametros);

			Map<String, Object> filtro = new HashMap<>();
			filtro.put("principal", Boolean.TRUE);
			Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);

			String strCidade = null;

			if ((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
				Empresa empresaPrincipal = ((List<Empresa>) listaEmpresas).get(0);
				Cliente clienteEmpresa = empresaPrincipal.getCliente();
				Cep cepEmpresa = null;
				parametros.put("numeroDocEmitente", clienteEmpresa.getNumeroDocumentoFormatado());
				parametros.put("emitente", clienteEmpresa.getNome());
				if (clienteEmpresa.getEnderecoPrincipal() != null) {
					cepEmpresa = clienteEmpresa.getEnderecoPrincipal().getCep();
					strCidade = cepEmpresa.getNomeMunicipio();
					parametros.put("cidadeCepEstado", strCidade + " - " + cepEmpresa.getCep() + " - " + cepEmpresa.getUf());
					parametros.put("endereco", clienteEmpresa.getEnderecoPrincipal().getEnderecoFormatado());
				}
			}

			BigDecimal valor = recebimento.getValorRecebimento();
			String valorRecebimentoString = Util.converterCampoCurrencyParaString(valor, Constantes.LOCALE_PADRAO);
			parametros.put("valor", valorRecebimentoString);
			Extenso valorRecebimento = new Extenso(valor);
			parametros.put("valorExtenso", valorRecebimento.toString());

			if (recebimento.getRecebimentoSituacao().getChavePrimaria() == idValorNaoConfere) {
				parametros.put("situacaoPagamento", "parcial");
			} else {
				parametros.put("situacaoPagamento", "total");
			}

			Fatura fatura = recebimento.getFaturaGeral().getFaturaAtual();
			String numeroFaturas = "";
			if (fatura != null) {
				numeroFaturas = String.valueOf(fatura.getChavePrimaria());
				parametros.put("numeroFaturas", numeroFaturas);
			}

			Calendar calendar = Calendar.getInstance(Constantes.LOCALE_PADRAO);
			calendar.setTime(recebimento.getDataRecebimento());
			String diaMes = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
			if (calendar.get(Calendar.DAY_OF_MONTH) < DECIMO_DIA) {
				diaMes = "0" + diaMes;
			}
			String mes = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Constantes.LOCALE_PADRAO);
			String ano = String.valueOf(calendar.get(Calendar.YEAR));

			String dataRecebimento = strCidade + ", " + diaMes + " de " + mes + " de " + ano;
			parametros.put(DATA_RECEBIMENTO, dataRecebimento);

			List<Recebimento> listaRecebimento = new ArrayList<>();
			listaRecebimento.add(recebimento);
			byte[] reciboQuitacao = RelatorioUtil.gerarRelatorioPDF(listaRecebimento, parametros, RELATORIO_RECIBO_QUITACAO);
			listaRelatorioRecibo.add(reciboQuitacao);
		}

		relatoriosReceboQuitacao = RelatorioUtil.unificarRelatoriosPdf(listaRelatorioRecibo);

		return relatoriosReceboQuitacao;
	}

	/**
	 *
	 * @param controladorCliente
	 * @param recebimento
	 * @param parametros
	 * @throws NegocioException
	 */
	private void verificaRecebimento(ControladorCliente controladorCliente, Recebimento recebimento,
			Map<String, Object> parametros) throws NegocioException {
		if (recebimento.getCliente() != null) {
			Cliente cliente = (Cliente) controladorCliente.obter(recebimento.getCliente().getChavePrimaria(), "enderecos");
			parametros.put("nomeCliente", cliente.getNome());
			if (cliente.getNumeroDocumentoFormatado() != null) {
				parametros.put("numeroDocumentacao", cliente.getNumeroDocumentoFormatado());
			} else {
				parametros.put("numeroDocumentacao", "");
			}

			verificaEndedrecoPrincipal(parametros, cliente);
		}
	}

	/**
	 *
	 * @param parametros
	 * @param cliente
	 */
	private void verificaEndedrecoPrincipal(Map<String, Object> parametros, Cliente cliente) {
		Cep cep;
		if (cliente.getEnderecoPrincipal() != null) {
			cep = cliente.getEnderecoPrincipal().getCep();
			if (!StringUtils.isEmpty(cep.getNomeMunicipio())) {
				parametros.put("cidade", cep.getNomeMunicipio());
			} else {
				parametros.put("cidade", "");
			}
			if ((cep.getTipoLogradouro() != null) && (cep.getLogradouro() != null)) {
				parametros.put("rua", cep.getTipoLogradouro() + " " + cep.getLogradouro());
			} else {
				parametros.put("rua", "");
			}
			if (cliente.getEnderecoPrincipal().getNumero() != null) {
				parametros.put("numero", cliente.getEnderecoPrincipal().getNumero());
			} else {
				parametros.put("numero", "");
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento#
	 * obterValorRecebimentoPelaCreditoDebitoARealizar
	 * (java.lang.Long)
	 */
	@Override
	public BigDecimal obterValorRecebimentoPelaCreditoDebitoARealizar(Long idCreditoDebitoARealizar) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select sum(recebimento.valorRecebimento) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName()).append(" recebimento ");
		hql.append(WHERE);
		hql.append(" recebimento.documentoCobrancaItem.creditoDebitoARealizar = :idCreditoDebitoARelizar");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCreditoDebitoARelizar", idCreditoDebitoARealizar);

		return (BigDecimal) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento#
	 * obterDataPagamentoPeloDocumentoCobranca(java
	 * .lang.Long)
	 */
	@Override
	public Date obterDataPagamentoPeloDocumentoCobranca(Long idCreditoDebitoARealizar) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append("select max(recebimento.dataRecebimento)");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" recebimento");
		hql.append(" where recebimento.documentoCobrancaItem.creditoDebitoARealizar.chavePrimaria = :idCreditoDebitoARealizar");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idCreditoDebitoARealizar", idCreditoDebitoARealizar);

		return (Date) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento
	 * #obterDataPagamentoPelaFatura
	 * (java.lang.Long)
	 */
	@Override
	public Date obterDataPagamentoPelaFatura(Long idCreditoDebitoARealizar) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append("select max(recebimento.dataRecebimento)");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" recebimento");
		hql.append(" left join recebimento.faturaGeral.faturaAtual.listaFaturaItem lista ");
		hql.append(WHERE);
		hql.append(" lista.creditoDebitoARealizar.chavePrimaria = :idCreditoDebitoARealizar ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idCreditoDebitoARealizar", idCreditoDebitoARealizar);

		return (Date) query.uniqueResult();
	}

	/**
	 * Método responsável por retornar a data de recebimento mais recente de um Recebimento que pertence a uma FaturaGeral.
	 *
	 * @param faturasGerais - {@link Collection}
	 * @return Data de Recebimento por código da Fatura Geral - {@link Map}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@Override
	public Map<Long, Collection<Date>> consultarDataPagamentoRecebimentoPorCodigoFaturaGeral(Collection<FaturaGeral> faturasGerais)
					throws NegocioException {

		Map<Long, Collection<Date>> dataPagamentoPorFaturaGeral = null;

		if (!Util.isNullOrEmpty(faturasGerais)) {

			String valorConstante =
							getControladorConstanteSistema().obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_CLASSIFICADO);

			StringBuilder hql = new StringBuilder();

			hql.append(" select faturaGeral.chavePrimaria as faturaGeral_chavePrimaria, ");
			hql.append(" max(recebimento.dataRecebimento) ");
			hql.append(FROM);
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" recebimento ");
			hql.append(" inner join recebimento.faturaGeral faturaGeral ");
			hql.append(" inner join recebimento.recebimentoSituacao situacao ");
			hql.append(WHERE);
			hql.append(" recebimento.devolucao is null ");
			if (valorConstante != null) {
				hql.append(" and situacao.chavePrimaria = :codigoSituacao ");
			}
			hql.append(" and faturaGeral IN (:faturasGerais) ");
			hql.append(" group by faturaGeral.chavePrimaria ");

			Query query = getSession().createQuery(hql.toString());

			query.setParameterList("faturasGerais", faturasGerais);
			query.setResultTransformer(new GGASTransformer(Date.class, "faturaGeral_chavePrimaria"));

			if (valorConstante != null) {
				query.setLong("codigoSituacao", Long.parseLong(valorConstante));
			}

			dataPagamentoPorFaturaGeral = (Map<Long, Collection<Date>>) query.list().get(0);
		}

		return dataPagamentoPorFaturaGeral;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.arrecadacao.recebimento.
	 * ControladorRecebimento
	 * #registrarLancamentoContabil
	 * (br.com.ggas.arrecadacao.recebimento.
	 * Recebimento, boolean)
	 */
	@Override
	public void registrarLancamentoContabil(Recebimento recebimento, boolean isPosterior, Segmento segmentoEstorno) throws GGASException {

		// O Sistema obtém a Referência Contábil
		ControladorParametroSistema controladorParametroSistema =
						(ControladorParametroSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorArrecadacao controladorArrecadacao = ServiceLocator.getInstancia().getControladorArrecadacao();
		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		Integer referenciaContabil =
						Integer.valueOf((String) controladorParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_CONTABIL));

		LancamentoContabilVO lancamentoContabilVO = new LancamentoContabilVO();
		lancamentoContabilVO.setListaLancamentosContabeisAnaliticos(new ArrayList<LancamentoContabilAnaliticoVO>());

		ControladorContabil controladorContabil =
						(ControladorContabil) ServiceLocator.getInstancia().getBeanPorID(ControladorContabil.BEAN_ID_CONTROLADOR_CONTABIL);

		/*
		 * Se a data da realização do evento for
		 * do mesmo mês e Ano da Referência
		 * Contábil,
		 * então a data contábil do evento será a
		 * própria data da realização do evento
		 */
		if ((Util.obterAnoMes(recebimento.getDataRecebimento())).compareTo(referenciaContabil) == 0) {

			DateTime dataRealizacaoEventodt = new DateTime(recebimento.getDataRecebimento());
			dataRealizacaoEventodt = Util.zerarHorario(dataRealizacaoEventodt);
			lancamentoContabilVO.setDataRealizacaoEvento(dataRealizacaoEventodt.toDate());
		} else {
			/*
			 * Se a data da realização do evento
			 * for diferente do mês e Ano da
			 * Referência Contábil,
			 * então a data contábil do evento
			 * será a data corrente.
			 */
			DateTime dataAtualdt = new DateTime(Calendar.getInstance().getTime());
			dataAtualdt = Util.zerarHorario(dataAtualdt);
			lancamentoContabilVO.setDataRealizacaoEvento(dataAtualdt.toDate());
		}

		if (recebimento.getFaturaGeral() != null) {

			// -------- lancamento contabil de fatura regime contabil de competencia
			Boolean temLancamentoContabil =
							controladorContabil.temLancamentoContabilSinteticoPraFatura(recebimento.getFaturaGeral().getFaturaAtual());
			if (!temLancamentoContabil) {
				ControladorFatura controladorFatura = ServiceLocator.getInstancia().getControladorFatura();
				ControladorConstanteSistema controladorConstanteSistema =
								(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
												ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
				ConstanteSistema constanteSistemaTipoNotaDebito =
								controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_DEBITO);

				Fatura fatura = recebimento.getFaturaGeral().getFaturaAtual();

				if (fatura.getTipoDocumento().getChavePrimaria() == Long.parseLong(constanteSistemaTipoNotaDebito.getValor())) {
					controladorFatura.registrarLancamentoContabil(fatura, OperacaoContabil.INCLUIR_NOTA,
									controladorFatura.parametrosMontarCodigoChaveEventoComercial(), null, Boolean.FALSE);
				} else {
					controladorFatura.registrarLancamentoContabil(fatura, OperacaoContabil.INCLUIR_FATURA,
									parametrosMontarCodigoChaveEventoComercial(), null, Boolean.FALSE);
				}
			}
			// ----------------------------------
			lancamentoContabilVO.setSegmento(recebimento.getFaturaGeral().getFaturaAtual().getSegmento());
		} else {
			lancamentoContabilVO.setSegmento(segmentoEstorno);
		}

		lancamentoContabilVO.setTributo(null);
		lancamentoContabilVO.setDadosAuditoria(recebimento.getDadosAuditoria());

		// Consultando conta bancaria a ser consultada
		Collection<ArrecadadorContratoConvenio> listaACConvenio =
						controladorArrecadacao.consultarACConvenioPorArrecadador(recebimento.getAvisoBancario().getArrecadador()
										.getChavePrimaria(), recebimento.getFormaArrecadacao().getChavePrimaria());
		for (ArrecadadorContratoConvenio arrecadadorContratoConvenio : listaACConvenio) {
			if (arrecadadorContratoConvenio.getContaCredito() != null) {
				lancamentoContabilVO.setContaBancaria(arrecadadorContratoConvenio.getContaCredito());
			}
		}

		if (recebimento.getCliente() != null) {
			lancamentoContabilVO.setCnpj(recebimento.getCliente().getCnpj());
		}

		String codigoChaveEvento = null;

		Map<String, BigDecimal> valoresTotalizados = new LinkedHashMap<>();

		codigoChaveEvento = montarCodigoChaveEventoComercial(recebimento, isPosterior);

		if (valoresTotalizados.containsKey(codigoChaveEvento)) {
			valoresTotalizados.put(codigoChaveEvento, valoresTotalizados.get(codigoChaveEvento).add(recebimento.getValorRecebimento()));
		} else {
			valoresTotalizados.put(codigoChaveEvento, recebimento.getValorRecebimento());
		}

		for (Map.Entry<String, BigDecimal> entry : valoresTotalizados.entrySet()) {
			String chave = entry.getKey();

			lancamentoContabilVO.setEventoComercial(controladorContabil.obterEventoComercial(controladorContabil
							.obterChaveEventoComercial(chave)));

			lancamentoContabilVO.setLancamentoItemContabil(null);

			LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVO = new LancamentoContabilAnaliticoVO();
			lancamentoContabilAnaliticoVO.setCodigoObjeto(recebimento.getChavePrimaria());
			lancamentoContabilAnaliticoVO.setValor(entry.getValue().setScale(DUAS_CASAS_DECIMAIS, BigDecimal.ROUND_HALF_UP));

			lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().clear();
			lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(lancamentoContabilAnaliticoVO);

			// ------------ popular conta Auxiliar e historico contabil
			List<Object> listaObjetos = new ArrayList<>();
			listaObjetos.add(recebimento.getCliente());
			ParametroSistema parametro = controladorParametroSistema.obterParametroPorCodigo(Constantes.CODIGO_TIPO_OPERACAO_SAIDA);
			EntidadeConteudo tipoOperacaoSaida =
							controladorEntidadeConteudo.obterTipoOperacaoDocumentoFiscal(Long.valueOf(parametro.getValor()));
			ControladorFatura controladorFatura =
							(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
			listaObjetos.add(controladorFatura.obterDocumentoFiscalPorFatura(recebimento.getFaturaGeral().getFaturaAtual(),
							tipoOperacaoSaida));
			listaObjetos.add(recebimento);
			controladorContabil.popularContaAuxiliarHistoricoContabil(lancamentoContabilVO, listaObjetos);
			// ---------------------------------------------------

			// --------- Populando Eventocomercial lancamento no lancamentocontabilVO para no inserir verificar o regime contabil --------
			Map<String, Object> filtro = controladorContabil.consultarEventoComercialLancamentoPorFiltro(lancamentoContabilVO);
			EventoComercialLancamento eventoComercialLancamento = controladorContabil.consultarEventoComercialLancamentoPorFiltro(filtro);
			lancamentoContabilVO.setEventoComercialLancamento(eventoComercialLancamento);
			// ----------------------------------------------------------------------------------------------------------------------------

			controladorContabil.inserirLancamentoContabilSintetico(lancamentoContabilVO);

			if (recebimento.getFaturaGeral() != null && recebimento.getFaturaGeral().getFaturaAtual() != null) {

				controladorContabil.relizarBaixaContasPDD(recebimento.getFaturaGeral().getFaturaAtual().getChavePrimaria(),
								Constantes.C_PROV_DEV_DUV_MOTIVO_BAIXA_RECEBIMENTO, lancamentoContabilVO.getEventoComercial());

			}

		}

	}

	/**
	 * Monta, a partir dos parâmetros informados,
	 * o código para recuperar a chave do
	 * evento comercial no mapa de eventos.
	 *
	 * @param recebimento
	 *            the recebimento
	 * @param isPosterior
	 *            the is posterior
	 * @return codigoChaveEvento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private String montarCodigoChaveEventoComercial(Recebimento recebimento, boolean isPosterior)
					throws NegocioException {

		ControladorParametroSistema controladorParametroSistema =
						(ControladorParametroSistema) ServiceLocator.getInstancia().getBeanPorID(
										ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long estornado = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_ESTORNADO));
		Long docInexistente =
						Long.valueOf((String) controladorParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_SITUACAO_RECEBIMENTO_DOCTO_INEXISTENTE));

		Long baixaPorDacao =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_RECEBIMENTO_BAIXA_POR_DACAO));

		if (recebimento.getRecebimentoSituacao().getChavePrimaria() == docInexistente) {
			return "EVENTO_COMERCIAL_NAO_IDENTIFICADO";
		} else if (recebimento.getRecebimentoSituacao().getChavePrimaria() == baixaPorDacao) {
			return "EVENTO_COMERCIAL_BAIXA_POR_DACAO";
		} else if (recebimento.getRecebimentoSituacao().getChavePrimaria() == estornado) {
			if (isPosterior) {
				return "EVENTO_COMERCIAL_ESTORNO_IDENTIFICADO_POSTERIOR";
			} else {
				return "EVENTO_COMERCIAL_ESTORNO_IDENTIFICADO";
			}
		} else {
			if (isPosterior) {
				return "EVENTO_COMERCIAL_RECEBIMENTO_IDENTIFICADO_POSTERIOR";
			} else {
				return "EVENTO_COMERCIAL_RECEBIMENTO_IDENTIFICADO";
			}
		}
	}

	/**
	 * Monta um mapa com os parâmetros usados para
	 * definir o evento comercial de uma fatura.
	 *
	 * @return mapParametros
	 */
	private Map<String, Long> parametrosMontarCodigoChaveEventoComercial() {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		Map<String, Long> parametros = new LinkedHashMap<>();

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		parametros.put(Constantes.C_GAS, Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS)));
		parametros.put(Constantes.C_MARGEM_DISTRIBUICAO,
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO)));
		parametros.put(Constantes.C_TRANSPORTE,
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TRANSPORTE)));
		parametros.put(Constantes.C_TIPO_RUBRICA_PARCELAMENTO, Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_RUBRICA_PARCELAMENTO)));
		parametros.put(Constantes.C_DEBITO,
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO)));
		parametros.put(Constantes.C_CREDITO,
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO)));

		return parametros;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.recebimento.ControladorRecebimento#consultarQtdDiasAtrasoRecebimento(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<SubRelatorioTitulosAbertoPorDataVencimentoVO> consultarQtdDiasAtrasoRecebimento(Long idFaturaGeral)
					throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select new br.com.ggas.web.relatorio.tituloemabertopordatavencimento.SubRelatorioTitulosAbertoPorDataVencimentoVO ( ");
		hql.append(" to_date(to_char(rece.dataRecebimento,'dd/MM/yyyy'),'dd/MM/yyyy') - ")
					.append("to_date(to_char(fatGeral.faturaAtual.dataVencimento,'dd/MM/yyyy'),'dd/MM/yyyy'), ")
					.append("rece.valorRecebimento, rece.dataRecebimento) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(RECE);
		hql.append(" join rece.faturaGeral fatGeral ");
		hql.append(WHERE);
		hql.append(" rece.faturaGeral.chavePrimaria = :idFaturaGeral ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (idFaturaGeral != null) {
			query.setLong(ID_FATURA_GERAL, idFaturaGeral);
		}

		Collection<Object> lista = query.list();

		return (Collection<SubRelatorioTitulosAbertoPorDataVencimentoVO>) (Collection<?>) lista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.recebimento.ControladorRecebimento#consultarQtdDiasAtrasoRecebimento(java.util.Map, java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<SubRelatorioPosicaoContasReceberVO> consultarQtdDiasAtrasoRecebimento(Map<String, Object> filtro, String classe)
					throws NegocioException {

		Long idFaturaGeral = null;
		Long[] idsFaturaGeral = null;
		Integer campoDiasAtraso = null;
		Date campoDataReferencia = null;

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select new ").append(classe).append(" ( ");
		hql.append(" rece.dataRecebimento - fatGeral.faturaAtual.dataVencimento, rece.valorRecebimento, rece.dataRecebimento) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(RECE);
		hql.append(" join rece.faturaGeral fatGeral ");
		hql.append(" join fatGeral.faturaAtual fat ");
		hql.append(WHERE);
		hql.append(" 1=1 ");

		if (filtro != null && !filtro.isEmpty()) {

			idFaturaGeral = (Long) filtro.get(ID_FATURA_GERAL);
			if (idFaturaGeral != null && idFaturaGeral > 0) {
				hql.append(" and rece.faturaGeral.chavePrimaria = :idFaturaGeral ");
			}

			idsFaturaGeral = (Long[]) filtro.get(IDS_FATURA_GERAL);
			if (idsFaturaGeral != null && idsFaturaGeral.length > 0) {
				hql.append(" and rece.faturaGeral.chavePrimaria in (:idsFaturaGeral) ");
			}

			campoDataReferencia = (Date) filtro.get("campoDataReferencia");
			if (campoDataReferencia != null) {
				hql.append(" and fat.dataVencimento <=  :campoDataReferencia");
				hql.append(" and rece.dataRecebimento >=  :campoDataReferencia");

			}

			campoDiasAtraso = (Integer) filtro.get("diasAtraso");
			if (campoDiasAtraso != null && !campoDiasAtraso.equals(0)) {
				hql.append(" and rece.dataRecebimento - fat.dataVencimento = :campoDiasAtraso");
			}

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (idFaturaGeral != null) {
			query.setLong(ID_FATURA_GERAL, idFaturaGeral);
		}

		if (idsFaturaGeral != null && idsFaturaGeral.length > 0) {
			query.setParameterList(IDS_FATURA_GERAL, idsFaturaGeral);
		}

		if (campoDataReferencia != null) {
			query.setDate("campoDataReferencia", campoDataReferencia);

		}

		if (campoDiasAtraso != null && !campoDiasAtraso.equals(0)) {
			query.setInteger("campoDiasAtraso", campoDiasAtraso);
		}

		Collection<Object> lista = query.list();

		return (Collection<SubRelatorioPosicaoContasReceberVO>) (Collection<?>) lista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.arrecadacao.recebimento.ControladorRecebimento#obterRecebimentos(java.lang.Integer, java.util.Date,
	 * java.lang.Long[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Recebimento> obterRecebimentos(Integer campoDiasAtraso, Date campoDataReferencia, Long[] idsFaturaGeral) {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rec ");
		hql.append(" left join fetch rec.faturaGeral fatGeral ");
		hql.append(" left join fetch fatGeral.faturaAtual fat ");
		hql.append(WHERE);
		hql.append(" rec.habilitado = true ");
		if (campoDataReferencia != null) {
			hql.append(" and fat.dataVencimento <=  ?");
			hql.append(" and rec.dataRecebimento >=  ?");

		}
		if (!campoDiasAtraso.equals(Integer.valueOf(0))) {
			hql.append(" and rec.dataRecebimento - fat.dataVencimento = ?");
		}

		if (idsFaturaGeral != null && idsFaturaGeral.length > 0) {
			hql.append(" and rec.faturaGeral.chavePrimaria in (:idsFaturaGeral) ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList(IDS_FATURA_GERAL, idsFaturaGeral);
		ajustarFiltroQuery(campoDiasAtraso, campoDataReferencia, query);

		return query.list();
	}

	/**
	 * Ajustar filtro query.
	 *
	 * @param campoDiasAtraso
	 *            the campo dias atraso
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @param query
	 *            the query
	 */
	private void ajustarFiltroQuery(Integer campoDiasAtraso, Date campoDataReferencia, Query query) {

		if (campoDiasAtraso == null || campoDiasAtraso.equals(0)) {
			if (campoDataReferencia != null) {
				query.setDate(0, campoDataReferencia);
				query.setDate(1, campoDataReferencia);
			}

		} else if ((!campoDiasAtraso.equals(0)) && (campoDataReferencia == null)) {
			query.setInteger(0, campoDiasAtraso);

		} else {

			query.setDate(0, campoDataReferencia);
			query.setDate(1, campoDataReferencia);
			query.setInteger(POSICAO, campoDiasAtraso);
		}

	}

	/**
	 * Registrar lancamento contabil despesa.
	 *
	 * @param recebimento the recebimento
	 * @param fatura the fatura
	 * @param valor the valor
	 * @throws GGASException the GGAS exception
	 */
	@Override
	public void registrarLancamentoContabilDespesa(Recebimento recebimento, Fatura fatura, BigDecimal valor) throws GGASException {

		ControladorRubrica controladorRubrica = ServiceLocator.getInstancia().getControladorRubrica();
		ControladorContabil controladorContabil = ServiceLocator.getInstancia().getControladorContabil();
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();

		Rubrica rubrica =
						(Rubrica) controladorRubrica.obter(Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_RUBRICA_DESPESA)));

		LancamentoContabilVO lancamentoContabilVO = new LancamentoContabilVO();
		lancamentoContabilVO.setListaLancamentosContabeisAnaliticos(new ArrayList<LancamentoContabilAnaliticoVO>());

		lancamentoContabilVO.setDataRealizacaoEvento(Calendar.getInstance().getTime());
		lancamentoContabilVO.setSegmento(fatura.getSegmento());
		lancamentoContabilVO.setContaBancaria(null);
		lancamentoContabilVO.setTributo(null);
		lancamentoContabilVO.setDadosAuditoria(fatura.getDadosAuditoria());

		if (fatura.getCliente() != null) {
			lancamentoContabilVO.setCnpj(fatura.getCliente().getCnpj());
		}

		lancamentoContabilVO.setEventoComercial(
						controladorContabil.obterEventoComercial(Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_EVENTO_COMERCIAL_DESPESA))));

		lancamentoContabilVO.setLancamentoItemContabil(rubrica.getLancamentoItemContabil());

		LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVO = new LancamentoContabilAnaliticoVO();
		lancamentoContabilAnaliticoVO.setCodigoObjeto(recebimento.getChavePrimaria());
		lancamentoContabilAnaliticoVO.setValor(valor.setScale(DUAS_CASAS_DECIMAIS, BigDecimal.ROUND_HALF_UP));

		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().clear();
		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(lancamentoContabilAnaliticoVO);

		// ------------ popular conta Auxiliar e historico contabil
		List<Object> listaObjetos = new ArrayList<>();
		listaObjetos.add(fatura.getCliente());
		controladorContabil.popularContaAuxiliarHistoricoContabil(lancamentoContabilVO, listaObjetos);
		// ---------------------------------------------------

		// --------- Populando Eventocomercial lancamento no lancamentocontabilVO para no inserir verificar o regime contabil --------
		Map<String, Object> filtro = controladorContabil.consultarEventoComercialLancamentoPorFiltro(lancamentoContabilVO);
		EventoComercialLancamento eventoComercialLancamento = controladorContabil
						.consultarEventoComercialLancamentoPorFiltro(filtro);
		lancamentoContabilVO.setEventoComercialLancamento(eventoComercialLancamento);
		// ----------------------------------------------------------------------------------------------------------------------------

		controladorContabil.inserirLancamentoContabilSintetico(lancamentoContabilVO);

	}

}
