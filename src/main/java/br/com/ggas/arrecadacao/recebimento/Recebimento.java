/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao.recebimento;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.arrecadacao.ArrecadadorMovimentoItem;
import br.com.ggas.arrecadacao.avisobancario.AvisoBancario;
import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pelos recebimentos
 */
public interface Recebimento extends EntidadeNegocio {

	String BEAN_ID_RECEBIMENTO = "recebimento";

	String RECEBIMENTO_CAMPO_STRING = "RECEBIMENTO";

	String VALOR_RECEBIMENTO = "VALOR_RECEBIMENTO";

	String DATA_RECEBIMENTO = "DATA_RECEBIMENTO";

	String ARRECADADOR = "ARRECADADOR";

	String FORMA_ARRECADACAO = "FORMA_ARRECADACAO";

	/**
	 * @return the valorPrincipal
	 */
	BigDecimal getValorPrincipal();

	/**
	 * @param valorPrincipal
	 *            the valorPrincipal to set
	 */
	void setValorPrincipal(BigDecimal valorPrincipal);

	/**
	 * @return the valorJurosMulta
	 */
	BigDecimal getValorJurosMulta();

	/**
	 * @param valorJurosMulta
	 *            the valorJurosMulta to set
	 */
	void setValorJurosMulta(BigDecimal valorJurosMulta);

	/**
	 * @return the valorDescontos
	 */
	BigDecimal getValorDescontos();

	/**
	 * @param valorDescontos
	 *            the valorDescontos to set
	 */
	void setValorDescontos(BigDecimal valorDescontos);

	/**
	 * @return the valorAbatimento
	 */
	BigDecimal getValorAbatimento();

	/**
	 * @param valorAbatimento
	 *            the valorAbatimento to set
	 */
	void setValorAbatimento(BigDecimal valorAbatimento);

	/**
	 * @return the valorCredito
	 */
	BigDecimal getValorCredito();

	/**
	 * @param valorCredito
	 *            the valorCredito to set
	 */
	void setValorCredito(BigDecimal valorCredito);

	/**
	 * @return the valorIOF
	 */
	BigDecimal getValorIOF();

	/**
	 * @param valorIOF
	 *            the valorIOF to set
	 */
	void setValorIOF(BigDecimal valorIOF);

	/**
	 * @return the valorCobranca
	 */
	BigDecimal getValorCobranca();

	/**
	 * @param valorCobranca
	 *            the valorCobranca to set
	 */
	void setValorCobranca(BigDecimal valorCobranca);

	/**
	 * @return the baixado
	 */
	boolean isBaixado();

	/**
	 * @param baixado
	 *            the baixado to set
	 */
	void setBaixado(boolean baixado);

	/**
	 * @return the recebimentoSituacao
	 */
	RecebimentoSituacao getRecebimentoSituacao();

	/**
	 * @param recebimentoSituacao
	 *            the recebimentoSituacao to set
	 */
	void setRecebimentoSituacao(RecebimentoSituacao recebimentoSituacao);

	/**
	 * @return the cliente
	 */
	Cliente getCliente();

	/**
	 * @param cliente
	 *            the cliente to set
	 */
	void setCliente(Cliente cliente);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the arrecadadorMovimentoItem
	 */
	ArrecadadorMovimentoItem getArrecadadorMovimentoItem();

	/**
	 * @param arrecadadorMovimentoItem
	 *            the arrecadadorMovimentoItem to
	 *            set
	 */
	void setArrecadadorMovimentoItem(ArrecadadorMovimentoItem arrecadadorMovimentoItem);

	/**
	 * @return the avisoBancario
	 */
	AvisoBancario getAvisoBancario();

	/**
	 * @param avisoBancario
	 *            the avisoBancario to set
	 */
	void setAvisoBancario(AvisoBancario avisoBancario);

	/**
	 * @return the faturaGeral
	 */
	FaturaGeral getFaturaGeral();

	/**
	 * @param faturaGeral
	 *            the faturaGeral to set
	 */
	void setFaturaGeral(FaturaGeral faturaGeral);

	/**
	 * @return the documentoCobrancaItem
	 */
	DocumentoCobrancaItem getDocumentoCobrancaItem();

	/**
	 * @param documentoCobrancaItem
	 *            the documentoCobrancaItem to set
	 */
	void setDocumentoCobrancaItem(DocumentoCobrancaItem documentoCobrancaItem);

	/**
	 * @return the formaArrecadacao
	 */
	EntidadeConteudo getFormaArrecadacao();

	/**
	 * @param formaArrecadacao
	 *            the formaArrecadacao to set
	 */
	void setFormaArrecadacao(EntidadeConteudo formaArrecadacao);

	/**
	 * @return the localidade
	 */
	Localidade getLocalidade();

	/**
	 * @param localidade
	 *            the localidade to set
	 */
	void setLocalidade(Localidade localidade);

	/**
	 * @return the anoMesContabil
	 */
	Integer getAnoMesContabil();

	/**
	 * @param anoMesContabil
	 *            the anoMesContabil to set
	 */
	void setAnoMesContabil(Integer anoMesContabil);

	/**
	 * @return String - Retorna ano mês contabil formatado.
	 */
	String getAnoMesContabilFormatado();

	/**
	 * @return the valorRecebimento
	 */
	BigDecimal getValorRecebimento();

	/**
	 * @param valorRecebimento
	 *            the valorRecebimento to set
	 */
	void setValorRecebimento(BigDecimal valorRecebimento);

	/**
	 * @return the valorRecebimentoFormatado
	 */
	String getValorRecebimentoFormatado();

	/**
	 * @return the dataRecebimento
	 */
	Date getDataRecebimento();

	/**
	 * @param dataRecebimento
	 *            the dataRecebimento to set
	 */
	void setDataRecebimento(Date dataRecebimento);

	/**
	 * @return the dataRecebimentoFormatada
	 */
	String getDataRecebimentoFormatada();

	/**
	 * @return the devolucao
	 */
	Devolucao getDevolucao();

	/**
	 * @param devolucao
	 *            the devolucao to set
	 */
	void setDevolucao(Devolucao devolucao);

	/**
	 * @return the valorExcedente
	 */
	BigDecimal getValorExcedente();

	/**
	 * @param valorExcedente
	 *            the valorExcedente to set
	 */
	void setValorExcedente(BigDecimal valorExcedente);

	/**
	 * @return the observacao
	 */
	String getObservacao();

	/**
	 * @param observacao
	 *            the observacao to set
	 */
	void setObservacao(String observacao);

	/**
	 * @return the sequencial
	 */
	Integer getSequencial();

	/**
	 * @param sequencial
	 *            the sequencial to set
	 */
	void setSequencial(Integer sequencial);

}
