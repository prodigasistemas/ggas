/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.arrecadacao.avisobancario.AvisoBancario;
import br.com.ggas.arrecadacao.documento.DocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutEnvioOcorrencia;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.arrecadacao.impl.DadosArquivoRetorno;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.contrato.contrato.EncargoTributario;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoNegociado;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface ControladorArrecadacao.
 */
public interface ControladorArrecadacao extends ControladorNegocio {

	/** The bean id controlador arrecadacao. */
	String BEAN_ID_CONTROLADOR_ARRECADACAO = "controladorArrecadacao";

	/** The erro negocio geracao nsa remessa. */
	String ERRO_NEGOCIO_GERACAO_NSA_REMESSA = "ERRO_NEGOCIO_GERACAO_NSA_REMESSA";

	/** The erro negocio geracao nsa retorno. */
	String ERRO_NEGOCIO_GERACAO_NSA_RETORNO = "ERRO_NEGOCIO_GERACAO_NSA_RETORNO";

	/** The sucesso envio arquivo retorno. */
	String SUCESSO_ENVIO_ARQUIVO_RETORNO = "SUCESSO_ENVIO_ARQUIVO_RETORNO";

	/** The erro negocio convenio sem nosso numero. */
	String ERRO_NEGOCIO_CONVENIO_SEM_NOSSO_NUMERO = "ERRO_NEGOCIO_CONVENIO_SEM_NOSSO_NUMERO";

	/** The erro negocio convenio nao cadastrado. */
	String ERRO_NEGOCIO_CONVENIO_NAO_CADASTRADO = "ERRO_NEGOCIO_CONVENIO_NAO_CADASTRADO";
	
	/** Erro negocio não existe leiaute. */
	String ERRO_SISTEMA_NAO_EXISTE_LEIAUTE_ARQUIVO = "ERRO_SISTEMA_NAO_EXISTE_LEIAUTE_ARQUIVO";
	
	/**
	 * Criar debito automatico movimento.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarDebitoAutomaticoMovimento();

	/**
	 * Criar cobranca bancaria movimento.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarCobrancaBancariaMovimento();

	/**
	 * Criar cliente debito automatico.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarClienteDebitoAutomatico();

	/**
	 * Método responsável por calcular juros e
	 * multa por acréscimo de impontualidade.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param dataPagamento
	 *            the data pagamento
	 * @param valorPagamento
	 *            the valor pagamento
	 * @param indicadorMulta
	 *            the indicador multa
	 * @param indicadorJurosMora
	 *            the indicador juros mora
	 * @return the debitos a cobrar
	 * @throws GGASException
	 *             the GGAS exception
	 */
	DebitosACobrar calcularAcrescimentoImpontualidade(
					Fatura fatura, Date dataPagamento, 
					BigDecimal valorPagamento, boolean indicadorMulta,
					boolean indicadorJurosMora) throws GGASException;
	
	/**
	 * Método responsável por calcular juros e
	 * multa por acréscimo de impontualidade.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param dataPagamento
	 *            the data pagamento
	 * @param valorPagamento
	 *            the valor pagamento
	 * @param indicadorMulta
	 *            the indicador multa
	 * @param indicadorJurosMora
	 *            the indicador juros mora
	 * @param indicadorCorrecaoMonetaria
	 *            the indicador correcao monetaria
	 * @return the debitos a cobrar
	 * @throws GGASException
	 *             the GGAS exception
	 */


	/**
	 * Método responsável por calcular juros e
	 * multa por acréscimo de impontualidade.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param dataPagamento
	 *            the data pagamento
	 * @param valorPagamento
	 *            the valor pagamento
	 * @param indicadorMulta
	 *            the indicador multa
	 * @param indicadorJurosMora
	 *            the indicador juros mora
	 * @param indicadorCorrecaoMonetaria
	 *            the indicador correcao monetaria
	 * @return the debitos a cobrar
	 * @throws GGASException
	 *             the GGAS exception
	 */
	DebitosACobrar calcularAcrescimentoImpontualidade(
					Fatura fatura, Date dataPagamento, 
					BigDecimal valorPagamento, boolean indicadorMulta,
					boolean indicadorJurosMora, boolean indicadorCorrecaoMonetaria) 
									throws GGASException;

	/**
	 * Método responsável por gerar os movimentos
	 * de débito automático.
	 * 
	 * @param listaDebitoAutomaticoMovimento
	 *            Lista de Movimento de Débito
	 *            Automático.
	 * @return the string builder
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             durante a execução do método.
	 */
	StringBuilder gerarMovimentoDebitoAutomatico(Collection<DebitoAutomaticoMovimento> listaDebitoAutomaticoMovimento)
					throws GGASException;

	/**
	 * Método responsável por gerar os movimentos
	 * de cobrança registrada.
	 * 
	 * @param listaCobrancaBancariaMovimento
	 *            Lista de movimento de cobrança
	 * @return the string builder
	 * @throws GGASException é lançado se algum valor estiver inválido
	 */
	StringBuilder gerarMovimentoCobrancaRegistrada(Collection<CobrancaBancariaMovimento> listaCobrancaBancariaMovimento)
					throws GGASException;

	/**
	 * Método responsável por gerar os movimentos
	 * de cobrança não registrada.
	 * 
	 * @param listaCobrancaBancariaMovimento
	 *            Lista de movimento de cobrança
	 * @return the string builder
 	 * @throws GGASException
	 *            Caso ocorra algum erro na
	 *            durante a execução do método.
	 */
	StringBuilder gerarMovimentoCobrancaNaoRegistrada(Collection<CobrancaBancariaMovimento> listaCobrancaBancariaMovimento)
					throws GGASException;

	/**
	 * Metodo responsavel por listar as Entidades
	 * 'Arrecadador' existentes ativas no sistema,
	 * carregando as entidades Banco e Cliente,
	 * caso existam.
	 * 
	 * @return Coleção de Arrecadador do sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Arrecadador> listarArrecadadorExistenteDescricao() throws NegocioException;

	/**
	 * Metodo responsavel por listar as Entidades
	 * 'TipoDocumento' existentes ativas no
	 * sistema.
	 * 
	 * @return Coleção de TipoDocumento do sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<TipoDocumento> listarTipoDocumentoExistente() throws NegocioException;

	/**
	 * Metodo responsavel por listar as Entidades
	 * 'TipoDocumento' relacionadas ao
	 * recebimento.
	 * 
	 * @return Coleção de TipoDocumento do sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<TipoDocumento> listarTipoDocumentoRecebimento() throws NegocioException;

	/**
	 * Metodo responsavel por obter uma entidade
	 * Arrecadador no sistema.
	 * 
	 * @param idArrecadador
	 *            the id arrecadador
	 * @return Uma Entidade Arrecador do sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Arrecadador obterArrecadador(Long idArrecadador) throws NegocioException;

	/**
	 * Metodo responsavel por obter uma entidade
	 * Arrecadador no sistema pelo Id do Banco.
	 * 
	 * @param idBanco
	 *            the id banco
	 * @return Uma Entidade Arrecador do sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Arrecadador obterArrecadadorPorBanco(Long idBanco) throws NegocioException;

	/**
	 * Método responsável obter, incrementar e
	 * atualizar o NSA Remessa.
	 * 
	 * @param chaveArrecadador
	 *            A chave do arrecadador.
	 * @param chaveTipoConvenio
	 *            A chave do Tipo de Convenio.
	 * @return o valor do NSA Remessa.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *            Caso ocorra algum erro na
	 *            durante a execução do método.
	 */
	Long gerarNSAArrecadador(Long chaveArrecadador, Long chaveTipoConvenio) throws GGASException;

	/**
	 * Método responsável por obter o código do
	 * Convênio.
	 * 
	 * @param chaveArrecadador
	 *            A chave do arrecadador.
	 * @param chaveTipoConvenio
	 *            A chave do Tipo de Convenio.
	 * @return O Código do Convênio
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *            Caso ocorra algum erro na
	 *            durante a execução do método.             
	 */
	String obterCodigoConvenio(Long chaveArrecadador, Long chaveTipoConvenio) throws GGASException;

	/**
	 * Método responsável por obter um Arrecadador
	 * Contrato Convênio de acordo com a chave do
	 * arrecadador e tipo do convênio.
	 * 
	 * @param chaveArrecadador
	 *            A chave do arrecadador.
	 * @param chaveTipoConvenio
	 *            A chave do Tipo de Convenio.
	 * @return ArrecadadorContratoConvenio	 
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *            Caso ocorra algum erro na
	 *            durante a execução do método.             
	 */
	ArrecadadorContratoConvenio obterACConvenioPorArrecadador(Long chaveArrecadador, Long chaveTipoConvenio) throws GGASException;

	/**
	 * metodo responsavel por retonar uma colecao
	 * de arrecadador contrato convenio.
	 * 
	 * @param chaveArrecadador
	 *            chave primaria do arrecadador
	 * @param chaveFormaArrecadacao
	 *            chave primaria da forma de
	 *            arrecadacao
	 * @return uma colecao de arrecador contrato
	 *         convenio do sistema	 
	 * @throws GGASException
	 *            Caso ocorra algum erro na
	 *            durante a execução do método.             
	 */
	Collection<ArrecadadorContratoConvenio> consultarACConvenioPorArrecadador(Long chaveArrecadador, Long chaveFormaArrecadacao)
					throws GGASException;

	/**
	 * Método responsável por obter o débito
	 * automático ativo.
	 * 
	 * @param idCliente
	 *            A Chave primária do Cliente
	 * @param idContrato
	 *            A chave primária do Contrato
	 * @return Um Débito automático
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	DebitoAutomatico obterDebitoAutomaticoAtivo(Long idCliente, Long idContrato) throws NegocioException;

	/**
	 * Método responsável por retornar um débito
	 * automático ativo.
	 * 
	 * @param idClienteDebitoAutomatico
	 *            idClienteDebitoAutomatico.
	 * @return DebitoAutomatico DebitoAutomatico.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	DebitoAutomatico obterDebitoAutomaticoAtivo(Long idClienteDebitoAutomatico) throws NegocioException;

	/**
	 * Método responsável por obter uma Entidade
	 * Arrecadacao Movimento.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarArrecadadorMovimento();

	/**
	 * Método responsável por obter uma Entidade
	 * Arrecadacao Movimento Item.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarArrecadadorMovimentoItem();

	/**
	 * Método responsável por obter uma Entidade
	 * Debito Automatico.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarDebitoAutomatico();

	/**
	 * Método responsável por obter uma Entidade
	 * Aviso Bancário.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarAvisoBancario();

	/**
	 * Método responsável por obter uma Entidade
	 * Debito Automatico Nao Processado.
	 * 
	 * @return the debito nao processado
	 */
	DebitoNaoProcessado criarDebitoNaoProcessado();

	/**
	 * Método responsável por obter uma Entidade
	 * de Dados Arquivo Retorno.
	 * 
	 * @return the dados arquivo retorno
	 */
	DadosArquivoRetorno criarDadosArquivoRetorno();

	/**
	 * Método responsável por persistir uma
	 * Entidade Arrecadacao Movimento.
	 * 
	 * @param arrecadadorMovimento
	 *            the arrecadador movimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirArrecadacaoMovimento(ArrecadadorMovimento arrecadadorMovimento) throws NegocioException;

	/**
	 * Método responsável por persistir uma
	 * Entidade Arrecadacao Movimento Item.
	 * 
	 * @param arrecadadorMovimentoItem
	 *            the arrecadador movimento item
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirArrecadacaoMovimentoItem(ArrecadadorMovimentoItem arrecadadorMovimentoItem) throws NegocioException;

	/**
	 * Método responsável por alterar uma Entidade
	 * Arrecadacao Movimento no banco.
	 * 
	 * @param arrecadadorMovimento
	 *            the arrecadador movimento
	 * @throws GGASException the negocio exception
	 */
	void alterarArrecadacaoMovimento(ArrecadadorMovimento arrecadadorMovimento) throws GGASException;

	/**
	 * Método responsável por obter o tipo de
	 * documento.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do tipo de
	 *            documento.
	 * @return Tipo de Documento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoDocumento obterTipoDocumento(Long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por alterar uma Entidade
	 * Debito Automatico no banco.
	 * 
	 * @param debitoAutomatico
	 *            the debito automatico
	 */
	void inserirDebitoAutomatico(DebitoAutomatico debitoAutomatico);

	/**
	 * Método responsável por alterar uma Entidade
	 * Debito Automatico no banco.
	 * 
	 * @param debitoAutomatico
	 *            the debito automatico
	 */
	void alterarDebitoAutomatico(DebitoAutomatico debitoAutomatico);

	/**
	 * Método responsável por criar um arquivo de remessa para o banco com os débitos não processados.
	 *
	 * @param colecaoDebitoNaoProcessado Colecao Debito Nao Processado
	 * @param idArrecadador Arrecadador
	 * @param arrecadadorMovimento the arrecadador movimento
	 * @param documentoLayout modelo do documento
	 * @throws GGASException Caso ocorra algum erro na invocação do método.
	 * @return StringBuilder com o arquivo montado
	 */
	StringBuilder montarArquivoDebitosNaoProcessados(Collection<DebitoNaoProcessado> colecaoDebitoNaoProcessado, Arrecadador idArrecadador,
					ArrecadadorMovimento arrecadadorMovimento, DocumentoLayout documentoLayout) throws GGASException;

	/**
	 * Método responsável por listar os movimentos
	 * de débitos automáticos.
	 * 
	 * @return Coleção de Movimento de Débito
	 *         Automático.
	 * @throws GGASException Caso ocorra algum erro na invocação do método.
	 */
	Collection<DebitoAutomaticoMovimento> listarDebitoAutomaticoMovimento() throws GGASException;

	/**
	 * Método responsávle por listar todos os
	 * Tipos de Documento.
	 * 
	 * @return Tipos Documento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<TipoDocumento> listarTipoDocumento() throws NegocioException;

	/**
	 * Método responsável por obter o último
	 * sequencial do aviso bancário para o
	 * arrecadador e data informada.
	 * 
	 * @param arrecadador
	 *            Arrecadador
	 * @param dataLancamento
	 *            Data de Lançamento
	 * @return Último Sequencial
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Integer obterUltimoSequencialAvisoBancario(Arrecadador arrecadador, Date dataLancamento) throws NegocioException;

	/**
	 * Método responsável por realizar a inserção
	 * dos avisos bancários informados no
	 * parâmetro.
	 * 
	 * @param mapAvisosBancarios
	 *            Mapa contendo os avisos
	 *            bancários, agrupados por data de
	 *            recebimento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void inserirAvisosBancarios(Map<Date, AvisoBancario> mapAvisosBancarios) throws NegocioException;

	/**
	 * Método responsável por obter o arrecadador
	 * contrato convênio.
	 * 
	 * @param codigoConvenio
	 *            código do convênio.
	 * @param chaveArrecadador
	 *            chave primária do arrecadador.
	 * @return ArrecadadorContratoConvenio
	 *         Arrecadador Contrato Convênio
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	ArrecadadorContratoConvenio obterArrecadadorContratoConvenio(String codigoConvenio, long chaveArrecadador) throws NegocioException;

	/**
	 * Obter arrecadador contrato convenio.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the arrecadador contrato convenio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ArrecadadorContratoConvenio obterArrecadadorContratoConvenio(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por incrementar o NSA de
	 * retorno.
	 * 
	 * @param chaveArrecadador
	 *            chave primária do arrecadador.
	 * @param chaveTipoConvenio
	 *            codigoConvenio código do
	 *            convênio.
	 * @throws GGASException Caso ocorra algum erro na invocação do método.
	 */
	void incrementarNSARetornoArrecadador(Long chaveArrecadador, Long chaveTipoConvenio) throws GGASException;

	/**
	 * Verificar se já existe Debito Automático
	 * Ativo para o ponto de consumo informado;.
	 * 
	 * @param idClienteDebitoAutomatico
	 *            Id Cliente Débito Automático.
	 * @param conta
	 *            Conta.
	 * @param agencia
	 *            Agencia.
	 * @return True caso exista, False caso
	 *         contrário.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean verificarExistenciaDebitoAutomaticoAtivo(Long idClienteDebitoAutomatico, String conta, String agencia) throws NegocioException;

	/**
	 * Método responsável por gerar débitos a
	 * cobrar por acréscimo de impontualidade.
	 * 
	 * @param recebimento
	 *            Um recebimento
	 * @param indicadorMulta
	 *            indicador de multa.
	 * @param indicadorJurosMora
	 *            indicador de juros mora.
	 * @param listaCreditoDebitoNegociado
	 *            Lista com Creditos e Debitos
	 *            inseridos.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void gerarDebitosACobrarPorAcrescimentoImpontualidade(Recebimento recebimento, boolean indicadorMulta, boolean indicadorJurosMora,
					Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado) throws GGASException;

	/**
	 * Método responsável por gerar débitos a
	 * cobrar por acréscimo de impontualidade.
	 *
	 * @param recebimento            Um recebimento
	 * @param indicadorMulta            indicador de multa.
	 * @param indicadorJurosMora            indicador de multa.
	 * @param indicadorCorrecaoMonetaria            indicador de correcao monetaria.
	 * @param listaCreditoDebitoNegociado            Lista com Creditos e Debitos
	 *            inseridos.
	 * @throws GGASException             the GGAS exception
	 */	

	
	/**
	 * Método responsável por gerar débitos a
	 * cobrar por acréscimo de impontualidade.
	 * 
	 * @param recebimento
	 *            Um recebimento
	 * @param indicadorMulta
	 *            indicador de multa.
	 * @param indicadorJurosMora
	 *            indicador de multa.
	 * @param indicadorCorrecaoMonetaria
	 *            indicador de correcao monetaria.
	 * @param listaCreditoDebitoNegociado
	 *            Lista com Creditos e Debitos
	 *            inseridos.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *             the GGAS exception
	 */	
	void gerarDebitosACobrarPorAcrescimentoImpontualidade(
					Recebimento recebimento, boolean indicadorMulta, 
					boolean indicadorJurosMora, boolean indicadorCorrecaoMonetaria, 
					Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado)
									throws GGASException;
	
	/**
	 * Método responsável por retornar uma lista
	 * de Cobranca Bancaria Movimento de acordo
	 * com o Tipo do Movimento.
	 * 
	 * @param tipoConvenio
	 *            Tipo do Convênio.
	 * @return Lista de Cobrança Bancária
	 *         Movimento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CobrancaBancariaMovimento> listarCobrancaBancariaMovimento(EntidadeConteudo tipoConvenio) throws NegocioException;

	/**
	 * Método responsável por retornar um Aviso
	 * Bancário de acordo com o código passado.
	 * 
	 * @param idAvisoBancario
	 *            código do aviso bancário
	 * @return Um Aviso Bancário
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	AvisoBancario obterAvisoBancario(Long idAvisoBancario) throws NegocioException;

	/**
	 * Método responsável por retornar uma Fatura
	 * Geral de acordo com o código passado.
	 * 
	 * @param idFaturaGeral
	 *            código da Fatura Geral
	 * @return uma Fatura Geral
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	FaturaGeral obterFaturaGeral(Long idFaturaGeral) throws NegocioException;

	/**
	 * Método responsável por retornar um
	 * DocumentoCobrancaItem de acordo com o
	 * código passado.
	 * 
	 * @param idDocumentoCobrancaItem
	 *            the id documento cobranca item
	 * @return um DocumentoCobrancaItem
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	DocumentoCobrancaItem obterDocumentoCobrancaItem(Long idDocumentoCobrancaItem) throws NegocioException;

	/**
	 * Obter ano mes contabil.
	 * 
	 * @param anoMesContabilParametro
	 *            the ano mes contabil parametro
	 * @param dataLancamento
	 *            the data lancamento
	 * @return the integer
	 */
	Integer obterAnoMesContabil(Integer anoMesContabilParametro, Date dataLancamento);

	/**
	 * Criar credito debito a realizar.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarCreditoDebitoARealizar();

	/**
	 * metodo responsavel por retornar uma agencia
	 * do sistema.
	 * 
	 * @param idAgencia
	 *            chave primaria da agencia
	 * @return um entidade do sistema do tipo
	 *         Banco
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Agencia obterAgenciaPorChave(Long idAgencia) throws NegocioException;
	
	/**
	 * Obter lista agencia.
	 *
	 * @param filtro the filtro
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<Agencia> obterListaAgencia(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável para obter o valor de um
	 * encargo tributário.
	 * 
	 * @param codigoParametroEncargo
	 *            the codigo parametro encargo
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	BigDecimal obterValorPercentual(String codigoParametroEncargo) throws NegocioException;

	/**
	 * Método responsável por retornar um Cliente
	 * Débito Automático de acordo com a
	 * identificação do cliente na empresa.
	 * 
	 * @param identificacaoClienteEmpresa
	 *            Identificação do Cliente na
	 *            Empresa.
	 * @return ClienteDebitoAutomatico Cliente
	 *         Débito Automático
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	ClienteDebitoAutomatico obterClienteDebitoAutomatico(Long identificacaoClienteEmpresa) throws NegocioException;

	/**
	 * Método responsável por retornar um Cliente
	 * Débito Automático de acordo com a
	 * identificação do cliente na empresa.
	 * 
	 * @param identificacaoClienteEmpresa
	 *            Identificação do Cliente na
	 *            Empresa.
	 * @param identificacaoContratoPai
	 *            Identificação do contrato.
	 * @return ClienteDebitoAutomatico Cliente
	 *         Débito Automático
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	ClienteDebitoAutomatico obterClienteDebitoAutomatico(Long identificacaoClienteEmpresa, Long identificacaoContratoPai)
					throws NegocioException;

	/**
	 * Método responsável por obter o
	 * ArrecadadorContratoConvenio que será usado
	 * na geração do boleto bancário.
	 * 
	 * @return Um ArrecadadorContratoConvenio.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	ArrecadadorContratoConvenio obterArrecadadorContratoConvenioParaBoletoBancario() throws NegocioException;
	
	/**
	 * Método responsável por calcular o nosso
	 * número a partir do
	 * ArrecadadorContratoConvenio.
	 * 
	 * @param arrecadadorContratoConvenio
	 *            O ArrecadadorContratoConvenio.
	 * @param dadosAuditoria
	 *            Dados de auditoria a serem
	 *            setados no
	 *            ArrecadadorContratoConvenio.
	 * @return O nosso número.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Long calcularNossoNumero(
					ArrecadadorContratoConvenio arrecadadorContratoConvenio, 
					DadosAuditoria dadosAuditoria) throws NegocioException;
	
	/**
	 * Método responsável por calcular o nosso
	 * número a partir do
	 * ArrecadadorContratoConvenio e valida o proximo numero.
	 * 
	 * @param arrecadadorContratoConvenio
	 *            O ArrecadadorContratoConvenio.
	 * @param dadosAuditoria
	 *            Dados de auditoria a serem
	 *            setados no
	 *            ArrecadadorContratoConvenio.
	 * @return O nosso número.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Long calcularNossoNumeroComValidacao(
					ArrecadadorContratoConvenio arrecadadorContratoConvenio, 
					DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Método responsável por retornar o valor de
	 * um encargo tributario.
	 * 
	 * @param chaveEncargo
	 *            the chave encargo
	 * @return the encargo tributario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	EncargoTributario obterValorNominalEncargoTributario(Long chaveEncargo) throws NegocioException;

	/**
	 * Método responsável por retornar o documento
	 * cobranca de um credito debito a realizar.
	 * 
	 * @param idCreditoDebitoARealizar
	 *            the id credito debito a realizar
	 * @return the date
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Date obterDataVencimentoDocumentoCobrancaPeloCreditoDebitoARealizar(Long idCreditoDebitoARealizar) throws NegocioException;

	/**
	 * Método responsável por obter o arrecadador
	 * contrato convênio ativo pelo tipo de
	 * convênio.
	 * 
	 * @param chaveTipoConvenio
	 *            A chave do tipo de convênio
	 * @return O arrecadador contrato convênio
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	ArrecadadorContratoConvenio obterACConvenioPorTipoConvenio(Long chaveTipoConvenio) throws NegocioException;

	/**
	 * Método responsável por obter um
	 * ClienteDebitoAutomatico de acordo com um
	 * filtro e um indicador se deve considerar
	 * apenas debito automático.
	 * 
	 * @param filtro
	 *            filtro de parâmetros para a
	 *            consulta
	 * @param isApenasDebitoAutomatico
	 *            indicador se deve considerar
	 *            apenas débitos automáticos
	 * @return the cliente debito automatico
	 */
	public ClienteDebitoAutomatico consultarClienteDebitoAutomatico(Map<String, Object> filtro, Boolean isApenasDebitoAutomatico);

	/**
	 * Método responsável por inserir um
	 * ClienteDebitoAutomatico.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return Chave primária da entidade
	 *         ClienteDebitoAutomatico
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Long inserirClienteDebitoAutomatico(Fatura fatura, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Método responsável por obter debito
	 * automatico movimento.
	 * 
	 * @param documentoCobranca
	 *            the documento cobranca
	 * @return debitoAutomaticoMovimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	DebitoAutomaticoMovimento obterDebitoAutomaticoMovimento(DocumentoCobranca documentoCobranca) throws NegocioException;

	/**
	 * Metodo responsavel por obter uma entidade
	 * ContaBancaria.
	 * 
	 * @param idContaBancaria
	 *            the id conta bancaria
	 * @return Uma Entidade ContaBancaria do
	 *         sistema
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ContaBancaria obterContaBancaria(Long idContaBancaria) throws NegocioException;

	/**
	 * Gets the classe entidade encargo tributario.
	 *
	 * @return the classe entidade encargo tributario
	 */
	Class<?> getClasseEntidadeEncargoTributario();

	/**
	 * Inserir estrutura credito debito.
	 * 
	 * @param recebimento the recebimento
	 * @param valorExcedente the valor excedente
	 * @param rubrica the rubrica
	 * @param origem the origem
	 * @param listaCreditoDebitoNegociado the lista credito debito negociado
	 * @throws GGASException the negocio exception
	 */
	void inserirEstruturaCreditoDebito(Recebimento recebimento, BigDecimal valorExcedente, Rubrica rubrica, String origem,
					Collection<CreditoDebitoNegociado> listaCreditoDebitoNegociado) throws GGASException;

	/**
	 * Listar cobranca bancaria movimento.
	 * 
	 * @param documentoCobranca
	 *            the documento cobranca
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CobrancaBancariaMovimento> listarCobrancaBancariaMovimento(DocumentoCobranca documentoCobranca) throws NegocioException;

	/**
	 * Checks if is documento vencido.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param dataPagamento
	 *            the data pagamento
	 * @return true, if is documento vencido
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean isDocumentoVencido(Fatura fatura, Date dataPagamento) throws NegocioException;
	
	/**
	 * Obtém o arrecadador com base no identificador e carrega as propriedades informadas por parâmetro
	 *
	 * @param idArrecadador chave primária do arrecadador
	 * @param propriedadesLazy uma lista de strings que devem ser projetadas
	 * @throws GGASException é lançado se ocorrer algum erro
	 * @return Arrecadador é retornado
	 */
	Arrecadador obterArrecadador(Long idArrecadador, String... propriedadesLazy) throws GGASException;
	
	
	
	/**
	 * Cria uma mapa de layouts de registros de documentos
	 * @param registros {@link Collection}
	 * @return Cria mapa de layouts de documento {@link Map}
	 */
	Map<String, DocumentoLayoutRegistro> criarMapaDocumentoLayoutRegistro(Collection<DocumentoLayoutRegistro> registros);

	/**
	 * Insere um movimento de cobrança bancária (COBRANCA_BANCARIA_MOVIMENTO)
	 * por ocorrência de envio do arquivo de remessa (DLEO_CD) conforme constante 
	 * configurada por banco.
	 * 
	 * @param fatura A fatura que está sendo alterada
	 * @param dadosAuditoria Os dados para auditoria
	 * @param constanteOcorrenciaEnvio A constante de ocorrência de envio
	 * @param documentoCobranca O Documento de Cobrança da Fatura
	 * @return Retorna a chave primária do Movimento de Cobrança Bancária inserido
	 * @throws NegocioException é lançado em caso de erro 
	 */
	public long inserirCobrancaBancariaMovimentoPorOcorrencia(Fatura fatura, DadosAuditoria dadosAuditoria,
			String constanteOcorrenciaEnvio, DocumentoCobranca documentoCobranca) throws NegocioException;
	
	/**
	 * Insere um movimento de cobrança bancário com ocorrência de envio de CANCELAMENTO
	 * 
	 * @param fatura A Fatura que está sendo cancelada
	 * @param dadosAuditoria Os dados para auditoria
	 * @return Retorna a chave primária do Movimento de Cobrança Bancária inserido
	 * @throws NegocioException é lançado em caso de erro
	 */
	public long inserirMovimentoDeCancelamento(Fatura fatura, DadosAuditoria dadosAuditoria) throws NegocioException;
	
	/**
	 * Obtém a ocorrência de envio através da chave primária
	 * 
	 * @param chavePrimaria A chave primária da ocorrência
	 * @return Retorna a ocorrência de envio pesquisada
	 * @throws NegocioException é lançado em caso de erro
	 */
	public DocumentoLayoutEnvioOcorrencia obterDocumentoLayoutEnvioOcorrencia(Long chavePrimaria) throws NegocioException;
	
	/**
	 * Obtém o movimento de cobrança bancária por chave primária
	 * 
	 * @param chavePrimaria A chave primária do Movimento de Cobrança Bancária
	 * @return Retorna o Movimento Cobrança Bancária pesquisado 
	 * @throws NegocioException é lançado em caso de erro
	 */
	public CobrancaBancariaMovimento obterCobrancaBancariaMovimento(Long chavePrimaria) throws NegocioException;
	
	/**
	 * Listar cobranca bancaria movimento habilitado.
	 * 
	 * @param documentoCobranca
	 *            the documento cobranca
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CobrancaBancariaMovimento> consultarCobrancaBancariaMovimentoHabilitado(DocumentoCobranca documentoCobranca)
			throws NegocioException;

	/**
	 * Método responsável por retornar um ClienteDebitoAutomatico preenchido apenas com sua chave Primaria
	 * 
	 * @param identificacaoClienteEmpresa {@link Long}
	 * @param identificacaoContratoPai {@link Long}
	 * @return ClienteDebitoAutomatico {@link ClienteDebitoAutomatico}
	 * @throws NegocioException {@link NegocioException}
	 */
	public ClienteDebitoAutomatico obterChaveClienteDebitoAutomatico(Long identificacaoClienteEmpresa,
			Long identificacaoContratoPai) throws NegocioException;
}
