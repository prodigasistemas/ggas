/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.arrecadacao;

import java.util.Collection;

import br.com.ggas.arrecadacao.documento.DocumentoModelo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela definição do tipo de documento.
 */
public interface TipoDocumento extends EntidadeNegocio {

	String BEAN_ID_TIPO_DOCUMENTO = "tipoDocumento";

	static final long TIPO_FATURA = 1;

	static final long TIPO_NOTAS_DEBITO = 2;

	static final long TIPO_NOTAS_CREDITO = 3;

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the descricaoAbreviada
	 */
	String getDescricaoAbreviada();

	/**
	 * @param descricaoAbreviada
	 *            the descricaoAbreviada to set
	 */
	void setDescricaoAbreviada(String descricaoAbreviada);

	/**
	 * @return the indicadorPagavel
	 */
	String getIndicadorPagavel();

	/**
	 * @param indicadorPagavel
	 *            the indicadorPagavel to set
	 */
	void setIndicadorPagavel(String indicadorPagavel);

	/**
	 * @return the indicadorCodigoBarras
	 */
	String getIndicadorCodigoBarras();

	/**
	 * @param indicadorCodigoBarras - Set indicador código barras.
	 */
	void setIndicadorCodigoBarras(String indicadorCodigoBarras);

	/**
	 * @return Collection<DocumentoModelo> - Retorna uma coleção de documento modelo.
	 */
	Collection<DocumentoModelo> getModelos();

	/**
	 * @param modelos - Set modelos.
	 */
	void setModelos(Collection<DocumentoModelo> modelos);

	/**
	 * @return String - Retorna Indicador pagavel formatado.
	 */
	public String getIndicadorPagavelFormatado();

	/**
	 * @return String - Retorna Indicador código de barras formatado.
	 */
	public String getIndicadorCodigoBarrasFormatado();

}
