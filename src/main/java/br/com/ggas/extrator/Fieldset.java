
package br.com.ggas.extrator;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * Classe Fieldset
 * 
 * @author larvieir
 *
 */
public class Fieldset {

	private static final int FIM_CAMPO_DATA = 8;

	private Map<String, String> mapa = new HashMap<>();

	private List<String> nomes = new ArrayList<>();

	/**
	 * Instantiates a new fieldset.
	 * 
	 * @param valor
	 *            the valor
	 * @param fieldnames
	 *            the fieldnames
	 */
	public Fieldset(String valor, String fieldnames) {

		nomes = toList(fieldnames);
		String[] valores = valor.replace("|", "#").replace("##", "#null#").split("#");

		if(nomes.size() != valores.length) {
			throw new IllegalArgumentException("Existe erros de mapeamento , o arquivo possui " + valores.length + " e foram mapeadas "
							+ nomes.size() + " colunas");
		} else {
			int i = 0;
			for (String v : valores) {
				mapa.put(nomes.get(i), v);
				i++;
			}
		}

	}

	/**
	 * Read long.
	 * 
	 * @param string
	 *            the string
	 * @return the long
	 */
	public Long readLong(String string) {

		if (string == null || "null".equals(string)) {
			return null;
		}

		if (StringUtils.isEmpty(mapa.get(string)) || "null".equals(mapa.get(string))) {
			return null;
		}

		return Long.parseLong(mapa.get(string));

	}

	/**
	 * Read string.
	 * 
	 * @param string
	 *            the string
	 * @return the string
	 */
	public String readString(String string) {

		if (StringUtils.isEmpty(mapa.get(string)) || "null".equals(string)) {
			return null;
		}

		if (StringUtils.isEmpty(mapa.get(string)) || "null".equals(mapa.get(string))) {
			return null;
		}

		return mapa.get(string);
	}

	/**
	 * Read date.
	 * 
	 * @param string
	 *            the string
	 * @return the date
	 */
	public Date readDate(String string) {

		if (string == null || "null".equals(string)) {
			return null;
		}

		if (StringUtils.isEmpty(mapa.get(string)) || "null".equals(mapa.get(string))) {
			return null;
		}

		String valor = mapa.get(string);

		String[] data = valor.substring(0, FIM_CAMPO_DATA).split("/");

		return new Date(Integer.parseInt(data[0] + 100), Integer.parseInt(data[0]) - 1, Integer.parseInt(data[0]));

	}

	/**
	 * Read boolean.
	 * 
	 * @param string
	 *            the string
	 * @return the boolean
	 */
	public Boolean readBoolean(String string) {

		if (string == null || "null".equals(string)) {
			return false;
		}

		if (StringUtils.isEmpty(mapa.get(string)) || "null".equals(mapa.get(string))) {
			return null;
		}

		String valor = mapa.get(string);

		return Boolean.parseBoolean(valor);
	}

	/**
	 * To list.
	 * 
	 * @param valores
	 *            the valores
	 * @return the list
	 */
	protected List<String> toList(String valores) {

		List<String> nomesLista = new ArrayList<>();

		String[] labels = valores.split(",");

		for (String label : labels) {
			nomesLista.add(label);
		}

		return nomesLista;
	}

	/**
	 * Read int.
	 * 
	 * @param string
	 *            the string
	 * @return the integer
	 */
	public Integer readInt(String string) {

		if (string == null || "null".equals(string)) {
			return null;
		}

		if (StringUtils.isEmpty(mapa.get(string)) || "null".equals(mapa.get(string))) {
			return null;
		}

		String valor = mapa.get(string);

		return Integer.parseInt(valor);

	}

}
