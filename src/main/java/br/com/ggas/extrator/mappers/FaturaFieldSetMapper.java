
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.arrecadacao.devolucao.impl.DevolucaoImpl;
import br.com.ggas.arrecadacao.impl.TipoDocumentoImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.imovel.impl.SegmentoImpl;
import br.com.ggas.cobranca.parcelamento.impl.ParcelamentoImpl;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.impl.ContratoImpl;
import br.com.ggas.faturamento.impl.CreditoDebitoSituacaoImpl;
import br.com.ggas.faturamento.impl.FaturaGeralImpl;
import br.com.ggas.faturamento.impl.FaturaImpl;
import br.com.ggas.faturamento.impl.FaturaMotivoRevisaoImpl;
import br.com.ggas.faturamento.impl.NaturezaOperacaoCFOPImpl;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.medicao.consumo.impl.HistoricoConsumoImpl;
import br.com.ggas.medicao.leitura.impl.HistoricoMedicaoImpl;
import br.com.ggas.medicao.rota.impl.RotaImpl;

/**
 * Classe responsável por fatura impl.
 */
public class FaturaFieldSetMapper implements FieldSetMapper<FaturaImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public FaturaImpl mapFieldSet(FieldSet fieldSet) {

		FaturaImpl f = new FaturaImpl();

		try {
			if((Long) fieldSet.readLong("FAGE_CD") != null) {
				FaturaGeralImpl faturaGeral = new FaturaGeralImpl();

				faturaGeral.setChavePrimaria(fieldSet.readLong("FAGE_CD"));
				f.setFaturaGeral(faturaGeral);
			}

			if((Long) fieldSet.readLong("DOTI_CD") != null) {
				TipoDocumentoImpl tipoDocumento = new TipoDocumentoImpl();

				tipoDocumento.setChavePrimaria(fieldSet.readLong("DOTI_CD"));
				f.setTipoDocumento(tipoDocumento);
			}

			if((Long) fieldSet.readLong("POCN_CD") != null) {
				PontoConsumoImpl pontoConsumo = new PontoConsumoImpl();

				pontoConsumo.setChavePrimaria(fieldSet.readLong("POCN_CD"));
				f.setPontoConsumo(pontoConsumo);
			}

			if((Long) fieldSet.readLong("CLIE_CD") != null) {
				ClienteImpl cliente = new ClienteImpl();

				cliente.setChavePrimaria(fieldSet.readLong("CLIE_CD"));
				f.setCliente(cliente);
			}

			if((Long) fieldSet.readLong("ROTA_CD") != null) {
				RotaImpl rota = new RotaImpl();

				rota.setChavePrimaria(fieldSet.readLong("ROTA_CD"));
				f.setRota(rota);
			}

			if((Long) fieldSet.readLong("CRDS_CD") != null) {
				CreditoDebitoSituacaoImpl creditoDebito = new CreditoDebitoSituacaoImpl();

				creditoDebito.setChavePrimaria(fieldSet.readLong("CRDS_CD"));
				f.setCreditoDebitoSituacao(creditoDebito);
			}

			if((Long) fieldSet.readLong("SEGM_CD") != null) {
				SegmentoImpl segmentoImpl = new SegmentoImpl();

				segmentoImpl.setChavePrimaria(fieldSet.readLong("SEGM_CD"));
				f.setSegmento(segmentoImpl);
			}

			if((Long) fieldSet.readLong("COHI_CD") != null) {
				HistoricoConsumoImpl historicoConsumo = new HistoricoConsumoImpl();

				historicoConsumo.setChavePrimaria(fieldSet.readLong("COHI_CD"));
				f.setHistoricoConsumo(historicoConsumo);
			}

			if((Long) fieldSet.readLong("MEHI_CD") != null) {
				HistoricoMedicaoImpl historicoMedicao = new HistoricoMedicaoImpl();

				historicoMedicao.setChavePrimaria(fieldSet.readLong("MEHI_CD"));
				f.setHistoricoMedicao(historicoMedicao);
			}

			if((Long) fieldSet.readLong("ENCO_CD_SITUACAO_PAGAMENTO") != null) {
				EntidadeConteudoImpl situacaoPagamento = new EntidadeConteudoImpl();

				situacaoPagamento.setChavePrimaria(fieldSet.readLong("ENCO_CD_SITUACAO_PAGAMENTO"));
				f.setSituacaoPagamento(situacaoPagamento);
			}

			if((Long) fieldSet.readLong("FAMR_CD") != null) {
				FaturaMotivoRevisaoImpl faturaMotivoRevisao = new FaturaMotivoRevisaoImpl();

				faturaMotivoRevisao.setChavePrimaria(fieldSet.readLong("FAMR_CD"));
				f.setMotivoRevisao(faturaMotivoRevisao);
			}

			if((Long) fieldSet.readLong("ENCO_CD_MOTIVO_INCLUSAO") != null) {
				EntidadeConteudoImpl entidadeConteudo = new EntidadeConteudoImpl();

				entidadeConteudo.setChavePrimaria(fieldSet.readLong("ENCO_CD_MOTIVO_INCLUSAO"));
				f.setMotivoInclusao(entidadeConteudo);
			}

			if((Long) fieldSet.readLong("ENCO_CD_MOTIVO_REFATURAMENTO") != null) {
				EntidadeConteudoImpl entidadeConteudo = new EntidadeConteudoImpl();

				entidadeConteudo.setChavePrimaria(fieldSet.readLong("ENCO_CD_MOTIVO_REFATURAMENTO"));
				f.setMotivoRefaturamento(entidadeConteudo);
			}

			if((Long) fieldSet.readLong("ENCO_CD_MOTIVO_CANCELAMENTO") != null) {
				EntidadeConteudoImpl entidadeConteudo = new EntidadeConteudoImpl();

				entidadeConteudo.setChavePrimaria(fieldSet.readLong("ENCO_CD_MOTIVO_CANCELAMENTO"));
				f.setMotivoCancelamento(entidadeConteudo);
			}

			if((Long) fieldSet.readLong("DEVO_CD") != null) {
				DevolucaoImpl devolucao = new DevolucaoImpl();

				devolucao.setChavePrimaria(fieldSet.readLong("DEVO_CD"));
				f.setDevolucao(devolucao);
			}

			if((Long) fieldSet.readLong("PARC_CD") != null) {
				ParcelamentoImpl parcelamento = new ParcelamentoImpl();

				parcelamento.setChavePrimaria(fieldSet.readLong("PARC_CD"));
				f.setParcelamento(parcelamento);
			}

			if((Long) fieldSet.readLong("FATU_CD_AGRUPADA") != null) {
				FaturaImpl fatura = new FaturaImpl();

				fatura.setChavePrimaria(fieldSet.readLong("FATU_CD_AGRUPADA"));
				f.setFaturaAgrupada(fatura);
			}

			if((Long) fieldSet.readLong("CONT_CD_PAI") != null) {
				Contrato contrato = new ContratoImpl();

				contrato.setChavePrimaria(fieldSet.readLong("CONT_CD_PAI"));
				f.setContrato(contrato);
			}

			if((Long) fieldSet.readLong("CONT_CD_ATUAL") != null) {
				Contrato contrato = new ContratoImpl();

				contrato.setChavePrimaria(fieldSet.readLong("CONT_CD_ATUAL"));
				f.setContratoAtual(contrato);
			}

			if((Long) fieldSet.readLong("NAOC_CD") != null) {
				NaturezaOperacaoCFOPImpl naturezaOperacaoCFOP = new NaturezaOperacaoCFOPImpl();

				naturezaOperacaoCFOP.setChavePrimaria(fieldSet.readLong("NAOC_CD"));
				f.setNaturezaOperacaoCFOP(naturezaOperacaoCFOP);
			}

			f.setChavePrimaria(fieldSet.readLong("FATU_CD"));
			f.setDataEmissao(fieldSet.readDate("FATU_DT_EMISSAO"));
			f.setDataVencimento(fieldSet.readDate("FATU_DT_VENCIMENTO"));
			f.setAnoMesReferencia(fieldSet.readInt("FATU_DT_AM_REFERENCIA"));
			f.setNumeroCiclo(fieldSet.readInt("FATU_NR_CICLO"));
			f.setValorTotal(fieldSet.readBigDecimal("FATU_VL_TOTAL"));
			f.setHabilitado(true);
			f.setVersao(1);
			f.setUltimaAlteracao(new Date());
			f.setDataRevisao(fieldSet.readDate("FATU_DT_REVISAO"));
			f.setObservacaoNota(fieldSet.readString("FATU_DS_OBSERVACAO_NOTA"));
			f.setMotivoAlteracao(fieldSet.readString("FATU_DS_MOTIVO_ALTER_VENC"));
			f.setValorConciliado(fieldSet.readBigDecimal("FATU_VL_CONCILIADO"));
			f.setDescricaoCancelamento(fieldSet.readString("FATU_DS_MOTIVO_CANCELA_NOTA"));
			f.setCobrada(fieldSet.readBoolean("FATU_IN_COBRADA"));
			f.setEncerramento(fieldSet.readBoolean("FATU_IN_ENCERRAMENTO"));
			f.setRegistroPerda(fieldSet.readBoolean("FATU_IN_REGISTRO_PERDA"));
			f.setProvisaoDevedoresDuvidosos(fieldSet.readBoolean("FATU_IN_REGISTRO_PERDA"));
			f.setProvisaoDevedoresDuvidosos(fieldSet.readBoolean("FATU_IN_PDD"));
			f.setDataCancelamento(fieldSet.readDate("FATU_DT_CANCELAMENTO"));
		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return f;
	}

}
