
package br.com.ggas.extrator.mappers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import br.com.ggas.cadastro.cliente.impl.EsferaPoderImpl;
import br.com.ggas.cadastro.cliente.impl.TipoClienteImpl;
import br.com.ggas.cadastro.cliente.impl.TipoPessoaImpl;
import br.com.ggas.extrator.FieldSetMapper;
import br.com.ggas.extrator.Fieldset;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade CLIENTE_TIPO e atributos da classe ClienteTipo
 */
public class ClienteTipoFieldSetMapper implements FieldSetMapper<TipoClienteImpl> {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.extrator.FieldSetMapper#mapFieldSet(br.com.ggas.extrator.Fieldset)
	 */
	@Override
	public TipoClienteImpl mapFieldSet(Fieldset fieldSet) {

		TipoClienteImpl tipoCliente = new TipoClienteImpl();

		try {
			if (fieldSet.readLong("ESPO_CD") != null) {
				EsferaPoderImpl esferaPoder = new EsferaPoderImpl();

				esferaPoder.setChavePrimaria(fieldSet.readLong("ESPO_CD"));
				tipoCliente.setEsferaPoder(esferaPoder);
			}

			tipoCliente.setChavePrimaria(fieldSet.readLong("CLTI_CD"));
			tipoCliente.setDescricao(fieldSet.readString("CLTI_DS"));
			tipoCliente.setTipo(fieldSet.readString("CLTI_IN_TIPO_PESSOA"));

			tipoCliente.setTipoPessoa(new TipoPessoaImpl(fieldSet.readInt("CLTI_IN_TIPO_PESSOA")));
			tipoCliente.setVersao(1);
			tipoCliente.setHabilitado(true);
			tipoCliente.setUltimaAlteracao(new Date());
		} catch(Exception e) {
			Log.error(e.getStackTrace(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}

		return tipoCliente;
	}

	/**
	 * Map field set.
	 * 
	 * @param lista
	 *            the lista
	 * @return the list
	 */
	public List<TipoClienteImpl> mapFieldSet(List<Fieldset> lista) {

		List<TipoClienteImpl> tipos = new ArrayList<>();

		for (Fieldset f : lista) {
			tipos.add(mapFieldSet(f));
		}
		return tipos;
	}

}
