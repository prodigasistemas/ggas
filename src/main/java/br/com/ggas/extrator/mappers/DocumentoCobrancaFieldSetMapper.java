
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.arrecadacao.CobrancaDebitoSituacao;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.impl.CobrancaDebitoSituacaoImpl;
import br.com.ggas.arrecadacao.impl.TipoDocumentoImpl;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.faturamento.impl.DocumentoCobrancaImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade DOCUMENTO_COBRANCA e atributos da classe DocumentoCobranca
 */
public class DocumentoCobrancaFieldSetMapper implements FieldSetMapper<DocumentoCobrancaImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public DocumentoCobrancaImpl mapFieldSet(FieldSet field) {

		DocumentoCobrancaImpl doc = new DocumentoCobrancaImpl();
		try {
			doc.setChavePrimaria(field.readLong("DOCO_CD"));
			doc.setVersao(field.readInt("DOCO_NR_VERSAO"));
			doc.setUltimaAlteracao(Calendar.getInstance().getTime());
			doc.setHabilitado(true);
			doc.setDataEmissao(field.readDate("DOCO_DT_EMISSAO"));
			doc.setDataVencimento(field.readDate("DOCO_DT_VENCIMENTO"));
			doc.setSequencial(field.readInt("DOCO_NR_SEQUENCIAL"));
			doc.setSequencialImpressao(field.readInt("DOCO_NR_SEQUENCIAL_IMPRESSAO"));
			doc.setNossoNumero(field.readLong("DOCO_NR_NOSSO_NUMERO"));
			doc.setValorTotal(field.readBigDecimal("DOCO_VL_TOTAL"));

			if((Long) field.readLong("CLIE_CD") != null) {
				Cliente cliente = new ClienteImpl();
				cliente.setChavePrimaria(field.readLong("CLIE_CD"));
				doc.setCliente(cliente);

			}
			if((Long) field.readLong("DOTI_CD") != null) {
				TipoDocumento tipoDocumento = new TipoDocumentoImpl();
				tipoDocumento.setChavePrimaria(field.readLong("DOTI_CD"));
				doc.setTipoDocumento(tipoDocumento);
			}

			if((Long) field.readLong("DOTI_CD") != null) {
				CobrancaDebitoSituacao cobrancaDebitoSituacao = new CobrancaDebitoSituacaoImpl();
				cobrancaDebitoSituacao.setChavePrimaria(field.readLong("DOTI_CD"));
				doc.setCobrancaDebitoSituacao(cobrancaDebitoSituacao);
			}

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}
		return doc;
	}

}
