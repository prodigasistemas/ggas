
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.cadastro.imovel.impl.ImovelRamoAtividadeImpl;
import br.com.ggas.cadastro.imovel.impl.RamoAtividadeImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade IMOVEL_RAMO_ATIVIDADE e atributos da classe ImovelRamoAtividade
 */
public class ImovelRamoAtividadeFieldSetMapper implements FieldSetMapper<ImovelRamoAtividadeImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ImovelRamoAtividadeImpl mapFieldSet(FieldSet fieldSet) {

		ImovelRamoAtividadeImpl i = new ImovelRamoAtividadeImpl();

		try {
			if((Long) fieldSet.readLong("IMOV_CD") != null) {
				ImovelImpl imovelImpl = new ImovelImpl();

				imovelImpl.setChavePrimaria(fieldSet.readLong("IMOV_CD"));
				i.setImovel(imovelImpl);
			}

			if((Long) fieldSet.readLong("RAAT_CD") != null) {
				RamoAtividadeImpl ramoAtividade = new RamoAtividadeImpl();

				ramoAtividade.setChavePrimaria(fieldSet.readLong("RAAT_CD"));
				i.setRamoAtividade(ramoAtividade);
			}

			i.setChavePrimaria(fieldSet.readLong("IMRA_CD"));
			i.setQuantidadeEconomia(fieldSet.readInt("IMRA_QN_ECONOMIA"));
			i.setVersao(1);
			i.setHabilitado(true);
			i.setUltimaAlteracao(new Date());

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return i;
	}

}
