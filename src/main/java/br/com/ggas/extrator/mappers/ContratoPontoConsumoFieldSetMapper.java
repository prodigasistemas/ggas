
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.impl.CepImpl;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.impl.ContratoImpl;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.impl.AcaoAnormalidadeConsumoImpl;
import br.com.ggas.medicao.leitura.TipoMedicao;
import br.com.ggas.medicao.leitura.impl.TipoMedicaoImpl;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.impl.PeriodicidadeImpl;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.medicao.vazaocorretor.impl.FaixaPressaoFornecimentoImpl;
import br.com.ggas.medicao.vazaocorretor.impl.UnidadeImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade CONTRATO_PONTO_CONSUMO e atributos da classe ContratoPontoConsumo
 */

public class ContratoPontoConsumoFieldSetMapper implements FieldSetMapper<ContratoPontoConsumoImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ContratoPontoConsumoImpl mapFieldSet(FieldSet field) {

		ContratoPontoConsumoImpl contratoPonto = new ContratoPontoConsumoImpl();
		try {

			contratoPonto.setChavePrimaria(field.readLong("COPC_CD"));
			contratoPonto.setVersao(field.readInt("COPC_NR_VERSAO"));
			contratoPonto.setUltimaAlteracao(Calendar.getInstance().getTime());
			contratoPonto.setMedidaPressao(field.readBigDecimal("COPC_MD_PRESSAO"));
			contratoPonto.setMedidaPressaoMinima(field.readBigDecimal("COPC_MD_PRESSAO_MINIMA"));
			contratoPonto.setMedidaPressaoMaxima(field.readBigDecimal("COPC_MD_PRESSAO_MAXIMA"));
			contratoPonto.setMedidaPressaoLimite(field.readBigDecimal("COPC_MD_PRESSAO_LIMITE"));
			contratoPonto.setMedidaVazaoInstantanea(field.readBigDecimal("COPC_MD_VAZAO_MAX_INSTANTANEA"));
			contratoPonto.setMedidaVazaoMaximaInstantanea(field.readBigDecimal("COPC_MD_VAZAO_INSTANTANEA"));
			contratoPonto.setMedidaVazaoMinimaInstantanea(field.readBigDecimal("COPC_MD_VAZAO_MIN_INSTANTANEA"));
			contratoPonto.setMedidaFornecimentoMaxDiaria(field.readBigDecimal("COPC_MD_FORNEC_MAX_DIARIO"));
			contratoPonto.setMedidaFornecimentoMinDiaria(field.readBigDecimal("COPC_MD_FORNEC_MIN_DIARIO"));
			contratoPonto.setMedidaFornecimentoMinMensal(field.readBigDecimal("COPC_MD_FORNEC_MIN_MENSAL"));
			contratoPonto.setMedidaFornecimentoMinAnual(field.readBigDecimal("COPC_MD_FORNEC_MIN_ANUAL"));
			contratoPonto.setNumeroFatorCorrecao(field.readBigDecimal("COPC_NR_FATOR_CORRECAO"));
			contratoPonto.setNumeroDiasVencimento(field.readInt("COPC_NR_DIA_VENCIMENTO"));
			contratoPonto.setIndicadorParadaProgramada(field.readBoolean("COPC_IN_PARADA_PROGRAMADA"));
			contratoPonto.setQuantidadeAnosParadaCliente(field.readInt("COPC_QN_ANOS_PARADA_CLIE"));
			contratoPonto.setQuantidadeTotalParadaCliente(field.readInt("COPC_QN_TOTAL_PARADA_CLIE"));
			contratoPonto.setQuantidadeMaximaAnoParadaCliente(field.readInt("COPC_QN_MAX_ANO_PARADA_CLIE"));
			contratoPonto.setDiasAntecedentesParadaCliente(field.readInt("COPC_QN_DIAS_ANTEC_PARADA_CLIE"));
			contratoPonto.setDiasConsecutivosParadaCliente(field.readInt("COPC_QN_DIA_CONSEC_PARADA_CLIE"));
			contratoPonto.setQuantidadeAnosParadaCDL(field.readInt("COPC_QN_ANOS_PARADA_CDL"));
			contratoPonto.setQuantidadeTotalParadaCDL(field.readInt("COPC_QN_TOTAL_PARADA_CDL"));
			contratoPonto.setQuantidadeMaximaAnoParadaCDL(field.readInt("COPC_QN_MAX_ANO_PARADA_CDL"));
			contratoPonto.setDiasAntecedentesParadaCDL(field.readInt("COPC_QN_DIAS_ANTEC_PARADA_CDL"));
			contratoPonto.setDiasConsecutivosParadaCDL(field.readInt("COPC_QN_DIA_CONSEC_PARADA_CDL"));
			contratoPonto.setDataInicioTeste(field.readDate("COPC_DT_INICIO_TESTE"));
			contratoPonto.setDataFimTeste(field.readDate("COPC_DT_FIM_TESTE"));
			contratoPonto.setNumeroImovel(field.readString("COPC_NR_IMOVEL"));
			contratoPonto.setComplementoEndereco(field.readString("COPC_DS_COMPLEMENTO_ENDERECO"));
			contratoPonto.setEmailEntrega(field.readString("COPC_DS_EMAIL_ENTREGA"));
			contratoPonto.setIndicadorCorretorVazao(field.readBoolean("COPC_IN_CORRETOR_VAZAO"));
			contratoPonto.setMedidaConsumoTeste(field.readBigDecimal("COPC_MD_CONSUMO_TESTE"));
			contratoPonto.setPrazoConsumoTeste(field.readInt("COPC_NR_PRAZO_CONSUMO_TESTE"));
			contratoPonto.setDiaGarantiaConversao(field.readInt("COPC_NR_DIA_GARANTIA_CONVERSAO"));
			contratoPonto.setDataInicioGarantiaConversao(field.readDate("COPC_DT_INI_GARANTIA_CONVERSAO"));
			contratoPonto.setEmailOperacional(field.readString("COPC_DS_EMAIL_OPERACIONAL"));
			contratoPonto.setCodigoDDDFaxOperacional(field.readInt("COPC_CD_DDD_FAX_OPERACIONAL"));
			contratoPonto.setNumeroFaxOperacional(field.readInt("COPC_NR_FAX_OPERACIONAL"));
			contratoPonto.setDataConsumoTesteExcedido(field.readDate("COPC_DT_CONSUMO_TESTE_EXCEDIDO"));
			contratoPonto.setMedidaConsumoTesteUsado(field.readBigDecimal("COPC_MD_CONSUMO_TESTE_USADO"));
			contratoPonto.setIsEmiteNotaFiscalEletronica(field.readBoolean("COPC_IN_EMISSAO_NFE"));
			contratoPonto.setNumeroHoraInicial(field.readInt("COPC_NR_HORA_INICAL"));

			if((Long) field.readLong("PRFF_CD") != null) {
				FaixaPressaoFornecimento faixaPressaoFornecimento = new FaixaPressaoFornecimentoImpl();
				faixaPressaoFornecimento.setChavePrimaria(field.readLong("PRFF_CD"));
				contratoPonto.setFaixaPressaoFornecimento(faixaPressaoFornecimento);
			}
			if((Long) field.readLong("POCN_CD") != null) {
				PontoConsumo pontoConsumo = new PontoConsumoImpl();
				pontoConsumo.setChavePrimaria(field.readLong("POCN_CD"));
				contratoPonto.setPontoConsumo(pontoConsumo);
			}
			if((Long) field.readLong("CONT_CD") != null) {
				Contrato contrato = new ContratoImpl();
				contrato.setChavePrimaria(field.readLong("CONT_CD"));
				contratoPonto.setContrato(contrato);

			}
			if((Long) field.readLong("UNID_CD_PRESSAO") != null) {
				Unidade unidadePressao = new UnidadeImpl();
				unidadePressao.setChavePrimaria(field.readLong("UNID_CD_PRESSAO"));
				contratoPonto.setUnidadePressao(unidadePressao);
			}
			if((Long) field.readLong("UNID_CD_PRESSAO_MINIMA") != null) {
				Unidade unidadePressaoMinima = new UnidadeImpl();
				unidadePressaoMinima.setChavePrimaria(field.readLong("UNID_CD_PRESSAO_MINIMA"));
				contratoPonto.setUnidadePressaoMinima(unidadePressaoMinima);
			}
			if((Long) field.readLong("UNID_CD_PRESSAO_MAXIMA") != null) {
				Unidade unidadePressaoMaxima = new UnidadeImpl();
				unidadePressaoMaxima.setChavePrimaria(field.readLong("UNID_CD_PRESSAO_MAXIMA"));
				contratoPonto.setUnidadePressaoMaxima(unidadePressaoMaxima);
			}
			if((Long) field.readLong("UNID_CD_PRESSAO_LIMITE") != null) {
				Unidade unidadePressaoLimite = new UnidadeImpl();
				unidadePressaoLimite.setChavePrimaria(field.readLong("UNID_CD_PRESSAO_LIMITE"));
				contratoPonto.setUnidadePressaoLimite(unidadePressaoLimite);

			}
			if((Long) field.readLong("UNID_CD_VAZAO_INSTANTANEA") != null) {
				Unidade unidadeVazaoInstantanea = new UnidadeImpl();
				unidadeVazaoInstantanea.setChavePrimaria(field.readLong("UNID_CD_VAZAO_INSTANTANEA"));
				contratoPonto.setUnidadeVazaoInstantanea(unidadeVazaoInstantanea);
			}
			if((Long) field.readLong("UNID_CD_VAZAO_MAX_INSTANTANEA") != null) {
				Unidade unidadeVazaoMaximaInstantanea = new UnidadeImpl();
				unidadeVazaoMaximaInstantanea.setChavePrimaria(field.readLong("UNID_CD_VAZAO_MAX_INSTANTANEA"));
				contratoPonto.setUnidadeVazaoMaximaInstantanea(unidadeVazaoMaximaInstantanea);
			}
			if((Long) field.readLong("UNID_CD_VAZAO_MIN_INSTANTANEA") != null) {
				Unidade unidadeVazaoMinimaInstantanea = new UnidadeImpl();
				unidadeVazaoMinimaInstantanea.setChavePrimaria(field.readLong("UNID_CD_VAZAO_MIN_INSTANTANEA"));
				contratoPonto.setUnidadeVazaoMinimaInstantanea(unidadeVazaoMinimaInstantanea);
			}
			if((Long) field.readLong("UNID_CD_FORNEC_MAX_DIARIO") != null) {
				Unidade unidadeFornecimentoMaxDiaria = new UnidadeImpl();
				unidadeFornecimentoMaxDiaria.setChavePrimaria(field.readLong("UNID_CD_FORNEC_MAX_DIARIO"));
				contratoPonto.setUnidadeFornecimentoMaxDiaria(unidadeFornecimentoMaxDiaria);
			}
			if((Long) field.readLong("UNID_CD_FORNEC_MIN_DIARIO") != null) {
				Unidade unidadeFornecimentoMinDiaria = new UnidadeImpl();
				unidadeFornecimentoMinDiaria.setChavePrimaria(field.readLong("UNID_CD_FORNEC_MIN_DIARIO"));
				contratoPonto.setUnidadeFornecimentoMinDiaria(unidadeFornecimentoMinDiaria);
			}
			if((Long) field.readLong("UNID_CD_FORNEC_MIN_MENSAL") != null) {
				Unidade unidadeFornecimentoMinMensal = new UnidadeImpl();
				unidadeFornecimentoMinMensal.setChavePrimaria(field.readLong("UNID_CD_FORNEC_MIN_DIARIO"));
				contratoPonto.setUnidadeFornecimentoMinMensal(unidadeFornecimentoMinMensal);
			}
			if((Long) field.readLong("UNID_CD_FORNEC_MIN_ANUAL") != null) {
				Unidade unidadeFornecimentoMinAnual = new UnidadeImpl();
				unidadeFornecimentoMinAnual.setChavePrimaria(field.readLong("UNID_CD_FORNEC_MIN_ANUAL"));
				contratoPonto.setUnidadeFornecimentoMinAnual(unidadeFornecimentoMinAnual);
			}
			if((Long) field.readLong("LEAC_CD_SEM_LEITURA") != null) {
				AcaoAnormalidadeConsumo acaoAnormalidadeConsumoSemLeitura = new AcaoAnormalidadeConsumoImpl();
				acaoAnormalidadeConsumoSemLeitura.setChavePrimaria(field.readLong("LEAC_CD_SEM_LEITURA"));
				contratoPonto.setAcaoAnormalidadeConsumoSemLeitura(acaoAnormalidadeConsumoSemLeitura);
			}
			if((Long) field.readLong("METO_CD") != null) {
				TipoMedicao tipoMedicao = new TipoMedicaoImpl();
				tipoMedicao.setChavePrimaria(field.readLong("METO_CD"));
				contratoPonto.setTipoMedicao(tipoMedicao);
			}
			if((Long) field.readLong("ENCO_CD_REGIME_CONSUMO") != null) {
				EntidadeConteudo regimeConsumo = new EntidadeConteudoImpl();
				regimeConsumo.setChavePrimaria(field.readLong("ENCO_CD_REGIME_CONSUMO"));
				contratoPonto.setRegimeConsumo(regimeConsumo);
			}
			if((Long) field.readLong("CEP_CD_ENTREGA") != null) {
				Cep cep = new CepImpl();
				cep.setChavePrimaria(field.readLong("CEP_CD_ENTREGA"));
				contratoPonto.setCep(cep);
			}
			if((Long) field.readLong("ENCO_CD_CONTRATO_COMPRA") != null) {
				EntidadeConteudo contratoCompra = new EntidadeConteudoImpl();
				contratoCompra.setChavePrimaria(field.readLong("ENCO_CD_CONTRATO_COMPRA"));
				contratoPonto.setContratoCompra(contratoCompra);
			}
			if((Long) field.readLong("PERI_CD") != null) {
				Periodicidade periodicidade = new PeriodicidadeImpl();
				periodicidade.setChavePrimaria(field.readLong("PERI_CD"));
				contratoPonto.setPeriodicidade(periodicidade);
			}

		} catch(Exception e) {
			Log.error(e.getStackTrace(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}
		return contratoPonto;
	}

}
