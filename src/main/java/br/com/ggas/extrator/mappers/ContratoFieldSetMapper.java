
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.contrato.contrato.ModeloContrato;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.contrato.contrato.impl.ContratoImpl;
import br.com.ggas.contrato.contrato.impl.ModeloContratoImpl;
import br.com.ggas.contrato.contrato.impl.SituacaoContratoImpl;
import br.com.ggas.contrato.proposta.Proposta;
import br.com.ggas.contrato.proposta.impl.PropostaImpl;
import br.com.ggas.faturamento.tarifa.IndiceFinanceiro;
import br.com.ggas.faturamento.tarifa.impl.IndiceFinanceiroImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade CONTRATO e atributos da classe Contrato
 */
public class ContratoFieldSetMapper implements FieldSetMapper<ContratoImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ContratoImpl mapFieldSet(FieldSet field) {

		ContratoImpl contrato = new ContratoImpl();

		try {
			contrato.setChavePrimaria(field.readLong("CONT_CD"));
			contrato.setVersao(field.readInt("CONT_NR_VERSAO"));
			contrato.setUltimaAlteracao(Calendar.getInstance().getTime());
			contrato.setHabilitado(field.readBoolean("CONT_IN_USO"));
			contrato.setNumero(field.readInt("CONT_NR"));
			contrato.setAnoContrato(field.readInt("CONT_DT_ANO"));
			contrato.setDataAssinatura(field.readDate("CONT_DT_ASSINATURA"));
			contrato.setNumeroAnterior(field.readString("CONT_NR_ANTERIOR"));
			contrato.setNumeroAditivo(field.readInt("CONT_NR_ADITIVO"));
			contrato.setDataAditivo(field.readDate("CONT_DT_ADITIVO"));
			contrato.setValorGastoMensal(field.readBigDecimal("CONT_VL_GASTO_MENSAL"));
			contrato.setMedidaEconomiaMensalGN(field.readBigDecimal("CONT_MD_ECONOMIA_MENSAL_GN"));
			contrato.setMedidaEconomiaAnualGN(field.readBigDecimal("CONT_MD_ECONOMIA_ANUAL_GN"));
			contrato.setPercentualEconomiaGN(field.readBigDecimal("CONT_PR_ECONOMIA_GN"));
			contrato.setNumeroDiaVencimentoFinanciamento(field.readInt("CONT_NR_DIA_VENC_FINANCIAMENTO"));
			contrato.setDataFimGarantiaFinanciamento(field.readDate("CONT_DT_FIM_GARANTIA_FINANC"));
			contrato.setDataInicioGarantiaFinanciamento(field.readDate("CONT_DT_INI_GARANTIA_FINANC"));
			contrato.setValorGarantiaFinanceira(field.readBigDecimal("CONT_VL_GARANTIA_FINANCEIRA"));
			contrato.setDescricaoGarantiaFinanceira(field.readString("CONT_DS_GARANTIA_FINANCEIRA"));
			contrato.setRenovacaoGarantiaFinanceira(field.readBoolean("CONT_IN_RENOV_GARANTIA_FINANC"));
			contrato.setDiasAntecedenciaRevisaoGarantiaFinanceira(field.readInt("CONT_NR_ANT_REV_GARANT_FINANC"));
			contrato.setPeriodicidadeReavaliacaoGarantias(field.readInt("CONT_NR_PERI_REAV_GARANT"));
			contrato.setRenovacaoAutomatica(field.readBoolean("CONT_IN_RENOVACAO_AUTOMATICA"));
			contrato.setDiasAntecedenciaRenovacao(field.readInt("CONT_NR_DIAS_ANTECED_RENOVACAO"));
			contrato.setDescricaoAditivo(field.readString("CONT_DS_ADITIVO"));
			contrato.setAgrupamentoConta(field.readBoolean("CONT_IN_AGRUPAMENTO_CONTA"));
			contrato.setAgrupamentoCobranca(field.readBoolean("CONT_IN_AGRUPAMENTO_COBRANCA"));
			contrato.setNumeroEmpenho(field.readString("CONT_NR_EMPENHO"));
			contrato.setNumeroDiasRenovacaoAutomatica(field.readInt("CONT_NR_DIAS_RENOVACAO_AUTOM"));
			contrato.setDataVencimentoObrigacoes(field.readDate("CONT_DT_VENC_OBRIGACOES"));
			contrato.setValorContrato(field.readBigDecimal("CONT_VL"));
			contrato.setIndicadorMulta(field.readBoolean("CONT_IN_MULTA"));
			contrato.setIndicadorJuros(field.readBoolean("CONT_IN_JUROS"));
			contrato.setValorParticipacaoCliente(field.readBigDecimal("CONT_VL_FINANCIAMENTO"));
			contrato.setQtdParcelasFinanciamento(field.readInt("CONT_NR_PARCELA_FINACIMENTO"));
			contrato.setPercentualJurosFinanciamento(field.readBigDecimal("CONT_PR_JUROS"));
			contrato.setPercentualTarifaDoP(field.readBigDecimal("CONT_PR_TARIFA_DOP"));
			contrato.setIndicadorAnoContratual(field.readBoolean("CONT_IN_ANO_CONTRATUAL"));
			contrato.setPercentualSobreTariGas(field.readBigDecimal("CONT_PR_TARIFA_GAS_FORA_ESPEC"));
			contrato.setValorInvestimento(field.readBigDecimal("CONT_VL_INVESTIMENTO"));
			contrato.setDataInvestimento(field.readDate("CONT_DT_INVESTIMENTO"));
			contrato.setFaturaEncerramentoGerada(field.readBoolean("CONT_IN_GERACAO_FATURA_ENC"));
			contrato.setDataRecisao(field.readDate("CONT_DT_RECISAO"));
			contrato.setChavePrimariaPai(field.readLong("CONT_CD_PAI"));

			if ((Long) field.readLong("ENCO_CD_INDICE_FINANCEIRO") != null) {
				IndiceFinanceiro indiceFinanceiro = new IndiceFinanceiroImpl();
				indiceFinanceiro.setChavePrimaria(field.readLong("INFI_CD"));
				contrato.setIndiceFinanceiro(indiceFinanceiro);

			}
			if ((Long) field.readLong("ENCO_CD_GARANTIA_FINANCEIRA") != null) {
				EntidadeConteudo garantiaFinanceira = new EntidadeConteudoImpl();
				garantiaFinanceira.setChavePrimaria(field.readLong("ENCO_CD_GARANTIA_FINANCEIRA"));
				contrato.setGarantiaFinanceira(garantiaFinanceira);

			}
			if ((Long) field.readLong("CLIE_CD_ASSINATURA") != null) {
				Cliente clienteAssinatura = new ClienteImpl();
				clienteAssinatura.setChavePrimaria(field.readLong("CLIE_CD_ASSINATURA"));
				contrato.setClienteAssinatura(clienteAssinatura);

			}

			if ((Long) field.readLong("PROP_CD") != null) {
				Proposta proposta = new PropostaImpl();
				proposta.setChavePrimaria(field.readLong("PROP_CD"));
				contrato.setProposta(proposta);

			}
			if ((Long) field.readLong("COSI_CD") != null) {
				SituacaoContrato situacao = new SituacaoContratoImpl();
				situacao.setChavePrimaria(field.readLong("COSI_CD"));
				contrato.setSituacao(situacao);
			}
			if ((Long) field.readLong("COME_CD") != null) {
				ModeloContrato modeloContrato = new ModeloContratoImpl();
				modeloContrato.setChavePrimaria(field.readLong("COME_CD"));
				contrato.setModeloContrato(modeloContrato);
			}
			if ((Long) field.readLong("ENCO_CD_AMORTIZACAO") != null) {
				EntidadeConteudo sistemaAmortizacao = new EntidadeConteudoImpl();
				sistemaAmortizacao.setChavePrimaria(field.readLong("ENCO_CD_AMORTIZACAO"));
				contrato.setSistemaAmortizacao(sistemaAmortizacao);

			}
			if ((Long) field.readLong("ENCO_CD_TIPO_AGRUPAMENTO") != null) {
				EntidadeConteudo tipoAgrupamento = new EntidadeConteudoImpl();
				tipoAgrupamento.setChavePrimaria(field.readLong("ENCO_CD_TIPO_AGRUPAMENTO"));
				contrato.setTipoAgrupamento(tipoAgrupamento);
			}
			if ((Long) field.readLong("ENCO_CD_FORMA_COBRANCA") != null) {
				EntidadeConteudo formaCobranca = new EntidadeConteudoImpl();
				formaCobranca.setChavePrimaria(field.readLong("ENCO_CD_FORMA_COBRANCA"));
				contrato.setFormaCobranca(formaCobranca);
			}
			if ((Long) field.readLong("ENCO_CD_MULTA_RECISORIA") != null) {
				EntidadeConteudo multaRecisoria = new EntidadeConteudoImpl();
				multaRecisoria.setChavePrimaria(field.readLong("ENCO_CD_MULTA_RECISORIA"));
				contrato.setMultaRecisoria(multaRecisoria);

			}
		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return contrato;

	}

}
