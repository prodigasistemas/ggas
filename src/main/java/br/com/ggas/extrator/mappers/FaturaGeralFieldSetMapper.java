
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.faturamento.impl.FaturaGeralImpl;
import br.com.ggas.faturamento.impl.FaturaImpl;

/**
 * Classe responsável por fatura geral.
 */
public class FaturaGeralFieldSetMapper implements FieldSetMapper<FaturaGeralImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public FaturaGeralImpl mapFieldSet(FieldSet fieldSet) {

		FaturaGeralImpl f = new FaturaGeralImpl();

		if((Long) fieldSet.readLong("FATU_CD_ATUAL") != null) {
			FaturaImpl fatura = new FaturaImpl();

			fatura.setChavePrimaria(fieldSet.readLong("FATU_CD_ATUAL"));
			f.setFaturaAtual(fatura);
		}

		f.setChavePrimaria(fieldSet.readLong("FAGE_CD"));
		f.setHabilitado(true);
		f.setVersao(1);
		f.setUltimaAlteracao(new Date());

		return f;
	}

}
