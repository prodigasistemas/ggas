
package br.com.ggas.extrator.mappers;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.leitura.impl.InstalacaoMedidorImpl;
import br.com.ggas.medicao.leitura.impl.MedidorLocalInstalacaoImpl;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.medidor.impl.HistoricoOperacaoMedidorImpl;
import br.com.ggas.medicao.medidor.impl.MedidorImpl;
import br.com.ggas.medicao.medidor.impl.MotivoOperacaoMedidorImpl;
import br.com.ggas.medicao.medidor.impl.OperacaoMedidorImpl;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.medicao.vazaocorretor.impl.UnidadeImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade MEDIDOR_OPERACAO_HISTORICO e atributos da classe
 * HistoricoOperacaoMedidor
 */
public class HistoricoOperacaoMedidorFieldSetMapper implements FieldSetMapper<HistoricoOperacaoMedidorImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public HistoricoOperacaoMedidorImpl mapFieldSet(FieldSet field) {

		HistoricoOperacaoMedidorImpl historico = new HistoricoOperacaoMedidorImpl();

		try {

			historico.setChavePrimaria(field.readLong("MEOH_CD"));
			historico.setVersao(field.readInt("MEOH_NR_VERSAO"));
			historico.setUltimaAlteracao(field.readDate("MEOH_TM_ULTIMA_ALTERACAO"));
			historico.setHabilitado(field.readBoolean("MEOH_IN_USO"));
			historico.setDescricao(field.readString("MEOH_DS"));
			historico.setDataPlanejada(field.readDate("MEOH_DT_PLANEJADA"));
			historico.setDataRealizada(field.readDate("MEOH_DT_REALIZADA"));
			historico.setNumeroLeitura(field.readBigDecimal("MEOH_MD_LEITURA"));
			historico.setMedidaPressaoAnterior(field.readBigDecimal("MEOH_MD_PRESSAO_ANTERIOR"));

			Long idMedidor = field.readLong("MEDI_CD");
			Medidor medidor = new MedidorImpl();
			medidor.setChavePrimaria(idMedidor);
			historico.setMedidor(medidor);

			Long idMedidorLocalInstalacao = field.readLong("MELI_CD");
			MedidorLocalInstalacao medidorLocalInstalacao = new MedidorLocalInstalacaoImpl();
			medidorLocalInstalacao.setChavePrimaria(idMedidorLocalInstalacao);
			historico.setMedidorLocalInstalacao(medidorLocalInstalacao);

			Long idFuncionario = field.readLong("FUNC_CD");
			Funcionario funcionario = new FuncionarioImpl();
			funcionario.setChavePrimaria(idFuncionario);
			historico.setFuncionario(funcionario);

			OperacaoMedidor operacaoMedidor = new OperacaoMedidorImpl();
			operacaoMedidor.setChavePrimaria(field.readLong("MEOP_CD"));
			historico.setOperacaoMedidor(operacaoMedidor);

			MotivoOperacaoMedidor motivoOperacaoMedidor = new MotivoOperacaoMedidorImpl();
			motivoOperacaoMedidor.setChavePrimaria(field.readLong("MEMP_CD"));
			historico.setMotivoOperacaoMedidor(motivoOperacaoMedidor);

			PontoConsumo pontoConsumo = new PontoConsumoImpl();
			pontoConsumo.setChavePrimaria(field.readLong("POCN_CD"));
			historico.setPontoConsumo(pontoConsumo);

			Unidade unidadePressaoAnterior = new UnidadeImpl();
			unidadePressaoAnterior.setChavePrimaria(field.readLong("UNID_CD_PRESSAO_ANTERIOR"));
			historico.setUnidadePressaoAnterior(unidadePressaoAnterior);

			InstalacaoMedidor instalacaoMedidor = new InstalacaoMedidorImpl();
			instalacaoMedidor.setChavePrimaria(field.readLong("MEIN_CD"));
			historico.setInstalacaoMedidor(instalacaoMedidor);

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return historico;
	}

}
