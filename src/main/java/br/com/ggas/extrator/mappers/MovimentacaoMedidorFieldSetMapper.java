
package br.com.ggas.extrator.mappers;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.cadastro.operacional.impl.MedidorLocalArmazenagemImpl;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MotivoMovimentacaoMedidor;
import br.com.ggas.medicao.medidor.impl.MedidorImpl;
import br.com.ggas.medicao.medidor.impl.MotivoMovimentacaoMedidorImpl;
import br.com.ggas.medicao.medidor.impl.MovimentacaoMedidorImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade MEDIDOR_MOVIMENTACAO e atributos da classe MovimentacaoMedidor
 */
public class MovimentacaoMedidorFieldSetMapper implements FieldSetMapper<MovimentacaoMedidorImpl> {

	private static final Logger LOG = Logger.getLogger(MovimentacaoMedidorFieldSetMapper.class);

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public MovimentacaoMedidorImpl mapFieldSet(FieldSet field) {

		MovimentacaoMedidorImpl mov = new MovimentacaoMedidorImpl();

		try {

			mov.setChavePrimaria(field.readLong("MEMO_CD"));
			mov.setVersao(field.readInt("MEMO_NR_VERSAO"));
			mov.setUltimaAlteracao(field.readDate("MEMO_TM_ULTIMA_ALTERACAO"));
			mov.setHabilitado(field.readBoolean("MEMO_IN_USO"));
			mov.setDataMovimento(field.readDate("MEMO_DT"));
			mov.setDescricaoParecer(field.readString("MEMO_DS_PARECER"));

			Medidor medidor = new MedidorImpl();
			medidor.setChavePrimaria(field.readLong("MEDI_CD"));
			mov.setMedidor(medidor);

			MotivoMovimentacaoMedidor motivoMovimentacaoMedidor = new MotivoMovimentacaoMedidorImpl();
			motivoMovimentacaoMedidor.setChavePrimaria(field.readLong("MEMM_CD"));
			mov.setMotivoMovimentacaoMedidor(motivoMovimentacaoMedidor);

			MedidorLocalArmazenagem localArmazenagemOrigem = new MedidorLocalArmazenagemImpl();
			localArmazenagemOrigem.setChavePrimaria(field.readLong("MELA_CD_ORIGEM"));
			mov.setLocalArmazenagemOrigem(localArmazenagemOrigem);

			MedidorLocalArmazenagem localArmazenagemDestino = new MedidorLocalArmazenagemImpl();
			localArmazenagemDestino.setChavePrimaria(field.readLong("MELA_CD_DESTINO"));
			mov.setLocalArmazenagemDestino(localArmazenagemDestino);

			Funcionario funcionario = new FuncionarioImpl();
			funcionario.setChavePrimaria(field.readLong("FUNC_CD"));
			mov.setFuncionario(funcionario);
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return mov;
	}

}
