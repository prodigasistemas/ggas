
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.cliente.impl.EsferaPoderImpl;

/**
 * Classe responsável por esfera do poder.
 */
public class EsferaPoderFieldSetMapper implements FieldSetMapper<EsferaPoderImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public EsferaPoderImpl mapFieldSet(FieldSet fieldSet) {

		EsferaPoderImpl esferaPoder = new EsferaPoderImpl();

		esferaPoder.setChavePrimaria(fieldSet.readLong("ESPO_CD"));
		esferaPoder.setDescricao(fieldSet.readString("ESPO_DS"));
		esferaPoder.setVersao(1);
		esferaPoder.setHabilitado(true);
		esferaPoder.setUltimaAlteracao(new Date());

		return esferaPoder;
	}

}
