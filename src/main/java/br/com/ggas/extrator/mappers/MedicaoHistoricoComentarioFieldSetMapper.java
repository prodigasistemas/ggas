
package br.com.ggas.extrator.mappers;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.controleacesso.impl.UsuarioImpl;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.impl.HistoricoMedicaoImpl;
import br.com.ggas.medicao.leitura.impl.MedicaoHistoricoComentarioImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade MEDICAO_HISTORICO_COMENTARIO e atributos da classe MedicaoHistoricoComentario
 */
public class MedicaoHistoricoComentarioFieldSetMapper implements FieldSetMapper<MedicaoHistoricoComentarioImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public MedicaoHistoricoComentarioImpl mapFieldSet(FieldSet field) {

		MedicaoHistoricoComentarioImpl med = new MedicaoHistoricoComentarioImpl();
		try {

			med.setChavePrimaria(field.readLong("MEHC_CD"));
			med.setUltimaAlteracao(field.readDate("MEHC_TM_ULTIMA_ALTERACAO"));
			med.setVersao(field.readInt("MEHC_NR_VERSAO"));
			med.setHabilitado(field.readBoolean("MEHC_IN_USO"));
			med.setDescricao(field.readString("MEHC_DS"));
			med.setNumero(field.readInt("MEHC_NR_COMENTARIO"));

			HistoricoMedicao historicoMedicao = new HistoricoMedicaoImpl();
			historicoMedicao.setChavePrimaria(field.readLong("MEHI_CD"));
			med.setHistoricoMedicao(historicoMedicao);

			Usuario usuario = new UsuarioImpl();
			usuario.setChavePrimaria(field.readLong("USSI_CD"));
			med.setUsuario(usuario);

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return med;
	}

}
