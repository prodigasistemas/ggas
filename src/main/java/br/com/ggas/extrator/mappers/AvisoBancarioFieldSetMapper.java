
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorMovimento;
import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.arrecadacao.avisobancario.impl.AvisoBancarioImpl;
import br.com.ggas.arrecadacao.impl.ArrecadadorImpl;
import br.com.ggas.arrecadacao.impl.ArrecadadorMovimentoImpl;
import br.com.ggas.arrecadacao.impl.ContaBancariaImpl;
/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade e atributos da classe
 */
public class AvisoBancarioFieldSetMapper implements FieldSetMapper<AvisoBancarioImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public AvisoBancarioImpl mapFieldSet(FieldSet field) {

		AvisoBancarioImpl aviso = new AvisoBancarioImpl();
		try {
			aviso.setChavePrimaria(field.readLong("AVBA_CD"));
			aviso.setVersao(field.readInt("AVBA_NR_VERSAO"));
			aviso.setUltimaAlteracao(Calendar.getInstance().getTime());
			aviso.setHabilitado(true);
			aviso.setAnoMesContabil(field.readInt("AVBA_DT_AM_CONTABIL"));
			aviso.setIndicadorCreditoDebito(field.readBoolean("AVBA_IN_CREDITO_DEBITO"));
			aviso.setNumeroDocumento(field.readInt("AVBA_NR_DOCUMENTO"));
			aviso.setNumeroSequencial(field.readInt("AVBA_NR_SEQUENCIAL"));
			aviso.setDataLancamento(field.readDate("AVBA_DT_LANCAMENTO"));
			aviso.setValorContabilizado(field.readBigDecimal("AVBA_VL_CONTABILIZADO"));
			aviso.setValorRealizado(field.readBigDecimal("AVBA_VL_REALIZADO"));
			aviso.setDataPrevista(field.readDate("AVBA_DT_PREVISTA"));
			aviso.setDataRealizada(field.readDate("AVBA_DT_REALIZADA"));
			aviso.setValorArrecadacaoCalculado(field.readBigDecimal("AVBA_VL_ARRECADACAO_CALCULADO"));
			aviso.setValorDevolucaoCalculado(field.readBigDecimal("AVBA_VL_DEVOLUCAO_CALCULADO"));
			aviso.setValorArrecadacaoInformado(field.readBigDecimal("AVBA_VL_ARRECADACAO_INFORMADO"));
			aviso.setValorDevolucaoInformado(field.readBigDecimal("AVBA_VL_DEVOLUCAO_INFORMADO"));
			if((Long) field.readLong("ARRE_CD") != null) {
				Arrecadador arrecadador = new ArrecadadorImpl();
				arrecadador.setChavePrimaria(field.readLong("ARRE_CD"));
				aviso.setArrecadador(arrecadador);
			}
			if((Long) field.readLong("ARMO_CD") != null) {
				ArrecadadorMovimento arrecadadorMovimento = new ArrecadadorMovimentoImpl();
				arrecadadorMovimento.setChavePrimaria(field.readLong("ARMO_CD"));
				aviso.setArrecadadorMovimento(arrecadadorMovimento);

			}
			if((Long) field.readLong("COBA_CD") != null) {
				ContaBancaria contaBancaria = new ContaBancariaImpl();
				contaBancaria.setChavePrimaria(field.readLong("COBA_CD"));
				aviso.setContaBancaria(contaBancaria);
			}

		} catch(Exception e) {
			Log.error(e.getStackTrace(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}

		return aviso;
	}

}
