
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.cliente.impl.ProfissaoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoContatoImpl;
import br.com.ggas.cadastro.imovel.impl.ContatoImovelImpl;
import br.com.ggas.cadastro.imovel.impl.ImovelImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade CLIENTE_CONTATO e atributos da classe ContatoImovel
 */

public class ContatoImovelFieldSetMapper implements FieldSetMapper<ContatoImovelImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ContatoImovelImpl mapFieldSet(FieldSet fieldSet) {

		ContatoImovelImpl c = new ContatoImovelImpl();

		try {
			if((Long) fieldSet.readLong("IMOV_CD") != null) {
				ImovelImpl imovelImpl = new ImovelImpl();

				imovelImpl.setChavePrimaria(fieldSet.readLong("IMOV_CD"));
				c.setImovel(imovelImpl);
			}

			if((Long) fieldSet.readLong("COTI_CD") != null) {
				TipoContatoImpl tipoContato = new TipoContatoImpl();

				tipoContato.setChavePrimaria(fieldSet.readLong("COTI_CD"));
				c.setTipoContato(tipoContato);
			}

			if((Long) fieldSet.readLong("PROF_CD") != null) {
				ProfissaoImpl profissao = new ProfissaoImpl();

				profissao.setChavePrimaria(fieldSet.readLong("PROF_CD"));
				c.setProfissao(profissao);
			}

			c.setChavePrimaria(fieldSet.readLong("IMCO_CD"));
			c.setNome(fieldSet.readString("IMCO_NM"));
			c.setCodigoDDD(fieldSet.readInt("IMCO_CD_DDD"));
			c.setFone(fieldSet.readString("IMCO_NR_FONE"));
			c.setRamal(fieldSet.readString("IMCO_NR_RAMAL"));
			c.setEmail(fieldSet.readString("IMCO_DS_EMAIL"));
			c.setDescricaoArea(fieldSet.readString("IMCO_DS_AREA"));
			c.setPrincipal(fieldSet.readBoolean("IMCO_IN_PRINCIPAL"));
			c.setVersao(1);
			c.setHabilitado(true);
			c.setUltimaAlteracao(new Date());

		} catch(Exception e) {
			Log.error(e.getStackTrace(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}

		return c;
	}

}
