
package br.com.ggas.extrator.mappers;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.impl.AnormalidadeLeituraImpl;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.leitura.SituacaoLeitura;
import br.com.ggas.medicao.leitura.impl.HistoricoMedicaoImpl;
import br.com.ggas.medicao.leitura.impl.InstalacaoMedidorImpl;
import br.com.ggas.medicao.leitura.impl.LeituristaImpl;
import br.com.ggas.medicao.leitura.impl.SituacaoLeituraImpl;

/**
 * Classe responsável por Histórico Medição.
 *
 */
public class HistoricoMedicaoFieldSetMapper implements FieldSetMapper<HistoricoMedicaoImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public HistoricoMedicaoImpl mapFieldSet(FieldSet field) {

		HistoricoMedicaoImpl historico = new HistoricoMedicaoImpl();

		try {

			historico.setChavePrimaria(field.readLong("MEHI_CD"));
			historico.setVersao(field.readInt("MEHI_NR_VERSAO"));
			historico.setUltimaAlteracao(field.readDate("MEHI_TM_ULTIMA_ALTERACAO"));
			historico.setHabilitado(field.readBoolean("MEHI_IN_USO"));
			historico.setAnoMesLeitura(field.readInt("MEHI_DT_AM_LEITURA"));
			historico.setSequenciaLeitura(field.readInt("MEHI_SQ"));
			historico.setNumeroCiclo(field.readInt("MEHI_NR_CICLO"));
			historico.setDataLeituraInformada(field.readDate("MEHI_TM_LEITURA_INFORMADA"));
			historico.setNumeroLeituraInformada(field.readBigDecimal("MEHI_MD_LEITURA_INFORMADA"));
			historico.setDataLeituraFaturada(field.readDate("MEHI_TM_LEITURA_FATURADA"));
			historico.setNumeroLeituraFaturada(field.readBigDecimal("MEHI_MD_LEITURA_FATURADA"));
			historico.setDataProcessamento(field.readDate("MEHI_TM_PROCESSAMENTO"));
			historico.setAnalisada(field.readBoolean("MEHI_IN_ANALISADA"));
			historico.setDataLeituraAnterior(field.readDate("MEHI_TM_LEITURA_ANTERIOR"));
			historico.setNumeroLeituraAnterior(field.readBigDecimal("MEHI_MD_LEITURA_ANTERIOR"));
			historico.setConsumoInformado(field.readBigDecimal("MEHI_MD_CONSUMO_INFORMADO"));
			historico.setCreditoGerado(field.readBigDecimal("MEHI_MD_CREDITO_GERADO"));
			historico.setCreditoSaldo(field.readBigDecimal("MEHI_MD_CREDITO_SALDO"));
			historico.setNumeroLeituraCorretor(field.readBigDecimal("MEHI_MD_LEITURA_CORRETOR"));
			historico.setConsumoCorretor(field.readBigDecimal("MEHI_MD_CONSUMO_CORRETOR"));
			historico.setConsumoMedidor(field.readBigDecimal("MEHI_MD_CONSUMO_MEDIDOR"));
			historico.setFatorPTZCorretor(field.readBigDecimal("MEHI_NR_FATOR_PTZ"));
			historico.setIndicadorInformadoCliente(field.readBoolean("MEHI_IN_INFORMADO_CLIENTE"));

			Long idLeiturista = field.readLong("LEIT_CD");
			Leiturista leiturista = new LeituristaImpl();
			leiturista.setChavePrimaria(idLeiturista);
			historico.setLeiturista(leiturista);

			Long idAnormalidadeLeitura = field.readLong("LEAN_CD_INFORMADA");
			AnormalidadeLeitura anormalidadeLeituraInformada = new AnormalidadeLeituraImpl();
			anormalidadeLeituraInformada.setChavePrimaria(idAnormalidadeLeitura);
			historico.setAnormalidadeLeituraInformada(anormalidadeLeituraInformada);

			Long idAnormalidadeLeituraFaturada = field.readLong("LEAN_CD_FATURADA");
			AnormalidadeLeitura anormalidadeLeituraFaturada = new AnormalidadeLeituraImpl();
			anormalidadeLeituraFaturada.setChavePrimaria(idAnormalidadeLeituraFaturada);
			historico.setAnormalidadeLeituraFaturada(anormalidadeLeituraFaturada);

			Long idAnormalidadeLeituraAnterior = field.readLong("LEAN_CD_ANTERIOR");
			AnormalidadeLeitura anormalidadeLeituraAnterior = new AnormalidadeLeituraImpl();
			anormalidadeLeituraAnterior.setChavePrimaria(idAnormalidadeLeituraAnterior);
			historico.setAnormalidadeLeituraAnterior(anormalidadeLeituraAnterior);

			Long idHistoricoInstalacaoMedidor = field.readLong("MEIN_CD");
			InstalacaoMedidor historicoInstalacaoMedidor = new InstalacaoMedidorImpl();
			historicoInstalacaoMedidor.setChavePrimaria(idHistoricoInstalacaoMedidor);
			historico.setHistoricoInstalacaoMedidor(historicoInstalacaoMedidor);

			Long idSituacaoLeitura = field.readLong("LESI_CD");
			SituacaoLeitura situacaoLeitura = new SituacaoLeituraImpl();
			situacaoLeitura.setChavePrimaria(idSituacaoLeitura);
			historico.setSituacaoLeitura(situacaoLeitura);

			Long idPontoConsumo = field.readLong("POCN_CD");
			PontoConsumo pontoConsumo = new PontoConsumoImpl();
			pontoConsumo.setChavePrimaria(idPontoConsumo);
			historico.setPontoConsumo(pontoConsumo);

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return historico;
	}

}
