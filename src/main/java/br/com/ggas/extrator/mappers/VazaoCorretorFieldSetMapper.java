
package br.com.ggas.extrator.mappers;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.cadastro.operacional.impl.MedidorLocalArmazenagemImpl;
import br.com.ggas.medicao.medidor.SituacaoMedidor;
import br.com.ggas.medicao.medidor.impl.SituacaoMedidorImpl;
import br.com.ggas.medicao.vazaocorretor.MarcaCorretor;
import br.com.ggas.medicao.vazaocorretor.ModeloCorretor;
import br.com.ggas.medicao.vazaocorretor.PressaoMaximaTransdutor;
import br.com.ggas.medicao.vazaocorretor.ProtocoloComunicacao;
import br.com.ggas.medicao.vazaocorretor.TemperaturaMaximaTransdutor;
import br.com.ggas.medicao.vazaocorretor.TipoMostrador;
import br.com.ggas.medicao.vazaocorretor.TipoTransdutorPressao;
import br.com.ggas.medicao.vazaocorretor.TipoTransdutorTemperatura;
import br.com.ggas.medicao.vazaocorretor.impl.MarcaCorretorImpl;
import br.com.ggas.medicao.vazaocorretor.impl.ModeloCorretorImpl;
import br.com.ggas.medicao.vazaocorretor.impl.ProtocoloComunicacaoImpl;
import br.com.ggas.medicao.vazaocorretor.impl.TipoTransdutorPressaoImpl;
import br.com.ggas.medicao.vazaocorretor.impl.TipoTransdutorTemperaturaImpl;
import br.com.ggas.medicao.vazaocorretor.impl.VazaoCorretorImpl;
import br.com.ggas.util.Fachada;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade CORRETOR_VAZAO e atributos da classe VazaoCorretor
 */
public class VazaoCorretorFieldSetMapper implements FieldSetMapper<VazaoCorretorImpl> {

	private static final Logger LOG = Logger.getLogger(VazaoCorretorFieldSetMapper.class);

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public VazaoCorretorImpl mapFieldSet(FieldSet field) {

		Fachada fachada = Fachada.getInstancia();
		VazaoCorretorImpl vazao = new VazaoCorretorImpl();
		try {

			vazao.setChavePrimaria(field.readLong("COVA_CD"));
			vazao.setVersao(field.readInt("COVA_NR_VERSAO"));
			vazao.setUltimaAlteracao(field.readDate("COVA_TM_ULTIMA_ALTERACAO"));
			vazao.setHabilitado(field.readBoolean("COVA_IN_USO"));
			vazao.setNumeroSerie(field.readString("COVA_NR_SERIE"));
			vazao.setNumeroAnoFabricacao(field.readInt("COVA_NR_ANO_FABRICACAO"));
			vazao.setCorrecaoPressao(field.readBoolean("COVA_IN_CORRECAO_PRESSAO"));
			vazao.setCorrecaoTemperatura(field.readBoolean("COVA_IN_CORRECAO_TEMPERATURA"));
			vazao.setControleVazao(field.readBoolean("COVA_IN_CONTROLE_VAZAO"));
			vazao.setLinearizacaoFatorK(field.readBoolean("COVA_IN_LINEARIZACAO_FATOR_K"));
			vazao.setNumeroDigitos(field.readInt("COVA_NR_DIGITO"));
			vazao.setTombamento(field.readString("COVA_NR_TOMBO"));

			Integer idTipoMostrador = field.readInt("COVA_IN_TIPO_MOSTRADOR");
			TipoMostrador tipo = Fachada.getInstancia().obterTipoMostrador(idTipoMostrador);
			vazao.setTipoMostrador(tipo);

			Long idTemperaturaMaxima = field.readLong("COTM_CD");
			TemperaturaMaximaTransdutor temp = fachada.obterTemperaturaMaximaTransdutor(idTemperaturaMaxima);
			vazao.setTemperaturaMaximaTransdutor(temp);

			Long idPressaoMaxima = field.readLong("COPT_CD");
			PressaoMaximaTransdutor pressao = fachada.obterPressaoMaximaTransdutor(idPressaoMaxima);
			vazao.setPressaoMaximaTransdutor(pressao);

			Long idMarcaCorretor = field.readLong("COMA_CD");
			MarcaCorretor marcaCorretor = new MarcaCorretorImpl();
			marcaCorretor.setChavePrimaria(idMarcaCorretor);
			vazao.setMarcaCorretor(marcaCorretor);

			Long idTipoTransdutor = field.readLong("TRTT_CD");
			TipoTransdutorTemperatura tipoTransdutor = new TipoTransdutorTemperaturaImpl();
			tipoTransdutor.setChavePrimaria(idTipoTransdutor);
			vazao.setTipoTransdutorTemperatura(tipoTransdutor);

			Long idTipoTransdutorPressao = field.readLong("TRPT_CD");
			TipoTransdutorPressao tipoTransdutorPressao = new TipoTransdutorPressaoImpl();
			tipoTransdutorPressao.setChavePrimaria(idTipoTransdutorPressao);
			vazao.setTipoTransdutorPressao(tipoTransdutorPressao);

			Long idProtocoloComunicacao = field.readLong("PRCD_CD");
			ProtocoloComunicacao protocoloComunicacao = new ProtocoloComunicacaoImpl();
			protocoloComunicacao.setChavePrimaria(idProtocoloComunicacao);
			vazao.setProtocoloComunicacao(protocoloComunicacao);

			Long idModelo = field.readLong("COMD_CD");
			ModeloCorretor modelo = new ModeloCorretorImpl();
			modelo.setChavePrimaria(idModelo);
			vazao.setModelo(modelo);

			Long idSituacaoMedidor = field.readLong("MESI_CD");
			SituacaoMedidor situacaoMedidor = new SituacaoMedidorImpl();
			situacaoMedidor.setChavePrimaria(idSituacaoMedidor);
			vazao.setSituacaoMedidor(situacaoMedidor);

			Long idLocalArmazenagem = field.readLong("MELA_CD");
			MedidorLocalArmazenagem localArmazenagem = new MedidorLocalArmazenagemImpl();
			localArmazenagem.setChavePrimaria(idLocalArmazenagem);
			vazao.setLocalArmazenagem(localArmazenagem);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return vazao;
	}

}
