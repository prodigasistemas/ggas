
package br.com.ggas.extrator.mappers;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.impl.AnormalidadeConsumoImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.consumo.TipoConsumo;
import br.com.ggas.medicao.consumo.impl.HistoricoConsumoImpl;
import br.com.ggas.medicao.consumo.impl.TipoConsumoImpl;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.impl.HistoricoMedicaoImpl;
import br.com.ggas.util.Fachada;

/**
 * Classe responsável por historico consumo.
 */
public class HistoricoConsumoFieldSetMapper implements FieldSetMapper<HistoricoConsumoImpl> {

	private Fachada fachada = Fachada.getInstancia();

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public HistoricoConsumoImpl mapFieldSet(FieldSet field) {

		HistoricoConsumoImpl historico = new HistoricoConsumoImpl();

		try {
			historico.setChavePrimaria(field.readLong("COHI_CD"));
			historico.setVersao(field.readInt("COHI_NR_VERSAO"));
			historico.setHabilitado(field.readBoolean("COHI_TM_ULTIMA_ALTERACAO"));

			historico.setAnoMesFaturamento(field.readInt("COHI_DT_AM_FATURAMENTO"));

			historico.setSequencia(field.readInt("COHI_SQ"));
			historico.setNumeroCiclo(field.readInt("COHI_NR_CICLO"));
			historico.setConsumo(field.readBigDecimal("COHI_MD_CONSUMO"));
			historico.setConsumoMedido(field.readBigDecimal("COHI_MD_CONSUMO_MEDIDO"));
			historico.setConsumoApurado(field.readBigDecimal("COHI_MD_CONSUMO_APURADO"));
			historico.setIndicadorImovelCondominio(field.readBoolean("COHI_IN_IMOVEL_CONDOMINIO"));
			historico.setConsumoMedio(field.readBigDecimal("COHI_MD_CONSUMO_MEDIO"));
			historico.setConsumoApuradoMedio(field.readBigDecimal("COHI_MD_CONSUMO_APURADO_MEDIO"));
			historico.setConsumoMinimo(field.readBigDecimal("COHI_MD_CONSUMO_MINIMO"));
			historico.setConsumoImovelVinculado(field.readBigDecimal("COHI_MD_CONSUMO_IMOVEL_VINC"));
			historico.setConsumoCredito(field.readBigDecimal("COHI_MD_CONSUMO_CREDITO"));
			historico.setIndicadorFaturamento(field.readBoolean("COHI_IN_FATURAMENTO"));
			historico.setDiasConsumo(field.readInt("COHI_QN_DIAS_CONSUMO"));
			historico.setMedidaPCS(field.readBigDecimal("COHI_MD_PCS"));
			historico.setMedidaZ(field.readBigDecimal("COHI_MD_Z"));
			historico.setFatorPCS(field.readBigDecimal("COHI_NR_FATOR_PCS"));
			historico.setFatorPTZ(field.readBigDecimal("COHI_NR_FATOR_PTZ"));
			historico.setFatorCorrecao(field.readBigDecimal("COHI_NR_FATOR_CORRECAO"));
			historico.setIndicadorConsumoCiclo(field.readBoolean("COHI_IN_CONSUMO_CICLO"));

			Long idChaveIntevalorPCS = field.readLong("PCIN_CD");
			IntervaloPCS intervalo = Fachada.getInstancia().obterIntervaloPCS(idChaveIntevalorPCS);

			historico.setIntervaloPCS(intervalo);

			Long idChaveLocalAmostragem = field.readLong("ENCO_CD_LOCAL_AMOSTRAGEM_PCS");
			EntidadeConteudo localAmostragem = fachada.obterEntidadeConteudo(idChaveLocalAmostragem);
			historico.setLocalAmostragemPCS(localAmostragem);

			Long idConsumoCondominio = field.readLong("COHI_CD_CONSUMO_CONDOMINIO");
			HistoricoConsumo hist = new HistoricoConsumoImpl();
			hist.setChavePrimaria(idConsumoCondominio);
			historico.setConsumoCondominio(hist);

			Long idHistoricoAnterior = field.readLong("MEHI_CD_ANTERIOR");
			HistoricoMedicao historicoMedicaoAnterior = new HistoricoMedicaoImpl();
			historicoMedicaoAnterior.setChavePrimaria(idHistoricoAnterior);

			historico.setHistoricoAnterior(historicoMedicaoAnterior);

			Long idHistoricoAtual = field.readLong("MEHI_CD_ATUAL");
			HistoricoMedicao historicoMedicaoAtual = new HistoricoMedicaoImpl();
			historicoMedicaoAtual.setChavePrimaria(idHistoricoAtual);
			historico.setHistoricoAtual(historicoMedicaoAtual);

			Long idPontoConsumo = field.readLong("POCN_CD");
			PontoConsumo p = new PontoConsumoImpl();
			p.setChavePrimaria(idPontoConsumo);
			historico.setPontoConsumo(p);

			Long idTipoConsumo = field.readLong("COTP_CD");
			TipoConsumo tipoConsumo = new TipoConsumoImpl();
			tipoConsumo.setChavePrimaria(idTipoConsumo);
			historico.setTipoConsumo(tipoConsumo);

			Long idAnormalidadeConsumo = field.readLong("COAN_CD");

			AnormalidadeConsumo anormalidadeConsumo = new AnormalidadeConsumoImpl();
			anormalidadeConsumo.setChavePrimaria(idAnormalidadeConsumo);

			historico.setAnormalidadeConsumo(anormalidadeConsumo);
		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return historico;
	}

}
