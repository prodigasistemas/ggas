
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.imovel.impl.SegmentoImpl;
import br.com.ggas.faturamento.creditodebito.impl.CreditoDebitoARealizarImpl;
import br.com.ggas.faturamento.impl.FaturaImpl;
import br.com.ggas.faturamento.impl.FaturaItemImpl;
import br.com.ggas.faturamento.impl.NaturezaOperacaoCFOPImpl;
import br.com.ggas.faturamento.rubrica.impl.RubricaImpl;
import br.com.ggas.faturamento.tarifa.impl.TarifaImpl;

/**
 * Classe responsável por fatura item.
 */
public class FaturaItemFieldSetMapper implements FieldSetMapper<FaturaItemImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public FaturaItemImpl mapFieldSet(FieldSet fieldSet) {

		FaturaItemImpl f = new FaturaItemImpl();

		if((Long) fieldSet.readLong("FATU_CD") != null) {
			FaturaImpl fatura = new FaturaImpl();

			fatura.setChavePrimaria(fieldSet.readLong("FATU_CD"));
			f.setFatura(fatura);
		}

		if((Long) fieldSet.readLong("TARI_CD") != null) {
			TarifaImpl tarifa = new TarifaImpl();

			tarifa.setChavePrimaria(fieldSet.readLong("TARI_CD"));
			f.setTarifa(tarifa);
		}

		if((Long) fieldSet.readLong("RUBR_CD") != null) {
			RubricaImpl rubrica = new RubricaImpl();

			rubrica.setChavePrimaria(fieldSet.readLong("RUBR_CD"));
			f.setRubrica(rubrica);
		}

		if((Long) fieldSet.readLong("CRDR_CD") != null) {
			CreditoDebitoARealizarImpl creditoDebitoRealizar = new CreditoDebitoARealizarImpl();

			creditoDebitoRealizar.setChavePrimaria(fieldSet.readLong("CRDR_CD"));
			f.setCreditoDebitoARealizar(creditoDebitoRealizar);
		}

		if((Long) fieldSet.readLong("NAOC_CD") != null) {
			NaturezaOperacaoCFOPImpl naturezaOperacaoCFOP = new NaturezaOperacaoCFOPImpl();

			naturezaOperacaoCFOP.setChavePrimaria(fieldSet.readLong("NAOC_CD"));
			f.setNaturezaOperacaoCFOP(naturezaOperacaoCFOP);
		}

		if((Long) fieldSet.readLong("SEGM_CD") != null) {
			SegmentoImpl segmento = new SegmentoImpl();

			segmento.setChavePrimaria(fieldSet.readLong("SEGM_CD"));
			f.setSegmento(segmento);
		}

		f.setChavePrimaria(fieldSet.readLong("FAIT_CD"));
		f.setValorTotal(fieldSet.readBigDecimal("FAIT_VL_TOTAL"));
		f.setValorUnitario(fieldSet.readBigDecimal("FAIT_VL_UNITARIO"));
		f.setQuantidadeTotalPrestacao(fieldSet.readInt("FAIT_QN_TOTAL_PRESTACAO"));
		f.setMedidaConsumo(fieldSet.readBigDecimal("FAIT_MD_CONSUMO"));
		f.setHabilitado(true);
		f.setVersao(1);
		f.setUltimaAlteracao(new Date());
		f.setQuantidade(fieldSet.readBigDecimal("FAIT_QN"));
		f.setNumeroSequencial(fieldSet.readInt("FAIT_NR_SEQUENCIAL"));

		return f;
	}

}
