
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoModalidadeImpl;
import br.com.ggas.contrato.programacao.impl.SolicitacaoConsumoPontoConsumoImpl;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.controleacesso.impl.UsuarioImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade PONTO_CONSUMO_SOLICITACAO_CONS e atributos da classe
 * SolicitacaoConsumoPontoConsumo
 */
public class SolicitacaoConsumoPontoConsumoFieldSetMapper implements FieldSetMapper<SolicitacaoConsumoPontoConsumoImpl> {
	
	private static final Logger LOG = Logger.getLogger(SolicitacaoConsumoPontoConsumoFieldSetMapper.class);

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public SolicitacaoConsumoPontoConsumoImpl mapFieldSet(FieldSet field) {

		SolicitacaoConsumoPontoConsumoImpl solicitacao = new SolicitacaoConsumoPontoConsumoImpl();
		try {

			solicitacao.setChavePrimaria(field.readLong("POSC_CD"));
			solicitacao.setVersao(field.readInt("POSC_NR_VERSAO"));
			solicitacao.setHabilitado(true);
			solicitacao.setUltimaAlteracao(Calendar.getInstance().getTime());
			solicitacao.setDataSolicitacao(field.readDate("POSC_DT_SOLICITACAO"));
			solicitacao.setValorQDC(field.readBigDecimal("POSC_MD_QDC"));
			solicitacao.setValorQDS(field.readBigDecimal("POSC_MD_QDS"));
			solicitacao.setValorQPNR(field.readBigDecimal("POSC_MD_QPNR"));
			solicitacao.setValorQDP(field.readBigDecimal("POSC_MD_QDP"));
			solicitacao.setIndicadorAceite(field.readBoolean("POSC_IN_ACEITE"));
			solicitacao.setDataOperacao(field.readDate("POSC_DT_OPERACAO"));
			solicitacao.setDescricaoMotivoNaoAceite(field.readString("POSC_DS_MOTIVO_NAO_ACEITE"));
			solicitacao.setIndicadorSolicitante(field.readBoolean("POSC_IN_SOLICITANTE"));

			if((Long) field.readLong("POCN_CD") != null) {
				PontoConsumo pontoConsumo = new PontoConsumoImpl();
				pontoConsumo.setChavePrimaria(field.readLong("POCN_CD"));
				solicitacao.setPontoConsumo(pontoConsumo);

			}
			if((Long) field.readLong("USSI_CD") != null) {
				Usuario usuario = new UsuarioImpl();
				usuario.setChavePrimaria(field.readLong("USSI_CD"));
				solicitacao.setUsuario(usuario);
			}
			if((Long) field.readLong("COPM_CD") != null) {
				ContratoPontoConsumoModalidade contratoPontoConsumoModalidade = new ContratoPontoConsumoModalidadeImpl();
				contratoPontoConsumoModalidade.setChavePrimaria(field.readLong("COPM_CD"));
				solicitacao.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidade);
			}
		
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return solicitacao;
	}

}
