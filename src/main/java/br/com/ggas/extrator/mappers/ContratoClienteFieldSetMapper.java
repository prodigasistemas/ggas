
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.impl.ContratoClienteImpl;
import br.com.ggas.contrato.contrato.impl.ContratoImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade CONTRATO_CLIENTE e atributos da classe ContratoCliente
 */
public class ContratoClienteFieldSetMapper implements FieldSetMapper<ContratoClienteImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ContratoClienteImpl mapFieldSet(FieldSet field) {

		ContratoClienteImpl contrato = new ContratoClienteImpl();
		try {

			contrato.setChavePrimaria(field.readLong("COCL_CD"));
			contrato.setVersao(field.readInt("COCL_NR_VERSAO"));
			contrato.setUltimaAlteracao(Calendar.getInstance().getTime());
			contrato.setHabilitado(field.readBoolean("COCL_IN_USO"));
			contrato.setRelacaoInicio(field.readDate("COCL_DT_RELACAO_INICIO"));
			contrato.setRelacaoFim(field.readDate("COCL_DT_RELACAO_FIM"));
			contrato.setIndicadorPrincipal(field.readBoolean("COCL_IN_PRINCIPAL"));

			if((Long) field.readLong("CONT_CD") != null) {
				Contrato c = new ContratoImpl();
				c.setChavePrimaria(field.readLong("CONT_CD"));
				contrato.setContrato(c);

			}
			if((Long) field.readLong("CLIE_CD") != null) {
				Cliente cliente = new ClienteImpl();
				cliente.setChavePrimaria(field.readLong("CLIE_CD"));
				contrato.setCliente(cliente);
			}

			if((Long) field.readLong("POCN_CD") != null) {
				PontoConsumo pontoConsumo = new PontoConsumoImpl();
				pontoConsumo.setChavePrimaria(field.readLong("POCN_CD"));
				contrato.setPontoConsumo(pontoConsumo);
			}
			if((Long) field.readLong("ENCO_CD_RESPONSABILIDADE") != null) {
				EntidadeConteudo responsabilidade = new EntidadeConteudoImpl();
				responsabilidade.setChavePrimaria(field.readLong("ENCO_CD_RESPONSABILIDADE"));
				contrato.setResponsabilidade(responsabilidade);
			}

		} catch(Exception e) {
			Log.error(e.getStackTrace(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}

		return contrato;
	}

}
