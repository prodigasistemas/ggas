
package br.com.ggas.extrator.mappers;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.leitura.MedidorProtecao;
import br.com.ggas.medicao.leitura.impl.InstalacaoMedidorImpl;
import br.com.ggas.medicao.leitura.impl.MedidorLocalInstalacaoImpl;
import br.com.ggas.medicao.leitura.impl.MedidorProtecaoImpl;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.impl.MedidorImpl;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.impl.VazaoCorretorImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade MEDIDOR_INSTALACAO e atributos da classe InstalacaoMedidor
 */
public class InstalacaoMedidorFieldSetMapper implements FieldSetMapper<InstalacaoMedidorImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public InstalacaoMedidorImpl mapFieldSet(FieldSet field) {

		InstalacaoMedidorImpl instalacao = new InstalacaoMedidorImpl();

		try {
			instalacao.setChavePrimaria(field.readLong("MEIN_CD"));
			instalacao.setVersao(field.readInt("MEIN_NR_VERSAO"));
			instalacao.setUltimaAlteracao(field.readDate("MEIN_TM_ULTIMA_ALTERACAO"));
			instalacao.setHabilitado(field.readBoolean("MEIN_IN_USO"));
			instalacao.setData(field.readDate("MEIN_DT"));
			instalacao.setDataCorretorVazao(field.readDate("MEIN_DT_COVA"));
			instalacao.setLeitura(field.readBigDecimal("MEIN_MD_LEITURA"));
			instalacao.setDataAtivacao(field.readDate("MEIN_DT_ATIVACAO"));
			instalacao.setLeituraAtivacao(field.readBigDecimal("MEIN_MD_LEITURA_ATIVACAO"));

			Medidor medidor = new MedidorImpl();
			medidor.setChavePrimaria(field.readLong("MEDI_CD"));
			instalacao.setMedidor(medidor);

			MedidorLocalInstalacao localInstalacao = new MedidorLocalInstalacaoImpl();
			localInstalacao.setChavePrimaria(field.readLong("MELI_CD"));
			instalacao.setLocalInstalacao(localInstalacao);

			PontoConsumo pontoConsumo = new PontoConsumoImpl();
			pontoConsumo.setChavePrimaria(field.readLong("POCN_CD"));
			instalacao.setPontoConsumo(pontoConsumo);

			MedidorProtecao protecao = new MedidorProtecaoImpl();
			protecao.setChavePrimaria(field.readLong("MEPR_CD"));
			instalacao.setProtecao(protecao);

			Funcionario funcionario = new FuncionarioImpl();
			funcionario.setChavePrimaria(field.readLong("FUNC_CD"));
			instalacao.setFuncionario(funcionario);

			VazaoCorretor vazaoCorretor = new VazaoCorretorImpl();
			vazaoCorretor.setChavePrimaria(field.readLong("COVA_CD"));
			instalacao.setVazaoCorretor(vazaoCorretor);

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}
		return instalacao;
	}

}
