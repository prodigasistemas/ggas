
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.contrato.contrato.ContratoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.impl.ContratoModalidadeImpl;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoModalidadeImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade CONTRATO_PONTO_CONSUMO_MODALIDE e atributos da classe
 * ContratoPontoConsumoModalidade
 */
public class ContratoPontoConsumoModalidadeFieldSetMapper implements FieldSetMapper<ContratoPontoConsumoModalidadeImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ContratoPontoConsumoModalidadeImpl mapFieldSet(FieldSet field) {
		final Logger LOG = Logger.getLogger(ContratoPontoConsumoModalidadeImpl.class);
		ContratoPontoConsumoModalidadeImpl contatoModalidade = new ContratoPontoConsumoModalidadeImpl();

		try {

			contatoModalidade.setChavePrimaria(field.readLong("COPM_CD"));
			contatoModalidade.setVersao(field.readInt("COPM_NR_VERSAO"));
			contatoModalidade.setUltimaAlteracao(Calendar.getInstance().getTime());
			contatoModalidade.setHabilitado(true);
			contatoModalidade.setIndicadorProgramacaoConsumo(field.readBoolean("COPM_IN_PROGRAMACAO_CONSUMO"));
			contatoModalidade.setIndicadorQDSMaiorQDC(field.readBoolean("COPM_IN_QDS_MAIOR_QDC"));
			contatoModalidade.setDiasAntecedenciaQDS(field.readInt("COPM_QN_DIAS_ANTECEDENCIA_QDS"));
			contatoModalidade.setPrazoRevisaoQDC(field.readDate("COPM_DT_PRAZO_REVISAO_QDC"));
			contatoModalidade.setMesesQDS(field.readInt("COPM_QN_MESES_QDS"));
			contatoModalidade.setIndicadorRetiradaAutomatica(field.readBoolean("COPM_IN_RETIRADA_AUTOMATICA"));
			contatoModalidade.setIndicadorConfirmacaoAutomaticaQDS(field.readBoolean("COPM_IN_CONFIRMACAO_AUTOM_QDS"));
			contatoModalidade.setDataInicioRetiradaQPNR(field.readDate("COPM_DT_INICIO_RETIRADA_QPNR"));
			contatoModalidade.setDataFimRetiradaQPNR(field.readDate("COPM_DT_FIM_RETIRADA_QPNR"));
			contatoModalidade.setMargemVarSOPAnual(field.readInt("COPM_PR_MARGEM_VAR_SOP_ANUAL"));
			contatoModalidade.setPercentualQDCContratoQPNR(field.readBigDecimal("COPM_PR_QDC_CONTRATO_QPNR"));
			contatoModalidade.setPercentualQDCFimContratoQPNR(field.readBigDecimal("COPM_PR_QDC_FIM_CONTRATO_QPNR"));
			contatoModalidade.setPercentualVarSuperiorQDC(field.readBigDecimal("COPM_PR_VARIACAO_SUPERIOR_QDC"));
			contatoModalidade.setAnosValidadeRetiradaQPNR(field.readInt("COPM_NR_ANOS_VALIDADE_QPNR"));

			if((Long) field.readLong("COPC_CD") != null) {
				ContratoPontoConsumo contratoPontoConsumo = new ContratoPontoConsumoImpl();
				contratoPontoConsumo.setChavePrimaria(field.readLong("COPC_CD"));
				contatoModalidade.setContratoPontoConsumo(contratoPontoConsumo);

			}
			if((Long) field.readLong("COMO_CD") != null) {
				ContratoModalidade contratoModalidade = new ContratoModalidadeImpl();
				contratoModalidade.setChavePrimaria(field.readLong("COPC_CD"));
				contatoModalidade.setContratoModalidade(contratoModalidade);
			}
			if((Long) field.readLong("ENCO_CD_CONSUMO_REF_SOP_ANUAL") != null) {
				EntidadeConteudo consumoRefSOPAnual = new EntidadeConteudoImpl();
				consumoRefSOPAnual.setChavePrimaria(field.readLong("ENCO_CD_CONSUMO_REF_SOP_ANUAL"));
				contatoModalidade.setConsumoRefSOPAnual(consumoRefSOPAnual);
			}

		} catch(Exception e) {
			LOG.error(e.getMessage(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}
		return contatoModalidade;
	}

}
