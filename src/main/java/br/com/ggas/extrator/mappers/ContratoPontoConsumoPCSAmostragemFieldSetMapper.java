
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoPCSAmostragemImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade CONTRATO_PONTO_CONS_PCS_AMOSTR e atributos da classe
 * ContratoPontoConsumoPCSAmostragem
 */
public class ContratoPontoConsumoPCSAmostragemFieldSetMapper implements FieldSetMapper<ContratoPontoConsumoPCSAmostragemImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ContratoPontoConsumoPCSAmostragemImpl mapFieldSet(FieldSet field) {

		ContratoPontoConsumoPCSAmostragemImpl contratoPCS = new ContratoPontoConsumoPCSAmostragemImpl();
		try {

			contratoPCS.setChavePrimaria(field.readLong("COPA_CD"));
			contratoPCS.setVersao(field.readInt("COPA_NR_VERSAO"));
			contratoPCS.setUltimaAlteracao(Calendar.getInstance().getTime());
			contratoPCS.setPrioridade(field.readInt("COPA_NR_PRIORIDADE"));

			if((Long) field.readLong("COPC_CD") != null) {
				ContratoPontoConsumo contratoPontoConsumo = new ContratoPontoConsumoImpl();
				contratoPontoConsumo.setChavePrimaria(field.readLong("COPC_CD"));
				contratoPCS.setContratoPontoConsumo(contratoPontoConsumo);

			}
			if((Long) field.readLong("ENCO_CD_LOCAL_AMOSTRAGEM_PCS") != null) {
				EntidadeConteudo localAmostragemPCS = new EntidadeConteudoImpl();
				localAmostragemPCS.setChavePrimaria(field.readLong("ENCO_CD_LOCAL_AMOSTRAGEM_PCS"));
				contratoPCS.setLocalAmostragemPCS(localAmostragemPCS);
			}

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return contratoPCS;
	}

}
