
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.endereco.impl.CepImpl;
import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.imovel.impl.RamoAtividadeImpl;
import br.com.ggas.cadastro.imovel.impl.SegmentoImpl;
import br.com.ggas.cadastro.imovel.impl.SituacaoConsumoImpl;
import br.com.ggas.cadastro.localidade.impl.QuadraFaceImpl;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.medicao.leitura.impl.InstalacaoMedidorImpl;
import br.com.ggas.medicao.rota.impl.RotaImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade PONTO_CONSUMO e atributos da classe PontoConsumo
 */
public class PontoConsumoFieldSetMapper implements FieldSetMapper<PontoConsumoImpl> {

	private static final Logger LOG = Logger.getLogger(PontoConsumoFieldSetMapper.class);

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public PontoConsumoImpl mapFieldSet(FieldSet fieldSet) {

		PontoConsumoImpl p = new PontoConsumoImpl();

		try {
			if((Long) fieldSet.readLong("IMOV_CD") != null) {
				ImovelImpl imovelImpl = new ImovelImpl();

				imovelImpl.setChavePrimaria(fieldSet.readLong("IMOV_CD"));
				p.setImovel(imovelImpl);
			}

			if((Long) fieldSet.readLong("POCS_CD") != null) {
				SituacaoConsumoImpl situacaoConsumoImpl = new SituacaoConsumoImpl();

				situacaoConsumoImpl.setChavePrimaria(fieldSet.readLong("POCS_CD"));
				p.setSituacaoConsumo(situacaoConsumoImpl);
			}

			if((Long) fieldSet.readLong("SEGM_CD") != null) {
				SegmentoImpl segmento = new SegmentoImpl();

				segmento.setChavePrimaria(fieldSet.readLong("SEGM_CD"));
				p.setSegmento(segmento);
			}

			if((Long) fieldSet.readLong("RAAT_CD") != null) {
				RamoAtividadeImpl ramoAtividade = new RamoAtividadeImpl();

				ramoAtividade.setChavePrimaria(fieldSet.readLong("RAAT_CD"));
				p.setRamoAtividade(ramoAtividade);
			}

			if((Long) fieldSet.readLong("ROTA_CD") != null) {
				RotaImpl rota = new RotaImpl();

				rota.setChavePrimaria(fieldSet.readLong("ROTA_CD"));
				p.setRota(rota);
			}

			if((Long) fieldSet.readLong("CEP_CD") != null) {
				CepImpl cep = new CepImpl();

				cep.setChavePrimaria(fieldSet.readLong("CEP_CD"));
				p.setCep(cep);
			}

			if((Long) fieldSet.readLong("MEIN_CD") != null) {
				InstalacaoMedidorImpl instalacaoMedidor = new InstalacaoMedidorImpl();

				instalacaoMedidor.setChavePrimaria(fieldSet.readLong("MEIN_CD"));
				p.setInstalacaoMedidor(instalacaoMedidor);
			}

			if((Long) fieldSet.readLong("QUFA_CD") != null) {
				QuadraFaceImpl quadraFace = new QuadraFaceImpl();

				quadraFace.setChavePrimaria(fieldSet.readLong("QUFA_CD"));
				p.setQuadraFace(quadraFace);
			}

			if((Long) fieldSet.readLong("ENCO_CD_MODALIDADE_USO") != null) {
				EntidadeConteudoImpl modalidadeUso = new EntidadeConteudoImpl();

				modalidadeUso.setChavePrimaria(fieldSet.readLong("ENCO_CD_MODALIDADE_USO"));
				p.setModalidadeUso(modalidadeUso);
			}

			p.setChavePrimaria(fieldSet.readLong("POCN_CD"));
			p.setDescricao(fieldSet.readString("POCN_DS"));
			p.setNumeroSequenciaLeitura(fieldSet.readInt("POCN_NR_SEQUENCIA_LEITURA"));
			p.setNumeroImovel(fieldSet.readString("POCN_NR_IMOVEL"));
			p.setDescricaoComplemento(fieldSet.readString("POCN_DS_COMPLEMENTO"));
			p.setVersao(fieldSet.readInt("POCN_NR_VERSAO"));
			p.setHabilitado(fieldSet.readBoolean("POCN_IN_USO"));
			p.setUltimaAlteracao(new Date());
			p.setEnderecoReferencia(fieldSet.readString("POCN_DS_ENDERECO_REFERENCIA"));
			p.setCodigoPontoConsumoSupervisorio(fieldSet.readString("POCN_CD_SUPERVISORIO"));
			p.setLatitudeGrau(fieldSet.readBigDecimal("POCN_NR_LATITUDE_GRAU"));
			p.setLongitudeGrau(fieldSet.readBigDecimal("POCN_NR_LONGITUDE_GRAU"));
			p.setCodigoLegado(fieldSet.readString("POCN_CD_LEGADO"));

			return p;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return p;
	}

}
