
package br.com.ggas.extrator.mappers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.ggas.cadastro.cliente.impl.AtividadeEconomicaImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteSituacaoImpl;
import br.com.ggas.cadastro.cliente.impl.FaixaRendaFamiliarImpl;
import br.com.ggas.cadastro.cliente.impl.NacionalidadeImpl;
import br.com.ggas.cadastro.cliente.impl.OrgaoExpedidorImpl;
import br.com.ggas.cadastro.cliente.impl.PessoaSexoImpl;
import br.com.ggas.cadastro.cliente.impl.ProfissaoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoClienteImpl;
import br.com.ggas.cadastro.geografico.impl.UnidadeFederacaoImpl;
import br.com.ggas.extrator.FieldSetMapper;
import br.com.ggas.extrator.Fieldset;
import br.com.ggas.extrator.cache.ClienteCache;
/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade e CLIENTE atributos da classe Cliente
 */
public class ClienteFieldSetMapper implements FieldSetMapper<ClienteImpl> {

	private static final int TAMANHO_CPF = 11;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.extrator.FieldSetMapper#mapFieldSet(br.com.ggas.extrator.Fieldset)
	 */
	@Override
	public ClienteImpl mapFieldSet(Fieldset fieldSet) {

		ClienteImpl c = new ClienteImpl();

		ClienteCache cache = ClienteCache.getInstance();

		try {
			c.setChavePrimaria(fieldSet.readLong("CLIE_CD"));

			validaNumeroChavePrimaria(fieldSet, c);

			validaNumeroClienteSituacao(fieldSet, c);

			validaNumeroClienteTipo(fieldSet, c, cache);

			validaNumeroOrgaoExpeditor(fieldSet, c);

			validaNumeroUnidadeFederacao(fieldSet, c);

			validaNumeroNacionalidade(fieldSet, c);

			validaNumeroProfissao(fieldSet, c);

			validaNumeroPessoaSexo(fieldSet, c);

			validaNumeroFaixaRendaFamiliar(fieldSet, c);

			validaNumeroAtividadeEconomica(fieldSet, c);

			c.setNome(fieldSet.readString("CLIE_NM"));
			c.setNomeAbreviado(fieldSet.readString("CLIE_NM_ABREVIADO"));
			if("null".equalsIgnoreCase(fieldSet.readString("CLIE_DS_EMAIL_PRINCIPAL"))){
				c.setEmailPrincipal("importadordedados@ggas.com.br");
			}else{
				c.setEmailPrincipal(fieldSet.readString("CLIE_DS_EMAIL_PRINCIPAL"));
			}
			c.setEmailSecundario(fieldSet.readString("CLIE_DS_EMAIL_SECUNDARIO"));
			if("null".equalsIgnoreCase(fieldSet.readString("CLIE_NR_CPF"))){
				c.setCpf("");
			}else{
				c.setCpf(fieldSet.readString("CLIE_NR_CPF"));
			}
			if("null".equalsIgnoreCase(fieldSet.readString("CLIE_NR_RG"))){
				c.setRg("");
			}else{
				c.setRg(fieldSet.readString("CLIE_NR_RG"));
			}

			c.setDataEmissaoRG(fieldSet.readDate("CLIE_DT_RG_EMISSAO"));
			c.setNumeroPassaporte(fieldSet.readString("CLIE_NR_PASSAPORTE"));

			c.setDataNascimento(fieldSet.readDate("CLIE_DT_NASCIMENTO"));
			
			c.setNomeMae(fieldSet.readString("CLIE_NM_MAE"));
			c.setNomePai(fieldSet.readString("CLIE_NM_PAI"));
			if("".equals(fieldSet.readString("CLIE_NM_FANTASIA"))){
				c.setNomeFantasia("Nome Fantasia pelo Importador");
			}else{
				c.setNomeFantasia(fieldSet.readString("CLIE_NM_FANTASIA"));
			}
			if("null".equalsIgnoreCase(fieldSet.readString("CLIE_NR_CNPJ"))){
				c.setCnpj("");
			}else{
				c.setCnpj(fieldSet.readString("CLIE_NR_CNPJ"));
			}

			if(c.getCpf() != null && c.getCpf().length() > TAMANHO_CPF && c.getCnpj() != null && c.getCnpj().length() == 0) {
				c.setCnpj(c.getCpf());
				c.setCpf("");
			}

			c.setInscricaoEstadual(fieldSet.readString("CLIE_NR_INSCRICAO_ESTADUAL"));
			c.setInscricaoMunicipal(fieldSet.readString("CLIE_NR_INSCRICAO_MUNICIPAL"));
			c.setInscricaoRural(fieldSet.readString("CLIE_NR_INSCRICAO_RURAL"));
			
			c.setContaAuxiliar(fieldSet.readString("CLIE_DS_CONTA_AUXILIAR"));

			c.setVersao(1);
			c.setUltimaAlteracao(new Date());
			c.setHabilitado(true);

		} catch(Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return c;
	}

	private void validaNumeroAtividadeEconomica(Fieldset fieldSet, ClienteImpl c) {
		try {
			if(fieldSet.readLong("ATEC_CD") != null) {
				AtividadeEconomicaImpl atividadeEconomica = new AtividadeEconomicaImpl();

				atividadeEconomica.setChavePrimaria(fieldSet.readLong("ATEC_CD"));
				c.setAtividadeEconomica(atividadeEconomica);
			}
		} catch(NumberFormatException e) {
			e.getMessage();
		}
	}

	private void validaNumeroFaixaRendaFamiliar(Fieldset fieldSet, ClienteImpl c) {
		try {
			if(fieldSet.readLong("REFF_CD") != null) {
				FaixaRendaFamiliarImpl faixaRendaFamiliar = new FaixaRendaFamiliarImpl();

				faixaRendaFamiliar.setChavePrimaria(fieldSet.readLong("REFF_CD"));
				c.setRendaFamiliar(faixaRendaFamiliar);
			}
		} catch(NumberFormatException e) {
			e.getMessage();
		}
	}

	private void validaNumeroPessoaSexo(Fieldset fieldSet, ClienteImpl c) {
		try {
			if(fieldSet.readLong("PESE_CD") != null) {
				PessoaSexoImpl pessoaSexo = new PessoaSexoImpl();

				pessoaSexo.setChavePrimaria(fieldSet.readLong("PESE_CD"));
				c.setSexo(pessoaSexo);
			}
		} catch(NumberFormatException e) {
			e.getMessage();
		}
	}

	private void validaNumeroProfissao(Fieldset fieldSet, ClienteImpl c) {
		try {
			if(fieldSet.readLong("PROF_CD") != null) {
				ProfissaoImpl profissao = new ProfissaoImpl();

				profissao.setChavePrimaria(fieldSet.readLong("PROF_CD"));
				c.setProfissao(profissao);
			}
		} catch(NumberFormatException e) {
			e.getMessage();
		}
	}

	private void validaNumeroNacionalidade(Fieldset fieldSet, ClienteImpl c) {
		try {
			if(fieldSet.readLong("NACI_CD") != null) {
				NacionalidadeImpl nacionalidade = new NacionalidadeImpl();

				nacionalidade.setChavePrimaria(fieldSet.readLong("NACI_CD"));
				c.setNacionalidade(nacionalidade);
			}
		} catch(NumberFormatException e) {
			e.getMessage();
		}
	}

	private void validaNumeroUnidadeFederacao(Fieldset fieldSet, ClienteImpl c) {
		try {
			if(fieldSet.readLong("UNFE_CD") != null) {
				UnidadeFederacaoImpl unidadeFederacao = new UnidadeFederacaoImpl();

				unidadeFederacao.setChavePrimaria(fieldSet.readLong("UNFE_CD"));
				c.setUnidadeFederacao(unidadeFederacao);
			}
		} catch(NumberFormatException e) {
			e.getMessage();
		}
	}

	private void validaNumeroOrgaoExpeditor(Fieldset fieldSet, ClienteImpl c) {
		try {
			if(fieldSet.readLong("ORER_CD") != null) {
				OrgaoExpedidorImpl orgaoExpedidor = new OrgaoExpedidorImpl();

				orgaoExpedidor.setChavePrimaria(fieldSet.readLong("ORER_CD"));
				c.setOrgaoExpedidor(orgaoExpedidor);
			}
		} catch(NumberFormatException e) {
			e.getMessage();
		}
	}

	private void validaNumeroClienteTipo(Fieldset fieldSet, ClienteImpl c, ClienteCache cache) {
		try {
			if(fieldSet.readLong("CLTI_CD") != null) {
				// pegou clienteTipo do cache
				TipoClienteImpl clienteTipo = cache.getClientesTipo(fieldSet.readLong("CLTI_CD"));
				c.setTipoCliente(clienteTipo);
			}
		} catch(NumberFormatException e) {
			e.getMessage();
		}
	}

	private void validaNumeroClienteSituacao(Fieldset fieldSet, ClienteImpl c) {
		try {
			if(fieldSet.readLong("CLSI_CD") != null) {
				ClienteSituacaoImpl clienteSituacao = new ClienteSituacaoImpl();

				clienteSituacao.setChavePrimaria(fieldSet.readLong("CLSI_CD"));
				c.setClienteSituacao(clienteSituacao);
			}
		} catch(NumberFormatException e) {
			e.getMessage();
		}
	}

	private void validaNumeroChavePrimaria(Fieldset fieldSet, ClienteImpl c) {
		try {
			if(fieldSet.readLong("CLIE_CD_RESPONSAVEL") != null) {
				ClienteImpl clienteResponsavel = new ClienteImpl();

				clienteResponsavel.setChavePrimaria(fieldSet.readLong("CLIE_CD_RESPONSAVEL"));
				c.setClienteResponsavel(clienteResponsavel);
			}
		} catch(NumberFormatException e) {
			e.getMessage();
		}
	}

	/**
	 * Map field set.
	 * 
	 * @param lista
	 *            the lista
	 * @return the list
	 */
	public List<ClienteImpl> mapFieldSet(List<Fieldset> lista) {

		List<ClienteImpl> clientes = new ArrayList<>();

		for (Fieldset fieldset : lista) {
			clientes.add(mapFieldSet(fieldset));
		}

		return clientes;
	}

}
