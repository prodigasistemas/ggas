
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoItemFaturamentoImpl;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.faturamento.tarifa.impl.TarifaImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade CONTRATO_PONTO_CONS_ITEM_FATUR e atributos da classe
 * ContratoPontoConsumoItemFaturamento
 */
public class ContratoPontoConsumoItemFaturamentoFieldSetMapper implements FieldSetMapper<ContratoPontoConsumoItemFaturamentoImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ContratoPontoConsumoItemFaturamentoImpl mapFieldSet(FieldSet field) {

		ContratoPontoConsumoItemFaturamentoImpl contratoFaturamento = new ContratoPontoConsumoItemFaturamentoImpl();
		try {
			contratoFaturamento.setChavePrimaria(field.readLong("COIF_CD"));
			contratoFaturamento.setVersao(field.readInt("COIF_NR_VERSAO"));
			contratoFaturamento.setUltimaAlteracao(Calendar.getInstance().getTime());
			contratoFaturamento.setNumeroDiaVencimento(field.readInt("COIF_NR_DIA_VENCIMENTO"));
			contratoFaturamento.setVencimentoDiaUtil(field.readBoolean("COIF_IN_VENCIMENTO_DIA_UTIL"));
			contratoFaturamento.setPercminimoQDC(field.readBigDecimal("COIF_PR_QDC"));

			if((Long) field.readLong("COPC_CD") != null) {
				ContratoPontoConsumo contratoPontoConsumo = new ContratoPontoConsumoImpl();
				contratoPontoConsumo.setChavePrimaria(field.readLong("COPC_CD"));
				contratoFaturamento.setContratoPontoConsumo(contratoPontoConsumo);

			}
			if((Long) field.readLong("ENCO_CD_ITEM_FATURA") != null) {
				EntidadeConteudo itemFatura = new EntidadeConteudoImpl();
				itemFatura.setChavePrimaria(field.readLong("ENCO_CD_ITEM_FATURA"));
				contratoFaturamento.setItemFatura(itemFatura);

			}
			if((Long) field.readLong("TARI_CD") != null) {
				Tarifa tarifa = new TarifaImpl();
				tarifa.setChavePrimaria(field.readLong("TARI_CD"));
				contratoFaturamento.setTarifa(tarifa);

			}

			contratoFaturamento.setIndicadorDepositoIdentificado(field.readBoolean("COIF_IN_DEPOSITO_IDENTIFICADO"));

			if((Long) field.readLong("ENCO_CD_FASE_REFERENCIA") != null) {
				EntidadeConteudo faseReferencia = new EntidadeConteudoImpl();
				faseReferencia.setChavePrimaria(field.readLong("ENCO_CD_FASE_REFERENCIA"));
				contratoFaturamento.setFaseReferencia(faseReferencia);
			}
			if((Long) field.readLong("ENCO_CD_OPCAO_FASE_REFERENCIA") != null) {
				EntidadeConteudo opcaoFaseReferencia = new EntidadeConteudoImpl();
				opcaoFaseReferencia.setChavePrimaria(field.readLong("ENCO_CD_OPCAO_FASE_REFERENCIA"));
				contratoFaturamento.setOpcaoFaseReferencia(opcaoFaseReferencia);
			}

		} catch(Exception e) {
			Log.error(e.getStackTrace(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}
		return contratoFaturamento;
	}

}
