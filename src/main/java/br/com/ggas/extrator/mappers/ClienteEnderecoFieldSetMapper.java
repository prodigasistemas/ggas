
package br.com.ggas.extrator.mappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.impl.ClienteEnderecoImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.cliente.impl.TipoEnderecoImpl;
import br.com.ggas.cadastro.endereco.impl.CepImpl;
import br.com.ggas.cadastro.geografico.impl.MunicipioImpl;
import br.com.ggas.extrator.FieldSetMapper;
import br.com.ggas.extrator.Fieldset;
import br.com.ggas.extrator.cache.ClienteCache;

/**
 * Classe responsável por cliente endereço.
 */
public class ClienteEnderecoFieldSetMapper implements FieldSetMapper<ClienteEnderecoImpl> {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.extrator.FieldSetMapper#mapFieldSet(br.com.ggas.extrator.Fieldset)
	 */
	@Override
	public ClienteEnderecoImpl mapFieldSet(Fieldset fieldSet) {

		ClienteEnderecoImpl c = new ClienteEnderecoImpl();

		ClienteCache cache = ClienteCache.getInstance();
		ClienteImpl cliente = cache.getCliente(fieldSet.readLong("CLIE_CD"));

		try {
			if (fieldSet.readLong("CEP_CD") != null) {
				CepImpl cep = new CepImpl();

				cep.setChavePrimaria(fieldSet.readLong("CEP_CD"));
				c.setCep(cep);
			}

			if (fieldSet.readLong("ENTI_CD") != null) {
				TipoEnderecoImpl enderecoTipo = new TipoEnderecoImpl();

				enderecoTipo.setChavePrimaria(fieldSet.readLong("ENTI_CD"));
				c.setTipoEndereco(enderecoTipo);
			}

			if (fieldSet.readLong("MUNI_CD") != null) {
				MunicipioImpl municipio = new MunicipioImpl();

				municipio.setChavePrimaria(fieldSet.readLong("MUNI_CD"));
				c.setMunicipio(municipio);
			}

			c.setNumero(fieldSet.readString("CLEN_NR"));
			c.setComplemento(fieldSet.readString("CLEN_DS_COMPLEMENTO"));
			c.setCaixaPostal(fieldSet.readInt("CLEN_NR_CAIXA_POSTAL"));
			c.setCorrespondencia(fieldSet.readBoolean("CLEN_IN_CORRESPONDENCIA"));
			c.setVersao(1);
			c.setHabilitado(true);
			c.setUltimaAlteracao(new Date());
			c.setEnderecoReferencia(fieldSet.readString("CLEN_DS_ENDERECO_REFERENCIA"));

		} catch(Exception e) {
			Log.error(e.getStackTrace(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}

		adicionarCliente(cliente, c);

		return c;
	}

	/**
	 * Adicionar cliente.
	 * 
	 * @param cliente
	 *            the cliente
	 * @param c
	 *            the c
	 */
	private void adicionarCliente(ClienteImpl cliente, ClienteEnderecoImpl c) {

		if(cliente != null) {

			if(cliente.getEnderecos() != null || !cliente.getEnderecos().isEmpty()) {
				cliente.getEnderecos().add(c);
			} else {
				Collection<ClienteEndereco> enderecos = new HashSet<>();
				enderecos.add(c);
				cliente.setEnderecos(enderecos);
			}
		}

	}

	/**
	 * Map field set.
	 * 
	 * @param lista
	 *            the lista
	 * @return the list
	 */
	public List<ClienteEnderecoImpl> mapFieldSet(List<Fieldset> lista) {

		List<ClienteEnderecoImpl> clientesEndereco = new ArrayList<>();

		for (Fieldset f : lista) {
			clientesEndereco.add(mapFieldSet(f));
		}

		return clientesEndereco;
	}

}
