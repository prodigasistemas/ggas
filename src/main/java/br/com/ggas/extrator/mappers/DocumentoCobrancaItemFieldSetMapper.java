
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.arrecadacao.CobrancaDebitoSituacao;
import br.com.ggas.arrecadacao.impl.CobrancaDebitoSituacaoImpl;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.creditodebito.CreditoDebitoARealizar;
import br.com.ggas.faturamento.creditodebito.impl.CreditoDebitoARealizarImpl;
import br.com.ggas.faturamento.impl.DocumentoCobrancaImpl;
import br.com.ggas.faturamento.impl.DocumentoCobrancaItemImpl;
import br.com.ggas.faturamento.impl.FaturaGeralImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade DOCUMENTO_COBRANCA_ITEM e atributos da classe DocumentoCobrancaItem
 */
public class DocumentoCobrancaItemFieldSetMapper implements FieldSetMapper<DocumentoCobrancaItemImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public DocumentoCobrancaItemImpl mapFieldSet(FieldSet field) {

		DocumentoCobrancaItemImpl docItem = new DocumentoCobrancaItemImpl();
		try {
			docItem.setChavePrimaria(field.readLong("DOCI_CD"));
			docItem.setVersao(field.readInt("DOCI_NR_VERSAO"));
			docItem.setUltimaAlteracao(Calendar.getInstance().getTime());
			docItem.setHabilitado(true);
			docItem.setValor(field.readBigDecimal("DOCI_VL"));
			docItem.setValorAcrecimos(field.readBigDecimal("DOCI_VL_ACRESCIMOS"));
			docItem.setValorDescontos(field.readBigDecimal("DOCI_VL_DESCONTOS"));
			docItem.setNumeroPrestacao(field.readInt("DOCI_NR_PRESTACAO"));
			docItem.setNumeroTotalPrestacao(field.readInt("DOCI_NR_TOTAL_PRESTACAO"));

			if((Long) field.readLong("DOCO_CD") != null) {
				DocumentoCobranca documentoCobranca = new DocumentoCobrancaImpl();
				documentoCobranca.setChavePrimaria(field.readLong("DOCO_CD"));
				docItem.setDocumentoCobranca(documentoCobranca);

			}
			if((Long) field.readLong("FAGE_CD") != null) {
				FaturaGeral faturaGeral = new FaturaGeralImpl();
				faturaGeral.setChavePrimaria(field.readLong("FAGE_CD"));
				docItem.setFaturaGeral(faturaGeral);

			}
			if((Long) field.readLong("CRDR_CD") != null) {
				CreditoDebitoARealizar creditoDebitoARealizar = new CreditoDebitoARealizarImpl();
				creditoDebitoARealizar.setChavePrimaria(field.readLong("CRDR_CD"));
				docItem.setCreditoDebitoARealizar(creditoDebitoARealizar);
			}
			if((Long) field.readLong("CODS_CD") != null) {
				CobrancaDebitoSituacao cobrancaDebitoSituacao = new CobrancaDebitoSituacaoImpl();
				cobrancaDebitoSituacao.setChavePrimaria(field.readLong("CRDR_CD"));
				docItem.setCobrancaDebitoSituacao(cobrancaDebitoSituacao);
			}
			if((Long) field.readLong("POCN_CD") != null) {
				PontoConsumo pontoConsumo = new PontoConsumoImpl();
				pontoConsumo.setChavePrimaria(field.readLong("POCN_CD"));
				docItem.setPontoConsumo(pontoConsumo);
			}

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}
		return docItem;
	}

}
