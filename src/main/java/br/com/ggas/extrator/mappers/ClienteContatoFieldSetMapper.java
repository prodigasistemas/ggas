
package br.com.ggas.extrator.mappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.cliente.impl.ContatoClienteImpl;
import br.com.ggas.cadastro.cliente.impl.ProfissaoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoContatoImpl;
import br.com.ggas.extrator.FieldSetMapper;
import br.com.ggas.extrator.Fieldset;
import br.com.ggas.extrator.cache.ClienteCache;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade CLIENTE_CONTATO e atributos da classe ClienteContato
 */
public class ClienteContatoFieldSetMapper implements FieldSetMapper<ContatoClienteImpl> {

	protected ClienteCache cache = ClienteCache.getInstance();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.extrator.FieldSetMapper#mapFieldSet(br.com.ggas.extrator.Fieldset)
	 */
	@Override
	public ContatoClienteImpl mapFieldSet(Fieldset fieldSet) {

		ContatoCliente c = new ContatoClienteImpl();

		ClienteImpl cliente = cache.getCliente(fieldSet.readLong("CLIE_CD"));
		
		try{			
			if (fieldSet.readLong("COTI_CD") != null){
				TipoContatoImpl tipoContato = new TipoContatoImpl();

				tipoContato.setChavePrimaria(fieldSet.readLong("COTI_CD"));
				c.setTipoContato(tipoContato);
			}
			
			if (fieldSet.readLong("PROF_CD") != null){
				ProfissaoImpl profissao = new ProfissaoImpl();

				profissao.setChavePrimaria(fieldSet.readLong("PROF_CD"));
				c.setProfissao(profissao);
			}

			c.setNome(fieldSet.readString("CLCO_NM"));
			c.setCodigoDDD(fieldSet.readInt("CLCO_CD_DDD"));
			c.setFone(fieldSet.readInt("CLCO_NR_FONE"));
			c.setRamal(fieldSet.readInt("CLCO_NR_FONE"));
			c.setDescricaoArea(fieldSet.readString("CLCO_DS_AREA"));
			c.setEmail(fieldSet.readString("CLCO_DS_EMAIL"));
			c.setPrincipal(fieldSet.readBoolean("CLCO_IN_PRINCIPAL"));
			c.setVersao(1);
			c.setHabilitado(true);
			c.setUltimaAlteracao(new Date());

		} catch(Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		adicionarCliente(cliente, c);

		return (ContatoClienteImpl) c;
	}

	/**
	 * Adicionar cliente.
	 * 
	 * @param cliente
	 *            the cliente
	 * @param c
	 *            the c
	 */
	private void adicionarCliente(ClienteImpl cliente, ContatoCliente c) {

		if(cliente != null) {
			if(cliente.getContatos() != null || !cliente.getContatos().isEmpty()) {
				cliente.getContatos().add(c);
			} else {
				Collection<ContatoCliente> contatos = new HashSet<>();
				contatos.add(c);
				cliente.setContatos(contatos);
			}
		}
	}

	/**
	 * Map field set.
	 * 
	 * @param lista
	 *            the lista
	 * @return the list
	 */
	public List<ContatoClienteImpl> mapFieldSet(List<Fieldset> lista) {

		List<ContatoClienteImpl> contatos = new ArrayList<>();
		for (Fieldset f : lista) {
			contatos.add(mapFieldSet(f));
		}
		return contatos;
	}

}
