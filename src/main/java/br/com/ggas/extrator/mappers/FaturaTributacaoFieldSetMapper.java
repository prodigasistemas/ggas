
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.faturamento.fatura.impl.FaturaTributacaoImpl;
import br.com.ggas.faturamento.impl.FaturaImpl;
import br.com.ggas.faturamento.tributo.impl.TributoImpl;

/**
 * Classe responsável por fatura tributação.
 */
public class FaturaTributacaoFieldSetMapper implements FieldSetMapper<FaturaTributacaoImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public FaturaTributacaoImpl mapFieldSet(FieldSet fieldSet) {

		FaturaTributacaoImpl f = new FaturaTributacaoImpl();

		if((Long) fieldSet.readLong("TRIB_CD") != null) {
			TributoImpl tributo = new TributoImpl();

			tributo.setChavePrimaria(fieldSet.readLong("TRIB_CD"));
			f.setTributo(tributo);
		}

		if((Long) fieldSet.readLong("FATU_CD") != null) {
			FaturaImpl fatura = new FaturaImpl();

			fatura.setChavePrimaria(fieldSet.readLong("FATU_CD"));
			f.setFatura(fatura);
		}

		f.setChavePrimaria(fieldSet.readLong("FATR_CD"));
		f.setPercentualAliquota(fieldSet.readBigDecimal("FATR_PR_ALIQUOTA"));
		f.setValorBaseCalculo(fieldSet.readBigDecimal("FATR_VL_BASE_CALCULO"));
		f.setValorImposto(fieldSet.readBigDecimal("FATR_VL_VALOR_IMPOSTO"));
		f.setValorBaseSubstituicao(fieldSet.readBigDecimal("FATR_VL_BASE_SUBST"));
		f.setValorSubstituicao(fieldSet.readBigDecimal("FATR_VL_SUBST"));
		f.setHabilitado(true);
		f.setUltimaAlteracao(new Date());
		f.setVersao(1);

		return f;
	}

}
