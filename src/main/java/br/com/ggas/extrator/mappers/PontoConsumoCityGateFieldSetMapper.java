
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.imovel.impl.PontoConsumoCityGateImpl;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.operacional.impl.CityGateImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade PONTO_CONSUMO_CITY_GATE e atributos da classe PontoConsumoCityGate
 */
public class PontoConsumoCityGateFieldSetMapper implements FieldSetMapper<PontoConsumoCityGateImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public PontoConsumoCityGateImpl mapFieldSet(FieldSet fieldSet) {

		PontoConsumoCityGateImpl p = new PontoConsumoCityGateImpl();

		try {
			if((Long) fieldSet.readLong("CIGA_CD") != null) {
				CityGateImpl imovelImpl = new CityGateImpl();

				imovelImpl.setChavePrimaria(fieldSet.readLong("CIGA_CD"));
				p.setCityGate(imovelImpl);
			}

			if((Long) fieldSet.readLong("POCN_CD") != null) {
				PontoConsumoImpl pontoConsumo = new PontoConsumoImpl();

				pontoConsumo.setChavePrimaria(fieldSet.readLong("POCN_CD"));
				p.setPontoConsumo(pontoConsumo);
			}

			p.setChavePrimaria(fieldSet.readLong("POCG_CD"));
			p.setPercentualParticipacao(fieldSet.readBigDecimal("POCG_PR_PARTICIPACAO"));
			p.setDataVigencia(fieldSet.readDate("POCG_DT_VIGENCIA"));
			p.setUltimaAlteracao(new Date());
			p.setHabilitado(fieldSet.readBoolean("POCG_IN_USO"));
			p.setVersao(1);

			return p;
		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return p;
	}

}
