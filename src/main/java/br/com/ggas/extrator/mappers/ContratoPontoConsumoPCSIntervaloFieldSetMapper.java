
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoPCSIntervaloImpl;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.consumo.impl.IntervaloPCSImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade CONTRATO_PONTO_CONS_PCS_INTERV e atributos da classe
 * ContratoPontoConsumoPCSIntervalo
 */
public class ContratoPontoConsumoPCSIntervaloFieldSetMapper implements FieldSetMapper<ContratoPontoConsumoPCSIntervaloImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ContratoPontoConsumoPCSIntervaloImpl mapFieldSet(FieldSet field) {

		ContratoPontoConsumoPCSIntervaloImpl contratoPCSIntervalo = new ContratoPontoConsumoPCSIntervaloImpl();
		try {
			contratoPCSIntervalo.setChavePrimaria(field.readLong("COPI_CD"));
			contratoPCSIntervalo.setVersao(field.readInt("COPI_NR_VERSAO"));
			contratoPCSIntervalo.setUltimaAlteracao(Calendar.getInstance().getTime());
			contratoPCSIntervalo.setPrioridade(field.readInt("COPI_NR_PRIORIDADE"));
			contratoPCSIntervalo.setTamanho(field.readInt("COPI_NR_TAMANHO"));
			if((Long) field.readLong("PCIN_CD") != null) {
				IntervaloPCS intervaloPCS = new IntervaloPCSImpl();
				intervaloPCS.setChavePrimaria(field.readLong("PCIN_CD"));
				contratoPCSIntervalo.setIntervaloPCS(intervaloPCS);
			}

			if((Long) field.readLong("PCIN_CD") != null) {
				ContratoPontoConsumo contratoPontoConsumo = new ContratoPontoConsumoImpl();
				contratoPontoConsumo.setChavePrimaria(field.readLong("PCIN_CD"));
				contratoPCSIntervalo.setContratoPontoConsumo(contratoPontoConsumo);
			}

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return contratoPCSIntervalo;
	}

}
