
package br.com.ggas.extrator.mappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.impl.ClienteFoneImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.cliente.impl.TipoFoneImpl;
import br.com.ggas.extrator.FieldSetMapper;
import br.com.ggas.extrator.Fieldset;
import br.com.ggas.extrator.cache.ClienteCache;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade CLIENTE_FONE e atributos da classe ClienteFone
 */
public class ClienteFoneFieldSetMapper implements FieldSetMapper<ClienteFoneImpl> {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.extrator.FieldSetMapper#mapFieldSet(br.com.ggas.extrator.Fieldset)
	 */
	@Override
	public ClienteFoneImpl mapFieldSet(Fieldset fieldSet) {

		ClienteFoneImpl clienteFone = new ClienteFoneImpl();

		ClienteCache cache = ClienteCache.getInstance();
		ClienteImpl cliente = cache.getCliente(fieldSet.readLong("CLIE_CD"));

		try {
			if(fieldSet.readLong("FOTI_CD") != null) {
				TipoFoneImpl tipoFone = new TipoFoneImpl();

				clienteFone.setTipoFone(tipoFone);
			}

			clienteFone.setCodigoDDD(fieldSet.readInt("CLFO_CD_DDD"));
			clienteFone.setNumero(fieldSet.readInt("CLFO_NR"));
			clienteFone.setRamal(fieldSet.readInt("CLFO_NR_RAMAL"));
			clienteFone.setIndicadorPrincipal(fieldSet.readBoolean("CLFO_IN_PRINCIPAL"));
			clienteFone.setVersao(1);
			clienteFone.setHabilitado(true);
			clienteFone.setUltimaAlteracao(new Date());
		} catch(Exception e) {
			Log.error(e.getStackTrace(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}

		adicionarCliente(cliente, clienteFone);

		return clienteFone;
	}

	/**
	 * Adicionar cliente.
	 * 
	 * @param cliente
	 *            the cliente
	 * @param c
	 *            the c
	 */
	private void adicionarCliente(ClienteImpl cliente, ClienteFoneImpl c) {

		if(cliente != null) {
			if(cliente.getFones() != null || !cliente.getFones().isEmpty()) {
				cliente.getFones().add(c);
			} else {
				Collection<ClienteFone> fones = new HashSet<>();
				fones.add(c);
				cliente.setFones(fones);
			}
		}
	}

	/**
	 * Map field set.
	 * 
	 * @param lista
	 *            the lista
	 * @return the list
	 */
	public List<ClienteFoneImpl> mapFieldSet(List<Fieldset> lista) {

		List<ClienteFoneImpl> fones = new ArrayList<>();

		for (Fieldset fieldset : lista) {
			fones.add(mapFieldSet(fieldset));
		}
		return fones;
	}

}
