
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.cadastro.imovel.impl.RedeInternaImovelImpl;
import br.com.ggas.cadastro.operacional.impl.RedeDiametroImpl;
import br.com.ggas.cadastro.operacional.impl.RedeMaterialImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade ImovelRedeInterna e atributos da classe ImovelRedeInterna
 */
public class ImovelRedeInternaFieldSetMapper implements FieldSetMapper<RedeInternaImovelImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public RedeInternaImovelImpl mapFieldSet(FieldSet fieldSet) {

		RedeInternaImovelImpl i = new RedeInternaImovelImpl();

		try {
			if((Long) fieldSet.readLong("IMOV_CD") != null) {
				ImovelImpl imovelImpl = new ImovelImpl();

				imovelImpl.setChavePrimaria(fieldSet.readLong("IMOV_CD"));
				i.setImovel(imovelImpl);
			}

			if((Long) fieldSet.readLong("REMA_CD") != null) {
				RedeMaterialImpl redeMaterial = new RedeMaterialImpl();

				redeMaterial.setChavePrimaria(fieldSet.readLong("REMA_CD"));
				i.setRedeMaterial(redeMaterial);
			}

			if((Long) fieldSet.readLong("REDI_CD_CENTRAL") != null) {
				RedeDiametroImpl redeDiametro = new RedeDiametroImpl();

				redeDiametro.setChavePrimaria(fieldSet.readLong("REDI_CD_CENTRAL"));
				i.setRedeDiametroCentral(redeDiametro);
			}

			if((Long) fieldSet.readLong("REDI_CD_PRUMADA_APARTAMENTO") != null) {
				RedeDiametroImpl redeDiametro = new RedeDiametroImpl();

				redeDiametro.setChavePrimaria(fieldSet.readLong("REDI_CD_PRUMADA_APARTAMENTO"));
				i.setRedeDiametroPrumadaApartamento(redeDiametro);
			}

			if((Long) fieldSet.readLong("REDI_CD_PRUMADA") != null) {
				RedeDiametroImpl redeDiametro = new RedeDiametroImpl();

				redeDiametro.setChavePrimaria(fieldSet.readLong("REDI_CD_PRUMADA"));
				i.setRedeDiametroPrumada(redeDiametro);
			}

			i.setChavePrimaria(fieldSet.readLong("IMRI_CD"));
			i.setQuantidadePrumada(fieldSet.readInt("IMOV_QN_PRUMADA"));
			i.setQuantidadeReguladorHall(fieldSet.readInt("IMRI_QN_REGULADOR_HALL"));
			i.setVentilacaoHall(fieldSet.readBoolean("IMRI_IN_VENTILACAO_HALL"));
			i.setVentilacaoApartamento(fieldSet.readBoolean("IMRI_IN_VENTILACAO_APARTAMENTO"));
			i.setVersao(fieldSet.readInt("IMRI_NR_VERSAO"));
			i.setHabilitado(fieldSet.readBoolean("IMRI_IN_USO"));
			i.setUltimaAlteracao(new Date());

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return i;
	}

}
