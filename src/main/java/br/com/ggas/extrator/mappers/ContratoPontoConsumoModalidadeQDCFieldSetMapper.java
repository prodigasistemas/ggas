
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoModalidadeImpl;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoModalidadeQDCImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade CONTRATO_PONTO_CONS_MODAL_QDC e atributos da classe
 * ContratoPontoConsumoModalidadeQDC
 */
public class ContratoPontoConsumoModalidadeQDCFieldSetMapper implements FieldSetMapper<ContratoPontoConsumoModalidadeQDCImpl> {
	
	protected static final Logger LOG = Logger.getLogger(ContratoPontoConsumoModalidadeQDCFieldSetMapper.class);
	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ContratoPontoConsumoModalidadeQDCImpl mapFieldSet(FieldSet field) {

		ContratoPontoConsumoModalidadeQDCImpl contratoQDC = new ContratoPontoConsumoModalidadeQDCImpl();

		try {

			contratoQDC.setChavePrimaria(field.readLong("COPQ_CD"));
			contratoQDC.setVersao(field.readInt("COPQ_NR_VERSAO"));
			contratoQDC.setUltimaAlteracao(Calendar.getInstance().getTime());
			contratoQDC.setDataVigencia(field.readDate("COPQ_DT"));
			contratoQDC.setMedidaVolume(field.readBigDecimal("COPQ_MD_VOLUME"));

			if((Long) field.readLong("COPM_CD") != null) {
				ContratoPontoConsumoModalidade contratoPontoConsumoModalidade = new ContratoPontoConsumoModalidadeImpl();
				contratoPontoConsumoModalidade.setChavePrimaria(field.readLong("COPM_CD"));
				contratoQDC.setContratoPontoConsumoModalidade(contratoPontoConsumoModalidade);

			}

		} catch(Exception e) {
			LOG.error(e.getMessage(), e);
			Logger.getLogger("ERROR").debug(e.getStackTrace());
		}
		return contratoQDC;
	}

}
