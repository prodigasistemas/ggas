
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.impl.ContratoImpl;
import br.com.ggas.contrato.contrato.impl.ContratoQDCImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade CONTRATO_QDC ContratoQDC e campos da classe ContratoQDC
 */
public class ContratoQDCFieldSetMapper implements FieldSetMapper<ContratoQDCImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ContratoQDCImpl mapFieldSet(FieldSet field) {

		ContratoQDCImpl contrato = new ContratoQDCImpl();
		try {
			contrato.setChavePrimaria(field.readLong("COQD_CD"));
			contrato.setVersao(field.readInt("COQD_NR_VERSAO"));
			contrato.setUltimaAlteracao(Calendar.getInstance().getTime());
			contrato.setHabilitado(true);
			contrato.setData(field.readDate("COQD_DT"));
			contrato.setMedidaVolume(field.readBigDecimal("COQD_MD_VOLUME"));

			if((Long) field.readLong("CONT_CD") != null) {
				Contrato contrat = new ContratoImpl();
				contrat.setChavePrimaria(field.readLong("CONT_CD"));
				contrato.setContrato(contrat);
			}

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return contrato;
	}

}
