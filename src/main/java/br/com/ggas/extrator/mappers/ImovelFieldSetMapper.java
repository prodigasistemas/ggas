
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.empresa.impl.EmpresaImpl;
import br.com.ggas.cadastro.imovel.impl.AreaConstruidaFaixaImpl;
import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.cadastro.imovel.impl.ModalidadeMedicaoImovelImpl;
import br.com.ggas.cadastro.imovel.impl.PadraoConstrucaoImpl;
import br.com.ggas.cadastro.imovel.impl.PavimentoCalcadaImpl;
import br.com.ggas.cadastro.imovel.impl.PavimentoRuaImpl;
import br.com.ggas.cadastro.imovel.impl.PerfilImovelImpl;
import br.com.ggas.cadastro.imovel.impl.SituacaoImovelImpl;
import br.com.ggas.cadastro.imovel.impl.TipoBotijaoImpl;
import br.com.ggas.cadastro.localidade.impl.QuadraFaceImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade IMOVEL e atributos da classe Imovel
 */
public class ImovelFieldSetMapper implements FieldSetMapper<ImovelImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ImovelImpl mapFieldSet(FieldSet fieldSet) {

		ImovelImpl i = new ImovelImpl();

		try {
			i.setChavePrimaria(fieldSet.readLong("IMOV_CD"));

			if ((Long) fieldSet.readLong("QUFA_CD") != null) {
				QuadraFaceImpl quadraFace = new QuadraFaceImpl();

				quadraFace.setChavePrimaria(fieldSet.readLong("QUFA_CD"));
				i.setQuadraFace(quadraFace);
			}

			if ((Long) fieldSet.readLong("ARCF_CD") != null) {
				AreaConstruidaFaixaImpl areaConstruidaFaixa = new AreaConstruidaFaixaImpl();

				areaConstruidaFaixa.setChavePrimaria(fieldSet.readLong("ARCF_CD"));
				i.setAreaConstruidaFaixa(areaConstruidaFaixa);
			}

			if ((Long) fieldSet.readLong("PACA_CD") != null) {
				PavimentoCalcadaImpl pavimentoCalcada = new PavimentoCalcadaImpl();

				pavimentoCalcada.setChavePrimaria(fieldSet.readLong("PACA_CD"));
				i.setPavimentoCalcada(pavimentoCalcada);
			}

			if ((Long) fieldSet.readLong("PARU_CD") != null) {
				PavimentoRuaImpl pavimentoRua = new PavimentoRuaImpl();

				pavimentoRua.setChavePrimaria(fieldSet.readLong("PARU_CD"));
				i.setPavimentoRua(pavimentoRua);
			}

			if ((Long) fieldSet.readLong("IMSI_CD") != null) {
				SituacaoImovelImpl situacaoImovel = new SituacaoImovelImpl();

				situacaoImovel.setChavePrimaria(fieldSet.readLong("IMSI_CD"));
				i.setSituacaoImovel(situacaoImovel);
			}

			if ((Long) fieldSet.readLong("IMPE_CD") != null) {
				PerfilImovelImpl perfilImovel = new PerfilImovelImpl();

				perfilImovel.setChavePrimaria(fieldSet.readLong("IMPE_CD"));
				i.setPerfilImovel(perfilImovel);
			}

			if ((Long) fieldSet.readLong("PACO_CD") != null) {
				PadraoConstrucaoImpl padraoConstrucao = new PadraoConstrucaoImpl();

				padraoConstrucao.setChavePrimaria(fieldSet.readLong("PACO_CD"));
				i.setPadraoConstrucao(padraoConstrucao);
			}

			if ((Long) fieldSet.readLong("IMOV_IN_MEDICAO") != null) {
				ModalidadeMedicaoImovelImpl modalidadeMedicaoImovel = new ModalidadeMedicaoImovelImpl();

				modalidadeMedicaoImovel.setCodigo(fieldSet.readInt("IMOV_IN_MEDICAO"));
				i.setModalidadeMedicaoImovel(modalidadeMedicaoImovel);
			}

			if ((Long) fieldSet.readLong("BOTI_CD") != null) {
				TipoBotijaoImpl tipoBotijao = new TipoBotijaoImpl();

				tipoBotijao.setChavePrimaria(fieldSet.readInt("BOTI_CD"));
				i.setTipoBotijao(tipoBotijao);
			}

			if ((Long) fieldSet.readLong("EMPR_CD") != null) {
				EmpresaImpl empresa = new EmpresaImpl();

				empresa.setChavePrimaria(fieldSet.readInt("EMPR_CD"));
				i.setEmpresa(empresa);
			}

			if ((Long) fieldSet.readLong("IMOV_CD_CONDOMINIO") != null) {
				ImovelImpl imovel = new ImovelImpl();

				imovel.setChavePrimaria(fieldSet.readInt("IMOV_CD_CONDOMINIO"));
				i.setImovelCondominio(imovel);
			}

			i.setNumeroLote(fieldSet.readInt("IMOV_NR_LOTE"));
			i.setNumeroSublote(fieldSet.readInt("IMOV_NR_SUBLOTE"));
			i.setNumeroImovel(fieldSet.readString("IMOV_NR_IMOVEL"));
			i.setDescricaoComplemento(fieldSet.readString("IMOV_DS_COMPLEMENTO"));
			i.setNome(fieldSet.readString("IMOV_NM"));
			i.setQuantidadeUnidadeConsumidora(fieldSet.readInt("IMOV_QN_UNIDADE_CONSUMIDORA"));
			i.setQuantidadePontoConsumo(fieldSet.readInt("IMOV_QN_PONTO_CONSUMO"));
			i.setNumeroSequenciaLeitura(fieldSet.readInt("IMOV_NR_SEQUENCIA_LEITURA"));
			i.setCondominio(fieldSet.readBoolean("IMOV_IN_CONDOMINIO"));
			i.setDataEntrega(fieldSet.readDate("IMOV_DT_ENTREGA"));
			i.setPortaria(fieldSet.readBoolean("IMOV_IN_PORTARIA"));
			i.setValvulaBloqueio(fieldSet.readBoolean("IMOV_IN_VALVULA_BLOQUEIO"));
			i.setQuantidadeBloco(fieldSet.readInt("IMOV_QN_BLOCO"));
			i.setQuantidadeAndar(fieldSet.readInt("IMOV_QN_ANDAR"));
			i.setQuantidadeApartamentoAndar(fieldSet.readInt("IMOV_QN_APARTAMENTO_ANDAR"));
			i.setRedePreexistente(fieldSet.readBoolean("IMOV_IN_REDE_PREEXISTENTE"));
			i.setHabilitado(true);
			i.setVersao(1);
			i.setUltimaAlteracao(new Date());
			i.setEnderecoReferencia(fieldSet.readString("IMOV_DS_ENDERECO_REFERENCIA"));
			i.setQuantidadeBanheiro(fieldSet.readInt("IMOV_QN_BANHEIRO"));

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return i;
	}

}
