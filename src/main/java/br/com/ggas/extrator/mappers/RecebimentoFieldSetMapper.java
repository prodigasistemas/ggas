
package br.com.ggas.extrator.mappers;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.arrecadacao.ArrecadadorMovimentoItem;
import br.com.ggas.arrecadacao.avisobancario.AvisoBancario;
import br.com.ggas.arrecadacao.avisobancario.impl.AvisoBancarioImpl;
import br.com.ggas.arrecadacao.devolucao.Devolucao;
import br.com.ggas.arrecadacao.devolucao.impl.DevolucaoImpl;
import br.com.ggas.arrecadacao.impl.ArrecadadorMovimentoItemImpl;
import br.com.ggas.arrecadacao.recebimento.RecebimentoSituacao;
import br.com.ggas.arrecadacao.recebimento.impl.RecebimentoImpl;
import br.com.ggas.arrecadacao.recebimento.impl.RecebimentoSituacaoImpl;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.impl.LocalidadeImpl;
import br.com.ggas.faturamento.DocumentoCobrancaItem;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.impl.DocumentoCobrancaItemImpl;
import br.com.ggas.faturamento.impl.FaturaGeralImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade RECEBIMENTO e atributos da classe Recebimento
 */
public class RecebimentoFieldSetMapper implements FieldSetMapper<RecebimentoImpl> {
	
	private static final Logger LOG = Logger.getLogger(RecebimentoFieldSetMapper.class);

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public RecebimentoImpl mapFieldSet(FieldSet field) {

		RecebimentoImpl recebimento = new RecebimentoImpl();

		try {
			recebimento.setChavePrimaria(field.readLong("RECE_CD"));
			recebimento.setVersao(field.readInt("RECE_NR_VERSAO"));
			recebimento.setUltimaAlteracao(Calendar.getInstance().getTime());
			recebimento.setHabilitado(true);
			recebimento.setAnoMesContabil(field.readInt("RECE_DT_AM_CONTABIL"));
			recebimento.setValorRecebimento(field.readBigDecimal("RECE_VL_RECEBIMENTO"));
			recebimento.setDataRecebimento(field.readDate("RECE_DT_RECEBIMENTO"));
			recebimento.setBaixado(field.readBoolean("RECE_IN_BAIXADO"));
			recebimento.setValorExcedente(field.readBigDecimal("RECE_VL_EXCEDENTE"));
			recebimento.setObservacao(field.readString("RECE_DS_OBSERVACAO"));
			recebimento.setValorPrincipal(field.readBigDecimal("RECE_VL_PRINCIPAL"));
			recebimento.setValorJurosMulta(field.readBigDecimal("RECE_VL_JUROS_MULTA"));
			recebimento.setValorDescontos(field.readBigDecimal("RECE_VL_DESCONTOS"));
			recebimento.setValorAbatimento(field.readBigDecimal("RECE_VL_ABATIMENTO"));
			recebimento.setValorCredito(field.readBigDecimal("RECE_VL_CREDITO"));
			recebimento.setValorIOF(field.readBigDecimal("RECE_VL_IOF"));
			recebimento.setValorCobranca(field.readBigDecimal("RECE_VL_COBRANCA"));
			recebimento.setSequencial(field.readInt("RECE_NR_SEQUENCIAL"));
			if((Long) field.readLong("REST_CD") != null) {
				RecebimentoSituacao recebimentoSituacao = new RecebimentoSituacaoImpl();
				recebimentoSituacao.setChavePrimaria(field.readLong("REST_CD"));
				recebimento.setRecebimentoSituacao(recebimentoSituacao);
			}
			if((Long) field.readLong("REST_CD") != null) {
				Cliente cliente = new ClienteImpl();
				cliente.setChavePrimaria(field.readLong("REST_CD"));
				recebimento.setCliente(cliente);

			}
			if((Long) field.readLong("POCN_CD") != null) {
				PontoConsumo pontoConsumo = new PontoConsumoImpl();
				pontoConsumo.setChavePrimaria(field.readLong("POCN_CD"));
				recebimento.setPontoConsumo(pontoConsumo);

			}
			if((Long) field.readLong("ARMI_CD") != null) {
				ArrecadadorMovimentoItem arrecadadorMovimentoItem = new ArrecadadorMovimentoItemImpl();
				arrecadadorMovimentoItem.setChavePrimaria(field.readLong("ARMI_CD"));
				recebimento.setArrecadadorMovimentoItem(arrecadadorMovimentoItem);
			}
			if((Long) field.readLong("AVBA_CD") != null) {
				AvisoBancario avisoBancario = new AvisoBancarioImpl();
				avisoBancario.setChavePrimaria(field.readLong("AVBA_CD"));
				recebimento.setAvisoBancario(avisoBancario);
			}
			if((Long) field.readLong("FAGE_CD") != null) {
				FaturaGeral faturaGeral = new FaturaGeralImpl();
				faturaGeral.setChavePrimaria(field.readLong("FAGE_CD"));
				recebimento.setFaturaGeral(faturaGeral);
			}
			if((Long) field.readLong("DOCI_CD") != null) {
				DocumentoCobrancaItem documentoCobrancaItem = new DocumentoCobrancaItemImpl();
				documentoCobrancaItem.setChavePrimaria(field.readLong("DOCI_CD"));
				recebimento.setDocumentoCobrancaItem(documentoCobrancaItem);
			}
			if((Long) field.readLong("ENCO_CD_FORMA_ARRECADACAO") != null) {
				EntidadeConteudo formaArrecadacao = new EntidadeConteudoImpl();
				formaArrecadacao.setChavePrimaria(field.readLong("DOCI_CD"));
				recebimento.setFormaArrecadacao(formaArrecadacao);
			}
			if((Long) field.readLong("DEVO_CD") != null) {
				Devolucao devolucao = new DevolucaoImpl();
				devolucao.setChavePrimaria(field.readLong("DEVO_CD"));
				recebimento.setDevolucao(devolucao);
			}
			if((Long) field.readLong("LOCA_CD") != null) {
				Localidade localidade = new LocalidadeImpl();
				localidade.setChavePrimaria(field.readLong("DEVO_CD"));
				recebimento.setLocalidade(localidade);
			}

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return recebimento;
	}
}
