
package br.com.ggas.extrator.mappers;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.medicao.consumo.impl.ImovelPCSZImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade IMOVEL_PCS_Z e atributos da classe ImovelPCSZ
 */
public class ImovelPCSZFieldSetMapper implements FieldSetMapper<ImovelPCSZImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public ImovelPCSZImpl mapFieldSet(FieldSet field) {

		ImovelPCSZImpl imovel = new ImovelPCSZImpl();

		try {
			imovel.setChavePrimaria(field.readLong("IMPZ_CD"));
			imovel.setVersao(field.readInt("IMPZ_NR_VERSAO"));
			imovel.setUltimaAlteracao(field.readDate("IMPZ_TM_ULTIMA_ALTERACAO"));
			imovel.setHabilitado(field.readBoolean("IMPZ_IN_USO"));
			imovel.setDataVigencia(field.readDate("IMPZ_DT"));
			imovel.setMedidaPCS(field.readInt("IMPZ_MD_PCS"));
			imovel.setFatorZ(field.readBigDecimal("IMPZ_NR_FATOR_Z"));
			imovel.setIndicadorAlteracao(field.readBoolean("IMPZ_IN_ALTERACAO"));

			Imovel imov = new ImovelImpl();
			imov.setChavePrimaria(field.readLong("IMOV_CD"));
			imovel.setImovel(imov);
		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return imovel;
	}

}
