
package br.com.ggas.extrator.mappers;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.leitura.impl.InstalacaoMedidorImpl;
import br.com.ggas.medicao.leitura.impl.MedidorLocalInstalacaoImpl;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.medidor.impl.MedidorImpl;
import br.com.ggas.medicao.medidor.impl.MotivoOperacaoMedidorImpl;
import br.com.ggas.medicao.medidor.impl.OperacaoMedidorImpl;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.impl.VazaoCorretorHistoricoOperacaoImpl;
import br.com.ggas.medicao.vazaocorretor.impl.VazaoCorretorImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade CORRETOR_VAZAO_OPE_HIST e atributos da classe
 * VazaoCorretorHistoricoOperacao
 */
public class VazaoCorretorHistoricoOperacaoFieldSetMapper implements FieldSetMapper<VazaoCorretorHistoricoOperacaoImpl> {

	private static final Logger LOG = Logger.getLogger(VazaoCorretorHistoricoOperacaoFieldSetMapper.class);

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public VazaoCorretorHistoricoOperacaoImpl mapFieldSet(FieldSet field) {

		VazaoCorretorHistoricoOperacaoImpl vazao = new VazaoCorretorHistoricoOperacaoImpl();

		try {

			vazao.setChavePrimaria(field.readLong("COVO_CD"));
			vazao.setVersao(field.readInt("COVO_NR_VERSAO"));
			vazao.setUltimaAlteracao(field.readDate("COVO_TM_ULTIMA_ALTERACAO"));
			vazao.setHabilitado(field.readBoolean("COVO_IN_USO"));
			vazao.setDataRealizada(field.readDate("COVO_TM_OPERACAO"));
			vazao.setLeitura(field.readBigDecimal("COVO_MD_LEITURA"));

			VazaoCorretor vazaoCorretor = new VazaoCorretorImpl();
			vazaoCorretor.setChavePrimaria(field.readLong("COVA_CD"));
			vazao.setVazaoCorretor(vazaoCorretor);

			PontoConsumo pontoConsumo = new PontoConsumoImpl();
			pontoConsumo.setChavePrimaria(field.readLong("POCN_CD"));
			vazao.setPontoConsumo(pontoConsumo);

			OperacaoMedidor operacaoMedidor = new OperacaoMedidorImpl();
			operacaoMedidor.setChavePrimaria(field.readLong("MEOP_CD"));
			vazao.setOperacaoMedidor(operacaoMedidor);

			Medidor medidor = new MedidorImpl();
			medidor.setChavePrimaria(field.readLong("MEDI_CD"));
			vazao.setMedidor(medidor);

			MotivoOperacaoMedidor motivoOperacaoMedidor = new MotivoOperacaoMedidorImpl();
			motivoOperacaoMedidor.setChavePrimaria(field.readLong("MEMP_CD"));
			vazao.setMotivoOperacaoMedidor(motivoOperacaoMedidor);

			MedidorLocalInstalacao localInstalacao = new MedidorLocalInstalacaoImpl();
			localInstalacao.setChavePrimaria(field.readLong("MELI_CD"));
			vazao.setLocalInstalacao(localInstalacao);

			Funcionario funcionario = new FuncionarioImpl();
			funcionario.setChavePrimaria(field.readLong("FUNC_CD"));
			vazao.setFuncionario(funcionario);

			InstalacaoMedidor instalacaoMedidor = new InstalacaoMedidorImpl();
			instalacaoMedidor.setChavePrimaria(field.readLong("MEIN_CD"));
			vazao.setInstalacaoMedidor(instalacaoMedidor);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return vazao;
	}

}
