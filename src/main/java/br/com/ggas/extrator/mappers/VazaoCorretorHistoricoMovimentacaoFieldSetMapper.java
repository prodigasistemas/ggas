
package br.com.ggas.extrator.mappers;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.cadastro.operacional.impl.MedidorLocalArmazenagemImpl;
import br.com.ggas.medicao.medidor.MotivoMovimentacaoMedidor;
import br.com.ggas.medicao.medidor.impl.MotivoMovimentacaoMedidorImpl;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.impl.VazaoCorretorHistoricoMovimentacaoImpl;
import br.com.ggas.medicao.vazaocorretor.impl.VazaoCorretorImpl;

/**
 * Classe responsável pela implementação do método de mapeamento de campos da
 * entidade CORRETOR_VAZAO_MOVIMENTACAO e atributos da classe
 * VazaoCorretorHistoricoMovimentacao
 */
public class VazaoCorretorHistoricoMovimentacaoFieldSetMapper implements FieldSetMapper<VazaoCorretorHistoricoMovimentacaoImpl> {

	private static final Logger LOG = Logger.getLogger(VazaoCorretorHistoricoMovimentacaoFieldSetMapper.class);

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public VazaoCorretorHistoricoMovimentacaoImpl mapFieldSet(FieldSet field) {

		VazaoCorretorHistoricoMovimentacaoImpl vazao = new VazaoCorretorHistoricoMovimentacaoImpl();

		try {

			vazao.setChavePrimaria(field.readLong("COMV_CD"));
			vazao.setVersao(field.readInt("COMV_NR_VERSAO"));
			vazao.setUltimaAlteracao(field.readDate("COMV_TM_ULTIMA_ALTERACAO"));
			vazao.setHabilitado(field.readBoolean("COMV_IN_USO"));
			vazao.setDescricaoParecer(field.readString("COMV_DS_PARECER"));
			vazao.setDataMovimento(field.readDate("COMV_DT"));

			Long idVazaoCorretor = field.readLong("COVA_CD");
			VazaoCorretor vazaoCorretor = new VazaoCorretorImpl();
			vazaoCorretor.setChavePrimaria(idVazaoCorretor);
			vazao.setVazaoCorretor(vazaoCorretor);

			Long idMotivoMovimentacao = field.readLong("MEMM_CD");
			MotivoMovimentacaoMedidor motivoMovimentacaoMedidor = new MotivoMovimentacaoMedidorImpl();
			motivoMovimentacaoMedidor.setChavePrimaria(idMotivoMovimentacao);
			vazao.setMotivoMovimentacaoMedidor(motivoMovimentacaoMedidor);

			Long idLocalArmazenagemOrigem = field.readLong("MELA_CD_ORIGEM");
			MedidorLocalArmazenagem localArmazenagemOrigem = new MedidorLocalArmazenagemImpl();
			localArmazenagemOrigem.setChavePrimaria(idLocalArmazenagemOrigem);
			vazao.setLocalArmazenagemOrigem(localArmazenagemOrigem);

			Long idLocalArmazenagemDestino = field.readLong("MELA_CD_DESTINO");
			MedidorLocalArmazenagem localArmazenagemDestino = new MedidorLocalArmazenagemImpl();
			localArmazenagemDestino.setChavePrimaria(idLocalArmazenagemDestino);
			vazao.setLocalArmazenagemDestino(localArmazenagemDestino);

			Long idFuncionario = field.readLong("FUNC_CD");
			Funcionario funcionario = new FuncionarioImpl();
			funcionario.setChavePrimaria(idFuncionario);
			vazao.setFuncionario(funcionario);

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return vazao;
	}

}
