
package br.com.ggas.extrator.mappers;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.cadastro.operacional.impl.MedidorLocalArmazenagemImpl;
import br.com.ggas.medicao.medidor.CapacidadeMedidor;
import br.com.ggas.medicao.medidor.DiametroMedidor;
import br.com.ggas.medicao.medidor.FatorK;
import br.com.ggas.medicao.medidor.MarcaMedidor;
import br.com.ggas.medicao.medidor.ModeloMedidor;
import br.com.ggas.medicao.medidor.SituacaoMedidor;
import br.com.ggas.medicao.medidor.TipoMedidor;
import br.com.ggas.medicao.medidor.impl.CapacidadeMedidorImpl;
import br.com.ggas.medicao.medidor.impl.DiametroMedidorImpl;
import br.com.ggas.medicao.medidor.impl.FatorKImpl;
import br.com.ggas.medicao.medidor.impl.MarcaMedidorImpl;
import br.com.ggas.medicao.medidor.impl.MedidorImpl;
import br.com.ggas.medicao.medidor.impl.ModeloMedidorImpl;
import br.com.ggas.medicao.medidor.impl.SituacaoMedidorImpl;
import br.com.ggas.medicao.medidor.impl.TipoMedidorImpl;
import br.com.ggas.medicao.vazaocorretor.FaixaTemperaturaTrabalho;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.medicao.vazaocorretor.impl.FaixaTemperaturaTrabalhoImpl;
import br.com.ggas.medicao.vazaocorretor.impl.UnidadeImpl;

/**
 * Classe responsável pela implementação do método
 * de mapeamento de campos da entidade MEDIDOR e atributos da classe Medidor
 */
public class MedidorFieldSetMapper implements FieldSetMapper<MedidorImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public MedidorImpl mapFieldSet(FieldSet field) {

		MedidorImpl med = new MedidorImpl();
		try {
			med.setChavePrimaria(field.readLong("MEDI_CD"));
			med.setVersao(field.readInt("MEDI_NR_VERSAO"));
			med.setUltimaAlteracao(field.readDate("MEDI_TM_ULTIMA_ALTERACAO"));
			med.setHabilitado(field.readBoolean("MEDI_IN_USO"));
			med.setNumeroInicial(field.readString("MEDI_NR_SERIE"));
			med.setAnoFabricacao(field.readInt("MEDI_NR_ANO_FABRICACAO"));
			med.setDataAquisicao(field.readDate("MEDI_DT_AQUISICAO"));
			med.setDataMaximaInstalacao(field.readDate("MEDI_DT_MAXIMA_INSTALACAO"));
			med.setDataUltimaCalibracao(field.readDate("MEDI_DT_ULTIMA_CALIBRACAO"));
			med.setAnoCalibracao(field.readInt("MEDI_QN_ANO_CALIBRACAO"));
			med.setTombamento(field.readString("MEDI_NR_TOMBO"));
			med.setDigito(field.readInt("MEDI_NR_DIGITO"));
			med.setLeituraAcumulada(field.readInt("MEDI_NR_LEITURA_ACUMULADA"));
			med.setPressaoMaximaTrabalho(field.readBigDecimal("MEDI_MD_PRESSAO_MAXIMA"));

			SituacaoMedidor situacaoMedidor = new SituacaoMedidorImpl();
			situacaoMedidor.setChavePrimaria(field.readLong("MESI_CD"));
			med.setSituacaoMedidor(situacaoMedidor);

			TipoMedidor tipoMedidor = new TipoMedidorImpl();
			tipoMedidor.setChavePrimaria(field.readLong("METI_CD"));
			med.setTipoMedidor(tipoMedidor);

			MarcaMedidor marcaMedidor = new MarcaMedidorImpl();
			marcaMedidor.setChavePrimaria(field.readLong("MEMA_CD"));
			med.setMarcaMedidor(marcaMedidor);

			DiametroMedidor diametroMedidor = new DiametroMedidorImpl();
			diametroMedidor.setChavePrimaria(field.readLong("MEDM_CD"));
			med.setDiametroMedidor(diametroMedidor);

			CapacidadeMedidor capacidadeMinima = new CapacidadeMedidorImpl();
			capacidadeMinima.setChavePrimaria(field.readLong("MECA_CD_MINIMO"));
			med.setCapacidadeMinima(capacidadeMinima);

			CapacidadeMedidor capacidadeMaxima = new CapacidadeMedidorImpl();
			capacidadeMaxima.setChavePrimaria(field.readLong("MECA_CD_MAXIMO"));
			med.setCapacidadeMaxima(capacidadeMaxima);

			MedidorLocalArmazenagem localArmazenagem = new MedidorLocalArmazenagemImpl();
			localArmazenagem.setChavePrimaria(field.readLong("MELA_CD"));
			med.setLocalArmazenagem(localArmazenagem);

			FaixaTemperaturaTrabalho faixaTemperaturaTrabalho = new FaixaTemperaturaTrabalhoImpl();
			faixaTemperaturaTrabalho.setChavePrimaria(field.readLong("TETF_CD"));
			med.setFaixaTemperaturaTrabalho(faixaTemperaturaTrabalho);

			Unidade unidadePressaoMaximaTrabalho = new UnidadeImpl();
			unidadePressaoMaximaTrabalho.setChavePrimaria(field.readLong("UNID_CD_PRESSAO_MAXIMA"));
			med.setUnidadePressaoMaximaTrabalho(unidadePressaoMaximaTrabalho);

			ModeloMedidor modelo = new ModeloMedidorImpl();
			modelo.setChavePrimaria(field.readLong("MEMD_CD"));
			med.setModelo(modelo);

			FatorK fatorK = new FatorKImpl();
			fatorK.setChavePrimaria(field.readLong("MEFK_CD"));
			med.setFatorK(fatorK);

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return med;
	}

}
