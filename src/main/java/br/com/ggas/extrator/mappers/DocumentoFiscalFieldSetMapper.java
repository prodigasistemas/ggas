
package br.com.ggas.extrator.mappers;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

import br.com.ggas.faturamento.fatura.impl.SerieImpl;
import br.com.ggas.faturamento.impl.DocumentoFiscalImpl;
import br.com.ggas.faturamento.impl.FaturaImpl;
import br.com.ggas.faturamento.impl.NaturezaOperacaoCFOPImpl;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;

/**
 * Classe responsável por documento fiscal.
 */
public class DocumentoFiscalFieldSetMapper implements FieldSetMapper<DocumentoFiscalImpl> {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
	 */
	@Override
	public DocumentoFiscalImpl mapFieldSet(FieldSet fieldSet) {

		DocumentoFiscalImpl c = new DocumentoFiscalImpl();

		try {
			if((Long) fieldSet.readLong("FATU_CD") != null) {
				FaturaImpl fatura = new FaturaImpl();

				fatura.setChavePrimaria(fieldSet.readLong("FATU_CD"));
				c.setFatura(fatura);
			}

			if((Long) fieldSet.readLong("NAOC_CD") != null) {
				NaturezaOperacaoCFOPImpl fatura = new NaturezaOperacaoCFOPImpl();

				fatura.setChavePrimaria(fieldSet.readLong("NAOC_CD"));
				c.setNaturezaOperacaoCFOP(fatura);
			}

			if((Long) fieldSet.readLong("ENCO_CD_TIPO_OPERACAO") != null) {
				EntidadeConteudoImpl entidadeConteudo = new EntidadeConteudoImpl();

				entidadeConteudo.setChavePrimaria(fieldSet.readLong("ENCO_CD_TIPO_OPERACAO"));
				c.setTipoOperacao(entidadeConteudo);
			}

			if((Long) fieldSet.readLong("ENCO_CD_TIPO_FATURAMENTO") != null) {
				EntidadeConteudoImpl entidadeConteudo = new EntidadeConteudoImpl();

				entidadeConteudo.setChavePrimaria(fieldSet.readLong("ENCO_CD_TIPO_FATURAMENTO"));
				c.setTipoFaturamento(entidadeConteudo);
			}

			if((Long) fieldSet.readLong("ENCO_CD_TIPO_EMISSAO_NFE") != null) {
				EntidadeConteudoImpl entidadeConteudo = new EntidadeConteudoImpl();

				entidadeConteudo.setChavePrimaria(fieldSet.readLong("ENCO_CD_TIPO_EMISSAO_NFE"));
				c.setTipoEmissaoNfe(entidadeConteudo);
			}

			if((Long) fieldSet.readLong("ENCO_CD_STATUS_NFE") != null) {
				EntidadeConteudoImpl entidadeConteudo = new EntidadeConteudoImpl();

				entidadeConteudo.setChavePrimaria(fieldSet.readLong("ENCO_CD_STATUS_NFE"));
				c.setStatusNfe(entidadeConteudo);
			}

			if((Long) fieldSet.readLong("SERI_CD") != null) {
				SerieImpl serie = new SerieImpl();

				serie.setChavePrimaria(fieldSet.readLong("SERI_CD"));
				c.setSerie(serie);
			}

			c.setChavePrimaria(fieldSet.readLong("DOFI_CD"));
			c.setNumero(fieldSet.readLong("DOFI_NR"));
			c.setDescricaoSerie(fieldSet.readString("DOFI_DS_SERIE"));
			c.setInscricaoEstadual(fieldSet.readString("DOFI_NR_INSC_ESTADUAL_SUBS"));
			c.setValorTotal(fieldSet.readBigDecimal("DOFI_VL_TOTAL"));
			c.setNomeCliente(fieldSet.readString("DOFI_NM_CLIENTE"));
			c.setCpfCpnj(fieldSet.readString("DOFI_NR_CPF_CNPJ"));
			c.setInscricaoEstadual(fieldSet.readString("DOFI_NR_INSC_ESTADUAL"));
			c.setInscEstadualTransportadora(fieldSet.readString("DOFI_NR_INSC_ESTADUAL_TRANSP"));
			c.setRg(fieldSet.readString("DOFI_NR_INSC_ESTADUAL_TRANSP"));
			c.setEndereco(fieldSet.readString("DOFI_DS_ENDERECO"));
			c.setComplemento(fieldSet.readString("DOFI_DS_COMPLEMENTO"));
			c.setBairro(fieldSet.readString("DOFI_NM_BAIRRO"));
			c.setCep(fieldSet.readString("DOFI_NR_CEP"));
			c.setMunicipio(fieldSet.readString("DOFI_NM_MUNICIPIO"));
			c.setUf(fieldSet.readString("DOFI_SG_UF"));
			c.setApresentacao(fieldSet.readDate("DOFI_DT_APRESENTACAO"));
			c.setDataEmissao(fieldSet.readDate("DOFI_DT_EMISSAO"));
			c.setHoraSaida(fieldSet.readDate("DOFI_TM_HORA_SAIDA"));
			c.setNomeTransportador(fieldSet.readString("DOFI_NM_TRANSPORTADOR"));
			c.setCodigoANTT(fieldSet.readInt("DOFI_CD_ANTT"));
			c.setPlacaVeiculo(fieldSet.readString("DOFI_DS_PLACA_VEICULO"));
			c.setUfPlaca(fieldSet.readString("DOFI_SG_UF_PLACA"));
			c.setCpfCNPJTransportadora(fieldSet.readString("DOFI_NR_CPF_CNPJ_TRANSPORT"));
			c.setEnderecoTransportadora(fieldSet.readString("DOFI_DS_ENDERECO_TRANSPORT"));
			c.setMunicipioTransportadora(fieldSet.readString("DOFI_DS_MUNICIPIO_TRANSPORT"));
			c.setUfTransportadora(fieldSet.readString("DOFI_SG_UF_TRANSPORTADORA"));
			c.setMensagem(fieldSet.readString("DOFI_DS_MENSAGEM"));
			c.setHabilitado(true);
			c.setVersao(1);
			c.setUltimaAlteracao(new Date());
			c.setChaveAcesso(fieldSet.readString("DOFI_NR_CHAVE_ACESSO_NFE"));
			c.setNumeroProtocolo(fieldSet.readString("DOFI_NR_PROTOCOLO"));
			c.setIndicadorTipoNota(fieldSet.readInt("DOFI_IN_NORMAL_ELETRONICA"));
		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
		}

		return c;
	}

}
