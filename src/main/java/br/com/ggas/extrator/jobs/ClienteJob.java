
package br.com.ggas.extrator.jobs;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;

import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.cliente.impl.ClienteEnderecoImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteFoneImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.cliente.impl.ContatoClienteImpl;
import br.com.ggas.cadastro.cliente.impl.TipoClienteImpl;
import br.com.ggas.extrator.Fieldset;
import br.com.ggas.extrator.cache.ClienteCache;
import br.com.ggas.extrator.mappers.ClienteContatoFieldSetMapper;
import br.com.ggas.extrator.mappers.ClienteEnderecoFieldSetMapper;
import br.com.ggas.extrator.mappers.ClienteFieldSetMapper;
import br.com.ggas.extrator.mappers.ClienteFoneFieldSetMapper;
import br.com.ggas.extrator.mappers.ClienteTipoFieldSetMapper;
import br.com.ggas.extrator.readers.ExtratorConstantes;
import br.com.ggas.extrator.readers.GenericReader;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe responsável por cliente job.
 */
@Service("ClienteJob")
public class ClienteJob extends HibernateDaoSupport {

	private static final Logger LOG = Logger.getLogger(ClienteJob.class);

	private int sucesso = 0;

	private int erro = 0;

	/**
	 * Instantiates a new cliente job.
	 * 
	 * @param sessionFactory the session factory
	 */
	@Autowired
	public ClienteJob(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	// contador de registros inseridos

	/**
	 * Processar.
	 * 
	 * @param log the log
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void processar(StringBuilder log) throws IOException {

		processarClienteTipo(log);
		processarCliente(log);
		processarClienteEndereco(log);
		processarClienteContato(log);
		processarClienteFone(log);

		persistir(log);

	}

	private void persistir(StringBuilder log) {

		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();

		Collection<ClienteImpl> clientes = ClienteCache.getInstance().getClientes();

		sucesso = 0;
		erro = 0;

		log.append("##### Inserindo Clientes ######\n");

		for (ClienteImpl cliente : clientes) {
			try {

				cliente.setChavePrimaria(0);

				controladorCliente.inserir(cliente);
				sucesso++;
			} catch (NegocioException e) {
				LOG.error(e.getMessage(), e);
				if (e.getMessage() != null) {
					log.append(e.getMessage());
					log.append(" ");
				}
				if (e.getChaveErro() != null) {
					log.append(e.getChaveErro());
					log.append(" - ");
				}
				if (e.getParametrosErro() != null) {
					for (Object o : e.getParametrosErro()) {
						log.append(o.toString());
						log.append(", ");
					}
				}
				log.append("\n");
				Map<String, Object> erros = e.getErros();
				if (erros != null) {
					for (Map.Entry<String, Object> entry : erros.entrySet()) {
						log.append(entry.getKey());
						log.append(" - ");
						log.append(entry.getValue().toString());
						log.append("\n");
					}
				}

				erro++;
			}
		}

		log.append("\nForam inseridos ");
		log.append(sucesso);
		log.append(" registros , foram abortados ");
		log.append(erro);
		log.append(" registros. \n\n");
	}

	/**
	 * Processar cliente.
	 * 
	 * @param log the log
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processarCliente(StringBuilder log) throws IOException {

		GenericReader r = new GenericReader();
		r.setFieldNames(ExtratorConstantes.FIELD_NAMES_CLIENTE);
		r.setPath(ExtratorConstantes.FILE_NAME_CLIENTE);

		sucesso = 0;
		erro = 0;

		log.append("##### Lendo Clientes ######\n");

		List<Fieldset> lista = r.read(log);
		List<ClienteImpl> clientes = new ClienteFieldSetMapper().mapFieldSet(lista);

		for (ClienteImpl cliente : clientes) {
			ClienteCache.getInstance().setCliente(cliente);
			sucesso++;
		}
		log.append("\nForam Lidos ");
		log.append(sucesso);
		log.append(" registros , foram abortados ");
		log.append(erro);
		log.append(" registros. \n\n");
	}

	/**
	 * Processar cliente contato.
	 * 
	 * @param log the log
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processarClienteContato(StringBuilder log) throws IOException {

		GenericReader r = new GenericReader();
		r.setFieldNames(ExtratorConstantes.FIELD_NAMES_CLIENTE_CONTATO);
		r.setPath(ExtratorConstantes.FILE_NAME_CLIENTE_CONTATO);

		sucesso = 0;
		erro = 0;

		log.append("##### Lendo ClientesEndereco ######\n");

		List<Fieldset> lista = r.read(log);
		List<ContatoClienteImpl> contatosClientes = new ClienteContatoFieldSetMapper().mapFieldSet(lista);

		for (int i = 0; i < contatosClientes.size(); i++) {

			try {
				sucesso++;
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				log.append(e.getMessage());
				erro++;
			}
		}
		log.append("\nForam lidos ");
		log.append(sucesso);
		log.append(" registros , foram abortados ");
		log.append(erro);
		log.append(" registros. \n\n");
	}

	/**
	 * Processar cliente endereco.
	 * 
	 * @param log the log
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processarClienteEndereco(StringBuilder log) throws IOException {

		GenericReader r = new GenericReader();
		r.setFieldNames(ExtratorConstantes.FIELD_NAMES_CLIENTE_ENDERECO);
		r.setPath(ExtratorConstantes.FILE_NAME_CLIENTE_ENDERECO);

		sucesso = 0;
		erro = 0;

		log.append("##### Lendo ClientesEndereco ######\n");

		List<Fieldset> lista = r.read(log);
		List<ClienteEnderecoImpl> clienteEnderecos = new ClienteEnderecoFieldSetMapper().mapFieldSet(lista);

		for (int i = 0; i < clienteEnderecos.size(); i++) {

			try {
				sucesso++;
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				log.append(e.getMessage());
				erro++;
			}
		}

		log.append("\nForam lidos ").append(sucesso).append(" registros , foram abortados ").append(erro).append(" registros. \n\n");
	}

	/**
	 * Processar cliente fone.
	 * 
	 * @param log the log
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processarClienteFone(StringBuilder log) throws IOException {

		GenericReader r = new GenericReader();
		r.setFieldNames(ExtratorConstantes.FIELD_NAMES_CLIENTE_FONE);
		r.setPath(ExtratorConstantes.FILE_NAME_CLIENTE_FONE);

		sucesso = 0;
		erro = 0;

		log.append("##### Lendo ClientesFone ######\n");

		List<Fieldset> lista = r.read(log);
		List<ClienteFoneImpl> clientesFones = new ClienteFoneFieldSetMapper().mapFieldSet(lista);

		for (int i = 0; i < clientesFones.size(); i++) {

			try {
				sucesso++;
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				log.append(e.getMessage());
				erro++;
			}
		}

		log.append("\nForam lidos ");
		log.append(sucesso);
		log.append(" registros , foram abortados ");
		log.append(erro);
		log.append(" registros. \n\n");
	}

	/**
	 * Processar cliente tipo.
	 * 
	 * @param log the log
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processarClienteTipo(StringBuilder log) throws IOException {

		GenericReader r = new GenericReader();
		r.setFieldNames(ExtratorConstantes.FIELD_NAMES_CLIENTE_TIPO);
		r.setPath(ExtratorConstantes.FILE_NAME_CLIENTE_TIPO);

		sucesso = 0;
		erro = 0;

		log.append("##### Lendo ClientesTipo ######\n");

		List<Fieldset> lista = r.read(log);
		List<TipoClienteImpl> tipoClientes = new ClienteTipoFieldSetMapper().mapFieldSet(lista);

		for (TipoClienteImpl tipo : tipoClientes) {
			try {
				ClienteCache.getInstance().setClientesTipo(tipo);
				sucesso++;
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				log.append(e.getMessage());
				erro++;
			}

		}
		log.append("\nForam lidos ");
		log.append(sucesso);
		log.append(" registros , foram abortados ");
		log.append(erro);
		log.append(" registros. \n\n");
	}

}
