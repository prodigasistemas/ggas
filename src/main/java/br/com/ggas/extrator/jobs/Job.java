
package br.com.ggas.extrator.jobs;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import br.com.ggas.geral.exception.NegocioException;
/**
 * Classe responsável pelos métodos do Job
 * 
 *
 */
public abstract class Job {

	/**
	 * Processar.
	 * 
	 * @param log
	 *            the log
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public abstract void processar(StringBuilder log) throws IOException;

	/**
	 * Tratar erro.
	 * 
	 * @param log
	 *            the log
	 * @param e
	 *            the e
	 */
	public void tratarErro(StringBuilder log, NegocioException e) {

		if(e.getMessage() != null) {
			log.append(e.getMessage());
			log.append("\n");
		}
		if(e.getChaveErro() != null) {
			log.append(e.getChaveErro());
			log.append(" - ");
		}
		if(e.getParametrosErro() != null) {
			for (Object o : e.getParametrosErro()) {
				log.append(o.toString());
				log.append(", ");
			}
		}
		Map<String, Object> erros = e.getErros();
		if(erros != null) {
			for (Entry<String, Object> entry : erros.entrySet()) {
				log.append(entry.getKey());
				log.append(" - ");
				log.append(entry.getValue().toString());
				log.append("\n");
			}
		}

	}

}
