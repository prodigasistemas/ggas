
package br.com.ggas.extrator.readers;

import java.io.FileNotFoundException;
import java.util.List;

import br.com.ggas.extrator.Fieldset;

/**
 * Classe responsável por reader.
 */
public abstract class Reader {

	/**
	 * Read.
	 * 
	 * @param log
	 *            the log
	 * @return the list
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public abstract List<Fieldset> read(StringBuilder log) throws FileNotFoundException;

}
