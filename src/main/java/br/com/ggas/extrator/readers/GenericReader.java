
package br.com.ggas.extrator.readers;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import br.com.ggas.extrator.Fieldset;

/**
 * Classe responsável por generic reader.
 */
public class GenericReader {

	private String path;

	private String fieldNames;

	/**
	 * Read.
	 * 
	 * @param log
	 *            the log
	 * @return the list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public List<Fieldset> read(StringBuilder log) throws IOException {

		String folder = "c:\\ggas\\";

		List<Fieldset> lista = new ArrayList<>();

		FileInputStream stream = new FileInputStream(folder.toUpperCase() + path.toUpperCase() + ".txt");//NOSONAR
		InputStreamReader reader = new InputStreamReader(stream, "ISO-8859-1");
		BufferedReader br = new BufferedReader(reader);
		String linha = br.readLine();

		int count = 0;

		while(linha != null) {
			// ignora os cabeçalhos e linhas vazias
			if(!linha.trim().isEmpty() && count != 0) {
				log.append(linha);
				log.append("\n");
				lista.add(new Fieldset(linha, fieldNames));
			}
			count++;
			linha = br.readLine();
		}
		br.close();
		return lista;

	}

	public String getPath() {

		return path;
	}

	public void setPath(String path) {

		this.path = path;
	}

	public String getFieldNames() {

		return fieldNames;
	}

	public void setFieldNames(String fieldNames) {

		this.fieldNames = fieldNames;
	}

}
