
package br.com.ggas.extrator.cache;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.cliente.impl.TipoClienteImpl;
/**
 * Classe responsável pelas operações de 
 * adição e remoção em um cache de cliente e tipo de cliente.
 * 
 *
 */
public class ClienteCache {

	private Map<Long, TipoClienteImpl> clientesTipo = new HashMap<>();

	private Map<Long, ClienteImpl> clientes = new HashMap<>();

	private static ClienteCache c = new ClienteCache();

	public static ClienteCache getInstance() {

		return c;
	}

	//
	public void setClientesTipo(TipoClienteImpl o) {

		clientesTipo.put(o.getChavePrimaria(), o);
	}

	/**
	 * Gets the clientes tipo.
	 * 
	 * @param chave
	 *            the chave
	 * @return the clientes tipo
	 */
	public TipoClienteImpl getClientesTipo(Long chave) {

		return clientesTipo.get(chave);
	}

	public void setCliente(ClienteImpl cliente) {

		clientes.put(cliente.getChavePrimaria(), cliente);

	}

	/**
	 * Gets the cliente.
	 * 
	 * @param chave
	 *            the chave
	 * @return the cliente
	 */
	public ClienteImpl getCliente(Long chave) {

		return clientes.get(chave);

	}

	public Collection<ClienteImpl> getClientes() {

		Collection<ClienteImpl> cliente = new HashSet<>();
		Set<Entry<Long,ClienteImpl>> chaves = clientes.entrySet();

		for (Entry<Long, ClienteImpl> entry : chaves) {
			cliente.add(entry.getValue());
		}

		return cliente;
	}

}
