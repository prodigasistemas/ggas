
package br.com.ggas.extrator;
/**
 * FieldSetMapper<T>
 * 
 * @param T
 * 		the T
 * 
 */
public interface FieldSetMapper<T> {

	/**
	 * Map field set.
	 * 
	 * @param fieldSet the field fieldSet
	 * @return the t
	 */
	public T mapFieldSet(Fieldset fieldSet);

}
