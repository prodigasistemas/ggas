
package br.com.ggas.extrator;

import br.com.ggas.batch.Batch;
import br.com.ggas.extrator.jobs.ClienteJob;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.ServiceLocator;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Classe responsável por extrato de batch.
 *
 */
@Component
public class ExtratorBatch implements Batch {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		StringBuilder log = new StringBuilder("Extrator de dados\n\n");
		try {
			ClienteJob job = (ClienteJob) ServiceLocator.getInstancia().getBeanPorID("ClienteJob");
			job.processar(log);

		} catch(Exception e) {
			StackTraceElement[] trace = e.getStackTrace();
			log.append(e.getMessage());
			log.append("\n");
			for (StackTraceElement stackTraceElement : trace) {
				log.append(stackTraceElement);
				log.append("\n");
			}

			throw new GGASException(e);

		}

		return log.toString();
	}

}
