/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 06/06/2013 10:52:46
 @author vpessoa
 */

package br.com.ggas.constantesistema.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
/**
 * Classe responsável por implementar os métodos relacionados as constantes do sistema.
 *
 */
public class ConstanteSistemaImpl extends EntidadeNegocioImpl implements ConstanteSistema {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5892297550291182012L;

	private String nome;

	private String descricao;

	private String valor;

	private Long classe;

	private Modulo modulo;

	private Tabela tabela;

	/**
	 * @return the nome
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * @return the descricao
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return the valor
	 */
	@Override
	public String getValor() {

		return valor;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ParametroSistema
	 * #getValorInteger()
	 */
	@Override
	public Integer getValorInteger() throws NegocioException {
		String valorInteger = this.getValor();
		if (StringUtils.isNotEmpty(valorInteger)) {
			try {
				return Integer.valueOf(valorInteger);
			} catch (NumberFormatException e) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, 
								getChavePrimaria());
			}
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, 
							getChavePrimaria());
		}
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ParametroSistema
	 * #getValorLong()
	 */
	@Override
	public Long getValorLong() throws NegocioException {
		String valorLong = this.getValor();
		if (StringUtils.isNotEmpty(valorLong)) {
			try {
				return Long.valueOf(valorLong);
			} catch (NumberFormatException e) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, getChavePrimaria());
			}
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, getChavePrimaria());
		}
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	@Override
	public void setValor(String valor) {

		this.valor = valor;
	}

	/**
	 * @return the classe
	 */
	@Override
	public Long getClasse() {

		return classe;
	}

	/**
	 * @param classe
	 *            the classe to set
	 */
	@Override
	public void setClasse(Long classe) {

		this.classe = classe;
	}

	/**
	 * @return the modulo
	 */
	@Override
	public Modulo getModulo() {

		return modulo;
	}

	/**
	 * @param modulo
	 *            the modulo to set
	 */
	@Override
	public void setModulo(Modulo modulo) {

		this.modulo = modulo;
	}

	/**
	 * @return the tabela
	 */
	@Override
	public Tabela getTabela() {

		return tabela;
	}

	/**
	 * @param tabela
	 *            the tabela to set
	 */
	@Override
	public void setTabela(Tabela tabela) {

		this.tabela = tabela;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
