/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de  
 * 
 @since 06/06/2013 18:39:50
 @author vpessoa
 */

package br.com.ggas.constantesistema.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPenalidade;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.NumeroUtil;
import br.com.ggas.util.ServiceLocator;
/**
 * Classe responsável pela implementação dos métodos relacionados ao Controlador de Constantes do Sistema. 
 *
 */
public class ControladorConstanteSistemaImpl extends ControladorNegocioImpl implements ControladorConstanteSistema {

	private static final String WHERE = " where ";

	private static final String FROM = " from ";

	private static final Logger LOG = Logger.getLogger(ControladorConstanteSistemaImpl.class);

	private Map<String, ConstanteSistema> contantesCache = new HashMap<>();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.constantesistema.ControladorConstanteSistema#obterConstantePorCodigo(java.lang.String)
	 */
	@Override
	public ConstanteSistema obterConstantePorCodigo(String codigo) {
		
		if(!contantesCache.containsKey(codigo)){
				
			LOG.debug("### obterConstantePorCodigo: " + codigo);
	
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(FROM);
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(WHERE);
			hql.append(" nome = ? ");
	
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setString(0, codigo);
	
			ConstanteSistema constante = (ConstanteSistema) query.uniqueResult();
			contantesCache.put(codigo, constante);
		}
		return contantesCache.get(codigo);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ConstanteSistema.BEAN_ID_CONSTANTE_SISTEMA);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(ConstanteSistema.BEAN_ID_CONSTANTE_SISTEMA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.constantesistema.ControladorConstanteSistema#listarConstantesSistemaPorTabela(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<ConstanteSistema> listarConstantesSistemaPorTabela(Long idTabela) {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" tabela.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idTabela);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.constantesistema.ControladorConstanteSistema#listarConstantesSistemaSemTabela()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<ConstanteSistema> listarConstantesSistemaSemTabela() {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" tabela.chavePrimaria = null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.constantesistema.ControladorConstanteSistema#obterValorConstanteSistemaPorCodigo(java.lang.String)
	 */
	@Override
	public String obterValorConstanteSistemaPorCodigo(String codigo) {
		ConstanteSistema constanteSistema = this.obterConstantePorCodigo(codigo);
		
		if(constanteSistema != null){
			return constanteSistema.getValor();
		}
		return null;		
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.constantesistema.ControladorConstanteSistema#obterConstantePorValor(java.lang.String)
	 */
	@Override
	public ConstanteSistema obterConstantePorValor(String valor) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" valor = :valor ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("valor", valor);

		return (ConstanteSistema) query.uniqueResult();
	}

	private Long obterValorConstanteLongPorCodigo(String chave) throws NegocioException {
		try {
			String valor = obterConstantePorCodigo(chave).getValor();
			if (StringUtils.isNotEmpty(valor)) {
				return Long.valueOf(valor);
			} else {
				throw new NegocioException(
								Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, chave);
			}
		} catch (Exception e) {
			LOG.error(e.getStackTrace(), e);
			throw new NegocioException(
							Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, chave);
		}
	}

	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #getValorConstanteServicoTipoHabilitacao()
	 */
	@Override
	public Long obterValorConstanteServicoTipoHabilitacao() throws NegocioException {
		return obterValorConstanteLongPorCodigo(
						Constantes.C_SERVICO_TIPO_HABILITACAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #getValorConstanteIntegracaoLancamentosContabeis()
	 */
	@Override
	public Long obterValorConstanteIntegracaoLancamentosContabeis() throws NegocioException {
		return obterValorConstanteLongPorCodigo(
						Constantes.C_INTEGRACAO_LANCAMENTOS_CONTABEIS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #getValorConstanteRetiradaAMaior()
	 */
	@Override
	public Long obterValorConstanteRetiradaAMaior() throws NegocioException {
		return obterValorConstanteLongPorCodigo(
						Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #getValorConstanteRetiradaAMenor()
	 */
	@Override
	public Long obterValorConstantePenalidadeRetiradaAMenor() throws NegocioException {
		return obterValorConstanteLongPorCodigo(
						Constantes.C_PENALIDADE_RETIRADA_A_MENOR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #igualValorConstanteRetiradaAMenor(
	 * java.lang.Long)
	 */
	@Override
	public boolean isValorConstantePenalidadeRetiradaAMenor(Long valor) 
					throws NegocioException {
		return valor.equals(obterValorConstanteLongPorCodigo(
						Constantes.C_PENALIDADE_RETIRADA_A_MENOR));
	}
	
	@Override
	public Long obterValorConstanteSubsTributMargemValorAgregado() throws NegocioException {

		return obterValorConstanteLongPorCodigo(Constantes.C_SUBST_TRIBUT_MARGEM_DE_VALOR_AGREGADO);
	}
	
	@Override
	public Long obterValorConstanteSubsTributPrecoMedioPondAConsFinal() throws NegocioException {

		return obterValorConstanteLongPorCodigo(Constantes.C_SUBST_TRIBUT_PRECO_MEDIO_POND_A_CONS_FINAL);
	}
	
	@Override
	public ConstanteSistema obterValorConstanteIntegracaoSituacaoProcessado() {

		return obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);
	}

	@Override
	public ConstanteSistema obterValorConstanteTipoDocumentoFatura() {

		return obterConstantePorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);
	}
	
	@Override
	public boolean isNotIntegracaoSituacaoProcessado(String situacao) {
		
		return !StringUtils.equalsIgnoreCase(situacao,
						obterValorConstanteIntegracaoSituacaoProcessado().getValor());
	}

	@Override
	public boolean isTipoDocumentoFatura(TipoDocumento tipoDocumento) throws NegocioException {
		
		return NumeroUtil.iguais(tipoDocumento.getChavePrimaria(), 
						obterValorConstanteTipoDocumentoFatura().getValorLong());
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteTipoAplicacaoTributoAliquota()
	 */
	@Override
	public ConstanteSistema obterConstanteTipoAplicacaoTributoAliquota() {
		
		return obterConstantePorCodigo(Constantes.C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteTipoAplicacaoTributoAliquotaPercentual()
	 */
	@Override
	public ConstanteSistema obterConstanteTipoAplicacaoTributoAliquotaPercentual() {
		
		return obterConstantePorCodigo(Constantes.C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_PERCENTUAL);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteTipoAplicacaoTributoAliquotaValor()
	 */
	@Override
	public ConstanteSistema obterConstanteTipoAplicacaoTributoAliquotaValor() {
		
		return obterConstantePorCodigo(Constantes.C_TIPO_APLICACAO_TRIBUTO_ALIQUOTA_VALOR);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #isTipoAplicacaoTributoAliquotaPercentual(
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public boolean isTipoAplicacaoTributoAliquotaPercentual(EntidadeConteudo entidadeConteudo) 
					throws NegocioException {
		
		return NumeroUtil.iguais(entidadeConteudo.getChavePrimaria(), 
						obterConstanteTipoAplicacaoTributoAliquotaPercentual().getValorLong());
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #isTipoAplicacaoTributoAliquotaValor(
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public boolean isTipoAplicacaoTributoAliquotaValor(EntidadeConteudo entidadeConteudo) 
					throws NegocioException {
		
		return NumeroUtil.iguais(entidadeConteudo.getChavePrimaria(), 
						obterConstanteTipoAplicacaoTributoAliquotaValor().getValorLong());
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteTipoAplicacaoReducaoBaseCalculo()
	 */
	@Override
	public ConstanteSistema obterConstanteTipoAplicacaoReducaoBaseCalculo() {
		
		return obterConstantePorCodigo(Constantes.C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteTipoAplicacaoReducaoBaseCalculoPercentual()
	 */
	@Override
	public ConstanteSistema obterConstanteTipoAplicacaoReducaoBaseCalculoPercentual() {
		
		return obterConstantePorCodigo(Constantes.C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_PERCENTUAL);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteTipoAplicacaoReducaoBaseCalculoFator()
	 */
	@Override
	public ConstanteSistema obterConstanteTipoAplicacaoReducaoBaseCalculoFator() {
		
		return obterConstantePorCodigo(Constantes.C_TIPO_APLICACAO_REDUCAO_BASE_CALCULO_FATOR);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #isTipoAplicacaoReducaoBaseCalculoPercentual(
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public boolean isTipoAplicacaoReducaoBaseCalculoPercentual(
					EntidadeConteudo entidadeConteudo) throws NegocioException {
		
		return NumeroUtil.iguais(entidadeConteudo.getChavePrimaria(), 
						obterConstanteTipoAplicacaoReducaoBaseCalculoPercentual().getValorLong());
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #isTipoAplicacaoReducaoBaseCalculoFator(
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public boolean isTipoAplicacaoReducaoBaseCalculoFator(
					EntidadeConteudo entidadeConteudo) throws NegocioException {
		
		return NumeroUtil.iguais(entidadeConteudo.getChavePrimaria(), 
						obterConstanteTipoAplicacaoReducaoBaseCalculoFator().getValorLong());
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteIcms()
	 */
	@Override
	public ConstanteSistema obterConstanteIcms() {
		
		return obterConstantePorCodigo(Constantes.C_ICMS);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #isIcms(
	 * br.com.ggas.faturamento.tributo.Tributo)
	 */
	@Override
	public boolean isIcms(Tributo tributo) throws NegocioException {
		
		return NumeroUtil.iguais(tributo.getChavePrimaria(), 
						obterConstanteIcms().getValorLong());
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema#obterConstanteTipoVigenciaSubstituicaoTributaria()
	 */
	@Override
	public ConstanteSistema obterConstanteTipoVigenciaSubstituicaoTributaria() {
		return obterConstantePorCodigo(Constantes.TIPO_VIGENCIA_SUBSTITUICAO_TRIBUTARIA);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteTipoVigenciaSubstituicaoTributariaDataUltimoDiaConsumo()
	 */
	@Override
	public ConstanteSistema obterConstanteTipoVigenciaSubstituicaoTributariaDataUltimoDiaConsumo() {
		return obterConstantePorCodigo(Constantes.VIGENCIA_SUBSTITUICAO_TRIBUTARIA_DATA_ULTIMO_DIA_CONSUMO);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteContratoPenalidadeTakeOrPay()
	 */
	@Override
	public ConstanteSistema obterConstanteContratoPenalidadeTakeOrPay() {
		return obterConstantePorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #isContratoPenalidadeTakeOrPay(
	 * br.com.ggas.contrato.contrato.ContratoPenalidade)
	 */
	@Override
	public boolean isContratoPenalidadeTakeOrPay(
					ContratoPenalidade cope) throws NegocioException {
		return NumeroUtil.iguais(cope.getPenalidade().getChavePrimaria(), 
						obterConstanteContratoPenalidadeTakeOrPay().getValorLong());
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteContratoPenalidadeRetiradaMaior()
	 */
	@Override
	public ConstanteSistema obterConstanteContratoPenalidadeRetiradaMaior() {
		return obterConstantePorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteContratoPenalidadeRetiradaMenor()
	 */
	@Override
	public ConstanteSistema obterConstanteContratoPenalidadeRetiradaMenor() {
		return obterConstantePorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteContratoCadastroBens()
	 */
	@Override
	public ConstanteSistema obterConstanteContratoCadastroBens() {
		return obterConstantePorCodigo(Constantes.C_INTEGRACAO_CADASTRO_BENS);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #obterConstanteTipoModeloContratoComplementar()
	 */
	@Override
	public ConstanteSistema obterConstanteTipoModeloContratoComplementar() {
		
		return obterConstantePorCodigo(Constantes.C_TIPO_MODELO_CONTRATO_COMPLEMENTAR);
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #isContratoTipoModeloContratoComplementar(
	 * br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public boolean isContratoTipoModeloContratoComplementar(
			EntidadeConteudo enco) throws NegocioException {
		
		return NumeroUtil.iguais(enco.getChavePrimaria(), 
				obterConstanteTipoModeloContratoComplementar().getValorLong());
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.constantesistema
	 * .ControladorConstanteSistema
	 * #isContratoPenalidadeRetiradaMaiorMenor(
	 * br.com.ggas.contrato.contrato.ContratoPenalidade)
	 */
	@Override
	public boolean isContratoPenalidadeRetiradaMaiorMenor(
					ContratoPenalidade cope) throws NegocioException {
		return NumeroUtil.igualAlgum(cope.getPenalidade().getChavePrimaria(), 
						obterConstanteContratoPenalidadeRetiradaMaior().getValorLong(),
						obterConstanteContratoPenalidadeRetiradaMenor().getValorLong());
	}

}
