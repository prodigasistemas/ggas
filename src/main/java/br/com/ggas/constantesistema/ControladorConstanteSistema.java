/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 06/06/2013 17:15:07
 @author vpessoa
 */

package br.com.ggas.constantesistema;

import java.util.List;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.contrato.contrato.ContratoPenalidade;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * The Interface ControladorConstanteSistema.
 */
public interface ControladorConstanteSistema extends ControladorNegocio {

	/** The bean id controlador constante sistema. */
	String BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA = "controladorConstanteSistema";

	/**
	 * Obter constante por codigo.
	 *
	 * @param codigo the codigo
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstantePorCodigo(String codigo);

	/**
	 * Listar constantes sistema por tabela.
	 *
	 * @param idTabela the id tabela
	 * @return the list
	 */
	List<ConstanteSistema> listarConstantesSistemaPorTabela(Long idTabela);

	/**
	 * Listar constantes sistema sem tabela.
	 *
	 * @return the list
	 */
	List<ConstanteSistema> listarConstantesSistemaSemTabela();

	/**
	 * Obter valor constante sistema por codigo.
	 *
	 * @param codigo the codigo
	 * @return the string
	 */
	String obterValorConstanteSistemaPorCodigo(String codigo);

	/**
	 * Obter constante por valor.
	 *
	 * @param valor the valor
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstantePorValor(String valor);

	/**
	 * Obter valor constante servico tipo habilitacao.
	 *
	 * @return the long
	 * @throws NegocioException the negocio exception
	 */
	Long obterValorConstanteServicoTipoHabilitacao() throws NegocioException;

	/**
	 * Obter valor constante integracao lancamentos contabeis.
	 *
	 * @return the long
	 * @throws NegocioException the negocio exception
	 */
	Long obterValorConstanteIntegracaoLancamentosContabeis() throws NegocioException;
	
	/**
	 * Obter valor constante retirada a maior.
	 *
	 * @return the long
	 * @throws NegocioException the negocio exception
	 */
	Long obterValorConstanteRetiradaAMaior() throws NegocioException;

	/**
	 * Obter valor constante penalidade retirada a menor.
	 *
	 * @return the long
	 * @throws NegocioException the negocio exception
	 */
	Long obterValorConstantePenalidadeRetiradaAMenor() throws NegocioException;
	
	/**
	 * Checks if is valor constante penalidade retirada a menor.
	 *
	 * @param valor the valor
	 * @return true, if is valor constante penalidade retirada a menor
	 * @throws NegocioException the negocio exception
	 */
	boolean isValorConstantePenalidadeRetiradaAMenor(Long valor) throws NegocioException;
	
	/**
	 * Obter valor constante subs tribut margem valor agregado.
	 *
	 * @return the long
	 * @throws NegocioException the negocio exception
	 */
	Long obterValorConstanteSubsTributMargemValorAgregado() throws NegocioException;
	
	/**
	 * Obter valor constante subs tribut preco medio pond a cons final.
	 *
	 * @return the long
	 * @throws NegocioException the negocio exception
	 */
	Long obterValorConstanteSubsTributPrecoMedioPondAConsFinal() throws NegocioException;

	/**
	 * Obter valor constante integracao situacao processado.
	 *
	 * @return the constante sistema
	 * @throws NegocioException the negocio exception
	 */
	ConstanteSistema obterValorConstanteIntegracaoSituacaoProcessado() throws NegocioException;
	
	/**
	 * Obter valor constante tipo documento fatura.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterValorConstanteTipoDocumentoFatura();

	/**
	 * Obter constante tipo aplicacao tributo aliquota.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteTipoAplicacaoTributoAliquota();

	/**
	 * Checks if is not integracao situacao processado.
	 *
	 * @param situacao the situacao
	 * @return true, if is not integracao situacao processado
	 */
	boolean isNotIntegracaoSituacaoProcessado(String situacao);

	/**
	 * Checks if is tipo documento fatura.
	 *
	 * @param tipoDocumento the tipo documento
	 * @return true, if is tipo documento fatura
	 * @throws NegocioException the negocio exception
	 */
	boolean isTipoDocumentoFatura(TipoDocumento tipoDocumento) throws NegocioException;

	/**
	 * Checks if is tipo aplicacao tributo aliquota valor.
	 *
	 * @param entidadeConteudo the entidade conteudo
	 * @return true, if is tipo aplicacao tributo aliquota valor
	 * @throws NegocioException the negocio exception
	 */
	boolean isTipoAplicacaoTributoAliquotaValor(EntidadeConteudo entidadeConteudo) throws NegocioException;

	/**
	 * Checks if is tipo aplicacao tributo aliquota percentual.
	 *
	 * @param entidadeConteudo the entidade conteudo
	 * @return true, if is tipo aplicacao tributo aliquota percentual
	 * @throws NegocioException the negocio exception
	 */
	boolean isTipoAplicacaoTributoAliquotaPercentual(EntidadeConteudo entidadeConteudo) throws NegocioException;

	/**
	 * Obter constante tipo aplicacao tributo aliquota valor.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteTipoAplicacaoTributoAliquotaValor();

	/**
	 * Obter constante tipo aplicacao tributo aliquota percentual.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteTipoAplicacaoTributoAliquotaPercentual();

	/**
	 * Checks if is tipo aplicacao reducao base calculo fator.
	 *
	 * @param entidadeConteudo the entidade conteudo
	 * @return true, if is tipo aplicacao reducao base calculo fator
	 * @throws NegocioException the negocio exception
	 */
	boolean isTipoAplicacaoReducaoBaseCalculoFator(EntidadeConteudo entidadeConteudo) throws NegocioException;

	/**
	 * Checks if is tipo aplicacao reducao base calculo percentual.
	 *
	 * @param entidadeConteudo the entidade conteudo
	 * @return true, if is tipo aplicacao reducao base calculo percentual
	 * @throws NegocioException the negocio exception
	 */
	boolean isTipoAplicacaoReducaoBaseCalculoPercentual(EntidadeConteudo entidadeConteudo) throws NegocioException;

	/**
	 * Obter constante tipo aplicacao reducao base calculo fator.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteTipoAplicacaoReducaoBaseCalculoFator();

	/**
	 * Obter constante tipo aplicacao reducao base calculo percentual.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteTipoAplicacaoReducaoBaseCalculoPercentual();

	/**
	 * Obter constante tipo aplicacao reducao base calculo.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteTipoAplicacaoReducaoBaseCalculo();

	/**
	 * Obter constante icms.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteIcms();

	/**
	 * Checks if is icms.
	 *
	 * @param tributo the tributo
	 * @return true, if is icms
	 * @throws NegocioException the negocio exception
	 */
	boolean isIcms(Tributo tributo) throws NegocioException;

	/**
	 * Obter constante tipo vigencia substituicao tributaria.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteTipoVigenciaSubstituicaoTributaria();

	/**
	 * Obter constante tipo vigencia substituicao 
	 * tributaria data ultimo dia consumo.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteTipoVigenciaSubstituicaoTributariaDataUltimoDiaConsumo();

	/**
	 * Obter constante contrato penalidade take or pay.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteContratoPenalidadeTakeOrPay();

	/**
	 * Checks if is contrato penalidade take or pay.
	 *
	 * @param cope the cope
	 * @return true, if is contrato penalidade take or pay
	 * @throws NegocioException the negocio exception
	 */
	boolean isContratoPenalidadeTakeOrPay(ContratoPenalidade cope) 
					throws NegocioException;

	/**
	 * Checks if is contrato penalidade retirada maior menor.
	 *
	 * @param cope the cope
	 * @return true, if is contrato penalidade retirada maior menor
	 * @throws NegocioException the negocio exception
	 */
	boolean isContratoPenalidadeRetiradaMaiorMenor(
					ContratoPenalidade cope) throws NegocioException;

	/**
	 * Obter constante contrato penalidade retirada menor.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteContratoPenalidadeRetiradaMenor();

	/**
	 * Obter constante contrato penalidade retirada maior.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteContratoPenalidadeRetiradaMaior();

	/**
	 * Obter constante contrato cadastro bens.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteContratoCadastroBens();

	/**
	 * Obter constante tipo modelo contrato complementar.
	 *
	 * @return the constante sistema
	 */
	ConstanteSistema obterConstanteTipoModeloContratoComplementar();

	/**
	 * Checks if is contrato tipo modelo contrato complementar.
	 *
	 * @param enco the enco
	 * @return true, if is contrato tipo modelo contrato complementar
	 * @throws NegocioException the negocio exception
	 */
	boolean isContratoTipoModeloContratoComplementar(
			EntidadeConteudo enco) throws NegocioException;

}
