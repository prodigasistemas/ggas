/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 06/06/2013 10:49:43
 @author vpessoa
 */

package br.com.ggas.constantesistema;

import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados as constantes do sistema
 *
 */
public interface ConstanteSistema extends EntidadeNegocio {

	String BEAN_ID_CONSTANTE_SISTEMA = "constanteSistema";
	
	String CONSTANTES = "CONSTANTES";

	String ENTIDADE_ROTULO_CONSTANTE_SISTEMA = "Constante do sistema";

	String ENTIDADE_ROTULO_CONSTANTES_SISTEMA = "Constante(s) do sistema";

	/**
	 * @return the nome
	 */
	public String getNome();

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome);

	/**
	 * @return the descricao
	 */
	public String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	public void setDescricao(String descricao);

	/**
	 * @return the valor
	 */
	public String getValor();

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(String valor);

	/**
	 * @return the classe
	 */
	public Long getClasse();

	/**
	 * @param classe
	 *            the classe to set
	 */
	public void setClasse(Long classe);

	/**
	 * @return the modulo
	 */
	public Modulo getModulo();

	/**
	 * @param modulo
	 *            the modulo to set
	 */
	public void setModulo(Modulo modulo);

	/**
	 * @return the tabela
	 */
	public Tabela getTabela();

	/**
	 * @param tabela
	 *            the tabela to set
	 */
	public void setTabela(Tabela tabela);

	/**
	 * @return Long - Verifica se o valor não é vazio, caso não seja converte em Long e o retorna.
	 * @throws NegocioException
	 */
	Long getValorLong() throws NegocioException;

	/**
	 * Gets the valor integer.
	 *
	 * @return the valor integer
	 * @throws NegocioException the negocio exception
	 */
	Integer getValorInteger() throws NegocioException;
}
