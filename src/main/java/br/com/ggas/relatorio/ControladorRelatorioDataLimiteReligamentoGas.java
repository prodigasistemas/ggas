package br.com.ggas.relatorio;

import java.util.Date;

import br.com.ggas.geral.exception.GGASException;
/**
 * Controlador reponsável pelo relatório para data limite de religamento gás 
 * 
 * */
public interface ControladorRelatorioDataLimiteReligamentoGas {
	
	String BEAN_ID_CONTROLADOR_RELATORIO_DATA_LIMITE_RELIGAMENTO_GAS = "controladorServicoAutorizavao";

	String ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO = "ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO";

	String ERRO_NEGOCIO_RELATORIO_DATA_LIMITE_RELIGAMENTO_GAS_NENHUM_REGISTRO_ENCONTRADO 
	= "ERRO_NEGOCIO_RELATORIO_DATA_LIMITE_RELIGAMENTO_GAS_NENHUM_REGISTRO_ENCONTRADO";

	String RELATORIO_DATA_LIMITE_RELIGAMENTO_GAS_JASPER = "relatorioDataLimiteReligamentoGas.jasper";

	/**
	 * Método reponsável por gerar o relatório.
	 * 
	 * @param dataIncial - {@link Date}
	 * @param exibirFiltros - {@link String}
	 * @param tipoRelatorio - {@link String}
	 * @param dataFinal - {@link Date}
	 * 
	 * @return byte[] - relatório
	 * @throws GGASException - {@link GGASException}
	 */
	byte[] gerarRelatorio(Date dataIncial, Date dataFinal, String exibirFiltros, String tipoRelatorio) throws GGASException;
}
