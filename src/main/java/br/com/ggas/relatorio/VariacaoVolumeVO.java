/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.log4j.Logger;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * 
 * @author pedro
 * Classe responsável pela transferencia de dados.
 */
public class VariacaoVolumeVO implements Serializable {

	private static final Logger LOG = Logger.getLogger(VariacaoVolumeVO.class);

	private static final long serialVersionUID = -2131067729112754272L;

	private String imovel;

	private Long chavePontoConsumo;

	private String pontoConsumo;

	private String segmento;

	private Integer diaReferencia;

	private BigDecimal volumeReferencia;

	private Integer anoMesReferencia;

	private Integer diaReferenciaAnterior;

	private BigDecimal volumeReferenciaAnterior;

	private Integer anoMesReferenciaAnterior;

	private BigDecimal variacao;

	/**
	 * Instantiates a new variacao volume vo.
	 */
	public VariacaoVolumeVO() {

		super();
		imovel = "";
		chavePontoConsumo = null;
		pontoConsumo = "";
		segmento = "";
		diaReferencia = null;
		volumeReferencia = null;
		anoMesReferencia = null;
		diaReferenciaAnterior = null;
		volumeReferenciaAnterior = null;
		anoMesReferenciaAnterior = null;
		variacao = null;
	}

	/**
	 * Instantiates a new variacao volume vo.
	 * 
	 * @param pChavePontoConsumo
	 *            the chave ponto consumo
	 * @param pImovel
	 *            the imovel
	 * @param pPontoConsumo
	 *            the ponto consumo
	 * @param pSegmento
	 *            the segmento
	 * @param pDiaReferencia
	 *            the dia referencia
	 * @param pVolRef
	 *            the vol ref
	 * @param pAnoMesRef
	 *            the ano mes ref
	 * @param pdiaReferenciaAnterior
	 *            the pdia referencia anterior
	 * @param pVolRefAnterior
	 *            the vol ref anterior
	 * @param pAnoMesRefAnterior
	 *            the ano mes ref anterior
	 * @param pVariacao
	 *            the variacao
	 */
	public VariacaoVolumeVO(Long pChavePontoConsumo, String pImovel, String pPontoConsumo, String pSegmento, Number pDiaReferencia,
							Number pVolRef, Number pAnoMesRef, Number pdiaReferenciaAnterior, Number pVolRefAnterior,
							Number pAnoMesRefAnterior, Number pVariacao) {

		imovel = pImovel;
		chavePontoConsumo = pChavePontoConsumo;

		if ((imovel != null) && (!"".equals(imovel.trim()))) {
			pontoConsumo = imovel.concat(" - ").concat(pPontoConsumo);
		} else {
			pontoConsumo = pPontoConsumo;
		}

		BigDecimal pVolRefAux = new BigDecimal(0);
		if(pVolRef != null) {
			pVolRefAux = new BigDecimal(pVolRef.longValue());
		}
		BigDecimal pVariacaoAux = new BigDecimal(0);
		if(pVariacao != null) {
			pVariacaoAux = new BigDecimal(pVariacao.longValue());
		}
		BigDecimal pVolRefAnteriorAux = new BigDecimal(0);
		if(pVolRefAnterior != null) {
			pVolRefAnteriorAux = new BigDecimal(pVolRefAnterior.longValue());
		}

		segmento = pSegmento;
		diaReferencia = pDiaReferencia.intValue();
		volumeReferencia = pVolRefAux;
		anoMesReferencia = pAnoMesRef.intValue();
		diaReferenciaAnterior = pdiaReferenciaAnterior.intValue();
		volumeReferenciaAnterior = pVolRefAnteriorAux;
		anoMesReferenciaAnterior = pAnoMesRefAnterior.intValue();
		variacao = pVariacaoAux;
	}

	public String getImovel() {

		return imovel;
	}

	public void setImovel(String imovel) {

		this.imovel = imovel;
	}

	public Long getChavePontoConsumo() {

		return chavePontoConsumo;
	}

	public void setChavePontoConsumo(Long chavePontoConsumo) {

		this.chavePontoConsumo = chavePontoConsumo;
	}

	public String getPontoConsumo() {

		return pontoConsumo;
	}

	public void setPontoConsumo(String pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	public Integer getDiaReferencia() {

		return diaReferencia;
	}

	public void setDiaReferencia(Integer diaReferencia) {

		this.diaReferencia = diaReferencia;
	}

	public BigDecimal getVolumeReferencia() {

		return volumeReferencia;
	}

	public void setVolumeReferencia(BigDecimal volumeReferencia) {

		this.volumeReferencia = volumeReferencia;
	}

	public Integer getAnoMesReferencia() {

		return anoMesReferencia;
	}

	public void setAnoMesReferencia(Integer anoMesReferencia) {

		this.anoMesReferencia = anoMesReferencia;
	}

	public Integer getDiaReferenciaAnterior() {

		return diaReferenciaAnterior;
	}

	public void setDiaReferenciaAnterior(Integer diaReferenciaAnterior) {

		this.diaReferenciaAnterior = diaReferenciaAnterior;
	}

	public BigDecimal getVolumeReferenciaAnterior() {

		return volumeReferenciaAnterior;
	}

	public void setVolumeReferenciaAnterior(BigDecimal volumeReferenciaAnterior) {

		this.volumeReferenciaAnterior = volumeReferenciaAnterior;
	}

	public Integer getAnoMesReferenciaAnterior() {

		return anoMesReferenciaAnterior;
	}

	public void setAnoMesReferenciaAnterior(Integer anoMesReferenciaAnterior) {

		this.anoMesReferenciaAnterior = anoMesReferenciaAnterior;
	}

	public BigDecimal getVariacao() {

		return variacao;
	}

	public void setVariacao(BigDecimal variacao) {

		this.variacao = variacao;
	}

	public String getVolumeReferenciaString() {

		String retorno = "";
		retorno = Util.converterCampoValorDecimalParaString("", volumeReferencia, Constantes.LOCALE_PADRAO);
		return retorno;
	}

	public String getVolumeReferenciaAnteriorString() {

		String retorno = "";
		retorno = Util.converterCampoValorDecimalParaString("", volumeReferenciaAnterior, Constantes.LOCALE_PADRAO);
		return retorno;
	}

	public String getVariacaoString() {

		String retorno = "";
		retorno = Util.converterCampoValorDecimalParaString("", variacao, Constantes.LOCALE_PADRAO);
		return retorno;
	}

	public String getAnoMesReferenciaString() {

		return Util.formatarAnoMes(anoMesReferencia);
	}

	public String getAnoMesReferenciaAnteriorString() {

		return Util.formatarAnoMes(anoMesReferenciaAnterior);
	}

}
