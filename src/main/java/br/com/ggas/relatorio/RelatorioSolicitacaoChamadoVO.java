package br.com.ggas.relatorio;

public class RelatorioSolicitacaoChamadoVO {

	private String mes;
	private String dias1 = "0";
	private String dias2 = "0";
	private String dias3 = "0";
	private String diasMaior3 = "0";

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getDias1() {
		return dias1;
	}

	public void setDias1(String dias1) {
		this.dias1 = dias1;
	}

	public String getDias2() {
		return dias2;
	}

	public void setDias2(String dias2) {
		this.dias2 = dias2;
	}

	public String getDias3() {
		return dias3;
	}

	public void setDias3(String dias3) {
		this.dias3 = dias3;
	}

	public String getDiasMaior3() {
		return diasMaior3;
	}

	public void setDiasMaior3(String diasMaior3) {
		this.diasMaior3 = diasMaior3;
	}

}
