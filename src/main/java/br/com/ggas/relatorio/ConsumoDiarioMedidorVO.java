/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.relatorio;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
/**
 * Classe reponsável por armazenar as informações do Consumo diário do medidor
 * 
 *
 */
public class ConsumoDiarioMedidorVO implements Serializable, Comparable<ConsumoDiarioMedidorVO> {
	
	private static final long serialVersionUID = 2202524331796816236L;
	
	private Long idMedidor;
	private String numeroSerie;
	private List<ConsumoDiarioVO> listaConsumoDiario;

	public Long getIdMedidor() {
		return idMedidor;
	}

	public void setIdMedidor(Long idMedidor) {
		this.idMedidor = idMedidor;
	}

	public String getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public List<ConsumoDiarioVO> getListaConsumoDiario() {
		return listaConsumoDiario;
	}

	public void setListaConsumoDiario(List<ConsumoDiarioVO> listaConsumoDiario) {
		this.listaConsumoDiario = listaConsumoDiario;
	}

	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(this.idMedidor);
		return builder.hashCode();
	}

	@Override
	public boolean equals(Object obj) {		
		if (!(obj instanceof ConsumoDiarioMedidorVO)) {
			return false;
		}
		ConsumoDiarioMedidorVO otherObj  = (ConsumoDiarioMedidorVO) obj;
		EqualsBuilder builder = new EqualsBuilder();
		builder.append(this.idMedidor, otherObj.getIdMedidor());
		return builder.isEquals();
	}

	@Override
	public int compareTo(ConsumoDiarioMedidorVO o) {
		if (o == null) {
			return 1;
		}
		
		CompareToBuilder compareTo = new CompareToBuilder();
		compareTo.append(this.idMedidor, o.getIdMedidor());
		
		return compareTo.toComparison();
	}
}
