/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.medicao.leitura;
/**
 * Classe responsável pela transferencia de dados relacionados 
 * aos registro de de anormalidades de leitura utilizado no relatório.
 */
public class DadosRegistrarLeiturasAnormalidades {

	private String matriculaFuncionario;

	private String dataLeitura;

	private String matriculaImovel;

	private String pontoConsumo;

	private String medidor;

	private String leitura;

	private String codigoAnormalidade;

	private String indicadorConfirmacao;

	private String errosTxt;

	private String grupoFaturamento;

	private String anoMesReferencia;

	private String ciclo;

	private String rota;
	
	private String dataInstalacaoMedidor;
	
	private String dataAtivacaoMedidor;

	/**
	 * Instantiates a new dados registrar leituras anormalidades.
	 */
	public DadosRegistrarLeiturasAnormalidades() {

		super();
	}

	/**
	 * @return the matriculaFuncionario
	 */
	public String getMatriculaFuncionario() {

		return matriculaFuncionario;
	}

	/**
	 * @param matriculaFuncionario
	 *            the matriculaFuncionario to set
	 */
	public void setMatriculaFuncionario(String matriculaFuncionario) {

		this.matriculaFuncionario = matriculaFuncionario;
	}

	/**
	 * @return the dataLeitura
	 */
	public String getDataLeitura() {

		return dataLeitura;
	}

	/**
	 * @param dataLeitura
	 *            the dataLeitura to set
	 */
	public void setDataLeitura(String dataLeitura) {

		this.dataLeitura = dataLeitura;
	}

	/**
	 * @return the matriculaImovel
	 */
	public String getMatriculaImovel() {

		return matriculaImovel;
	}

	/**
	 * @param matriculaImovel
	 *            the matriculaImovel to set
	 */
	public void setMatriculaImovel(String matriculaImovel) {

		this.matriculaImovel = matriculaImovel;
	}

	/**
	 * @return the pontoConsumo
	 */
	public String getPontoConsumo() {

		return pontoConsumo;
	}

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	public void setPontoConsumo(String pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return the medidor
	 */
	public String getMedidor() {

		return medidor;
	}

	/**
	 * @param medidor
	 *            the medidor to set
	 */
	public void setMedidor(String medidor) {

		this.medidor = medidor;
	}

	/**
	 * @return the leitura
	 */
	public String getLeitura() {

		return leitura;
	}

	/**
	 * @param leitura
	 *            the leitura to set
	 */
	public void setLeitura(String leitura) {

		this.leitura = leitura;
	}

	/**
	 * @return the codigoAnormalidade
	 */
	public String getCodigoAnormalidade() {

		return codigoAnormalidade;
	}

	/**
	 * @param codigoAnormalidade
	 *            the codigoAnormalidade to set
	 */
	public void setCodigoAnormalidade(String codigoAnormalidade) {

		this.codigoAnormalidade = codigoAnormalidade;
	}

	/**
	 * @return the indicadorConfirmacao
	 */
	public String getIndicadorConfirmacao() {

		return indicadorConfirmacao;
	}

	/**
	 * @param indicadorConfirmacao
	 *            the indicadorConfirmacao to set
	 */
	public void setIndicadorConfirmacao(String indicadorConfirmacao) {

		this.indicadorConfirmacao = indicadorConfirmacao;
	}

	/**
	 * @return the errosTxt
	 */
	public String getErrosTxt() {

		return errosTxt;
	}

	/**
	 * @param errosTxt
	 *            the errosTxt to set
	 */
	public void setErrosTxt(String errosTxt) {

		this.errosTxt = errosTxt;
	}

	/**
	 * @return the grupoFaturamento
	 */
	public String getGrupoFaturamento() {

		return grupoFaturamento;
	}

	/**
	 * @param grupoFaturamento
	 *            the grupoFaturamento to set
	 */
	public void setGrupoFaturamento(String grupoFaturamento) {

		this.grupoFaturamento = grupoFaturamento;
	}

	/**
	 * @return the anoMesReferencia
	 */
	public String getAnoMesReferencia() {

		return anoMesReferencia;
	}

	/**
	 * @param anoMesReferencia
	 *            the anoMesReferencia to set
	 */
	public void setAnoMesReferencia(String anoMesReferencia) {

		this.anoMesReferencia = anoMesReferencia;
	}

	/**
	 * @return the ciclo
	 */
	public String getCiclo() {

		return ciclo;
	}

	/**
	 * @param ciclo
	 *            the ciclo to set
	 */
	public void setCiclo(String ciclo) {

		this.ciclo = ciclo;
	}

	/**
	 * @return the rota
	 */
	public String getRota() {

		return rota;
	}

	/**
	 * @param rota
	 *            the rota to set
	 */
	public void setRota(String rota) {

		this.rota = rota;
	}

	public String getDataInstalacaoMedidor() {
		return dataInstalacaoMedidor;
	}

	public void setDataInstalacaoMedidor(String dataInstalacaoMedidor) {
		this.dataInstalacaoMedidor = dataInstalacaoMedidor;
	}

	public String getDataAtivacaoMedidor() {
		return dataAtivacaoMedidor;
	}

	public void setDataAtivacaoMedidor(String dataAtivacaoMedidor) {
		this.dataAtivacaoMedidor = dataAtivacaoMedidor;
	}

}
