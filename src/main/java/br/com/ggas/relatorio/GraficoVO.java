/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio;

public class GraficoVO {

	@SuppressWarnings("rawtypes")
	private Comparable chaveLinha;

	@SuppressWarnings("rawtypes")
	private Comparable chaveColuna;

	private Number valor;

	/**
	 * Instantiates a new grafico vo.
	 */
	public GraficoVO() {

		super();
	}

	/**
	 * Instantiates a new grafico vo.
	 * 
	 * @param pChaveLinha
	 *            the chave linha
	 * @param pChaveColuna
	 *            the chave coluna
	 * @param pValor
	 *            the valor
	 */
	@SuppressWarnings("rawtypes")
	public GraficoVO(Comparable pChaveLinha, Comparable pChaveColuna, Number pValor) {

		this.chaveLinha = pChaveLinha;
		this.chaveColuna = pChaveColuna;
		this.valor = pValor;
	}

	@SuppressWarnings("rawtypes")
	public Comparable getChaveLinha() {

		return chaveLinha;
	}

	@SuppressWarnings("rawtypes")
	public void setChaveLinha(Comparable chaveLinha) {

		this.chaveLinha = chaveLinha;
	}

	@SuppressWarnings("rawtypes")
	public Comparable getChaveColuna() {

		return chaveColuna;
	}

	@SuppressWarnings("rawtypes")
	public void setChaveColuna(Comparable chaveColuna) {

		this.chaveColuna = chaveColuna;
	}

	public Number getValor() {

		return valor;
	}

	public void setValor(Number valor) {

		this.valor = valor;
	}

}
