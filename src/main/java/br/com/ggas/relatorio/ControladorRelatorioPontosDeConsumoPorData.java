package br.com.ggas.relatorio;

import java.util.Date;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.exception.GGASException;
/**
 * Controlador responsável pelo relatório de pontos de consumo por data
 */
public interface ControladorRelatorioPontosDeConsumoPorData {

	String BEAN_ID_CONTROLADOR_RELATORIO_PONTOS_CONSUMO_POR_DATA = "controladorPontoConsumo";

	String ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO = "ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO";

	String ERRO_NEGOCIO_RELATORIO_PONTOS_DE_COMSUMO_NENHUM_REGISTRO_ENCONTRADO 
	= "ERRO_NEGOCIO_RELATORIO_PONTOS_DE_COMSUMO_NENHUM_REGISTRO_ENCONTRADO";

	String RELATORIO_PONTO_CONSUMO_POR_DATA_JASPER = "relatorioPontoConsumoPorData.jasper";

	/**
	 * Método reponsável por gerar o relatório.
	 * 
	 * @param dataIncial 
	 * @param exibirFiltros
	 * @param tipoRelatorio
	 * @param dataFinal
	 *            
	 * @return the byte[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] gerarRelatorio(Date dataIncial, Date dataFinal, String exibirFiltros, String tipoRelatorio, Segmento segmento, Boolean isAtivoPrimeiraVez) throws GGASException;

}
