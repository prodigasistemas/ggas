/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.exportacaodados.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.relatorio.exportacaodados.Consulta;
import br.com.ggas.relatorio.exportacaodados.ConsultaParametro;
import br.com.ggas.util.Constantes;

class ConsultaImpl extends EntidadeNegocioImpl implements Consulta {

	private static final long serialVersionUID = -6286336458824647239L;

	private String nome;

	private Modulo modulo;

	private String sql;

	private String descricao;

	private Operacao operacao;

	private Collection<ConsultaParametro> listaParametro = new HashSet<ConsultaParametro>();

	/**
	 * @return the nome
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/**
	 * @param nome
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * @return the modulo
	 */
	@Override
	public Modulo getModulo() {

		return modulo;
	}

	/**
	 * @param modulo
	 */
	@Override
	public void setModulo(Modulo modulo) {

		this.modulo = modulo;
	}

	/**
	 * @return the sql
	 */
	@Override
	public String getSql() {

		return sql;
	}

	/**
	 * @param sql
	 */
	@Override
	public void setSql(String sql) {

		this.sql = sql;
	}

	/**
	 * @return the descricao
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return the operacao
	 */
	@Override
	public Operacao getOperacao() {

		return operacao;
	}

	/**
	 * @param operacao
	 */
	@Override
	public void setOperacao(Operacao operacao) {

		this.operacao = operacao;
	}

	/**
	 * @return the listaParametro
	 */
	@Override
	public Collection<ConsultaParametro> getListaParametro() {

		return listaParametro;
	}

	/**
	 * @param listaParametro
	 */
	@Override
	public void setListaParametro(Collection<ConsultaParametro> listaParametro) {

		this.listaParametro = listaParametro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(this.getNome())) {
			stringBuilder.append("Nome");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.getModulo() == null) {
			stringBuilder.append("Módulo");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(this.getSql())) {
			stringBuilder.append("Sql");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
		}

		return erros;
	}
}
