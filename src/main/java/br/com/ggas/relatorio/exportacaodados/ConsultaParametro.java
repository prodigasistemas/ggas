/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.exportacaodados;

import java.util.ArrayList;
import java.util.List;

import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface ConsultaParametro extends EntidadeNegocio {

	String BEAN_ID_CONSULTA_PARAMETRO = "consultaParametro";

	String CODIGO = "CONSULTA_PARAMETRO_CODIGO";

	String NOME = "CONSULTA_PARAMETRO_NOME";

	String ENTIDADE_CONTEUDO = "CONSULTA_PARAMETRO_ENTIDADE_CONTEUDO";

	String NUMERO_MAX_CARACTERES = "CONSULTA_PARAMETRO_NUMERO_MAX_CARACTERES";

	String MASCARA = "CONSULTA_PARAMETRO_MASCARA";

	String ORDEM = "CONSULTA_PARAMETRO_ORDEM";

	String FK = "CONSULTA_PARAMETRO_FK";

	String MULTIVALORADO = "CONSULTA_PARAMETRO_MULTIVALORADO";

	String TABELA = "CONSULTA_PARAMETRO_TABELA";

	String CHAVE_ENTIDADE_COMPLEMENTAR = "CONSULTA_PARAMETRO_CHAVE_ENTIDADE_COMPLEMENTAR";

	String IDENTIFICADOR_CODIGO_PARAMETRO = ":";

	String BOLEANO = "Boleano";

	public static enum MascaraParametro {
		DATA_DD_MM_AAAA("d/m/y", "222"), DATA_MM_AAAA("m/y", "221"), DATA_AAAA_MM("y/m", "221"), MONETARIO_REAL("99,99", "220"),
		MONETARIO_DOLLAR("99.99", "220"), CPF("999.999.999-99", "220"), CNPJ("99.999.999/9999-99", "220"), CEP("99999-999", "220");

		private String mascara;

		private String tipo;

		/**
		 * Instantiates a new mascara parametro.
		 * 
		 * @param mascara
		 *            the mascara
		 * @param tipo
		 *            the tipo
		 */
		MascaraParametro(String mascara, String tipo) {

			this.mascara = mascara;
			this.tipo = tipo;
		}

		public String getMascara() {

			return this.mascara;
		}

		public String getTipo() {

			return this.tipo;
		}

		/**
		 * Gets the lista por tipo.
		 * 
		 * @param tipo
		 *            the tipo
		 * @return the lista por tipo
		 */
		public static MascaraParametro[] getListaPorTipo(String tipo) {

			List<MascaraParametro> lista = new ArrayList<MascaraParametro>();
			for (MascaraParametro mascara : MascaraParametro.values()) {
				if(mascara.getTipo().equals(tipo)) {
					lista.add(mascara);
				}
			}
			MascaraParametro[] m = new MascaraParametro[lista.size()];
			for (int i = 0; i < lista.size(); i++) {
				m[i] = lista.get(i);
			}
			return m;
		}

		/*
		 * (non-Javadoc)
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {

			return this.mascara;
		}

	}

	/**
	 * @return the consulta
	 */
	Consulta getConsulta();

	/**
	 * @param consulta
	 */
	void setConsulta(Consulta consulta);

	/**
	 * @return the codigo
	 */
	String getCodigo();

	/**
	 * @param codigo
	 */
	void setCodigo(String codigo);

	/**
	 * @return the nomeCampo
	 */
	String getNomeCampo();

	/**
	 * @param nomeCampo
	 */
	void setNomeCampo(String nomeCampo);

	/**
	 * @return the entidadeConteudo
	 */
	EntidadeConteudo getEntidadeConteudo();

	/**
	 * @param entidadeConteudo
	 */
	void setEntidadeConteudo(EntidadeConteudo entidadeConteudo);

	/**
	 * @return the numeroMaximoCaracteres
	 */
	Integer getNumeroMaximoCaracteres();

	/**
	 * @param numeroMaximoCaracteres
	 */
	void setNumeroMaximoCaracteres(Integer numeroMaximoCaracteres);

	/**
	 * @return the mascara
	 */
	String getMascara();

	/**
	 * @param mascara
	 */
	void setMascara(String mascara);

	/**
	 * @return the ordem
	 */
	Integer getOrdem();

	/**
	 * @param ordem
	 */
	void setOrdem(Integer ordem);

	/**
	 * @return the fk
	 */
	Boolean getFk();

	/**
	 * @param fk
	 */
	void setFk(Boolean fk);

	/**
	 * @return the multivalorado
	 */
	Boolean getMultivalorado();

	/**
	 * @param multivalorado
	 */
	void setMultivalorado(Boolean multivalorado);

	/**
	 * @return the tabela
	 */
	Tabela getTabela();

	/**
	 * @param tabela
	 */
	void setTabela(Tabela tabela);

	/**
	 * @return the chaveEntidadeComplementar
	 */
	Long getChaveEntidadeComplementar();

	/**
	 * @param chaveEntidadeComplementar
	 */
	void setChaveEntidadeComplementar(Long chaveEntidadeComplementar);

}
