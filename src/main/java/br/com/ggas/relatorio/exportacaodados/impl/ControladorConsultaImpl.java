/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.exportacaodados.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.controleacesso.ChaveOperacaoSistema;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.relatorio.exportacaodados.Consulta;
import br.com.ggas.relatorio.exportacaodados.ConsultaParametro;
import br.com.ggas.relatorio.exportacaodados.ConsultaParametro.MascaraParametro;
import br.com.ggas.relatorio.exportacaodados.ControladorConsulta;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

public class ControladorConsultaImpl extends ControladorNegocioImpl implements ControladorConsulta {
	
	private static final Logger LOG = Logger.getLogger(ControladorConsultaImpl.class);
	private static final String[] declaracoesInvalidas = {"UPDATE ", "DELETE ", "DROP ", "ALTER "};

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Consulta.BEAN_ID_CONSULTA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Consulta.BEAN_ID_CONSULTA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl
	 * #preAtualizacao(br.com
	 * .ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Consulta consulta = (Consulta) entidadeNegocio;
		validarConsultaExistente(consulta);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl
	 * #preInsercao(br.com.ggas
	 * .geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Consulta consulta = (Consulta) entidadeNegocio;
		validarConsultaExistente(consulta);
	}

	/**
	 * Validar duplicidade nome modulo.
	 * 
	 * @param consulta
	 *            the consulta
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDuplicidadeNomeModulo(Consulta consulta) throws NegocioException {

		Criteria criteria = getCriteria();
		List<Long> listaConsulta = null;
		// validar para proibir uma consulta com o
		// mesmo nome com o mesmo modulo
		criteria.add(Restrictions.eq("nome", consulta.getNome()).ignoreCase());
		Modulo modulo = consulta.getModulo();
		criteria.add(Restrictions.eq("modulo", modulo));
		listaConsulta = criteria.setProjection(Projections.property("chavePrimaria")).list();
		if (!listaConsulta.isEmpty() && !listaConsulta.contains(consulta.getChavePrimaria())) {
			
			throw new NegocioException(ControladorConsulta.ERRO_NEGOCIO_CONSULTA_EXISTENTE, true);
		}
	}
	

	/**
	 * Validar consulta existente.
	 * 
	 * @param consulta
	 *            the consulta
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl
	 * #validarConsultaExistente
	 * (br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	private void validarConsultaExistente(Consulta consulta) throws NegocioException {

		validarDuplicidadeNomeModulo(consulta);

		for (String string : declaracoesInvalidas) {
			if (consulta.getSql().toUpperCase().indexOf(string) != -1) {
				throw new NegocioException(ERRO_NEGOCIO_CONSULTA_SQL_INVALIDO, true);
			}
		}

		if (consulta.getListaParametro() != null) {
			Set<Integer> ordens = new HashSet<Integer>();
			for (ConsultaParametro cp : Util.ordenarColecaoPorAtributo(consulta.getListaParametro(), "ordem", true)) {
				if (cp.getOrdem() != null && ordens.contains(cp.getOrdem())) {
					throw new NegocioException(ERRO_NEGOCIO_CONSULTA_PARAMETRO_ORDEM_EXISTENTE, true);
				} else {
					ordens.add(cp.getOrdem());
				}

				if (consulta.getSql().toUpperCase().indexOf(cp.getCodigo().toUpperCase()) == -1) {
					throw new NegocioException(ERRO_NEGOCIO_CONSULTA_PARAMETRO_CODIGO_NAO_LOCALIZADO_SQL, true);
				}
			}
		}

		// o prefixo definido para identificar um
		// parametro na declaração SQL
		String paramPrefixo = "{";
		// o sufixo definido para identificar um
		// parametro na declaração SQL
		String paramSufixo = "}";
		boolean abriuAspas = false;
		String parametroAtual = "";
		int indiceFinal = 0;
		boolean hasParametro = false;
		for (int i = 0; i < consulta.getSql().length(); i++) {
			if (consulta.getSql().charAt(i) == '\"') {
				abriuAspas = !abriuAspas;
			}
			if (consulta.getSql().charAt(i) == paramPrefixo.charAt(0)) {
				if (!abriuAspas) {
					indiceFinal = consulta.getSql().substring(i).indexOf(paramSufixo) + i;
					parametroAtual = consulta.getSql().substring(i + 1, indiceFinal);
					if (consulta.getListaParametro() != null) {
						for (ConsultaParametro cp : consulta.getListaParametro()) {
							if (cp.getCodigo().equalsIgnoreCase(parametroAtual)) {
								hasParametro = true;
								break;
							}
						}
					}
				}
			} else {
				hasParametro = true;
			}
		}

		if (Boolean.FALSE.equals(hasParametro)) {
			throw new NegocioException(ERRO_NEGOCIO_CONSULTA_PARAMETRO_INEXISTENTE, true);
		}

		try {
			PreparedStatement pstmt = getPreparedStatementExecucaoConsulta(consulta, criarMapDadosParametroTesteExecucao(consulta));
			pstmt.executeQuery();
			pstmt.close();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(ERRO_NEGOCIO_CONSULTA_EXECUCAO_SQL, true);
		}
	}

	/**
	 * Criar map dados parametro teste execucao.
	 * 
	 * @param consulta
	 *            the consulta
	 * @return the map
	 */
	private Map<String, Object[]> criarMapDadosParametroTesteExecucao(Consulta consulta) {

		Map<String, Object[]> mapCamposExecucaoConsulta = new HashMap<String, Object[]>();
		List<ConsultaParametro> lista = Util.ordenarColecaoPorAtributo(consulta.getListaParametro(), "ordem", true);
		for (ConsultaParametro consultaParametro : lista) {
			Object[] valores = new Object[1];
			Object valor = null;
			if ("DATA".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				valor = new Date();
			}
			if ("BOLEANO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				valor = Boolean.TRUE;
			}
			if ("NUMÉRICO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				valor = Long.valueOf(0);
			}
			if ("ALFANUMÉRICO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())) {
				valor = "";
			}
			valores[0] = valor;
			mapCamposExecucaoConsulta.put(consultaParametro.getCodigo(), valores);
		}
		return mapCamposExecucaoConsulta;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * ControladorConsultaImpl
	 * #consultarConsulta(java.util.Map)
	 */
	@Override
	public Collection<Consulta> consultarConsulta(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.addOrder(Order.asc("nome"));

		if (filtro != null) {

			// validar para proibir uma consulta
			// com o mesmo nome com o mesmo modulo
			String nome = (String) filtro.get("nome");
			if (!StringUtils.isEmpty(nome)) {
				criteria.add(Restrictions.ilike("nome", Util.formatarTextoConsulta(nome)));
			}

			Long idModulo = (Long) filtro.get("idModulo");
			if (idModulo != null) {
				criteria.add(Restrictions.eq("modulo.chavePrimaria", idModulo));
			}

			Long idOperacao = (Long) filtro.get("idOperacao");
			if (idOperacao != null) {
				criteria.add(Restrictions.eq("operacao.chavePrimaria", idOperacao));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.exportacaodados.
	 * ControladorConsulta
	 * #buscarConsultaExportacaoDispositivosMoveis
	 * ()
	 */
	@Override
	public Consulta buscarConsultaExportacaoDispositivosMoveis() throws NegocioException {

		Consulta consultaExportacao = null;

		ControladorAcesso controladorAcesso = (ControladorAcesso) ServiceLocator.getInstancia().getBeanPorID(
						ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idOperacao", controladorAcesso.obterChaveOperacaoSistema(ChaveOperacaoSistema.EXPORTAR_DADOS_PARA_DISPOSITIVOS_MOVEIS));
		Collection<Consulta> consultas = consultarConsulta(filtro);

		if (!consultas.isEmpty()) {
			consultaExportacao = consultas.iterator().next();
		}

		return consultaExportacao;
	}

	/**
	 * Obter consulta por processo.
	 * 
	 * @param chavePrimariaProcesso
	 *            the chave primaria processo
	 * @return the consulta
	 */
	private Consulta obterConsultaPorProcesso(long chavePrimariaProcesso) {

		StringBuilder hql = new StringBuilder();
		hql.append("select consulta");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" consulta ");
		hql.append(" inner join consulta.operacao operacao");
		hql.append(" where operacao.chavePrimaria = :chavePrimariaProcesso");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaProcesso", chavePrimariaProcesso);
		return (Consulta) query.setMaxResults(1).uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.exportacaodados.ControladorConsulta#obterConsultaExportacaoPorProcesso(long)
	 */
	@Override
	public Consulta obterConsultaExportacaoPorProcesso(long chavePrimariaProcesso) throws NegocioException {

		return obterConsultaPorProcesso(chavePrimariaProcesso);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.exportacaodados.ControladorConsulta#obterConsultaExportacaoLimitadaPorProcesso(long, boolean)
	 */
	@Override
	public Consulta obterConsultaExportacaoLimitadaPorProcesso(long chavePrimariaProcesso, boolean isReexportar) throws NegocioException {

		Consulta consultaExportacao = obterConsultaPorProcesso(chavePrimariaProcesso);

		if (isReexportar) {
			limitarConsultaExportacao(consultaExportacao, 2);
		} else {
			limitarConsultaExportacao(consultaExportacao, 1);
		}

		return consultaExportacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.relatorio.exportacaodados.ControladorConsulta#limitarConsultaExportacao(br.com.ggas.relatorio.exportacaodados.Consulta,
	 * long)
	 */
	@Override
	public void limitarConsultaExportacao(Consulta consulta, long status) {

		String sql = consulta.getSql().toUpperCase();
		int intervaloOrderby = sql.indexOf(" ORDER");
		consulta.setSql(sql.substring(0, intervaloOrderby) + " AND LEMS_CD = " + status + sql.substring(intervaloOrderby, sql.length()));
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.exportacaodados.ControladorConsulta#confirmarExportacao(br.com.ggas.relatorio.exportacaodados.Consulta,
	 * java.util.Map)
	 */
	@Override
	public void confirmarExportacao(Consulta consulta, Map<String, Object[]> mapDadosParametro) throws NegocioException {

		String sql = consulta.getSql().toUpperCase();
		int intervaloWhere = sql.indexOf("WHERE 1=1 ");
		int intervaloOrderby = sql.indexOf(" ORDER");
		consulta.setSql("UPDATE LEITURA_MOVIMENTO SET LEMS_CD = 2 WHERE 1=1 " + sql.substring(intervaloWhere + 10, intervaloOrderby));
		PreparedStatement pstmt = getPreparedStatementExecucaoConsulta(consulta, mapDadosParametro);
		try {
			pstmt.executeUpdate();
			pstmt.getConnection().commit();
			pstmt.close();
		} catch (Exception e) {
			throw new NegocioException(e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorConsultaImpl
	 * #validarRemoverConsulta(java.lang.Long[])
	 */
	@Override
	public void validarRemoverConsulta(Long[] chavesPrimarias) throws NegocioException {

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorConsultaImpl
	 * #validarAtualizarConsulta(java.lang.Long[])
	 */
	@Override
	public void validarAtualizarConsulta(Long[] chavesPrimarias) throws NegocioException {

		if (chavesPrimarias == null || chavesPrimarias.length != 1) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorConsultaImpl
	 * #listarMascaraParametro()
	 */
	@Override
	public MascaraParametro[] listarMascaraParametro(String tipo) {

		return ConsultaParametro.MascaraParametro.getListaPorTipo(tipo);
	}

	public Class<?> getClasseEntidadeTabela() {

		return ServiceLocator.getInstancia().getClassPorID(Tabela.BEAN_ID_TABELA);
	}

	public Class<?> getClasseEntidadeColuna() {

		return ServiceLocator.getInstancia().getClassPorID(Coluna.BEAN_ID_COLUNA_TABELA);
	}

	public Class<?> getClasseLeituraMovimento() {

		return ServiceLocator.getInstancia().getClassPorID(LeituraMovimento.BEAN_ID_LEITURA_MOVIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.relatorio.exportacaodados.impl
	 * .ControladorConsultaImpl
	 * #listarEntidadeReferenciada()
	 */
	@Override
	public Collection<Tabela> listarEntidadeReferenciada() throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select tabela");
		hql.append(" from ");
		hql.append(getClasseEntidadeTabela().getSimpleName());
		hql.append(" tabela where tabela.consultaDinamica = true");
		hql.append(" order by descricao asc ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.relatorio.exportacaodados.impl
	 * .ControladorImovel
	 * #obterEntidadeReferenciadaPorNome(String
	 * nomeTabela)
	 */
	@Override
	public Collection<Coluna> obterColunasLeituraMovimentoPorNomeTabela(String nomeTabela) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select coluna");
		hql.append(" from ");
		hql.append(getClasseEntidadeColuna().getSimpleName());
		hql.append(" coluna ");
		hql.append(" inner join coluna.tabela tabela");
		hql.append(" where tabela.nome = :nomeTabela");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString())
						.setString("nomeTabela", nomeTabela);
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.relatorio.exportacaodados.impl
	 * .ControladorConsulta
	 * #criarConsultaParametro()
	 */
	@Override
	public EntidadeNegocio criarConsultaParametro() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ConsultaParametro.BEAN_ID_CONSULTA_PARAMETRO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.localidade.
	 * ControladorConsulta
	 * #validarConsultaParametro()
	 */
	@Override
	public void validarConsultaParametro(ConsultaParametro consultaParametro, Collection<ConsultaParametro> listaConsultaParametro,
					int indice) throws NegocioException {

		consultaParametro.validarDados();
		Map<String, Object> validacao = consultaParametro.validarDados();
		if (!validacao.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_CONSULTA_PARAMETRO, true);
		}
		if (consultaParametro.getTabela() != null && (consultaParametro.getTabela().getNome().equals(Tabela.TABELA_ENTIDADE_CONTEUDO)
						|| consultaParametro.getTabela().getNome().equals(Tabela.TABELA_UNIDADE)) && 
						consultaParametro.getChaveEntidadeComplementar() == null) {

			throw new NegocioException(ERRO_NEGOCIO_CONSULTA_PARAMETRO_ENTIDADE_COMPLEMENTAR, true);
		}
		if (listaConsultaParametro != null) {
			int indiceLista = 0;
			for (ConsultaParametro parametro : listaConsultaParametro) {
				if (parametro.getCodigo().equalsIgnoreCase(consultaParametro.getCodigo()) && indice != indiceLista) {
					throw new NegocioException(ERRO_NEGOCIO_CONSULTA_PARAMETRO_CODIGO_EXISTENTE, true);
				}
				if (parametro.getNomeCampo().equalsIgnoreCase(consultaParametro.getNomeCampo()) && indice != indiceLista) {
					throw new NegocioException(ERRO_NEGOCIO_CONSULTA_PARAMETRO_NOME_CAMPO_EXISTENTE, true);
				}
				indiceLista++;
			}
		}
	}

	/**
	 * Gets the parametro por codigo.
	 * 
	 * @param codigo
	 *            the codigo
	 * @param lista
	 *            the lista
	 * @return the parametro por codigo
	 */
	private ConsultaParametro getParametroPorCodigo(String codigo, Collection<ConsultaParametro> lista) {

		ConsultaParametro consultaParametro = null;
		for (ConsultaParametro parametro : lista) {
			if (parametro.getCodigo().equalsIgnoreCase(codigo)) {
				consultaParametro = parametro;
				break;
			}
		}
		return consultaParametro;
	}

	/**
	 * Gets the prepared statement execucao consulta.
	 * 
	 * @param consulta
	 *            the consulta
	 * @param mapDadosParametro
	 *            the map dados parametro
	 * @return the prepared statement execucao consulta
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private PreparedStatement getPreparedStatementExecucaoConsulta(Consulta consulta, Map<String, Object[]> mapDadosParametro)
					throws NegocioException {

		PreparedStatement pstmt = null;
		try {
			// recupera a sessão do hibernate
			Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();

			// verifica se há parametros
			if (consulta.getListaParametro() != null && !consulta.getListaParametro().isEmpty()) {

				// rotina para recuperar a
				// quantidade de parametros,
				// considerando seu tipo
				// multivalorado ou não.
				int qtdParametros = 0;
				for (ConsultaParametro param : consulta.getListaParametro()) {
					// pega os valores
					// correspondentes ao
					// parametro em questão
					// (considerando ser
					// multivalorado ou não)
					Object[] valores = mapDadosParametro.get(param.getCodigo());

					if ("ALFANUMÉRICO".equals(param.getEntidadeConteudo().getDescricao())) {
						for (Object parametro : valores) {
							String[] chaves = String.valueOf(parametro).split(",");
							for (String chave : chaves) {
								qtdParametros++;
							}

						}
					} else {
						for (int i = 0; i < valores.length; i++) {
							qtdParametros++;
						}
					}
				}

				// o prefixo definido para
				// identificar um parametro na
				// declaração SQL
				String paramPrefixo = "{";
				// o sufixo definido para
				// identificar um parametro na
				// declaração SQL
				String paramSufixo = "}";

				// prepara uma string sql que irá
				// tratar os parametros
				// multivalorados
				String sqlAlterado = consulta.getSql();
				for (ConsultaParametro param : consulta.getListaParametro()) {
					if (param.getMultivalorado()) {
						Object[] valores = mapDadosParametro.get(param.getCodigo());
						
						StringBuilder atualizado = new StringBuilder();
						// um laço que repete o
						// parametro na string de
						// acordo com o tamanho de
						// seus valores
						// (multivalorado)
						for (int i = 0; i < valores.length; i++) {

							if ("ALFANUMÉRICO".equals(param.getEntidadeConteudo().getDescricao())) {
									atualizado.append("?,");

							} else {
								atualizado.append(paramPrefixo).append(param.getCodigo().toUpperCase()).append(paramSufixo);
								if (i < valores.length - 1) {
									atualizado.append(", ");
								}
							}
						}
						atualizado.deleteCharAt(atualizado.length() - 1);
						// efetua o tratamento
						// para parametros
						// multivalorados
						sqlAlterado = sqlAlterado.replace(paramPrefixo + param.getCodigo().toUpperCase() + paramSufixo,
										atualizado.toString());
					}
				}

				// prepara uma string para setar
				// no preparedStatement, trocando
				// os parametros por uma "?"
				String sqlParaExecucao = sqlAlterado;
				int indice = 0;
				for (ConsultaParametro param : consulta.getListaParametro()) {
					sqlParaExecucao = sqlParaExecucao.replace(paramPrefixo + param.getCodigo().toUpperCase() + paramSufixo, "?");
				}

				// rotina que verifica se a string
				// possui algum parametro entre
				// aspas
				// (no caso do usuario inserir um
				// parametro entre as aspas duplas
				// do alias
				boolean abriuAspas = false;
				StringBuilder copiaSqlParaExecucao = new StringBuilder();
				for (int i = 0; i < sqlParaExecucao.length(); i++) {
					if (sqlParaExecucao.charAt(i) == '\"') {
						abriuAspas = !abriuAspas;
					}

					if (sqlParaExecucao.charAt(i) == '?') {
						if (!abriuAspas) {
							copiaSqlParaExecucao.append(sqlParaExecucao.charAt(i));
						}
					} else {
						copiaSqlParaExecucao.append(sqlParaExecucao.charAt(i));
					}
				}

				// seta a string no
				// preparedStatement
				pstmt = session.connection().prepareStatement(copiaSqlParaExecucao.toString());

				// O set de parametros no
				// preparedStatement é realizado
				// de forma procedural
				// a rotina verifica a ordem de
				// set e seu respectivo parametro
				// e armazena num map
				Map<Integer, ConsultaParametro> posicoesPorParametro = new HashMap<Integer, ConsultaParametro>();
				indice = 0;
				String parametroAtual = "";
				int indiceFinal = 0;
				abriuAspas = false;
				// o laço é realizado no getSQL()
				// pois esta string ainda preserva
				// os parametros com prefixo e
				// sufixo.
				for (int i = 0; i < consulta.getSql().length(); i++) {
					if (consulta.getSql().charAt(i) == '\"') {
						abriuAspas = !abriuAspas;
					}
					// se encontrou o prefixo,
					// registra o indice do seu
					// sufixo.
					if (consulta.getSql().charAt(i) == paramPrefixo.charAt(0)) {
						indiceFinal = consulta.getSql().substring(i).indexOf(paramSufixo) + i;
						parametroAtual = consulta.getSql().substring(i + 1, indiceFinal);
						// se o parametro nao está
						// entre aspas, é valido.
						if (!abriuAspas) {
							ConsultaParametro cp = getParametroPorCodigo(parametroAtual, consulta.getListaParametro());
							posicoesPorParametro.put(indice, cp);
							indice++;
						}
					}
				}

				// rotina que efetua o set de
				// parametros de forma procedural
				// auxiliado pelo map definido
				// anteriormente.
				int indiceValorado = 0;
				for (int i = 0; i < posicoesPorParametro.size(); i++) {
					ConsultaParametro parametro = posicoesPorParametro.get(i);
					Object[] valor = mapDadosParametro.get(parametro.getCodigo());
					// caso seja multivalorado, o
					// indice de valor é
					// incremental
					if (parametro.getMultivalorado()) {

						if ("ALFANUMÉRICO".equalsIgnoreCase(parametro.getEntidadeConteudo().getDescricao())) {
							if ("rota".equals(parametro.getCodigo())) {
								int posicao = 0;
								for (Object chave : valor) {
									pstmt.setString(posicao + 1, (String) chave);
									posicao++;
								}
							} else {
								pstmt.setString(i + 1, valor[indiceValorado].toString());
							}
						}
						if ("BOLEANO".equalsIgnoreCase(parametro.getEntidadeConteudo().getDescricao())) {
							pstmt.setBoolean(i + 1, (Boolean) valor[indiceValorado]);
						}
						if ("DATA".equalsIgnoreCase(parametro.getEntidadeConteudo().getDescricao())) {
							pstmt.setDate(i + 1, new java.sql.Date(((Date) valor[indiceValorado]).getTime()));
						}
						if ("NUMÉRICO".equalsIgnoreCase(parametro.getEntidadeConteudo().getDescricao())) {
							if (valor[indiceValorado] instanceof Integer) {
								pstmt.setInt(i + 1, (Integer) valor[indiceValorado]);
							}
							if (valor[indiceValorado] instanceof Long) {
								pstmt.setLong(i + 1, (Long) valor[indiceValorado]);
							}
							if (valor[indiceValorado] instanceof Double) {
								pstmt.setDouble(i + 1, (Double) valor[indiceValorado]);
							}
						}

						if (indiceValorado == valor.length - 1) {
							indiceValorado = 0;
						}
					} else {
						// caso nao seja
						// multivalorado, o indice
						// do valor é sempre o
						// primeiro
						if ("ALFANUMÉRICO".equalsIgnoreCase(parametro.getEntidadeConteudo().getDescricao())) {
							pstmt.setString(i + 1, valor[0].toString());
						}
						if ("BOLEANO".equalsIgnoreCase(parametro.getEntidadeConteudo().getDescricao())) {
							pstmt.setBoolean(i + 1, (Boolean) valor[0]);
						}
						if ("DATA".equalsIgnoreCase(parametro.getEntidadeConteudo().getDescricao())) {
							pstmt.setDate(i + 1, new java.sql.Date(((Date) valor[0]).getTime()));
						}
						if ("NUMÉRICO".equalsIgnoreCase(parametro.getEntidadeConteudo().getDescricao())) {
							if (valor[0] instanceof Integer) {
								pstmt.setInt(i + 1, (Integer) valor[0]);
							}
							if (valor[0] instanceof Long) {
								pstmt.setLong(i + 1, (Long) valor[0]);
							}
							if (valor[0] instanceof Double) {
								pstmt.setDouble(i + 1, (Double) valor[0]);
							}
						}
					}

				}
			} else {
				// caso a consulta nao possua
				// parametros
				pstmt = session.connection().prepareStatement(consulta.getSql()); // NOSONAR {Alterar para utilizar CONSTANTES}
			}

		} catch (Exception e) {

			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e1) {
				LOG.info(e);
				LOG.error(e.getStackTrace(), e);
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}

			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			throw new NegocioException(ERRO_NEGOCIO_CONSULTA_EXECUCAO_SQL, true);
		}
		
		return pstmt;
	}

	/**
	 * Validar execucao consulta.
	 * 
	 * @param separador
	 *            the separador
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarExecucaoConsulta(String separador) throws NegocioException {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (separador == null || "".equals(separador)) {
			stringBuilder.append("Separador");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
			throw new NegocioException(erros);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.relatorio.exportacaodados.impl
	 * .ControladorConsultaImpl
	 * #listarEntidadeReferenciada()
	 */
	@Override
	public byte[] obterDadosExecucaoConsulta(Consulta consulta, Map<String, Object[]> mapDadosParametro, String separador)
					throws NegocioException {

		validarExecucaoConsulta(separador);
		StringBuilder sb = new StringBuilder();
		boolean possuiDados = false;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			pstmt = getPreparedStatementExecucaoConsulta(consulta, mapDadosParametro);
			// cria o resultset
			rs = pstmt.executeQuery();
			// recupera o metadata
			ResultSetMetaData rsm = rs.getMetaData();

			// monta o cabeçalho
			StringBuilder cabecalho = new StringBuilder();
			for (int i = 1; i <= rsm.getColumnCount(); i++) {
				cabecalho.append("\"").append(rsm.getColumnName(i)).append("\"");
				if (i < rsm.getColumnCount()) {
					cabecalho.append(separador);
				}
			}
			sb.append(cabecalho.toString()).append("\r\n");

			// laço que cria as linhas dos
			// registros
			while (rs.next()) {
				possuiDados = true;

				String[] valores = new String[rsm.getColumnCount()];
				for (int j = 0; j < valores.length; j++) {
					valores[j] = rs.getString(j + 1);
					if (valores[j] != null) {
						valores[j] = valores[j].replace(" 00:00:00.0", "");
					} else {
						valores[j] = "";
					}
				}
				StringBuilder linha = new StringBuilder();
				for (int n = 0; n < valores.length; n++) {
					linha.append(valores[n]);
					if (n < valores.length - 1) {
						linha.append(separador);
					}
				}
				linha.append("\r\n");
				sb.append(linha.toString());
			}

			// fecha recursos

		} catch (Exception e) {
			Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			throw new NegocioException(ERRO_NEGOCIO_CONSULTA_EXECUCAO_SQL, true);
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				Logger.getLogger("ERROR").debug(e.getStackTrace(), e);
			}
		}

		if (!possuiDados) {
			throw new NegocioException(ERRO_NEGOCIO_EXPORTACAO_SEM_DADOS, true);
		}

		return sb.toString().getBytes();
	}

	/**
	 * Método responsável obter uma lista dos registros da entidade referenciada escolhida.
	 * 
	 * @param tabela
	 *            the tabela
	 * @return the collection
	 * @throws NegocioException
	 *             Caso ocorra algum erro na invocação do método.
	 */
	@Override
	public Collection<EntidadeNegocio> listarRegistrosDaEntidadeReferenciada(Tabela tabela) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append("select tabela ");
		hql.append(" from ");
		hql.append(tabela.getNomeClasse());
		hql.append(" tabela");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.exportacaodados.ControladorConsulta#exportarDadosMoveis(java.util.Map)
	 */
	@Override
	public byte[] exportarDadosMoveis(Map<String, Object> parametros) throws NegocioException {

		ControladorLeituraMovimento controladorLeituraMovimento = ServiceLocator.getInstancia().getControladorLeituraMovimento();

		Collection<LeituraMovimento> listaLeituraMovimento = controladorLeituraMovimento.consultarLeituraMovimento(parametros);

		StringBuilder sb = new StringBuilder();
		String sqlInsert = "INSERT INTO rotas (contador,nomefantasia,logradouro,bairro,serie,medidor,media3,"
						+ "ultleitura,dataultleitura,lido,anomesfaturamento,ciclo,pontoconsumo) VALUES (";

		boolean possuiDados = false;
		int i = 0;
		for (LeituraMovimento leituraMovimento : listaLeituraMovimento) {
			possuiDados = true;
			String dados = "";
			dados += leituraMovimento.getSequencialLeitura() + ",";
			dados += "'" + leituraMovimento.getNomeCliente() + "',";

			String endereco = leituraMovimento.getEndereco();
			String[] arrayEndereco = endereco.split(",");
			if (arrayEndereco.length > 2) {
				String logradouro = arrayEndereco[0] + arrayEndereco[1];
				String bairro = leituraMovimento.getBairro();
				dados = dados + "'" + logradouro + "'," + "'" + bairro + "',";
			}

			dados += "'" + leituraMovimento.getNumeroSerieMedidor() + "',";
			dados += "'" + leituraMovimento.getNumeroSerieMedidor() + "',";
			dados += "" + leituraMovimento.getConsumoMedio() + ",";
			dados += "" + leituraMovimento.getLeituraAnterior() + ",";
			String dataFormatada = Util.converterDataParaStringSemHora(leituraMovimento.getDataLeituraAnterior(), "dd/MM/yyyy");
			dados += "'" + dataFormatada + "',";
			String isLeituraRealizada = "";
			dados += "'" + isLeituraRealizada + "',";
			dados += "'" + leituraMovimento.getAnoMesFaturamento() + "',";
			dados += "'" + leituraMovimento.getCiclo() + "',";
			dados += "" + leituraMovimento.getPontoConsumo().getChavePrimaria();

			dados += ") ";

			i++;
			if (listaLeituraMovimento.size() != i) {
				dados += "\r\n";
			}

			sb.append(sqlInsert);
			sb.append(dados);

		}

		if (!possuiDados) {
			throw new NegocioException(ERRO_NEGOCIO_EXPORTACAO_SEM_DADOS, true);
		}

		return sb.toString().getBytes();
	}
	
	/**
	 * Setar lista parametro consulta.
	 * 
	 * @param consulta - {@link Consulta}
	 * @param listaConsultaParametro - {@link Collection}
	 */
	public void setarListaParametroConsulta(Consulta consulta, Collection<ConsultaParametro> listaConsultaParametro) {

		consulta.getListaParametro().clear();
		if (listaConsultaParametro != null) {
			for (ConsultaParametro consultaParametro : listaConsultaParametro) {
				consultaParametro.setConsulta(consulta);
				consultaParametro.setUltimaAlteracao(Calendar.getInstance().getTime());
				consulta.getListaParametro().add(consultaParametro);
			}
		}
	}
	
	public void tratarParametrosData(ConsultaParametro consultaParametro, Object[] valor, int i) throws GGASException {
		valor[i] = valor[i].toString().replace("_", "");

		// tratamento dos caracteres
		// de máscara '.', ',' e '/'
		// para campos numéricos
		if ("NUMÉRICO".equalsIgnoreCase(consultaParametro.getEntidadeConteudo().getDescricao())
				&& consultaParametro.getMascara() != null) {
			if (MascaraParametro.MONETARIO_REAL.getMascara().equals(consultaParametro.getMascara())) {
				valor[i] = valor[i].toString().replace(",", ".");
			} else if (!MascaraParametro.MONETARIO_DOLLAR.getMascara().equals(consultaParametro.getMascara())) {
				valor[i] = Util.removerCaracteresEspeciais(valor[i].toString());
			}
		}
		if (valor[i].toString().isEmpty()) {
			throw new GGASException(ControladorConsulta.ERRO_NEGOCIO_CONSULTA_PARAMETRO, true);
		}
	}

}
