/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.exportacaodados.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.relatorio.exportacaodados.Consulta;
import br.com.ggas.relatorio.exportacaodados.ConsultaParametro;
import br.com.ggas.util.Constantes;

public class ConsultaParametroImpl extends EntidadeNegocioImpl implements ConsultaParametro {

	private static final long serialVersionUID = 4350709595380316213L;

	private Consulta consulta;

	private String codigo;

	private String nomeCampo;

	private EntidadeConteudo entidadeConteudo;

	private Integer numeroMaximoCaracteres;

	private String mascara;

	private Integer ordem;

	private Boolean fk;

	private Boolean multivalorado;

	private Tabela tabela;

	private Long chaveEntidadeComplementar;

	/**
	 * @return the consulta
	 */
	@Override
	public Consulta getConsulta() {

		return consulta;
	}

	/**
	 * @param consulta
	 */
	@Override
	public void setConsulta(Consulta consulta) {

		this.consulta = consulta;
	}

	/**
	 * @return the codigo
	 */
	@Override
	public String getCodigo() {

		return codigo;
	}

	/**
	 * @param codigo
	 */
	@Override
	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	/**
	 * @return the nomeCampo
	 */
	@Override
	public String getNomeCampo() {

		return nomeCampo;
	}

	/**
	 * @param nomeCampo
	 */
	@Override
	public void setNomeCampo(String nomeCampo) {

		this.nomeCampo = nomeCampo;
	}

	/**
	 * @return the entidadeConteudo
	 */
	@Override
	public EntidadeConteudo getEntidadeConteudo() {

		return entidadeConteudo;
	}

	/**
	 * @param entidadeConteudo
	 */
	@Override
	public void setEntidadeConteudo(EntidadeConteudo entidadeConteudo) {

		this.entidadeConteudo = entidadeConteudo;
	}

	/**
	 * @return the numeroMaximoCaracteres
	 */
	@Override
	public Integer getNumeroMaximoCaracteres() {

		return numeroMaximoCaracteres;
	}

	/**
	 * @param numeroMaximoCaracteres
	 */
	@Override
	public void setNumeroMaximoCaracteres(Integer numeroMaximoCaracteres) {

		this.numeroMaximoCaracteres = numeroMaximoCaracteres;
	}

	/**
	 * @return the mascara
	 */
	@Override
	public String getMascara() {

		return mascara;
	}

	/**
	 * @param mascara
	 */
	@Override
	public void setMascara(String mascara) {

		this.mascara = mascara;
	}

	/**
	 * @return the ordem
	 */
	@Override
	public Integer getOrdem() {

		return ordem;
	}

	/**
	 * @param ordem
	 */
	@Override
	public void setOrdem(Integer ordem) {

		this.ordem = ordem;
	}

	/**
	 * @return the fk
	 */
	@Override
	public Boolean getFk() {

		return fk;
	}

	/**
	 * @param fk
	 */
	@Override
	public void setFk(Boolean fk) {

		this.fk = fk;
	}

	/**
	 * @return the multivalorado
	 */
	@Override
	public Boolean getMultivalorado() {

		return multivalorado;
	}

	/**
	 * @param multivalorado
	 */
	@Override
	public void setMultivalorado(Boolean multivalorado) {

		this.multivalorado = multivalorado;
	}

	/**
	 * @return the tabela
	 */
	@Override
	public Tabela getTabela() {

		return tabela;
	}

	/**
	 * @param tabela
	 */
	@Override
	public void setTabela(Tabela tabela) {

		this.tabela = tabela;
	}

	/**
	 * @return the chaveEntidadeComplementar
	 */
	@Override
	public Long getChaveEntidadeComplementar() {

		return chaveEntidadeComplementar;
	}

	/**
	 * @param chaveEntidadeComplementar
	 */
	@Override
	public void setChaveEntidadeComplementar(Long chaveEntidadeComplementar) {

		this.chaveEntidadeComplementar = chaveEntidadeComplementar;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.getEntidadeConteudo() == null) {
			stringBuilder.append(ENTIDADE_CONTEUDO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if (!this.getEntidadeConteudo().getDescricao().equals(BOLEANO) && this.getNumeroMaximoCaracteres() == null) {
				stringBuilder.append(NUMERO_MAX_CARACTERES);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(StringUtils.isEmpty(this.getCodigo())) {
				stringBuilder.append(CODIGO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if(StringUtils.isEmpty(this.getNomeCampo())) {
				stringBuilder.append(NOME);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
		}

		return erros;
	}
}
