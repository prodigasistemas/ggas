/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.exportacaodados;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.relatorio.exportacaodados.ConsultaParametro.MascaraParametro;

/**
 * 
 *
 */
public interface ControladorConsulta extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_CONSULTA = "controladorConsulta";

	String ERRO_NEGOCIO_CONSULTA_EXISTENTE = "ERRO_NEGOCIO_CONSULTA_EXISTENTE";

	String ERRO_NEGOCIO_CONSULTA_EXECUCAO_SQL = "ERRO_NEGOCIO_CONSULTA_EXECUCAO_SQL";

	String ERRO_NEGOCIO_CONSULTA_SQL_INVALIDO = "ERRO_NEGOCIO_CONSULTA_SQL_INVALIDO";

	String ERRO_NEGOCIO_CONSULTA_PARAMETRO_CODIGO_NAO_LOCALIZADO_SQL = "ERRO_NEGOCIO_CONSULTA_PARAMETRO_CODIGO_NAO_LOCALIZADO_SQL";

	String ERRO_NEGOCIO_CONSULTA_PARAMETRO_CODIGO_EXISTENTE = "ERRO_NEGOCIO_CONSULTA_PARAMETRO_CODIGO_EXISTENTE";

	String ERRO_NEGOCIO_CONSULTA_PARAMETRO_NOME_CAMPO_EXISTENTE = "ERRO_NEGOCIO_CONSULTA_PARAMETRO_NOME_CAMPO_EXISTENTE";

	String ERRO_NEGOCIO_CONSULTA_PARAMETRO_ORDEM_EXISTENTE = "ERRO_NEGOCIO_CONSULTA_PARAMETRO_ORDEM_EXISTENTE";

	String ERRO_NEGOCIO_CONSULTA_PARAMETRO_ENTIDADE_COMPLEMENTAR = "ERRO_NEGOCIO_CONSULTA_PARAMETRO_ENTIDADE_COMPLEMENTAR";

	String ERRO_NEGOCIO_CONSULTA_PARAMETRO_INEXISTENTE = "ERRO_NEGOCIO_CONSULTA_PARAMETRO_INEXISTENTE";

	String ERRO_NEGOCIO_EXPORTACAO_SEM_DADOS = "ERRO_NEGOCIO_EXPORTACAO_SEM_DADOS";

	public static String ERRO_NEGOCIO_CONSULTA_PARAMETRO_EXCLUIR_SELECIONADO = "ERRO_NEGOCIO_CONSULTA_PARAMETRO_EXCLUIR_SELECIONADO";

	public static String ERRO_NEGOCIO_CONSULTA_PARAMETRO = "ERRO_NEGOCIO_CONSULTA_PARAMETRO";

	String ERRO_NEGOCIO_CONSULTA_EXPORTACAO_INEXISTENTE = "ERRO_NEGOCIO_CONSULTA_EXPORTACAO_INEXISTENTE";

	String SQL_FROM_WHERE_FIXO_LEITURA_MOVIMENTO = "FROM LEITURA_MOVIMENTO WHERE 1=1";

	String SQL_ORDER_BY_FIXO_LEITURA_MOVIMENTO = "ORDER BY LEMO_NR_ROTA, LEMO_NR_SEQUENCIA_LEITURA, LEMO_DS_PONTO_CONSUMO";

	/**
	 * Metodo responsavel por consultar consulta.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return colecao de consultas
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Consulta> consultarConsulta(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Recupera a consulta utilizada na exportação
	 * de dados para dispositivos móveis.
	 * 
	 * @return consultaExportacao
	 * @throws NegocioException
	 *             Caso a consulta não seja
	 *             encontrada
	 */
	Consulta buscarConsultaExportacaoDispositivosMoveis() throws NegocioException;

	/**
	 * Obter consulta exportacao por processo.
	 * 
	 * @param chavePrimariaProcesso
	 *            the chave primaria processo
	 * @return the consulta
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Consulta obterConsultaExportacaoPorProcesso(long chavePrimariaProcesso) throws NegocioException;

	/**
	 * Obter consulta exportacao limitada por processo.
	 * 
	 * @param chavePrimariaProcesso
	 *            the chave primaria processo
	 * @param isReexportar
	 *            the is reexportar
	 * @return the consulta
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Consulta obterConsultaExportacaoLimitadaPorProcesso(long chavePrimariaProcesso, boolean isReexportar) throws NegocioException;

	/**
	 * Metodo responsavel por validar a remocao de
	 * uma consulta.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarRemoverConsulta(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por verificar se a(s)
	 * chave(s) foram selecionada(s)
	 * para atualizar uma consulta.
	 * 
	 * @param chavesPrimarias
	 *            Chaves das consultas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarAtualizarConsulta(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por listar as mascaras
	 * de parametro.
	 * 
	 * @param tipo
	 *            the tipo
	 * @return the mascara parametro[]
	 */
	MascaraParametro[] listarMascaraParametro(String tipo);

	/**
	 * Método responsável por listar entidades
	 * referenciadas.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Tabela> listarEntidadeReferenciada() throws NegocioException;

	/**
	 * Método responsável por listar entidades
	 * referenciadas por chave primaria.
	 * 
	 * @param nomeTabela
	 *            the nome tabela
	 * @return the collection
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Coluna> obterColunasLeituraMovimentoPorNomeTabela(String nomeTabela) throws NegocioException;

	/**
	 * Método responsável por criar uma entidade
	 * de negocio.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarConsultaParametro();

	/**
	 * Método responsável por validar um parametro
	 * da consulta.
	 * 
	 * @param consultaParametro
	 *            the consulta parametro
	 * @param listaConsultaParametro
	 *            the lista consulta parametro
	 * @param indice
	 *            the indice
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarConsultaParametro(ConsultaParametro consultaParametro, Collection<ConsultaParametro> listaConsultaParametro, int indice)
					throws NegocioException;

	/**
	 * Método responsável obter dados do resultado
	 * da execução de uma consulta.
	 * 
	 * @param consulta
	 *            the consulta
	 * @param mapDadosParametro
	 *            the map dados parametro
	 * @param separador
	 *            the separador
	 * @return the byte[]
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	byte[] obterDadosExecucaoConsulta(Consulta consulta, Map<String, Object[]> mapDadosParametro, String separador) throws NegocioException;

	/**
	 * Método responsável obter uma lista dos
	 * registros da entidade referenciada
	 * escolhida.
	 * 
	 * @param tabela
	 *            the tabela
	 * @return the collection
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<EntidadeNegocio> listarRegistrosDaEntidadeReferenciada(Tabela tabela) throws NegocioException;

	/**
	 * Confirmar exportacao.
	 * 
	 * @param consulta
	 *            the consulta
	 * @param mapDadosParametro
	 *            the map dados parametro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void confirmarExportacao(Consulta consulta, Map<String, Object[]> mapDadosParametro) throws NegocioException;

	/**
	 * Limitar consulta exportacao.
	 * 
	 * @param consulta
	 *            the consulta
	 * @param status
	 *            the status
	 */
	void limitarConsultaExportacao(Consulta consulta, long status);

	/**
	 * Exportar dados moveis.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	byte[] exportarDadosMoveis(Map<String, Object> parametros) throws NegocioException;
	
	/**
	 * Setar lista parametro consulta.
	 * 
	 * @param consulta - {@link Consulta}
	 * @param listaConsultaParametro - {@link Collection}
	 */
	void setarListaParametroConsulta(Consulta consulta, Collection<ConsultaParametro> listaConsultaParametro);
	
	/**
	 * Realiza o tratamento de parametros Data.
	 * 
	 * @param consultaParametro - {@link ConsultaParametro}
	 * @param valor - {@link Object}
	 * @param i - {@link Integer}
	 * @throws GGASException - {@link GGASException}
	 */
	void tratarParametrosData(ConsultaParametro consultaParametro, Object[] valor, int i) throws GGASException;
}
