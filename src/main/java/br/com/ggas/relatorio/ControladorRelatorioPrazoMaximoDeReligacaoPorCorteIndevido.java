package br.com.ggas.relatorio;

import java.util.Date;

import br.com.ggas.geral.exception.GGASException;
/**
 * Controlador responsável pelo relatório do prazo máximo para religação por corte indevido.
 */
public interface ControladorRelatorioPrazoMaximoDeReligacaoPorCorteIndevido {

	String BEAN_ID_CONTROLADOR_RELATORIO_PRAZO_MAXIMO_RELIGAMENTO_CORTE_INDEVIDO = "controladorServicoAutorizavao";

	String ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO = "ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO";

	String ERRO_NEGOCIO_RELATORIO_PRAZO_MAXIMO_RELIGAMENTO_CORTE_INDEVIDO_NENHUM_REGISTRO_ENCONTRADO 
	= "ERRO_NEGOCIO_RELATORIO_PRAZO_MAXIMO_RELIGAMENTO_CORTE_INDEVIDO_NENHUM_REGISTRO_ENCONTRADO";

	String RELATORIO_PRAZO_MAXIMO_RELIGAMENTO_CORTE_INDEVIDO_JASPER = "relatorioDataLimiteReligacaoGasPorCorteIndevido.jasper";

	/**
	 * Método reponsável por gerar o relatório.
	 * 
	 * @param dataIncial
	 * @param exibirFiltros
	 * @param tipoRelatorio
	 * @param dataFinal
	 * 
	 * @return the byte[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] gerarRelatorio(Date dataIncial, Date dataFinal, String exibirFiltros, String tipoRelatorio) throws GGASException;
}
