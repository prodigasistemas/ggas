package br.com.ggas.relatorio.ligacaogas;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.ServiceLocator;
/**
 * Repositório responsável pela ligação de gás
 */
@Repository
public class RepositorioLigacaoGas extends RepositorioGenerico {
	/**
	 * @param sessionFactory
	 */
	@Autowired
	public RepositorioLigacaoGas(SessionFactory sessionFactory){
		setSessionFactory(sessionFactory);
	}
	@Override
	public EntidadeNegocio criar() {
		
		return new ServicoAutorizacao();
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServicoAutorizacao.class;
	}
	/**
	 * @param dataInicio
	 * @param dataFinal
	 *  @return lista de datas limites para ligacao de gás - {@link List}
	 */
	@SuppressWarnings("unchecked")
	public List<ServicoAutorizacaoRelatorioVO> listarDatasLimiteParaLigacaoGas(Date dataInicio, Date dataFinal) {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		StringBuilder hql = new StringBuilder();
		hql.append(" select cliente.nome as nomeCliente, "
						+ "servicoAutorizacao.dataGeracao as dataGerada, "
						+ "servicoAutorizacao.dataEncerramento as dataEncerramento, "
						+ "entidadeConteudo.descricao as descricao");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" servicoAutorizacao ");
		hql.append(" inner join servicoAutorizacao.cliente cliente ");
		hql.append(" inner join servicoAutorizacao.servicoTipo servicoTipo ");
		hql.append(" inner join servicoAutorizacao.contrato contrato ");
		hql.append(" inner join contrato.listaContratoPontoConsumo listaContratoPontoConsumo");
		hql.append(" inner join listaContratoPontoConsumo.faixaPressaoFornecimento faixaPressaoFornecimento");
		hql.append(" inner join faixaPressaoFornecimento.entidadeConteudo entidadeConteudo");
		hql.append(" where ");
		hql.append(" servicoTipo.descricao ");
		hql.append(" LIKE ");
		hql.append("'%INICIO DE FORNECIMENTO MEDIDOR%'");

		if(dataInicio != null){
			hql.append(" and ");
			hql.append(" servicoAutorizacao.dataGeracao >= :inicio ");
		}
		if(dataFinal != null){
			if(dataInicio == null){
				hql.append(" and ");
				hql.append(" iservicoAutorizacao.dataGeracao <= :fim ");
			}else{
				hql.append(" And servicoAutorizacao.dataGeracao <= :fim ");
			}
		}
		hql.append(" order by cliente.nome ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(ServicoAutorizacaoRelatorioVO.class));
		if(dataInicio != null){
			query.setDate("inicio", dataInicio );
		}
		if(dataFinal != null){
			query.setDate("fim", dataFinal );
		}

		return query.list();

	}
	
	/**
	 * @param param
	 * Este método retorna uma lista com parâmetros de sistema.
	 * @return lista de parametros - {@link List}
	 */
	@SuppressWarnings("unchecked")
	public List<String> buscarValorParamentro(String param){
		
		ServiceLocator.getInstancia().getControladorParametroSistema();
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select parametroSistema.valor as valor ");
		hql.append(" from ");
		hql.append(" ParametroSistemaImpl ");
		hql.append(" parametroSistema ");
		hql.append(" Where ");
		hql.append(" parametroSistema.codigo ");
		hql.append(" LIKE ");
		hql.append(" :param ");

		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("param", "%"+param+"%");
		
		return query.list();
	}
	
}
