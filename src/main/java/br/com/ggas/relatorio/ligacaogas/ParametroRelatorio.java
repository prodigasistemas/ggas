package br.com.ggas.relatorio.ligacaogas;

/**
 * Classe responsável por agrupar parametros do relatório
 */
public class ParametroRelatorio {

	private String nomeCliente;
	private String dataGerada;
	private String dataEncerramento;
	private String dataLimite;
	private String status;
	private String pressao;
	
	public String getNomeCliente() {
	
		return nomeCliente;
	}
	
	public void setNomeCliente(String nomeCliente) {
	
		this.nomeCliente = nomeCliente;
	}
	
	public String getDataGerada() {
	
		return dataGerada;
	}
	
	public void setDataGerada(String dataGerada) {
	
		this.dataGerada = dataGerada;
	}
	
	public String getDataEncerramento() {
	
		return dataEncerramento;
	}
	
	public void setDataEncerramento(String dataEncerramento) {
	
		this.dataEncerramento = dataEncerramento;
	}
	
	public String getDataLimite() {
	
		return dataLimite;
	}
	
	public void setDataLimite(String dataLimite) {
	
		this.dataLimite = dataLimite;
	}
	
	public String getStatus() {
	
		return status;
	}
	
	public void setStatus(String status) {
	
		this.status = status;
	}

	
	public String getPressao() {
	
		return pressao;
	}

	
	public void setPressao(String pressao) {
	
		this.pressao = pressao;
	}
}
