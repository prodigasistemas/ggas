
package br.com.ggas.relatorio.ligacaogas;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.relatorio.ControladorRelatorioLigacaoGas;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Util;
/**
 * Controller responsável pelo relatório de ligação de gás
 */
@Controller
@SessionAttributes({"servicoAutorizacao", "ServicoAutorizacaoRelatorioVO"})
public class RelatorioLigacaoGasAction extends GenericAction {

	private static final Logger LOG = Logger.getLogger(RelatorioLigacaoGasAction.class);

	@Autowired
	private ControladorRelatorioLigacaoGas controladorRelatorioLigacaoGas;

	/**
	 * Método responsável pela exibição da tela de resultados
	 * da pesquisa de relatório refernte a data limite da ligação gás.
	 * @return retrona uma tela de pesquisa de relatorio de ligação.
	 */
	@RequestMapping("exibirPesquisaRelatorioDataLimiteLigacaoGas")
	public ModelAndView exibirTela() {
		return new ModelAndView("exibirPesquisaRelatorioLigacaoGas");
	}
	
	/**
	 * Método responsável pela tela gerar relatórios data limite ligação gás
	 * 
	 * @param dataIncial
	 * @param response
	 * @param tipoRelatorio
	 * @param exibirFiltros
	 * @param dataFinal 
	 * @return retorna uma tela de pesquisa de relatorio com a data limite para ligacao de gas.
	 */
	@RequestMapping("gerarRelatorioLigacaoGas")
	public ModelAndView gerarRelatorioDataLimiteReligamentoGas(@RequestParam("dataInicio") String dataIncial,
					@RequestParam("dataFinal") String dataFinal, HttpServletResponse response,
					@RequestParam("tipoRelatorio") String tipoRelatorio, @RequestParam("exibirFiltros") String exibirFiltros)
					throws ParseException {

		FormatoImpressao formatoImpressao = Util.obterFormatoImpressao(tipoRelatorio);
		ModelAndView model = new ModelAndView("forward:/exibirPesquisaRelatorioDataLimiteLigacaoGas");

		byte[] relatorio = null;
		try {

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String diaMesAno = df.format(Calendar.getInstance().getTime());
			df = new SimpleDateFormat("HH'h'mm");
			String hora = df.format(Calendar.getInstance().getTime());
			
			DateFormat formatador = new SimpleDateFormat("dd/MM/yy");
			Date inicio = null;
			
			if("".equals(dataIncial)){
				inicio = null;
			}else{
				inicio = formatador.parse(dataIncial);
			}
			Date fim = null;
			
			if("".equals(dataFinal)){
				fim = null;
			}else{
				fim = formatador.parse(dataFinal);
			}

			relatorio = controladorRelatorioLigacaoGas.gerarRelatorio(inicio, fim, exibirFiltros, tipoRelatorio);
			ServletOutputStream servletOutputStream = response.getOutputStream();
			response.setContentType(this.obterContentTypeRelatorio(formatoImpressao));
			response.setContentLength(relatorio.length);
			response.addHeader(
							"Content-Disposition",
							"attachment; filename=RelatórioLigacaoGas_" + diaMesAno + "_" + hora
											+ Util.obterFormatoRelatorio(formatoImpressao));
			servletOutputStream.write(relatorio, 0, relatorio.length);
			servletOutputStream.flush();
			servletOutputStream.close();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			mensagemAlerta(model, e.getMessage() );
		} catch (GGASException e) {
			LOG.debug(e.getStackTrace(), e);
			mensagemAlerta(model, e.getMessage() );
		}
		return model;
	}

}
