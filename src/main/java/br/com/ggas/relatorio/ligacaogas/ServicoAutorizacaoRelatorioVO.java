/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.ligacaogas;

import java.util.Date;

/**
 * Classe responsável pela autorização dos relatórios
 */
public class ServicoAutorizacaoRelatorioVO {

	private String nomeCliente;

	private Date dataGerada;

	private Date dataEncerramento;
	
	private String descricao;
	
	/**
	 * Construtor
	 * 
	 * @param nomeCliente - {@link String}
	 * @param dataGerada - {@link Date}
	 * @param dataEncerramento - {@link Date}
	 * @param descricao - {@link String}
	 */
	public ServicoAutorizacaoRelatorioVO(String nomeCliente, Date dataGerada, Date dataEncerramento, String descricao) {
		
		this.nomeCliente = nomeCliente;
		if(dataGerada != null) {
			this.dataGerada = (Date) dataGerada.clone();
		}
		if(dataEncerramento != null) {
			this.dataEncerramento = (Date) dataEncerramento.clone();
		}
		this.descricao = descricao;
	}
	/**
	 * Construtor da classe ServicoAutorizacaoRelatorioVO
	 */
	public ServicoAutorizacaoRelatorioVO(){
		super();
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	/**
	 * @param nomeCliente
	 */
	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public Date getDataGerada() {
		Date data = null;
		if (this.dataGerada != null) {
			data = (Date) dataGerada.clone();
		}
		return data;
	}
	
	/**
	 * @param dataGerada
	 */
	public void setDataGerada(Date dataGerada) {
		if (dataGerada != null) {
			this.dataGerada = (Date) dataGerada.clone();
		} else {
			this.dataGerada = null;
		}
	}

	public Date getDataEncerramento() {
		Date data = null;
		if (this.dataEncerramento != null) {
			data = (Date) dataEncerramento.clone();
		}
		return data;
	}
	
	/**
	 * @param dataEncerramento
	 */
	public void setDataEncerramento(Date dataEncerramento) {
		if (dataEncerramento != null) {
			this.dataEncerramento = (Date) dataEncerramento.clone();
		} else {
			this.dataEncerramento = null;
		}
	}
	
	public String getDescricao() {
	
		return descricao;
	}

	/**
	 * @param descricao
	 */
	public void setDescricao(String descricao) {
	
		this.descricao = descricao;
	}
}
