package br.com.ggas.relatorio;

import java.io.Serializable;

public class ServicoAutorizacaoResumoVO implements Serializable {

	private static final long serialVersionUID = -6115623455901953657L;
	
	private String servicoTipo;
	private Integer totalAberto;
	private Integer totalExecutada;
	private Integer totalPendenteAtualizacao;
	private Integer totalCancelada;
	private Integer totalEncerrada;
	private Integer total;
	
	public String getServicoTipo() {
		return servicoTipo;
	}
	public void setServicoTipo(String servicoTipo) {
		this.servicoTipo = servicoTipo;
	}
	public Integer getTotalAberto() {
		return totalAberto;
	}
	public void setTotalAberto(Integer totalAberto) {
		this.totalAberto = totalAberto;
	}
	public Integer getTotalExecutada() {
		return totalExecutada;
	}
	public void setTotalExecutada(Integer totalExecutada) {
		this.totalExecutada = totalExecutada;
	}
	public Integer getTotalPendenteAtualizacao() {
		return totalPendenteAtualizacao;
	}
	public void setTotalPendenteAtualizacao(Integer totalPendenteAtualizacao) {
		this.totalPendenteAtualizacao = totalPendenteAtualizacao;
	}
	public Integer getTotalCancelada() {
		return totalCancelada;
	}
	public void setTotalCancelada(Integer totalCancelada) {
		this.totalCancelada = totalCancelada;
	}
	public Integer getTotalEncerrada() {
		return totalEncerrada;
	}
	public void setTotalEncerrada(Integer totalEncerrada) {
		this.totalEncerrada = totalEncerrada;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
}
