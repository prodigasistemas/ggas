package br.com.ggas.relatorio.impl;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.relatorio.ControladorRelatorioClientesLigadosPrimeiraVez;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.web.relatorio.clientesligadosprimeiravez.ClientesLigadosPrimeiraVezTotalizadorVO;
import br.com.ggas.web.relatorio.clientesligadosprimeiravez.ClientesLigadosPrimeiraVezVO;
import br.com.ggas.web.relatorio.clientesligadosprimeiravez.RepositorioClientesLigadosPrimeiraVez;

@Service("controladorRelatorioClientesLigadosPrimeiraVez")
@Transactional
public class ControladorRelatorioClientesLigadosPrimeiraVezImpl implements ControladorRelatorioClientesLigadosPrimeiraVez {

	
	@Autowired
	private RepositorioClientesLigadosPrimeiraVez repositorioClientesLigadosPrimeiraVez;
	
	@Override
	public Class<?> getClasseEntidadeHistoricoOperacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoOperacaoMedidor.BEAN_ID_HISTORICO_OPERACAO_MEDIDOR);
//		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}
	
	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public byte[] gerarRelatorio(Date dataInicial, Date dataFinal, String exibirFiltros, String tipoRelatorio)
			throws GGASException {
		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(tipoRelatorio);
		Collection<ClientesLigadosPrimeiraVezVO> collRelatorio = obterClientesLigadosPelaPrimeiraVez(dataInicial,dataFinal);
		
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		String dataParamInicio = null;
		String artigo = null;
		if(dataInicial != null){
			dataParamInicio = sf.format(dataInicial);
			artigo = "à";
			
		}
		String dataParamFim = null;
		if(dataFinal != null){
			dataParamFim = sf.format(dataFinal);
		}
		
		String dataImpressao = sf.format(Calendar.getInstance().getTime());
		Map parametros = new HashMap();
		parametros.put("dataInicial", dataParamInicio);
		parametros.put("dataFinal", dataParamFim);
		parametros.put("dataImpressao", dataImpressao);
		parametros.put("formatoImpressao", formatoImpressao);
		parametros.put("artigo", artigo);
		parametros.put("listaPontosConsumo", collRelatorio);
		
	    ControladorEmpresa controladorEmpresa =
	            (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		
	    parametros.put("imagem",
	            Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
		
		this.validarDadosGerarRelatorioClientesLigadosPrimeiraVez(parametros);
		
		if(collRelatorio.isEmpty()) {
			collRelatorio = null;
		}
		
	    Collection<ClientesLigadosPrimeiraVezTotalizadorVO> listaTotalizador = this.calcularQuantidadeSegmento(collRelatorio);
	    
	    parametros.put("listaTotalizador", listaTotalizador);
		
	    Collection<ClientesLigadosPrimeiraVezVO> dados = new ArrayList<ClientesLigadosPrimeiraVezVO>();
	    dados.add(collRelatorio.iterator().next());
		
		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_CLIENTES_LIGADOS_PRIMEIRA_VEZ_JASPER, formatoImpressao);
	}

	
	/**
	 * Validar dados gerar relatorio clientes ligados primeira vez
	 * 
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosGerarRelatorioClientesLigadosPrimeiraVez(Map<String, Object> parametros) throws NegocioException {

		if (parametros.get("formatoImpressao") == null) {

			throw new NegocioException(ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO, true);

		}

	}
	
	private Collection<ClientesLigadosPrimeiraVezVO> obterClientesLigadosPelaPrimeiraVez(Date dataInicial, Date dataFinal){
		return this.repositorioClientesLigadosPrimeiraVez.obterHistoricoOperacaoMedidorClientesLigadosPrimeiraVez(dataInicial, dataFinal);
	}

    private Collection<ClientesLigadosPrimeiraVezTotalizadorVO> calcularQuantidadeSegmento(
            Collection<ClientesLigadosPrimeiraVezVO> collRelatorio) {
        Collection<ClientesLigadosPrimeiraVezTotalizadorVO> totalizador = new ArrayList<ClientesLigadosPrimeiraVezTotalizadorVO>();
        
        Map<String, Long> mapaTotalizadorSegmentos = collRelatorio.stream()
                .collect(Collectors.groupingBy(ClientesLigadosPrimeiraVezVO::getSegmento, Collectors.counting()));		
        
        for(Entry<String, Long> entry : mapaTotalizadorSegmentos.entrySet()) {
        	ClientesLigadosPrimeiraVezTotalizadorVO pontoConsumoTotalizador = new ClientesLigadosPrimeiraVezTotalizadorVO();
            pontoConsumoTotalizador.setDescricaoTotalizador(entry.getKey());
            pontoConsumoTotalizador.setQuantidadeTotalizador(entry.getValue());
        
            totalizador.add(pontoConsumoTotalizador);
        }
        
        totalizador = totalizador.stream().sorted(Comparator.comparing(p -> p.getDescricaoTotalizador()))
                .collect(Collectors.toList());		
        
        ClientesLigadosPrimeiraVezTotalizadorVO pontoConsumoTotalizador = new ClientesLigadosPrimeiraVezTotalizadorVO();
        pontoConsumoTotalizador.setDescricaoTotalizador("TOTAL");
        pontoConsumoTotalizador.setQuantidadeTotalizador(Long.valueOf(collRelatorio.size()));

        
        totalizador.add(pontoConsumoTotalizador);
        
        return totalizador;
    }
}
