package br.com.ggas.relatorio.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;

import br.com.ggas.relatorio.ControladorRelatorioMedidores;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;

import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.web.relatorio.medidores.MedidoresVO;
import br.com.ggas.web.relatorio.medidores.RepositorioMedidores;
import br.com.ggas.web.relatorio.medidores.SubstituicaoMedidorVO;


@Service("controladorRelatorioMedidores")
@Transactional
public class ControladorRelatorioMedidoresImpl implements ControladorRelatorioMedidores {


	
	public static final String ID_IMOVEL = "idImovel";

	
	@Autowired
	private RepositorioMedidores repositorioMedidores;
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	public byte[] gerarRelatorio(Date dataInicial, Date dataFinal, String exibirFiltros, String tipoRelatorio, String numeroSerie, 
			String marcaMedidor, String tipoMedidor, String modoUso, String modelo, String localArmazenagem
			, String habilitado, String anoFabricacao)
			throws GGASException {
		//String modelo, String tipoMedidor, String modeloMedidor, String situacaoMedidor, String habilitado
		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(tipoRelatorio);
		Collection<MedidoresVO> collRelatorio = obterMedidoresPorData(dataInicial, dataFinal, exibirFiltros, tipoRelatorio,
				numeroSerie, marcaMedidor, tipoMedidor, modoUso, modelo, localArmazenagem, habilitado, anoFabricacao);
		
		if(collRelatorio.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO, true);
		}
		
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		String dataParamInicio = null;
		String artigo = null;
		if(dataInicial != null){
			dataParamInicio = sf.format(dataInicial);
			artigo = "à";
			
		}
		String dataParamFim = null;
		if(dataFinal != null){
			dataParamFim = sf.format(dataFinal);
		}
		
		String dataImpressao = sf.format(Calendar.getInstance().getTime());
		Map parametros = new HashMap();
		parametros.put("dataInicial", dataParamInicio);
		parametros.put("dataFinal", dataParamFim);
		parametros.put("dataImpressao", dataImpressao);
		parametros.put("formatoImpressao", formatoImpressao);
		parametros.put("artigo", artigo);
		parametros.put("listagemMedidores", collRelatorio);

		validarDadosGerarRelatorioMedidores(parametros);
		
		Collection<MedidoresVO> dados = new ArrayList<MedidoresVO>();
		dados.add(collRelatorio.iterator().next());
		
		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_MEDIDORES, formatoImpressao);

	}
	
	
	private Collection<MedidoresVO> obterMedidoresPorData(Date dataInicial, Date dataFinal, String exibirFiltros, 
			String tipoRelatorio,String numeroSerie, String marcaMedidor, String tipoMedidor, String modoUso, String modelo, String localArmazenagem
			, String habilitado, String anoFabricacao){
		return this.repositorioMedidores.listarMedidoresPorData(dataInicial, dataFinal, numeroSerie, marcaMedidor, tipoMedidor, modoUso, modelo,
		 localArmazenagem, habilitado, anoFabricacao);
	}

	
	/**
	 * Validar dados gerar relatorio pontos de consumo por data.
	 * 
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosGerarRelatorioMedidores(Map<String, Object> parametros) throws NegocioException {

		if (parametros.get("formatoImpressao") == null) {

			throw new NegocioException(ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO, true);

		}

	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	public byte[] gerarRelatorio(Date dataInicial, Date dataFinal, String exibirFiltros, String tipoRelatorio, String operacao, 
			List<Long> equipe, List<Long> servico, String tipoRelacaoRelatorio, Map<String,Object> filtros) throws GGASException {
		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(tipoRelatorio);
		Collection<SubstituicaoMedidorVO> collRelatorio = obterSubstituicaoMedidor(dataInicial, dataFinal, exibirFiltros, operacao, equipe, servico);
		
		if(collRelatorio.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO, true);
		}
		
		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		String dataParamInicio = null;
		if(dataInicial != null){
			dataParamInicio = sf.format(dataInicial);
			
		}
		String dataParamFim = null;
		if(dataFinal != null){
			dataParamFim = sf.format(dataFinal);
		}
		  ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
				    ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		
		Map parametros = new HashMap();
		parametros.put("dataInicial", dataParamInicio);
		parametros.put("dataFinal", dataParamFim);
		parametros.put("formatoImpressao", formatoImpressao);
		parametros.put("listagemSubstituicao", collRelatorio);
		parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria());
		parametros.put("filtros",filtros);
		parametros.put("totalizadorMedidores",collRelatorio.size());
		validarDadosGerarRelatorioMedidores(parametros);
		
		Collection<SubstituicaoMedidorVO> dados = new ArrayList<SubstituicaoMedidorVO>();
		dados.add(collRelatorio.iterator().next());
		if(operacao != null) {
			if(operacao.equals("Substituição")) {
				parametros.put("tipoOperacao","Substituídos");
			}else {
				parametros.put("tipoOperacao","Retirados");
			}
		}else {
			parametros.put("tipoOperacao", "Substituídos e Retirados");
		}
		if(tipoRelacaoRelatorio.equals("listagem")) {
			return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_RETIRADA_MEDIDOR, formatoImpressao);
		}else {
			return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_SUBSTITUICAO_MEDIDOR, formatoImpressao);
		}
	}
	
	public Collection<SubstituicaoMedidorVO> obterSubstituicaoMedidor(Date dataInicial, Date dataFinal, String exibirFiltros, String operacao, List<Long> equipe, List<Long> servico){
		return this.repositorioMedidores.listarSubstituicaoMedidores(dataInicial, dataFinal, operacao, equipe, servico);
	}
}
