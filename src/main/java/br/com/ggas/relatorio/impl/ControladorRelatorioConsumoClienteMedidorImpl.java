/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MedidorVirtualVO;
import br.com.ggas.relatorio.ConsumoDiarioMedidorVO;
import br.com.ggas.relatorio.ConsumoDiarioVO;
import br.com.ggas.relatorio.ControladorRelatorioConsumoClienteMedidor;
import br.com.ggas.relatorio.InformacaoClientePontoConsumo;
import br.com.ggas.util.Agrupador;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.Pair;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.Util;

public class ControladorRelatorioConsumoClienteMedidorImpl extends ControladorNegocioImpl
implements ControladorRelatorioConsumoClienteMedidor {	

	private static final String ID_MEDIDOR = "idMedidor";
	private static final String SUBTRACAO = "-";
	private static final String SOMA = "+";
	private static final String COMPOSICAO_VIRTUAL = "composicaoVirtual";
	private static final String NOME_CLIENTE = "nomeCliente";
	private static final String DESC_PONTO_CONSUMO = "descPontoConsumo";
	private static final String EXIBIR_FILTROS = "exibirFiltros";
	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";
	private static final String ANO_MES_FATURAMENTO = "anoMesFaturamento";
	private static final String NUMERO_CICLO = "numeroCiclo";
	private static final String DATA_FINAL_CONSUMO = "dataFinalConsumo";
	private static final String DATA_INICIAL_CONSUMO = "dataInicialConsumo";
	private static final String APLICACAO_FORMULA = "aplicacaoFormula";
	
	private static Fachada fachada = Fachada.getInstancia();

	@Override
	public byte[] gerarRelatorio(Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException {

		Map<String, Object> parametros = new HashMap<String, Object>();
		byte[] relatorioConsumoClienteMedidor;
		
		Empresa empresa = fachada.obterEmpresaPrincipal();
		
		if (empresa!= null && empresa.getLogoEmpresa() != null) {
			parametros.put("imagem",
					Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
		}
		
		List<ConsumoDiarioMedidorVO> listaConsumoDiarioMedidor = this.montarListaConsumoDiarioMedidor(filtro);
		this.montarParametrosFiltro(parametros, filtro);

		
		if (!Util.isNullOrEmpty(listaConsumoDiarioMedidor)) {
			relatorioConsumoClienteMedidor =
					RelatorioUtil.gerarRelatorio(listaConsumoDiarioMedidor, parametros, ARQUIVO_RELATORIO_CONSUMO, formatoImpressao);
		} else {
			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return relatorioConsumoClienteMedidor;
	}

	private List<ConsumoDiarioMedidorVO> montarListaConsumoDiarioMedidor(Map<String, Object> filtro) throws GGASException {
		Long idPontoConsumo = (Long) filtro.get(ID_PONTO_CONSUMO);
		Long idMedidor = (Long) filtro.get(ID_MEDIDOR);
		String anoMesFaturamento = (String) filtro.get(ANO_MES_FATURAMENTO);
		Integer numeroCiclo = (Integer) filtro.get(NUMERO_CICLO);
		
		List<HistoricoConsumo> listaHistoricoConsumo = new ArrayList<HistoricoConsumo>();
		
		Medidor medidor = fachada.obterMedidor(idMedidor);
		if (medidor != null && StringUtils.isNotBlank(medidor.getComposicaoVirtual())) {
			List<MedidorVirtualVO> listaMedidoresIndependentes = fachada.obterComposicaoMedidorVirtual(medidor.getComposicaoVirtual());
			if (!Util.isNullOrEmpty(listaMedidoresIndependentes)) {
				List<String> listaEnderecoRemoto = new ArrayList<String>();
				for (MedidorVirtualVO medidorIndependente : listaMedidoresIndependentes) {
					listaEnderecoRemoto.add(medidorIndependente.getEnderecoRemoto());
				}
				listaHistoricoConsumo = new ArrayList<>(fachada.obterHistoricoConsumoPorMedidoresIndependentes(
						idPontoConsumo, Integer.valueOf(anoMesFaturamento), numeroCiclo));
			}
			if (!Util.isNullOrEmpty(listaHistoricoConsumo)) {
				
				filtro.put(DATA_INICIAL_CONSUMO, listaHistoricoConsumo.get(0).getHistoricoAtual().getDataLeituraInformada());
				filtro.put(DATA_FINAL_CONSUMO, listaHistoricoConsumo.get(listaHistoricoConsumo.size() - 1)
						.getHistoricoAtual().getDataLeituraInformada());
				filtro.put(COMPOSICAO_VIRTUAL, medidor.getComposicaoVirtual());
				
				Map<Pair<Long, String>, Collection<HistoricoConsumo>> 
				historicoConsumoMedidores = agruparHistoricoConsumoPorMedidor(listaHistoricoConsumo);
				
				filtro.put(APLICACAO_FORMULA, realizarAplicacaoFormulaComposicaoVirtual(historicoConsumoMedidores, filtro));
				
				List<ConsumoDiarioMedidorVO> listaConsumoDiarioMedidor = new ArrayList<ConsumoDiarioMedidorVO>();
				
				Iterator<Entry<Pair<Long, String>, Collection<HistoricoConsumo>>> it = historicoConsumoMedidores.entrySet().iterator();
				while (it.hasNext()) {
					Entry<Pair<Long, String>, Collection<HistoricoConsumo>> historicoConsumo = it.next();
					
					ConsumoDiarioMedidorVO consumoDiario = new ConsumoDiarioMedidorVO();
					consumoDiario.setIdMedidor(historicoConsumo.getKey().getFirst());
					consumoDiario.setNumeroSerie(historicoConsumo.getKey().getSecond());
					
					if (!Util.isNullOrEmpty(historicoConsumo.getValue())) {
						List<ConsumoDiarioVO> listaConsumoDiario = new ArrayList<ConsumoDiarioVO>();
						for (HistoricoConsumo elemento : historicoConsumo.getValue()) {
							listaConsumoDiario.add(
									new ConsumoDiarioVO(
											elemento.getChavePrimaria(), 
											elemento.getHistoricoAtual().getDataLeituraInformada(), 
											elemento.getConsumoApurado()));
						}
						consumoDiario.setListaConsumoDiario(listaConsumoDiario);
					}
					
					listaConsumoDiarioMedidor.add(consumoDiario);
				}
				Collections.sort(listaConsumoDiarioMedidor);
				return listaConsumoDiarioMedidor;
			}
		}
		
		return Collections.emptyList();
	}
	
	private Map<Pair<Long, String>, Collection<HistoricoConsumo>> 
	agruparHistoricoConsumoPorMedidor(List<HistoricoConsumo> listaHistoricoConsumo){
		if (!Util.isNullOrEmpty(listaHistoricoConsumo)) {
			return Util.agrupar(listaHistoricoConsumo, new Agrupador<HistoricoConsumo, Pair<Long, String>>(){
				
				@Override
				public Pair<Long, String> getChaveAgrupadora(HistoricoConsumo elemento) {
					return new Pair<Long, String>(
							elemento.getHistoricoAtual().getHistoricoInstalacaoMedidor().getMedidor().getChavePrimaria()
						   ,elemento.getHistoricoAtual().getHistoricoInstalacaoMedidor().getMedidor().getNumeroSerie());
				}
			});
		}
		return MapUtils.EMPTY_MAP;
	}
	
	/**
	 * Montar parametros filtro.
	 * 
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void montarParametrosFiltro(Map<String, Object> parametros, Map<String, Object> filtro) throws GGASException {
		Long idPontoConsumo = (Long) filtro.get(ID_PONTO_CONSUMO);
		String anoMesFaturamento = (String) filtro.get(ANO_MES_FATURAMENTO);
		Integer numeroCiclo = (Integer) filtro.get(NUMERO_CICLO);
		Date dataInicialConsumo = (Date) filtro.get(DATA_INICIAL_CONSUMO);
		Date dataFinalConsumo = (Date) filtro.get(DATA_FINAL_CONSUMO);
		BigDecimal aplicacaoFormula = (BigDecimal) filtro.get(APLICACAO_FORMULA);

		Boolean exibirFiltro = (Boolean) filtro.get(EXIBIR_FILTROS);
		parametros.put(EXIBIR_FILTROS, exibirFiltro);
		
		if (dataInicialConsumo != null && dataFinalConsumo != null && DataUtil.menorOuIgualQue(dataInicialConsumo, dataFinalConsumo)) {
			parametros.put(DATA_INICIAL_CONSUMO, dataInicialConsumo);
			parametros.put(DATA_FINAL_CONSUMO, dataFinalConsumo);
		}
	
		if(idPontoConsumo != null) {
			parametros.put(ID_PONTO_CONSUMO, idPontoConsumo);
			
			InformacaoClientePontoConsumo informacaoClientePontoConsumo = obterInformacoesClientePontoConsumo(idPontoConsumo);
			
			if (informacaoClientePontoConsumo != null) {
				parametros.put(DESC_PONTO_CONSUMO, informacaoClientePontoConsumo.getDescPontoConsumo());
				parametros.put(NOME_CLIENTE, informacaoClientePontoConsumo.getNomeCliente());			
			}
		}
		
		if(StringUtils.isNotBlank(anoMesFaturamento)) {
			parametros.put(ANO_MES_FATURAMENTO, anoMesFaturamento);
		}
		
		if(numeroCiclo != null) {
			parametros.put(NUMERO_CICLO,numeroCiclo);
		}
		
		if (aplicacaoFormula != null) {
			parametros.put(APLICACAO_FORMULA, aplicacaoFormula);
		}
	}
	
	private InformacaoClientePontoConsumo obterInformacoesClientePontoConsumo(Long idPontoConsumo){
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT distinct ");
		sql.append("    pocn.pocn_cd as idPontoConsumo, ");
		sql.append("    pocn.pocn_ds as descPontoConsumo, ");
		sql.append("    cl.clie_nm as nomeCliente ");
		sql.append(" FROM ");
		sql.append("    ponto_consumo pocn ");
		sql.append("    INNER JOIN imovel imo ON pocn.imov_cd = imo.imov_cd ");
		sql.append("    INNER JOIN cliente_imovel clim ON imo.imov_cd = clim.imov_cd ");
		sql.append("    INNER JOIN cliente cl ON clim.clie_cd = cl.clie_cd ");
		sql.append(" WHERE pocn.pocn_cd = :idPontoConsumo ");
		
		SQLQuery query = createSQLQuery(sql.toString());
		query.setLong(ID_PONTO_CONSUMO, idPontoConsumo);
		
		query.addScalar(ID_PONTO_CONSUMO, LongType.INSTANCE);
		query.addScalar(DESC_PONTO_CONSUMO, StringType.INSTANCE);
		query.addScalar(NOME_CLIENTE, StringType.INSTANCE);
		
		query.setResultTransformer(Transformers.aliasToBean(InformacaoClientePontoConsumo.class));		
		
		return (InformacaoClientePontoConsumo) query.uniqueResult();
	}
	
	private BigDecimal realizarAplicacaoFormulaComposicaoVirtual(Map<Pair<Long, String>, 
			Collection<HistoricoConsumo>> colecaoHistConsumoAgrupadoMedidor, Map<String, Object> filtro) 
					throws GGASException {
		BigDecimal valorTotalFormula = BigDecimal.ZERO;
		String composicaoVirtual = (String) filtro.get(COMPOSICAO_VIRTUAL);
		
		if (StringUtils.isNotBlank(composicaoVirtual)) {
			List<MedidorVirtualVO> listaMedidoresComposicaoVirtual = fachada.obterComposicaoMedidorVirtual(composicaoVirtual);
			
			for (MedidorVirtualVO medidorIndependente : listaMedidoresComposicaoVirtual) {
				if (colecaoHistConsumoAgrupadoMedidor != null && !colecaoHistConsumoAgrupadoMedidor.isEmpty()) {
					List<HistoricoConsumo> listaConsumo = obterListaConsumoAplicadoPorMedidorIndependente(
							colecaoHistConsumoAgrupadoMedidor, medidorIndependente.getId(), medidorIndependente.getNumeroSerie());
					
					for (HistoricoConsumo histConsumo : listaConsumo) {
						if (SOMA.equals(medidorIndependente.getOperacao())) {
							valorTotalFormula = valorTotalFormula.add(histConsumo.getConsumoApurado());
						} else if (SUBTRACAO.equals(medidorIndependente.getOperacao())){
							valorTotalFormula = valorTotalFormula.add(BigDecimal.valueOf(-1).multiply(histConsumo.getConsumoApurado()));
						}
					}					
				}
			}
		}
		return valorTotalFormula;
	}

	private List<HistoricoConsumo> obterListaConsumoAplicadoPorMedidorIndependente(Map<Pair<Long, String>,
			Collection<HistoricoConsumo>> colecaoHistConsumo, Long idMedidorVirtual, String numSerie) {
		Collection<HistoricoConsumo> colhistMedVirtual = colecaoHistConsumo.get(new Pair<Long, String>(idMedidorVirtual, numSerie));
		
		if (!Util.isNullOrEmpty(colhistMedVirtual)){
			return new ArrayList<HistoricoConsumo>(colhistMedVirtual);
		}
		
		return new ArrayList<HistoricoConsumo>();
	}

	@Override
	public EntidadeNegocio criar() {
		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {
		return null;
	}

}
