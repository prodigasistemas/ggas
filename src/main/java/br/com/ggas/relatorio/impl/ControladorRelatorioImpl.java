package br.com.ggas.relatorio.impl;

import java.util.Collection;

import org.hibernate.Query;

import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.relatorio.ControladorRelatorio;
import br.com.ggas.relatorio.RelatorioToken;
import br.com.ggas.util.ServiceLocator;

public class ControladorRelatorioImpl extends ControladorNegocioImpl implements ControladorRelatorio{

	@Override
	public EntidadeNegocio criar() {
		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {
		return ServiceLocator.getInstancia().getClassPorID(RelatorioToken.BEAN_ID_LAYOUT_RELATORIO);
	}
	
	@Override
	public Collection<RelatorioToken> obterListaRelatorioTokens(Long chaveTipoRelatorio) {

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT relatorioToken ");
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" relatorioToken ");
		hql.append(" WHERE ");
		hql.append(" relatorioToken.tipoRelatorio.chavePrimaria = :chaveTipoRelatorio ");
		hql.append( " ORDER BY relatorioToken.descricao ASC ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveTipoRelatorio", chaveTipoRelatorio);
		
		return query.list();
	}

}
