/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.collections.CollectionUtils;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.leitura.ControladorLeiturista;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.relatorio.ControladorRelatorioRota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;

class ControladorRelatorioRotaImpl extends ControladorNegocioImpl implements ControladorRelatorioRota {

	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_ROTA_DADOS_PONTO_CONSUMO = new DynaProperty[] {

	new DynaProperty("sequencial", String.class), new DynaProperty("pontoConsumo", String.class), 
		new DynaProperty("situacao", String.class), new DynaProperty(
					"medidor", String.class), new DynaProperty("teste", String.class)

	};

	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_ROTA_DADOS_ROTA = new DynaProperty[] {

	new DynaProperty("descricaoRota", String.class), new DynaProperty("periodicidade", String.class), new DynaProperty("qtdaMedidores",
					Integer.class), new DynaProperty("descricaoGrupoFaturamento", String.class), new DynaProperty("dadosPontoConsumo",
					List.class)

	};

	private static final DynaProperty[] ATRIBUTOS_RELATORIO_ROTA = new DynaProperty[] {

	new DynaProperty("exibirFiltros", Boolean.class), new DynaProperty("imagem", String.class), 
		new DynaProperty("dadosRota", List.class), new DynaProperty(
					"tipoRota", String.class), new DynaProperty("periodicidade", String.class), new DynaProperty("tipoLeitura",
					String.class), new DynaProperty("numeroRota", String.class), 
		new DynaProperty("grupoFaturamento", String.class), new DynaProperty(
					"setorComercial", String.class), new DynaProperty("empresa", String.class), 
		new DynaProperty("leiturista", String.class), new DynaProperty(
					"indicadorUso", String.class)

	};

	private static final DynaClass SUB_RELATORIO_ROTA_DADOS_PONTO_CONSUMO = new BasicDynaClass("RelatorioRota", null,
					ATRIBUTOS_SUB_RELATORIO_ROTA_DADOS_PONTO_CONSUMO);

	private static final DynaClass SUB_RELATORIO_ROTA_DADOS_ROTA = new BasicDynaClass("RelatorioRota", null,
					ATRIBUTOS_SUB_RELATORIO_ROTA_DADOS_ROTA);

	private static final DynaClass RELATORIO_ROTA = new BasicDynaClass("RelatorioRota", null, ATRIBUTOS_RELATORIO_ROTA);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioRota#gerarRelatorio(java.util.Map)
	 */
	@Override
	public byte[] gerarRelatorio(Map<String, Object> parametros) throws GGASException {

		this.validarDadosGerarRelatorioRota(parametros);

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf((String) parametros.get("formatoImpressao"));
		Collection<Object> collRelatorio = new ArrayList<Object>();
		collRelatorio.add(this.obterDadosRelatorio(parametros));

		return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_ROTA_JASPER, formatoImpressao);

	}

	/**
	 * Validar dados gerar relatorio rota.
	 * 
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosGerarRelatorioRota(Map<String, Object> parametros) throws NegocioException {

		if (parametros.get("formatoImpressao") == null) {

			throw new NegocioException(ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO, true);

		}

	}

	/**
	 * Obter dados relatorio.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the object
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Object obterDadosRelatorio(Map<String, Object> parametros) throws GGASException {

		String urlLogoEmpresa = Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(this.obterEmpresaPrincipal().getChavePrimaria());
		Boolean exibirFiltros = (Boolean) parametros.get("exibirFiltros");

		List<Object> dadosRota = this.obterDadosRota(parametros);

		DynaBean relatorio = new BasicDynaBean(RELATORIO_ROTA);
		if (this.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			relatorio.set("imagem", urlLogoEmpresa);
		}
		relatorio.set("exibirFiltros", exibirFiltros);
		relatorio.set("dadosRota", dadosRota);

		if (exibirFiltros) {

			Long idPeriodicidade = (Long) parametros.get("idPeriodicidade");
			Long idTipoLeitura = (Long) parametros.get("idTipoLeitura");
			String numeroRota = (String) parametros.get("numeroRota");
			Long idGrupoFaturamento = (Long) parametros.get("idGrupoFaturamento");
			Long idSetorComercial = (Long) parametros.get("idSetorComercial");
			Long idEmpresa = (Long) parametros.get("idEmpresa");
			Long idLeiturista = (Long) parametros.get("idLeiturista");
			Boolean habilitado = (Boolean) parametros.get("habilitado");

			relatorio.set("tipoRota", "");

			if (idPeriodicidade != null) {
				ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
				Periodicidade periodicidade = controladorRota.obterPeriodicidade(idPeriodicidade);
				relatorio.set("periodicidade", periodicidade.getDescricao());
			} else {
				relatorio.set("periodicidade", "");
			}

			if (idTipoLeitura != null) {
				ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
				TipoLeitura tipoLeitura = controladorRota.obterTipoLeitura(idTipoLeitura);
				relatorio.set("tipoLeitura", tipoLeitura.getDescricao());
			} else {
				relatorio.set("tipoLeitura", "");
			}

			if (numeroRota != null) {
				relatorio.set("numeroRota", numeroRota);
			} else {
				relatorio.set("numeroRota", "");
			}

			if (idGrupoFaturamento != null) {
				ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
				GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(idGrupoFaturamento);
				relatorio.set("grupoFaturamento", grupoFaturamento.getDescricao());
			} else {
				relatorio.set("grupoFaturamento", "");
			}

			if (idSetorComercial != null) {
				ControladorSetorComercial controladorSetorComercial = (ControladorSetorComercial) ServiceLocator.getInstancia()
								.getControladorNegocio(ControladorSetorComercial.BEAN_ID_CONTROLADOR_SETOR_COMERCIAL);
				SetorComercial setorComercial = (SetorComercial) controladorSetorComercial.obter(idSetorComercial);
				relatorio.set("setorComercial", setorComercial.getDescricao());
			} else {
				relatorio.set("setorComercial", "");
			}

			if (idEmpresa != null) {
				ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
				Empresa empresa = (Empresa) controladorEmpresa.obter(idEmpresa);
				relatorio.set("empresa", empresa.getCliente().getNome());
			} else {
				relatorio.set("empresa", "");
			}

			if (idLeiturista != null) {
				ControladorLeiturista controladorLeiturista = (ControladorLeiturista) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorLeiturista.BEAN_ID_CONTROLADOR_LEITURISTA);
				Leiturista leiturista = (Leiturista) controladorLeiturista.obter(idLeiturista);
				relatorio.set("leiturista", leiturista.getFuncionario().getNome());
			} else {
				relatorio.set("leiturista", "");
			}

			if (habilitado != null) {

				if (habilitado) {

					relatorio.set("indicadorUso", "Ativo");

				} else {

					relatorio.set("indicadorUso", "Inativo");

				}

			} else {

				relatorio.set("indicadorUso", "Todos");

			}

		}

		return relatorio;

	}

	/**
	 * Obter dados rota.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the list
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private List<Object> obterDadosRota(Map<String, Object> parametros) throws GGASException {

		Map<Long, Collection<PontoConsumo>> map = this.obterPontosConsumoPorRota(parametros);

		List<Object> listaSubRelatorioRota = new ArrayList<Object>();

		for (Map.Entry<Long, Collection<PontoConsumo>> entry : map.entrySet()) {

			Collection<PontoConsumo> listaPontos = entry.getValue();

			String descricaoRota = null;
			String periodicidade = null;
			Integer qtdaMedidores = 0;
			String descricaoGrupoFaturamento = "";

			List<Object> listaSubRelatorioPontoConsumo = new ArrayList<Object>();
			if (!CollectionUtils.isEmpty(listaPontos)) {
				for (PontoConsumo pontoConsumo : listaPontos) {

					DynaBean subRelatorioPontoConsumo = new BasicDynaBean(SUB_RELATORIO_ROTA_DADOS_PONTO_CONSUMO);
					subRelatorioPontoConsumo.set(
									"sequencial",
									this.obterSequencialLeitura(pontoConsumo.getImovel().getNumeroSequenciaLeitura(),
													pontoConsumo.getNumeroSequenciaLeitura()));
					subRelatorioPontoConsumo.set("pontoConsumo", pontoConsumo.getImovel().getNome() + " - " + pontoConsumo.getDescricao());
					subRelatorioPontoConsumo.set("situacao", pontoConsumo.getSituacaoConsumo().getDescricao());
					if (pontoConsumo.getInstalacaoMedidor() != null) {
						subRelatorioPontoConsumo.set("medidor", pontoConsumo.getInstalacaoMedidor().getMedidor().getNumeroSerie());
					}

					listaSubRelatorioPontoConsumo.add(subRelatorioPontoConsumo);

					descricaoRota = pontoConsumo.getRota().getNumeroRota();
					periodicidade = pontoConsumo.getRota().getPeriodicidade().getDescricao();

					if (pontoConsumo.getInstalacaoMedidor() != null && pontoConsumo.getInstalacaoMedidor().getMedidor() != null) {
						qtdaMedidores = qtdaMedidores + 1;
					}

					descricaoGrupoFaturamento = pontoConsumo.getRota().getGrupoFaturamento().getDescricao();

				}
			}

			DynaBean subRelatorioDadosRota = new BasicDynaBean(SUB_RELATORIO_ROTA_DADOS_ROTA);
			subRelatorioDadosRota.set("descricaoRota", descricaoRota);
			subRelatorioDadosRota.set("periodicidade", periodicidade);
			subRelatorioDadosRota.set("qtdaMedidores", qtdaMedidores);
			subRelatorioDadosRota.set("descricaoGrupoFaturamento", descricaoGrupoFaturamento);
			subRelatorioDadosRota.set("dadosPontoConsumo", listaSubRelatorioPontoConsumo);

			listaSubRelatorioRota.add(subRelatorioDadosRota);

		}

		return listaSubRelatorioRota;

	}

	/**
	 * Obter sequencial leitura.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @return the string
	 */
	private String obterSequencialLeitura(Integer x, Integer y) {

		StringBuilder sequencial = new StringBuilder();

		if (x != null) {

			sequencial.append(x);

		}

		if (x != null && y != null) {

			sequencial.append(" - ");

		}

		if (y != null) {

			String sequencialPc = String.valueOf(y);

			while (sequencialPc.length() < 6) {

				sequencialPc = " " + sequencialPc;

			}

			sequencial.append(sequencialPc);

		} else {

			sequencial.append("      ");

		}

		return sequencial.toString();
	}

	/**
	 * Obter pontos consumo por rota.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Map<Long, Collection<PontoConsumo>> obterPontosConsumoPorRota(Map<String, Object> parametros) throws GGASException {

		Map<Long, Collection<PontoConsumo>> mapRotaPontoConsumo = new TreeMap<Long, Collection<PontoConsumo>>();

		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorRota.BEAN_ID_CONTROLADOR_ROTA);

		Collection<Rota> rotas = controladorRota.consultarRota(parametros);

		for (Rota rota : rotas) {
			Collection<PontoConsumo> listaPontoConsumo = controladorPontoConsumo.carregarPontosConsumoAssociados(rota, null, null)
							.getFirst();
			mapRotaPontoConsumo.put(rota.getChavePrimaria(), listaPontoConsumo);
		}

		return mapRotaPontoConsumo;
	}

	/**
	 * Obter empresa principal.
	 * 
	 * @return the empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Empresa obterEmpresaPrincipal() throws NegocioException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("principal", Boolean.TRUE);
		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);

		Empresa empresa = null;
		if ((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
			empresa = ((List<Empresa>) listaEmpresas).get(0);
		}

		return empresa;
	}

}
