/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.relatorio.ControladorRelatorioAnaliseDemanda;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorRelatorioAnaliseDemandaImpl extends ControladorNegocioImpl implements ControladorRelatorioAnaliseDemanda {

	private static final String DATA_INICIAL = "dataInicial";
	
	private static final String DATA_FINAL = "dataFinal";
	
	private static final String CLIENTE = "cliente";
	
	private static final String IMOVEL = "imovel";
	
	private static final String PONTOS_CONSUMO = "pontosConsumo";
	
	private static final String SEGMENTOS = "segmentos";
	
	private static final String RAMOS_ATIVIDADE = "ramosAtividade";
	
	private static final String CONSUMO_REALIZADO = "consumoRealizado";
	
	private static final String CONSUMO_PROGRAMADO = "consumoProgramado";
	
	private static final String DESCRICAO_SEGMENTO = "descricaoSegmento";
	
	private static final String VOL_MEDIO_ANUAL = "volMedioAnual";
	
	private static final String VOL_MEDIO_MENSAL = "volMedioMensal";
	
	private static final String VOL_MEDIO_DIARIO = "volMedioDiario";
	
	private static final String VOL_MEDIO_PRIMEIRO_DIA_SEMAMA = "volMedioPrimeiroDiaSemana";
	
	private static final String VOL_MEDIO_SEGUNDO_DIA_SEMANA = "volMedioSegundoDiaSemana";
	
	private static final String VOL_MEDIO_TERCEIRO_DIA_SEMANA = "volMedioTerceiroDiaSemana";
	
	private static final String VOL_MEDIO_QUARTO_DIA_SEMANA = "volMedioQuartoDiaSemana";
	
	private static final String VOL_MEDIO_QUINTO_DIA_SEMANA = "volMedioQuintoDiaSemana";
	
	private static final String VOL_MEDIO_SEXTO_DIA_SEMANA = "volMedioSextoDiaSemana";
	
	private static final String VOL_MEDIO_SETIMO_DIA_SEMANA = "volMedioSetimoDiaSemana";
	
	private static final String ID_SEGMENTO = "idSegmento";
	
	private static final String IMAGEM = "imagem";
	
	private static final String EXIBIR_FILTROS = "exibirFiltros";
	
	private static final String ID_CLIENTE = "idCliente";
	
	private static final String ID_IMOVEL = "idImovel";
	
	private static final String IDS_SEGMENTOS = "idsSegmentos";
	
	private static final String IDS_RAMOS_ATIVIDADES = "idsRamosAtividades";
	
	private static final String LINE_SEPARATOR = "line.separator";
	
	private static final String PONTO_CONSUMO = "pontoConsumo";
	
	private static final String SEGMENTO = "segmento";
	
	private static final String CHAVE_PRIMARIA = "chavePrimaria";
	
	private static final String DESCRICAO = "descricao";
	
	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_FILTRO = new DynaProperty[] {new DynaProperty(
					DATA_INICIAL, Date.class), new DynaProperty(DATA_FINAL, Date.class), new DynaProperty(CLIENTE, String.class), 
	                new DynaProperty(IMOVEL, String.class), new DynaProperty(PONTOS_CONSUMO, String.class), 
	                new DynaProperty(SEGMENTOS, String.class), new DynaProperty(RAMOS_ATIVIDADE, String.class)};

	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_GRAFICO = new DynaProperty[] {

	new DynaProperty("data", Date.class), new DynaProperty("valorConsumo", BigDecimal.class), new DynaProperty("valorProgramado",
					BigDecimal.class),

	};

	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_CONSUMO_PROGRAMADO = new DynaProperty[] {

	new DynaProperty("data", Date.class), new DynaProperty(CONSUMO_REALIZADO, BigDecimal.class), new DynaProperty(CONSUMO_PROGRAMADO,
					BigDecimal.class),

	};

	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_SEGMENTO = new DynaProperty[] {

	new DynaProperty(DESCRICAO_SEGMENTO, String.class), new DynaProperty(VOL_MEDIO_ANUAL, BigDecimal.class), new DynaProperty(
					VOL_MEDIO_MENSAL, BigDecimal.class), new DynaProperty(VOL_MEDIO_DIARIO, BigDecimal.class), new DynaProperty(
					VOL_MEDIO_PRIMEIRO_DIA_SEMAMA, BigDecimal.class), new DynaProperty(VOL_MEDIO_SEGUNDO_DIA_SEMANA, BigDecimal.class), new DynaProperty(
					VOL_MEDIO_TERCEIRO_DIA_SEMANA, BigDecimal.class), new DynaProperty(VOL_MEDIO_QUARTO_DIA_SEMANA, BigDecimal.class), new DynaProperty(
					VOL_MEDIO_QUINTO_DIA_SEMANA, BigDecimal.class), new DynaProperty(VOL_MEDIO_SEXTO_DIA_SEMANA, BigDecimal.class), new DynaProperty(
					VOL_MEDIO_SETIMO_DIA_SEMANA, BigDecimal.class)

	};

	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_PONTO_CONSUMO = new DynaProperty[] {

	new DynaProperty("descricaoPontoConsumo", String.class), new DynaProperty(ID_SEGMENTO, Long.class), new DynaProperty(
					DESCRICAO_SEGMENTO, String.class), new DynaProperty("descricaoRamoAtividades", String.class), new DynaProperty(
					VOL_MEDIO_ANUAL, BigDecimal.class), new DynaProperty(VOL_MEDIO_MENSAL, BigDecimal.class), new DynaProperty(
					VOL_MEDIO_DIARIO, BigDecimal.class), new DynaProperty(VOL_MEDIO_PRIMEIRO_DIA_SEMAMA, BigDecimal.class), new DynaProperty(
					VOL_MEDIO_SEGUNDO_DIA_SEMANA, BigDecimal.class), new DynaProperty(VOL_MEDIO_TERCEIRO_DIA_SEMANA, BigDecimal.class), new DynaProperty(
					VOL_MEDIO_QUARTO_DIA_SEMANA, BigDecimal.class), new DynaProperty(VOL_MEDIO_QUINTO_DIA_SEMANA, BigDecimal.class), new DynaProperty(
					VOL_MEDIO_SEXTO_DIA_SEMANA, BigDecimal.class), new DynaProperty(VOL_MEDIO_SETIMO_DIA_SEMANA, BigDecimal.class)

	};

	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_SEMANAIS = new DynaProperty[] {

	new DynaProperty("dataFinalApuracaoConsumo", Date.class), new DynaProperty("primeiroDiaSemana", Date.class), new DynaProperty(
					"segundoDiaSemana", Date.class), new DynaProperty("terceiroDiaSemana", Date.class), new DynaProperty("quartoDiaSemana",
					Date.class), new DynaProperty("quintoDiaSemana", Date.class), new DynaProperty("sextoDiaSemana", Date.class), new DynaProperty(
					"setimoDiaSemana", Date.class), new DynaProperty("dadosPontosConsumo", List.class), new DynaProperty("dadosSegmentos",
					List.class), new DynaProperty("dadosGrafico", List.class)

	};

	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_GERAIS = new DynaProperty[] {

	new DynaProperty(DATA_INICIAL, Date.class), new DynaProperty(IMAGEM, String.class), new DynaProperty("dadosSemanais", List.class),
	new DynaProperty("dadosConsumoProgramado", List.class)

	};

	private static final DynaProperty[] ATRIBUTOS_RELATORIO_ANALISE_DE_DEMANDA = new DynaProperty[] {

	new DynaProperty("dadosGerais", List.class), new DynaProperty(EXIBIR_FILTROS, Boolean.class), new DynaProperty(IMAGEM, String.class),
	new DynaProperty(DATA_INICIAL, Date.class), new DynaProperty(DATA_FINAL, Date.class), new DynaProperty(CLIENTE, String.class), 
	new DynaProperty(IMOVEL, String.class), new DynaProperty(PONTOS_CONSUMO, String.class), new DynaProperty(SEGMENTOS, String.class),
	new DynaProperty(RAMOS_ATIVIDADE, String.class)

	};

	private static final DynaClass SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_FILTRO = new BasicDynaClass(
					"subRelatorioAnaliseDemandaDadosFiltro", null, ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_FILTRO);

	private static final DynaClass SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_GRAFICO = new BasicDynaClass(
					"subRelatorioAnaliseDemandaDadosGrafico", null, ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_GRAFICO);

	private static final DynaClass SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_CONSUMO_PROGRAMADO = new BasicDynaClass(
					"subRelatorioAnaliseDemandaDadosConsumoProgramado", null,
					ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_CONSUMO_PROGRAMADO);

	private static final DynaClass SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_SEGMENTO = new BasicDynaClass(
					"subRelatorioAnaliseDemandaDadosSegmento", null, ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_SEGMENTO);

	private static final DynaClass SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_PONTO_CONSUMO = new BasicDynaClass(
					"subRelatorioAnaliseDemandaDadosPontoConsumo", null, ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_PONTO_CONSUMO);

	private static final DynaClass SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_SEMANAIS = new BasicDynaClass(
					"subRelatorioAnaliseDemandaDadosSemanais", null, ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_SEMANAIS);

	private static final DynaClass SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_GERAIS = new BasicDynaClass(
					"subRelatorioAnaliseDemandaDadosGerais", null, ATRIBUTOS_SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_GERAIS);

	private static final DynaClass RELATORIO_ANALISE_DE_DEMANDA = new BasicDynaClass("RelatorioAnaliseDemanda", null,
					ATRIBUTOS_RELATORIO_ANALISE_DE_DEMANDA);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioAnaliseDemanda#gerarRelatorio(java.util.Map)
	 */
	@Override
	public byte[] gerarRelatorio(Map<String, Object> parametros) throws NegocioException {

		this.validarDadosGerarRelatorioAnaliseDemanda(parametros);

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf((String) parametros.get("formatoImpressao"));
		String urlLogoEmpresa = null;
		if (this.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			urlLogoEmpresa = Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(this.obterEmpresaPrincipal().getChavePrimaria());
		}

		Date dataInicial = (Date) parametros.get(DATA_INICIAL);
		Date dataFinal = (Date) parametros.get(DATA_FINAL);

		Collection<Object> collRelatorio = new ArrayList<Object>();
		Collection<Object> collDadosGerais = new ArrayList<Object>();
		Map<Integer, List<Date[]>> mapa = this.apurarPorSemanaQuantidadeRelatorio(dataInicial, dataFinal);

		Long idCliente = (Long) parametros.get(ID_CLIENTE);
		Long idImovel = (Long) parametros.get(ID_IMOVEL);
		Long[] idsSegmentos = (Long[]) parametros.get(IDS_SEGMENTOS);
		Long[] idsRamosAtividades = (Long[]) parametros.get(IDS_RAMOS_ATIVIDADES);
		Long[] idsPontoConsumo = (Long[]) parametros.get("idsPonto");
		Boolean consumoProgramado = (Boolean) parametros.get(CONSUMO_PROGRAMADO);
		Boolean exibirGrafico = (Boolean) parametros.get("exibirGrafico");
		Boolean exibirFiltros = (Boolean) parametros.get(EXIBIR_FILTROS);

		for (Map.Entry<Integer, List<Date[]>> entry : mapa.entrySet()) {

			Object dadosGerais = this.obterDadosGerais(entry.getValue(), idCliente, idImovel, idsSegmentos, idsRamosAtividades,
							idsPontoConsumo, urlLogoEmpresa, consumoProgramado, exibirGrafico);

			if (dadosGerais != null) {
				collDadosGerais.add(dadosGerais);
			}

		}

		if (!collDadosGerais.isEmpty()) {

			DynaBean relatorioAnaliseDemanda = new BasicDynaBean(RELATORIO_ANALISE_DE_DEMANDA);
			relatorioAnaliseDemanda.set("dadosGerais", collDadosGerais);
			relatorioAnaliseDemanda.set(EXIBIR_FILTROS, exibirFiltros);

			if (exibirFiltros) {

				DynaBean dadosFiltro = (DynaBean) this.obterDadosFiltro(parametros);
				relatorioAnaliseDemanda.set(IMAGEM, urlLogoEmpresa);
				relatorioAnaliseDemanda.set(DATA_INICIAL, dadosFiltro.get(DATA_INICIAL));
				relatorioAnaliseDemanda.set(DATA_FINAL, dadosFiltro.get(DATA_FINAL));
				relatorioAnaliseDemanda.set(CLIENTE, dadosFiltro.get(CLIENTE));
				relatorioAnaliseDemanda.set(IMOVEL, dadosFiltro.get(IMOVEL));
				relatorioAnaliseDemanda.set(PONTOS_CONSUMO, dadosFiltro.get(PONTOS_CONSUMO));
				relatorioAnaliseDemanda.set(SEGMENTOS, dadosFiltro.get(SEGMENTOS));
				relatorioAnaliseDemanda.set(RAMOS_ATIVIDADE, dadosFiltro.get(RAMOS_ATIVIDADE));

			}

			collRelatorio.add(relatorioAnaliseDemanda);

		} else {

			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_ANALISE_DEMANDA_NENHUM_REGISTRO_ENCONTRADO, true);

		}

		return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_ANALISE_DEMANDA, formatoImpressao);
	}

	/**
	 * Obter dados gerais.
	 * 
	 * @param listaSemanas
	 *            the lista semanas
	 * @param idCliente
	 *            the id cliente
	 * @param idImovel
	 *            the id imovel
	 * @param idsSegmentos
	 *            the ids segmentos
	 * @param idsRamosAtividades
	 *            the ids ramos atividades
	 * @param idsPontoConsumo
	 *            the ids ponto consumo
	 * @param urlLogoEmpresa
	 *            the url logo empresa
	 * @param consumoProgramado
	 *            the consumo programado
	 * @param exibirGrafico
	 *            the exibir grafico
	 * @return the object
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Object obterDadosGerais(List<Date[]> listaSemanas, Long idCliente, Long idImovel, Long[] idsSegmentos,
					Long[] idsRamosAtividades, Long[] idsPontoConsumo, String urlLogoEmpresa, Boolean consumoProgramado,
					Boolean exibirGrafico) throws NegocioException {

		Date[] primeiraSemanaMes = listaSemanas.get(0);
		Date[] ultimaSemanaMes = listaSemanas.get(listaSemanas.size() - 1);

		Map<PontoConsumo, List<HistoricoConsumo>> dadosConsumo = this.listarPontosConsumoMedicao(primeiraSemanaMes[0], ultimaSemanaMes[1],
						idCliente, idImovel, idsSegmentos, idsRamosAtividades, idsPontoConsumo);

		if (dadosConsumo != null) {

			Map<Date, Object> mapDadosConsumoProgramado = null;
			List<Object> dadosConsumoProgramado = null;

			if (consumoProgramado || exibirGrafico) {

				Map<PontoConsumo, List<SolicitacaoConsumoPontoConsumo>> dadosSolicitacoesConsumo = this.listarPontosConsumoSolicitacao(
								primeiraSemanaMes[0], ultimaSemanaMes[1], dadosConsumo);
				mapDadosConsumoProgramado = this.obterDadosConsumoProgramado(primeiraSemanaMes[0], ultimaSemanaMes[1], dadosConsumo,
								dadosSolicitacoesConsumo);

				if (mapDadosConsumoProgramado != null) {

					dadosConsumoProgramado = new ArrayList<Object>();

					for (Object dados : mapDadosConsumoProgramado.values()) {
						dadosConsumoProgramado.add(dados);
					}

				}

			}

			List<Object> dadosSemanais = this.obterDadosSemanais(listaSemanas, dadosConsumo, mapDadosConsumoProgramado, exibirGrafico);

			DynaBean dadosGerais = new BasicDynaBean(SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_GERAIS);
			dadosGerais.set(IMAGEM, urlLogoEmpresa);
			dadosGerais.set(DATA_INICIAL, primeiraSemanaMes[0]);
			dadosGerais.set("dadosSemanais", dadosSemanais);
			if(consumoProgramado){
				dadosGerais.set("dadosConsumoProgramado", dadosConsumoProgramado);
			}else{
				dadosGerais.set("dadosConsumoProgramado", null);
			}

			return dadosGerais;

		} else {

			return null;

		}

	}

	/**
	 * Obter dados filtro.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the object
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Object obterDadosFiltro(Map<String, Object> parametros) throws NegocioException {

		Date dataInicial = (Date) parametros.get(DATA_INICIAL);
		Date dataFinal = (Date) parametros.get(DATA_FINAL);
		Long idCliente = (Long) parametros.get(ID_CLIENTE);
		Long idImovel = (Long) parametros.get(ID_IMOVEL);
		Long[] idsPontoConsumo = (Long[]) parametros.get("idsPontoConsumo");
		Long[] idsSegmentos = (Long[]) parametros.get(IDS_SEGMENTOS);
		Long[] idsRamosAtividades = (Long[]) parametros.get(IDS_RAMOS_ATIVIDADES);

		DynaBean subRelatorio = new BasicDynaBean(SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_FILTRO);

		subRelatorio.set(DATA_INICIAL, dataInicial);
		subRelatorio.set(DATA_FINAL, dataFinal);

		if (idCliente != null) {
			subRelatorio.set(CLIENTE, this.obterCliente(idCliente).getNome());
		} else {
			subRelatorio.set(CLIENTE, "");
		}

		if (idImovel != null) {
			subRelatorio.set(IMOVEL, this.obterImovel(idImovel).getNome());
		} else {
			subRelatorio.set(IMOVEL, "");
		}

		if (idsPontoConsumo != null) {

			StringBuilder sb = new StringBuilder();
			Boolean primeiraLinha = Boolean.TRUE;
			for (PontoConsumo pontoConsumo : this.listarPontosConsumo(parametros)) {

				if (!primeiraLinha) {
					sb.append(System.getProperty(LINE_SEPARATOR)).append(" - ").append(pontoConsumo.getDescricao());
				} else {
					sb.append(" - ").append(pontoConsumo.getDescricao());
					primeiraLinha = Boolean.FALSE;
				}

			}

			subRelatorio.set(PONTOS_CONSUMO, sb.toString());

		} else {
			subRelatorio.set(PONTOS_CONSUMO, null);
		}

		if (idsSegmentos != null) {

			StringBuilder sb = new StringBuilder();
			Boolean primeiraLinha = Boolean.TRUE;
			for (Segmento segmento : this.listarSegmentos(parametros)) {

				if (!primeiraLinha) {
					sb.append(System.getProperty(LINE_SEPARATOR)).append(" - ").append(segmento.getDescricao());
				} else {
					sb.append(" - ").append(segmento.getDescricao());
					primeiraLinha = Boolean.FALSE;
				}

			}

			subRelatorio.set(SEGMENTOS, sb.toString());

		} else {
			subRelatorio.set(SEGMENTOS, null);
		}

		if (idsRamosAtividades != null) {

			StringBuilder sb = new StringBuilder();
			Boolean primeiraLinha = Boolean.TRUE;
			for (RamoAtividade ramoAtividade : this.listarRamosAtividades(parametros)) {

				if (!primeiraLinha) {
					sb.append(System.getProperty(LINE_SEPARATOR)).append(" - ").append(ramoAtividade.getDescricao());
				} else {
					sb.append(" - ").append(ramoAtividade.getDescricao());
					primeiraLinha = Boolean.FALSE;
				}

			}

			subRelatorio.set(RAMOS_ATIVIDADE, sb.toString());

		} else {
			subRelatorio.set(RAMOS_ATIVIDADE, null);
		}

		return subRelatorio;
	}

	/**
	 * Obter dados consumo programado.
	 * 
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @param dadosConsumo
	 *            the dados consumo
	 * @param dadosSolicitacoesConsumo
	 *            the dados solicitacoes consumo
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Map<Date, Object> obterDadosConsumoProgramado(Date dataInicial, Date dataFinal,
					Map<PontoConsumo, List<HistoricoConsumo>> dadosConsumo,
					Map<PontoConsumo, List<SolicitacaoConsumoPontoConsumo>> dadosSolicitacoesConsumo) throws NegocioException {

		Map<Date, BigDecimal> mapConsumoProgramado = new TreeMap<Date, BigDecimal>();
		Map<Date, BigDecimal> mapConsumoRealizado = new TreeMap<Date, BigDecimal>();
		Map<Date, Object> mapConsumo = new TreeMap<Date, Object>();

		if (dadosConsumo != null) {

			for (List<HistoricoConsumo> listaHistoricoConsumos : dadosConsumo.values()) {

				if (listaHistoricoConsumos != null) {

					for (HistoricoConsumo historicoConsumo : listaHistoricoConsumos) {

						Date key = historicoConsumo.getHistoricoAtual().getDataLeituraFaturada();
						BigDecimal valorVolumeMedio = mapConsumoRealizado.get(key);
						mapConsumoRealizado.remove(key);
						mapConsumoRealizado.put(key, this.addConsumo(historicoConsumo.getConsumoApurado(), valorVolumeMedio));

					}

				}

			}

		}

		if (dadosSolicitacoesConsumo != null) {

			for (List<SolicitacaoConsumoPontoConsumo> listaSolicitacao : dadosSolicitacoesConsumo.values()) {

				if (listaSolicitacao != null) {

					for (SolicitacaoConsumoPontoConsumo solicitacao : listaSolicitacao) {

						Date key = solicitacao.getDataSolicitacao();
						BigDecimal valorVolumeMedio = mapConsumoProgramado.get(key);
						mapConsumoProgramado.remove(key);
						mapConsumoProgramado.put(key, this.addConsumo(solicitacao.getValorQDP(), valorVolumeMedio));

					}

				}

			}

		}

		Calendar c1 = Calendar.getInstance();
		c1.setTime(dataInicial);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(dataFinal);

		for (Calendar c = (Calendar) c1.clone(); c.compareTo(c2) <= 0; c.add(Calendar.DATE, +1)) {

			DynaBean subRelatorio = new BasicDynaBean(SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_CONSUMO_PROGRAMADO);
			subRelatorio.set("data", c.getTime());
			if(mapConsumoRealizado.get(c.getTime()) != null){
				subRelatorio.set(CONSUMO_REALIZADO, mapConsumoRealizado.get(c.getTime()));
			}else{
				subRelatorio.set(CONSUMO_REALIZADO, BigDecimal.ZERO);
			}
			if(mapConsumoProgramado.get(c.getTime()) != null){
				subRelatorio.set(CONSUMO_PROGRAMADO, mapConsumoProgramado.get(c.getTime()));
			}else{
				subRelatorio.set(CONSUMO_PROGRAMADO, BigDecimal.ZERO);
			}

			mapConsumo.put(c.getTime(), subRelatorio);

		}

		return mapConsumo;

	}

	/**
	 * Obter dados semanais.
	 * 
	 * @param listaSemanas
	 *            the lista semanas
	 * @param dadosConsumo
	 *            the dados consumo
	 * @param dadosConsumoProgramado
	 *            the dados consumo programado
	 * @param exibirGrafico
	 *            the exibir grafico
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<Object> obterDadosSemanais(List<Date[]> listaSemanas, Map<PontoConsumo, List<HistoricoConsumo>> dadosConsumo,
					Map<Date, Object> dadosConsumoProgramado, Boolean exibirGrafico) throws NegocioException {

		if (dadosConsumo != null) {

			List<Object> subRelatorios = new ArrayList<Object>();

			for (Date[] semana : listaSemanas) {

				Date dataInicioSemana = semana[0];
				Date dataFimSemana = semana[1];

				Date[] diasSemana = this.obterDiasSemana(dataInicioSemana, dataFimSemana);

				DynaBean subRelatorio = new BasicDynaBean(SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_SEMANAIS);
				subRelatorio.set("dataFinalApuracaoConsumo", dataFimSemana);
				subRelatorio.set("primeiroDiaSemana", diasSemana[0]);
				subRelatorio.set("segundoDiaSemana", diasSemana[1]);
				subRelatorio.set("terceiroDiaSemana", diasSemana[2]);
				subRelatorio.set("quartoDiaSemana", diasSemana[3]);
				subRelatorio.set("quintoDiaSemana", diasSemana[4]);
				subRelatorio.set("sextoDiaSemana", diasSemana[5]);
				subRelatorio.set("setimoDiaSemana", diasSemana[6]);

				List<Object> dadosPontosConsumo = this.obterDadosPontosConsumo(dataInicioSemana, dataFimSemana, dadosConsumo, diasSemana);

				subRelatorio.set("dadosPontosConsumo", dadosPontosConsumo);
				subRelatorio.set("dadosSegmentos", this.obterDadosSegmentos(dadosPontosConsumo));
				if(exibirGrafico){
					subRelatorio.set("dadosGrafico", this.obterDadosGrafico(diasSemana, dadosConsumoProgramado));
				}else{
					subRelatorio.set("dadosGrafico", null);
				}

				subRelatorios.add(subRelatorio);

			}

			return subRelatorios;

		} else {
			return null;
		}

	}

	/**
	 * Obter dados grafico.
	 * 
	 * @param diasSemana
	 *            the dias semana
	 * @param dadosConsumoProgramado
	 *            the dados consumo programado
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<Object> obterDadosGrafico(Date[] diasSemana, Map<Date, Object> dadosConsumoProgramado) throws NegocioException {

		if (dadosConsumoProgramado != null) {

			List<Object> subRelatorios = new ArrayList<Object>();

			for (Date data : diasSemana) {

				if (data != null) {

					DynaBean dados = (DynaBean) dadosConsumoProgramado.get(data);

					DynaBean subRelatorio = new BasicDynaBean(SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_GRAFICO);
					subRelatorio.set("data", data);
					if(dados.get(CONSUMO_REALIZADO) != null){
						subRelatorio.set("valorConsumo", dados.get(CONSUMO_REALIZADO));
					}else{
						subRelatorio.set("valorConsumo", BigDecimal.ZERO);
					}
					
					if(dados.get(CONSUMO_PROGRAMADO) != null){
						subRelatorio.set("valorProgramado", dados.get(CONSUMO_PROGRAMADO));
					}else{
						subRelatorio.set("valorProgramado", BigDecimal.ZERO);
					}

					subRelatorios.add(subRelatorio);
				}

			}

			return subRelatorios;

		} else {

			return null;

		}

	}

	/**
	 * Obter dados segmentos.
	 * 
	 * @param dadosPontosConsumo
	 *            the dados pontos consumo
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<Object> obterDadosSegmentos(List<Object> dadosPontosConsumo) throws NegocioException {

		Map<Long, Object> mapSegmento = new LinkedHashMap<Long, Object>();
		List<Object> subRelatorios = null;

		if (dadosPontosConsumo != null) {

			for (Object dadosPontoConsumo : dadosPontosConsumo) {

				DynaBean dadosPc = (DynaBean) dadosPontoConsumo;
				Long idSegmento = (Long) dadosPc.get(ID_SEGMENTO);
				String descricaoSegmento = (String) dadosPc.get(DESCRICAO_SEGMENTO);
				BigDecimal volMedioAnual = (BigDecimal) dadosPc.get(VOL_MEDIO_ANUAL);
				BigDecimal volMedioMensal = (BigDecimal) dadosPc.get(VOL_MEDIO_MENSAL);
				BigDecimal volMedioDiario = (BigDecimal) dadosPc.get(VOL_MEDIO_DIARIO);
				BigDecimal volMedioPrimeiroDiaSemana = (BigDecimal) dadosPc.get(VOL_MEDIO_PRIMEIRO_DIA_SEMAMA);
				BigDecimal volMedioSegundoDiaSemana = (BigDecimal) dadosPc.get(VOL_MEDIO_SEGUNDO_DIA_SEMANA);
				BigDecimal volMedioTerceiroDiaSemana = (BigDecimal) dadosPc.get(VOL_MEDIO_TERCEIRO_DIA_SEMANA);
				BigDecimal volMedioQuartoDiaSemana = (BigDecimal) dadosPc.get(VOL_MEDIO_QUARTO_DIA_SEMANA);
				BigDecimal volMedioQuintoDiaSemana = (BigDecimal) dadosPc.get(VOL_MEDIO_QUINTO_DIA_SEMANA);
				BigDecimal volMedioSextoDiaSemana = (BigDecimal) dadosPc.get(VOL_MEDIO_SEXTO_DIA_SEMANA);
				BigDecimal volMedioSetimoDiaSemana = (BigDecimal) dadosPc.get(VOL_MEDIO_SETIMO_DIA_SEMANA);

				if (mapSegmento.containsKey(idSegmento)) {

					DynaBean dadosSg = (DynaBean) mapSegmento.get(idSegmento);

					dadosSg.set(VOL_MEDIO_ANUAL, this.addConsumo(volMedioAnual, (BigDecimal) dadosSg.get(VOL_MEDIO_ANUAL)));
					dadosSg.set(VOL_MEDIO_MENSAL, this.addConsumo(volMedioMensal, (BigDecimal) dadosSg.get(VOL_MEDIO_MENSAL)));
					dadosSg.set(VOL_MEDIO_DIARIO, this.addConsumo(volMedioDiario, (BigDecimal) dadosSg.get(VOL_MEDIO_DIARIO)));
					dadosSg.set(VOL_MEDIO_PRIMEIRO_DIA_SEMAMA,
									this.addConsumo(volMedioPrimeiroDiaSemana, (BigDecimal) dadosSg.get(VOL_MEDIO_PRIMEIRO_DIA_SEMAMA)));
					dadosSg.set(VOL_MEDIO_SEGUNDO_DIA_SEMANA,
									this.addConsumo(volMedioSegundoDiaSemana, (BigDecimal) dadosSg.get(VOL_MEDIO_SEGUNDO_DIA_SEMANA)));
					dadosSg.set(VOL_MEDIO_TERCEIRO_DIA_SEMANA,
									this.addConsumo(volMedioTerceiroDiaSemana, (BigDecimal) dadosSg.get(VOL_MEDIO_TERCEIRO_DIA_SEMANA)));
					dadosSg.set(VOL_MEDIO_QUARTO_DIA_SEMANA,
									this.addConsumo(volMedioQuartoDiaSemana, (BigDecimal) dadosSg.get(VOL_MEDIO_QUARTO_DIA_SEMANA)));
					dadosSg.set(VOL_MEDIO_QUINTO_DIA_SEMANA,
									this.addConsumo(volMedioQuintoDiaSemana, (BigDecimal) dadosSg.get(VOL_MEDIO_QUINTO_DIA_SEMANA)));
					dadosSg.set(VOL_MEDIO_SEXTO_DIA_SEMANA,
									this.addConsumo(volMedioSextoDiaSemana, (BigDecimal) dadosSg.get(VOL_MEDIO_SEXTO_DIA_SEMANA)));
					dadosSg.set(VOL_MEDIO_SETIMO_DIA_SEMANA,
									this.addConsumo(volMedioSetimoDiaSemana, (BigDecimal) dadosSg.get(VOL_MEDIO_SETIMO_DIA_SEMANA)));

				} else {

					DynaBean subRelatorio = new BasicDynaBean(SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_SEGMENTO);
					subRelatorio.set(DESCRICAO_SEGMENTO, descricaoSegmento);
					subRelatorio.set(VOL_MEDIO_ANUAL, volMedioAnual);
					subRelatorio.set(VOL_MEDIO_MENSAL, volMedioMensal);
					subRelatorio.set(VOL_MEDIO_DIARIO, volMedioDiario);
					subRelatorio.set(VOL_MEDIO_PRIMEIRO_DIA_SEMAMA, volMedioPrimeiroDiaSemana);
					subRelatorio.set(VOL_MEDIO_SEGUNDO_DIA_SEMANA, volMedioSegundoDiaSemana);
					subRelatorio.set(VOL_MEDIO_TERCEIRO_DIA_SEMANA, volMedioTerceiroDiaSemana);
					subRelatorio.set(VOL_MEDIO_QUARTO_DIA_SEMANA, volMedioQuartoDiaSemana);
					subRelatorio.set(VOL_MEDIO_QUINTO_DIA_SEMANA, volMedioQuintoDiaSemana);
					subRelatorio.set(VOL_MEDIO_SEXTO_DIA_SEMANA, volMedioSextoDiaSemana);
					subRelatorio.set(VOL_MEDIO_SETIMO_DIA_SEMANA, volMedioSetimoDiaSemana);

					mapSegmento.put(idSegmento, subRelatorio);

				}

			}

			if (!mapSegmento.isEmpty()) {

				subRelatorios = new ArrayList<Object>();

				for (Map.Entry<Long, Object> entry : mapSegmento.entrySet()) {

					subRelatorios.add(entry.getValue());

				}

			}

		}

		return subRelatorios;
	}

	/**
	 * Obter dados pontos consumo.
	 * 
	 * @param dataInicioSemana
	 *            the data inicio semana
	 * @param dataFimSemana
	 *            the data fim semana
	 * @param dados
	 *            the dados
	 * @param diasSemana
	 *            the dias semana
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<Object> obterDadosPontosConsumo(Date dataInicioSemana, Date dataFimSemana,
					Map<PontoConsumo, List<HistoricoConsumo>> dados, Date[] diasSemana) throws NegocioException {

		if (dados != null) {

			Calendar cal = Calendar.getInstance();
			cal.setTime(dataInicioSemana);

			Date dataInicioAno = Util.obterPrimeiroDiaMes(cal.get(Calendar.YEAR));

			cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);

			Date dataInicioMes = cal.getTime();

			List<Object> subRelatorios = new ArrayList<Object>();

			for (Map.Entry<PontoConsumo, List<HistoricoConsumo>> entry : dados.entrySet()) {
				PontoConsumo pontoConsumo = entry.getKey();
				BigDecimal[] volumes = this.obterVolumesConsumoPontoConsumo(dataInicioAno, dataInicioMes, dataInicioSemana, dataFimSemana,
								entry.getValue(), diasSemana);

				DynaBean subRelatorio = new BasicDynaBean(SUB_RELATORIO_ANALISE_DE_DEMANDA_DADOS_PONTO_CONSUMO);
				subRelatorio.set(ID_SEGMENTO, pontoConsumo.getSegmento().getChavePrimaria());
				subRelatorio.set(DESCRICAO_SEGMENTO, pontoConsumo.getSegmento().getDescricao());
				if(pontoConsumo.getDescricao() != null){
					subRelatorio.set("descricaoPontoConsumo", pontoConsumo.getDescricao());
				}else{
					subRelatorio.set("descricaoPontoConsumo", "");
				}
				if(pontoConsumo.getSegmento().getRamosAtividades() != null){
					subRelatorio.set("descricaoRamoAtividades", pontoConsumo
								.getSegmento().getRamosAtividades().iterator().next().getDescricao());
				}else{
					subRelatorio.set("descricaoRamoAtividades", "");
				}
				subRelatorio.set(VOL_MEDIO_ANUAL, volumes[0]);
				subRelatorio.set(VOL_MEDIO_MENSAL, volumes[1]);
				subRelatorio.set(VOL_MEDIO_DIARIO, volumes[2]);
				subRelatorio.set(VOL_MEDIO_PRIMEIRO_DIA_SEMAMA, volumes[3]);
				subRelatorio.set(VOL_MEDIO_SEGUNDO_DIA_SEMANA, volumes[4]);
				subRelatorio.set(VOL_MEDIO_TERCEIRO_DIA_SEMANA, volumes[5]);
				subRelatorio.set(VOL_MEDIO_QUARTO_DIA_SEMANA, volumes[6]);
				subRelatorio.set(VOL_MEDIO_QUINTO_DIA_SEMANA, volumes[7]);
				subRelatorio.set(VOL_MEDIO_SEXTO_DIA_SEMANA, volumes[8]);
				subRelatorio.set(VOL_MEDIO_SETIMO_DIA_SEMANA, volumes[9]);

				subRelatorios.add(subRelatorio);
			}

			return subRelatorios;

		} else {
			return null;
		}

	}

	/**
	 * Obter volumes consumo ponto consumo.
	 * 
	 * @param dataInicioAno
	 *            the data inicio ano
	 * @param dataInicioMes
	 *            the data inicio mes
	 * @param dataInicioSemana
	 *            the data inicio semana
	 * @param dataFimSemana
	 *            the data fim semana
	 * @param listaHistoricoConsumo
	 *            the lista historico consumo
	 * @param diasSemana
	 *            the dias semana
	 * @return the big decimal[]
	 */
	private BigDecimal[] obterVolumesConsumoPontoConsumo(Date dataInicioAno, Date dataInicioMes, Date dataInicioSemana, Date dataFimSemana,
					List<HistoricoConsumo> listaHistoricoConsumo, Date[] diasSemana) {

		BigDecimal volMedioAnual = null;
		BigDecimal volMedioMensal = null;
		BigDecimal volMedioDiario = null;
		BigDecimal volMedioPrimeiroDiaSemana = null;
		BigDecimal volMedioSegundoDiaSemana = null;
		BigDecimal volMedioTerceiroDiaSemana = null;
		BigDecimal volMedioQuartoDiaSemana = null;
		BigDecimal volMedioQuintoDiaSemana = null;
		BigDecimal volMedioSextoDiaSemana = null;
		BigDecimal volMedioSetimoDiaSemana = null;

		for (HistoricoConsumo historicoConsumo : listaHistoricoConsumo) {

			Date dataLeitura = historicoConsumo.getHistoricoAtual().getDataLeituraFaturada();

			if (historicoConsumo.getConsumoApurado() != null) {

				// Consumo Anual
				if ((dataLeitura.compareTo(dataInicioAno) >= 0) && (dataLeitura.compareTo(dataFimSemana) <= 0)) {

					volMedioAnual = this.addConsumo(historicoConsumo.getConsumoApurado(), volMedioAnual);

				}

				// Consumo Mensal
				if ((dataLeitura.compareTo(dataInicioMes) >= 0) && (dataLeitura.compareTo(dataFimSemana) <= 0)) {

					volMedioMensal = this.addConsumo(historicoConsumo.getConsumoApurado(), volMedioMensal);

				}

				// Consumo Diário
				if ((dataLeitura.compareTo(dataInicioSemana) >= 0) && (dataLeitura.compareTo(dataFimSemana) <= 0)) {

					volMedioDiario = this.addConsumo(historicoConsumo.getConsumoApurado(), volMedioDiario);

				}

				// Primeiro dia semana
				if (diasSemana[0] != null && dataLeitura.compareTo(diasSemana[0]) == 0) {

					volMedioPrimeiroDiaSemana = this.addConsumo(historicoConsumo.getConsumoApurado(), volMedioPrimeiroDiaSemana);

				}

				// Segundo dia semana
				if (diasSemana[1] != null && dataLeitura.compareTo(diasSemana[1]) == 0) {

					volMedioSegundoDiaSemana = this.addConsumo(historicoConsumo.getConsumoApurado(), volMedioSegundoDiaSemana);

				}

				// Terceiro dia semana
				if (diasSemana[2] != null && dataLeitura.compareTo(diasSemana[2]) == 0) {

					volMedioTerceiroDiaSemana = this.addConsumo(historicoConsumo.getConsumoApurado(), volMedioTerceiroDiaSemana);

				}

				// Quarto dia semana
				if (diasSemana[3] != null && dataLeitura.compareTo(diasSemana[3]) == 0) {

					volMedioQuartoDiaSemana = this.addConsumo(historicoConsumo.getConsumoApurado(), volMedioQuartoDiaSemana);

				}

				// Quinto dia semana
				if (diasSemana[4] != null && dataLeitura.compareTo(diasSemana[4]) == 0) {

					volMedioQuintoDiaSemana = this.addConsumo(historicoConsumo.getConsumoApurado(), volMedioQuintoDiaSemana);

				}

				// Sexto dia semana
				if (diasSemana[5] != null && dataLeitura.compareTo(diasSemana[5]) == 0) {

					volMedioSextoDiaSemana = this.addConsumo(historicoConsumo.getConsumoApurado(), volMedioSextoDiaSemana);

				}

				// SSetimo dia semana
				if (diasSemana[6] != null && dataLeitura.compareTo(diasSemana[6]) == 0) {

					volMedioSetimoDiaSemana = this.addConsumo(historicoConsumo.getConsumoApurado(), volMedioSetimoDiaSemana);

				}

			}

		}

		return new BigDecimal[] {volMedioAnual, volMedioMensal, volMedioDiario, volMedioPrimeiroDiaSemana, volMedioSegundoDiaSemana, 
		                         volMedioTerceiroDiaSemana, volMedioQuartoDiaSemana, volMedioQuintoDiaSemana, volMedioSextoDiaSemana, 
		                         volMedioSetimoDiaSemana};
	}

	/**
	 * Adds the consumo.
	 * 
	 * @param valorConsumoApurado
	 *            the valor consumo apurado
	 * @param valorVolumeMedio
	 *            the valor volume medio
	 * @return the big decimal
	 */
	private BigDecimal addConsumo(final BigDecimal valorConsumoApurado, final BigDecimal valorVolumeMedio) {

		BigDecimal valorVolumeMedioLocal = valorVolumeMedio;
		if (valorConsumoApurado != null && valorVolumeMedioLocal != null) {

			valorVolumeMedioLocal = valorVolumeMedioLocal.add(valorConsumoApurado);

		} else {

			valorVolumeMedioLocal = valorConsumoApurado;

		}

		return valorVolumeMedioLocal;

	}

	/**
	 * Validar dados gerar relatorio analise demanda.
	 * 
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosGerarRelatorioAnaliseDemanda(Map<String, Object> parametros) throws NegocioException {

		if (parametros.get("formatoImpressao") == null) {

			throw new NegocioException(ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO, true);

		}

		Date dataInicial = (Date) parametros.get(DATA_INICIAL);
		Date dataFinal = (Date) parametros.get(DATA_FINAL);

		if (dataInicial == null && dataFinal == null) {

			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_ANALISE_DEMANDA_CAMPO_PERIODO_OBRIGATORIO, true);

		} else {

			if (dataInicial == null) {

				throw new NegocioException(ERRO_NEGOCIO_RELATORIO_ANALISE_DEMANDA_CAMPO_DATA_INICIAL_OBRIGATORIO, true);

			} else if (dataFinal == null) {

				throw new NegocioException(ERRO_NEGOCIO_RELATORIO_ANALISE_DEMANDA_CAMPO_DATA_FINAL_OBRIGATORIO, true);

			} else if (dataInicial.compareTo(dataFinal) > 0) {

				throw new NegocioException(ERRO_NEGOCIO_RELATORIO_ANALISE_DEMANDA_CAMPO_DATA_INICIAL_MAIOR_DATA_FINAL, true);

			}

		}

	}

	/**
	 * Listar pontos consumo solicitacao.
	 * 
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @param dadosConsumo
	 *            the dados consumo
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private Map<PontoConsumo, List<SolicitacaoConsumoPontoConsumo>> listarPontosConsumoSolicitacao(Date dataInicial, Date dataFinal,
					Map<PontoConsumo, List<HistoricoConsumo>> dadosConsumo) throws NegocioException {

		if (dadosConsumo != null) {

			Map<PontoConsumo, List<SolicitacaoConsumoPontoConsumo>> mapSolicitacoes = null;
			List<Long> idsPontoConsumo = new ArrayList<Long>();

			for (PontoConsumo pontoConsumo : dadosConsumo.keySet()) {
				idsPontoConsumo.add(pontoConsumo.getChavePrimaria());
			}

			Criteria criteria = this.createCriteria(SolicitacaoConsumoPontoConsumo.class);
			criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO, Criteria.INNER_JOIN);

			criteria.add(Restrictions.in("pontoConsumo.chavePrimaria", idsPontoConsumo));
			criteria.add(Restrictions.between("dataSolicitacao", dataInicial, dataFinal));
			criteria.addOrder(Order.asc("pontoConsumo.descricao"));

			List<SolicitacaoConsumoPontoConsumo> listaSolicitacao = criteria.list();

			if (listaSolicitacao != null) {

				mapSolicitacoes = new LinkedHashMap<PontoConsumo, List<SolicitacaoConsumoPontoConsumo>>();

				PontoConsumo pontoConsumo = null;
				List<SolicitacaoConsumoPontoConsumo> lista = null;

				for (SolicitacaoConsumoPontoConsumo solicitacao : listaSolicitacao) {

					if (pontoConsumo == null) {

						pontoConsumo = solicitacao.getPontoConsumo();
						lista = new ArrayList<SolicitacaoConsumoPontoConsumo>();
						lista.add(solicitacao);

					} else if (pontoConsumo.getChavePrimaria() == solicitacao.getPontoConsumo().getChavePrimaria()) {

						lista.add(solicitacao);

					} else {

						mapSolicitacoes.put(pontoConsumo, lista);

						pontoConsumo = solicitacao.getPontoConsumo();
						lista = new ArrayList<SolicitacaoConsumoPontoConsumo>();
						lista.add(solicitacao);

					}

				}

			}

			return mapSolicitacoes;

		} else {

			return null;

		}

	}

	/**
	 * Listar pontos consumo medicao.
	 * 
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @param idCliente
	 *            the id cliente
	 * @param idImovel
	 *            the id imovel
	 * @param idsSegmentos
	 *            the ids segmentos
	 * @param idsRamosAtividades
	 *            the ids ramos atividades
	 * @param idsPontoConsumo
	 *            the ids ponto consumo
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private Map<PontoConsumo, List<HistoricoConsumo>> listarPontosConsumoMedicao(Date dataInicial, Date dataFinal, Long idCliente,
					Long idImovel, Long[] idsSegmentos, Long[] idsRamosAtividades, Long[] idsPontoConsumo) throws NegocioException {

		Map<PontoConsumo, List<HistoricoConsumo>> mapa = null;

		Calendar cal = Calendar.getInstance();
		cal.setTime(dataInicial);

		Date dataInicialModificada = Util.obterPrimeiroDiaMes(cal.get(Calendar.YEAR));

		Criteria criteria = this.createCriteria(HistoricoConsumo.class);
		criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO, Criteria.INNER_JOIN);
		criteria.createAlias("pontoConsumo.imovel", IMOVEL, Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.segmento", SEGMENTO, Criteria.INNER_JOIN);
		criteria.createAlias("pontoConsumo.ramoAtividade", "ramoAtividade", Criteria.INNER_JOIN);
		criteria.createAlias("historicoAtual", "historicoAtual", Criteria.INNER_JOIN);

		criteria.add(Restrictions.between("historicoAtual.dataLeituraFaturada", dataInicialModificada, dataFinal));
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
		criteria.add(Restrictions.eq("indicadorFaturamento", Boolean.TRUE));
		criteria.add(Restrictions.eq("indicadorConsumoCiclo", Boolean.FALSE));

		if (idCliente != null) {
			criteria.createCriteria("pontoConsumo.imovel.listaClienteImovel").createCriteria(CLIENTE)
							.add(Restrictions.eq(CHAVE_PRIMARIA, idCliente));
		}

		if (idImovel != null) {
			criteria.add(Restrictions.eq("pontoConsumo.imovel.chavePrimaria", idImovel));
		}

		if (idsSegmentos != null) {
			criteria.add(Restrictions.in("pontoConsumo.segmento.chavePrimaria", idsSegmentos));
		}

		if (idsRamosAtividades != null) {
			criteria.add(Restrictions.in("pontoConsumo.ramoAtividade.chavePrimaria", idsRamosAtividades));
		}

		if (idsPontoConsumo != null) {
			criteria.add(Restrictions.in("pontoConsumo.chavePrimaria", idsPontoConsumo));
		}

		criteria.addOrder(Order.asc("pontoConsumo.descricao"));

		List<HistoricoConsumo> listaHistorico = criteria.list();

		if (listaHistorico != null && !listaHistorico.isEmpty()) {

			mapa = new LinkedHashMap<PontoConsumo, List<HistoricoConsumo>>();

			PontoConsumo pontoConsumo = null;
			List<HistoricoConsumo> lista = null;

			for (HistoricoConsumo historicoConsumo : listaHistorico) {

				if (pontoConsumo == null) {

					pontoConsumo = historicoConsumo.getPontoConsumo();
					lista = new ArrayList<HistoricoConsumo>();
					lista.add(historicoConsumo);

				} else if (pontoConsumo.getChavePrimaria() == historicoConsumo.getPontoConsumo().getChavePrimaria()) {

					lista.add(historicoConsumo);

				} else {

					mapa.put(pontoConsumo, lista);

					pontoConsumo = historicoConsumo.getPontoConsumo();
					lista = new ArrayList<HistoricoConsumo>();
					lista.add(historicoConsumo);

				}

			}

			if (pontoConsumo != null && !mapa.containsKey(pontoConsumo)) {
				mapa.put(pontoConsumo, lista);
			}

		}

		return mapa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioAnaliseDemanda#listarPontosConsumoRelatorioAnaliseDemanda(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<PontoConsumo> listarPontosConsumoRelatorioAnaliseDemanda(Map<String, Object> filtro) throws NegocioException {

		Long idCliente = (Long) filtro.get(ID_CLIENTE);
		Long idImovel = (Long) filtro.get(ID_IMOVEL);
		Long[] idsSegmentos = (Long[]) filtro.get(IDS_SEGMENTOS);
		Long[] idsRamosAtividades = (Long[]) filtro.get(IDS_RAMOS_ATIVIDADES);
		Long[] idsPontoConsumo = (Long[]) filtro.get("idsPonto");

		Criteria criteria = this.createCriteria(PontoConsumo.class);
		criteria.createAlias(IMOVEL, IMOVEL, Criteria.LEFT_JOIN);
		criteria.createAlias(SEGMENTO, SEGMENTO, Criteria.LEFT_JOIN);

		if (idCliente != null) {
			criteria.createCriteria("imovel.listaClienteImovel").createCriteria(CLIENTE)
							.add(Restrictions.eq(CHAVE_PRIMARIA, filtro.get(ID_CLIENTE)));
		}

		if (idImovel != null) {
			criteria.add(Restrictions.eq("imovel.chavePrimaria", idImovel));
		}

		if (idsSegmentos != null) {
			criteria.add(Restrictions.in("segmento.chavePrimaria", idsSegmentos));
		}

		if (idsRamosAtividades != null) {
			criteria.add(Restrictions.in("ramoAtividade.chavePrimaria", idsRamosAtividades));
		}

		if (idsPontoConsumo != null) {
			criteria.add(Restrictions.in(CHAVE_PRIMARIA, idsPontoConsumo));
		}

		boolean paginacaoPadrao = false;
		if (filtro.containsKey("colecaoPaginada")) {

			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if (paginacaoPadrao) {
			criteria.addOrder(Order.asc(DESCRICAO));
		}

		return criteria.list();

	}

	/**
	 * Obter empresa principal.
	 * 
	 * @return the empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Empresa obterEmpresaPrincipal() throws NegocioException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("principal", Boolean.TRUE);
		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);

		Empresa empresa = null;
		if ((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
			empresa = ((List<Empresa>) listaEmpresas).get(0);
		}

		return empresa;
	}

	/**
	 * Apurar por semana quantidade relatorio.
	 * 
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @return the map
	 */
	private Map<Integer, List<Date[]>> apurarPorSemanaQuantidadeRelatorio(Date dataInicio, Date dataFim) {

		Map<Integer, List<Date[]>> relatoriosPorSemana = new HashMap<Integer, List<Date[]>>();
		Map<Integer, Date[]> relatoriosPorMes = this.apurarPorMesQuantidadeRelatorio(dataInicio, dataFim);

		for (Map.Entry<Integer, Date[]> entry : relatoriosPorMes.entrySet()) {
			relatoriosPorSemana.put(entry.getKey(), this.obterSemanasPorMes(entry.getValue()));

		}

		return relatoriosPorSemana;
	}

	/**
	 * Apurar por mes quantidade relatorio.
	 * 
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @return the map
	 */
	private Map<Integer, Date[]> apurarPorMesQuantidadeRelatorio(Date dataInicio, Date dataFim) {

		Map<Integer, Date[]> mapa = new HashMap<Integer, Date[]>();

		Calendar c1 = Calendar.getInstance();
		c1.setTime(dataInicio);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(dataFim);

		int mesAtual = c1.get(Calendar.MONTH);
		int count = 1;

		Calendar c3 = (Calendar) c1.clone();
		c3.set(Calendar.DAY_OF_MONTH, c1.getActualMaximum(Calendar.DAY_OF_MONTH));

		if (c3.getTime().compareTo(dataFim) <= 0) {

			mapa.put(count, new Date[] {c1.getTime(), c3.getTime()});

		} else {

			mapa.put(count, new Date[] {c1.getTime(), dataFim});

		}

		for (Calendar c = (Calendar) c1.clone(); c.compareTo(c2) <= 0; c.add(Calendar.DATE, +1)) {

			if (mesAtual != c.get(Calendar.MONTH)) {

				count = count + 1;
				mesAtual = c.get(Calendar.MONTH);

				c3 = (Calendar) c.clone();
				c3.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));

				if (c3.getTime().compareTo(dataFim) <= 0) {

					mapa.put(count, new Date[] {c.getTime(), c3.getTime()});

				} else {

					mapa.put(count, new Date[] {c.getTime(), dataFim});
					break;
				}

			}

		}

		return mapa;
	}

	/**
	 * Obter semanas por mes.
	 * 
	 * @param datas
	 *            the datas
	 * @return the list
	 */
	private List<Date[]> obterSemanasPorMes(Date[] datas) {

		List<Date[]> lista = new ArrayList<Date[]>();

		Calendar c1 = Calendar.getInstance();
		c1.setTime(datas[0]);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(datas[1]);

		for (Calendar c = (Calendar) c1.clone(); c.compareTo(c2) <= 0; c.add(Calendar.DATE, +1)) {

			Date[] semana = new Date[] {c.getTime(), this.obterUltimoDiaSemana(c.getTime(), c2.getTime())};

			lista.add(semana);

			c.setTime(semana[1]);

		}

		return lista;
	}

	/**
	 * Obter ultimo dia semana.
	 * 
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @return the date
	 */
	private Date obterUltimoDiaSemana(Date dataInicio, Date dataFim) {

		Calendar c1 = Calendar.getInstance();
		c1.setTime(dataInicio);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(dataFim);

		Date retorno = c1.getTime();

		for (Calendar c = (Calendar) c1.clone(); c.compareTo(c2) <= 0; c.add(Calendar.DATE, +1)) {

			if (c.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {

				retorno = c.getTime();
				break;

			} else {

				retorno = c.getTime();

			}

		}

		return retorno;
	}

	/**
	 * Obter dias semana.
	 * 
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @return the date[]
	 */
	private Date[] obterDiasSemana(Date dataInicial, Date dataFinal) {

		Calendar c1 = Calendar.getInstance();
		c1.setTime(dataInicial);

		Calendar c2 = Calendar.getInstance();
		c2.setTime(dataFinal);

		Date[] datas = new Date[7];
		int count = 0;

		for (Calendar c = (Calendar) c1.clone(); c.compareTo(c2) <= 0; c.add(Calendar.DATE, +1)) {

			datas[count] = c.getTime();
			count = count + 1;
		}

		while (count < 7) {

			datas[count] = null;
			count = count + 1;
		}

		return datas;
	}

	/**
	 * Obter cliente.
	 * 
	 * @param idCliente
	 *            the id cliente
	 * @return the cliente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Cliente obterCliente(Long idCliente) throws NegocioException {

		Criteria criteria = this.createCriteria(Cliente.class);
		criteria.add(Restrictions.idEq(idCliente));

		return (Cliente) criteria.uniqueResult();
	}

	/**
	 * Obter imovel.
	 * 
	 * @param idImovel
	 *            the id imovel
	 * @return the imovel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Imovel obterImovel(Long idImovel) throws NegocioException {

		Criteria criteria = this.createCriteria(Imovel.class);
		criteria.add(Restrictions.idEq(idImovel));

		return (Imovel) criteria.uniqueResult();
	}

	/**
	 * Listar pontos consumo.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private List<PontoConsumo> listarPontosConsumo(Map<String, Object> parametros) throws NegocioException {

		Long[] idsPontoConsumo = (Long[]) parametros.get("idsPontoConsumo");

		Criteria criteria = this.createCriteria(PontoConsumo.class);
		criteria.add(Restrictions.in(CHAVE_PRIMARIA, idsPontoConsumo));

		criteria.addOrder(Order.asc(DESCRICAO));

		return criteria.list();
	}

	/**
	 * Listar segmentos.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private List<Segmento> listarSegmentos(Map<String, Object> parametros) throws NegocioException {

		Long[] idsSegmentos = (Long[]) parametros.get(IDS_SEGMENTOS);

		Criteria criteria = this.createCriteria(Segmento.class);
		criteria.add(Restrictions.in(CHAVE_PRIMARIA, idsSegmentos));

		criteria.addOrder(Order.asc(DESCRICAO));

		return criteria.list();
	}

	/**
	 * Listar ramos atividades.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private List<RamoAtividade> listarRamosAtividades(Map<String, Object> parametros) throws NegocioException {

		Long[] idsRamosAtividades = (Long[]) parametros.get(IDS_RAMOS_ATIVIDADES);

		Criteria criteria = this.createCriteria(RamoAtividade.class);
		criteria.add(Restrictions.in(CHAVE_PRIMARIA, idsRamosAtividades));

		criteria.addOrder(Order.asc(DESCRICAO));

		return criteria.list();
	}
}
