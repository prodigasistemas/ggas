/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 05/01/2014 09:53:58
 @author wcosta
 */

package br.com.ggas.relatorio.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte;
import br.com.ggas.geral.Mes;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.relatorio.ControladorRelatorioPrazoMinimoAntecedencia;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.prazominimoantecedencia.RelatorioAvisoCorteVO;

public class ControladorRelatorioPrazoMinimoAntecedenciaImpl extends ControladorNegocioImpl 
	implements ControladorRelatorioPrazoMinimoAntecedencia {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioPrazoMinimoAntecedencia#gerarRelatorioAvisoCorte(java.util.Map)
	 */
	@Override
	public byte[] gerarRelatorioAvisoCorte(Map<String, Object> filtro) throws GGASException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		ServiceLocator.getInstancia().getBeanPorID(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

		ControladorMedidor controladorMedidor = (ControladorMedidor) ServiceLocator.getInstancia().getBeanPorID(
						ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);

		ControladorAvisoCorte controladorAvisoCorte = (ControladorAvisoCorte) ServiceLocator.getInstancia().getBeanPorID(
						ControladorAvisoCorte.BEAN_ID_CONTROLADOR_AVISO_CORTE);

		byte[] relatorio = null;

		if(filtro != null && filtro.containsKey("mesAno")) {

			Map<Integer, String> mapaMesesAno = Mes.MESES_ANO;

			Collection<RelatorioAvisoCorteVO> listaRelatorioAvisoCorteVo = new ArrayList<RelatorioAvisoCorteVO>();

			String anoMes = Util.converterMesAnoEmAnoMes((String) filtro.get("mesAno"));
			int anoMesInicio = Integer.parseInt(anoMes.substring(0, 4) + "01");
			int anoMesFinal = Integer.parseInt(anoMes);
			int mesFinal = Integer.parseInt(anoMes.substring(4, 6));

			for (int periodo = anoMesInicio; periodo <= anoMesFinal; periodo++) {

				Map<String, Date> mapaDataPeriodo = Util.obterDataInicialDataFimPeriodo(periodo);

				Collection<HistoricoOperacaoMedidor> listaBloqueios = controladorMedidor.obterHistoricoOperacaoMedidorPorPeriodo(
								OperacaoMedidor.CODIGO_BLOQUEIO, mapaDataPeriodo.get("dataInicial"), mapaDataPeriodo.get("dataFinal"));

				Integer ateUmDia = 0;
				Integer ateDoisDias = 0;
				Integer ateTresDias = 0;
				Integer maiorTresDias = 0;

				Iterator<HistoricoOperacaoMedidor> iterator = listaBloqueios.iterator();
				while(iterator.hasNext()) {

					HistoricoOperacaoMedidor historico = iterator.next();

					if(historico.getPontoConsumo() != null) {

						Map<String, Object> filtroAvisoCorte = new HashMap<String, Object>();
						filtroAvisoCorte.put("idPontoConsumo", historico.getPontoConsumo().getChavePrimaria());
						filtroAvisoCorte.put("dataAviso", historico.getDataRealizada());
						filtroAvisoCorte.put("ordenacao", "dataAviso");

						Collection<AvisoCorte> listaAvisoCorteBanco = controladorAvisoCorte.listar(filtroAvisoCorte);

						if(listaAvisoCorteBanco != null && !listaAvisoCorteBanco.isEmpty()) {

							Date dataAviso = listaAvisoCorteBanco.iterator().next().getDataAviso();
							Integer diferencaDias = Util.intervaloDatas(dataAviso, historico.getDataRealizada());
							if(diferencaDias <= 1) {
								ateUmDia = ateUmDia + 1;
							} else if(diferencaDias == 2) {
								ateDoisDias = ateDoisDias + 1;
							} else if(diferencaDias == 3) {
								ateTresDias = ateTresDias + 1;
							} else if(diferencaDias > 3) {
								maiorTresDias = maiorTresDias + 1;
							}

						}

					}

				}

				RelatorioAvisoCorteVO relAvisoCorteVO = new RelatorioAvisoCorteVO();
				relAvisoCorteVO.setMes(Integer.valueOf(String.valueOf(periodo).substring(4, 6)));
				relAvisoCorteVO.setDescricaoMes(mapaMesesAno.get(Integer.valueOf(String.valueOf(periodo).substring(4, 6))));
				relAvisoCorteVO.setAteUmDia(ateUmDia);
				relAvisoCorteVO.setAteDoisDias(ateDoisDias);
				relAvisoCorteVO.setAteTresDias(ateTresDias);
				relAvisoCorteVO.setMaiorTresDias(maiorTresDias);
				listaRelatorioAvisoCorteVo.add(relAvisoCorteVO);

			}

			if(mesFinal < 12) {
				int anoMesInicio2 = Integer.parseInt(anoMes) + 1;
				int anoMesFinal2 = Integer.parseInt(anoMes.substring(0, 4) + "12");

				for (int periodo = anoMesInicio2; periodo <= anoMesFinal2; periodo++) {
					RelatorioAvisoCorteVO relAvisoCorteVO = new RelatorioAvisoCorteVO();
					relAvisoCorteVO.setMes(Integer.parseInt(String.valueOf(periodo).substring(4, 6)));
					relAvisoCorteVO.setDescricaoMes(mapaMesesAno.get(Integer.parseInt(String.valueOf(periodo).substring(4, 6))));
					relAvisoCorteVO.setAteUmDia(0);
					relAvisoCorteVO.setAteDoisDias(0);
					relAvisoCorteVO.setAteTresDias(0);
					relAvisoCorteVO.setMaiorTresDias(0);
					listaRelatorioAvisoCorteVo.add(relAvisoCorteVO);
				}
			}

			Map<String, Object> parametros = new HashMap<String, Object>();

			// Imagem da empresa
			ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

			Empresa principal = controladorEmpresa.obterEmpresaPrincipal();
			if(principal.getLogoEmpresa() != null) {
				parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(principal.getChavePrimaria()));
			}

			parametros.put("nomeResponsavel", filtro.get("nomeResponsavel"));
			parametros.put("nomeArquivo", "relatorioPrazoMinimoEnvioAvisoCorte.pdf");
			parametros.put("anoReferencia", anoMes.substring(0, 4));

			String nomeFormato = (String) filtro.get("formatoImpressao");
			FormatoImpressao formato = FormatoImpressao.valueOf(nomeFormato);
			relatorio = RelatorioUtil.gerarRelatorio(listaRelatorioAvisoCorteVo, parametros,
							ControladorRelatorioPrazoMinimoAntecedencia.RELATORIO_PRAZO_MINIMO_AVISO_CORTE, formato);

		}

		return relatorio;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioPrazoMinimoAntecedencia#gerarRelatorioNotificacaoCorte(java.util.Map)
	 */
	@Override
	public byte[] gerarRelatorioNotificacaoCorte(Map<String, Object> filtro) throws GGASException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		ServiceLocator.getInstancia().getBeanPorID(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

		ControladorMedidor controladorMedidor = (ControladorMedidor) ServiceLocator.getInstancia().getBeanPorID(
						ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);

		ControladorAvisoCorte controladorAvisoCorte = (ControladorAvisoCorte) ServiceLocator.getInstancia().getBeanPorID(
						ControladorAvisoCorte.BEAN_ID_CONTROLADOR_AVISO_CORTE);

		byte[] relatorio = null;

		if(filtro != null && filtro.containsKey("mesAno")) {

			Map<Integer, String> mapaMesesAno = Mes.MESES_ANO;

			Collection<RelatorioAvisoCorteVO> listaRelatorioAvisoCorteVo = new ArrayList<RelatorioAvisoCorteVO>();

			String anoMes = Util.converterMesAnoEmAnoMes((String) filtro.get("mesAno"));
			int anoMesInicio = Integer.parseInt(anoMes.substring(0, 4) + "01");
			int anoMesFinal = Integer.parseInt(anoMes);
			int mesFinal = Integer.parseInt(anoMes.substring(4, 6));

			for (int periodo = anoMesInicio; periodo <= anoMesFinal; periodo++) {

				Map<String, Date> mapaDataPeriodo = Util.obterDataInicialDataFimPeriodo(periodo);

				Collection<HistoricoOperacaoMedidor> listaBloqueios = controladorMedidor.obterHistoricoOperacaoMedidorPorPeriodo(
								OperacaoMedidor.CODIGO_BLOQUEIO, mapaDataPeriodo.get("dataInicial"), mapaDataPeriodo.get("dataFinal"));

				// Para o relatório referente a notificação as colunas respectivamente abaixo são:
				// até 3 dias, até 10 dias, até 15 dias e maior que 15 dias
				Integer ateUmDia = 0;
				Integer ateDoisDias = 0;
				Integer ateTresDias = 0;
				Integer maiorTresDias = 0;

				Iterator<HistoricoOperacaoMedidor> iterator = listaBloqueios.iterator();
				while(iterator.hasNext()) {

					HistoricoOperacaoMedidor historico = iterator.next();

					if(historico.getPontoConsumo() != null) {

						Map<String, Object> filtroAvisoCorte = new HashMap<String, Object>();
						filtroAvisoCorte.put("idPontoConsumo", historico.getPontoConsumo().getChavePrimaria());
						filtroAvisoCorte.put("dataNotificacao", historico.getDataRealizada());
						filtroAvisoCorte.put("ordenacao", "dataNotificacao");

						Collection<AvisoCorte> listaAvisoCorteBanco = controladorAvisoCorte.listar(filtroAvisoCorte);

						if(listaAvisoCorteBanco != null && !listaAvisoCorteBanco.isEmpty()) {

							Date dataNotificacao = listaAvisoCorteBanco.iterator().next().getDataNotificacao();
							Integer diferencaDias = Util.intervaloDatas(dataNotificacao, historico.getDataRealizada());
							// Para o relatório referente a notificação as colunas respectivamente abaixo são:
							// até 3 dias, até 10 dias, até 15 dias e maior que 15 dias
							if(diferencaDias <= 3) {
								ateUmDia = ateUmDia + 1;
							} else if(diferencaDias == 10) {
								ateDoisDias = ateDoisDias + 1;
							} else if(diferencaDias == 15) {
								ateTresDias = ateTresDias + 1;
							} else if(diferencaDias > 15) {
								maiorTresDias = maiorTresDias + 1;
							}

						}

					}

				}

				RelatorioAvisoCorteVO relAvisoCorteVO = new RelatorioAvisoCorteVO();
				relAvisoCorteVO.setMes(Integer.valueOf(String.valueOf(periodo).substring(4, 6)));
				relAvisoCorteVO.setDescricaoMes(mapaMesesAno.get(Integer.valueOf(String.valueOf(periodo).substring(4, 6))));
				relAvisoCorteVO.setAteUmDia(ateUmDia);
				relAvisoCorteVO.setAteDoisDias(ateDoisDias);
				relAvisoCorteVO.setAteTresDias(ateTresDias);
				relAvisoCorteVO.setMaiorTresDias(maiorTresDias);
				listaRelatorioAvisoCorteVo.add(relAvisoCorteVO);

			}

			if(mesFinal < 12) {
				int anoMesInicio2 = Integer.parseInt(anoMes) + 1;
				int anoMesFinal2 = Integer.parseInt(anoMes.substring(0, 4) + "12");

				for (int periodo = anoMesInicio2; periodo <= anoMesFinal2; periodo++) {
					RelatorioAvisoCorteVO relAvisoCorteVO = new RelatorioAvisoCorteVO();
					relAvisoCorteVO.setMes(Integer.parseInt(String.valueOf(periodo).substring(4, 6)));
					relAvisoCorteVO.setDescricaoMes(mapaMesesAno.get(Integer.parseInt(String.valueOf(periodo).substring(4, 6))));
					relAvisoCorteVO.setAteUmDia(0);
					relAvisoCorteVO.setAteDoisDias(0);
					relAvisoCorteVO.setAteTresDias(0);
					relAvisoCorteVO.setMaiorTresDias(0);
					listaRelatorioAvisoCorteVo.add(relAvisoCorteVO);
				}
			}

			Map<String, Object> parametros = new HashMap<String, Object>();

			// Imagem da empresa
			ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

			Empresa principal = controladorEmpresa.obterEmpresaPrincipal();
			if(principal.getLogoEmpresa() != null) {
				parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(principal.getChavePrimaria()));
			}

			parametros.put("nomeResponsavel", filtro.get("nomeResponsavel"));
			parametros.put("nomeArquivo", "relatorioPrazoMinimoNotificacaoCorte.pdf");
			parametros.put("anoReferencia", anoMes.substring(0, 4));

			String nomeFormato = (String) filtro.get("formatoImpressao");
			FormatoImpressao formato = FormatoImpressao.valueOf(nomeFormato);
			relatorio = RelatorioUtil.gerarRelatorio(listaRelatorioAvisoCorteVo, parametros,
							ControladorRelatorioPrazoMinimoAntecedencia.RELATORIO_PRAZO_MINIMO_NOTIFICACAO_CORTE, formato);

		}

		return relatorio;

	}

}
