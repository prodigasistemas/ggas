/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *13/11/2013
 * vpessoa
 * 08:35:33
 */

package br.com.ggas.relatorio.impl;

import java.awt.Image;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;

import org.apache.commons.beanutils.BeanToPropertyValueTransformer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import com.ibm.icu.util.Calendar;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoClienteVO;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistorico;
import br.com.ggas.atendimento.chamado.dominio.ChamadoHistoricoVO;
import br.com.ggas.atendimento.chamado.dominio.ChamadoVO;
import br.com.ggas.atendimento.chamado.dominio.ChamadoVORelatatorio;
import br.com.ggas.atendimento.chamado.dominio.ChamadoVORelatorioClientePontoConsumo;
import br.com.ggas.atendimento.chamado.dominio.ChamadoVORelatorioQuantitativo;
import br.com.ggas.atendimento.chamado.dominio.ClienteVO;
import br.com.ggas.atendimento.chamado.dominio.FiltrosRelatorioHelper;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.questionario.dominio.SituacaoQuestionario;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.resposta.negocio.ControladorResposta;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVORelatorio;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.faturamento.apuracaopenalidade.impl.PontoConsumoVO;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.relatorio.ChamadoResumoVO;
import br.com.ggas.relatorio.ControladorRelatorioChamado;
import br.com.ggas.relatorio.RelatorioSolicitacaoChamadoVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import oracle.net.aso.p;

/**
 * @author vpessoa
 */
@Service("controladorRelatorioChamado")
@Transactional
public class ControladorRelatorioChamadoImpl implements ControladorRelatorioChamado {

	private static final int ULTIMA_VIRGULA = 2;

	private static final String PERIODO_DE = "Período de ";

	@Autowired
	private ControladorChamado controladorChamado;

	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;
	
	private static final int MAX_SQL_IN = 1000;

	/**
	 * Controlador Questionario
	 */
	@Autowired
	private ControladorQuestionario controladorQuestionario;

	/**
	 * Controlador resposta
	 */
	@Autowired
	private ControladorResposta controladorResposta;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioChamado#gerarRelatorio(java.lang.Long)
	 */
	@Override
	public byte[] gerarRelatorio(Long chavePrimariaChamado, Boolean comAS) throws NegocioException {

		Chamado chamado = controladorChamado.obterChamado(chavePrimariaChamado);
		ChamadoVORelatatorio relatorio = this.converterChamadoParaVO(chamado, comAS);

		Map<String, Object> parametros = new HashMap<String, Object>();
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		parametros.put("imagem",
						Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		FormatoImpressao formatoImpressao = FormatoImpressao.PDF;
		Collection<Object> collRelatorio = new ArrayList<Object>();
		collRelatorio.add(relatorio);

		return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_CHAMADO, formatoImpressao);
	}

	/**
	 * Converter chamado para vo.
	 * @param comAS - {@link Boolean}
	 * @param chamado
	 *            the chamado
	 * @return the chamado vo relatatorio
	 */
	public ChamadoVORelatatorio converterChamadoParaVO(Chamado chamado, Boolean comAS) {

		ChamadoVORelatatorio relatorio = new ChamadoVORelatatorio();
		relatorio.setComAutorizacaoServico(comAS);
		if(chamado.getChamadoAssunto() != null) {
			relatorio.setAssuntoChamado(chamado.getChamadoAssunto().getDescricao());
		}
		relatorio.setCanalAtendimento(chamado.getCanalAtendimento().getDescricao());
		if(chamado.getImovel() != null) {
			relatorio.setCepImovel(chamado.getImovel().getQuadraFace().getEndereco().getCep().getCep());
			relatorio.setEnderecoImovel(chamado.getImovel().getEnderecoFormatado());
			relatorio.setMatriculaImovel(String.valueOf(chamado.getImovel().getChavePrimaria()));
			relatorio.setNomeFantasiaImovel(chamado.getImovel().getNome());
		}
		// FIXME: tratar
		if(chamado.getCliente() != null) {
			if (chamado.getCliente().getCpf() != null) {
				relatorio.setCpfCnpjCliente(chamado.getCliente().getCpf());
			} else {
				relatorio.setCpfCnpjCliente(chamado.getCliente().getCnpj());
			}
			relatorio.setNomeCliente(chamado.getCliente().getNome());
			relatorio.setNomeFantasiaCliente(chamado.getCliente().getNomeFantasia());
			relatorio.setPassaporteCliente(chamado.getCliente().getNumeroPassaporte());
		}
		relatorio.setCpfCnpjSolicitante(chamado.getCpfCnpjSolicitante());
		relatorio.setDescricao(chamado.getDescricao());
		if(chamado.getPontoConsumo() != null) {
			relatorio.setDescricaoPontoConsumo(chamado.getPontoConsumo().getDescricao());
		}
		relatorio.setNomeSolicitante(chamado.getNomeSolicitante());
		if(chamado.getContrato() != null) {
			relatorio.setNumeroContrato(chamado.getContrato().getNumeroFormatado());
		}
		relatorio.setNumeroProtocolo(String.valueOf(chamado.getProtocolo().getNumeroProtocolo()));
		if (chamado.getUsuarioResponsavel() != null && chamado.getUsuarioResponsavel().getFuncionario() != null) {
			relatorio.setResponsavel(chamado.getUsuarioResponsavel().getFuncionario().getNome());
		}
		if(chamado.getChamadoAssunto() != null) {
			relatorio.setTipoChamado(chamado.getChamadoAssunto().getChamadoTipo().getDescricao());
		}
		if(chamado.getUnidadeOrganizacional() != null) {
			relatorio.setUnidadeOrganizacional(chamado.getUnidadeOrganizacional().getDescricao());
		}
		relatorio.setChamadoHistoricoList(converterChamadoHistoricoParaChamadoHistoricoVO(chamado.getChamadoHistoricos()));

		if (comAS) {
			relatorio.setServicoAutorizacaoList(converterServicoAutorizacaoParaServicoAutorizacaoVO(chamado.getListaServicoAutorizacao()));
		}
		
		if(chamado.getHoraAcionamento() != null) {
			relatorio.setHoraAcionamento(chamado.getHoraAcionamento());
		}

		return relatorio;
	}

	/**
	 * Converte para para formato do relatorio
	 * 
	 * @param listaServicoAutorizacao
	 * @return servicoAutorizacaoVORelatorio
	 */
	private List<ServicoAutorizacaoVORelatorio> converterServicoAutorizacaoParaServicoAutorizacaoVO(
			Collection<ServicoAutorizacao> listaServicoAutorizacao) {

		List<ServicoAutorizacaoVORelatorio> servicoAutorizacaoVORelatorioList = new ArrayList<ServicoAutorizacaoVORelatorio>();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		for (ServicoAutorizacao servicoAutorizacao : listaServicoAutorizacao) {
			ServicoAutorizacaoVORelatorio servicoAutorizacaoVORelatorio = new ServicoAutorizacaoVORelatorio();

			if (servicoAutorizacao.getDataEncerramento() != null) {
				servicoAutorizacaoVORelatorio.setDataEncerramento(formato.format(servicoAutorizacao.getDataEncerramento()));
			} else {
				servicoAutorizacaoVORelatorio.setDataEncerramento("");
			}

			servicoAutorizacaoVORelatorio.setDataPrevisaoEncerramento(formato.format(servicoAutorizacao.getDataPrevisaoEncerramento()));
			servicoAutorizacaoVORelatorio.setServicoTipo(servicoAutorizacao.getServicoTipo().getDescricao());
			servicoAutorizacaoVORelatorio.setPrioridadeServicoTipo(servicoAutorizacao.getServicoTipoPrioridade().getDescricao());
			servicoAutorizacaoVORelatorio.setEquipe(servicoAutorizacao.getEquipe().getNome());
			servicoAutorizacaoVORelatorio.setStatus(servicoAutorizacao.getStatus().getDescricao());

			servicoAutorizacaoVORelatorioList.add(servicoAutorizacaoVORelatorio);
		}

		return servicoAutorizacaoVORelatorioList;

	}

	/**
	 * Converter chamado historico para chamado historico vo.
	 * 
	 * @param chamadoHistorico
	 *            the chamado historico
	 * @return the list
	 */
	public List<ChamadoHistoricoVO> converterChamadoHistoricoParaChamadoHistoricoVO(Collection<ChamadoHistorico> chamadoHistorico) {

		List<ChamadoHistorico> chamadoHistoricoList = new ArrayList<ChamadoHistorico>();
		chamadoHistoricoList.addAll(chamadoHistorico);
		List<ChamadoHistoricoVO> chamadoHistoricoVOList = new ArrayList<ChamadoHistoricoVO>();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		ChamadoHistoricoVO vo;
		for (ChamadoHistorico historico : chamadoHistoricoList) {
			vo = new ChamadoHistoricoVO();
			vo.setDescricao(historico.getDescricao());
			if(historico.getMotivo() != null) {
				vo.setMotivo(historico.getMotivo().getDescricao());
			}
			if(historico.getOperacao() != null) {
				vo.setOperacao(historico.getOperacao().getDescricao());
			}
			vo.setDataUltimaAlteracao(formato.format(historico.getUltimaAlteracao()));
			vo.setDataUltimaAlteracaoSemFormatar(historico.getUltimaAlteracao());
			if(historico.getUsuario().getFuncionario() != null) {
				vo.setUsuario(historico.getUsuario().getFuncionario().getNome());
			}
			if(historico.getUsuarioResponsavel() != null && historico.getUsuarioResponsavel().getFuncionario() != null) {
				vo.setUsuarioResponsavel(historico.getUsuarioResponsavel().getFuncionario().getNome());
			}
			if(historico.getUnidadeOrganizacional() != null) {
				vo.setUnidadeOrganizacional(historico.getUnidadeOrganizacional().getDescricao());
			}
			if(historico.getStatus() != null) {
				vo.setSituacao(historico.getStatus().getDescricao());
			}
			chamadoHistoricoVOList.add(vo);
		}
		return Util.ordenarColecaoPorAtributo(chamadoHistoricoVOList, "dataUltimaAlteracaoSemFormatar", Boolean.TRUE);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.relatorio.ControladorRelatorioChamado#gerarRelatorioChamadoClientePontoConsumo(br.com.ggas.atendimento.chamado.dominio
	 * .ChamadoVO, br.com.ggas.util.FormatoImpressao, java.lang.String)
	 */
	@Override
	public byte[] gerarRelatorioChamadoClientePontoConsumo(ChamadoVO chamadoVO, FormatoImpressao formatoImpressao, String exibirFiltros)
			throws GGASException {
		
		List<Chamado> listaChamados = (List<Chamado>) controladorChamado
				.consultarChamado(chamadoVO, Boolean.TRUE);
		
		List<Chamado> listaChamadoPorReferencia = montarListaChamadosPorReferencia(listaChamados, chamadoVO);

		List<ChamadoClienteVO> dados = converterChamadoParaChamadoVORealatorio(listaChamadoPorReferencia);
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		parametros.put("dataAtual", formato.format(new Date()));
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		Image logo = new ImageIcon(empresa.getLogoEmpresa()).getImage();
		parametros.put("logo", logo);

		Collection<FiltrosRelatorioHelper> filtros = new HashSet<FiltrosRelatorioHelper>();
		FiltrosRelatorioHelper filtro = new FiltrosRelatorioHelper();

		if(chamadoVO.getNumeroProtocolo() != null){
			filtro.setNumeroProtocolo(chamadoVO.getNumeroProtocolo().toString());
		}else{
			filtro.setNumeroProtocolo("");
		}

		if(chamadoVO.getChamadoAssuntos() != null) {
			StringBuilder descricaoFormatada = new StringBuilder();
			String separador = ",";
			for (ChamadoAssunto chamado : chamadoVO.getChamadoAssuntos()) {
				if(chamado.getDescricao() != null) {
					descricaoFormatada.append(chamado.getDescricao());
					descricaoFormatada.append(separador);
				}

			}

			if(chamadoVO.getChamadoAssuntos() != null){
				filtro.setAssuntoChamado(descricaoFormatada.toString());
			}else{
				filtro.setAssuntoChamado("");
			}
		}

		if(chamadoVO.getServicosTipo() != null) {
			StringBuilder descricaoFormatada = new StringBuilder();
			String separador = ",";
			for (ServicoTipo servicos : chamadoVO.getServicosTipo()) {
				String descricao = servicos.getDescricao();
				if(descricao != null) {
					descricaoFormatada.append(descricao);
					descricaoFormatada.append(separador);
				}

			}

			if(chamadoVO.getServicosTipo() != null){
				filtro.setTipoServico(descricaoFormatada.toString());
			}else{
				filtro.setTipoServico("");
			}
		}

		if(chamadoVO.getNomeCliente() != null){
			filtro.setCliente(chamadoVO.getNomeCliente());
		}else{
			filtro.setCliente("");
		}
		if (!Util.isNullOrEmpty(chamadoVO.getListaCanalAtendimento())) {
			StringBuilder canalAtendimento = new StringBuilder();
			for (CanalAtendimento canal : chamadoVO.getListaCanalAtendimento()) {
				canalAtendimento.append(canal.getDescricao()).append(", ");
			}
			canalAtendimento.setLength(canalAtendimento.length() - ULTIMA_VIRGULA);
			filtro.setCanalAtendimento(canalAtendimento.toString());
		}else{
			filtro.setCanalAtendimento("");
		}
		if(chamadoVO.getUnidadeOrganizacional() != null){
			filtro.setUnidadeOrganizacional(chamadoVO.getUnidadeOrganizacional().getDescricao());
		}else{
			filtro.setUnidadeOrganizacional("");
		}
		if(chamadoVO.getUsuarioResponsavel() != null){
			filtro.setResponsavel(chamadoVO.getUsuarioResponsavel().getFuncionario().getNome());
		}else{
			filtro.setResponsavel("");
		}

		preencherTipoServico(chamadoVO, filtro);
		preencherSegmento(chamadoVO, filtro);
		preencherStatus(chamadoVO, filtro);
		preencherPrazo(chamadoVO, filtro);

		parametros.put("exibirFiltros", Boolean.valueOf(exibirFiltros));
		if(chamadoVO.getDataInicioCriacao() != null && !"".equalsIgnoreCase(chamadoVO.getDataInicioCriacao())
						&& chamadoVO.getDataFimCriacao() != null && !"".equalsIgnoreCase(chamadoVO.getDataFimCriacao())) {
			parametros.put("periodo", PERIODO_DE + chamadoVO.getDataInicioCriacao() + " a " + chamadoVO.getDataFimCriacao());
		}

		if (StringUtils.isNotBlank(chamadoVO.getDataInicioResolucao()) && StringUtils.isNotBlank(chamadoVO.getDataFimResolucao())) {
			filtro.setPeriodoResolucao(
					PERIODO_DE.concat(chamadoVO.getDataInicioResolucao()).concat(" a ").concat(chamadoVO.getDataFimResolucao()));
		} else {
			filtro.setPeriodoResolucao("");
		}
		
		filtros.add(filtro);
		parametros.put("filtros", filtros);
		parametros.put("listaSolicitacaoChamado", montarListaSolicitacaoChamado(listaChamados, chamadoVO));
		parametros.put("logoArsal", Constantes.URL_IMAGENS_GGAS + "logoArsal.png");
		parametros.put("newLogoAlgas", Constantes.URL_IMAGENS_GGAS + "newLogoAlgas.png");
		parametros.put("listaChamadosReferencia", dados);
		parametros.put("referencia", obterReferenciaRelatorioChamado(chamadoVO));

		Boolean hasIndicadorRegulatorio = false;
		for(Chamado chamado : listaChamados) {
			if(chamado.getChamadoAssunto() != null) {
				if(chamado.getChamadoAssunto().getIndicadorRegulatorio() != null) {
					hasIndicadorRegulatorio = true;
					parametros.put("ocorrencias", "PRAZO MÁXIMO PARA A VERIFICAÇÃO DE "+chamado.getChamadoAssunto().getIndicadorRegulatorio().getDescricao());
					break;
				}
			}
		}
		String nomeRelatorioRegulatorio = "";
		for(Chamado chamado : listaChamados) {
			if(chamado.getChamadoAssunto() != null) {
				if(StringUtils.isNotEmpty(chamado.getChamadoAssunto().getNomeRelatorioRegulatorio())) {
					nomeRelatorioRegulatorio = chamado.getChamadoAssunto().getNomeRelatorioRegulatorio();
					break;
				}
			}
		}

		parametros.put("nomeRelatorioRegulatorio",nomeRelatorioRegulatorio);
		parametros.put("hasIndicadorRegulatorio",hasIndicadorRegulatorio);
		
		if(!hasIndicadorRegulatorio) {
			parametros.put("ocorrencias", chamadoVO.getChamadoAssuntos().stream().map(ChamadoAssunto::getDescricao)
				.collect(Collectors.joining(", ")));
		}

		
		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIOS_CHAMADO_CLIENTE_PONTOCOSUMO, formatoImpressao);
	}

	
	private List<Chamado> montarListaChamadosPorReferencia(List<Chamado> listaChamados, ChamadoVO chamadoVO) {

		if (!StringUtils.isEmpty(chamadoVO.getPeriodoCriacao())) {
			return listaChamados.parallelStream()
					.filter(p -> p.getProtocolo().getUltimaAlteracao() != null && this.verificarChamadoNoPeriodoSelecionado(p.getProtocolo().getUltimaAlteracao(),
							Integer.valueOf(chamadoVO.getPeriodoCriacao().split("/")[0])))
					.collect(Collectors.toList());
		} else if (!StringUtils.isEmpty(chamadoVO.getPeriodoPrevisaoEncerramento())) {
			return listaChamados.parallelStream().filter(p -> p.getDataPrevisaoEncerramento() != null && this.verificarChamadoNoPeriodoSelecionado(p.getDataPrevisaoEncerramento(),
					Integer.valueOf(chamadoVO.getPeriodoPrevisaoEncerramento().split("/")[0]))).collect(Collectors.toList());
		} else if (!StringUtils.isEmpty(chamadoVO.getPeriodoResolucao())) {
			return listaChamados.parallelStream().filter(p -> p.getDataResolucao() != null && this.verificarChamadoNoPeriodoSelecionado(p.getDataResolucao(),
					Integer.valueOf(chamadoVO.getPeriodoResolucao().split("/")[0]))).collect(Collectors.toList());
		}

		return new ArrayList<Chamado>();

	}
	
	private Boolean verificarChamadoNoPeriodoSelecionado(Date data, Integer mes) {
		
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(data);
		
		if(calendario.get(Calendar.MONTH) == mes - 1) {
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}

	private String obterReferenciaRelatorioChamado(ChamadoVO chamadoVO) {
		if(!StringUtils.isEmpty(chamadoVO.getPeriodoCriacao())) {
			return chamadoVO.getPeriodoCriacao();
		} else if(!StringUtils.isEmpty(chamadoVO.getPeriodoPrevisaoEncerramento())) {
			return chamadoVO.getPeriodoPrevisaoEncerramento();
		} else if(!StringUtils.isEmpty(chamadoVO.getPeriodoResolucao())) {
			return chamadoVO.getPeriodoResolucao();
		}
		
		return "";
	}

	private List<RelatorioSolicitacaoChamadoVO> montarListaSolicitacaoChamado(List<Chamado> listaChamados, ChamadoVO chamadoVO) {
		List<RelatorioSolicitacaoChamadoVO> retorno = new ArrayList<RelatorioSolicitacaoChamadoVO>();
		Calendar periodoSelecionado = Calendar.getInstance();
		String[] periodo = null;
		
		if(!StringUtils.isEmpty(chamadoVO.getPeriodoCriacao())) {
			periodo = chamadoVO.getPeriodoCriacao().split("/");
		} else if(!StringUtils.isEmpty(chamadoVO.getPeriodoPrevisaoEncerramento())) {
			periodo = chamadoVO.getPeriodoPrevisaoEncerramento().split("/");
		} else if (!StringUtils.isEmpty(chamadoVO.getPeriodoResolucao())) {
			periodo = chamadoVO.getPeriodoResolucao().split("/");
		}
		
		periodoSelecionado.set(Calendar.MONTH, Integer.valueOf(periodo[0]) - 1);
		periodoSelecionado.set(Calendar.YEAR, Integer.valueOf(periodo[1]));
		
		
		for(int i = 0; i < 12; i ++) {
			RelatorioSolicitacaoChamadoVO relatorio = new RelatorioSolicitacaoChamadoVO();
			relatorio.setMes(DataUtil.obterNomeMesPeloNumero(i + 1));
			
			if(i > periodoSelecionado.get(Calendar.MONTH)) {
				relatorio.setDias1("");
				relatorio.setDias2("");
				relatorio.setDias3("");
				relatorio.setDiasMaior3("");
			}
			
			retorno.add(relatorio);
		}
		
		for(Chamado chamado : listaChamados) {
			if(chamado.getDataResolucao() != null) {
				Integer dias = DataUtil.diferencaDiasEntreDatas(chamado.getProtocolo().getUltimaAlteracao(), chamado.getDataResolucao());
				Integer mes = DataUtil.obterMes(chamado.getDataResolucao());
				RelatorioSolicitacaoChamadoVO relatorio = retorno.get(mes);
				
				if (dias <= 1) {
					relatorio.setDias1(adicionarQuantidadeSolicitacao(relatorio.getDias1()));
				} else if (dias == 2) {
					relatorio.setDias2(adicionarQuantidadeSolicitacao(relatorio.getDias2()));
				} else if (dias == 3) {
					relatorio.setDias3(adicionarQuantidadeSolicitacao(relatorio.getDias3()));
				} else if (dias > 3) {
					relatorio.setDiasMaior3(adicionarQuantidadeSolicitacao(relatorio.getDiasMaior3()));
				}
				
			}
		}
		
		return retorno;
	}
	
	private String adicionarQuantidadeSolicitacao(String quantidadeSolicitacao) {
		
		if(StringUtils.isEmpty(quantidadeSolicitacao)) {
			return "";
		}
		Integer quantidade = Integer.valueOf(quantidadeSolicitacao) + 1;
		
		return String.valueOf(quantidade);
	}

	/**
	 * Preencher filtro de segmento
	 * 
	 * @param chamadoVO
	 * @param parametros
	 * @throws NegocioException
	 */
	private void preencherSegmento(ChamadoVO chamadoVO, FiltrosRelatorioHelper filtro) throws NegocioException {
		filtro.setSegmento("");
		if (chamadoVO.getIdSegmentoChamado() != null && chamadoVO.getIdSegmentoChamado() != -1L) {
			ControladorSegmento controladorSegmento = ServiceLocator.getInstancia().getControladorSegmento();
			EntidadeNegocio entidade = controladorSegmento.obter(chamadoVO.getIdSegmentoChamado());

			if (entidade != null) {
				Segmento segmento = (Segmento) entidade;
				filtro.setSegmento(segmento.getDescricao());
			}
		}
	}

	/**
	 * Preencher filtro de status
	 * 
	 * @param chamadoVO
	 * @param parametros
	 */
	private void preencherStatus(ChamadoVO chamadoVO, FiltrosRelatorioHelper filtro) {
		if(chamadoVO.getListaStatus() != null) {
			StringBuilder descricaoFormatada = new StringBuilder();
			String separador = ",";
			for (String status : chamadoVO.getListaStatus()) {
				if(status != null) {
					descricaoFormatada.append(status);
					descricaoFormatada.append(separador);
				}
			}

			if(chamadoVO.getListaStatus() != null){
				filtro.setStatus(descricaoFormatada.toString());
			}else{
				filtro.setStatus("");
			}
		}
	}

	/**
	 * Preenche o filtro de prazo
	 * 
	 * @param chamadoVO
	 * @param parametros
	 */
	private void preencherPrazo(ChamadoVO chamadoVO, FiltrosRelatorioHelper filtro) {
		if (chamadoVO.getListaPrazo() != null && !chamadoVO.getListaPrazo().isEmpty()) {

			StringBuilder restricaoPrazo = new StringBuilder();
			if (chamadoVO.getListaPrazo().contains("ATRASADOS")) {
				restricaoPrazo.append("Atrasados");
			}
			if (chamadoVO.getListaPrazo().contains("HOJE")) {
				Util.acrescentarConjuncao(restricaoPrazo);
				restricaoPrazo.append("a Vencer");
			}
			if (chamadoVO.getListaPrazo().contains("FUTUROS")) {
				Util.acrescentarConjuncao(restricaoPrazo);
				restricaoPrazo.append("no Prazo");
			}
			filtro.setPrazo(restricaoPrazo.toString());
		} else {
			filtro.setPrazo("");
		}
	}
	
	/**
	 * Carrega a situação do questionário dos chamados
	 * 
	 * @param listaChamados
	 */
	private void carregarSituacaoQuestionario(Collection<Chamado> listaChamados) {
		for (Chamado chamado : listaChamados) {
			if (chamado.getChamadoAssunto().getQuestionario() != null) {

				boolean respondido = controladorResposta
						.questionarioRespondido(chamado.getChamadoAssunto().getQuestionario().getChavePrimaria(), chamado);
				if (respondido) {
					chamado.setSituacaoQuestionario(SituacaoQuestionario.RESPONDIDO);
				} else {
					chamado.setSituacaoQuestionario(SituacaoQuestionario.NAO_RESPONDIDO);
				}
			} else {
				chamado.setSituacaoQuestionario(SituacaoQuestionario.NAO_POSSUI);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.relatorio.ControladorRelatorioChamado#gerarRelatorioChamadoClientePontoConsumo(br.com.ggas.atendimento.chamado.dominio
	 * .ChamadoVO, br.com.ggas.util.FormatoImpressao, java.lang.String)
	 */
	@Override
	public byte[] gerarRelatorioChamadoQuantitativo(ChamadoVO chamadoVO, FormatoImpressao formatoImpressao, String exibirFiltros)
			throws GGASException {

		List<Chamado> listaChamados = (List<Chamado>) controladorChamado.consultarChamado(chamadoVO, Boolean.TRUE);
		
		listaChamados = montarListaChamadosPorReferencia(listaChamados, chamadoVO);
		
		
		carregarSituacaoQuestionario(listaChamados);

		ChamadoVORelatorioQuantitativo dados = new ChamadoVORelatorioQuantitativo(
				(List<Chamado>) controladorChamado.consultarChamado(chamadoVO, Boolean.TRUE));

		
		Map<String, Object> parametros = new HashMap<String, Object>();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		parametros.put("dataAtual", formato.format(new Date()));
		ControladorEmpresa controladorEmpresa =
				(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		if (empresa.getLogoEmpresa() != null) {
			Image logo = new ImageIcon(empresa.getLogoEmpresa()).getImage();

			parametros.put("logo", logo);
		}

		Collection<FiltrosRelatorioHelper> filtros = new HashSet<FiltrosRelatorioHelper>();
		FiltrosRelatorioHelper filtro = new FiltrosRelatorioHelper();

		preencherDadosQuantitativos(dados, parametros);

		if (chamadoVO.getNumeroProtocolo() != null) {
			filtro.setNumeroProtocolo(chamadoVO.getNumeroProtocolo().toString());
		} else {
			filtro.setNumeroProtocolo("");
		}

		if (chamadoVO.getChamadoAssuntos() != null) {
			StringBuilder descricaoFormatada = new StringBuilder();
			String separador = ",";
			for (ChamadoAssunto assunto : chamadoVO.getChamadoAssuntos()) {
				if (assunto.getDescricao() != null) {
					descricaoFormatada.append(assunto.getDescricao());
					descricaoFormatada.append(separador);
				}

			}
			filtro.setAssuntoChamado(descricaoFormatada.toString());
		} else {
			filtro.setAssuntoChamado("");
		}

		if (chamadoVO.getNomeCliente() != null) {
			filtro.setCliente(chamadoVO.getNomeCliente());
		} else {
			filtro.setCliente("");
		}
		if (!Util.isNullOrEmpty(chamadoVO.getListaCanalAtendimento())) {
			StringBuilder canalAtendimento = new StringBuilder();
			for (CanalAtendimento canal : chamadoVO.getListaCanalAtendimento()) {
				canalAtendimento.append(canal.getDescricao()).append(", ");
			}
			canalAtendimento.setLength(canalAtendimento.length() - ULTIMA_VIRGULA);
			filtro.setCanalAtendimento(canalAtendimento.toString());
		} else {
			filtro.setCanalAtendimento("");
		}
		if (chamadoVO.getUnidadeOrganizacional() != null) {
			filtro.setUnidadeOrganizacional(chamadoVO.getUnidadeOrganizacional().getDescricao());
		} else {
			filtro.setUnidadeOrganizacional("");
		}
		if (chamadoVO.getUsuarioResponsavel() != null) {
			filtro.setResponsavel(chamadoVO.getUsuarioResponsavel().getFuncionario().getNome());
		} else {
			filtro.setResponsavel("");
		}

		preencherTipoServico(chamadoVO, filtro);
		preencherStatus(chamadoVO, filtro);
		preencherSegmento(chamadoVO, filtro);
		preencherPrazo(chamadoVO, filtro);

		filtro.setCategoria("");
		filtro.setQuestionario("");
		filtro.setCategoriasSegmentos("");

		parametros.put("exibirFiltros", Boolean.valueOf(exibirFiltros));

		if (chamadoVO.getDataInicioCriacao() != null && !"".equalsIgnoreCase(chamadoVO.getDataInicioCriacao())
				&& chamadoVO.getDataFimCriacao() != null && !"".equalsIgnoreCase(chamadoVO.getDataFimCriacao())) {
			parametros.put("periodo", PERIODO_DE + chamadoVO.getDataInicioCriacao() + " a " + chamadoVO.getDataFimCriacao());
		}

		if (StringUtils.isNotBlank(chamadoVO.getDataInicioResolucao()) && StringUtils.isNotBlank(chamadoVO.getDataFimResolucao())) {
			filtro.setPeriodoResolucao(
					PERIODO_DE.concat(chamadoVO.getDataInicioResolucao()).concat(" a ").concat(chamadoVO.getDataFimResolucao()));
		} else {
			filtro.setPeriodoResolucao("");
		}

		filtros.add(filtro);
		parametros.put("filtros", filtros);

		return RelatorioUtil.gerarRelatorio(null, parametros, RELATORIOS_QUANTITATIVO_CHAMADO, formatoImpressao);
	}

	/**
	 * Preencher filtro tipo serviço
	 * 
	 * @param chamadoVO
	 * @param parametros
	 */
	private void preencherTipoServico(ChamadoVO chamadoVO, FiltrosRelatorioHelper filtro) {
		if (chamadoVO.getServicosTipo() != null) {
			StringBuilder descricaoFormatada = new StringBuilder();
			String separador = ",";
			for (ServicoTipo servicos : chamadoVO.getServicosTipo()) {
				String descricao = servicos.getDescricao();
				if (descricao != null) {
					descricaoFormatada.append(descricao);
					descricaoFormatada.append(separador);
				}

			}
			filtro.setTipoServico(descricaoFormatada.toString());
		} else {
			filtro.setTipoServico("");
		}
	}

	/**
	 * Preenche dados do relatorio
	 * 
	 * @param dados
	 * @param parametros
	 */
	private void preencherDadosQuantitativos(ChamadoVORelatorioQuantitativo dados, Map<String, Object> parametros) {
		parametros.put("finalizadosNoPrazo", dados.getFinalizadosNoPrazo());
		parametros.put("finalizadosComAtraso", dados.getFinalizadosComAtraso());
		parametros.put("totalFinalizados", dados.getFinalizadosComAtraso() + dados.getFinalizadosNoPrazo());
		parametros.put("totalNaoFinalizados", dados.getEmAtraso() + dados.getEmAberto() + dados.getEmAndamento());
		parametros.put("emAndamento", dados.getEmAndamento());
		parametros.put("emAberto", dados.getEmAberto());
		parametros.put("emAtraso", dados.getEmAtraso());
		parametros.put("totalVazamentos", dados.getTotalVazamentos());
		parametros.put("vazamentosConfirmados", dados.getVazamentosConfirmados());
		parametros.put("vazamentosNaoConfirmados", dados.getTotalVazamentos() - dados.getVazamentosConfirmados());
		parametros.put("dia1a10", dados.getDia1a10());
		parametros.put("dia11a20", dados.getDia11a20());
		parametros.put("dia21a31", dados.getDia21a31());
		parametros.put("manha", dados.getManha());
		parametros.put("tarde", dados.getTarde());
		parametros.put("intervalo", dados.getIntervalo());
		parametros.put("foraHora", dados.getForaHora());
		parametros.put("totalChamados", dados.getTotalChamados());
		parametros.put("segmentos", dados.getSegmentos());
		parametros.put("categorias", dados.getCategorias());
		parametros.put("questionario", dados.getQuestionario());
		parametros.put("categoriasSegmentos", dados.getCategoriasSegmentos());
		parametros.put("listaCanalAtendimento", dados.getListaCanalAtendimento());
		parametros.put("emergencia", dados.getEmergencia());
		parametros.put("urgencia", dados.getUrgencia());
		parametros.put("comum", dados.getComum());
		parametros.put("convencional", dados.getConvencional());
	}

	/**
	 * Converter chamado para chamado vo realatorio.
	 * 
	 * @param chamados the chamados
	 * @return the list
	 * @throws NegocioException the negocio exception
	 */
	
	private List<ChamadoClienteVO> converterChamadoParaChamadoVORealatorio(List<Chamado> chamados)
					throws NegocioException {

		if(chamados.isEmpty()) {
			throw new NegocioException("ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO", true);
		}
		

		List<ChamadoClienteVO> listaChamadoClienteVO = new ArrayList<ChamadoClienteVO>();
		
		for(Chamado chamado : chamados) {
			ChamadoClienteVO chamadoClienteVO = new ChamadoClienteVO();
			
			chamadoClienteVO.setChamado(chamado);
			chamadoClienteVO.setCliente(chamado.getCliente());
			chamadoClienteVO.setPontoConsumo(chamado.getPontoConsumo());
			chamadoClienteVO.setNomeCliente(chamado.getCliente() != null ? chamado.getCliente().getNome() : "");
			chamadoClienteVO.setNome(chamado.getUsuarioResponsavel() != null ? chamado.getUsuarioResponsavel().getDescricao() : "");
			chamadoClienteVO.setDataAberturaFormatada(DataUtil.converterDataParaString(chamado.getProtocolo().getUltimaAlteracao()));
			chamadoClienteVO.setDataResolucaoFormatada(
					chamado.getDataResolucao() != null ? DataUtil.converterDataParaString(chamado.getDataResolucao())
							: "");
			
			listaChamadoClienteVO.add(chamadoClienteVO);
			
		}
		
		return listaChamadoClienteVO;
	}

	/**
	 * 
	 * @param chamados
	 * @param listaCliente
	 * @param listaChamadoSemCliente
	 */
	private void montaListaClientes(List<Chamado> chamados, List<Cliente> listaCliente,
			List<Chamado> listaChamadoSemCliente) {
		for (Chamado chamado2 : chamados) {
			if(chamado2.getCliente() != null) {
				if(!listaCliente.contains(chamado2.getCliente())) {
					listaCliente.add(chamado2.getCliente());
				}
			} else {
				listaChamadoSemCliente.add(chamado2);
			}
		}
	}

	/**
	 * 
	 * @param chamados
	 * @param cliente
	 * @param listaChamadoComClienteSemPontoConsumo
	 */
	private void iteraChamados(List<Chamado> chamados, Cliente cliente,
			List<Chamado> listaChamadoComClienteSemPontoConsumo) {
		for (Chamado chamado : chamados) {
			if(chamado.getCliente() != null && chamado.getCliente().equals(cliente) && chamado.getPontoConsumo() == null) {
				listaChamadoComClienteSemPontoConsumo.add(chamado);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private ListMultimap<Cliente, PontoConsumo> construirMapaPontosConsumo(List<Cliente> listaCliente)
			throws NegocioException {
		Collection<ContratoPontoConsumo> contratosPontoConsumo = new HashSet<ContratoPontoConsumo>();
		for(int i = 0; i < listaCliente.size(); i += MAX_SQL_IN) {
			int limite = Math.min(i + MAX_SQL_IN, listaCliente.size());
			Collection<Long> chavesClientes =
					CollectionUtils.collect(listaCliente.subList(i, limite),
							new BeanToPropertyValueTransformer("chavePrimaria"));
			contratosPontoConsumo.addAll(
					controladorPontoConsumo.listarContratoPontoConsumoPorCliente(
							chavesClientes.toArray(new Long[0])));
		}
		ImmutableListMultimap<Cliente, ContratoPontoConsumo> mapaCliente = Multimaps.index(
				contratosPontoConsumo,
				new Function<ContratoPontoConsumo, Cliente>(){
			
			@Override
			public Cliente apply(ContratoPontoConsumo input) {
				return input.getContrato().getClienteAssinatura();
			}
			
		});
		return Multimaps.transformValues(mapaCliente,
				new Function<ContratoPontoConsumo, PontoConsumo>(){
			
			@Override
			public PontoConsumo apply(ContratoPontoConsumo input) {
				return input.getPontoConsumo();
			}
			
		});
	}

	/**
	 * Converter ponto consumo para ponto consumo vo.
	 * 
	 * @param lista
	 *            the lista
	 * @param chamados
	 *            the chamados
	 * @return the list
	 */
	private List<PontoConsumoVO> converterPontoConsumoParaPontoConsumoVO(List<PontoConsumo> lista, List<Chamado> chamados) {

		List<PontoConsumoVO> listaPontoConsulmoVO = new ArrayList<PontoConsumoVO>();
		// criação da lista retornada

		PontoConsumoVO pontoConsumoVo = null;
		for (PontoConsumo pontoConsumo : lista) {
			pontoConsumoVo = new PontoConsumoVO();
			pontoConsumoVo.setPontoConsumo(pontoConsumo);
			List<Chamado> listaChamados = new ArrayList<Chamado>();
			for (Chamado chamado : chamados) {
				if(chamado.getPontoConsumo() != null && chamado.getPontoConsumo().equals(pontoConsumo)) {
					listaChamados.add(chamado);
				}
			}
			pontoConsumoVo.setListaChamado(listaChamados);
			if(pontoConsumoVo.getListaChamado() != null && !pontoConsumoVo.getListaChamado().isEmpty()) {
				listaPontoConsulmoVO.add(pontoConsumoVo);
			}
		}

		return listaPontoConsulmoVO;

	}

	/**
	 * Criar lista ponto consumo vo vazio.
	 * 
	 * @param lista
	 *            the lista
	 * @return the list
	 */
	private List<PontoConsumoVO> criarListaPontoConsumoVOVazio(List<Chamado> lista) {

		List<PontoConsumoVO> listPontoConsumoVOVazio = new ArrayList<PontoConsumoVO>();
		PontoConsumoVO pontoConsumoVOVazio = new PontoConsumoVO();
		pontoConsumoVOVazio.setListaChamado(lista);
		listPontoConsumoVOVazio.add(pontoConsumoVOVazio);

		return listPontoConsumoVOVazio;

	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.relatorio.ControladorRelatorioChamado#gerarRelatorioChamadoClientePontoConsumo(br.com.ggas.atendimento.chamado.dominio
	 * .ChamadoVO, br.com.ggas.util.FormatoImpressao, java.lang.String)
	 */
	@Override
	public byte[] gerarRelatorioChamadoAnaltico(ChamadoVO chamadoVO, FormatoImpressao formatoImpressao, String exibirFiltros)
			throws GGASException {

		List<Chamado> listaChamados = (List<Chamado>) controladorChamado.consultarChamado(chamadoVO, Boolean.TRUE);
		
		if (listaChamados.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO, true);
		}
		
		ControladorEmpresa controladorEmpresa =
				(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		
		parametros.put("listaChamados", listaChamados);

		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		
		parametros.put("imagem",
				Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
		
		criarParametroPeriodoSelecionado(parametros, chamadoVO);
		
		Collection<Chamado> dados = new ArrayList<Chamado>();
		
		dados.add(listaChamados.get(0));
		
		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_ANALITICO_CHAMADO, formatoImpressao);
	}

	private void criarParametroPeriodoSelecionado(Map<String, Object> parametros, ChamadoVO chamadoVO) {
		String dataInicial = "";
		String dataFim = "";
		
		if(!StringUtils.isEmpty(chamadoVO.getDataInicioCriacao()) && !StringUtils.isEmpty(chamadoVO.getDataFimCriacao())) {
			dataInicial =  chamadoVO.getDataInicioCriacao();
			dataFim = chamadoVO.getDataFimCriacao();
		} else if(!StringUtils.isEmpty(chamadoVO.getDataInicioResolucao()) && !StringUtils.isEmpty(chamadoVO.getDataFimResolucao())) {
			dataInicial =  chamadoVO.getDataInicioResolucao();
			dataFim = chamadoVO.getDataFimResolucao();
		} else if(!StringUtils.isEmpty(chamadoVO.getDataInicioEncerramento()) && !StringUtils.isEmpty(chamadoVO.getDataFimEncerramento())) {
			dataInicial =  chamadoVO.getDataInicioEncerramento();
			dataFim = chamadoVO.getDataFimEncerramento();
		}
		
		parametros.put("dataInicial", dataInicial);
		parametros.put("dataFinal", dataFim);
		
	}

	@Override
	public byte[] gerarRelatorioChamadoResumo(ChamadoVO chamadoVO, FormatoImpressao formatoImpressao,
			String exibirFiltros) throws GGASException {
		
		
		List<ChamadoResumoVO> listaChamados = (List<ChamadoResumoVO>) controladorChamado.consultarChamadoResumo(chamadoVO, Boolean.TRUE, Boolean.FALSE);
		
		List<ChamadoResumoVO> listaTopCincoChamados = (List<ChamadoResumoVO>) controladorChamado.consultarChamadoResumo(chamadoVO, Boolean.TRUE, Boolean.TRUE);
		
		
		if (listaChamados.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO, true);
		}
		
		ControladorEmpresa controladorEmpresa =
				(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		
		
		Integer totalChamados = listaChamados != null && !listaChamados.isEmpty()? listaChamados.stream()
				.mapToInt(ChamadoResumoVO::getTotal)
				.sum() : null;
		
		parametros.put("listagemResumoChamados", listaChamados);
		parametros.put("listaTopCincoChamados", listaTopCincoChamados);
		parametros.put("totalChamados",totalChamados);
		parametros.put("filtros", chamadoVO);

		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		
		parametros.put("imagem",
				Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
		
		criarParametroPeriodoSelecionado(parametros, chamadoVO);
		
		Collection<ChamadoResumoVO> dados = new ArrayList<ChamadoResumoVO>();
		
		dados.add(listaChamados.get(0));
		
		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_RESUMO_CHAMADO, formatoImpressao);
	}

}
