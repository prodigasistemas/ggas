/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 28/08/2014 17:13:03
 @author rfilho
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoModalidade;
import br.com.ggas.contrato.contrato.ContratoPenalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.apuracaopenalidade.ApuracaoQuantidadePenalidadePeriodicidade;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.relatorio.ControladorRelatorioCobrancaRecuperacaoPenalidades;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.cobrancarecuperacaopenalidades.RelatorioCobrancaRecuperacaoPenalidadesVO;
import br.com.ggas.web.relatorio.cobrancarecuperacaopenalidades.SubRelatorioCobrancaRecuperacaoPenalidadesPeriodicidadeVO;
import br.com.ggas.web.relatorio.cobrancarecuperacaopenalidades.SubRelatorioCobrancaRecuperacaoPenalidadesTopVO;
import br.com.ggas.web.relatorio.cobrancarecuperacaopenalidades.SubRelatorioCobrancaRecuperacaoPenalidadesVO;

public class ControladorRelatorioCobrancaRecuperacaoPenalidadesImpl extends ControladorNegocioImpl 
			implements ControladorRelatorioCobrancaRecuperacaoPenalidades {

	private static final String RELATORIO_COBRANCA_RECUPERACAO_PENALIDADES = "relatorioCobrancaRecuperacaoPenalidades.jasper";

	private static final String MENSAL = "Mensal";

	private static final String TRIMESTRAL = "Trimestral";

	private static final String ANUAL = "Anual";

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.relatorio.ControladorRelatorioCobrancaRecuperacaoPenalidades#gerarRelatorioCobrancaRecuperacaoPenalidades(java.util.Map)
	 */
	@Override
	public byte[] gerarRelatorioCobrancaRecuperacaoPenalidades(Map<String, Object> filtro) throws GGASException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

		ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);

		Map<String, Object> parametros = new HashMap<String, Object>();
		if (controladorEmpresa.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem",
							Constantes.URL_LOGOMARCA_EMPRESA
											+ String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		}

		FormatoImpressao formatoImpressao = null;

		formatoImpressao = (FormatoImpressao) filtro.get("formatoImpressao");
		this.montarParametroRelatorio(parametros, filtro);

		Long idCliente = (Long) filtro.get("idCliente");

		Long idImovel = (Long) filtro.get("idImovel");

		Date dataInicialApuracao = (Date) filtro.get("dataEmissaoInicial");
		Date dataFinalApuracao = (Date) filtro.get("dataEmissaoFinal");

		String tipoExibicao = (String) filtro.get("tipoExibicao");

		Boolean exibirFiltros = (Boolean) filtro.get("exibirFiltros");
		parametros.put("exibirFiltros", exibirFiltros);

		parametros.put("tipoExibicao", tipoExibicao);
		Collection<RelatorioCobrancaRecuperacaoPenalidadesVO> dados = null;

		Collection<PontoConsumo> listaPontosConsumo = null;

		Cliente cliente = null;
		if (idCliente != null) {
			cliente = controladorCliente.obterCliente(idCliente, new String[] {"enderecos"});
			parametros.put("nomeCliente", cliente.getNome());
			listaPontosConsumo = controladorPontoConsumo.listarPontoConsumoPorCliente(idCliente);
		}
		Imovel imov = null;
		if (idImovel != null) {
			imov = (Imovel) controladorImovel.obter(idImovel, ImovelImpl.class);
			parametros.put("nomeImovel", imov.getNome());
			listaPontosConsumo = removerPontoConsumoRepedido(controladorPontoConsumo.listarPontoConsumoComContratoPorImovel(idImovel));
		}

		if (listaPontosConsumo == null) {

			Map<String, Object> param = new HashMap<String, Object>();
			param.put("habilitado", Boolean.TRUE);
			listaPontosConsumo = controladorPontoConsumo.listarPontoConsumoContratoAtivo();
		}
		dados = popularDadosRelatorio(listaPontosConsumo, dataInicialApuracao, dataFinalApuracao, parametros);
		if (dados == null || dados.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NAO_EXISTEM_DADOS_GERACAO_RELATORIO);
		}

		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_COBRANCA_RECUPERACAO_PENALIDADES, formatoImpressao);
	}

	/**
	 * Montar parametro relatorio.
	 * 
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void montarParametroRelatorio(Map<String, Object> parametros, Map<String, Object> filtro) throws NegocioException {

		if (filtro != null && !filtro.isEmpty()) {
			Date dataInicio = (Date) filtro.get("dataEmissaoInicial");
			Date dataFinal = (Date) filtro.get("dataEmissaoFinal");

			if (dataInicio != null) {
				parametros.put("dataInicial", DataUtil.converterDataParaString(dataInicio));

			} else {
				throw new NegocioException(Constantes.ERRO_INFORME_DATA_INICIAL, true);
			}

			if (dataFinal != null) {
				parametros.put("dataFinal", DataUtil.converterDataParaString(dataFinal));
			} else {
				throw new NegocioException(Constantes.ERRO_INFORME_DATA_FINAL, true);
			}

			if (Util.compararDatas(dataInicio, dataFinal) > 0) {
				throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_DATA_INICIAL_MAIOR_TARIFA_CAD, dataFinal);
			}

		}

	}

	/**
	 * Popular dados relatorio.
	 * 
	 * @param listaPontosConsumo
	 *            the lista pontos consumo
	 * @param dataInicialApuracao
	 *            the data inicial apuracao
	 * @param dataFinalApuracao
	 *            the data final apuracao
	 * @param parametros
	 *            the parametros
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<RelatorioCobrancaRecuperacaoPenalidadesVO> popularDadosRelatorio(Collection<PontoConsumo> listaPontosConsumo,
					Date dataInicialApuracao, Date dataFinalApuracao, Map<String, Object> parametros) throws GGASException {

		return popularRelatorio(listaPontosConsumo, dataInicialApuracao, dataFinalApuracao, parametros);
	}

	/**
	 * Popular relatorio.
	 * 
	 * @param listaPontosConsumo
	 *            the lista pontos consumo
	 * @param dataInicialApuracao
	 *            the data inicial apuracao
	 * @param dataFinalApuracao
	 *            the data final apuracao
	 * @param parametros
	 *            the parametros
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<RelatorioCobrancaRecuperacaoPenalidadesVO> popularRelatorio(Collection<PontoConsumo> listaPontosConsumo,
					Date dataInicialApuracao, Date dataFinalApuracao, Map<String, Object> parametros) throws GGASException {

		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		ServiceLocator.getInstancia().getControladorNegocio(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

		ControladorApuracaoPenalidade controladorApuracaoPenalidade = (ControladorApuracaoPenalidade) ServiceLocator.getInstancia()
						.getBeanPorID(ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Long itemFaturaGas = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));

		Long situacaoPagamentoPaga = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO));

		String valorPeriodicidadeMensal = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_PERIODICIDADE_PENALIDADE_MENSAL);
		String valorPeriodicidadeTrimestral = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_PERIODICIDADE_PENALIDADE_TRIMESTRAL);
		String valorPeriodicidadeAnual = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_PERIODICIDADE_PENALIDADE_ANUAL);

		Collection<RelatorioCobrancaRecuperacaoPenalidadesVO> lista = new ArrayList<RelatorioCobrancaRecuperacaoPenalidadesVO>();

		String tipoExibicao = (String) parametros.get("tipoExibicao");

		// TODO: Se não tiver pontos selecionados... listar os ApuracaoQuantidadePenalidadePeriodicidade de acordo com o periodo passado
		if (listaPontosConsumo != null && !listaPontosConsumo.isEmpty()) {
			for (PontoConsumo pontoConsumo : listaPontosConsumo) {

				RelatorioCobrancaRecuperacaoPenalidadesVO cobracaRecuperacaoPenalidade = new RelatorioCobrancaRecuperacaoPenalidadesVO();

				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put("idPontoConsumo", pontoConsumo.getChavePrimaria());
				filtro.put("dataApuracaoInicial", dataInicialApuracao);
				filtro.put("dataApuracaoFinal", dataFinalApuracao);
				filtro.put("ordenarDataApuracao", Boolean.TRUE);
				Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoPenalidadePeri = controladorApuracaoPenalidade
								.listarApuracaoQuantidadePenalidadePeriodicidades(filtro);
				if (listaApuracaoPenalidadePeri != null && !listaApuracaoPenalidadePeri.isEmpty()) {
					Cliente cliente = listaApuracaoPenalidadePeri.iterator().next().getContratoAtual().getClienteAssinatura();
					if (cliente != null) {
						StringBuilder nomeClientePontoConsumo = new StringBuilder();

						String nomeCliente = cliente.getNome();
						String descricaoPontoConsumo = pontoConsumo.getDescricao();
						String separador = " - ";
						if (nomeCliente != null) {
							nomeClientePontoConsumo.append(nomeCliente);
							nomeClientePontoConsumo.append(separador);
						}

						if (descricaoPontoConsumo != null) {
							nomeClientePontoConsumo.append(descricaoPontoConsumo);
						}

						BigDecimal saldoRecuperadoVolumeTotal = BigDecimal.ZERO;

						// Saldo Recuperavel anterior
						BigDecimal saldoVolumeRecuperavel = controladorApuracaoPenalidade.obterSaldoRecuperavel(dataInicialApuracao,
										pontoConsumo.getChavePrimaria());

						// procurar a primeira fatura emitida maior ou igual a data inicial de apuração

						Collection<Fatura> listaFatura = controladorFatura.listarFaturasPorPontoConsumo(pontoConsumo.getChavePrimaria());
						Date dataInicio = dataInicialApuracao;
						if (listaFatura != null && !listaFatura.isEmpty()) {
							for (Fatura fatura : listaFatura) {
								if (fatura.getDataEmissao().after(dataInicio)) {
									dataInicio = fatura.getDataEmissao();
									break;
								}

							}
						}

						BigDecimal tarifaMediaGasSaldo = controladorApuracaoPenalidade.obterTarifaMedia(pontoConsumo, itemFaturaGas,
										dataInicio, saldoVolumeRecuperavel, true, true);

						cobracaRecuperacaoPenalidade.setSaldoVolumeRecuperavelAnterior(saldoVolumeRecuperavel);
						cobracaRecuperacaoPenalidade.setSaldoValorRecuperavelAnterior(saldoVolumeRecuperavel.multiply(tarifaMediaGasSaldo));
						cobracaRecuperacaoPenalidade.setNomeClientePontoConsumo(nomeClientePontoConsumo.toString());
						cobracaRecuperacaoPenalidade.setSegmento(pontoConsumo.getSegmento().getDescricao());

						if (!listaApuracaoPenalidadePeri.isEmpty()) {

							SubRelatorioCobrancaRecuperacaoPenalidadesVO subAux = new SubRelatorioCobrancaRecuperacaoPenalidadesVO();

							for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : listaApuracaoPenalidadePeri) {

								SubRelatorioCobrancaRecuperacaoPenalidadesVO sub = popularRelatorioTop(
										controladorContrato, controladorApuracaoPenalidade, itemFaturaGas,
										situacaoPagamentoPaga, pontoConsumo, saldoRecuperadoVolumeTotal,
										saldoVolumeRecuperavel, apuracaoQuantidadePenalidadePeriodicidade);

								Map<String, Date> datas = Util.obterDataInicialDataFimPeriodo(Util.obterAnoMes(sub.getDataInicio()));
								if (sub.getDataInicio().compareTo(datas.get("dataInicial")) >= 0
												&& datas.get("dataFinal").compareTo(sub.getDataInicio()) >= 0) {

									subAux = sub;
									subAux.setDataInicio(datas.get("dataInicial"));
									subAux.setDataFim(datas.get("dataFinal"));
									if (subAux.getVolumeCobrado() != null && sub.getVolumeCobrado() != null) {
										subAux.setVolumeCobrado(subAux.getVolumeCobrado().add(sub.getVolumeCobrado()));
									}
									if (subAux.getValorCobrado() != null && sub.getValorCobrado() != null) {
										subAux.setValorCobrado(subAux.getValorCobrado().add(sub.getValorCobrado()));
									}
									if (subAux.getValorRecuperavel() != null && sub.getValorRecuperavel() != null) {
										subAux.setValorRecuperavel(subAux.getValorRecuperavel().add(sub.getValorRecuperavel()));
									}
									if (subAux.getValorRecuperado() != null && sub.getValorRecuperado() != null) {
										subAux.setValorRecuperado(subAux.getValorRecuperado().add(sub.getValorRecuperado()));
									}
									if (subAux.getVolumeRecuperado() != null && sub.getVolumeRecuperado() != null) {
										subAux.setVolumeRecuperado(subAux.getVolumeRecuperado().add(sub.getVolumeRecuperado()));
									}
									if (subAux.getVolumeRecuperavel() != null && sub.getVolumeRecuperavel() != null) {
										subAux.setVolumeRecuperavel(subAux.getVolumeRecuperavel().add(sub.getVolumeRecuperavel()));
									}
									if (subAux.getSaldoARecuperar() != null && sub.getSaldoARecuperar() != null) {
										subAux.setSaldoARecuperar(subAux.getSaldoARecuperar().add(sub.getSaldoARecuperar()));
									}
									if (subAux.getSaldoRecuperavel() != null && sub.getSaldoRecuperavel() != null) {
										subAux.setSaldoRecuperavel(subAux.getSaldoRecuperavel().add(sub.getSaldoRecuperavel()));
									}

								} else {
									cobracaRecuperacaoPenalidade.getColecaoVOs().add(subAux);
									subAux = new SubRelatorioCobrancaRecuperacaoPenalidadesVO();
								}

							}

							cobracaRecuperacaoPenalidade.getColecaoVOs().add(subAux);

							if ("top".equals(tipoExibicao)) {
								// Agrupar ApuracaoQuantidadePenalidadePeriodicidade por periodicidade

								SubRelatorioCobrancaRecuperacaoPenalidadesPeriodicidadeVO detalhamentoPeriodicidade = 
												new SubRelatorioCobrancaRecuperacaoPenalidadesPeriodicidadeVO();
								Map<String, Collection<ApuracaoQuantidadePenalidadePeriodicidade>> mapa = agruparApuracaoPorPeriodicidadePenalidade(
												valorPeriodicidadeMensal, valorPeriodicidadeTrimestral, valorPeriodicidadeAnual,
												listaApuracaoPenalidadePeri);

								if (mapa != null) {
									Collection<ApuracaoQuantidadePenalidadePeriodicidade> aqppListaMensal = mapa.get(MENSAL);
									if (aqppListaMensal != null && !aqppListaMensal.isEmpty()) {

										detalhamentoPeriodicidade.setPeriodicidade(MENSAL);
										String tipoImovel = obterTipoImovel(aqppListaMensal);
										detalhamentoPeriodicidade.setTipoImovel(tipoImovel);
										for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : aqppListaMensal) {
											SubRelatorioCobrancaRecuperacaoPenalidadesTopVO subTop = new SubRelatorioCobrancaRecuperacaoPenalidadesTopVO();

											popularSubRelatorioTopPcm(apuracaoQuantidadePenalidadePeriodicidade, subTop,
															controladorConstanteSistema, filtro);
											detalhamentoPeriodicidade.getColecaoTopVOs().add(subTop);
										}
										cobracaRecuperacaoPenalidade.getColecaoPeriodicidadeVOs().add(detalhamentoPeriodicidade);
									}
									Collection<ApuracaoQuantidadePenalidadePeriodicidade> aqppListaTrimestral = mapa.get(TRIMESTRAL);
									if (aqppListaTrimestral != null && !aqppListaTrimestral.isEmpty()) {
										detalhamentoPeriodicidade.setPeriodicidade(TRIMESTRAL);
										for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : aqppListaTrimestral) {
											SubRelatorioCobrancaRecuperacaoPenalidadesTopVO subTop = new SubRelatorioCobrancaRecuperacaoPenalidadesTopVO();

											popularSubRelatorioTopPcm(apuracaoQuantidadePenalidadePeriodicidade, subTop,
															controladorConstanteSistema, filtro);
											detalhamentoPeriodicidade.getColecaoTopVOs().add(subTop);
										}
										cobracaRecuperacaoPenalidade.getColecaoPeriodicidadeVOs().add(detalhamentoPeriodicidade);
									}
									Collection<ApuracaoQuantidadePenalidadePeriodicidade> aqppListaAnual = mapa.get(ANUAL);
									if (aqppListaAnual != null && !aqppListaAnual.isEmpty()) {
										detalhamentoPeriodicidade.setPeriodicidade(ANUAL);
										for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : aqppListaAnual) {
											SubRelatorioCobrancaRecuperacaoPenalidadesTopVO subTop = new SubRelatorioCobrancaRecuperacaoPenalidadesTopVO();

											popularSubRelatorioTopPcm(apuracaoQuantidadePenalidadePeriodicidade, subTop,
															controladorConstanteSistema, filtro);
											detalhamentoPeriodicidade.getColecaoTopVOs().add(subTop);
										}
										cobracaRecuperacaoPenalidade.getColecaoPeriodicidadeVOs().add(detalhamentoPeriodicidade);
									}
								}
							}

							totalizarSaldoPorCliente(cobracaRecuperacaoPenalidade);
							lista.add(cobracaRecuperacaoPenalidade);
						}
					}
				}
			}
		}
		return lista;

	}

	/**
	 * Obter tipo imovel.
	 * 
	 * @param aqppListaMensal
	 *            the aqpp lista mensal
	 * @return the string
	 */
	private String obterTipoImovel(Collection<ApuracaoQuantidadePenalidadePeriodicidade> aqppListaMensal) {

		String tipoImovel = null;
		for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : aqppListaMensal) {
			Imovel imovel = apuracaoQuantidadePenalidadePeriodicidade.getPontoConsumo().getImovel();

			ModalidadeMedicaoImovel modalidade = imovel.getModalidadeMedicaoImovel();
			if (modalidade != null) {
				tipoImovel = modalidade.getDescricao();
				break;
			} else {
				tipoImovel = "Individual";
			}

		}

		return tipoImovel;
	}

	/**
	 * Totalizar saldo por cliente.
	 * 
	 * @param cobracaRecuperacaoPenalidade
	 *            the cobraca recuperacao penalidade
	 */
	private void totalizarSaldoPorCliente(RelatorioCobrancaRecuperacaoPenalidadesVO cobracaRecuperacaoPenalidade) {

		BigDecimal saldoTotalVolumeTopPercenCobrancaCliente = BigDecimal.ZERO;
		BigDecimal saldoPenalidadeTop = BigDecimal.ZERO;
		BigDecimal saldoVolAMaior = BigDecimal.ZERO;
		BigDecimal saldoTotalMaiorPercenCobranca = BigDecimal.ZERO;
		BigDecimal saldoPenalidadeAMaior = BigDecimal.ZERO;
		for (SubRelatorioCobrancaRecuperacaoPenalidadesPeriodicidadeVO sub : cobracaRecuperacaoPenalidade.getColecaoPeriodicidadeVOs()) {
			for (SubRelatorioCobrancaRecuperacaoPenalidadesTopVO subDetalhado : sub.getColecaoTopVOs()) {

				BigDecimal volTopPercentualCobranca; 
				if(subDetalhado.getVolTopPercentualCobranca() != null){
					volTopPercentualCobranca = subDetalhado.getVolTopPercentualCobranca();
				}else{
					volTopPercentualCobranca = BigDecimal.ZERO;
				}
				BigDecimal penalidadeTop;
				if(subDetalhado.getPenalidadeTop() != null){
					penalidadeTop = subDetalhado.getPenalidadeTop();
				}else{
					penalidadeTop = BigDecimal.ZERO;
				}

				BigDecimal volAMaior; 
				if(subDetalhado.getVolMaior() != null){
					volAMaior = subDetalhado.getVolMaior();
				}else{
					volAMaior = BigDecimal.ZERO;
				}

				BigDecimal maiorPercenCobranca; 
				if(subDetalhado.getVolMaiorPercentualCobranca() != null){
					maiorPercenCobranca = subDetalhado.getVolMaiorPercentualCobranca();
				}else{
					maiorPercenCobranca = BigDecimal.ZERO;
				}

				BigDecimal penalidadeMaiorPcm; 
				if(subDetalhado.getPenalidadeMaiorPcm() != null){
					penalidadeMaiorPcm = subDetalhado.getPenalidadeMaiorPcm();
				}else{
					penalidadeMaiorPcm = BigDecimal.ZERO;
				}

				saldoTotalVolumeTopPercenCobrancaCliente = volTopPercentualCobranca.add(saldoTotalVolumeTopPercenCobrancaCliente);

				saldoPenalidadeTop = penalidadeTop.add(saldoPenalidadeTop);
				saldoVolAMaior = volAMaior.add(saldoVolAMaior);

				saldoTotalMaiorPercenCobranca = maiorPercenCobranca.add(saldoTotalMaiorPercenCobranca);
				saldoPenalidadeAMaior = penalidadeMaiorPcm.add(saldoPenalidadeAMaior);
			}

		}
		cobracaRecuperacaoPenalidade.setSaldoTotalVolumeTopPercenCobrancaCliente(saldoTotalVolumeTopPercenCobrancaCliente);
		cobracaRecuperacaoPenalidade.setSaldoPenalidadeTop(saldoPenalidadeTop);
		cobracaRecuperacaoPenalidade.setSaldoVolAMaior(saldoVolAMaior);
		cobracaRecuperacaoPenalidade.setSaldoTotalMaiorPercenCobranca(saldoTotalMaiorPercenCobranca);
		cobracaRecuperacaoPenalidade.setSaldoPenalidadeAMaior(saldoPenalidadeAMaior);
	}

	/**
	 * Popular sub relatorio top pcm.
	 * 
	 * @param apuracaoQuantidadePenalidadePeriodicidade
	 *            the apuracao quantidade penalidade periodicidade
	 * @param subTop
	 *            the sub top
	 * @param controladorConstanteSistema
	 *            the controlador constante sistema
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void popularSubRelatorioTopPcm(ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade,
					SubRelatorioCobrancaRecuperacaoPenalidadesTopVO subTop, ControladorConstanteSistema controladorConstanteSistema,
					Map<String, Object> filtro) throws NegocioException {

		ServiceLocator.getInstancia().getBeanPorID(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);

		ControladorApuracaoPenalidade controladorApuracaoPenalidade = (ControladorApuracaoPenalidade) ServiceLocator.getInstancia()
						.getBeanPorID(ControladorApuracaoPenalidade.BEAN_ID_CONTROLADOR_APURACAO_PENALIDADE);

		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		String parametroTakeOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
		String parametroVolumeAMaior = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);

		Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));
		Long situacaoPagamentoPaga = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO));

		String codPenalidadeRetiradaMaior = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);

		String codPenalidadeRetiradaMenor = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MENOR);

		PontoConsumo pontoConsumo = apuracaoQuantidadePenalidadePeriodicidade.getPontoConsumo();
		Contrato contrato = apuracaoQuantidadePenalidadePeriodicidade.getContratoAtual();
		ContratoModalidade contratoModalidade = apuracaoQuantidadePenalidadePeriodicidade.getContratoModalidade();
		BigDecimal qnr = apuracaoQuantidadePenalidadePeriodicidade.getQtdaNaoRetirada();

		filtro.get("dataApuracaoInicio");
		filtro.get("dataApuracaoFim");

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("idContrato", contrato.getChavePrimaria());
		// TODO:substituir o nome do parametro por penalidade
		params.put("idPenalidadeRetiradaMaiorMenor", apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria());
		params.put("dataInicioVigencia", apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao());
		params.put("dataFimVigencia", apuracaoQuantidadePenalidadePeriodicidade.getDataFimApuracao());

		BigDecimal valorPercentualRetMaiorMenor = BigDecimal.ZERO;
		BigDecimal compromissoRetiradaTop = BigDecimal.ZERO;

		ContratoPenalidade contratoPenalidadeRetiradaMaior = controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

		if (contratoPenalidadeRetiradaMaior != null) {
			valorPercentualRetMaiorMenor = contratoPenalidadeRetiradaMaior.getValorPercentualRetMaiorMenor();
			compromissoRetiradaTop = contratoPenalidadeRetiradaMaior.getPercentualMargemVariacao();
		}

		if (apuracaoQuantidadePenalidadePeriodicidade.getContratoModalidade() != null) {
			ContratoPontoConsumoModalidade cpcm = controladorContrato.obterContratoPontoConsumoModalidade(
							apuracaoQuantidadePenalidadePeriodicidade.getPontoConsumo().getChavePrimaria(),
							apuracaoQuantidadePenalidadePeriodicidade.getContratoAtual().getChavePrimaria(),
							apuracaoQuantidadePenalidadePeriodicidade.getContratoModalidade().getChavePrimaria());
			if (cpcm != null) {
				params.put("idContratoModalidade", cpcm.getChavePrimaria());
				ContratoPontoConsumoPenalidade contratoPontoConsumoPenalidade = controladorContrato
								.obterContratoPontoConsumoPenalidadeVigencia(params);
				if (contratoPontoConsumoPenalidade != null) {
					valorPercentualRetMaiorMenor = contratoPontoConsumoPenalidade.getValorPercentualRetMaiorMenor();
					compromissoRetiradaTop = contratoPontoConsumoPenalidade.getPercentualMargemVariacao();
				}
			}
		}

		params.put("idPenalidadeRetiradaMaiorMenor", Long.valueOf(codPenalidadeRetiradaMenor));

		controladorContrato.obterContratoPenalidadeVigenciaRetiradaMaiorMenor(params);

		// Variaveis Locais

		// Data de apuração
		subTop.setDataInicio(apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao());
		subTop.setDataFim(apuracaoQuantidadePenalidadePeriodicidade.getDataFimApuracao());

		// Obter diferenças de dias pra calcular a quantidade contratada no período

		// 2. ConsumoContratado - verifica a qdc do ponto de consumo do contrato e faz um somatorio dessa qdc de todas as modalidades do
		// periodo
		// obs:caso nao exista qdc do ponto de consumo do contrato o sistema deve fazer o somatorio da qdc do contrato

		// Soma os QDC's de ponto consumo contrato
		BigDecimal somatorioQDC = null;

		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

		somatorioQDC = obterQdcContratoPontoConsumoModalidade(contratoPontoConsumo.getListaContratoPontoConsumoModalidade(),
						apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao(),
						apuracaoQuantidadePenalidadePeriodicidade.getDataFimApuracao());
		if (somatorioQDC == null || somatorioQDC.compareTo(BigDecimal.ZERO) == 0) {
			somatorioQDC = controladorContrato.obterSomaQdcContratoPorPeriodo(contratoPontoConsumo.getContrato().getChavePrimaria(),
							apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao(),
							apuracaoQuantidadePenalidadePeriodicidade.getDataFimApuracao());
		}

		subTop.setConsumoContratado(somatorioQDC);

		// Consumo apurado naquele periodo

		Collection<Date> diasFornecimento = Util.obterDatasIntermediarias(
						apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao(),
						apuracaoQuantidadePenalidadePeriodicidade.getDataFimApuracao());

		BigDecimal consumoRealizado = BigDecimal.ZERO;
		for (Date data : diasFornecimento) {
			consumoRealizado = consumoRealizado.add(controladorApuracaoPenalidade.obterConsumoDiario(pontoConsumo, data));
		}

		subTop.setConsumoRealizado(consumoRealizado);

		if (contratoModalidade != null) {
			contratoModalidade.getChavePrimaria();
		}

		// Consumo Máximo
		if (valorPercentualRetMaiorMenor != null
						&& apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria() == Integer
										.parseInt(codPenalidadeRetiradaMaior)) {
			subTop.setConsumoMaximo(somatorioQDC.multiply(valorPercentualRetMaiorMenor.add(BigDecimal.ONE)));
			subTop.setConsumoMinimo(BigDecimal.ZERO);
		} else if (compromissoRetiradaTop != null
						&& apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria() == Long.parseLong(parametroTakeOrPay)) {

			subTop.setConsumoMinimo(somatorioQDC.multiply(compromissoRetiradaTop).add(BigDecimal.ONE));
			subTop.setConsumoMaximo(BigDecimal.ZERO);
		}


		calcularTop(subTop, apuracaoQuantidadePenalidadePeriodicidade);

		// Vol Take or Pay x Percentual de Cobrança m³
		BigDecimal volTopPercentCobranca = BigDecimal.ZERO;
		if (apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria() == Long.parseLong(parametroTakeOrPay)) {
			if (qnr.compareTo(BigDecimal.ZERO) <= 0) {
				qnr = BigDecimal.ZERO;
			}
			subTop.setVolTopPercentualCobranca(qnr);
		} else {
			subTop.setVolTopPercentualCobranca(volTopPercentCobranca);
		}

		// Tarifa Média de Cobrança

		if (qnr == null || qnr.compareTo(BigDecimal.ZERO) <= 0) {
			qnr = BigDecimal.ZERO;
		}

		subTop.setTarifaMedia(apuracaoQuantidadePenalidadePeriodicidade.getValorTarifaMedia());

		// Penalidade Top
		BigDecimal valorTarifa; 
		if(apuracaoQuantidadePenalidadePeriodicidade.getValorTarifaMedia() != null){
			valorTarifa = apuracaoQuantidadePenalidadePeriodicidade.getValorTarifaMedia();
		}else{
			valorTarifa = BigDecimal.ZERO;
		}
		subTop.setPenalidadeTop(qnr.multiply(valorTarifa));

		calcularVolMaior(subTop, apuracaoQuantidadePenalidadePeriodicidade);

		// Vol Maior x Percentual de cobrança
		BigDecimal volMaiorPercentCobranca = BigDecimal.ZERO;
		if (apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria() == Long.parseLong(parametroVolumeAMaior)) {
			subTop.setVolMaiorPercentualCobranca(apuracaoQuantidadePenalidadePeriodicidade.getValorCalculadoPercenCobranca());

		} else {
			subTop.setVolMaiorPercentualCobranca(volMaiorPercentCobranca);
		}

		// Penalidade a maior pcm
		subTop.setPenalidadeMaiorPcm(apuracaoQuantidadePenalidadePeriodicidade.getValorCobrado());

		Fatura fatura = apuracaoQuantidadePenalidadePeriodicidade.getFatura();

		if (fatura != null) {
			subTop.setCobrado("Sim");
			Date emissaoND = fatura.getDataEmissao();
			Date vencimentoND = fatura.getDataVencimento();

			subTop.setNotaDebito(String.valueOf(fatura.getChavePrimaria()));
			subTop.setEmissaoND(DataUtil.converterDataParaString(emissaoND));
			subTop.setVencimentoND(DataUtil.converterDataParaString(vencimentoND));

			if (fatura.getSituacaoPagamento().getChavePrimaria() == situacaoPagamentoPaga) {
				subTop.setQuitado("Sim");
			} else {
				subTop.setQuitado("Não");
			}
		} else {
			subTop.setCobrado("-");
			subTop.setQuitado("-");
			subTop.setNotaDebito("-");
			subTop.setEmissaoND("-");
			subTop.setVencimentoND("-");
		}

	}


	/**
	 * Calcular vol maior.
	 * 
	 * @param subTop
	 *            the sub top
	 * @param apuracaoQuantidadePenalidadePeriodicidade
	 *            the apuracao quantidade penalidade periodicidade
	 */
	private void calcularVolMaior(SubRelatorioCobrancaRecuperacaoPenalidadesTopVO subTop,
					ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade) {

		// Vol. A Maior = Consumo Realizado – Consumo Máximo

		if (apuracaoQuantidadePenalidadePeriodicidade.getQtdaDiariaRetiradaMaior() != null
						&& apuracaoQuantidadePenalidadePeriodicidade.getQtdaDiariaRetiradaMaior().compareTo(BigDecimal.ZERO) > 0) {
			subTop.setVolMaior(apuracaoQuantidadePenalidadePeriodicidade.getQtdaDiariaRetiradaMaior());
		} else {
			subTop.setVolMaior(BigDecimal.ZERO);
		}

	}

	/**
	 * Calcular top.
	 * 
	 * @param subTop
	 *            the sub top
	 * @param apuracaoQuantidadePenalidadePeriodicidade
	 *            the apuracao quantidade penalidade periodicidade
	 */
	private void calcularTop(SubRelatorioCobrancaRecuperacaoPenalidadesTopVO subTop,
					ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade) {

		// Take or Pay = Consumo Mínimo – Consumo Realizado

		if (apuracaoQuantidadePenalidadePeriodicidade.getQtdaNaoRetirada() != null
						&& apuracaoQuantidadePenalidadePeriodicidade.getQtdaNaoRetirada().compareTo(BigDecimal.ZERO) > 0) {
			subTop.setTakeOrPay(apuracaoQuantidadePenalidadePeriodicidade.getQtdaNaoRetirada());
		} else {
			subTop.setTakeOrPay(BigDecimal.ZERO);
		}

	}

	/**
	 * Agrupar apuracao por periodicidade penalidade.
	 * 
	 * @param valorPeriodicidadeMensal
	 *            the valor periodicidade mensal
	 * @param valorPeriodicidadeTrimestral
	 *            the valor periodicidade trimestral
	 * @param valorPeriodicidadeAnual
	 *            the valor periodicidade anual
	 * @param listaApuracaoPenalidadePeri
	 *            the lista apuracao penalidade peri
	 * @return the map
	 */
	private Map<String, Collection<ApuracaoQuantidadePenalidadePeriodicidade>> agruparApuracaoPorPeriodicidadePenalidade(
					String valorPeriodicidadeMensal, String valorPeriodicidadeTrimestral, String valorPeriodicidadeAnual,
					Collection<ApuracaoQuantidadePenalidadePeriodicidade> listaApuracaoPenalidadePeri) {

		Map<String, Collection<ApuracaoQuantidadePenalidadePeriodicidade>> mapaPeriodidicadeApuracao = 
						new HashMap<String, Collection<ApuracaoQuantidadePenalidadePeriodicidade>>();
		for (ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade : listaApuracaoPenalidadePeri) {
			Long idPeriodicidadeMensal = Long.valueOf(valorPeriodicidadeMensal);
			Long idPeriodicidadeTrismestral = Long.valueOf(valorPeriodicidadeTrimestral);
			Long idPeriodicidadeAnual = Long.valueOf(valorPeriodicidadeAnual);

			if (apuracaoQuantidadePenalidadePeriodicidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeMensal) {
				if (mapaPeriodidicadeApuracao.containsKey(MENSAL)) {
					mapaPeriodidicadeApuracao.get(MENSAL).add(apuracaoQuantidadePenalidadePeriodicidade);
				} else {
					Collection<ApuracaoQuantidadePenalidadePeriodicidade> colecaoAqpp = new ArrayList<ApuracaoQuantidadePenalidadePeriodicidade>();
					colecaoAqpp.add(apuracaoQuantidadePenalidadePeriodicidade);
					mapaPeriodidicadeApuracao.put(MENSAL, colecaoAqpp);
				}
			}
			if (apuracaoQuantidadePenalidadePeriodicidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeTrismestral) {
				if (mapaPeriodidicadeApuracao.containsKey(TRIMESTRAL)) {
					mapaPeriodidicadeApuracao.get(TRIMESTRAL).add(apuracaoQuantidadePenalidadePeriodicidade);
				} else {
					Collection<ApuracaoQuantidadePenalidadePeriodicidade> colecaoAqpp = new ArrayList<ApuracaoQuantidadePenalidadePeriodicidade>();
					colecaoAqpp.add(apuracaoQuantidadePenalidadePeriodicidade);
					mapaPeriodidicadeApuracao.put(TRIMESTRAL, colecaoAqpp);
				}
			}

			if (apuracaoQuantidadePenalidadePeriodicidade.getPeriodicidadePenalidade().getChavePrimaria() == idPeriodicidadeAnual) {
				if (mapaPeriodidicadeApuracao.containsKey(ANUAL)) {
					mapaPeriodidicadeApuracao.get(ANUAL).add(apuracaoQuantidadePenalidadePeriodicidade);
				} else {
					Collection<ApuracaoQuantidadePenalidadePeriodicidade> colecaoAqpp = new ArrayList<ApuracaoQuantidadePenalidadePeriodicidade>();
					colecaoAqpp.add(apuracaoQuantidadePenalidadePeriodicidade);
					mapaPeriodidicadeApuracao.put(ANUAL, colecaoAqpp);
				}
			}

		}
		return mapaPeriodidicadeApuracao;
	}

	/**
	 * Popular relatorio top.
	 * 
	 * @param controladorContrato
	 *            the controlador contrato
	 * @param controladorApuracaoPenalidade
	 *            the controlador apuracao penalidade
	 * @param itemFaturaGas
	 *            the item fatura gas
	 * @param situacaoPagamentoPaga
	 *            the situacao pagamento paga
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param saldoRecuperadoVolumeTotal
	 *            the saldo recuperado volume total
	 * @param saldoRecuperadoValorTotal
	 *            the saldo recuperado valor total
	 * @param saldoVolumeRecuperavel
	 *            the saldo volume recuperavel
	 * @param apuracaoQuantidadePenalidadePeriodicidade
	 *            the apuracao quantidade penalidade periodicidade
	 * @return the sub relatorio cobranca recuperacao penalidades vo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private SubRelatorioCobrancaRecuperacaoPenalidadesVO popularRelatorioTop(ControladorContrato controladorContrato,
			ControladorApuracaoPenalidade controladorApuracaoPenalidade, Long itemFaturaGas, Long situacaoPagamentoPaga,
			PontoConsumo pontoConsumo, BigDecimal saldoRecuperadoVolumeTotal, BigDecimal saldoVolumeRecuperavel,
			ApuracaoQuantidadePenalidadePeriodicidade apuracaoQuantidadePenalidadePeriodicidade)
			throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String parametroTakeOrPay = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);
		String parametroVolumeAMaior = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_RETIRADA_A_MAIOR);

		Contrato contrato = apuracaoQuantidadePenalidadePeriodicidade.getContratoAtual();
		BigDecimal qpnr = apuracaoQuantidadePenalidadePeriodicidade.getQtdaPagaNaoRetirada();
		BigDecimal qnr = apuracaoQuantidadePenalidadePeriodicidade.getQtdaNaoRetirada();
		BigDecimal volumeRecuperavel = BigDecimal.ZERO;

		// Se na apuração não possuir o contrato modalidade, passa nulo na consulta
		ContratoModalidade contratoModalidade = apuracaoQuantidadePenalidadePeriodicidade.getContratoModalidade();
		Long idContratoModalidade = null;
		if (contratoModalidade != null) {
			idContratoModalidade = contratoModalidade.getChavePrimaria();
		}

		ContratoPontoConsumoModalidade cpcm = controladorContrato.obterContratoPontoConsumoModalidade(pontoConsumo.getChavePrimaria(),
						contrato.getChavePrimaria(), idContratoModalidade);

		BigDecimal pctMaximo = BigDecimal.ONE;
		BigDecimal pctMinimo = BigDecimal.ZERO;
		volumeRecuperavel = qpnr;

		if (cpcm != null) {

			if (cpcm.getPercentualQDCContratoQPNR() != null && cpcm.getPercentualMinimoQDCContratoQPNR() != null) {
				pctMinimo = cpcm.getPercentualMinimoQDCContratoQPNR();
				if (contrato.getDataVencimentoObrigacoes() != null
								&& Util.getDataCorrente(false).compareTo(contrato.getDataVencimentoObrigacoes()) > 0
								&& cpcm.getPercentualQDCFimContratoQPNR() != null) {
					pctMaximo = cpcm.getPercentualQDCFimContratoQPNR();
				} else {
					pctMaximo = cpcm.getPercentualQDCContratoQPNR();
				}
			}

			Boolean habilitaSaldo = Boolean.TRUE;
			Date dataCorrente = Util.getDataCorrente(false);
			if (cpcm.getDataInicioRetiradaQPNR() != null && cpcm.getDataFimRetiradaQPNR() != null) {
				if (cpcm.getDataInicioRetiradaQPNR().compareTo(dataCorrente) >= 0
								&& dataCorrente.compareTo(cpcm.getDataFimRetiradaQPNR()) >= 0) {
					habilitaSaldo = Boolean.FALSE;
				}
			} else if (cpcm.getDataInicioRetiradaQPNR() != null) {
				if (cpcm.getDataInicioRetiradaQPNR().compareTo(dataCorrente) >= 0) {
					habilitaSaldo = Boolean.FALSE;
				}
			} else if (cpcm.getDataFimRetiradaQPNR() != null && dataCorrente.compareTo(cpcm.getDataFimRetiradaQPNR()) >= 0) {
				habilitaSaldo = Boolean.FALSE;
			}

			if (habilitaSaldo) {

				Date dataApuracaoAux = null;
				if (cpcm.getAnosValidadeRetiradaQPNR() != null) {
					DateTime dataAux = new DateTime(apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao());
					dataAux = dataAux.minusYears(cpcm.getAnosValidadeRetiradaQPNR());
					dataApuracaoAux = dataAux.toDate();
				}

				if (dataApuracaoAux != null
								&& apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao().compareTo(dataApuracaoAux) >= 0) {

					BigDecimal recuperadoAQPP = apuracaoQuantidadePenalidadePeriodicidade.getQtdaPagaNaoRetirada();
					if (apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada() != null) {
						recuperadoAQPP = recuperadoAQPP.subtract(apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada());
					}

					BigDecimal volMaximo = BigDecimal.ZERO;
					BigDecimal volApurado = BigDecimal.ZERO;
					BigDecimal refencia = BigDecimal.ZERO;

					Integer qtdDias = Util.intervaloDatas(apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao(),
									apuracaoQuantidadePenalidadePeriodicidade.getDataFimApuracao());
					volMaximo = apuracaoQuantidadePenalidadePeriodicidade.getQtdaDiariaContratadaMedia()
									.multiply(pctMaximo.subtract(pctMinimo)).multiply(new BigDecimal(qtdDias));
					volApurado = apuracaoQuantidadePenalidadePeriodicidade.getQtdaDiariaRetirada()
									.subtract(pctMinimo.multiply(apuracaoQuantidadePenalidadePeriodicidade.getQtdaDiariaContratadaMedia()))
									.multiply(new BigDecimal(qtdDias));

					if (volMaximo.compareTo(volApurado) < 0) {
						refencia = volMaximo;
					} else {
						refencia = volApurado;
					}

					if (recuperadoAQPP.compareTo(refencia) > 0) {
						recuperadoAQPP = refencia;
					}

					volumeRecuperavel = recuperadoAQPP;

				}

			}

		}

		SubRelatorioCobrancaRecuperacaoPenalidadesVO sub = new SubRelatorioCobrancaRecuperacaoPenalidadesVO();

		sub.setDataInicio(apuracaoQuantidadePenalidadePeriodicidade.getDataInicioApuracao());
		sub.setDataFim(apuracaoQuantidadePenalidadePeriodicidade.getDataFimApuracao());

		BigDecimal volumeCobrado = BigDecimal.ZERO;

		if (apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria() == Long.parseLong(parametroTakeOrPay)) {
			volumeCobrado = apuracaoQuantidadePenalidadePeriodicidade.getQtdaNaoRetirada();
		} else if (apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getChavePrimaria() == Long.parseLong(parametroVolumeAMaior)) {
			volumeCobrado = apuracaoQuantidadePenalidadePeriodicidade.getQtdaDiariaRetiradaMaior();
		}

		sub.setVolumeCobrado(volumeCobrado);

		if(apuracaoQuantidadePenalidadePeriodicidade.getValorCobrado() != null){
			sub.setValorCobrado(apuracaoQuantidadePenalidadePeriodicidade.getValorCobrado());
		}else{
			sub.setValorCobrado(BigDecimal.ZERO);
		}

		if (qnr == null || qnr.compareTo(BigDecimal.ZERO) < 0) {
			qnr = BigDecimal.ZERO;
		}

		BigDecimal tarifaMediaGas = controladorApuracaoPenalidade.obterTarifaMedia(
						apuracaoQuantidadePenalidadePeriodicidade.getPontoConsumo(), itemFaturaGas,
						apuracaoQuantidadePenalidadePeriodicidade.getDataFimApuracao(), qnr, true, true);

		if (qpnr != null && volumeRecuperavel != null) {
			sub.setVolumeRecuperavel(volumeRecuperavel.setScale(2, RoundingMode.HALF_DOWN));
			sub.setValorRecuperavel(volumeRecuperavel.multiply(tarifaMediaGas).setScale(2, RoundingMode.HALF_DOWN));
		} else {
			sub.setVolumeRecuperavel(BigDecimal.ZERO);
			sub.setValorRecuperavel(BigDecimal.ZERO);
		}

		// Total recuperado m³

		if (apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada() != null) {
			// saldoRecuperadoVolumeTotal não é utilizada no método nem retornada
			// se variavel for util, deve ser calculada em método a parte
			saldoRecuperadoVolumeTotal = saldoRecuperadoVolumeTotal.add(apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada()); // NOSONAR
			sub.setVolumeRecuperado(apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada());
		} else {
			sub.setVolumeRecuperado(BigDecimal.ZERO);
		}

		if (apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada() != null) {
			BigDecimal valorRecuperado = apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada().multiply(tarifaMediaGas);
			sub.setValorRecuperado(valorRecuperado);
		} else {
			sub.setValorRecuperado(BigDecimal.ZERO);
		}

		sub.setSaldoARecuperar(BigDecimal.ZERO);

		BigDecimal saldo = BigDecimal.ZERO;
		if (apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada() != null) {
			saldo = saldoVolumeRecuperavel.subtract(apuracaoQuantidadePenalidadePeriodicidade.getQtdaRecuperada());

			sub.setSaldoRecuperavel(saldo.add(sub.getValorRecuperavel()));
		} else {
			sub.setSaldoRecuperavel(BigDecimal.ZERO);
			sub.setSaldoARecuperar(BigDecimal.ZERO);
		}

		if(apuracaoQuantidadePenalidadePeriodicidade.getPenalidade() != null){
			sub.setHistoricoCobrancaRecuperacao( apuracaoQuantidadePenalidadePeriodicidade.getPenalidade().getDescricao());
		}else{
			sub.setHistoricoCobrancaRecuperacao("");
		}

		Fatura fatura = apuracaoQuantidadePenalidadePeriodicidade.getFatura();

		if (fatura != null) {

			Date dataEmissao = apuracaoQuantidadePenalidadePeriodicidade.getFatura().getDataEmissao();

			Date dataVencimento = apuracaoQuantidadePenalidadePeriodicidade.getFatura().getDataVencimento();

			sub.setDataEmissao(DataUtil.converterDataParaString(dataEmissao));
			sub.setDataVencimento(DataUtil.converterDataParaString(dataVencimento));

			sub.setNumeroDocumento(String.valueOf(apuracaoQuantidadePenalidadePeriodicidade.getFatura().getChavePrimaria()));

			if (fatura.getSituacaoPagamento().getChavePrimaria() == situacaoPagamentoPaga) {
				sub.setQuitado("Sim");
			} else {
				sub.setQuitado("Não");
			}

		} else {
			sub.setQuitado("-");
			sub.setDataEmissao("-");
			sub.setDataVencimento("-");
			sub.setNumeroDocumento("-");
		}
		return sub;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/**
	 * Remover ponto consumo repedido.
	 * 
	 * @param listaPontoConsumo
	 *            the lista ponto consumo
	 * @return the collection
	 */
	private Collection<PontoConsumo> removerPontoConsumoRepedido(Collection<PontoConsumo> listaPontoConsumo) {

		Collection<PontoConsumo> listaPontoConsumoAux = new ArrayList<PontoConsumo>();

		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			if (!listaPontoConsumoAux.contains(pontoConsumo)) {
				listaPontoConsumoAux.add(pontoConsumo);
			}
		}
		return listaPontoConsumoAux;
	}

	/**
	 * Obter qdc contrato ponto consumo modalidade.
	 * 
	 * @param listaContratoPontoConsumoModalidade
	 *            the lista contrato ponto consumo modalidade
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal obterQdcContratoPontoConsumoModalidade(
					Collection<ContratoPontoConsumoModalidade> listaContratoPontoConsumoModalidade, Date dataInicial, Date dataFinal)
					throws NegocioException {

		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

		BigDecimal qdc = null;

		if (!listaContratoPontoConsumoModalidade.isEmpty()) {
			qdc = controladorContrato.consultarContratoPontoConsumoModalidadeQDCPorModalidade(listaContratoPontoConsumoModalidade,
							dataInicial, dataFinal);
		}

		return qdc;
	}

}
