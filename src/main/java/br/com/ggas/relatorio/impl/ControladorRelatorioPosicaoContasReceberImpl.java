/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 05/12/2013 16:07:22
 @author ccavalcanti
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.DebitosACobrar;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.relatorio.ControladorRelatorioPosicaoContasReceber;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.posicaocontasreceber.RelatorioPosicaoContasReceberVO;
import br.com.ggas.web.relatorio.posicaocontasreceber.SubRelatorioPosicaoContasReceberVO;

public class ControladorRelatorioPosicaoContasReceberImpl extends ControladorNegocioImpl 
		implements ControladorRelatorioPosicaoContasReceber {

	private static final String RELATORIO_POSICAO_DO_CONTAS_A_RECEBER = "relatorioPosicaoContasReceber.jasper";

	private static final String ID_CLIENTE = "idCliente";

	private static final String DATA_VENCIMENTO_INICIAL = "dataVencimentoInicial";

	private static final String DATA_VENCIMENTO_FINAL = "dataVencimentoFinal";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String ERRO_NEGOCIO_DATA_VENCIMENTO_INICIO_DATA_VENCIMENTO_FINAL = 
										"ERRO_NEGOCIO_DATA_VENCIMENTO_INICIO_DATA_VENCIMENTO_FINAL";

	private static final String ERRO_NEGOCIO_RELATORIO_SEM_DADOS = "ERRO_NEGOCIO_RELATORIO_SEM_DADOS";

	private static final String ERRO_DATA_VENCIMENTO_OBRIGATORIO = "ERRO_DATA_VENCIMENTO_OBRIGATORIO";

	private static final long PENDENTE = 91;

	private static final long PARCIALMENTE_QUITADO = 92;

	private ControladorCobranca controladorCobranca = ServiceLocator.getInstancia().getControladorCobranca();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioPosicaoContasReceber#gerarPosicaoContasReceber(java.util.Map,
	 * br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarPosicaoContasReceber(Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Map<String, Object> parametros = new HashMap<String, Object>();
		if (controladorEmpresa.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem",
							Constantes.URL_LOGOMARCA_EMPRESA
											+ String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		}

		this.montarParametroRelatorio(parametros, filtro);

		Collection<RelatorioPosicaoContasReceberVO> listaRelatorioPosicaoContasReceberVO = this.montarListaDadosRelatorio(filtro);

		return RelatorioUtil.gerarRelatorio(listaRelatorioPosicaoContasReceberVO, parametros, RELATORIO_POSICAO_DO_CONTAS_A_RECEBER,
						formatoImpressao);
	}

	/**
	 * Montar parametro relatorio.
	 * 
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void montarParametroRelatorio(Map<String, Object> parametros, Map<String, Object> filtro) throws NegocioException {

		if (filtro != null && !filtro.isEmpty()) {

			Long idCliente = (Long) filtro.get(ID_CLIENTE);
			if ((idCliente != null) && (idCliente > 0)) {
				ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getBeanPorID(
								ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
				Cliente cliente = (Cliente) controladorCliente.obter(idCliente);
				parametros.put("nomeCliente", cliente.getNome());
			}

			Date dataVencimentoInicial = (Date) filtro.get(DATA_VENCIMENTO_INICIAL);
			Date dataVencimentoFinal = (Date) filtro.get(DATA_VENCIMENTO_FINAL);
			if ((dataVencimentoInicial != null) && (dataVencimentoFinal != null)) {
				parametros.put(DATA_VENCIMENTO_INICIAL, DataUtil.converterDataParaString(dataVencimentoInicial));
				parametros.put(DATA_VENCIMENTO_FINAL, DataUtil.converterDataParaString(dataVencimentoFinal));
			}

			Date dataReferencia = (Date) filtro.get("dataReferencia");
			if (dataReferencia != null) {
				parametros.put("dataReferencia", DataUtil.converterDataParaString(dataReferencia));
			} else {
				dataReferencia = new Date();
				parametros.put("dataReferencia", DataUtil.converterDataParaString(dataReferencia));
				filtro.put("dataReferencia", dataReferencia);
			}

			Long idSegmento = (Long) filtro.get("idSegmento");
			if ((idSegmento != null) && (idSegmento > 0)) {
				parametros.put("segmento", idSegmento);
			}

			Integer diasAtraso = (Integer) filtro.get("diasAtraso");
			if ((diasAtraso != null) && (diasAtraso > 0)) {
				parametros.put("diasAtraso", diasAtraso);
			}

			Boolean exibirFiltrosRelatorio = (Boolean) filtro.get(EXIBIR_FILTROS);
			parametros.put("exibirFiltros", exibirFiltrosRelatorio);
		}
	}

	/**
	 * Validar filtros.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarFiltros(Map<String, Object> filtro) throws NegocioException {

		Date dataVencimentoInicial = (Date) filtro.get(DATA_VENCIMENTO_INICIAL);
		Date dataVencimentoFinal = (Date) filtro.get(DATA_VENCIMENTO_FINAL);
		Long[] idsPontoConsumo = (Long[]) filtro.get("idsPontoConsumo");
		if (idsPontoConsumo != null && idsPontoConsumo.length > 0) {
			filtro.put("chavesPontoConsumo", idsPontoConsumo);
		}

		if ((dataVencimentoInicial != null) && (dataVencimentoFinal != null)) {
			if (Util.compararDatas(dataVencimentoInicial, dataVencimentoFinal) > 0) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_VENCIMENTO_INICIO_DATA_VENCIMENTO_FINAL, true);
			}
		} else if (dataVencimentoFinal == null || dataVencimentoInicial == null) {
			throw new NegocioException(ERRO_DATA_VENCIMENTO_OBRIGATORIO, true);
		}

	}

	/**
	 * Obter colecao faturas.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param controladorRecebimento
	 *            the controlador recebimento
	 * @param colecaoFaturas
	 *            the colecao faturas
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<Fatura> obterColecaoFaturas(Map<String, Object> filtro, ControladorRecebimento controladorRecebimento,
					Collection<Fatura> colecaoFaturas) throws NegocioException {

		Integer diasAtraso = null;
		Date campoDataReferencia = null;
		Collection<Fatura> colecaoFaturasLocal = new ArrayList<Fatura>();
		Collection<Fatura> colecaoFaturasAtt = new ArrayList<Fatura>();

		if (filtro != null && !filtro.isEmpty()) {

			campoDataReferencia = (Date) filtro.get("dataReferencia");
			diasAtraso = (Integer) filtro.get("diasAtraso");

			// Caso exista campo data referencia ou dias em atraso
			if (campoDataReferencia != null || diasAtraso != null && !diasAtraso.equals(0)) {

				// Pra cada Fatura, tem que verificar se possui Recebimento se
				// possuir, verifica se a data de recebimento dela é maior que a
				// data de refência.

				for (Fatura fatura : colecaoFaturas) {
					Date dataRecebimentoPelaFatura = controladorCobranca.obterUltimaDataRecebimentoPelaFatura(fatura.getChavePrimaria());

					if (dataRecebimentoPelaFatura == null) {
						colecaoFaturasLocal.add(fatura);
					} else {
						if (campoDataReferencia == null) {
							campoDataReferencia = new Date();
						}
						if (dataRecebimentoPelaFatura.after(campoDataReferencia)) {
							colecaoFaturasLocal.add(fatura);
						}
					}

				}

				filtro.put("dataReferencia", campoDataReferencia);
				if (diasAtraso != null) {
					for (Fatura fatura : colecaoFaturasLocal) {
						Date dataVencimento = fatura.getDataVencimento();

						int diasAtrasado = Util.intervaloDatas(dataVencimento, campoDataReferencia);
						if (diasAtrasado == diasAtraso) {
							colecaoFaturasAtt.add(fatura);
						}

					}
				} else {
					colecaoFaturasAtt = colecaoFaturasLocal;
				}

			} else {
				// Verifica dessa coleção quais são as faturas que não estão quitadas ou canceladas

				for (Fatura fatura : colecaoFaturas) {
					if (fatura.getSituacaoPagamento().getChavePrimaria() == PENDENTE
									|| fatura.getSituacaoPagamento().getChavePrimaria() == PARCIALMENTE_QUITADO) {
						colecaoFaturasAtt.add(fatura);
					}
				}

			}

		}
		return colecaoFaturasAtt;
	}

	/**
	 * Montar lista dados relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<RelatorioPosicaoContasReceberVO> montarListaDadosRelatorio(Map<String, Object> filtro) throws GGASException {

		List<SubRelatorioPosicaoContasReceberVO> listaSubRelatorioPosicaoContasReceberVO = null;

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ControladorRecebimento controladorRecebimento = (ControladorRecebimento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);

		ServiceLocator.getInstancia().getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		this.validarFiltros(filtro);
		Date campoDataReferencia = (Date) filtro.get("dataReferencia");

		// Faturas no intervalo de periodo de vencimento
		Collection<Fatura> colecaoFaturasFiltro = controladorFatura.consultarFatura(filtro);

		// Caso a fatura não tenha Recebimento, já é um titulo a receber...
		// caso tenha vejo a data de recebimento for maior que a data de referência tbm inclui no relatório
		// caso a data de referência for nula,pega a data atual
		Collection<Fatura> colecaoFaturas = this.obterColecaoFaturas(filtro, controladorRecebimento, colecaoFaturasFiltro);

		List<RelatorioPosicaoContasReceberVO> listaRelatorioPosicaoContasReceberVO = new ArrayList<RelatorioPosicaoContasReceberVO>();
		Map<String, List<SubRelatorioPosicaoContasReceberVO>> mapaSubRelatorioPosicaoContasReceberVO = 
						new LinkedHashMap<String, List<SubRelatorioPosicaoContasReceberVO>>();

		if (colecaoFaturas != null && !colecaoFaturas.isEmpty()) {

			Map<String, Collection<Fatura>> mapaImovelOuClienteFatura = controladorFatura.agruparFaturasPorImovelOuCliente(colecaoFaturas);

			for (Map.Entry<String, Collection<Fatura>> entry : mapaImovelOuClienteFatura.entrySet()) {
				String agrupamento = entry.getKey();
				Collection<Fatura> listaFaturas = entry.getValue();

				listaSubRelatorioPosicaoContasReceberVO = new ArrayList<SubRelatorioPosicaoContasReceberVO>();

				for (Fatura fatura : listaFaturas) {

					SubRelatorioPosicaoContasReceberVO subRelatorioPosicaoContasReceberVO = new SubRelatorioPosicaoContasReceberVO();

					if (fatura.getPontoConsumo() != null && fatura.getPontoConsumo().getDescricao() != null) {
						subRelatorioPosicaoContasReceberVO
										.setDescricaoPontoConsumo(String.valueOf(fatura.getPontoConsumo().getDescricao()));
					} else {

						if (fatura.getCliente() != null && fatura.getCliente().getNomeFantasia() != null) {
							subRelatorioPosicaoContasReceberVO.setDescricaoPontoConsumo(String.valueOf(fatura.getCliente()
											.getNomeFantasia()));
						}
					}

					if (fatura.getChavePrimaria() > 0) {
						subRelatorioPosicaoContasReceberVO.setNumeroTitulo(String.valueOf(fatura.getChavePrimaria()));
					}

					if (fatura.getDataEmissaoFormatada() != null) {

						subRelatorioPosicaoContasReceberVO.setDataEmissao(fatura.getDataEmissaoFormatada());
					}

					if (fatura.getDataVencimento() != null) {

						subRelatorioPosicaoContasReceberVO.setDataVencimento(Util.converterDataParaStringSemHora(
										fatura.getDataVencimento(), Constantes.FORMATO_DATA_BR));
					}

					if (fatura.getTipoDocumento().getDescricao() != null) {
						subRelatorioPosicaoContasReceberVO.setTipoDocumento(fatura.getTipoDocumento().getDescricao());
					}

					filtro.clear();
					filtro.put("idFaturaGeral", fatura.getChavePrimaria());

					SubRelatorioPosicaoContasReceberVO subRelatorioPosicaoContasReceberVOBanco = null;

					Integer qtdDiasAtraso = null;

					qtdDiasAtraso = Util.intervaloDatas(fatura.getDataVencimento(), campoDataReferencia);

					if(qtdDiasAtraso != null){
						subRelatorioPosicaoContasReceberVO.setQtdDiasAtraso(String.valueOf(qtdDiasAtraso.intValue()));
					} else {
						subRelatorioPosicaoContasReceberVO.setQtdDiasAtraso(String.valueOf(0));
					}
					
					
					if (fatura.getValorTotal() != null) {

						subRelatorioPosicaoContasReceberVO.setValorHistorico(fatura.getValorTotal());
					}

					this.adicionarJurosEMultaNaFatura(fatura, subRelatorioPosicaoContasReceberVOBanco, subRelatorioPosicaoContasReceberVO);

					listaSubRelatorioPosicaoContasReceberVO.add(subRelatorioPosicaoContasReceberVO);
				}

				mapaSubRelatorioPosicaoContasReceberVO.put(agrupamento, listaSubRelatorioPosicaoContasReceberVO);
			}
		}

		if (mapaSubRelatorioPosicaoContasReceberVO.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		for (Map.Entry<String, List<SubRelatorioPosicaoContasReceberVO>> entry : mapaSubRelatorioPosicaoContasReceberVO.entrySet()) {

			String agrupamento = entry.getKey();
			BigDecimal totalQtdDiasAtraso = BigDecimal.ZERO;
			BigDecimal totalValorDebito = BigDecimal.ZERO;
			BigDecimal totalValorCorrigido = BigDecimal.ZERO;
			int totalqtdTitulo = 0;

			RelatorioPosicaoContasReceberVO relatorioPosicaoContasReceberVO = new RelatorioPosicaoContasReceberVO();
			relatorioPosicaoContasReceberVO.setNomeClienteOuImovel(agrupamento);

			List<SubRelatorioPosicaoContasReceberVO> listaSubRel = entry.getValue();

			for (SubRelatorioPosicaoContasReceberVO subRelatorioPosicaoContasReceberVO : listaSubRel) {

				if (subRelatorioPosicaoContasReceberVO.getQtdDiasAtraso() != null
								&& !"null".equals(subRelatorioPosicaoContasReceberVO.getQtdDiasAtraso())) {

					BigDecimal qtdDiasAtraso = Util.converterCampoStringParaValorBigDecimal("",
									subRelatorioPosicaoContasReceberVO.getQtdDiasAtraso(), Constantes.FORMATO_VALOR_BR,
									Constantes.LOCALE_PADRAO);

					totalQtdDiasAtraso = totalQtdDiasAtraso.add(qtdDiasAtraso);
					subRelatorioPosicaoContasReceberVO.setTotalQtdDiasAtraso(String.valueOf(totalQtdDiasAtraso));

				} else {

					subRelatorioPosicaoContasReceberVO.setQtdDiasAtraso(String.valueOf(0));
				}

				if (subRelatorioPosicaoContasReceberVO.getValorHistorico() != null
								&& !(subRelatorioPosicaoContasReceberVO.getValorHistorico().compareTo(BigDecimal.ZERO)==0)) {

					totalValorDebito = totalValorDebito.add(subRelatorioPosicaoContasReceberVO.getValorHistorico());
					subRelatorioPosicaoContasReceberVO.setTotalValorHistorico(totalValorDebito);
				}

				if (subRelatorioPosicaoContasReceberVO.getValorCorrigido() != null) {

					totalValorCorrigido = totalValorCorrigido.add(subRelatorioPosicaoContasReceberVO.getValorCorrigido());
					subRelatorioPosicaoContasReceberVO.setTotalValorCorrigido(totalValorCorrigido);
				}
				++totalqtdTitulo;
				subRelatorioPosicaoContasReceberVO.setTotalqtdTitulo(String.valueOf(totalqtdTitulo));
			}

			relatorioPosicaoContasReceberVO.setColecaoVOs(listaSubRel);
			listaRelatorioPosicaoContasReceberVO.add(relatorioPosicaoContasReceberVO);

		}

		return listaRelatorioPosicaoContasReceberVO;
	}

	/**
	 * Adicionar juros e multa na fatura.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param subRelatorioPosicaoContasReceberVOBanco
	 *            the sub relatorio posicao contas receber vo banco
	 * @param subRelatorioPosicaoContasReceberVORelatorio
	 *            the sub relatorio posicao contas receber vo relatorio
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Map<String, BigDecimal> adicionarJurosEMultaNaFatura(Fatura fatura,
					SubRelatorioPosicaoContasReceberVO subRelatorioPosicaoContasReceberVOBanco,
					SubRelatorioPosicaoContasReceberVO subRelatorioPosicaoContasReceberVORelatorio) throws GGASException {

		Map<String, BigDecimal> mapaJurosMultaValorCorrigido = this.calcularValorCorrigidoJurosMulta(fatura,
						subRelatorioPosicaoContasReceberVOBanco);

		subRelatorioPosicaoContasReceberVORelatorio.setValorCorrigido(mapaJurosMultaValorCorrigido.get("valorCorrigido"));

		return mapaJurosMultaValorCorrigido;

	}

	/**
	 * Calcular valor corrigido juros multa.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param subRelatorioPosicaoContasReceberVO
	 *            the sub relatorio posicao contas receber vo
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Map<String, BigDecimal> calcularValorCorrigidoJurosMulta(Fatura fatura,
					SubRelatorioPosicaoContasReceberVO subRelatorioPosicaoContasReceberVO) throws GGASException {

		DebitosACobrar debitosACobrarVO;
		BigDecimal valorCorrigido = fatura.getValorTotal();
		BigDecimal valorJuros = null;
		BigDecimal valorMulta = null;
		Map<String, BigDecimal> mapaJurosMultaValorCorrigido = new LinkedHashMap<String, BigDecimal>();

		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia().getBeanPorID(
						ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);

		Date dataPagamento = null;
		BigDecimal valorPagamento = null;

		if (subRelatorioPosicaoContasReceberVO != null) {

			dataPagamento = subRelatorioPosicaoContasReceberVO.getDataPagamento();
			valorPagamento = subRelatorioPosicaoContasReceberVO.getValorPagamento();
		} else {

			valorPagamento = BigDecimal.ZERO;
		}

		debitosACobrarVO = controladorArrecadacao.calcularAcrescimentoImpontualidade(fatura, dataPagamento, valorPagamento, true, true);

		if (debitosACobrarVO != null) {

			if (debitosACobrarVO.getJurosMora() != null) {
				valorJuros = fatura.getValorTotal().multiply(debitosACobrarVO.getJurosMora());
				valorCorrigido = valorCorrigido.add(valorJuros);
			}
			if (debitosACobrarVO.getMulta() != null) {
				valorMulta = fatura.getValorTotal().multiply(debitosACobrarVO.getMulta());
				valorCorrigido = valorCorrigido.add(valorMulta);
			}
		}

		mapaJurosMultaValorCorrigido.put("valorCorrigido", valorCorrigido);

		return mapaJurosMultaValorCorrigido;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioPosicaoContasReceber#listarPontosConsumoRelatorioPosicaoContasReceber(java.util.Map)
	 */
	@Override
	public List<PontoConsumo> listarPontosConsumoRelatorioPosicaoContasReceber(Map<String, Object> filtro) {

		Long idCliente = (Long) filtro.get(ID_CLIENTE);
		Long[] idsSegmentos = (Long[]) filtro.get("idsSegmentos");

		Criteria criteria = this.createCriteria(PontoConsumo.class);
		criteria.createAlias("segmento", "segmento", Criteria.LEFT_JOIN);

		if (idCliente != null) {
			criteria.createCriteria("imovel.listaClienteImovel").createCriteria("cliente")
							.add(Restrictions.eq("chavePrimaria", filtro.get(ID_CLIENTE)));
		}

		if (idsSegmentos != null) {
			criteria.add(Restrictions.in("segmento.chavePrimaria", idsSegmentos));
		}

		boolean paginacaoPadrao = false;
		if (filtro.containsKey("colecaoPaginada")) {

			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if (paginacaoPadrao) {
			criteria.addOrder(Order.asc("descricao"));
		}

		return criteria.list();
	}

}
