/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioDeInadimplentes;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorRelatorioDeInadimplentesImpl extends ControladorNegocioImpl implements ControladorRelatorioDeInadimplentes {

	private static final DynaProperty[] ATRIBUTOS_RELATORIO_INADIMPLENTES = new DynaProperty[] {new DynaProperty("dadosSegmentos",
					List.class), new DynaProperty("dadosPontosConsumo", List.class), new DynaProperty("dadosPontosConsumo", List.class), new DynaProperty(
					"dadosFiltro", List.class), new DynaProperty("dataBase", String.class), new DynaProperty("exibirResponsavelRevisao",
					Boolean.class)};

	private static final DynaClass RELATORIO_INADIMPLENTES = new BasicDynaClass("RelatorioInadimplentes", null,
					ATRIBUTOS_RELATORIO_INADIMPLENTES);

	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_INADIMPLENTES_DADOS_PONTO_CONSUMO = new DynaProperty[] {new DynaProperty(
					"descricaoPontoConsumo", String.class), new DynaProperty("nomeCliente", String.class), new DynaProperty(
					"descricaoSegmento", String.class), new DynaProperty("dataVencimento", String.class), new DynaProperty("diasAtraso",
					Integer.class), new DynaProperty("valor", BigDecimal.class), new DynaProperty("volume", BigDecimal.class), new DynaProperty(
					"titulo", String.class), new DynaProperty("situacao", String.class), new DynaProperty("responsavelRevisao",
					String.class)};

	private static final DynaClass SUB_RELATORIO_INADIMPLENTES_DADOS_PONTO_CONSUMO = new BasicDynaClass(
					"subRelatorioInadimplentesDadosPontoConsumo", null, ATRIBUTOS_SUB_RELATORIO_INADIMPLENTES_DADOS_PONTO_CONSUMO);

	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_INADIMPLENTES_DADOS_SEGMENTOS = new DynaProperty[] {new DynaProperty(
					"descricaoSegmento", String.class), new DynaProperty("numeroTitulosAberto", Integer.class), new DynaProperty(
					"valorTitulosAberto", BigDecimal.class), new DynaProperty("volumes", BigDecimal.class), new DynaProperty(
					"numeroTitulosMenorIgualTrintaDias", Integer.class), new DynaProperty("valorTitulosMenorIgualTrintaDias",
					BigDecimal.class), new DynaProperty("numeroTitulosMenorIgualSessentaDias", Integer.class), new DynaProperty(
					"valorTitulosMenorIgualSessentaDias", BigDecimal.class), new DynaProperty("numeroTitulosMenorIgualNoventaDias",
					Integer.class), new DynaProperty("valorTitulosMenorIgualNoventaDias", BigDecimal.class), new DynaProperty(
					"numeroTitulosMaiorNoventaDias", Integer.class), new DynaProperty("valorTitulosMaiorNoventaDias", BigDecimal.class)};

	private static final DynaClass SUB_RELATORIO_INADIMPLENTES_DADOS_SEGMENTOS = new BasicDynaClass(
					"subRelatorioInadimplentesDadosSegmentos", null, ATRIBUTOS_SUB_RELATORIO_INADIMPLENTES_DADOS_SEGMENTOS);

	private static final DynaProperty[] ATRIBUTOS_SUB_RELATORIO_INADIMPLENTES_DADOS_FILTRO = new DynaProperty[] {new DynaProperty(
					"pontosConsumo", String.class), new DynaProperty("cliente", String.class), new DynaProperty("imovel", String.class), new DynaProperty(
					"segmentos", String.class)};

	private static final DynaClass SUB_RELATORIO_INADIMPLENTES_DADOS_FILTRO = new BasicDynaClass("subRelatorioInadimplentesDadosFiltro",
					null, ATRIBUTOS_SUB_RELATORIO_INADIMPLENTES_DADOS_FILTRO);

	private static final String DIAS_VENCIDO = "diasVencido";

	private static final String MAIS_90_DIAS = "mais90Dias";

	private static final String TODOS = "todos";

	private final ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

	private final ControladorCobranca controladorCobranca = (ControladorCobranca) ServiceLocator.getInstancia().getControladorNegocio(
					ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);

	private final ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
					ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.
	 * ControladorRelatorioDeInadimplentes
	 * #gerarRelatorio(java.util.Map)
	 */
	@Override
	public byte[] gerarRelatorio(Map<String, Object> parametros) throws NegocioException, FormatoInvalidoException {

		this.validarDadosGerarRelatorioInadimplentes(parametros);

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf((String) parametros.get("formatoImpressao"));
		if(this.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(this.obterEmpresaPrincipal().getChavePrimaria()));
		}

		Collection<Object> collRelatorio = new ArrayList<Object>();
		collRelatorio.add(this.obterRelatorio(parametros));

		return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_DE_INADIMPLENTES, formatoImpressao);
	}

	/**
	 * Validar dados gerar relatorio inadimplentes.
	 * 
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosGerarRelatorioInadimplentes(Map<String, Object> parametros) throws NegocioException {

		if(parametros.get("formatoImpressao") == null) {

			throw new NegocioException(ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO, true);

		}

		if(parametros.get("dataBase") == null) {

			throw new NegocioException(ERRO_NEGOCIO_CAMPO_DATA_BASE_NAO_INFORMADO, true);

		}

	}

	/**
	 * Obter empresa principal.
	 * 
	 * @return the empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Empresa obterEmpresaPrincipal() throws NegocioException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("principal", Boolean.TRUE);
		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);

		Empresa empresa = null;
		if((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
			empresa = ((List<Empresa>) listaEmpresas).get(0);
		}

		return empresa;
	}

	/**
	 * Obter relatorio.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the object
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private Object obterRelatorio(Map<String, Object> parametros) throws NegocioException, FormatoInvalidoException {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		Date dataBase = (Date) parametros.get("dataBase");

		List<Fatura> faturas = this.listarFaturas(parametros);
		Integer diasAtraso = (Integer) parametros.get("diasAtraso");
		String diasAtrasoParametro = (String) parametros.get(DIAS_VENCIDO);
		if(diasAtraso != null && diasAtrasoParametro.equals(TODOS)) {
			Date dataReferencia = (Date) parametros.get("dataBase");
			Collection<Fatura> fatuasAtrasadas = controladorFatura.removerFaturaPorDiasAtraso(faturas, diasAtraso, dataReferencia);
			faturas.clear();
			faturas.addAll(fatuasAtrasadas);
		}
		List<Fatura> faturaInadimplente = faturaInadimplente(faturas, dataBase);
		DynaBean relatorioInadimplentes = new BasicDynaBean(RELATORIO_INADIMPLENTES);
		relatorioInadimplentes.set("dataBase", df.format(dataBase));
		List<Object> listaFaturaPorPontoConsumo = this.obterDadosPontosConsumo(faturaInadimplente, parametros);
		if(listaFaturaPorPontoConsumo != null && !listaFaturaPorPontoConsumo.isEmpty()){
			relatorioInadimplentes.set("dadosPontosConsumo", listaFaturaPorPontoConsumo);
			relatorioInadimplentes.set("dadosSegmentos", this.obterDadosSegmentos(faturaInadimplente, parametros));
			relatorioInadimplentes.set("dadosFiltro", this.obterDadosFiltro(parametros));
			relatorioInadimplentes.set("exibirResponsavelRevisao", parametros.get("faturaRevisao"));
		} else {
			throw new NegocioException(ControladorRelatorioDeInadimplentes.ERRO_NEGOCIO_NENHUMA_FATURA_EM_ATRASO_FOI_ENCONTRADA, true);
		}

		return relatorioInadimplentes;
	}

	/**
	 * Obter dados filtro.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<Object> obterDadosFiltro(Map<String, Object> parametros) throws NegocioException {

		Boolean exibirFiltros = (Boolean) parametros.get("exibirFiltros");

		if(exibirFiltros != null && exibirFiltros) {

			List<Object> subRelatorios = new ArrayList<Object>();

			Long idCliente = (Long) parametros.get("idCliente");
			Long idImovel = (Long) parametros.get("idImovel");
			Long[] idsSegmentos = (Long[]) parametros.get("idsSegmentos");
			Long[] idsPontoConsumo = (Long[]) parametros.get("idsPontoConsumo");

			DynaBean subRelatorio = new BasicDynaBean(SUB_RELATORIO_INADIMPLENTES_DADOS_FILTRO);

			if(idCliente != null) {
				subRelatorio.set("cliente", this.obterCliente(idCliente).getNome());
			} else {
				subRelatorio.set("cliente", "");
			}

			if(idImovel != null) {
				subRelatorio.set("imovel", this.obterImovel(idImovel).getNome());
			} else {
				subRelatorio.set("imovel", "");
			}

			if(idsSegmentos != null) {

				StringBuilder sb = new StringBuilder();
				Boolean primeiraLinha = Boolean.TRUE;
				for (Segmento segmento : this.listarSegmentos(parametros)) {
					if(!primeiraLinha) {
						sb.append(System.getProperty("line.separator")).append(segmento.getDescricao());
					} else {
						sb.append(segmento.getDescricao());
						primeiraLinha = Boolean.FALSE;
					}
				}

				subRelatorio.set("segmentos", sb.toString());
			} else {
				subRelatorio.set("segmentos", "");
			}

			if(idsPontoConsumo != null) {

				Map<String, Object> filtro = new HashMap<String, Object>();
				filtro.put("idsPonto", parametros.get("idsPontoConsumo"));

				StringBuilder sb = new StringBuilder();
				Boolean primeiraLinha = Boolean.TRUE;
				for (PontoConsumo pontoConsumo : this.listarPontosConsumoRelatorioInadimplentes(filtro)) {
					if(!primeiraLinha) {
						sb.append(System.getProperty("line.separator")).append( pontoConsumo.getDescricao());
					} else {
						sb.append(pontoConsumo.getDescricao());
						primeiraLinha = Boolean.FALSE;
					}
				}

				subRelatorio.set("pontosConsumo", sb.toString());

			} else {
				subRelatorio.set("pontosConsumo", "");
			}

			subRelatorios.add(subRelatorio);

			return subRelatorios;

		} else {
			return null;
		}

	}

	/**
	 * Obter dados segmentos.
	 * 
	 * @param faturas
	 *            the faturas
	 * @param parametros
	 *            the parametros
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<Object> obterDadosSegmentos(List<Fatura> faturas, Map<String, Object> parametros) throws NegocioException {

		if(faturas != null && !faturas.isEmpty()) {

			List<Object> subRelatorios = new ArrayList<Object>();
			Map<Segmento, List<Fatura>> mapSegmento = obterFaturasAgrupadasPorSegmento(faturas, parametros);

			for (Segmento segmento : mapSegmento.keySet()) {

				Map<String, Object> mapaValores = this.obterTotaisDadosSegmentos(segmento, mapSegmento);

				DynaBean subRelatorio = new BasicDynaBean(SUB_RELATORIO_INADIMPLENTES_DADOS_SEGMENTOS);
				subRelatorio.set("descricaoSegmento", segmento.getDescricao());
				subRelatorio.set("numeroTitulosAberto", mapaValores.get("numeroTitulosAberto"));
				subRelatorio.set("valorTitulosAberto", mapaValores.get("valorTitulosAberto"));
				subRelatorio.set("volumes", mapaValores.get("volumes"));
				subRelatorio.set("numeroTitulosMenorIgualTrintaDias", mapaValores.get("numeroTitulosMenorIgualTrintaDias"));
				subRelatorio.set("valorTitulosMenorIgualTrintaDias", mapaValores.get("valorTitulosMenorIgualTrintaDias"));
				subRelatorio.set("numeroTitulosMenorIgualSessentaDias", mapaValores.get("numeroTitulosMenorIgualSessentaDias"));
				subRelatorio.set("valorTitulosMenorIgualSessentaDias", mapaValores.get("valorTitulosMenorIgualSessentaDias"));
				subRelatorio.set("numeroTitulosMenorIgualNoventaDias", mapaValores.get("numeroTitulosMenorIgualNoventaDias"));
				subRelatorio.set("valorTitulosMenorIgualNoventaDias", mapaValores.get("valorTitulosMenorIgualNoventaDias"));
				subRelatorio.set("numeroTitulosMaiorNoventaDias", mapaValores.get("numeroTitulosMaiorNoventaDias"));
				subRelatorio.set("valorTitulosMaiorNoventaDias", mapaValores.get("valorTitulosMaiorNoventaDias"));

				subRelatorios.add(subRelatorio);
			}

			return subRelatorios;

		} else {
			return null;
		}

	}

	/**
	 * Obter totais dados segmentos.
	 * 
	 * @param segmento
	 *            the segmento
	 * @param mapSegmento
	 *            the map segmento
	 * @return the map
	 */
	private Map<String, Object> obterTotaisDadosSegmentos(Segmento segmento, Map<Segmento, List<Fatura>> mapSegmento) {

		Map<String, Object> mapa = new HashMap<String, Object>();

		Date dataAtual = new Date();

		Integer numeroTitulosAberto = 0;
		BigDecimal valorTitulosAberto = BigDecimal.ZERO;

		BigDecimal somaVolumes = BigDecimal.ZERO;

		Integer numeroTitulosMenorIgualTrintaDias = 0;
		BigDecimal valorTitulosMenorIgualTrintaDias = BigDecimal.ZERO;

		Integer numeroTitulosMenorIgualSessentaDias = 0;
		BigDecimal valorTitulosMenorIgualSessentaDias = BigDecimal.ZERO;

		Integer numeroTitulosMenorIgualNoventaDias = 0;
		BigDecimal valorTitulosMenorIgualNoventaDias = BigDecimal.ZERO;

		Integer numeroTitulosMaiorNoventaDias = 0;
		BigDecimal valorTitulosMaiorNoventaDias = BigDecimal.ZERO;

		for (Fatura fatura : mapSegmento.get(segmento)) {

			somaVolumes = calcularVolumeFatura(somaVolumes, fatura);

			int diasAtraso = Util.intervaloDatas(fatura.getDataVencimento(), dataAtual);

			numeroTitulosAberto = (numeroTitulosAberto + 1);
			valorTitulosAberto = valorTitulosAberto.add(fatura.getValorTotal());

			if(diasAtraso <= 30) {

				numeroTitulosMenorIgualTrintaDias = (numeroTitulosMenorIgualTrintaDias + 1);
				valorTitulosMenorIgualTrintaDias = valorTitulosMenorIgualTrintaDias.add(fatura.getValorTotal());

			} else if(diasAtraso <= 60) {

				numeroTitulosMenorIgualSessentaDias = (numeroTitulosMenorIgualSessentaDias + 1);
				valorTitulosMenorIgualSessentaDias = valorTitulosMenorIgualSessentaDias.add(fatura.getValorTotal());

			} else if(diasAtraso <= 90) {

				numeroTitulosMenorIgualNoventaDias = (numeroTitulosMenorIgualNoventaDias + 1);
				valorTitulosMenorIgualNoventaDias = valorTitulosMenorIgualNoventaDias.add(fatura.getValorTotal());

			} else {

				numeroTitulosMaiorNoventaDias = (numeroTitulosMaiorNoventaDias + 1);
				valorTitulosMaiorNoventaDias = valorTitulosMaiorNoventaDias.add(fatura.getValorTotal());

			}

		}

		mapa.put("numeroTitulosAberto", numeroTitulosAberto);
		mapa.put("valorTitulosAberto", valorTitulosAberto);
		mapa.put("volumes", somaVolumes);

		mapa.put("numeroTitulosMenorIgualTrintaDias", numeroTitulosMenorIgualTrintaDias);
		mapa.put("valorTitulosMenorIgualTrintaDias", valorTitulosMenorIgualTrintaDias);

		mapa.put("numeroTitulosMenorIgualSessentaDias", numeroTitulosMenorIgualSessentaDias);
		mapa.put("valorTitulosMenorIgualSessentaDias", valorTitulosMenorIgualSessentaDias);

		mapa.put("numeroTitulosMenorIgualNoventaDias", numeroTitulosMenorIgualNoventaDias);
		mapa.put("valorTitulosMenorIgualNoventaDias", valorTitulosMenorIgualNoventaDias);

		mapa.put("numeroTitulosMaiorNoventaDias", numeroTitulosMaiorNoventaDias);
		mapa.put("valorTitulosMaiorNoventaDias", valorTitulosMaiorNoventaDias);

		return mapa;
	}

	/**
	 * Obter dados pontos consumo.
	 * 
	 * @param faturas
	 *            the faturas
	 * @param parametros
	 *            the parametros
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private List<Object> obterDadosPontosConsumo(List<Fatura> faturas, Map<String, Object> parametros) throws NegocioException,
					FormatoInvalidoException {

		BigDecimal somaVolumes = BigDecimal.ZERO;
		if(faturas != null && !faturas.isEmpty()) {

			SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
			List<Object> subRelatorios = new ArrayList<Object>();
			Date dataReferencia = (Date) parametros.get("dataBase");
			String diasAtrasoParametro = (String) parametros.get(DIAS_VENCIDO);

			for (Fatura fatura : faturas) {

				int diasAtraso = Util.intervaloDatas(fatura.getDataVencimento(), dataReferencia);

				if(diasAtrasoParametro.equals(MAIS_90_DIAS)) {
					if(diasAtraso > 90) {
						popularSubRelatorioPontosConsumo(somaVolumes, df, subRelatorios, fatura, dataReferencia);
					}

				} else if(diasAtrasoParametro.equals(TODOS)) {
					popularSubRelatorioPontosConsumo(somaVolumes, df, subRelatorios, fatura, dataReferencia);
				} else {
					int campoDiasAtrasoParametro = Util.converterCampoStringParaValorInteger("Dias Atraso", diasAtrasoParametro);
					if(diasAtraso <= campoDiasAtrasoParametro) {
						popularSubRelatorioPontosConsumo(somaVolumes, df, subRelatorios, fatura, dataReferencia);
					}
				}

			}

			return subRelatorios;

		} else {
			return null;
		}

	}

	/**
	 * Fatura inadimplente.
	 * 
	 * @param faturasInadimplentes
	 *            the faturas inadimplentes
	 * @param dataReferencia
	 *            the data referencia
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<Fatura> faturaInadimplente(List<Fatura> faturasInadimplentes, Date dataReferencia) throws NegocioException {

		String situacaoPaga = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

		List<Fatura> listaFaturasInadimplentes = new ArrayList<Fatura>();
		for (Fatura fatura : faturasInadimplentes) {
			Date dataPagamento = controladorCobranca.obterUltimaDataRecebimentoPelaFatura(fatura.getChavePrimaria());

			Date dataCancelamento = fatura.getDataCancelamento();
			if (fatura.getSituacaoPagamento().getChavePrimaria() == Long.parseLong(situacaoPaga)) {
				if (dataPagamento != null && dataPagamento.after(dataReferencia)) {
					listaFaturasInadimplentes.add(fatura);

				}

			} else if(dataCancelamento != null) {
				if(dataCancelamento.after(dataReferencia)) {
					if(dataPagamento != null) {
						if(dataPagamento.after(dataReferencia)) {
							listaFaturasInadimplentes.add(fatura);
						}
					} else {
						listaFaturasInadimplentes.add(fatura);
					}

				}
			} else if(Util.intervaloDatas(fatura.getDataVencimento(), dataReferencia) > 0) {
				listaFaturasInadimplentes.add(fatura);
			}
		}
		return listaFaturasInadimplentes;

	}

	/**
	 * Popular sub relatorio pontos consumo.
	 * 
	 * @param somaVolumes
	 *            the soma volumes
	 * @param df
	 *            the df
	 * @param subRelatorios
	 *            the sub relatorios
	 * @param fatura
	 *            the fatura
	 * @param dataReferencia
	 *            the data referencia
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private BigDecimal popularSubRelatorioPontosConsumo(BigDecimal somaVolumes, SimpleDateFormat df, List<Object> subRelatorios,
					Fatura fatura, Date dataReferencia) throws NegocioException {

		DynaBean subRelatorio = new BasicDynaBean(SUB_RELATORIO_INADIMPLENTES_DADOS_PONTO_CONSUMO);

		int diasAtraso = Util.intervaloDatas(fatura.getDataVencimento(), dataReferencia);

		if(fatura.getPontoConsumo() != null) {
			String nome = fatura.getPontoConsumo().getImovel().getNome();
			String descricao = fatura.getPontoConsumo().getDescricao();
			StringBuilder descricaoFormatada = new StringBuilder();
			String separador = " - ";

			if(nome != null) {
				descricaoFormatada.append(nome);
				descricaoFormatada.append(separador);

			}
			if(descricao != null) {
				descricaoFormatada.append(descricao);
			}
			subRelatorio.set("descricaoPontoConsumo", descricaoFormatada.toString());
		} else {
			subRelatorio.set("descricaoPontoConsumo", "");
		}

		if (fatura.getCliente() != null) {
			subRelatorio.set("nomeCliente", fatura.getCliente().getNome());
		} else {
			subRelatorio.set("nomeCliente", "");
		}

		if (fatura.getSegmento() != null) {
			subRelatorio.set("descricaoSegmento", fatura.getSegmento().getDescricao());
		} else {
			subRelatorio.set("descricaoSegmento", "");
		}
		
		if (fatura.getPontoConsumo() != null) {
			subRelatorio.set("situacao", fatura.getPontoConsumo().getSituacaoConsumo().getDescricao());
		} else {
			subRelatorio.set("situacao", "");
		}
		
		if (fatura.getDataVencimento() != null) {
			subRelatorio.set("dataVencimento", df.format(fatura.getDataVencimento()));
		} else {
			subRelatorio.set("dataVencimento", "");
		}
		
		subRelatorio.set("valor", fatura.getValorTotal());
		subRelatorio.set("titulo", this.obterNumeroDocumentoFiscal(fatura));
		subRelatorio.set("diasAtraso", diasAtraso);

		Funcionario funcionario = fatura.getResponsavelRevisao();
		if(funcionario != null) {
			subRelatorio.set("responsavelRevisao", funcionario.getNome());
		}

		subRelatorio.set("volume", calcularVolumeFatura(somaVolumes, fatura));

		subRelatorios.add(subRelatorio);
		return somaVolumes;
	}

	/**
	 * Calcular volume fatura.
	 * 
	 * @param somaVolumes
	 *            the soma volumes
	 * @param fatura
	 *            the fatura
	 * @return the big decimal
	 */
	private BigDecimal calcularVolumeFatura(BigDecimal somaVolumes, Fatura fatura) {

		if(fatura.getListaFaturaItem() != null && !fatura.getListaFaturaItem().isEmpty()) {
			FaturaItem faturaItemUnico = fatura.getListaFaturaItem().iterator().next();
			BigDecimal quantidadeMetroCubico = BigDecimal.ZERO;
			quantidadeMetroCubico = faturaItemUnico.getQuantidade().setScale(0, BigDecimal.ROUND_HALF_UP);
			return somaVolumes.add(quantidadeMetroCubico);
		}
		return somaVolumes;
	}

	/**
	 * Listar faturas.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private List<Fatura> listarFaturas(Map<String, Object> parametros) throws NegocioException {

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ServiceLocator.getInstancia().getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Date dataBase = (Date) parametros.get("dataBase");
		Long idCliente = (Long) parametros.get("idCliente");
		Long idImovel = (Long) parametros.get("idImovel");
		Long[] idsSegmentos = (Long[]) parametros.get("idsSegmentos");
		Long[] idsPontoConsumo = (Long[]) parametros.get("idsPontoConsumo");
		Long idSituacaoPontoConsumo = (Long) parametros.get("idSituacaoPontoConsumo");
		Boolean indicadorRevisao = (Boolean) parametros.get("faturaRevisao");
		Long creditoDebitoNormal = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_NORMAL));
		Long creditoDebitoIncluido = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_INCLUIDO));

		Criterion restricao1 = Restrictions.eq("creditoDebitoSituacao.chavePrimaria", creditoDebitoIncluido);

		Criterion restricao2 = Restrictions.eq("creditoDebitoSituacao.chavePrimaria", creditoDebitoNormal);

		Criteria criteria = this.createCriteria(Fatura.class);
		criteria.createAlias("cliente", "cliente", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo", "pontoConsumo", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.imovel", "imovel", Criteria.LEFT_JOIN);
		criteria.createAlias("pontoConsumo.situacaoConsumo", "situacaoConsumo", Criteria.LEFT_JOIN);
		criteria.createAlias("segmento", "segmento", Criteria.INNER_JOIN);

		criteria.add(Restrictions.lt("dataVencimento", dataBase));
		criteria.add(Restrictions.le("dataEmissao", dataBase));
		criteria.add(Restrictions.or(restricao1, restricao2));

		if(idsPontoConsumo != null) {

			criteria.add(Restrictions.in("pontoConsumo.chavePrimaria", idsPontoConsumo));

		} else {

			if(idCliente != null) {
				criteria.add(Restrictions.eq("cliente.chavePrimaria", idCliente));
			}

			if(idImovel != null) {
				criteria.add(Restrictions.eq("imovel.chavePrimaria", idImovel));
			}

			if(idsSegmentos != null) {
				criteria.add(Restrictions.in("segmento.chavePrimaria", idsSegmentos));
			}

		}

		if(idSituacaoPontoConsumo != null && idSituacaoPontoConsumo > 0) {
			criteria.add(Restrictions.eq("situacaoConsumo.chavePrimaria", idSituacaoPontoConsumo));
		}

		if(!indicadorRevisao) {
			criteria.add(Restrictions.isNull("dataRevisao"));
		}

		criteria.addOrder(Order.asc("pontoConsumo.chavePrimaria"));

		List<Fatura> faturas = criteria.list();

		if(faturas == null || faturas.isEmpty()) {

			throw new NegocioException(ERRO_NEGOCIO_NENHUMA_FATURA_EM_ATRASO_FOI_ENCONTRADA, true);

		}

		return faturas;

	}

	/**
	 * Obter numero documento fiscal.
	 * 
	 * @param fatura
	 *            the fatura
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private String obterNumeroDocumentoFiscal(Fatura fatura) throws NegocioException {

		String retorno = "";

		Criteria criteria = this.createCriteria(DocumentoFiscal.class);
		criteria.add(Restrictions.eq("fatura.chavePrimaria", fatura.getChavePrimaria()));

		List<DocumentoFiscal> docs = criteria.list();

		if(docs != null && !docs.isEmpty()) {
			retorno = docs.get(0).getNumero().toString();
		}

		return retorno;
	}

	/**
	 * Listar segmentos.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private List<Segmento> listarSegmentos(Map<String, Object> parametros) throws NegocioException {

		Long[] idsSegmentos = (Long[]) parametros.get("idsSegmentos");

		Criteria criteria = this.createCriteria(Segmento.class);

		if(idsSegmentos != null) {
			criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idsSegmentos));
		}

		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	/**
	 * Obter faturas agrupadas por segmento.
	 * 
	 * @param faturas
	 *            the faturas
	 * @param parametros
	 *            the parametros
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Map<Segmento, List<Fatura>> obterFaturasAgrupadasPorSegmento(List<Fatura> faturas, Map<String, Object> parametros)
					throws NegocioException {

		List<Segmento> segmentos = this.listarSegmentos(parametros);
		Map<Segmento, List<Fatura>> mapSegmento = new LinkedHashMap<Segmento, List<Fatura>>();

		for (Segmento segmento : segmentos) {

			List<Fatura> listaFaturaPorSegmento = new ArrayList<Fatura>();
			mapSegmento.put(segmento, listaFaturaPorSegmento);

			for (Fatura fatura : faturas) {
				if(fatura.getSegmento() != null && fatura.getSegmento().getChavePrimaria() == segmento.getChavePrimaria()) {
					listaFaturaPorSegmento.add(fatura);
				}
			}
		}

		return mapSegmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.
	 * ControladorRelatorioDeInadimplentes
	 * #listarPontosConsumo(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<PontoConsumo> listarPontosConsumoRelatorioInadimplentes(Map<String, Object> filtro) throws NegocioException {

		Long idCliente = (Long) filtro.get("idCliente");
		Long idImovel = (Long) filtro.get("idImovel");
		Long[] idsSegmentos = (Long[]) filtro.get("idsSegmentos");
		Long[] idsPontoConsumo = (Long[]) filtro.get("idsPonto");

		Criteria criteria = this.createCriteria(PontoConsumo.class);
		criteria.createAlias("imovel", "imovel", Criteria.LEFT_JOIN);
		criteria.createAlias("segmento", "segmento", Criteria.LEFT_JOIN);

		if(idCliente != null) {
			criteria.createCriteria("imovel.listaClienteImovel").createCriteria("cliente")
							.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, filtro.get("idCliente")));
		}

		if(idImovel != null) {
			criteria.add(Restrictions.eq("imovel.chavePrimaria", idImovel));
		}

		if(idsSegmentos != null) {
			criteria.add(Restrictions.in("segmento.chavePrimaria", idsSegmentos));
		}

		if(idsPontoConsumo != null) {
			criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idsPontoConsumo));
		}

		boolean paginacaoPadrao = false;
		if(filtro.containsKey("colecaoPaginada")) {

			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if(StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if(paginacaoPadrao) {
			criteria.addOrder(Order.asc("descricao"));
		}

		return criteria.list();
	}

	/**
	 * Obter cliente.
	 * 
	 * @param idCliente
	 *            the id cliente
	 * @return the cliente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Cliente obterCliente(Long idCliente) throws NegocioException {

		Criteria criteria = this.createCriteria(Cliente.class);
		criteria.add(Restrictions.idEq(idCliente));

		return (Cliente) criteria.uniqueResult();
	}

	/**
	 * Obter imovel.
	 * 
	 * @param idImovel
	 *            the id imovel
	 * @return the imovel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Imovel obterImovel(Long idImovel) throws NegocioException {

		Criteria criteria = this.createCriteria(Imovel.class);
		criteria.add(Restrictions.idEq(idImovel));

		return (Imovel) criteria.uniqueResult();
	}

}
