/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe AbaImpl representa uma AbaImpl no sistema.
 *
 * @since 17/12/2009
 * 
 */

package br.com.ggas.relatorio.impl;

import java.util.Map;

import br.com.ggas.relatorio.RelatorioToken;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe responsavel por gerenciar os layouts dos contratos
 *
 */
class RelatorioTokenImpl extends EntidadeNegocioImpl implements RelatorioToken {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -39567332876089363L;

	/**
	 * descricao do token
	 */
	private String descricao;

	/**
	 * valor do token
	 */
	private String valor;

	/**
	 * alias do token
	 */
	private String alias;
	
	private EntidadeConteudo tipoRelatorio;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.RelatorioToken#
	 * getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.RelatorioToken#
	 * setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.RelatorioToken#
	 * setValor(java.lang.String)
	 */
	@Override
	public void setValor(String valor) {
		this.valor = valor;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.RelatorioToken#
	 * getValor()
	 */
	@Override
	public String getValor() {
		return this.valor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.RelatorioToken#
	 * setAlias(java.lang.String)
	 */
	@Override
	public void setAlias(String alias) {
		this.alias = alias;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.impl.RelatorioToken#
	 * getAlias()
	 */
	@Override
	public String getAlias() {
		return this.alias;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public void setTipoRelatorio(EntidadeConteudo tipoRelatorio) {
		this.tipoRelatorio = tipoRelatorio;
	}

	@Override
	public EntidadeConteudo getTipoRelatorio() {
		return this.tipoRelatorio;
	}

}
