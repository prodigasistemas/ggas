
package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.tarifa.ControladorTarifa;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioVolumesFaturadosSegmento;
import br.com.ggas.relatorio.RelatorioVolumesFaturadosVO;
import br.com.ggas.relatorio.SubRelatorioVolumesFaturadosVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.volumesfaturadossegmento.RelatorioVolumesFaturadosSegmentoAction;
/**
 * Classe responsável pelas operações referentes aos 
 * VolumesFaturadosSegmento
 */
public class ControladorRelatorioVolumesFaturadosSegmentoImpl 
	extends ControladorNegocioImpl implements ControladorRelatorioVolumesFaturadosSegmento {

	public static final String TIPO_RELATORIO = "tipoExibicao";

	private static final String PARAM_SEGMENTO = "descricaoSegmento";

	private static final String IMAGEM = "imagem";

	private static final String RELATORIO_VOLUMES_FATURADOS_SEGMENTO = "relatorioVolumesFaturadosSegmento.jasper";

	private static final Object FORMATO_IMPRESSAO = "formatoImpressao";
	
	private static final String EXIBIR_FILTROS = "exibirFiltros";

	public static final String EXIBIR_VALORES_NULOS = "exibirValoresNulos";

	public static final String SOMA_VALORES_GAS = "SOMA_VALORES_GAS";
	
	public static final String SOMA_VOLUME_GAS = "SOMA_VOLUME_GAS";
	

	public ControladorFatura getControladorFatura() {

		return (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
	}

	public ControladorParametroSistema getControladorParametroSistema() {

		return (ControladorParametroSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
	}

	public ControladorConstanteSistema getControladorControladorConstanteSistema() {

		return (ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
	}

	public ControladorEntidadeConteudo getControladorEntidadeConteudo() {

		return (ControladorEntidadeConteudo) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
	}

	private ControladorSegmento getControladorSegmento() {

		return (ControladorSegmento) ServiceLocator.getInstancia().getControladorNegocio(ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioVolumesFaturadosSegmento#consultar(java.util.Map)
	 */
	@Override
	public byte[] consultar(Map<String, Object> filtro) throws GGASException {

		Map<String, Object> parametros = new HashMap<String, Object>();

		validarFiltrosObrigatorios(filtro, parametros);

		Collection<RelatorioVolumesFaturadosVO> dados = consultarVolumesFaturadosSegmento(filtro);
		if (dados == null || dados.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NAO_EXISTEM_DADOS_FILTRO_INFORMADO);
		}

		// Trata o Formato de Impressão
		FormatoImpressao formatoImpressao = null;
		if (filtro.containsKey(FORMATO_IMPRESSAO)) {
			formatoImpressao = (FormatoImpressao) filtro.get(FORMATO_IMPRESSAO);
		} else {
			throw new NegocioException("Não foi escolhido um formato para o relatório.");
		}

		// Tipo Exibição Relatório
		String tipoExibicao = (String) filtro.get(TIPO_RELATORIO);

		parametros.put(TIPO_RELATORIO, tipoExibicao);

		// Exibir Filtros

		Boolean filtros = (Boolean) filtro.get(EXIBIR_FILTROS);

		parametros.put(EXIBIR_FILTROS, filtros);

		// Logotipo Empresa
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Empresa cdlEmpresa = controladorEmpresa.obterEmpresaPrincipal();
		if (cdlEmpresa.getLogoEmpresa() != null) {
			parametros.put(IMAGEM, Constantes.URL_LOGOMARCA_EMPRESA + cdlEmpresa.getChavePrimaria());
		}

		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_VOLUMES_FATURADOS_SEGMENTO, formatoImpressao);

	}

	/**
	 * Consultar volumes faturados segmento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Collection<RelatorioVolumesFaturadosVO> consultarVolumesFaturadosSegmento(Map<String, Object> filtro) throws NegocioException {

		ControladorFatura controladorFatura = this.getControladorFatura();

		Collection<Fatura> colecaoFaturas = controladorFatura.consultarFatura(filtro);

		Map<HistoricoConsumo, Collection<Fatura>> mapaHistConsFatura = controladorFatura.agruparFaturasPorHistoricoConsumo(colecaoFaturas);

		BigDecimal somaVolumes = BigDecimal.ZERO;
		BigDecimal somaValoresGas = BigDecimal.ZERO;
		this.getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = this.getControladorControladorConstanteSistema();

		String chaveGas = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS);

		ControladorEntidadeConteudo controladorEntidadeConteudo = this.getControladorEntidadeConteudo();

		EntidadeConteudo itemFaturaGas = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(chaveGas));

		Map<Segmento, Collection<SubRelatorioVolumesFaturadosVO>> mapaSegmento = 
						new HashMap<Segmento, Collection<SubRelatorioVolumesFaturadosVO>>();

		Calendar.getInstance().getTime();
		Set<Long> clientesFaturados = new HashSet<Long>();
		Set<Long> clientesAtivos = new HashSet<Long>();
		Set<Long> clientesSemConsumo = new HashSet<Long>();

		for (Entry<HistoricoConsumo, Collection<Fatura>> entry : mapaHistConsFatura.entrySet()) {

			Collection<Fatura> listaFaturas = entry.getValue();

			BigDecimal qtdDiasPeriodo = BigDecimal.valueOf(entry.getKey().getDiasConsumo());

			EntidadeConteudo tipoOperacaoEntrada = controladorEntidadeConteudo.obterTipoOperacaoDocumentoFiscal(Long
							.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_ENTRADA)));

			for (Fatura fatura : listaFaturas) {
				DocumentoFiscal documentoFiscalEntrada = obtemDocumentoFiscal(controladorFatura, tipoOperacaoEntrada, fatura);
				if (documentoFiscalEntrada == null) {

					SubRelatorioVolumesFaturadosVO subRelVO = new SubRelatorioVolumesFaturadosVO();

					if ("Ativo".equalsIgnoreCase(fatura.getContratoAtual().getSituacao().getDescricao())) {
						subRelVO.setIdClienteAtivos(fatura.getCliente().getChavePrimaria());
					}

					subRelVO.setIdCliente(fatura.getCliente().getChavePrimaria());
					if (fatura.getPontoConsumo() != null) {

						if (fatura.getPontoConsumo().getImovel() != null
										&& (!StringUtils.isEmpty(fatura.getPontoConsumo().getImovel().getNome()) || !StringUtils
														.isEmpty(fatura.getPontoConsumo().getDescricao()))) {
							subRelVO.setNomeFantasia(fatura.getPontoConsumo().getImovel().getNome()
											.concat(" - ".concat(fatura.getPontoConsumo().getDescricao())));
						}

						if (!StringUtils.isEmpty(fatura.getPontoConsumo().getEnderecoFormatado())) {
							subRelVO.setEndereco(fatura.getPontoConsumo().getEnderecoFormatado());
						}

						if (fatura.getPontoConsumo().getCep() != null && fatura.getPontoConsumo().getCep().getMunicipio() != null) {
							subRelVO.setCidade(fatura.getPontoConsumo().getCep().getNomeMunicipio());
						}

						ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator
										.getInstancia().getControladorNegocio(
														ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

						controladorHistoricoMedicao.obterPrimeiroHistoricoMedicao(fatura.getPontoConsumo().getChavePrimaria());

						ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia()
										.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
						ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(fatura
										.getPontoConsumo());

						if (contratoPontoConsumo != null) {
							subRelVO.setDataAssinaturaContrato(contratoPontoConsumo.getContrato().getDataAssinatura());
							contratoPontoConsumo.getContrato().getDataVencimentoObrigacoes();

						}

						if ((contratoPontoConsumo != null) && (contratoPontoConsumo.getPeriodicidade().getQuantidadeDias() != null)) {
							contratoPontoConsumo.getPeriodicidade().getQuantidadeDias();
						}
						if (fatura.getCliente() != null) {
							subRelVO.setNomeCliente(fatura.getCliente().getNome());
							if (!StringUtils.isEmpty(fatura.getCliente().getNomeFantasia()) && fatura.getPontoConsumo() != null) {

								String nome = fatura.getPontoConsumo().getImovel().getNome();
								String descricao = fatura.getPontoConsumo().getDescricao();
								StringBuilder descricaoFormatada = new StringBuilder();
								String separador = "-";

								if (nome != null) {
									descricaoFormatada.append(nome);
									descricaoFormatada.append(separador);
								}
								if (descricao != null) {
									descricaoFormatada.append(descricao);
								}
								subRelVO.setRazaoSocial(descricaoFormatada.toString());
							}
						}

						Map<String, BigDecimal> mapaSomaGas = populaSubRelVOComDadosFaturaItem(controladorFatura, somaVolumes,
								somaValoresGas, itemFaturaGas, qtdDiasPeriodo, fatura, subRelVO);
						
						somaValoresGas = mapaSomaGas.get(SOMA_VALORES_GAS);
						somaVolumes = mapaSomaGas.get(SOMA_VOLUME_GAS);
						
						Collection<SubRelatorioVolumesFaturadosVO> lista = null;
						if (mapaSegmento.containsKey(fatura.getSegmento())) {
							lista = mapaSegmento.get(fatura.getSegmento());
						} else {
							lista = new ArrayList<SubRelatorioVolumesFaturadosVO>();
						}
						lista.add(subRelVO);
						Util.ordenarColecaoPorAtributo(lista, "nomeFantasia", true);
						mapaSegmento.put(fatura.getSegmento(), lista);

					}
				}

			}
		}

		BigDecimal totalMetroCubicoMediaDiaria = BigDecimal.ZERO;
		BigDecimal totalMetroCubicoPeriodo = BigDecimal.ZERO;
		BigDecimal totalPercentualVolume = BigDecimal.ZERO;
		BigDecimal totalTarifaMediaGas = BigDecimal.ZERO;
		BigDecimal totalPercentualFaturamentoGas = BigDecimal.ZERO;

		Collection<RelatorioVolumesFaturadosVO> retorno = new ArrayList<RelatorioVolumesFaturadosVO>();

		for (Entry<Segmento, Collection<SubRelatorioVolumesFaturadosVO>> entry : mapaSegmento.entrySet()) {
			Segmento segmento = entry.getKey();
			RelatorioVolumesFaturadosVO relVolFaturado = new RelatorioVolumesFaturadosVO();

			relVolFaturado.setSegmento(segmento.getDescricao());

			String tipo = (String) filtro.get(TIPO_RELATORIO);
			relVolFaturado.setTipoExibicao(tipo);
			Collection<SubRelatorioVolumesFaturadosVO> listaSubRel = entry.getValue();
			for (SubRelatorioVolumesFaturadosVO sub : listaSubRel) {
				clientesFaturados.add(sub.getIdCliente());
				clientesAtivos.add(sub.getIdClienteAtivos());
				clientesSemConsumo.add(sub.getIdClienteSemConsumo());
			}

			Integer quantidadeClientesFatu = clientesFaturados.size();
			Integer quantidadeCliAtivos = clientesAtivos.size();
			Integer quantidadeClieSemFaturamento = null;
			if (clientesSemConsumo.size() == 1) {
				for (Long id : clientesSemConsumo) {
					if (id == null) {
						quantidadeClieSemFaturamento = 0;
					}
				}
			} else {
				quantidadeClieSemFaturamento = clientesSemConsumo.size();
			}
			for (SubRelatorioVolumesFaturadosVO subRel : listaSubRel) {

				relVolFaturado.setQuantidadeClientesFaturados(quantidadeClientesFatu);
				relVolFaturado.setQuantidadeClientesAtivos(quantidadeCliAtivos);
				relVolFaturado.setClientesSemConsumo(quantidadeClieSemFaturamento);
				if (subRel.getMetroCubicoMediaDiaria() != null) {
					totalMetroCubicoMediaDiaria = totalMetroCubicoMediaDiaria.add(subRel.getMetroCubicoMediaDiaria());
					relVolFaturado.setTotalMetroCubicoMediaDiaria(totalMetroCubicoMediaDiaria);
				}
				if (subRel.getMetroCubicoPeriodo() != null) {
					totalMetroCubicoPeriodo = totalMetroCubicoPeriodo.add(subRel.getMetroCubicoPeriodo());
					relVolFaturado.setTotalMetroCubicoPeriodo(totalMetroCubicoPeriodo);

					if (totalMetroCubicoPeriodo.compareTo(BigDecimal.ZERO) > 0) {
						subRel.setPercentualVolume(subRel.getMetroCubicoPeriodo().divide(somaVolumes, 8, RoundingMode.HALF_UP)
										.multiply(BigDecimal.valueOf(100)).setScale(2, RoundingMode.HALF_UP));
					} else {
						subRel.setPercentualVolume(BigDecimal.ZERO.setScale(2));
					}
				}
				if (subRel.getPercentualVolume() != null) {
					totalPercentualVolume = totalPercentualVolume.add(subRel.getPercentualVolume());
				}

				if (subRel.getTarifaMediaGas() != null) {
					totalTarifaMediaGas = totalTarifaMediaGas.add(subRel.getTarifaMediaGas());
					relVolFaturado.setTotalTarifaMediaGas(totalTarifaMediaGas);

					if (somaValoresGas.compareTo(BigDecimal.ZERO) > 0) {
						subRel.setPercentualFaturamentoGas(subRel.getValorMediaGas().divide(somaValoresGas, 8, RoundingMode.HALF_UP)
										.multiply(BigDecimal.valueOf(100)).setScale(2, RoundingMode.HALF_UP));
					} else {
						subRel.setPercentualFaturamentoGas(BigDecimal.ZERO);

					}
				}

				if (subRel.getPercentualFaturamentoGas() != null) {
					totalPercentualFaturamentoGas = totalPercentualFaturamentoGas.add(subRel.getPercentualFaturamentoGas());
				}

			}
			relVolFaturado.setColecaoVOs(listaSubRel);
			retorno.add(relVolFaturado);
			totalMetroCubicoMediaDiaria = BigDecimal.ZERO;
			totalMetroCubicoPeriodo = BigDecimal.ZERO;
			totalPercentualVolume = BigDecimal.ZERO;
			totalTarifaMediaGas = BigDecimal.ZERO;
			totalPercentualFaturamentoGas = BigDecimal.ZERO;
		}

		return retorno;
	}

	private Map<String, BigDecimal> populaSubRelVOComDadosFaturaItem(ControladorFatura controladorFatura, BigDecimal somaVolumes,
			BigDecimal somaValoresGas, EntidadeConteudo itemFaturaGas, BigDecimal qtdDiasPeriodo, Fatura fatura,
			SubRelatorioVolumesFaturadosVO subRelVO) {
		Map<String, BigDecimal> mapaSomaGas;
		if(controladorFatura.isFaturaAgrupada(fatura)){
			mapaSomaGas = populaSubRelVOComDadosFaturaItem(somaValoresGas, itemFaturaGas,
					qtdDiasPeriodo, fatura.getFaturaAgrupada(), subRelVO, somaVolumes);
		}else{
			mapaSomaGas = populaSubRelVOComDadosFaturaItem(somaValoresGas, itemFaturaGas,
					qtdDiasPeriodo, fatura, subRelVO, somaVolumes);
		}
		return mapaSomaGas;
	}

	private Map <String, BigDecimal> populaSubRelVOComDadosFaturaItem(BigDecimal somaValoresGas, EntidadeConteudo itemFaturaGas,
			BigDecimal qtdDiasPeriodo, Fatura fatura, SubRelatorioVolumesFaturadosVO subRelVO, BigDecimal somaVolumes) {
		BigDecimal volumes = somaVolumes;
		BigDecimal valores = somaValoresGas;
		if (fatura.getListaFaturaItem() != null && !fatura.getListaFaturaItem().isEmpty()) {
			FaturaItem faturaItemUnico = fatura.getListaFaturaItem().iterator().next();
			BigDecimal quantidadeMetroCubico = BigDecimal.ZERO;
			quantidadeMetroCubico = faturaItemUnico.getQuantidade().setScale(0, BigDecimal.ROUND_HALF_UP);

			subRelVO.setMetroCubicoPeriodo(quantidadeMetroCubico);
			volumes = somaVolumes.add(quantidadeMetroCubico);

			if (qtdDiasPeriodo.compareTo(BigDecimal.ZERO) > 0) {
				subRelVO.setMetroCubicoMediaDiaria(quantidadeMetroCubico.divide(qtdDiasPeriodo, RoundingMode.HALF_UP));
			}
			for (FaturaItem faturaItem : fatura.getListaFaturaItem()) {
				if (faturaItem.getRubrica() != null && faturaItem.getRubrica().getItemFatura() != null
								&& faturaItem.getRubrica().getItemFatura().equals(itemFaturaGas)) {

					subRelVO.setTarifaMediaGas(faturaItem.getValorUnitario());
					subRelVO.setValorMediaGas(faturaItem.getValorTotal());
					valores = somaValoresGas.add(faturaItem.getValorTotal());
				}
			}
			if (somaValoresGas.compareTo(BigDecimal.ZERO) <= 0) {
				subRelVO.setIdClienteSemConsumo(fatura.getCliente().getChavePrimaria());
			}
		}
		
		Map <String, BigDecimal> mapaSomaGas = new HashMap<>();
		mapaSomaGas.put(SOMA_VOLUME_GAS, volumes);
		mapaSomaGas.put(SOMA_VALORES_GAS, valores);
		
		return mapaSomaGas;
	}

	private DocumentoFiscal obtemDocumentoFiscal(ControladorFatura controladorFatura, EntidadeConteudo tipoOperacaoEntrada, Fatura fatura)
			throws NegocioException {
		DocumentoFiscal documentoFiscalEntrada = null;
		if(controladorFatura.isFaturaAgrupada(fatura)){
			documentoFiscalEntrada = controladorFatura.obterDocumentoFiscalPorFatura(fatura.getFaturaAgrupada(), tipoOperacaoEntrada);
		}else{
			documentoFiscalEntrada = controladorFatura.obterDocumentoFiscalPorFatura(fatura, tipoOperacaoEntrada);
		}
		return documentoFiscalEntrada;
	}

	/**
	 * Validar filtros obrigatorios.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param parametros
	 *            the parametros
	 * @param segmento
	 *            the segmento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarFiltrosObrigatorios(Map<String, Object> filtro, Map<String, Object> parametros)
					throws NegocioException {

		if (filtro.containsKey(RelatorioVolumesFaturadosSegmentoAction.IDS_SEGMENTOS)
						&& filtro.get(RelatorioVolumesFaturadosSegmentoAction.IDS_SEGMENTOS) != null) {
			ControladorSegmento controlSegmento = getControladorSegmento();
			Long[] chaves = (Long[]) filtro.get(RelatorioVolumesFaturadosSegmentoAction.IDS_SEGMENTOS);
			if (chaves != null && chaves.length > 0) {
				StringBuilder seg = new StringBuilder();
				for (Long idSegmento : chaves) {
					Segmento segmento = (Segmento) controlSegmento.obter(idSegmento);
					seg.append(segmento.getDescricao()).append(", ");
				}
				parametros.put(PARAM_SEGMENTO, seg.toString().substring(0, seg.toString().length() - 2));
			}
		} else {
			throw new NegocioException(Constantes.ERRO_SELECIONE_ALGUM_SEGMENTO, true);
		}

		boolean periodoValido = false;
		Date dataInicial = null;
		if (filtro.containsKey("dataEmissaoInicial")) {
			dataInicial = (Date) filtro.get("dataEmissaoInicial");
			Date dataFinal = null;
			if (filtro.containsKey("dataEmissaoFinal")) {
				dataFinal = (Date) filtro.get("dataEmissaoFinal");
				if (Util.compararDatas(dataInicial, dataFinal) > 0) {
					throw new NegocioException(ControladorTarifa.ERRO_NEGOCIO_DATA_INICIAL_MAIOR_TARIFA_CAD, dataFinal);
				} else {
					parametros.put(RelatorioVolumesFaturadosSegmentoAction.PARAM_DATA_EMISSAO_INICIAL, 
							DataUtil.converterDataParaString(dataInicial));
					parametros.put(RelatorioVolumesFaturadosSegmentoAction.PARAM_DATA_EMISSAO_FINAL, 
							DataUtil.converterDataParaString(dataFinal));
					periodoValido = true;
				}
			} else {
				throw new NegocioException(Constantes.ERRO_INFORME_DATA_FINAL, true);
			}
		} else {
			if (filtro.containsKey("dataEmissaoFinal")) {
				throw new NegocioException(Constantes.ERRO_INFORME_DATA_INICIAL, true);
			}
		}

		boolean anoMesReferenciaNumeroCicloValido = false;
		Integer anoMesReferencia = (Integer) filtro.get(RelatorioVolumesFaturadosSegmentoAction.ANO_MES_REFERENCIA);
		if (anoMesReferencia != null) {
			anoMesReferenciaNumeroCicloValido = true;
			Integer numeroCiclo = (Integer) filtro.get(RelatorioVolumesFaturadosSegmentoAction.NUMERO_CICLO);
			parametros.put(RelatorioVolumesFaturadosSegmentoAction.PARAM_ANO_MES_NUMERO_CICLO,
							Util.converterAnoMesNumeroCicloParaString(anoMesReferencia, numeroCiclo));
		}

		if (!periodoValido && !anoMesReferenciaNumeroCicloValido) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SERVICO_TIPO_PRIORIDADE_RELACIONADO, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

}
