/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 05/12/2013 16:07:22
 @author ccavalcanti
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.DebitosACobrar;
import br.com.ggas.arrecadacao.recebimento.ControladorRecebimento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.relatorio.ControladorRelatorioTituloAbertoPorDataVencimento;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.tituloemabertopordatavencimento.RelatorioTitulosAbertoPorDataVencimentoVO;
import br.com.ggas.web.relatorio.tituloemabertopordatavencimento.SubRelatorioTitulosAbertoPorDataVencimentoVO;
/**
 *
 * Classe responsável pelas operações referentes aos
 * TituloAbertoPorDataVencimento
 */
public class ControladorRelatorioTituloAbertoPorDataVencimentoImpl extends ControladorNegocioImpl
	implements ControladorRelatorioTituloAbertoPorDataVencimento {

	private static final String RELATORIO_TITULO_ABERTO_POR_DATA_VENCIMENTO_ANALITICO =
					"relatorioTituloAbertoPorDataVencimentoAnalitico.jasper";

	private static final String RELATORIO_TITULO_ABERTO_POR_DATA_VENCIMENTO_SINTETICO =
					"relatorioTituloAbertoPorDataVencimentoSintetico.jasper";

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_IMOVEL = "idImovel";

	private static final String CHAVES_PONTO_CONSUMO = "chavesPontoConsumo";

	private static final String DATA_EMISSAO_INICIAL = "dataEmissaoInicial";

	private static final String DATA_EMISSAO_FINAL = "dataEmissaoFinal";

	private static final String DATA_VENCIMENTO_INICIAL = "dataVencimentoInicial";

	private static final String DATA_VENCIMENTO_FINAL = "dataVencimentoFinal";

	private static final String TIPO_EXIBICAO = "tipoExibicao";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String ERRO_NEGOCIO_DATA_EMISSAO_INICIO_MAIOR_DATA_EMISSAO_FINAL =
					"ERRO_NEGOCIO_DATA_EMISSAO_INICIO_MAIOR_DATA_EMISSAO_FINAL";

	private static final String ERRO_NEGOCIO_DATA_VENCIMENTO_INICIO_DATA_VENCIMENTO_FINAL =
					"ERRO_NEGOCIO_DATA_VENCIMENTO_INICIO_DATA_VENCIMENTO_FINAL";

	private static final String ERRO_NEGOCIO_RELATORIO_SEM_DADOS = "ERRO_NEGOCIO_RELATORIO_SEM_DADOS";

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.relatorio.ControladorRelatorioTituloAbertoPorDataVencimento#gerarRelatorioTituloAbertoPorDataVencimento(java.util.Map,
	 * br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorioTituloAbertoPorDataVencimento(Map<String, Object> filtro, FormatoImpressao formatoImpressao)
					throws GGASException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		Map<String, Object> parametros = new HashMap<String, Object>();
		if (controladorEmpresa.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem",
							Constantes.URL_LOGOMARCA_EMPRESA
							+ String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		}

		this.montarParametroRelatorio(parametros, filtro);

		configurarFaturasPendentes(filtro, controladorConstanteSistema);

		Collection<RelatorioTitulosAbertoPorDataVencimentoVO> listaRelatorioTituloAbertoPorDataVencimentoVO = this
						.montarListaDadosRelatorio(filtro);

		byte[] relatorioTituloAbertoPorDataVencimento = null;

		if ("analitico".equals(filtro.get(TIPO_EXIBICAO))) {

			relatorioTituloAbertoPorDataVencimento = RelatorioUtil.gerarRelatorio(listaRelatorioTituloAbertoPorDataVencimentoVO,
							parametros, RELATORIO_TITULO_ABERTO_POR_DATA_VENCIMENTO_ANALITICO, formatoImpressao);

		} else if ("sintetico".equals(filtro.get(TIPO_EXIBICAO))) {

			relatorioTituloAbertoPorDataVencimento = RelatorioUtil.gerarRelatorio(listaRelatorioTituloAbertoPorDataVencimentoVO,
							parametros, RELATORIO_TITULO_ABERTO_POR_DATA_VENCIMENTO_SINTETICO, formatoImpressao);
		}

		return relatorioTituloAbertoPorDataVencimento;
	}

	/**
	 * Configurar faturas pendentes.
	 *
	 * @param filtro
	 *            the filtro
	 * @param controladorConstanteSistema
	 *            the controlador constante sistema
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	private void configurarFaturasPendentes(Map<String, Object> filtro, ControladorConstanteSistema controladorConstanteSistema)
					throws FormatoInvalidoException {

		String codigoSituacaoPagamentoPendente = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);
		String codigoSituacaoPagamentoParcialmentePago = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PARCIALMENTE_PAGO);

		Long idCodigoSituacaoPendente = Util.converterCampoStringParaValorLong("Recebimento Pendente", codigoSituacaoPagamentoPendente);
		Long idCodigoSituacaoParcialmentePago = Util.converterCampoStringParaValorLong("Recebimento Pago Parcialmente",
						codigoSituacaoPagamentoParcialmentePago);

		List<Long> idsSituacao = new ArrayList<Long>();
		idsSituacao.add(idCodigoSituacaoParcialmentePago);
		idsSituacao.add(idCodigoSituacaoPendente);
		Long[] ids = new Long[idsSituacao.size()];
		idsSituacao.toArray(ids);
		filtro.put("situacaoPagamento", ids);

	}

	/**
	 * Montar parametro relatorio.
	 *
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void montarParametroRelatorio(Map<String, Object> parametros, Map<String, Object> filtro) throws NegocioException {

		if (filtro != null) {
			Long idCliente = (Long) filtro.get(ID_CLIENTE);
			if ((idCliente != null) && (idCliente > 0)) {
				ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getBeanPorID(
								ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
				Cliente cliente = (Cliente) controladorCliente.obter(idCliente);
				parametros.put("nomeCliente", cliente.getNome());
			}

			Long idImovel = (Long) filtro.get(ID_IMOVEL);
			if ((idImovel != null) && (idImovel > 0)) {
				ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia().getBeanPorID(
								ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
				Imovel imovel = (Imovel) controladorImovel.obter(idImovel);
				parametros.put("nomeImovel", imovel.getNome());
			}

			Long[] chavesPontoConsumo = (Long[]) filtro.get(CHAVES_PONTO_CONSUMO);
			if ((chavesPontoConsumo != null) && (chavesPontoConsumo.length > 0)) {
				ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getBeanPorID(
								ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

				Map<String, Object> filtroPontoConsumo = new HashMap<String, Object>();
				filtroPontoConsumo.put("chavesPrimarias", chavesPontoConsumo);

				Collection<PontoConsumo> listaPontoConsumo = controladorPontoConsumo.consultarPontosConsumo(filtroPontoConsumo);
				StringBuilder descricaoPontoConsumo = new StringBuilder();
				for (PontoConsumo pontoConsumolocal : listaPontoConsumo) {

					if (!StringUtils.isEmpty(descricaoPontoConsumo.toString())) {
						descricaoPontoConsumo = descricaoPontoConsumo.append(", ");
					}

					descricaoPontoConsumo.append(pontoConsumolocal.getDescricao());
				}

				parametros.put("pontosConsumo", descricaoPontoConsumo.toString());
			}

			Date dataEmissaoInicial = (Date) filtro.get(DATA_EMISSAO_INICIAL);
			Date dataEmissaoFinal = (Date) filtro.get(DATA_EMISSAO_FINAL);
			if ((dataEmissaoInicial != null) && (dataEmissaoFinal != null)) {
				parametros.put(DATA_EMISSAO_INICIAL, DataUtil.converterDataParaString(dataEmissaoInicial));
				parametros.put(DATA_EMISSAO_FINAL, DataUtil.converterDataParaString(dataEmissaoFinal));
			}

			Date dataVencimentoInicial = (Date) filtro.get(DATA_VENCIMENTO_INICIAL);
			Date dataVencimentoFinal = (Date) filtro.get(DATA_VENCIMENTO_FINAL);
			if ((dataVencimentoInicial != null) && (dataVencimentoFinal != null)) {
				parametros.put(DATA_VENCIMENTO_INICIAL, DataUtil.converterDataParaString(dataVencimentoInicial));
				parametros.put(DATA_VENCIMENTO_FINAL, DataUtil.converterDataParaString(dataVencimentoFinal));
			}

			String situacaoDebito = (String) filtro.get("situacaoDebito");
			if (situacaoDebito != null) {
				if ("true".equals(situacaoDebito)) {

					parametros.put("situacao", "À Vencer");
				} else if ("false".equals(situacaoDebito)) {

					parametros.put("situacao", "Vencida");
				} else {

					parametros.put("situacao", "Todos");
					filtro.put("situacaoDebito", null);
				}
			}

			Boolean exibirFiltrosRelatorio = (Boolean) filtro.get(EXIBIR_FILTROS);
			parametros.put("exibirFiltros", exibirFiltrosRelatorio);
		}
	}

	/**
	 * Validar filtros.
	 *
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarFiltros(Map<String, Object> filtro) throws NegocioException {

		Date dataEmissaoInicial = (Date) filtro.get(DATA_EMISSAO_INICIAL);
		Date dataEmissaoFinal = (Date) filtro.get(DATA_EMISSAO_FINAL);
		if (dataEmissaoInicial != null && dataEmissaoFinal != null && Util.compararDatas(dataEmissaoInicial, dataEmissaoFinal) > 0) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_EMISSAO_INICIO_MAIOR_DATA_EMISSAO_FINAL, true);
		}

		Date dataVencimentoInicial = (Date) filtro.get(DATA_VENCIMENTO_INICIAL);
		Date dataVencimentoFinal = (Date) filtro.get(DATA_VENCIMENTO_FINAL);
		if (dataVencimentoInicial != null && dataVencimentoFinal != null
						&& Util.compararDatas(dataVencimentoInicial, dataVencimentoFinal) > 0) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_VENCIMENTO_INICIO_DATA_VENCIMENTO_FINAL, true);
		}

	}

	/**
	 * Montar lista dados relatorio.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<RelatorioTitulosAbertoPorDataVencimentoVO> montarListaDadosRelatorio(Map<String, Object> filtro)
					throws GGASException {

		List<SubRelatorioTitulosAbertoPorDataVencimentoVO> listaSubRelatorioTituloAbertoPorDataVencimentoVO = null;

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorRecebimento controladorRecebimento = (ControladorRecebimento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRecebimento.BEAN_ID_CONTROLADOR_RECEBIMENTO);

		this.validarFiltros(filtro);

		Collection<Fatura> colecaoFaturas = controladorFatura.consultarFatura(filtro);
		List<RelatorioTitulosAbertoPorDataVencimentoVO> listaDataVencimentoFaturaVO = new ArrayList<RelatorioTitulosAbertoPorDataVencimentoVO>();
		Map<String, List<SubRelatorioTitulosAbertoPorDataVencimentoVO>> mapaSubRelatorioDataVencimentoFaturaVO =
						new LinkedHashMap<String, List<SubRelatorioTitulosAbertoPorDataVencimentoVO>>();

		if (colecaoFaturas != null && !colecaoFaturas.isEmpty()) {

			Map<String, Collection<Fatura>> mapaDataVencimentoFatura = controladorFatura.agruparFaturasPorDataVencimento(colecaoFaturas);

			for (Map.Entry<String, Collection<Fatura>> entry : mapaDataVencimentoFatura.entrySet()) {

				String dataVencimento = entry.getKey();
				Collection<Fatura> listaFaturas = entry.getValue();

				listaSubRelatorioTituloAbertoPorDataVencimentoVO = new ArrayList<SubRelatorioTitulosAbertoPorDataVencimentoVO>();

				for (Fatura fatura : listaFaturas) {

					SubRelatorioTitulosAbertoPorDataVencimentoVO subRelatorioTituloAbertoPorDataVencimentoVO =
									new SubRelatorioTitulosAbertoPorDataVencimentoVO();

					if (fatura.getCliente() != null && fatura.getCliente().getNome() != null) {
						subRelatorioTituloAbertoPorDataVencimentoVO.setNomeCliente(String.valueOf(fatura.getCliente().getNome()));
					}

					if ("analitico".equals(filtro.get(TIPO_EXIBICAO)) && fatura.getContratoAtual() != null) {
						subRelatorioTituloAbertoPorDataVencimentoVO.setNumeroContrato(fatura.getContratoAtual().getNumeroFormatado());
					}

					if (fatura.getChavePrimaria() > 0) {
						subRelatorioTituloAbertoPorDataVencimentoVO.setNumeroTitulo(String.valueOf(fatura.getChavePrimaria()));
					}

					DocumentoFiscal documentoFiscal = controladorFatura.obterDocumentoFiscalPorFaturaMaisRecente(fatura.getChavePrimaria());
					if ("analitico".equals(filtro.get(TIPO_EXIBICAO)) && documentoFiscal != null) {
						subRelatorioTituloAbertoPorDataVencimentoVO.setNumeroNotaFiscal(String.valueOf(documentoFiscal.getNumero()));
					}

					if ("analitico".equals(filtro.get(TIPO_EXIBICAO)) && fatura.getTipoDocumento().getDescricao() != null) {
						subRelatorioTituloAbertoPorDataVencimentoVO.setTipoDocumento(fatura.getTipoDocumento().getDescricao());
					}

					if ("analitico".equals(filtro.get(TIPO_EXIBICAO))) {

						String situacaoDebito = null;

						if (DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime()).compareTo(fatura.getDataVencimento()) <= 0) {

							situacaoDebito = "À vencer";
						} else {

							situacaoDebito = "Vencida";
						}
						subRelatorioTituloAbertoPorDataVencimentoVO.setSituacao(situacaoDebito);
					}

					if (fatura.getDataEmissaoFormatada() != null) {

						subRelatorioTituloAbertoPorDataVencimentoVO.setDataEmissao(fatura.getDataEmissaoFormatada());
					}

					filtro.put("idFaturaGeral", fatura.getChavePrimaria());
					filtro.put("isConsultarQuantidadeDiasAtraso", Boolean.TRUE);

					Collection<SubRelatorioTitulosAbertoPorDataVencimentoVO> listaqtdDiasAtrasoValorDataRecebimento = controladorRecebimento
									.consultarQtdDiasAtrasoRecebimento(fatura.getChavePrimaria());

					SubRelatorioTitulosAbertoPorDataVencimentoVO subRelatorioTituloAbertoPorDataVencimentoVOBanco = null;
					Double qtdDiasAtraso = null;

					if (listaqtdDiasAtrasoValorDataRecebimento != null && !listaqtdDiasAtrasoValorDataRecebimento.isEmpty()) {

						subRelatorioTituloAbertoPorDataVencimentoVOBanco = listaqtdDiasAtrasoValorDataRecebimento.iterator().next();

						qtdDiasAtraso = subRelatorioTituloAbertoPorDataVencimentoVOBanco.getQtdDiasAtrasoDouble();

					} else {

						qtdDiasAtraso = (double) Util.intervaloDatas(fatura.getDataVencimento(),
										DataUtil.gerarDataHmsZerados(Calendar.getInstance().getTime()));
					}

					qtdDiasAtraso = this.verificarQtdDiasAtrasoNegativa(qtdDiasAtraso);

					subRelatorioTituloAbertoPorDataVencimentoVO.setQtdDiasAtraso(String.valueOf(qtdDiasAtraso));

					if (fatura.getValorTotal() != null) {

						subRelatorioTituloAbertoPorDataVencimentoVO.setValorDebito(fatura.getValorTotal());
					}

					this.adicionarJurosEMultaNaFatura(fatura, subRelatorioTituloAbertoPorDataVencimentoVOBanco,
									subRelatorioTituloAbertoPorDataVencimentoVO);

					listaSubRelatorioTituloAbertoPorDataVencimentoVO.add(subRelatorioTituloAbertoPorDataVencimentoVO);
				}

				mapaSubRelatorioDataVencimentoFaturaVO.put(dataVencimento, listaSubRelatorioTituloAbertoPorDataVencimentoVO);
			}
		}

		if (mapaSubRelatorioDataVencimentoFaturaVO.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		for (Map.Entry<String, List<SubRelatorioTitulosAbertoPorDataVencimentoVO>> entry : mapaSubRelatorioDataVencimentoFaturaVO
						.entrySet()) {
			String dataVencimento = entry.getKey();
			BigDecimal totalQtdDiasAtraso = BigDecimal.ZERO;
			BigDecimal totalValorDebito = BigDecimal.ZERO;
			BigDecimal totalValorMultaJuros = BigDecimal.ZERO;
			BigDecimal totalValorCorrigido = BigDecimal.ZERO;
			int totalqtdTitulo = 0;

			RelatorioTitulosAbertoPorDataVencimentoVO relatorioTituloAbertoPorDataVencimentoVO = new RelatorioTitulosAbertoPorDataVencimentoVO();
			relatorioTituloAbertoPorDataVencimentoVO.setDataVencimento(dataVencimento);

			List<SubRelatorioTitulosAbertoPorDataVencimentoVO> listaSubRel = entry.getValue();

			for (SubRelatorioTitulosAbertoPorDataVencimentoVO subRelatorioTitulosAbertoPorDataVencimentoVO : listaSubRel) {

				if (subRelatorioTitulosAbertoPorDataVencimentoVO.getQtdDiasAtraso() != null
								&& !"null".equals(subRelatorioTitulosAbertoPorDataVencimentoVO.getQtdDiasAtraso())) {

					BigDecimal qtdDiasAtraso = Util.converterCampoStringParaValorBigDecimal("",
									subRelatorioTitulosAbertoPorDataVencimentoVO.getQtdDiasAtraso(), Constantes.FORMATO_VALOR_BR,
									Constantes.LOCALE_PADRAO);

					totalQtdDiasAtraso = totalQtdDiasAtraso.add(qtdDiasAtraso);
					subRelatorioTitulosAbertoPorDataVencimentoVO.setTotalQtdDiasAtraso(String.valueOf(totalQtdDiasAtraso));

				} else {

					subRelatorioTitulosAbertoPorDataVencimentoVO.setQtdDiasAtraso(String.valueOf(0));
				}

				if (subRelatorioTitulosAbertoPorDataVencimentoVO.getValorDebito() != null) {

					totalValorDebito = totalValorDebito.add(subRelatorioTitulosAbertoPorDataVencimentoVO.getValorDebito());
					subRelatorioTitulosAbertoPorDataVencimentoVO.setTotalValorDebito(totalValorDebito);
				}

				if (subRelatorioTitulosAbertoPorDataVencimentoVO.getValorMultaJuros() != null) {

					totalValorMultaJuros = totalValorMultaJuros.add(subRelatorioTitulosAbertoPorDataVencimentoVO.getValorMultaJuros());
					subRelatorioTitulosAbertoPorDataVencimentoVO.setTotalValorMultaJuros(totalValorMultaJuros);
				}

				if (subRelatorioTitulosAbertoPorDataVencimentoVO.getValorCorrigido() != null) {

					totalValorCorrigido = totalValorCorrigido.add(subRelatorioTitulosAbertoPorDataVencimentoVO.getValorCorrigido());
					subRelatorioTitulosAbertoPorDataVencimentoVO.setTotalValorCorrigido(totalValorCorrigido);
				}

				totalqtdTitulo = 1 + totalqtdTitulo;
				subRelatorioTitulosAbertoPorDataVencimentoVO.setTotalqtdTitulo(String.valueOf(totalqtdTitulo));

			}

			relatorioTituloAbertoPorDataVencimentoVO.setColecaoVOs(listaSubRel);
			listaDataVencimentoFaturaVO.add(relatorioTituloAbertoPorDataVencimentoVO);

		}

		return listaDataVencimentoFaturaVO;
	}

	/**
	 * Verificar qtd dias atraso negativa.
	 *
	 * @param qtdDiasAtraso
	 *            the qtd dias atraso
	 * @return the double
	 */
	private Double verificarQtdDiasAtrasoNegativa(Double qtdDiasAtraso) {

		Double doubleZero = new Double(0.0);

		if (qtdDiasAtraso == null || qtdDiasAtraso.compareTo(doubleZero) < 0) {

			return new Double(0);
		}

		return qtdDiasAtraso;
	}

	/**
	 * Adicionar juros e multa na fatura.
	 *
	 * @param fatura
	 *            the fatura
	 * @param subRelatorioTituloAbertoPorDataVencimentoVOBanco
	 *            the sub relatorio titulo aberto por data vencimento vo banco
	 * @param subRelatorioTituloAbertoPorDataVencimentoVORelatorio
	 *            the sub relatorio titulo aberto por data vencimento vo relatorio
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Map<String, BigDecimal> adicionarJurosEMultaNaFatura(Fatura fatura,
					SubRelatorioTitulosAbertoPorDataVencimentoVO subRelatorioTituloAbertoPorDataVencimentoVOBanco,
					SubRelatorioTitulosAbertoPorDataVencimentoVO subRelatorioTituloAbertoPorDataVencimentoVORelatorio) throws GGASException {

		Map<String, BigDecimal> mapaJurosMultaValorCorrigido = this.calcularValorCorrigidoJurosMulta(fatura,
						subRelatorioTituloAbertoPorDataVencimentoVOBanco);

		subRelatorioTituloAbertoPorDataVencimentoVORelatorio.setValorCorrigido(mapaJurosMultaValorCorrigido.get("valorCorrigido"));

		BigDecimal valorMultaJuros = this.calcularValorMultaJuros(mapaJurosMultaValorCorrigido.get("valorJuros"),
						mapaJurosMultaValorCorrigido.get("valorMulta"));

		subRelatorioTituloAbertoPorDataVencimentoVORelatorio.setValorMultaJuros(valorMultaJuros);

		return mapaJurosMultaValorCorrigido;

	}

	/**
	 * Calcular valor multa juros.
	 *
	 * @param valorJuros
	 *            the valor juros
	 * @param valorMulta
	 *            the valor multa
	 * @return the big decimal
	 */
	private BigDecimal calcularValorMultaJuros(BigDecimal valorJuros, BigDecimal valorMulta) {

		BigDecimal valorMultaJuros = BigDecimal.ZERO;

		if (valorJuros != null) {
			valorMultaJuros = valorMultaJuros.add(valorJuros);
		}

		if (valorMulta != null) {
			valorMultaJuros = valorMultaJuros.add(valorMulta);
		}

		return valorMultaJuros;
	}

	/**
	 * Calcular valor corrigido juros multa.
	 *
	 * @param fatura
	 *            the fatura
	 * @param subRelatorioTituloAbertoPorDataVencimentoVO
	 *            the sub relatorio titulo aberto por data vencimento vo
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Map<String, BigDecimal> calcularValorCorrigidoJurosMulta(Fatura fatura,
					SubRelatorioTitulosAbertoPorDataVencimentoVO subRelatorioTituloAbertoPorDataVencimentoVO) throws GGASException {

		DebitosACobrar debitosACobrarVO;
		BigDecimal valorCorrigido = fatura.getValorTotal();
		BigDecimal valorJuros = null;
		BigDecimal valorMulta = null;
		Map<String, BigDecimal> mapaJurosMultaValorCorrigido = new LinkedHashMap<String, BigDecimal>();

		ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia().getBeanPorID(
						ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);

		Date dataPagamento = null;
		BigDecimal valorPagamento = null;

		if (subRelatorioTituloAbertoPorDataVencimentoVO != null) {

			dataPagamento = subRelatorioTituloAbertoPorDataVencimentoVO.getDataPagamento();
			valorPagamento = subRelatorioTituloAbertoPorDataVencimentoVO.getValorPagamento();
		} else {

			dataPagamento = new Date();
			valorPagamento = BigDecimal.ZERO;
		}

		debitosACobrarVO = controladorArrecadacao.calcularAcrescimentoImpontualidade(fatura, dataPagamento, valorPagamento, true, true);

		if (debitosACobrarVO != null) {

			if (debitosACobrarVO.getJurosMora() != null) {
				valorJuros = fatura.getValorTotal().multiply(debitosACobrarVO.getJurosMora());
				valorCorrigido = valorCorrigido.add(valorJuros);
			}
			if (debitosACobrarVO.getMulta() != null) {
				valorMulta = fatura.getValorTotal().multiply(debitosACobrarVO.getMulta());
				valorCorrigido = valorCorrigido.add(valorMulta);
			}
		}

		mapaJurosMultaValorCorrigido.put("valorMulta", valorMulta);
		mapaJurosMultaValorCorrigido.put("valorJuros", valorJuros);
		mapaJurosMultaValorCorrigido.put("valorCorrigido", valorCorrigido);

		return mapaJurosMultaValorCorrigido;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

}
