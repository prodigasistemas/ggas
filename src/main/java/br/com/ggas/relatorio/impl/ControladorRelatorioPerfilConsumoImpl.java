/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contrato.contrato.ContratoCliente;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.relatorio.ControladorRelatorioPerfilConsumo;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.perfilconsumocliente.PerfilConsumoVO;

public class ControladorRelatorioPerfilConsumoImpl extends ControladorNegocioImpl implements ControladorRelatorioPerfilConsumo {

	private static final String RELATORIO_PERFIL_CONSUMO = "relatorioPerfilConsumoCliente.jasper";

	private static final String ERRO_NEGOCIO_RELATORIO_SEM_DADOS = "ERRO_NEGOCIO_RELATORIO_SEM_DADOS";

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_IMOVEL = "idImovel";

	private static final String NUMERO_CONTRATO = "numeroContrato";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String NOME_CLIENTE = "nomeCliente";

	private static final String DESCRICAO_SEGMENTO = "descricaoSegmento";

	private static final Integer QUATRO_CASAS_DECIMAIS = 4;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.impl.
	 * ControladorRelatorioPerfilConsumo
	 * #getClasseEntidadePontoConsumo()
	 */
	@Override
	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.impl.
	 * ControladorRelatorioPerfilConsumo
	 * #getClasseEntidadeContratoCliente()
	 */
	@Override
	public Class<?> getClasseEntidadeContratoCliente() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoCliente.BEAN_ID_CONTRATO_CLIENTE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.impl.
	 * ControladorRelatorioPerfilConsumo
	 * #gerarRelatorio(java.util.Map,
	 * br.com.ggas.util
	 * .RelatorioUtil.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorio(Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException {

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		this.validarFiltro(filtro);

		Map<String, Object> parametros = new HashMap<String, Object>();
		if(this.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(this.obterEmpresaPrincipal().getChavePrimaria()));
		}

		this.montarParametrosFiltro(parametros, filtro);

		Collection<PerfilConsumoVO> listaPerfilConsumoVO = this.montarListaDadosRelatorio(filtro);

		filtro.put("idTipoDocumento", TipoDocumento.TIPO_FATURA);
		filtro.put("chavesCreditoDebitoSituacao",
						new Long[] {CreditoDebitoSituacao.NORMAL, CreditoDebitoSituacao.REFATURADA, CreditoDebitoSituacao.INCLUIDA,
						            	CreditoDebitoSituacao.PARCELADA});

		// Este método irá consultar as faturas
		// para o filtro informado, é preciso
		// retirar as duplicidades.
		Collection<Fatura> colecaoFatura = controladorFatura.consultarFatura(filtro);

		Map<HistoricoConsumo, Collection<Fatura>> mapaHistoricoConsumoFatura = controladorFatura
						.agruparFaturasPorHistoricoConsumo(colecaoFatura);

		Collection<Collection<Fatura>> colecoesDuplicadas = mapaHistoricoConsumoFatura.values();

		MultiKeyMap mapaVolumeFaturado = new MultiKeyMap();

		for (Collection<Fatura> colecaoFaturaDuplicada : colecoesDuplicadas) {

			// Pegar apenas a primeira fatura
			Fatura fatura = colecaoFaturaDuplicada.iterator().next();

			if(fatura.getListaFaturaItem() != null && !fatura.getListaFaturaItem().isEmpty()) {

				FaturaItem faturaItem = fatura.getListaFaturaItem().iterator().next();

				if(fatura.getContrato() != null && fatura.getPontoConsumo() != null) {

					// Colocar no mapa,
					// organizando por anoMes,
					// Contrato e Ponto de
					// Consumo.
					if(mapaVolumeFaturado.containsKey(fatura.getAnoMesReferencia(), fatura.getContrato().getChavePrimaria(), fatura
									.getPontoConsumo().getChavePrimaria())) {

						// Se já existe, somar
						// valor.
						BigDecimal volume = (BigDecimal) mapaVolumeFaturado.get(fatura.getAnoMesReferencia(), fatura.getContrato()
										.getChavePrimaria(), fatura.getPontoConsumo().getChavePrimaria());
						volume = volume.add(faturaItem.getQuantidade());
						mapaVolumeFaturado.put(fatura.getAnoMesReferencia(), fatura.getContrato().getChavePrimaria(), fatura
										.getPontoConsumo().getChavePrimaria(), volume);

					} else {

						// Se ainda não existe no
						// mapa, adicionar.
						mapaVolumeFaturado.put(fatura.getAnoMesReferencia(), fatura.getContrato().getChavePrimaria(), fatura
										.getPontoConsumo().getChavePrimaria(), faturaItem.getQuantidade());
					}

				}

			}
		}

		Map<Integer, BigDecimal> mapaTotalizador = new HashMap<Integer, BigDecimal>();

		// Varrer a lista de vo's pra adicionar os
		// valores.
		for (PerfilConsumoVO perfilConsumoVO : listaPerfilConsumoVO) {

			Long idContrato = perfilConsumoVO.getIdContrato();
			Long idPontoConsumo = perfilConsumoVO.getIdPontoConsumo();

			// Verifica qual foi o ano selecionado
			String anoSelecionado = (String) filtro.get("anoSelecionado");

			for (int mes = 1; mes <= 12; mes++) {

				// Tenta obter os valores no mapa
				// para cada mês do ano, contrato
				// e ponto de consumo.
				Integer anoMes = Integer.valueOf(anoSelecionado + Util.adicionarZeros(String.valueOf(mes)));
				BigDecimal volume = (BigDecimal) mapaVolumeFaturado.get(anoMes, idContrato, idPontoConsumo);
				if(volume != null) {
					perfilConsumoVO.setarVolume(volume, anoMes);
				}

				// Monta o mapa totalizador.
				if(mapaTotalizador.containsKey(mes) && mapaTotalizador.get(mes) != null) {
					BigDecimal volumeAnterior = mapaTotalizador.get(mes);
					if(volume != null) {
						mapaTotalizador.put(mes, volumeAnterior.add(volume));
					}
				} else {
					mapaTotalizador.put(mes, volume);
				}

			}
		}

		this.setarParametrosTotalizadores(parametros, mapaTotalizador);

		return RelatorioUtil.gerarRelatorio(listaPerfilConsumoVO, parametros, RELATORIO_PERFIL_CONSUMO, formatoImpressao);
	}

	/**
	 * Métorod responsável por setar os parâmetros
	 * que totaliza cada coluna dos meses.
	 * 
	 * @param parametros
	 *            Mapa de parâmetros.
	 * @param mapaTotalizador
	 *            Mapa totalizador.
	 */
	private void setarParametrosTotalizadores(Map<String, Object> parametros, Map<Integer, BigDecimal> mapaTotalizador) {

		Map<Integer, String> carregarMeses = Util.carregarMeses();
		for (int i = 1; i <= carregarMeses.size(); i++) {
			if (mapaTotalizador.get(i) != null) {
				String key = "total" + carregarMeses.get(i);
				parametros.put(key,
						Util.converterCampoValorDecimalParaString("", mapaTotalizador.get(i), Constantes.LOCALE_PADRAO, QUATRO_CASAS_DECIMAIS));
			}
		}
	}

	/**
	 * Validar filtro.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void validarFiltro(Map<String, Object> filtro) throws GGASException {

		String anoSelecionado = (String) filtro.get("anoSelecionado");

		if(StringUtils.isEmpty(anoSelecionado)) {
			throw new NegocioException(ERRO_NEGOCIO_ANO_OBRIGATORIO, true);
		}		

	}

	/**
	 * Montar parametros filtro.
	 * 
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void montarParametrosFiltro(Map<String, Object> parametros, Map<String, Object> filtro) throws GGASException {

		ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getBeanPorID(
						ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

		ControladorSegmento controladorSegmento = (ControladorSegmento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);

		ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia().getBeanPorID(
						ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);

		/**
		 * obs.: falta o filtro de pontos de
		 * consumo no relatório.
		 */
		Cliente cliente = null;
		Long idCliente = (Long) filtro.get(ID_CLIENTE);
		if(idCliente != null && idCliente > 0) {
			cliente = controladorCliente.obterCliente(idCliente);
		}

		Imovel imovel = null;
		Long idImovel = (Long) filtro.get(ID_IMOVEL);
		if(idImovel != null && idImovel > 0) {
			imovel = (Imovel) controladorImovel.obter(idImovel);
		}

		// TODO implementar. obter descrições dos
		// pontos de consumo selecionados.
		Long[] chavesPontoConsumo = (Long[]) filtro.get("chavesPontoConsumo");

		String numeroContrato = (String) filtro.get(NUMERO_CONTRATO);

		ModalidadeMedicaoImovel modalidadeMedicao = null;
		Long idModalidadeMedicao = (Long) filtro.get("idModalidadeMedicao");
		if(idModalidadeMedicao != null && idModalidadeMedicao > 0) {
			modalidadeMedicao = controladorImovel.obterModalidadeMedicaoImovel(Integer.valueOf(idModalidadeMedicao.toString()));
		}

		Segmento segmento = null;
		Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
		if(idSegmento != null && idSegmento > 0) {
			segmento = controladorSegmento.obterSegmento(idSegmento);
		}

		String dataAssinatura = (String) filtro.get("dataAssinatura");
		String dataInicioConsumo = (String) filtro.get("dataInicio");
		String dataEncerramento = (String) filtro.get("dataEncerramento");
		String anoSelecionado = (String) filtro.get("anoSelecionado");

		Boolean exibirFiltro = (Boolean) filtro.get(EXIBIR_FILTROS);
		parametros.put(EXIBIR_FILTROS, exibirFiltro);

		if(exibirFiltro) {

			if(cliente != null) {
				parametros.put(NOME_CLIENTE, cliente.getNome());
			}

			if(imovel != null) {
				parametros.put("descricaoImovel", imovel.getNome());
			}

			if(!StringUtils.isEmpty(numeroContrato)) {
				parametros.put(NUMERO_CONTRATO, numeroContrato);
			}

			if(modalidadeMedicao != null) {
				parametros.put("modalidadeMedicao", modalidadeMedicao.getDescricao());
			}

			if(segmento != null) {
				parametros.put(DESCRICAO_SEGMENTO, segmento.getDescricao());
			}

			if(!StringUtils.isEmpty(dataAssinatura)) {
				parametros.put("dataAssinatura", dataAssinatura);
			}

			if(!StringUtils.isEmpty(dataInicioConsumo)) {
				parametros.put("dataInicioConsumo", dataInicioConsumo);
			}

			if(!StringUtils.isEmpty(dataEncerramento)) {
				parametros.put("dataEncerramento", dataEncerramento);
			}

			if(!StringUtils.isEmpty(anoSelecionado)) {
				parametros.put("ano", anoSelecionado);
			}

			StringBuilder descricaoPontoConsumo = new StringBuilder();
			if((chavesPontoConsumo != null) && (chavesPontoConsumo.length > 0)) {
				ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getBeanPorID(
								ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
				for (int i = 0; i < chavesPontoConsumo.length; i++) {
					PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(chavesPontoConsumo[i]);
					if(i + 1 == chavesPontoConsumo.length) {
						descricaoPontoConsumo.append(pontoConsumo.getDescricao());
					} else {
						descricaoPontoConsumo.append(pontoConsumo.getDescricao()).append(", ");
					}
				}
				parametros.put("pontosConsumoSelecionados", descricaoPontoConsumo.toString());
			}
		}
	}

	/**
	 * Obter empresa principal.
	 * 
	 * @return the empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Empresa obterEmpresaPrincipal() throws NegocioException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put("principal", Boolean.TRUE);

		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);

		Empresa empresa = null;
		if((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
			empresa = ((List<Empresa>) listaEmpresas).get(0);
		}

		return empresa;
	}

	/**
	 * Montar lista dados relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<PerfilConsumoVO> montarListaDadosRelatorio(Map<String, Object> filtro) throws GGASException {

		ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia().getBeanPorID(
						ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);

		ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator.getInstancia().getBeanPorID(
						ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

		Collection<PerfilConsumoVO> listaPerfilConsumoVO = new ArrayList<PerfilConsumoVO>();

		Collection<Object> listaObjetosConsulta = this.obterDadosRelatorio(filtro);

		if (!listaObjetosConsulta.isEmpty()) {

			Long chaveContratoAnterior = null;
			Long chavePontoConsumoAnterior = null;

			for (Object resultado : listaObjetosConsulta) {
				Object[] resultadoArray = (Object[]) resultado;
				//
				Long chavePontoConsumoCorrente = null;

				// Tenta pegar a chave do contrato
				// pai, se n conseguir, pega do
				// contrato.
				Long chaveContratoCorrente = null;

				if((Long) resultadoArray[14] != null) {
					chaveContratoCorrente = (Long) resultadoArray[14];
				} else {
					chaveContratoCorrente = (Long) resultadoArray[12];
				}

				chavePontoConsumoCorrente = (Long) resultadoArray[13];

				/**
				 * Só irá adicionar na lista de vo
				 * caso seja o primeiro registro
				 * ou seja de um contratoPai
				 * diferente
				 * com pontoConsumo diferente,
				 * isso evitará duplicidades.
				 */
				if((!chaveContratoCorrente.equals(chaveContratoAnterior))
								|| (chaveContratoCorrente.equals(chaveContratoAnterior) && !chavePontoConsumoCorrente
												.equals(chavePontoConsumoAnterior))) {

					chaveContratoAnterior = chaveContratoCorrente;
					chavePontoConsumoAnterior = chavePontoConsumoCorrente;

					PerfilConsumoVO perfilConsumoVO = new PerfilConsumoVO();

					perfilConsumoVO.setNomeCliente((String) resultadoArray[0]);
					perfilConsumoVO.setDescricaoSegmento((String) resultadoArray[1]);
					perfilConsumoVO.setDescricaoPontoConsumo((String) resultadoArray[2]);
					perfilConsumoVO.setDescricaoImovel((String) resultadoArray[3]);

					if(((Date) resultadoArray[4]) != null) {
						perfilConsumoVO.setDataAssinaturaContrato(Util.converterDataParaStringSemHora((Date) resultadoArray[4],
										Constantes.FORMATO_DATA_BR));
					}

					if(((InstalacaoMedidor) resultadoArray[5]) != null) {

						InstalacaoMedidor instalacaoMedidor = (InstalacaoMedidor) resultadoArray[5];
						Date dataInicioConsumo = controladorHistoricoMedicao.setarDataLeituraAnterior(
										instalacaoMedidor,null, null);
						perfilConsumoVO.setDataInicioConsumo(Util.converterDataParaStringSemHora(dataInicioConsumo,
										Constantes.FORMATO_DATA_BR));

					}

					if(((Date) resultadoArray[6]) != null) {
						perfilConsumoVO.setDataEncerramento(Util.converterDataParaStringSemHora((Date) resultadoArray[6],
										Constantes.FORMATO_DATA_BR));
					}

					perfilConsumoVO.setZonaBloqueio((String) resultadoArray[9]);

					ModalidadeMedicaoImovel modalidadeMedicao = null;
					Integer idModalidadeMedicao = (Integer) resultadoArray[7];

					if(idModalidadeMedicao != null && idModalidadeMedicao > 0) {
						modalidadeMedicao = controladorImovel.obterModalidadeMedicaoImovel(idModalidadeMedicao);
						perfilConsumoVO.setModalidadeMedicao(modalidadeMedicao.getDescricao());
					}

					perfilConsumoVO.setDescricaoRamoAtividade((String) resultadoArray[8]);
					perfilConsumoVO.setModalidadeUso((String) resultadoArray[10]);

					perfilConsumoVO.setIdCliente((Long) resultadoArray[11]);
					perfilConsumoVO.setIdContrato((Long) resultadoArray[12]);
					perfilConsumoVO.setIdPontoConsumo((Long) resultadoArray[13]);

					listaPerfilConsumoVO.add(perfilConsumoVO);
				}

			}
		} else {
			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return listaPerfilConsumoVO;
	}

	/**
	 * Obter dados relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<Object> obterDadosRelatorio(Map<String, Object> filtro) throws GGASException {

		Long idImovel = (Long) filtro.get(ID_IMOVEL);
		Long idCliente = (Long) filtro.get(ID_CLIENTE);
		Long[] chavesPontoConsumo = (Long[]) filtro.get("chavesPontoConsumo");
		Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
		String numeroContrato = (String) filtro.get(NUMERO_CONTRATO);
		Long idModalidadeMedicao = (Long) filtro.get("idModalidadeMedicao");
		String dataAssinatura = (String) filtro.get("dataAssinatura");

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" cliente.nome, ");
		// [0]
		hql.append(" segmento.descricao, ");
		// [1]
		hql.append(" pontoConsumo.descricao, ");
		// [2]
		hql.append(" imovel.nome, ");
		// [3]
		hql.append(" contrato.dataAssinatura, ");
		// [4]
		hql.append(" instalacaoMedidor, ");
		// [5]
		hql.append(" contrato.dataRecisao, ");
		// [6]
		hql.append(" imovel.modalidadeMedicaoImovel.codigo, ");
		// [7]
		hql.append(" ramoAtividade.descricao, ");
		// [8]
		hql.append(" zonaBloqueio.descricao, ");
		// [9]
		hql.append(" modalidadeUso.descricao, ");
		// [10]
		hql.append(" cliente.chavePrimaria, ");
		// [11]
		hql.append(" contrato.chavePrimaria, ");
		// [12]
		hql.append(" pontoConsumo.chavePrimaria, ");
		// [13]
		hql.append(" contrato.chavePrimariaPai ");
		// [14]
		hql.append(" from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPontoConsumo ");

		hql.append(" inner join contratoPontoConsumo.contrato contrato ");
		hql.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join pontoConsumo.imovel imovel ");
		hql.append(" inner join pontoConsumo.ramoAtividade ramoAtividade ");
		hql.append(" inner join pontoConsumo.segmento segmento ");
		hql.append(" left join pontoConsumo.instalacaoMedidor instalacaoMedidor ");
		hql.append(" inner join contrato.clienteAssinatura cliente ");
		hql.append(" left join pontoConsumo.imovel.quadraFace.quadra.zonaBloqueio zonaBloqueio ");
		hql.append(" left join pontoConsumo.modalidadeUso modalidadeUso ");
		hql.append(" where cliente.habilitado = true ");

		if(idImovel != null) {
			hql.append(" and imovel.chavePrimaria = :idImovel ");
		}

		if(idCliente != null) {
			hql.append(" and cliente.chavePrimaria = :idCliente ");
		}

		if(idSegmento != null) {
			hql.append(" and segmento.chavePrimaria = :idSegmento ");
		}

		if(idModalidadeMedicao != null) {
			hql.append(" and imovel.modalidadeMedicaoImovel.codigo = :idModalidadeMedicao ");
		}

		if(numeroContrato != null && !numeroContrato.isEmpty()) {
			hql.append(" and contrato.numero = :numeroContrato ");
		}

		if(dataAssinatura != null && !dataAssinatura.isEmpty()) {
			hql.append(" and contrato.dataAssinatura = :dataAssinatura ");
		}

		Map<String, List<Long>> mapaPropriedades = new HashMap<>();
		if(chavesPontoConsumo != null && chavesPontoConsumo.length > 0) {
			mapaPropriedades = 
					HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontoConsumo));
		}

		hql.append(" order by contrato.chavePrimaria, contrato.chavePrimariaPai, pontoConsumo.chavePrimaria ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(idImovel != null) {
			query.setLong(ID_IMOVEL, idImovel);
		}

		if(idCliente != null) {
			query.setLong(ID_CLIENTE, idCliente);
		}

		if(idSegmento != null) {
			query.setLong(ID_SEGMENTO, idSegmento);
		}

		if(idModalidadeMedicao != null) {
			query.setLong("idModalidadeMedicao", idModalidadeMedicao);
		}

		if(numeroContrato != null && !numeroContrato.isEmpty()) {
			Integer numContrato = Integer.valueOf(numeroContrato);
			query.setInteger(NUMERO_CONTRATO, numContrato);
		}

		if(dataAssinatura != null && !dataAssinatura.isEmpty()) {
			Date data = Util.converterCampoStringParaData("Data de Assinatura", dataAssinatura, Constantes.FORMATO_DATA_BR);
			query.setDate("dataAssinatura", data);
		}

		if(chavesPontoConsumo != null && chavesPontoConsumo.length > 0 && MapUtils.isNotEmpty(mapaPropriedades)) {
			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.impl.
	 * ControladorRelatorioPerfilConsumo#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.impl.
	 * ControladorRelatorioPerfilConsumo
	 * #getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

}
