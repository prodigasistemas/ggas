package br.com.ggas.relatorio.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioDataLimiteReligamentoGas;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.web.relatorio.dataLimiteReligamentoGas.ParamentroRelatorio;
import br.com.ggas.web.relatorio.dataLimiteReligamentoGas.RepositorioDataLimiteReligamentoGas;
import br.com.ggas.web.relatorio.dataLimiteReligamentoGas.ServicoAutorizacaoRelatorioVO;

@Service("controladorRelatorioDataLimiteReligamentoGas")
@Transactional
public class ControladorRelatorioDataLimiteReligamentoGasImpl implements ControladorRelatorioDataLimiteReligamentoGas{

	@Autowired
	private RepositorioDataLimiteReligamentoGas repositorioDataLimiteReligamentoGas;
	
	private static String STATUS_NO_PRAZO = "No Prazo";
	
	private static String STATUS_FORA_PRAZO = "Fora do Prazo";

	private static String VALOR_PARAMETRO = "DATA_LIMITE_RELIGACAO_GAS";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioPontosDeConsumoPorDataImpl(java.util.Map)
	 */
	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public byte[] gerarRelatorio(Date dataIncial, Date dataFinal, String exibirFiltros, String tipoRelatorio) throws GGASException {

		Collection<ParamentroRelatorio> collRelatorio = new ArrayList<ParamentroRelatorio>();

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(tipoRelatorio);
		List<ServicoAutorizacaoRelatorioVO> listaServicoAutorizacao = (List<ServicoAutorizacaoRelatorioVO>) 
						this.obterDatasLimiteReligamentoGas(dataIncial, dataFinal);

		int paramentro = this.obterParametro();
		

		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar c = Calendar.getInstance();

		for (int i = 0; i < listaServicoAutorizacao.size(); i++) {

			ParamentroRelatorio parametroRelatorio = new ParamentroRelatorio();
			String status = "";
			
			if( listaServicoAutorizacao.get(i).getDataEncerramento() != null ){
				parametroRelatorio.setDataEncerramento(sf.format(listaServicoAutorizacao.get(i).getDataEncerramento()));				
			}
			parametroRelatorio.setDataGerada(sf.format(listaServicoAutorizacao.get(i).getDataGerada()));
			parametroRelatorio.setNomeCliente(listaServicoAutorizacao.get(i).getNomeCliente());
			c.setTime( listaServicoAutorizacao.get(i).getDataGerada());
			c.add(Calendar.DATE, paramentro);
			parametroRelatorio.setDataLimite(sf.format(c.getTime()));
			Date hoje = new Date();
			
			if( listaServicoAutorizacao.get(i).getDataEncerramento() != null ){				
				if(	listaServicoAutorizacao.get(i).getDataEncerramento().after(c.getTime())){
					status = STATUS_FORA_PRAZO;
				}else{
					status = STATUS_NO_PRAZO;
				}
			}else{
				if( hoje.after(c.getTime()) ){
					status = STATUS_FORA_PRAZO;					
				}else{
					status = STATUS_NO_PRAZO;
				}
			}
			
			parametroRelatorio.setStatus(status);

			collRelatorio.add(parametroRelatorio);
		}

		String dataParamInicio = null;
		String artigo = null;
		if(dataIncial != null){
			dataParamInicio = sf.format(dataIncial);
			artigo = "à";
		}
		String dataParamFim = null;
		if(dataFinal != null){
			dataParamFim = sf.format(dataFinal);
		}

		String dataImpressao = sf.format(Calendar.getInstance().getTime());
		Map parametros = new HashMap();
		parametros.put("dataInicial", dataParamInicio);
		parametros.put("dataFinal", dataParamFim);
		parametros.put("dataImpressao", dataImpressao);
		parametros.put("formatoImpressao", formatoImpressao);
		parametros.put("artigo", artigo);
		
		this.validarDadosGerarRelatorioPontoConsumoPorData(parametros);

		return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_DATA_LIMITE_RELIGAMENTO_GAS_JASPER, formatoImpressao);
	}


	/**
	 * Validar dados gerar relatorio pontos de consumo por data.
	 * 
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosGerarRelatorioPontoConsumoPorData(Map<String, Object> parametros) throws NegocioException {

		if (parametros.get("formatoImpressao") == null) {

			throw new NegocioException(ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO, true);

		}

	}


	/**
	 * Obter pontos consumo por data.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the map
	 */
	private Collection<ServicoAutorizacaoRelatorioVO> obterDatasLimiteReligamentoGas(Date dataIncio, 
			Date dataFinal) {

		return repositorioDataLimiteReligamentoGas.listarDatasLimiteParaReligamentoGas(dataIncio, dataFinal);
	}

	private int obterParametro() {

		String paramentro = "";

		List<String> parametros = repositorioDataLimiteReligamentoGas.buscarValorParamentro(VALOR_PARAMETRO);

		for (int i = 0; i < parametros.size(); i++) {
			paramentro = parametros.get(i);
		}

		return Integer.parseInt(paramentro);
	}

}

