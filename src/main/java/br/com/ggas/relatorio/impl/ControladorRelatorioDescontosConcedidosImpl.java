/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturaItemImpressao;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioDescontosConcedidos;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.descontosConcedidos.RelatorioDescontosConcedidosVO;

/**
 *
 *
 */
class ControladorRelatorioDescontosConcedidosImpl extends ControladorNegocioImpl implements ControladorRelatorioDescontosConcedidos {

	private static final String RELATORIO_DESCONTOS_CONCEDIDOS = "relatorioDescontosConcedidos.jasper";

	private static final String ERRO_NEGOCIO_RELATORIO_SEM_DADOS = "ERRO_NEGOCIO_RELATORIO_SEM_DADOS";

	private static final String DATA_INICIAL = "dataInicial";

	private static final String DATA_FINAL = "dataFinal";

	private static final String DATA_EMISSAO = "dataEmissao";

	private static final String ID_CLIENTE = "idCliente";

	private static final String NUMERO_CONTRATO = "numeroContrato";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String NOME_CLIENTE = "nomeCliente";

	private static final String DESCRICAO_SEGMENTO = "descricaoSegmento";

	private static final String RUBRICAS_ASSOCIADAS = "rubricasAssociadas";

	private static final String ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String NUMERO_NOTA = "numeroNota";

	private static final String NOME_PONTO_CONUSMO = "nomePontoConsumo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeFaturaItem() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaItem.BEAN_ID_FATURA_ITEM);
	}

	public Class<?> getClasseEntidadeFaturaItemImpressao() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaItemImpressao.BEAN_ID_FATURA_ITEM_IMPRESSAO);
	}

	public Class<?> getClasseEntidadeDocumentoFiscal() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoFiscal.BEAN_ID_DOCUMENTO_FISCAL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioDescontosConcedidos#gerarRelatorio(java.util.Map, br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorio(Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Map<String, Object> parametros = new HashMap<String, Object>();
		if(controladorEmpresa.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem",
							Constantes.URL_LOGOMARCA_EMPRESA
											+ String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		}
		this.montarParametrosFiltro(parametros, filtro);

		Collection<RelatorioDescontosConcedidosVO> listaRelatorioDescontosConcedidosVO = this.montarDadosRelatorio(filtro);

		return RelatorioUtil.gerarRelatorio(listaRelatorioDescontosConcedidosVO, parametros, RELATORIO_DESCONTOS_CONCEDIDOS,
						formatoImpressao);
	}

	/**
	 * Montar parametros filtro.
	 *
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void montarParametrosFiltro(Map<String, Object> parametros, Map<String, Object> filtro) throws GGASException {

		Segmento segmento = null;
		Cliente cliente = null;
		PontoConsumo pontoConsumo = null;
		Long idCliente = (Long) filtro.get(ID_CLIENTE);
		Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
		Long idPontoConsumo = (Long) filtro.get(ID_PONTO_CONSUMO);
		String numeroContratoAux = (String) filtro.get(NUMERO_CONTRATO);
		String numeroNota = (String) filtro.get(NUMERO_NOTA);
		Date dataInicial = (Date) filtro.get(DATA_INICIAL);
		Date dataFinal = (Date) filtro.get(DATA_FINAL);
		Date dataEmissao = (Date) filtro.get(DATA_EMISSAO);

		if(idSegmento != null && idSegmento > 0) {
			ControladorSegmento controladorSegmento = (ControladorSegmento) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);

			segmento = controladorSegmento.obterSegmento(idSegmento);
		}

		if(idCliente != null && idCliente > 0) {
			ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getBeanPorID(
							ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

			cliente = controladorCliente.obterCliente(idCliente, "enderecos");
		}

		if(idPontoConsumo != null && idPontoConsumo > 0) {
			ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getBeanPorID(
							ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

			pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo);
		}

		Boolean exibirFiltro = (Boolean) filtro.get(EXIBIR_FILTROS);
		parametros.put(EXIBIR_FILTROS, exibirFiltro);
		if(exibirFiltro) {
			if(segmento != null) {
				parametros.put(DESCRICAO_SEGMENTO, segmento.getDescricao());
			}
			if(cliente != null) {
				parametros.put(NOME_CLIENTE, cliente.getNome());
			}
			if(!StringUtils.isEmpty(numeroContratoAux)) {
				parametros.put(NUMERO_CONTRATO, Util.converterCampoStringParaValorInteger("Número Contrato", numeroContratoAux));
			}
			if(dataEmissao != null) {
				parametros.put(DATA_EMISSAO, Util.converterDataParaStringSemHora(dataEmissao, Constantes.FORMATO_DATA_BR));
			}
			if(dataInicial != null) {
				parametros.put(DATA_INICIAL, Util.converterDataParaStringSemHora(dataInicial, Constantes.FORMATO_DATA_BR));
			}
			if(dataFinal != null) {
				parametros.put(DATA_FINAL, Util.converterDataParaStringSemHora(dataFinal, Constantes.FORMATO_DATA_BR));
			}
			if(!StringUtils.isEmpty(numeroNota)) {
				parametros.put(NUMERO_NOTA, Util.converterCampoStringParaValorInteger("Número Nota", numeroNota));
			}
			if(pontoConsumo != null) {
				parametros.put(NOME_PONTO_CONUSMO, pontoConsumo.getDescricao());
			}
		}

	}

	/**
	 * Montar dados relatorio.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<RelatorioDescontosConcedidosVO> montarDadosRelatorio(Map<String, Object> filtro) throws GGASException {

		Collection<RelatorioDescontosConcedidosVO> listaRelatorioDescontosConcedidosVO = new ArrayList<RelatorioDescontosConcedidosVO>();

		Collection<Object> listaObjetosDocumentoFiscal = this.obterDadosRelatorioDocumentoFiscal(filtro);
		Collection<Object> listaObjetosfatura = this.obterDadosRelatorioFatura(filtro);
		if(!listaObjetosDocumentoFiscal.isEmpty() || !listaObjetosfatura.isEmpty()) {
			for (Object resultado : listaObjetosDocumentoFiscal) {
				RelatorioDescontosConcedidosVO relatorioDescontosConcedidosVO = new RelatorioDescontosConcedidosVO();
				Object[] resultadoArray = (Object[]) resultado;
				relatorioDescontosConcedidosVO.setPontoConsumo((String) resultadoArray[0]);
				relatorioDescontosConcedidosVO.setSegmento((String) resultadoArray[1]);
				relatorioDescontosConcedidosVO.setNotaFiscal(String.valueOf(resultadoArray[2]));
				relatorioDescontosConcedidosVO.setEmissao((Date) resultadoArray[3]);
				relatorioDescontosConcedidosVO.setVencimento((Date) resultadoArray[4]);
				relatorioDescontosConcedidosVO.setVolume((BigDecimal) resultadoArray[5]);
				relatorioDescontosConcedidosVO.setDesconto((BigDecimal) resultadoArray[6]);
				relatorioDescontosConcedidosVO.setDescricao((String) resultadoArray[7]);
				relatorioDescontosConcedidosVO.setNomeCliente((String) resultadoArray[8]);
				relatorioDescontosConcedidosVO.setSomaCredito((BigDecimal) resultadoArray[9]);

				listaRelatorioDescontosConcedidosVO.add(relatorioDescontosConcedidosVO);
			}

			for (Object resultado : listaObjetosfatura) {
				RelatorioDescontosConcedidosVO relatorioDescontosConcedidosVO = new RelatorioDescontosConcedidosVO();
				Object[] resultadoArray = (Object[]) resultado;
				relatorioDescontosConcedidosVO.setPontoConsumo((String) resultadoArray[0]);
				relatorioDescontosConcedidosVO.setSegmento((String) resultadoArray[1]);
				relatorioDescontosConcedidosVO.setNotaFiscal(String.valueOf(resultadoArray[2]));
				relatorioDescontosConcedidosVO.setEmissao((Date) resultadoArray[3]);
				relatorioDescontosConcedidosVO.setVencimento((Date) resultadoArray[4]);
				relatorioDescontosConcedidosVO.setVolume((BigDecimal) resultadoArray[5]);
				relatorioDescontosConcedidosVO.setDesconto((BigDecimal) resultadoArray[6]);
				relatorioDescontosConcedidosVO.setDescricao((String) resultadoArray[7]);
				relatorioDescontosConcedidosVO.setNomeCliente((String) resultadoArray[8]);
				relatorioDescontosConcedidosVO.setSomaCredito((BigDecimal) resultadoArray[9]);

				listaRelatorioDescontosConcedidosVO.add(relatorioDescontosConcedidosVO);
			}
		} else {
			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		if(!listaRelatorioDescontosConcedidosVO.isEmpty()) {
			this.ordenarListaRelatorioDescontosConcedidosVO(listaRelatorioDescontosConcedidosVO);
		}

		return listaRelatorioDescontosConcedidosVO;
	}

	/**
	 * Obter dados relatorio documento fiscal.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings({"unchecked", "unused"})
	private Collection<Object> obterDadosRelatorioDocumentoFiscal(Map<String, Object> filtro) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		Long codigoTipoCredito = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO));

		Long codigoTipoOperacaoSaida = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_SAIDA));

		Long[] listaRubricas = (Long[]) filtro.get(RUBRICAS_ASSOCIADAS);
		Long idCliente = (Long) filtro.get(ID_CLIENTE);
		Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
		Long idPontoConsumo = (Long) filtro.get(ID_PONTO_CONSUMO);
		String numeroNotaAux = (String) filtro.get(NUMERO_CONTRATO);
		Integer numeroNota = null;
		if(!StringUtils.isEmpty(numeroNotaAux)) {
			numeroNota = Util.converterCampoStringParaValorInteger("Número Nota", numeroNotaAux);
		}

		Date dataInicial = null;
		Date dataFinal = null;
		Date dataEmissao = null;

		dataInicial = (Date) filtro.get(DATA_INICIAL);
		dataFinal = (Date) filtro.get(DATA_FINAL);
		dataEmissao = (Date) filtro.get(DATA_EMISSAO);

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select ");
		hql.append(" pontoConsumo.descricao, ");
		hql.append(" segmento.descricao, ");
		hql.append(" documentoFiscal.numero, ");
		hql.append(" fatura.dataEmissao, ");
		hql.append(" fatura.dataVencimento, ");
		hql.append(" faturaItem.quantidade, ");
		hql.append(" fatura.valorTotal, ");
		hql.append(" rubrica.descricao, ");
		hql.append(" cliente.nome, ");
		hql.append(" ( ");
		hql.append(" select sum(faturaItemSomaCredito.valorTotal) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" faturaSomaCredito, ");
		hql.append(getClasseEntidadeFaturaItem().getSimpleName());
		hql.append(" faturaItemSomaCredito, ");
		hql.append(getClasseEntidadeFaturaItemImpressao().getSimpleName());
		hql.append(" faturaItemImpressaoSomaCredito ");
		hql.append(" where ");
		hql.append(" fatura.chavePrimaria = faturaSomaCredito.chavePrimaria ");
		hql.append(" and faturaSomaCredito.chavePrimaria = faturaItemSomaCredito.fatura.chavePrimaria ");
		hql.append(" and faturaItemSomaCredito.chavePrimaria = faturaItemImpressaoSomaCredito.faturaItem.chavePrimaria ");
		hql.append(" ) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura, ");
		hql.append(getClasseEntidadeFaturaItem().getSimpleName());
		hql.append(" faturaItem, ");
		hql.append(getClasseEntidadeDocumentoFiscal().getSimpleName());
		hql.append(" documentoFiscal ");
		hql.append(" inner join fatura.contrato contrato ");
		hql.append(" inner join fatura.pontoConsumo pontoConsumo ");
		hql.append(" inner join pontoConsumo.segmento segmento ");
		hql.append(" inner join faturaItem.rubrica rubrica ");
		hql.append(" inner join rubrica.lancamentoItemContabil lancamentoItemContabil ");
		hql.append(" inner join fatura.cliente cliente ");
		hql.append(" where fatura.chavePrimaria = faturaItem.fatura.chavePrimaria ");
		hql.append(" and fatura.chavePrimaria = documentoFiscal.fatura.chavePrimaria ");
		hql.append(" and lancamentoItemContabil.tipoCreditoDebito.chavePrimaria = :chaveTipoLancamento ");
		hql.append(" and documentoFiscal.tipoOperacao.chavePrimaria = :chaveTipoOperacao ");

		if(listaRubricas != null && listaRubricas.length > 0) {
			hql.append(" and rubrica.chavePrimaria in (:rubricasAssociadas) ");
		}
		if(idPontoConsumo != null && idPontoConsumo > 0) {
			hql.append(" and pontoConsumo.chavePrimaria = :idPontoConsumo ");
		}
		if(idCliente != null) {
			hql.append(" and contrato.clienteAssinatura.chavePrimaria = :idCliente ");
		}
		if(numeroNota != null) {
			hql.append(" and documentoFiscal.numero = :numeroNota ");
		}
		if(idSegmento != null) {
			hql.append(" and segmento.chavePrimaria = :idSegmento ");
		}
		if(dataEmissao != null) {
			hql.append(" and fatura.dataEmissao = :dataEmissao ");
		}
		if(dataInicial != null && dataFinal != null) {
			hql.append(" and fatura.dataVencimento between :dataInicial and :dataFinal ");
		}

		hql.append(" order by contrato.clienteAssinatura.chavePrimaria, contrato.chavePrimaria, segmento.chavePrimaria, faturaItem.quantidade ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chaveTipoLancamento", codigoTipoCredito);
		query.setLong("chaveTipoOperacao", codigoTipoOperacaoSaida);

		if(idPontoConsumo != null && idPontoConsumo > 0) {
			query.setLong(ID_PONTO_CONSUMO, idPontoConsumo);
		}
		if(listaRubricas != null && listaRubricas.length > 0) {
			query.setParameterList(RUBRICAS_ASSOCIADAS, listaRubricas);
		}
		if(idCliente != null) {
			query.setLong(ID_CLIENTE, idCliente);
		}
		if(numeroNota != null) {
			query.setInteger(NUMERO_NOTA, numeroNota);
		}
		if(idSegmento != null) {
			query.setLong(ID_SEGMENTO, idSegmento);
		}
		if(dataEmissao != null) {
			query.setDate(DATA_EMISSAO, dataEmissao);
		}
		if(dataInicial != null && dataFinal != null) {
			query.setDate(DATA_INICIAL, dataInicial);
			query.setDate(DATA_FINAL, dataFinal);
		}

		return query.list();
	}

	/**
	 * Obter dados relatorio fatura.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<Object> obterDadosRelatorioFatura(Map<String, Object> filtro) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ServiceLocator.getInstancia().getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long codigoTipoCredito = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO));

		Long chaveTipoDocumento = Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_NOTA_CREDITO));

		Long[] listaRubricas = (Long[]) filtro.get(RUBRICAS_ASSOCIADAS);
		Long idCliente = (Long) filtro.get(ID_CLIENTE);
		Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
		Long idPontoConsumo = (Long) filtro.get(ID_PONTO_CONSUMO);
		String numeroNotaAux = (String) filtro.get(NUMERO_CONTRATO);
		Integer numeroNota = null;
		if(!StringUtils.isEmpty(numeroNotaAux)) {
			numeroNota = Util.converterCampoStringParaValorInteger("Número Nota", numeroNotaAux);
		}

		Date dataInicial = null;
		Date dataFinal = null;
		Date dataEmissao = null;

		dataInicial = (Date) filtro.get(DATA_INICIAL);
		dataFinal = (Date) filtro.get(DATA_FINAL);
		dataEmissao = (Date) filtro.get(DATA_EMISSAO);

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select ");

		hql.append(" pontoConsumo.descricao, ");

		hql.append(" segmento.descricao, ");

		hql.append(" fatura.chavePrimaria, ");
		hql.append(" fatura.dataEmissao, ");
		hql.append(" fatura.dataVencimento, ");
		hql.append(" faturaItem.quantidade, ");
		hql.append(" fatura.valorTotal, ");
		hql.append(" rubrica.descricao, ");
		hql.append(" cliente.nome, ");
		hql.append(" ( ");
		hql.append(" select sum(faturaItemSomaCredito.valorTotal) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" faturaSomaCredito, ");
		hql.append(getClasseEntidadeFaturaItem().getSimpleName());
		hql.append(" faturaItemSomaCredito, ");
		hql.append(getClasseEntidadeFaturaItemImpressao().getSimpleName());
		hql.append(" faturaItemImpressaoSomaCredito ");
		hql.append(" where ");
		hql.append(" fatura.chavePrimaria = faturaSomaCredito.chavePrimaria ");
		hql.append(" and faturaSomaCredito.chavePrimaria = faturaItemSomaCredito.fatura.chavePrimaria ");
		hql.append(" and faturaItemSomaCredito.chavePrimaria = faturaItemImpressaoSomaCredito.faturaItem.chavePrimaria ");
		hql.append(" ) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura, ");
		hql.append(getClasseEntidadeFaturaItem().getSimpleName());
		hql.append(" faturaItem ");

		hql.append(" left join fatura.contrato contrato ");

		hql.append(" left join fatura.pontoConsumo pontoConsumo ");

		hql.append(" left join pontoConsumo.segmento segmento ");

		hql.append(" inner join faturaItem.rubrica rubrica ");
		hql.append(" inner join rubrica.lancamentoItemContabil lancamentoItemContabil ");
		hql.append(" inner join fatura.cliente cliente ");
		hql.append(" where fatura.chavePrimaria = faturaItem.fatura.chavePrimaria ");
		hql.append(" and lancamentoItemContabil.tipoCreditoDebito.chavePrimaria = :chaveTipoLancamento ");
		hql.append(" and fatura.tipoDocumento.chavePrimaria = :chaveTipoDocumento ");

		if(listaRubricas != null && listaRubricas.length > 0) {
			hql.append(" and rubrica.chavePrimaria in (:rubricasAssociadas) ");
		}
		if(idPontoConsumo != null && idPontoConsumo > 0) {
			hql.append(" and pontoConsumo.chavePrimaria = :idPontoConsumo ");

		}
		if(idSegmento != null) {
			hql.append(" and segmento.chavePrimaria = :idSegmento ");
		}
		if(idCliente != null) {
			hql.append(" and contrato.clienteAssinatura.chavePrimaria = :idCliente ");
		}
		if(numeroNota != null) {
			hql.append(" and fatura.chavePrimaria = :numeroNota ");
		}

		if(dataEmissao != null) {
			hql.append(" and fatura.dataEmissao = :dataEmissao ");
		}
		if(dataInicial != null && dataFinal != null) {
			hql.append(" and fatura.dataVencimento between :dataInicial and :dataFinal ");
		}

		hql.append(" order by fatura.chavePrimaria, faturaItem.quantidade ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chaveTipoLancamento", codigoTipoCredito);
		query.setLong("chaveTipoDocumento", chaveTipoDocumento);

		if(idPontoConsumo != null && idPontoConsumo > 0) {
			query.setLong(ID_PONTO_CONSUMO, idPontoConsumo);

		}
		if(idSegmento != null) {
			query.setLong(ID_SEGMENTO, idSegmento);
		}
		if(listaRubricas != null && listaRubricas.length > 0) {
			query.setParameterList(RUBRICAS_ASSOCIADAS, listaRubricas);
		}
		if(idCliente != null) {
			query.setLong(ID_CLIENTE, idCliente);
		}
		if(numeroNota != null) {
			query.setInteger(NUMERO_NOTA, numeroNota);
		}

		if(dataEmissao != null) {
			query.setDate(DATA_EMISSAO, dataEmissao);
		}
		if(dataInicial != null && dataFinal != null) {
			query.setDate(DATA_INICIAL, dataInicial);
			query.setDate(DATA_FINAL, dataFinal);
		}

		return query.list();
	}

	/**
	 * Ordenar lista relatorio descontos concedidos vo.
	 *
	 * @param listaRelatorioDescontosConcedidosVO
	 *            the lista relatorio descontos concedidos vo
	 */
	private void ordenarListaRelatorioDescontosConcedidosVO(Collection<RelatorioDescontosConcedidosVO> listaRelatorioDescontosConcedidosVO) {

		List<RelatorioDescontosConcedidosVO> listaRelatorioDescontosConcedidosVOOrdenada = new ArrayList<RelatorioDescontosConcedidosVO>();
		for (RelatorioDescontosConcedidosVO relatorioDescontosConcedidosVO : listaRelatorioDescontosConcedidosVO) {
			listaRelatorioDescontosConcedidosVOOrdenada.add(relatorioDescontosConcedidosVO);
		}
		Collections.sort(listaRelatorioDescontosConcedidosVOOrdenada, new Comparator<RelatorioDescontosConcedidosVO>(){

			@Override
			public int compare(RelatorioDescontosConcedidosVO o1, RelatorioDescontosConcedidosVO o2) {

				return o1.getEmissao().compareTo(o2.getEmissao());
			}
		});

		listaRelatorioDescontosConcedidosVO.clear();
		listaRelatorioDescontosConcedidosVO.addAll(listaRelatorioDescontosConcedidosVOOrdenada);

	}
}
