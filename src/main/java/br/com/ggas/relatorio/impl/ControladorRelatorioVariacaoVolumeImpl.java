/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.hibernate.Query;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.relatorio.ControladorRelatorioVariacaoVolume;
import br.com.ggas.relatorio.VariacaoVolumeVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.RelatorioVariacaoVolumeAction;

/**
 * 
 *
 */
class ControladorRelatorioVariacaoVolumeImpl extends ControladorNegocioImpl implements ControladorRelatorioVariacaoVolume {

	private static final String RELATORIO_VARIACAO_VOLUME = "relatorioVariacoesVolume.jasper";

	private static final String ERRO_PERIODO_NAO_INFORMADO = "ERRO_PERIODO_NAO_INFORMADO";

	public static final String ID_TIPO_RELATORIO = "idRelatorio";

	public static final String TIPO_EXIBICAO_ANALITICO = "analitico";

	public static final String TIPO_EXIBICAO_SINTETICO = "sintetico";

	public static final String LISTA_TIPO_GRAFICO = "listaTipoGrafico";

	public static final String ID_TIPO_GRAFICO = "idTipoGrafico";

	public static final String ID_GRAFICO = "grafico";

	public static final String IMAGEM = "imagem";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	public Class<?> getClasseEntidadeHistoricoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeSegmento() {

		return ServiceLocator.getInstancia().getClassPorID(Segmento.BEAN_ID_SEGMENTO);
	}

	/**
	 * Remover ponto consumo repedido.
	 * 
	 * @param listaPontoConsumo
	 *            the lista ponto consumo
	 * @return the collection
	 */
	private Collection<PontoConsumo> removerPontoConsumoRepedido(Collection<PontoConsumo> listaPontoConsumo) {

		Collection<PontoConsumo> listaPontoConsumoAux = new ArrayList<PontoConsumo>();

		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			if(!listaPontoConsumoAux.contains(pontoConsumo)) {
				listaPontoConsumoAux.add(pontoConsumo);
			}
		}
		return listaPontoConsumoAux;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioVariacaoVolume#gerarRelatorioVariacaoVolume(java.util.Map)
	 */
	@Override
	public byte[] gerarRelatorioVariacaoVolume(Map<String, Object> filtro) throws NegocioException {

		Map<String, Object> parametros = new HashMap<String, Object>();

		Integer referencia = null;
		byte[] relatorio = null;

		if(!filtro.containsKey(RelatorioVariacaoVolumeAction.REFERENCIA)) {
			throw new NegocioException(ERRO_PERIODO_NAO_INFORMADO);

		} else {
			referencia = Integer.valueOf((String) filtro.get(RelatorioVariacaoVolumeAction.REFERENCIA));

			parametros.put(RelatorioVariacaoVolumeAction.PARAM_REFERENCIA, Util.formatarAnoMes(referencia));

			// Verifica se deve exibir Filtros
			Boolean isExibirFiltros = null;
			if(filtro.containsKey(RelatorioVariacaoVolumeAction.EXIBIR_FILTROS)) {
				isExibirFiltros = (Boolean) filtro.get(RelatorioVariacaoVolumeAction.EXIBIR_FILTROS);
				parametros.put(RelatorioVariacaoVolumeAction.EXIBIR_FILTROS, isExibirFiltros);
			}

			Collection<PontoConsumo> listaPontosConsumo = null;
			if(filtro.containsKey(RelatorioVariacaoVolumeAction.PONTOS_CONSUMO)) {

				ControladorPontoConsumo controlPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
								.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
				Map<String, Object> filtroPontoConsumo = new HashMap<String, Object>();

				filtroPontoConsumo.put("chavesPrimarias", filtro.get(RelatorioVariacaoVolumeAction.PONTOS_CONSUMO));
				listaPontosConsumo = controlPontoConsumo.consultarPontosConsumo(filtroPontoConsumo);

			} else if(filtro.containsKey(RelatorioVariacaoVolumeAction.IMOVEL)) {
				ControladorPontoConsumo controlPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
								.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
				Long idImovel = (Long) filtro.get(RelatorioVariacaoVolumeAction.IMOVEL);
				listaPontosConsumo = removerPontoConsumoRepedido(controlPontoConsumo.listarPontoConsumoComContratoPorImovel(idImovel));
			} else if(filtro.containsKey(RelatorioVariacaoVolumeAction.CLIENTE)) {
				ControladorPontoConsumo controlPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
								.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
				Long idCliente = (Long) filtro.get(RelatorioVariacaoVolumeAction.CLIENTE);
				listaPontosConsumo = removerPontoConsumoRepedido(controlPontoConsumo.listarPontoConsumoComContratoPorCliente(idCliente));
			}

			// Concatena a descrição dos pontos de
			// consumo para mostrar no filtro do
			// relatório
			if(listaPontosConsumo != null && !listaPontosConsumo.isEmpty()) {
				StringBuilder pontosConsumo = new StringBuilder();
				Iterator<PontoConsumo> itPontoConsumo = listaPontosConsumo.iterator();
				while(itPontoConsumo.hasNext()) {
					pontosConsumo.append(itPontoConsumo.next().getDescricao());
					if(itPontoConsumo.hasNext()) {
						pontosConsumo.append(", ");
					}
				}
				parametros.put(RelatorioVariacaoVolumeAction.PARAM_PONTOS_CONSUMO, pontosConsumo.toString());
			}

			// insere a descrição do segmento no
			// filtro do relatório
			Segmento segmento = null;
			if(filtro.containsKey(RelatorioVariacaoVolumeAction.SEGMENTO) && filtro.get(RelatorioVariacaoVolumeAction.SEGMENTO) != null) {
				ControladorSegmento controlSegmento = (ControladorSegmento) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);
				Long chave = (Long) filtro.get(RelatorioVariacaoVolumeAction.SEGMENTO);
				if(chave != null && chave > 0) {
					segmento = (Segmento) controlSegmento.obter(chave);
					parametros.put(RelatorioVariacaoVolumeAction.PARAM_SEGMENTO, segmento.getDescricao());
				}
			}

			Collection<VariacaoVolumeVO> dados = null;
			dados = consultarVariacaoVolume(listaPontosConsumo, segmento, referencia);

			// Trata o Formato de Impressão
			FormatoImpressao formatoImpressao = null;
			if(filtro.containsKey(RelatorioVariacaoVolumeAction.FORMATO_IMPRESSAO)) {
				formatoImpressao = FormatoImpressao.valueOf((String) filtro.get(RelatorioVariacaoVolumeAction.FORMATO_IMPRESSAO));
			} else {
				throw new NegocioException("Não foi escolhido um formato para o relatório.");
			}

			// Logotipo Empresa
			ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
			Empresa cdlEmpresa = controladorEmpresa.obterEmpresaPrincipal();
			if(cdlEmpresa.getLogoEmpresa() != null) {
				parametros.put(IMAGEM, Constantes.URL_LOGOMARCA_EMPRESA + cdlEmpresa.getChavePrimaria());
			}

			if(dados != null && !dados.isEmpty()) {
				relatorio = RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_VARIACAO_VOLUME, formatoImpressao);
			} else {
				throw new NegocioException("Não existem dados para o filtro informado.");
			}
		}
		return relatorio;
	}

	/**
	 * Consultar variacao volume.
	 * 
	 * @param listaPontoConsumo
	 *            the lista ponto consumo
	 * @param segmento
	 *            the segmento
	 * @param referencia
	 *            the referencia
	 * @return the collection
	 */
	private Collection<VariacaoVolumeVO> consultarVariacaoVolume(Collection<PontoConsumo> listaPontoConsumo, Segmento segmento,
					Integer referencia) {

		Collection<VariacaoVolumeVO> dadosMesAtual = consultarVariacaoVolumeMensal(listaPontoConsumo, segmento, referencia);
		Collection<VariacaoVolumeVO> dadosMesAnterior = consultarVariacaoVolumeMensal(listaPontoConsumo, segmento, Util
						.regredirReferenciaCiclo(referencia, 1, 1).get("referencia"));

		Collection<VariacaoVolumeVO> dados = new ArrayList<VariacaoVolumeVO>();

		if((dadosMesAtual != null && !dadosMesAtual.isEmpty()) && (dadosMesAnterior != null && !dadosMesAnterior.isEmpty())) {

			for (VariacaoVolumeVO variacaoVolumeVO : dadosMesAtual) {
				for (VariacaoVolumeVO variacaoVolumeVOAnterior : dadosMesAnterior) {
					if(variacaoVolumeVOAnterior.getChavePontoConsumo().equals(variacaoVolumeVO.getChavePontoConsumo())) {
						variacaoVolumeVO.setDiaReferenciaAnterior(variacaoVolumeVOAnterior.getDiaReferencia());
						variacaoVolumeVO.setAnoMesReferenciaAnterior(variacaoVolumeVOAnterior.getAnoMesReferencia());
						variacaoVolumeVO.setVolumeReferenciaAnterior(variacaoVolumeVOAnterior.getVolumeReferenciaAnterior());

						BigDecimal variacao = BigDecimal.ZERO;
						variacao = variacaoVolumeVO.getVolumeReferencia().min(variacaoVolumeVO.getVolumeReferenciaAnterior())
										.divide(new BigDecimal(1000));
						variacaoVolumeVO.setVariacao(variacao);
					}
				}

				if(variacaoVolumeVO.getVolumeReferenciaAnterior()
								.divide(new BigDecimal(variacaoVolumeVO.getDiaReferenciaAnterior()), 5, RoundingMode.HALF_UP).floatValue() >= 1000) {
					dados.add(variacaoVolumeVO);
				}

			}

			// Logica de Remover
		}

		return dados;
	}

	/**
	 * Consultar variacao volume mensal.
	 * 
	 * @param listaPontoConsumo
	 *            the lista ponto consumo
	 * @param segmento
	 *            the segmento
	 * @param referencia
	 *            the referencia
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<VariacaoVolumeVO> consultarVariacaoVolumeMensal(Collection<PontoConsumo> listaPontoConsumo, Segmento segmento,
					Integer referencia) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select new br.com.ggas.relatorio.VariacaoVolumeVO ( ");
		hql.append("  pontoCons.chavePrimaria , pontoCons.imovel.nome , pontoCons.descricao, segm.descricao, ");
		hql.append("  sum(A.diasConsumo), sum(A.consumoApurado), A.anoMesFaturamento, ");
		hql.append("  sum(A.diasConsumo), sum(A.consumoApurado), A.anoMesFaturamento,  ");
		hql.append("  sum(A.consumoApurado) ");
		hql.append(" ) ");
		hql.append(" from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append("  as A ");
		hql.append(" , ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" as pontoCons ");
		hql.append(" , ");
		hql.append(getClasseEntidadeSegmento().getSimpleName());
		hql.append(" as segm ");

		hql.append(" where ");

		hql.append("     A.pontoConsumo.chavePrimaria = pontoCons.chavePrimaria ");
		hql.append(" and pontoCons.segmento.chavePrimaria = segm.chavePrimaria ");

		hql.append(" and A.indicadorFaturamento = true ");
		hql.append(" and A.habilitado = true ");
		hql.append(" and A.anoMesFaturamento = :referencia ");
		hql.append(" and A.indicadorConsumoCiclo = true ");

		Map<String, List<Long>> mapaPropriedades = new HashMap<>();
		if(listaPontoConsumo != null && !listaPontoConsumo.isEmpty()) {
			mapaPropriedades = HibernateHqlUtil.adicionarClausulaIn(hql, "A.pontoConsumo.chavePrimaria", "PT_CONS",
							Util.recuperarChavesPrimarias(listaPontoConsumo));
		}

		if(segmento != null) {
			hql.append(" and segm.chavePrimaria = :idSegmento ");
		}

		hql.append(" group by pontoCons.chavePrimaria , pontoCons.imovel.nome, ");
		hql.append(" 	  pontoCons.descricao, segm.descricao, A.anoMesFaturamento ");

		hql.append(" order by 2,6 ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setInteger("referencia", referencia);

		if(listaPontoConsumo != null && !listaPontoConsumo.isEmpty() && MapUtils.isNotEmpty(mapaPropriedades)) {
			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		}
		if(segmento != null) {
			query.setLong("idSegmento", segmento.getChavePrimaria());
		}

		return query.list();

	}

}
