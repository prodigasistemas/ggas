/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturaItemImpressao;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.relatorio.ControladorRelatorioVolumesFaturados;
import br.com.ggas.relatorio.RelatorioVolumesFaturadosVO;
import br.com.ggas.relatorio.SubRelatorioVolumesFaturadosVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.RelatorioVolumesFaturadosAction;

/**
 * 
 *
 */
class ControladorRelatorioVolumesFaturadosImpl extends ControladorNegocioImpl implements ControladorRelatorioVolumesFaturados {

	private static final String RELATORIO_VOLUMES_FATURADOS = "relatorioVolumesFaturados.jasper";

	public static final String ID_TIPO_RELATORIO = "idRelatorio";

	public static final String IMAGEM = "imagem";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	public Class<?> getClasseEntidadeHistoricoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeSegmento() {

		return ServiceLocator.getInstancia().getClassPorID(Segmento.BEAN_ID_SEGMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioVolumesFaturados#gerarRelatorioVolumesFaturados(java.util.Map)
	 */
	@Override
	public byte[] gerarRelatorioVolumesFaturados(Map<String, Object> filtro) throws NegocioException {

		byte[] relatorio;
		Map<String, Object> parametros = new HashMap<String, Object>();

		// Verifica se deve exibir Filtros
		Boolean isExibirFiltros = null;
		if (filtro.containsKey(RelatorioVolumesFaturadosAction.EXIBIR_FILTROS)) {

			isExibirFiltros = (Boolean) filtro.get(RelatorioVolumesFaturadosAction.EXIBIR_FILTROS);
			parametros.put(RelatorioVolumesFaturadosAction.EXIBIR_FILTROS, isExibirFiltros);

		}

		Collection<PontoConsumo> listaPontosConsumo = null;
		if (filtro.containsKey(RelatorioVolumesFaturadosAction.PONTOS_CONSUMO)) {

			ControladorPontoConsumo controlPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

			Map<String, Object> filtroPontoConsumo = new HashMap<String, Object>();
			filtroPontoConsumo.put("chavesPrimarias", filtro.get(RelatorioVolumesFaturadosAction.PONTOS_CONSUMO));
			listaPontosConsumo = controlPontoConsumo.consultarPontosConsumo(filtroPontoConsumo);

		} else if (filtro.containsKey(RelatorioVolumesFaturadosAction.IMOVEL)) {

			ControladorPontoConsumo controlPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
			ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
			Long idImovel = (Long) filtro.get(RelatorioVolumesFaturadosAction.IMOVEL);

			Imovel imovel = (Imovel) controladorImovel.obter(idImovel);
			listaPontosConsumo = controlPontoConsumo.listarPontoConsumoComContratoAtivoPorImovel(idImovel);
			parametros.put(RelatorioVolumesFaturadosAction.PARAM_IMOVEL, imovel.getNome());

		} else if (filtro.containsKey(RelatorioVolumesFaturadosAction.CLIENTE)) {

			ControladorPontoConsumo controlPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
			ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
			Long idCliente = (Long) filtro.get(RelatorioVolumesFaturadosAction.CLIENTE);

			Cliente cliente = (Cliente) controladorCliente.obter(idCliente);
			listaPontosConsumo = controlPontoConsumo.listarPontoConsumoPorCliente(idCliente);
			parametros.put(RelatorioVolumesFaturadosAction.PARAM_CLIENTE, cliente.getNome());
		}

		if (listaPontosConsumo != null && !listaPontosConsumo.isEmpty()) {

			StringBuilder pontosConsumo = new StringBuilder();
			Iterator<PontoConsumo> itPontoConsumo = listaPontosConsumo.iterator();

			while (itPontoConsumo.hasNext()) {

				pontosConsumo.append(itPontoConsumo.next().getDescricao());

				if (itPontoConsumo.hasNext()) {

					pontosConsumo.append(", ");

				}
			}
			parametros.put(RelatorioVolumesFaturadosAction.PARAM_PONTOS_CONSUMO, pontosConsumo.toString());
		}

		Segmento segmento = null;
		if (filtro.containsKey(RelatorioVolumesFaturadosAction.SEGMENTO) && filtro.get(RelatorioVolumesFaturadosAction.SEGMENTO) != null) {

			ControladorSegmento controlSegmento = (ControladorSegmento) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);
			Long chave = (Long) filtro.get(RelatorioVolumesFaturadosAction.SEGMENTO);

			if (chave != null && chave > 0) {

				segmento = (Segmento) controlSegmento.obter(chave);
				parametros.put(RelatorioVolumesFaturadosAction.PARAM_SEGMENTO, segmento.getDescricao());

			}
		}

		Date dataInicial = null;
		if (filtro.containsKey("dataEmissaoInicial")) {

			dataInicial = (Date) filtro.get("dataEmissaoInicial");
			parametros.put(RelatorioVolumesFaturadosAction.PARAM_DATA_INICIAL, DataUtil.converterDataParaString(dataInicial));

		} else {
			throw new NegocioException("Não foi informado a Data Inicial para o relatório.");
		}

		Date dataFinal = null;
		if (filtro.containsKey("dataEmissaoFinal")) {

			dataFinal = (Date) filtro.get("dataEmissaoFinal");
			parametros.put(RelatorioVolumesFaturadosAction.PARAM_DATA_FINAL, DataUtil.converterDataParaString(dataFinal));

		} else {
			throw new NegocioException("Não foi informado a Data Final para o relatório.");
		}

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoCreditoDebitoNormal = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_NORMAL);

		String codigoCreditoDebitoParcelada = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_PARCELADO);

		Long[] arraySituacao = {Long.valueOf(codigoCreditoDebitoNormal), Long.valueOf(codigoCreditoDebitoParcelada)};
		filtro.put(CREDITOS_DEBITO_SITUACAO, arraySituacao);

		// Trata o Formato de Impressão
		FormatoImpressao formatoImpressao = null;
		if (filtro.containsKey(RelatorioVolumesFaturadosAction.FORMATO_IMPRESSAO)) {
			formatoImpressao = (FormatoImpressao) filtro.get(RelatorioVolumesFaturadosAction.FORMATO_IMPRESSAO);
		} else {
			throw new NegocioException("Não foi escolhido um formato para o relatório.");
		}

		// Logotipo Empresa
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Empresa cdlEmpresa = controladorEmpresa.obterEmpresaPrincipal();
		if (cdlEmpresa.getLogoEmpresa() != null) {
			parametros.put(IMAGEM, Constantes.URL_LOGOMARCA_EMPRESA + cdlEmpresa.getChavePrimaria());
		}

		Collection<RelatorioVolumesFaturadosVO> dados = this.consultarVolumesFaturados(filtro);
						
		if (dados != null && !dados.isEmpty()) {
			SubRelatorioVolumesFaturadosVO subAux = dados.iterator().next().getColecaoVOs().iterator().next();
			
			DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
			symbols.setDecimalSeparator(',');
			symbols.setGroupingSeparator('.');

			String pattern = "#,##0.###";
			DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
			
			parametros.put("volumeTotalFaturado", decimalFormat.format(subAux.getTotalVolumeFaturado()));
			parametros.put("quantidadeDiasMes", subAux.getQuantidadeDiasMes().toString());
			parametros.put("valorTotalFaturado", decimalFormat.format(subAux.getTotalValorFaturado()));
			
			ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
					ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
			
			BigDecimal[] valorVolumeTotal = controladorFatura.consultarValorVolumeTotalPeriodo(
					(Date) filtro.get("dataEmissaoInicial"), (Date) filtro.get("dataEmissaoFinal"));
			
			parametros.put("valorTotalFaturas", decimalFormat.format(valorVolumeTotal[0]));
			parametros.put("volumeTotalFaturas", decimalFormat.format(valorVolumeTotal[1]));
			relatorio = RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_VOLUMES_FATURADOS, formatoImpressao);
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return relatorio;
	}

	/**
	 * Consultar volumes faturados.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unused")
	private Collection<RelatorioVolumesFaturadosVO> consultarVolumesFaturados(Map<String, Object> filtro) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		Collection<Fatura> colecaoFaturas = controladorFatura.consultarFaturasRelatorioVolume(filtro);

		Map<HistoricoConsumo, Collection<Fatura>> mapaHistConsFatura = controladorFatura.agruparFaturasPorHistoricoConsumo(colecaoFaturas);
		BigDecimal somaVolumes = BigDecimal.ZERO;
		BigDecimal somaValoresGas = BigDecimal.ZERO;
		BigDecimal somaValoresMargem = BigDecimal.ZERO;
		BigDecimal somaValoresTransp = BigDecimal.ZERO;
		BigDecimal somaVolumesFaturados = BigDecimal.ZERO;
		Integer quantidadeDiasMes = 1;
		BigDecimal somaValoresFaturados = BigDecimal.ZERO;
		
		String chaveGas = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS);
		String chaveMargem = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO);
		String chaveTransp = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TRANSPORTE);

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		EntidadeConteudo itemFaturaGas = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(chaveGas));
		EntidadeConteudo itemFaturaMargem = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(chaveMargem));
		EntidadeConteudo itemFaturaTransp = controladorEntidadeConteudo.obterEntidadeConteudo(Long.valueOf(chaveTransp));

		Map<Segmento, Collection<SubRelatorioVolumesFaturadosVO>> mapaSegmento = 
						new HashMap<Segmento, Collection<SubRelatorioVolumesFaturadosVO>>();
		for (Entry<HistoricoConsumo, Collection<Fatura>> entry : mapaHistConsFatura.entrySet()) {

			Collection<Fatura> listaFaturas = entry.getValue();

			DateTime dataInicial = new DateTime((Date)filtro.get("dataEmissaoInicial"));
			DateTime dataFinal = new DateTime((Date)filtro.get("dataEmissaoFinal"));

			
			if(dataInicial.getMonthOfYear() != dataFinal.getMonthOfYear()) {
				quantidadeDiasMes = dataInicial.toCalendar(Locale.getDefault()).getActualMaximum(Calendar.DAY_OF_MONTH)
						+ dataFinal.toCalendar(Locale.getDefault()).getActualMaximum(Calendar.DAY_OF_MONTH);
			} else {
				quantidadeDiasMes = dataInicial.toCalendar(Locale.getDefault()).getActualMaximum(Calendar.DAY_OF_MONTH);
			}

			BigDecimal qtdDiasPeriodo = BigDecimal.valueOf(entry.getKey().getDiasConsumo());

			EntidadeConteudo tipoOperacaoEntrada = controladorEntidadeConteudo.obterTipoOperacaoDocumentoFiscal(Long
							.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_ENTRADA)));

			for (Fatura fatura : listaFaturas) {
				DocumentoFiscal documentoFiscalEntrada = null;  //controladorFatura.obterDocumentoFiscalPorFatura(fatura, tipoOperacaoEntrada);
				if (documentoFiscalEntrada == null) {
					SubRelatorioVolumesFaturadosVO subRelVO = new SubRelatorioVolumesFaturadosVO();
					
					if (fatura.getPontoConsumo() != null) {

						if (fatura.getPontoConsumo().getImovel() != null
										&& (!StringUtils.isEmpty(fatura.getPontoConsumo().getImovel().getNome()) || !StringUtils
														.isEmpty(fatura.getPontoConsumo().getDescricao()))) {
							subRelVO.setNomeFantasia(fatura.getPontoConsumo().getImovel().getNome()
											.concat(" - ".concat(fatura.getPontoConsumo().getDescricao())));
						}

						if (!StringUtils.isEmpty(fatura.getPontoConsumo().getEnderecoFormatado())) {
							subRelVO.setEndereco(fatura.getPontoConsumo().getEnderecoFormatado());
						}

						if (fatura.getPontoConsumo().getCep() != null && fatura.getPontoConsumo().getCep().getMunicipio() != null) {
							subRelVO.setCidade(fatura.getPontoConsumo().getCep().getNomeMunicipio());
						}

						ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator
										.getInstancia().getControladorNegocio(
														ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

//						HistoricoMedicao historicoMedicao = controladorHistoricoMedicao.obterPrimeiroHistoricoMedicao(fatura
//										.getPontoConsumo().getChavePrimaria());
						
//						ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia()
//										.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
//						contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(fatura
//										.getPontoConsumo());

						
//						if ((contratoPontoConsumo != null) && (contratoPontoConsumo.getPeriodicidade().getQuantidadeDias() != null)) {
//							Integer periodicidadeDias = contratoPontoConsumo.getPeriodicidade().getQuantidadeDias();
//						}

//						if (historicoMedicao != null) {
//
//							subRelVO.setMesAnoCiclo(historicoMedicao
//											.getAnoMesCicloFormatado()
//											.toString()
//											.substring(4, 6)
//											.concat("/")
//											.concat(historicoMedicao.getAnoMesCicloFormatado().toString().substring(0, 4).concat("-")
//															.concat(historicoMedicao.getAnoMesCicloFormatado().toString().substring(6))));
//
//							String dataLeituraAnterior = null;
//
//							if (historicoMedicao.getDataLeituraAnterior() != null) {
//
//								dataLeituraAnterior = Util.converterDataParaStringSemHora(historicoMedicao.getDataLeituraAnterior(),
//												Constantes.FORMATO_DATA_BR);
//
//							} else if (historicoMedicao.getHistoricoInstalacaoMedidor() != null) {
//
//								dataLeituraAnterior = Util.converterDataParaStringSemHora(historicoMedicao.getHistoricoInstalacaoMedidor()
//												.getDataAtivacao(), Constantes.FORMATO_DATA_BR);
//							} else {
//
//								dataLeituraAnterior = "";
//							}
//
//							String dataFimLeituraAnterior = null;
//
//							if (historicoMedicao.getDataLeituraFaturada() != null) {
//
//								dataFimLeituraAnterior = Util.converterDataParaStringSemHora(historicoMedicao.getDataLeituraFaturada(),
//												Constantes.FORMATO_DATA_BR);
//
//							} else if (historicoMedicao.getDataLeituraInformada() != null) {
//
//								dataFimLeituraAnterior = Util.converterDataParaStringSemHora(historicoMedicao.getDataLeituraInformada(),
//												Constantes.FORMATO_DATA_BR);
//							} else {
//
//								dataFimLeituraAnterior = "";
//
//							}
//
//							subRelVO.setDataInicioConsumo(dataLeituraAnterior.concat(" - ".concat(dataFimLeituraAnterior)));
//
//						} else {
//							subRelVO.setDataInicioConsumo(" - ");
//							subRelVO.setMesAnoCiclo(" - ");
//						}


					}

					if (fatura.getCliente() != null && !StringUtils.isEmpty(fatura.getCliente().getNome())) {
						subRelVO.setRazaoSocial(fatura.getCliente().getNome());
					}

					if (fatura.getListaFaturaItem() != null && !fatura.getListaFaturaItem().isEmpty()) {
						Iterator<FaturaItem> iteratorFatura = fatura.getListaFaturaItem().iterator();
						FaturaItem faturaItemUnico = null;
						while(iteratorFatura.hasNext()) {
							faturaItemUnico	 = iteratorFatura.next();
							if(faturaItemUnico.getTarifa() != null) {
								break;
							}
						}
						
						BigDecimal quantidadeMetroCubico = BigDecimal.ZERO;
						quantidadeMetroCubico = faturaItemUnico.getQuantidade().setScale(0, BigDecimal.ROUND_HALF_UP);

						subRelVO.setMetroCubicoPeriodo(quantidadeMetroCubico);
						somaVolumes = somaVolumes.add(quantidadeMetroCubico);
						
						BigDecimal consumoApurado = BigDecimal.ONE;
						
						if (quantidadeMetroCubico.compareTo(BigDecimal.ZERO) == 0) {
							subRelVO.setMediaVolume(BigDecimal.ZERO);
						} else {
							consumoApurado = quantidadeMetroCubico;
							subRelVO.setMediaVolume(
									consumoApurado.divide(new BigDecimal(quantidadeDiasMes), 2, RoundingMode.HALF_UP));
						}
						
						subRelVO.setVolumeFaturado(consumoApurado.setScale(2, RoundingMode.HALF_UP));
						subRelVO.setVolumeMedido(fatura.getConsumoMedido());

						subRelVO.setTarifaMedia(fatura.getValorTotal()
								.divide(consumoApurado, 4, RoundingMode.HALF_UP));
						subRelVO.setSegmento(fatura.getPontoConsumo().getSegmento().getDescricao());
						subRelVO.setRamoAtividade(fatura.getPontoConsumo().getRamoAtividade().getDescricao());
						subRelVO.setClienteDesde(
								DataUtil.converterDataParaString(fatura.getContrato().getDataAssinatura()));
						subRelVO.setValorFaturado(fatura.getValorTotal());
						subRelVO.setImovel(fatura.getPontoConsumo().getImovel().getNome());
						subRelVO.setPontoConsumo(fatura.getPontoConsumo().getDescricao());
						
						if(fatura.getPontoConsumo().getCodigoLegado() != null) {
							subRelVO.setCodigoPontoConsumo(fatura.getPontoConsumo().getCodigoLegado());
						} else {
							subRelVO.setCodigoPontoConsumo(String.valueOf(fatura.getPontoConsumo().getChavePrimaria()));
						}
						
						if(faturaItemUnico.getTarifa() != null) {
							subRelVO.setTipoTarifa(faturaItemUnico.getTarifa().getDescricao());
						} else {
							Collection<FaturaItemImpressao> colecaoFaturaItem = controladorFatura
									.consultarFaturaItemImpressaoPorFaturaItem(faturaItemUnico);
							if(!colecaoFaturaItem.isEmpty() && colecaoFaturaItem.iterator().next().getTarifa() != null) {
								subRelVO.setTipoTarifa(colecaoFaturaItem.iterator().next().getTarifa().getDescricao());
							}
						}
						
						somaVolumesFaturados = somaVolumesFaturados.add(consumoApurado.setScale(2, RoundingMode.HALF_UP));
						somaValoresFaturados = somaValoresFaturados.add(fatura.getValorTotal());

						if (qtdDiasPeriodo.compareTo(BigDecimal.ZERO) > 0) {
							subRelVO.setMetroCubicoMediaDiaria(quantidadeMetroCubico.divide(qtdDiasPeriodo, RoundingMode.HALF_UP));
						}

						for (FaturaItem faturaItem : fatura.getListaFaturaItem()) {
							if (faturaItem.getRubrica() != null && faturaItem.getRubrica().getItemFatura() != null) {
								if (faturaItem.getRubrica().getItemFatura().equals(itemFaturaGas)) {
									subRelVO.setTarifaMediaGas(faturaItem.getValorUnitario());
									subRelVO.setValorMediaGas(faturaItem.getValorTotal());
									somaValoresGas = somaValoresGas.add(faturaItem.getValorTotal());
								} else if (faturaItem.getRubrica().getItemFatura().equals(itemFaturaMargem)) {
									subRelVO.setTarifaMediaMargem(faturaItem.getValorUnitario());
									subRelVO.setValorMediaMargem(faturaItem.getValorTotal());
									somaValoresMargem = somaValoresMargem.add(faturaItem.getValorTotal());
								} else if (faturaItem.getRubrica().getItemFatura().equals(itemFaturaTransp)) {
									subRelVO.setTarifaMediaTransporte(faturaItem.getValorUnitario());
									subRelVO.setValorMediaTransporte(faturaItem.getValorTotal());
									somaValoresTransp = somaValoresTransp.add(faturaItem.getValorTotal());
								}
							}
						}

						Collection<SubRelatorioVolumesFaturadosVO> lista = null;
						if (mapaSegmento.containsKey(fatura.getSegmento())) {
							lista = mapaSegmento.get(fatura.getSegmento());
						} else {
							lista = new ArrayList<SubRelatorioVolumesFaturadosVO>();
						}					
						lista.add(subRelVO);
						mapaSegmento.put(fatura.getSegmento(), lista);
					}
					
				}
			}
		}

		BigDecimal totalMetroCubicoMediaDiaria = BigDecimal.ZERO;
		BigDecimal totalMetroCubicoPeriodo = BigDecimal.ZERO;
		BigDecimal totalPercentualVolume = BigDecimal.ZERO;
		BigDecimal totalTarifaMediaGas = BigDecimal.ZERO;
		BigDecimal totalPercentualFaturamentoGas = BigDecimal.ZERO;
		BigDecimal totalTarifaMediaMargem = BigDecimal.ZERO;
		BigDecimal totalPercentualFaturamentoMargem = BigDecimal.ZERO;
		BigDecimal totalTarifaMediaTransporte = BigDecimal.ZERO;
		BigDecimal totalPercentualFaturamentoTransporte = BigDecimal.ZERO;

		Collection<RelatorioVolumesFaturadosVO> retorno = new ArrayList<RelatorioVolumesFaturadosVO>();
		for (Entry<Segmento, Collection<SubRelatorioVolumesFaturadosVO>> entry : mapaSegmento.entrySet()) {
			Segmento segmento = entry.getKey();
			RelatorioVolumesFaturadosVO relVolFaturado = new RelatorioVolumesFaturadosVO();
			relVolFaturado.setSegmento(segmento.getDescricao());
			Collection<SubRelatorioVolumesFaturadosVO> listaSubRel = entry.getValue();

			for (SubRelatorioVolumesFaturadosVO subRel : listaSubRel) {
				if (subRel.getMetroCubicoMediaDiaria() != null) {
					totalMetroCubicoMediaDiaria = totalMetroCubicoMediaDiaria.add(subRel.getMetroCubicoMediaDiaria());
					relVolFaturado.setTotalMetroCubicoMediaDiaria(totalMetroCubicoMediaDiaria);
				}

				if (subRel.getMetroCubicoPeriodo() != null) {
					totalMetroCubicoPeriodo = totalMetroCubicoPeriodo.add(subRel.getMetroCubicoPeriodo());
					relVolFaturado.setTotalMetroCubicoPeriodo(totalMetroCubicoPeriodo);

					if (totalMetroCubicoPeriodo.compareTo(BigDecimal.ZERO) > 0) {
						subRel.setPercentualVolume(subRel.getMetroCubicoPeriodo().divide(somaVolumes, 8, RoundingMode.HALF_UP)
										.multiply(BigDecimal.valueOf(100)).setScale(2, RoundingMode.HALF_UP));
					} else {
						subRel.setPercentualVolume(BigDecimal.ZERO.setScale(2));
					}
				}

				if (subRel.getPercentualVolume() != null) {
					totalPercentualVolume = totalPercentualVolume.add(subRel.getPercentualVolume());
				}

				if (subRel.getTarifaMediaGas() != null) {
					totalTarifaMediaGas = totalTarifaMediaGas.add(subRel.getTarifaMediaGas());
					relVolFaturado.setTotalTarifaMediaGas(totalTarifaMediaGas);

					if (somaValoresGas.compareTo(BigDecimal.ZERO) > 0) {
						subRel.setPercentualFaturamentoGas(subRel.getValorMediaGas().divide(somaValoresGas, 8, RoundingMode.HALF_UP)
										.multiply(BigDecimal.valueOf(100)).setScale(2, RoundingMode.HALF_UP));
					} else {
						subRel.setPercentualFaturamentoGas(BigDecimal.ZERO);
					}
				}

				if (subRel.getPercentualFaturamentoGas() != null) {
					totalPercentualFaturamentoGas = totalPercentualFaturamentoGas.add(subRel.getPercentualFaturamentoGas());
				}

				if (subRel.getTarifaMediaMargem() != null) {
					totalTarifaMediaMargem = totalTarifaMediaMargem.add(subRel.getTarifaMediaMargem());
					subRel.setTotalTarifaMediaMargem(totalTarifaMediaMargem);

					if (somaValoresMargem.compareTo(BigDecimal.ZERO) > 0) {
						subRel.setPercentualFaturamentoMargem(subRel.getValorMediaMargem()
										.divide(somaValoresMargem, 8, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100))
										.setScale(2, RoundingMode.HALF_UP));
					} else {
						subRel.setPercentualFaturamentoMargem(BigDecimal.ZERO);
					}
				}

				if (subRel.getPercentualFaturamentoMargem() != null) {
					totalPercentualFaturamentoMargem = totalPercentualFaturamentoMargem.add(subRel.getPercentualFaturamentoMargem());
					subRel.setTotalPercentualFaturamentoMargem(totalPercentualFaturamentoMargem);
				}

				if (subRel.getTarifaMediaTransporte() != null) {
					totalTarifaMediaTransporte = totalTarifaMediaTransporte.add(subRel.getTarifaMediaTransporte());
					subRel.setTotalTarifaMediaTransporte(totalTarifaMediaTransporte);

					if (somaValoresTransp.compareTo(BigDecimal.ZERO) > 0) {
						subRel.setPercentualFaturamentoTransporte(subRel.getValorMediaTransporte()
										.divide(somaValoresTransp, 8, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100))
										.setScale(2, RoundingMode.HALF_UP));
					} else {
						subRel.setPercentualFaturamentoTransporte(BigDecimal.ZERO);
					}
				}

				if (subRel.getPercentualFaturamentoTransporte() != null) {
					totalPercentualFaturamentoTransporte = totalPercentualFaturamentoTransporte.add(subRel
									.getPercentualFaturamentoTransporte());
					subRel.setTotalPercentualFaturamentoTransporte(totalPercentualFaturamentoTransporte);
				}
				subRel.setTotalVolumeFaturado(somaVolumesFaturados);
				subRel.setQuantidadeDiasMes(quantidadeDiasMes);
				subRel.setTotalValorFaturado(somaValoresFaturados);

			}
			relVolFaturado.setColecaoVOs(listaSubRel);
			retorno.add(relVolFaturado);

			totalMetroCubicoMediaDiaria = BigDecimal.ZERO;
			totalMetroCubicoPeriodo = BigDecimal.ZERO;
			totalPercentualVolume = BigDecimal.ZERO;
			totalTarifaMediaGas = BigDecimal.ZERO;
			totalPercentualFaturamentoGas = BigDecimal.ZERO;
			totalTarifaMediaMargem = BigDecimal.ZERO;
			totalPercentualFaturamentoMargem = BigDecimal.ZERO;
			totalTarifaMediaTransporte = BigDecimal.ZERO;
			totalPercentualFaturamentoTransporte = BigDecimal.ZERO;
		}
		return retorno;
	}

}
