/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividade;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.Mes;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioResumoVolume;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.resumoVolume.ResumoVolumeVO;

public class ControladorRelatorioResumoVolumeImpl extends ControladorNegocioImpl implements ControladorRelatorioResumoVolume {

	private static final String ID_CLIENTE = "idCliente";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String ANO = "ano";

	private static final String MES = "mes";

	private static final String ID_RAMO_ATIVIDADE = "idRamoAtividade";

	private static final String CHAVES_PONTO_CONSUMO = "chavesPontoConsumo";

	private static final String TIPO_EXIBICAO = "tipoExibicao";

	private static final String FORMATO_IMPRESSAO = "formatoImpressao";

	private static final String RELATORIO_RESUMO_VOLUME = "relatorioResumoVolume.jasper";

	private static final String RELATORIO_RESUMO_VOLUME_ANALITICO = "relatorioResumoVolumeAnalitico.jasper";

	private static final String CREDITO_DEBITO_SITUACAO_NORMAL = "CREDITO_DEBITO_SITUACAO_NORMAL";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private String anoMes = "";

	/**
	 * Método responsável por validar os filtros do relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void validarFiltroRelatorioResumoVolume(Map<String, Object> filtro) throws NegocioException {

		if(filtro != null) {
			String mes = (String) filtro.get(MES);
			String ano = (String) filtro.get(ANO);

			if(mes == null || ano == null) {
				throw new NegocioException(ControladorRelatorioResumoVolume.CAMPOS_OBRIGATORIOS, true);
			}
		}

	}

	/**
	 * Método responsável por gerar o relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	@Override
	public byte[] gerarRelatorioResumoVolume(Map<String, Object> filtro) throws NegocioException, FormatoInvalidoException {

		Map<String, Object> parametros = new HashMap<String, Object>();
		if(this.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(this.obterEmpresaPrincipal().getChavePrimaria()));
		}

		this.montarParametroRelatorio(parametros, filtro);

		Collection<ResumoVolumeVO> collResumoVolumeVO = new ArrayList<ResumoVolumeVO>();
		byte[] relatorioResumoVolume;
		String tipoExibicao = "";

		if("sintetico".equals(filtro.get(TIPO_EXIBICAO))) {
			collResumoVolumeVO = montarRelatorioSinteticoResumoVolume(filtro);
			tipoExibicao = RELATORIO_RESUMO_VOLUME;
		} else if("analitico".equals(filtro.get(TIPO_EXIBICAO))) {

			collResumoVolumeVO = montarRelatorioAnaliticoResumoVolume(filtro);
			tipoExibicao = RELATORIO_RESUMO_VOLUME_ANALITICO;
		}

		if (collResumoVolumeVO != null && !collResumoVolumeVO.isEmpty()) {
			relatorioResumoVolume = RelatorioUtil.gerarRelatorio(collResumoVolumeVO, parametros, tipoExibicao,
							(FormatoImpressao) filtro.get(FORMATO_IMPRESSAO));
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return relatorioResumoVolume;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioResumoVolume#obterEmpresaPrincipal()
	 */
	@Override
	public Empresa obterEmpresaPrincipal() throws NegocioException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("principal", Boolean.TRUE);
		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);

		Empresa empresa = null;
		if((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
			empresa = ((List<Empresa>) listaEmpresas).get(0);
		}

		return empresa;
	}

	/**
	 * Método responsável por montar os parametros
	 * do relatorio de acordo com os
	 * filtros selecionados pelo usuário.
	 * 
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public void montarParametroRelatorio(Map<String, Object> parametros, Map<String, Object> filtro) throws NegocioException {

		boolean filtroSelecionado = false;
		if(filtro != null) {
			Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
			if((idSegmento != null) && (idSegmento > 0)) {
				ControladorSegmento controladorSegmento = (ControladorSegmento) ServiceLocator.getInstancia().getBeanPorID(
								ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);
				Segmento segmento = controladorSegmento.obterSegmento(idSegmento);
				parametros.put("filtroSegmento", segmento.getDescricao());
				filtroSelecionado = true;
			}

			Long idRamoAtividade = (Long) filtro.get(ID_RAMO_ATIVIDADE);
			if((idRamoAtividade != null) && (idRamoAtividade > 0)) {
				ControladorRamoAtividade controladorRamoAtividade = (ControladorRamoAtividade) ServiceLocator.getInstancia().getBeanPorID(
								ControladorRamoAtividade.BEAN_ID_CONTROLADOR_RAMO_ATIVIDADE);
				RamoAtividade ramoAtividade = (RamoAtividade) controladorRamoAtividade.obter(idRamoAtividade);
				parametros.put("filtroRamoAtividade", ramoAtividade.getDescricao());
				filtroSelecionado = true;
			}

			Long idCliente = (Long) filtro.get(ID_CLIENTE);
			if((idCliente != null) && (idCliente > 0)) {
				ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getBeanPorID(
								ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
				Cliente cliente = (Cliente) controladorCliente.obter(idCliente);
				parametros.put("filtroCliente", cliente.getNome());
				filtroSelecionado = true;
			}

			String ano = (String) filtro.get(ANO);
			if((ano != null) && (!ano.isEmpty())) {
				parametros.put("filtroAno", ano);
				filtroSelecionado = true;
			}

			String mes = (String) filtro.get(MES);
			String descricaoMes = null;
			if(mes != null) {
				descricaoMes = Mes.MESES_ANO.get(Integer.valueOf(mes));
			}

			if((mes != null) && (!mes.isEmpty()) && descricaoMes != null && !descricaoMes.isEmpty()) {
				parametros.put("filtroMes", descricaoMes);
				filtroSelecionado = true;
			}

			if((ano != null) && (!ano.isEmpty()) && (mes != null) && (!mes.isEmpty()) && descricaoMes != null && !descricaoMes.isEmpty()) {
				String subTitulo = "Informações de ".concat(descricaoMes).concat(" de ").concat(ano);
				parametros.put("subTitulo", subTitulo);
			}

			Long[] chavesPontoConsumo = (Long[]) filtro.get(CHAVES_PONTO_CONSUMO);
			StringBuilder descricaoPontoConsumo = new StringBuilder();
			if((chavesPontoConsumo != null) && (chavesPontoConsumo.length > 0)) {
				ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getBeanPorID(
								ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
				for (int i = 0; i < chavesPontoConsumo.length; i++) {
					PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(chavesPontoConsumo[i]);
					if(i + 1 == chavesPontoConsumo.length) {
						descricaoPontoConsumo.append(pontoConsumo.getDescricao());
					} else {
						descricaoPontoConsumo.append(pontoConsumo.getDescricao()).append(", ");
					}
				}
				parametros.put("filtroPontoConsumo", descricaoPontoConsumo.toString());
				filtroSelecionado = true;
			}

			Boolean exibirFiltros = (Boolean) filtro.get(EXIBIR_FILTROS);
			if(exibirFiltros != null && exibirFiltros && filtroSelecionado) {
				parametros.put("exibirFiltros", filtroSelecionado);
			}
		}
	}

	/**
	 * Método responsável por montar uma lista com
	 * o anoMesReferencia e os anoMesReferencia
	 * anteriores.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return List<Integer>
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public List<Integer> verificarAnoMes(Map<String, Object> filtro) throws FormatoInvalidoException, NegocioException {

		List<Integer> auxListaInteger = new ArrayList<Integer>();
		String ano = (String) filtro.get(ANO);
		String mes = (String) filtro.get(MES);
		Integer anoMesInteiro = 0;

		if(mes.length() == 1) {
			mes = "0".concat(mes);
		}

		anoMes = ano.concat(mes);

		if("01".equals(mes)) {
			anoMesInteiro = Util.converterCampoStringParaValorAnoMes("", anoMes);
			auxListaInteger.add(anoMesInteiro);
		} else {
			int count = 1;
			List<String> auxLista = new ArrayList<String>();
			String temp = "";
			while(!temp.equals(mes)) {
				if("09".equals(temp) || "10".equals(temp) || "11".equals(temp) || "12".equals(temp)) {
					temp = "" + count;
				} else {
					temp = "0" + count;
				}
				auxLista.add(ano.concat(temp));
				count++;
			}
			for (String auxListaElem : auxLista) {
				auxListaInteger.add(Util.converterCampoStringParaValorAnoMes("", auxListaElem));
			}
		}
		return auxListaInteger;
	}

	/**
	 * Método responsável por montar o relatorio
	 * analitico de resumo volume.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return Collection<ResumoVolumeVO>
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	@Override
	public Collection<ResumoVolumeVO> montarRelatorioAnaliticoResumoVolume(Map<String, Object> filtro) throws NegocioException,
					FormatoInvalidoException {

		Collection<ResumoVolumeVO> collVO = new ArrayList<ResumoVolumeVO>();
		BigDecimal totalPontoConsumo = BigDecimal.ZERO;
		BigDecimal totalVolumeNoMes = BigDecimal.ZERO;
		BigDecimal totalVolumeAteMes = BigDecimal.ZERO;
		BigDecimal totalVolumeNoMesPorDia = BigDecimal.ZERO;
		BigDecimal totalVolumeAteMesPorDia = BigDecimal.ZERO;
		Boolean possuiAnoMes = false;

		this.prepararFiltro(filtro);

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		List<Fatura> listaFaturasDuplicadas = (List<Fatura>) controladorFatura.consultarFatura(filtro);

		for (Fatura fatura : listaFaturasDuplicadas) {
			if(fatura.getAnoMesReferencia().equals(Integer.parseInt(anoMes))) {
				possuiAnoMes = true;
				break;
			}
		}

		if(!possuiAnoMes) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		Map<HistoricoConsumo, Collection<Fatura>> listaFaturas = controladorFatura
						.agruparFaturasPorHistoricoConsumo(listaFaturasDuplicadas);

		Collection<Collection<Fatura>> colecaoFaturas = listaFaturas.values();
		List<Fatura> listaFatura = new ArrayList<Fatura>();

		for (Collection<Fatura> collFaturas : colecaoFaturas) {
			listaFatura.add(collFaturas.iterator().next());
		}

		ResumoVolumeVO resumoVolumeVO = new ResumoVolumeVO();
		List<Long> chaveCliente = new ArrayList<Long>();
		for (Fatura fatura : listaFatura) {
			if(!chaveCliente.contains(fatura.getCliente().getChavePrimaria())) {
				chaveCliente.add(fatura.getCliente().getChavePrimaria());
			}
		}

		List<Long> chavePontoConsumo = new ArrayList<Long>();
		for (Fatura fatura : listaFatura) {
			if(!chavePontoConsumo.contains(fatura.getPontoConsumo().getChavePrimaria())) {
				chavePontoConsumo.add(fatura.getPontoConsumo().getChavePrimaria());
			}
		}

		BigDecimal volumeNoMes = BigDecimal.ZERO;
		BigDecimal volumeAteMes = BigDecimal.ZERO;
		List<Integer> auxListaInteger = this.verificarAnoMes(filtro);

		for (Long chaveCli : chaveCliente) {
			for (Long chavePontoCons : chavePontoConsumo) {
				volumeAteMes = BigDecimal.ZERO;
				for (Integer periodoAnoMes : auxListaInteger) {
					volumeNoMes = BigDecimal.ZERO;
					for (Fatura fatura : listaFatura) {
						if(fatura.getCliente().getChavePrimaria() == chaveCli && fatura.getAnoMesReferencia().equals(periodoAnoMes)
										&& fatura.getPontoConsumo() != null
										&& fatura.getPontoConsumo().getChavePrimaria() == chavePontoCons) {
							// TKT# 4201
							volumeNoMes = BigDecimal.ZERO;
							Collection<FaturaItem> listaFaturaItem = fatura.getListaFaturaItem();
							if(listaFaturaItem != null && !listaFaturaItem.isEmpty()) {
								for (FaturaItem faturaItem : listaFaturaItem) {
									resumoVolumeVO = new ResumoVolumeVO();
									if(faturaItem.getMedidaConsumo() != null) {
										volumeNoMes = volumeNoMes.add(faturaItem.getMedidaConsumo());
									} else {
										volumeNoMes = volumeNoMes.add(faturaItem.getQuantidade());
									}

									resumoVolumeVO.setNomeCliente(fatura.getCliente().getNome());
									resumoVolumeVO.setDescricaoSegmento(fatura.getSegmento().getDescricao());

									resumoVolumeVO.setVolumeNoMes(volumeNoMes);
									resumoVolumeVO.setReferenciaCiclo(fatura.getCicloReferenciaFormatado());

									if(fatura.getPontoConsumo() != null) {
										if((fatura.getPontoConsumo().getImovel() != null)
														&& (fatura.getPontoConsumo().getImovel().getNome() != null)
														&& (!"".equals(fatura.getPontoConsumo().getImovel().getNome().trim()))) {

											resumoVolumeVO.setDescricaoPontoConsumo(fatura.getPontoConsumo().getImovel().getNome() + " - "
															+ fatura.getPontoConsumo().getDescricao());
										} else {
											resumoVolumeVO.setDescricaoPontoConsumo(fatura.getPontoConsumo().getDescricao());
										}

									}
									volumeAteMes = volumeAteMes.add(volumeNoMes);
									resumoVolumeVO.setVolumeAteMes(volumeAteMes);

									if(resumoVolumeVO.getNomeCliente() != null) {
										collVO.add(resumoVolumeVO);
									}
								}
							}
						}
					}
				}
			}
		}

		resumoVolumeVO.setTotalVolumeNoMes(totalVolumeNoMes);
		resumoVolumeVO.setTotalVolumeAteMes(totalVolumeAteMes);

		resumoVolumeVO.setTotalVolumeNoMesPorDia(totalVolumeNoMesPorDia);
		resumoVolumeVO.setTotalVolumeAteMesPorDia(totalVolumeAteMesPorDia);
		resumoVolumeVO.setTotalPontoConsumo(totalPontoConsumo);

		return collVO;
	}

	/**
	 * Método responsável por montar o relatorio
	 * sintetico de resumo volume.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return Collection<ResumoVolumeVO>
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	@Override
	public Collection<ResumoVolumeVO> montarRelatorioSinteticoResumoVolume(Map<String, Object> filtro) throws NegocioException,
					FormatoInvalidoException {

		Collection<ResumoVolumeVO> collVO = new ArrayList<ResumoVolumeVO>();
		this.prepararFiltro(filtro);

		Boolean possuiAnoMes = false;

		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		List<Fatura> listaFaturasDuplicadas = (List<Fatura>) controladorFatura.consultarFatura(filtro);
		for (Fatura fatura : listaFaturasDuplicadas) {
			if(fatura.getAnoMesReferencia().equals(Integer.parseInt(anoMes))) {
				possuiAnoMes = true;
				break;
			}
		}

		if(!possuiAnoMes) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		Map<HistoricoConsumo, Collection<Fatura>> listaFaturas = controladorFatura
						.agruparFaturasPorHistoricoConsumo(listaFaturasDuplicadas);

		Collection<Collection<Fatura>> colecaoFaturas = listaFaturas.values();
		List<Fatura> listaFatura = new ArrayList<Fatura>();

		for (Collection<Fatura> collFaturas : colecaoFaturas) {
			listaFatura.add(collFaturas.iterator().next());
		}

		ResumoVolumeVO resumoVolumeVO = new ResumoVolumeVO();
		BigDecimal quantidadePontoConsumo = BigDecimal.ZERO;
		BigDecimal totalPontoConsumo = BigDecimal.ZERO;
		BigDecimal totalVolumeNoMes = BigDecimal.ZERO;
		BigDecimal totalVolumeAteMes = BigDecimal.ZERO;
		BigDecimal totalVolumeNoMesPorDia = BigDecimal.ZERO;
		BigDecimal totalVolumeAteMesPorDia = BigDecimal.ZERO;

		ControladorSegmento controladorSegmento = (ControladorSegmento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);

		Collection<Segmento> listaSegmento = controladorSegmento.listarSegmento();
		List<Fatura> listaFaturaSegmento = new ArrayList<Fatura>();
		if (listaFatura != null && !listaFatura.isEmpty()) {
			if (filtro.get(ID_SEGMENTO) == null) {
				for (Segmento segmento : listaSegmento) {
					resumoVolumeVO = new ResumoVolumeVO();
					resumoVolumeVO.setDescricaoSegmento(segmento.getDescricao());
					quantidadePontoConsumo = BigDecimal.ZERO;

					for (Fatura fatura : listaFatura) {
						if(fatura.getPontoConsumo() != null && fatura.getSegmento().getChavePrimaria() == segmento.getChavePrimaria()) {
							quantidadePontoConsumo = quantidadePontoConsumo.add(new BigDecimal("1"));
							listaFaturaSegmento.add(fatura);
						}
					}

					totalPontoConsumo = totalPontoConsumo.add(quantidadePontoConsumo);
					resumoVolumeVO.setQuantidadePontoConsumo(quantidadePontoConsumo);

					if(listaFaturaSegmento != null && listaFaturaSegmento.isEmpty()) {
						resumoVolumeVO.setVolumeNoMes(new BigDecimal("0"));
						resumoVolumeVO.setVolumeAteMes(new BigDecimal("0"));
						resumoVolumeVO.setVolumeNoMesPorDia(new BigDecimal("0"));
						resumoVolumeVO.setVolumeAteMesPorDia(new BigDecimal("0"));

						resumoVolumeVO.setTotalVolumeNoMes(new BigDecimal("0"));
						resumoVolumeVO.setTotalVolumeNoMesPorDia(new BigDecimal("0"));
						resumoVolumeVO.setTotalVolumeAteMes(new BigDecimal("0"));
						resumoVolumeVO.setTotalVolumeAteMesPorDia(new BigDecimal("0"));
						collVO.add(resumoVolumeVO);
					} else {
						preencherResumoVolumeVO(listaFaturaSegmento, resumoVolumeVO, anoMes);
						listaFaturaSegmento = new ArrayList<Fatura>();

						totalVolumeNoMes = totalVolumeNoMes.add(resumoVolumeVO.getVolumeNoMes());
						totalVolumeAteMes = totalVolumeAteMes.add(resumoVolumeVO.getVolumeAteMes());

						totalVolumeNoMesPorDia = totalVolumeNoMesPorDia.add(resumoVolumeVO.getVolumeNoMesPorDia());
						totalVolumeAteMesPorDia = totalVolumeAteMesPorDia.add(resumoVolumeVO.getVolumeAteMesPorDia());

						collVO.add(resumoVolumeVO);
					}
				}
			} else {
				resumoVolumeVO = new ResumoVolumeVO();

				for (Segmento segmento : listaSegmento) {
					if((Long) filtro.get(ID_SEGMENTO) == segmento.getChavePrimaria()) {
						resumoVolumeVO.setDescricaoSegmento(segmento.getDescricao());
					}
				}

				resumoVolumeVO.setQuantidadePontoConsumo(new BigDecimal(listaFatura.size()));
				totalPontoConsumo = new BigDecimal(listaFatura.size());

				preencherResumoVolumeVO(listaFatura, resumoVolumeVO, anoMes);

				totalVolumeNoMes = totalVolumeNoMes.add(resumoVolumeVO.getVolumeNoMes());
				totalVolumeAteMes = totalVolumeAteMes.add(resumoVolumeVO.getVolumeAteMes());

				totalVolumeNoMesPorDia = totalVolumeNoMesPorDia.add(resumoVolumeVO.getVolumeNoMesPorDia());
				totalVolumeAteMesPorDia = totalVolumeAteMesPorDia.add(resumoVolumeVO.getVolumeAteMesPorDia());
				collVO.add(resumoVolumeVO);
			}

			resumoVolumeVO.setTotalVolumeNoMes(totalVolumeNoMes);
			resumoVolumeVO.setTotalVolumeAteMes(totalVolumeAteMes);

			resumoVolumeVO.setTotalVolumeNoMesPorDia(totalVolumeNoMesPorDia);
			resumoVolumeVO.setTotalVolumeAteMesPorDia(totalVolumeAteMesPorDia);
			resumoVolumeVO.setTotalPontoConsumo(totalPontoConsumo);
		}
		return collVO;
	}

	/**
	 * Método responsável por montar preencher a
	 * entidade ResumoVolumeVO do relatorio
	 * sintetico Resumo Volume.
	 * 
	 * @param listaFatura
	 *            the lista fatura
	 * @param resumoVolumeVO
	 *            the resumo volume vo
	 * @param anoMes
	 *            the ano mes
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	@Override
	public void preencherResumoVolumeVO(List<Fatura> listaFatura, ResumoVolumeVO resumoVolumeVO, String anoMes)
					throws FormatoInvalidoException {

		BigDecimal volumeNoMes = BigDecimal.ZERO;
		BigDecimal volumeAteMes = BigDecimal.ZERO;
		BigDecimal volumeNoMesPorDia = BigDecimal.ZERO;
		BigDecimal volumeAteMesPorDia = BigDecimal.ZERO;
		BigDecimal ultimoDiaNoMes = BigDecimal.ZERO;
		BigDecimal ultimoDiaAteMes = BigDecimal.ZERO;
		Integer anoMesInteiro = 0;

		for (Fatura fatura : listaFatura) {
			Collection<FaturaItem> listaFaturaItem = fatura.getListaFaturaItem();
			if(listaFaturaItem != null && !listaFaturaItem.isEmpty()) {
				for (FaturaItem faturaItem : listaFaturaItem) {
					anoMesInteiro = Util.converterCampoStringParaValorAnoMes("", anoMes);
					if(anoMesInteiro.equals(fatura.getAnoMesReferencia())) {
						if(faturaItem.getMedidaConsumo() != null) {
							volumeNoMes = volumeNoMes.add(faturaItem.getMedidaConsumo());
						} else {
							volumeNoMes = volumeNoMes.add(faturaItem.getQuantidade());
						}
						ultimoDiaNoMes = Util.obterUltimoDiaMes(anoMes.substring(0, 3), anoMes.substring(4));
					} else {

						if(faturaItem.getMedidaConsumo() != null) {
							volumeAteMes = volumeAteMes.add(faturaItem.getMedidaConsumo());
						} else {
							volumeAteMes = volumeAteMes.add(faturaItem.getQuantidade());
						}

						String anoReferencia = fatura.getAnoMesReferencia() + "";
						String mesReferencia = fatura.getAnoMesReferencia() + "";

						ultimoDiaAteMes = ultimoDiaAteMes.add(Util.obterUltimoDiaMes(anoReferencia.substring(0, 4),
										mesReferencia.substring(4)));
					}
				}
			}
		}

		if(volumeAteMes.compareTo(BigDecimal.ZERO) > 0) {
			volumeAteMes = volumeAteMes.add(volumeNoMes);
		}

		if(ultimoDiaNoMes.compareTo(BigDecimal.ZERO) > 0) {
			volumeNoMesPorDia = volumeNoMes.divide(ultimoDiaNoMes, RoundingMode.HALF_UP);

			if(ultimoDiaNoMes.compareTo(BigDecimal.ZERO) > 0) {
				ultimoDiaAteMes = ultimoDiaAteMes.add(ultimoDiaNoMes);
			}
		}

		if(ultimoDiaAteMes.compareTo(BigDecimal.ZERO) > 0) {
			volumeAteMesPorDia = volumeAteMes.divide(ultimoDiaAteMes, RoundingMode.HALF_UP);
		}

		resumoVolumeVO.setVolumeNoMes(volumeNoMes);
		resumoVolumeVO.setVolumeAteMes(volumeAteMes);

		resumoVolumeVO.setVolumeNoMesPorDia(volumeNoMesPorDia);
		resumoVolumeVO.setVolumeAteMesPorDia(volumeAteMesPorDia);
	}

	/**
	 * Método responsável por preparar os filtros
	 * do relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void prepararFiltro(Map<String, Object> filtro) throws FormatoInvalidoException, NegocioException {

		List<Integer> auxListaInteger = this.verificarAnoMes(filtro);
		if (auxListaInteger != null && !auxListaInteger.isEmpty()) {
			filtro.put("listaAnoMesReferencia", auxListaInteger);
		}

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoCreditoDebitoSituacaoNormal = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(CREDITO_DEBITO_SITUACAO_NORMAL);

		filtro.put("idCreditoDebitoSituacao", Long.valueOf(codigoCreditoDebitoSituacaoNormal));

		String codigoTipoDocumento = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);

		filtro.put("idTipoDocumento", Long.valueOf(codigoTipoDocumento));

		Long codigoFaturaItemGas = Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));
		Long codigoFaturaItemTransporte = Long.parseLong(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TRANSPORTE));
		Long codigoFaturaItemMargem = Long.parseLong(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO));

		List<Long> auxListaRubricaInteger = new ArrayList<Long>();
		auxListaRubricaInteger.add(codigoFaturaItemGas);
		auxListaRubricaInteger.add(codigoFaturaItemTransporte);
		auxListaRubricaInteger.add(codigoFaturaItemMargem);

		filtro.put("listaRubrica", auxListaRubricaInteger);

		filtro.put("ordenacaoCliente", true);
		filtro.put("ordenacaoPontoConsumo", true);
		filtro.put("ordenacaoSegmento", true);
		filtro.put("ordenacaoAnoMesReferencia", true);
		filtro.put("ordenacaoNumeroCiclo", true);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (Fatura) ServiceLocator.getInstancia().getBeanPorID(Fatura.BEAN_ID_FATURA);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}
}
