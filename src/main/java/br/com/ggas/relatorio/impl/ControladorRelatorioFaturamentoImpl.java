/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioFaturamento;
import br.com.ggas.util.*;
import br.com.ggas.web.relatorio.faturamento.RelatorioFaturamentoVO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class ControladorRelatorioFaturamentoImpl extends ControladorNegocioImpl implements ControladorRelatorioFaturamento {

	private static final String RELATORIO_FATURAMENTO = "relatorioFaturamento.jasper";

	private static final String ID_CLIENTE = "idCliente";

	private static final String CHAVES_PONTO_CONSUMO = "chavesPontoConsumo";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String ERRO_NEGOCIO_RELATORIO_SEM_DADOS = "ERRO_NEGOCIO_RELATORIO_SEM_DADOS";

	private static final String ID_CONTRATO = "idContrato";

	private static final String IDS_CONTRATO = "idsContrato";

	private static final String DATA_EMISSAO_INICIAL = "dataEmissaoInicial";

	private static final String DATA_EMISSAO_FINAL = "dataEmissaoFinal";

	private static final String DATA_VENCIMENTO_INICIAL = "dataVencimentoInicial";

	private static final String DATA_VENCIMENTO_FINAL = "dataVencimentoFinal";

	private static final String ERRO_NEGOCIO_DATA_EMISSAO_INICIO_MAIOR_DATA_EMISSAO_FINAL =
					"ERRO_NEGOCIO_DATA_EMISSAO_INICIO_MAIOR_DATA_EMISSAO_FINAL";

	private static final String ERRO_NEGOCIO_DATA_VENCIMENTO_INICIO_DATA_VENCIMENTO_FINAL =
					"ERRO_NEGOCIO_DATA_VENCIMENTO_INICIO_DATA_VENCIMENTO_FINAL";

	private static final String NUMERO_NOTA_FISCAL_INICIAL = "numeroNotaFiscalInicial";

	private static final String NUMERO_NOTA_FISCAL_FINAL = "numeroNotaFiscalFinal";

	private static final String ERRO_NEGOCIO_NUMERO_NOTA_FISCAL_INICIO_FINAL = "ERRO_NEGOCIO_NUMERO_NOTA_FISCAL_INICIO_FINAL";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupo";

	private static final String ID_ROTA = "idRota";

	private static final String ANO_MES_REFERENCIA = "anoMesReferencia";

	private static final String NUMERO_CICLO = "numeroCiclo";

	private static final String FILTRO_ID_GRUPO_FATURAMENTO = "filtroIdGrupo";

	private static final String FILTRO_ID_ROTA = "filtroIdRota";

	private static final String FILTRO_ANO_MES_REFERENCIA = "filtroAnoMesReferencia";

	private static final String FILTRO_NUMERO_CICLO = "filtroNumeroCiclo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioFaturamento#gerarRelatorio(java.util.Map, br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorio(Map<String, Object> filtro, FormatoImpressao formatoImpressao,
					Boolean exibirFiltros) throws GGASException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Map<String, Object> parametros = new HashMap<String, Object>();
		if (controladorEmpresa.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem",
							Constantes.URL_LOGOMARCA_EMPRESA
											+ String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		}

		if(exibirFiltros != null && exibirFiltros){
			montarParametroRelatorio(parametros, filtro);
		}

		Collection<RelatorioFaturamentoVO> listaRelatorioFaturamentoVO = this.montarListaDadosRelatorio(filtro);

		return RelatorioUtil.gerarRelatorio(listaRelatorioFaturamentoVO, parametros, RELATORIO_FATURAMENTO, formatoImpressao);
	}

	/**
	 * Montar lista dados relatorio.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<RelatorioFaturamentoVO> montarListaDadosRelatorio(Map<String, Object> filtro) throws GGASException {

		Collection<RelatorioFaturamentoVO> listaRelatorioFaturamentoVO = new ArrayList<RelatorioFaturamentoVO>();
		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ConstanteSistema constanteRubrica = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_RUBRICA_CONSUMO_GAS);

		ControladorRubrica controladorRubrica = (ControladorRubrica) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorRubrica.BEAN_ID_CONTROLADOR_RUBRICA);

		Rubrica rubricaConsumoGas = (Rubrica) controladorRubrica.obter(Long.parseLong(constanteRubrica.getValor()));

		validarFiltros(filtro);

		Collection<Fatura> listaFaturas = controladorFatura.consultarFaturaRelatorio(filtro);
		Long idTributoIcms = Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ICMS));
		if (!listaFaturas.isEmpty()) {
			for (Fatura fatura : listaFaturas) {
				RelatorioFaturamentoVO relatorioFaturamentoVO = new RelatorioFaturamentoVO();

				if (fatura.getCliente() != null) {
					relatorioFaturamentoVO.setCodigoCliente(String.valueOf(fatura.getCliente().getChavePrimaria()));
					relatorioFaturamentoVO.setNomeFantasia(fatura.getCliente().getNomeFantasia());
					relatorioFaturamentoVO.setRazaoSocial(fatura.getCliente().getNome());
				}

				if (fatura.getContratoAtual() != null) {
					relatorioFaturamentoVO.setContrato(fatura.getContratoAtual().getNumeroFormatado());
				}
				if (fatura.getSegmento() != null) {
					relatorioFaturamentoVO.setSegmento(fatura.getSegmento().getDescricao());
				}
				Long numerodocumentoFiscal = controladorFatura.obterNumeroDocumentoFiscalPorFaturaMaisRecente(fatura.getChavePrimaria());
				if (numerodocumentoFiscal != null) {
					relatorioFaturamentoVO.setNumeroNF(String.valueOf(numerodocumentoFiscal));
				}

				relatorioFaturamentoVO.setDataEmissao(fatura.getDataEmissao());
				relatorioFaturamentoVO.setDataVencimento(fatura.getDataVencimento());
				relatorioFaturamentoVO.setValorIcms(controladorFatura.consultaValorDeImpostoDeFaturaTributacaoPorFatura(fatura
						.getChavePrimaria(), idTributoIcms));

				Collection<HistoricoConsumo> historicosConsumo =
						controladorFatura.consultarHistoricoConsumoPorFaturaAgrupamento(fatura.getChavePrimaria());
				if(historicosConsumo!=null && !historicosConsumo.isEmpty()){
					populaDadosDeConsumoFaturaAgrupada(relatorioFaturamentoVO, historicosConsumo);
					relatorioFaturamentoVO.setTarifaUnitaria(controladorFatura
							.obterValorUnitarioDeFaturaItem(fatura.getChavePrimaria(), rubricaConsumoGas.getChavePrimaria()));

				}else if (fatura.getHistoricoConsumo() != null) {
					relatorioFaturamentoVO.setVolumeFaturado(fatura.getHistoricoConsumo().getConsumoApurado());
					relatorioFaturamentoVO.setQtdDias(fatura.getHistoricoConsumo().getDiasConsumo());
					if (fatura.getHistoricoConsumo().getConsumoApurado() != null
									&& fatura.getHistoricoConsumo().getConsumoApurado().compareTo(new BigDecimal(0)) != 0
									&& fatura.getHistoricoConsumo().getDiasConsumo().compareTo(Integer.valueOf(0)) != 0) {
						relatorioFaturamentoVO.setM3Dias(fatura.getHistoricoConsumo().getConsumoApurado()
										.divide(new BigDecimal(fatura.getHistoricoConsumo().getDiasConsumo()), 4, RoundingMode.HALF_UP));
					}
					relatorioFaturamentoVO.setTarifaUnitaria(controladorFatura
							.obterValorUnitarioDeFaturaItem(fatura.getChavePrimaria(), rubricaConsumoGas.getChavePrimaria()));
				}
				relatorioFaturamentoVO.setValorFaturado(fatura.getValorTotal());

				if (fatura.getContratoAtual() != null && relatorioFaturamentoVO.getVolumeFaturado() != null) {
					listaRelatorioFaturamentoVO.add(relatorioFaturamentoVO);
				}

				if (fatura.getPontoConsumo() != null) {
					relatorioFaturamentoVO.setPontoConsumo(fatura.getPontoConsumo().getDescricao());
				}
			}
		}

		if (listaRelatorioFaturamentoVO.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return listaRelatorioFaturamentoVO;
	}

	private void populaDadosDeConsumoFaturaAgrupada(RelatorioFaturamentoVO relatorioFaturamentoVO,
			Collection<HistoricoConsumo> historicosConsumo) {

		for (HistoricoConsumo historicoConsumo : historicosConsumo) {
			if (relatorioFaturamentoVO.getVolumeFaturado() == null) {
				relatorioFaturamentoVO.setVolumeFaturado(historicoConsumo.getConsumoApurado());
			} else {
				relatorioFaturamentoVO.setVolumeFaturado(
						relatorioFaturamentoVO.getVolumeFaturado().add(historicoConsumo.getConsumoApurado()));
			}
			relatorioFaturamentoVO.setQtdDias(historicoConsumo.getDiasConsumo());
		}
		relatorioFaturamentoVO.setM3Dias(relatorioFaturamentoVO.getVolumeFaturado()
				.divide(new BigDecimal(relatorioFaturamentoVO.getQtdDias()), 4, RoundingMode.HALF_UP));
	}

	/**
	 * Validar filtros.
	 *
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarFiltros(Map<String, Object> filtro) throws NegocioException {

		Date dataEmissaoInicial = (Date) filtro.get(DATA_EMISSAO_INICIAL);
		Date dataEmissaoFinal = (Date) filtro.get(DATA_EMISSAO_FINAL);
		if (dataEmissaoInicial != null && dataEmissaoFinal != null && Util.compararDatas(dataEmissaoInicial, dataEmissaoFinal) > 0) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_EMISSAO_INICIO_MAIOR_DATA_EMISSAO_FINAL, true);
		}

		Date dataVencimentoInicial = (Date) filtro.get(DATA_VENCIMENTO_INICIAL);
		Date dataVencimentoFinal = (Date) filtro.get(DATA_VENCIMENTO_FINAL);
		if (dataVencimentoInicial != null
				&& dataVencimentoFinal != null
				&& Util.compararDatas(dataVencimentoInicial,
						dataVencimentoFinal) > 0) {
			throw new NegocioException(
					ERRO_NEGOCIO_DATA_VENCIMENTO_INICIO_DATA_VENCIMENTO_FINAL,
					true);
		}

		Long numeroNotaFiscalInicial = (Long) filtro.get(NUMERO_NOTA_FISCAL_INICIAL);
		Long numeroNotaFiscalFinal = (Long) filtro.get(NUMERO_NOTA_FISCAL_FINAL);
		if ((numeroNotaFiscalInicial != null)
				&& (numeroNotaFiscalFinal != null)
				&& numeroNotaFiscalInicial > numeroNotaFiscalFinal) {
			throw new NegocioException(
					ERRO_NEGOCIO_NUMERO_NOTA_FISCAL_INICIO_FINAL, true);
		}

		Collection<Contrato> contratos = null;
		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getBeanPorID(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		if (filtro.get(ID_CONTRATO) != null && !"0".equals(filtro.get(ID_CONTRATO).toString())) {
			contratos = controladorContrato.consultarContratoPorNumero(filtro.get(ID_CONTRATO).toString());
			Long[] chavesContrato = new Long[contratos.size()];
			int i = 0;
			for (Contrato contrato : contratos) {
				chavesContrato[i] = contrato.getChavePrimaria();
				i++;
			}
			if (!contratos.isEmpty()) {
				filtro.put(IDS_CONTRATO, chavesContrato);
			} else {
				Long[] chave = {Long.parseLong(filtro.get(ID_CONTRATO).toString())};
				filtro.put(IDS_CONTRATO, chave);
			}
			filtro.remove(ID_CONTRATO);

		}

		Integer anoMesReferencia = (Integer) filtro.get(ANO_MES_REFERENCIA);
		if (anoMesReferencia != null &&
						anoMesReferencia.toString().length() < Constantes.QTD_CARACTER_ANOMES) {
			throw new NegocioException(Constantes.ERRO_DADOS_INVALIDOS,
							Constantes.ANO_MES_FATURAMENTO);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/**
	 * Montar parametro relatorio.
	 *
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void montarParametroRelatorio(Map<String, Object> parametros, Map<String, Object> filtro) throws NegocioException {

		boolean exibirFiltros = false;
		if (filtro != null) {
			Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
			if ((idSegmento != null) && (idSegmento > 0)) {
				ControladorSegmento controladorSegmento = (ControladorSegmento) ServiceLocator.getInstancia().getBeanPorID(
								ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);
				Segmento segmento = controladorSegmento.obterSegmento(idSegmento);
				parametros.put("filtroSegmento", segmento.getDescricao());
				exibirFiltros = true;
			}

			Long idCliente = (Long) filtro.get(ID_CLIENTE);
			if ((idCliente != null) && (idCliente > 0)) {
				ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getBeanPorID(
								ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
				Cliente cliente = (Cliente) controladorCliente.obter(idCliente);
				parametros.put("filtroCliente", cliente.getNome());
				exibirFiltros = true;
			}

			Long[] chavesPontoConsumo = (Long[]) filtro.get(CHAVES_PONTO_CONSUMO);
			if ((chavesPontoConsumo != null) && (chavesPontoConsumo.length > 0)) {
				ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getBeanPorID(
								ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
				PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(chavesPontoConsumo[0]);
				parametros.put("filtroPontoConsumo", pontoConsumo.getDescricao());
				exibirFiltros = true;
			}

			Date dataEmissaoInicial = (Date) filtro.get("dataEmissaoInicial");
			Date dataEmissaoFinal = (Date) filtro.get("dataEmissaoFinal");
			if ((dataEmissaoInicial != null) && (dataEmissaoFinal != null)) {
				parametros.put("filtroDataEmissaoInicial", DataUtil.converterDataParaString(dataEmissaoInicial));
				parametros.put("filtroDataEmissaoFinal", DataUtil.converterDataParaString(dataEmissaoFinal));
				exibirFiltros = true;
			}

			Date dataVencimentoInicial = (Date) filtro.get("dataVencimentoInicial");
			Date dataVencimentoFinal = (Date) filtro.get("dataVencimentoFinal");
			if ((dataVencimentoInicial != null) && (dataVencimentoFinal != null)) {
				parametros.put("filtroDataVencimentoInicial", DataUtil.converterDataParaString(dataVencimentoInicial));
				parametros.put("filtroDataVencimentoFinal", DataUtil.converterDataParaString(dataVencimentoFinal));
				exibirFiltros = true;
			}

			Long idContrato = (Long) filtro.get("idContrato");
			if (idContrato != null && idContrato > 0) {
				parametros.put("filtroContrato", idContrato);
				exibirFiltros = true;
			}

			Long numeroNotaFiscalInicial = (Long) filtro.get("numeroNotaFiscalInicial");
			Long numeroNotaFiscalFinal = (Long) filtro.get("numeroNotaFiscalFinal");

			if ((numeroNotaFiscalInicial != null && numeroNotaFiscalInicial > 0)
							&& (numeroNotaFiscalFinal != null && numeroNotaFiscalFinal > 0)) {
				parametros.put("filtroNumeroNotaFiscalInicial", numeroNotaFiscalInicial);
				parametros.put("filtroNumeroNotaFiscalFinal", numeroNotaFiscalFinal);
				exibirFiltros = true;
			}

			Integer anoMesReferencia = (Integer) filtro.get(ANO_MES_REFERENCIA);
			if (anoMesReferencia != null && anoMesReferencia > 0) {
				parametros.put(FILTRO_ANO_MES_REFERENCIA, anoMesReferencia);
				exibirFiltros = true;
			}
			Integer numeroCiclo = (Integer) filtro.get(NUMERO_CICLO);
			if (numeroCiclo != null && numeroCiclo > 0) {
				parametros.put(FILTRO_NUMERO_CICLO, numeroCiclo);
				exibirFiltros = true;
			}
			Long idGrupo = (Long) filtro.get(ID_GRUPO_FATURAMENTO);
			if (idGrupo != null && idGrupo > 0) {
				parametros.put(FILTRO_ID_GRUPO_FATURAMENTO, idGrupo);
				exibirFiltros = true;
			}
			Long idRota = (Long) filtro.get(ID_ROTA);
			if (idRota != null && idRota > 0) {
				parametros.put(FILTRO_ID_ROTA, idRota);
				exibirFiltros = true;
			}

			parametros.put("exibirFiltros", exibirFiltros);
		}
	}

}
