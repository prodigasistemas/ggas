/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioLigacaoGas;
import br.com.ggas.relatorio.ligacaogas.ParametroRelatorio;
import br.com.ggas.relatorio.ligacaogas.RepositorioLigacaoGas;
import br.com.ggas.relatorio.ligacaogas.ServicoAutorizacaoRelatorioVO;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
/**
 * Implementação do Controlador de relatório de ligação de gás.
 */
@Service("controladorRelatorioLigacaoGas")
@Transactional
public class ControladorRelatorioLigacaoGasImpl implements ControladorRelatorioLigacaoGas{

	@Autowired
	private RepositorioLigacaoGas repositorioLigacaGas;

	private static String STATUS_NO_PRAZO = "No Prazo";

	private static String STATUS_FORA_PRAZO = "Fora do Prazo";

	private static String VALOR_PARAMETRO_BAIXA_PRESSAO = "DATA_LIMITE_LIGACAO_GAS_BAIXA_PRESSAO";
	
	private static String VALOR_PARAMETRO_MEDIA_PRESSAO = "DATA_LIMITE_LIGACAO_GAS_MEDIA_PRESSAO";
	
	private static String VALOR_PARAMETRO_ALTA_PRESSAO = "DATA_LIMITE_LIGACAO_GAS_ALTA_PRESSAO";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioPontosDeConsumoPorDataImpl(java.util.Map)
	 */
	/**
	 * @param dataIncial
	 * @param exibirFiltros
	 * @param tipoRelatorio
	 * @param dataFinal
	 */
	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public byte[] gerarRelatorio(Date dataIncial, Date dataFinal, String exibirFiltros, String tipoRelatorio) throws GGASException {

		Collection<ParametroRelatorio> collRelatorio = new ArrayList<ParametroRelatorio>();

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(tipoRelatorio);
		List<ServicoAutorizacaoRelatorioVO> listaServicoAutorizacao 
		= this.obterDatasLimiteLigamentoGas(dataIncial, dataFinal);

		int parametro = 0;
		

		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = Calendar.getInstance();

		for (int i = 0; i < listaServicoAutorizacao.size(); i++) {
			
			String status = "";
			ParametroRelatorio parametroRelatorio = new ParametroRelatorio();
			if(listaServicoAutorizacao.get(i).getDataEncerramento() != null){
				parametroRelatorio.setDataEncerramento(sf.format(listaServicoAutorizacao.get(i).getDataEncerramento()));
			}
			parametroRelatorio.setDataGerada(sf.format(listaServicoAutorizacao.get(i).getDataGerada()));
			parametroRelatorio.setNomeCliente(listaServicoAutorizacao.get(i).getNomeCliente());
			calendar.setTime( listaServicoAutorizacao.get(i).getDataGerada());
			
			parametro = verificarParametro(listaServicoAutorizacao, i);
			
			calendar.add(Calendar.DATE, parametro);
			Date hoje = new Date();
			
			status = analizarPrazo(calendar, hoje);
			
			parametroRelatorio.setDataLimite(sf.format(calendar.getTime()));
			parametroRelatorio.setStatus(status);
			parametroRelatorio.setPressao(listaServicoAutorizacao.get(i).getDescricao());

			collRelatorio.add(parametroRelatorio);
		}

		String dataParamInicio = null;
		String artigo = null;
		if(dataIncial != null){
			dataParamInicio = sf.format(dataIncial);
			artigo = "à";
		}
		String dataParamFim = null;
		if(dataFinal != null){
			dataParamFim = sf.format(dataFinal);
		}

		String dataImpressao = sf.format(Calendar.getInstance().getTime());
		Map parametros = new HashMap();
		parametros.put("dataInicial", dataParamInicio);
		parametros.put("dataFinal", dataParamFim);
		parametros.put("dataImpressao", dataImpressao);
		parametros.put("formatoImpressao", formatoImpressao);
		parametros.put("artigo", artigo);
		
		this.validarDadosGerarRelatorioPontoConsumoPorData(parametros);

		return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_DATA_LIMITE_LIGAMENTO_GAS_JASPER, formatoImpressao);
	}


	private int verificarParametro(List<ServicoAutorizacaoRelatorioVO> listaServicoAutorizacao, int i) {
		int parametro = 0;
		if("ALTA".equals(listaServicoAutorizacao.get(i).getDescricao())){
			parametro = this.obterParametro(VALOR_PARAMETRO_ALTA_PRESSAO);
		}else if("MÉDIA".equals(listaServicoAutorizacao.get(i).getDescricao())){
			parametro = this.obterParametro(VALOR_PARAMETRO_MEDIA_PRESSAO);
		}else if("BAIXA".equals(listaServicoAutorizacao.get(i).getDescricao())){
			parametro = this.obterParametro(VALOR_PARAMETRO_BAIXA_PRESSAO);
		}
		return parametro;
	}


	private String analizarPrazo(Calendar c, Date hoje) {
		String status;
			
		if (hoje.after(c.getTime())) {
			status = STATUS_FORA_PRAZO;
		} else {
			status = STATUS_NO_PRAZO;
		}
		
		return status;
	}


	/**
	 * Validar dados gerar relatorio pontos de consumo por data.
	 * 
	 * @param parametros
	 * 
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosGerarRelatorioPontoConsumoPorData(Map<String, Object> parametros) throws NegocioException {

		if (parametros.get("formatoImpressao") == null) {

			throw new NegocioException(ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO, true);

		}

	}


	/**
	 * Obter pontos consumo por data.
	 * 
	 * @param parametros
	 * @param dataInicio
	 * @param dataFinal
	 * 
	 * @return the map
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private List<ServicoAutorizacaoRelatorioVO> obterDatasLimiteLigamentoGas(Date dataInicio, Date dataFinal) {

		return  repositorioLigacaGas.listarDatasLimiteParaLigacaoGas(dataInicio, dataFinal);
	}

	private int obterParametro(String param) {

		String paramentro = "";

		List<String> parametros = repositorioLigacaGas.buscarValorParamentro(param);

		for (int i = 0; i < parametros.size(); i++) {
			paramentro = parametros.get(i);
		}

		return Integer.parseInt(paramentro);
	}

}
