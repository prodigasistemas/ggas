
package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.BasicDynaClass;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.DynaClass;
import org.apache.commons.beanutils.DynaProperty;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.DebitosACobrar;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.relatorio.ControladorRelatorioTitulosAtraso;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.titulosatraso.TitulosAtrasadoDetalhadosV0;
import br.com.ggas.web.relatorio.titulosatraso.TitulosAtrasadoV0;
/**
 * Classe responsável pelas operações referentes aos 
 * TitulosAtraso
 */
public class ControladorRelatorioTitulosAtrasoImpl extends ControladorNegocioImpl implements ControladorRelatorioTitulosAtraso {

	private ControladorArrecadacao controladorArrecadacao;

	private ControladorFatura controladorFatura;

	private static final DynaProperty[] ATRIBUTOS_RELATORIO_TITULOSATRASO = new DynaProperty[] 
					{new DynaProperty("dadosFatura", List.class), new DynaProperty(
					"dataReferencia", Date.class), new DynaProperty("diasAtraso", Integer.class), new DynaProperty("exibirCliente",
					Boolean.class)};

	private static final DynaClass RELATORIO_TITULOSATRASO = new BasicDynaClass("RelatorioTitulosAtraso", null,
					ATRIBUTOS_RELATORIO_TITULOSATRASO);

	private static final String IMAGEM = "imagem";
	
	private static final String EXIBIR_FILTROS = "exibirFiltros";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioTitulosAtraso#consultar(java.util.Map)
	 */
	@Override
	public byte[] consultar(Map<String, Object> filtro) throws GGASException {

		byte[] relatorio = null;
		Boolean exibirFiltros = (Boolean) filtro.get(EXIBIR_FILTROS);
		Date campoDataReferencia = null;
		List<Fatura> idsFaturasVencidas;
		Integer campoDiasAtraso = (Integer) filtro.get("diasAtraso");

		campoDataReferencia = validarFiltros(campoDataReferencia, filtro);

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Empresa cdlEmpresa = controladorEmpresa.obterEmpresaPrincipal();
		if(cdlEmpresa.getLogoEmpresa() != null) {
			filtro.put(IMAGEM, Constantes.URL_LOGOMARCA_EMPRESA + cdlEmpresa.getChavePrimaria());
		}

		if(exibirFiltros != null) {
			filtro.put(EXIBIR_FILTROS, exibirFiltros);
		}
		idsFaturasVencidas = consultarFaturasPendentes(filtro, campoDataReferencia);
		if(idsFaturasVencidas != null && !idsFaturasVencidas.isEmpty()) {
			Map<String, Collection<Fatura>> mapaImovelOuClienteFatura = null;

			Collection<TitulosAtrasadoV0> dados = new ArrayList<TitulosAtrasadoV0>();

			mapaImovelOuClienteFatura = controladorFatura.agruparFaturasPorImovelOuCliente(idsFaturasVencidas);

			relatorio = gerarRelatorio(mapaImovelOuClienteFatura, filtro, dados, campoDataReferencia, campoDiasAtraso);
			return relatorio;
		} else {
			throw new GGASException(ERRO_NEGOCIO_NENHUMA_FATURA_EM_ATRASO_FOI_ENCONTRADA, true);
		}

	}

	/**
	 * Validar filtros.
	 * 
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @param filtro
	 *            the filtro
	 * @return the date
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Date validarFiltros(Date campoDataReferencia, Map<String, Object> filtro) throws GGASException {

		String data = (String) filtro.get("dataReferencia");
		Integer campoDiasAtraso = (Integer) filtro.get("diasAtraso");

		if((data == null || "".equals(data)) && (campoDiasAtraso == null || campoDiasAtraso.equals(0))) {
			throw new NegocioException(Constantes.ERRO_SELECIONAR_FILTRO, true);
		}

		if(!data.isEmpty()) {
			campoDataReferencia = Util.converterCampoStringParaData("Data de Referência", data, Constantes.FORMATO_DATA_BR);
		}
		return campoDataReferencia;
	}

	/**
	 * Consultar faturas pendentes.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<Fatura> consultarFaturasPendentes(Map<String, Object> filtro, Date campoDataReferencia) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorCobranca controladorCobranca = (ControladorCobranca) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);

		controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Integer campoDiasAtraso = (Integer) filtro.get("diasAtraso");

		Long[] idsSegmentos = (Long[]) filtro.get("idsSegmentos");
		Long[] arrayPagamentos = (Long[]) filtro.get("arrayPagamento");
		
		String situacaoPaga = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_RECEBIMENTO_PAGO);

		Map<String, Object[]> parametros = new HashMap<String, Object[]>();

		parametros.put("idsSegmentos", idsSegmentos);
		parametros.put("arrayPagamento", arrayPagamentos);
		
		Collection<Fatura> listaFatura = controladorFatura.consultarFaturas(null, null, parametros, null, campoDataReferencia,  Boolean.TRUE);
		Date dataAtual;
		if(campoDataReferencia == null) {
			dataAtual = Calendar.getInstance().getTime();
		} else {
			dataAtual = campoDataReferencia;
		}
		List<Fatura> faturasAtrasoAtt = new ArrayList<Fatura>();

		for (Fatura fatura : listaFatura) {
			Date dataPagamento = controladorCobranca.obterUltimaDataRecebimentoPelaFatura(fatura.getChavePrimaria());
			Date dataCancelamento = fatura.getDataCancelamento();

			if(campoDiasAtraso != null && !campoDiasAtraso.equals(0)) {
				Date dataVencimento = fatura.getDataVencimento();
				// Se a fatura estiver paga, verifica se a data de pagamento da mesma foi feita depois da data de referência se sim essa
				// fatura estava
				// atrasada na data de referencia
				if (fatura.getSituacaoPagamento().getChavePrimaria() == Long.parseLong(situacaoPaga)) {
					if (dataPagamento != null && dataPagamento.after(dataAtual)) {
						int diasAtraso = Util.intervaloDatas(dataVencimento, dataAtual);
						if (diasAtraso == campoDiasAtraso) {
							faturasAtrasoAtt.add(fatura);
						}

					}
					// verifica se a fatura estava cancelada nessa data
				} else if(dataCancelamento != null) {
					if(dataCancelamento.after(dataAtual)) {
						if(dataPagamento != null) {
							BigDecimal resultado = BigDecimal.ZERO;
							BigDecimal valor = controladorCobranca.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());
							resultado = fatura.getValorTotal().subtract(valor);
							if(resultado.doubleValue() <= 0) {
								int diasAtraso = Util.intervaloDatas(dataVencimento, dataAtual);
								if(diasAtraso == campoDiasAtraso) {
									faturasAtrasoAtt.add(fatura);
								}
							}

						} else {
							int diasAtraso = Util.intervaloDatas(dataVencimento, dataAtual);
							if(diasAtraso == campoDiasAtraso) {
								faturasAtrasoAtt.add(fatura);
							}
						}
					}

				} else {
					int diasAtraso = Util.intervaloDatas(dataVencimento, dataAtual);
					if(diasAtraso == campoDiasAtraso) {
						faturasAtrasoAtt.add(fatura);
					}
				}

			} else {
				if (fatura.getSituacaoPagamento().getChavePrimaria() == Long.parseLong(situacaoPaga)) {
					if (dataPagamento != null && dataPagamento.after(campoDataReferencia)) {
						// fatura atrasada naquela data de referência
						faturasAtrasoAtt.add(fatura);
					}

				} else if(dataCancelamento != null) {
					if(dataCancelamento.after(dataAtual)) {
						if(dataPagamento != null) {
							if(dataPagamento.after(dataAtual)) {
								faturasAtrasoAtt.add(fatura);
							}
						} else {
							faturasAtrasoAtt.add(fatura);
						}
					}
				} else {
					faturasAtrasoAtt.add(fatura);
				}
			}
		}

		return faturasAtrasoAtt;

	}

	/**
	 * Gerar relatorio.
	 * 
	 * @param mapaImovelOuClienteFatura
	 *            the mapa imovel ou cliente fatura
	 * @param filtro
	 *            the filtro
	 * @param dados
	 *            the dados
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @param campoDiasAtraso
	 *            the campo dias atraso
	 * @return the byte[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private byte[] gerarRelatorio(Map<String, Collection<Fatura>> mapaImovelOuClienteFatura, Map<String, Object> filtro,
					Collection<TitulosAtrasadoV0> dados, Date campoDataReferencia, Integer campoDiasAtraso) throws GGASException {

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf((String) filtro.get("formatoImpressao"));
		Collection<Object> collRelatorio = new ArrayList<Object>();
		collRelatorio.add(this.obterRelatorio(filtro, mapaImovelOuClienteFatura, dados, campoDataReferencia, campoDiasAtraso));

		return RelatorioUtil.gerarRelatorio(collRelatorio, filtro, RELATORIO_TITULOS_ATRASO, formatoImpressao);
	}

	/**
	 * Obter relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param mapaImovelOuClienteFatura
	 *            the mapa imovel ou cliente fatura
	 * @param dados
	 *            the dados
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @param campoDiasAtraso
	 *            the campo dias atraso
	 * @return the object
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Object obterRelatorio(Map<String, Object> filtro, Map<String, Collection<Fatura>> mapaImovelOuClienteFatura,
					Collection<TitulosAtrasadoV0> dados, Date campoDataReferencia, Integer campoDiasAtraso) throws GGASException {

		DynaBean relatorioTitulosAtraso = new BasicDynaBean(RELATORIO_TITULOSATRASO);
		relatorioTitulosAtraso.set("dadosFatura",
						this.obterDadosFatura(mapaImovelOuClienteFatura, dados, campoDataReferencia, campoDiasAtraso));
		relatorioTitulosAtraso.set("dataReferencia", campoDataReferencia);
		relatorioTitulosAtraso.set("diasAtraso", campoDiasAtraso);
		relatorioTitulosAtraso.set("exibirCliente", Boolean.parseBoolean((String) filtro.get("exibirCliente")));
		return relatorioTitulosAtraso;
	}

	/**
	 * Obter dados fatura.
	 * 
	 * @param mapaImovelOuClienteFatura
	 *            the mapa imovel ou cliente fatura
	 * @param dados
	 *            the dados
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @param campoDiasAtraso
	 *            the campo dias atraso
	 * @return the list
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private List<Object> obterDadosFatura(Map<String, Collection<Fatura>> mapaImovelOuClienteFatura, Collection<TitulosAtrasadoV0> dados,
					Date campoDataReferencia, Integer campoDiasAtraso) throws GGASException {

		return copiarFaturasAgrupadas(mapaImovelOuClienteFatura, dados, campoDataReferencia, campoDiasAtraso);
	}

	/**
	 * Copiar faturas agrupadas.
	 * 
	 * @param mapaImovelOuClienteFatura
	 *            the mapa imovel ou cliente fatura
	 * @param dados
	 *            the dados
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @param campoDiasAtraso
	 *            the campo dias atraso
	 * @return the list
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private List<Object> copiarFaturasAgrupadas(Map<String, Collection<Fatura>> mapaImovelOuClienteFatura,
					Collection<TitulosAtrasadoV0> dados, Date campoDataReferencia, Integer campoDiasAtraso) throws GGASException {

		List<Object> subRelatorios = new ArrayList<Object>();
		for (Map.Entry<String, Collection<Fatura>> entry : mapaImovelOuClienteFatura.entrySet()) {

			String nomeFantasia = entry.getKey();
			if(nomeFantasia != null) {
				TitulosAtrasadoV0 titulosAtrasadoV0 = new TitulosAtrasadoV0();
				List<TitulosAtrasadoDetalhadosV0> titulosAtrasadosDetalhados = new ArrayList<TitulosAtrasadoDetalhadosV0>();
				titulosAtrasadoV0.setTitulosAtrasados(titulosAtrasadosDetalhados);
				titulosAtrasadoV0.setNomeFantasiaImovel(nomeFantasia);
				dados.add(titulosAtrasadoV0);

				Collection<Fatura> listaFaturas = entry.getValue();
				for (Fatura fat : listaFaturas) {
					popularTitulosAtrasados(titulosAtrasadoV0, fat, campoDataReferencia, campoDiasAtraso);
				}

				for (TitulosAtrasadoDetalhadosV0 t : titulosAtrasadoV0.getTitulosAtrasados()) {
					t.setQuantidadeDocumentos(titulosAtrasadoV0.getTitulosAtrasados().size());
				}

			}
		}

		subRelatorios.addAll(dados);
		return subRelatorios;
	}

	/**
	 * Popular titulos atrasados.
	 * 
	 * @param titulosAtrasadoV0
	 *            the titulos atrasado v0
	 * @param fatura
	 *            the fatura
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @param campoDiasAtraso
	 *            the campo dias atraso
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void popularTitulosAtrasados(TitulosAtrasadoV0 titulosAtrasadoV0, Fatura fatura, Date campoDataReferencia,
					Integer campoDiasAtraso) throws GGASException {

		TitulosAtrasadoDetalhadosV0 tituloAtrasado = new TitulosAtrasadoDetalhadosV0();
		tituloAtrasado.setDataEmissao(fatura.getDataEmissao());
		tituloAtrasado.setDataVencimento(fatura.getDataVencimento());
		tituloAtrasado.setNumeroDocumento(fatura.getChavePrimaria());
		tituloAtrasado.setTipoDocumento(fatura.getTipoDocumento().getDescricao());
		tituloAtrasado.setValorHistorico(fatura.getValorTotal());
		if(fatura.getPontoConsumo() != null) {
			tituloAtrasado.setNomeFantasiaPontoConsumo(fatura.getPontoConsumo().getImovel().getNome());
		} else {
			if(fatura.getCliente() != null) {
				tituloAtrasado.setNomeFantasiaPontoConsumo(fatura.getCliente().getNomeFantasia());
			}
		}
		if(fatura.getCliente() != null) {
			titulosAtrasadoV0.setNomeCliente(fatura.getCliente().getNome());
		}

		calcularJurosEMulta(fatura, tituloAtrasado, campoDataReferencia, campoDiasAtraso);

		if(tituloAtrasado.getDiasAtraso() > 0) {

			titulosAtrasadoV0.getTitulosAtrasados().add(tituloAtrasado);
		}
	}

	/**
	 * Calcular juros e multa.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param tituloAtrasado
	 *            the titulo atrasado
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @param campoDiasAtraso
	 *            the campo dias atraso
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void calcularJurosEMulta(Fatura fatura, TitulosAtrasadoDetalhadosV0 tituloAtrasado, Date campoDataReferencia,
					Integer campoDiasAtraso) throws GGASException {

		configControladorArrecadacao();
		ServiceLocator.getInstancia().getBeanPorID(DebitosACobrar.BEAN_ID_DEBITOS_A_COBRAR);
		adicionarJurosEMultaNaFatura(fatura, tituloAtrasado, campoDataReferencia);

		popularDiasAtrasoFatura(fatura, tituloAtrasado, campoDataReferencia, campoDiasAtraso);

	}

	/**
	 * Adicionar juros e multa na fatura.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param tituloAtrasado
	 *            the titulo atrasado
	 * @param campoDataReferencia
	 *            the campo data referencia
	 * @return the date
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Date adicionarJurosEMultaNaFatura(Fatura fatura, TitulosAtrasadoDetalhadosV0 tituloAtrasado, Date campoDataReferencia)
					throws GGASException {

		ControladorCobranca controladorCobranca = (ControladorCobranca) ServiceLocator.getInstancia().getBeanPorID(
						ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);

		BigDecimal valorCorrigido = null;
		DebitosACobrar debitosACobrarVO;
		Date dataPagamento = null;

		BigDecimal valorPagamento = controladorCobranca.obterValorRecebimentoPelaFatura(fatura.getChavePrimaria());

		if(campoDataReferencia == null) {
			dataPagamento = new Date();
		} else {
			dataPagamento = campoDataReferencia;
		}

		valorCorrigido = fatura.getValorTotal();
		debitosACobrarVO = this.controladorArrecadacao
						.calcularAcrescimentoImpontualidade(fatura, dataPagamento, valorPagamento, true, true);
		if(debitosACobrarVO != null) {
			if(debitosACobrarVO.getJurosMora() != null) {
				valorCorrigido = valorCorrigido.add(debitosACobrarVO.getJurosMora());
			}
			if(debitosACobrarVO.getMulta() != null) {
				valorCorrigido = valorCorrigido.add(debitosACobrarVO.getMulta());
			}
		}

		tituloAtrasado.setValorCorrigido(valorCorrigido);
		return dataPagamento;
	}

	/**
	 * Popular dias atraso fatura.
	 * 
	 * @param fatura
	 *            the fatura
	 * @param tituloAtrasado
	 *            the titulo atrasado
	 * @param dataReferencia
	 *            the data referencia
	 * @param campoDiasAtraso
	 *            the campo dias atraso
	 */
	private void popularDiasAtrasoFatura(Fatura fatura, TitulosAtrasadoDetalhadosV0 tituloAtrasado, Date dataReferencia,
					Integer campoDiasAtraso) {

		int qtdeDiasAtraso = 0;
		if(dataReferencia != null) {
			qtdeDiasAtraso = Util.intervaloDatas(fatura.getDataVencimento(), dataReferencia);
		} else {
			qtdeDiasAtraso = campoDiasAtraso;
		}
		tituloAtrasado.setDiasAtraso(qtdeDiasAtraso);
	}

	/**
	 * Config controlador arrecadacao.
	 */
	private void configControladorArrecadacao() {

		this.controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeRecebimento() {

		return ServiceLocator.getInstancia().getClassPorID(Recebimento.BEAN_ID_RECEBIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Fatura.BEAN_ID_FATURA);
	}
}
