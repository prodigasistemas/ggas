/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Query;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.ImovelRamoAtividade;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.ModalidadeMedicaoImovelImpl;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.relatorio.ControladorRelatorioClienteUnidadeConsumidora;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.web.relatorio.clienteunidadeconsumidora.ClienteUnidadeConsumidoraVO;

@SuppressWarnings("unchecked")
public class ControladorRelatorioClienteUnidadeConsumidoraImpl extends ControladorNegocioImpl 
	implements ControladorRelatorioClienteUnidadeConsumidora {

	/**
	 * Popula o relatório com uma coleção de
	 * entidades baseado nos filtros.
	 * 
	 * @param dados
	 *            Registos para a população do
	 *            relatório
	 * @param filtro
	 *            Parâmetros para a geração do
	 *            relatório
	 * @return bytesRelatorio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public byte[] gerarRelatorio(Collection<ClienteUnidadeConsumidoraVO> dados, Map<String, Object> filtro) throws NegocioException {

		validarDados(dados);
		Map<String, Object> parametros = new HashMap<String, Object>();

		// Imagem da empresa
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Empresa principal = controladorEmpresa.obterEmpresaPrincipal();
		if(principal.getLogoEmpresa() != null) {
			parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(principal.getChavePrimaria()));
		}

		// Exibição de filtros
		Boolean exibirFiltros = (Boolean) filtro.get("exibirFiltros");
		parametros.put("exibirFiltros", exibirFiltros);

		if(exibirFiltros != null && exibirFiltros) {
			if(filtro.containsKey("idImovel")) {
				parametros.put("valorImovel", filtro.get("idImovel"));
			}

			if(filtro.containsKey("idModalidadeMedicao")) {
				String modalidadeMedicao = ModalidadeMedicaoImovelImpl.getDescricaoPorCodigo((Integer) filtro.get("idModalidadeMedicao"));
				parametros.put("valorModalidadeMedicao", modalidadeMedicao);
			}
		}

		String nomeFormato = (String) filtro.get("formatoImpressao");
		FormatoImpressao formato = FormatoImpressao.valueOf(nomeFormato);

		return RelatorioUtil.gerarRelatorio(dados, parametros, "relatorioClienteUnidadeConsumidora.jasper", formato);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.relatorio.impl.
	 * ControladorRelatorioFaturasEmitidas
	 * #consultar(java.util
	 * .Map)
	 */
	@Override
	public byte[] consultar(Map<String, Object> filtro) throws GGASException {

		byte[] relatorio = null;
		Collection<ClienteUnidadeConsumidoraVO> dados = new ArrayList<ClienteUnidadeConsumidoraVO>();

		Long idImovel = null;
		if(filtro.containsKey("idImovel")) {
			idImovel = (Long) filtro.get("idImovel");
		}

		Integer idModalidadeMedicao = null;
		if(filtro.containsKey("idModalidadeMedicao")) {
			idModalidadeMedicao = (Integer) filtro.get("idModalidadeMedicao");
		}

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeImovel().getSimpleName());
		hql.append(" imovel ");
		hql.append(" inner join fetch imovel.quadraFace quadraFace ");
		hql.append(" inner join fetch quadraFace.endereco endereco ");
		hql.append(" inner join fetch endereco.cep cep ");

		hql.append(" where ");
		hql.append(" imovel.imovelCondominio is null ");

		if(idImovel != null && idImovel > 0) {
			hql.append(" and imovel.chavePrimaria = :idImovel ");
		}

		if(idModalidadeMedicao != null && idModalidadeMedicao > 0) {
			hql.append(" and imovel.modalidadeMedicaoImovel.codigo = :idModalidadeMedicao ");
		}

		hql.append(" order by ");
		hql.append(" imovel.nome, cep.bairro, imovel.modalidadeMedicaoImovel.codigo ");

		Query query = getSession().createQuery(hql.toString());

		if(idImovel != null && idImovel > 0) {
			query.setLong("idImovel", idImovel);
		}

		if(idModalidadeMedicao != null && idModalidadeMedicao > 0) {
			query.setInteger("idModalidadeMedicao", idModalidadeMedicao);
		}

		Collection<Imovel> listaImovel = query.list();

		for (Imovel imovel : listaImovel) {

			Long numeroUCIndividual = obterValorNumeroUCParaModalidadeMedicaoIndividual(imovel.getChavePrimaria());
			Long numeroPontoConsumo = calcularNumeroUCModalidadeMedicaoColetiva(imovel.getChavePrimaria());
			Long numeroUCImovel = numeroImoveisAssociadosAoImovel(imovel.getChavePrimaria());
			String modalidadeMedicao = null;

			// Modalidade de medição
			if(imovel.getModalidadeMedicaoImovel() != null) {
				if(imovel.getModalidadeMedicaoImovel().getCodigo() == ModalidadeMedicaoImovel.INDIVIDUAL) {
					modalidadeMedicao = ModalidadeMedicaoImovel.DESCRICAO_INDIVIDUAL;
				} else if(imovel.getModalidadeMedicaoImovel().getCodigo() == ModalidadeMedicaoImovel.COLETIVA) {
					modalidadeMedicao = ModalidadeMedicaoImovel.DESCRICAO_COLETIVA;
				}

			}

			String endereco = imovel.getQuadraFace().getEndereco().getCep().getTipoLogradouro()
							+ imovel.getQuadraFace().getEndereco().getCep().getLogradouro();
			String bairro = imovel.getQuadraFace().getEndereco().getCep().getBairro();
			String cidade = imovel.getQuadraFace().getEndereco().getCep().getNomeMunicipio();

			ClienteUnidadeConsumidoraVO clienteUnidadeConsumidoraVO = new ClienteUnidadeConsumidoraVO();

			clienteUnidadeConsumidoraVO.setNomeImovel(imovel.getNome());
			clienteUnidadeConsumidoraVO.setEndereco(endereco);
			clienteUnidadeConsumidoraVO.setBairro(bairro);
			clienteUnidadeConsumidoraVO.setCidade(cidade);

			if(numeroUCIndividual != null) {
				clienteUnidadeConsumidoraVO.setNumeroUCIndividual(numeroUCIndividual.intValue());
			}

			if(numeroPontoConsumo != null) {
				clienteUnidadeConsumidoraVO.setNumeroPontoConsumo(numeroPontoConsumo.intValue());
			}

			if(numeroUCImovel != null) {
				clienteUnidadeConsumidoraVO.setNumeroUCImovel(numeroUCImovel.intValue());
			}

			clienteUnidadeConsumidoraVO.setModalidadeMedicao(modalidadeMedicao);

			dados.add(clienteUnidadeConsumidoraVO);
		}
		relatorio = gerarRelatorio(dados, filtro);

		return relatorio;
	}

	/**
	 * Obter valor numero uc para modalidade medicao individual.
	 * 
	 * @param idImovel
	 *            the id imovel
	 * @return the long
	 */
	private Long obterValorNumeroUCParaModalidadeMedicaoIndividual(Long idImovel) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select sum(ira.quantidadeEconomia) ");
		hql.append(" from ").append(getClasseEntidadeImovelRamoAtividade().getSimpleName());
		hql.append(" ira where ira.imovel = :idImovel ");

		Query query = getSession().createQuery(hql.toString());
		query.setLong("idImovel", idImovel);

		return (Long) query.uniqueResult();
	}

	/**
	 * Calcular numero uc modalidade medicao coletiva.
	 * 
	 * @param idImovel
	 *            the id imovel
	 * @return the long
	 */
	private Long calcularNumeroUCModalidadeMedicaoColetiva(Long idImovel) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select count(*) ");
		hql.append(" from ").append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo where pontoConsumo.imovel = :idImovel ");

		Query query = getSession().createQuery(hql.toString());
		query.setLong("idImovel", idImovel);

		return (Long) query.uniqueResult();
	}

	/**
	 * Numero imoveis associados ao imovel.
	 * 
	 * @param idImovel
	 *            the id imovel
	 * @return the long
	 */
	private Long numeroImoveisAssociadosAoImovel(Long idImovel) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select count(*) ");
		hql.append(" from ").append(getClasseEntidadeImovel().getSimpleName());
		hql.append(" countUnidade WHERE countUnidade.imovelCondominio = :idImovel ");

		Query query = getSession().createQuery(hql.toString());
		query.setLong("idImovel", idImovel);

		return (Long) query.uniqueResult();
	}


	/**
	 * Valida uma coleção de dados será exibida em
	 * relatório.
	 * 
	 * @param dados
	 *            Registros a validar
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDados(Collection<ClienteUnidadeConsumidoraVO> dados) throws NegocioException {

		if(dados == null || dados.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Fatura.BEAN_ID_FATURA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeImovel() {

		return ServiceLocator.getInstancia().getClassPorID(Imovel.BEAN_ID_IMOVEL);
	}

	public Class<?> getClasseEntidadeImovelRamoAtividade() {

		return ServiceLocator.getInstancia().getClassPorID(ImovelRamoAtividade.BEAN_ID_IMOVEL_RAMO_ATIVIDADE);
	}

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

}
