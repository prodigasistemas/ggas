/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 17/01/2014 14:26:50
 @author ifrancisco
 */

package br.com.ggas.relatorio.impl;

import java.awt.Image;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ImageIcon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.chamado.dominio.ClienteVO;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.dominio.EquipeVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoMotivoEncerramento;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoRelatorioVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVO;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVORelatorio;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.apuracaopenalidade.impl.PontoConsumoVO;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ChamadoResumoVO;
import br.com.ggas.relatorio.ControladorRelatorioServicoAutorizacao;
import br.com.ggas.relatorio.ServicoAutorizacaoResumoVO;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * @author ifrancisco
 */
@Service("controladorRelatorioServicoAutorizacao")
@Transactional
public class ControladorRelatorioServicoAutorizacaoImpl implements ControladorRelatorioServicoAutorizacao {

	private static final String PARAMENTRO_EQUIPE = "equipe";

	private static final String PARAMETRO_CLIENTE = "cliente";

	@Autowired
	private ControladorServicoAutorizacao controladorServicoAutorizacao;

	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.relatorio.ControladorRelatorioServicoAutorizacao#gerarRelatorioServicoAltorizacao(br.com.ggas.atendimento.servicoautorizacao
	 * .dominio.ServicoAutorizacaoVO, br.com.ggas.util.FormatoImpressao, java.lang.String, java.lang.String)
	 */
	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public byte[] gerarRelatorioServicoAltorizacao(ServicoAutorizacaoVO servicoAutorizacaoVO, FormatoImpressao formatoImpressao,
					String agrupamento, String exibirFiltros) throws ParseException, GGASException {

		List<ServicoAutorizacaoRelatorioVO> dados = new ArrayList<ServicoAutorizacaoRelatorioVO>();

		if(PARAMETRO_CLIENTE.equals(agrupamento)) {
			dados = montarListaServicoAutorizacaoRelatorioVO((List<ServicoAutorizacao>) controladorServicoAutorizacao
							.listarServicoAutorizacao(servicoAutorizacaoVO, Boolean.FALSE));
		}
		if(PARAMENTRO_EQUIPE.equals(agrupamento)) {
			dados = montarListaServicoAutorizacaoRelatorioVOPorEquipe((List<ServicoAutorizacao>) controladorServicoAutorizacao
							.listarServicoAutorizacao(servicoAutorizacaoVO, Boolean.FALSE));
		}

		if(dados.isEmpty()) {
			throw new NegocioException("ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO", true);
		}
		Map parametros = new HashMap();
		parametros.put("dataEmissao", DataUtil.converterDataParaString(new Date()));
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		Image logo = new ImageIcon(empresa.getLogoEmpresa()).getImage();
		parametros.put("logo", logo);
		if(servicoAutorizacaoVO.getDataGeracaoInicio() != null && !servicoAutorizacaoVO.getDataGeracaoInicio().isEmpty()
						&& servicoAutorizacaoVO.getDataGeracaoFim() != null && !servicoAutorizacaoVO.getDataGeracaoFim().isEmpty()) {
			String periodo = "Período de " + servicoAutorizacaoVO.getDataGeracaoInicio() + " a " + servicoAutorizacaoVO.getDataGeracaoFim();
			parametros.put("periodo", periodo);
		}
		parametros.put("agrupamento", agrupamento);

		if(servicoAutorizacaoVO.getListaStatus() != null) {
			StringBuilder descricaoFormatada = new StringBuilder();
			String separador = ",";
			for (String status : servicoAutorizacaoVO.getListaStatus()) {
				if(status != null) {
					descricaoFormatada.append(status);
					descricaoFormatada.append(separador);
				}
			}

			if(servicoAutorizacaoVO.getListaStatus() != null){
				parametros.put("status", descricaoFormatada.toString());
			}else{
				parametros.put("status", "");
			}
		}

		if(servicoAutorizacaoVO.getServicosTipos() != null) {
			StringBuilder descricaoFormatada = new StringBuilder();
			String separador = ",";
			for (ServicoTipo servicos : servicoAutorizacaoVO.getServicosTipos()) {
				String descricao = servicos.getDescricao();
				if(descricao != null) {
					descricaoFormatada.append(descricao);
					descricaoFormatada.append(separador);
				}

			}
			if(servicoAutorizacaoVO.getServicosTipos() != null){
				parametros.put("tipoServico", descricaoFormatada.toString());
			}else{
				parametros.put("tipoServico", "");
			}
		}
		if(servicoAutorizacaoVO.getEquipe() != null){
			parametros.put(PARAMENTRO_EQUIPE, servicoAutorizacaoVO.getEquipe().getNome());
		}else{
			parametros.put(PARAMENTRO_EQUIPE, "");
		}
		if(servicoAutorizacaoVO.getNumeroContrato() != null){
			parametros.put("contrato", servicoAutorizacaoVO.getNumeroContrato());
		}else{
			parametros.put("contrato", "");
		}
		if(servicoAutorizacaoVO.getServicoTipoPrioridade() != null){
			parametros.put("prioridade", servicoAutorizacaoVO.getServicoTipoPrioridade().getDescricao());
		}else{
			parametros.put("prioridade", "");
		}
		if(servicoAutorizacaoVO.getNomeCliente() != null){
			parametros.put(PARAMETRO_CLIENTE, servicoAutorizacaoVO.getNomeCliente());
		}else{
			parametros.put(PARAMETRO_CLIENTE, "");
		}
		if(servicoAutorizacaoVO.getNomeImovel() != null){
			parametros.put("imovel", servicoAutorizacaoVO.getNomeImovel());
		}else{
			parametros.put("imovel", "");
		}
		if(servicoAutorizacaoVO.getNumeroProtocoloChamado() != null){
			parametros.put("numeroProtocolo", servicoAutorizacaoVO.getNumeroProtocoloChamado());
		}else{
			parametros.put("numeroProtocolo", "");
		}
		parametros.put("exibirFiltros", Boolean.valueOf(exibirFiltros));

		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_SERVICO_AUTORIZACAO, formatoImpressao);

	}
	
	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public byte[] gerarRelatorioServicoAutorizacaoSintetico(ServicoAutorizacaoVO servicoAutorizacaoVO,
			FormatoImpressao formatoImpressao, String agrupamento, String exibirFiltros)
			throws ParseException, GGASException {
		
		List<ServicoAutorizacaoRelatorioVO> dados = new ArrayList<ServicoAutorizacaoRelatorioVO>();
		

		if(PARAMETRO_CLIENTE.equals(agrupamento)) {
			dados = montarListaServicoAutorizacaoRelatorioVO((List<ServicoAutorizacao>) controladorServicoAutorizacao
							.listarServicoAutorizacao(servicoAutorizacaoVO, Boolean.FALSE));
		}
		if(PARAMENTRO_EQUIPE.equals(agrupamento)) {
			dados = montarListaServicoAutorizacaoRelatorioVOPorEquipe((List<ServicoAutorizacao>) controladorServicoAutorizacao
							.listarServicoAutorizacao(servicoAutorizacaoVO, Boolean.FALSE));
		}
		
		
		if(dados.isEmpty()) {
			throw new NegocioException("ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO", true);
		}
		
		Map parametros = montarParametrosRelatorio(servicoAutorizacaoVO, exibirFiltros, agrupamento);
		
		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_SERVICO_AUTORIZACAO_SINTETICO, formatoImpressao);
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	private Map montarParametrosRelatorio(ServicoAutorizacaoVO servicoAutorizacaoVO, String exibirFiltros, String agrupamento) throws GGASException {
		
		Map parametros = new HashMap();
		
		parametros.put("dataEmissao", DataUtil.converterDataParaString(new Date()));
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		Image logo = new ImageIcon(empresa.getLogoEmpresa()).getImage();
		parametros.put("logo", logo);
		
		if(servicoAutorizacaoVO.getDataGeracaoInicio() != null && !servicoAutorizacaoVO.getDataGeracaoInicio().isEmpty()
						&& servicoAutorizacaoVO.getDataGeracaoFim() != null && !servicoAutorizacaoVO.getDataGeracaoFim().isEmpty()) {
			String periodo = "Período de " + servicoAutorizacaoVO.getDataGeracaoInicio() + " a " + servicoAutorizacaoVO.getDataGeracaoFim();
			parametros.put("periodo", periodo);
		}else {
			parametros.put("periodo", "");
		}
		if(servicoAutorizacaoVO.getDataPrevisaoInicio() != null && !servicoAutorizacaoVO.getDataPrevisaoInicio().isEmpty()
				&& servicoAutorizacaoVO.getDataPrevisaoFim() != null && !servicoAutorizacaoVO.getDataPrevisaoFim().isEmpty()) {
			String periodoPrevisao = "Período de previsão de " +  servicoAutorizacaoVO.getDataPrevisaoInicio() + " a " + servicoAutorizacaoVO.getDataPrevisaoFim();
			parametros.put("periodoPrevisao", periodoPrevisao);
		}
		if(servicoAutorizacaoVO.getDataEncerramentoInicio() != null && !servicoAutorizacaoVO.getDataEncerramentoInicio().isEmpty()
				&& servicoAutorizacaoVO.getDataEncerramentoFim() != null && !servicoAutorizacaoVO.getDataEncerramentoFim().isEmpty()) {
			String periodoEncerramento = "Período de encerramento de " +  servicoAutorizacaoVO.getDataEncerramentoInicio() + " a " + servicoAutorizacaoVO.getDataEncerramentoFim();
			parametros.put("periodoEncerramento", periodoEncerramento);
		}
		
		parametros.put("agrupamento", agrupamento);

		if(servicoAutorizacaoVO.getListaStatus() != null) {
			StringBuilder descricaoFormatada = new StringBuilder();
			String separador = ",";
			for (String status : servicoAutorizacaoVO.getListaStatus()) {
				if(status != null) {
					descricaoFormatada.append(status);
					descricaoFormatada.append(separador);
				}
			}

			if(servicoAutorizacaoVO.getListaStatus() != null){
				parametros.put("status", descricaoFormatada.toString());
			}else{
				parametros.put("status", "");
			}
		}

		if(servicoAutorizacaoVO.getServicosTipos() != null) {
			StringBuilder descricaoFormatada = new StringBuilder();
			String separador = ",";
			for (ServicoTipo servicos : servicoAutorizacaoVO.getServicosTipos()) {
				String descricao = servicos.getDescricao();
				if(descricao != null) {
					descricaoFormatada.append(descricao);
					descricaoFormatada.append(separador);
				}

			}
			if(servicoAutorizacaoVO.getServicosTipos() != null){
				parametros.put("tipoServico", descricaoFormatada.toString());
			}else{
				parametros.put("tipoServico", "");
			}
		}
		if(servicoAutorizacaoVO.getEquipe() != null){
			parametros.put(PARAMENTRO_EQUIPE, servicoAutorizacaoVO.getEquipe().getNome());
		}else{
			parametros.put(PARAMENTRO_EQUIPE, "");
		}
		if(servicoAutorizacaoVO.getNumeroContrato() != null){
			parametros.put("contrato", servicoAutorizacaoVO.getNumeroContrato());
		}else{
			parametros.put("contrato", "");
		}
		if(servicoAutorizacaoVO.getServicoTipoPrioridade() != null){
			parametros.put("prioridade", servicoAutorizacaoVO.getServicoTipoPrioridade().getDescricao());
		}else{
			parametros.put("prioridade", "");
		}
		if(servicoAutorizacaoVO.getNomeCliente() != null){
			parametros.put(PARAMETRO_CLIENTE, servicoAutorizacaoVO.getNomeCliente());
		}else{
			parametros.put(PARAMETRO_CLIENTE, "");
		}
		if(servicoAutorizacaoVO.getNomeImovel() != null){
			parametros.put("imovel", servicoAutorizacaoVO.getNomeImovel());
		}else{
			parametros.put("imovel", "");
		}
		if(servicoAutorizacaoVO.getNumeroProtocoloChamado() != null){
			parametros.put("numeroProtocolo", servicoAutorizacaoVO.getNumeroProtocoloChamado());
		}else{
			parametros.put("numeroProtocolo", "");
		}
		if(servicoAutorizacaoVO.getServicoAtraso() != null) {
			if(servicoAutorizacaoVO.getServicoAtraso()) {
				parametros.put("servicoAtraso", "Sim");
			}else {
				parametros.put("servicoAtraso", "Não");
			}
		}else {
			parametros.put("servicoAtraso", "");
		}
		
		if(servicoAutorizacaoVO.getEncerradoAposPrazo() != null) {
			if(servicoAutorizacaoVO.getEncerradoAposPrazo()) {
				parametros.put("encerradoAposPrazo", "Sim");
			}else {
				parametros.put("encerradoAposPrazo", "Não");
			}
		}else {
			parametros.put("encerradoAposPrazo", "");
		}
		
		if(servicoAutorizacaoVO.getIndicadorAssociado() != null) {
			if(servicoAutorizacaoVO.getIndicadorAssociado()) {
				parametros.put("indicadorAssociado", "Com Cliente Associado");
			}else {
				parametros.put("indicadorAssociado", "Sem Cliente Associado");
			}
		}else {
			parametros.put("indicadorAssociado", "");
		}
		
		if(servicoAutorizacaoVO.getTipagemRelatorio().equals("relatorioServicoAutorizacaoSintetico")) {
			parametros.put("tipagemRelatorio", "Sintético");
		}else {
			parametros.put("tipagemRelatorio", "Analítico");
		}
		
		parametros.put("exibirFiltros", Boolean.valueOf(exibirFiltros));
		
		return parametros;
	}
	
	/**
	 * Montar lista servico autorizacao relatorio vo.
	 * 
	 * @param lista
	 *            the lista
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private List<ServicoAutorizacaoRelatorioVO> montarListaServicoAutorizacaoRelatorioVO(List<ServicoAutorizacao> lista)
					throws NegocioException {

		List<ClienteVO> listaClienteVO = new ArrayList<ClienteVO>();
		List<ServicoAutorizacao> listaServicosSemCliente = new ArrayList<ServicoAutorizacao>();
		int numeroPontoConsumoVazio = 0;
		// monta lista de cliente
		List<Cliente> listaCliente = new ArrayList<Cliente>();
		for (ServicoAutorizacao servicoAutorizacao : lista) {
			if(servicoAutorizacao.getDataExecucao() != null) {
				servicoAutorizacao.setDataExecucaoFormatadaTexto(servicoAutorizacao.getDataExecucaoFormatada());
			}
			if(servicoAutorizacao.getCliente() != null && !listaCliente.contains(servicoAutorizacao.getCliente())) {
				listaCliente.add(servicoAutorizacao.getCliente());
			}
			if(servicoAutorizacao.getCliente() == null) {
				listaServicosSemCliente.add(servicoAutorizacao);
			}

		}

		ClienteVO clienteVO = null;
		for (Cliente cliente : listaCliente) {
			clienteVO = new ClienteVO();
			clienteVO.setCliente(cliente);
			List<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>(controladorPontoConsumo.listarPontoConsumoPorCliente(cliente
							.getChavePrimaria()));
			List<PontoConsumoVO> listaPontoConsumoVO = new ArrayList<PontoConsumoVO>();
			if(!listaPontoConsumo.isEmpty()) {
				listaPontoConsumoVO = converterPontoConsumoParaPontoConsumoVO(listaPontoConsumo, lista);
			}
			// verificando se existe serviço de autorização com cliente mais sem ponto de consumo.
			List<ServicoAutorizacao> listaServioComClienteSemPontoConsumo = new ArrayList<ServicoAutorizacao>();
			for (ServicoAutorizacao servicoAutorizacao : lista) {
				// verifica na lista geral se o sevico tem o cliente e não tem ponto de consumo
				if(servicoAutorizacao.getCliente() != null && servicoAutorizacao.getCliente().equals(cliente)
								&& servicoAutorizacao.getPontoConsumo() == null) {
					// caso tenha adiciona em uma lista
					listaServioComClienteSemPontoConsumo.add(servicoAutorizacao);
				}
			}
			List<PontoConsumoVO> listaPontoVOVazio = null;
			if(!listaServioComClienteSemPontoConsumo.isEmpty()) {
				// chama o método que cria o pondo de consumo vazio e seta em uma lista vazia e logo apos seta no cliente
				listaPontoVOVazio = criarListaPontoConsumoVOVazio(listaServioComClienteSemPontoConsumo);
				numeroPontoConsumoVazio += listaPontoVOVazio.size();
			}
			if(listaPontoVOVazio != null) {
				listaPontoConsumoVO.addAll(listaPontoVOVazio);
			}
			if(!listaPontoConsumoVO.isEmpty()) {
				clienteVO.setListaPontoConsumoVO(listaPontoConsumoVO);
				listaClienteVO.add(clienteVO);
			}

		}

		// criando as listas de pontos de consumo que não tem cliente acossiado
		List<ServicoAutorizacao> listaSemPontoConsumo = new ArrayList<ServicoAutorizacao>();
		List<ServicoAutorizacao> listaComPontoConsumo = new ArrayList<ServicoAutorizacao>();
		for (ServicoAutorizacao servicoAutorizacao : listaServicosSemCliente) {
			if(servicoAutorizacao.getPontoConsumo() == null) {
				listaSemPontoConsumo.add(servicoAutorizacao);
			} else {
				listaComPontoConsumo.add(servicoAutorizacao);
			}
		}

		Cliente cliente = new ClienteImpl();
		// criar cliente vazio onde o serviço de autorização contem ponto de consumo mais o ponto de consumo não contem cliente.
		ClienteVO clienteVazio = null;
		if(!listaComPontoConsumo.isEmpty()) {
			clienteVazio = new ClienteVO();
			clienteVazio.setCliente(cliente);
			List<PontoConsumo> listaPontoConsumoConcreto = new ArrayList<PontoConsumo>();
			for (ServicoAutorizacao servicoAutorizacao : listaComPontoConsumo) {
				if(servicoAutorizacao.getPontoConsumo() != null) {
					PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(servicoAutorizacao.getPontoConsumo()
									.getChavePrimaria());
					if(pontoConsumo != null && !listaPontoConsumoConcreto.contains(pontoConsumo)) {
						listaPontoConsumoConcreto.add(pontoConsumo);
					}
				}
			}
			clienteVazio.setListaPontoConsumoVO(converterPontoConsumoParaPontoConsumoVO(listaPontoConsumoConcreto, lista));
		}

		// criar cliente e ponto de consumo vazio para os servicos que n contem os objetos

		ClienteVO clienteVazio2 = null;
		if(!listaSemPontoConsumo.isEmpty()) {
			clienteVazio2 = new ClienteVO();
			clienteVazio2.setCliente(cliente);
			List<PontoConsumoVO> listPontoConsumoVOVazio = new ArrayList<PontoConsumoVO>();
			PontoConsumoVO pontoConsumoVOVazio = new PontoConsumoVO();
			pontoConsumoVOVazio.setListaServicoAutorizacao(listaSemPontoConsumo);
			listPontoConsumoVOVazio.add(pontoConsumoVOVazio);
			clienteVazio2.setListaPontoConsumoVO(listPontoConsumoVOVazio);
			numeroPontoConsumoVazio++;
		}
		int numeroClientesVazios = 0;
		if(clienteVazio != null) {
			listaClienteVO.add(clienteVazio);
			numeroClientesVazios++;
		}
		if(clienteVazio2 != null) {
			listaClienteVO.add(clienteVazio2);
			numeroClientesVazios++;
		}

		List<ServicoAutorizacaoRelatorioVO> listaDados = new ArrayList<ServicoAutorizacaoRelatorioVO>();
		if(!listaClienteVO.isEmpty()) {
			ServicoAutorizacaoRelatorioVO servicoAutorizacaoRelatorioVO = new ServicoAutorizacaoRelatorioVO();
			servicoAutorizacaoRelatorioVO.setListaClienteVO(listaClienteVO);
			listaDados.add(servicoAutorizacaoRelatorioVO);

			servicoAutorizacaoRelatorioVO.setTotalGeralCliente(listaClienteVO.size() - numeroClientesVazios);
			int valorTotalGeralPontoCOnsumo = 0;
			int valorTotalServicoAutorizacao = 0;
			for (ClienteVO client : listaClienteVO) {
				valorTotalGeralPontoCOnsumo += client.getListaPontoConsumoVO().size();
				for (PontoConsumoVO pontoConsumoVO : client.getListaPontoConsumoVO()) {
					valorTotalServicoAutorizacao += pontoConsumoVO.getListaServicoAutorizacao().size();
				}
			}
			servicoAutorizacaoRelatorioVO.setTotalAutorizacaoServico(valorTotalServicoAutorizacao);
			valorTotalGeralPontoCOnsumo -= numeroPontoConsumoVazio;
			servicoAutorizacaoRelatorioVO.setTotalGeralPontoConsumo(valorTotalGeralPontoCOnsumo);

		}
		return listaDados;
	}

	/**
	 * lista foi montada apartir do objeto cliente por conta do relatorio que recebe o objeto cliente.
	 * 
	 * @param lista {@link List}
	 * @return listaDados {@link List}
	 */
	private List<ServicoAutorizacaoRelatorioVO> montarListaServicoAutorizacaoRelatorioVOPorEquipe(List<ServicoAutorizacao> lista) {

		int valorTotalGeralPontoCOnsumo = 0;
		List<Equipe> listaEquipe = new ArrayList<Equipe>();
		List<ServicoAutorizacao> listaServicoSemPontoConsumo = new ArrayList<ServicoAutorizacao>();
		for (ServicoAutorizacao servicoAutorizacao : lista) {
			if (servicoAutorizacao.getEquipe() != null && !listaEquipe.contains(servicoAutorizacao.getEquipe())) {
				listaEquipe.add(servicoAutorizacao.getEquipe());
			}
			if (servicoAutorizacao.getPontoConsumo() == null) {
				listaServicoSemPontoConsumo.add(servicoAutorizacao);
			}

		}
		List<EquipeVO> listaEquipesVO = new ArrayList<EquipeVO>();
		List<ClienteVO> listaClienteVO = new ArrayList<ClienteVO>();
		EquipeVO equipeVO = null;
		for (Equipe equipe : listaEquipe) {
			// cliente
			ClienteVO clienteVO = new ClienteVO();
			Cliente cliente = new ClienteImpl();
			cliente.setNome(equipe.getNome());
			clienteVO.setCliente(cliente);

			equipeVO = new EquipeVO();
			equipeVO.setEquipe(equipe);
			List<PontoConsumo> listaPontoCosumo = new ArrayList<PontoConsumo>(listarPontosConsumoEquipe(lista, equipe));

			// cliente
			clienteVO.setListaPontoConsumoVO(converterPontoConsumoParaPontoConsumoVO(listaPontoCosumo, lista));

			if(clienteVO.getListaPontoConsumoVO() != null) {
				valorTotalGeralPontoCOnsumo += clienteVO.getListaPontoConsumoVO().size();
			}
			if(!listaServicoSemPontoConsumo.isEmpty()) {
				clienteVO.getListaPontoConsumoVO().addAll(this.criarListaPontoConsumoVOVazio(listaServicoSemPontoConsumo));
			}
			listaClienteVO.add(clienteVO);
		}

		List<ServicoAutorizacaoRelatorioVO> listaDados = new ArrayList<ServicoAutorizacaoRelatorioVO>();
		verificaListaClienteVO(lista, valorTotalGeralPontoCOnsumo, listaEquipesVO, listaClienteVO, listaDados);
		return listaDados;
	}

	private void verificaListaClienteVO(List<ServicoAutorizacao> lista, int valorTotalGeralPontoCOnsumo,
			List<EquipeVO> listaEquipesVO, List<ClienteVO> listaClienteVO,
			List<ServicoAutorizacaoRelatorioVO> listaDados) {
		if(!listaClienteVO.isEmpty()) {

			ServicoAutorizacaoRelatorioVO servicoAutorizacaoRelatorioVO = new ServicoAutorizacaoRelatorioVO();
			servicoAutorizacaoRelatorioVO.setListaClienteVO(listaClienteVO);
			listaDados.add(servicoAutorizacaoRelatorioVO);

			servicoAutorizacaoRelatorioVO.setTotalGeralCliente(listaEquipesVO.size());

			int valorTotalServicoAutorizacao = lista.size();

			servicoAutorizacaoRelatorioVO.setTotalAutorizacaoServico(valorTotalServicoAutorizacao);
			servicoAutorizacaoRelatorioVO.setTotalGeralPontoConsumo(valorTotalGeralPontoCOnsumo);
		}
	}

	/**
	 * Converter ponto consumo para ponto consumo vo.
	 * 
	 * @param listaPontoConsumo
	 *            the lista ponto consumo
	 * @param listaServicos
	 *            the lista servicos
	 * @return the list
	 */
	private List<PontoConsumoVO> converterPontoConsumoParaPontoConsumoVO(List<PontoConsumo> listaPontoConsumo,
					List<ServicoAutorizacao> listaServicos) {

		List<PontoConsumoVO> listaPontoConsulmoVO = new ArrayList<PontoConsumoVO>();
		// criação da lista retornada
		PontoConsumoVO pontoConsumoVo = null;
		for (PontoConsumo pontoConsumo : listaPontoConsumo) {
			pontoConsumoVo = new PontoConsumoVO();
			pontoConsumoVo.setPontoConsumo(pontoConsumo);
			List<ServicoAutorizacao> lista = new ArrayList<ServicoAutorizacao>();
			for (ServicoAutorizacao servicoAutorizacao : listaServicos) {
				if(servicoAutorizacao.getPontoConsumo() != null && servicoAutorizacao.getPontoConsumo().equals(pontoConsumo)) {
					lista.add(servicoAutorizacao);
				}
			}
			pontoConsumoVo.setListaServicoAutorizacao(lista);
			if(pontoConsumoVo.getListaServicoAutorizacao() != null && !pontoConsumoVo.getListaServicoAutorizacao().isEmpty()) {
				listaPontoConsulmoVO.add(pontoConsumoVo);
			}

		}

		return listaPontoConsulmoVO;
	}

	/**
	 * Criar lista ponto consumo vo vazio.
	 * 
	 * @param lista
	 *            the lista
	 * @return the list
	 */
	private List<PontoConsumoVO> criarListaPontoConsumoVOVazio(List<ServicoAutorizacao> lista) {

		List<PontoConsumoVO> listPontoConsumoVOVazio = new ArrayList<PontoConsumoVO>();
		PontoConsumoVO pontoConsumoVOVazio = new PontoConsumoVO();
		pontoConsumoVOVazio.setListaServicoAutorizacao(lista);
		listPontoConsumoVOVazio.add(pontoConsumoVOVazio);

		return listPontoConsumoVOVazio;

	}

	/**
	 * Listar pontos consumo equipe.
	 * 
	 * @param listaServico
	 *            the lista servico
	 * @param equipe
	 *            the equipe
	 * @return the list
	 */
	private List<PontoConsumo> listarPontosConsumoEquipe(List<ServicoAutorizacao> listaServico, Equipe equipe) {

		List<PontoConsumo> lista = new ArrayList<PontoConsumo>();
		for (ServicoAutorizacao servicoAutorizacao : listaServico) {
			if(servicoAutorizacao.getEquipe() != null && servicoAutorizacao.getEquipe().equals(equipe)) {
				lista.add(servicoAutorizacao.getPontoConsumo());
			}
		}
		return lista;
	}

	@SuppressWarnings({"unchecked","rawtypes"})
	@Override
	public byte[] gerarRelatorioServicoAutorizacaoResumo(ServicoAutorizacaoVO servicoAutorizacaoVO,
			FormatoImpressao formatoImpressao, String agrupamento, String exibirFiltros)
			throws ParseException, GGASException {
		
		List<ServicoAutorizacaoResumoVO> listagemAutorizacaoServicoResumo = (List<ServicoAutorizacaoResumoVO>) controladorServicoAutorizacao.consultarServicoAutorizacaoResumo(servicoAutorizacaoVO,
				formatoImpressao, agrupamento,exibirFiltros, Boolean.FALSE);
		
		List<ServicoAutorizacaoResumoVO> listagemTopCincoAutorizacaoServicoResumo = (List<ServicoAutorizacaoResumoVO>) controladorServicoAutorizacao.consultarServicoAutorizacaoResumo(servicoAutorizacaoVO,
				formatoImpressao, agrupamento,exibirFiltros, Boolean.TRUE);
		

		if(listagemAutorizacaoServicoResumo.isEmpty()) {
			throw new NegocioException("ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO", true);
		}
		
		Map parametros = montarParametrosRelatorio(servicoAutorizacaoVO, exibirFiltros, agrupamento);
		
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
				ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		
		Long totalAS = listagemAutorizacaoServicoResumo != null && !listagemAutorizacaoServicoResumo.isEmpty() ? 
				listagemAutorizacaoServicoResumo.stream()
                .mapToLong(ServicoAutorizacaoResumoVO::getTotal)
                .sum() : null;
		
		Collection<ServicoAutorizacaoResumoVO> dados = new ArrayList<ServicoAutorizacaoResumoVO>();
		
		dados.add(listagemAutorizacaoServicoResumo.get(0));
		
		parametros.put("imagem",
				Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
		parametros.put("listagemAutorizacaoServicoResumo", listagemAutorizacaoServicoResumo);
		parametros.put("listagemTopCincoAutorizacaoServicoResumo", listagemTopCincoAutorizacaoServicoResumo);
		parametros.put("totalAS", totalAS);

		
		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_SERVICO_AUTORIZACAO_RESUMO, formatoImpressao);
	}



}
