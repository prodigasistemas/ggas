package br.com.ggas.relatorio.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioPrazoMinimoEntregaFatura;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.web.relatorio.prazoParaEntregaFatura.FaturaVO;
import br.com.ggas.web.relatorio.prazoParaEntregaFatura.ParamentroRelatorio;
import br.com.ggas.web.relatorio.prazoParaEntregaFatura.RepositorioPrazoMinimoEntregaFatura;
/**
 * Classe responsável pelas operações referentes ao relatório da entidade 
 * PrazoMinimoEntregaFaturaImpl
 */
@Service("controladorRelatorioPrazoMinimoEntregaFaturaImpl")
@Transactional
public class ControladorRelatorioPrazoMinimoEntregaFaturaImpl implements ControladorRelatorioPrazoMinimoEntregaFatura{

	@Autowired
	private RepositorioPrazoMinimoEntregaFatura repositorioPrazoMinimoEntregaFatura;
	
	private static String STATUS_NO_PRAZO = "No Prazo";
	
	private static String STATUS_FORA_PRAZO = "Fora do Prazo";

	private static String VALOR_PARAMETRO = "PARAMETRO_PRAZO_MIN_ENTREGA_FATURA_VENCIMENTO";
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioPontosDeConsumoPorDataImpl(java.util.Map)
	 */
	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public byte[] gerarRelatorio(Date dataIncial, Date dataFinal, String exibirFiltros, String tipoRelatorio) throws GGASException {

		Collection<ParamentroRelatorio> collRelatorio = new ArrayList<ParamentroRelatorio>();
		ParamentroRelatorio parametroRelatorio;

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(tipoRelatorio);
		List<FaturaVO> listaFatura = (List<FaturaVO>) 
						this.listaFaturasPrazoDeVencimento(dataIncial, dataFinal);

		int paramentro = this.obterParametro();
		
		String status = "";

		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar dataLimite = Calendar.getInstance();

		for (int i = 0; i < listaFatura.size(); i++) {

			parametroRelatorio = new ParamentroRelatorio(); 
			parametroRelatorio.setDataVencimentoFatura(sf.format(listaFatura.get(i).getDataVencimentoFatura()));
			parametroRelatorio.setDataFatura((sf.format(listaFatura.get(i).getDataFatura())));
			parametroRelatorio.setNomeCliente(listaFatura.get(i).getNomeCliente());
			
			dataLimite.setTime( listaFatura.get(i).getDataVencimentoFatura() );
			dataLimite.add(Calendar.DAY_OF_MONTH, -paramentro);
			
			if(listaFatura.get(i).getDataVencimentoFatura() != null){
				
				if(listaFatura.get(i).getDataFatura().compareTo(dataLimite.getTime() ) > 0 ){
					status = STATUS_FORA_PRAZO;
				}else{
					status = STATUS_NO_PRAZO;
				}
			}else{
				status = STATUS_FORA_PRAZO;
			}
			
			parametroRelatorio.setDataLimite(sf.format(dataLimite.getTime()));
			parametroRelatorio.setStatus(status);

			collRelatorio.add(parametroRelatorio);
		}

		String dataParamInicio = null;
		String artigo = null;
		if(dataIncial != null){
			dataParamInicio = sf.format(dataIncial);
			artigo = "à";
		}
		String dataParamFim = null;
		if(dataFinal != null){
			dataParamFim = sf.format(dataFinal);
		}

		String dataImpressao = sf.format(Calendar.getInstance().getTime());
		Map parametros = new HashMap();
		parametros.put("dataInicial", dataParamInicio);
		parametros.put("dataFinal", dataParamFim);
		parametros.put("dataImpressao", dataImpressao);
		parametros.put("formatoImpressao", formatoImpressao);
		parametros.put("artigo", artigo);

		this.validarDadosGerarRelatorioPontoConsumoPorData(parametros);

		return RelatorioUtil.gerarRelatorio(collRelatorio, parametros, RELATORIO_PRAZO_MINIMO_ENTREGA_FATURA_JASPER, formatoImpressao);
	}
	
	/**
	 * Validar dados gerar relatorio pontos de consumo por data.
	 * 
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosGerarRelatorioPontoConsumoPorData(Map<String, Object> parametros) throws NegocioException {

		if (parametros.get("formatoImpressao") == null) {

			throw new NegocioException(ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO, true);

		}

	}

	private Collection<FaturaVO> listaFaturasPrazoDeVencimento(Date dataIncio, Date dataFinal) {

		return repositorioPrazoMinimoEntregaFatura.listaFaturasPrazoDeVencimento(dataIncio, dataFinal);
	}

	private int obterParametro() {

		String parametro = "";

		List<String> parametros = repositorioPrazoMinimoEntregaFatura.buscarValorParamentro(VALOR_PARAMETRO);

		for (int i = 0; i < parametros.size(); i++) {
			parametro = parametros.get(i);
		}

		return Integer.parseInt(parametro);
	}
}
