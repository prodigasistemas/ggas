/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.CityGateMedicao;
import br.com.ggas.medicao.consumo.ControladorCityGateMedicao;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioVolumeCompradoDistribuido;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.ListaUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.volumeCompradoDistribuido.RelatorioVolumeCompradoDistribuidoAction;
import br.com.ggas.web.relatorio.volumeCompradoDistribuido.RelatorioVolumeCompradoDistribuidoVO;
import br.com.ggas.web.relatorio.volumeCompradoDistribuido.SegmentoMedidoFaturaVO;
import br.com.ggas.web.relatorio.volumeCompradoDistribuido.VolumeCompradoDistribuidoVO;

public class ControladorRelatorioVolumeCompradoDistribuidoImpl extends ControladorNegocioImpl 
	implements ControladorRelatorioVolumeCompradoDistribuido {

	private static final int POSICAO_SEXTO_SEGMENTO = 5;

	private static final int POSICAO_QUINTO_SEGMENTO = 4;

	private static final int POSICAO_QUARTO_SEGMENTO = 3;

	private static final int POSICAO_TERCEIRO_SEGMENTO = 2;

	private static final int POSICAO_SEGUNDO_SEGMENTO = 1;

	private static final int POSICAO_PRIMEIRO_SEGMENTO = 0;

	private static final String[] PARAMETROS_RELATORIO_CABECALHO_COLUNAS = 
				{"cabecalhoPrimeiroSegmento", "cabecalhoSegundoSegmento", 
				 "cabecalhoTerceiroSegmento", "cabecalhoQuartoSegmento", 
				 "cabecalhoQuintoSegmento"};

	private static final String RELATORIO_FATURAMENTO = "relatorioVolumeCompradoDistribuido.jasper";

	private static final String DATA_EMISSAO_INICIAL = "dataEmissaoInicial";

	private static final String DATA_EMISSAO_FINAL = "dataEmissaoFinal";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioVolumeCompradoDistribuido#gerarRelatorio(java.util.Map,
	 * br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorio(Map<String, Object> filtro, FormatoImpressao formatoImpressao) 
					throws GGASException {

		validarFiltro(filtro);

		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia().getBeanPorID(
						ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long codigoDocumentoTipoFatura = Long.parseLong(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA));
		Long codigoCredDebSituacaoNormal = Long.parseLong(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_NORMAL));
		Long codigoCredDebSituacaoIncluida = Long.parseLong(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_INCLUIDO));
		Long codigoCredDebSituacaoParcelada = Long.parseLong((String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CREDITO_DEBITO_SITUACAO_PARCELADA));
		Collection<Long> listaCredDebSitua = new ArrayList<Long>();
		listaCredDebSitua.add(codigoCredDebSituacaoNormal);
		listaCredDebSitua.add(codigoCredDebSituacaoIncluida);
		listaCredDebSitua.add(codigoCredDebSituacaoParcelada);

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Map<String, Object> parametros = new HashMap<String, Object>();
		if(controladorEmpresa.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem",
							Constantes.URL_LOGOMARCA_EMPRESA
											+ String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		}

		montarParametroRelatorio(parametros, filtro);
		filtro.put("idCreditoDebitoSituacoes", listaCredDebSitua);
		filtro.put("idTipoDocumento", codigoDocumentoTipoFatura);
		filtro.put("possuiHistoricoConsumo", "true");

		Collection<RelatorioVolumeCompradoDistribuidoVO> listaRelatorioVolumeCompradoDistribuidoVO = 
						this.montarListaDadosRelatorio(filtro);
		
		if(listaRelatorioVolumeCompradoDistribuidoVO.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return RelatorioUtil.gerarRelatorio(listaRelatorioVolumeCompradoDistribuidoVO, parametros, RELATORIO_FATURAMENTO, formatoImpressao);
	}

	/**
	 * Validar filtro.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarFiltro(Map<String, Object> filtro) throws NegocioException {

		if(!(filtro.containsKey(DATA_EMISSAO_INICIAL) && filtro.containsKey(DATA_EMISSAO_FINAL))) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_PERIODO_INICIO_DATA_PERIODO_FINAL, true);
		}
		Date dataEmissaoInicial = (Date) filtro.get(DATA_EMISSAO_INICIAL);
		Date dataEmissaoFinal = (Date) filtro.get(DATA_EMISSAO_FINAL);
		if((dataEmissaoInicial != null) && (dataEmissaoFinal != null) && (Util.compararDatas(dataEmissaoInicial, dataEmissaoFinal) > 0)) {
			
			throw new NegocioException(ERRO_NEGOCIO_DATA_EMISSAO_INICIO_MAIOR_DATA_EMISSAO_FINAL, true);
		}
	}

	/**
	 * Montar lista dados relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<RelatorioVolumeCompradoDistribuidoVO> montarListaDadosRelatorio(
					Map<String, Object> filtro) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorCityGateMedicao controladorCityGateMedicao = (ControladorCityGateMedicao) ServiceLocator.getInstancia().getBeanPorID(
						ControladorCityGateMedicao.BEAN_ID_CONTROLADOR_CITY_GATE_MEDICAO);

		String codigoRubricaGas = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS);
		String codigoRubricaMargem = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MARGEM_DISTRIBUICAO);
		String codigoRubricaTransporte = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TRANSPORTE);

		Collection<Fatura> listaFaturas = new ArrayList<Fatura>();

		Long[] cpsAdicionadas = (Long[]) filtro.get(RelatorioVolumeCompradoDistribuidoAction.CPS_ADICIONADAS);
		
		// obter as faturas sem duplicatas em
		// relação ao consumo historico
		Collection<Fatura> listaFaturasDuplicadas = controladorFatura.consultarFatura(filtro);
		Map<HistoricoConsumo, Collection<Fatura>> mapa = controladorFatura.agruparFaturasPorHistoricoConsumo(listaFaturasDuplicadas);
		Collection<Collection<Fatura>> colecaoAgrupadaFaturas = mapa.values();
		for (Collection<Fatura> colecao : colecaoAgrupadaFaturas) {
			listaFaturas.add(colecao.iterator().next());
		}

		Map<Date, VolumeCompradoDistribuidoVO> mapaDataFatura = new LinkedHashMap<Date, VolumeCompradoDistribuidoVO>();

		for (Fatura fatura : listaFaturas) {

			VolumeCompradoDistribuidoVO volumeCompradoDistribuidoVO;

			if(mapaDataFatura.containsKey(fatura.getDataEmissao())) {
				volumeCompradoDistribuidoVO = mapaDataFatura.get(fatura.getDataEmissao());

			} else {
				volumeCompradoDistribuidoVO = new VolumeCompradoDistribuidoVO();
				volumeCompradoDistribuidoVO.setListaCityGateMedicao(controladorCityGateMedicao.listarCityGateMedicaoPorData(fatura
								.getDataEmissao()));
				volumeCompradoDistribuidoVO.setData(fatura.getDataEmissao());
			}

			// dividir volume medido e faturado
			// por segmento
			if(fatura.getSegmento() != null) {

				SegmentoMedidoFaturaVO segmentoMedidoFaturaVO = new SegmentoMedidoFaturaVO();
				segmentoMedidoFaturaVO.setSegmento(fatura.getSegmento());

				BigDecimal diasConsumoFatura = BigDecimal.ONE;
				if(fatura.getHistoricoConsumo().getDiasConsumo().intValue() != 0) {
					diasConsumoFatura = new BigDecimal(fatura.getHistoricoConsumo().getDiasConsumo());
				}

				BigDecimal volumeMedido = BigDecimal.ZERO;
				if(fatura.getHistoricoConsumo().getConsumoMedido() != null) {
					volumeMedido = fatura.getHistoricoConsumo().getConsumoMedido().divide(diasConsumoFatura, RoundingMode.HALF_UP)
									.setScale(4, RoundingMode.HALF_UP);
				}
				segmentoMedidoFaturaVO.setMedido(volumeMedido);
				segmentoMedidoFaturaVO.setFaturado(BigDecimal.ZERO);

				for (FaturaItem faturaItem : fatura.getListaFaturaItem()) {
					if (faturaItem.getRubrica().getItemFatura() != null
									&& (String.valueOf(faturaItem.getRubrica().getItemFatura().getChavePrimaria()).equals(codigoRubricaGas)
									|| String.valueOf(faturaItem.getRubrica().getItemFatura().getChavePrimaria()).equals(codigoRubricaMargem)
									|| String.valueOf(faturaItem.getRubrica().getItemFatura().getChavePrimaria()).equals(codigoRubricaTransporte))) {

						segmentoMedidoFaturaVO.setFaturado(faturaItem.getMedidaConsumo());
					}
				}

				// acumular
				if(volumeCompradoDistribuidoVO.getSegmentoMedidoFaturado().containsKey(fatura.getSegmento())) {
					SegmentoMedidoFaturaVO retornoMap = volumeCompradoDistribuidoVO.getSegmentoMedidoFaturado().get(fatura.getSegmento());

					segmentoMedidoFaturaVO.setMedido(segmentoMedidoFaturaVO.getMedido().add(retornoMap.getMedido()));
					segmentoMedidoFaturaVO.setFaturado(segmentoMedidoFaturaVO.getFaturado().add(retornoMap.getFaturado()));
				}

				if(segmentoMedidoFaturaVO.getFaturado() != null && segmentoMedidoFaturaVO.getMedido() != null
								&& segmentoMedidoFaturaVO.getSegmento() != null) {
					volumeCompradoDistribuidoVO.getSegmentoMedidoFaturado().put(fatura.getSegmento(), segmentoMedidoFaturaVO);
				}
			}

			mapaDataFatura.put(volumeCompradoDistribuidoVO.getData(), volumeCompradoDistribuidoVO);

		}

		Collection<VolumeCompradoDistribuidoVO> lista = mapaDataFatura.values();

		for (VolumeCompradoDistribuidoVO volumeCompradoDistribuidoVO : lista) {
			BigDecimal totalMedido = new BigDecimal(0);
			BigDecimal totalFaturado = new BigDecimal(0);

			for (SegmentoMedidoFaturaVO segmentoMedidoFaturaVO : volumeCompradoDistribuidoVO.getSegmentoMedidoFaturado().values()) {
				totalMedido = totalMedido.add(segmentoMedidoFaturaVO.getMedido());
				totalFaturado = totalFaturado.add(segmentoMedidoFaturaVO.getFaturado());
			}
			volumeCompradoDistribuidoVO.setTotalMedido(totalMedido);
			volumeCompradoDistribuidoVO.setTotalFaturado(totalFaturado);
		}

		return montarRelatorioVolumeCompradoDistribuidoVO(lista, cpsAdicionadas);
	}

	/**
	 * Montar relatorio volume comprado distribuido vo.
	 * 
	 * @param lista
	 *            the lista
	 * @param cpsAdicionadas 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Collection<RelatorioVolumeCompradoDistribuidoVO> montarRelatorioVolumeCompradoDistribuidoVO(
					Collection<VolumeCompradoDistribuidoVO> lista, Long[] cpsAdicionadas) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = 
						(ControladorParametroSistema) ServiceLocator.getInstancia().getBeanPorID(
										ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		
		Long codigoPrimeiroSegmento = ListaUtil.getElementByIndex(cpsAdicionadas, POSICAO_PRIMEIRO_SEGMENTO);
		Long codigoSegundoSegmento = ListaUtil.getElementByIndex(cpsAdicionadas, POSICAO_SEGUNDO_SEGMENTO);
		Long codigoTerceiroSegmento = ListaUtil.getElementByIndex(cpsAdicionadas, POSICAO_TERCEIRO_SEGMENTO);
		Long codigoQuartaSegmento = ListaUtil.getElementByIndex(cpsAdicionadas, POSICAO_QUARTO_SEGMENTO);
		Long codigoQuintaSegmento = ListaUtil.getElementByIndex(cpsAdicionadas, POSICAO_QUINTO_SEGMENTO);
		Long codigoSextoSegmento = ListaUtil.getElementByIndex(cpsAdicionadas, POSICAO_SEXTO_SEGMENTO);
		
		String pcsReferencia = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_PCS_REFERENCIA);

		ControladorCityGateMedicao controladorCityGateMedicao = (ControladorCityGateMedicao) ServiceLocator.getInstancia().getBeanPorID(
						ControladorCityGateMedicao.BEAN_ID_CONTROLADOR_CITY_GATE_MEDICAO);
		Collection<RelatorioVolumeCompradoDistribuidoVO> relatorio = new ArrayList<RelatorioVolumeCompradoDistribuidoVO>();

		for (VolumeCompradoDistribuidoVO volumeCompradoDistribuidoVO : lista) {
			RelatorioVolumeCompradoDistribuidoVO dado = new RelatorioVolumeCompradoDistribuidoVO();
			dado.setData(volumeCompradoDistribuidoVO.getData());

			Collection<CityGateMedicao> listaCityGateCityGateMedicao = controladorCityGateMedicao
							.listarCityGateMedicaoPorData(volumeCompradoDistribuidoVO.getData());
			Double volumeDistribuidora = 0D;
			Double volumeSupridora = 0D;
			Double comprado = 0D;
			for (CityGateMedicao cityGateMedicao : listaCityGateCityGateMedicao) {
				if(cityGateMedicao.getMedidaVolumeDistribuidora() != null) {
					volumeDistribuidora = volumeDistribuidora + cityGateMedicao.getMedidaVolumeDistribuidora();
				}

				if(cityGateMedicao.getMedidaVolumeSupridora() != null) {
					volumeSupridora = volumeSupridora + cityGateMedicao.getMedidaVolumeSupridora();
					comprado = comprado
									+ (cityGateMedicao.getMedidaVolumeSupridora() * (Double.parseDouble(cityGateMedicao
													.getMedidaVolumeSupridora().toString()) / Double.parseDouble(pcsReferencia)));
				}
			}
			Double m3 = volumeSupridora - volumeDistribuidora;
			Double percent = ((volumeSupridora - volumeDistribuidora) / volumeSupridora) * 100;

			dado.setVolumeDistribuidora(volumeDistribuidora);
			dado.setVolumeSupridora(volumeSupridora);
			dado.setComprado(comprado);
			dado.setM3(m3);
			dado.setPercent(percent);

			Collection<SegmentoMedidoFaturaVO> listaSegmentoMedidoFaturaVO = 
							volumeCompradoDistribuidoVO.getSegmentoMedidoFaturado().values();
			
			for (SegmentoMedidoFaturaVO segmentoMedidoFaturaVO : listaSegmentoMedidoFaturaVO) {
				
				long cpMedidoFaturaVo = segmentoMedidoFaturaVO.getSegmento().getChavePrimaria();
				
				if(Objects.equals(codigoPrimeiroSegmento, cpMedidoFaturaVo)) {
					dado.setComercialMedido(segmentoMedidoFaturaVO.getMedido());
					dado.setComercialFaturado(segmentoMedidoFaturaVO.getFaturado());
				} else if(Objects.equals(codigoSegundoSegmento, cpMedidoFaturaVo)) {
					dado.setVeicularMedido(segmentoMedidoFaturaVO.getMedido());
					dado.setVeicularFaturado(segmentoMedidoFaturaVO.getFaturado());
				} else if(Objects.equals(codigoTerceiroSegmento, cpMedidoFaturaVo)) {
					dado.setIndustrialMedido(segmentoMedidoFaturaVO.getMedido());
					dado.setIndustrialFaturado(segmentoMedidoFaturaVO.getFaturado());
				} else if(Objects.equals(codigoQuartaSegmento, cpMedidoFaturaVo)) {
					dado.setResidencialMedido(segmentoMedidoFaturaVO.getMedido());
					dado.setResidencialFaturado(segmentoMedidoFaturaVO.getFaturado());
				} else if(Objects.equals(codigoSextoSegmento, cpMedidoFaturaVo)) {
					dado.setPublicoMedido(segmentoMedidoFaturaVO.getMedido());
					dado.setPublicoFaturado(segmentoMedidoFaturaVO.getFaturado());
				} else if(Objects.equals(codigoQuintaSegmento, cpMedidoFaturaVo)) {
					dado.setCogeracaoMedido(segmentoMedidoFaturaVO.getMedido());
					dado.setCogeracaoFaturado(segmentoMedidoFaturaVO.getFaturado());
				}
			}

			dado.setTotalMedido(volumeCompradoDistribuidoVO.getTotalMedido());
			dado.setTotalFaturado(volumeCompradoDistribuidoVO.getTotalFaturado());

			relatorio.add(dado);
		}

		return relatorio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/**
	 * Montar parametro relatorio.
	 * 
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void montarParametroRelatorio(Map<String, Object> parametros, 
					Map<String, Object> filtro) throws NegocioException {

		ControladorSegmento controladorSegmento =
						ServiceLocator.getInstancia().getControladorSegmento();
		
		if(filtro != null) {
			
			Date dataEmissaoInicial = (Date) filtro.get("dataEmissaoInicial");
			Date dataEmissaoFinal = (Date) filtro.get("dataEmissaoFinal");
			
			if((dataEmissaoInicial != null) && (dataEmissaoFinal != null)) {
				parametros.put("filtroDataEmissaoInicial", 
								DataUtil.converterDataParaString(dataEmissaoInicial));
				parametros.put("filtroDataEmissaoFinal", 
								DataUtil.converterDataParaString(dataEmissaoFinal));
			}

			Boolean exibirFiltros = (Boolean) filtro.get(EXIBIR_FILTROS);
			if (exibirFiltros == null) {
				parametros.put(EXIBIR_FILTROS, Boolean.FALSE);
			} else {
				parametros.put(EXIBIR_FILTROS, exibirFiltros);
			}
			
			Long[] cpsAdicionadas = (Long[]) filtro.get(
							RelatorioVolumeCompradoDistribuidoAction.CPS_ADICIONADAS);
			if (cpsAdicionadas != null) {
				for (int i = 0; i < cpsAdicionadas.length; i++) {
					Segmento segmento = (Segmento) controladorSegmento.obter(cpsAdicionadas[i]);
					parametros.put(PARAMETROS_RELATORIO_CABECALHO_COLUNAS[i], segmento.getDescricao());
				}
			}

		}
	}

}
