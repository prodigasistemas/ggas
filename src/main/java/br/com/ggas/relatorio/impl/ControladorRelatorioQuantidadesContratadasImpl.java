/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.contrato.contrato.ContratoQDC;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.relatorio.ControladorRelatorioQuantidadesContratadas;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.quantidadesContratadas.QuantidadesContratadasVO;
/**
 * 
 * Classe responsável pelas operações referentes as QuantidadesContratadas 
 * 
 */
public class ControladorRelatorioQuantidadesContratadasImpl extends ControladorNegocioImpl 
				implements ControladorRelatorioQuantidadesContratadas {

	private static final String RELATORIO_QUANTIDADES_CONTRATADAS = "relatorioQuantidadesContratadas.jasper";

	private static final String ERRO_NEGOCIO_RELATORIO_SEM_DADOS = "ERRO_NEGOCIO_RELATORIO_SEM_DADOS";

	private static final String DATA_INICIAL = "dataInicial";

	private static final String DATA_FINAL = "dataFinal";

	private static final String DATA_ASSINATURA = "dataAssinatura";

	private static final String DATA_VENCIMENTO = "dataVencimento";

	private static final String ID_CLIENTE = "idCliente";

	private static final String NUMERO_CONTRATO = "numeroContrato";

	private static final String NUMERO_CONTRATO_FORMATADO = "numeroContratoFormatado";

	private static final String ID_SEGMENTO = "idSegmento";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String NOME_CLIENTE = "nomeCliente";

	private static final String DESCRICAO_SEGMENTO = "descricaoSegmento";

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeContrato() {

		return ServiceLocator.getInstancia().getClassPorID(Contrato.BEAN_ID_CONTRATO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumoModalidade() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoModalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumoModalidadeQDC() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoModalidadeQDC.BEAN_ID_CONTRATO_PONTO_CONSUMO_MODALIDADE_QDC);
	}

	public Class<?> getClasseEntidadeContratoQDC() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoQDC.BEAN_ID_CONTRATO_QDC);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumoPenalidade() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoPenalidade.BEAN_ID_CONTRATO_PONTO_CONSUMO_PENALIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioQuantidadesContratadas#consultar(java.util.Map)
	 */
	@Override
	public byte[] consultar(Map<String, Object> filtro) {

		// TODO: Implementar
		return new byte[0];
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioQuantidadesContratadas#gerarRelatorio(java.util.Map,
	 * br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorio(Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException {

		Map<String, Object> parametros = new HashMap<String, Object>();
		if (this.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(this.obterEmpresaPrincipal().getChavePrimaria()));
		}

		Collection<QuantidadesContratadasVO> listaQuantidadesContratadasVO = this.montarListaDadosRelatorio(filtro);
		this.montarParametrosFiltro(parametros, filtro);

		return RelatorioUtil.gerarRelatorio(listaQuantidadesContratadasVO, parametros, RELATORIO_QUANTIDADES_CONTRATADAS, formatoImpressao);
	}

	/**
	 * Montar parametros filtro.
	 * 
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void montarParametrosFiltro(Map<String, Object> parametros, Map<String, Object> filtro) throws GGASException {

		ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getBeanPorID(
						ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
		ControladorSegmento controladorSegmento = (ControladorSegmento) ServiceLocator.getInstancia().getBeanPorID(
						ControladorSegmento.BEAN_ID_CONTROLADOR_SEGMENTO);

		Segmento segmento = null;
		Cliente cliente = null;
		Long idCliente = (Long) filtro.get(ID_CLIENTE);
		Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
		String numeroContratoFormatado = (String) filtro.get(NUMERO_CONTRATO_FORMATADO);
		Date dataInicial = (Date) filtro.get(DATA_INICIAL);
		Date dataFinal = (Date) filtro.get(DATA_FINAL);
		Date dataAssinatura = (Date) filtro.get(DATA_ASSINATURA);
		Date dataVencimento = (Date) filtro.get(DATA_VENCIMENTO);

		if (idSegmento != null && idSegmento > 0) {
			segmento = controladorSegmento.obterSegmento(idSegmento);
		}
		if (idCliente != null && idCliente > 0) {
			cliente = controladorCliente.obterCliente(idCliente, "enderecos");
		}

		Boolean exibirFiltro = (Boolean) filtro.get(EXIBIR_FILTROS);
		parametros.put(EXIBIR_FILTROS, exibirFiltro);
		if (exibirFiltro) {
			if (segmento != null) {
				parametros.put(DESCRICAO_SEGMENTO, segmento.getDescricao());
			}
			if (cliente != null) {
				parametros.put(NOME_CLIENTE, cliente.getNome());
			}
			if (!StringUtils.isEmpty(numeroContratoFormatado)) {
				parametros.put(NUMERO_CONTRATO, numeroContratoFormatado);
			}
			if (dataAssinatura != null) {
				parametros.put(DATA_ASSINATURA, Util.converterDataParaStringSemHora(dataAssinatura, Constantes.FORMATO_DATA_BR));
			}
			if (dataVencimento != null) {
				parametros.put(DATA_VENCIMENTO, Util.converterDataParaStringSemHora(dataVencimento, Constantes.FORMATO_DATA_BR));
			}
			if (dataInicial != null) {
				parametros.put(DATA_INICIAL, Util.converterDataParaStringSemHora(dataInicial, Constantes.FORMATO_DATA_BR));
			}
			if (dataFinal != null) {
				parametros.put(DATA_FINAL, Util.converterDataParaStringSemHora(dataFinal, Constantes.FORMATO_DATA_BR));
			}
		}

	}

	/**
	 * Obter empresa principal.
	 * 
	 * @return the empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Empresa obterEmpresaPrincipal() throws NegocioException {

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("principal", Boolean.TRUE);
		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtro);

		Empresa empresa = null;
		if ((listaEmpresas != null) && (!listaEmpresas.isEmpty())) {
			empresa = ((List<Empresa>) listaEmpresas).get(0);
		}

		return empresa;
	}

	// ordenar a lista
	// contratoPontoConsumoModalidadeQDC em ordem
	/**
	 * Ordena contrato pc mod qdc por data vigencia.
	 * 
	 * @param contratoPontoConsumoModalidade
	 *            the contrato ponto consumo modalidade
	 * @return the list
	 */
	// decrescente em relação a DataVigencia
	private List<ContratoPontoConsumoModalidadeQDC> ordenaContratoPCModQDCPorDataVigencia(
					ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) {

		List<ContratoPontoConsumoModalidadeQDC> listaAux = new ArrayList<ContratoPontoConsumoModalidadeQDC>();
		for (ContratoPontoConsumoModalidadeQDC contratoPCModQDC : contratoPontoConsumoModalidade
						.getListaContratoPontoConsumoModalidadeQDC()) {
			listaAux.add(contratoPCModQDC);
		}
		Collections.sort(listaAux, new Comparator<ContratoPontoConsumoModalidadeQDC>(){

			@Override
			public int compare(ContratoPontoConsumoModalidadeQDC o1, ContratoPontoConsumoModalidadeQDC o2) {

				return o1.getDataVigencia().compareTo(o2.getDataVigencia());
			}
		});

		Collections.reverse(listaAux);
		return listaAux;
	}

	/**
	 * Montar lista dados relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<QuantidadesContratadasVO> montarListaDadosRelatorio(Map<String, Object> filtro) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Collection<QuantidadesContratadasVO> listaQuantidadesContratadasVO = new ArrayList<QuantidadesContratadasVO>();

		Date dataInicial = (Date) filtro.get(DATA_INICIAL);
		Date dataFinal = (Date) filtro.get(DATA_FINAL);

		Collection<Object[]> listaObjetosConsulta = this.obterDadosRelatorio(filtro);
		if (listaObjetosConsulta != null && !listaObjetosConsulta.isEmpty()) {
			for (Object[] resultado : listaObjetosConsulta) {

				ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) resultado[0];
				PontoConsumo pontoConsumo = (PontoConsumo) resultado[1];
				Imovel imovel = (Imovel) resultado[2];
				Segmento segmento = (Segmento) resultado[3];
				Contrato contrato = (Contrato) resultado[4];

				// Procura por QDC do Ponto de
				// Consumo
				if (contratoPontoConsumo.getListaContratoPontoConsumoModalidade() != null
								&& !contratoPontoConsumo.getListaContratoPontoConsumoModalidade().isEmpty()) {
					for (ContratoPontoConsumoModalidade contratoPontoConsumoModalidade : contratoPontoConsumo
									.getListaContratoPontoConsumoModalidade()) {

						if (contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC() != null
										&& !contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC().isEmpty()) {

							for (ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC : 
									ordenaContratoPCModQDCPorDataVigencia(contratoPontoConsumoModalidade)) {

								// lembrar de
								// fazer o msm
								// para o QDC por
								// contrato
								if (contratoPontoConsumoModalidadeQDC.getDataVigencia().compareTo(dataFinal) < 0) {

									QuantidadesContratadasVO quantidadesContratadasVO = new QuantidadesContratadasVO();
									preencherNumeroContratoFiltro(filtro, contrato);

									quantidadesContratadasVO.setDescricaoSegmento(segmento.getDescricao());
									quantidadesContratadasVO.setNumeroContrato(contrato.getNumeroFormatado());

									quantidadesContratadasVO.setDescricaoModalidade(contratoPontoConsumoModalidade.getContratoModalidade()
													.getDescricao());

									if (contratoPontoConsumoModalidade.getListaContratoPontoConsumoPenalidade() != null) {
										String codPenalidadeTakeOrPay = controladorConstanteSistema
														.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);

										Long chavePrimariaPenalidadeTakeOrPay = Long.parseLong(codPenalidadeTakeOrPay);

										for (ContratoPontoConsumoPenalidade contratoPCPena : contratoPontoConsumoModalidade
														.getListaContratoPontoConsumoPenalidade()) {

											if (contratoPCPena.getPenalidade().getChavePrimaria() == chavePrimariaPenalidadeTakeOrPay
															&& contratoPCPena.getPeriodicidadePenalidade().getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_ANUAL) {

												BigDecimal percentualMargemVariacao = contratoPCPena.getPercentualMargemVariacao()
																.multiply(BigDecimal.valueOf(100));
												quantidadesContratadasVO.setTOP(percentualMargemVariacao);

												break;
											}

										}

									}

									quantidadesContratadasVO.setNomeCliente(contrato.getClienteAssinatura().getNome());
									quantidadesContratadasVO.setDescricaoPontoConsumo(imovel.getNome() + " - "
													+ pontoConsumo.getDescricao());
									quantidadesContratadasVO.setDescricaoModalidade(contratoPontoConsumoModalidade.getContratoModalidade()
													.getDescricao());

									quantidadesContratadasVO.setDataVigenciaQDT(contratoPontoConsumoModalidadeQDC.getDataVigencia());
									quantidadesContratadasVO.setQDC(contratoPontoConsumoModalidadeQDC.getMedidaVolume());

									quantidadesContratadasVO.setDataAssinatura(contrato.getDataAssinatura());
									quantidadesContratadasVO.setDataVencimento(contrato.getDataVencimentoObrigacoes());
									quantidadesContratadasVO.setDataRecisao(contrato.getDataRecisao());

									listaQuantidadesContratadasVO.add(quantidadesContratadasVO);
								}

								if (contratoPontoConsumoModalidadeQDC.getDataVigencia().compareTo(dataInicial) <= 0) {
									break;
								}

							}
							// Procura por QDC do
							// Contrato
						} else {
							QuantidadesContratadasVO quantidadesContratadasVO = qdcPorContrato(contrato, dataInicial,
									dataFinal, filtro);
							if (quantidadesContratadasVO != null) {
								listaQuantidadesContratadasVO.add(quantidadesContratadasVO);
							}

						}
					}
					// Procura por QDC do Contrato
				} else {
					QuantidadesContratadasVO quantidadesContratadasVO = qdcPorContrato(contrato, dataInicial, dataFinal,
							filtro);
					if (quantidadesContratadasVO != null) {
						listaQuantidadesContratadasVO.add(quantidadesContratadasVO);
					}

				}
			}
		} else {
			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return listaQuantidadesContratadasVO;
	}

	/**
	 * Preencher numero contrato filtro.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param contrato
	 *            the contrato
	 */
	private void preencherNumeroContratoFiltro(Map<String, Object> filtro, Contrato contrato) {

		String numeroContrato = (String) filtro.get(NUMERO_CONTRATO);
		if (!StringUtils.isEmpty(numeroContrato)) {
			filtro.put(NUMERO_CONTRATO_FORMATADO, contrato.getNumeroFormatado());
		}
	}

	/**
	 * Preencher dados quantidades contratadas vo.
	 * 
	 * @param quantidadesContratadasVO
	 *            the quantidades contratadas vo
	 * @param contrato
	 *            the contrato
	 */
	private void preencherDadosQuantidadesContratadasVO(QuantidadesContratadasVO quantidadesContratadasVO, Contrato contrato) {

		if (contrato.getListaContratoPontoConsumo() != null && contrato.getListaContratoPontoConsumo().size() == 1) {

			ContratoPontoConsumo contratoPC = contrato.getListaContratoPontoConsumo().iterator().next();
			String nomeImovel = contratoPC.getPontoConsumo().getImovel().getNome();
			String descricaoPC = contratoPC.getPontoConsumo().getDescricao();

			quantidadesContratadasVO.setDescricaoPontoConsumo(nomeImovel + " - " + descricaoPC);
			quantidadesContratadasVO.setDescricaoSegmento(contratoPC.getPontoConsumo().getSegmento().getDescricao());

			if (contratoPC.getListaContratoPontoConsumoModalidade() != null
							&& contratoPC.getListaContratoPontoConsumoModalidade().size() == 1) {

				ContratoPontoConsumoModalidade contratoPCMod = contratoPC.getListaContratoPontoConsumoModalidade().iterator().next();
				quantidadesContratadasVO.setDescricaoModalidade(contratoPCMod.getContratoModalidade().getDescricao());

				if (contratoPCMod.getListaContratoPontoConsumoPenalidade() != null) {

					ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
									.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

					String codPenalidadeTakeOrPay = controladorConstanteSistema
									.obterValorConstanteSistemaPorCodigo(Constantes.C_PENALIDADE_TAKE_OR_PAY);

					Long chavePrimariaPenalidadeTakeOrPay = Long.parseLong(codPenalidadeTakeOrPay);

					for (ContratoPontoConsumoPenalidade contratoPCPena : contratoPCMod.getListaContratoPontoConsumoPenalidade()) {

						if (contratoPCPena.getPenalidade().getChavePrimaria() == chavePrimariaPenalidadeTakeOrPay
										&& contratoPCPena.getPeriodicidadePenalidade().getChavePrimaria() == EntidadeConteudo.CHAVE_PERIODICIDADE_PENALIDADE_ANUAL) {

							BigDecimal percentualMargemVariacao = contratoPCPena.getPercentualMargemVariacao().multiply(
											BigDecimal.valueOf(100));
							quantidadesContratadasVO.setTOP(percentualMargemVariacao);

							break;
						}

					}

				}

			}
		}
	}

	/**
	 * Qdc por contrato.
	 * 
	 * @param contrato {@link Contrato}
	 * @param dataInicial {@link Date}
	 * @param dataFinal {@link Date}
	 * @param filtro {@link Map}
	 * @return quantidadesContratadasVO {@link QuantidadesContratadasVO}
	 */
	private QuantidadesContratadasVO qdcPorContrato(Contrato contrato, Date dataInicial, Date dataFinal,
			Map<String, Object> filtro) {

		QuantidadesContratadasVO quantidadesContratadasVO = null;

		if (contrato.getListaContratoQDC() != null && !contrato.getListaContratoQDC().isEmpty()) {
			for (ContratoQDC contratoQDC : contrato.getListaContratoQDC()) {

				if (contratoQDC.getData().compareTo(dataFinal) < 0) {
					quantidadesContratadasVO = new QuantidadesContratadasVO();

					quantidadesContratadasVO.setNumeroContrato(contrato.getNumeroFormatado());
					preencherNumeroContratoFiltro(filtro, contrato);

					quantidadesContratadasVO.setNomeCliente(contrato.getClienteAssinatura().getNome());

					quantidadesContratadasVO.setDataVigenciaQDT(contratoQDC.getData());
					quantidadesContratadasVO.setQDC(contratoQDC.getMedidaVolume());

					quantidadesContratadasVO.setDataAssinatura(contrato.getDataAssinatura());
					quantidadesContratadasVO.setDataVencimento(contrato.getDataVencimentoObrigacoes());
					quantidadesContratadasVO.setDataRecisao(contrato.getDataRecisao());

					preencherDadosQuantidadesContratadasVO(quantidadesContratadasVO, contrato);
				}

				if (contratoQDC.getData().compareTo(dataInicial) <= 0) {
					break;
				}
			}
		}
		return quantidadesContratadasVO;
	}


	/**
	 * Obter dados relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<Object[]> obterDadosRelatorio(Map<String, Object> filtro) throws GGASException {

		Long[] arraySituacao = {SituacaoContrato.ALTERADO, SituacaoContrato.EM_CRIACAO};

		Date dataInicial = null;
		Date dataFinal = null;
		Date dataVencimento = null;
		Date dataAssinatura = null;

		dataInicial = (Date) filtro.get(DATA_INICIAL);
		dataFinal = (Date) filtro.get(DATA_FINAL);
		dataAssinatura = (Date) filtro.get(DATA_ASSINATURA);
		dataVencimento = (Date) filtro.get(DATA_VENCIMENTO);

		Long idCliente = (Long) filtro.get(ID_CLIENTE);
		String numeroContratoAux = (String) filtro.get(NUMERO_CONTRATO);
		Integer numeroContrato = null;
		if (!StringUtils.isEmpty(numeroContratoAux)) {
			numeroContrato = Util.converterCampoStringParaValorInteger("Número Contrato", numeroContratoAux);
		}

		Collection<Object[]> arrayObject = null;

		Long idSegmento = (Long) filtro.get(ID_SEGMENTO);
		StringBuilder hql = new StringBuilder();
		Query query = null;

		hql.append(" from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPontoConsumo ");
		hql.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join pontoConsumo.imovel imovel ");
		hql.append(" inner join pontoConsumo.segmento segmento ");
		hql.append(" inner join contratoPontoConsumo.contrato contrato ");

		hql.append(" where ( exists ( ");
		hql.append(" from ");
		hql.append(getClasseEntidadeContratoPontoConsumoModalidade().getSimpleName());
		hql.append(" contratoPontoConsumoModalidade, ");
		hql.append(getClasseEntidadeContratoPontoConsumoModalidadeQDC().getSimpleName());
		hql.append(" contratoPontoConsumoModalidadeQDC ");
		hql.append(" where  contratoPontoConsumoModalidade.contratoPontoConsumo.chavePrimaria  = contratoPontoConsumo.chavePrimaria ");
		hql.append(" and contratoPontoConsumoModalidade.chavePrimaria =  "
						+ "contratoPontoConsumoModalidadeQDC.contratoPontoConsumoModalidade.chavePrimaria)");

		hql.append(" or exists ( ");
		hql.append(" from ");
		hql.append(getClasseEntidadeContratoQDC().getSimpleName());
		hql.append(" coqd ");
		hql.append(" where  coqd.contrato.chavePrimaria  = contrato.chavePrimaria)) ");

		hql.append(" and contrato.situacao.chavePrimaria not in (:arraySituacao) ");

		if (idSegmento != null) {
			hql.append(" and segmento.chavePrimaria = :idSegmento ");
		}

		if (idCliente != null) {
			hql.append(" and contrato.clienteAssinatura = :idCliente ");
		}

		if (numeroContrato != null) {
			hql.append(" and contrato.numero = :numeroContrato ");
		}

		if (dataInicial != null && dataFinal != null) {
			hql.append(" and contrato.dataAssinatura <=  :dataFinal ");
			hql.append(" and (contrato.dataVencimentoObrigacoes >=  :dataInicial or contrato.dataVencimentoObrigacoes is null)");
			hql.append(" and (contrato.dataAditivo is null or contrato.dataAditivo <=  :dataFinal)");
		}
		if (dataVencimento != null) {
			hql.append(" and contrato.dataVencimentoObrigacoes = :dataVencimento ");
		}

		if (dataAssinatura != null) {
			hql.append(" and contrato.dataAssinatura = :dataAssinatura ");
		}
		hql.append(" and contrato.habilitado = true ");

		hql.append(" order by imovel.nome, contrato.chavePrimaria ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (idSegmento != null) {
			query.setLong(ID_SEGMENTO, idSegmento);
		}

		query.setParameterList("arraySituacao", arraySituacao);

		if (idCliente != null) {
			query.setLong(ID_CLIENTE, idCliente);
		}
		if (numeroContrato != null) {
			query.setInteger(NUMERO_CONTRATO, numeroContrato);
		}
		if (dataInicial != null && dataFinal != null) {
			query.setDate(DATA_INICIAL, dataInicial);
			query.setDate(DATA_FINAL, dataFinal);
		}
		if (dataVencimento != null) {
			query.setDate(DATA_VENCIMENTO, dataVencimento);
		}

		if (dataAssinatura != null) {
			query.setDate(DATA_ASSINATURA, dataAssinatura);
		}

		arrayObject = query.list();

		return arrayObject;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

}
