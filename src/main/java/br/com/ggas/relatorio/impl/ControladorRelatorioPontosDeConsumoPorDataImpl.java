package br.com.ggas.relatorio.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.relatorio.ControladorRelatorioPontosDeConsumoPorData;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.web.relatorio.pontoConsumoPorData.PontoConsumoPorDataTotalizadorVO;
import br.com.ggas.web.relatorio.pontoConsumoPorData.PontoConsumoPorDataVO;
import br.com.ggas.web.relatorio.pontoConsumoPorData.RepositorioPontoConsumoPorData;

/**
 * @author vpessoa
 */
@Service("controladorRelatorioPontosDeConsumoPorData")
@Transactional
public class ControladorRelatorioPontosDeConsumoPorDataImpl implements ControladorRelatorioPontosDeConsumoPorData{

	@Autowired
	private RepositorioPontoConsumoPorData repositorioPontoConsumoPorData;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioPontosDeConsumoPorDataImpl(java.util.Map)
	 */
	@Override
	@SuppressWarnings({"rawtypes", "unchecked"})
	public byte[] gerarRelatorio(Date dataInicial, Date dataFinal, String exibirFiltros, String tipoRelatorio, Segmento segmento, Boolean isAtivoPrimeiraVez) throws GGASException {

		FormatoImpressao formatoImpressao = FormatoImpressao.valueOf(tipoRelatorio);
		Collection<PontoConsumoPorDataVO> collRelatorio = this.obterPontosConsumoPorData(dataInicial, dataFinal, segmento, isAtivoPrimeiraVez);
		
		
		if (collRelatorio.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_NENHUM_REGISTRO_ENCONTRADO, true);
		}


		SimpleDateFormat sf = new SimpleDateFormat("dd/MM/yyyy");
		String dataParamInicio = null;
		String artigo = null;
		if(dataInicial != null){
			dataParamInicio = sf.format(dataInicial);
			artigo = "à";
			
		}
		String dataParamFim = null;
		if(dataFinal != null){
			dataParamFim = sf.format(dataFinal);
		}
		
		String dataImpressao = sf.format(Calendar.getInstance().getTime());
		Map parametros = new HashMap();
		parametros.put("dataInicial", dataParamInicio);
		parametros.put("dataFinal", dataParamFim);
		parametros.put("dataImpressao", dataImpressao);
		parametros.put("formatoImpressao", formatoImpressao);
		parametros.put("artigo", artigo);
		parametros.put("listaPontosConsumo", collRelatorio);
		
		ControladorEmpresa controladorEmpresa =
				(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();

		parametros.put("imagem",
				Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
		
		this.validarDadosGerarRelatorioPontoConsumoPorData(parametros);
		
		Collection<PontoConsumoPorDataTotalizadorVO> listaTotalizador = this.calcularQuantidadeSegmento(collRelatorio);
		
		parametros.put("listaTotalizador", listaTotalizador);
		
		Collection<PontoConsumoPorDataVO> dados = new ArrayList<PontoConsumoPorDataVO>();
		dados.add(collRelatorio.iterator().next());
		

		return RelatorioUtil.gerarRelatorio(dados, parametros, RELATORIO_PONTO_CONSUMO_POR_DATA_JASPER, formatoImpressao);
	}

	private Collection<PontoConsumoPorDataTotalizadorVO> calcularQuantidadeSegmento(
			Collection<PontoConsumoPorDataVO> collRelatorio) {
		Collection<PontoConsumoPorDataTotalizadorVO> totalizador = new ArrayList<PontoConsumoPorDataTotalizadorVO>();
		
		Map<String, Long> mapaTotalizadorSegmentos = collRelatorio.stream()
				.collect(Collectors.groupingBy(PontoConsumoPorDataVO::getSegmento, Collectors.counting()));		
		
		for(Entry<String, Long> entry : mapaTotalizadorSegmentos.entrySet()) {
			PontoConsumoPorDataTotalizadorVO pontoConsumoTotalizador = new PontoConsumoPorDataTotalizadorVO();
			pontoConsumoTotalizador.setDescricaoTotalizador(entry.getKey());
			pontoConsumoTotalizador.setQuantidadeTotalizador(entry.getValue());
		
			totalizador.add(pontoConsumoTotalizador);
		}
		
		totalizador = totalizador.stream().sorted(Comparator.comparing(p -> p.getDescricaoTotalizador()))
				.collect(Collectors.toList());		
		
		PontoConsumoPorDataTotalizadorVO pontoConsumoTotalizador = new PontoConsumoPorDataTotalizadorVO();
		pontoConsumoTotalizador.setDescricaoTotalizador("TOTAL");
		pontoConsumoTotalizador.setQuantidadeTotalizador(Long.valueOf(collRelatorio.size()));

		
		totalizador.add(pontoConsumoTotalizador);
		
		return totalizador;
	}

	/**
	 * Validar dados gerar relatorio pontos de consumo por data.
	 * 
	 * @param parametros
	 *            the parametros
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosGerarRelatorioPontoConsumoPorData(Map<String, Object> parametros) throws NegocioException {

		if (parametros.get("formatoImpressao") == null) {

			throw new NegocioException(ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO, true);

		}

	}


	/**
	 * Obter pontos consumo por data.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the map
	 */
	private Collection<PontoConsumoPorDataVO> obterPontosConsumoPorData(Date dataIncio, Date dataFinal, Segmento segmento, Boolean isAtivoPrimeiraVez) {

		return repositorioPontoConsumoPorData.listarPontosConsumoAtivoPorData(dataIncio, dataFinal, segmento, isAtivoPrimeiraVez);
	}

}
