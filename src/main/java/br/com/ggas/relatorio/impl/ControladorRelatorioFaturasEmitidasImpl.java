/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;

import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioFaturasEmitidas;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.faturasemitidas.FaturasEmitidasVO;

@SuppressWarnings("unchecked")
public class ControladorRelatorioFaturasEmitidasImpl extends ControladorNegocioImpl implements ControladorRelatorioFaturasEmitidas {

	private static final String FATURA_AGRUPADA = "faturaAgrupada";
	
	/**
	 * Popula o relatório com uma coleção de
	 * entidades baseado nos filtros.
	 * 
	 * @param dados
	 *            Registos para a população do
	 *            relatório
	 * @param filtro
	 *            Parâmetros para a geração do
	 *            relatório
	 * @return bytesRelatorio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public byte[] gerarRelatorio(Collection<FaturasEmitidasVO> dados, Map<String, Object> filtro) throws NegocioException {

		validarDados(dados);
		Map<String, Object> parametros = new HashMap<String, Object>();

		// Imagem da empresa
		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		Empresa principal = controladorEmpresa.obterEmpresaPrincipal();
		if(principal.getLogoEmpresa() != null) {
			parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(principal.getChavePrimaria()));
		}

		// Exibição de filtros
		Boolean exibirFiltros = (Boolean) filtro.get("exibirFiltros");
		parametros.put("exibirFiltros", exibirFiltros);

		parametros.put("periodoDataInicial", filtro.get("dataInicial"));
		parametros.put("periodoDataFinal", filtro.get("dataFinal"));

		String nomeFormato = (String) filtro.get("formatoImpressao");
		FormatoImpressao formato = FormatoImpressao.valueOf(nomeFormato);

		return RelatorioUtil.gerarRelatorio(dados, parametros, "relatorioFaturasEmitidas.jasper", formato);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.impl.
	 * ControladorRelatorioFaturasEmitidas
	 * #consultar(java.util.Map)
	 */
	@Override
	public byte[] consultar(Map<String, Object> filtro) throws GGASException {

		byte[] relatorio = null;

		validarFiltros(filtro);

		Date dataInicial = Util.converterCampoStringParaData("", (String) filtro.get("dataInicial"), Constantes.FORMATO_DATA_BR);
		Date dataFinal = Util.converterCampoStringParaData("", (String) filtro.get("dataFinal"), Constantes.FORMATO_DATA_BR);

		Criteria criteria = this.createCriteria(getClasseEntidade());
		criteria.setProjection(Projections.projectionList().add(Projections.groupProperty("dataEmissao"), "data")
						.add(Projections.count("id"), "qtdFaturas"));

		criteria.add(Restrictions.between("dataEmissao", dataInicial, dataFinal));
		criteria.addOrder(Order.asc("dataEmissao"));
		criteria.add(Restrictions.isNull(FATURA_AGRUPADA));

		criteria.setResultTransformer(new AliasToBeanResultTransformer(FaturasEmitidasVO.class));
		Collection<FaturasEmitidasVO> dados = criteria.list();

		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String situacaoCancelada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_CANCELADO);
		String situacaoCanceladaRefat = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_CANCELADO_POR_REFATURAMENTO);

		Criteria criteriaCanceladas = this.createCriteria(getClasseEntidade());
		criteriaCanceladas.setProjection(Projections.projectionList().add(Projections.groupProperty("dataEmissao"), "data")
						.add(Projections.count("id"), "qtdFaturasCanceladas"));

		criteriaCanceladas.add(Restrictions.between("dataEmissao", dataInicial, dataFinal));
		criteriaCanceladas.add(Restrictions.in("creditoDebitoSituacao.chavePrimaria",
						new Long[] {Long.valueOf(situacaoCancelada), Long.valueOf(situacaoCanceladaRefat)}));

		criteriaCanceladas.addOrder(Order.asc("dataEmissao"));

		criteriaCanceladas.setResultTransformer(new AliasToBeanResultTransformer(FaturasEmitidasVO.class));
		List<FaturasEmitidasVO> contagemCanceladas = new ArrayList<FaturasEmitidasVO>(criteriaCanceladas.list());

		FaturasEmitidasVO tempVO = null;
		int indice = -1;
		for (FaturasEmitidasVO dado : dados) {
			indice = contagemCanceladas.indexOf(dado);
			dado.setQtdFaturasCanceladas(0L);
			if(indice > -1) {
				tempVO = contagemCanceladas.get(indice);
				dado.setQtdFaturasCanceladas(tempVO.getQtdFaturasCanceladas());
				contagemCanceladas.remove(indice);
			}
		}

		Integer qtdTotalFaturas = 0;
		Integer qtdTotalFaturasCanceladas = 0;

		for (FaturasEmitidasVO faturasEmitidasVO : dados) {
			qtdTotalFaturas += faturasEmitidasVO.getQtdFaturas().intValue();
			faturasEmitidasVO.setQtdTotalFaturas(qtdTotalFaturas.longValue());

			qtdTotalFaturasCanceladas += faturasEmitidasVO.getQtdFaturasCanceladas().intValue();
			faturasEmitidasVO.setQtdTotalFaturasCanceladas(qtdTotalFaturasCanceladas.longValue());
		}

		relatorio = gerarRelatorio(dados, filtro);

		return relatorio;
	}

	/**
	 * Valida os filtros usado para consultar os
	 * dados do relatório.
	 * 
	 * @param filtro
	 *            Filtro para a consulta.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void validarFiltros(Map<String, Object> filtro) throws GGASException {

		String delimitador = "";
		StringBuilder campos = new StringBuilder();
		Map<String, Object> erros = new HashMap<String, Object>();

		Date dataInicial = null;
		String campoDataInicial = (String) filtro.get("dataInicial");
		if(campoDataInicial == null || "".equals(campoDataInicial)) {
			campos.append(delimitador).append("Data Inicial");
			delimitador = ", ";
		} else {
			dataInicial = Util.converterCampoStringParaData("", campoDataInicial, Constantes.FORMATO_DATA_BR);
		}

		Date dataFinal = null;
		String campoDataFinal = (String) filtro.get("dataFinal");
		if(campoDataFinal == null || "".equals(campoDataFinal)) {
			campos.append(delimitador).append("Data Final");
			delimitador = ", ";
		} else {
			dataFinal = Util.converterCampoStringParaData("", campoDataFinal, Constantes.FORMATO_DATA_BR);
		}

		if(campos.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, campos.toString());
			throw new NegocioException(erros);
		}

		if(campoDataInicial != null && dataInicial.after(dataFinal)) {
			throw new NegocioException(Constantes.ERRO_DATA_INICIAL_MAIOR_DATA_FINAL, true);
		}
	}

	/**
	 * Valida uma coleção de dados será exibida em
	 * relatório.
	 * 
	 * @param dados
	 *            Registros a validar
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDados(Collection<FaturasEmitidasVO> dados) throws NegocioException {

		if(dados == null || dados.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Fatura.BEAN_ID_FATURA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

}
