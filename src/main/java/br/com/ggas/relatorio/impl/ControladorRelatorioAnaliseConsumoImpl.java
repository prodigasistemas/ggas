/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.faturamento.ControladorCalculoFornecimentoGas;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.exception.EscalaNaoInformadaException;
import br.com.ggas.faturamento.exception.PrecisaoNaoInformadaException;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.impl.DadosFaixaVO;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.impl.DadosVigenciaVO;
import br.com.ggas.faturamento.rubrica.ControladorRubrica;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.Tarifa;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.ControladorRelatorioAnaliseConsumo;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.relatorio.analiseconsumo.DadosRelatorioAnaliseConsumoVO;
import br.com.ggas.web.relatorio.analiseconsumo.RelatorioAnaliseConsumoDadosResumoVO;
import br.com.ggas.web.relatorio.analiseconsumo.RelatorioAnaliseConsumoVO;

public class ControladorRelatorioAnaliseConsumoImpl extends ControladorNegocioImpl implements ControladorRelatorioAnaliseConsumo {

	private static final String ERRO_NEGOCIO_RELATORIO_SEM_DADOS = "ERRO_NEGOCIO_RELATORIO_SEM_DADOS";

	private static final String DATA_INICIAL = "dataEmissaoInicial";

	private static final String DATA_FINAL = "dataEmissaoFinal";

	private static final String ID_CLIENTE = "idCliente";

	private static final String EXIBIR_FILTROS = "exibirFiltros";

	private static final String CLIENTE = "cliente";

	private static final String RELATORIO_ANALISE_CONSUMO = "relatorioAnaliseConsumoCliente.jasper";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeFaturaItem() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaItem.BEAN_ID_FATURA_ITEM);
	}

	public Class<?> getClasseEntidadeDocumentoFiscal() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoFiscal.BEAN_ID_DOCUMENTO_FISCAL);
	}

	public Class<?> getClasseEntidadeHistoricoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	public Class<?> getClasseEntidadeHistoricoMedicao() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoMedicao.BEAN_ID_MEDICAO_HISTORICO);
	}

	public Class<?> getClasseEntidadeRubrica() {

		return ServiceLocator.getInstancia().getClassPorID(Rubrica.BEAN_ID_RUBRICA);
	}

	public Class<?> getClasseEntidadeSegmento() {

		return ServiceLocator.getInstancia().getClassPorID(Segmento.BEAN_ID_SEGMENTO);
	}

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeImovel() {

		return ServiceLocator.getInstancia().getClassPorID(Imovel.BEAN_ID_IMOVEL);
	}


	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}


	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioAnaliseConsumo#gerarRelatorio(java.util.Map, br.com.ggas.util.FormatoImpressao)
	 */
	@Override
	public byte[] gerarRelatorio(Map<String, Object> filtro, FormatoImpressao formatoImpressao) throws GGASException {

		Map<String, Object> parametros = new HashMap<String, Object>();
		byte[] relatorioAnaliseConsumo;

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		if(controladorEmpresa.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			parametros.put("imagem",
							Constantes.URL_LOGOMARCA_EMPRESA
											+ String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		}
		this.montarParametrosFiltro(parametros, filtro);

		Collection<RelatorioAnaliseConsumoVO> listaAnaliseConsumo = this.montarListaAnaliseConsumo(filtro);
		if(!listaAnaliseConsumo.isEmpty()) {
			relatorioAnaliseConsumo = RelatorioUtil.gerarRelatorio(listaAnaliseConsumo, parametros, RELATORIO_ANALISE_CONSUMO,
							formatoImpressao);
		} else {
			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return relatorioAnaliseConsumo;
	}

	/**
	 * Preenche dados resumo.
	 *
	 * @param cohi
	 *            the cohi
	 * @param mehi
	 *            the mehi
	 * @param listaTipoConsumoReal
	 *            the lista tipo consumo real
	 * @param listaDadosRelatorioAnaliseConsumoVO
	 *            the lista dados relatorio analise consumo vo
	 */
	private void preencheDadosResumo(HistoricoConsumo cohi, HistoricoMedicao mehi, List<Long> listaTipoConsumoReal,
					Collection<DadosRelatorioAnaliseConsumoVO> listaDadosRelatorioAnaliseConsumoVO) {

		DadosRelatorioAnaliseConsumoVO dadosRelatorioAnaliseConsumoVO = new DadosRelatorioAnaliseConsumoVO();

		dadosRelatorioAnaliseConsumoVO.setExibeData(Boolean.TRUE);
		dadosRelatorioAnaliseConsumoVO.setData(mehi.getDataLeituraFaturada());

		// Se o tipo de consumo for REAL, repete o
		// valor da coluna volume corrigido
		if(listaTipoConsumoReal.contains(cohi.getTipoConsumo().getChavePrimaria())) {
			//Possivel Implementação Futura.
		} else {
			// Se o tipo de consumo não for
			// Real, dividir o valor pela
			// quantidade de dias
			Integer qtdDiasConsumo = cohi.getDiasConsumo();
			if(BigDecimal.valueOf(qtdDiasConsumo).compareTo(BigDecimal.ONE) > 1) {
				BigDecimal volumeMedido = cohi.getConsumoMedido();
				BigDecimal volumeFaturado = volumeMedido.divide(BigDecimal.valueOf(qtdDiasConsumo));
				dadosRelatorioAnaliseConsumoVO.setVolumeFaturado(volumeFaturado);
			} else {
				//Possivel Implementação Futura.
			}
		}

		dadosRelatorioAnaliseConsumoVO.setVolumeMedido(cohi.getConsumoMedido());
		dadosRelatorioAnaliseConsumoVO.setPcs(cohi.getMedidaPCS());
		dadosRelatorioAnaliseConsumoVO.setFatorCorrecao(cohi.getFatorCorrecao());
		dadosRelatorioAnaliseConsumoVO.setVolumeCorrigido(cohi.getConsumoApurado());
		dadosRelatorioAnaliseConsumoVO.setDescricaoRubrica(null);
		dadosRelatorioAnaliseConsumoVO.setNumeroDocFiscal(null);
		listaDadosRelatorioAnaliseConsumoVO.add(dadosRelatorioAnaliseConsumoVO);
	}

	/**
	 * Preencher relatorio analise consumo vo.
	 *
	 * @param relatorioAnaliseConsumoVO1
	 *            the relatorio analise consumo v o1
	 * @param cohi
	 *            the cohi
	 * @param listaDadosRelatorioAnaliseConsumoVO
	 *            the lista dados relatorio analise consumo vo
	 * @param tam
	 *            the tam
	 * @param listaRelatorioAnaliseConsumoVO
	 *            the lista relatorio analise consumo vo
	 * @param filtro
	 *            the filtro
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void preencherRelatorioAnaliseConsumoVO(RelatorioAnaliseConsumoVO relatorioAnaliseConsumoVO1,
			HistoricoConsumo cohi, Collection<DadosRelatorioAnaliseConsumoVO> listaDadosRelatorioAnaliseConsumoVO,
			Collection<RelatorioAnaliseConsumoVO> listaRelatorioAnaliseConsumoVO, Map<String, Object> filtro)
			throws GGASException {

		relatorioAnaliseConsumoVO1.setImovelPontoConsumo(cohi.getPontoConsumo().getImovel().getNome() + " - "
						+ cohi.getPontoConsumo().getDescricao());

		relatorioAnaliseConsumoVO1.setQtdDias(cohi.getDiasConsumo());

		Collection<Object[]> listaDadosResumo = selecionarRubricaConsumoGas(montarListaDadosResumo(cohi.getChavePrimaria(), filtro));
		Collection<RelatorioAnaliseConsumoDadosResumoVO> listaRelatorioAnaliseConsumoDadosResumoVO;

		if(!listaDadosResumo.isEmpty()) {
			this.preencheRelatorioAnaliseConsumoVO(listaDadosResumo, cohi, relatorioAnaliseConsumoVO1);

			for (Object[] obj : listaDadosResumo) {

				FaturaItem faturaItem = (FaturaItem) obj[1];
				for (DadosRelatorioAnaliseConsumoVO dadosRelatorioAnaliseConsumoVO : listaDadosRelatorioAnaliseConsumoVO) {
					dadosRelatorioAnaliseConsumoVO.setTotalVolumeFaturado(faturaItem.getMedidaConsumo());
				}
			}

			Date dataInicial = null;
			Date dataFinal = null;

			if(cohi.getHistoricoAnterior() != null && cohi.getHistoricoAnterior().getDataLeituraInformada() != null) {
				dataInicial = cohi.getHistoricoAnterior().getDataLeituraInformada();
			}

			dataFinal = cohi.getHistoricoAtual().getDataLeituraInformada();

			if(dataInicial != null && dataInicial.before(dataFinal)) {
				dataInicial = Util.incrementarDia(dataInicial, 1);
			} else if(dataInicial == null) {
				dataInicial = DataUtil.decrementarDia(dataFinal, cohi.getDiasConsumo());
			}

			if(dataInicial == null) {
				relatorioAnaliseConsumoVO1.setDataInicial("");
			} else {
				relatorioAnaliseConsumoVO1.setDataInicial(Util.converterDataParaStringSemHora(dataInicial, Constantes.FORMATO_DATA_BR));
			}

			relatorioAnaliseConsumoVO1.setListaDados(listaDadosRelatorioAnaliseConsumoVO);
			listaRelatorioAnaliseConsumoDadosResumoVO = montarResumoRelatorio(listaDadosResumo, dataInicial, cohi.getHistoricoAtual()
							.getDataLeituraInformada());

			if(!listaRelatorioAnaliseConsumoDadosResumoVO.isEmpty()) {
				relatorioAnaliseConsumoVO1.setListaDadosResumo(listaRelatorioAnaliseConsumoDadosResumoVO);
				listaRelatorioAnaliseConsumoVO.add(relatorioAnaliseConsumoVO1);
			}
		}
	}

	/**
	 * Montar lista analise consumo.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<RelatorioAnaliseConsumoVO> montarListaAnaliseConsumo(Map<String, Object> filtro) throws GGASException {

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Collection<RelatorioAnaliseConsumoVO> listaRelatorioAnaliseConsumoVO = new ArrayList<RelatorioAnaliseConsumoVO>();

		Collection<Object[]> listaObjetosRelatorio = this.obterDadosRelatorio(filtro);

		List<Long> listaTipoConsumoReal = new ArrayList<Long>();
		listaTipoConsumoReal.add(Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL_COMPOSTO)));
		listaTipoConsumoReal.add(Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL_ATUAL)));
		listaTipoConsumoReal.add(Long.valueOf(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL)));

		Collection<DadosRelatorioAnaliseConsumoVO> listaDadosRelatorioAnaliseConsumoVO = new ArrayList<DadosRelatorioAnaliseConsumoVO>();
		Collection<Long> listaIDPontoConsumo = new ArrayList<Long>();
		RelatorioAnaliseConsumoVO relatorioAnaliseConsumoVO1 = null;

		if(!listaObjetosRelatorio.isEmpty()) {
			for (Object[] obj : listaObjetosRelatorio) {

				HistoricoConsumo cohi = (HistoricoConsumo) obj[0];
				if(!listaIDPontoConsumo.contains(cohi.getPontoConsumo().getChavePrimaria())) {
					listaIDPontoConsumo.add(cohi.getPontoConsumo().getChavePrimaria());
				}
			}

		}

		if(!listaObjetosRelatorio.isEmpty()) {
			Collection<Object[]> listaObjetosRelatorioAux = new ArrayList<Object[]>();
			for (Object[] objects : listaObjetosRelatorio) {

				HistoricoConsumo cohi = (HistoricoConsumo) objects[0];

				if(cohi.getMedidaPCS() == null) {
					listaObjetosRelatorioAux.add(objects);
				}
			}

			for (Long idCohi : listaIDPontoConsumo) {
				Integer tam = 0;

				for (Object[] obj : listaObjetosRelatorio) {
					HistoricoConsumo cohi = (HistoricoConsumo) obj[0];
					HistoricoMedicao mehi = (HistoricoMedicao) obj[1];

					if(cohi.getPontoConsumo().getChavePrimaria() == idCohi) {
						relatorioAnaliseConsumoVO1 = new RelatorioAnaliseConsumoVO();

						if(cohi.getMedidaPCS() != null) {
							/* Entra aqui se for um historico consumo que não está ligado a uma fatura*/
							if(!cohi.isIndicadorConsumoCiclo()) {
								/* Entra aqui se a periodicida de docilco for diário */
								preencheDadosResumo(cohi, mehi, listaTipoConsumoReal, listaDadosRelatorioAnaliseConsumoVO);
							} else {
								/* Entra qui se a periodicidade do cilco forperiódico*/
								preencheDadosResumo(cohi, mehi, listaTipoConsumoReal, listaDadosRelatorioAnaliseConsumoVO);
								preencherRelatorioAnaliseConsumoVO(relatorioAnaliseConsumoVO1, // dataInicial,
												cohi, listaDadosRelatorioAnaliseConsumoVO, listaRelatorioAnaliseConsumoVO, filtro);
								listaDadosRelatorioAnaliseConsumoVO = new ArrayList<DadosRelatorioAnaliseConsumoVO>();
							}

						} else {
							// entra aqui quando
							// a periodicidade
							// for quinzenal
							boolean passou = false;
							Object[] objectAux = null;
							for (Object[] object : listaObjetosRelatorioAux) {
								HistoricoConsumo cohiAux = (HistoricoConsumo) object[0];
								if(cohiAux.getPontoConsumo().getChavePrimaria() == idCohi && Boolean.FALSE.equals(passou)) {
									preencherRelatorioAnaliseConsumoVO(
													relatorioAnaliseConsumoVO1,
													cohiAux, listaDadosRelatorioAnaliseConsumoVO, listaRelatorioAnaliseConsumoVO,
													filtro);
									listaDadosRelatorioAnaliseConsumoVO = new ArrayList<DadosRelatorioAnaliseConsumoVO>();
									objectAux = object;
									tam = 0;
									passou = true;
								}
							}
							listaObjetosRelatorioAux.remove(objectAux);
						}
					}
				}
			}
		} else {
			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return listaRelatorioAnaliseConsumoVO;
	}

	/**
	 * Montar parametros filtro.
	 *
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void montarParametrosFiltro(Map<String, Object> parametros, Map<String, Object> filtro) throws GGASException {

		Cliente cliente = null;
		Long idCliente = (Long) filtro.get(ID_CLIENTE);
		String dataInicial = (String) filtro.get(DATA_INICIAL);
		String dataFinal = (String) filtro.get(DATA_FINAL);

		Boolean exibirFiltro = (Boolean) filtro.get(EXIBIR_FILTROS);
		parametros.put(EXIBIR_FILTROS, exibirFiltro);
		if(exibirFiltro) {

			if(idCliente != null && idCliente > 0) {
				ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getBeanPorID(
								ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

				cliente = controladorCliente.obterCliente(idCliente, "enderecos");
			}

			if(cliente != null) {
				parametros.put(CLIENTE, cliente.getChavePrimaria());
			}
			if(dataInicial != null) {
				parametros.put("dataInicial", Util.converterAnoMesEmMesAno(dataInicial));
			}
			if(dataFinal != null) {
				parametros.put("dataFinal", Util.converterAnoMesEmMesAno(dataFinal));
			}
		}

	}

	/**
	 * Obter dados relatorio.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<Object[]> obterDadosRelatorio(Map<String, Object> filtro) throws GGASException {

		String dataEmissaoInicial = null;
		String dataEmissaoFinal = null;
		Long idCliente = null;

		dataEmissaoInicial = (String) filtro.get(DATA_INICIAL);
		dataEmissaoFinal = (String) filtro.get(DATA_FINAL);
		idCliente =  (Long) filtro.get(ID_CLIENTE);

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" historicoConsumo ");

		hql.append(" inner join historicoConsumo.historicoAtual mehi ");
		hql.append(" ");

		hql.append(" where historicoConsumo.habilitado is true ");
		hql.append(" and historicoConsumo.indicadorFaturamento is true ");

		if(idCliente != null) {
			hql.append(" and historicoConsumo.pontoConsumo.chavePrimaria in (");
			hql.append(" select contratoPontoConsumo.pontoConsumo from ");
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(" contratoPontoConsumo inner join contratoPontoConsumo.contrato contrato");
			hql.append(" where contrato.clienteAssinatura = :idCliente ");
			hql.append(" and contrato.habilitado = true) ");
		}
		if(dataEmissaoInicial != null && dataEmissaoFinal != null) {
			hql.append(" and mehi.anoMesLeitura between :dataEmissaoInicial and :dataEmissaoFinal  ");
		}

		hql.append(" order by  mehi.dataLeituraFaturada, historicoConsumo.pontoConsumo.chavePrimaria, historicoConsumo.chavePrimaria ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(dataEmissaoInicial != null && dataEmissaoFinal != null) {
			query.setString(DATA_INICIAL, dataEmissaoInicial);
			query.setString(DATA_FINAL, dataEmissaoFinal);
		}

		if(idCliente != null) {
			query.setLong(ID_CLIENTE, idCliente);
		}

		return query.list();
	}

	/**
	 * Montar resumo relatorio.
	 *
	 * @param listaDadosResumo
	 *            the lista dados resumo
	 * @param dataInicial
	 *            the data inicial
	 * @param dataFinal
	 *            the data final
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<RelatorioAnaliseConsumoDadosResumoVO> montarResumoRelatorio(Collection<Object[]> listaDadosResumo, Date dataInicial,
					Date dataFinal) throws GGASException {

		ControladorCalculoFornecimentoGas controladorCalculo = (ControladorCalculoFornecimentoGas) ServiceLocator.getInstancia()
						.getBeanPorID(ControladorCalculoFornecimentoGas.BEAN_ID_CONTROLADOR_CALCULO_FORNECIMENTO_GAS);

		ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		Collection<RelatorioAnaliseConsumoDadosResumoVO> listaDadosResumoVO = new ArrayList<RelatorioAnaliseConsumoDadosResumoVO>();

		for (Object[] obj : listaDadosResumo) {

			Fatura fatura = (Fatura) obj[0];
			FaturaItem faturaItem = (FaturaItem) obj[1];
			Rubrica rubrica = (Rubrica) obj[2];

			EntidadeConteudo itemFatura = rubrica.getItemFatura();
			Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();
			listaPontoConsumo.add(fatura.getPontoConsumo());
			BigDecimal consumoApurado = faturaItem.getQuantidade();

			Contrato contrato = fatura.getContrato();
			Boolean considerarDescontos = Boolean.TRUE;

			String pArredondamentoCalculo = (String) controladorParametrosSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_ARREDONDAMENTO_CALCULO);
			String pEscala = (String) controladorParametrosSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ESCALA_CALCULO);

			final Integer parametroArredondamentoCalculo;
			if(pArredondamentoCalculo == null) {
				throw new PrecisaoNaoInformadaException();
			} else {
				parametroArredondamentoCalculo = Integer.valueOf(pArredondamentoCalculo);
			}
			final Integer parametroEscala;
			if(pEscala == null) {
				throw new EscalaNaoInformadaException();
			} else {
				parametroEscala = Integer.valueOf(pEscala);
			}

			Collection<Tarifa> colecaoTarifas = null;
			Collection<PontoConsumo> listaPontoConsumoAux = new ArrayList<PontoConsumo>();

			for (PontoConsumo pontoConsumoAux : listaPontoConsumo) {

				if(contrato == null) {
					colecaoTarifas = controladorCalculo.listarTarifasPorPontoConsumo(pontoConsumoAux, null, itemFatura);
				} else {
					colecaoTarifas = controladorCalculo.listarTarifasPorPontoConsumoSemContratoAtivo(pontoConsumoAux, itemFatura,
									contrato.getChavePrimaria());
				}
				if(colecaoTarifas != null && !colecaoTarifas.isEmpty()) {
					listaPontoConsumoAux.add(pontoConsumoAux);
				}
			}
			if(listaPontoConsumoAux != null && !listaPontoConsumoAux.isEmpty()) {

				DadosFornecimentoGasVO dadosFornecimentoGasVO = controladorCalculo.calcularValorFornecimentoGas(itemFatura, dataInicial,
								dataFinal, consumoApurado, listaPontoConsumoAux, considerarDescontos, contrato, fatura.getFaturaAvulso(),
								null, Boolean.FALSE, null, null);

				Collection<DadosVigenciaVO> listaDadosVigenciaVO = dadosFornecimentoGasVO.getDadosVigencia();
				for (DadosVigenciaVO dadosVigenciaVO : listaDadosVigenciaVO) {

					List<DadosFaixaVO> listaDadosFaixaVO = dadosVigenciaVO.getDadosFaixa();
					Tarifa tarifa = dadosVigenciaVO.getTarifaVigencia().getTarifa();

					this.ordenarlistaDadosFaixaVO(listaDadosFaixaVO);
					BigDecimal preco = null;
					for (DadosFaixaVO dadosFaixaVO : listaDadosFaixaVO) {
						RelatorioAnaliseConsumoDadosResumoVO dadosResumoVO = new RelatorioAnaliseConsumoDadosResumoVO();

						dadosResumoVO.setTabelaPreco(tarifa.getDescricao());
						dadosResumoVO.setFaixa(dadosFaixaVO.getTarifaVigenciaFaixa().getMedidaInicio() + " - "
										+ dadosFaixaVO.getTarifaVigenciaFaixa().getMedidaFim());
						dadosResumoVO.setProduto(rubrica.getDescricaoImpressao());
						dadosResumoVO.setVolume(dadosFaixaVO.getConsumoApurado());
						BigDecimal valorTotal = dadosFaixaVO.getValorVariavelComImpostoComDesconto().add(
										dadosFaixaVO.getValorFixoComImpostoComDesconto());
						try {
							preco = valorTotal.divide(dadosFaixaVO.getConsumoApurado(), parametroEscala, parametroArredondamentoCalculo);
						} catch(ArithmeticException e) {
							LOG.error(e.getStackTrace(), e);
							preco = BigDecimal.ZERO;
						}

						dadosResumoVO.setPreco(preco);
						dadosResumoVO.setTotal(valorTotal);

						// Volume: volume
						// consumido na faixa,
						// retorno da rotina de
						// cálculo de valor de
						// fornecimento de gás
						// Preço: valor unitário
						// da faixa, retorno da
						// rotina de cálculo de
						// valor de fornecimento
						// de gás
						// Valor total: valor
						// cobrado na faixa,
						// retorno da rotina de
						// cálculo de valor de
						// fornecimento de gás

						listaDadosResumoVO.add(dadosResumoVO);
					}
				}

			}
		}
		return listaDadosResumoVO;
	}

	/**
	 * Ordenarlista dados faixa vo.
	 *
	 * @param listaDadosFaixaVO
	 *            the lista dados faixa vo
	 */
	private void ordenarlistaDadosFaixaVO(List<DadosFaixaVO> listaDadosFaixaVO) {

		List<DadosFaixaVO> listaDadosFaixaVOOrdenada = new ArrayList<DadosFaixaVO>();
		for (DadosFaixaVO relatorioDescontosConcedidosVO : listaDadosFaixaVO) {
			listaDadosFaixaVOOrdenada.add(relatorioDescontosConcedidosVO);
		}
		Collections.sort(listaDadosFaixaVOOrdenada, new Comparator<DadosFaixaVO>(){

			@Override
			public int compare(DadosFaixaVO o1, DadosFaixaVO o2) {

				return o1.getTarifaVigenciaFaixa().getMedidaInicio().compareTo(o2.getTarifaVigenciaFaixa().getMedidaInicio());
			}
		});

		listaDadosFaixaVO.clear();
		listaDadosFaixaVO.addAll(listaDadosFaixaVOOrdenada);

	}

	/**
	 * Montar lista dados resumo.
	 *
	 * @param idCohi
	 *            the id cohi
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<Object[]> montarListaDadosResumo(Long idCohi, Map<String, Object> filtro) throws GGASException {

		ServiceLocator.getInstancia().getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String tipoDocumento = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_DOCUMENTO_FATURA);

		String codigoCreditoDebitoNormal = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_NORMAL);

		String codigoCreditoDebitoIncluida = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_INCLUIDO);

		String codigoCreditoDebitoParcelada = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_PARCELADO);

		Long[] arraySituacao = {Long.valueOf(codigoCreditoDebitoNormal), Long.valueOf(codigoCreditoDebitoParcelada), Long
						.valueOf(codigoCreditoDebitoIncluida)};

		Long idCliente = (Long) filtro.get(ID_CLIENTE);

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" fatura ");

		hql.append(" inner join fatura.listaFaturaItem faturaItem ");
		hql.append(" inner join faturaItem.rubrica rubrica");
		hql.append(" inner join fatura.segmento segmento");

		hql.append(" where fatura.creditoDebitoSituacao.chavePrimaria in (:arraySituacao) ");
		hql.append(" and fatura.tipoDocumento.chavePrimaria = :paramTipoDocumento ");
		hql.append(" and faturaItem.medidaConsumo is not null ");
		hql.append(" and fatura.historicoConsumo.chavePrimaria = :idCohi ");

		if(idCliente != null) {
			hql.append(" and fatura.cliente.chavePrimaria = :idCliente  ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("arraySituacao", arraySituacao);
		query.setParameter("paramTipoDocumento", Long.parseLong(tipoDocumento));
		query.setLong("idCohi", idCohi);

		if(idCliente != null) {
			query.setLong(ID_CLIENTE, idCliente);
		}

		return query.list();
	}

	/**
	 * Selecionar rubrica consumo gas.
	 *
	 * @param listaDadosResumo
	 *            the lista dados resumo
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<Object[]> selecionarRubricaConsumoGas(Collection<Object[]> listaDadosResumo) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorRubrica controladorRubrica = (ControladorRubrica) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorRubrica.BEAN_ID_CONTROLADOR_RUBRICA);

		Integer codigoFaturaItemGas = Integer.parseInt(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_GAS));
		Long codigoFaturaItemGasAux = codigoFaturaItemGas.longValue();
		Rubrica rubricaAux = controladorRubrica.obterRubricaPorItemFatura(codigoFaturaItemGasAux);

		Collection<Object[]> listaDadosResumoAux = new ArrayList<Object[]>();

		for (Object[] obj : listaDadosResumo) {
			Rubrica rubrica = (Rubrica) obj[2];
			if(rubrica.getChavePrimaria() == rubricaAux.getChavePrimaria()) {
				listaDadosResumoAux.add(obj);
			}
		}
		return listaDadosResumoAux;
	}

	/**
	 * Preenche relatorio analise consumo vo.
	 *
	 * @param listaDadosResumo
	 *            the lista dados resumo
	 * @param cohi
	 *            the cohi
	 * @param relatorioAnaliseConsumoVO1
	 *            the relatorio analise consumo v o1
	 */
	private void preencheRelatorioAnaliseConsumoVO(Collection<Object[]> listaDadosResumo, HistoricoConsumo cohi,
					RelatorioAnaliseConsumoVO relatorioAnaliseConsumoVO1)  {

		for (Object[] arrayDadosResumo : listaDadosResumo) {

			Fatura fatura = (Fatura) arrayDadosResumo[0];

			Date dataInicial = null;
			Date dataFinal = null;
			if(cohi != null) {
				if(cohi.getHistoricoAnterior() != null && cohi.getHistoricoAnterior().getDataLeituraInformada() != null) {

					dataInicial = cohi.getHistoricoAnterior().getDataLeituraInformada();
				}
				dataFinal = cohi.getHistoricoAtual().getDataLeituraInformada();
				if(dataInicial != null && dataInicial.before(dataFinal)) {
					dataInicial = Util.incrementarDia(dataInicial, 1);
				} else if(dataInicial == null) {
					dataInicial = DataUtil.decrementarDia(dataFinal, cohi.getDiasConsumo());
				}
			}

			if(fatura.getCliente().getCpf() != null) {
				relatorioAnaliseConsumoVO1.setCpfCnpj(fatura.getCliente().getCpf());
			} else {
				relatorioAnaliseConsumoVO1.setCpfCnpj(fatura.getCliente().getCnpj());
			}

			relatorioAnaliseConsumoVO1.setNomeCliente(fatura.getCliente().getNome());
			relatorioAnaliseConsumoVO1.setNomeFantasia(fatura.getCliente().getNomeFantasia());

			String[] numDocFiscal = buscarDocFiscalPorFaturaItem(listaDadosResumo);
			relatorioAnaliseConsumoVO1.setNumeroDocFiscal(numDocFiscal[0]);

			if(cohi != null) {
				relatorioAnaliseConsumoVO1.setTipoConsumo(cohi.getTipoConsumo().getDescricao());
			}
			if(dataInicial != null) {
				relatorioAnaliseConsumoVO1.setDataInicial(Util.converterDataParaStringSemHora(dataInicial, Constantes.FORMATO_DATA_BR));
			} else {
				relatorioAnaliseConsumoVO1.setDataInicial("");
			}

			if(dataFinal != null) {
				relatorioAnaliseConsumoVO1.setDataFinal(Util.converterDataParaStringSemHora(dataFinal, Constantes.FORMATO_DATA_BR));
			}
		}
	}

	/**
	 * Buscar doc fiscal por fatura item.
	 *
	 * @param listaDadosResumo
	 *            the lista dados resumo
	 * @return the string[]
	 */
	private String[] buscarDocFiscalPorFaturaItem(Collection<Object[]> listaDadosResumo) {

		Collection<DocumentoFiscal> listaDocumentoFiscal = new ArrayList<DocumentoFiscal>();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String[] numDocFiscal = new String[listaDocumentoFiscal.size() + 1];

		for (Object[] obj : listaDadosResumo) {

			Fatura fatura = (Fatura) obj[0];

			ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

			listaDocumentoFiscal = controladorFatura.obterDocumentosFiscaisPorFatura(fatura.getChavePrimaria());
		}

		Long codigoTipoOperacaoSaida = Long.parseLong(controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_SAIDA));
		int count = 0;

		for (DocumentoFiscal documentoFiscal : listaDocumentoFiscal) {
			if(documentoFiscal.getTipoOperacao().getChavePrimaria() == codigoTipoOperacaoSaida) {
				numDocFiscal[count] = String.valueOf(documentoFiscal.getNumero());
				count++;
			}

		}
		return numDocFiscal;
	}

}
