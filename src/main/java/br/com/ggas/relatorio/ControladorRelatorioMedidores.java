package br.com.ggas.relatorio;

import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.geral.exception.GGASException;

public interface ControladorRelatorioMedidores {

	String BEAN_ID_CONTROLADOR_RELATORIO_MEDIDORES = "controladorRelatorioMedidores";
	
	String ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO = "ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO";

	String ERRO_NEGOCIO_RELATORIO_MEDIDORES_NENHUM_REGISTRO_ENCONTRADO = "ERRO_NEGOCIO_RELATORIO_MEDIDORES_NENHUM_REGISTRO_ENCONTRADO";
	
	String RELATORIO_MEDIDORES = "relatorioMedidores.jasper";
	
	String RELATORIO_SUBSTITUICAO_MEDIDOR = "relatorioSubstituicaoMedidores.jasper";
	
	String RELATORIO_RETIRADA_MEDIDOR = "relatorioRetiradaMedidores.jasper";
	/**
	 * Método reponsável por gerar o relatório.
	 * 
	 * @param dataInicial
	 * @param dataFinal 
	 * @param exibirFiltros
	 * @param tipoRelatorio
	 *            
	 * @return the byte[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] gerarRelatorio(Date dataInicial, Date dataFinal, String exibirFiltros, String tipoRelatorio, String numeroSerie, 
			String marcaMedidor, String tipoMedidor, String modoUso, String modelo, String localArmazenagem
			, String habilitado, String anoFabricacao) throws GGASException;
	

	byte[] gerarRelatorio(Date dataInicial, Date dataFinal, String exibirFiltros, String tipoRelatorio, String operacao, 
			List<Long> equipe, List<Long> servico, String tipoRelacaoRelatorio, Map<String,Object> filtros) throws GGASException;
}
