/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.relatorio.comum;

import java.util.Collection;

import br.com.ggas.cadastro.imovel.Segmento;
/**
 * 
 * @author pedro
 *	Classe responsável pela transfereência de dados
 */
public class SelecionarListaOrdenadaVO {
	
	private Collection<Segmento> listaSegmentos;
	
	private String nomeMetodoRecarregarGrid;
	
	private String nomeParametroCpsAdicionadas;
	
	private Integer numeroMinimoEntidade;
	
	private Integer numeroMaximoEntidade ;
	
	private String dataEmissaoInicial;
	
	private String dataEmissaoFinal;

	/**
	 * @return the listaSegmentos
	 */
	public Collection<Segmento> getListaSegmentos() {
	
		return listaSegmentos;
	}

	
	/**
	 * @param listaSegmentos the listaSegmentos to set
	 */
	public void setListaSegmentos(Collection<Segmento> listaSegmentos) {
	
		this.listaSegmentos = listaSegmentos;
	}

	
	/**
	 * @return the nomeMetodoRecarregarGrid
	 */
	public String getNomeMetodoRecarregarGrid() {
	
		return nomeMetodoRecarregarGrid;
	}

	
	/**
	 * @param nomeMetodoRecarregarGrid the nomeMetodoRecarregarGrid to set
	 */
	public void setNomeMetodoRecarregarGrid(String nomeMetodoRecarregarGrid) {
	
		this.nomeMetodoRecarregarGrid = nomeMetodoRecarregarGrid;
	}

	
	/**
	 * @return the nomeParametroCpsAdicionadas
	 */
	public String getNomeParametroCpsAdicionadas() {
	
		return nomeParametroCpsAdicionadas;
	}

	
	/**
	 * @param nomeParametroCpsAdicionadas the nomeParametroCpsAdicionadas to set
	 */
	public void setNomeParametroCpsAdicionadas(String nomeParametroCpsAdicionadas) {
	
		this.nomeParametroCpsAdicionadas = nomeParametroCpsAdicionadas;
	}

	
	/**
	 * @return the numeroMinimoEntidade
	 */
	public Integer getNumeroMinimoEntidade() {
	
		return numeroMinimoEntidade;
	}

	
	/**
	 * @param numeroMinimoEntidade the numeroMinimoEntidade to set
	 */
	public void setNumeroMinimoEntidade(Integer numeroMinimoEntidade) {
	
		this.numeroMinimoEntidade = numeroMinimoEntidade;
	}

	
	/**
	 * @return the numeroMaximoEntidade
	 */
	public Integer getNumeroMaximoEntidade() {
	
		return numeroMaximoEntidade;
	}

	
	/**
	 * @param numeroMaximoEntidade the numeroMaximoEntidade to set
	 */
	public void setNumeroMaximoEntidade(Integer numeroMaximoEntidade) {
	
		this.numeroMaximoEntidade = numeroMaximoEntidade;
	}

	
	/**
	 * @return the dataEmissaoInicial
	 */
	public String getDataEmissaoInicial() {
	
		return dataEmissaoInicial;
	}

	
	/**
	 * @param dataEmissaoInicial the dataEmissaoInicial to set
	 */
	public void setDataEmissaoInicial(String dataEmissaoInicial) {
	
		this.dataEmissaoInicial = dataEmissaoInicial;
	}

	
	/**
	 * @return the dataEmissaoFinal
	 */
	public String getDataEmissaoFinal() {
	
		return dataEmissaoFinal;
	}

	
	/**
	 * @param dataEmissaoFinal the dataEmissaoFinal to set
	 */
	public void setDataEmissaoFinal(String dataEmissaoFinal) {
	
		this.dataEmissaoFinal = dataEmissaoFinal;
	}
	
}
