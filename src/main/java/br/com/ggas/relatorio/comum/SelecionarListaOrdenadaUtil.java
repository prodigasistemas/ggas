/*
Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.relatorio.comum;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.exception.NegocioException;

/**
 * The Class SelecionarListaOrdenada.
 */
public class SelecionarListaOrdenadaUtil {
	
	/**
	 * By SONAR:
	 * Utility classes, which are a collection of static members, 
	 * are not meant to be instantiated. Java adds an implicit public 
	 * constructor to every class which does not define at least one explicitly. 
	 * Hence, at least one non-public constructor should be defined.
	 */
	private SelecionarListaOrdenadaUtil() {
		
	}
	
	/**
	 * Montar selecionar lista ordenada.
	 *
	 * @param model the model
	 * @param vo - {@link SelecionarListaOrdenadaVO}
	 * @param listaSegmentos the lista segmentos
	 * @param nomeMetodoRecarregarGrid the nome metodo recarregar grid
	 * @param nomeParametroCpsAdicionadas the nome parametro cps adicionadas
	 * @param numeroMinimoEntidade the numero minimo entidade
	 * @param numeroMaximoEntidade the numero maximo entidade
	 * @param dataEmissaoInicial the data emissao inicial
	 * @param dataEmissaoFinal the data emissao final
	 */
	public static void montarSelecionarListaOrdenada(
					ModelAndView model, SelecionarListaOrdenadaVO vo) {
		
		model.addObject("listaSegmentos", vo.getListaSegmentos());
		model.addObject("nomeMetodoRecarregarGrid", vo.getNomeMetodoRecarregarGrid());
		model.addObject("nomeParametroCpsAdicionadas", vo.getNomeParametroCpsAdicionadas());
		model.addObject("numeroMinimoEntidade", vo.getNumeroMinimoEntidade());
		model.addObject("numeroMaximoEntidade", vo.getNumeroMaximoEntidade());
		model.addObject("dataEmissaoInicial", vo.getDataEmissaoInicial());
		model.addObject("dataEmissaoFinal", vo.getDataEmissaoFinal());
		
	}
	
	/**
	 * Montar grid lista ordenada segmentos.
	 *
	 * @param model the model
	 * @param controladorSegmento the controlador segmento
	 * @param cpsSegmentosAdicionados the cps segmentos adicionados
	 * @throws NegocioException the negocio exception
	 */
	public static void montarGridListaOrdenadaSegmentos(
					ModelAndView model, ControladorSegmento controladorSegmento, 
					Long[] cpsSegmentosAdicionados) throws NegocioException {
		
		List<Segmento> listaSegmentosSelecionados = new ArrayList<Segmento>();
		
		if (cpsSegmentosAdicionados != null) {
			for (Long cpSegmentoAdicionadas : cpsSegmentosAdicionados) {
				Segmento segmentosAdicionados = (Segmento) controladorSegmento.obter(cpSegmentoAdicionadas);
				listaSegmentosSelecionados.add(segmentosAdicionados);			
			}
		}
		
		model.addObject("listaEntidadesSelecionadas", listaSegmentosSelecionados);
		model.addObject("labelEntidade", "Segmento");

	}
	
	/**
	 * Montar grid lista ordenada segmentos.
	 *
	 * @param controladorSegmento the controlador segmento
	 * @param cpsSegmentosAdicionados the cps segmentos adicionados
	 * @return the model and view
	 * @throws NegocioException the negocio exception
	 */
	public static ModelAndView montarGridListaOrdenadaSegmentos(
					ControladorSegmento controladorSegmento, 
					Long[] cpsSegmentosAdicionados) throws NegocioException {
		
		ModelAndView model = new ModelAndView("gridSelecionarListaOrdenada");
		
		montarGridListaOrdenadaSegmentos(model, controladorSegmento, cpsSegmentosAdicionados);
		
		return model;
		
	}
	
}
