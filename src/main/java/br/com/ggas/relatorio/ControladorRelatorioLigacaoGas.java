package br.com.ggas.relatorio;

import java.util.Date;

import br.com.ggas.geral.exception.GGASException;


public interface ControladorRelatorioLigacaoGas {

	String BEAN_ID_CONTROLADOR_RELATORIO_DATA_LIMITE_LIGACAO_GAS = "controladorServicoAutorizavao";
	
	String ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO = "ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO";
	
	String ERRO_NEGOCIO_RELATORIO_DATA_LIMITE_LIGACAO_GAS_NENHUM_REGISTRO_ENCONTRADO = "ERRO_NEGOCIO_RELATORIO_DATA_LIMITE_"
					+ "LIGACAO_GAS_NENHUM_REGISTRO_ENCONTRADO";	
	

	String RELATORIO_DATA_LIMITE_LIGAMENTO_GAS_JASPER = "relatorioDataLimiteLigacaoGas.jasper";
	
	/**
		 * Método reponsável por gerar o relatório.
		 * @param dataIncial - {@link Date}
		 * @param dataFinal - {@link Date}
		 * @param exibirFiltros - {@link String}
		 * @param tipoRelatorio - {@link String}
		 * @param parametros
		 *            the parametros
		 * @return the byte[]
		 * @throws GGASException
		 *             the GGAS exception
	 */
	
	byte[] gerarRelatorio(Date dataIncial, Date dataFinal, String exibirFiltros, String tipoRelatorio) throws GGASException;
}
