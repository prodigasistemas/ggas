package br.com.ggas.relatorio;

import java.util.Date;
import java.util.Map;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.util.FormatoImpressao;

public interface ControladorRelatorioClientesLigadosPrimeiraVez {
	
	String BEAN_ID_CONTROLADOR_RELATORIO_CLIENTES_LIGADOS_PRIMEIRA_VEZ = "controladorRelatorioClientesLigadosPrimeiraVez";
	
	String RELATORIO_CLIENTES_LIGADOS_PRIMEIRA_VEZ_JASPER = "relatorioClientesLigadosPrimeiraVez.jasper";
	
	String ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO = "ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO";
	
	Class<?> getClasseEntidadeHistoricoOperacaoMedidor();
		
	/**
	 * Método reponsável por gerar o relatório.
	 * 
	 * @param parametros
	 * @param formatoImpressao
	 * @return the byte[]
	 * @throws GGASException
	 */
	
	byte[] gerarRelatorio(Date dataInicial, Date dataFinal, String exibirFiltros, String tipoRelatorio) throws GGASException;

	
	
}
