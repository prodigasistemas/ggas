
package br.com.ggas.relatorio;

import java.util.Map;

import br.com.ggas.geral.exception.GGASException;

public interface ControladorRelatorioVolumesFaturadosSegmento {

	String BEAN_ID_CONTROLADOR_RELATORIO_VOLUMES_FATURADOS_SEGMENTO = "controladorRelatorioVolumesFaturadosSegmento";

	/**
	 * Consultar.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the byte[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] consultar(Map<String, Object> filtro) throws GGASException;

}
