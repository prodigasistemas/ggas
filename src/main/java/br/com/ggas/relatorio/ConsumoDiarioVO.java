/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
package br.com.ggas.relatorio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
/**
 * 
 * Classe responsável pela armazenagem dos dados do Consumo Diário
 *
 */
public class ConsumoDiarioVO implements Serializable {
	private static final long serialVersionUID = 2835592067091808546L;
	
	private Long idHistoricoConsumo;
	private Date dataLeituraInformada;
	private BigDecimal consumoApurado;
	
	/**
	 * Constroi um objeto de consumo diario
	 * 
	 * @param idHistoricoConsumo chave primaria do historico de consumo
	 * @param dataLeituraInformada data da leitura do medidor
	 * @param consumoApurado consumo apurado com a aplicacao dos fatores de correcao
	 * **/
	public ConsumoDiarioVO(Long idHistoricoConsumo, Date dataLeituraInformada, BigDecimal consumoApurado) {
		this.idHistoricoConsumo = idHistoricoConsumo;
		this.dataLeituraInformada = dataLeituraInformada;
		this.consumoApurado = consumoApurado;
	}
	
	public Long getIdHistoricoConsumo() {
		return idHistoricoConsumo;
	}
	public void setIdHistoricoConsumo(Long idHistoricoConsumo) {
		this.idHistoricoConsumo = idHistoricoConsumo;
	}
	public Date getDataLeituraInformada() {
		return dataLeituraInformada;
	}
	public void setDataLeituraInformada(Date dataLeituraInformada) {
		this.dataLeituraInformada = dataLeituraInformada;
	}
	public BigDecimal getConsumoApurado() {
		return consumoApurado;
	}
	public void setConsumoApurado(BigDecimal consumoApurado) {
		this.consumoApurado = consumoApurado;
	}
	
	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder();
		builder.append(this.idHistoricoConsumo);
		return builder.hashCode();
	}

	@Override
	public boolean equals(Object obj) {		
		if (!(obj instanceof ConsumoDiarioVO)) {
			return false;
		}
		ConsumoDiarioVO otherObj  = (ConsumoDiarioVO) obj;
		EqualsBuilder builder = new EqualsBuilder();
		builder.append(this.idHistoricoConsumo, otherObj.getIdHistoricoConsumo());
		return builder.isEquals();
	}	
}
