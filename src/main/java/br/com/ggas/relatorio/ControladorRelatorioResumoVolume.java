/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.web.relatorio.resumoVolume.ResumoVolumeVO;

public interface ControladorRelatorioResumoVolume extends ControladorNegocio {

	String CAMPOS_OBRIGATORIOS = "CAMPOS_OBRIGATORIOS";

	String BEAN_ID_CONTROLADOR_RELATORIO_RESUMO_VOLUME = "controladorRelatorioResumoVolume";

	/**
	 * Método responsável por validar os filtros
	 * do relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarFiltroRelatorioResumoVolume(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por gerar o relatorio.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	byte[] gerarRelatorioResumoVolume(Map<String, Object> filtro) throws NegocioException, FormatoInvalidoException;

	/**
	 * Obter empresa principal.
	 * 
	 * @return the empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Empresa obterEmpresaPrincipal() throws NegocioException;

	/**
	 * Método responsável por montar os parametros
	 * do relatorio de acordo com os
	 * filtros selecionados pelo usuário.
	 * 
	 * @param parametros
	 *            the parametros
	 * @param filtro
	 *            the filtro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void montarParametroRelatorio(Map<String, Object> parametros, Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por montar uma lista com
	 * o anoMesReferencia e os anoMesReferencia
	 * anteriores.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return List<Integer>
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<Integer> verificarAnoMes(Map<String, Object> filtro) throws FormatoInvalidoException, NegocioException;

	/**
	 * Método responsável por montar o relatorio
	 * analitico de resumo volume.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return Collection<ResumoVolumeVO>
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	Collection<ResumoVolumeVO> montarRelatorioAnaliticoResumoVolume(Map<String, Object> filtro) throws NegocioException,
					FormatoInvalidoException;

	/**
	 * Método responsável por montar o relatorio
	 * sintetico de resumo volume.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return Collection<ResumoVolumeVO>
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	Collection<ResumoVolumeVO> montarRelatorioSinteticoResumoVolume(Map<String, Object> filtro) throws NegocioException,
					FormatoInvalidoException;

	/**
	 * Método responsável por montar preencher a
	 * entidade ResumoVolumeVO do relatorio
	 * sintetico Resumo Volume.
	 * 
	 * @param listaFatura
	 *            the lista fatura
	 * @param resumoVolumeVO
	 *            the resumo volume vo
	 * @param anoMes
	 *            the ano mes
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	void preencherResumoVolumeVO(List<Fatura> listaFatura, ResumoVolumeVO resumoVolumeVO, String anoMes) throws FormatoInvalidoException;
}
