package br.com.ggas.relatorio;

import java.util.Date;

import br.com.ggas.geral.exception.GGASException;
/**
 * Controlador responsável pelo relatório de prazo mínimo para entrega de fatura.
 */
public interface ControladorRelatorioPrazoMinimoEntregaFatura {
	
	String BEAN_ID_CONTROLADOR_RELATORIO_PRAZO_MINIMO_ENTREGA_FATURA = "controladorFatura";

	String ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO = "ERRO_NEGOCIO_FORMATO_IMPRESSAO_NAO_INFORMADO";

	String ERRO_NEGOCIO_RELATORIO_PRAZO_MINIMO_ENTREGA_FATURA = "ERRO_NEGOCIO_RELATORIO_PRAZO_MINIMO_ENTREGA_FATURA";

	String RELATORIO_PRAZO_MINIMO_ENTREGA_FATURA_JASPER = "relatorioPrazoMinimoEntregaFatura.jasper";

	/**
	 * Método reponsável por gerar o relatório.
	 * 
	 * @param dataIncial - {@link Date}
	 * @param exibirFiltros - {@link String}
	 * @param tipoRelatorio - {@link String}
	 * @param dataFinal - {@link Date}
	 *            
	 * @return the byte[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] gerarRelatorio(Date dataIncial, Date dataFinal, String exibirFiltros, String tipoRelatorio) throws GGASException;
}
