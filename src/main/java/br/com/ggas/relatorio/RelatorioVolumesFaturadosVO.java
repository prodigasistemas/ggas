/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;

import org.apache.log4j.Logger;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

public class RelatorioVolumesFaturadosVO implements Serializable {

	private static final long serialVersionUID = -2131067729112754272L;

	private String segmento;

	private Collection<SubRelatorioVolumesFaturadosVO> colecaoVOs;

	private String tipoExibicao;

	private BigDecimal totalMetroCubicoPeriodo;

	private BigDecimal totalMetroCubicoMediaDiaria;

	private BigDecimal totalTarifaMediaGas;

	private Integer quantidadeClientesFaturados;

	private Integer quantidadeClientesAtivos;

	private Integer clientesSemConsumo;
	
	private static final Logger LOG = Logger.getLogger(RelatorioVolumesFaturadosVO.class);
	
	public String getTipoExibicao() {

		return tipoExibicao;
	}

	public void setTipoExibicao(String tipoExibicao) {

		this.tipoExibicao = tipoExibicao;
	}

	public String getSegmento() {

		return segmento;
	}

	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	public Collection<SubRelatorioVolumesFaturadosVO> getColecaoVOs() {

		return colecaoVOs;
	}

	public void setColecaoVOs(Collection<SubRelatorioVolumesFaturadosVO> colecaoVOs) {

		this.colecaoVOs = colecaoVOs;
	}

	public BigDecimal getTotalMetroCubicoPeriodo() {

		return totalMetroCubicoPeriodo;
	}

	public void setTotalMetroCubicoPeriodo(BigDecimal totalMetroCubicoPeriodo) {

		this.totalMetroCubicoPeriodo = totalMetroCubicoPeriodo;
	}

	public String getTotalMetroCubicoPeriodoFormatado() {

		String retorno = " - ";
		if(totalMetroCubicoPeriodo != null) {
			retorno = Util.formatarValorNumerico(totalMetroCubicoPeriodo.intValue());
		}

		return retorno;
	}

	public BigDecimal getTotalMetroCubicoMediaDiaria() {

		return totalMetroCubicoMediaDiaria;
	}

	public void setTotalMetroCubicoMediaDiaria(BigDecimal totalMetroCubicoMediaDiaria) {

		this.totalMetroCubicoMediaDiaria = totalMetroCubicoMediaDiaria;
	}

	public String getTotalMetroCubicoMediaDiariaFormatado() {

		String retorno = " - ";
		if(totalMetroCubicoMediaDiaria != null) {
			retorno = Util.formatarValorNumerico(totalMetroCubicoMediaDiaria.intValue());
		}

		return retorno;
	}

	public BigDecimal getTotalTarifaMediaGas() {

		return totalTarifaMediaGas;
	}

	public void setTotalTarifaMediaGas(BigDecimal totalTarifaMediaGas) {

		this.totalTarifaMediaGas = totalTarifaMediaGas;
	}

	public String getTotalTarifaMediaGasFormatado() {

		String retorno = " - ";
		if(totalTarifaMediaGas != null) {
			BigDecimal divide = totalTarifaMediaGas.divide(new BigDecimal(colecaoVOs.size()), 4, RoundingMode.HALF_UP);
			retorno = Util.converterCampoValorDecimalParaString("", divide, Constantes.LOCALE_PADRAO);
		}

		return retorno;
	}

	public Integer getQuantidadeClientesFaturados() {

		return quantidadeClientesFaturados;
	}

	public void setQuantidadeClientesFaturados(Integer quantidadeClientesFaturados) {

		this.quantidadeClientesFaturados = quantidadeClientesFaturados;
	}

	public Integer getQuantidadeClientesAtivos() {

		return quantidadeClientesAtivos;
	}

	public void setQuantidadeClientesAtivos(Integer quantidadeClientesAtivos) {

		this.quantidadeClientesAtivos = quantidadeClientesAtivos;
	}

	public Integer getClientesSemConsumo() {

		return clientesSemConsumo;
	}

	public void setClientesSemConsumo(Integer clientesSemConsumo) {

		this.clientesSemConsumo = clientesSemConsumo;
	}

	public String getTotalPercentualVolumeFormatado() {

		return Util.converterCampoValorDecimalParaString("", new BigDecimal(100), Constantes.LOCALE_PADRAO, 2);
	}

	public String getTotalPercentualFaturamentoGasFormatado() {

		return Util.converterCampoValorDecimalParaString("", new BigDecimal(100), Constantes.LOCALE_PADRAO, 2);
	}

}
