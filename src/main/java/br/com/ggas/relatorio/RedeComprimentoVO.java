package br.com.ggas.relatorio;

import java.math.BigDecimal;

public class RedeComprimentoVO {
	
	private Long chavePrimaria;
	private BigDecimal redeComprimento;
	private BigDecimal redeReferencia;
	
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	public BigDecimal getRedeComprimento() {
		return redeComprimento;
	}
	public void setRedeComprimento(BigDecimal redeComprimento) {
		this.redeComprimento = redeComprimento;
	}
	public BigDecimal getRedeReferencia() {
		return redeReferencia;
	}
	public void setRedeReferencia(BigDecimal redeReferencia) {
		this.redeReferencia = redeReferencia;
	}
	
}
