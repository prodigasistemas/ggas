
package br.com.ggas.relatorio;

import java.util.Map;

import br.com.ggas.geral.exception.GGASException;

public interface ControladorRelatorioTitulosAtraso {

	String BEAN_ID_CONTROLADOR_RELATORIO_TITULOS_ATRASO = "controladorRelatorioTitulosAtraso";

	String RELATORIO_TITULOS_ATRASO = "relatorioTitulosAtraso.jasper";

	String ERRO_NEGOCIO_NENHUMA_FATURA_EM_ATRASO_FOI_ENCONTRADA = "ERRO_NEGOCIO_NENHUMA_FATURA_EM_ATRASO_FOI_ENCONTRADA";

	/**
	 * Consultar.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the byte[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] consultar(Map<String, Object> filtro) throws GGASException;

}
