/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *13/11/2013
 * vpessoa
 * 08:37:55
 */

package br.com.ggas.relatorio;

import br.com.ggas.atendimento.chamado.dominio.ChamadoVO;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.FormatoImpressao;

/**
 * @author vpessoa
 */
public interface ControladorRelatorioChamado {

	String RELATORIO_CHAMADO = "relatorioChamado.jasper";

	String RELATORIOS_CHAMADO_CLIENTE_PONTOCOSUMO = "relatorioChamadoClientePontoConsumo.jasper";

	String RELATORIOS_QUANTITATIVO_CHAMADO = "relatorioQuantitativoChamado.jasper";
	
	String RELATORIO_ANALITICO_CHAMADO = "relatorioChamadoAnalitico.jasper";
	
	String RELATORIO_RESUMO_CHAMADO = "relatorioResumoChamados.jasper";

	/**
	 * Gerar relatorio.
	 * 
	 * @param chavePrimariaChamado - {@link Long}
	 * @param comAS - {@link Boolean}
	 * @return the byte[]
	 * @throws NegocioException
	 *             the negocio exception
	 */
	byte[] gerarRelatorio(Long chavePrimariaChamado, Boolean comAS) throws NegocioException;

	/**
	 * Gerar relatorio chamado cliente ponto consumo.
	 * 
	 * @param chamadoVO
	 *            the chamado vo
	 * @param formatoImpressao
	 *            the formato impressao
	 * @param exibirFiltros
	 *            the exibir filtros
	 * @return the byte[]
	 * @throws GGASException
	 *             the GGAS exception
	 */
	byte[] gerarRelatorioChamadoClientePontoConsumo(ChamadoVO chamadoVO, FormatoImpressao formatoImpressao, String exibirFiltros)
			throws GGASException;

	/**
	 * Gerar relatorio chamado quantitativo
	 * 
	 * @param chamadoVO - {@link ChamadoVO}
	 * @param formatoImpressao - {@link FormatoImpressao}
	 * @param exibirFiltros - {@link String}
	 * @return relatorioChamadoQuantitativo - array de bytes
	 * @throws GGASException - {@link GGASException}
	 */
	byte[] gerarRelatorioChamadoQuantitativo(ChamadoVO chamadoVO, FormatoImpressao formatoImpressao, String exibirFiltros)
			throws GGASException;

	byte[] gerarRelatorioChamadoAnaltico(ChamadoVO chamadoVO, FormatoImpressao formatoImpressao, String exibirFiltros)
			throws GGASException;

	byte[] gerarRelatorioChamadoResumo(ChamadoVO chamadoVO, FormatoImpressao formatoImpressao, String exibirFiltros) throws GGASException;

}
