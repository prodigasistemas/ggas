/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.log4j.Logger;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

public class SubRelatorioVolumesFaturadosVO implements Serializable {
	
	private static final Logger LOG = Logger.getLogger(SubRelatorioVolumesFaturadosVO.class);

	private static final long serialVersionUID = -2131067729112754272L;

	private String razaoSocial;

	private String nomeFantasia;

	private String endereco;

	private String cidade;

	private String dataInicioConsumo;

	private String mesAnoCiclo;

	private BigDecimal metroCubicoMediaDiaria;

	private BigDecimal metroCubicoPeriodo;

	private BigDecimal percentualVolume;

	private BigDecimal tarifaMediaGas;

	private BigDecimal percentualFaturamentoGas;

	private BigDecimal tarifaMediaMargem;

	private BigDecimal percentualFaturamentoMargem;

	private BigDecimal tarifaMediaTransporte;

	private BigDecimal percentualFaturamentoTransporte;

	private BigDecimal totalTarifaMediaMargem;

	private BigDecimal totalPercentualFaturamentoMargem;

	private BigDecimal totalTarifaMediaTransporte;

	private BigDecimal totalPercentualFaturamentoTransporte;

	private BigDecimal valorMediaGas;

	private BigDecimal valorMediaMargem;

	private BigDecimal valorMediaTransporte;

	private Date dataAssinaturaContrato;

	private Long idCliente;

	private Long idClienteAtivos;

	private Long idClienteSemConsumo;

	private String nomeCliente;
	
	private BigDecimal volumeMedido;
	
	private BigDecimal volumeFaturado;
	
	private BigDecimal mediaVolume;
	
	private BigDecimal tarifaMedia;
	
	private String ramoAtividade;
	
	private String segmento;
	
	private String clienteDesde;
	
	private BigDecimal totalVolumeFaturado;
	
	private Integer quantidadeDiasMes;
	
	private BigDecimal valorFaturado;
	
	private String tipoTarifa;
	
	private String pontoConsumo;
	
	private String imovel;

	private BigDecimal totalValorFaturado;
	
	private String codigoPontoConsumo;

	/**
	 * Instantiates a new sub relatorio volumes faturados vo.
	 */
	public SubRelatorioVolumesFaturadosVO() {

		super();
		razaoSocial = "";
		nomeFantasia = "";
		endereco = "";
		cidade = "";
		mesAnoCiclo = "";
		dataInicioConsumo = "";
		metroCubicoMediaDiaria = null;
		metroCubicoPeriodo = null;
		percentualVolume = null;
		tarifaMediaGas = null;
		valorMediaGas = null;
		percentualFaturamentoGas = null;
		tarifaMediaMargem = null;
		valorMediaMargem = null;
		percentualFaturamentoMargem = null;
		tarifaMediaTransporte = null;
		valorMediaTransporte = null;
		percentualFaturamentoTransporte = null;
	}

	public String getRazaoSocial() {

		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {

		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {

		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {

		this.nomeFantasia = nomeFantasia;
	}

	public String getEndereco() {

		return endereco;
	}

	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	public String getCidade() {

		return cidade;
	}

	public void setCidade(String cidade) {

		this.cidade = cidade;
	}

	public String getDataInicioConsumo() {

		return dataInicioConsumo;
	}

	public void setMesAnoCiclo(String mesAnoCiclo) {

		this.mesAnoCiclo = mesAnoCiclo;
	}

	public String getMesAnoCiclo() {

		return mesAnoCiclo;
	}

	public void setDataInicioConsumo(String dataInicioConsumo) {

		this.dataInicioConsumo = dataInicioConsumo;
	}

	public BigDecimal getMetroCubicoMediaDiaria() {

		return metroCubicoMediaDiaria;
	}

	public void setMetroCubicoMediaDiaria(BigDecimal metroCubicoMediaDiaria) {

		this.metroCubicoMediaDiaria = metroCubicoMediaDiaria;
	}

	public BigDecimal getMetroCubicoPeriodo() {

		return metroCubicoPeriodo;
	}

	public void setMetroCubicoPeriodo(BigDecimal metroCubicoPeriodo) {

		this.metroCubicoPeriodo = metroCubicoPeriodo;
	}

	public BigDecimal getPercentualVolume() {

		return percentualVolume;
	}

	public void setPercentualVolume(BigDecimal percentualVolume) {

		this.percentualVolume = percentualVolume;
	}

	public BigDecimal getTarifaMediaGas() {

		return tarifaMediaGas;
	}

	public void setTarifaMediaGas(BigDecimal tarifaMediaGas) {

		this.tarifaMediaGas = tarifaMediaGas;
	}

	public BigDecimal getValorMediaGas() {

		return valorMediaGas;
	}

	public void setValorMediaGas(BigDecimal valorMediaGas) {

		this.valorMediaGas = valorMediaGas;
	}

	public BigDecimal getPercentualFaturamentoGas() {

		return percentualFaturamentoGas;
	}

	public void setPercentualFaturamentoGas(BigDecimal percentualFaturamentoGas) {

		this.percentualFaturamentoGas = percentualFaturamentoGas;
	}

	public BigDecimal getTarifaMediaMargem() {

		return tarifaMediaMargem;
	}

	public void setTarifaMediaMargem(BigDecimal tarifaMediaMargem) {

		this.tarifaMediaMargem = tarifaMediaMargem;
	}

	public BigDecimal getValorMediaMargem() {

		return valorMediaMargem;
	}

	public void setValorMediaMargem(BigDecimal valorMediaMargem) {

		this.valorMediaMargem = valorMediaMargem;
	}

	public BigDecimal getPercentualFaturamentoMargem() {

		return percentualFaturamentoMargem;
	}

	public void setPercentualFaturamentoMargem(BigDecimal percentualFaturamentoMargem) {

		this.percentualFaturamentoMargem = percentualFaturamentoMargem;
	}

	public BigDecimal getTarifaMediaTransporte() {

		return tarifaMediaTransporte;
	}

	public void setTarifaMediaTransporte(BigDecimal tarifaMediaTransporte) {

		this.tarifaMediaTransporte = tarifaMediaTransporte;
	}

	public BigDecimal getValorMediaTransporte() {

		return valorMediaTransporte;
	}

	public void setValorMediaTransporte(BigDecimal valorMediaTransporte) {

		this.valorMediaTransporte = valorMediaTransporte;
	}

	public BigDecimal getPercentualFaturamentoTransporte() {

		return percentualFaturamentoTransporte;
	}

	public void setPercentualFaturamentoTransporte(BigDecimal percentualFaturamentoTransporte) {

		this.percentualFaturamentoTransporte = percentualFaturamentoTransporte;
	}

	public BigDecimal getTotalTarifaMediaMargem() {

		return totalTarifaMediaMargem;
	}

	public void setTotalTarifaMediaMargem(BigDecimal totalTarifaMediaMargem) {

		this.totalTarifaMediaMargem = totalTarifaMediaMargem;
	}

	public BigDecimal getTotalPercentualFaturamentoMargem() {

		return totalPercentualFaturamentoMargem;
	}

	public void setTotalPercentualFaturamentoMargem(BigDecimal totalPercentualFaturamentoMargem) {

		this.totalPercentualFaturamentoMargem = totalPercentualFaturamentoMargem;
	}

	public BigDecimal getTotalTarifaMediaTransporte() {

		return totalTarifaMediaTransporte;
	}

	public void setTotalTarifaMediaTransporte(BigDecimal totalTarifaMediaTransporte) {

		this.totalTarifaMediaTransporte = totalTarifaMediaTransporte;
	}

	public BigDecimal getTotalPercentualFaturamentoTransporte() {

		return totalPercentualFaturamentoTransporte;
	}

	public void setTotalPercentualFaturamentoTransporte(BigDecimal totalPercentualFaturamentoTransporte) {

		this.totalPercentualFaturamentoTransporte = totalPercentualFaturamentoTransporte;
	}

	public Date getDataAssinaturaContrato() {
		Date data = null;
		if (this.dataAssinaturaContrato != null) {
			data = (Date) dataAssinaturaContrato.clone();
		}
		return data;
	}

	public void setDataAssinaturaContrato(Date dataAssinaturaContrato) {

		this.dataAssinaturaContrato = dataAssinaturaContrato;
	}

	public Long getIdCliente() {

		return idCliente;
	}

	public void setIdCliente(Long idCliente) {

		this.idCliente = idCliente;
	}

	public Long getIdClienteAtivos() {

		return idClienteAtivos;
	}

	public void setIdClienteAtivos(Long idClienteAtivos) {

		this.idClienteAtivos = idClienteAtivos;
	}

	public Long getIdClienteSemConsumo() {

		return idClienteSemConsumo;
	}

	public void setIdClienteSemConsumo(Long idClienteSemConsumo) {

		this.idClienteSemConsumo = idClienteSemConsumo;
	}

	public String getMetroCubicoMediaDiariaFormatado() {

		String retorno = " - ";
		if(metroCubicoMediaDiaria != null) {
			retorno = Util.formatarValorNumerico(metroCubicoMediaDiaria.intValue());
		}

		return retorno;
	}

	public String getMetroCubicoPeriodoFormatado() {

		String retorno = " - ";
		if(metroCubicoPeriodo != null) {
			retorno = Util.formatarValorNumerico(metroCubicoPeriodo.intValue());
		}

		return retorno;
	}

	public String getPercentualVolumesFormatado() {

		String retorno = " - ";
		if(percentualVolume != null) {
			retorno = Util.converterCampoValorDecimalParaString("", percentualVolume, Constantes.LOCALE_PADRAO, 2);
		}

		return retorno;
	}

	public String getTarifaMediaGasFormatado() {

		String retorno = " - ";
		if(tarifaMediaGas != null) {
			retorno = Util.converterCampoValorDecimalParaString("", tarifaMediaGas, Constantes.LOCALE_PADRAO);
		}

		return retorno;
	}

	public String getPercentualFaturamentoGasFormatado() {

		String retorno = " - ";
		if(percentualFaturamentoGas != null) {
			retorno = Util.converterCampoValorDecimalParaString("", percentualFaturamentoGas, Constantes.LOCALE_PADRAO, 2);
		}

		return retorno;
	}

	public String getTarifaMediaMargemFormatado() {

		String retorno = " - ";
		if(tarifaMediaMargem != null) {
			retorno = Util.converterCampoValorDecimalParaString("", tarifaMediaMargem, Constantes.LOCALE_PADRAO);
		}

		return retorno;
	}

	public String getPercentualFaturamentoMargemFormatado() {

		String retorno = " - ";
		if(percentualFaturamentoMargem != null) {
			retorno = Util.converterCampoValorDecimalParaString("", percentualFaturamentoMargem, Constantes.LOCALE_PADRAO, 2);
		}

		return retorno;
	}

	public String getTarifaMediaTransporteFormatado() {

		String retorno = " - ";
		if(tarifaMediaTransporte != null) {
			retorno = Util.converterCampoValorDecimalParaString("", tarifaMediaTransporte, Constantes.LOCALE_PADRAO);
		}

		return retorno;
	}

	public String getPercentualFaturamentoTransporteFormatado() {

		String retorno = " - ";
		if(percentualFaturamentoTransporte != null) {
			retorno = Util.converterCampoValorDecimalParaString("", percentualFaturamentoTransporte, Constantes.LOCALE_PADRAO, 2);
		}

		return retorno;
	}

	public String getPercentualVolumeFormatado() {

		String retorno = " - ";
		if(percentualVolume != null) {
			retorno = Util.converterCampoValorDecimalParaString("", percentualVolume, Constantes.LOCALE_PADRAO, 2);
		}

		return retorno;
	}

	public String getTotalTarifaMediaMargemFormatado() {

		String retorno = " - ";
		if(totalTarifaMediaMargem != null) {
			retorno = Util.converterCampoValorDecimalParaString("", totalTarifaMediaMargem, Constantes.LOCALE_PADRAO);
		}

		return retorno;
	}

	public String getTotalPercentualFaturamentoMargemFormatado() {

		String retorno = " - ";
		if(totalPercentualFaturamentoMargem != null) {
			retorno = Util.converterCampoValorDecimalParaString("", totalPercentualFaturamentoMargem, Constantes.LOCALE_PADRAO, 2);
		}

		return retorno;
	}

	public String getTotalTarifaMediaTransporteFormatado() {

		String retorno = " - ";
		if(totalTarifaMediaTransporte != null) {
			retorno = Util.converterCampoValorDecimalParaString("", totalTarifaMediaTransporte, Constantes.LOCALE_PADRAO);
		}

		return retorno;
	}

	public String getTotalPercentualFaturamentoTransporteFormatado() {

		String retorno = " - ";
		if(totalPercentualFaturamentoTransporte != null) {
			retorno = Util.converterCampoValorDecimalParaString("", totalPercentualFaturamentoTransporte, Constantes.LOCALE_PADRAO, 2);
		}

		return retorno;
	}

	public String getNomeCliente() {

		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	public BigDecimal getVolumeMedido() {
		return volumeMedido;
	}

	public void setVolumeMedido(BigDecimal volumeMedido) {
		this.volumeMedido = volumeMedido;
	}

	public BigDecimal getVolumeFaturado() {
		return volumeFaturado;
	}

	public void setVolumeFaturado(BigDecimal volumeFaturado) {
		this.volumeFaturado = volumeFaturado;
	}

	public BigDecimal getMediaVolume() {
		return mediaVolume;
	}

	public void setMediaVolume(BigDecimal mediaVolume) {
		this.mediaVolume = mediaVolume;
	}

	public BigDecimal getTarifaMedia() {
		return tarifaMedia;
	}

	public void setTarifaMedia(BigDecimal tarifaMedia) {
		this.tarifaMedia = tarifaMedia;
	}

	public String getRamoAtividade() {
		return ramoAtividade;
	}

	public void setRamoAtividade(String ramoAtividade) {
		this.ramoAtividade = ramoAtividade;
	}

	public String getSegmento() {
		return segmento;
	}

	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	public String getClienteDesde() {
		return clienteDesde;
	}

	public void setClienteDesde(String clienteDesde) {
		this.clienteDesde = clienteDesde;
	}

	public BigDecimal getTotalVolumeFaturado() {
		return totalVolumeFaturado;
	}

	public void setTotalVolumeFaturado(BigDecimal totalVolumeFaturado) {
		this.totalVolumeFaturado = totalVolumeFaturado;
	}

	public Integer getQuantidadeDiasMes() {
		return quantidadeDiasMes;
	}

	public void setQuantidadeDiasMes(Integer quantidadeDiasMes) {
		this.quantidadeDiasMes = quantidadeDiasMes;
	}

	public BigDecimal getTotalValorFaturado() {
		return totalValorFaturado;
	}

	public void setTotalValorFaturado(BigDecimal totalValorFaturado) {
		this.totalValorFaturado = totalValorFaturado;
	}

	public BigDecimal getValorFaturado() {
		return valorFaturado;
	}

	public void setValorFaturado(BigDecimal valorFaturado) {
		this.valorFaturado = valorFaturado;
	}

	public String getTipoTarifa() {
		return tipoTarifa;
	}

	public void setTipoTarifa(String tipoTarifa) {
		this.tipoTarifa = tipoTarifa;
	}

	public String getPontoConsumo() {
		return pontoConsumo;
	}

	public void setPontoConsumo(String pontoConsumo) {
		this.pontoConsumo = pontoConsumo;
	}

	public String getImovel() {
		return imovel;
	}

	public void setImovel(String imovel) {
		this.imovel = imovel;
	}

	public String getCodigoPontoConsumo() {
		return codigoPontoConsumo;
	}

	public void setCodigoPontoConsumo(String codigoPontoConsumo) {
		this.codigoPontoConsumo = codigoPontoConsumo;
	}

}
