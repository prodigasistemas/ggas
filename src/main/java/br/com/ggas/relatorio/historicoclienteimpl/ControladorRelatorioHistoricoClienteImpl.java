/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.relatorio.historicoclienteimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.DebitosACobrar;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.arrecadacao.recebimento.Recebimento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.relatorio.ControladorRelatorioHistoricoCliente;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.FormatoImpressao;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.web.relatorio.clientehistorico.ClienteHistoricoVO;
import br.com.ggas.web.relatorio.clientehistorico.FaturaHistoricoVO;
import br.com.ggas.web.relatorio.clientehistorico.PontoConsumoHistoricoVO;

public class ControladorRelatorioHistoricoClienteImpl extends ControladorNegocioImpl implements ControladorRelatorioHistoricoCliente {

	private static final String ERRO_NEGOCIO_RELATORIO_SEM_DADOS = "ERRO_NEGOCIO_RELATORIO_SEM_DADOS";

	private static final String ID_CLIENTE = "idCliente";

	private static final String DATA_EMISSAO_INICIAL = "dataEmissaoInicial";

	private static final String DATA_EMISSAO_FINAL = "dataEmissaoFinal";

	private static final String DATA_VENCIMENTO_INICIAL = "dataVencimentoInicial";

	private static final String DATA_VENCIMENTO_FINAL = "dataVencimentoFinal";

	private static final String SITUACAO = "situacao";


	private static final String RELATORIO_HISTORICO_CLIENTE = "relatorioHistoricoCliente.jasper";

	private final ControladorCliente controladorCliente = (ControladorCliente) ServiceLocator.getInstancia().getBeanPorID(
					ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);

	private final ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getBeanPorID(
					ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

	private final ControladorArrecadacao controladorArrecadacao = (ControladorArrecadacao) ServiceLocator.getInstancia().getBeanPorID(
					ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO);

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	@Override
	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	@Override
	public Class<?> getClasseCliente() {

		return ServiceLocator.getInstancia().getClassPorID(Cliente.BEAN_ID_CLIENTE);
	}

	public Class<?> getClasseEntidadeDocumentoFiscal() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoFiscal.BEAN_ID_DOCUMENTO_FISCAL);
	}

	public Class<?> getClasseEntidadeDocumentoTipo() {

		return ServiceLocator.getInstancia().getClassPorID(TipoDocumento.BEAN_ID_TIPO_DOCUMENTO);
	}

	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	public Class<?> getClasseEntidadeFaturaGeral() {

		return ServiceLocator.getInstancia().getClassPorID(FaturaGeral.BEAN_ID_FATURA_GERAL);
	}

	public Class<?> getClasseEntidadeRecebimento() {

		return ServiceLocator.getInstancia().getClassPorID(Recebimento.BEAN_ID_RECEBIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/**
	 * Listar ponto consumo historico cliente.
	 *
	 * @param filtro
	 *            the filtro
	 * @return the cliente historico vo
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings({})
	public ClienteHistoricoVO listarPontoConsumoHistoricoCliente(Map<String, Object> filtro) throws GGASException {

		ClienteHistoricoVO resultado = new ClienteHistoricoVO();

		if(filtro != null) {
			Long idCliente = (Long) filtro.get(ID_CLIENTE);
			Cliente cliente = controladorCliente.obterCliente(idCliente, Cliente.PROPRIEDADES_LAZY);
			resultado.setNomeCliente(cliente.getNome());
			Collection<PontoConsumo> listaPontoConsumo = controladorPontoConsumo.listarPontoConsumoPorChaveCliente(idCliente);
			Collection<PontoConsumoHistoricoVO> listaPontoConsumoHistoricoVO = new ArrayList<PontoConsumoHistoricoVO>();

			for (PontoConsumo pontoConsumo : listaPontoConsumo) {

				PontoConsumoHistoricoVO pontoConsumoHistoricoVO = popularPontoConsumoHistoricoVO(filtro, pontoConsumo);
				if(pontoConsumoHistoricoVO != null) {
					listaPontoConsumoHistoricoVO.add(pontoConsumoHistoricoVO);
				}
			}
			if(!listaPontoConsumoHistoricoVO.isEmpty()) {
				validarDados(listaPontoConsumoHistoricoVO);
				resultado.setListaPontoConsumoHistoricoVO(listaPontoConsumoHistoricoVO);
			} else {
				throw new GGASException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
			}
		}

		return resultado;
	}

	/**
	 * Popular ponto consumo historico vo.
	 *
	 * @param filtro
	 *            the filtro
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the ponto consumo historico vo
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private PontoConsumoHistoricoVO popularPontoConsumoHistoricoVO(Map<String, Object> filtro, PontoConsumo pontoConsumo)
					throws GGASException {

		PontoConsumoHistoricoVO pontoConsumoHistoricoVO = new PontoConsumoHistoricoVO();
		pontoConsumoHistoricoVO.setDescricaoPontoConsumo(pontoConsumo.getDescricao());

		Collection<FaturaHistoricoVO> listaFaturaHistoricoVO = listarFaturaHistoricoVO(filtro, pontoConsumo);
		if(!listaFaturaHistoricoVO.isEmpty()) {
			pontoConsumoHistoricoVO.setListaFaturaHistoricoVO(listaFaturaHistoricoVO);
		} else {
			pontoConsumoHistoricoVO = null;
		}

		return pontoConsumoHistoricoVO;
	}

	/**
	 * Listar fatura historico vo.
	 *
	 * @param filtro
	 *            the filtro
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Collection<FaturaHistoricoVO> listarFaturaHistoricoVO(Map<String, Object> filtro, PontoConsumo pontoConsumo)
					throws GGASException {

		Long idCliente = (Long) filtro.get(ID_CLIENTE);

		Collection<FaturaHistoricoVO> listaFaturaHistoricoVO = new ArrayList<FaturaHistoricoVO>();

		Collection<Fatura> listaFatura = listarFaturaPorPontoConsumoCliente(filtro, pontoConsumo);
		for (Fatura fatura : listaFatura) {

			Collection<DocumentoFiscal> listaDocumentoFiscal = listarDocumentoFiscal(idCliente, fatura);

			for (DocumentoFiscal documentoFiscal : listaDocumentoFiscal) {

				FaturaHistoricoVO faturaHistoricoVO = new FaturaHistoricoVO();

				faturaHistoricoVO.setAtributosFaturaDocumentoFiscal(fatura, documentoFiscal);

				Collection<Recebimento> listaRecebimento = listarRecebimentoPorCliente(idCliente, documentoFiscal);

				for (Recebimento recebimento : listaRecebimento) {

					DebitosACobrar debitosCobrarVO = controladorArrecadacao.calcularAcrescimentoImpontualidade(fatura,
									recebimento.getDataRecebimento(), recebimento.getValorRecebimento(), true, true);

					faturaHistoricoVO.setAtributosFaturaRecebimento(fatura, recebimento, debitosCobrarVO);

				}

				listaFaturaHistoricoVO.add(faturaHistoricoVO);
			}

		}
		return listaFaturaHistoricoVO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.relatorio.ControladorRelatorioHistoricoCliente#gerarRelatorio(java.util.Map)
	 */
	@Override
	public byte[] gerarRelatorio(Map<String, Object> filtro) throws GGASException {

		FormatoImpressao formatoImpressao = (FormatoImpressao) filtro.get("formatoImpressao");
		Collection<Object> collRelatorio = new ArrayList<Object>();

		ClienteHistoricoVO clienteVO = listarPontoConsumoHistoricoCliente(filtro);

		ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(
						ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		if(controladorEmpresa.obterEmpresaPrincipal().getLogoEmpresa() != null) {
			filtro.put("imagem",
							Constantes.URL_LOGOMARCA_EMPRESA
											+ String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
		}

		collRelatorio.add(clienteVO);

		return RelatorioUtil.gerarRelatorio(collRelatorio, filtro, RELATORIO_HISTORICO_CLIENTE, formatoImpressao);
	}

	/**
	 * Validar dados.
	 *
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDados(Collection<PontoConsumoHistoricoVO> dados) throws NegocioException {

		if(dados == null || dados.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CLIENTE_NAO_POSSUI_ESSE_CODIGO_PONTO_CONSUMO, true);
		}
	}

	/**
	 * Listar fatura por ponto consumo cliente.
	 *
	 * @param filtro
	 *            the filtro
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the collection
	 */
	private Collection<Fatura> listarFaturaPorPontoConsumoCliente(Map<String, Object> filtro, PontoConsumo pontoConsumo) {

		filtro.get("idCliente");
		filtro.get("chavesPrimarias");
		Date dataHoje = new Date();

		Criteria criteria = this.createCriteria(Fatura.class, "fatu");

		criteria.add(Restrictions.eq("fatu.pontoConsumo.chavePrimaria", pontoConsumo.getChavePrimaria()));

		Date dataEmissaoInicial = (Date) filtro.get(DATA_EMISSAO_INICIAL);
		Date dataEmissaoFinal = (Date) filtro.get(DATA_EMISSAO_FINAL);
		if (dataEmissaoInicial != null && !dataEmissaoInicial.toString().isEmpty() && dataHoje.after(dataEmissaoInicial)
						&& dataEmissaoFinal != null && !dataEmissaoFinal.toString().isEmpty()) {

			if (dataHoje.after(dataEmissaoFinal)) {
				criteria.add(Restrictions.between("fatu.dataEmissao", dataEmissaoInicial, dataEmissaoFinal));
			} else {
				criteria.add(Restrictions.between("fatu.dataEmissao", dataEmissaoInicial, dataHoje));
			}
			criteria.add(Restrictions.ge("fatu.dataEmissao", dataEmissaoInicial));
		}
		if(dataEmissaoFinal != null && !dataEmissaoFinal.toString().isEmpty() && dataHoje.after(dataEmissaoFinal)) {

			criteria.add(Restrictions.lt("fatu.dataEmissao", dataEmissaoFinal));
		}

		Date dataVencimentoInicial = (Date) filtro.get(DATA_VENCIMENTO_INICIAL);
		Date dataVencimentoFinal = (Date) filtro.get(DATA_VENCIMENTO_FINAL);
		if(dataVencimentoInicial != null && !dataVencimentoInicial.toString().isEmpty()) {
			if(dataVencimentoFinal != null && !dataVencimentoFinal.toString().isEmpty()) {
				criteria.add(Restrictions.between("fatu.dataVencimento", dataVencimentoInicial, dataVencimentoFinal));
			} else {
				criteria.add(Restrictions.gt("fatu.dataVencimento", dataVencimentoInicial));
			}
		} else if(dataVencimentoFinal != null && !dataVencimentoFinal.toString().isEmpty() && dataHoje.after(dataVencimentoFinal)) {

			criteria.add(Restrictions.le("fatu.dataVencimento", dataVencimentoFinal));
		}

		String situacao = (String) filtro.get(SITUACAO);
		if(situacao != null && !situacao.isEmpty()) {
			if("true".equalsIgnoreCase(situacao)) {
				criteria.add(Restrictions.ge("fatu.dataVencimento", dataHoje));
			} else if("false".equalsIgnoreCase(situacao)) {
				criteria.add(Restrictions.lt("fatu.dataVencimento", dataHoje));
			}
		}

		return criteria.list();
	}

	/**
	 * Listar documento fiscal.
	 *
	 * @param idCliente
	 *            the id cliente
	 * @param fatura
	 *            the fatura
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<DocumentoFiscal> listarDocumentoFiscal(Long idCliente, Fatura fatura) {

		Criteria criteria = this.createCriteria(DocumentoFiscal.class, "dofi");
		criteria.createCriteria("dofi.fatura", "fatu");
		criteria.add(Restrictions.eq("fatu.chavePrimaria", fatura.getChavePrimaria()));
		criteria.add(Restrictions.eq("fatu.cliente.chavePrimaria", idCliente));

		return criteria.list();
	}

	/**
	 * Listar recebimento por cliente.
	 *
	 * @param idCliente
	 *            the id cliente
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<Recebimento> listarRecebimentoPorCliente(Long idCliente, DocumentoFiscal documentoFiscal) {

		Criteria criteria = this.createCriteria(Recebimento.class, "rece");
		criteria.add(Restrictions.eq("rece.cliente.chavePrimaria", idCliente));
		if(documentoFiscal.getFatura() != null) {
			criteria.createCriteria("rece.faturaGeral", "fage");
			criteria.createCriteria("fage.faturaAtual", "fatu");
			criteria.add(Restrictions.eq("fatu.chavePrimaria", documentoFiscal.getFatura().getChavePrimaria()));
		}

		return criteria.list();
	}

}
