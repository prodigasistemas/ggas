package br.com.ggas.relatorio;

import java.io.Serializable;

public class ChamadoResumoVO implements Serializable {
	
	private static final long serialVersionUID = -7397125526658350766L;
	
	private String tipoChamado;
	private String assuntoChamado;
	private Integer totalFinalizado;
	private Integer totalRascunho;
	private Integer totalAberto;
	private Integer totalPendente;
	private Integer totalEmAndamento;
	private Integer total;
	private String statusChamado;
	
	public String getTipoChamado() {
		return tipoChamado;
	}
	public void setTipoChamado(String tipoChamado) {
		this.tipoChamado = tipoChamado;
	}
	public String getAssuntoChamado() {
		return assuntoChamado;
	}
	public void setAssuntoChamado(String assuntoChamado) {
		this.assuntoChamado = assuntoChamado;
	}
	public Integer getTotalFinalizado() {
		return totalFinalizado;
	}
	public void setTotalFinalizado(Integer totalFinalizado) {
		this.totalFinalizado = totalFinalizado;
	}
	public Integer getTotalRascunho() {
		return totalRascunho;
	}
	public void setTotalRascunho(Integer totalRascunho) {
		this.totalRascunho = totalRascunho;
	}
	public Integer getTotalAberto() {
		return totalAberto;
	}
	public void setTotalAberto(Integer totalAberto) {
		this.totalAberto = totalAberto;
	}
	public Integer getTotalPendente() {
		return totalPendente;
	}
	public void setTotalPendente(Integer totalPendente) {
		this.totalPendente = totalPendente;
	}
	public Integer getTotalEmAndamento() {
		return totalEmAndamento;
	}
	public void setTotalEmAndamento(Integer totalEmAndamento) {
		this.totalEmAndamento = totalEmAndamento;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public String getStatusChamado() {
		return statusChamado;
	}
	public void setStatusChamado(String statusChamado) {
		this.statusChamado = statusChamado;
	}

}
