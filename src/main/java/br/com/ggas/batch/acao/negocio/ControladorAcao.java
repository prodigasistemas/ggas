/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *30/12/2013
 * vpessoa
 * 15:52:51
 */

package br.com.ggas.batch.acao.negocio;

import java.util.Collection;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.acao.dominio.Acao;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Controlador Acao
 * 
 * @author vpessoa
 */
public interface ControladorAcao {

	/**
	 * Obter acoes.
	 * 
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> obterAcoes(boolean habilitado) throws NegocioException;

	/**
	 * Obter acao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the acao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Acao obterAcao(Long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Inserir.
	 * 
	 * @param acao
	 *            the acao
	 * @return the acao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Acao inserir(Acao acao) throws NegocioException;

	/**
	 * Consultar acao.
	 * 
	 * @param acao
	 *            the acao
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> consultarAcao(Acao acao, boolean habilitado) throws NegocioException;

	/**
	 * Atualizar.
	 * 
	 * @param acao
	 *            the acao
	 * @return the acao
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	Acao atualizar(Acao acao) throws NegocioException, ConcorrenciaException;

	/**
	 * Remover acao.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerAcao(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

}
