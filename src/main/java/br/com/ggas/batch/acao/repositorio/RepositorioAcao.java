/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *30/12/2013
 * vpessoa
 * 15:58:14
 */

package br.com.ggas.batch.acao.repositorio;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.batch.acao.dominio.Acao;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Util;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de 
 * 
 * @author vpessoa
 */
@Repository
public class RepositorioAcao extends RepositorioGenerico {

	public static final String TIPO_DOCUMENTO = "tipoDocumento";

	/**
	 * Instantiates a new repositorio acao.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioAcao(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new Acao();
	}

	@Override
	public Class<Acao> getClasseEntidade() {

		return Acao.class;

	}

	/**
	 * Consultar acao.
	 * 
	 * @param acao
	 *            the acao
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("squid:S1192")
	public Collection<EntidadeNegocio> consultarAcao(Acao acao, Boolean habilitado) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode(TIPO_DOCUMENTO, FetchMode.JOIN);
		criteria.setFetchMode("segmentoPesquisa", FetchMode.JOIN);

		if(acao.getDescricao() != null && !acao.getDescricao().isEmpty()) {
			criteria.add(Restrictions.ilike("descricao", Util.formatarTextoConsulta(acao.getDescricao())));
		}
		if(acao.getTipoDocumento() != null) {
			criteria.createAlias(TIPO_DOCUMENTO, TIPO_DOCUMENTO);
			criteria.add(Restrictions.eq("tipoDocumento.chavePrimaria", acao.getTipoDocumento().getChavePrimaria()));
		}
		if(habilitado != null) {
			criteria.add(Restrictions.eq("habilitado", habilitado));
		}

		return criteria.list();

	}
}
