/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *23/12/2013
 * vpessoa
 * 16:34:01
 */

package br.com.ggas.batch.acao.dominio;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.questionario.dominio.Questionario;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de 
 * 
 * @author vpessoa
 */
public class Acao extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * 
	 */
	private static final long serialVersionUID = -6837313650509303522L;

	private String descricao;

	private Acao acaoPrecedente;

	private EntidadeConteudo tipoAcao;

	private Integer numeroDiasValidade;

	private Integer numeroDiasVencimento;

	private EntidadeConteudo tipoNumeroDiasVencimento;

	private EntidadeConteudo tipoNumeroDiasValidade;

	private TipoDocumento tipoDocumento;

	private Boolean indicadorGerarAutorizacaoServico;

	private ServicoTipo servicoTipo;

	private Equipe equipe;

	private Boolean considerarDebitosAVencer;

	private Boolean bloquearDocumentosPagaveis;

	private Boolean indicadorGerarChamadoPesquisa;

	private ChamadoAssunto chamadoAssuntoPesquisa;

	private UnidadeOrganizacional unidadeOrganizacionalPesquisa;

	private Funcionario funcionarioPesquisa;

	private ServicoTipo servicoTipoPesquisa;

	private Operacao operacao;

	private Questionario questionario;

	@Transient
	private Modulo modulo;

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public Acao getAcaoPrecedente() {

		return acaoPrecedente;
	}

	public void setAcaoPrecedente(Acao acaoPrecedente) {

		this.acaoPrecedente = acaoPrecedente;
	}

	public EntidadeConteudo getTipoAcao() {

		return tipoAcao;
	}

	public void setTipoAcao(EntidadeConteudo tipoAcao) {

		this.tipoAcao = tipoAcao;
	}

	public Integer getNumeroDiasValidade() {

		return numeroDiasValidade;
	}

	public void setNumeroDiasValidade(Integer numeroDiasValidade) {

		this.numeroDiasValidade = numeroDiasValidade;
	}

	public Integer getNumeroDiasVencimento() {

		return numeroDiasVencimento;
	}

	public void setNumeroDiasVencimento(Integer numeroDiasVencimento) {

		this.numeroDiasVencimento = numeroDiasVencimento;
	}

	public EntidadeConteudo getTipoNumeroDiasVencimento() {

		return tipoNumeroDiasVencimento;
	}

	public void setTipoNumeroDiasVencimento(EntidadeConteudo tipoNumeroDiasVencimento) {

		this.tipoNumeroDiasVencimento = tipoNumeroDiasVencimento;
	}

	public EntidadeConteudo getTipoNumeroDiasValidade() {

		return tipoNumeroDiasValidade;
	}

	public void setTipoNumeroDiasValidade(EntidadeConteudo tipoNumeroDiasValidade) {

		this.tipoNumeroDiasValidade = tipoNumeroDiasValidade;
	}

	public TipoDocumento getTipoDocumento() {

		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {

		this.tipoDocumento = tipoDocumento;
	}

	public ServicoTipo getServicoTipo() {

		return servicoTipo;
	}

	public void setServicoTipo(ServicoTipo servicoTipo) {

		this.servicoTipo = servicoTipo;
	}

	public Equipe getEquipe() {

		return equipe;
	}

	public void setEquipe(Equipe equipe) {

		this.equipe = equipe;
	}

	public ChamadoAssunto getChamadoAssuntoPesquisa() {

		return chamadoAssuntoPesquisa;
	}

	public void setChamadoAssuntoPesquisa(ChamadoAssunto chamadoAssuntoPesquisa) {

		this.chamadoAssuntoPesquisa = chamadoAssuntoPesquisa;
	}

	public UnidadeOrganizacional getUnidadeOrganizacionalPesquisa() {

		return unidadeOrganizacionalPesquisa;
	}

	public void setUnidadeOrganizacionalPesquisa(UnidadeOrganizacional unidadeOrganizacionalPesquisa) {

		this.unidadeOrganizacionalPesquisa = unidadeOrganizacionalPesquisa;
	}

	public Funcionario getFuncionarioPesquisa() {

		return funcionarioPesquisa;
	}

	public void setFuncionarioPesquisa(Funcionario funcionarioPesquisa) {

		this.funcionarioPesquisa = funcionarioPesquisa;
	}

	public ServicoTipo getServicoTipoPesquisa() {

		return servicoTipoPesquisa;
	}

	public void setServicoTipoPesquisa(ServicoTipo servicoTipoPesquisa) {

		this.servicoTipoPesquisa = servicoTipoPesquisa;
	}

	public Operacao getOperacao() {

		return operacao;
	}

	public void setOperacao(Operacao operacao) {

		this.operacao = operacao;
	}

	public Boolean getIndicadorGerarAutorizacaoServico() {

		return indicadorGerarAutorizacaoServico;
	}

	public void setIndicadorGerarAutorizacaoServico(Boolean indicadorGerarAutorizacaoServico) {

		this.indicadorGerarAutorizacaoServico = indicadorGerarAutorizacaoServico;
	}

	public Boolean getConsiderarDebitosAVencer() {

		return considerarDebitosAVencer;
	}

	public void setConsiderarDebitosAVencer(Boolean considerarDebitosAVencer) {

		this.considerarDebitosAVencer = considerarDebitosAVencer;
	}

	public Boolean getBloquearDocumentosPagaveis() {

		return bloquearDocumentosPagaveis;
	}

	public void setBloquearDocumentosPagaveis(Boolean bloquearDocumentosPagaveis) {

		this.bloquearDocumentosPagaveis = bloquearDocumentosPagaveis;
	}

	public Boolean getIndicadorGerarChamadoPesquisa() {

		return indicadorGerarChamadoPesquisa;
	}

	public void setIndicadorGerarChamadoPesquisa(Boolean indicadorGerarChamadoPesquisa) {

		this.indicadorGerarChamadoPesquisa = indicadorGerarChamadoPesquisa;
	}

	public Questionario getQuestionario() {

		return questionario;
	}

	public void setQuestionario(Questionario questionario) {

		this.questionario = questionario;
	}

	public Modulo getModulo() {

		if(operacao != null) {
			return operacao.getModulo();
		} else {
			return modulo;
		}
	}

	public void setModulo(Modulo modulo) {

		this.modulo = modulo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao) || descricao.trim().length() == 0) {
			stringBuilder.append(Constantes.CAMPO_DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(modulo == null) {
			stringBuilder.append(Constantes.CAMPO_MODULO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(operacao == null) {
			stringBuilder.append(Constantes.CAMPO_OPERACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(indicadorGerarAutorizacaoServico) {
			if(servicoTipo == null) {
				stringBuilder.append(Constantes.TIPO_SERVICO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(equipe == null) {
				stringBuilder.append(Constantes.EQUIPE);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}
		if(indicadorGerarChamadoPesquisa) {
			if(questionario == null) {
				stringBuilder.append(Constantes.QUESTIONARIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(chamadoAssuntoPesquisa == null) {
				stringBuilder.append(Constantes.CAMPO_CHAMADO_ASSUNTO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(unidadeOrganizacionalPesquisa == null) {
				stringBuilder.append(Constantes.CAMPO_UNIDADE_ORGANIZACIONAL);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if(servicoTipoPesquisa == null) {
				stringBuilder.append(Constantes.TIPO_SERVICO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {

			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));

		}

		return erros;
	}
}
