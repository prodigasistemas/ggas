/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *30/12/2013
 * vpessoa
 * 15:53:41
 */

package br.com.ggas.batch.acao.negocio.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.acao.dominio.Acao;
import br.com.ggas.batch.acao.negocio.ControladorAcao;
import br.com.ggas.batch.acao.repositorio.RepositorioAcao;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de 
 * 
 * @author vpessoa
 */

@Service("controladorAcao")
@Transactional
public class ControladorAcaoImpl implements ControladorAcao {

	@Autowired
	private RepositorioAcao repositorioAcao;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.acao.negocio.ControladorAcao#obterAcoes(boolean)
	 */
	@Override
	public Collection<EntidadeNegocio> obterAcoes(boolean habilitado) throws NegocioException {

		return repositorioAcao.obterTodas(habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.acao.negocio.ControladorAcao#obterAcao(java.lang.Long, java.lang.String[])
	 */
	@Override
	public Acao obterAcao(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Acao) repositorioAcao.obter(chavePrimaria, propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.acao.negocio.ControladorAcao#inserir(br.com.ggas.batch.acao.dominio.Acao)
	 */
	@Override
	public Acao inserir(Acao acao) throws NegocioException {

		repositorioAcao.inserir(acao);

		return acao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.acao.negocio.ControladorAcao#consultarAcao(br.com.ggas.batch.acao.dominio.Acao, boolean)
	 */
	@Override
	public Collection<EntidadeNegocio> consultarAcao(Acao acao, boolean habilitado) throws NegocioException {

		return repositorioAcao.consultarAcao(acao, habilitado);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.acao.negocio.ControladorAcao#atualizar(br.com.ggas.batch.acao.dominio.Acao)
	 */
	@Override
	public Acao atualizar(Acao acao) throws NegocioException, ConcorrenciaException {

		repositorioAcao.atualizar(acao);

		return acao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.acao.negocio.ControladorAcao#removerAcao(java.lang.Long[], br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerAcao(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.repositorioAcao.remover(chavesPrimarias, dadosAuditoria);
	}

}
