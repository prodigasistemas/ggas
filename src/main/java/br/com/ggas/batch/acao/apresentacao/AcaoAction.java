/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *30/12/2013
 * vpessoa
 * 11:15:44
 */

package br.com.ggas.batch.acao.apresentacao;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.arrecadacao.ControladorTipoDocumento;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.questionario.negocio.ControladorQuestionario;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.acao.dominio.Acao;
import br.com.ggas.batch.acao.negocio.ControladorAcao;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de AcaoAction
 * 
 * @author vpessoa
 */
@Controller
public class AcaoAction extends GenericAction {

	private static final String OPERACOES_BATCH = "operacoesBatch";

	private static final Logger LOG = Logger.getLogger(AcaoAction.class);

	@Autowired
	private ControladorAcao controladorAcao;

	@Autowired
	@Qualifier("controladorTipoDocumento")
	private ControladorTipoDocumento controladorTipoDocumento;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier("controladorModulo")
	private ControladorModulo controladorModulo;

	@Autowired
	private ControladorEquipe controladorEquipe;

	@Autowired
	private ControladorServicoTipo controladorServicoTipo;

	@Autowired
	private ControladorChamadoTipo controladorChamadoTipo;

	@Autowired
	@Qualifier("controladorUnidadeOrganizacional")
	private ControladorUnidadeOrganizacional controladorUnidadeOrganizacional;

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	@Autowired
	private ControladorQuestionario controladorQuestionario;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;

	/**
	 * Exibir inclusao acao.
	 * 
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirInclusaoAcao")
	public ModelAndView exibirInclusaoAcao() throws NegocioException {

		ModelAndView model = new ModelAndView("exibirInclusaoAcao");
		Acao acao = new Acao();
		acao.setIndicadorGerarAutorizacaoServico(Boolean.FALSE);
		acao.setIndicadorGerarChamadoPesquisa(Boolean.FALSE);
		acao.setConsiderarDebitosAVencer(Boolean.FALSE);
		acao.setBloquearDocumentosPagaveis(Boolean.FALSE);
		ConstanteSistema constanteSistema = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_OPERACAO_SISTEMA_GERAR_PESQ_SATISF);

		model.addObject("operacaoPesquisa", constanteSistema.getValor());
		model.addObject("acao", acao);
		carregarCombos(model, acao.getModulo());
		model.addObject("fluxoInclusao", Boolean.TRUE);
		return model;
	}

	/**
	 * Incluir acao.
	 * 
	 * @param acao
	 *            the acao
	 * @param result
	 *            the result
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("incluirAcao")
	public ModelAndView incluirAcao(@ModelAttribute("Acao") Acao acao, BindingResult result) throws NegocioException {
		String constante;
		ModelAndView model = new ModelAndView("forward:/pesquisarAcao");
		try {
			if (acao.getChavePrimaria() == 0) {
				controladorAcao.inserir(acao);
				constante = Constantes.SUCESSO_ENTIDADE_INCLUIDA;
			} else {
				controladorAcao.atualizar(acao);
				constante = Constantes.SUCESSO_ENTIDADE_ALTERADA;
			}
			mensagemSucesso(model, constante, GenericAction.obterMensagem(Constantes.ACAO));
		} catch (GGASException e) {
			model = new ModelAndView("exibirInclusaoAcao");
			carregarCombos(model, acao.getModulo());
			model.addObject("acao", acao);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Exibir pesquisa acao.
	 * 
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirPesquisaAcao")
	public ModelAndView exibirPesquisaAcao() throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaAcao");
		model.addObject("listaTipoDocumento", controladorTipoDocumento.obterTodas(Boolean.TRUE));
		return model;
	}

	/**
	 * Pesquisar acao.
	 * 
	 * @param acao
	 *            the acao
	 * @param result
	 *            the result
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarAcao")
	public ModelAndView pesquisarAcao(@ModelAttribute("Acao") Acao acao, BindingResult result) throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaAcao");
		try {
			carregarCombos(model, acao.getModulo());
			model.addObject("acao", acao);
			model.addObject("listaAcao", controladorAcao.consultarAcao(acao, Boolean.TRUE));
		} catch (GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Exibir detalhamento acao.
	 * 
	 * @param chavesPrimaria
	 *            the chaves primaria
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirDetalhamentoAcao")
	public ModelAndView exibirDetalhamentoAcao(@RequestParam("chavesPrimaria") Long chavesPrimaria) throws GGASException {

		Long chavePrimaria = chavesPrimaria;

		ModelAndView model = new ModelAndView("exibirDetalhamentoAcao");
		model.addObject("acao", controladorAcao.obterAcao(chavePrimaria, "acaoPrecedente", "tipoAcao", "tipoNumeroDiasVencimento",
						"tipoNumeroDiasValidade", "tipoDocumento", "servicoTipo", "equipe", "operacao",
						"chamadoAssuntoPesquisa.chamadoTipo", "unidadeOrganizacionalPesquisa", "servicoTipoPesquisa", "questionario"));

		return model;
	}

	/**
	 * Exibir alteracao acao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirAlteracaoAcao")
	public ModelAndView exibirAlteracaoAcao(@RequestParam("chavesPrimaria") Long chavePrimaria, HttpServletRequest request)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirAlteracaoAcao");

		Acao acao = controladorAcao.obterAcao(chavePrimaria, "acaoPrecedente", "tipoAcao", "tipoNumeroDiasVencimento",
						"tipoNumeroDiasValidade", "tipoDocumento", "servicoTipo", "equipe", "operacao",
						"chamadoAssuntoPesquisa.chamadoTipo", "unidadeOrganizacionalPesquisa", "servicoTipoPesquisa", "questionario");
		ConstanteSistema constanteSistema = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_OPERACAO_SISTEMA_GERAR_PESQ_SATISF);

		model.addObject("operacaoPesquisa", constanteSistema.getValor());
		model.addObject("acao", acao);
		model.addObject(OPERACOES_BATCH,
						controladorModulo.consultarOperacoesBatchPorModulo(acao.getOperacao().getModulo().getChavePrimaria()));
		carregarCombos(model, acao.getModulo());
		model.addObject("fluxoAlteracao", Boolean.TRUE);

		return model;
	}

	/**
	 * Carregar operacoes.
	 * 
	 * @param chaveModulo
	 *            the chave modulo
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("carregarOperacoes")
	public ModelAndView carregarOperacoes(@RequestParam("chaveModulo") Long chaveModulo) throws NegocioException {

		ModelAndView model = new ModelAndView("comboOperacoes");
		model.addObject(OPERACOES_BATCH, controladorModulo.consultarOperacoesBatchPorModulo(chaveModulo));
		return model;
	}

	/**
	 * Remover acao.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("removerAcao")
	public ModelAndView removerAcao(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws NegocioException {

		ModelAndView model = new ModelAndView("forward:/pesquisarAcao");
		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());
			controladorAcao.removerAcao(chavesPrimarias, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, "Ação");
		} catch (DataIntegrityViolationException e) {
			LOG.error(e.getMessage(), e);
			try {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_ACAO_RELACIONADO, true);
			} catch (NegocioException ex) {
				mensagemErroParametrizado(model, ex);
			}
		}
		return model;
	}

	/**
	 * Carregar combos.
	 * 
	 * @param model
	 *            the model
	 * @param modulo
	 * 			  o módulo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void carregarCombos(ModelAndView model, Modulo modulo) throws NegocioException {

		model.addObject("listaPrecedente", controladorAcao.obterAcoes(Boolean.TRUE));
		model.addObject("listaTipoDocumento", controladorTipoDocumento.obterTodas(Boolean.TRUE));
		model.addObject("listaDias", controladorEntidadeConteudo.obterListaOpcaoDias());
		model.addObject("listaEquipe", controladorEquipe.listar());
		ServicoTipo servicoTipo = new ServicoTipo();
		servicoTipo.setIndicadorGeraLote(Boolean.TRUE);
		model.addObject("listaServicoTipo", controladorServicoTipo.consultarServicoTipo(servicoTipo, Boolean.TRUE));
		ConstanteSistema constanteChamadoTipo = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_CHAMADO_TIPO_PESQUISA);
		
		ChamadoTipo chamadoTipo;
		try {
			chamadoTipo = controladorChamadoTipo.consultarChamadoTipo(Long.valueOf(constanteChamadoTipo.getValor()));
			Collection<ChamadoAssunto> listaAssunto = new ArrayList<ChamadoAssunto>();
			listaAssunto.addAll(chamadoTipo.getListaAssuntos());
			model.addObject("listaChamadoAssunto", listaAssunto);
		} catch (NegocioException e) {
			chamadoTipo = null;
			LOG.error(e.getMessage(), e);
		}
		
		model.addObject("listaUnidadeOrganizacional", controladorUnidadeOrganizacional.obterTodas());
		model.addObject("listaSegmento", controladorSegmento.listarSegmento());
		model.addObject("listaQuestionario", controladorQuestionario.consultarQuestionario(null));
		model.addObject("modulos", controladorModulo.consultarModulos(null));
		if (modulo != null) {
			popularComboOperacao(model, modulo);
		}
	}

	private void popularComboOperacao(ModelAndView model, Modulo modulo) throws NegocioException {
		long chavePrimariaModuloSelecionado = modulo.getChavePrimaria();
		Collection<Operacao> operacoesBatchPorModulo = controladorModulo.consultarOperacoesBatchPorModulo(chavePrimariaModuloSelecionado);
		model.addObject(OPERACOES_BATCH, operacoesBatchPorModulo);
	}

}
