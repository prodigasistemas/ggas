/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.batch.acaocomando.dominio;

import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.cadastro.operacional.ZonaBloqueio;

/**
 * Classe criada para dimimuir dependências da classe {@link AcaoComando}.
 * Há uma regra no Sonar que gera violações em uma classe que contenha mais de 20 imports.
 * Essa regra indica que classes com muitas responsabilidades devem ser separadas em classes menores.
 * 
 * @author lcavalcanti
 */
public class AcaoComandoEstendida extends AcaoComando {

	private static final long serialVersionUID = -4935271599442482322L;

	private CityGate cityGate;
	
	private Tronco tronco;
	
	private ZonaBloqueio zonaBloqueio;

	/**
	 * Obtem City Gate
	 * 
	 * @return cityGate
	 */
	public CityGate getCityGate() {
		return cityGate;
	}

	/**
	 * Atribui valor ao City Gate
	 * 
	 * @param cityGate
	 */
	public void setCityGate(CityGate cityGate) {
		this.cityGate = cityGate;
	}

	/**
	 * Obtem Tronco
	 * 
	 * @return tronco
	 */
	public Tronco getTronco() {
		return tronco;
	}

	/**
	 * Atribui valor ao Tronco
	 * 
	 * @param tronco
	 */
	public void setTronco(Tronco tronco) {
		this.tronco = tronco;
	}

	/**
	 * Obtem Zona de Bloqueio
	 * 
	 * @return zonaBloqueio
	 */
	public ZonaBloqueio getZonaBloqueio() {
		return zonaBloqueio;
	}

	/**
	 * Attribui valor a Zona de Bloqueio 
	 * 
	 * @param zonaBloqueio
	 */
	public void setZonaBloqueio(ZonaBloqueio zonaBloqueio) {
		this.zonaBloqueio = zonaBloqueio;
	}

}
