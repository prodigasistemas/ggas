/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *03/01/2014
 * vpessoa
 * 17:21:43
 */

package br.com.ggas.batch.acaocomando.negocio;

import java.util.Collection;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoEstendida;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoPontoConsumo;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoVO;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de ControladorAcaoComando
 * 
 * @author vpessoa
 */
public interface ControladorAcaoComando {

	/**
	 * Inserir acao comando.	
	 * 
	 * @param acaoComandoVO
	 *            the acao comando vo
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException {@link GGASException}
	 */
	public void inserirAcaoComando(AcaoComandoVO acaoComandoVO) throws GGASException;

	/**
	 * Consultar comandos.
	 * 
	 * @param chaveOperacao
	 *            the chave operacao
	 * @return the collection
	 */
	public Collection<EntidadeNegocio> consultarComandos(Long chaveOperacao);

	/**
	 * Consultar acao comando.
	 * 
	 * @param acaoComando
	 *            the acao comando
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<EntidadeNegocio> consultarAcaoComando(AcaoComandoVO acaoComando, Boolean habilitado) throws NegocioException;

	/**
	 * Obter acao comando.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the acao comando
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public AcaoComandoEstendida obterAcaoComando(Long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Atualizar.
	 * 
	 * @param acaoComandoVO
	 *            the acao comando vo
	 * @return the acao comando
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException {@link GGASException}
	 */
	public AcaoComando atualizar(AcaoComandoVO acaoComandoVO) throws GGASException;

	/**
	 * Remover acao comando.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void removerAcaoComando(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Converter acao comando vo em acao comando.
	 * 
	 * @param acaoComandoVO
	 *            the acao comando vo
	 * @return the acao comando
	 * @throws GGASException {@link GGASException}
	 */
	public AcaoComando converterAcaoComandoVOemAcaoComando(AcaoComandoVO acaoComandoVO) throws GGASException;
	
	/**
	 * Pesquisar pontos de consumo pelo comando de acao
	 * 
	 * @param chavePrimaria - {@link long}
	 * @return Collection<AcaoComandoPontoConsumo> - Coleção AcaoComandoPontoConsumo
	 * @throws NegocioException - {@link NegocioException}
	 */
	public Collection<AcaoComandoPontoConsumo> pesquisarPontosConsumoPorAcaoComando(long chavePrimaria) throws NegocioException;
}
