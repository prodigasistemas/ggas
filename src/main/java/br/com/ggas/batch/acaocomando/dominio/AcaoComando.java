/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.batch.acaocomando.dominio;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.batch.acao.dominio.Acao;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.medidor.MarcaMedidor;
import br.com.ggas.medicao.medidor.ModeloMedidor;
import br.com.ggas.medicao.vazaocorretor.MarcaCorretor;
import br.com.ggas.medicao.vazaocorretor.ModeloCorretor;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;

/**
 * Acao Comando
 * 
 * @author vpessoa
 */
public class AcaoComando extends EntidadeNegocioImpl {

	private static final long serialVersionUID = 2840439234760771330L;

	private static final int LIMITE_CAMPO = 2;
	private static final String EM_BRANCO = "";

	private String nome;

	private String descricao;

	private Boolean indicadorSimulacao;

	private Boolean indicadorAleatorio;

	private Acao acao;

	private GrupoFaturamento grupoFaturamento;

	private Localidade localidade;

	private SetorComercial setorComercial;

	private Municipio municipio;

	private MarcaMedidor marcaMedidor;

	private ModeloMedidor modeloMedidor;

	private MarcaCorretor marcaCorretor;

	private ModeloCorretor modeloCorretor;

	private Boolean indicadorCondominio;

	private SituacaoConsumo situacaoConsumo;

	private Segmento segmento;

	private Integer qtdPontoConsumo;

	private ModalidadeMedicaoImovel modalidadeMedicaoImovel;

	private Date dataInstalacaoInicio;

	private Date dataInstalacaoFim;

	private Integer anoFabricacaoInicio;

	private Integer anoFabricacaoFim;
	
	private Collection<AcaoComandoPontoConsumo> listaPontoConsumo = new HashSet<>();

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Acao getAcao() {
		return acao;
	}

	public void setAcao(Acao acao) {
		this.acao = acao;
	}

	public GrupoFaturamento getGrupoFaturamento() {
		return grupoFaturamento;
	}

	public void setGrupoFaturamento(GrupoFaturamento grupoFaturamento) {
		this.grupoFaturamento = grupoFaturamento;
	}

	public Localidade getLocalidade() {
		return localidade;
	}

	public void setLocalidade(Localidade localidade) {
		this.localidade = localidade;
	}

	public SetorComercial getSetorComercial() {
		return setorComercial;
	}

	public void setSetorComercial(SetorComercial setorComercial) {
		this.setorComercial = setorComercial;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public MarcaMedidor getMarcaMedidor() {
		return marcaMedidor;
	}

	public void setMarcaMedidor(MarcaMedidor marcaMedidor) {
		this.marcaMedidor = marcaMedidor;
	}

	public ModeloMedidor getModeloMedidor() {
		return modeloMedidor;
	}

	public void setModeloMedidor(ModeloMedidor modeloMedidor) {
		this.modeloMedidor = modeloMedidor;
	}

	public MarcaCorretor getMarcaCorretor() {
		return marcaCorretor;
	}

	public void setMarcaCorretor(MarcaCorretor marcaCorretor) {
		this.marcaCorretor = marcaCorretor;
	}

	public ModeloCorretor getModeloCorretor() {
		return modeloCorretor;
	}

	public void setModeloCorretor(ModeloCorretor modeloCorretor) {
		this.modeloCorretor = modeloCorretor;
	}

	public SituacaoConsumo getSituacaoConsumo() {
		return situacaoConsumo;
	}

	public void setSituacaoConsumo(SituacaoConsumo situacaoConsumo) {
		this.situacaoConsumo = situacaoConsumo;
	}

	public Segmento getSegmento() {
		return segmento;
	}

	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}

	public Integer getQtdPontoConsumo() {
		return qtdPontoConsumo;
	}

	public void setQtdPontoConsumo(Integer qtdPontoConsumo) {
		this.qtdPontoConsumo = qtdPontoConsumo;
	}

	public Boolean getIndicadorSimulacao() {
		return indicadorSimulacao;
	}

	public void setIndicadorSimulacao(Boolean indicadorSimulacao) {
		this.indicadorSimulacao = indicadorSimulacao;
	}

	public Boolean getIndicadorAleatorio() {
		return indicadorAleatorio;
	}

	public void setIndicadorAleatorio(Boolean indicadorAleatorio) {
		this.indicadorAleatorio = indicadorAleatorio;
	}

	public Boolean getIndicadorCondominio() {
		return indicadorCondominio;
	}

	public void setIndicadorCondominio(Boolean indicadorCondominio) {
		this.indicadorCondominio = indicadorCondominio;
	}

	public Date getDataInstalacaoInicio() {
		if (dataInstalacaoInicio != null) {
			return new Date(dataInstalacaoInicio.getTime());
		} else {
			return null;
		}
	}

	public void setDataInstalacaoInicio(Date dataInstalacaoInicio) {
		if (dataInstalacaoInicio != null) {
			this.dataInstalacaoInicio = new Date(dataInstalacaoInicio.getTime());
		} else {
			this.dataInstalacaoInicio = null;
		}
	}

	public String getDataInstalacaoInicioFormatada() {
		if (dataInstalacaoInicio != null) {
			return DataUtil.converterDataParaString(dataInstalacaoInicio, false);
		} else {
			return EM_BRANCO;
		}
	}

	public Date getDataInstalacaoFim() {
		if (dataInstalacaoFim != null) {
			return new Date(dataInstalacaoFim.getTime());
		} else {
			return null;
		}
	}

	public void setDataInstalacaoFim(Date dataInstalacaoFim) {
		if (dataInstalacaoFim != null) {
			this.dataInstalacaoFim = new Date(dataInstalacaoFim.getTime());
		} else {
			this.dataInstalacaoFim = null;
		}
	}

	public String getDataInstalacaoFimFormatada() {
		if (dataInstalacaoFim != null) {
			return DataUtil.converterDataParaString(dataInstalacaoFim, false);
		} else {
			return EM_BRANCO;
		}
	}

	public ModalidadeMedicaoImovel getModalidadeMedicaoImovel() {
		return modalidadeMedicaoImovel;
	}

	public void setModalidadeMedicaoImovel(ModalidadeMedicaoImovel modalidadeMedicaoImovel) {
		this.modalidadeMedicaoImovel = modalidadeMedicaoImovel;
	}

	public Integer getAnoFabricacaoInicio() {
		return anoFabricacaoInicio;
	}

	public void setAnoFabricacaoInicio(Integer anoFabricacaoInicio) {
		this.anoFabricacaoInicio = anoFabricacaoInicio;
	}

	public Integer getAnoFabricacaoFim() {
		return anoFabricacaoFim;
	}

	public void setAnoFabricacaoFim(Integer anoFabricacaoFim) {
		this.anoFabricacaoFim = anoFabricacaoFim;
	}

	public Collection<AcaoComandoPontoConsumo> getListaPontoConsumo() {
		return listaPontoConsumo;
	}

	public void setListaPontoConsumo(Collection<AcaoComandoPontoConsumo> listaPontoConsumo) {
		this.listaPontoConsumo = listaPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (StringUtils.isEmpty(descricao) || descricao.trim().length() == 0) {
			stringBuilder.append(Constantes.CAMPO_DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (StringUtils.isEmpty(nome) || nome.trim().length() == 0) {
			stringBuilder.append(Constantes.CAMPO_NOME);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (acao == null) {
			stringBuilder.append(Constantes.CAMPO_ACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (indicadorSimulacao == null) {
			stringBuilder.append(Constantes.ATIVIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {

			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));

		}

		return erros;
	}
}
