/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *26/12/2013
 * vpessoa
 * 10:42:19
 */

package br.com.ggas.batch.acaocomando.dominio;

import java.util.Map;

import br.com.ggas.batch.acao.dominio.Acao;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de  AcaoComandoVO
 * 
 * @author vpessoa
 */
public class AcaoComandoVO extends EntidadeNegocioImpl {

	private static final long serialVersionUID = 2840439234760771330L;

	private String nome;

	private String descricao;

	private Boolean indicadorSimulacao;

	private Boolean indicadorAleatorio;

	private Acao acao;

	private Long grupoFaturamento;

	private Long localidade;

	private Long setorComercial;

	private Long municipio;

	private Long marcaMedidor;

	private Long modeloMedidor;

	private Long marcaCorretor;

	private Long modeloCorretor;

	private Boolean indicadorCondominio;

	private Long situacaoConsumo;

	private Long segmento;

	private Integer qtdPontoConsumo;

	private Long cityGate;
	
	private Long tronco;
	
	private Long zonaBloqueio;

	private Integer modalidadeMedicaoImovel;
	
	private String dataInstalacaoInicio;
	
	private String dataInstalacaoFim;
	
	private Integer anoFabricacaoInicio;
	
	private Integer anoFabricacaoFim;
	
	public String getNome() {

		return nome;
	}

	public void setNome(String nome) {

		this.nome = nome;
	}

	public String getDescricao() {

		return descricao;
	}

	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	public Acao getAcao() {

		return acao;
	}

	public void setAcao(Acao acao) {

		this.acao = acao;
	}

	public Integer getQtdPontoConsumo() {

		return qtdPontoConsumo;
	}

	public void setQtdPontoConsumo(Integer qtdPontoConsumo) {

		this.qtdPontoConsumo = qtdPontoConsumo;
	}

	public Long getGrupoFaturamento() {

		return grupoFaturamento;
	}

	public void setGrupoFaturamento(Long grupoFaturamento) {

		this.grupoFaturamento = grupoFaturamento;
	}

	public Long getLocalidade() {

		return localidade;
	}

	public void setLocalidade(Long localidade) {

		this.localidade = localidade;
	}

	public Long getSetorComercial() {

		return setorComercial;
	}

	public void setSetorComercial(Long setorComercial) {

		this.setorComercial = setorComercial;
	}

	public Long getMunicipio() {

		return municipio;
	}

	public void setMunicipio(Long municipio) {

		this.municipio = municipio;
	}

	public Long getMarcaMedidor() {

		return marcaMedidor;
	}

	public void setMarcaMedidor(Long marcaMedidor) {

		this.marcaMedidor = marcaMedidor;
	}

	public Long getModeloMedidor() {

		return modeloMedidor;
	}

	public void setModeloMedidor(Long modeloMedidor) {

		this.modeloMedidor = modeloMedidor;
	}

	public Long getMarcaCorretor() {

		return marcaCorretor;
	}

	public void setMarcaCorretor(Long marcaCorretor) {

		this.marcaCorretor = marcaCorretor;
	}

	public Long getModeloCorretor() {

		return modeloCorretor;
	}

	public void setModeloCorretor(Long modeloCorretor) {

		this.modeloCorretor = modeloCorretor;
	}

	public Long getSituacaoConsumo() {

		return situacaoConsumo;
	}

	public void setSituacaoConsumo(Long situacaoConsumo) {

		this.situacaoConsumo = situacaoConsumo;
	}

	public Long getSegmento() {

		return segmento;
	}

	public void setSegmento(Long segmento) {

		this.segmento = segmento;
	}

	public Boolean getIndicadorSimulacao() {

		return indicadorSimulacao;
	}

	public void setIndicadorSimulacao(Boolean indicadorSimulacao) {

		this.indicadorSimulacao = indicadorSimulacao;
	}

	public Boolean getIndicadorAleatorio() {

		return indicadorAleatorio;
	}

	public void setIndicadorAleatorio(Boolean indicadorAleatorio) {

		this.indicadorAleatorio = indicadorAleatorio;
	}

	public Boolean getIndicadorCondominio() {

		return indicadorCondominio;
	}

	public void setIndicadorCondominio(Boolean indicadorCondominio) {

		this.indicadorCondominio = indicadorCondominio;
	}
	
	public Long getCityGate() {
		return cityGate;
	}

	public void setCityGate(Long cityGate) {
		this.cityGate = cityGate;
	}

	public Long getTronco() {
		return tronco;
	}

	public void setTronco(Long tronco) {
		this.tronco = tronco;
	}

	public Long getZonaBloqueio() {
		return zonaBloqueio;
	}

	public void setZonaBloqueio(Long zonabloqueio) {
		this.zonaBloqueio = zonabloqueio;
	}

	public Integer getModalidadeMedicaoImovel() {
		return modalidadeMedicaoImovel;
	}

	public void setModalidadeMedicaoImovel(Integer modalidadeMedicaoImovel) {
		this.modalidadeMedicaoImovel = modalidadeMedicaoImovel;
	}

	public String getDataInstalacaoInicio() {
		return dataInstalacaoInicio;
	}

	public void setDataInstalacaoInicio(String dataInstalacaoInicio) {
		this.dataInstalacaoInicio = dataInstalacaoInicio;
	}

	public String getDataInstalacaoFim() {
		return dataInstalacaoFim;
	}

	public void setDataInstalacaoFim(String dataInstalacaoFim) {
		this.dataInstalacaoFim = dataInstalacaoFim;
	}
	
	public Integer getAnoFabricacaoInicio() {
		return anoFabricacaoInicio;
	}

	public void setAnoFabricacaoInicio(Integer anoFabricacaoInicio) {
		this.anoFabricacaoInicio = anoFabricacaoInicio;
	}

	public Integer getAnoFabricacaoFim() {
		return anoFabricacaoFim;
	}

	public void setAnoFabricacaoFim(Integer anoFabricacaoFim) {
		this.anoFabricacaoFim = anoFabricacaoFim;
	}

	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}

