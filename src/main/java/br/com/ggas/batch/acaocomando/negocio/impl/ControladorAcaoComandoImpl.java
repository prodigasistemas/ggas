/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 03/01/2014 vpessoa 17:23:40
 */

package br.com.ggas.batch.acaocomando.negocio.impl;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoEstendida;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoPontoConsumo;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoVO;
import br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando;
import br.com.ggas.batch.acaocomando.repositorio.RepositorioAcaoComando;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.medidor.MarcaMedidor;
import br.com.ggas.medicao.medidor.ModeloMedidor;
import br.com.ggas.medicao.vazaocorretor.MarcaCorretor;
import br.com.ggas.medicao.vazaocorretor.ModeloCorretor;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de 
 * 
 * @author vpessoa
 */
@Service("controladorAcaoComando")
@Transactional
public class ControladorAcaoComandoImpl implements ControladorAcaoComando {

	@Autowired
	private RepositorioAcaoComando repositorioAcaoComando;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando#inserirAcaoComando(br.com.ggas.batch.acaocomando.dominio.AcaoComandoVO)
	 */
	@Override
	public void inserirAcaoComando(AcaoComandoVO acaoComandoVO) throws GGASException { 

		AcaoComando acaoComando = converterAcaoComandoVOemAcaoComando(acaoComandoVO);
		validaDadosAcaoComando(acaoComando);
		repositorioAcaoComando.inserir(acaoComando);

	}

	private void validaDadosAcaoComando(AcaoComando acaoComando) throws NegocioException {
		
		if(acaoComando.getAnoFabricacaoInicio()!=null 
				&& acaoComando.getAnoFabricacaoFim()!=null 
				&& acaoComando.getAnoFabricacaoInicio()>acaoComando.getAnoFabricacaoFim()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ANO_FABRICACAO_INICIO_POSTERIOR, true);
		}
		if(acaoComando.getDataInstalacaoInicio()!=null 
				&& acaoComando.getDataInstalacaoFim()!=null
				&& acaoComando.getDataInstalacaoInicio().after(acaoComando.getDataInstalacaoFim())) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_DATA_POSTERIOR, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando#converterAcaoComandoVOemAcaoComando(br.com.ggas.batch.acaocomando.
	 * dominio .AcaoComandoVO)
	 */
	@Override
	public AcaoComando converterAcaoComandoVOemAcaoComando(AcaoComandoVO acaoComandoVO) throws GGASException {

		ServiceLocator service = ServiceLocator.getInstancia();
		AcaoComandoEstendida acaoComandoEstendida = new AcaoComandoEstendida();

		acaoComandoEstendida.setNome(acaoComandoVO.getNome());
		acaoComandoEstendida.setDescricao(acaoComandoVO.getDescricao());

		if (acaoComandoVO.getAcao() != null) {
			acaoComandoEstendida.setAcao(acaoComandoVO.getAcao());
		}

		if (acaoComandoVO.getGrupoFaturamento() != -1) {
			acaoComandoEstendida.setGrupoFaturamento((GrupoFaturamento) service
					.getBeanPorIDeSeteChavePrimaria(GrupoFaturamento.BEAN_ID_GRUPO_FATURAMENTO, acaoComandoVO.getGrupoFaturamento()));
		}

		if (acaoComandoVO.getLocalidade() != -1) {
			acaoComandoEstendida.setLocalidade(
					(Localidade) service.getBeanPorIDeSeteChavePrimaria(Localidade.BEAN_ID_LOCALIDADE, acaoComandoVO.getLocalidade()));
		}

		if (acaoComandoVO.getMarcaCorretor() != -1) {
			acaoComandoEstendida.setMarcaCorretor((MarcaCorretor) service
					.getBeanPorIDeSeteChavePrimaria(MarcaCorretor.BEAN_ID_MARCA_CORRETOR, acaoComandoVO.getMarcaCorretor()));
		}

		if (acaoComandoVO.getMarcaMedidor() != -1) {
			acaoComandoEstendida.setMarcaMedidor((MarcaMedidor) service.getBeanPorIDeSeteChavePrimaria(MarcaMedidor.BEAN_ID_MARCA_MEDIDOR,
					acaoComandoVO.getMarcaMedidor()));
		}

		if (acaoComandoVO.getModeloCorretor() != -1) {
			acaoComandoEstendida.setModeloCorretor((ModeloCorretor) service
					.getBeanPorIDeSeteChavePrimaria(ModeloCorretor.BEAN_ID_MODELO_CORRETOR, acaoComandoVO.getModeloCorretor()));
		}

		if (acaoComandoVO.getModeloMedidor() != -1) {
			acaoComandoEstendida.setModeloMedidor((ModeloMedidor) service
					.getBeanPorIDeSeteChavePrimaria(ModeloMedidor.BEAN_ID_MODELO_MEDIDOR, acaoComandoVO.getModeloMedidor()));
		}

		if (acaoComandoVO.getMunicipio() != -1) {
			acaoComandoEstendida.setMunicipio(
					(Municipio) service.getBeanPorIDeSeteChavePrimaria(Municipio.BEAN_ID_MUNICIPIO, acaoComandoVO.getMunicipio()));
		}

		if (acaoComandoVO.getSegmento() != -1) {
			acaoComandoEstendida
					.setSegmento((Segmento) service.getBeanPorIDeSeteChavePrimaria(Segmento.BEAN_ID_SEGMENTO, acaoComandoVO.getSegmento()));
		}

		if (acaoComandoVO.getSituacaoConsumo() != -1) {
			acaoComandoEstendida.setSituacaoConsumo((SituacaoConsumo) service
					.getBeanPorIDeSeteChavePrimaria(SituacaoConsumo.BEAN_ID_SITUACAO_CONSUMO, acaoComandoVO.getSituacaoConsumo()));
		}

		if (acaoComandoVO.getQtdPontoConsumo() != null) {
			acaoComandoEstendida.setQtdPontoConsumo(acaoComandoVO.getQtdPontoConsumo());
		}

		if (acaoComandoVO.getIndicadorSimulacao() != null) {
			acaoComandoEstendida.setIndicadorSimulacao(acaoComandoVO.getIndicadorSimulacao());
		}

		if (acaoComandoVO.getIndicadorCondominio() != null) {
			acaoComandoEstendida.setIndicadorCondominio(acaoComandoVO.getIndicadorCondominio());
		}

		if (acaoComandoVO.getSetorComercial() != -1) {
			acaoComandoEstendida.setSetorComercial((SetorComercial) service
					.getBeanPorIDeSeteChavePrimaria(SetorComercial.BEAN_ID_SETOR_COMERCIAL, acaoComandoVO.getSetorComercial()));
		}

		if (acaoComandoVO.getModalidadeMedicaoImovel() != -1) {
			acaoComandoEstendida.setModalidadeMedicaoImovel(
					Fachada.getInstancia().buscarModalidadeMedicaoImovelPorCodigo(acaoComandoVO.getModalidadeMedicaoImovel()));
		}
		if (acaoComandoVO.getCityGate() != null && acaoComandoVO.getCityGate() != -1) {
			acaoComandoEstendida.setCityGate(Fachada.getInstancia().obterCityGate(String.valueOf(acaoComandoVO.getCityGate())));
		}
		if (acaoComandoVO.getTronco() != null && acaoComandoVO.getTronco() != -1) {
			acaoComandoEstendida.setTronco(Fachada.getInstancia().buscarTroncoPorChave(acaoComandoVO.getTronco()));
		}
		if (acaoComandoVO.getZonaBloqueio() != null && acaoComandoVO.getZonaBloqueio() != -1) {
			acaoComandoEstendida.setZonaBloqueio(Fachada.getInstancia().buscarZonaBloqueioChave(acaoComandoVO.getZonaBloqueio()));
		}
		populaCamposData(acaoComandoVO, acaoComandoEstendida);

		acaoComandoEstendida.setChavePrimaria(acaoComandoVO.getChavePrimaria());
		acaoComandoEstendida.setHabilitado(acaoComandoVO.isHabilitado());
		acaoComandoEstendida.setVersao(acaoComandoVO.getVersao());

		return acaoComandoEstendida;

	}

	private void populaCamposData(AcaoComandoVO acaoComandoVO, AcaoComandoEstendida acaoComandoEstendida) throws GGASException {
		if (acaoComandoVO.getAnoFabricacaoInicio() != -1) {
			acaoComandoEstendida.setAnoFabricacaoInicio(acaoComandoVO.getAnoFabricacaoInicio());
		}
		if (acaoComandoVO.getAnoFabricacaoFim() != -1) {
			acaoComandoEstendida.setAnoFabricacaoFim(acaoComandoVO.getAnoFabricacaoFim());
		}

		if (StringUtils.isNotEmpty(acaoComandoVO.getDataInstalacaoInicio())) {
			acaoComandoEstendida.setDataInstalacaoInicio(
					Util.converterCampoStringParaData("data", acaoComandoVO.getDataInstalacaoInicio(), Constantes.FORMATO_DATA_BR));
		}

		if (StringUtils.isNotEmpty(acaoComandoVO.getDataInstalacaoInicio())) {
			acaoComandoEstendida.setDataInstalacaoFim(
					Util.converterCampoStringParaData("data", acaoComandoVO.getDataInstalacaoFim(), Constantes.FORMATO_DATA_BR));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando#consultarComandos(java.lang.Long)
	 */
	@Override
	public Collection<EntidadeNegocio> consultarComandos(Long chaveOperacao) {

		return repositorioAcaoComando.consultarComandos(chaveOperacao);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando#consultarAcaoComando(br.com.ggas.batch.acaocomando.dominio.AcaoComandoVO
	 * , java.lang.Boolean)
	 */
	@Override
	public Collection<EntidadeNegocio> consultarAcaoComando(AcaoComandoVO acaoComando, Boolean habilitado) throws NegocioException {

		return repositorioAcaoComando.consultarAcaoComando(acaoComando, habilitado);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando#obterAcaoComando(java.lang.Long, java.lang.String[])
	 */
	@Override
	public AcaoComandoEstendida obterAcaoComando(Long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (AcaoComandoEstendida) repositorioAcaoComando.obter(chavePrimaria, propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando#atualizar(br.com.ggas.batch.acaocomando.dominio.AcaoComandoVO)
	 */
	@Override
	public AcaoComando atualizar(AcaoComandoVO acaoComandoVO) throws GGASException {

		AcaoComando acaoComando = converterAcaoComandoVOemAcaoComando(acaoComandoVO);
		validaDadosAcaoComando(acaoComando);
		repositorioAcaoComando.atualizar(acaoComando);

		return acaoComando;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando#removerAcaoComando(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerAcaoComando(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.repositorioAcaoComando.remover(chavesPrimarias, dadosAuditoria);
	}

	/**
	 * Pesquisar pontos de consumo pelo comando de acao
	 * 
	 * @param chavePrimaria
	 * @return Collection<AcaoComandoPontoConsumo>
	 * @throws NegocioException
	 */
	@Override
	public Collection<AcaoComandoPontoConsumo> pesquisarPontosConsumoPorAcaoComando(long chavePrimaria) throws NegocioException {
		return repositorioAcaoComando.pesquisarPontosConsumoPorAcaoComando(chavePrimaria);
	}
}
