/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *30/12/2013
 * vpessoa
 * 11:15:44
 */

package br.com.ggas.batch.acaocomando.apresentacao;

import static br.com.ggas.util.Constantes.ATRIBUTO_OPERACAO;
import static br.com.ggas.util.Constantes.ATRIBUTO_USUARIO_LOGADO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.ggas.arrecadacao.ControladorTipoDocumento;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.acao.dominio.Acao;
import br.com.ggas.batch.acao.negocio.ControladorAcao;
import br.com.ggas.batch.acaocomando.dominio.AcaoComando;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoVO;
import br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando;
import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.cadastro.operacional.ControladorZonaBloqueio;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de AcaoComandoAction
 * 
 * @author vpessoa
 */
@Controller
public class AcaoComandoAction extends GenericAction {
	
	private static final String ACAO_COMANDO = "acaoComando";

	private static final Logger LOG = Logger.getLogger(AcaoComandoAction.class);
	
	private static final String CITY_GATE_CLASS_NAME = "br.com.ggas.cadastro.operacional.impl.CityGateImpl";

	@Autowired
	private ControladorAcao controladorAcao;

	@Autowired
	private ControladorAcaoComando controladorAcaoComando;

	@Autowired
	@Qualifier("controladorTipoDocumento")
	private ControladorTipoDocumento controladorTipoDocumento;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier("controladorFatura")
	private ControladorFatura controladorFatura;

	@Autowired
	@Qualifier("controladorLocalidade")
	private ControladorLocalidade controladorLocalidade;

	@Autowired
	@Qualifier("controladorSetorComercial")
	private ControladorSetorComercial controladorSetorComercial;

	@Autowired
	@Qualifier("controladorMunicipio")
	private ControladorMunicipio controladorMunicipio;

	@Autowired
	@Qualifier("controladorMedidor")
	private ControladorMedidor controladorMedidor;

	@Autowired
	@Qualifier("controladorVazaoCorretor")
	private ControladorVazaoCorretor controladorVazaoCorretor;

	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;
	
	@Autowired
	@Qualifier("controladorZonaBloqueio")
	private ControladorZonaBloqueio controladorZonaBloqueio;
	
	private static final String HABILITADO = "habilitado";

	/**
	 * Exibir pesquisa acao comando.
	 * 
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirPesquisaAcaoComando")
	public ModelAndView exibirPesquisaAcaoComando() throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaAcaoComando");
		AcaoComando acaoComando = new AcaoComando();
		acaoComando.setHabilitado(Boolean.TRUE);
		model.addObject(ACAO_COMANDO, acaoComando);
		carregarCombos(model);
		return model;
	}

	/**
	 * Exibir inclusao acao comando.
	 * 
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("exibirInclusaoAcaoComando")
	public ModelAndView exibirInclusaoAcaoComando() throws GGASException {

		ModelAndView model = new ModelAndView("exibirInclusaoAcaoComando");
		Acao acao = new Acao();
		acao.setIndicadorGerarAutorizacaoServico(Boolean.TRUE);
		acao.setIndicadorGerarChamadoPesquisa(Boolean.TRUE);
		acao.setConsiderarDebitosAVencer(Boolean.TRUE);
		acao.setBloquearDocumentosPagaveis(Boolean.TRUE);
		model.addObject("acao", acao);
		carregarCombos(model);
		model.addObject("fluxoInclusao", Boolean.TRUE);
		return model;
	}

	/**
	 * Incluir acao.
	 * 
	 * @param acaoComando
	 *            the acao comando
	 * @param result
	 *            the result
	 * @return the model and view
	 * @throws GGASException {@link GGASException}
	 */
	@RequestMapping("incluirAcaoComando")
	public ModelAndView incluirAcao(@ModelAttribute("AcaoComando") AcaoComandoVO acaoComando, BindingResult result) throws GGASException {
		String constante;
		ModelAndView model = new ModelAndView("forward:/pesquisarAcaoComando");
		try {
			if(acaoComando.getChavePrimaria() == 0) {
				controladorAcaoComando.inserirAcaoComando(acaoComando);
				constante = Constantes.SUCESSO_ENTIDADE_INCLUIDA;
			} else {
				controladorAcaoComando.atualizar(acaoComando);
				constante = Constantes.SUCESSO_ENTIDADE_ALTERADA;
			}
			mensagemSucesso(model, constante, GenericAction.obterMensagem(Constantes.ACAO));
		} catch(GGASException e) {
			model = new ModelAndView("exibirInclusaoAcaoComando");
			carregarCombos(model);
			AcaoComando acaoComandoConvertido = controladorAcaoComando.converterAcaoComandoVOemAcaoComando(acaoComando);
			model.addObject(ACAO_COMANDO, acaoComandoConvertido);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Carregar comandos.
	 * 
	 * @param chaveOperacao
	 *            the chave operacao
	 * @return the model and view
	 */
	@RequestMapping("carregarComandos")
	public ModelAndView carregarComandos(@RequestParam("chaveOperacao") Long chaveOperacao) {

		ModelAndView model = new ModelAndView("comboComandos");
		model.addObject("comandos", controladorAcaoComando.consultarComandos(chaveOperacao));
		return model;
	}

	/**
	 * Pesquisar acao comando.
	 * 
	 * @param acaoComando
	 *            the acao comando
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarAcaoComando")
	public ModelAndView pesquisarAcaoComando(@ModelAttribute("AcaoComandoVO") AcaoComandoVO acaoComando, BindingResult result,
					HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaAcaoComando");

		Boolean habilitado = null;
		if(request.getAttribute(HABILITADO) != null) {
			habilitado = Boolean.parseBoolean((String) request.getAttribute(HABILITADO));
		}
		try {
			model.addObject(ACAO_COMANDO, acaoComando);
			model.addObject("listaAcaoComando", controladorAcaoComando.consultarAcaoComando(acaoComando, habilitado));
			model.addObject("listaAcoes", controladorAcao.obterAcoes(Boolean.TRUE));
		} catch(GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Exibir detalhamento acao comando.
	 * 
	 * @param chavesPrimaria
	 *            the chaves primaria
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirDetalhamentoAcaoComando")
	public ModelAndView exibirDetalhamentoAcaoComando(@RequestParam("chavesPrimaria") Long chavesPrimaria) throws GGASException {

		Long chavePrimaria = chavesPrimaria;

		ModelAndView model = new ModelAndView("exibirDetalhamentoAcaoComando");
		model.addObject(ACAO_COMANDO, controladorAcaoComando.obterAcaoComando(chavePrimaria, "acao", "grupoFaturamento", "localidade",
						"setorComercial", "municipio", "marcaMedidor", "modeloMedidor", "marcaCorretor", "modeloCorretor",
						"situacaoConsumo", "segmento"));
		carregarCombos(model);

		return model;
	}

	/**
	 * Exibir alteracao acao comando.
	 * 
	 * @param chavesPrimaria
	 *            the chaves primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirAlteracaoAcaoComando")
	public ModelAndView exibirAlteracaoAcaoComando(@RequestParam("chavesPrimaria") Long chavesPrimaria, HttpServletRequest request)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirAlteracaoAcaoComando");

		AcaoComando acaoComando = controladorAcaoComando.obterAcaoComando(chavesPrimaria, "acao", "grupoFaturamento", "localidade",
						"setorComercial", "municipio", "marcaMedidor", "modeloMedidor", "marcaCorretor", "modeloCorretor",
						"situacaoConsumo", "segmento", "cityGate", "tronco");
		model.addObject(ACAO_COMANDO, acaoComando);
		carregarCombos(model);
		model.addObject("fluxoAlteracao", Boolean.TRUE);

		return model;
	}

	/**
	 * Remover acao comando.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("removerAcaoComando")
	public ModelAndView removerAcaoComando(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws NegocioException {

		ModelAndView model = new ModelAndView("forward:/exibirPesquisaAcaoComando");
		try {
			DadosAuditoria dadosAuditoria = Util.getDadosAuditoria((Usuario) request.getSession().getAttribute(ATRIBUTO_USUARIO_LOGADO),
							(Operacao) request.getAttribute(ATRIBUTO_OPERACAO), request.getRemoteAddr());
			controladorAcaoComando.removerAcaoComando(chavesPrimarias, dadosAuditoria);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, "Ação Comando");
		} catch(NegocioException e) {
			LOG.error(e.getMessage(), e);
			try {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_ACAOCOMANDO_RELACIONADO, true);
			} catch(NegocioException ex) {
				mensagemErroParametrizado(model, ex);
			}
		}
		return model;
	}

	/**
	 * Carregar combos.
	 * 
	 * @param model
	 *            the model
	 * @throws GGASException {@link GGASException}
	 */
	public void carregarCombos(ModelAndView model) throws GGASException {

		model.addObject("listaAcoes", controladorAcao.obterAcoes(Boolean.TRUE));
		model.addObject("listaTipoDocumento", controladorTipoDocumento.obterTodas(Boolean.TRUE));
		model.addObject("listaDias", controladorEntidadeConteudo.obterListaOpcaoDias());
		model.addObject("listaGrupoFaturamento", controladorFatura.consultarGrupoFaturamento(null));
		model.addObject("listaLocalidade", controladorLocalidade.consultarLocalidades(null));
		model.addObject("listaSetorComercial", controladorSetorComercial.consultarSetorComercial(null));
		model.addObject("listaMunicipio", controladorMunicipio.consultarMunicipios(null));
		model.addObject("listaMarcaMedidor", controladorMedidor.listarMarcaMedidor());
		model.addObject("listaModeloMedidor", controladorMedidor.listarModeloMedidor());
		model.addObject("listaMarcaCorretor", controladorVazaoCorretor.listarMarcaVazaoCorretor());
		model.addObject("listaModeloCorretor", controladorVazaoCorretor.listarModeloVazaoCorretor());
		model.addObject("listaSituacaoConsumo", controladorPontoConsumo.listarSituacoesPontoConsumo());
		model.addObject("listaSegmento", controladorSegmento.listarSegmento());
		model.addObject("listaCityGateAcao", listarCityGate());
		model.addObject("listaZonaBloqueio", controladorZonaBloqueio.obterTodas(true));
		model.addObject("listaAnoFabricacaoInicio", carregarAnos());
		model.addObject("listaAnoFabricacaoFim", carregarAnos());
		model.addObject("listaModalidadeMedicaoImovel", Fachada.getInstancia().listarModalidadeMedicaoImovel());

	}
	
	/**
	 * Método responsável por carregar anos para combo.
	 * 
	 * @return Lista com anos.
	 */
	private List<Integer> carregarAnos() {
		List<Integer> anos = new ArrayList<>();

		int anoAtual = Calendar.getInstance().get(Calendar.YEAR);
		for (int ano = Constantes.ANO_1960; ano <= anoAtual; anoAtual--) {
			anos.add(anoAtual);
		}
		return anos;
	}

	/**
	 * Método responsável por listar todos os City Gate.
	 * 
	 * @return Lista de City gate
	 * @throws GGASException
	 */
	private Collection<TabelaAuxiliar> listarCityGate() throws GGASException {
		Map<String, Object> filtro = new HashMap<>();
		filtro.put(HABILITADO, true);
		Fachada fachada = Fachada.getInstancia();
		return fachada.pesquisarTabelaAuxiliar(filtro, CITY_GATE_CLASS_NAME);
	}
	
}
