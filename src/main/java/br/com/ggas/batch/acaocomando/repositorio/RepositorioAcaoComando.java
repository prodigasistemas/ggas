/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *03/01/2014
 * vpessoa
 * 17:42:15
 */

package br.com.ggas.batch.acaocomando.repositorio;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.batch.acaocomando.dominio.AcaoComandoEstendida;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoPontoConsumo;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoVO;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.Util;

/**
 * Classe Responsável pelas ações relacionadas à funcionalidade de RepositorioAcaoComando
 * 
 * @author vpessoa
 */
@Repository
public class RepositorioAcaoComando extends RepositorioGenerico {

	/**
	 * Instantiates a new repositorio acao comando.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioAcaoComando(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new AcaoComandoEstendida();
	}

	@Override
	public Class<AcaoComandoEstendida> getClasseEntidade() {

		return AcaoComandoEstendida.class;

	}

	/**
	 * Consultar comandos.
	 * 
	 * @param chaveOperacao
	 *            the chave operacao
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	public Collection<EntidadeNegocio> consultarComandos(Long chaveOperacao) {
		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.setFetchMode("acao", FetchMode.JOIN);
		criteria.createAlias("acao.operacao", "operacao", Criteria.LEFT_JOIN);
		criteria.add(Restrictions.eq("operacao.chavePrimaria", chaveOperacao));
		
		return criteria.list();
	}

	/**
	 * Consultar acao comando.
	 * 
	 * @param acaoComando
	 *            the acao comando
	 * @param habilitado
	 *            the habilitado
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<EntidadeNegocio> consultarAcaoComando(AcaoComandoVO acaoComando, Boolean habilitado) throws NegocioException {
		Criteria criteria = createCriteria(getClasseEntidade());
		criteria.setFetchMode("acao", FetchMode.JOIN);

		if(acaoComando.getDescricao() != null && !acaoComando.getDescricao().isEmpty()) {
			criteria.add(Restrictions.ilike("descricao", Util.formatarTextoConsulta(acaoComando.getDescricao())));
		}

		if(acaoComando.getNome() != null && !acaoComando.getNome().isEmpty()) {
			criteria.add(Restrictions.ilike("nome", Util.formatarTextoConsulta(acaoComando.getNome())));
		}
		
		if(acaoComando.getAcao() != null) {
			criteria.createAlias("acao", "acao");
			criteria.add(Restrictions.eq("acao.chavePrimaria", acaoComando.getAcao().getChavePrimaria()));
		}
		
		if(habilitado != null) {
			criteria.add(Restrictions.eq("habilitado", habilitado));
		}

		return criteria.list();
	}

	/**
	 * Pesquisar pontos de consumo pelo comando de acao
	 * 
	 * @param chavePrimaria - {@link long}
	 * @return Collection<AcaoComandoPontoConsumo> - {@link AcaoComandoPontoConsumo}
	 * @throws NegocioException - {@link NegocioException}
	 */
	@SuppressWarnings("unchecked")
	public Collection<AcaoComandoPontoConsumo> pesquisarPontosConsumoPorAcaoComando(long chavePrimaria) throws NegocioException {
		Criteria criteria = createCriteria(AcaoComandoPontoConsumo.class);
		criteria.setFetchMode("pontoConsumo", FetchMode.JOIN);
		criteria.add(Restrictions.eq("acaoComando.chavePrimaria", chavePrimaria));

		return criteria.list();
	}
}
