package br.com.ggas.batch.impl;

/**
 * Classe responsável pela transferência de dados entre as telas relacionadas a
 * Controle de Processos
 *
 */
public class ProcessoVO {

	private Integer ciclo;
	private Long chavePrimaria;
	private Long idGrupoFaturamento;
	private Long idRota;
	private Long idStatusNFE;
	private Long idComando;
	private Long modulo;
	private String periodoEmissao;
	private String dataInicio;
	private String dataFim;
	private String tipoDataRetroativaInvalida;
	private String dataEmissao;
	private String anoMesFaturamento;
	private String anoMesReferencia;
	private String horaInicio;
	private String horaFim;
	private String dataInicioAgendamento;
	private String dataFinalAgendamento;
	private Integer hora;
	private Boolean indicadorAgendamento;
	private Long[] rotasSelecionadas;
	
	public Integer getCiclo() {
		return ciclo;
	}
	public void setCiclo(Integer ciclo) {
		this.ciclo = ciclo;
	}
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}
	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}
	public Long getIdRota() {
		return idRota;
	}
	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}
	public Long getIdStatusNFE() {
		return idStatusNFE;
	}
	public void setIdStatusNFE(Long idStatusNFE) {
		this.idStatusNFE = idStatusNFE;
	}
	public Long getIdComando() {
		return idComando;
	}
	public void setIdComando(Long idComando) {
		this.idComando = idComando;
	}
	public Long getModulo() {
		return modulo;
	}
	public void setModulo(Long modulo) {
		this.modulo = modulo;
	}
	public String getPeriodoEmissao() {
		return periodoEmissao;
	}
	public void setPeriodoEmissao(String periodoEmissao) {
		this.periodoEmissao = periodoEmissao;
	}
	public String getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}
	public String getDataFim() {
		return dataFim;
	}
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}
	public String getTipoDataRetroativaInvalida() {
		return tipoDataRetroativaInvalida;
	}
	public void setTipoDataRetroativaInvalida(String tipoDataRetroativaInvalida) {
		this.tipoDataRetroativaInvalida = tipoDataRetroativaInvalida;
	}
	public String getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public String getAnoMesFaturamento() {
		return anoMesFaturamento;
	}
	public void setAnoMesFaturamento(String anoMesFaturamento) {
		this.anoMesFaturamento = anoMesFaturamento;
	}
	public String getAnoMesReferencia() {
		return anoMesReferencia;
	}
	public void setAnoMesReferencia(String anoMesReferencia) {
		this.anoMesReferencia = anoMesReferencia;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getHoraFim() {
		return horaFim;
	}
	public void setHoraFim(String horaFim) {
		this.horaFim = horaFim;
	}
	public String getDataInicioAgendamento() {
		return dataInicioAgendamento;
	}
	public void setDataInicioAgendamento(String dataInicioAgendamento) {
		this.dataInicioAgendamento = dataInicioAgendamento;
	}
	public String getDataFinalAgendamento() {
		return dataFinalAgendamento;
	}
	public void setDataFinalAgendamento(String dataFinalAgendamento) {
		this.dataFinalAgendamento = dataFinalAgendamento;
	}
	public Integer getHora() {
		return hora;
	}
	public void setHora(Integer hora) {
		this.hora = hora;
	}
	public Boolean getIndicadorAgendamento() {
		return indicadorAgendamento;
	}
	public void setIndicadorAgendamento(Boolean indicadorAgendamento) {
		this.indicadorAgendamento = indicadorAgendamento;
	}
	public Long[] getRotasSelecionadas() {
		return rotasSelecionadas;
	}
	public void setRotasSelecionadas(Long[] rotasSelecionadas) {
		this.rotasSelecionadas = rotasSelecionadas;
	}
}
