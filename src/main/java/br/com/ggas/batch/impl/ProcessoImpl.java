/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.batch.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.joda.time.Interval;
import org.joda.time.Period;

import br.com.ggas.batch.Processo;
import br.com.ggas.batch.ProcessoDocumento;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela representação de um processo
 */
public class ProcessoImpl extends EntidadeNegocioImpl implements Processo {

	private static final int LIMITE_CAMPO = 2;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -5829582715708265250L;

	private Usuario usuario;

	private Operacao operacao;

	private String descricao;

	private Map<String, String> parametros;

	private int situacao;

	private int periodicidade;

	private Date inicio;

	private Date fim;

	private byte[] logErro;

	private byte[] logExecucao;

	private boolean diaNaoUtil;

	private boolean agendado;

	private Date dataInicioAgendamento;

	private Date dataFinalAgendamento;

	private String emailResponsavel;

	private Processo pai;

	private Collection<Processo> filhos = new HashSet<>();

	private boolean enviarLogExecucao;
	
	private Collection<ProcessoDocumento> listaProcessoDocumento = new HashSet<>();

	private String usuarioExecucao;

	private String unidadeOrganizacionalExecucao;
	

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#isEnviarLogExecucao
	 * ()
	 */
	@Override
	public boolean isEnviarLogExecucao() {

		return enviarLogExecucao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#setEnviarLogExecucao
	 * (boolean)
	 */
	@Override
	public void setEnviarLogExecucao(boolean enviarLogExecucao) {

		this.enviarLogExecucao = enviarLogExecucao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Processo#getFilhos()
	 */
	@Override
	public Collection<Processo> getFilhos() {

		return filhos;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#setFilhos(java
	 * .util.Collection)
	 */
	@Override
	public void setFilhos(Collection<Processo> filhos) {

		this.filhos = filhos;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Processo#getPai()
	 */
	@Override
	public Processo getPai() {

		return pai;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#setPai(br.com
	 * .ggas.batch.Processo)
	 */
	@Override
	public void setPai(Processo pai) {

		this.pai = pai;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#setDescricao
	 * (java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return the tempoExecucao
	 */
	@Override
	public String getTempoExecucao() {

		StringBuilder tempoExecucao = new StringBuilder();
		Interval interval = null;
		Period period = null;

		if(Util.isAllNotNull(this.inicio) && Util.isAllNotNull(this.fim)) {
			interval = new Interval(this.inicio.getTime(), this.fim.getTime());
			period = interval.toPeriod();
			if(period.getHours() > 0) {
				tempoExecucao.append(period.getHours());
				tempoExecucao.append("h ");
			}
			if(period.getMinutes() > 0) {
				tempoExecucao.append(period.getMinutes());
				tempoExecucao.append("m ");
			}
			tempoExecucao.append(period.getSeconds());
			tempoExecucao.append("s");

		} else if(this.inicio != null && this.fim == null && situacao == SituacaoProcesso.SITUACAO_EXECUCAO) {

			if(Calendar.getInstance().getTime().getTime() >= this.inicio.getTime()) {
				interval = new Interval(this.inicio.getTime(), Calendar.getInstance().getTime().getTime());
			} else {
				interval = new Interval(this.inicio.getTime(), this.inicio.getTime());
			}

			period = interval.toPeriod();
			if(period.getHours() > 0) {
				tempoExecucao.append(period.getHours());
				tempoExecucao.append("h ");
			}
			if(period.getMinutes() > 0) {
				tempoExecucao.append(period.getMinutes());
				tempoExecucao.append("m ");
			}
			tempoExecucao.append(period.getSeconds());
			tempoExecucao.append("s");
		}

		return tempoExecucao.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.Processo#getLogErro()
	 */
	@Override
	public byte[] getLogErro() {
		byte[] retorno = null;
		if (this.logErro != null) {
			retorno = logErro.clone();
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#setLogErro(byte
	 * [])
	 */
	@Override
	public void setLogErro(byte[] logErro) {
		if(logErro != null){
			this.logErro = logErro.clone();
		} else {
			this.logErro = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.Processo#getLogExecucao()
	 */
	@Override
	public byte[] getLogExecucao() {
		byte[] retorno = null;
		if (this.logExecucao != null) {
			retorno = logExecucao.clone();
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#setLogExecucao
	 * (byte[])
	 */
	@Override
	public void setLogExecucao(byte[] logExecucao) {
		if(logExecucao != null){
			this.logExecucao = logExecucao.clone();
		} else {
			this.logExecucao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#getUsuario()
	 */
	@Override
	public Usuario getUsuario() {

		return usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#setUsuario(br
	 * .com.ggas.controleacesso.Usuario)
	 */
	@Override
	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#getParametros()
	 */
	@Override
	public Map<String, String> getParametros() {

		return parametros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#setParametros
	 * (java.util.Map)
	 */
	@Override
	public void setParametros(Map<String, String> parametros) {

		this.parametros = parametros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.impl.Processo#getOperacao
	 * ()
	 */
	@Override
	public Operacao getOperacao() {

		return operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.impl.Processo#setOperacao
	 * (br.com.ggas.controleacesso.Operacao)
	 */
	@Override
	public void setOperacao(Operacao operacao) {

		this.operacao = operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#getPeriodicidade
	 * ()
	 */
	@Override
	public int getPeriodicidade() {

		return periodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#setPeriodicidade
	 * (int)
	 */
	@Override
	public void setPeriodicidade(int periodicidade) {

		this.periodicidade = periodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.impl.Processo#getSituacao
	 * ()
	 */
	@Override
	public int getSituacao() {

		return situacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.impl.Processo#setSituacao
	 * (int)
	 */
	@Override
	public void setSituacao(int situacao) {

		this.situacao = situacao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.impl.Processo#getInicio()
	 */
	@Override
	public Date getInicio() {
		Date data = null;
		if (this.inicio != null) {
			data = (Date) inicio.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.impl.Processo#setInicio
	 * (java.util.Date)
	 */
	@Override
	public void setInicio(Date inicio) {
		if(inicio != null){
			this.inicio = (Date) inicio.clone();
		} else {
			this.inicio = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.impl.Processo#getFim()
	 */
	@Override
	public Date getFim() {
		Date data = null;
		if(this.fim != null) {
			data = (Date) this.fim.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.impl.Processo#setFim(
	 * java.util.Date)
	 */
	@Override
	public void setFim(Date fim) {
		if (fim != null) {
			this.fim = (Date) fim.clone();
		} else {
			this.fim = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.impl.Processo#isDiaNaoUtil
	 * ()
	 */
	@Override
	public boolean isDiaNaoUtil() {

		return diaNaoUtil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.impl.Processo#setDiaNaoUtil
	 * (boolean)
	 */
	@Override
	public void setDiaNaoUtil(boolean diaNaoUtil) {

		this.diaNaoUtil = diaNaoUtil;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#isAgendado()
	 */
	@Override
	public boolean isAgendado() {

		return agendado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#setAgendado(
	 * boolean)
	 */
	@Override
	public void setAgendado(boolean agendado) {

		this.agendado = agendado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#getEmailResponsavel
	 * ()
	 */
	@Override
	public String getEmailResponsavel() {

		return emailResponsavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Processo#setEmailResponsavel
	 * (java.lang.String)
	 */
	@Override
	public void setEmailResponsavel(String emailResponsavel) {

		this.emailResponsavel = emailResponsavel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.Processo# getDataInicioAgendamento()
	 */
	@Override
	public Date getDataInicioAgendamento() {
		Date data = null;
		if (this.dataInicioAgendamento != null) {
			data = (Date) dataInicioAgendamento.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Processo#
	 * setDataInicioAgendamento(java.util.Date)
	 */
	@Override
	public void setDataInicioAgendamento(Date dataInicioAgendamento) {
		if(dataInicioAgendamento != null){
			this.dataInicioAgendamento = (Date) dataInicioAgendamento.clone();
		} else {
			this.dataInicioAgendamento = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Processo#
	 * getDataFinalAgendamento()
	 */
	@Override
	public Date getDataFinalAgendamento() {
		Date data = null;
		if(this.dataFinalAgendamento != null) {
			data = (Date) dataFinalAgendamento.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Processo#
	 * setDataFinalAgendamento(java.util.Date)
	 */
	@Override
	public void setDataFinalAgendamento(Date dataFinalAgendamento) {
		if(dataFinalAgendamento != null){
			this.dataFinalAgendamento = (Date) dataFinalAgendamento.clone();
		} else {
			this.dataFinalAgendamento = null;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Processo#getListaProcessoDocumento()
	 */
	@Override
	public Collection<ProcessoDocumento> getListaProcessoDocumento() {
		return listaProcessoDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Processo#setListaProcessoDocumento(java.util.Collection)
	 */
	@Override
	public void setListaProcessoDocumento(Collection<ProcessoDocumento> listaProcessoDocumento) {
		this.listaProcessoDocumento = listaProcessoDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.operacao == null) {
			stringBuilder.append(PROCESSO_ROTULO_PROCESSO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.periodicidade < 0) {
			stringBuilder.append(PROCESSO_ROTULO_PERIODICIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.dataInicioAgendamento == null) {
			stringBuilder.append(PROCESSO_ROTULO_DATA_INICIO_AGENDAMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.situacao < 0) {
			stringBuilder.append(PROCESSO_ROTULO_SITUACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

	@Override
	public String getUsuarioExecucao() {
		return usuarioExecucao;
	}

	@Override
	public void setUsuarioExecucao(String usuarioExecucao) {
		this.usuarioExecucao = usuarioExecucao;
	}

	@Override
	public String getUnidadeOrganizacionalExecucao() {
		return unidadeOrganizacionalExecucao;
	}

	@Override
	public void setUnidadeOrganizacionalExecucao(String unidadeOrganizacionalExecucao) {
		this.unidadeOrganizacionalExecucao = unidadeOrganizacionalExecucao;
	}

}
