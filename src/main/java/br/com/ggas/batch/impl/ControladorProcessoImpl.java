/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.batch.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.criterion.Subqueries;
import org.jfree.util.Log;
import org.joda.time.DateTime;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.PeriodicidadeProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.ProcessoDocumento;
import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Recurso;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.feriado.ControladorFeriado;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.Pair;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.batch.ProcessoAction;
/**
 * Classe responsável pelas ações relacionados a um processo
 */
class ControladorProcessoImpl extends ControladorNegocioImpl implements ControladorProcesso {

	private static final String INICIO = "inicio";

	private static final String OPERACAO = "operacao";

	private static final String SITUACAO = "situacao";

	private static final String DATA_INICIO_AGENDAMENTO = "dataInicioAgendamento";

	private static final int QUINZENA = 15;

	private static final int SETE_DIAS = 7;

	private static final Logger LOG = Logger.getLogger(ControladorProcessoImpl.class);

	private static final String NUMERO_CICLO = "numeroCiclo";

	private static final String ANO_MES_FATURAMENTO = "anoMesFaturamento";

	private static final String ID_ROTA = "idRota";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String ID_CRONOGRAMA_ATIVIDADE_FATURAMENTO = "idCronogramaAtividadeFaturamento";

	private static final String QUERY_REINICIAR_PROCESSOS = "reiniciarProcessos";
	
	private static final String ANORMALIDADE_FATURAMENTO = "Anormalidades de Faturamento";
	
	public static final String ATRIBUTO_USUARIO_LOGADO = "usuarioLogado";
	
	private StringBuilder log;
	
	private StringBuilder logCabecalho;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Processo.BEAN_ID_PROCESSO);
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#
	 * getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Processo.BEAN_ID_PROCESSO);
	}

	public Class<?> getClasseEntidadeAtividadeSistema() {

		return ServiceLocator.getInstancia().getClassPorID(AtividadeSistema.BEAN_ID_ATIVIDADE_SISTEMA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#posAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void posAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Processo processo = (Processo) entidadeNegocio;
		this.verificarPeriodicidade(processo);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#
	 * consultarPendentes()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Long> consultarPendentes() throws NegocioException {

		ControladorFeriado controladorFeriado = ServiceLocator.getInstancia().getControladorFeriado();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Date dataAtual = Calendar.getInstance().getTime();

		String idMunicipio = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MUNICIPIO_IMPLANTADOS);

		Criteria criteria = getCriteria();
		criteria.setProjection(Projections.id()).add(Restrictions.le(DATA_INICIO_AGENDAMENTO, dataAtual))
						.add(Restrictions.eq(SITUACAO, SituacaoProcesso.SITUACAO_ESPERA));

		if (!controladorFeriado.isDiaUtil(dataAtual, Long.valueOf(idMunicipio), Boolean.TRUE)) {
			criteria.add(Restrictions.eq("diaNaoUtil", true));
		}

		criteria.addOrder(Order.asc(DATA_INICIO_AGENDAMENTO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.batch.ControladorProcesso#
	 * existeProcessoExecutandoOperacao
	 * (br.com.ggas.controleacesso.Operacao)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public boolean existeProcessoExecutandoOperacao(Operacao operacao) throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.setProjection(Projections.id()).add(Restrictions.eq(OPERACAO, operacao))
						.add(Restrictions.eq(SITUACAO, SituacaoProcesso.SITUACAO_EXECUCAO));

		List<Long> lista = criteria.list();

		return lista != null && !lista.isEmpty();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.ControladorProcesso#consultar
	 * ()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Processo> consultar() throws NegocioException {

		Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();

		Criteria criteria = sessao.createCriteria(getClasseEntidade());

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.ControladorProcesso#consultar
	 * (br.com.ggas.controleacesso.Operacao,
	 * java.util.Date, java.util.Date, int, int)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Pair<Long, List<Processo>> consultarProcessos(Map<String, Object> filtro) throws NegocioException {
		Criteria criteria = obterCriteriaPesquisaProcesso(filtro);
		Long totalRegistros = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
		criteria.setProjection(null);
		criteria.setFirstResult((Integer) filtro.get(ProcessoAction.OFFSET));
		criteria.setMaxResults((Integer) filtro.get(ProcessoAction.QTD_REGISTROS));
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		if (filtro.get(ProcessoAction.COLUNA_ORDENACAO) != null) {
			String colunaOrdenacao = (String) filtro.get(ProcessoAction.COLUNA_ORDENACAO);
			if ("asc".equals(filtro.get(ProcessoAction.DIRECAO_ORDENACAO))) {
				criteria.addOrder(Order.asc(colunaOrdenacao));
			} else {
				criteria.addOrder(Order.desc(colunaOrdenacao));
			}
		} else {
			criteria.addOrder(Order.desc(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA));
		}
		List<Processo> processos = criteria.list();

		return new Pair<>(totalRegistros, processos);
	}

	/**
	 * Cria um objeto Criteria a partir dos filtros informados
	 * @param filtro O mapa representando os filtros da pesquisa
	 * @return O Criteria referente ao filtro informado
	 */
	@SuppressWarnings("squid:S1192")
	private Criteria obterCriteriaPesquisaProcesso(Map<String, Object> filtro) {
		Long idModulo;
		Long idOperacao;
		Date inicio;
		Date fim;
		Integer idSituacao;
		Integer idPeriodicidade;
		Date dataInicioAgendamento;
		Date dataFinalAgendamento;
		Boolean processoPai;
		Boolean agendamento;

		Criteria criteria = getCriteria();

		if (filtro != null) {
			Long idGrupoFaturamento = (Long) filtro.get(ProcessoAction.ID_GRUPO_FATURAMENTO);
			if (idGrupoFaturamento != null && idGrupoFaturamento > 0) {
				DetachedCriteria criteriaGrupo = DetachedCriteria.forClass(ParametroProcessoImpl.class, "param");
				criteriaGrupo
						.add(Restrictions.like("parametro", ID_GRUPO_FATURAMENTO))
						.add(Restrictions.like("valor", String.valueOf(idGrupoFaturamento)))
						.add(Property.forName("param.processo.chavePrimaria").eqProperty("this.chavePrimaria"));

				criteria.add(Subqueries.exists(criteriaGrupo.setProjection(Projections.property("param.processo.chavePrimaria"))));
			}
			Long[] rotasSelecionadas = (Long[]) filtro.get(ProcessoAction.ROTAS_SELECIONADAS);
			if (rotasSelecionadas != null && rotasSelecionadas.length > 0) {
				DetachedCriteria criteriaGrupo = DetachedCriteria.forClass(ParametroProcessoImpl.class, "param");
				Junction rotaDisjunction = Restrictions.disjunction();
				for (Long rota : rotasSelecionadas) {
					SimpleExpression condicaoRota = Restrictions.like("valor", String.valueOf(rota));
					rotaDisjunction.add(condicaoRota);
				}
				criteriaGrupo
						.add(Restrictions.like("parametro", ID_ROTA))
						.add(rotaDisjunction)
						.add(Property.forName("param.processo.chavePrimaria").eqProperty("this.chavePrimaria"));
				criteria.add(Subqueries.exists(criteriaGrupo.setProjection(Projections.property("param.processo.chavePrimaria"))));
			}

			Collection<Long> idProcessos = (Collection<Long>) filtro.get("idProcessos");
			if (idProcessos != null && !idProcessos.isEmpty()) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idProcessos));
			}

			idModulo = (Long) filtro.get("idModulo");
			if (idModulo != null && idModulo > 0) {
				criteria.createCriteria(OPERACAO).add(Restrictions.eq("modulo.chavePrimaria", idModulo));
			}

			idOperacao = (Long) filtro.get("idOperacao");
			if (idOperacao != null && idOperacao > 0) {
				if (idModulo == null) {
					criteria.createCriteria(OPERACAO);
				}
				criteria.add(Restrictions.eq("operacao.chavePrimaria", idOperacao));
			}

			agendamento = (Boolean) filtro.get("indicadorAgendamento");
			if (agendamento != null) {
				criteria.add(Restrictions.eq("agendado", agendamento));
			}

			idSituacao = (Integer) filtro.get("idSituacao");
			if (idSituacao != null && idSituacao > -1) {
				criteria.add(Restrictions.eq(SITUACAO, idSituacao));
			}
			idPeriodicidade = (Integer) filtro.get("idPeriodicidade");
			if (idPeriodicidade != null && idPeriodicidade > -1) {
				criteria.add(Restrictions.eq("periodicidade", idPeriodicidade));
			}

			inicio = (Date) filtro.get(INICIO);
			if (inicio != null) {
				DateTime data = montarDateTimeFromDate(inicio);
				criteria.add(Restrictions.ge(INICIO, data.toDate()));
			}

			fim = (Date) filtro.get("fim");
			if (fim != null) {
				DateTime data = montarDateTimeFromDate(fim);
				criteria.add(Restrictions.le("fim", data.plusDays(1).minusSeconds(1).toDate()));
			}

			dataInicioAgendamento = (Date) filtro.get(DATA_INICIO_AGENDAMENTO);
			if (dataInicioAgendamento != null) {
				DateTime data = montarDateTimeFromDate(dataInicioAgendamento);
				criteria.add(Restrictions.ge(DATA_INICIO_AGENDAMENTO, data.toDate()));
			}

			dataFinalAgendamento = (Date) filtro.get("dataFinalAgendamento");
			if (dataFinalAgendamento != null) {
				DateTime data = montarDateTimeFromDate(dataFinalAgendamento);
				criteria.add(Restrictions.le("dataFinalAgendamento", data.plusDays(1).minusSeconds(1).toDate()));
			}

			processoPai = (Boolean) filtro.get("processoPai");
			if (processoPai != null) {
				criteria.add(Restrictions.isNull("pai"));
			}
		}

		return criteria;
	}

	/**
	 * Cria um objeto DateTime a partir de uma data. Os valores de tempo são zerados.
	 * @param dataObj A data de referencia
	 * @return O objeto DateTime montado
	 */
	private DateTime montarDateTimeFromDate(Date dataObj) {
		DateTime data = new DateTime(dataObj);
		data = data.withHourOfDay(0);
		data = data.withMinuteOfHour(0);
		data = data.withSecondOfMinute(0);
		data = data.withMillisOfSecond(0);
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#
	 * reiniciarProcessos()
	 */
	@Override
	public void reiniciarProcessos() throws GGASException {

		Query query = null;

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().getNamedQuery(QUERY_REINICIAR_PROCESSOS);

		query.executeUpdate();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.ControladorProcesso#executar
	 * (br.com.procenge
	 * .ggas.batch.Processo)
	 */
	@Override
	public String executar(Processo processoAtual) throws GGASException {
		ControladorProcesso controladorProcesso = (ControladorProcesso) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);

		
		
		Processo processo = processoAtual;
		processo = (Processo) controladorProcesso.obter(processo.getChavePrimaria());
		Collection<Recurso> recursos = processo.getOperacao().getRecursos();
		Recurso recurso = null;
		AtividadeSistema atividadeSistema = null;
		StringBuilder logExecucao = new StringBuilder();

		LOG.info("Executando o processo: " + processo.getChavePrimaria());
		LOG.info("Operação: " + processo.getOperacao().getDescricao());
		LOG.info("Inicio: " + processo.getInicio());
		LOG.info("Usuario: " + processo.getUsuario().getLogin());
		LOG.info("Diretorio do GGAS: " + System.getProperty("user.dir"));
		LOG.info("Usuario do PC: " + System.getProperty("user.name"));

		
		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorRota.BEAN_ID_CONTROLADOR_ROTA);

		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
						.getInstancia().getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

		for (Iterator<Recurso> iterator = recursos.iterator(); iterator.hasNext();) {
			recurso = iterator.next();
			if (!recurso.isWeb()) {
				try {
					Class<?> clazz = Class.forName(recurso.getRecurso());
					Batch batch = (Batch) ServiceLocator.getInstancia().getBean(clazz);
					
					LOG.info("Iniciando o processamento do Batch: " + batch.getClass().getName());
					
					StringBuilder logCabecalhoInicio = new StringBuilder();
					
//					logCabecalhoInicio.append("\r\n[");
//					logCabecalhoInicio.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true));
//					logCabecalhoInicio.append("] Iniciando o processamento do Batch.");
//					logCabecalhoInicio.append("\r\n");
//					logCabecalhoInicio.append("\r\n");

					boolean batchRealizadoComSucesso = true;

					Map<String, Object> parametros = new HashMap<String, Object>();
					parametros.put("processo", processo);
					parametros.put(ATRIBUTO_USUARIO_LOGADO, processo.getUsuario());
					
					boolean possuiParametro = this.adicionarParametrosDoProcesso(parametros, processo);
					
					boolean operacaoAtividadeSistema = false;
					boolean atividadeCronograma = false;
					atividadeSistema = controladorCronogramaFaturamento.obterAtividadeSistemaPorOperacao(processo.getOperacao()
									.getChavePrimaria());
					if (atividadeSistema != null) {
						operacaoAtividadeSistema = true;
						atividadeCronograma = atividadeSistema.isCronograma();
						parametros.put("atividadeSistema", atividadeSistema);
					}

					if (operacaoAtividadeSistema && (atividadeCronograma && !possuiParametro)) {

						this.criarProcessosAtividadeCronograma(atividadeSistema, processo);

					} else {

						boolean atividadePrecedenteRealizada = false;

						if (atividadeCronograma) {

							// Verificar se atividade predecessora foi realizada - [#3959]
							String idGrupoFaturamento = (String) parametros.get(ID_GRUPO_FATURAMENTO);
							GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(Long.valueOf(idGrupoFaturamento));

							CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = controladorCronogramaFaturamento
											.consultarCronogramaAtividadeFaturamento(atividadeSistema, grupoFaturamento);

							String referencia = Util.formatarAnoMes(grupoFaturamento.getAnoMesReferencia());
							int ciclo = grupoFaturamento.getNumeroCiclo();
							// FIXME: Considerar a rota conforme criarProcessosAtividadeCronograma()
							if (cronogramaAtividadeFaturamento != null) {
								atividadePrecedenteRealizada = controladorProcesso.verificarRealizacaoAtividadeSistemaPrecedente(
												atividadeSistema, null, cronogramaAtividadeFaturamento);
								if (!atividadePrecedenteRealizada) {
									logExecucao.append("\r\n Atividade predecessora não realizada. ");
									batchRealizadoComSucesso = false;
								}
							} else {
								
								logExecucao.append("\r\n Não existe cronograma da atividade solicitada para referência-ciclo ");
								logExecucao.append(referencia);
								logExecucao.append("-");
								logExecucao.append(ciclo);
								
								batchRealizadoComSucesso = false;
							}
						}

						//logCabecalhoInicio.append("\r\n Iniciando Batch de Faturar Grupo... \r\n\r\n"); 
						//logCabecalhoInicio.append("Grupo: ").append(parametros.get(ID_GRUPO_FATURAMENTO));
						//logCabecalhoInicio.append("\r\n");
						
						// se não é atividade de cronograma, pode executar, se for, só pode executar se a predecessora foi realizada.
						if (!atividadeCronograma || (atividadeCronograma && atividadePrecedenteRealizada)) {
							String logProcessar = batch.processar(parametros);
							if (logProcessar != null && logProcessar.length() > 0) {
								logExecucao.append(logProcessar);
							}

						} else {
							logExecucao.append("\r\n Atividade predecessora não realizada. ");
							batchRealizadoComSucesso = false;
						}

					}
					LOG.info("Batch finalizado com sucesso: " + batch.getClass().getName());
					
					this.setLog(logExecucao);
					this.setLogCabecalho(logCabecalhoInicio);
					
					this.adicionarBatchFinalizadoAoArquivo(batchRealizadoComSucesso, logExecucao); 
									
				} catch (ClassNotFoundException | SecurityException e) {
					throw new GGASException(e);
				}
			}
		}

		return logExecucao.toString();

	}
	/**
	 * enviar Email
	 * 
	 * @param emailResponsavel
	 *   the atividade emailResponsavel
	 * @param relatorioProcesso
	 *    the atividade relatorioProcesso      
	 * @throws NegocioException
	 */
	public void enviarEmail(String emailResponsavel, String relatorioProcesso) throws GGASException {
		
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		
		ParametroSistema parametroSistema = controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);					
		
		JavaMailUtil email = new JavaMailUtil();
		
		String montarEmail = null;
		montarEmail = this.logCabecalho.toString() + this.log.toString();

		email.enviar(parametroSistema.getValor(), emailResponsavel, "Autorização de Serviço para Comunicação em Lote", montarEmail, false, false);
		
	}
	
	private boolean adicionarParametrosDoProcesso(Map<String, Object> parametros, Processo processo){
		boolean possuiParametro = false;
		if (processo.getParametros() != null && !processo.getParametros().isEmpty()) {
			possuiParametro = true;
			parametros.putAll(processo.getParametros());
		}
		return possuiParametro;
	}
	
	private void adicionarBatchFinalizadoAoArquivo(boolean batchRealizadoComSucesso,StringBuilder logExecucao){
		if (batchRealizadoComSucesso) {
			logExecucao.append("\r\n[");
			logExecucao.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true));
			logExecucao.append("] Batch finalizado com sucesso.");
		}
	}
	
	
	/**
	 * Criar processos atividade cronograma.
	 * 
	 * @param atividadeSistema
	 *            the atividade sistema
	 * @param processo
	 *            the processo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void criarProcessosAtividadeCronograma(AtividadeSistema atividadeSistema, Processo processo) throws NegocioException {

		if (processo.getPai() == null) {

			ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(
							ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

			ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
							.getInstancia().getBeanPorID(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

			Collection<GrupoFaturamento> listaGrupoFaturamento = controladorFatura.consultarGrupoFaturamentoAnoMesReferencia();

			// Collection<CronogramaAtividadeFaturamento>
			// cronogramaAtividadeFaturamentos =
			// controladorCronogramaFaturamento
			Collection<CronogramaAtividadeFaturamento> cronogramaAtividadeFaturamentos = new ArrayList<CronogramaAtividadeFaturamento>();

			for (GrupoFaturamento grupoFaturamento : listaGrupoFaturamento) {
				Collection<CronogramaAtividadeFaturamento> cronogramaAtividadeFaturamentosAux = controladorCronogramaFaturamento
								.consultarCronogramaAtividadeFaturamentoPorGrupoFaturamentoAnoMesCiclo(grupoFaturamento.getChavePrimaria(),
												grupoFaturamento.getAnoMesReferencia(), grupoFaturamento.getNumeroCiclo(), atividadeSistema);
				for (CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento : cronogramaAtividadeFaturamentosAux) {
					cronogramaAtividadeFaturamentos.add(cronogramaAtividadeFaturamento);
				}
			}

			boolean atividadePrecedenteRealizada = false;

			for (CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento : cronogramaAtividadeFaturamentos) {
				if (cronogramaAtividadeFaturamento.getDataRealizacao() == null
								&& processo.getInicio().compareTo(cronogramaAtividadeFaturamento.getDataInicio()) >= 0) {
					GrupoFaturamento grupoFaturamento = cronogramaAtividadeFaturamento.getCronogramaFaturamento().getGrupoFaturamento();
					Collection<CronogramaRota> cronogramaRotas = controladorCronogramaFaturamento
									.consultarCronogramaRota(cronogramaAtividadeFaturamento);

					if (cronogramaRotas == null || cronogramaRotas.isEmpty()) {
						atividadePrecedenteRealizada = verificarRealizacaoAtividadeSistemaPrecedente(atividadeSistema, null,
										cronogramaAtividadeFaturamento);
						if (atividadePrecedenteRealizada) {
							Processo novoProcesso = criarProcessoCronograma(processo);
							novoProcesso.getParametros().put(ID_CRONOGRAMA_ATIVIDADE_FATURAMENTO,
											String.valueOf(cronogramaAtividadeFaturamento.getChavePrimaria()));
							novoProcesso.getParametros().put(ID_GRUPO_FATURAMENTO, String.valueOf(grupoFaturamento.getChavePrimaria()));
							novoProcesso.getParametros()
											.put(ANO_MES_FATURAMENTO,
															String.valueOf(cronogramaAtividadeFaturamento.getCronogramaFaturamento()
																			.getAnoMesFaturamento()));
							novoProcesso.getParametros().put(NUMERO_CICLO,
											String.valueOf(cronogramaAtividadeFaturamento.getCronogramaFaturamento().getNumeroCiclo()));
							novoProcesso.setDescricao(processo.getDescricao() + " Grupo Faturamento: "
											+ grupoFaturamento.getDescricaoAbreviada());

							this.inserir(novoProcesso);
						} else {
							throw new NegocioException(Constantes.ERRO_NEGOCIO_ATIVIDADE_PREDECESSORA_NAO_REALIZADA, false);
						}
					} else {
						for (CronogramaRota cronogramaRota : cronogramaRotas) {
							atividadePrecedenteRealizada = verificarRealizacaoAtividadeSistemaPrecedente(atividadeSistema, cronogramaRota,
											null);
							if (atividadePrecedenteRealizada) {
								Processo novoProcesso = criarProcessoCronograma(processo);
								novoProcesso.getParametros().put(ID_CRONOGRAMA_ATIVIDADE_FATURAMENTO,
												String.valueOf(cronogramaAtividadeFaturamento.getChavePrimaria()));
								novoProcesso.getParametros().put(ID_GRUPO_FATURAMENTO, String.valueOf(grupoFaturamento.getChavePrimaria()));
								novoProcesso.getParametros().put(ID_ROTA, String.valueOf(cronogramaRota.getRota().getChavePrimaria()));
								novoProcesso.getParametros().put(
												ANO_MES_FATURAMENTO,
												String.valueOf(cronogramaAtividadeFaturamento.getCronogramaFaturamento()
																.getAnoMesFaturamento()));
								novoProcesso.getParametros().put(NUMERO_CICLO,
												String.valueOf(cronogramaAtividadeFaturamento.getCronogramaFaturamento().getNumeroCiclo()));
								novoProcesso.setDescricao(processo.getDescricao() + " Grupo Faturamento: "
												+ grupoFaturamento.getDescricaoAbreviada() + " Rota: "
												+ cronogramaRota.getRota().getNumeroRota());

								this.inserir(novoProcesso);

							} else {
								throw new NegocioException(Constantes.ERRO_NEGOCIO_ATIVIDADE_PREDECESSORA_NAO_REALIZADA, false);
							}
						}
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.ControladorProcesso#verificarRealizacaoAtividadeSistemaPrecedente(br.com.ggas.faturamento.cronograma.AtividadeSistema
	 * , br.com.ggas.medicao.rota.CronogramaRota, br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento)
	 */
	@Override
	public boolean verificarRealizacaoAtividadeSistemaPrecedente(AtividadeSistema atividadeSistema, CronogramaRota cronogramaRota,
					CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento) throws NegocioException {

		boolean atividadePrecedenteRealizada = false;
		AtividadeSistema atividadeSistemaPrecedente = atividadeSistema.getAtividadePrecedente();

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getBeanPorID(
						ControladorRota.BEAN_ID_CONTROLADOR_ROTA);

		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
						.getInstancia().getBeanPorID(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

		if (atividadeSistemaPrecedente != null && atividadeSistemaPrecedente.getChavePrimaria() > 0) {
			if (cronogramaRota != null) {
				CronogramaRota cronogramaRotaPrecedente = controladorRota.obterCronogramaRotaAtual(cronogramaRota.getRota(),
								atividadeSistemaPrecedente.getChavePrimaria(), true, true);
				if (cronogramaRotaPrecedente != null && cronogramaRotaPrecedente.getDataRealizada() != null) {
					atividadePrecedenteRealizada = true;
				}
			} else {
				CronogramaAtividadeFaturamento cronogramaAtividadeFaturamentoPrecedente = controladorCronogramaFaturamento
								.consultarCronogramaAtividadeFaturamento(atividadeSistemaPrecedente,
												cronogramaAtividadeFaturamento.getCronogramaFaturamento());
				if (cronogramaAtividadeFaturamentoPrecedente != null
								&& cronogramaAtividadeFaturamentoPrecedente.getDataRealizacao() != null) {
					atividadePrecedenteRealizada = true;
				}
			}
		} else {
			atividadePrecedenteRealizada = true;
		}

		return atividadePrecedenteRealizada;
	}

	/**
	 * Criar processo cronograma.
	 * 
	 * @param processo
	 *            the processo
	 * @return the processo
	 */
	private Processo criarProcessoCronograma(Processo processo) {

		Processo novoProcesso = null;
		novoProcesso = (Processo) criar();
		novoProcesso.setPai(processo);
		novoProcesso.setChavePrimaria(0);
		novoProcesso.setVersao(0);
		novoProcesso.setUsuario(processo.getUsuario());
		novoProcesso.setOperacao(processo.getOperacao());
		novoProcesso.setPeriodicidade(PeriodicidadeProcesso.SEM_PERIODICIDADE);
		novoProcesso.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
		novoProcesso.setInicio(null);
		novoProcesso.setFim(null);
		novoProcesso.setLogErro(null);
		novoProcesso.setLogExecucao(null);
		novoProcesso.setAgendado(true);
		novoProcesso.setDiaNaoUtil(processo.isDiaNaoUtil());
		novoProcesso.setDataInicioAgendamento(Calendar.getInstance().getTime());
		novoProcesso.setParametros(new HashMap<String, String>());
		novoProcesso.setEmailResponsavel(processo.getEmailResponsavel());
		return novoProcesso;
	}

	/**
	 * Criar periodicidade processo.
	 * 
	 * @return the periodicidade processo
	 */
	private PeriodicidadeProcesso criarPeriodicidadeProcesso() {

		return (PeriodicidadeProcesso) ServiceLocator.getInstancia().getBeanPorID(PeriodicidadeProcesso.BEAN_ID_PERIODICIDADE_PROCESSO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#
	 * listarPeriodicidadeProcesso()
	 */
	@Override
	public Collection<PeriodicidadeProcesso> listarPeriodicidadeProcesso() {

		Collection<PeriodicidadeProcesso> periodicidades = new ArrayList<PeriodicidadeProcesso>();
		periodicidades.add(this.obterPeriodicidadeProcesso(PeriodicidadeProcesso.PERIODICIDADE_DIARIO));
		periodicidades.add(this.obterPeriodicidadeProcesso(PeriodicidadeProcesso.PERIODICIDADE_HORARIA));
		periodicidades.add(this.obterPeriodicidadeProcesso(PeriodicidadeProcesso.PERIODICIDADE_MENSAL));
		periodicidades.add(this.obterPeriodicidadeProcesso(PeriodicidadeProcesso.SEM_PERIODICIDADE));
		periodicidades.add(this.obterPeriodicidadeProcesso(PeriodicidadeProcesso.PERIODICIDADE_QUINZENAL));
		periodicidades.add(this.obterPeriodicidadeProcesso(PeriodicidadeProcesso.PERIODICIDADE_SEMANAL));

		return periodicidades;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#
	 * obterPeriodicidadeProcesso(int)
	 */
	@Override
	public PeriodicidadeProcesso obterPeriodicidadeProcesso(int codigo) {

		PeriodicidadeProcesso periodicidadeProcesso = null;

		switch (codigo) {
			case PeriodicidadeProcesso.SEM_PERIODICIDADE :
				periodicidadeProcesso = this.criarPeriodicidadeProcesso();
				periodicidadeProcesso.setCodigo(PeriodicidadeProcesso.SEM_PERIODICIDADE);
				break;
			case PeriodicidadeProcesso.PERIODICIDADE_HORARIA :
				periodicidadeProcesso = this.criarPeriodicidadeProcesso();
				periodicidadeProcesso.setCodigo(PeriodicidadeProcesso.PERIODICIDADE_HORARIA);
				break;
			case PeriodicidadeProcesso.PERIODICIDADE_DIARIO :
				periodicidadeProcesso = this.criarPeriodicidadeProcesso();
				periodicidadeProcesso.setCodigo(PeriodicidadeProcesso.PERIODICIDADE_DIARIO);
				break;
			case PeriodicidadeProcesso.PERIODICIDADE_SEMANAL :
				periodicidadeProcesso = this.criarPeriodicidadeProcesso();
				periodicidadeProcesso.setCodigo(PeriodicidadeProcesso.PERIODICIDADE_SEMANAL);
				break;
			case PeriodicidadeProcesso.PERIODICIDADE_QUINZENAL :
				periodicidadeProcesso = this.criarPeriodicidadeProcesso();
				periodicidadeProcesso.setCodigo(PeriodicidadeProcesso.PERIODICIDADE_QUINZENAL);
				break;
			case PeriodicidadeProcesso.PERIODICIDADE_MENSAL :
				periodicidadeProcesso = this.criarPeriodicidadeProcesso();
				periodicidadeProcesso.setCodigo(PeriodicidadeProcesso.PERIODICIDADE_MENSAL);
				break;
			default :
				break;
		}

		return periodicidadeProcesso;
	}

	/**
	 * Criar situacao processo.
	 * 
	 * @return the situacao processo
	 */
	private SituacaoProcesso criarSituacaoProcesso() {

		return (SituacaoProcesso) ServiceLocator.getInstancia().getBeanPorID(SituacaoProcesso.BEAN_ID_SITUACAO_PROCESSO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.ControladorProcesso#
	 * listarSituacaoProcesso()
	 */
	@Override
	public Collection<SituacaoProcesso> listarSituacaoProcesso() {

		Collection<SituacaoProcesso> situacoes = new ArrayList<SituacaoProcesso>();
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_ESPERA));
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_EXECUCAO));
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_CONCLUIDO));
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_ERRO));
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_CANCELADO));
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_ABORTADO));
		return situacoes;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.ControladorProcesso#
	 * obterSituacaoProcesso(int)
	 */
	@Override
	public SituacaoProcesso obterSituacaoProcesso(int codigo) {

		SituacaoProcesso situacaoProcesso = null;
		if (codigo == SituacaoProcesso.SITUACAO_ESPERA) {
			situacaoProcesso = this.criarSituacaoProcesso();
			situacaoProcesso.setCodigo(SituacaoProcesso.SITUACAO_ESPERA);
		} else if (codigo == SituacaoProcesso.SITUACAO_EXECUCAO) {
			situacaoProcesso = this.criarSituacaoProcesso();
			situacaoProcesso.setCodigo(SituacaoProcesso.SITUACAO_EXECUCAO);
		} else if (codigo == SituacaoProcesso.SITUACAO_CONCLUIDO) {
			situacaoProcesso = this.criarSituacaoProcesso();
			situacaoProcesso.setCodigo(SituacaoProcesso.SITUACAO_CONCLUIDO);
		} else if (codigo == SituacaoProcesso.SITUACAO_ERRO) {
			situacaoProcesso = this.criarSituacaoProcesso();
			situacaoProcesso.setCodigo(SituacaoProcesso.SITUACAO_ERRO);
		} else if (codigo == SituacaoProcesso.SITUACAO_CANCELADO) {
			situacaoProcesso = this.criarSituacaoProcesso();
			situacaoProcesso.setCodigo(SituacaoProcesso.SITUACAO_CANCELADO);
		} else if (codigo == SituacaoProcesso.SITUACAO_ABORTADO) {
			situacaoProcesso = this.criarSituacaoProcesso();
			situacaoProcesso.setCodigo(SituacaoProcesso.SITUACAO_ABORTADO);
		}

		return situacaoProcesso;
	}

	/**
	 * Verificar periodicidade.
	 * 
	 * @param processo
	 *            the processo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarPeriodicidade(Processo processo) throws NegocioException {

		int periodicidade = processo.getPeriodicidade();

		if (processo.getSituacao() == SituacaoProcesso.SITUACAO_CONCLUIDO && periodicidade > 0) {
			this.reprogramarExecucao(processo);
		}

	}

	/**
	 * Reprogramar execucao.
	 * 
	 * @param processo
	 *            the processo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void reprogramarExecucao(Processo processo) throws NegocioException {

		ControladorFeriado controladorFeriado = ServiceLocator.getInstancia().getControladorFeriado();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		DateTime dataInicioAgendamento = new DateTime();
		Processo novoProcesso = null;
		int periodicidade = processo.getPeriodicidade();

		novoProcesso = (Processo) criar();
		novoProcesso.setChavePrimaria(0);
		novoProcesso.setVersao(0);
		novoProcesso.setParametros(this.copiarParametros(processo.getParametros()));
		novoProcesso.setUsuario(processo.getUsuario());
		novoProcesso.setOperacao(processo.getOperacao());
		novoProcesso.setPeriodicidade(periodicidade);
		novoProcesso.setSituacao(SituacaoProcesso.SITUACAO_ESPERA);
		novoProcesso.setInicio(null);
		novoProcesso.setFim(null);
		novoProcesso.setLogErro(null);
		novoProcesso.setLogExecucao(null);
		novoProcesso.setAgendado(processo.isAgendado());
		novoProcesso.setDiaNaoUtil(processo.isDiaNaoUtil());
		novoProcesso.setDescricao(processo.getDescricao());
		novoProcesso.setEmailResponsavel(processo.getEmailResponsavel());

		DateTime dataFinalAgendamento = null;

		if (processo.getDataFinalAgendamento() != null) {
			novoProcesso.setDataFinalAgendamento(processo.getDataFinalAgendamento());
			dataFinalAgendamento = new DateTime(processo.getDataFinalAgendamento());
		}

		dataInicioAgendamento = new DateTime(processo.getDataInicioAgendamento());
		switch (novoProcesso.getPeriodicidade()) {
			case PeriodicidadeProcesso.PERIODICIDADE_DIARIO :
				dataInicioAgendamento = dataInicioAgendamento.plusDays(1);
				break;
			case PeriodicidadeProcesso.PERIODICIDADE_SEMANAL :
				dataInicioAgendamento = dataInicioAgendamento.plusDays(SETE_DIAS);
				break;
			case PeriodicidadeProcesso.PERIODICIDADE_MENSAL :
				dataInicioAgendamento = dataInicioAgendamento.plusMonths(1);
				break;
			case PeriodicidadeProcesso.PERIODICIDADE_QUINZENAL :
				dataInicioAgendamento = dataInicioAgendamento.plusDays(QUINZENA);
				break;
			case PeriodicidadeProcesso.PERIODICIDADE_HORARIA :
				dataInicioAgendamento = dataInicioAgendamento.plusHours(1);
				break;
			default :
				break;
		}

		String idMunicipio = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MUNICIPIO_IMPLANTADOS);

		if (!novoProcesso.isDiaNaoUtil() && !controladorFeriado.isDiaUtil(dataInicioAgendamento.toDate(), Long.valueOf(idMunicipio), Boolean.TRUE)) {
			dataInicioAgendamento = new DateTime(controladorFeriado.obterProximoDiaUtil(dataInicioAgendamento.toDate(),
							Long.parseLong(idMunicipio), Boolean.TRUE));
		}

		novoProcesso.setDataInicioAgendamento(dataInicioAgendamento.toDate());

		if (dataFinalAgendamento != null && dataInicioAgendamento.compareTo(dataFinalAgendamento) <= 0) {
			this.inserir(novoProcesso);
		}
	}

	/**
	 * Copiar parametros.
	 * 
	 * @param parametros
	 *            the parametros
	 * @return the map
	 */
	private Map<String, String> copiarParametros(Map<String, String> parametros) {

		Map<String, String> copia = new HashMap<String, String>();
		Entry<?, ?> entry = null;

		for (Iterator<?> it = parametros.entrySet().iterator(); it.hasNext();) {
			entry = (Entry<?, ?>) it.next();
			copia.put((String) entry.getKey(), (String) entry.getValue());
		}

		return copia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#
	 * listarSituacoesAlteracaoAgendamentoProcesso
	 * ()
	 */
	@Override
	public Collection<SituacaoProcesso> listarSituacoesAlteracaoAgendamentoProcesso() throws NegocioException {

		Collection<SituacaoProcesso> situacoes = new ArrayList<SituacaoProcesso>();
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_ABORTADO));
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_CANCELADO));
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_CONCLUIDO));
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_ERRO));
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_ESPERA));
		situacoes.add(this.obterSituacaoProcesso(SituacaoProcesso.SITUACAO_EXECUCAO));

		return situacoes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#
	 * validarAlteracaoAgendamentoProcesso
	 * (java.lang.Long)
	 */
	@Override
	public void validarAlteracaoAgendamentoProcesso(Long chavePrimariaProcesso) throws NegocioException {

		if (chavePrimariaProcesso == null || chavePrimariaProcesso <= 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_SELECAO_DE_CHAVES, true);
		}

		Processo processoAgendado = (Processo) this.obter(chavePrimariaProcesso);
		this.getHibernateTemplate().getSessionFactory().getCurrentSession().evict(processoAgendado);
		if (!processoAgendado.isAgendado()) {
			throw new NegocioException(ERRO_NEGOCIO_PROCESSO_NAO_AGENDADO, true);
		} else {
			if (processoAgendado.getSituacao() != SituacaoProcesso.SITUACAO_CANCELADO
							&& processoAgendado.getSituacao() != SituacaoProcesso.SITUACAO_ESPERA) {
				throw new NegocioException(ERRO_NEGOCIO_SITUACAO_PROCESSO_INVALIDA_ALTERACAO, new Object[] {this.obterSituacaoProcesso(
								processoAgendado.getSituacao()).getDescricao()});
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Processo processo = (Processo) entidadeNegocio;

		validarDataAgendada(processo);
		validarEmailResponsavel(processo);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Processo processo = (Processo) entidadeNegocio;

		validarDataAgendada(processo);
		validarEmailResponsavel(processo);
	}

	/**
	 * Validar data agendada.
	 * 
	 * @param processo
	 *            the processo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDataAgendada(Processo processo) throws NegocioException {

		ControladorFeriado controladorFeriado = ServiceLocator.getInstancia().getControladorFeriado();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		String idMunicipio = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MUNICIPIO_IMPLANTADOS);

		if (!processo.isDiaNaoUtil() && !controladorFeriado.isDiaUtil(processo.getDataInicioAgendamento(), Long.valueOf(idMunicipio), Boolean.TRUE)) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_AGENDADA_DIA_NAO_UTIL, true);
		}
	}

	/**
	 * Validar email responsavel.
	 * 
	 * @param processo
	 *            the processo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarEmailResponsavel(Processo processo) throws NegocioException {

		if ((processo.getEmailResponsavel() != null) && (!StringUtils.isEmpty(processo.getEmailResponsavel()))) {
			String emailProcesso = processo.getEmailResponsavel().replaceAll(",", ";");
			String[] emails = emailProcesso.split(";");
			if (emails.length != 0) {
				for (String email : emails) {
					if (!Util.validarDominio(email.trim(), Constantes.EXPRESSAO_REGULAR_EMAIL)) {
						throw new NegocioException(ERRO_NEGOCIO_EMAIL_INVALIDO, Processo.EMAIL);
					}
				}
			} else {
				if (!Util.validarDominio(emailProcesso, Constantes.EXPRESSAO_REGULAR_EMAIL)) {
					throw new NegocioException(ERRO_NEGOCIO_EMAIL_INVALIDO, Processo.EMAIL);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#
	 * atualizacaoAgendamento
	 * (br.com.ggas.batch.Processo)
	 */
	@Override
	public void atualizarAgendamento(Processo processo) throws NegocioException, ConcorrenciaException {

		validarAlteracaoAgendamentoProcesso(processo.getChavePrimaria());

		this.atualizar(processo);
	}

	/**
	 * Atualizar via batch.
	 * 
	 * @param entidadeNegocio
	 *            the entidade negocio
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void atualizarViaBatch(EntidadeNegocio entidadeNegocio) throws NegocioException {

		entidadeNegocio.incrementarVersao();
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(entidadeNegocio);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#
	 * validarDataAgendamento
	 * (br.com.ggas.batch.Processo)
	 */
	@Override
	public void validarDataAgendamento(Processo processo, String horaInicio, String horaFim) throws NegocioException {

		DateTime dateTimeProcesso = new DateTime(processo.getDataInicioAgendamento());
		DateTime dateTime = new DateTime();

		if (dateTimeProcesso.isBefore(dateTime)) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_AGENDAMENTO_MENOR_QUE_DATA_ATUAL, true);
		}

		if (processo.getDataInicioAgendamento() != null) {
			Calendar data = Calendar.getInstance();
			data.setTime(processo.getDataInicioAgendamento());

			Integer horaInicioAux = null;
			if (!StringUtils.isEmpty(horaInicio)) {
				horaInicioAux = Integer.valueOf(horaInicio);
			}

			Integer horaFimAux = null;
			if (!StringUtils.isEmpty(horaFim)) {
				horaFimAux = Integer.valueOf(horaFim);
			}

			int horaTela = data.get(Calendar.HOUR_OF_DAY);

			if ((horaInicioAux != null) && (horaFimAux == null)) {
				if (data.get(Calendar.HOUR_OF_DAY) <= horaInicioAux) {
					throw new NegocioException(ERRO_NEGOCIO_DATA_INICIAL_MENOR_QUE_DATA_PROCESSO, horaInicio);
				}
			} else if ((horaInicioAux == null) && (horaFimAux != null)) {
				if (data.get(Calendar.HOUR_OF_DAY) >= horaFimAux) {
					throw new NegocioException(ERRO_NEGOCIO_DATA_FINAL_MAIOR_QUE_DATA_PROCESSO, horaFim);
				}
			} else if ((horaInicioAux != null && horaFimAux != null) && (horaTela < horaInicioAux || horaTela > horaFimAux)) {
				throw new NegocioException(ERRO_NEGOCIO_DATA_AGENDAMENTO_FORA_INTERVALO_PROCESSO, horaInicio + " à " + horaFim);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#validarDatasPesquisaProcesso(java.lang.String, java.lang.String)
	 */
	@Override
	public void validarDatasPesquisaProcesso(String dataInicioAgendamento, String dataFinalAgendamento) throws GGASException {

		Date dataInicio = Util.converterCampoStringParaData("Data", dataInicioAgendamento, Constantes.FORMATO_DATA_BR);
		Date dataFinal = Util.converterCampoStringParaData("Data", dataFinalAgendamento, Constantes.FORMATO_DATA_BR);

		if (dataInicio.after(dataFinal)) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_AGENDAMENTO_INICIAL_MAIOR_DATA_AGENDAMENTO_FINAL, true);
		}
	}

	/**
	 * Validar situacao cancelamento.
	 * 
	 * @param processo
	 *            the processo
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public void validarSituacaoCancelamento(Processo processo) throws GGASException {

		if (processo.getSituacao() != SituacaoProcesso.SITUACAO_ESPERA) {
			throw new NegocioException(ERRO_NEGOCIO_SITUACAO_PROCESSO_INVALIDA_CANCELAMENTO, new Object[] {this.obterSituacaoProcesso(
							processo.getSituacao()).getDescricao()});
		}
	}

	/**
	 * Validar situacao.
	 * 
	 * @param listaProcesso
	 *            the lista processo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarSituacao(Collection<Processo> listaProcesso) throws NegocioException {

		if (!listaProcesso.isEmpty()) {
			for (Processo processo : listaProcesso) {
				if (processo.getSituacao() != SituacaoProcesso.SITUACAO_ESPERA) {
					throw new NegocioException(ERRO_NEGOCIO_SITUACAO_PROCESSO_INVALIDA_CANCELAMENTO, true);
				}
			}
		}
	}

	/**
	 * Listar processo chaves primarias.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings({"squid:S1192", "unchecked"})
	private Collection<Processo> listarProcessoChavesPrimarias(Long[] chavesPrimarias) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" chavePrimaria in (:chavesPrimarias) ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("chavesPrimarias", chavesPrimarias);
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#
	 * cancelarAgendamento
	 * (br.com.ggas.batch.Processo)
	 */
	@Override
	public void cancelarAgendamento(Long[] chavesPrimarias) throws NegocioException, ConcorrenciaException {

		Collection<Processo> listaProcesso = listarProcessoChavesPrimarias(chavesPrimarias);

		if ((listaProcesso != null) && (!listaProcesso.isEmpty())) {
			this.validarSituacao(listaProcesso);
			for (Processo processo : listaProcesso) {
				if (processo.getSituacao() == SituacaoProcesso.SITUACAO_ESPERA) {
					processo.setSituacao(SituacaoProcesso.SITUACAO_CANCELADO);
					this.atualizar(processo);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#getProximoProcessoEmEsperaPorOperacao(br.com.ggas.controleacesso.Operacao)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Processo getProximoProcessoEmEsperaPorOperacao(Operacao operacao) {

		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq(SITUACAO, SituacaoProcesso.SITUACAO_ESPERA));
		Processo retorno = null;

		Criteria criteriaOperacao = criteria.createCriteria(OPERACAO);
		criteriaOperacao.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, operacao.getChavePrimaria()));

		criteria.addOrder(Order.asc(INICIO));

		List<Processo> lista = criteria.list();
		if (lista != null && !lista.isEmpty()) {
			retorno = lista.get(0);
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#obterAtividadeSistema(br.com.ggas.controleacesso.Operacao)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public AtividadeSistema obterAtividadeSistema(Operacao operacao) {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeAtividadeSistema().getSimpleName());
		hql.append(" atividadeSistema ");
		hql.append(" where ");
		hql.append(" atividadeSistema.operacao.chavePrimaria = :chavePrimaria ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, operacao.getChavePrimaria());

		return (AtividadeSistema) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#obterAtividadeSistemaPorChaveOperacao(java.lang.Long)
	 */
	@Override
	public AtividadeSistema obterAtividadeSistemaPorChaveOperacao(Long idOperacao) {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeAtividadeSistema().getSimpleName());
		hql.append(" atividadeSistema ");
		hql.append(" where ");
		hql.append(" atividadeSistema.operacao.chavePrimaria = :chavePrimaria ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, idOperacao);

		return (AtividadeSistema) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#obterAtividadeSistema(java.lang.Long)
	 */
	@Override
	public AtividadeSistema obterAtividadeSistema(Long chavePrimaria) {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeAtividadeSistema().getSimpleName());
		hql.append(" atividadeSistema ");
		hql.append(" where ");
		hql.append(" atividadeSistema.chavePrimaria = :chavePrimaria ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);

		return (AtividadeSistema) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#
	 * validarEmailAtividadeSelecionada
	 * (br.com.ggas.batch.Processo)
	 */
	@Override
	public void validarEmailObrigatorioAtividadeSelecionada(Processo processo) throws NegocioException {
		//Metódo não é utilizado nessa classe.
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcesso#salvarRelatorio(br.com.ggas.batch.Processo, br.com.ggas.batch.ControladorProcessoDocumento, java.io.ByteArrayInputStream, java.lang.String, java.lang.String)
	 */
	@Override
	public void salvarRelatorio(Processo processo, ControladorProcessoDocumento controladorProcessoDocumento,
			ByteArrayInputStream byteArrayInputStream, final String nomeDocumento, String diretorioDocumento)
			throws NegocioException, IOException {
		File pdf;
		FileOutputStream saida;
		pdf = Util.getFile(diretorioDocumento);
		saida = new FileOutputStream(pdf);
		
		ProcessoDocumento processoDocumento =
				controladorProcessoDocumento.criaEPopulaProcessoDocumento(processo, diretorioDocumento, nomeDocumento);
		controladorProcessoDocumento.inserirProcessoDocumento(processoDocumento);
		
		int data;
		while ((data = byteArrayInputStream.read()) != -1) {
			char ch = (char) data;
			saida.write(ch);
		}
		saida.flush();
		saida.close();
	}
	
	public StringBuilder getLog() {
		return log;
	}

	public void setLog(StringBuilder logExecucao) {
		this.log = logExecucao;
	}

	public StringBuilder getLogCabecalho() {
		return logCabecalho;
	}

	public void setLogCabecalho(StringBuilder logCabecalho) {
		this.logCabecalho = logCabecalho;
	}

}
