/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.batch.impl;

import java.util.Map;

import br.com.ggas.batch.Processo;
import br.com.ggas.batch.ProcessoDocumento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

/**
 * Classe que contem os caminhos dos diretórios 
 * em que os relatórios gerados pelos processos foram salvos.
 * 
 * @author lcavalcanti
 *
 */
public class ProcessoDocumentoImpl extends EntidadeNegocioImpl implements ProcessoDocumento {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -8209288097450392687L;

	private Processo processo;
	private String diretorioDocumento;
	private String nomeDocumento;
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ProcessoDocumento#getDiretorioDocumento()
	 */
	@Override
	public String getDiretorioDocumento() {
		return diretorioDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ProcessoDocumento#setDiretorioDocumento(java.lang.String)
	 */
	@Override
	public void setDiretorioDocumento(String diretorioDocumento) {
		this.diretorioDocumento = diretorioDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ProcessoDocumento#getProcesso()
	 */
	@Override
	public Processo getProcesso() {
		return processo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ProcessoDocumento#setProcesso(br.com.ggas.batch.Processo)
	 */
	@Override
	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ProcessoDocumento#getNomeDocumento()
	 */
	@Override
	public String getNomeDocumento() {
		return nomeDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ProcessoDocumento#setNomeDocumento(java.lang.String)
	 */
	@Override
	public void setNomeDocumento(String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}
}
