/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.batch.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.ProcessoDocumento;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 * Classe responsável pela manipulação e pela 
 * aplicação das regras de negócio na entidade
 * ProcessoDocumento que representa todos os 
 * documentos gerados pelos processos executados.
 * A entidade ProcessoDocumento retem as informações
 * referentes ao caminho onde o arquivo foi gerado 
 * para posterior impressão dos mesmos.
 *  
 */
class ControladorProcessoDocumentoImpl extends ControladorNegocioImpl implements ControladorProcessoDocumento {
	
	/**
	 * Constante PROCESSO
	 */
	private static final String PROCESSO = "processo";
	
	/**
	 * Constante CHAVE_PRIMARIA_PROCESSO
	 */
	private static final String CHAVE_PRIMARIA_PROCESSO = "processo.chavePrimaria";

	/**
	 * Constante DIRETORIO_DOCUMENTO
	 */
	private static final String DIRETORIO_DOCUMENTO = "diretorioDocumento";

	/**
	 * Constante NOME_DOCUMENTO
	 */
	private static final String NOME_DOCUMENTO = "nomeDocumento";


	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ProcessoDocumento.BEAN_ID_PROCESSO_DOCUMENTO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(ProcessoDocumento.BEAN_ID_PROCESSO_DOCUMENTO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.ControladorProcessoDocumento#insereProcessoDocumento(br.com.ggas.batch.ProcessoDocumento)
	 */
	@Override
	public void inserirProcessoDocumento(ProcessoDocumento processoDocumento) throws NegocioException {
		if(processoDocumento!=null){
			inserir(processoDocumento);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.ControladorProcessoDocumento#criaEPopulaProcessoDocumento(br.com.ggas.batch.Processo, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public ProcessoDocumento criaEPopulaProcessoDocumento(Processo processo, String diretorioDocumento, String nomeDocumento) {
		ProcessoDocumento processoDocumento = null;
		if(processo!=null){
			processoDocumento =
					(ProcessoDocumento) ServiceLocator.getInstancia().getBeanPorID(ProcessoDocumento.BEAN_ID_PROCESSO_DOCUMENTO);
			processoDocumento.setDiretorioDocumento(diretorioDocumento);
			processoDocumento.setProcesso(processo);
			processoDocumento.setNomeDocumento(nomeDocumento);
		}
		return processoDocumento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.batch.ControladorProcessoDocumento#consultarProcessoDocumento(java.util.Map)
	 */
	@Override
	public Collection<ProcessoDocumento> consultarProcessoDocumento(Map<String, Object> filtro) {
		Criteria criteria = this.createCriteria(getClasseEntidade());

		if (filtro != null) {
			Long chavePrimariaProcesso = (Long) filtro.get(PROCESSO);
			if (chavePrimariaProcesso != null && chavePrimariaProcesso > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA_PROCESSO, chavePrimariaProcesso));
			}
			
			String diretorioDocumento = (String) filtro.get(DIRETORIO_DOCUMENTO);
			if(!StringUtils.isEmpty(diretorioDocumento)){
				criteria.add(Restrictions.eq(DIRETORIO_DOCUMENTO, diretorioDocumento));
			}
			String nomeDocumento = (String) filtro.get(NOME_DOCUMENTO);
			if(!StringUtils.isEmpty(nomeDocumento)){
				criteria.add(Restrictions.like(NOME_DOCUMENTO, nomeDocumento).ignoreCase());
			}
		}
		
		return criteria.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.ControladorProcessoDocumento#removerProcessoDocumentoPorDiretorioDocumento(java.lang.String)
	 */
	@Override
	public void removerProcessoDocumentoPorDiretorioDocumento(String diretorioDocumento) throws NegocioException {
		
		Map<String, Object> filtro = new HashMap<>();
		filtro.put(DIRETORIO_DOCUMENTO, diretorioDocumento);
		Collection<ProcessoDocumento> listaProcessoDocumento = this.consultarProcessoDocumento(filtro);
		
		for (ProcessoDocumento processoDocumento : listaProcessoDocumento) {
			this.remover(processoDocumento);

		}
	}

}
