package br.com.ggas.batch.impl;

import java.util.Date;

/**
 * Classe responsável por armazenar as informações do processo que serão enviadas para o DataTable de processos
 */
public class ProcessoDTO {
	private static final int PERIODICIDADE_NAO_SE_REPETE = 0;
	private static final int PERIODICIDADE_DIARIA = 1;
	private static final int PERIODICIDADE_MENSAL = 2;
	private static final int PERIODICIDADE_SEMANAL = 3;
	private static final int PERIODICIDADE_QUINZENAL = 4;
	private static final int PERIODICIDADE_HORARIA = 5;

	private Long chavePrimaria;
	private String descricao;
	private Integer periodicidade;
	private String periodicidadeExtenso;
	private Integer situacao;
	private Date inicioAgendamento;
	private Date fimAgendamento;
	private Date inicioExecucao;
	private Date fimExecucao;
	private String tempoExecucao;
	private boolean logErro;
	private boolean logExecucao;
	private Long chaveDocumento;
	private int quantidadeDocumento;
	private boolean indicadorDocumentoZip;
	private int versao;

	/**
	 * Gets a chave primária
	 * @return a chave primária
	 */
	public Long getChavePrimaria() {
		return chavePrimaria;
	}

	/**
	 * Sets a chave primária
	 * @param chavePrimaria a chave primária
	 */
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * Gets a descricao
	 * @return a descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Sets a descricao
	 * @param descricao a descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Gets a periodicidade
	 * @return a periodicidade
	 */
	public Integer getPeriodicidade() {
		return periodicidade;
	}

	/**
	 * Sets a periodicidade
	 * @param periodicidade a periodicidade
	 */
	public void setPeriodicidade(Integer periodicidade) {
		this.periodicidade = periodicidade;
		String periodicidadeStr;
		switch (periodicidade) {
			case PERIODICIDADE_NAO_SE_REPETE:
				periodicidadeStr = "Não repete";
				break;
			case PERIODICIDADE_DIARIA:
				periodicidadeStr = "Diária";
				break;
			case PERIODICIDADE_MENSAL:
				periodicidadeStr = "Mensal";
				break;
			case PERIODICIDADE_SEMANAL:
				periodicidadeStr = "Semanal";
				break;
			case PERIODICIDADE_QUINZENAL:
				periodicidadeStr = "Quinzenal";
				break;
			case PERIODICIDADE_HORARIA:
				periodicidadeStr = "Horária";
				break;
			default:
				periodicidadeStr = "";
				break;
		}
		setPeriodicidadeExtenso(periodicidadeStr);
	}

	/**
	 * Gets a descrição da periodicidade
	 * @return a descrição da periodicidade
	 */
	public String getPeriodicidadeExtenso() {
		return periodicidadeExtenso;
	}

	/**
	 * Sets a periodicidade por extenso
	 * @param periodicidadeExtenso a periodicidade por extenso
	 */
	public void setPeriodicidadeExtenso(String periodicidadeExtenso) {
		this.periodicidadeExtenso = periodicidadeExtenso;
	}

	/**
	 * Gets a situacao
	 * @return a situacao
	 */
	public Integer getSituacao() {
		return situacao;
	}

	/**
	 * Sets a situacao
	 * @param situacao a situacao
	 */
	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	/**
	 * Gets a data inicial do agendamento
	 * @return a data inicial do agendamento
	 */
	public Date getInicioAgendamento() {
		Date retorno = null;
		if (inicioAgendamento != null) {
			retorno = new Date(inicioAgendamento.getTime());
		}
		return retorno;
	}

	/**
	 * Sets a data inicial do agendamento
	 * @param inicioAgendamento a data inicial do agendamento
	 */
	public void setInicioAgendamento(Date inicioAgendamento) {
		if (inicioAgendamento != null) {
			this.inicioAgendamento = new Date(inicioAgendamento.getTime());
		}
	}

	/**
	 * Gets a data final do agendamento
	 * @return a data final do agendamento
	 */
	public Date getFimAgendamento() {
		Date retorno = null;
		if (fimAgendamento != null) {
			retorno = new Date(fimAgendamento.getTime());
		}
		return retorno;
	}

	/**
	 * Sets a data final do agendamento
	 * @param fimAgendamento a data final do agendamento
	 */
	public void setFimAgendamento(Date fimAgendamento) {
		if (fimAgendamento != null) {
			this.fimAgendamento = new Date(fimAgendamento.getTime());
		}
	}

	/**
	 * Gets a data do inicio da execução
	 * @return a data do inicio da execução
	 */
	public Date getInicioExecucao() {
		Date retorno = null;
		if (inicioExecucao != null) {
			retorno = new Date(inicioExecucao.getTime());
		}
		return retorno;

	}

	/**
	 * Sets a data do inicio da execução
	 * @param inicioExecucao a data do inicio da execução
	 */
	public void setInicioExecucao(Date inicioExecucao) {
		if (inicioExecucao != null) {
			this.inicioExecucao = new Date(inicioExecucao.getTime());
		}
	}

	/**
	 * Gets a data do fim da execução
	 * @return a data do fim da execução
	 */
	public Date getFimExecucao() {
		Date retorno = null;
		if (fimExecucao != null) {
			retorno = new Date(fimExecucao.getTime());
		}
		return retorno;
	}

	/**
	 * Sets a data do fim da execução
	 * @param fimExecucao a data do fim da execução
	 */
	public void setFimExecucao(Date fimExecucao) {
		if (inicioExecucao != null) {
			this.fimExecucao = new Date(fimExecucao.getTime());
		}
	}

	/**
	 * Gets o tempo de execução
	 * @return o tempo de execução
	 */
	public String getTempoExecucao() {
		return tempoExecucao;
	}

	/**
	 * Sets o tempo de execução
	 * @param tempoExecucao o tempo de execução
	 */
	public void setTempoExecucao(String tempoExecucao) {
		this.tempoExecucao = tempoExecucao;
	}

	/**
	 * Gets o log de erro
	 * @return o log de erro
	 */
	public boolean getLogErro() {
		return logErro;
	}

	/**
	 * Sets o log de erro
	 * @param logErro o log de erro
	 */
	public void setLogErro(boolean logErro) {
		this.logErro = logErro;
	}

	/**
	 * Gets o log de execução
	 * @return o log de execução
	 */
	public boolean getLogExecucao() {
		return logExecucao;
	}

	/**
	 * Sets o log de execução
	 * @param logExecucao o log de execução
	 */
	public void setLogExecucao(boolean logExecucao) {
		this.logExecucao = logExecucao;
	}

	/**
	 * Gets a chave do documento
	 * @return a chave do documento
	 */
	public Long getChaveDocumento() {
		return chaveDocumento;
	}

	/**
	 * Sets a chave do documento
	 * @param chaveDocumento a chave do documento
	 */
	public void setChaveDocumento(Long chaveDocumento) {
		this.chaveDocumento = chaveDocumento;
	}

	/**
	 * Gets a quantidade de documentos gerados
	 * @return a quantidade de documentos gerados
	 */
	public int getQuantidadeDocumento() {
		return quantidadeDocumento;
	}

	/**
	 * Sets a quantidade de documentos gerados
	 * @param quantidadeDocumento a quantidade de documentos gerados
	 */
	public void setQuantidadeDocumento(int quantidadeDocumento) {
		this.quantidadeDocumento = quantidadeDocumento;
	}

	/**
	 * Indica se o documento é um zip
	 * @return true se for zip, false caso contrário
	 */
	public boolean isIndicadorDocumentoZip() {
		return indicadorDocumentoZip;
	}

	/**
	 * Sets o indicadorDocumentoZip
	 * @param indicadorDocumentoZip true se for zip, false caso contrário
	 */
	public void setIndicadorDocumentoZip(boolean indicadorDocumentoZip) {
		this.indicadorDocumentoZip = indicadorDocumentoZip;
	}

	/**
	 * Gets a versão
	 * @return a versão
	 */
	public int getVersao() {
		return versao;
	}

	/**
	 * Sets a versão
	 * @param versao a versão
	 */
	public void setVersao(int versao) {
		this.versao = versao;
	}
}
