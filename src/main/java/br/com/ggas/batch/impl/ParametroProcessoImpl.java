package br.com.ggas.batch.impl;

import br.com.ggas.batch.Processo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

import java.util.Map;

/**
 * Classe responsável pela representação de um parametro do processo
 */
public class ParametroProcessoImpl extends EntidadeNegocioImpl {

	private Long id;

	private String parametro;

	private String valor;

	private Processo processo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets o nome do parametro
	 * @return o nome do parametro
	 */
	public String getParametro() {
		return parametro;
	}

	/**
	 * Set o nome do parametro
	 * @param parametro o nome do parametro
	 */
	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	/**
	 * Gets o valor do parametro
	 * @return o valor do parametro
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * Sets o valor
	 * @param valor o valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * Gets o processo para o qual o parametro está definido
	 * @return o processo para o qual o parametro está definido
	 */
	public Processo getProcesso() {
		return processo;
	}

	/**
	 * Set o processo para o qual o parametro está definido
	 * @param processo o processo para o qual o parametro está definido
	 */
	public void setProcesso(Processo processo) {
		this.processo = processo;
	}

	@Override public Map<String, Object> validarDados() {
		return null;
	}
}
