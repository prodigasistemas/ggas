/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.batch.impl;

import br.com.ggas.batch.SituacaoProcesso;
import br.com.ggas.geral.dominio.impl.EntidadeDominioImpl;

/**
 * 
 *
 */
class SituacaoProcessoImpl extends EntidadeDominioImpl implements SituacaoProcesso {

	/**
     * 
     */
	private static final long serialVersionUID = 6269591005576379746L;

	/**
	 * Instantiates a new situacao processo impl.
	 */
	public SituacaoProcessoImpl() {

		super();
	}

	/**
	 * Instantiates a new situacao processo impl.
	 * 
	 * @param codigo
	 *            the codigo
	 */
	public SituacaoProcessoImpl(int codigo) {

		super();
		this.setCodigo(codigo);
		// by gmatos
		// on 15/10/09
		// 10:08
	}

	@Override
	public void setCodigo(int codigo) {

		super.setCodigo(codigo);

		if(codigo == SITUACAO_ESPERA) {
			super.setDescricao(DESCRICAO_SITUACAO_ESPERA);
		} else if(codigo == SITUACAO_EXECUCAO) {
			super.setDescricao(DESCRICAO_SITUACAO_EXECUCAO);
		} else if(codigo == SITUACAO_CONCLUIDO) {
			super.setDescricao(DESCRICAO_SITUACAO_CONCLUIDO);
		} else if(codigo == SITUACAO_ERRO) {
			super.setDescricao(DESCRICAO_SITUACAO_ERRO);
		} else if(codigo == SITUACAO_CANCELADO) {
			super.setDescricao(DESCRICAO_SITUACAO_CANCELADO);
		} else if(codigo == SITUACAO_ABORTADO) {
			super.setDescricao(DESCRICAO_SITUACAO_ABORTADO);
		}
	}
}
