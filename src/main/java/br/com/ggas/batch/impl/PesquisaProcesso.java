package br.com.ggas.batch.impl;

/**
 * Classe responsável por armazenar os filtros relativos à pesquisa de processos
 */
public class PesquisaProcesso {

	private int qtdRegistros;
	private int offset;
	private String colunaOrdenacao;
	private String direcaoOrdenacao;

	private Long idGrupoFaturamento;
	private Long idRota;
	private Long modulo;
	private Long operacao;
	private Integer periodicidade;
	private Integer situacao;

	private String dataInicio;
	private String dataFim;
	private String dataInicioAgendamento;
	private String dataFinalAgendamento;
	private Boolean indicadorAgendamento;
	private Long[] rotasSelecionadas;

	/**
	 * Gets a quantidade de registros
	 * @return a quantidade de registros
	 */
	public int getQtdRegistros() {
		return qtdRegistros;
	}

	/**
	 * Sets a quantidade de registros
	 * @param qtdRegistros a quantidade de registros
	 */
	public void setQtdRegistros(int qtdRegistros) {
		this.qtdRegistros = qtdRegistros;
	}

	/**
	 * Gets o offset
	 * @return o offset
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * Sets o offset
	 * @param offset o offset
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	/**
	 * Gets o id do grupo de faturamento
	 * @return o id do grupo de faturamento
	 */
	public Long getIdGrupoFaturamento() {
		return idGrupoFaturamento;
	}

	/**
	 * Sets o id do grupo de faturamento
	 * @param idGrupoFaturamento o id do grupo de faturamento
	 */
	public void setIdGrupoFaturamento(Long idGrupoFaturamento) {
		this.idGrupoFaturamento = idGrupoFaturamento;
	}

	/**
	 * Gets o id da rota
	 * @return o id da rota
	 */
	public Long getIdRota() {
		return idRota;
	}

	/**
	 * Sets o id da rota
	 * @param idRota o id da rota
	 */
	public void setIdRota(Long idRota) {
		this.idRota = idRota;
	}

	/**
	 * Gets o módulo
	 * @return o módulo
	 */
	public Long getModulo() {
		return modulo;
	}

	/**
	 * Sets o módulo
	 * @param modulo o módulo
	 */
	public void setModulo(Long modulo) {
		this.modulo = modulo;
	}

	/**
	 * Gets a operação
	 * @return a operação
	 */
	public Long getOperacao() {
		return operacao;
	}

	/**
	 * Sets a operação
	 * @param operacao a operação
	 */
	public void setOperacao(Long operacao) {
		this.operacao = operacao;
	}

	/**
	 * Gets a periodicidade
	 * @return a periodicidade
	 */
	public Integer getPeriodicidade() {
		return periodicidade;
	}

	/**
	 * Sets a periodicidade
	 * @param periodicidade a periodicidade
	 */
	public void setPeriodicidade(Integer periodicidade) {
		this.periodicidade = periodicidade;
	}

	/**
	 * Gets a situação
	 * @return a situação
	 */
	public Integer getSituacao() {
		return situacao;
	}

	/**
	 * Sets a situação
	 * @param situacao a situação
	 */
	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	/**
	 * Gets a data de inicio da execução
	 * @return a data de inicio da execução
	 */
	public String getDataInicio() {
		return dataInicio;
	}

	/**
	 * Sets a data de inicio da execução
	 * @param dataInicio a data de inicio da execução
	 */
	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	/**
	 * Gets a data de fim da execução
	 * @return a data de fim da execução
	 */
	public String getDataFim() {
		return dataFim;
	}

	/**
	 * Sets a data de fim da execução
	 * @param dataFim a data de fim da execução
	 */
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	/**
	 * Gets a data de inicio do agendamento
	 * @return a data de inicio do agendamento
	 */
	public String getDataInicioAgendamento() {
		return dataInicioAgendamento;
	}

	/**
	 * Sets a data de inicio do agendamento
	 * @param dataInicioAgendamento a data de inicio do agendamento
	 */
	public void setDataInicioAgendamento(String dataInicioAgendamento) {
		this.dataInicioAgendamento = dataInicioAgendamento;
	}

	/**
	 * Gets a data de fim do agendamento
	 * @return a data de fim do agendamento
	 */
	public String getDataFinalAgendamento() {
		return dataFinalAgendamento;
	}

	/**
	 * Sets a data de fim do agendamento
	 * @param dataFinalAgendamento a data de fim do agendamento
	 */
	public void setDataFinalAgendamento(String dataFinalAgendamento) {
		this.dataFinalAgendamento = dataFinalAgendamento;
	}

	/**
	 * Retorna o indicador de agendamento
	 * @return o indicador de agendamento
	 */
	public Boolean getIndicadorAgendamento() {
		return indicadorAgendamento;
	}

	/**
	 * Sets o indicador de agendamento
	 * @param indicadorAgendamento o indicador de agendamento
	 */
	public void setIndicadorAgendamento(Boolean indicadorAgendamento) {
		this.indicadorAgendamento = indicadorAgendamento;
	}

	/**
	 * Gets as rotas selecionadas
	 * @return as rotas selecionadas
	 */
	public Long[] getRotasSelecionadas() {
		Long[] retorno = null;
		if (this.rotasSelecionadas != null) {
			retorno = this.rotasSelecionadas.clone();
		}
		return retorno;
	}

	/**
	 * Sets as rotas selecionadas
	 * @param rotasSelecionadas as rotas selecionadas
	 */
	public void setRotasSelecionadas(Long[] rotasSelecionadas) {
		if (rotasSelecionadas != null) {
			this.rotasSelecionadas = rotasSelecionadas.clone();
		}
	}

	/**
	 * Gets a coluna selecionada para ordenação
	 * @return a coluna selecionada para ordenação
	 */
	public String getColunaOrdenacao() {
		return colunaOrdenacao;
	}

	/**
	 * Sets a coluna selecionada para ordenação
	 * @param colunaOrdenacao a coluna selecionada para ordenação
	 */
	public void setColunaOrdenacao(String colunaOrdenacao) {
		this.colunaOrdenacao = colunaOrdenacao;
	}

	/**
	 * Gets a coluna selecionada para ordenação
	 * @return a coluna selecionada para ordenação
	 */
	public String getDirecaoOrdenacao() {
		return direcaoOrdenacao;
	}

	/**
	 * Sets a coluna selecionada para ordenação
	 * @param direcaoOrdenacao a coluna selecionada para ordenação
	 */
	public void setDirecaoOrdenacao(String direcaoOrdenacao) {
		this.direcaoOrdenacao = direcaoOrdenacao;
	}
}
