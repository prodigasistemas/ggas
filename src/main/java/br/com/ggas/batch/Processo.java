/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.batch;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pelos métodos de processo
 */
public interface Processo extends EntidadeNegocio {

	/**
	 * ENTIDADE_ROTULO_PROCESSO
	 */
	String ENTIDADE_ROTULO_PROCESSO = "ENTIDADE_ROTULO_PROCESSO";

	/**
	 * PROCESSO_ROTULO_INICIO
	 */
	String PROCESSO_ROTULO_INICIO = "PROCESSO_ROTULO_INICIO";

	/**
	 * PROCESSO_ROTULO_FIM
	 */
	String PROCESSO_ROTULO_FIM = "PROCESSO_ROTULO_FIM";

	/**
	 * PROCESSO_ROTULO_PERIODICIDADE
	 */
	String PROCESSO_ROTULO_PERIODICIDADE = "PROCESSO_ROTULO_PERIODICIDADE";

	/**
	 * PROCESSO_ROTULO_PROCESSO
	 */
	String PROCESSO_ROTULO_PROCESSO = "PROCESSO_ROTULO_PROCESSO";

	/**
	 * PROCESSO_ROTULO_DATA_INICIO_AGENDAMENTO
	 */
	String PROCESSO_ROTULO_DATA_INICIO_AGENDAMENTO = "PROCESSO_ROTULO_DATA_INICIO_AGENDAMENTO";

	/**
	 * PROCESSO_ROTULO_DATA_FINAL_AGENDAMENTO
	 */
	String PROCESSO_ROTULO_DATA_FINAL_AGENDAMENTO = "PROCESSO_ROTULO_DATA_FINAL_AGENDAMENTO";

	/**
	 * PROCESSO_ROTULO_SITUACAO
	 */
	String PROCESSO_ROTULO_SITUACAO = "PROCESSO_ROTULO_SITUACAO";

	/**
	 * BEAN_ID_PROCESSO
	 */
	String BEAN_ID_PROCESSO = "processo";

	String EMAIL = "EMAIL_PROCESSO";

	/**
	 * @return the enviarLogExecucao
	 */
	boolean isEnviarLogExecucao();

	/**
	 * @param enviarLogExecucao
	 *            the enviarLogExecucao to set
	 */
	void setEnviarLogExecucao(boolean enviarLogExecucao);

	/**
	 * @return the usuario
	 */
	Usuario getUsuario();

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	void setUsuario(Usuario usuario);

	/**
	 * @return the parametros
	 */
	Map<String, String> getParametros();

	/**
	 * @param parametros
	 *            the parametros to set
	 */
	void setParametros(Map<String, String> parametros);

	/**
	 * @return the operacao
	 */
	Operacao getOperacao();

	/**
	 * @param operacao
	 *            the operacao to set
	 */
	void setOperacao(Operacao operacao);

	/**
	 * @return the periodicidade
	 */
	int getPeriodicidade();

	/**
	 * @param periodicidade
	 *            the periodicidade to set
	 */
	void setPeriodicidade(int periodicidade);

	/**
	 * @return the situacao
	 */
	int getSituacao();

	/**
	 * @param situacao
	 *            the situacao to set
	 */
	void setSituacao(int situacao);

	/**
	 * @return the inicio
	 */
	Date getInicio();

	/**
	 * @param inicio
	 *            the inicio to set
	 */
	void setInicio(Date inicio);

	/**
	 * @return the fim
	 */
	Date getFim();

	/**
	 * @param fim
	 *            the fim to set
	 */
	void setFim(Date fim);

	/**
	 * @return the logErro
	 */
	byte[] getLogErro();

	/**
	 * @param logErro
	 *            the logErro to set
	 */
	void setLogErro(byte[] logErro);

	/**
	 * @return the logExecucao
	 */
	byte[] getLogExecucao();

	/**
	 * @param logExecucao
	 *            the logExecucao to set
	 */
	void setLogExecucao(byte[] logExecucao);

	/**
	 * @return the tempoExecucao
	 */
	String getTempoExecucao();

	/**
	 * @return the diaNaoUtil
	 */
	boolean isDiaNaoUtil();

	/**
	 * @param diaNaoUtil
	 *            the diaNaoUtil to set
	 */
	void setDiaNaoUtil(boolean diaNaoUtil);

	/**
	 * @return the agendado
	 */
	boolean isAgendado();

	/**
	 * @param agendado
	 *            the agendado to set
	 */
	void setAgendado(boolean agendado);

	/**
	 * @return the dataInicioAgendamento
	 */
	Date getDataInicioAgendamento();

	/**
	 * @param dataInicioAgendamento
	 *            the dataInicioAgendamento to set
	 */
	void setDataInicioAgendamento(Date dataInicioAgendamento);

	/**
	 * @return the dataFinalAgendamento
	 */
	Date getDataFinalAgendamento();

	/**
	 * @param dataFinalAgendamento
	 *            the dataFinalAgendamento to set
	 */
	void setDataFinalAgendamento(Date dataFinalAgendamento);

	/**
	 * @param emailResponsavel
	 */
	void setEmailResponsavel(String emailResponsavel);

	/**
	 * @return email do responsável
	 */
	String getEmailResponsavel();

	/**
	 * @return String - Retorna Descrição.
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the pai
	 */
	Processo getPai();

	/**
	 * @param pai
	 *            the pai to set
	 */
	void setPai(Processo pai);

	/**
	 * @return the filhos
	 */
	Collection<Processo> getFilhos();

	/**
	 * @param filhos
	 *            the filhos to set
	 */
	void setFilhos(Collection<Processo> filhos);

	/**
	 * Recupera lista ProcessoDocumento
	 * 
	 * @return listaProcessoDocumento
	 */
	Collection<ProcessoDocumento> getListaProcessoDocumento();

	/**
	 * Define valor para listaProcessoDocumento
	 * 
	 * @param listaProcessoDocumento
	 */
	void setListaProcessoDocumento(Collection<ProcessoDocumento> listaProcessoDocumento);

	/**
	 * Recupera o nome do funcionário que executou o processo
	 *
	 * @return usuarioExecucao
	 */
	String getUsuarioExecucao();

	/**
	 * Define valor para usuarioExecucao
	 *
	 * @param usuarioExecucao
	 */
	void setUsuarioExecucao(String usuarioExecucao);

	/**
	 * Recupera o nome do unidade organizacional que o funcionário
	 * pertencia no momento da execução
	 *
	 * @return unidadeOrganizacionalExecucao
	 */
	String getUnidadeOrganizacionalExecucao();

	/**
	 * Define valor para unidadeOrganizacionalExecucao
	 *
	 * @param unidadeOrganizacionalExecucao
	 */
	void setUnidadeOrganizacionalExecucao(String unidadeOrganizacionalExecucao);

}
