/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.batch;

import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.util.Pair;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Classe ControladorProcesso
 * 
 * @author Procenge
 */
public interface ControladorProcesso extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_PROCESSO = "controladorProcesso";

	String SUCESSO_PROCESSO_EXCLUIDO = "SUCESSO_PROCESSO_EXCLUIDO";

	String SUCESSO_PROCESSO_REINICIADO = "SUCESSO_PROCESSO_REINICIADO";

	String SUCESSO_PROCESSO_AGENDADO = "SUCESSO_PROCESSO_AGENDADO";

	String SUCESSO_PROCESSO_ALTERADO = "SUCESSO_PROCESSO_ALTERADO";

	String SUCESSO_PROCESSO_EXECUTADO = "SUCESSO_PROCESSO_EXECUTADO";

	String SUCESSO_PROCESSO_CANCELADO = "SUCESSO_PROCESSO_CANCELADO";

	String ERRO_NEGOCIO_PROCESSO_NAO_AGENDADO = "ERRO_NEGOCIO_PROCESSO_NAO_AGENDADO";

	String ERRO_NEGOCIO_SITUACAO_PROCESSO_INVALIDA_ALTERACAO = "ERRO_NEGOCIO_SITUACAO_PROCESSO_INVALIDA_ALTERACAO";

	String ERRO_NEGOCIO_DATA_AGENDADA_DIA_NAO_UTIL = "ERRO_NEGOCIO_DATA_AGENDADA_DIA_NAO_UTIL";

	String ERRO_NEGOCIO_EMAIL_INVALIDO = "ERRO_NEGOCIO_EMAIL_INVALIDO";

	String ERRO_NEGOCIO_DATA_AGENDAMENTO_MENOR_QUE_DATA_ATUAL = "ERRO_NEGOCIO_DATA_AGENDAMENTO_MENOR_QUE_DATA_ATUAL";

	String ERRO_NEGOCIO_DATA_AGENDAMENTO_INICIAL_MAIOR_DATA_AGENDAMENTO_FINAL = 
					"ERRO_NEGOCIO_DATA_AGENDAMENTO_INICIAL_MAIOR_DATA_AGENDAMENTO_FINAL";

	String ERRO_NEGOCIO_SITUACAO_PROCESSO_INVALIDA_CANCELAMENTO = "ERRO_NEGOCIO_SITUACAO_PROCESSO_INVALIDA_CANCELAMENTO";

	String ERRO_NEGOCIO_DATA_INICIAL_MENOR_QUE_DATA_PROCESSO = "ERRO_NEGOCIO_DATA_INICIAL_MENOR_QUE_DATA_PROCESSO";

	String ERRO_NEGOCIO_DATA_FINAL_MAIOR_QUE_DATA_PROCESSO = "ERRO_NEGOCIO_DATA_FINAL_MAIOR_QUE_DATA_PROCESSO";

	String ERRO_NEGOCIO_DATA_AGENDAMENTO_FORA_INTERVALO_PROCESSO = "ERRO_NEGOCIO_DATA_AGENDAMENTO_FORA_INTERVALO_PROCESSO";

	String ERRO_NEGOCIO_EMAIL_OBRIGATORIO_ATIVIDADE_SELECIONADA = "ERRO_NEGOCIO_EMAIL_OBRIGATORIO_ATIVIDADE_SELECIONADA";

	/**
	 * Método responsável por consultar as chaves
	 * dos processos pendentes.
	 * 
	 * @return Uma coleção de chaves de processos
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Long> consultarPendentes() throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * processos em execução.
	 * 
	 * @param operacao
	 *            A operação
	 * @return boolean True Caso exista algum
	 *         processo executando a operação
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean existeProcessoExecutandoOperacao(Operacao operacao) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * processos do sistema.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Processo> consultar() throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * processos do sistema.
	 * 
	 * @param filtro
	 *            Um flitro de consulta
	 * @return Uma coleção de processos
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Pair<Long, List<Processo>> consultarProcessos(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por reiniciar todos os
	 * processos.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void reiniciarProcessos() throws GGASException;

	/**
	 * Método responsável por executar o processo.
	 * 
	 * @param processo
	 *            O processado
	 * @return O log da execução
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	String executar(Processo processo) throws GGASException;

	/**
	 * Método responsável por enviar o e-mail depois do processo
	 * @param emailResponsavel - {@link String}
	 * @param relatorioProcesso - {@link String}
	 * @throws GGASException 
	 * 						the GGAS Exception
	 */
	void enviarEmail(String emailResponsavel, String relatorioProcesso) throws GGASException;
	
	/**
	 * Método responsável por obter todas as
	 * situações do processo.
	 * 
	 * @return Uma coleção com situações
	 */
	Collection<SituacaoProcesso> listarSituacaoProcesso();

	/**
	 * Método responsável por obter uma situação
	 * do processo.
	 * 
	 * @param codigo
	 *            O código da situação
	 * @return Uma situação
	 */
	SituacaoProcesso obterSituacaoProcesso(int codigo);

	/**
	 * Método responsável por obter todas as
	 * periodicidades do processo.
	 * 
	 * @return Uma coleção com periodicidades
	 */
	Collection<PeriodicidadeProcesso> listarPeriodicidadeProcesso();

	/**
	 * Método responsável por obter a
	 * periodicidade do processo.
	 * 
	 * @param codigo
	 *            O código da periodicidade
	 * @return Uma Periodicidade
	 */
	PeriodicidadeProcesso obterPeriodicidadeProcesso(int codigo);

	/**
	 * Método responsável por listar as situações
	 * de processo permitidas na alteração de
	 * agendamento de processo.
	 * 
	 * @return Uma coleção com situações.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<SituacaoProcesso> listarSituacoesAlteracaoAgendamentoProcesso() throws NegocioException;

	/**
	 * Método responsável por validar se o
	 * processo pode ter seu agendamento alterado.
	 * 
	 * @param chavePrimariaProcesso
	 *            A chave primária do processo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarAlteracaoAgendamentoProcesso(Long chavePrimariaProcesso) throws NegocioException;

	/**
	 * Método responsável por atualizar o
	 * agendamento de um processo.
	 * 
	 * @param processo
	 *            O processo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro de
	 *             concorrência do método.
	 * @throws GGASException the GGAS Exception
	 * 				
	 */
	void atualizarAgendamento(Processo processo) throws GGASException;

	/**
	 * Metodo responsavel por validar a data de
	 * agendamento.
	 * 
	 * @param processo
	 *            the processo
	 * @param horaInicio
	 *            the hora inicio
	 * @param horaFim
	 *            the hora fim
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDataAgendamento(Processo processo, String horaInicio, String horaFim) throws NegocioException;

	/**
	 * Validar datas pesquisa processo.
	 * 
	 * @param dataInicioAgendamento
	 *            the data inicio agendamento
	 * @param dataFinalAgendamento
	 *            the data final agendamento
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void validarDatasPesquisaProcesso(String dataInicioAgendamento, String dataFinalAgendamento) throws GGASException;

	/**
	 * *
	 * Método responsável por validar se a
	 * situação do processo para cancelamento e em
	 * espera.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException    
	 * 			   the ggas exception      
	 */
	public void cancelarAgendamento(Long[] chavesPrimarias) throws GGASException;

	/**
	 * Gets the proximo processo em espera por operacao.
	 * 
	 * @param operacao
	 *            the operacao
	 * @return the proximo processo em espera por operacao
	 */
	public Processo getProximoProcessoEmEsperaPorOperacao(Operacao operacao);

	/**
	 * Obter atividade sistema.
	 * 
	 * @param operacao
	 *            the operacao
	 * @return the atividade sistema
	 */
	public AtividadeSistema obterAtividadeSistema(Operacao operacao);

	/**
	 * Obter atividade sistema.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the atividade sistema
	 */
	public AtividadeSistema obterAtividadeSistema(Long chavePrimaria);

	/**
	 * Obter atividade sistema por chave operacao.
	 * 
	 * @param idOperacao
	 *            the id operacao
	 * @return the atividade sistema
	 */
	AtividadeSistema obterAtividadeSistemaPorChaveOperacao(Long idOperacao);

	/**
	 * Método responsável por validar se
	 * obrigatoriedade do email referente a
	 * atividade selecionada.
	 * 
	 * @param processo
	 *            the processo
	 * @throws NegocioException
	 *             Caso o email responsavel do
	 *             processo for null ou string
	 *             vazia
	 */
	void validarEmailObrigatorioAtividadeSelecionada(Processo processo) throws NegocioException;

	/**
	 * Método responsável por verificar se a
	 * atividade precedente já foi executada.
	 * 
	 * @param atividadeSistema
	 *            Atividade Sistema
	 * @param cronogramaRota
	 *            Cronograma Rota
	 * @param cronogramaAtividadeFaturamento
	 *            cronogramaAtividadeFaturamento
	 * @return Booleano indicando se a atividade
	 *         já foi executada ou não.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	boolean verificarRealizacaoAtividadeSistemaPrecedente(AtividadeSistema atividadeSistema, CronogramaRota cronogramaRota,
					CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento) throws NegocioException;

	/**
	 * Salva Relatório
	 * 
	 * @param processo {@link Processo}
	 * @param controladorProcessoDocumento {@link ControladorProcessoDocumento}
	 * @param byteArrayInputStream {@link ByteArrayInputStream}
	 * @param nomeDocumento {@link String}
	 * @param diretorioDocumento {@link String}
	 * @throws FileNotFoundException {@link FileNotFoundException}
	 * @throws NegocioException {@link NegocioException}
	 * @throws IOException {@link IOException}
	 */
	void salvarRelatorio(Processo processo, ControladorProcessoDocumento controladorProcessoDocumento,
			ByteArrayInputStream byteArrayInputStream, String nomeDocumento, String diretorioDocumento)
			throws NegocioException, IOException;

	
}
