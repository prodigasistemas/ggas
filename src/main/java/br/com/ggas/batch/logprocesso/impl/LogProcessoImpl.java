package br.com.ggas.batch.logprocesso.impl;

import br.com.ggas.batch.logprocesso.LogProcesso;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;

import java.util.Date;
import java.util.Map;

/**
 * Classe responsável por implementar os métodos do controller LogProcesso
 */
public class LogProcessoImpl extends EntidadeNegocioImpl implements LogProcesso {

	private Date dataHora;

	private String modulo;

	private String processo;

	private String usuario;

	private String arquivo;

	private String metodo;

	private Long linha;

	private String mensagem;

	private String trilhaRastreabilidade;

	@Override public Date getDataHora() {
		return dataHora;
	}

	@Override public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	@Override public String getModulo() {
		return modulo;
	}

	@Override public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	@Override public String getProcesso() {
		return processo;
	}

	@Override public void setProcesso(String processo) {
		this.processo = processo;
	}

	@Override public String getUsuario() {
		return usuario;
	}

	@Override public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	@Override public String getArquivo() {
		return arquivo;
	}

	@Override public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}

	@Override public String getMetodo() {
		return metodo;
	}

	@Override public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	@Override public Long getLinha() {
		return linha;
	}

	@Override public void setLinha(Long linha) {
		this.linha = linha;
	}

	@Override public String getMensagem() {
		return mensagem;
	}

	@Override public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	@Override public String getTrilhaRastreabilidade() {
		return trilhaRastreabilidade;
	}

	@Override public void setTrilhaRastreabilidade(String trilhaRastreabilidade) {
		this.trilhaRastreabilidade = trilhaRastreabilidade;
		if (trilhaRastreabilidade.length() > TAMANHO_MAXIMO_STACK_TRACE) {
			this.trilhaRastreabilidade = trilhaRastreabilidade.substring(0, TAMANHO_MAXIMO_STACK_TRACE - 1);
		}
	}

	@Override public Map<String, Object> validarDados() {
		return null;
	}

}
