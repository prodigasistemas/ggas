package br.com.ggas.batch.logprocesso;

import br.com.ggas.geral.negocio.EntidadeNegocio;

import java.util.Date;

/**
 * Interface responsável por definir os métodos do Log dos processos
 */
public interface LogProcesso extends EntidadeNegocio {
	String BEAN_ID_LOG_PROCESSO = "logProcesso";
	Integer TAMANHO_MAXIMO_STACK_TRACE = 4000;

	/**
	 * @return the dataHora
	 */
	Date getDataHora();

	/**
	 * @param dataHora the dataHoraErro to set
	 */
	void setDataHora(Date dataHora);

	/**
	 * @return the modulo
	 */
	String getModulo();

	/**
	 * @param modulo the dataHoraErro to set
	 */
	void setModulo(String modulo);

	/**
	 * @return the processo
	 */
	String getProcesso();

	/**
	 * @param processo the processo to set
	 */
	void setProcesso(String processo);

	/**
	 * @return the usuario
	 */
	String getUsuario();

	/**
	 * @param usuario the usuario to set
	 */
	void setUsuario(String usuario);

	/**
	 * @return the arquivo
	 */
	String getArquivo();

	/**
	 * @param arquivo the arquivo to ser
	 */
	void setArquivo(String arquivo);

	/**
	 * @return the metodo
	 */
	String getMetodo();

	/**
	 * @param metodo the metodo to set
	 */
	void setMetodo(String metodo);

	/**
	 * @return the linha
	 */
	Long getLinha();

	/**
	 * @param linha the linha to set
	 */
	void setLinha(Long linha);

	/**
	 * @return the Mensagem
	 */
	String getMensagem();

	/**
	 * @param mensagem the mensagem to set
	 */
	void setMensagem(String mensagem);

	/**
	 * @return the stack trace
	 */
	String getTrilhaRastreabilidade();

	/**
	 * @param trilhaRastreabilidade the stack trace to set
	 */
	void setTrilhaRastreabilidade(String trilhaRastreabilidade);
}
