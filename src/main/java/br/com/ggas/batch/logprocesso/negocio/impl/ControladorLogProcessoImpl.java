package br.com.ggas.batch.logprocesso.negocio.impl;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.logprocesso.LogProcesso;
import br.com.ggas.batch.logprocesso.negocio.ControladorLogProcesso;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.ServiceLocator;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.util.Date;

/**
 * Classe responsável pela implementação dos métodos relacionados ao controlador Log Processo
 */
public class ControladorLogProcessoImpl extends ControladorNegocioImpl implements ControladorLogProcesso {

	@Override public Long inserirLogProcesso(LogProcesso logProcesso) {

		try {
			return super.inserir(logProcesso);
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
		}
		return null;
	}

	@Override public Long gerarInsetirLogProcesso(DadosAuditoria dadosAuditoria, Exception e) {

		try {
			return super.inserir(gerarLPPorDadosAuditoriaEExeception(dadosAuditoria, e));
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
		}
		return null;
	}

	@Override public EntidadeNegocio criar() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(LogProcesso.BEAN_ID_LOG_PROCESSO);
	}

	@Override public Class<?> getClasseEntidade() {
		return ServiceLocator.getInstancia().getClassPorID(LogProcesso.BEAN_ID_LOG_PROCESSO);
	}

	/**
	 * Método responsável por gerar o Log do processo baseado nos dados de auditoria e exception.
	 *
	 * @param dados dados de auditoria para gerar o log de erro do processo
	 * @param e     a exception para gerar o log de erro do processo
	 * @return logProcesso
	 */
	private LogProcesso gerarLPPorDadosAuditoriaEExeception(DadosAuditoria dados, Exception e) {

		LogProcesso logProcesso = (LogProcesso) this.criar();
		logProcesso.setDataHora(new Date());
		preencherLogProcessoComDadosAuditoria(logProcesso, dados);
		preencherLogProcessoComDadosDoException(logProcesso, e);
		return logProcesso;
	}

	/**
	 * Método responsável preencher no objeto de log Processo com os dados referente ao processo presente
	 * no objeto de dadosAuditoria.
	 *
	 * @param logProcesso logProcesso objeto que será persistido
	 * @param dados       dados de auditoria
	 */
	private void preencherLogProcessoComDadosAuditoria(LogProcesso logProcesso, DadosAuditoria dados) {

		if (dados != null) {
			if (dados.getOperacao() != null) {
				logProcesso.setProcesso(dados.getOperacao().getDescricao());
				if (dados.getOperacao().getModulo() != null) {
					logProcesso.setModulo(dados.getOperacao().getModulo().getDescricao());
				}
			}
			if (dados.getUsuario() != null) {
				logProcesso.setUsuario(dados.getUsuario().getDescricao());
			}
		}
	}

	/**
	 * Método responsável preencher no objeto de log Processo com os dados referente ao processo
	 * presente no objeto de dadosAuditoria.
	 *
	 * @param logProcesso objeto que será persistido
	 * @param e           a exception para preencher o logProcesso com dados do erro
	 */
	private void preencherLogProcessoComDadosDoException(LogProcesso logProcesso, Exception e) {

		if (e.getStackTrace()[0] != null) {
			logProcesso.setArquivo(e.getStackTrace()[0].getFileName());
			logProcesso.setMetodo(e.getStackTrace()[0].getMethodName());
			logProcesso.setLinha((long) e.getStackTrace()[0].getLineNumber());
			if (e instanceof NegocioException) {
				logProcesso.setMensagem(((NegocioException) e).getChaveErro().replaceAll("_", " "));
			} else {
				if (e.getCause() != null) {
					logProcesso.setMensagem(e.getCause().getMessage());
				} else {
					logProcesso.setMensagem(e.getMessage());
				}
			}
			logProcesso.setTrilhaRastreabilidade(ExceptionUtils.getFullStackTrace(e));
		}
	}
}
