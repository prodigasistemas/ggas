package br.com.ggas.batch.logprocesso.negocio;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.logprocesso.LogProcesso;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * Interface responsável pelos métodos do contoladorLogProcesso
 */
public interface ControladorLogProcesso extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_LOG_PROCESSO = "controladorLogProcesso";

	/**
	 * Inserir log de erro dos processos.
	 *
	 * @param logProcesso o log Processo que será inserido
	 * @return o id do log inserido
	 */
	Long inserirLogProcesso(LogProcesso logProcesso);

	/**
	 * Gerar e inserir log de erro dos processos baseado nos dados de auditoria
	 * e da exception lançada.
	 *
	 * @param dadosAuditoria os dados de auditoria para gerar o log de erro do processo
	 * @param e              a exception para gerar o log de erro do processo
	 * @return o id do log inserido
	 */
	Long gerarInsetirLogProcesso(DadosAuditoria dadosAuditoria, Exception e);

}
