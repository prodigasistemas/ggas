/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.parametrosistema.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.parametrosistema.TipoParametroSistema;
import br.com.ggas.util.Constantes;

/**
 * 
 *
 */
public class ParametroSistemaImpl extends EntidadeNegocioImpl implements ParametroSistema {

	private static final long serialVersionUID = 8368890327866333280L;

	private String codigo;

	private String descricao;

	private TipoParametroSistema tipoParametroSistema;

	private String classeEntidade;

	private String valorParametro;

	private boolean habilitado;

	private Modulo modulo;

	private Tabela tabela;

	/**
	 * @return the codigo
	 */
	@Override
	public String getCodigo() {

		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	@Override
	public void setCodigo(String codigo) {

		this.codigo = codigo;
	}

	/**
	 * @return the descricao
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/**
	 * @return the tipoParametroSistema
	 */
	@Override
	public TipoParametroSistema getTipoParametroSistema() {

		return tipoParametroSistema;
	}

	/**
	 * @param tipoParametroSistema
	 *            the tipoParametroSistema to set
	 */
	@Override
	public void setTipoParametroSistema(TipoParametroSistema tipoParametroSistema) {

		this.tipoParametroSistema = tipoParametroSistema;
	}

	/**
	 * @return the classeEntidade
	 */
	@Override
	public String getClasseEntidade() {

		return classeEntidade;
	}

	/**
	 * @param classeEntidade
	 *            the classeEntidade to set
	 */
	@Override
	public void setClasseEntidade(String classeEntidade) {

		this.classeEntidade = classeEntidade;
	}

	/**
	 * @return the valor
	 */
	@Override
	public String getValor() {

		return valorParametro;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ParametroSistema
	 * #getValorInteger()
	 */
	@Override
	public Integer getValorInteger() throws NegocioException {
		String valor = this.getValor();
		if (StringUtils.isNotEmpty(valor)) {
			try {
				return Integer.valueOf(valor);
			} catch (NumberFormatException e) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, 
								getChavePrimaria());
			}
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, 
							getChavePrimaria());
		}
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ParametroSistema
	 * #getValorLong()
	 */
	@Override
	public Long getValorLong() throws NegocioException {
		String valor = this.getValor();
		if (StringUtils.isNotEmpty(valor)) {
			try {
				return Long.valueOf(valor);
			} catch (NumberFormatException e) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, getChavePrimaria());
			}
		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_BUSCAR_CONSTANTE, getChavePrimaria());
		}
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	@Override
	public void setValor(String valor) {

		this.valorParametro = valor;
	}

	/**
	 * @return the habilitado
	 */
	@Override
	public boolean isHabilitado() {

		return habilitado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.parametrosistema.ParametroSistema
	 * #getModulo()
	 */
	@Override
	public Modulo getModulo() {

		return modulo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.parametrosistema.ParametroSistema
	 * #
	 * setModulo(br.com.ggas.controleacesso.Modulo
	 * )
	 */
	@Override
	public void setModulo(Modulo modulo) {

		this.modulo = modulo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.parametrosistema.ParametroSistema
	 * #getTabela()
	 */
	@Override
	public Tabela getTabela() {

		return tabela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.parametrosistema.ParametroSistema
	 * #
	 * setTabela(br.com.ggas.controleacesso.Tabela
	 * )
	 */
	@Override
	public void setTabela(Tabela tabela) {

		this.tabela = tabela;
	}

	/**
	 * @param habilitado
	 *            the habilitado to set
	 */
	@Override
	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.codigo == null || this.codigo.length() == 0) {
			stringBuilder.append(PARAMETRO_SISTEMA_ROTULO_CODIGO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.descricao == null || this.descricao.length() == 0) {
			stringBuilder.append(PARAMETRO_SISTEMA_ROTULO_DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.valorParametro == null || this.valorParametro.length() == 0) {
			stringBuilder.append(PARAMETRO_SISTEMA_ROTULO_VALOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
		}

		return erros;
	}

}
