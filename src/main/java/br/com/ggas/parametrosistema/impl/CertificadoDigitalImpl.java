package br.com.ggas.parametrosistema.impl;

import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.parametrosistema.CertificadoDigital;

public class CertificadoDigitalImpl extends EntidadeNegocioImpl implements CertificadoDigital {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3392645891867842937L;
	private byte[] arquivo;
	private String senha;

	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

	@Override
	public byte[] getArquivo() {
		return this.arquivo;
	}

	@Override
	public void setArquivo(byte[] arquivo) {
		this.arquivo = arquivo;
	}

	@Override
	public String getSenha() {
		return this.senha;
	}

	@Override
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
}