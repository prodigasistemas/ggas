/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.parametrosistema.impl;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.web.multipart.MultipartFile;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.parametrosistema.CertificadoDigital;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.parametrosistema.TipoParametroSistema;
import br.com.ggas.parametrosistema.Variavel;
import br.com.ggas.util.BooleanUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;

/**
 *
 *
 */
class ControladorParametroSistemaImpl extends ControladorNegocioImpl implements ControladorParametroSistema {

	private Map<String, Object> cache = new HashMap<String, Object>();

	private Map<String, ParametroSistema> parametroCache = new HashMap<String, ParametroSistema>();

	private static final Logger LOG = Logger.getLogger(ControladorParametroSistemaImpl.class);
	
	private static final int VALOR_INTEIRO_PARA_FALSO = 0;

	public Map<String, Object> getCache() {

		return cache;
	}

	public void setCache(Map<String, Object> cache) {

		this.cache = cache;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ParametroSistema.BEAN_ID_PARAMETRO_SISTEMA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(ParametroSistema.BEAN_ID_PARAMETRO_SISTEMA);
	}
	
	public Class<?> getClasseEntidadeCertificadoDigital() {
		return ServiceLocator.getInstancia().getClassPorID(CertificadoDigital.BEAN_ID_CERTIFICADO_DIGITAL);
	}
	
	public EntidadeNegocio criarCertificadoDigital() {
		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(CertificadoDigital.BEAN_ID_CERTIFICADO_DIGITAL);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema.
	 * ControladorParametroSistema
	 * #criarTipoParametroSistema()
	 */
	@Override
	public TipoParametroSistema criarTipoParametroSistema() {

		return (TipoParametroSistema) ServiceLocator.getInstancia().getBeanPorID(TipoParametroSistema.BEAN_ID_TIPO_PARAMETRO_SISTEMA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema.
	 * ControladorParametroSistema
	 * #obterTipoParametroSistema(int)
	 */
	@Override
	public TipoParametroSistema obterTipoParametroSistema(int tipoParametro) {

		TipoParametroSistema tipoParametroSistema = null;

		if(tipoParametro == TipoParametroSistema.TIPO_ENTIDADE_DINAMICA) {
			tipoParametroSistema = this.criarTipoParametroSistema();
			tipoParametroSistema.setCodigo(TipoParametroSistema.TIPO_ENTIDADE_DINAMICA);
		} else if(tipoParametro == TipoParametroSistema.TIPO_NEGOCIO) {
			tipoParametroSistema = this.criarTipoParametroSistema();
			tipoParametroSistema.setCodigo(TipoParametroSistema.TIPO_NEGOCIO);
		} else if(tipoParametro == TipoParametroSistema.TIPO_ESTATICO) {
			tipoParametroSistema = this.criarTipoParametroSistema();
			tipoParametroSistema.setCodigo(TipoParametroSistema.TIPO_NEGOCIO);
		}

		return tipoParametroSistema;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema.
	 * ControladorParametroSistema
	 * #consultarParametroSistema()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<ParametroSistema> consultarParametroSistema() throws NegocioException {

		Criteria criteria = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(getClasseEntidade());
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema.
	 * ControladorParametroSistema
	 * #inserirParametroSistema(java.lang.String,
	 * java.lang.String, java.lang.Object, int,
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public long inserirParametroSistema(String codigo, String descricao, Object valor, int tipoParametro, DadosAuditoria dadosAuditoria)
					throws NegocioException {

		LOG.debug("### inserir parametro: " + codigo + " " + descricao + " " + valor);
		long idParametro = 0;
		ParametroSistema parametroSistema = null;

		parametroSistema = this.obterParametroPorCodigo(codigo);

		if(parametroSistema == null) {
			parametroSistema = (ParametroSistema) ServiceLocator.getInstancia().getBeanPorID(ParametroSistema.BEAN_ID_PARAMETRO_SISTEMA);
			TipoParametroSistema tipo = null;

			parametroSistema.setCodigo(codigo);
			parametroSistema.setDescricao(descricao);
			parametroSistema.setUltimaAlteracao(Calendar.getInstance().getTime());
			parametroSistema.setHabilitado(true);
			parametroSistema.setDadosAuditoria(dadosAuditoria);

			if(tipoParametro == TipoParametroSistema.TIPO_ENTIDADE_DINAMICA) {
				Integer id = null;
				Method metodo = null;

				tipo = this.obterTipoParametroSistema(TipoParametroSistema.TIPO_ENTIDADE_DINAMICA);

				try {
					metodo = valor.getClass().getMethod("getId");
					id = (Integer) metodo.invoke(valor);
				} catch(SecurityException e) {
					throw new NegocioException(e);
				} catch (NoSuchMethodException e) {
					LOG.error(e.getMessage(), e);
					throw new NegocioException("Objeto não possui método getId(): " + e.getMessage());
				} catch(IllegalArgumentException e) {
					throw new NegocioException(e);
				} catch(IllegalAccessException e) {
					throw new NegocioException(e);
				} catch(InvocationTargetException e) {
					throw new NegocioException(e);
				}

				parametroSistema.setValor(String.valueOf(id));
				parametroSistema.setClasseEntidade(valor.getClass().getName());
			} else if(tipoParametro == TipoParametroSistema.TIPO_NEGOCIO) {
				tipo = this.obterTipoParametroSistema(TipoParametroSistema.TIPO_NEGOCIO);
				parametroSistema.setValor(String.valueOf(valor));

			} else {
				tipo = this.obterTipoParametroSistema(TipoParametroSistema.TIPO_ESTATICO);
				parametroSistema.setValor(String.valueOf(valor));
			}

			parametroSistema.setTipoParametroSistema(tipo);

			// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
			idParametro = (Long) getHibernateTemplate().getSessionFactory().getCurrentSession().save(parametroSistema);

		} else {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_ENTIDADE_CADASTRADA, ParametroSistema.ENTIDADE_ROTULO_PARAMETRO_SISTEMA);
		}

		return idParametro;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroPorCodigo(
	 * java.lang.String, boolean)
	 */
	@Override
	public ParametroSistema obterParametroPorCodigo(String codigo) throws NegocioException {
		return obterParametroPorCodigo(codigo, true);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema.
	 * ControladorParametroSistema
	 * #obterParametroPorCodigo(java.lang.String)
	 */
	@Override
	public ParametroSistema obterParametroPorCodigo(String codigo, boolean cacheable) throws NegocioException {

		LOG.debug("### obterParametroPorCodigo: " + codigo);
		if(!parametroCache.containsKey(codigo)){
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where ");
			hql.append(" codigo = ? ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setString(0, codigo);
			query.setCacheable(cacheable);

			ParametroSistema parametro = (ParametroSistema) query.uniqueResult();
			parametroCache.put(codigo, parametro);
		}
		return parametroCache.get(codigo);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema.ControladorParametroSistema#obterCodigoDoParametroPorValor(java.lang.String)
	 */
	@Override
	public ParametroSistema obterCodigoDoParametroPorValor(String codigoParametroEncargo) {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" valor = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(0, codigoParametroEncargo);
		query.setCacheable(true);
		return (ParametroSistema) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema.
	 * ControladorParametroSistema
	 * #obterValorDoParametroPorCodigo
	 * (java.lang.String,
	 * br.com.ggas.parametrosistema.Variavel[])
	 */
	@Override
	public Object obterValorDoParametroPorCodigo(String codigo, Variavel... variaveis) throws NegocioException {

		ParametroSistema parametro = null;
		Object retorno = null;

		if(cache.containsKey(codigo) && variaveis.length == 0) {
			LOG.debug("### <<< CACHE >>> ### obterValorDoParametroPorCodigo: " + codigo);
			retorno = cache.get(codigo);
		} else {

			parametro = this.obterParametroPorCodigo(codigo);

			if(parametro != null) {
				if(parametro.getTipoParametroSistema().getCodigo() == TipoParametroSistema.TIPO_ESTATICO) {
					retorno = parametro.getValor();
				} else if(parametro.getTipoParametroSistema().getCodigo() == TipoParametroSistema.TIPO_ENTIDADE_DINAMICA) {
					retorno = getHibernateTemplate().getSessionFactory().getCurrentSession()
									.get(parametro.getClasseEntidade(), Integer.valueOf(parametro.getValor()));
				} else if(parametro.getTipoParametroSistema().getCodigo() == TipoParametroSistema.TIPO_NEGOCIO) {

					String logicaNegocio = parametro.getValor();
					Binding binding = new Binding();
					if((variaveis != null) && (variaveis.length > 0)) {
						Variavel variavel = null;
						for (int i = 0; i < variaveis.length; i++) {
							variavel = variaveis[i];
							binding.setVariable(variavel.getNome(), variavel.getObjeto());
						}
					}

					try {
						GroovyShell shell = new GroovyShell(binding);
						shell.evaluate(logicaNegocio);
						retorno = binding.getVariable("retornoParametro");
					} catch(Exception e) {
						LOG.info("### Erro ao tentar obter o parametro: " + codigo + " : " + e.getMessage(), e);
						throw new NegocioException(Constantes.ERRO_PARAMETRO_NEGOCIO, codigo);
					}

				}

			} else {
				throw new NegocioException(ERRO_NEGOCIO_PARAMETRO_SISTEMA_NAO_ENCONTRADO, codigo);
			}
			LOG.debug("### [[[ BANCO ]]] ###  obterValorDoParametroPorCodigo: " + codigo + " do banco");
			cache.put(codigo, retorno);
		}
		return retorno;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema.ControladorParametroSistema#atualizar(br.com.ggas.parametrosistema.ParametroSistema)
	 */
	@Override
	public void atualizar(ParametroSistema parametroSistema) throws ConcorrenciaException, NegocioException {

		super.atualizar(parametroSistema);
		if(cache.containsKey(parametroSistema.getCodigo())) {
			this.cache.put(parametroSistema.getCodigo(), parametroSistema.getValor());
		}
		if(parametroCache.containsKey(parametroSistema.getCodigo())) {
			this.parametroCache.put(parametroSistema.getCodigo(), parametroSistema);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema.
	 * ControladorParametroSistema
	 * #obterParametrosSistemaPorModulo(long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<ParametroSistema> obterParametrosSistemaPorModulo(long idModulo) throws NegocioException {

		Criteria criteria = this.createCriteria(ParametroSistema.class);
		criteria.add(Restrictions.eq("modulo.chavePrimaria", idModulo));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema.
	 * ControladorParametroSistema
	 * #salvarAlteracaoParametros(java.util.Map)
	 */
	@Override
	public void salvarAlteracaoParametros(Map<String, Map<String, String>> dados, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		for (Map.Entry<String, Map<String, String>> entry : dados.entrySet()) {
			String nomeAba = entry.getKey();
			Map<String, String> dadosParametros = entry.getValue();

			for (Map.Entry<String, String> e : dadosParametros.entrySet()) {
				String codigoParametro = e.getKey();
				String valorParametro = e.getValue();
				ParametroSistema parametroSistema = this.obterParametroPorCodigo(codigoParametro);

				if(parametroSistema == null) {
					throw new NegocioException(Constantes.ERRO_PARAMETRO_NEGOCIO, codigoParametro);
				}

				this.validarDadosParametro(valorParametro, codigoParametro, nomeAba, parametroSistema.getDescricao());

				if(!valorParametro.equals(parametroSistema.getValor())) {

					parametroSistema.setValor(valorParametro);
					parametroSistema.setDadosAuditoria(dadosAuditoria);
					this.atualizar(parametroSistema);

				}

			}

		}

	}

	/**
	 * Validar dados parametro.
	 *
	 * @param valorParametro
	 *            the valor parametro
	 * @param codigoParametro
	 *            the codigo parametro
	 * @param nomeAba
	 *            the nome aba
	 * @param descricaoParametro
	 *            the descricao parametro
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosParametro(String valorParametro, String codigoParametro, String nomeAba, String descricaoParametro)
					throws NegocioException {

		if(valorParametro == null) {

			throw new NegocioException(ERRO_NEGOCIO_PARAMETRO_SISTEMA_VALOR_NULO, new Object[] {nomeAba, descricaoParametro});

		} else {

			if(codigoParametro.equals(Constantes.PARAMETRO_QUANTIDADE_MINIMA_CRONOGRAMAS_ABERTOS)) {

				if(StringUtils.isNumeric(valorParametro)) {

					Integer valor = Integer.valueOf(valorParametro);

					if(valor < 1) {

						throw new NegocioException(ERRO_NEGOCIO_PARAMETRO_SISTEMA_QUANTIDADE_MINIMA_CRONOGRAMAS_ROTA_MENOR_QUE_1,
										new Object[] {nomeAba, descricaoParametro});
					}

				} else {
					throw new NegocioException(ERRO_NEGOCIO_PARAMETRO_SISTEMA_VALOR_INVALIDO, new Object[] {nomeAba, descricaoParametro});
				}

			} else if (codigoParametro.equals(Constantes.PRAZO_MAXIMO_CRITICIDADE)) {
				Integer valorMaximo = Integer.valueOf(valorParametro);
				Integer valorMinimo = obterParametroPorCodigo(Constantes.PRAZO_MINIMO_CRITICIDADE).getValorInteger();
				
				if(valorMaximo < valorMinimo) {
					throw new NegocioException(ERRO_NEGOCIO_PRAZO_MAXIMO_MENOR_MINIMO, new Object[] {nomeAba});
				}
			} else if ((codigoParametro.equals(Constantes.LATITUDE_EMPRESA)
					|| codigoParametro.equals(Constantes.LONGITUDE_EMPRESA)) && valorParametro.indexOf(',') != -1) {
				throw new NegocioException(ERRO_FORMATO_INVALIDO_LATITUDE_LONGITUDE, new Object[] { nomeAba });
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroIntegracaoLimiteEnderecoCliente()
	 */
	@Override
	public ParametroSistema obterParametroIntegracaoLimiteEnderecoCliente()
					throws NegocioException {
		return obterParametroPorCodigo(
						Constantes.P_LIMITE_INTEGRACAO_ENDERECO_CLIENTE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroAlteraVencimentoFaturaAutomatico()
	 */
	@Override
	public ParametroSistema obterParametroAlteraVencimentoFaturaAutomatico()
			throws NegocioException {
		return obterParametroPorCodigo(
				Constantes.ALTERA_VENCIMENTO_FATURA_AUTOMATICO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroAlteraVencimentoFaturaDebitoAtraso()
	 */
	@Override
	public ParametroSistema obterParametroAlteraVencimentoFaturaDebitoAtraso()
			throws NegocioException {
		return obterParametroPorCodigo(
				Constantes.ALTERA_VENCIMENTO_FATURA_DEBITO_ATRASO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroPrazoAlteraVencimentoFaturaAutomatico()
	 */
	@Override
	public ParametroSistema obterParametroPrazoAlteraVencimentoFaturaAutomatico()
			throws NegocioException {
		return obterParametroPorCodigo(
				Constantes.PRAZO_ALTERA_VENCIMENTO_FATURA_AUTOMATICO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroEnviaSms()
	 */
	@Override
	public ParametroSistema obterParametroEnviaSms()
			throws NegocioException {
		return obterParametroPorCodigo(Constantes.ENVIA_SMS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterValorParametroEnviaSms()
	 */
	@Override
	public boolean obterValorParametroEnviaSms()
					throws NegocioException {
		return BooleanUtil.converterStringCharParaBooleano(
						obterParametroEnviaSms().getValor(), true);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroCredencialSms()
	 */
	@Override
	public ParametroSistema obterParametroCredencialSms()
			throws NegocioException {
		return obterParametroPorCodigo(Constantes.CREDENCIAL_SMS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroProjetoSms()
	 */
	@Override
	public ParametroSistema obterParametroProjetoSms()
			throws NegocioException {
		return obterParametroPorCodigo(Constantes.PROJETO_SMS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroUsuarioAuxiliarSms()
	 */
	@Override
	public ParametroSistema obterParametroUsuarioAuxiliarSms()
			throws NegocioException {
		return obterParametroPorCodigo(Constantes.USUARIO_AUXILIAR_SMS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroProxyHostSms()
	 */
	@Override
	public ParametroSistema obterParametroProxyHostSms()
			throws NegocioException {
		return obterParametroPorCodigo(Constantes.PROXY_HOST_SMS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroProxyPortaSms()
	 */
	@Override
	public ParametroSistema obterParametroProxyPortaSms()
			throws NegocioException {
		return obterParametroPorCodigo(Constantes.PROXY_PORTA_SMS);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroEmailRemetentePadrao()
	 */
	@Override
	public ParametroSistema obterParametroEmailRemetentePadrao()
			throws NegocioException {
		return obterParametroPorCodigo(Constantes.PARAMETRO_EMAIL_REMETENTE_PADRAO);
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroCodigoSituacaoRecebimentoDocumentoInexistente()
	 */
	@Override
	public ParametroSistema obterParametroCodigoSituacaoRecebimentoDocumentoInexistente()
					throws NegocioException {
		return obterParametroPorCodigo(Constantes.PARAMETRO_CODIGO_SITUACAO_RECEBIMENTO_DOCTO_INEXISTENTE);
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterParametroVigenciaSubstituicaoTributaria()
	 */
	@Override
	public ParametroSistema obterParametroVigenciaSubstituicaoTributaria()
					throws NegocioException {
		return obterParametroPorCodigo(Constantes.P_VIGENCIA_SUBSTITUICAO_TRIBUTARIA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.parametrosistema
	 * .ControladorParametroSistema
	 * #obterValorParametroEmailRemetentePadrao()
	 */
	@Override
	public String obterValorParametroEmailRemetentePadrao()
			throws NegocioException {
		return obterParametroEmailRemetentePadrao().getValor();
	}
	
	/* (non-Javadoc)
	 * @see br.com.ggas.parametrosistema.ControladorParametroSistema#obterValorParametroUtilizacaoMultiplosCiclos()
	 */
	@Override
	public boolean obterValorParametroUtilizacaoMultiplosCiclos()
			throws NegocioException {
		ParametroSistema  parametro =  obterParametroPorCodigo(Constantes.PARAMETRO_UTILIZAR_MULTIPLOS_CICLOS);
		if( parametro == null || !StringUtils.isNumeric(parametro.getValor())) {
			return false;
		}
		return Integer.parseInt(parametro.getValor()) != VALOR_INTEIRO_PARA_FALSO;
	}

	@Override
	public boolean obterIndicadorFaturamentoParalelo() throws NegocioException {
		return BooleanUtil.converterStringCharParaBooleano(obterParametroPorCodigo(Constantes.INDICADOR_FATURAMENTO_PARALELO).getValor(), true);
	}
	
	@Override
	public boolean obterIndicadorContabilizacaoLancamentoContabil() throws NegocioException {
		return BooleanUtil.converterStringCharParaBooleano(
				obterParametroPorCodigo(Constantes.INDICADOR_CONTABILIZAR_LANCAMENTO_CONTABIL).getValor(), true);
	}
	
	@Override
	public boolean obterIndicadorAmbienteProducao() throws NegocioException {
		return BooleanUtil.converterStringCharParaBooleano(obterParametroPorCodigo(Constantes.INDICADOR_AMBIENTE_PRODUCAO).getValor(), true);
	}

	@Override
	public ParametroSistema obterParametroSistemaPorCodigoSemCache(String codigo) {
		
		LOG.debug("### obterParametroPorCodigoSemCache: " + codigo);
		
		parametroCache.clear();
		
		if(!parametroCache.containsKey(codigo)){
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where ");
			hql.append(" codigo = ? ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setString(0, codigo);
			query.setCacheable(true);
			
			ParametroSistema parametro = (ParametroSistema) query.uniqueResult();
			parametroCache.put(codigo, parametro);
		}
		return parametroCache.get(codigo);
	}

	@Override
	public void incluirCertificadoDigital(MultipartFile arquivoCertificado, String senhaCertificado)
			throws IOException, NegocioException, ConcorrenciaException {
		CertificadoDigital certificadoDigital = this.obterCertificadoDigital();
		Boolean isInclusao = Boolean.FALSE;

		if (certificadoDigital == null) {
			certificadoDigital = (CertificadoDigital) this.criarCertificadoDigital();
			isInclusao = Boolean.TRUE;
		}

		certificadoDigital.setArquivo(arquivoCertificado.getBytes());
		certificadoDigital.setSenha(senhaCertificado);

		if (isInclusao) {
			this.inserir(certificadoDigital);
		} else {
			this.atualizar(certificadoDigital, CertificadoDigital.class);
		}

	}

	
	@Override
	public CertificadoDigital obterCertificadoDigital() {
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCertificadoDigital().getSimpleName());
		
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setMaxResults(1);
		
		return (CertificadoDigital) query.uniqueResult();

	}
}
