/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.parametrosistema;

import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.controleacesso.Modulo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface ParametroSistema extends EntidadeNegocio {

	String BEAN_ID_PARAMETRO_SISTEMA = "parametroSistema";

	/**
	 * ENTIDADE_ROTULO_PARAMETRO_SISTEMA
	 */
	String ENTIDADE_ROTULO_PARAMETRO_SISTEMA = "ENTIDADE_ROTULO_PARAMETRO_SISTEMA";

	/**
	 * PARAMETRO_SISTEMA_ROTULO_CODIGO
	 */
	String PARAMETRO_SISTEMA_ROTULO_CODIGO = "PARAMETRO_SISTEMA_ROTULO_CODIGO";

	/**
	 * PARAMETRO_SISTEMA_ROTULO_VALOR
	 */
	String PARAMETRO_SISTEMA_ROTULO_VALOR = "PARAMETRO_SISTEMA_ROTULO_VALOR";

	/**
	 * PARAMETRO_SISTEMA_ROTULO_DESCRICAO
	 */
	String PARAMETRO_SISTEMA_ROTULO_DESCRICAO = "PARAMETRO_SISTEMA_ROTULO_DESCRICAO";

	/**
	 * PARAMETRO_SISTEMA_ROTULO_HABILITADO
	 */
	String PARAMETRO_SISTEMA_ROTULO_HABILITADO = "PARAMETRO_SISTEMA_ROTULO_HABILITADO";

	/**
	 * PARAMETRO_SISTEMA_ROTULO_TIPO
	 */
	String PARAMETRO_SISTEMA_ROTULO_TIPO = "PARAMETRO_SISTEMA_ROTULO_TIPO";

	/**
	 * TAMANHO_DESCRICAO_COMPLEMENTAR_CRE_DEB_REALIZAR
	 */
	String TAMANHO_DESCRICAO_COMPLEMENTAR_CRE_DEB_REALIZAR = "TAMANHO_DESCRICAO_COMPLEMENTAR_CRE_DEB_REALIZAR";

	/**
	 * TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA
	 */
	String TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA = "TEXTO_SUBSTITUICAO_TRIBUTARIA_NOTA";

	String NUMERO_MAXIMO_DIGITOS_CORRETOR = "NUMERO_MAXIMO_DIGITOS_CORRETOR";
	
	String OBRIGATORIO_RETORNO_PROTOCOLO = "OBRIGATORIO_RETORNO_PROTOCOLO";
	
	String NUMERO_DIAS_CHECAGEM_CORTE = "NUMERO_DIAS_CHECAGEM_CORTE";

	/**
	 * @return the codigo
	 */
	String getCodigo();

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	void setCodigo(String codigo);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the tipoParametroSistema
	 */
	TipoParametroSistema getTipoParametroSistema();

	/**
	 * @param tipoParametroSistema
	 *            the tipoParametroSistema to set
	 */
	void setTipoParametroSistema(TipoParametroSistema tipoParametroSistema);

	/**
	 * @return the classeEntidade
	 */
	String getClasseEntidade();

	/**
	 * @param classeEntidade
	 *            the classeEntidade to set
	 */
	void setClasseEntidade(String classeEntidade);

	/**
	 * @return the valor
	 */
	String getValor();

	/**
	 * @param valor
	 *            the valor to set
	 */
	void setValor(String valor);

	/**
	 * @return the habilitado
	 */
	@Override
	boolean isHabilitado();

	/**
	 * @param habilitado
	 *            the habilitado to set
	 */
	@Override
	void setHabilitado(boolean habilitado);

	/**
	 * @return
	 */
	Modulo getModulo();

	/**
	 * @param modulo
	 */
	void setModulo(Modulo modulo);

	/**
	 * @return
	 */
	Tabela getTabela();

	/**
	 * @param tabela
	 */
	void setTabela(Tabela tabela);

	/**
	 * Gets the valor long.
	 *
	 * @return the valor long
	 * @throws NegocioException the negocio exception
	 */
	Long getValorLong() throws NegocioException;

	/**
	 * Gets the valor integer.
	 *
	 * @return the valor integer
	 * @throws NegocioException the negocio exception
	 */
	Integer getValorInteger() throws NegocioException;

}
