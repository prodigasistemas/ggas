package br.com.ggas.parametrosistema;

import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface CertificadoDigital extends EntidadeNegocio {
	String BEAN_ID_CERTIFICADO_DIGITAL = "certificadoDigital";
	
	public byte[] getArquivo();
	
	public void setArquivo(byte[] arquivo);
	
	String getSenha();

	void setSenha(String senha); 
}