/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.parametrosistema;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

public interface ControladorParametroSistema extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA = "controladorParametroSistema";

	String ERRO_NEGOCIO_PARAMETRO_SISTEMA_NAO_ENCONTRADO = "ERRO_NEGOCIO_PARAMETRO_SISTEMA_NAO_ENCONTRADO";

	String ERRO_NEGOCIO_PARAMETRO_SISTEMA_VALOR_NULO = "ERRO_NEGOCIO_PARAMETRO_SISTEMA_VALOR_NULO";
	
	String ERRO_NEGOCIO_PRAZO_MAXIMO_MENOR_MINIMO = "ERRO_NEGOCIO_PRAZO_MAXIMO_MENOR_MINIMO";

	String ERRO_NEGOCIO_PARAMETRO_SISTEMA_VALOR_INVALIDO = "ERRO_NEGOCIO_PARAMETRO_SISTEMA_VALOR_INVALIDO";

	String ERRO_NEGOCIO_PARAMETRO_SISTEMA_QUANTIDADE_MINIMA_CRONOGRAMAS_ROTA_MENOR_QUE_1 = 
					"ERRO_NEGOCIO_PARAMETRO_SISTEMA_QUANTIDADE_MINIMA_CRONOGRAMAS_ROTA_MENOR_QUE_1";

	String DIRETORIO_ARQUIVOS_RETORNO = "DIRETORIO_ARQUIVOS_RETORNO";

	String DIRETORIO_ARQUIVOS_UPLOAD = "DIRETORIO_ARQUIVOS_UPLOAD";
	
	String ERRO_FORMATO_INVALIDO_LATITUDE_LONGITUDE = "ERRO_FORMATO_INVALIDO_LATITUDE_LONGITUDE";

	/**
	 * Método responsável por inserir um parâmetro
	 * no sistema.
	 * 
	 * @param codigo
	 *            O código do parâmetro.
	 * @param descricao
	 *            A descrição do parâmetro.
	 * @param valor
	 *            O Valor do parâmetro.
	 * @param tipoParametro
	 *            O tipo do parâmetro.
	 * @param dadosAuditoria
	 *            Dados da auditoria
	 * @return the long
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	long inserirParametroSistema(String codigo, String descricao, Object valor, int tipoParametro, DadosAuditoria dadosAuditoria)
					throws NegocioException;

	/**
	 * Método responsável por obter o parâmetro
	 * através de um código.
	 * 
	 * @param codigo
	 *            O código do parâmetro.
	 * @return O parâmetro
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	ParametroSistema obterParametroPorCodigo(String codigo) throws NegocioException;

	/**
	 * Método responsável por obter o parâmetro
	 * através de um código informando se é para colocar
	 * na cache.
	 *
	 * @param codigo 
	 *            O código do parâmetro.
	 * @param cacheable the cacheable
	 * 			  Indica se é para colocar na cache
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroPorCodigo(String codigo, boolean cacheable) throws NegocioException;

	/**
	 * Método responsável por obter um valor do
	 * parâmetro através de um código.
	 * 
	 * @param codigo
	 *            O código do parâmetro.
	 * @param variaveis
	 *            As variáveis que podem ser
	 *            usadas para obter o valor do
	 *            parametro
	 * @return O Valor do parâmetro.
	 * @throws NegocioException
	 *             NegocioException Caso ocora
	 *             algum erro na execução do
	 *             método.
	 */
	Object obterValorDoParametroPorCodigo(String codigo, Variavel... variaveis) throws NegocioException;

	/**
	 * Método responsável por consultar todos os
	 * parametros do sistema.
	 * 
	 * @return Uma lista de parametros.
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	List<ParametroSistema> consultarParametroSistema() throws NegocioException;

	/**
	 * Método responsável por criar um tipo de
	 * parametro.
	 * 
	 * @return Um TipoParametroSistema
	 * @throws NegocioException
	 *             Caso ocora algum erro na
	 *             execução do método.
	 */
	TipoParametroSistema criarTipoParametroSistema() throws NegocioException;

	/**
	 * Método responsável por obter um tipo de
	 * parametro.
	 * 
	 * @param tipoParametro
	 *            O código to tipo de parametro
	 * @return Um TipoParametroSistema
	 * @throws NegocioException
	 *             NegocioException Caso ocora
	 *             algum erro na execução do
	 *             método.
	 */
	TipoParametroSistema obterTipoParametroSistema(int tipoParametro) throws NegocioException;

	/**
	 * Atualizar.
	 * 
	 * @param parametroSistema
	 *            the parametro sistema
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void atualizar(ParametroSistema parametroSistema) throws ConcorrenciaException, NegocioException;

	/**
	 * Método responsável por obter todos os
	 * parametros de um determinado modulo do
	 * sistema.
	 * 
	 * @param idModulo
	 *            the id modulo
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<ParametroSistema> obterParametrosSistemaPorModulo(long idModulo) throws NegocioException;

	/**
	 * Método responsável por salvar as alterações
	 * de cada parametro do sistema.
	 * 
	 * @param dados
	 *            the dados
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void salvarAlteracaoParametros(Map<String, Map<String, String>> dados, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException;

	/**
	 * Obter codigo do parametro por valor.
	 * 
	 * @param codigoParametroEncargo
	 *            the codigo parametro encargo
	 * @return the parametro sistema
	 */
	ParametroSistema obterCodigoDoParametroPorValor(String codigoParametroEncargo);
	
	/**
	 * Obter parametro integracao limite cliente enderecos
	 * (descricao = "P_LIMITE_INTEGRACAO_ENDERECO_CLIENTE")
	 * 
	 * @return parametro integracao cadastro clientes
	 * @throws NegocioException
	 */
	ParametroSistema obterParametroIntegracaoLimiteEnderecoCliente() throws NegocioException;

	/**
	 * Obter parametro altera vencimento fatura automatico.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroAlteraVencimentoFaturaAutomatico() throws NegocioException;

	
	/**
	 * Obter parametro altera vencimento fatura debito atraso.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroAlteraVencimentoFaturaDebitoAtraso() throws NegocioException;

	/**
	 * Obter parametro prazo altera vencimento fatura automatico.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroPrazoAlteraVencimentoFaturaAutomatico() throws NegocioException;

	/**
	 * Obter parametro envia sms.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroEnviaSms() throws NegocioException;

	/**
	 * Obter valor parametro envia sms.
	 *
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	boolean obterValorParametroEnviaSms() throws NegocioException;

	/**
	 * Obter parametro credencial sms.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroCredencialSms() throws NegocioException;

	/**
	 * Obter parametro projeto sms.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroProjetoSms() throws NegocioException;

	/**
	 * Obter parametro usuario auxiliar sms.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroUsuarioAuxiliarSms() throws NegocioException;

	/**
	 * Obter parametro proxy host sms.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroProxyHostSms() throws NegocioException;

	/**
	 * Obter parametro proxy porta sms.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroProxyPortaSms() throws NegocioException;

	/**
	 * Obter valor parametro email remetente padrao.
	 *
	 * @return the string
	 * @throws NegocioException the negocio exception
	 */
	String obterValorParametroEmailRemetentePadrao() throws NegocioException;

	/**
	 * Obter parametro email remetente padrao.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroEmailRemetentePadrao() throws NegocioException;

	/**
	 * Obter parametro codigo situacao recebimento documento inexistente.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroCodigoSituacaoRecebimentoDocumentoInexistente() throws NegocioException;

	/**
	 * Obter parametro vigencia substituicao tributaria.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	ParametroSistema obterParametroVigenciaSubstituicaoTributaria() throws NegocioException;
	
	/**
	 * Obter o valor do parametro de utilizacao do multiplos ciclos.
	 *
	 * @return the parametro sistema
	 * @throws NegocioException the negocio exception
	 */
	boolean obterValorParametroUtilizacaoMultiplosCiclos() throws NegocioException;

	/**
	 * Obtém o parâmetro que indica se o faturamento executa o preparar e processar dados em paralelo.
	 * @return indicador - boolean
	 * @throws NegocioException - {@link NegocioException}
	 */
	boolean obterIndicadorFaturamentoParalelo() throws NegocioException;

	/**
	 * Obtém o parâmetro que indica se o faturamento executa a contabilização dos lançamentos contábeis.
	 * @return indicador - boolean
	 * @throws NegocioException - {@link NegocioException}
	 */
	boolean obterIndicadorContabilizacaoLancamentoContabil() throws NegocioException;
	
	/**
	 * Obtém o parâmetro que indica se o amviente e o de producao.
	 * @return indicador - boolean
	 * @throws NegocioException - {@link NegocioException}
	 */
	boolean obterIndicadorAmbienteProducao() throws NegocioException;
	
	ParametroSistema obterParametroSistemaPorCodigoSemCache(String codigo);

	void incluirCertificadoDigital(MultipartFile arquivoCertificado, String senhaCertificado) throws IOException, NegocioException, ConcorrenciaException;

	CertificadoDigital obterCertificadoDigital();
}
