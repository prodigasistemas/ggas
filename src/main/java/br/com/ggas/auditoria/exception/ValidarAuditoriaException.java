
package br.com.ggas.auditoria.exception;

import org.hibernate.CallbackException;
/**
 *	Classe responsável pela Exception lançada pela auditoria caso aconteça um erro
 */
public class ValidarAuditoriaException extends CallbackException {

	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new validar auditoria exception.
	 * 
	 * @param root
	 *            the root
	 */
	public ValidarAuditoriaException(Exception root) {

		super(root);
	}

	/**
	 * Instantiates a new validar auditoria exception.
	 * 
	 * @param message
	 *            the message
	 * @param e
	 *            the e
	 */
	public ValidarAuditoriaException(String message, Exception e) {

		super(message, e);
	}

	/**
	 * Instantiates a new validar auditoria exception.
	 * 
	 * @param message
	 *            the message
	 */
	public ValidarAuditoriaException(String message) {

		super(message);
	}
}
