/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.auditoria;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
/**
 *	Interface responsável pelos métodos relacionados ao controlador de auditoria
 */
public interface ControladorAuditoria extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_AUDITORIA = "controladorAuditoria";

	String ERRO_NEGOCIO_TABELA_VAZIA = "ERRO_NEGOCIO_TABELA_VAZIA";

	/**
	 * Método para obter a lista de colunas
	 * auditáveis.
	 * 
	 * @return the collection
	 */
	public Collection<Coluna> obterListaColunasAuditaveis();

	/**
	 * Método para listar as tabelas do sistema.
	 * 
	 * @return Collection<Tabela>
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Tabela> listarTabelas() throws NegocioException;

	/**
	 * Método para listar as tabelas selecionadas
	 * do sistema.
	 * 
	 * @param idTabela
	 *            the id tabela
	 * @return Tabela
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Tabela obterTabelaSelecionada(Long idTabela) throws NegocioException;

	/**
	 * Obter tabela.
	 * 
	 * @param idTabela
	 *            the id tabela
	 * @return the tabela
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Tabela obterTabela(Long idTabela) throws NegocioException;

	/**
	 * Método para listar as colunas da tabela
	 * selecionada.
	 * 
	 * @param idTabela
	 *            the id tabela
	 * @return Collection<ColunaTabela>
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Coluna> listarColunasTabelaSelecionada(Long idTabela) throws NegocioException;

	/**
	 * Obtem um objeto Tabela pela chave primária
	 * @param idMenu A chave primária da tabela
	 * @return O objeto Tabela
	 * @throws NegocioException O Negocio Exception
	 */
	Tabela obterTabelaMenu(Long idMenu) throws NegocioException;

	/**
	 * Verificar colunas auditaveis.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param colunasAuditaveis
	 *            the colunas auditaveis
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the tabela
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Tabela verificarColunasAuditaveis(Long chavePrimaria, String[] colunasAuditaveis, DadosAuditoria dadosAuditoria)
					throws NegocioException;

	/**
	 * Atualizar.
	 * 
	 * @param tabela
	 *            the tabela
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException {@link GGASException}
	 */
	void atualizar(Tabela tabela) throws GGASException;

	/**
	 * Listar colunas tabela variavel.
	 * 
	 * @param idTabela
	 *            the id tabela
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Coluna> listarColunasTabelaVariavel(Long idTabela) throws NegocioException;

	/**
	 * Obter coluna tabela por variavel.
	 * 
	 * @param variavel
	 *            the variavel
	 * @return the coluna
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Coluna obterColunaTabelaPorVariavel(String variavel) throws NegocioException;

	/**
	 * Listar tabela variavel.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Coluna> listarTabelaVariavel() throws NegocioException;

	/**
	 * Listar tabelas constantes.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<Tabela> listarTabelasConstantes() throws NegocioException;

	/**
	 * Consultar tabela por nome da classe.
	 * 
	 * @param className
	 *            the class name
	 * @return the tabela
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Tabela consultarTabelaPorNomeDaClasse(String className) throws GGASException;

	/**
	 * Consulta entidades do tipo {@link Coluna} pelo atributo: {@code codigoVariavel}.
	 * @param codigos - {@link Set}
	 * @return mapa de {@link Coluna} por atributo {@code codigoVariavel} - {@link Map}
	 */
	Map<String, Coluna> obterColunasPorCodigoVariavel(Set<String> codigos);
}
