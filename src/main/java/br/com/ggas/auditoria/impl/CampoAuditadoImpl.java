/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.auditoria.impl;

import br.com.ggas.auditoria.CampoAuditado;
/**
 * Classe responsável pelos métodos da implementacao da CampoAuditadoImpl
 */
class CampoAuditadoImpl implements CampoAuditado {

	private static final long serialVersionUID = 2488666010865303409L;

	private long chavePrimaria;

	private String coluna;

	private String valor;

	/**
	 * @return the chavePrimaria
	 */
	@Override
	public long getChavePrimaria() {

		return chavePrimaria;
	}

	/**
	 * @param chavePrimaria
	 *            the chavePrimaria to set
	 */
	@Override
	public void setChavePrimaria(long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	/**
	 * @return the coluna
	 */
	@Override
	public String getColuna() {

		return coluna;
	}

	/**
	 * @param coluna
	 *            the coluna to set
	 */
	@Override
	public void setColuna(String coluna) {

		this.coluna = coluna;
	}

	/**
	 * @return the valor
	 */
	@Override
	public String getValor() {

		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	@Override
	public void setValor(String valor) {

		this.valor = valor;
	}

}
