/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.auditoria.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.cadastro.imovel.impl.TabelaImpl;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
/**
 * Classe reponsável pelas ações relacionadas a auditoria
 */
public class ControladorAuditoriaImpl extends ControladorNegocioImpl implements ControladorAuditoria {

	private static final String TABELA = " tabela ";
	private static final String COLUNA_TABELA_HABILITADO_TRUE = " colunaTabela.habilitado = true ";
	private static final String WHERE = " where ";
	private static final String FROM = " from ";
	private static final String ID_TABELA = "idTabela";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	public Class<?> getClasseEntidadeTabela() {

		return ServiceLocator.getInstancia().getClassPorID(Tabela.BEAN_ID_TABELA);
	}

	public Class<?> getClasseEntidadeColunaTabela() {

		return ServiceLocator.getInstancia().getClassPorID(Coluna.BEAN_ID_COLUNA_TABELA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#obterListaColunasAuditaveis()
	 */
	@SuppressWarnings({"squid:S1192", "unchecked"})
	@Override
	public Collection<Coluna> obterListaColunasAuditaveis() {

		StringBuilder hql = new StringBuilder();
		hql.append(" select colunaTabela ");
		hql.append(FROM);
		hql.append(getClasseEntidadeColunaTabela().getSimpleName());
		hql.append(" colunaTabela ");
		hql.append(WHERE);
		hql.append(COLUNA_TABELA_HABILITADO_TRUE);
		hql.append(" and colunaTabela.auditavel = true ");
		hql.append(" and colunaTabela.habilitado = true ");
		hql.append(" and colunaTabela.nomePropriedade is not null ");
		hql.append(" and colunaTabela.tabela.auditavel = true ");
		hql.append(" and colunaTabela.tabela.habilitado = true ");
		hql.append(" and colunaTabela.tabela.nomeClasse is not null ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#listarTabelas()
	 */
	@SuppressWarnings({"squid:S1192", "unchecked"})
	@Override
	public Collection<Tabela> listarTabelas() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeTabela().getSimpleName());
		hql.append(TABELA);
		hql.append(WHERE);
		hql.append(" tabela.habilitado = true ");
		hql.append(" order by tabela.nome asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	@Override
	public Tabela obterTabelaMenu(Long idMenu) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append("from TabelaImpl tabela");
		hql.append(" where tabela.menu.chavePrimaria = :idMenu");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idMenu", idMenu);
		
		if(query.list() !=null && query.list().size()>1){
			return (Tabela) query.list().get(0);
		}

		return (Tabela) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#obterTabelaSelecionada(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Tabela obterTabelaSelecionada(Long idTabela) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeTabela().getSimpleName());
		hql.append(TABELA);
		hql.append(" left join fetch  tabela.listaColunaTabela ");
		hql.append(WHERE);
		hql.append(" tabela.habilitado = true ");
		hql.append("and tabela.chavePrimaria = :idTabela ");
		hql.append("order by tabela.nome asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_TABELA, idTabela);

		return (Tabela) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#obterTabela(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("squid:S1192")
	public Tabela obterTabela(Long idTabela) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeTabela().getSimpleName());
		hql.append(TABELA);
		hql.append(WHERE);
		hql.append(" tabela.habilitado = true ");
		hql.append("and tabela.chavePrimaria = :idTabela ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_TABELA, idTabela);

		return (Tabela) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#consultarTabelaPorNomeDaClasse(java.lang.String)
	 */
	@Override
	public Tabela consultarTabelaPorNomeDaClasse(String className) throws GGASException {

		StringBuilder hql = new StringBuilder();
		Query query;

		hql.append("from TabelaImpl tabela");
		hql.append(" where tabela.nomeClasse = :nomeClasse");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setCacheable(true);

		query.setParameter("nomeClasse", className);

		Tabela tabela = (TabelaImpl) query.uniqueResult();

		if(tabela == null) {
			throw new GGASException(Constantes.ERRO_TABELA_NAO_ENCONTRADA, new Object[] {className});
		}

		return tabela;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#listarColunasTabelaSelecionada(java.lang.Long)
	 */
	@Override
	@SuppressWarnings({"squid:S1192", "unchecked"})
	public Collection<Coluna> listarColunasTabelaSelecionada(Long idTabela) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct  colunaTabela");
		hql.append(FROM);
		hql.append(getClasseEntidadeColunaTabela().getSimpleName());
		hql.append(" colunaTabela inner join fetch colunaTabela.tabela ");
		hql.append(WHERE);
		hql.append(COLUNA_TABELA_HABILITADO_TRUE);
		hql.append("and colunaTabela.tabela.chavePrimaria = :idTabela ");
		hql.append("order by colunaTabela.descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_TABELA, idTabela);

		if(query.list().isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_TABELA_VAZIA, true);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#verificarColunasAuditaveis(java.lang.Long, java.lang.String[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public Tabela verificarColunasAuditaveis(Long chavePrimaria, String[] colunasAuditaveis, DadosAuditoria dadosAuditoria)
					throws NegocioException {

		Boolean auditavel = false;

		Tabela tabela = obterTabela(chavePrimaria);

		if(colunasAuditaveis != null && colunasAuditaveis.length > 0) {
			for (Coluna colunaTabela : tabela.getListaColunaTabela()) {
				colunaTabela.setAuditavel(false);
				auditavel = verificaSeAuditaveis(colunasAuditaveis, dadosAuditoria, auditavel, colunaTabela);
			}

		} else {
			for (Coluna colunaTabela : tabela.getListaColunaTabela()) {
				colunaTabela.setAuditavel(false);
				colunaTabela.setDadosAuditoria(dadosAuditoria);
			}
		}

		tabela.setAuditavel(auditavel);
		return tabela;
	}

	/**
	 * Método responsável por verificar se colunas são auditáveis.
	 * 
	 * @param colunasAuditaveis
	 * @param dadosAuditoria
	 * @param auditavel
	 * @param colunaTabela
	 * @return boolean
	 */
	private Boolean verificaSeAuditaveis(String[] colunasAuditaveis, DadosAuditoria dadosAuditoria, Boolean auditavel,
			Coluna colunaTabela) {
		
		Boolean permiteAuditacao = auditavel;
		
		for (int i = 0; i < colunasAuditaveis.length; i++) {
			if(colunaTabela.getChavePrimaria() == Long.parseLong(colunasAuditaveis[i])) {
				permiteAuditacao = true;
				colunaTabela.setAuditavel(true);
				colunaTabela.setDadosAuditoria(dadosAuditoria);
				break;
			}
		}
		return permiteAuditacao;
	}

	/**
	 * Atualizar tabela.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param auditavel
	 *            the auditavel
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void atualizarTabela(Long chavePrimaria, Boolean auditavel) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" update ");
		hql.append(getClasseEntidadeTabela().getSimpleName());
		hql.append(" set ");
		hql.append(" auditavel = :auditavel");
		hql.append(" where chavePrimaria = :chavePrimaria");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setBoolean("auditavel", auditavel);
		query.setLong("chavePrimaria", chavePrimaria);
		query.executeUpdate();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#atualizar(br.com.ggas.cadastro.imovel.Tabela)
	 */
	@Override
	public void atualizar(Tabela tabela) throws NegocioException, ConcorrenciaException {

		super.validarDadosEntidade(tabela);
		super.validarVersaoEntidade(tabela, this.getClasseEntidadeTabela());
		atualizarListaColunas(tabela);
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(tabela);
	}

	/**
	 * Atualizar lista colunas.
	 * 
	 * @param tabela
	 *            the tabela
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void atualizarListaColunas(Tabela tabela) throws NegocioException {

		Boolean auditavel = tabela.getAuditavel();
		List<Coluna> colunas = new ArrayList<>();
		for (Coluna c : tabela.getListaColunaTabela()) {
			colunas.add(c);
		}
		getHibernateTemplate().getSessionFactory().getCurrentSession().refresh(tabela);

		for (Coluna c : tabela.getListaColunaTabela()) {
			for (Coluna coluna : colunas) {
				if(c.getChavePrimaria() == coluna.getChavePrimaria()) {
					c.setAuditavel(coluna.getAuditavel());
					break;
				}
			}
		}
		atualizarTabela(tabela.getChavePrimaria(), auditavel);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#listarColunasTabelaVariavel(java.lang.Long)
	 */
	@SuppressWarnings({"squid:S1192", "unchecked"})
	@Override
	public Collection<Coluna> listarColunasTabelaVariavel(Long idTabela) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct  colunaTabela");
		hql.append(FROM);
		hql.append(getClasseEntidadeColunaTabela().getSimpleName());
		hql.append(" colunaTabela inner join fetch colunaTabela.tabela ");
		hql.append(WHERE);
		hql.append(COLUNA_TABELA_HABILITADO_TRUE);
		hql.append(" and colunaTabela.variavel = true ");
		hql.append("and colunaTabela.tabela.chavePrimaria = :idTabela ");
		hql.append("order by colunaTabela.descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(ID_TABELA, idTabela);

		if(query.list().isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_TABELA_VAZIA, true);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#obterColunaTabelaPorVariavel(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Coluna obterColunaTabelaPorVariavel(String variavel) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct  colunaTabela");
		hql.append(FROM);
		hql.append(getClasseEntidadeColunaTabela().getSimpleName());
		hql.append(" colunaTabela inner join fetch colunaTabela.tabela ");
		hql.append(WHERE);
		hql.append(COLUNA_TABELA_HABILITADO_TRUE);
		hql.append(" and colunaTabela.variavel = true ");
		hql.append(" and colunaTabela.codigoVariavel = :codigoVariavel ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("codigoVariavel", variavel);

		return (Coluna) query.uniqueResult();
	}
	
	
	
	/**
	 * Consulta entidades do tipo {@link Coluna} pelo atributo: {@code codigoVariavel}.
	 * @param codigos - {@link Set}
	 * @return mapa de {@link Coluna} por atributo {@code codigoVariavel} - {@link Map}
	 */
	@Override
	public Map<String, Coluna> obterColunasPorCodigoVariavel(Set<String> codigos) {

		Map<String, Coluna> colunas = new HashMap<>();

		if (codigos != null && !codigos.isEmpty()) {

			StringBuilder hql = new StringBuilder();
			hql.append(" select distinct colunaTabela ");
			hql.append(FROM);
			hql.append(getClasseEntidadeColunaTabela().getSimpleName());
			hql.append(" colunaTabela ");
			hql.append(" inner join fetch colunaTabela.tabela ");
			hql.append(WHERE);
			hql.append(COLUNA_TABELA_HABILITADO_TRUE);
			hql.append(" and colunaTabela.variavel = true ");
			hql.append(" and colunaTabela.codigoVariavel IN (:codigos) ");

			Query query = super.getSession().createQuery(hql.toString());
			query.setParameterList("codigos", codigos);
			
			colunas.putAll(this.agruparColunaPorCodigoVariavel( query.list() ));
		}

		return colunas;
	}
	
	/**
	 * Mapeia uma lista de entidades do tipo {@link Coluna}
	 * pelo seu atributo: {@code codigoVariavel}.
	 * @param colunas - {@link List}
	 * @return mapa de {@link Coluna} por atributo {@code codigoVariavel} - {@link Map}
	 */
	private Map<String, Coluna> agruparColunaPorCodigoVariavel(List<Coluna> colunas) {
		
		Map<String, Coluna> colunasPorCodigo = new HashMap<>();
		
		if (!Util.isNullOrEmpty(colunas)) {
			
			for (Coluna coluna : colunas) {
				colunasPorCodigo.put(coluna.getCodigoVariavel(), coluna);
			}
		}
		return colunasPorCodigo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#listarTabelaVariavel()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Coluna> listarTabelaVariavel() {

		Criteria criteria = this.createCriteria(getClasseEntidadeColunaTabela(), "coluna");

		criteria.setProjection(Projections.distinct(Projections.property("coluna.tabela")));

		criteria.add(Restrictions.eq("coluna.habilitado", true));

		criteria.add(Restrictions.eq("coluna.variavel", true));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.ControladorAuditoria#listarTabelasConstantes()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Tabela> listarTabelasConstantes() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeTabela().getSimpleName());
		hql.append(TABELA);
		hql.append(WHERE);
		hql.append(" tabela.habilitado = true and ");
		hql.append(" tabela.constante = true ");
		hql.append(" order by tabela.nome asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

}
