/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.auditoria.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.util.Constantes;

class DadosAuditoriaImpl implements DadosAuditoria {

	private static final int LIMITE_CAMPO = 2;

	private static final long serialVersionUID = -5708474265708137220L;

	private Usuario usuario;

	private Operacao operacao;

	private String ip;

	private Date dataHora;

	private Long chaveAuditoriaPai;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.vo.DadosAuditoria
	 * #getUsuario()
	 */
	@Override
	public Usuario getUsuario() {

		return usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.vo.DadosAuditoria
	 * #setUsuario
	 * (br.com.ggas.controleacesso.Usuario)
	 */
	@Override
	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.vo.DadosAuditoria
	 * #getOperacao()
	 */
	@Override
	public Operacao getOperacao() {

		return operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.vo.DadosAuditoria
	 * #setOperacao
	 * (br.com.ggas.controleacesso.Operacao)
	 */
	@Override
	public void setOperacao(Operacao operacao) {

		this.operacao = operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.vo.DadosAuditoria
	 * #getIp()
	 */
	@Override
	public String getIp() {

		return ip;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.vo.DadosAuditoria
	 * #setIp(java.lang.String)
	 */
	@Override
	public void setIp(String ip) {

		this.ip = ip;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.vo.DadosAuditoria
	 * #getDataHora()
	 */
	@Override
	public Date getDataHora() {

		return dataHora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.vo.DadosAuditoria
	 * #setDataHora(java.util.Date)
	 */
	@Override
	public void setDataHora(Date dataHora) {

		this.dataHora = dataHora;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.DadosAuditoria#
	 * getChaveAuditoriaPai()
	 */
	@Override
	public Long getChaveAuditoriaPai() {

		return chaveAuditoriaPai;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.auditoria.DadosAuditoria#
	 * setChaveAuditoriaPai(java.lang.Long)
	 */
	@Override
	public void setChaveAuditoriaPai(Long chaveAuditoriaPai) {

		this.chaveAuditoriaPai = chaveAuditoriaPai;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.vo.DadosAuditoria
	 * #validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(this.operacao == null) {
			stringBuilder.append(Operacao.ENTIDADE_ROTULO_OPERACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.operacao != null && this.operacao.isAuditavel()) {
			if(this.usuario == null) {
				stringBuilder.append(Usuario.ENTIDADE_ROTULO_USUARIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if(this.ip == null || this.ip.length() == 0) {
				stringBuilder.append(DADOS_AUDITORIA_ROTULO_IP);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if(this.dataHora == null) {
				stringBuilder.append(DADOS_AUDITORIA_ROTULO_DATA_HORA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO));
		}

		return erros;
	}

}
