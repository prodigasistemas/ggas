/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.auditoria.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import br.com.ggas.auditoria.Auditoria;
import br.com.ggas.auditoria.CampoAuditado;

/**
 * Classe responsável pelos métodos da implementacao da AuditoriaImpl
 */
class AuditoriaImpl implements Auditoria {

	private static final long serialVersionUID = 9176505672569045488L;

	private long chavePrimaria;

	private long chaveEntidade;

	private Long chavePrimariaPai;

	private String classeEntidade;

	private String tabela;

	private String operacao;

	private String tipoOperacao;

	private String usuario;

	private Date dataHora;

	private String ip;

	private Collection<CampoAuditado> camposAuditados = new HashSet<>();

	/**
	 * @return the camposAuditados
	 */
	@Override
	public Collection<CampoAuditado> getCamposAuditados() {

		return camposAuditados;
	}

	/**
	 * @param camposAuditados
	 *            the camposAuditados to set
	 */
	@Override
	public void setCamposAuditados(Collection<CampoAuditado> camposAuditados) {

		this.camposAuditados = camposAuditados;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #getChavePrimaria()
	 */
	@Override
	public long getChavePrimaria() {

		return chavePrimaria;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #setChavePrimaria(long)
	 */
	@Override
	public void setChavePrimaria(long chavePrimaria) {

		this.chavePrimaria = chavePrimaria;
	}

	@Override
	public Long getChavePrimariaPai() {

		return chavePrimariaPai;
	}

	@Override
	public void setChavePrimariaPai(Long chavePrimariaPai) {

		this.chavePrimariaPai = chavePrimariaPai;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #getChaveEntidade()
	 */
	@Override
	public long getChaveEntidade() {

		return chaveEntidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #setChaveEntidade(long)
	 */
	@Override
	public void setChaveEntidade(long chaveEntidade) {

		this.chaveEntidade = chaveEntidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #getClasseEntidade()
	 */
	@Override
	public String getClasseEntidade() {

		return classeEntidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #setClasseEntidade(java.lang.String)
	 */
	@Override
	public void setClasseEntidade(String classeEntidade) {

		this.classeEntidade = classeEntidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.Auditoria#getTabela()
	 */
	@Override
	public String getTabela() {

		return tabela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.Auditoria#setTabela
	 * (java.lang.String)
	 */
	@Override
	public void setTabela(String tabela) {

		this.tabela = tabela;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #getOperacao()
	 */
	@Override
	public String getOperacao() {

		return operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #setOperacao(java.lang.String)
	 */
	@Override
	public void setOperacao(String operacao) {

		this.operacao = operacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #getTipoOperacao()
	 */
	@Override
	public String getTipoOperacao() {

		return tipoOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #setTipoOperacao(java.lang.String)
	 */
	@Override
	public void setTipoOperacao(String tipoOperacao) {

		this.tipoOperacao = tipoOperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #getUsuario()
	 */
	@Override
	public String getUsuario() {

		return usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #setUsuario(java.lang.String)
	 */
	@Override
	public void setUsuario(String usuario) {

		this.usuario = usuario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #getDataHora()
	 */
	@Override
	public Date getDataHora() {

		return dataHora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #setDataHora(java.util.Date)
	 */
	@Override
	public void setDataHora(Date dataHora) {

		this.dataHora = dataHora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #getIp()
	 */
	@Override
	public String getIp() {

		return ip;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.auditoria.impl.TrilhaAuditoria
	 * #setIp(java.lang.String)
	 */
	@Override
	public void setIp(String ip) {

		this.ip = ip;
	}

}
