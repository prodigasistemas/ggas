/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.batch;

import java.io.Serializable;
import java.util.Collection;

import br.com.ggas.cadastro.imovel.Imovel;

/**
 * Classe Gerar Dados.
 * 
 * 
 *
 */
public class GerarDadosVO implements Serializable {

	private static final long serialVersionUID = -2841726497639990301L;

	private Imovel imovel;

	private String grupoFaturamento;

	private String dataLeitura;

	private String rotaCiclo;

	private String detalhesImovel;

	private String matricula;

	private String totalPontos;

	private Collection<GerarDadosPontoConsumoVO> listaGerarDadosPontoConsumo;

	public String getTotalPontos() {

		return totalPontos;
	}

	public void setTotalPontos(String totalPontos) {

		this.totalPontos = totalPontos;
	}

	public Imovel getImovel() {

		return imovel;
	}

	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	public String getGrupoFaturamento() {

		return grupoFaturamento;
	}

	public void setGrupoFaturamento(String grupoFaturamento) {

		this.grupoFaturamento = grupoFaturamento;
	}

	public String getDataLeitura() {

		return dataLeitura;
	}

	public void setDataLeitura(String dataLeitura) {

		this.dataLeitura = dataLeitura;
	}

	public String getRotaCiclo() {

		return rotaCiclo;
	}

	public void setRotaCiclo(String rotaCiclo) {

		this.rotaCiclo = rotaCiclo;
	}

	public String getMatricula() {

		return matricula;
	}

	public void setMatricula(String matricula) {

		this.matricula = matricula;
	}

	public String getDetalhesImovel() {

		return detalhesImovel;
	}

	public void setDetalhesImovel(String detalhesImovel) {

		this.detalhesImovel = detalhesImovel;
	}

	public Collection<GerarDadosPontoConsumoVO> getListaGerarDadosPontoConsumo() {

		return listaGerarDadosPontoConsumo;
	}

	public void setListaGerarDadosPontoConsumo(Collection<GerarDadosPontoConsumoVO> listaGerarDadosPontoConsumo) {

		this.listaGerarDadosPontoConsumo = listaGerarDadosPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;

		if (matricula == null) {
			result = prime * result + 0;
		} else {
			result = prime * result + matricula.hashCode();
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(this == obj) {
			return true;
		}
		if(obj == null) {
			return false;
		}
		if(getClass() != obj.getClass()) {
			return false;
		}
		GerarDadosVO other = (GerarDadosVO) obj;
		if(matricula == null) {
			if(other.matricula != null) {
				return false;
			}
		} 
		else if(!matricula.equals(other.matricula)) {
			return false;
		}
		return true;
	}

}
