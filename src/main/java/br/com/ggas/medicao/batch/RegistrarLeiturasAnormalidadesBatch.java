/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.batch;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Calendar;
import java.util.Collection;
import java.util.Map;
import java.util.ResourceBundle;

import javax.mail.util.ByteArrayDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.InfraestruturaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.medicao.leitura.DadosRegistrarLeiturasAnormalidades;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.JavaMailUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.Util;
/**
 * Classe responsável por registrar as anormalidades de leitura.
 *
 */
@Component
public class RegistrarLeiturasAnormalidadesBatch extends AbstractMedicaoBatch {

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String PROCESSO = "processo";

	private static final String ARQUIVO_RELATORIO_LEITURAS_ANORMALIDADES = "RelatorioLeiturasAnormalidades";

	private static final String SALVANDO_RELATORIO_EM_PDF_NO_SERVIDOR = "\r\nSalvando relatório em pdf no servidor.";

	private static final String NAO_FORAM_ENCONTRADAS_INCONSISTENCIAS_NAS_LEITURAS = "\r\nNão foram encontrados registros inconsistentes.";

	private static final String ENVIANDO_O_RELATORIO_POR_E_MAIL_PARA = "\r\nEnviando o relatório por e-mail para: ";

	private static final String SEGUE_ANEXO_O_RELATORIO = "Segue anexo o relatório.";

	private static final String RELATORIO_LEITURAS_ANORMALIDADES = "Relatório de registros de Leituras";

	private static final String APPLICATION_PDF = "application/pdf";

	private static final String FORAM_ENCONTRADAS_ANORMALIDADES_EM_ALGUMAS_LEITURAS = 
					"\r\nForam encontrados registros inconsistentes de leituras";

	private static final String RELATORIO_REGISTRAR_LEITURAS_ANORMALIDADES_JASPER = "relatorioRegistrarLeiturasAnormalidades.jasper";

	private static final String CAMPO_DIRETORIO_DOS_RELATORIOS_GERADOS = "Diretório dos Relatórios Gerados";

	public static final String ATRIBUTO_USUARIO_LOGADO = "usuarioLogado";
	
	@Autowired
	private ControladorProcessoDocumento controladorProcessoDocumento;

	@Autowired
	private ControladorLeituraMovimento controladorLeituraMovimento;

	@Autowired
	@Qualifier(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA)
	private ControladorParametroSistema controladorParametrosSistema;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(final Map<String, Object> parametros) throws GGASException {
		StringBuilder logProcessamento = new StringBuilder();
		Processo processo = (Processo) parametros.get(PROCESSO);
		String[] chaveRotas = null;
		Rota rota = null;
		Collection<Rota> rotas = null;
		AtividadeSistema atividadeSistema = controladorProcesso.obterAtividadeSistema(processo.getOperacao());
		GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(Long.valueOf((String) parametros.get(ID_GRUPO_FATURAMENTO)));

		if (parametros.containsKey("idRota")) {
			rota = (Rota) controladorRota.obter(Long.parseLong((String) parametros.get("idRota")));
			String[] aRray = new String[1];
			aRray[0] = String.valueOf(rota.getChavePrimaria());
			chaveRotas = aRray;
		} else if (parametros.containsKey(ID_GRUPO_FATURAMENTO)) {
			rotas = controladorRota.obterRotasCronogramaFaturamento(grupoFaturamento);
			chaveRotas = arrayString(rotas);
		}
		Long[] leituras = new Long[0];
		if (parametros.containsKey("leituras")) {
			String leiturasStr = (String) parametros.get("leituras");
			String[] strings = leiturasStr.replace("[", "").replace("]", "").split(", ");
			leituras = new Long[strings.length];
			for (int i = 0; i < leituras.length; i++) {
				leituras[i] = Long.parseLong(strings[i]);
			}
		}

		logProcessamento.append(" RETORNO DE LEITURAS ");
		if (grupoFaturamento != null) {
			logProcessamento.append("\n\r Grupo: ").append(grupoFaturamento.getDescricao());
		}

		String criterio = "\n\r Rota: ";
		if (rota != null) {
			logProcessamento.append("\n\r Rota: ").append(rota.getNumeroRota());
		} else {
			for (Rota rotaLog : rotas) {
				logProcessamento.append(criterio).append(rotaLog.getNumeroRota());
				criterio = " , ";
			}
		}

		if (leituras.length > 0) {
			logProcessamento.append(" leituras: ").append((String) parametros.get("leituras"));
		}

		Collection<DadosRegistrarLeiturasAnormalidades> dados = null;

		if (chaveRotas != null) {
			dados = controladorLeituraMovimento.registrarLeituraAnormalidade(Util.arrayStringParaArrayLong(chaveRotas), atividadeSistema,
							Long.valueOf((String) parametros.get(ID_GRUPO_FATURAMENTO)), leituras, logProcessamento, 
							(Usuario) parametros.get(ATRIBUTO_USUARIO_LOGADO), Boolean.FALSE);
		}

		if (dados != null && !dados.isEmpty()) {
			logProcessamento.append("\r\n" + FORAM_ENCONTRADAS_ANORMALIDADES_EM_ALGUMAS_LEITURAS);

			byte[] bytes = RelatorioUtil.gerarRelatorioPDF(dados, null, RELATORIO_REGISTRAR_LEITURAS_ANORMALIDADES_JASPER);

			if (bytes != null) {
				logProcessamento.append("\r\n" + SALVANDO_RELATORIO_EM_PDF_NO_SERVIDOR);
				
				String pathDiretorio = (String) controladorParametrosSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_RELATORIO_PDF);
				
				validarDiretorioMalConfigurado(pathDiretorio);
				try {
					ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
					File diretorio = Util.getFile(pathDiretorio);			
					validarExistenciaDiretorio(diretorio);
					
					final String nomeDocumento = ARQUIVO_RELATORIO_LEITURAS_ANORMALIDADES + String.valueOf(processo.getChavePrimaria())
												+ Calendar.getInstance().getTime().getTime();
					String diretorioDocumento = pathDiretorio + nomeDocumento + Constantes.EXTENSAO_ARQUIVO_PDF;
					controladorProcesso.salvarRelatorio(processo, controladorProcessoDocumento, byteArrayInputStream, nomeDocumento,
							diretorioDocumento);
				} catch (Exception e) {
					throw new InfraestruturaException(e.getMessage(), e);
				}

				if (processo.getEmailResponsavel() != null && processo.getEmailResponsavel().length() > 0) {
					logProcessamento.append("\r\n").append(ENVIANDO_O_RELATORIO_POR_E_MAIL_PARA).append(processo.getEmailResponsavel());

					ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(bytes, APPLICATION_PDF);

					JavaMailUtil javaMailUtil = (JavaMailUtil) serviceLocator.getBeanPorID(JavaMailUtil.BEAN_ID_JAVA_MAIL_UTIL);
					javaMailUtil.enviar(atividadeSistema.getDescricaoEmailRemetente(), processo.getEmailResponsavel(),
									RELATORIO_LEITURAS_ANORMALIDADES, SEGUE_ANEXO_O_RELATORIO, ARQUIVO_RELATORIO_LEITURAS_ANORMALIDADES
													+ String.valueOf(processo.getChavePrimaria()) + Constantes.EXTENSAO_ARQUIVO_PDF,
									byteArrayDataSource);
				}
			}
		} else {
			logProcessamento.append("\r\n" + NAO_FORAM_ENCONTRADAS_INCONSISTENCIAS_NAS_LEITURAS);
		}

		return logProcessamento.toString();
	}

	private void validarDiretorioMalConfigurado(String pathDiretorio) throws NegocioException {
		String ultimoCaractere = pathDiretorio.substring(pathDiretorio.length()-1, pathDiretorio.length());
		
		if(!ultimoCaractere.equals(File.separator)) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_DIRETORIO_MAL_CONFIGURADO, CAMPO_DIRETORIO_DOS_RELATORIOS_GERADOS);
		}
	}

	private void validarExistenciaDiretorio(File diretorio) throws NegocioException {
		if (!diretorio.exists() && !diretorio.mkdirs()) {
			throw new NegocioException(ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, Constantes.LOCALE_PADRAO),
							Constantes.PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_CRIAR_DIRETORIO);
		}
	}

	/**
	 * Array string.
	 * 
	 * @param rota
	 *            the rota
	 * @return the string[]
	 */
	private static String[] arrayString(Collection<Rota> rota) {

		String[] novoArray = new String[rota.size()];
		int i = 0;
		for (Rota rotas : rota) {
			novoArray[i] = String.valueOf(rotas.getChavePrimaria());
			i++;
		}

		return novoArray;
	}

}
