/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.batch;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Classe abstrata de batch dos processos de medição.
 * Possui métodos em comum aos processos de medição (Gerar dados, registrar leitura e consistir)
 *
 * @author jose.victor@logiquesistemas.com.br
 */
public abstract class AbstractMedicaoBatch implements Batch {

	protected static final ServiceLocator serviceLocator = ServiceLocator.getInstancia();

	@Autowired
	@Qualifier(ControladorCronogramaAtividadeFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_ATIVIDADE_FATURAMENTO)
	protected ControladorCronogramaAtividadeFaturamento controladorCronAtvFaturamento;

	@Autowired
	protected ControladorRota controladorRota;

	@Autowired
	protected ControladorProcesso controladorProcesso;

	/**
	 * Atualiza um cronograma de atividade de faturamento informando que o processo foi executado no instante atual.
	 * Essa informação é atualizada no banco somente se existir um {@link CronogramaAtividadeFaturamento} que correspondam ao
	 * grupo de faturamento e a atividade de sistema informada
	 *
	 * @param logBuilder builder de log do processamento
	 * @param grupoFaturamento entidade do grupo de faturamento
	 * @param atividadeSistema atividade de sistema cujo cronoframa será atualizado
	 * @throws ConcorrenciaException caso a entidade atualizada de forma concorrente
	 * @throws NegocioException caso ocorra alguma falha genérica no processamento
	 */
	protected void atualizarCronogramaAtividadeFaturamentoSeExistir(StringBuilder logBuilder, GrupoFaturamento grupoFaturamento,
			AtividadeSistema atividadeSistema) throws ConcorrenciaException, NegocioException {

		Optional<CronogramaAtividadeFaturamento> optCronogramaAtividade = Optional.empty();
		if(grupoFaturamento != null && atividadeSistema != null) {
			optCronogramaAtividade = controladorCronAtvFaturamento
					.obterCrononogramaAtividadePorRotaAtividade(grupoFaturamento.getChavePrimaria(), atividadeSistema.getChavePrimaria(),
							grupoFaturamento.getAnoMesReferencia());
		}

		if (optCronogramaAtividade.isPresent()) {

			final CronogramaAtividadeFaturamento cronogramaAtividade = optCronogramaAtividade.get();
			cronogramaAtividade.setDataRealizacao(DataUtil.toDate(LocalDateTime.now()));
			controladorCronAtvFaturamento.atualizar(cronogramaAtividade);

		} else {
			logBuilder.append("Entidade de Cronograma de atividade de faturamento não encontrada\n");
		}

	}

}
