/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.batch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Batch de processamento em background do processo de consistir leitura
 */
@Component
public class ConsistirLeituraConsumoBatch extends AbstractMedicaoBatch {
	
	private static final String PROCESSO = "processo";

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String ID_ROTA = "idRota";

	@Autowired
	private ControladorHistoricoConsumo controladorHistoricoConsumo;
	
	@Autowired
	private ControladorHistoricoMedicao controladorHistoricoMedicao;
	
	StringBuilder logProcessamento; 

	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		logProcessamento  = new StringBuilder();
		// Consistir por grupo de fauramento
		String idGrupoFaturamento = (String) parametros.get(ID_GRUPO_FATURAMENTO);
		String idRota = (String) parametros.get(ID_ROTA);
		if (!StringUtils.isEmpty(idGrupoFaturamento)) {
			GrupoFaturamento grupoFaturamento = null;

			grupoFaturamento = controladorRota.obterGrupoFaturamento(Long.valueOf(idGrupoFaturamento));
			if (grupoFaturamento != null) {
				logProcessamento.append("\n\rConsistindo leitura e consumo para grupo: ")
				.append(grupoFaturamento.getDescricao())
				.append(" . Para a referência:")
				.append(grupoFaturamento.getAnoMesReferencia())
				.append("/")
				.append(grupoFaturamento.getNumeroCiclo());
				Rota rota = null;
				Collection<Rota> listRota = new ArrayList<Rota>();

				if (!StringUtils.isEmpty(idRota)) {

					rota = controladorRota.obterAtributosRota(Long.valueOf(idRota));
					logProcessamento.append(" .Rota: ").append(rota.getNumeroRota());
					listRota.add(rota);

				} else {
					listRota = controladorRota.listarAtributosRotasPorGrupoFaturamento(grupoFaturamento.getChavePrimaria());
				}

				// Obter os pontos de consumo com
				// situação ativa
				ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
								.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
				
				List<PontoConsumo> listaPontoConsumo = null;
				if(!"ELETROCORRETOR".equals(rota.getGrupoFaturamento().getTipoLeitura().getDescricao())) {
					listaPontoConsumo = controladorPontoConsumo.obterPontosConsumoLeituraMovumento(rota);
				} else {
					listaPontoConsumo = controladorPontoConsumo.obterPontosConsumoSuperMedicaoDiaria(rota);
				}

				this.removerPontosNaoRegistrados(listaPontoConsumo, rota);

				
				if(!"ELETRO-MENSAL".equals(rota.getNumeroRota())) {
					controladorHistoricoConsumo.removerPontosConsumoFaturados(listaPontoConsumo, rota);
				}

				Collection<PontoConsumo> pontosAtivos = controladorPontoConsumo
								.consultarAtributosDePontosDeConsumo(listaPontoConsumo);

				logProcessamento.append("\n\rQuantidade de Pontos de Consumo ativos: ").append(pontosAtivos.size());

				controladorHistoricoConsumo.consistirLeituraMedicao(pontosAtivos, null, null, logProcessamento, false, null);

				Long[] chavesPrimariasRotas = Util.collectionParaArrayChavesPrimarias(listRota);
				AtividadeSistema atividadeSistema = controladorProcesso
								.obterAtividadeSistema(AtividadeSistema.CODIGO_ATIVIDADE_CONSISTIR_LEITURA);
				Map<Rota, Collection<CronogramaRota>> mapaCronogramaRotaPorRota = controladorRota
								.obterMapaCronogramaRotaPorRotas(chavesPrimariasRotas, atividadeSistema);

				controladorRota.atualizarCronogramaPorRota(mapaCronogramaRotaPorRota, atividadeSistema, logProcessamento, null);
			}
		}
		
		try {
		//Relatório de Consistência de Consumo
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(ID_GRUPO_FATURAMENTO, idGrupoFaturamento);
			filtro.put(ID_ROTA, idRota);

			logProcessamento.append("\n\nCriando relatório para consistir leitura consumo");

			logProcessamento = controladorHistoricoConsumo.criarRelatorioConsistirLeituraConsumo(filtro, processo, logProcessamento);
		
		} catch (Exception e) {
			logProcessamento.append("\n\nOcorreu um erro ao criar o relatório de consistir leitura consumo: ");
			logProcessamento.append(e);
			
		}

		
		return logProcessamento.toString();
	}
	
	private void removerPontosNaoRegistrados(List<PontoConsumo> pontosConsumo, Rota rota) {
		List<PontoConsumo> pontosNaoRegistrados = controladorHistoricoMedicao.obterPontosConsumoNaoRegistrados(pontosConsumo, rota);
		
		if (!pontosNaoRegistrados.isEmpty()) {
			logProcessamento.append("\n\rQuantidade de Pontos de Consumo não registrados: ").append(pontosNaoRegistrados.size());
			for (PontoConsumo pontoNaoRegistrado: pontosNaoRegistrados) {
				logProcessamento.append("\n   ").append(pontoNaoRegistrado.getDescricao() + " ( " + pontoNaoRegistrado.getChavePrimaria() + " )");
				pontosConsumo.remove(pontoNaoRegistrado);
			}
		}
	}
	


}
