/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.batch;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Classe responsavel por calcular o consumo medido
 *
 */
@Component
public class CalcularConsumoMedioBatch implements Batch {

	private static final String ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String PROCESSO = "processo";

	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		StringBuilder logProcessamento = new StringBuilder();
		Processo processo = (Processo) parametros.get(PROCESSO);

		try {

			ControladorHistoricoConsumo controladorHistoricoConsumo = (ControladorHistoricoConsumo) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);

			ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

			// Consistir por grupo de fauramento
			String idGrupoFaturamento = (String) parametros.get(ID_GRUPO_FATURAMENTO);
			if(!StringUtils.isEmpty(idGrupoFaturamento)) {
				GrupoFaturamento grupoFaturamento = null;

				ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(
								ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
				grupoFaturamento = controladorRota.obterGrupoFaturamento(Long.valueOf(idGrupoFaturamento));

				if(grupoFaturamento != null) {
					logProcessamento.append("\n\rCalculando consumo médio para o grupo: ").append(grupoFaturamento.getDescricao());
					// Obter os pontos de consumo com
					// situação ativa
					ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
									.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

					Collection<PontoConsumo> pontosAtivos = controladorPontoConsumo.consultarPontoConsumo(grupoFaturamento, null);
					for (PontoConsumo pontoConsumo : pontosAtivos) {

						// o contrato ativo do ponto
						// de consumo
						ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

						// Obter consumo médio do
						// ponto de consumo
						BigDecimal consumoMedioPonto = BigDecimal.ZERO;
						BigDecimal consumoMedioApuradoPonto = BigDecimal.ZERO;
						Collection<HistoricoConsumo> historicosConsumo = controladorHistoricoConsumo.consultarHistoricosConsumo(
										pontoConsumo, null);
						for (HistoricoConsumo historicoConsumo : historicosConsumo) {

							Map<String, BigDecimal> consumoMedioPontoMap = controladorHistoricoConsumo.calcularConsumoMedio(pontoConsumo,
											contratoPontoConsumo, historicoConsumo.getAnoMesFaturamento(),
											historicoConsumo.getNumeroCiclo(), historicoConsumo.getDiasConsumo());
							if(consumoMedioPontoMap != null && consumoMedioPontoMap.get("consumoMedio") != null) {
								consumoMedioPonto = consumoMedioPontoMap.get("consumoMedio");
							}
							if(consumoMedioPontoMap != null && consumoMedioPontoMap.get("consumoMedioApurado") != null) {
								consumoMedioApuradoPonto = consumoMedioPontoMap.get("consumoMedioApurado");
							}

							historicoConsumo.setConsumoMedio(consumoMedioPonto);
							historicoConsumo.setConsumoApuradoMedio(consumoMedioApuradoPonto);
							controladorHistoricoConsumo.atualizar(historicoConsumo);
						}
					}
				}
			}

		} catch(HibernateException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new HibernateException(e);
		} catch(NegocioException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new NegocioException(e);
		} catch(GGASException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		} catch(Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		}

		return logProcessamento.toString();
	}
}
