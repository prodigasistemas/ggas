/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.batch;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.BooleanUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável por gerar dados a partir das leituras registradas
 *
 */
@Component
public class GerarDadosDeLeituraBatch extends AbstractMedicaoBatch {

	private static final String PROCESSO = "processo";

	@Autowired
	private ControladorLeituraMovimento controladorLeituraMovimento;

	@Qualifier(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO)
	@Autowired
	private ControladorCronogramaFaturamento cronogramaFaturamento;

	@Autowired
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	@Autowired
	private ControladorContrato controladorContrato;
	
	private StringBuilder logProcessamento;
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Batch#processar(java.
	 * util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		logProcessamento = new StringBuilder();
		
		Processo processo = (Processo) parametros.get(PROCESSO);

		Usuario usuario = processo.getUsuario();
		Date dataPrevistaLeitura = Calendar.getInstance().getTime();
		Map<String, Integer> sequencias = null;

		CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = null;
		GrupoFaturamento grupoFaturamento = null;
		Rota rota = null;

		AtividadeSistema atividadeSistema = controladorProcesso.obterAtividadeSistema(processo.getOperacao());

		// Obtendo os parâmetros do cronograma
		if (parametros.containsKey("idCronogramaAtividadeFaturamento")) {
			cronogramaAtividadeFaturamento = cronogramaFaturamento.obterCronogramaAtividadeFaturamento(Long.parseLong((String) parametros
							.get("idCronogramaAtividadeFaturamento")));
			dataPrevistaLeitura = cronogramaAtividadeFaturamento.getDataInicio();
		} else {
			escreverNovaLinha(logProcessamento, "Chave do cronograma de atividade do faturamento não contida nos parâmetros do processo: "
					+ parametros.containsKey("idCronogramaAtividadeFaturamento"));
		}

		grupoFaturamento = controladorRota.obterGrupoFaturamento(Long.valueOf((String) parametros.get("idGrupoFaturamento")));
		Collection<Rota> listRota = new HashSet<>();
		Collection<Rota> lRotaSemCronograma = new HashSet<>();

		if (parametros.containsKey("idRota")) {
			rota = (Rota) controladorRota.obter(Long.parseLong((String) parametros.get("idRota")),"pontosConsumo");
			listRota.add(rota);
		} else if (parametros.containsKey("idGrupoFaturamento")) {
			listRota = controladorRota.obterRotasCronogramaFaturamento(grupoFaturamento);
		}

		if (grupoFaturamento != null) {
			escreverNovaLinha(logProcessamento, "Gerar Dados para Leitura para grupo: "
					+ grupoFaturamento.getDescricao() + " referencia: "
					+ grupoFaturamento.getAnoMesReferencia() + " ciclo: "
					+ grupoFaturamento.getNumeroCiclo());
		} else {
			escreverNovaLinha(logProcessamento, "Gerar Dados para Leitura para grupo de faturamento NULO");
		}

		CronogramaRota cronogramaRota = null;
		Date dataAtual = Calendar.getInstance().getTime();
		StringBuilder complemento = new StringBuilder();
		Collection<PontoConsumo> pontosConsumo = new LinkedHashSet<PontoConsumo>();
		Collection<PontoConsumo> pontosConsumoAux = new LinkedHashSet<PontoConsumo>();
		Collection<PontoConsumo> pontosSemLeitura;
		controladorPontoConsumo.ordenarPontoConsumo();
		
		String parametroMedirFiscalizacao = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_MEDIR_ROTA_FISCALIZACAO);
		String parametroConsiderarDataUltimaLeitura = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CONSIDERAR_ULTIMA_LEITURA_GERAR_DADOS);
		
		
		Boolean permiteMedirRotaFiscalizacao = false;
		if ("1".equals(parametroMedirFiscalizacao)) {
			permiteMedirRotaFiscalizacao = true;
		}

		Map<Rota, CronogramaRota> cronogramaPorRota = new HashMap<>();

		long totalPontoConsumoAtivos = 0;
		for (Rota lRota : listRota) {

			cronogramaRota = controladorRota.obterCronogramaRotaAtualNaoFaturado(lRota);
			
			controladorRota.mapearCronogramaRota(cronogramaPorRota, cronogramaRota, lRota);
			
			if (cronogramaRota != null) {
				if (dataAtual.compareTo(cronogramaRota.getCronogramaAtividadeFaturamento().getDataInicio()) < 0) {
					lRotaSemCronograma.add(lRota);
				} else {
					pontosSemLeitura = controladorPontoConsumo.consultarPontoConsumoPorRotaFaturavel(lRota, Boolean.TRUE);
					totalPontoConsumoAtivos += pontosSemLeitura.size();
					if (permiteMedirRotaFiscalizacao) {
						pontosSemLeitura.addAll(controladorPontoConsumo.consultarPontoConsumoPorRotaFiscalizacao(lRota));
					}
					if (!pontosSemLeitura.isEmpty()) {
						pontosConsumo.addAll(pontosSemLeitura);
						pontosConsumoAux.addAll(pontosSemLeitura);

						complemento.append(lRota.getNumeroRota()).append(" (").append(pontosSemLeitura.size()).append(" pontos de consumo) \n");
					} else {
						complemento.append(lRota.getNumeroRota()).append(" (sem pontos de consumo para serem lidos) \n");
					}
				}
			} else {
				escreverNovaLinha(logProcessamento, lRota.getChavePrimaria() + " cronograma nulo");
			}
		}
		escreverNovaLinha(logProcessamento,"\rQuantidade de Rotas:" + listRota.size());
		escreverNovaLinha(logProcessamento,"Total de Pontos de consumo ativos nas rotas:"+totalPontoConsumoAtivos);
		escreverNovaLinha(logProcessamento,"Listando Rotas(s): \n" + complemento.toString());

		// removendo rotas sem cronograma iniciado.
		if (!lRotaSemCronograma.isEmpty()) {
			escreverNovaLinha(logProcessamento, "Removendo rotas sem cronograma, total de: "+lRotaSemCronograma.size());
			listRota.removeAll(lRotaSemCronograma);
		}


		long numeroPontoConsumoRemovido = pontosConsumo.size();
		if ("1".equals(parametroConsiderarDataUltimaLeitura)) {
			controladorLeituraMovimento.removerPontosConsumoQtdDiasLeitura(pontosConsumo, cronogramaPorRota);
		}
		numeroPontoConsumoRemovido -= pontosConsumo.size();
		// remover da lista de pontos de consumo da rota aqueles cuja quantidade de dias de consumo seja menor que
		// SEGM_NR_DIAS_FAT_LIGACAO_NOVA do seu segmento.
		if (numeroPontoConsumoRemovido != 0) {
			escreverNovaLinha(logProcessamento, "\n\rForam encontrados " + numeroPontoConsumoRemovido
					+ " pontos de consumo da rota aqueles cuja quantidade de dias "
					+ "de consumo seja menor que o número dias do faturamento referente ao seu segmento ");
			pontosConsumoAux.removeAll(pontosConsumo);
			for (PontoConsumo pc : pontosConsumoAux) {
				escreverNovaLinha(logProcessamento, "Ponto de consumo: " + pc.getDescricao());
			}
		}

		this.removerPontosSemContrato(pontosConsumo, rota);
		this.removerPontosInativos(pontosConsumo);
		
		// faz uma copia dos pontos de consumo para o metodo getControladorRota().atualizarCronogramaPorPontosConsumo
		Collection<PontoConsumo> pontosConsumoCopia = new HashSet<PontoConsumo>();
		pontosConsumoCopia.addAll(pontosConsumo);

		// Remover da lista os pontos de consumo com leitura movimento.
		Long[] chavesRestantes = Util.collectionParaArrayChavesPrimarias(pontosConsumo);
		if (chavesRestantes.length > 0) {
			Collection<PontoConsumo> pontosConsumoLeituraMovimento = controladorPontoConsumo.retirarPontoConsumoLeituraMovimento(
							chavesRestantes, grupoFaturamento);
			if (!pontosConsumoLeituraMovimento.isEmpty()) {
				numeroPontoConsumoRemovido = pontosConsumo.size();
				pontosConsumo.removeAll(pontosConsumoLeituraMovimento);
				numeroPontoConsumoRemovido -= pontosConsumo.size();
				if (numeroPontoConsumoRemovido != 0) {
					escreverNovaLinha(logProcessamento, "Foram encontrados " + numeroPontoConsumoRemovido
							+ " ponto(s) de consumo que já foram enviados para leitura. ");
					pontosConsumoAux.removeAll(pontosConsumo);
					for (PontoConsumo pc : pontosConsumoAux) {
						escreverNovaLinha(logProcessamento,"Ponto de consumo: " + pc.getDescricao());
					}
				}
			}
		}

		
		// É preciso retirar da lista os pontos de consumo que possuem corretor de vazão.
		if (!pontosConsumo.isEmpty()) {
			numeroPontoConsumoRemovido = pontosConsumo.size();
			retiraPontosConsumoComCorretor(logProcessamento, pontosConsumo);
			numeroPontoConsumoRemovido -= pontosConsumo.size();
			if (numeroPontoConsumoRemovido != 0) {
				escreverNovaLinha(logProcessamento, "Foram encontrados " + numeroPontoConsumoRemovido
						+ " pontos de consumo que possuem corretor de vazão ");
				pontosConsumoAux.removeAll(pontosConsumo);
				for (PontoConsumo pc : pontosConsumoAux) {
					escreverNovaLinha(logProcessamento,"Ponto de consumo: " + pc.getDescricao());
				}
			}
		}
		
		escreverNovaLinha(logProcessamento, "\rTotal de Pontos de consumo aptos que serão inseridos na " +
				"tabela leitura movimento: " + pontosConsumo.size());

		if (!pontosConsumo.isEmpty()) {
			sequencias = gerarSequenciaPontoConsumo(pontosConsumo);
		} else {
			escreverNovaLinha(logProcessamento, "Nenhuma sequência lida dos pontos de consumos");
		}

		// Obter a data de leitura
		if (parametros.containsKey("dataLeitura")) {
			dataPrevistaLeitura = Util.converterCampoStringParaData("CRONOGRAMA_ROTA_DATA_PREVISTA",
							(String) parametros.get("dataLeitura"), Constantes.FORMATO_DATA_BR);
		}

		// Validar se os pontos de consumos foram informados
		if (pontosConsumo == null || pontosConsumo.isEmpty()) {
			controladorRota.atualizarCronogramaPorRota(AtividadeSistema.CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA, listRota, logProcessamento);
		} else {
			
			// Validar se a sequencia de leitura foi definida
			if (sequencias == null || sequencias.isEmpty()) {
				throw new GGASException("\n\rA sequencia de leitura não foi definida!");
			}

			Collection<Rota> rotas = new ArrayList<Rota>();
			List<String> errosChecagemImovel = new ArrayList<>();

			Map<Long, List<PontoConsumo>> mapaPontosConsumoPorRota = new HashMap<>();
			for (PontoConsumo pontoConsumo : pontosConsumo) {
				if (!mapaPontosConsumoPorRota.containsKey(pontoConsumo.getRota().getChavePrimaria())) {
					mapaPontosConsumoPorRota.put(pontoConsumo.getRota().getChavePrimaria(), new ArrayList<>());

				}
				mapaPontosConsumoPorRota.get(pontoConsumo.getRota().getChavePrimaria()).add(pontoConsumo);
			}
			logProcessamento.append("\r");
			long numeroPontoConsumo;
			long numeroPontoConsumoFalha;
			for (Map.Entry<Long, List<PontoConsumo>> entry : mapaPontosConsumoPorRota.entrySet()) {

				numeroPontoConsumo = entry.getValue().size();
				numeroPontoConsumoFalha = 0;

				entry.getValue().stream().findFirst().ifPresent(pontoConsumo ->
						logProcessamento.append("\nRota: ").append(pontoConsumo.getRota().getNumeroRota()));

				for (PontoConsumo pontoConsumo : entry.getValue()) {

					if (pontoConsumo.getImovel().getNome() == null || pontoConsumo.getImovel().getNome().isEmpty()) {
						errosChecagemImovel.add("Ponto de Consumo: " + pontoConsumo.getDescricao() + " - Nome do imóvel NULO ou Vazio");
					}

					if (!rotas.contains(pontoConsumo.getRota())) {
						rotas.add(pontoConsumo.getRota());
						if (pontoConsumo.getInstalacaoMedidor() == null) {
							errosChecagemImovel.add("Ponto de Consumo: " + pontoConsumo.getDescricao() + " - SEM MEDIDOR ASSOCIADO");
						}
					}
					
					if (pontoConsumo.getQuadraFace() == null || pontoConsumo.getImovel().getQuadraFace() == null) {
						errosChecagemImovel.add("Ponto de Consumo: " + pontoConsumo.getDescricao() + " - sem quadra face cadastrada ");
					}

					escreverNovaLinha(logProcessamento,
							"Concluido checagem do ponto de consumo " + "(PK do ponto de consumo): " + pontoConsumo.getChavePrimaria() + " " + 
									pontoConsumo.getDescricao());

					if (!errosChecagemImovel.isEmpty()) {
						numeroPontoConsumoFalha++;
					}
				}

				logProcessamento.append("\nPontos de consumo gerados dados com sucesso: ").append(numeroPontoConsumo - numeroPontoConsumoFalha);
				logProcessamento.append("\nPontos de consumo com problemas: ").append(numeroPontoConsumoFalha);

				if (!errosChecagemImovel.isEmpty()) {
					logProcessamento.append("\n\rListando os pontos que apresentaram problemas:");
					for (String erro : errosChecagemImovel) {
						logProcessamento.append("\n").append(erro);
					}
				}

			}
			escreverNovaLinha(logProcessamento, "\rGerando os Movimentos de Leitura.");
			Collection<LeituraMovimento> leiturasMovimentos = this.controladorLeituraMovimento.criarLeiturasMovimento(pontosConsumo,
							dataPrevistaLeitura, usuario, sequencias, atividadeSistema, processo, logProcessamento);
			this.controladorLeituraMovimento.inserirLeiturasMovimento(leiturasMovimentos);
			controladorRota.atualizarCronogramaPorRota(AtividadeSistema.CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA, listRota, logProcessamento);
		}

		return logProcessamento.toString();

	}

	private void retiraPontosConsumoComCorretor(StringBuilder logProcessamento, Collection<PontoConsumo> pontosConsumo) {
		Collection<PontoConsumo> pontosConsumoCorretorVazao = this.obterPontosConsumoCorretorVazao(pontosConsumo);
		if (!pontosConsumoCorretorVazao.isEmpty()) {
			escreverNovaLinha(logProcessamento, "\n\rRemovendo ponto(s) de consumo que possuem corretor de vazao instalado:");
		}
		for (PontoConsumo pontoConsumo : pontosConsumoCorretorVazao) {
			
			logProcessamento.append("\n\r").append(pontoConsumo.getDescricao());
			pontosConsumo.removeAll(pontosConsumoCorretorVazao);
		}
	}

	/**
	 * Obter pontos consumo corretor vazao.
	 * 
	 * @param pontosConsumo
	 *            the pontos consumo
	 * @return the collection
	 */
	private Collection<PontoConsumo> obterPontosConsumoCorretorVazao(Collection<PontoConsumo> pontosConsumo) {

		Collection<PontoConsumo> listaPontoConsumoCorretorVazao = new ArrayList<PontoConsumo>();

		for (PontoConsumo pontoConsumo : pontosConsumo) {

			if (pontoConsumo.getInstalacaoMedidor() != null && pontoConsumo.getInstalacaoMedidor().getVazaoCorretor() != null) {
				listaPontoConsumoCorretorVazao.add(pontoConsumo);
			}
		}

		return listaPontoConsumoCorretorVazao;
	}

	/**
	 * Gerar sequencia ponto consumo.
	 * 
	 * @param pontosConsumo
	 *            the pontos consumo
	 * @return the map
	 */
	private Map<String, Integer> gerarSequenciaPontoConsumo(Collection<PontoConsumo> pontosConsumo) {
		
		Map<String, Integer> sequencias;
		sequencias = new LinkedHashMap<String, Integer>();

		for (PontoConsumo pontoConsumo : pontosConsumo) {
			sequencias.put(String.valueOf(pontoConsumo.getChavePrimaria()), pontoConsumo.getNumeroSequenciaLeitura());
		}

		return sequencias;
	}

	private static void escreverNovaLinha(StringBuilder logProcessamento, String texto) {
		logProcessamento.append("\n");
		logProcessamento.append(texto);
	}
	
	private void removerPontosSemContrato(Collection<PontoConsumo> pontosConsumo, Rota rota) {
	
		try {
			ParametroSistema parametroContrato = controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_VALIDA_CONTRATO);
			
			if (BooleanUtil.converterStringCharParaBooleano(parametroContrato.getValor())) {
				List<PontoConsumo> pontosComContratoAtivo = controladorContrato.obterPontosConsumoComContratoAtivo(rota);
				List<PontoConsumo> pontosSemContratoAtivo = new ArrayList<PontoConsumo>();
				
				for (PontoConsumo ponto : pontosConsumo) {
					
					if (!pontosComContratoAtivo.contains(ponto)) {
						pontosSemContratoAtivo.add(ponto);
					}
				}
				pontosConsumo.removeAll(pontosSemContratoAtivo);
				
				logProcessamento.append("\n\rQuantidade de Pontos de Consumo sem contrato ativo: " + pontosSemContratoAtivo.size()); 

				for(PontoConsumo pontoConsumo : pontosSemContratoAtivo) {
					logProcessamento.append("\n  ").append(pontoConsumo.getDescricao() + " ( " + pontoConsumo.getChavePrimaria() + " )");
				}
				
			} 
		} catch (NegocioException e) {
			logProcessamento.append("\n\nOcorreu um erro ao consultar o parametro para validação da existência do contrato ");
			logProcessamento.append(e);
		}
		
	}
	
	private void removerPontosInativos(Collection<PontoConsumo> pontosConsumo) {
		
		List<PontoConsumo> pontosInativos = new ArrayList<PontoConsumo>();
		
		for (PontoConsumo ponto : pontosConsumo) {
			
			if (!ponto.isHabilitado()) {
				pontosInativos.add(ponto);
			}
		}
		pontosConsumo.removeAll(pontosInativos);
		
		logProcessamento.append("\n\rQuantidade de Pontos de Consumo INATIVOS: " + pontosInativos.size()); 

		for(PontoConsumo pontoConsumo : pontosInativos) {
			logProcessamento.append("\n  ").append(pontoConsumo.getDescricao() + " ( " + pontoConsumo.getChavePrimaria() + " )");
		}
	}
	
}
