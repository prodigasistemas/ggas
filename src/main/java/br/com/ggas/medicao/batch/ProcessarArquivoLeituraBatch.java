/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.batch;

import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.OrigemLeituraMovimento;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.util.AsyncExecutor;
import br.com.ggas.util.CheckedSupplier;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.MensagemUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Classe responsável por processar os arquivos de leitura.
 * 
 */
@Component
public class ProcessarArquivoLeituraBatch implements Batch {
	
	private static final String DATA_LEITURA = "dataLeitura";

	private static final String CODIGO_ANORMALIDADE_LEITURA = "codigoAnormalidadeLeitura";

	private static final String VALOR_LEITURA = "valorLeitura";

	private static final Logger LOG = Logger.getLogger(ProcessarArquivoLeituraBatch.class);

	public static final String NAO_INFORMADO = "não informado";

	public static final String PARAMETRO_ARQUIVO = "arquivo";

	private static final String PROCESSO = "processo";

	private final ControladorLeituraMovimento controladorLeituraMovimento;

	/**
	 * Instantiates a new processar arquivo leitura batch.
	 */
	public ProcessarArquivoLeituraBatch() {

		controladorLeituraMovimento = (ControladorLeituraMovimento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.batch.Batch#processar(java.
	 * util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		StringBuilder logProcessamento = new StringBuilder();
		Processo processo = (Processo) parametros.get(PROCESSO);

		try {

			String caminhoArquivo = (String) parametros.get(PARAMETRO_ARQUIVO);

			if(StringUtils.isEmpty(caminhoArquivo)) {
				throw new GGASException("O arquivo não foi informado.");
			}

			logProcessamento.append("\r\n Processando o arquivo: ").append(caminhoArquivo).append(" \r\n\r\n");
			File arquivo = null;

			arquivo = Util.getFile(caminhoArquivo);
			if(!arquivo.exists() || !arquivo.isFile()) {
				throw new GGASException("O arquivo não foi encontrado.");
			}
			
			SAXBuilder saxBuilder = new SAXBuilder();
			Document document = saxBuilder.build(arquivo);
			Element element = document.getRootElement();
			
			List<Element> elementos = element.getChildren("leitura");

			SituacaoLeituraMovimento emLeitura = (SituacaoLeituraMovimento) ServiceLocator.getInstancia().getBeanPorID(
							SituacaoLeituraMovimento.BEAN_ID_SITUACAO_LEITURA_MOVIMENTO);
			emLeitura.setChavePrimaria(SituacaoLeituraMovimento.EM_LEITURA);

			SituacaoLeituraMovimento leituraRetornada = (SituacaoLeituraMovimento) ServiceLocator.getInstancia().getBeanPorID(
							SituacaoLeituraMovimento.BEAN_ID_SITUACAO_LEITURA_MOVIMENTO);
			leituraRetornada.setChavePrimaria(SituacaoLeituraMovimento.LEITURA_RETORNADA);

			LeituraMovimento filtro = (LeituraMovimento) controladorLeituraMovimento.criar();
			filtro.setSituacaoLeitura(emLeitura);

			StringBuilder logLeiturasFalhas = new StringBuilder();
			AsyncExecutor asyncExecutor = ServiceLocator.getInstancia().getBean(AsyncExecutor.class);
			List<CheckedSupplier<Boolean, GGASException>> functionsList = new ArrayList<>();

			int totalLeituras = 0;
			int sucesso = 0;
			long numeroSucesso = 0;
			// Consulta e atualização das leituras
			// em aberto.
			for (Element elemento : elementos) {
				totalLeituras++;
				functionsList.add(() -> ServiceLocator.getInstancia().getControladorLeituraMovimento()
						.atualizarLeituraMovimento(elemento, logLeiturasFalhas, leituraRetornada, emLeitura));
			}
			
			List<Boolean> listaSucesso = AsyncExecutor.execute(asyncExecutor, functionsList);
			
			numeroSucesso = listaSucesso.stream().filter(p -> p == Boolean.TRUE).count();
		

			logProcessamento.append("Total de registros: ").append(totalLeituras).append(" Sucesso: ").append(numeroSucesso).append(" Erros: ")
					.append((totalLeituras - numeroSucesso)).append(" \r\n\r\n");
			
			if (logLeiturasFalhas.length() > 0) {
				logProcessamento.append(logLeiturasFalhas.toString());
			}

		} catch(HibernateException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new HibernateException(e);
		} catch(NegocioException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new NegocioException(e);
		} catch(GGASException e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		} catch(Exception e) {
			processo.setLogErro(MensagemUtil.gerarMensagemErro(e).getBytes());
			throw new GGASException(e);
		}

		return logProcessamento.toString();
	}
	
}
