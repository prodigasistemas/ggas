/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe HistoricoConsumoImpl representa uma HistoricoConsumoImpl no sistema.
 *
 * @since 01/10/2008
 *
 */

package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.consumo.TipoConsumo;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável por representar o histórico de consumo.
 *
 */
public class HistoricoConsumoImpl extends EntidadeNegocioImpl implements HistoricoConsumo {

	private static final int CONSTANTE_NOVENTA_NOVE = 99;

	private static final int PERCENTUAL = 100;

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7284788787410467214L;

	private HistoricoConsumo consumoCondominio;

	private Integer anoMesFaturamento;

	private Integer sequencia;

	private Integer numeroCiclo;

	private BigDecimal consumo;

	private BigDecimal consumoMedido;

	private BigDecimal consumoApurado;

	private HistoricoMedicao historicoAnterior;

	private HistoricoMedicao historicoAtual;

	private Boolean indicadorImovelCondominio;

	private BigDecimal consumoMedio;

	private BigDecimal consumoMinimo;

	private BigDecimal consumoImovelVinculado;

	private BigDecimal consumoCredito;

	private BigDecimal consumoCreditoMedia;

	private Boolean indicadorFaturamento;

	private PontoConsumo pontoConsumo;

	private TipoConsumo tipoConsumo;

	private AnormalidadeConsumo anormalidadeConsumo;

	private Integer diasConsumo;

	private BigDecimal consumoApuradoMedio;

	private BigDecimal medidaPCS;

	private BigDecimal medidaZ;

	private BigDecimal fatorPCS;

	private BigDecimal fatorPTZ;

	private BigDecimal fatorCorrecao;

	private IntervaloPCS intervaloPCS;

	private EntidadeConteudo localAmostragemPCS;

	private boolean indicadorConsumoCiclo;

	private HistoricoConsumo historicoConsumoSintetizador;

	private BigDecimal variacaoConsumo;

	private Boolean indicadorDrawback;
	
	private Medidor medidor;
	
	private String composicaoVirtual;
	
	private Boolean indicadorMedicaoIndependenteAtual;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getFatorCorrecao()
	 */
	@Override
	public BigDecimal getFatorCorrecao() {

		return fatorCorrecao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #setFatorCorrecao(java.math.BigDecimal)
	 */
	@Override
	public void setFatorCorrecao(BigDecimal fatorCorrecao) {

		this.fatorCorrecao = fatorCorrecao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getConsumoCondominio()
	 */
	@Override
	public HistoricoConsumo getConsumoCondominio() {

		return consumoCondominio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #setConsumoCondominio(br.com.ggas.medicao.
	 * consumo.HistoricoConsumo)
	 */
	@Override
	public void setConsumoCondominio(HistoricoConsumo consumoCondominio) {

		this.consumoCondominio = consumoCondominio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getAnoMesFaturamento()
	 */
	@Override
	public Integer getAnoMesFaturamento() {

		return anoMesFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getAnoMesFaturamentoFormatado()
	 */
	@Override
	public String getAnoMesFaturamentoFormatado() {

		return Util.formatarAnoMes(anoMesFaturamento);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setAnoMesFaturamento(java.lang.Integer)
	 */
	@Override
	public void setAnoMesFaturamento(Integer anoMesFaturamento) {

		this.anoMesFaturamento = anoMesFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getSequencia()
	 */
	@Override
	public Integer getSequencia() {

		return sequencia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setSequencia(java.lang.Integer)
	 */
	@Override
	public void setSequencia(Integer sequencia) {

		this.sequencia = sequencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getNumeroCiclo()
	 */
	@Override
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #setNumeroCiclo(java.lang.Integer)
	 */
	@Override
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getConsumo()
	 */
	@Override
	public BigDecimal getConsumo() {

		return consumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setConsumo(java.math.BigDecimal)
	 */
	@Override
	public void setConsumo(BigDecimal consumo) {

		this.consumo = consumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getConsumoMedido()
	 */
	@Override
	public BigDecimal getConsumoMedido() {

		return consumoMedido;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setConsumoMedido(java.math.BigDecimal)
	 */
	@Override
	public void setConsumoMedido(BigDecimal consumoMedido) {

		this.consumoMedido = consumoMedido;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getConsumoApurado()
	 */
	@Override
	public BigDecimal getConsumoApurado() {

		return consumoApurado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setConsumoApurado(java.math.BigDecimal)
	 */
	@Override
	public void setConsumoApurado(BigDecimal consumoApurado) {

		this.consumoApurado = consumoApurado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getHistoricoAnterior()
	 */
	@Override
	public HistoricoMedicao getHistoricoAnterior() {

		return historicoAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setHistoricoAnterior(br.com
	 * .ggas.medicao.leitura.HistoricoMedicao)
	 */
	@Override
	public void setHistoricoAnterior(HistoricoMedicao historicoAnterior) {

		this.historicoAnterior = historicoAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getHistoricoAtual()
	 */
	@Override
	public HistoricoMedicao getHistoricoAtual() {

		return historicoAtual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setHistoricoAtual(br.com.ggas
	 * .medicao.leitura.HistoricoMedicao)
	 */
	@Override
	public void setHistoricoAtual(HistoricoMedicao historicoAtual) {

		this.historicoAtual = historicoAtual;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #getIndicadorImovelCondominio()
	 */
	@Override
	public Boolean getIndicadorImovelCondominio() {

		return indicadorImovelCondominio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setIndicadorImovelCondominio
	 * (java.lang.Boolean)
	 */
	@Override
	public void setIndicadorImovelCondominio(Boolean indicadorImovelCondominio) {

		this.indicadorImovelCondominio = indicadorImovelCondominio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getConsumoMedio()
	 */
	@Override
	public BigDecimal getConsumoMedio() {

		return consumoMedio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setConsumoMedio(java.math.BigDecimal)
	 */
	@Override
	public void setConsumoMedio(BigDecimal consumoMedio) {

		this.consumoMedio = consumoMedio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getConsumoMinimo()
	 */
	@Override
	public BigDecimal getConsumoMinimo() {

		return consumoMinimo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setConsumoMinimo(java.math.BigDecimal)
	 */
	@Override
	public void setConsumoMinimo(BigDecimal consumoMinimo) {

		this.consumoMinimo = consumoMinimo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #getConsumoImovelVinculado()
	 */
	@Override
	public BigDecimal getConsumoImovelVinculado() {

		return consumoImovelVinculado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setConsumoImovelVinculado(java
	 * .math.BigDecimal)
	 */
	@Override
	public void setConsumoImovelVinculado(BigDecimal consumoImovelVinculado) {

		this.consumoImovelVinculado = consumoImovelVinculado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getConsumoCredito()
	 */
	@Override
	public BigDecimal getConsumoCredito() {

		return consumoCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setConsumoCredito(java.math.BigDecimal)
	 */
	@Override
	public void setConsumoCredito(BigDecimal consumoCredito) {

		this.consumoCredito = consumoCredito;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getIndicadorFaturamento()
	 */
	@Override
	public Boolean getIndicadorFaturamento() {

		return indicadorFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setIndicadorFaturamento(java.lang.Boolean)
	 */
	@Override
	public void setIndicadorFaturamento(Boolean indicadorFaturamento) {

		this.indicadorFaturamento = indicadorFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setPontoConsumo(br.com.ggas
	 * .cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getTipoConsumo()
	 */
	@Override
	public TipoConsumo getTipoConsumo() {

		return tipoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setTipoConsumo(br.com.ggas.
	 * medicao.consumo.TipoConsumo)
	 */
	@Override
	public void setTipoConsumo(TipoConsumo tipoConsumo) {

		this.tipoConsumo = tipoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo#getAnormalidadeConsumo()
	 */
	@Override
	public AnormalidadeConsumo getAnormalidadeConsumo() {

		return anormalidadeConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * HistoricoConsumo
	 * #setAnormalidadeConsumo(br.com
	 * .ggas.medicao.consumo.AnormalidadeConsumo)
	 */
	@Override
	public void setAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo) {

		this.anormalidadeConsumo = anormalidadeConsumo;
	}

	@Override
	public BigDecimal getConsumoApuradoPercentualConsumoMedio() {

		BigDecimal percentual = BigDecimal.ZERO;
		if(getConsumoApurado() != null && getConsumoApuradoMedio() != null && getConsumoApuradoMedio().compareTo(BigDecimal.ZERO) > 0) {
			percentual = getConsumoApurado().subtract(getConsumoApuradoMedio());
			percentual = percentual.multiply(new BigDecimal(PERCENTUAL));
			percentual = percentual.divide(getConsumoApuradoMedio(), RoundingMode.HALF_EVEN);
		}
		return percentual;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getDiasConsumo()
	 */
	@Override
	public Integer getDiasConsumo() {

		return diasConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #setDiasConsumo(java.lang.Integer)
	 */
	@Override
	public void setDiasConsumo(Integer diasConsumo) {

		this.diasConsumo = diasConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();

		if(fatorPTZ != null) {
			BigInteger inteiro = fatorPTZ.toBigInteger();
			if(Integer.parseInt(inteiro.toString()) > CONSTANTE_NOVENTA_NOVE) {
				erros.put(Constantes.ERRO_NEGOCIO_HISTORICO_CONSUMO_PTZ, "");
				//id ponto consumo será usado para montar o log
				erros.put("idPontoConsumo", this.pontoConsumo.getChavePrimaria());

			}
		}
		return erros;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getConsumoApuradoMedio()
	 */
	@Override
	public BigDecimal getConsumoApuradoMedio() {

		return consumoApuradoMedio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #
	 * setConsumoApuradoMedio(java.math.BigDecimal
	 * )
	 */
	@Override
	public void setConsumoApuradoMedio(BigDecimal consumoApuradoMedio) {

		this.consumoApuradoMedio = consumoApuradoMedio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getMedidaPCS()
	 */
	@Override
	public BigDecimal getMedidaPCS() {

		return medidaPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #setMedidaPCS(java.math.BigDecimal)
	 */
	@Override
	public void setMedidaPCS(BigDecimal medidaPCS) {

		this.medidaPCS = medidaPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getMedidaZ()
	 */
	@Override
	public BigDecimal getMedidaZ() {

		return medidaZ;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #setMedidaZ(java.math.BigDecimal)
	 */
	@Override
	public void setMedidaZ(BigDecimal medidaZ) {

		this.medidaZ = medidaZ;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getFatorPCS()
	 */
	@Override
	public BigDecimal getFatorPCS() {

		return fatorPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #setFatorPCS(java.math.BigDecimal)
	 */
	@Override
	public void setFatorPCS(BigDecimal fatorPCS) {

		this.fatorPCS = fatorPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getFatorPTZ()
	 */
	@Override
	public BigDecimal getFatorPTZ() {

		return fatorPTZ;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #setFatorPTZ(java.math.BigDecimal)
	 */
	@Override
	public void setFatorPTZ(BigDecimal fatorPTZ) {

		this.fatorPTZ = fatorPTZ;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getIntervaloPCS()
	 */
	@Override
	public IntervaloPCS getIntervaloPCS() {

		return intervaloPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #
	 * setIntervaloPCS(br.com.ggas.medicao.consumo
	 * .IntervaloPCS)
	 */
	@Override
	public void setIntervaloPCS(IntervaloPCS intervaloPCS) {

		this.intervaloPCS = intervaloPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #getLocalAmostragemPCS()
	 */
	@Override
	public EntidadeConteudo getLocalAmostragemPCS() {

		return localAmostragemPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #
	 * setLocalAmostragemPCS(br.com.ggas.contrato.
	 * EntidadeConteudo)
	 */
	@Override
	public void setLocalAmostragemPCS(EntidadeConteudo localAmostragemPCS) {

		this.localAmostragemPCS = localAmostragemPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #isIndicadorConsumoCiclo()
	 */
	@Override
	public boolean isIndicadorConsumoCiclo() {

		return indicadorConsumoCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.HistoricoConsumo
	 * #setIndicadorConsumoCiclo(boolean)
	 */
	@Override
	public void setIndicadorConsumoCiclo(boolean indicadorConsumoCiclo) {

		this.indicadorConsumoCiclo = indicadorConsumoCiclo;
	}

	@Override
	public BigDecimal getConsumoDiario() {

		BigDecimal consumoDiario = null;

		if(this.getConsumoApurado() != null && this.getDiasConsumo() != null && this.getDiasConsumo() > 0) {
			consumoDiario = getConsumoApurado().divide(new BigDecimal(this.getDiasConsumo()), 4, RoundingMode.HALF_UP);
		}

		return consumoDiario;
	}

	@Override
	public HistoricoConsumo getHistoricoConsumoSintetizador() {

		return historicoConsumoSintetizador;
	}

	@Override
	public void setHistoricoConsumoSintetizador(HistoricoConsumo historicoConsumoSintetizador) {

		this.historicoConsumoSintetizador = historicoConsumoSintetizador;
	}

	@Override
	public BigDecimal getConsumoCreditoMedia() {

		return consumoCreditoMedia;
	}

	@Override
	public void setConsumoCreditoMedia(BigDecimal consumoCreditoMedia) {

		this.consumoCreditoMedia = consumoCreditoMedia;
	}

	@Override
	public BigDecimal getVariacaoConsumo() {

		return variacaoConsumo;
	}

	@Override
	public void setVariacaoConsumo(BigDecimal variacaoConsumo) {

		this.variacaoConsumo = variacaoConsumo;
	}

	@Override
	public Boolean getIndicadorDrawback() {

		return indicadorDrawback;
	}

	@Override
	public void setIndicadorDrawback(Boolean indicadorDrawback) {

		this.indicadorDrawback = indicadorDrawback;
	}
	@Override
	public Medidor getMedidor() {

		return medidor;
	}
	@Override
	public void setMedidor(Medidor medidor) {

		this.medidor = medidor;
	}
	@Override
	public String getComposicaoVirtual() {

		return composicaoVirtual;
	}
	@Override
	public void setComposicaoVirtual(String composicaoVirtual) {

		this.composicaoVirtual = composicaoVirtual;
	}
	@Override
	public Boolean getIndicadorMedicaoIndependenteAtual() {

		return indicadorMedicaoIndependenteAtual;
	}
	@Override
	public void setIndicadorMedicaoIndependenteAtual(Boolean indicadorMedicaoIndependenteAtual) {

		this.indicadorMedicaoIndependenteAtual = indicadorMedicaoIndependenteAtual;
	}

	@Override
	public int compareTo(HistoricoConsumo o) {
		return historicoAtual.getDataLeituraInformada().compareTo(o.getHistoricoAtual().getDataLeituraInformada());
	}
	
	@Override
	public Boolean possuiAnormalidadeConsumo() {
		return anormalidadeConsumo != null;
	}
	
	@Override
	public Boolean possuiAnormalidadeConsumoImpeditiva() {
		return possuiAnormalidadeConsumo() && anormalidadeConsumo.getBloquearFaturamento();
	}
	
	@Override
	/**
	 * @return Bigdecimal - Calcula o consumo medio apurado
	 */
	public BigDecimal calcularConsumoApuradoMedio(String parametroArredondamento) {
		return consumoApurado.divide(new BigDecimal(diasConsumo), 8, Integer.parseInt(parametroArredondamento));
	}
	
}
