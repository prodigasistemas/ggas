/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe CityGateMedicaoImpl representa uma CityGateMedicao no sistema.
 *
 * @since 05/11/2009
 * 
 */

package br.com.ggas.medicao.consumo.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.consumo.CityGateMedicao;
import br.com.ggas.util.Constantes;

class CityGateMedicaoImpl extends EntidadeNegocioImpl implements CityGateMedicao {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = -5171964629887226061L;

	private CityGate cityGate;

	private Date dataMedicao;

	private Integer medidaPCSFixo;

	private Integer medidaPCSSupridora;

	private Integer medidaPCSDistribuidora;

	private Integer medidaVolumeSupridora;

	private Integer medidaVolumeDistribuidora;

	private boolean indicadorAlteracao;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * CityGateMedicao#getCityGate()
	 */
	@Override
	public CityGate getCityGate() {

		return cityGate;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * CityGateMedicao
	 * #setCityGate(br.com.ggas.cadastro
	 * .operacional.CityGate)
	 */
	@Override
	public void setCityGate(CityGate cityGate) {

		this.cityGate = cityGate;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * CityGateMedicao#getDataMedicao()
	 */
	@Override
	public Date getDataMedicao() {

		return dataMedicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * CityGateMedicao
	 * #setDataMedicao(java.util.Date)
	 */
	@Override
	public void setDataMedicao(Date dataMedicao) {

		this.dataMedicao = dataMedicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * CityGateMedicao#getMedidaPCSFixo()
	 */
	@Override
	public Integer getMedidaPCSFixo() {

		return medidaPCSFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * CityGateMedicao
	 * #setMedidaPCSFixo(java.math.Integer)
	 */
	@Override
	public void setMedidaPCSFixo(Integer medidaPCSFixo) {

		this.medidaPCSFixo = medidaPCSFixo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * CityGateMedicao#getMedidaPCSSupridora()
	 */
	@Override
	public Integer getMedidaPCSSupridora() {

		return medidaPCSSupridora;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * CityGateMedicao
	 * #setMedidaPCSSupridora(java.math.Integer)
	 */
	@Override
	public void setMedidaPCSSupridora(Integer medidaPCSSupridora) {

		this.medidaPCSSupridora = medidaPCSSupridora;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * CityGateMedicao#getMedidaPCSDistribuidora()
	 */
	@Override
	public Integer getMedidaPCSDistribuidora() {

		return medidaPCSDistribuidora;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * CityGateMedicao
	 * #setMedidaPCSDistribuidora(java
	 * .math.Integer)
	 */
	@Override
	public void setMedidaPCSDistribuidora(Integer medidaPCSDistribuidora) {

		this.medidaPCSDistribuidora = medidaPCSDistribuidora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.CityGateMedicao
	 * #getMedidaVolumeSupridora()
	 */
	@Override
	public Integer getMedidaVolumeSupridora() {

		return medidaVolumeSupridora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.CityGateMedicao
	 * #
	 * setMedidaVolumeSupridora(java.lang.Integer)
	 */
	@Override
	public void setMedidaVolumeSupridora(Integer medidaVolumeSupridora) {

		this.medidaVolumeSupridora = medidaVolumeSupridora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.CityGateMedicao
	 * #getMedidaVolumeDistribuidora()
	 */
	@Override
	public Integer getMedidaVolumeDistribuidora() {

		return medidaVolumeDistribuidora;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.CityGateMedicao
	 * #
	 * setMedidaVolumeDistribuidora(java.lang.Integer
	 * )
	 */
	@Override
	public void setMedidaVolumeDistribuidora(Integer medidaVolumeDistribuidora) {

		this.medidaVolumeDistribuidora = medidaVolumeDistribuidora;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(cityGate == null) {
			stringBuilder.append(CITY_GATE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.dataMedicao == null) {
			stringBuilder.append(DATA_MEDICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.CityGateMedicao
	 * #setIndicadorAlteracao(java.lang.boolean)
	 */
	@Override
	public void setIndicadorAlteracao(boolean indicadorAlteracao) {

		this.indicadorAlteracao = indicadorAlteracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.CityGateMedicao
	 * #isIndicadorRecalcular()
	 */
	@Override
	public boolean isIndicadorAlteracao() {

		return indicadorAlteracao;
	}
}
