/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.consumo.impl;

import java.io.Serializable;

/**
 * Classe SubReport Analise Consumo.
 *
 *
 */
public class SubReportAnaliseConsumoVO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 4123557037476898732L;

	private String referencia;

	private String leituraMedidaInformada;

	private String fatorCorrecao;

	private String consumoAtualApurado;

	private String consumoMedio;

	private String anormalidadeLeituraIdentificada;

	private String anormalidadeConsumoIdentificada;

	private String medidor;

	private String leituraInformada;

	private String fatorPTZ;

	private String fatorPCS;

	/**
	 * @return the referencia
	 */
	public String getReferencia() {

		return referencia;
	}

	/**
	 * @param referencia
	 *            the referencia to set
	 */
	public void setReferencia(String referencia) {

		this.referencia = referencia;
	}

	/**
	 * @return the leituraMedidaInformada
	 */
	public String getLeituraMedidaInformada() {

		return leituraMedidaInformada;
	}

	/**
	 * @param leituraMedidaInformada
	 *            the leituraMedidaInformada to
	 *            set
	 */
	public void setLeituraMedidaInformada(String leituraMedidaInformada) {

		this.leituraMedidaInformada = leituraMedidaInformada;
	}

	/**
	 * @return the fatorCorrecao
	 */
	public String getFatorCorrecao() {

		return fatorCorrecao;
	}

	/**
	 * @param fatorCorrecao
	 *            the fatorCorrecao to set
	 */
	public void setFatorCorrecao(String fatorCorrecao) {

		this.fatorCorrecao = fatorCorrecao;
	}

	/**
	 * @return the consumoAtualApurado
	 */
	public String getConsumoAtualApurado() {

		return consumoAtualApurado;
	}

	/**
	 * @param consumoAtualApurado
	 *            the consumoAtualApurado to set
	 */
	public void setConsumoAtualApurado(String consumoAtualApurado) {

		this.consumoAtualApurado = consumoAtualApurado;
	}

	/**
	 * @return the consumoMedio
	 */
	public String getConsumoMedio() {

		return consumoMedio;
	}

	/**
	 * @param consumoMedio
	 *            the consumoMedio to set
	 */
	public void setConsumoMedio(String consumoMedio) {

		this.consumoMedio = consumoMedio;
	}

	/**
	 * @return the anormalidadeLeituraIdentificada
	 */
	public String getAnormalidadeLeituraIdentificada() {

		return anormalidadeLeituraIdentificada;
	}

	/**
	 * @param anormalidadeLeituraIdentificada
	 *            the
	 *            anormalidadeLeituraIdentificada
	 *            to set
	 */
	public void setAnormalidadeLeituraIdentificada(String anormalidadeLeituraIdentificada) {

		this.anormalidadeLeituraIdentificada = anormalidadeLeituraIdentificada;
	}

	/**
	 * @return the anormalidadeConsumoIdentificada
	 */
	public String getAnormalidadeConsumoIdentificada() {

		return anormalidadeConsumoIdentificada;
	}

	/**
	 * @param anormalidadeConsumoIdentificada
	 *            the
	 *            anormalidadeConsumoIdentificada
	 *            to set
	 */
	public void setAnormalidadeConsumoIdentificada(String anormalidadeConsumoIdentificada) {

		this.anormalidadeConsumoIdentificada = anormalidadeConsumoIdentificada;
	}

	/**
	 * @return the medidor
	 */
	public String getMedidor() {

		return medidor;
	}

	/**
	 * @param medidor
	 *            the medidor to set
	 */
	public void setMedidor(String medidor) {

		this.medidor = medidor;
	}

	/**
	 * @return the leituraInformada
	 */
	public String getLeituraInformada() {

		return leituraInformada;
	}

	/**
	 * @param leituraInformada
	 *            the leituraInformada to set
	 */
	public void setLeituraInformada(String leituraInformada) {

		this.leituraInformada = leituraInformada;
	}

	/**
	 * @return the fatorPTZ
	 */
	public String getFatorPTZ() {

		return fatorPTZ;
	}

	/**
	 * @param fatorPTZ
	 *            the fatorPTZ to set
	 */
	public void setFatorPTZ(String fatorPTZ) {

		this.fatorPTZ = fatorPTZ;
	}

	/**
	 * @return the fatorPCS
	 */
	public String getFatorPCS() {

		return fatorPCS;
	}

	/**
	 * @param fatorPCS
	 *            the fatorPCS to set
	 */
	public void setFatorPCS(String fatorPCS) {

		this.fatorPCS = fatorPCS;
	}
}
