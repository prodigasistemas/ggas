/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
/**
 * Interface responsável pela assinatura dos métodos relacionados
 * a ConsumoDistribuidoData
 *
 */
public interface ConsumoDistribuidoData extends Serializable {

	String BEAN_ID_CONSUMO_DISTRIBUIDO_DATA = "consumoDistribuidoData";

	/**
	 * @return the data
	 */
	Date getData();

	/**
	 * @param data
	 *            the data to set
	 */
	void setData(Date data);

	/**
	 * @return the volumeQDC
	 */
	BigDecimal getVolumeQDC();

	/**
	 * @param volumeQDC
	 *            the volumeQDC to set
	 */
	void setVolumeQDC(BigDecimal volumeQDC);

	/**
	 * @return the volumeQDP
	 */
	BigDecimal getVolumeQDP();

	/**
	 * @param volumeQDP
	 *            the volumeQDP to set
	 */
	void setVolumeQDP(BigDecimal volumeQDP);

	/**
	 * @return the volumeQF
	 */
	BigDecimal getVolumeQF();

	/**
	 * @param volumeQF
	 *            the volumeQF to set
	 */
	void setVolumeQF(BigDecimal volumeQF);

	/**
	 * @return the volumeQDRMaior
	 */
	BigDecimal getVolumeQDRMaior();

	/**
	 * @param volumeQDRMaior
	 *            the volumeQDRMaior to set
	 */
	void setVolumeQDRMaior(BigDecimal volumeQDRMaior);

	/**
	 * @return the volumeQNRpp
	 */
	BigDecimal getVolumeQNRpp();

	/**
	 * @param volumeQNRpp
	 *            the volumeQNRpp to set
	 */
	void setVolumeQNRpp(BigDecimal volumeQNRpp);

	/**
	 * @return the volumeQNRfm
	 */
	BigDecimal getVolumeQNRfm();

	/**
	 * @param volumeQNRfm
	 *            the volumeQNRfm to set
	 */
	void setVolumeQNRfm(BigDecimal volumeQNRfm);

	/**
	 * @return the volumeQNRff
	 */
	BigDecimal getVolumeQNRff();

	/**
	 * @param volumeQNRff
	 *            the volumeQNRff to set
	 */
	void setVolumeQNRff(BigDecimal volumeQNRff);

	/**
	 * @return the volumeQNRPNPcdl
	 */
	BigDecimal getVolumeQNRPNPcdl();

	/**
	 * @param volumeQNRPNPcdl
	 *            the volumeQNRPNPcdl to set
	 */
	void setVolumeQNRPNPcdl(BigDecimal volumeQNRPNPcdl);

}
