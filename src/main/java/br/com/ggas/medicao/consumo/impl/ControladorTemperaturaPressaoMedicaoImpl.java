/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.ControladorTemperaturaPressaoMedicao;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.TemperaturaPressaoMedicao;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorTemperaturaPressaoMedicaoImpl extends ControladorNegocioImpl implements ControladorTemperaturaPressaoMedicao {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(TemperaturaPressaoMedicao.BEAN_ID_TEMPERATURA_PRESSAO_MEDICAO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(TemperaturaPressaoMedicao.BEAN_ID_TEMPERATURA_PRESSAO_MEDICAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ControladorTemperaturaPressaoMedicao#
	 * obterTemperaturaMedicaoVigente(br.com.ggas.
	 * cadastro.geografico.Municipio,
	 * br.com.ggas.cadastro.localidade.Localidade,
	 * java.util.Date)
	 */
	@Override
	public TemperaturaPressaoMedicao obterTemperaturaMedicaoVigente(Municipio municipio, Localidade localidade, Date dataReferencia) {

		String parametro = "( municipio.chavePrimaria = " + municipio.getChavePrimaria() + " or " + "regiao.chavePrimaria = "
						+ municipio.getRegiao().getChavePrimaria() + " or " + "unidadeFederacao.chavePrimaria = "
						+ municipio.getUnidadeFederacao().getChavePrimaria() + ")";

		Query query = criarConsultaTemperaturaMedicaoVigente(parametro, dataReferencia);

		query.setMaxResults(1);

		return (TemperaturaPressaoMedicao) query.uniqueResult();
	}

	/**
	 * Criar consulta temperatura medicao vigente.
	 * 
	 * @param parametro
	 *            the parametro
	 * @param dataReferencia
	 *            the data referencia
	 * @return the query
	 */
	private Query criarConsultaTemperaturaMedicaoVigente(String parametro, Date dataReferencia) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(parametro);
		hql.append(" and dataInicio <= :dataReferencia ");
		hql.append(" order by dataInicio desc, municipio.chavePrimaria asc, regiao.chavePrimaria asc, unidadeFederacao.chavePrimaria asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setDate("dataReferencia", dataReferencia);
		query.setMaxResults(1);

		return query;
	}
	
	@Override
	public Collection<TemperaturaPressaoMedicao> obterTemperaturaMedicaoPorMunicipio(Municipio municipio) {

		Collection<TemperaturaPressaoMedicao> retorno = new ArrayList<TemperaturaPressaoMedicao>();

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" municipio.chavePrimaria = :municipioChavePrimaria ");
		hql.append(" or regiao.chavePrimaria = :regiaoChavePrimaria ");
		hql.append(" or unidadeFederacao.chavePrimaria = :unidadeFederacaoPrimaria ");
		hql.append(" order by dataInicio desc, municipio.chavePrimaria asc, regiao.chavePrimaria asc, unidadeFederacao.chavePrimaria asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (municipio.getChavePrimaria() > 0) {
			query.setLong("municipioChavePrimaria", municipio.getChavePrimaria());
		}
		if (municipio.getRegiao().getChavePrimaria() > 0) {
			query.setLong("regiaoChavePrimaria", municipio.getRegiao().getChavePrimaria());
		}
		if (municipio.getUnidadeFederacao().getChavePrimaria() > 0) {
			query.setLong("unidadeFederacaoPrimaria", municipio.getUnidadeFederacao().getChavePrimaria());
		}

		Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoMedicao = query.list();
		retorno.addAll(listaTemperaturaPressaoMedicao);

		return retorno;

	}
	

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ControladorTemperaturaPressaoMedicao#
	 * listarTemperaturaPressaoMedicaoManutencao(java
	 * .lang.Long,
	 * java.lang.Long, java.lang.Long,
	 * java.lang.Long, java.util.Date,
	 * java.lang.Long)
	 */
	@Override
	public Collection<TemperaturaPressaoMedicao> listarTemperaturaPressaoMedicaoManutencao(Long chaveLocalidade, Long chaveMicrorregiao,
					Long chaveMunicipio, Long chaveRegiao, Date dataInicial, Long chaveUnidadeFederacao) throws NegocioException {

		Collection<TemperaturaPressaoMedicao> listaVigenciaPosterior = listarTemperaturaPressaoMedicaoVigenciaPosterior(chaveLocalidade,
						chaveMicrorregiao, chaveMunicipio, chaveRegiao, dataInicial, chaveUnidadeFederacao);

		Collection<TemperaturaPressaoMedicao> listaVigenciaAnterior = listarTemperaturaPressaoMedicaoVigenciaAnterior(chaveLocalidade,
						chaveMicrorregiao, chaveMunicipio, chaveRegiao, dataInicial, chaveUnidadeFederacao);

		if((listaVigenciaPosterior != null) && (!listaVigenciaPosterior.isEmpty())) {
			Collection<TemperaturaPressaoMedicao> lista = new ArrayList<TemperaturaPressaoMedicao>();

			for (TemperaturaPressaoMedicao temperaturaPressaoMedicaoPosterior : listaVigenciaPosterior) {
				boolean add = true;

				if((listaVigenciaAnterior != null) && (!listaVigenciaAnterior.isEmpty())) {
					for (TemperaturaPressaoMedicao temperaturaPressaoMedicaoAnterior : listaVigenciaAnterior) {

						if(temperaturaPressaoMedicaoAnterior.getChavePrimaria() == temperaturaPressaoMedicaoPosterior.getChavePrimaria()) {
							add = false;
							break;
						}

					}
				}

				if(add) {
					lista.add(temperaturaPressaoMedicaoPosterior);
				}
			}

			listaVigenciaAnterior.addAll(lista);

		}

		return listaVigenciaAnterior;
	}

	/**
	 * Listar temperatura pressao medicao vigencia anterior.
	 * 
	 * @param chaveLocalidade
	 *            the chave localidade
	 * @param chaveMicrorregiao
	 *            the chave microrregiao
	 * @param chaveMunicipio
	 *            the chave municipio
	 * @param chaveRegiao
	 *            the chave regiao
	 * @param dataInicial
	 *            the data inicial
	 * @param chaveUnidadeFederacao
	 *            the chave unidade federacao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<TemperaturaPressaoMedicao> listarTemperaturaPressaoMedicaoVigenciaAnterior(Long chaveLocalidade,
					Long chaveMicrorregiao, Long chaveMunicipio, Long chaveRegiao, Date dataInicial, Long chaveUnidadeFederacao)
					throws NegocioException {

		Collection<TemperaturaPressaoMedicao> retorno = new ArrayList<TemperaturaPressaoMedicao>();

		Date dataReferencia = new Date();
		if(dataInicial != null) {
			dataReferencia = dataInicial;
		}

		// Obtém a medição vigente em relação a
		// data referência
		TemperaturaPressaoMedicao temperaturaPressaoMedicaoVigente = this.obterTemperaturaPressaoMedicaoVigentePorData(chaveLocalidade,
						chaveMicrorregiao, chaveMunicipio, chaveRegiao, dataReferencia, chaveUnidadeFederacao);

		if(temperaturaPressaoMedicaoVigente != null) {
			retorno.add(temperaturaPressaoMedicaoVigente);

			// Obtém medições posteriores a data
			// referência e anteriores a data
			// corrente
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where ");

			hql.append(" dataInicio > :dataReferencia ");
			hql.append(" and dataInicio < :dataAtual ");

			if(chaveUnidadeFederacao != null && chaveUnidadeFederacao > 0) {
				hql.append(" and unidadeFederacao.chavePrimaria = :chaveUnidadeFederacao ");
			}
			if(chaveLocalidade != null && chaveLocalidade > 0) {
				hql.append(" and localidade.chavePrimaria = :chaveLocalidade ");
			}
			if(chaveMicrorregiao != null && chaveMicrorregiao > 0) {
				hql.append(" and microrregiao.chavePrimaria = :chaveMicrorregiao ");
			}
			if(chaveMunicipio != null && chaveMunicipio > 0) {
				hql.append(" and municipio.chavePrimaria = :chaveMunicipio ");
			}
			if(chaveRegiao != null && chaveRegiao > 0) {
				hql.append(" and regiao.chavePrimaria = :chaveRegiao ");
			}

			hql.append(" order by dataInicio ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			Util.adicionarRestricaoDataSemHora(query, temperaturaPressaoMedicaoVigente.getDataInicio(), "dataReferencia", Boolean.FALSE);
			Util.adicionarRestricaoDataSemHora(query, new Date(), "dataAtual", Boolean.TRUE);

			if(chaveUnidadeFederacao != null && chaveUnidadeFederacao > 0) {
				query.setLong("chaveUnidadeFederacao", chaveUnidadeFederacao);
			}
			if(chaveLocalidade != null && chaveLocalidade > 0) {
				query.setLong("chaveLocalidade", chaveLocalidade);
			}
			if(chaveMicrorregiao != null && chaveMicrorregiao > 0) {
				query.setLong("chaveMicrorregiao", chaveMicrorregiao);
			}
			if(chaveMunicipio != null && chaveMunicipio > 0) {
				query.setLong("chaveMunicipio", chaveMunicipio);
			}
			if(chaveRegiao != null && chaveRegiao > 0) {
				query.setLong("chaveRegiao", chaveRegiao);
			}

			Collection<TemperaturaPressaoMedicao> listaVigenciaAnterior = query.list();

			if(listaVigenciaAnterior != null && !listaVigenciaAnterior.isEmpty()) {
				retorno.addAll(listaVigenciaAnterior);
			}
		}

		return retorno;
	}

	/**
	 * Listar temperatura pressao medicao vigencia posterior.
	 * 
	 * @param chaveLocalidade
	 *            the chave localidade
	 * @param chaveMicrorregiao
	 *            the chave microrregiao
	 * @param chaveMunicipio
	 *            the chave municipio
	 * @param chaveRegiao
	 *            the chave regiao
	 * @param dataInicial
	 *            the data inicial
	 * @param chaveUnidadeFederacao
	 *            the chave unidade federacao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<TemperaturaPressaoMedicao> listarTemperaturaPressaoMedicaoVigenciaPosterior(Long chaveLocalidade,
					Long chaveMicrorregiao, Long chaveMunicipio, Long chaveRegiao, Date dataInicial, Long chaveUnidadeFederacao)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" dataInicio > :data ");

		if(chaveUnidadeFederacao != null && chaveUnidadeFederacao > 0) {
			hql.append(" and unidadeFederacao.chavePrimaria = :chaveUnidadeFederacao ");
		}
		if(chaveLocalidade != null && chaveLocalidade > 0) {
			hql.append(" and localidade.chavePrimaria = :chaveLocalidade ");
		}
		if(chaveMicrorregiao != null && chaveMicrorregiao > 0) {
			hql.append(" and microrregiao.chavePrimaria = :chaveMicrorregiao ");
		}
		if(chaveMunicipio != null && chaveMunicipio > 0) {
			hql.append(" and municipio.chavePrimaria = :chaveMunicipio ");
		}
		if(chaveRegiao != null && chaveRegiao > 0) {
			hql.append(" and regiao.chavePrimaria = :chaveRegiao ");
		}

		hql.append(" order by dataInicio ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(dataInicial == null) {
			Util.adicionarRestricaoDataSemHora(query, new Date(), "data", Boolean.FALSE);
		} else {
			Util.adicionarRestricaoDataSemHora(query, dataInicial, "data", Boolean.FALSE);
		}

		if(chaveUnidadeFederacao != null && chaveUnidadeFederacao > 0) {
			query.setLong("chaveUnidadeFederacao", chaveUnidadeFederacao);
		}
		if(chaveLocalidade != null && chaveLocalidade > 0) {
			query.setLong("chaveLocalidade", chaveLocalidade);
		}
		if(chaveMicrorregiao != null && chaveMicrorregiao > 0) {
			query.setLong("chaveMicrorregiao", chaveMicrorregiao);
		}
		if(chaveMunicipio != null && chaveMunicipio > 0) {
			query.setLong("chaveMunicipio", chaveMunicipio);
		}
		if(chaveRegiao != null && chaveRegiao > 0) {
			query.setLong("chaveRegiao", chaveRegiao);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ControladorTemperaturaPressaoMedicao#
	 * validarPesquisaManutencaoTemperaturaPressaoMedicao
	 * (java.lang.String,
	 * java.lang.Long, java.util.Date)
	 */
	@Override
	public void validarPesquisaManutencaoTemperaturaPressaoMedicao(String codigoTipoAbrangencia, Long chaveAbrangencia, Date dataInicial)
					throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(codigoTipoAbrangencia)) {
			stringBuilder.append("Tipo de abrangência");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(chaveAbrangencia == null || chaveAbrangencia <= 0) {
			stringBuilder.append("Área de abrangência");
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder
							.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ControladorTemperaturaPressaoMedicao#
	 * atualizarListaTemperaturaPressaoMedicao(java
	 * .util.Collection)
	 */
	@Override
	public boolean atualizarListaTemperaturaPressaoMedicao(Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoMedicao)
					throws NegocioException, ConcorrenciaException {

		boolean retorno = false;

		Set<Date> datas = new HashSet<Date>();
		if(listaTemperaturaPressaoMedicao != null && !listaTemperaturaPressaoMedicao.isEmpty()) {
			// Percorre a lista para validar os
			// dados
			for (TemperaturaPressaoMedicao temperaturaPressaoMedicao : listaTemperaturaPressaoMedicao) {

				// Valida se existe alguma data
				// inferior a data corrente
				if(!retorno) {

					retorno = this.validarDataInicioTemperaturaMedicaoPressao(temperaturaPressaoMedicao);

				} else {

					this.validarDataInicioTemperaturaMedicaoPressao(temperaturaPressaoMedicao);

				}

				// Valida se existe alguma data
				// duplicada
				if(temperaturaPressaoMedicao.getDataInicio() != null) {

					if(this.temperaturaPressaoMedicaoJaExistente(temperaturaPressaoMedicao)) {

						throw new NegocioException(ERRO_NEGOCIO_DATAS_DUPLICADAS, false);

					} else if(datas.contains(temperaturaPressaoMedicao.getDataInicio())) {

						throw new NegocioException(ERRO_NEGOCIO_DATAS_DUPLICADAS, false);

					} else {
						datas.add(temperaturaPressaoMedicao.getDataInicio());
					}

				}

			}

			for (TemperaturaPressaoMedicao temperaturaPressaoMedicao : listaTemperaturaPressaoMedicao) {
				if(temperaturaPressaoMedicao.getChavePrimaria() <= 0) {
					this.inserir(temperaturaPressaoMedicao);
				} else {
					this.validarDadosEntidade(temperaturaPressaoMedicao);

					this.getSession().flush();
					this.getSession().clear();
					this.atualizar(temperaturaPressaoMedicao);

				}
			}
		}

		return retorno;

	}

	/**
	 * Obter ultimo consumo historico faturado.
	 * 
	 * @param temperaturaPressaoMedicao
	 *            the temperatura pressao medicao
	 * @param consumoFaturado
	 *            the consumo faturado
	 * @return the date
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Date obterUltimoConsumoHistoricoFaturado(TemperaturaPressaoMedicao temperaturaPressaoMedicao, boolean consumoFaturado)
					throws NegocioException {

		Criteria criteria = createCriteria(HistoricoConsumo.class);
		criteria.setProjection(Projections.max("historicoAtual.dataLeituraFaturada"));
		criteria.createAlias("pontoConsumo", "pontoConsumo");
		criteria.createAlias("pontoConsumo.imovel", "imovel");
		criteria.createAlias("imovel.quadraFace", "quadraFace");
		criteria.createAlias("quadraFace.quadra", "quadra");
		criteria.createAlias("quadra.setorComercial", "setorComercial");
		criteria.createAlias("setorComercial.municipio", "municipio");
		criteria.createAlias("historicoAtual", "historicoAtual");
		criteria.createAlias("historicoAtual.historicoInstalacaoMedidor", "instalacaoMedidor");

		// habilitado

		if(temperaturaPressaoMedicao.getMunicipio() != null) {

			criteria.add(Restrictions.eq("municipio.chavePrimaria", temperaturaPressaoMedicao.getMunicipio().getChavePrimaria()));

		} else if(temperaturaPressaoMedicao.getUnidadeFederacao() != null) {

			criteria.add(Restrictions.eq("municipio.unidadeFederacao.chavePrimaria", temperaturaPressaoMedicao.getUnidadeFederacao()
							.getChavePrimaria()));

		} else if(temperaturaPressaoMedicao.getRegiao() != null) {

			criteria.createAlias("municipio.microrregiao", "microrregiao");
			criteria.add(Restrictions.eq("microrregiao.regiao.chavePrimaria", temperaturaPressaoMedicao.getRegiao().getChavePrimaria()));

		}

		criteria.add(Restrictions.eq("indicadorFaturamento", consumoFaturado));
		criteria.add(Restrictions.isNotNull("historicoAtual"));
		criteria.add(Restrictions.isNull("instalacaoMedidor.vazaoCorretor"));

		return (Date) criteria.uniqueResult();
	}

	/**
	 * Validar data inicio temperatura medicao pressao.
	 * 
	 * @param temperaturaPressaoMedicao
	 *            the temperatura pressao medicao
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private boolean validarDataInicioTemperaturaMedicaoPressao(TemperaturaPressaoMedicao temperaturaPressaoMedicao) throws NegocioException {

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		boolean retorno = false;

		try {
			Date dataInicioTemperaturaMedicaoPressao = temperaturaPressaoMedicao.getDataInicio();
			Date dataAtual = df.parse(df.format(new Date()));
			Date dataLeitura = this.obterDataUltimoConsumo(temperaturaPressaoMedicao, true);

			if((dataInicioTemperaturaMedicaoPressao != null) && (dataLeitura != null)) {

				Date dataInicioTemperaturaPressaoMedicao = df.parse(df.format(dataInicioTemperaturaMedicaoPressao));

				// Data informada anterior a data
				// atual
				if(dataInicioTemperaturaMedicaoPressao.before(dataAtual)) {

					if(!dataInicioTemperaturaPressaoMedicao.after(dataLeitura)) {

						throw new NegocioException(ERRO_NEGOCIO_DATAS_ANTERIORES_AO_ULTIMO_FATURAMENTO, false);

					} else {

						dataLeitura = this.obterDataUltimoConsumo(temperaturaPressaoMedicao, false);

						if((dataLeitura != null) && (dataInicioTemperaturaPressaoMedicao.before(dataLeitura))) {

							retorno = true;

						}

					}

				}

			}

		} catch(ParseException e) {
			throw new NegocioException(e.getMessage());
		}

		return retorno;
	}

	/**
	 * Obter data ultimo consumo.
	 * 
	 * @param temperaturaPressaoMedicao
	 *            the temperatura pressao medicao
	 * @param faturado
	 *            the faturado
	 * @return the date
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private Date obterDataUltimoConsumo(TemperaturaPressaoMedicao temperaturaPressaoMedicao, boolean faturado) throws NegocioException {

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date dataUltimaleitura = this.obterUltimoConsumoHistoricoFaturado(temperaturaPressaoMedicao, faturado);

		if(dataUltimaleitura != null) {

			try {
				dataUltimaleitura = df.parse(df.format(dataUltimaleitura));
			} catch(ParseException e) {
				throw new NegocioException(e.getMessage());
			}

		}

		return dataUltimaleitura;
	}

	/**
	 * Temperatura pressao medicao ja existente.
	 * 
	 * @param temperaturaPressaoMedicao
	 *            the temperatura pressao medicao
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private boolean temperaturaPressaoMedicaoJaExistente(TemperaturaPressaoMedicao temperaturaPressaoMedicao) throws NegocioException {

		SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		boolean retorno = false;

		Date dataInicio = null;

		try {
			dataInicio = df2.parse(df1.format(temperaturaPressaoMedicao.getDataInicio()) + " 00:00:00");
		} catch(ParseException e) {
			throw new NegocioException(e.getMessage());
		}

		Criteria criteria = this.createCriteria(TemperaturaPressaoMedicao.class);
		criteria.add(Restrictions.eq("dataInicio", dataInicio));

		if(temperaturaPressaoMedicao.getLocalidade() != null) {
			criteria.add(Restrictions.eq("localidade.chavePrimaria", temperaturaPressaoMedicao.getLocalidade().getChavePrimaria()));
		}
		if(temperaturaPressaoMedicao.getMicrorregiao() != null) {
			criteria.add(Restrictions.eq("microrregiao.chavePrimaria", temperaturaPressaoMedicao.getMicrorregiao().getChavePrimaria()));
		}
		if(temperaturaPressaoMedicao.getMunicipio() != null) {
			criteria.add(Restrictions.eq("municipio.chavePrimaria", temperaturaPressaoMedicao.getMunicipio().getChavePrimaria()));
		}
		if(temperaturaPressaoMedicao.getRegiao() != null) {
			criteria.add(Restrictions.eq("regiao.chavePrimaria", temperaturaPressaoMedicao.getRegiao().getChavePrimaria()));
		}
		if(temperaturaPressaoMedicao.getUnidadeFederacao() != null) {
			criteria.add(Restrictions.eq("unidadeFederacao.chavePrimaria", temperaturaPressaoMedicao.getUnidadeFederacao()
							.getChavePrimaria()));
		}

		List<TemperaturaPressaoMedicao> lista = criteria.list();

		if(lista != null && !lista.isEmpty()) {
			if(temperaturaPressaoMedicao.getChavePrimaria() != 0) {
				for (TemperaturaPressaoMedicao temperaturaPressaoMedicaoBD : lista) {
					if(temperaturaPressaoMedicaoBD.getChavePrimaria() == temperaturaPressaoMedicao.getChavePrimaria()) {
						retorno = false;
					} else {
						retorno = true;
					}
				}
			} else {
				retorno = true;
			}
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ControladorTemperaturaPressaoMedicao#
	 * obterTemperaturaPressaoMedicaoVigentePorData(
	 * java.lang.Long,
	 * java.lang.Long, java.lang.Long,
	 * java.lang.Long, java.util.Date,
	 * java.lang.Long)
	 */
	@Override
	public TemperaturaPressaoMedicao obterTemperaturaPressaoMedicaoVigentePorData(Long chaveLocalidade, Long chaveMicrorregiao,
					Long chaveMunicipio, Long chaveRegiao, Date dataReferencia, Long chaveUnidadeFederacao) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where ");
		hql.append(" dataInicio <= :dataReferencia ");

		if(chaveUnidadeFederacao != null && chaveUnidadeFederacao > 0) {
			hql.append(" and unidadeFederacao.chavePrimaria = :chaveUnidadeFederacao ");
		}
		if(chaveLocalidade != null && chaveLocalidade > 0) {
			hql.append(" and localidade.chavePrimaria = :chaveLocalidade ");
		}
		if(chaveMicrorregiao != null && chaveMicrorregiao > 0) {
			hql.append(" and microrregiao.chavePrimaria = :chaveMicrorregiao ");
		}
		if(chaveMunicipio != null && chaveMunicipio > 0) {
			hql.append(" and municipio.chavePrimaria = :chaveMunicipio ");
		}
		if(chaveRegiao != null && chaveRegiao > 0) {
			hql.append(" and regiao.chavePrimaria = :chaveRegiao ");
		}

		hql.append(" order by dataInicio desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setDate("dataReferencia", dataReferencia);

		if(chaveUnidadeFederacao != null && chaveUnidadeFederacao > 0) {
			query.setLong("chaveUnidadeFederacao", chaveUnidadeFederacao);
		}
		if(chaveLocalidade != null && chaveLocalidade > 0) {
			query.setLong("chaveLocalidade", chaveLocalidade);
		}
		if(chaveMicrorregiao != null && chaveMicrorregiao > 0) {
			query.setLong("chaveMicrorregiao", chaveMicrorregiao);
		}
		if(chaveMunicipio != null && chaveMunicipio > 0) {
			query.setLong("chaveMunicipio", chaveMunicipio);
		}
		if(chaveRegiao != null && chaveRegiao > 0) {
			query.setLong("chaveRegiao", chaveRegiao);
		}
		query.setMaxResults(1);

		return (TemperaturaPressaoMedicao) query.uniqueResult();
	}

	@Override
	public List<TemperaturaPressaoMedicao> obterTemperaturaPressaoMedicaoVigente(Municipio municipio,
			Localidade localidade) {

		Criteria criteria = this.createCriteria(TemperaturaPressaoMedicao.class);
		criteria.addOrder(Order.asc("dataInicio"));

		Criterion criterionLocalidade = Restrictions.eq("localidade.chavePrimaria", localidade.getChavePrimaria());
		Criterion criterionMunicipio = Restrictions.eq("municipio.chavePrimaria", municipio.getChavePrimaria());
		Criterion criterionRegiao = Restrictions.eq("regiao.chavePrimaria",
				municipio.getMicrorregiao().getRegiao().getChavePrimaria());
		Criterion criterionUnidadeFederacao = Restrictions.eq("unidadeFederacao.chavePrimaria",
				municipio.getUnidadeFederacao().getChavePrimaria());

		criteria.add(Restrictions.or(Restrictions.or(criterionLocalidade, criterionMunicipio),
				Restrictions.or(criterionRegiao, criterionUnidadeFederacao)));

		return criteria.list();
	}
	

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ControladorTemperaturaPressaoMedicao#
	 * permitirAtualizacaoTemperaturaPressaoMedicao
	 * (br.com.ggas.medicao.consumo.impl.
	 * TemperaturaPressaoMedicaoImpl)
	 */
	@Override
	public boolean permitirAtualizacaoTemperaturaPressaoMedicao(TemperaturaPressaoMedicao temperaturaPressaoMedicao)
					throws NegocioException {

		boolean retorno = true;

		Date dataUltimaLeitura = this.obterDataUltimoConsumo(temperaturaPressaoMedicao, true);

		if(dataUltimaLeitura != null) {
			DateTime dataPrimeiraPermitida = new DateTime(dataUltimaLeitura);
			dataPrimeiraPermitida = dataPrimeiraPermitida.plusDays(1);
			dataPrimeiraPermitida = dataPrimeiraPermitida.withHourOfDay(0);
			dataPrimeiraPermitida = dataPrimeiraPermitida.withMinuteOfHour(0);
			dataPrimeiraPermitida = dataPrimeiraPermitida.withSecondOfMinute(0);
			dataPrimeiraPermitida = dataPrimeiraPermitida.withMillisOfSecond(0);

			if(temperaturaPressaoMedicao.getDataInicio().before(dataPrimeiraPermitida.toDate())) {
				retorno = false;
			}

		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.ControladorTemperaturaPressaoMedicao#consultarTemperaturaPressaoMedicao(java.util.Map)
	 */
	@Override
	public Collection<TemperaturaPressaoMedicao> consultarTemperaturaPressaoMedicao(Map<String, Object> filtro) {

		Criteria criteria = getCriteria();

		if(filtro != null) {

			Long unidadeFederacao = (Long) filtro.get("idUnidadeFederacao");
			if((unidadeFederacao != null) && (unidadeFederacao > 0)) {
				criteria.createAlias("unidadeFederacao", "unidadeFederacao");
				criteria.add(Restrictions.eq("unidadeFederacao.chavePrimaria", unidadeFederacao));
			}

			Long idMunicipio = (Long) filtro.get("idMunicipio");
			if((idMunicipio != null) && (idMunicipio > 0)) {
				criteria.add(Restrictions.eq("municipio.chavePrimaria", idMunicipio));
			}

		}

		return criteria.list();
	}
}
