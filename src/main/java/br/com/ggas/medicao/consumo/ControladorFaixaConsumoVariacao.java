
package br.com.ggas.medicao.consumo;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados
 * a ControladorFaixaConsumoVariacao
 *
 */
public interface ControladorFaixaConsumoVariacao extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_ACESSO = "controladorFaixaConsumoVariacao";

	String ERRO_FAIXA_CONSUMO_VARIACAO = "ERRO_FAIXA_CONSUMO_VARIACAO";

	/**
	 * Consultar todos faixa consumo variacao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<FaixaConsumoVariacao> consultarTodosFaixaConsumoVariacao(Map<String, Object> filtro);

	/**
	 * Obter faixa consumo variacao no intervalo.
	 * 
	 * @param listaFaixaConsumoVariacao
	 *            the lista faixa consumo variacao
	 * @param consumo
	 *            the consumo
	 * @return the faixa consumo variacao
	 */
	FaixaConsumoVariacao obterFaixaConsumoVariacaoNoIntervalo(Collection<FaixaConsumoVariacao> listaFaixaConsumoVariacao, BigDecimal consumo);

	/**
	 * Validar faixas zeradas faixa consumo variacao.
	 * 
	 * @param listaDadosFaixa
	 *            the lista dados faixa
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */
	void validarFaixasZeradasFaixaConsumoVariacao(List<FaixaConsumoVariacao> listaDadosFaixa) throws NegocioException,
					FormatoInvalidoException;

	/**
	 * Consultar faixas consumo variacao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<FaixaConsumoVariacao> consultarFaixasConsumoVariacao(Map<String, Object> filtro) throws NegocioException;
}
