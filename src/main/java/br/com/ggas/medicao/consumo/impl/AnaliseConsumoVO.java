/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo.impl;

import java.io.Serializable;
import java.util.Collection;

/**
 * Classe para análise de Consumo
 *
 */
public class AnaliseConsumoVO implements Serializable {

	private static final long serialVersionUID = 8889163747116920034L;

	private String localidade;

	private String leiturista;

	private String setor;

	private String grupo;

	private String descricaoPontoConsumo;

	private String matriculaImovelAssociado;

	private String segmento;

	private String situacaoPontoConsumo;

	private String medidorInstaladoPontoConsumo;

	private String dataInstalacaoMedidor;

	private String nome;

	private String enderecoPontoConsumo;

	private String quadra;

	private String quadraFace;

	private String rota;

	private String sequenciaLeituraRota;

	private String dataLeituraInformada;

	private String dataAnteriorLeituraInformada;

	private String linha1Referencia;

	private String linha1Leitura;

	private String linha1Medido;

	private String linha1Media;

	private String linha1Faturado;

	private String linha1CDL;

	private String linha1CDLN;

	private String linha1Medidor;

	private String linha2Referencia;

	private String linha2Leitura;

	private String linha2Medido;

	private String linha2Media;

	private String linha2Faturado;

	private String linha2CDL;

	private String linha2CDLN;

	private String linha2Medidor;

	private String linha3Referencia;

	private String linha3Leitura;

	private String linha3Medido;

	private String linha3Media;

	private String linha3Faturado;

	private String linha3CDL;

	private String linha3CDLN;

	private String linha3Medidor;

	private String linha4Referencia;

	private String linha4Leitura;

	private String linha4Medido;

	private String linha4Media;

	private String linha4Faturado;

	private String linha4CDL;

	private String linha4CDLN;

	private String linha4Medidor;

	private String linha5Referencia;

	private String linha5Leitura;

	private String linha5Medido;

	private String linha5Media;

	private String linha5Faturado;

	private String linha5CDL;

	private String linha5CDLN;

	private String linha5Medidor;

	private String linha6Referencia;

	private String linha6Leitura;

	private String linha6Medido;

	private String linha6Media;

	private String linha6Faturado;

	private String linha6CDL;

	private String linha6CDLN;

	private String linha6Medidor;

	private String linha1LeituraInformada;

	private String linha2LeituraInformada;

	private String linha3LeituraInformada;

	private String linha4LeituraInformada;

	private String linha5LeituraInformada;

	private String linha6LeituraInformada;

	private String linha1ptz;

	private String linha2ptz;

	private String linha3ptz;

	private String linha4ptz;

	private String linha5ptz;

	private String linha6ptz;

	private String linha1pcs;

	private String linha2pcs;

	private String linha3pcs;

	private String linha4pcs;

	private String linha5pcs;

	private String linha6pcs;

	/**
	 * @return the localidade
	 */
	public String getLocalidade() {

		return localidade;
	}

	/**
	 * @param localidade
	 *            the localidade to set
	 */
	public void setLocalidade(String localidade) {

		this.localidade = localidade;
	}

	/**
	 * @return the leiturista
	 */
	public String getLeiturista() {

		return leiturista;
	}

	/**
	 * @param leiturista
	 *            the leiturista to set
	 */
	public void setLeiturista(String leiturista) {

		this.leiturista = leiturista;
	}

	/**
	 * @return the setor
	 */
	public String getSetor() {

		return setor;
	}

	/**
	 * @param setor
	 *            the setor to set
	 */
	public void setSetor(String setor) {

		this.setor = setor;
	}

	/**
	 * @return the grupo
	 */
	public String getGrupo() {

		return grupo;
	}

	/**
	 * @param grupo
	 *            the grupo to set
	 */
	public void setGrupo(String grupo) {

		this.grupo = grupo;
	}

	/**
	 * @return the descricaoPontoConsumo
	 */
	public String getDescricaoPontoConsumo() {

		return descricaoPontoConsumo;
	}

	/**
	 * @param descricaoPontoConsumo
	 *            the descricaoPontoConsumo to set
	 */
	public void setDescricaoPontoConsumo(String descricaoPontoConsumo) {

		this.descricaoPontoConsumo = descricaoPontoConsumo;
	}

	/**
	 * @return the matriculaImovelAssociado
	 */
	public String getMatriculaImovelAssociado() {

		return matriculaImovelAssociado;
	}

	/**
	 * @param matriculaImovelAssociado
	 *            the matriculaImovelAssociado to
	 *            set
	 */
	public void setMatriculaImovelAssociado(String matriculaImovelAssociado) {

		this.matriculaImovelAssociado = matriculaImovelAssociado;
	}

	/**
	 * @return the segmento
	 */
	public String getSegmento() {

		return segmento;
	}

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	/**
	 * @return the situacaoPontoConsumo
	 */
	public String getSituacaoPontoConsumo() {

		return situacaoPontoConsumo;
	}

	/**
	 * @param situacaoPontoConsumo
	 *            the situacaoPontoConsumo to set
	 */
	public void setSituacaoPontoConsumo(String situacaoPontoConsumo) {

		this.situacaoPontoConsumo = situacaoPontoConsumo;
	}

	/**
	 * @return the medidorInstaladoPontoConsumo
	 */
	public String getMedidorInstaladoPontoConsumo() {

		return medidorInstaladoPontoConsumo;
	}

	/**
	 * @param medidorInstaladoPontoConsumo
	 *            the medidorInstaladoPontoConsumo
	 *            to set
	 */
	public void setMedidorInstaladoPontoConsumo(String medidorInstaladoPontoConsumo) {

		this.medidorInstaladoPontoConsumo = medidorInstaladoPontoConsumo;
	}

	/**
	 * @return the dataInstalacaoMedidor
	 */
	public String getDataInstalacaoMedidor() {

		return dataInstalacaoMedidor;
	}

	/**
	 * @param dataInstalacaoMedidor
	 *            the dataInstalacaoMedidor to set
	 */
	public void setDataInstalacaoMedidor(String dataInstalacaoMedidor) {

		this.dataInstalacaoMedidor = dataInstalacaoMedidor;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {

		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {

		this.nome = nome;
	}

	/**
	 * @return the enderecoPontoConsumo
	 */
	public String getEnderecoPontoConsumo() {

		return enderecoPontoConsumo;
	}

	/**
	 * @param enderecoPontoConsumo
	 *            the enderecoPontoConsumo to set
	 */
	public void setEnderecoPontoConsumo(String enderecoPontoConsumo) {

		this.enderecoPontoConsumo = enderecoPontoConsumo;
	}

	/**
	 * @return the quadra
	 */
	public String getQuadra() {

		return quadra;
	}

	/**
	 * @param quadra
	 *            the quadra to set
	 */
	public void setQuadra(String quadra) {

		this.quadra = quadra;
	}

	/**
	 * @return the quadraFace
	 */
	public String getQuadraFace() {

		return quadraFace;
	}

	/**
	 * @param quadraFace
	 *            the quadraFace to set
	 */
	public void setQuadraFace(String quadraFace) {

		this.quadraFace = quadraFace;
	}

	/**
	 * @return the rota
	 */
	public String getRota() {

		return rota;
	}

	/**
	 * @param rota
	 *            the rota to set
	 */
	public void setRota(String rota) {

		this.rota = rota;
	}

	/**
	 * @return the sequenciaLeituraRota
	 */
	public String getSequenciaLeituraRota() {

		return sequenciaLeituraRota;
	}

	/**
	 * @param sequenciaLeituraRota
	 *            the sequenciaLeituraRota to set
	 */
	public void setSequenciaLeituraRota(String sequenciaLeituraRota) {

		this.sequenciaLeituraRota = sequenciaLeituraRota;
	}

	/**
	 * @return the dataLeituraInformada
	 */
	public String getDataLeituraInformada() {

		return dataLeituraInformada;
	}

	/**
	 * @param dataLeituraInformada
	 *            the dataLeituraInformada to set
	 */
	public void setDataLeituraInformada(String dataLeituraInformada) {

		this.dataLeituraInformada = dataLeituraInformada;
	}

	/**
	 * @return the dataAnteriorLeituraInformada
	 */
	public String getDataAnteriorLeituraInformada() {

		return dataAnteriorLeituraInformada;
	}

	/**
	 * @param dataAnteriorLeituraInformada
	 *            the dataAnteriorLeituraInformada
	 *            to set
	 */
	public void setDataAnteriorLeituraInformada(String dataAnteriorLeituraInformada) {

		this.dataAnteriorLeituraInformada = dataAnteriorLeituraInformada;
	}

	/**
	 * @return the linha1Referencia
	 */
	public String getLinha1Referencia() {

		return linha1Referencia;
	}

	/**
	 * @param linha1Referencia
	 *            the linha1Referencia to set
	 */
	public void setLinha1Referencia(String linha1Referencia) {

		this.linha1Referencia = linha1Referencia;
	}

	/**
	 * @return the linha1Leitura
	 */
	public String getLinha1Leitura() {

		return linha1Leitura;
	}

	/**
	 * @param linha1Leitura
	 *            the linha1Leitura to set
	 */
	public void setLinha1Leitura(String linha1Leitura) {

		this.linha1Leitura = linha1Leitura;
	}

	/**
	 * @return the linha1Medido
	 */
	public String getLinha1Medido() {

		return linha1Medido;
	}

	/**
	 * @param linha1Medido
	 *            the linha1Medido to set
	 */
	public void setLinha1Medido(String linha1Medido) {

		this.linha1Medido = linha1Medido;
	}

	/**
	 * @return the linha1Faturado
	 */
	public String getLinha1Faturado() {

		return linha1Faturado;
	}

	/**
	 * @param linha1Faturado
	 *            the linha1Faturado to set
	 */
	public void setLinha1Faturado(String linha1Faturado) {

		this.linha1Faturado = linha1Faturado;
	}

	/**
	 * @return the linha1CDL
	 */
	public String getLinha1CDL() {

		return linha1CDL;
	}

	/**
	 * @param linha1CDL
	 *            the linha1CDL to set
	 */
	public void setLinha1CDL(String linha1CDL) {

		this.linha1CDL = linha1CDL;
	}

	/**
	 * @return the linha1CDLN
	 */
	public String getLinha1CDLN() {

		return linha1CDLN;
	}

	/**
	 * @param linha1CDLN
	 *            the linha1CDLN to set
	 */
	public void setLinha1CDLN(String linha1CDLN) {

		this.linha1CDLN = linha1CDLN;
	}

	/**
	 * @return the linha2Referencia
	 */
	public String getLinha2Referencia() {

		return linha2Referencia;
	}

	/**
	 * @param linha2Referencia
	 *            the linha2Referencia to set
	 */
	public void setLinha2Referencia(String linha2Referencia) {

		this.linha2Referencia = linha2Referencia;
	}

	/**
	 * @return the linha2Leitura
	 */
	public String getLinha2Leitura() {

		return linha2Leitura;
	}

	/**
	 * @param linha2Leitura
	 *            the linha2Leitura to set
	 */
	public void setLinha2Leitura(String linha2Leitura) {

		this.linha2Leitura = linha2Leitura;
	}

	/**
	 * @return the linha2Medido
	 */
	public String getLinha2Medido() {

		return linha2Medido;
	}

	/**
	 * @param linha2Medido
	 *            the linha2Medido to set
	 */
	public void setLinha2Medido(String linha2Medido) {

		this.linha2Medido = linha2Medido;
	}

	/**
	 * @return the linha2Faturado
	 */
	public String getLinha2Faturado() {

		return linha2Faturado;
	}

	/**
	 * @param linha2Faturado
	 *            the linha2Faturado to set
	 */
	public void setLinha2Faturado(String linha2Faturado) {

		this.linha2Faturado = linha2Faturado;
	}

	/**
	 * @return the linha2CDL
	 */
	public String getLinha2CDL() {

		return linha2CDL;
	}

	/**
	 * @param linha2CDL
	 *            the linha2CDL to set
	 */
	public void setLinha2CDL(String linha2CDL) {

		this.linha2CDL = linha2CDL;
	}

	/**
	 * @return the linha2CDLN
	 */
	public String getLinha2CDLN() {

		return linha2CDLN;
	}

	/**
	 * @param linha2CDLN
	 *            the linha2CDLN to set
	 */
	public void setLinha2CDLN(String linha2CDLN) {

		this.linha2CDLN = linha2CDLN;
	}

	/**
	 * @return the linha3Referencia
	 */
	public String getLinha3Referencia() {

		return linha3Referencia;
	}

	/**
	 * @param linha3Referencia
	 *            the linha3Referencia to set
	 */
	public void setLinha3Referencia(String linha3Referencia) {

		this.linha3Referencia = linha3Referencia;
	}

	/**
	 * @return the linha3Leitura
	 */
	public String getLinha3Leitura() {

		return linha3Leitura;
	}

	/**
	 * @param linha3Leitura
	 *            the linha3Leitura to set
	 */
	public void setLinha3Leitura(String linha3Leitura) {

		this.linha3Leitura = linha3Leitura;
	}

	/**
	 * @return the linha3Medido
	 */
	public String getLinha3Medido() {

		return linha3Medido;
	}

	/**
	 * @param linha3Medido
	 *            the linha3Medido to set
	 */
	public void setLinha3Medido(String linha3Medido) {

		this.linha3Medido = linha3Medido;
	}

	/**
	 * @return the linha3Faturado
	 */
	public String getLinha3Faturado() {

		return linha3Faturado;
	}

	/**
	 * @param linha3Faturado
	 *            the linha3Faturado to set
	 */
	public void setLinha3Faturado(String linha3Faturado) {

		this.linha3Faturado = linha3Faturado;
	}

	/**
	 * @return the linha3CDL
	 */
	public String getLinha3CDL() {

		return linha3CDL;
	}

	/**
	 * @param linha3CDL
	 *            the linha3CDL to set
	 */
	public void setLinha3CDL(String linha3CDL) {

		this.linha3CDL = linha3CDL;
	}

	/**
	 * @return the linha3CDLN
	 */
	public String getLinha3CDLN() {

		return linha3CDLN;
	}

	/**
	 * @param linha3CDLN
	 *            the linha3CDLN to set
	 */
	public void setLinha3CDLN(String linha3CDLN) {

		this.linha3CDLN = linha3CDLN;
	}

	/**
	 * @return the linha4Referencia
	 */
	public String getLinha4Referencia() {

		return linha4Referencia;
	}

	/**
	 * @param linha4Referencia
	 *            the linha4Referencia to set
	 */
	public void setLinha4Referencia(String linha4Referencia) {

		this.linha4Referencia = linha4Referencia;
	}

	/**
	 * @return the linha4Leitura
	 */
	public String getLinha4Leitura() {

		return linha4Leitura;
	}

	/**
	 * @param linha4Leitura
	 *            the linha4Leitura to set
	 */
	public void setLinha4Leitura(String linha4Leitura) {

		this.linha4Leitura = linha4Leitura;
	}

	/**
	 * @return the linha4Medido
	 */
	public String getLinha4Medido() {

		return linha4Medido;
	}

	/**
	 * @param linha4Medido
	 *            the linha4Medido to set
	 */
	public void setLinha4Medido(String linha4Medido) {

		this.linha4Medido = linha4Medido;
	}

	/**
	 * @return the linha4Faturado
	 */
	public String getLinha4Faturado() {

		return linha4Faturado;
	}

	/**
	 * @param linha4Faturado
	 *            the linha4Faturado to set
	 */
	public void setLinha4Faturado(String linha4Faturado) {

		this.linha4Faturado = linha4Faturado;
	}

	/**
	 * @return the linha4CDL
	 */
	public String getLinha4CDL() {

		return linha4CDL;
	}

	/**
	 * @param linha4CDL
	 *            the linha4CDL to set
	 */
	public void setLinha4CDL(String linha4CDL) {

		this.linha4CDL = linha4CDL;
	}

	/**
	 * @return the linha4CDLN
	 */
	public String getLinha4CDLN() {

		return linha4CDLN;
	}

	/**
	 * @param linha4CDLN
	 *            the linha4CDLN to set
	 */
	public void setLinha4CDLN(String linha4CDLN) {

		this.linha4CDLN = linha4CDLN;
	}

	/**
	 * @return the linha5Referencia
	 */
	public String getLinha5Referencia() {

		return linha5Referencia;
	}

	/**
	 * @param linha5Referencia
	 *            the linha5Referencia to set
	 */
	public void setLinha5Referencia(String linha5Referencia) {

		this.linha5Referencia = linha5Referencia;
	}

	/**
	 * @return the linha5Leitura
	 */
	public String getLinha5Leitura() {

		return linha5Leitura;
	}

	/**
	 * @param linha5Leitura
	 *            the linha5Leitura to set
	 */
	public void setLinha5Leitura(String linha5Leitura) {

		this.linha5Leitura = linha5Leitura;
	}

	/**
	 * @return the linha5Medido
	 */
	public String getLinha5Medido() {

		return linha5Medido;
	}

	/**
	 * @param linha5Medido
	 *            the linha5Medido to set
	 */
	public void setLinha5Medido(String linha5Medido) {

		this.linha5Medido = linha5Medido;
	}

	/**
	 * @return the linha5Faturado
	 */
	public String getLinha5Faturado() {

		return linha5Faturado;
	}

	/**
	 * @param linha5Faturado
	 *            the linha5Faturado to set
	 */
	public void setLinha5Faturado(String linha5Faturado) {

		this.linha5Faturado = linha5Faturado;
	}

	/**
	 * @return the linha5CDL
	 */
	public String getLinha5CDL() {

		return linha5CDL;
	}

	/**
	 * @param linha5CDL
	 *            the linha5CDL to set
	 */
	public void setLinha5CDL(String linha5CDL) {

		this.linha5CDL = linha5CDL;
	}

	/**
	 * @return the linha5CDLN
	 */
	public String getLinha5CDLN() {

		return linha5CDLN;
	}

	/**
	 * @param linha5CDLN
	 *            the linha5CDLN to set
	 */
	public void setLinha5CDLN(String linha5CDLN) {

		this.linha5CDLN = linha5CDLN;
	}

	/**
	 * @return the linha6Referencia
	 */
	public String getLinha6Referencia() {

		return linha6Referencia;
	}

	/**
	 * @param linha6Referencia
	 *            the linha6Referencia to set
	 */
	public void setLinha6Referencia(String linha6Referencia) {

		this.linha6Referencia = linha6Referencia;
	}

	/**
	 * @return the linha6Leitura
	 */
	public String getLinha6Leitura() {

		return linha6Leitura;
	}

	/**
	 * @param linha6Leitura
	 *            the linha6Leitura to set
	 */
	public void setLinha6Leitura(String linha6Leitura) {

		this.linha6Leitura = linha6Leitura;
	}

	/**
	 * @return the linha6Medido
	 */
	public String getLinha6Medido() {

		return linha6Medido;
	}

	/**
	 * @param linha6Medido
	 *            the linha6Medido to set
	 */
	public void setLinha6Medido(String linha6Medido) {

		this.linha6Medido = linha6Medido;
	}

	/**
	 * @return the linha6Faturado
	 */
	public String getLinha6Faturado() {

		return linha6Faturado;
	}

	/**
	 * @param linha6Faturado
	 *            the linha6Faturado to set
	 */
	public void setLinha6Faturado(String linha6Faturado) {

		this.linha6Faturado = linha6Faturado;
	}

	/**
	 * @return the linha6CDL
	 */
	public String getLinha6CDL() {

		return linha6CDL;
	}

	/**
	 * @param linha6CDL
	 *            the linha6CDL to set
	 */
	public void setLinha6CDL(String linha6CDL) {

		this.linha6CDL = linha6CDL;
	}

	/**
	 * @return the linha6CDLN
	 */
	public String getLinha6CDLN() {

		return linha6CDLN;
	}

	/**
	 * @param linha6CDLN
	 *            the linha6CDLN to set
	 */
	public void setLinha6CDLN(String linha6CDLN) {

		this.linha6CDLN = linha6CDLN;
	}

	/**
	 * @return the linha1Media
	 */
	public String getLinha1Media() {

		return linha1Media;
	}

	/**
	 * @param linha1Media
	 *            the linha1Media to set
	 */
	public void setLinha1Media(String linha1Media) {

		this.linha1Media = linha1Media;
	}

	/**
	 * @return the linha2Media
	 */
	public String getLinha2Media() {

		return linha2Media;
	}

	/**
	 * @param linha2Media
	 *            the linha2Media to set
	 */
	public void setLinha2Media(String linha2Media) {

		this.linha2Media = linha2Media;
	}

	/**
	 * @return the linha3Media
	 */
	public String getLinha3Media() {

		return linha3Media;
	}

	/**
	 * @param linha3Media
	 *            the linha3Media to set
	 */
	public void setLinha3Media(String linha3Media) {

		this.linha3Media = linha3Media;
	}

	/**
	 * @return the linha4Media
	 */
	public String getLinha4Media() {

		return linha4Media;
	}

	/**
	 * @param linha4Media
	 *            the linha4Media to set
	 */
	public void setLinha4Media(String linha4Media) {

		this.linha4Media = linha4Media;
	}

	/**
	 * @return the linha5Media
	 */
	public String getLinha5Media() {

		return linha5Media;
	}

	/**
	 * @param linha5Media
	 *            the linha5Media to set
	 */
	public void setLinha5Media(String linha5Media) {

		this.linha5Media = linha5Media;
	}

	/**
	 * @return the linha6Media
	 */
	public String getLinha6Media() {

		return linha6Media;
	}

	/**
	 * @param linha6Media
	 *            the linha6Media to set
	 */
	public void setLinha6Media(String linha6Media) {

		this.linha6Media = linha6Media;
	}

	/**
	 * @return the linha1Medidor
	 */
	public String getLinha1Medidor() {

		return linha1Medidor;
	}

	/**
	 * @param linha1Medidor
	 *            the linha1Medidor to set
	 */
	public void setLinha1Medidor(String linha1Medidor) {

		this.linha1Medidor = linha1Medidor;
	}

	/**
	 * @return the linha2Medidor
	 */
	public String getLinha2Medidor() {

		return linha2Medidor;
	}

	/**
	 * @param linha2Medidor
	 *            the linha2Medidor to set
	 */
	public void setLinha2Medidor(String linha2Medidor) {

		this.linha2Medidor = linha2Medidor;
	}

	/**
	 * @return the linha3Medidor
	 */
	public String getLinha3Medidor() {

		return linha3Medidor;
	}

	/**
	 * @param linha3Medidor
	 *            the linha3Medidor to set
	 */
	public void setLinha3Medidor(String linha3Medidor) {

		this.linha3Medidor = linha3Medidor;
	}

	/**
	 * @return the linha4Medidor
	 */
	public String getLinha4Medidor() {

		return linha4Medidor;
	}

	/**
	 * @param linha4Medidor
	 *            the linha4Medidor to set
	 */
	public void setLinha4Medidor(String linha4Medidor) {

		this.linha4Medidor = linha4Medidor;
	}

	/**
	 * @return the linha5Medidor
	 */
	public String getLinha5Medidor() {

		return linha5Medidor;
	}

	/**
	 * @param linha5Medidor
	 *            the linha5Medidor to set
	 */
	public void setLinha5Medidor(String linha5Medidor) {

		this.linha5Medidor = linha5Medidor;
	}

	/**
	 * @return the linha6Medidor
	 */
	public String getLinha6Medidor() {

		return linha6Medidor;
	}

	/**
	 * @param linha6Medidor
	 *            the linha6Medidor to set
	 */
	public void setLinha6Medidor(String linha6Medidor) {

		this.linha6Medidor = linha6Medidor;
	}

	/**
	 * @return the linha1LeituraInformada
	 */
	public String getLinha1LeituraInformada() {

		return linha1LeituraInformada;
	}

	/**
	 * @param linha1LeituraInformada
	 *            the linha1LeituraInformada to
	 *            set
	 */
	public void setLinha1LeituraInformada(String linha1LeituraInformada) {

		this.linha1LeituraInformada = linha1LeituraInformada;
	}

	/**
	 * @return the linha2LeituraInformada
	 */
	public String getLinha2LeituraInformada() {

		return linha2LeituraInformada;
	}

	/**
	 * @param linha2LeituraInformada
	 *            the linha2LeituraInformada to
	 *            set
	 */
	public void setLinha2LeituraInformada(String linha2LeituraInformada) {

		this.linha2LeituraInformada = linha2LeituraInformada;
	}

	/**
	 * @return the linha3LeituraInformada
	 */
	public String getLinha3LeituraInformada() {

		return linha3LeituraInformada;
	}

	/**
	 * @param linha3LeituraInformada
	 *            the linha3LeituraInformada to
	 *            set
	 */
	public void setLinha3LeituraInformada(String linha3LeituraInformada) {

		this.linha3LeituraInformada = linha3LeituraInformada;
	}

	/**
	 * @return the linha4LeituraInformada
	 */
	public String getLinha4LeituraInformada() {

		return linha4LeituraInformada;
	}

	/**
	 * @param linha4LeituraInformada
	 *            the linha4LeituraInformada to
	 *            set
	 */
	public void setLinha4LeituraInformada(String linha4LeituraInformada) {

		this.linha4LeituraInformada = linha4LeituraInformada;
	}

	/**
	 * @return the linha5LeituraInformada
	 */
	public String getLinha5LeituraInformada() {

		return linha5LeituraInformada;
	}

	/**
	 * @param linha5LeituraInformada
	 *            the linha5LeituraInformada to
	 *            set
	 */
	public void setLinha5LeituraInformada(String linha5LeituraInformada) {

		this.linha5LeituraInformada = linha5LeituraInformada;
	}

	/**
	 * @return the linha6LeituraInformada
	 */
	public String getLinha6LeituraInformada() {

		return linha6LeituraInformada;
	}

	/**
	 * @param linha6LeituraInformada
	 *            the linha6LeituraInformada to
	 *            set
	 */
	public void setLinha6LeituraInformada(String linha6LeituraInformada) {

		this.linha6LeituraInformada = linha6LeituraInformada;
	}

	/**
	 * @return the linha1ptz
	 */
	public String getLinha1ptz() {

		return linha1ptz;
	}

	/**
	 * @param linha1ptz
	 *            the linha1ptz to set
	 */
	public void setLinha1ptz(String linha1ptz) {

		this.linha1ptz = linha1ptz;
	}

	/**
	 * @return the linha2ptz
	 */
	public String getLinha2ptz() {

		return linha2ptz;
	}

	/**
	 * @param linha2ptz
	 *            the linha2ptz to set
	 */
	public void setLinha2ptz(String linha2ptz) {

		this.linha2ptz = linha2ptz;
	}

	/**
	 * @return the linha3ptz
	 */
	public String getLinha3ptz() {

		return linha3ptz;
	}

	/**
	 * @param linha3ptz
	 *            the linha3ptz to set
	 */
	public void setLinha3ptz(String linha3ptz) {

		this.linha3ptz = linha3ptz;
	}

	/**
	 * @return the linha4ptz
	 */
	public String getLinha4ptz() {

		return linha4ptz;
	}

	/**
	 * @param linha4ptz
	 *            the linha4ptz to set
	 */
	public void setLinha4ptz(String linha4ptz) {

		this.linha4ptz = linha4ptz;
	}

	/**
	 * @return the linha5ptz
	 */
	public String getLinha5ptz() {

		return linha5ptz;
	}

	/**
	 * @param linha5ptz
	 *            the linha5ptz to set
	 */
	public void setLinha5ptz(String linha5ptz) {

		this.linha5ptz = linha5ptz;
	}

	/**
	 * @return the linha6ptz
	 */
	public String getLinha6ptz() {

		return linha6ptz;
	}

	/**
	 * @param linha6ptz
	 *            the linha6ptz to set
	 */
	public void setLinha6ptz(String linha6ptz) {

		this.linha6ptz = linha6ptz;
	}

	/**
	 * @return the linha1pcs
	 */
	public String getLinha1pcs() {

		return linha1pcs;
	}

	/**
	 * @param linha1pcs
	 *            the linha1pcs to set
	 */
	public void setLinha1pcs(String linha1pcs) {

		this.linha1pcs = linha1pcs;
	}

	/**
	 * @return the linha2pcs
	 */
	public String getLinha2pcs() {

		return linha2pcs;
	}

	/**
	 * @param linha2pcs
	 *            the linha2pcs to set
	 */
	public void setLinha2pcs(String linha2pcs) {

		this.linha2pcs = linha2pcs;
	}

	/**
	 * @return the linha3pcs
	 */
	public String getLinha3pcs() {

		return linha3pcs;
	}

	/**
	 * @param linha3pcs
	 *            the linha3pcs to set
	 */
	public void setLinha3pcs(String linha3pcs) {

		this.linha3pcs = linha3pcs;
	}

	/**
	 * @return the linha4pcs
	 */
	public String getLinha4pcs() {

		return linha4pcs;
	}

	/**
	 * @param linha4pcs
	 *            the linha4pcs to set
	 */
	public void setLinha4pcs(String linha4pcs) {

		this.linha4pcs = linha4pcs;
	}

	/**
	 * @return the linha5pcs
	 */
	public String getLinha5pcs() {

		return linha5pcs;
	}

	/**
	 * @param linha5pcs
	 *            the linha5pcs to set
	 */
	public void setLinha5pcs(String linha5pcs) {

		this.linha5pcs = linha5pcs;
	}

	/**
	 * @return the linha6pcs
	 */
	public String getLinha6pcs() {

		return linha6pcs;
	}

	/**
	 * @param linha6pcs
	 *            the linha6pcs to set
	 */
	public void setLinha6pcs(String linha6pcs) {

		this.linha6pcs = linha6pcs;
	}

	/**
	 * Preencher ultimas medicoes.
	 * 
	 * @param listaUltimasMedicoes
	 *            the lista ultimas medicoes
	 */
	public void preencherUltimasMedicoes(Collection<SubReportAnaliseConsumoVO> listaUltimasMedicoes) {

		int i = 0;

		for (SubReportAnaliseConsumoVO subReportAnaliseConsumoVO : listaUltimasMedicoes) {

			if(i == 0) {
				linha1Referencia = subReportAnaliseConsumoVO.getReferencia();
				linha1Leitura = subReportAnaliseConsumoVO.getLeituraMedidaInformada();
				linha1Medido = subReportAnaliseConsumoVO.getFatorCorrecao();
				linha1Media = subReportAnaliseConsumoVO.getConsumoAtualApurado();
				linha1Faturado = subReportAnaliseConsumoVO.getConsumoMedio();
				linha1CDL = subReportAnaliseConsumoVO.getAnormalidadeLeituraIdentificada();
				linha1CDLN = subReportAnaliseConsumoVO.getAnormalidadeConsumoIdentificada();
				linha1Medidor = subReportAnaliseConsumoVO.getMedidor();
				linha1LeituraInformada = subReportAnaliseConsumoVO.getLeituraInformada();
				linha1ptz = subReportAnaliseConsumoVO.getFatorPTZ();
				linha1pcs = subReportAnaliseConsumoVO.getFatorPCS();
			} else if(i == 1) {
				linha2Referencia = subReportAnaliseConsumoVO.getReferencia();
				linha2Leitura = subReportAnaliseConsumoVO.getLeituraMedidaInformada();
				linha2Medido = subReportAnaliseConsumoVO.getFatorCorrecao();
				linha2Media = subReportAnaliseConsumoVO.getConsumoAtualApurado();
				linha2Faturado = subReportAnaliseConsumoVO.getConsumoMedio();
				linha2CDL = subReportAnaliseConsumoVO.getAnormalidadeLeituraIdentificada();
				linha2CDLN = subReportAnaliseConsumoVO.getAnormalidadeConsumoIdentificada();
				linha2Medidor = subReportAnaliseConsumoVO.getMedidor();
				linha2LeituraInformada = subReportAnaliseConsumoVO.getLeituraInformada();
				linha2ptz = subReportAnaliseConsumoVO.getFatorPTZ();
				linha2pcs = subReportAnaliseConsumoVO.getFatorPCS();
			} else if(i == 2) {
				linha3Referencia = subReportAnaliseConsumoVO.getReferencia();
				linha3Leitura = subReportAnaliseConsumoVO.getLeituraMedidaInformada();
				linha3Medido = subReportAnaliseConsumoVO.getFatorCorrecao();
				linha3Media = subReportAnaliseConsumoVO.getConsumoAtualApurado();
				linha3Faturado = subReportAnaliseConsumoVO.getConsumoMedio();
				linha3CDL = subReportAnaliseConsumoVO.getAnormalidadeLeituraIdentificada();
				linha3CDLN = subReportAnaliseConsumoVO.getAnormalidadeConsumoIdentificada();
				linha3Medidor = subReportAnaliseConsumoVO.getMedidor();
				linha3LeituraInformada = subReportAnaliseConsumoVO.getLeituraInformada();
				linha3ptz = subReportAnaliseConsumoVO.getFatorPTZ();
				linha3pcs = subReportAnaliseConsumoVO.getFatorPCS();
			} else if(i == 3) {
				linha4Referencia = subReportAnaliseConsumoVO.getReferencia();
				linha4Leitura = subReportAnaliseConsumoVO.getLeituraMedidaInformada();
				linha4Medido = subReportAnaliseConsumoVO.getFatorCorrecao();
				linha4Media = subReportAnaliseConsumoVO.getConsumoAtualApurado();
				linha4Faturado = subReportAnaliseConsumoVO.getConsumoMedio();
				linha4CDL = subReportAnaliseConsumoVO.getAnormalidadeLeituraIdentificada();
				linha4CDLN = subReportAnaliseConsumoVO.getAnormalidadeConsumoIdentificada();
				linha4Medidor = subReportAnaliseConsumoVO.getMedidor();
				linha4LeituraInformada = subReportAnaliseConsumoVO.getLeituraInformada();
				linha4ptz = subReportAnaliseConsumoVO.getFatorPTZ();
				linha4pcs = subReportAnaliseConsumoVO.getFatorPCS();
			} else if(i == 4) {
				linha5Referencia = subReportAnaliseConsumoVO.getReferencia();
				linha5Leitura = subReportAnaliseConsumoVO.getLeituraMedidaInformada();
				linha5Medido = subReportAnaliseConsumoVO.getFatorCorrecao();
				linha5Media = subReportAnaliseConsumoVO.getConsumoAtualApurado();
				linha5Faturado = subReportAnaliseConsumoVO.getConsumoMedio();
				linha5CDL = subReportAnaliseConsumoVO.getAnormalidadeLeituraIdentificada();
				linha5CDLN = subReportAnaliseConsumoVO.getAnormalidadeConsumoIdentificada();
				linha5Medidor = subReportAnaliseConsumoVO.getMedidor();
				linha5LeituraInformada = subReportAnaliseConsumoVO.getLeituraInformada();
				linha5ptz = subReportAnaliseConsumoVO.getFatorPTZ();
				linha5pcs = subReportAnaliseConsumoVO.getFatorPCS();
			} else if(i == 5) {
				linha6Referencia = subReportAnaliseConsumoVO.getReferencia();
				linha6Leitura = subReportAnaliseConsumoVO.getLeituraMedidaInformada();
				linha6Medido = subReportAnaliseConsumoVO.getFatorCorrecao();
				linha6Media = subReportAnaliseConsumoVO.getConsumoAtualApurado();
				linha6Faturado = subReportAnaliseConsumoVO.getConsumoMedio();
				linha6CDL = subReportAnaliseConsumoVO.getAnormalidadeLeituraIdentificada();
				linha6CDLN = subReportAnaliseConsumoVO.getAnormalidadeConsumoIdentificada();
				linha6Medidor = subReportAnaliseConsumoVO.getMedidor();
				linha6LeituraInformada = subReportAnaliseConsumoVO.getLeituraInformada();
				linha6ptz = subReportAnaliseConsumoVO.getFatorPTZ();
				linha6pcs = subReportAnaliseConsumoVO.getFatorPCS();
			}

			i++;
		}
	}
}
