/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import br.com.ggas.medicao.consumo.ModalidadeDataQuantidade;

/**
 * Classe Modalidade Data Quantidade.
 * 
 *
 */
public class ModalidadeDataQuantidadeImpl implements ModalidadeDataQuantidade {

	private static final long serialVersionUID = -5873711310551919023L;

	private Date data;

	private Boolean contratoEmPeriodoTeste;

	private BigDecimal volumeQDP;

	private BigDecimal volumeQDC;

	private BigDecimal volumeQDR;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.impl. ModalidadeDataQuantidade#getData()
	 */
	@Override
	public Date getData() {
		Date dataTmp = null;
		if (this.data != null) {
			dataTmp = (Date) data.clone();
		}
		return dataTmp;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ModalidadeDataQuantidade#getVolumeQDP()
	 */
	@Override
	public BigDecimal getVolumeQDP() {

		return volumeQDP;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ModalidadeDataQuantidade
	 * #setVolumeQDP(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDP(BigDecimal volumeQDP) {

		this.volumeQDP = volumeQDP;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ModalidadeDataQuantidade#getVolumeQDC()
	 */
	@Override
	public BigDecimal getVolumeQDC() {

		return volumeQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ModalidadeDataQuantidade
	 * #setVolumeQDC(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDC(BigDecimal volumeQDC) {

		this.volumeQDC = volumeQDC;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.impl. ModalidadeDataQuantidade
	 * #setData(java.util.Date)
	 */
	@Override
	public void setData(Date data) {
		if (data != null) {
			this.data = (Date) data.clone();
		} else {
			this.data = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ModalidadeDataQuantidade
	 * #getContratoEmPeriodoTeste()
	 */
	@Override
	public Boolean getContratoEmPeriodoTeste() {

		return contratoEmPeriodoTeste;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ModalidadeDataQuantidade
	 * #setContratoEmPeriodoTeste
	 * (java.lang.Boolean)
	 */
	@Override
	public void setContratoEmPeriodoTeste(Boolean contratoEmPeriodoTeste) {

		this.contratoEmPeriodoTeste = contratoEmPeriodoTeste;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ModalidadeDataQuantidade
	 * #getQtdDiariaRetiradaQDR()
	 */
	@Override
	public BigDecimal getVolumeQDR() {

		return volumeQDR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ModalidadeDataQuantidade
	 * #setQtdDiariaRetiradaQDR
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDR(BigDecimal volumeQDR) {

		this.volumeQDR = volumeQDR;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if(obj == null){ 
			return false;
		}
		if(this == obj){ 
			return true;
		}
		if(!(obj instanceof ModalidadeDataQuantidadeImpl)) {
			return false;
		}
		if(this.data != null) {
			ModalidadeDataQuantidadeImpl that = (ModalidadeDataQuantidadeImpl) obj;
			return new EqualsBuilder().append(this.data, that.data).isEquals();
		} else {
			return super.equals(obj);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		if(this.data != null) {
			return new HashCodeBuilder(3, 5).append(this.data).toHashCode();
		} else {
			return super.hashCode();
		}

	}

}
