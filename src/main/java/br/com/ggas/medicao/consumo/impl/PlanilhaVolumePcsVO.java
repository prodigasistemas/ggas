package br.com.ggas.medicao.consumo.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe Planilha Volume Pcs.
 * 
 *
 */
public class PlanilhaVolumePcsVO implements Serializable {
	private static final long serialVersionUID = 8889163747116920034L;

	private String mes;
	private String ano;

	private Map<Integer, PlanilhaVolumePcsLinhaVO> linha;

	/**
	 * Construtor Planilha Volume Pcs.
	 * 
	 */
	public PlanilhaVolumePcsVO() {
		super();
		this.linha = new HashMap<Integer, PlanilhaVolumePcsLinhaVO>();
	}
	
	/**
	 * Construtor Planilha Volume Pcs.
	 * @param mes
	 * 			the mes
	 * @param ano
	 * 			the ano
	 * 
	 */
	public PlanilhaVolumePcsVO(String mes, String ano) {
		super();
		this.mes = mes;
		this.ano = ano;
		this.linha = new HashMap<Integer, PlanilhaVolumePcsLinhaVO>();
	}
	
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAno() {
		return ano;
	}
	public void setAno(String ano) {
		this.ano = ano;
	}


	public Map<Integer, PlanilhaVolumePcsLinhaVO> getLinha() {
		return linha;
	}


	public void setLinha(Map<Integer, PlanilhaVolumePcsLinhaVO> linha) {
		this.linha = linha;
	}
}
