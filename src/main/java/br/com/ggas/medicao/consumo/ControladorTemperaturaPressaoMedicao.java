/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados
 * ao ControladorTemperaturaPressaoMedicao
 *
 */
public interface ControladorTemperaturaPressaoMedicao extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_TEMPERATURA_PRESSAO_MEDICAO = "controladorTemperaturaPressaoMedicao";

	String ERRO_NEGOCIO_DATAS_INFERIORES_DATA_CORRENTE = "ERRO_NEGOCIO_DATAS_INFERIORES_DATA_CORRENTE";

	String ERRO_NEGOCIO_DATAS_ANTERIORES_AO_ULTIMO_FATURAMENTO = "ERRO_NEGOCIO_DATAS_ANTERIORES_AO_ULTIMO_FATURAMENTO";

	String ERRO_NEGOCIO_DATAS_DUPLICADAS = "ERRO_NEGOCIO_DATAS_DUPLICADAS";

	/**
	 * Obter temperatura medicao vigente.
	 * 
	 * @param municipio
	 *            the municipio
	 * @param localidade
	 *            the localidade
	 * @param dataReferencia
	 *            the data referencia
	 * @return the temperatura pressao medicao
	 */
	TemperaturaPressaoMedicao obterTemperaturaMedicaoVigente(Municipio municipio, Localidade localidade, Date dataReferencia);

	/**
	 * Método responsável por listar as medições
	 * de temperatura e pressão
	 * disponíveis para manutenção.
	 * 
	 * @param chaveLocalidade
	 *            Uma chave primária de
	 *            localidade.
	 * @param chaveMicrorregiao
	 *            Uma chave primária de
	 *            microrregiao.
	 * @param chaveMunicipio
	 *            Uma chave primária de município.
	 * @param chaveRegiao
	 *            Uma chave primária de região.
	 * @param dataInicial
	 *            Uma data.
	 * @param unidadeFederacao
	 *            the unidade federacao
	 * @return Coleção de medições de temperatura
	 *         e pressão.
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	Collection<TemperaturaPressaoMedicao> listarTemperaturaPressaoMedicaoManutencao(Long chaveLocalidade, Long chaveMicrorregiao,
					Long chaveMunicipio, Long chaveRegiao, Date dataInicial, Long unidadeFederacao) throws NegocioException;

	/**
	 * Método responsável por validar a pesquisa
	 * para manutenção de medições de
	 * temperatura e pressão.
	 * 
	 * @param codigoTipoAbrangencia
	 *            O código do tipo de abrangência.
	 * @param chaveAbrangencia
	 *            A chave primária da abrangência.
	 * @param dataInicial
	 *            Uma data inicial.
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	void validarPesquisaManutencaoTemperaturaPressaoMedicao(String codigoTipoAbrangencia, Long chaveAbrangencia, Date dataInicial)
					throws NegocioException;

	/**
	 * Método responsável por atualizar os dados
	 * de medição de temperatura e
	 * pressão.
	 * 
	 * @param listaTemperaturaPressaoMedicao
	 *            Coleção de dados de medição de
	 *            temperatura e pressão;
	 * @return boolean indicando a necessidade de
	 *         processar consistencia de leituras
	 *         para
	 *         que os dados informados sejam
	 *         considerador durante a apuração de
	 *         volumes
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro de
	 *             concorrência.
	 */
	boolean atualizarListaTemperaturaPressaoMedicao(Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoMedicao)
					throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por obter uma medição de
	 * temperatura e pressão por uma
	 * data informada.
	 * 
	 * @param chaveLocalidade
	 *            Uma chave primária de
	 *            localidade.
	 * @param chaveMicrorregiao
	 *            Uma chave primária de
	 *            microrregiao.
	 * @param chaveMunicipio
	 *            Uma chave primária de município.
	 * @param chaveRegiao
	 *            Uma chave primária de região.
	 * @param dataReferencia
	 *            the data referencia
	 * @param chaveUnidadeFederacao
	 *            Uma chave primária de
	 *            unidadeFederacao.
	 * @return A medição de temperatura e pressão.
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	TemperaturaPressaoMedicao obterTemperaturaPressaoMedicaoVigentePorData(Long chaveLocalidade, Long chaveMicrorregiao,
					Long chaveMunicipio, Long chaveRegiao, Date dataReferencia, Long chaveUnidadeFederacao) throws NegocioException;

	/**
	 * Método responsável por retornar as medições de temperatura e pressão por município e localidade.
	 * 
	 * @param municipio          Um município.
	 * @param localidade         Uma localidade.
	 * @return A medição de temperatura e pressão.
	 * @throws NegocioException Caso ocorra alguma violação nas regras de negócio.
	 */
	List<TemperaturaPressaoMedicao>  obterTemperaturaPressaoMedicaoVigente(Municipio municipio, Localidade localidade) throws NegocioException;

	/**
	 * Método responsável por verificar se a
	 * alteração dos dados de medição de
	 * temperatura medição pressão é permitida.
	 * 
	 * @param temperaturaPressaoMedicao
	 *            the temperatura pressao medicao
	 * @return true caso permitida a alteração.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean permitirAtualizacaoTemperaturaPressaoMedicao(TemperaturaPressaoMedicao temperaturaPressaoMedicao) throws NegocioException;

	/**
	 * Consultar temperatura pressao medicao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 */
	Collection<TemperaturaPressaoMedicao> consultarTemperaturaPressaoMedicao(Map<String, Object> filtro);

	/**
	 * Obter temperatura medicao vigente.
	 * 
	 * @param municipio {@link Municipio}
	 * @return medição de temperatura e pressão - {@link TemperaturaPressaoMedicao}
	 */
	Collection<TemperaturaPressaoMedicao> obterTemperaturaMedicaoPorMunicipio(Municipio municipio);

}
