/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.sql.Date;

import br.com.ggas.medicao.consumo.ConsumoDistribuidoData;

/**
 * Classe Consumo Distribuido Data.
 * 
 * 
 * @author vmelo
 * 
 */
public class ConsumoDistribuidoDataImpl implements ConsumoDistribuidoData {

	private static final long serialVersionUID = -8978354846426070975L;

	private Date data;

	private BigDecimal volumeQDC;

	private BigDecimal volumeQDP;

	private BigDecimal volumeQF;

	private BigDecimal volumeQDRMaior;

	private BigDecimal volumeQNRpp;

	private BigDecimal volumeQNRfm;

	private BigDecimal volumeQNRff;

	private BigDecimal volumeQNRPNPcdl;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.impl. ConsumoDistribuidoData#getData()
	 */
	@Override
	public Date getData() {
		Date dataTmp = null;
		if (this.data != null) {
			dataTmp = (Date) data.clone();
		}
		return dataTmp;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData
	 * #setData(java.sql.Date)
	 */
	@Override
	public void setData(Date data) {

		if(data != null) {
			this.data = (Date) data.clone();
		} else {
			this.data = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData#getVolumeQDC()
	 */
	@Override
	public BigDecimal getVolumeQDC() {

		return volumeQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData
	 * #setVolumeQDC(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDC(BigDecimal volumeQDC) {

		this.volumeQDC = volumeQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData#getVolumeQDP()
	 */
	@Override
	public BigDecimal getVolumeQDP() {

		return volumeQDP;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData
	 * #setVolumeQDP(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDP(BigDecimal volumeQDP) {

		this.volumeQDP = volumeQDP;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData#getVolumeQF()
	 */
	@Override
	public BigDecimal getVolumeQF() {

		return volumeQF;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData
	 * #setVolumeQF(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQF(BigDecimal volumeQF) {

		this.volumeQF = volumeQF;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData#getVolumeQDRMaior()
	 */
	@Override
	public BigDecimal getVolumeQDRMaior() {

		return volumeQDRMaior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData
	 * #setVolumeQDRMaior(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDRMaior(BigDecimal volumeQDRMaior) {

		this.volumeQDRMaior = volumeQDRMaior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData#getVolumeQNRpp()
	 */
	@Override
	public BigDecimal getVolumeQNRpp() {

		return volumeQNRpp;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData
	 * #setVolumeQNRpp(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQNRpp(BigDecimal volumeQNRpp) {

		this.volumeQNRpp = volumeQNRpp;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData#getVolumeQNRfm()
	 */
	@Override
	public BigDecimal getVolumeQNRfm() {

		return volumeQNRfm;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData
	 * #setVolumeQNRfm(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQNRfm(BigDecimal volumeQNRfm) {

		this.volumeQNRfm = volumeQNRfm;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData#getVolumeQNRff()
	 */
	@Override
	public BigDecimal getVolumeQNRff() {

		return volumeQNRff;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData
	 * #setVolumeQNRff(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQNRff(BigDecimal volumeQNRff) {

		this.volumeQNRff = volumeQNRff;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData#getVolumeQNRPNPcdl()
	 */
	@Override
	public BigDecimal getVolumeQNRPNPcdl() {

		return volumeQNRPNPcdl;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoData
	 * #setVolumeQNRPNPcdl(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQNRPNPcdl(BigDecimal volumeQNRPNPcdl) {

		this.volumeQNRPNPcdl = volumeQNRPNPcdl;
	}

}
