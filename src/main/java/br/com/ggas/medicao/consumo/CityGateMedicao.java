/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo;

import java.util.Date;

import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados
 * a entidade CityGateMedicao
 *
 */
public interface CityGateMedicao extends EntidadeNegocio {

	String BEAN_ID_CITY_GATE_MEDICAO = "cityGateMedicao";

	String CITY_GATE_MEDICOES = "CITY_GATE_MEDICOES_ROTULO";
	
	String CITY_GATE = "CITY_GATE_MEDICAO_CITY_GATE";

	String DATA_MEDICAO = "CITY_GATE_MEDICAO_DATA_MEDICAO";

	/**
	 * @return the cityGate
	 */
	CityGate getCityGate();

	/**
	 * @param cityGate
	 *            the cityGate to set
	 */
	void setCityGate(CityGate cityGate);

	/**
	 * @return the dataMedicao
	 */
	Date getDataMedicao();

	/**
	 * @param dataMedicao
	 *            the dataMedicao to set
	 */
	void setDataMedicao(Date dataMedicao);

	/**
	 * @return the medidaPCSFixo
	 */
	Integer getMedidaPCSFixo();

	/**
	 * @param medidaPCSFixo
	 *            the medidaPCSFixo to set
	 */
	void setMedidaPCSFixo(Integer medidaPCSFixo);

	/**
	 * @return the medidaPCSSupridora
	 */
	Integer getMedidaPCSSupridora();

	/**
	 * @param medidaPCSSupridora
	 *            the medidaPCSSupridora to set
	 */
	void setMedidaPCSSupridora(Integer medidaPCSSupridora);

	/**
	 * @return the medidaPCSDistribuidora
	 */
	Integer getMedidaPCSDistribuidora();

	/**
	 * @param medidaPCSDistribuidora
	 *            the medidaPCSDistribuidora to
	 *            set
	 */
	void setMedidaPCSDistribuidora(Integer medidaPCSDistribuidora);

	/**
	 * @return the medidaVolumeSupridora
	 */
	Integer getMedidaVolumeSupridora();

	/**
	 * @param medidaVolumeSupridora
	 *            the medidaVolumeSupridora to set
	 */
	void setMedidaVolumeSupridora(Integer medidaVolumeSupridora);

	/**
	 * @return the medidaVolumeDistribuidora
	 */
	Integer getMedidaVolumeDistribuidora();

	/**
	 * @param medidaVolumeDistribuidora
	 *            the medidaVolumeDistribuidora to
	 *            set
	 */
	void setMedidaVolumeDistribuidora(Integer medidaVolumeDistribuidora);

	/**
	 * @param indicadorAlteracao
	 */
	void setIndicadorAlteracao(boolean indicadorAlteracao);

	/**
	 * @return
	 */
	boolean isIndicadorAlteracao();
}
