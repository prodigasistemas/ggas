package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.util.Collection;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.consumo.ColetorConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.util.Fachada;

/**
 * Classe Coletor Consumo Puro.
 * 
 *
 */
public class ColetorConsumoPuro implements ColetorConsumo {

	/** The Constant CONSUMO. */
	private static final String CONSUMO = "consumo";
	
	@Override
	public BigDecimal coletarPorConsulta(int anoMesFaturamento, int ciclo,
			Collection<PontoConsumo> listaPontoConsumoAgrupados) throws GGASException {
		return Fachada.getInstancia().obterConsumoPontoConsumoPorReferencia(anoMesFaturamento, ciclo,
				listaPontoConsumoAgrupados);
	}

	@Override
	public BigDecimal coletarPorPropriedade(HistoricoConsumo historico) {
		return historico.getConsumo();
	}

	@Override
	public String getChaveMapaDados() {
		return CONSUMO;
	}

}
