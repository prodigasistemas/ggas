/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.geografico.Microrregiao;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.Regiao;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.vazaocorretor.Unidade;

/**
 * Interface que representa a temperatura e
 * pressão para uma local que são
 * utilizadas em medição.
 */
public interface TemperaturaPressaoMedicao extends EntidadeNegocio {

	String BEAN_ID_TEMPERATURA_PRESSAO_MEDICAO = "temperaturaPressaoMedicao";

	String TEMPERATURA_PRESSAO_MEDICOES = "TEMPERATURA_PRESSAO_MEDICOES_ROTULO";

	String TEMPERATURA_PRESSAO_MEDICAO = "TEMPERATURA_PRESSAO_MEDICAO_ROTULO";

	String TEMPERATURA = "TEMPERATURA_PRESSAO_MEDICAO_TEMPERATURA";

	String UNIDADE_TEMPERATURA = "TEMPERATURA_PRESSAO_MEDICAO_UNIDADE_TEMPERATURA";

	String PRESSAO_ATMOSFERICA = "TEMPERATURA_PRESSAO_MEDICAO_PRESSAO_ATMOSFERICA";

	String UNIDADE_PRESSAO_ATMOSFERICA = "TEMPERATURA_PRESSAO_MEDICAO_UNIDADE_PRESSAO_ATMOSFERICA";

	String DATA_INICIO = "TEMPERATURA_PRESSAO_MEDICAO_DATA_INICIO";

	String AREA_ABRANGENCIA = "TEMPERATURA_PRESSAO_MEDICAO_AREA_ABRANGENCIA";

	/**
	 * @return the regiao
	 */
	Regiao getRegiao();

	/**
	 * @param regiao
	 *            the regiao to set
	 */
	void setRegiao(Regiao regiao);

	/**
	 * @return the microrregiao
	 */
	Microrregiao getMicrorregiao();

	/**
	 * @param microrregiao
	 *            the microrregiao to set
	 */
	void setMicrorregiao(Microrregiao microrregiao);

	/**
	 * @return the municipio
	 */
	Municipio getMunicipio();

	/**
	 * @param municipio
	 *            the municipio to set
	 */
	void setMunicipio(Municipio municipio);

	/**
	 * @return the localidade
	 */
	Localidade getLocalidade();

	/**
	 * @param localidade
	 *            the localidade to set
	 */
	void setLocalidade(Localidade localidade);

	/**
	 * @return the pressaoAtmosferica
	 */
	BigDecimal getPressaoAtmosferica();

	/**
	 * @param pressaoAtmosferica
	 *            the pressaoAtmosferica to set
	 */
	void setPressaoAtmosferica(BigDecimal pressaoAtmosferica);

	/**
	 * @return the unidadePressaoAtmosferica
	 */
	Unidade getUnidadePressaoAtmosferica();

	/**
	 * @param unidadePressaoAtmosferica
	 *            the unidadePressaoAtmosferica to
	 *            set
	 */
	void setUnidadePressaoAtmosferica(Unidade unidadePressaoAtmosferica);

	/**
	 * @return the temperatura
	 */
	BigDecimal getTemperatura();

	/**
	 * @param temperatura
	 *            the temperatura to set
	 */
	void setTemperatura(BigDecimal temperatura);

	/**
	 * @return the unidadeTemperatura
	 */
	Unidade getUnidadeTemperatura();

	/**
	 * @param unidadeTemperatura
	 *            the unidadeTemperatura to set
	 */
	void setUnidadeTemperatura(Unidade unidadeTemperatura);

	/**
	 * @return the dataInicio
	 */
	Date getDataInicio();

	/**
	 * @param dataInicio
	 *            the dataInicio to set
	 */
	void setDataInicio(Date dataInicio);

	/**
	 * @param unidadeFederacao
	 *            the unidadeFederacao to set
	 */
	void setUnidadeFederacao(UnidadeFederacao unidadeFederacao);

	/**
	 * @return the unidadeFederacao
	 */
	UnidadeFederacao getUnidadeFederacao();

}
