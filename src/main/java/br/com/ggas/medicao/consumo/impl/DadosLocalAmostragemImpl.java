/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.geral.EntidadeConteudo;

class DadosLocalAmostragemImpl implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -8168396018775946066L;

	private Date data;

	private Integer medidaPCS;

	private CityGate cityGate;

	private EntidadeConteudo localAmostragem;

	private Integer medidaVolume;

	private BigDecimal participacao;
	
	private static final int PRIME = 31;

	/**
	 * Instantiates a new dados local amostragem impl.
	 */
	public DadosLocalAmostragemImpl() {

	}

	/**
	 * Instantiates a new dados local amostragem impl.
	 * 
	 * @param medidaVolume
	 *            the medida volume
	 */
	public DadosLocalAmostragemImpl(Integer medidaVolume) {

		this.medidaVolume = medidaVolume;
	}

	/**
	 * Instantiates a new dados local amostragem impl.
	 * 
	 * @param data
	 *            the data
	 * @param medidaPCS
	 *            the medida pcs
	 */
	public DadosLocalAmostragemImpl(Date data, Integer medidaPCS) {

		this.data = data;
		this.medidaPCS = medidaPCS;
	}

	/**
	 * Instantiates a new dados local amostragem impl.
	 * 
	 * @param data
	 *            the data
	 * @param medidaPCS
	 *            the medida pcs
	 * @param cityGate
	 *            the city gate
	 */
	public DadosLocalAmostragemImpl(Date data, Integer medidaPCS, CityGate cityGate) {

		this.data = data;
		this.medidaPCS = medidaPCS;
		this.cityGate = cityGate;
	}

	/**
	 * Instantiates a new dados local amostragem impl.
	 * 
	 * @param data
	 *            the data
	 * @param medidaPCS
	 *            the medida pcs
	 * @param medidaVolume
	 *            the medida volume
	 * @param cityGate
	 *            the city gate
	 */
	public DadosLocalAmostragemImpl(Date data, Integer medidaPCS, Integer medidaVolume, CityGate cityGate) {

		this.data = data;
		this.medidaPCS = medidaPCS;
		this.medidaVolume = medidaVolume;
		this.cityGate = cityGate;
	}

	/**
	 * @return the data
	 */
	public Date getData() {

		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(Date data) {

		this.data = data;
	}

	/**
	 * @return the cityGate
	 */
	public CityGate getCityGate() {

		return cityGate;
	}

	/**
	 * @param cityGate
	 *            the cityGate to set
	 */
	public void setCityGate(CityGate cityGate) {

		this.cityGate = cityGate;
	}

	/**
	 * @return the localAmostragem
	 */
	public EntidadeConteudo getLocalAmostragem() {

		return localAmostragem;
	}

	/**
	 * @param localAmostragem
	 *            the localAmostragem to set
	 */
	public void setLocalAmostragem(EntidadeConteudo localAmostragem) {

		this.localAmostragem = localAmostragem;
	}

	/**
	 * @return the medidaVolume
	 */
	public Integer getMedidaVolume() {

		return medidaVolume;
	}

	/**
	 * @param medidaVolume
	 *            the medidaVolume to set
	 */
	public void setMedidaVolume(Integer medidaVolume) {

		this.medidaVolume = medidaVolume;
	}

	/**
	 * @return the medidaPCS
	 */
	public Integer getMedidaPCS() {

		return medidaPCS;
	}

	/**
	 * @param medidaPCS
	 *            the medidaPCS to set
	 */
	public void setMedidaPCS(Integer medidaPCS) {

		this.medidaPCS = medidaPCS;
	}

	/**
	 * @return the participacao
	 */
	public BigDecimal getParticipacao() {

		return participacao;
	}

	/**
	 * @param participacao
	 *            the participacao to set
	 */
	public void setParticipacao(BigDecimal participacao) {

		this.participacao = participacao;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = PRIME;
		int result = 1;
		
		if (cityGate == null) {
			result = prime * result + 0;
		} else {
			result = prime * result + cityGate.hashCode();
		}

		if (data == null) {
			result = prime * result + 0;
		} else {
			result = prime * result + data.hashCode();
		}
		
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		
		if(this == obj) {
			return true;
		}
		if(obj == null) {
			return false;
		}
		if(getClass() != obj.getClass()) {
			return false;
		}
		final DadosLocalAmostragemImpl other = (DadosLocalAmostragemImpl) obj;
		if(cityGate == null) {
			if(other.cityGate != null) {
				return false;
			}
		} else if(!cityGate.equals(other.cityGate)) {
			return false;
		}
		if(data == null) {
			if(other.data != null) {
				return false;
			}
		} else if(!data.equals(other.data)) {
			return false;
		}
		return true;
	}
}
