/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ImovelPCSZImpl representa uma ImovelPCSZImpl no sistema.
 *
 * @since 04/11/2009
 * 
 */

package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.consumo.ImovelPCSZ;

/**
 * Classe Imovel PCSZ.
 * 
 *
 */
public class ImovelPCSZImpl extends EntidadeNegocioImpl implements ImovelPCSZ {

	private static final long serialVersionUID = -1871399747073057532L;

	private Imovel imovel;

	private Date dataVigencia;

	private Integer medidaPCS;

	private BigDecimal fatorZ;

	private boolean indicadorAlteracao;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.ImovelPCSZ#
	 * getImovel()
	 */
	@Override
	public Imovel getImovel() {

		return imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.ImovelPCSZ#
	 * setImovel
	 * (br.com.ggas.cadastro.imovel.Imovel)
	 */
	@Override
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * PontoConsumoPCSZ#getDataVigencia()
	 */
	@Override
	public Date getDataVigencia() {
		Date data = null;
		if(this.dataVigencia != null) {
			data = (Date) dataVigencia.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.impl. PontoConsumoPCSZ
	 * #setDataVigencia(java.util.Date)
	 */
	@Override
	public void setDataVigencia(Date dataVigencia) {
		if (dataVigencia != null) {
			this.dataVigencia = (Date) dataVigencia.clone();
		} else {
			this.dataVigencia = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * PontoConsumoPCSZ#getMedidaPCS()
	 */
	@Override
	public Integer getMedidaPCS() {

		return medidaPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * PontoConsumoPCSZ
	 * #setMedidaPCS(java.lang.Integer)
	 */
	@Override
	public void setMedidaPCS(Integer medidaPCS) {

		this.medidaPCS = medidaPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * PontoConsumoPCSZ#getFatorZ()
	 */
	@Override
	public BigDecimal getFatorZ() {

		return fatorZ;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * PontoConsumoPCSZ
	 * #setFatorZ(java.math.BigDecimal)
	 */
	@Override
	public void setFatorZ(BigDecimal fatorZ) {

		this.fatorZ = fatorZ;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.ImovelPCSZ#
	 * setIndicadorAlteracao(java.lang.boolean)
	 */
	@Override
	public void setIndicadorAlteracao(boolean indicadorAlteracao) {

		this.indicadorAlteracao = indicadorAlteracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.ImovelPCSZ#
	 * isIndicadorAlteracao()
	 */
	@Override
	public boolean isIndicadorAlteracao() {

		return indicadorAlteracao;
	}

}
