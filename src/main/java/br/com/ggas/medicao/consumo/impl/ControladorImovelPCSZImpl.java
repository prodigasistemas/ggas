/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;
import org.joda.time.Days;

import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.ImovelPCSZ;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe responsável por implementar os métodos
 * de consulta e persistência
 * relativos a entidade ImovelPCSZ.
 */
class ControladorImovelPCSZImpl extends ControladorNegocioImpl implements ControladorImovelPCSZ {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(ImovelPCSZ.BEAN_ID_IMOVEL_PCS_Z);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(ImovelPCSZ.BEAN_ID_IMOVEL_PCS_Z);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ControladorImovelPCSZ
	 * #listarPCSZImovel(br.com
	 * .ggas.cadastro.imovel.Imovel,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public Collection<ImovelPCSZ> listarPCSZImovel(Imovel imovel, Integer ano, Integer mes) throws NegocioException {

		Collection<ImovelPCSZ> listaPCSZImovel = new ArrayList<ImovelPCSZ>();

		if((ano == null && mes != null) || (ano != null && mes == null)) {
			throw new NegocioException(ControladorImovelPCSZ.ERRO_NEGOCIO_ANO_MES_NAO_INFORMADOS, false);
		} else {
			if(ano != null) {
				listaPCSZImovel.addAll(listarPCSZImovelPorAnoMes(imovel, ano, mes));
			} else {
				listaPCSZImovel.addAll(listarPCSZImovelEmAberto(imovel));
			}
		}
		return listaPCSZImovel;
	}

	/**
	 * Listar pcsz imovel em aberto.
	 *
	 * @param imovel
	 *            the imovel
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<ImovelPCSZ> listarPCSZImovelEmAberto(Imovel imovel) {

		Collection<ImovelPCSZ> listaPCSZImovelEmAberto = new ArrayList<ImovelPCSZ>();

		// 1. Consulta maior data de movimento
		// para o Imóvel
		Date maiorDataPCS = null;
		Date maiorDataFatorZ = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select max(dataVigencia) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" and imovel.chavePrimaria = :idImovel ");
		hql.append(" and fatorZ is not null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idImovel", imovel.getChavePrimaria());

		maiorDataFatorZ = (Date) query.uniqueResult();

		if(maiorDataFatorZ == null) {
			query = null;

			hql = new StringBuilder();
			hql.append(" select max(dataVigencia) from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where habilitado = true ");
			hql.append(" and imovel.chavePrimaria = :idImovel ");
			hql.append(" and medidaPCS is not null ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong("idImovel", imovel.getChavePrimaria());

			maiorDataPCS = (Date) query.uniqueResult();
		}

		// 2.1 Caso não exista nenhum registro
		// para o imóvel informado, gerar a lista
		// apenas pra o mês corrente.
		if(maiorDataFatorZ == null && maiorDataPCS == null) {
			DateTime dataReferencia = new DateTime();
			dataReferencia = dataReferencia.withDayOfMonth(1);

			while(dataReferencia.getMonthOfYear() == new DateTime().getMonthOfYear() && dataReferencia.compareTo(new DateTime()) <= 0) {

				ImovelPCSZ imovelPCSZ = (ImovelPCSZ) criar();
				imovelPCSZ.setDataVigencia(dataReferencia.toDate());
				imovelPCSZ.setImovel(imovel);

				listaPCSZImovelEmAberto.add(imovelPCSZ);

				dataReferencia = dataReferencia.plusDays(1);
			}
		} else {

			// 2.2 Caso contrário, gerar a lista
			// até a data corrente
			hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where habilitado = true ");
			hql.append(" and imovel.chavePrimaria = :idImovel ");
			hql.append(" and dataVigencia >= :data ");
			hql.append(" order by dataVigencia ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong("idImovel", imovel.getChavePrimaria());

			// Se existe um registro cadastrado
			// cuja a medida da supridora não
			// esteja informada, considerá-lo
			if(maiorDataFatorZ != null) {
				Util.adicionarRestricaoDataSemHora(query, maiorDataFatorZ, "data", Boolean.FALSE);
			} else {
				Util.adicionarRestricaoDataSemHora(query, new DateTime(maiorDataPCS).toDate(), "data", Boolean.FALSE);
			}

			listaPCSZImovelEmAberto = query.list();

			// Verifica se o último registro é
			// menor do que a data corrente, para
			// completar a lista se for o caso
			if(listaPCSZImovelEmAberto != null && !listaPCSZImovelEmAberto.isEmpty()) {
				List<ImovelPCSZ> lista = new ArrayList<ImovelPCSZ>(listaPCSZImovelEmAberto);
				ImovelPCSZ ultimoImovelPCSZ = lista.get(lista.size() - 1);
				DateTime dataReferencia = new DateTime(ultimoImovelPCSZ.getDataVigencia());

				DateTime dataHoje = new DateTime();
				dataHoje = dataHoje.withHourOfDay(0);
				dataHoje = dataHoje.withMinuteOfHour(0);
				dataHoje = dataHoje.withSecondOfMinute(0);
				dataHoje = dataHoje.withMillisOfSecond(0);

				if(dataHoje.compareTo(dataReferencia) > 0) {
					dataReferencia = dataReferencia.plusDays(1);
					while(dataHoje.compareTo(dataReferencia) >= 0 && dataReferencia.compareTo(new DateTime()) <= 0) {
						ImovelPCSZ imovelPCSZ = (ImovelPCSZ) criar();
						imovelPCSZ.setDataVigencia(dataReferencia.toDate());
						imovelPCSZ.setImovel(imovel);

						listaPCSZImovelEmAberto.add(imovelPCSZ);

						dataReferencia = dataReferencia.plusDays(1);
					}
				}
			}
		}

		return listaPCSZImovelEmAberto;
	}

	/**
	 * Listar pcsz imovel por ano mes.
	 *
	 * @param imovel
	 *            the imovel
	 * @param ano
	 *            the ano
	 * @param mes
	 *            the mes
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	private Collection<ImovelPCSZ> listarPCSZImovelPorAnoMes(Imovel imovel, Integer ano, Integer mes) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" and imovel.chavePrimaria = :idImovel ");
		hql.append(" and year(dataVigencia) = :ano ");
		hql.append(" and month(dataVigencia) = :mes ");
		hql.append(" order by dataVigencia ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idImovel", imovel.getChavePrimaria());
		query.setInteger("ano", ano);
		query.setInteger("mes", mes);

		Collection<ImovelPCSZ> listaImovelPCSZ = query.list();

		// Verifica se o último registro é menor
		// do que a data corrente, para completar
		// a lista se for o caso
		DateTime dataReferencia = null;
		if(listaImovelPCSZ != null && !listaImovelPCSZ.isEmpty()) {
			List<ImovelPCSZ> lista = new ArrayList<ImovelPCSZ>(listaImovelPCSZ);
			ImovelPCSZ ultimoImovelPCSZ = lista.get(lista.size() - 1);
			dataReferencia = new DateTime(ultimoImovelPCSZ.getDataVigencia());
		}

		// Obtem a data limite como o ultimo dia
		// do mês corrente.
		DateTime dataLimite = new DateTime();
		dataLimite = dataLimite.withDayOfMonth(1);
		dataLimite = dataLimite.withYear(ano);
		dataLimite = dataLimite.withMonthOfYear(mes);
		dataLimite = dataLimite.withHourOfDay(0);
		dataLimite = dataLimite.withMinuteOfHour(0);
		dataLimite = dataLimite.withSecondOfMinute(0);
		dataLimite = dataLimite.withMillisOfSecond(0);
		dataLimite = dataLimite.plusMonths(1);
		dataLimite = dataLimite.minusDays(1);

		if(dataReferencia == null) {
			dataReferencia = dataLimite.withDayOfMonth(1);
		}

		if(dataLimite.compareTo(dataReferencia) > 0) {
			if(listaImovelPCSZ != null && !listaImovelPCSZ.isEmpty()) {
				dataReferencia = dataReferencia.plusDays(1);
			}
			while(dataLimite.compareTo(dataReferencia) >= 0 && mes.equals(dataReferencia.getMonthOfYear())
							&& dataReferencia.compareTo(new DateTime()) <= 0) {
				ImovelPCSZ imovelPCSZ = (ImovelPCSZ) criar();
				imovelPCSZ.setDataVigencia(dataReferencia.toDate());
				imovelPCSZ.setImovel(imovel);
				
				if(listaImovelPCSZ ==null){
					listaImovelPCSZ = new ArrayList<>();
				}

				listaImovelPCSZ.add(imovelPCSZ);

				dataReferencia = dataReferencia.plusDays(1);
			}
		}

		return listaImovelPCSZ;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.ControladorImovelPCSZ#permitirAtualizacaoImovelPCSZ(br.com.ggas.medicao.consumo.ImovelPCSZ)
	 */
	@Override
	public boolean permitirAtualizacaoImovelPCSZ(ImovelPCSZ imovelPCSZ) throws NegocioException {

		boolean retorno = true;
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Integer permite = Integer.valueOf((String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_PERMITE_ALTERACAO_PCSZ_IMOVEL));

		if(permite != null && permite <= 0) {

			HistoricoConsumo historicoConsumoFaturado = this.obterUltimoConsumoHistorico(imovelPCSZ, true);

			if(historicoConsumoFaturado != null) {

				Date dataUltimaLeitura = historicoConsumoFaturado.getHistoricoAtual().getDataLeituraFaturada();

				if(dataUltimaLeitura != null) {
					DateTime dataPrimeiraPermitida = new DateTime(dataUltimaLeitura);
					dataPrimeiraPermitida = dataPrimeiraPermitida.plusDays(1);
					dataPrimeiraPermitida = dataPrimeiraPermitida.withHourOfDay(0);
					dataPrimeiraPermitida = dataPrimeiraPermitida.withMinuteOfHour(0);
					dataPrimeiraPermitida = dataPrimeiraPermitida.withSecondOfMinute(0);
					dataPrimeiraPermitida = dataPrimeiraPermitida.withMillisOfSecond(0);

					if(imovelPCSZ.getDataVigencia().before(dataPrimeiraPermitida.toDate())) {
						retorno = false;
					}
				}

			}

		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.ControladorImovelPCSZ#atualizarListaImovelPCSZ(java.util.Collection)
	 */
	@Override
	public boolean atualizarListaImovelPCSZ(Collection<ImovelPCSZ> listaImovelPCSZ) throws NegocioException, ConcorrenciaException {

		boolean retorno = false;

		if(listaImovelPCSZ != null && !listaImovelPCSZ.isEmpty()) {

			// Preencher janelas de PCS de imóvel
			// não preenchidos
			DateTime primeiraDataLista = new DateTime(listaImovelPCSZ.iterator().next().getDataVigencia());

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" select max(dataVigencia)from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where habilitado = true ");
			hql.append(" and imovel.chavePrimaria = :idImovel ");
			hql.append(" and dataVigencia < :data ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong("idImovel", listaImovelPCSZ.iterator().next().getImovel().getChavePrimaria());

			Util.adicionarRestricaoDataSemHora(query, primeiraDataLista.toDate(), "data", Boolean.TRUE);

			Date ultimaDataCadastrada = (Date) query.uniqueResult();

			Days dias = Days.daysBetween(new DateTime(ultimaDataCadastrada), primeiraDataLista);

			if(dias.getDays() > 1) {
				DateTime dataReferencia = new DateTime(ultimaDataCadastrada).plusDays(1);
				while(primeiraDataLista.compareTo(dataReferencia) >= 0 && dataReferencia.compareTo(primeiraDataLista) <= 0) {
					ImovelPCSZ imovelPCSZ = (ImovelPCSZ) criar();
					imovelPCSZ.setDataVigencia(dataReferencia.toDate());
					imovelPCSZ.setImovel(listaImovelPCSZ.iterator().next().getImovel());

					listaImovelPCSZ.add(imovelPCSZ);

					dataReferencia = dataReferencia.plusDays(1);
				}
			}

			for (ImovelPCSZ imovelPCSZ : listaImovelPCSZ) {
				query = null;

				hql = new StringBuilder();
				hql.append(" from ");
				hql.append(getClasseEntidade().getSimpleName());
				hql.append(" where habilitado = true ");
				hql.append(" and imovel.chavePrimaria = :idImovel ");
				hql.append(" and dataVigencia between :dataInicial and :dataFinal ");

				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

				query.setLong("idImovel", imovelPCSZ.getImovel().getChavePrimaria());
				Util.adicionarRestricaoDataSemHora(query, imovelPCSZ.getDataVigencia(), "dataInicial", Boolean.TRUE);
				Util.adicionarRestricaoDataSemHora(query, imovelPCSZ.getDataVigencia(), "dataFinal", Boolean.FALSE);

				ImovelPCSZ imovelPCSZAnterior = (ImovelPCSZ) query.uniqueResult();

				if (imovelPCSZAnterior != null && isimovelPCSZAlterado(imovelPCSZAnterior, imovelPCSZ)) {

					// verifica se houve alguma
					// alteração
					imovelPCSZAnterior.setHabilitado(false);
					// se o cityGateMedicao
					// tiver algum historico
					// historico ele deverá
					// ser recalculado
					imovelPCSZ.setIndicadorAlteracao(true);
					super.atualizar(imovelPCSZAnterior);

				}

				if(!retorno) {

					retorno = this.validarDateVigenciaImovelPCSZ(imovelPCSZ);

				} else {

					this.validarDateVigenciaImovelPCSZ(imovelPCSZ);

				}

				if(imovelPCSZAnterior == null || isimovelPCSZAlterado(imovelPCSZAnterior, imovelPCSZ)) {
					super.inserir(imovelPCSZ);
				}
			}
		}

		return retorno;
	}

	/**
	 * Checks if is imovel pcsz alterado.
	 *
	 * @param imovelPCSZAnterior
	 *            the imovel pcsz anterior
	 * @param imovelPCSZ
	 *            the imovel pcsz
	 * @return true, if is imovel pcsz alterado
	 */
	private boolean isimovelPCSZAlterado(ImovelPCSZ imovelPCSZAnterior, ImovelPCSZ imovelPCSZ) {

		boolean retorno = false;
		BigDecimal imovelPCSZFatorZ ;
		BigDecimal imovelPCSZAnteriorFatorZ;
		Integer imovelPCSZMedidaPCS;
		Integer imovelPCSZAnteriorMedidaPCS ;

		if (imovelPCSZAnterior.getMedidaPCS() == null) {
			imovelPCSZAnteriorMedidaPCS = 0;
		} else {
			imovelPCSZAnteriorMedidaPCS = imovelPCSZAnterior.getMedidaPCS();
		}

		if (imovelPCSZ.getMedidaPCS() == null) {
			imovelPCSZMedidaPCS = 0;
		} else {
			imovelPCSZMedidaPCS = imovelPCSZ.getMedidaPCS();
		}

		if (imovelPCSZAnterior.getFatorZ() == null) {
			imovelPCSZAnteriorFatorZ = BigDecimal.ZERO;
		} else {
			imovelPCSZAnteriorFatorZ = imovelPCSZAnterior.getFatorZ();
		}

		if (imovelPCSZ.getFatorZ() == null) {
			imovelPCSZFatorZ = BigDecimal.ZERO;
		} else {
			imovelPCSZFatorZ = imovelPCSZ.getFatorZ();
		}

		if(imovelPCSZAnteriorMedidaPCS.compareTo(imovelPCSZMedidaPCS) != 0) {
			retorno = true;
		} else if(imovelPCSZAnteriorFatorZ.compareTo(imovelPCSZFatorZ) != 0) {
			retorno = true;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ControladorImovelPCSZ
	 * #obterImovelPCSZVigente(java.lang.Long,
	 * java.util.Date)
	 */
	@Override
	public ImovelPCSZ obterImovelPCSZVigente(Long chavePrimariaImovel, Date dataReferencia) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select chavePrimaria ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where imovel.chavePrimaria = :idImovel ");
		hql.append(" and dataVigencia <= :dataReferencia ");
		hql.append(" and fatorZ is not null ");
		hql.append(" and habilitado = true ");
		hql.append(" order by dataVigencia desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idImovel", chavePrimariaImovel);
		query.setDate("dataReferencia", dataReferencia);
		query.setMaxResults(1);
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidade()));

		return (ImovelPCSZ) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ControladorImovelPCSZ
	 * #obterProximoImovelPCSZVigente
	 * (java.lang.Long,
	 * java.util.Date)
	 */
	@Override
	public ImovelPCSZ obterProximoImovelPCSZVigente(Long chavePrimariaImovelPCSZ, Date dataLimite) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select imovPCSZ from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" imovPCSZ, ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" refImovPCSZ ");
		hql.append(" where imovPCSZ.chavePrimaria <> :idImovelPCZ ");
		hql.append(" and refImovPCSZ.chavePrimaria = :idImovelPCZ ");
		hql.append(" and imovPCSZ.imovel.chavePrimaria = refImovPCSZ.imovel.chavePrimaria ");
		hql.append(" and imovPCSZ.dataVigencia > refImovPCSZ.dataVigencia ");
		hql.append(" and imovPCSZ.dataVigencia <= :dataLimite ");
		hql.append(" and imovPCSZ.habilitado = true ");
		hql.append(" and imovPCSZ.fatorZ is not null ");
		hql.append(" order by imovPCSZ.dataVigencia ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idImovelPCZ", chavePrimariaImovelPCSZ);
		query.setDate("dataLimite", dataLimite);
		query.setMaxResults(1);

		return (ImovelPCSZ) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ControladorImovelPCSZ
	 * #permitirManutencaoFatorZ(long)
	 */
	@Override
	public boolean permitirManutencaoFatorZ(long chavePrimariaImovel) throws NegocioException {

		boolean retorno = true;
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();

		// Permite alteração apenas se a
		// quantidade de pontos de consumos com
		// corretores de vazão foi menor do que o
		// total de pontos de consumo do imóvel.
		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select count(ponto) from ");
		hql.append(controladorPontoConsumo.getClasseEntidade().getSimpleName());
		hql.append(" ponto ");
		hql.append(" where ponto.imovel.chavePrimaria = :idImovel ");
		hql.append(" and ponto.instalacaoMedidor.vazaoCorretor is not null ");
		hql.append(" and ponto.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idImovel", chavePrimariaImovel);

		Long quantidadeComCorretorVazao = (Long) query.uniqueResult();
		if(quantidadeComCorretorVazao != null && quantidadeComCorretorVazao > 0) {

			hql = new StringBuilder();
			hql.append(" select count(ponto) from ");
			hql.append(controladorPontoConsumo.getClasseEntidade().getSimpleName());
			hql.append(" ponto ");
			hql.append(" where ponto.imovel.chavePrimaria = :idImovel ");
			hql.append(" and ponto.habilitado = true ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong("idImovel", chavePrimariaImovel);
			Long quantidadeTotal = (Long) query.uniqueResult();

			if(quantidadeComCorretorVazao.equals(quantidadeTotal)) {
				retorno = false;
			}
		}

		return retorno;
	}

	/**
	 * Validar date vigencia imovel pcsz.
	 *
	 * @param imovelPCSZ
	 *            the imovel pcsz
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private boolean validarDateVigenciaImovelPCSZ(ImovelPCSZ imovelPCSZ) throws NegocioException {

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		boolean retorno = false;

		try {
			Date dataVigencia = imovelPCSZ.getDataVigencia();
			Date dataAtual = df.parse(df.format(new Date()));

			if (dataVigencia != null && dataAtual.after(dataVigencia)) {
				HistoricoConsumo historicoConsumoFaturado = this
						.obterUltimoConsumoHistorico(imovelPCSZ, true);

				if (historicoConsumoFaturado != null) {

					HistoricoMedicao historicoMedicao = historicoConsumoFaturado
							.getHistoricoAtual();

					if (historicoMedicao != null
							&& historicoMedicao.getDataLeituraFaturada() != null) {

						Date dataInicio = df.parse(df.format(dataVigencia));
						Date dataLeituraFaturada = df.parse(df
								.format(historicoMedicao
										.getDataLeituraFaturada()));

						if (dataInicio.before(dataLeituraFaturada)) {

							throw new NegocioException(
									ERRO_NEGOCIO_DATAS_ANTERIORES_AO_ULTIMO_FATURAMENTO,
									false);

						} else {

							HistoricoConsumo historicoConsumoNaoFaturado = this
									.obterUltimoConsumoHistorico(imovelPCSZ,
											false);

							if (historicoConsumoNaoFaturado != null) {

								HistoricoMedicao historicoMedicaoConsumoNaoFaturado = historicoConsumoNaoFaturado
										.getHistoricoAtual();

								if (historicoMedicaoConsumoNaoFaturado
										.getDataLeituraFaturada() != null) {

									dataLeituraFaturada = df
											.parse(df
													.format(historicoMedicaoConsumoNaoFaturado
															.getDataLeituraFaturada()));

									if (dataInicio.before(dataLeituraFaturada)) {

										retorno = true;

									}
								}
							}
						}
					}

				}

			}

		} catch(ParseException e) {
			throw new NegocioException(e.getMessage());
		}

		return retorno;
	}

	private HistoricoConsumo obterUltimoConsumoHistorico(ImovelPCSZ imovelPCSZ, boolean consumoFaturado) throws NegocioException {

		HistoricoConsumo historicoConsumo = null;

		Criteria criteria = createCriteria(HistoricoConsumo.class);
		criteria.setProjection(Projections.max("chavePrimaria"));
		criteria.createAlias("pontoConsumo", "pontoConsumo");
		criteria.createAlias("pontoConsumo.imovel", "imovel");
		criteria.add(Restrictions.eq("imovel.chavePrimaria", imovelPCSZ.getImovel().getChavePrimaria()));
		criteria.add(Restrictions.eq("indicadorFaturamento", consumoFaturado));
		criteria.add(Restrictions.isNotNull("historicoAtual"));

		Long id = (Long) criteria.uniqueResult();

		if(id != null) {

			historicoConsumo = (HistoricoConsumo) createCriteria(HistoricoConsumo.class).add(Restrictions.idEq(id))
							.setFetchMode("historicoAtual", FetchMode.JOIN).uniqueResult();

		}

		return historicoConsumo;

	}
}
