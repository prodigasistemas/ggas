package br.com.ggas.medicao.consumo;

import java.math.BigDecimal;
import java.util.Collection;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.exception.GGASException;

/**
 * The Interface ColetorConsumo.
 */
public interface ColetorConsumo {

	/**
	 * Coletar por consulta.
	 *
	 * @param anoMesFaturamento the ano mes faturamento
	 * @param ciclo the ciclo
	 * @param listaPontoConsumoAgrupados the lista ponto consumo agrupados
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	BigDecimal coletarPorConsulta(int anoMesFaturamento, int ciclo, Collection<PontoConsumo> listaPontoConsumoAgrupados) 
					throws GGASException;

	/**
	 * Coletar por propriedade.
	 *
	 * @param historico the historico
	 * @return the big decimal
	 */
	BigDecimal coletarPorPropriedade(HistoricoConsumo historico);

	/**
	 * Gets the chave mapa dados.
	 *
	 * @return the chave mapa dados
	 */
	String getChaveMapaDados();

}
