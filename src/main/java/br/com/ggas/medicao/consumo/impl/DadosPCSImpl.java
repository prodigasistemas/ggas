/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.consumo.impl;

import java.io.Serializable;
import java.math.BigDecimal;

import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.medicao.consumo.IntervaloPCS;

/**
 * 
 *
 */
class DadosPCSImpl implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 9096666423448124873L;

	private BigDecimal medidaPCS;

	private BigDecimal fatorPCS;

	private IntervaloPCS intervaloPCS;

	private EntidadeConteudo localAmostragemPCS;

	private BigDecimal consumoApurado;

	/**
	 * @return the medidaPCS
	 */
	public BigDecimal getMedidaPCS() {

		return medidaPCS;
	}

	/**
	 * @param medidaPCS
	 *            the medidaPCS to set
	 */
	public void setMedidaPCS(BigDecimal medidaPCS) {

		this.medidaPCS = medidaPCS;
	}

	/**
	 * @return the fatorPCS
	 */
	public BigDecimal getFatorPCS() {

		return fatorPCS;
	}

	/**
	 * @param fatorPCS
	 *            the fatorPCS to set
	 */
	public void setFatorPCS(BigDecimal fatorPCS) {

		this.fatorPCS = fatorPCS;
	}

	/**
	 * @return the intervaloPCS
	 */
	public IntervaloPCS getIntervaloPCS() {

		return intervaloPCS;
	}

	/**
	 * @param intervaloPCS
	 *            the intervaloPCS to set
	 */
	public void setIntervaloPCS(IntervaloPCS intervaloPCS) {

		this.intervaloPCS = intervaloPCS;
	}

	/**
	 * @return the localAmostragemPCS
	 */
	public EntidadeConteudo getLocalAmostragemPCS() {

		return localAmostragemPCS;
	}

	/**
	 * @param localAmostragemPCS
	 *            the localAmostragemPCS to set
	 */
	public void setLocalAmostragemPCS(EntidadeConteudo localAmostragemPCS) {

		this.localAmostragemPCS = localAmostragemPCS;
	}

	/**
	 * @return the consumoApurado
	 */
	public BigDecimal getConsumoApurado() {

		return consumoApurado;
	}

	/**
	 * @param consumoApurado
	 *            the consumoApurado to set
	 */
	public void setConsumoApurado(BigDecimal consumoApurado) {

		this.consumoApurado = consumoApurado;
	}

}
