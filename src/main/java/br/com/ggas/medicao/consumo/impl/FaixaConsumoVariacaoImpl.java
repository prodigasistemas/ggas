/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe FaixaConsumoVariacaoImpl representa uma FaixaConsumoVariacaoImpl no sistema.
 *
 * @since 30/09/2009
 * 
 */

package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.consumo.FaixaConsumoVariacao;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pelo Faixa de
 *  Variação de Consumo 
 *
 */
public class FaixaConsumoVariacaoImpl extends EntidadeNegocioImpl implements FaixaConsumoVariacao, Cloneable {

	private static final int CONSTANTE_ZERO = 0;

	private static final int CONSTANTE_FAIXA_INFERIOR = 1000000;

	private static final double CONSTANTE_FAIXA_SUPERIOR = 999999.99999999998;

	private static final long serialVersionUID = 1371955143942472002L;

	private String descricao;

	private BigDecimal faixaInferior;

	private BigDecimal faixaSuperior;

	private BigDecimal percentualInferior;

	private BigDecimal percentualSuperior;

	private Segmento segmento;

	private BigDecimal baixoConsumo;

	private BigDecimal altoConsumo;

	private BigDecimal estouroConsumo;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * FaixaConsumoVariacao#getSegmento()
	 */
	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * FaixaConsumoVariacao
	 * #setSegmento(br.com.ggas
	 * .cadastro.imovel.Segmento)
	 */
	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * FaixaConsumoVariacao#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * FaixaConsumoVariacao
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * FaixaConsumoVariacao#getFaixaInferior()
	 */
	@Override
	public BigDecimal getFaixaInferior() {

		return faixaInferior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * FaixaConsumoVariacao
	 * #setFaixaInferior(java.math.BigDecimal)
	 */
	@Override
	public void setFaixaInferior(BigDecimal faixaInferior) {

		this.faixaInferior = faixaInferior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * FaixaConsumoVariacao#getFaixaSuperior()
	 */
	@Override
	public BigDecimal getFaixaSuperior() {

		return faixaSuperior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * FaixaConsumoVariacao
	 * #setFaixaSuperior(java.math.BigDecimal)
	 */
	@Override
	public void setFaixaSuperior(BigDecimal faixaSuperior) {

		this.faixaSuperior = faixaSuperior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * FaixaConsumoVariacao
	 * #getPercentualInferior()
	 */
	@Override
	public BigDecimal getPercentualInferior() {

		return percentualInferior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * FaixaConsumoVariacao
	 * #setPercentualInferior(java
	 * .math.BigDecimal)
	 */
	@Override
	public void setPercentualInferior(BigDecimal percentualInferior) {

		this.percentualInferior = percentualInferior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * FaixaConsumoVariacao
	 * #getPercentualSuperior()
	 */
	@Override
	public BigDecimal getPercentualSuperior() {

		return percentualSuperior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * FaixaConsumoVariacao
	 * #setPercentualSuperior(java
	 * .math.BigDecimal)
	 */
	@Override
	public void setPercentualSuperior(BigDecimal percentualSuperior) {

		this.percentualSuperior = percentualSuperior;
	}

	@Override
	public BigDecimal getBaixoConsumo() {

		return baixoConsumo;
	}

	@Override
	public void setBaixoConsumo(BigDecimal baixoConsumo) {

		this.baixoConsumo = baixoConsumo;
	}

	@Override
	public BigDecimal getAltoConsumo() {

		return altoConsumo;
	}

	@Override
	public void setAltoConsumo(BigDecimal altoConsumo) {

		this.altoConsumo = altoConsumo;
	}

	@Override
	public BigDecimal getEstouroConsumo() {

		return estouroConsumo;
	}

	@Override
	public void setEstouroConsumo(BigDecimal estouroConsumo) {

		this.estouroConsumo = estouroConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(segmento == null) {
			stringBuilder.append(SEGMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(faixaInferior == null) {
			stringBuilder.append(FAIXA_INFERIOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if(faixaInferior.compareTo(BigDecimal.ZERO) == CONSTANTE_ZERO) {
				faixaInferior = BigDecimal.ZERO;
			}
		}

		if(faixaSuperior == null) {
			stringBuilder.append(FAIXA_SUPERIOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		} else {
			if(faixaInferior != null && faixaSuperior.compareTo(faixaInferior) <= CONSTANTE_ZERO) {
				stringBuilder.append(FAIXA_SUPERIOR_MENOR_QUE_FAIXA_SUPERIOR);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

			if(faixaInferior != null && faixaSuperior.compareTo(BigDecimal.valueOf(CONSTANTE_FAIXA_INFERIOR)) != CONSTANTE_ZERO
							&& faixaSuperior.compareTo(BigDecimal.valueOf(CONSTANTE_FAIXA_SUPERIOR)) >= CONSTANTE_ZERO) {
				stringBuilder.append(ERRO_VALOR_MAXIMO_DA_FAIXA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		if(percentualInferior == null) {
			stringBuilder.append(PERCENTUAL_INFERIOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(percentualSuperior == null) {
			stringBuilder.append(PERCENTUAL_SUPERIOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > CONSTANTE_ZERO) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(CONSTANTE_ZERO, stringBuilder.toString().length() - 2));
		}

		return erros;
	}

	public Boolean getHabilitado() {

		return super.isHabilitado();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {

		return super.clone();
	}

}
