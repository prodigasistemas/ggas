/*
 Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

 Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */
/***
 * Classe de VO para geração de relatório de Consistir Leitura e Consumo
 */

package br.com.ggas.medicao.consumo.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe para geração do
 *  relatório de Leitura e Consumo
 *
 */
public class AnaliseConsistirLeituraConsumoVO implements Serializable {
		
	private static final long serialVersionUID = 1L;

	private Long cdPontoConsumo;
	
	private String dsPontoConsumo;
	
	private String dsSegmento;
	
	private Date dataLeitura;
	
	private BigDecimal consumo;
	
	private BigDecimal consumoApurado;
	
	private BigDecimal pcs;
	
	private BigDecimal consumoMedio;
	
	private String dsGrupoFaturamento;
	
	private String dsRota;


	public Long getCdPontoConsumo() {
		return cdPontoConsumo;
	}

	public void setCdPontoConsumo(Long cdPontoConsumo) {
		this.cdPontoConsumo = cdPontoConsumo;
	}

	public String getDsPontoConsumo() {
		return dsPontoConsumo;
	}

	public void setDsPontoConsumo(String dsPontoConsumo) {
		this.dsPontoConsumo = dsPontoConsumo;
	}

	public String getDsSegmento() {
		return dsSegmento;
	}

	public void setDsSegmento(String dsSegmento) {
		this.dsSegmento = dsSegmento;
	}

	public String getDataLeitura() {
		SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
		return data.format(dataLeitura);
	}

	public void setDataLeitura(Date pDataleitura) {
		dataLeitura = pDataleitura;
	}

	/**
	 * Arredondamento do consumo
	 * @return
	 */
	public BigDecimal getConsumo() {
		if(consumo != null) {
			return consumo.setScale(0, RoundingMode.HALF_EVEN);
		}else {
			return consumo;
		}
	}

	public void setConsumo(BigDecimal consumo) {
		this.consumo = consumo;
	}

	/**
	 * Arredondamento do Consumo Apurado
	 * @return
	 */
	public BigDecimal getConsumoApurado() {
		if(consumoApurado != null) {
			return consumoApurado.setScale(0, RoundingMode.HALF_EVEN);
		}else {
			return consumoApurado;
		}
	}

	public void setConsumoApurado(BigDecimal consumoApurado) {
		this.consumoApurado = consumoApurado;
	}

	public BigDecimal getPcs() {
		return pcs;
	}

	public void setPcs(BigDecimal pcs) {
		this.pcs = pcs;
	}
	/**
	 * Arredondamento do Consumo Médio
	 * @return
	 */
	public BigDecimal getConsumoMedio() {
		if(consumoMedio != null) {
			return consumoMedio.setScale(0, RoundingMode.HALF_EVEN);
		}else {
			return consumoMedio;
		}
	}

	public void setConsumoMedio(BigDecimal consumoMedio) {
		this.consumoMedio = consumoMedio;
	}
	
	public String getGrupoFaturamento() {
		return dsGrupoFaturamento;
	}

	public void setGrupoFaturamento(String dsGrupoFaturamento) {
		this.dsGrupoFaturamento = dsGrupoFaturamento;
	}

	public String getDsRota() {
		return dsRota;
	}

	public void setDsRota(String dsRota) {
		this.dsRota = dsRota;
	}

}
