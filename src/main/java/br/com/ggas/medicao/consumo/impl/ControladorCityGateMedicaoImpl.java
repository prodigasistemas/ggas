/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.Days;

import br.com.ggas.cadastro.localidade.RedeDistribuicaoTronco;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.faturamento.impl.FaturaImpl;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.CityGateMedicao;
import br.com.ggas.medicao.consumo.ControladorCityGateMedicao;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * Classe responsável por implementar os métodos de consulta e persistência relativos a entidade CityGateMedicao.
 */
class ControladorCityGateMedicaoImpl extends ControladorNegocioImpl implements ControladorCityGateMedicao {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(CityGateMedicao.BEAN_ID_CITY_GATE_MEDICAO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(CityGateMedicao.BEAN_ID_CITY_GATE_MEDICAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.ControladorCityGateMedicao#criarCityGate()
	 */
	@Override
	public EntidadeNegocio criarCityGate() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(CityGate.BEAN_ID_CITY_GATE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ControladorCityGateMedicao#
	 * listarAnosDisponiveisCityGateMedicao(br.com.
	 * ggas.cadastro.operacional.CityGate)
	 */
	@Override
	public Collection<Integer> listarAnosDisponiveisCityGateMedicao(CityGate cityGate) throws NegocioException {

		Collection<Integer> anosDisponiveis = new ArrayList<Integer>();

		Date dataAtual = new Date();
		int anoAtual = new DateTime().getYear();

		StringBuilder hql = new StringBuilder();
		hql.append(" select min(dataMedicao) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" and cityGate.chavePrimaria = :idCityGate ");
		hql.append(" and dataMedicao < :dataAtual");
		hql.append(" order by dataMedicao desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCityGate", cityGate.getChavePrimaria());
		query.setDate("dataAtual", dataAtual);
		query.setMaxResults(1);

		Date dataMinima = (Date) query.uniqueResult();
		Calendar calendar = Calendar.getInstance();
		if (dataMinima != null) {
			calendar.setTime(dataMinima);
		} else {
			calendar.setTime(dataAtual);
		}

		for (int ano = anoAtual; ano >= calendar.get(Calendar.YEAR); ano--) {
			anosDisponiveis.add(ano);
		}

		return anosDisponiveis;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.
	 * ControladorCityGateMedicao
	 * #listarCityGateMedicaoManutencao
	 * (br.com.ggas.cadastro.operacional.CityGate,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public Collection<CityGateMedicao> listarCityGateMedicaoManutencao(CityGate cityGate, Integer ano, Integer mes) throws NegocioException {

		if (cityGate == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, CityGate.CITY_GATE);
		}

		Collection<CityGateMedicao> listaCityGateMedicao = new ArrayList<CityGateMedicao>();

		if ((ano == null && mes != null) || (ano != null && mes == null)) {
			throw new NegocioException(ControladorCityGateMedicao.ERRO_NEGOCIO_ANO_MES_NAO_INFORMADOS, false);
		} else {
			if (ano != null && mes != null) {
				listaCityGateMedicao.addAll(listarCityGateMedicaoPorAnoMes(cityGate, ano, mes));
			} else {
				listaCityGateMedicao.addAll(listarCityGateMedicaoEmAberto(cityGate));
			}
		}

		return listaCityGateMedicao;
	}

	/**
	 * Listar city gate medicao por ano mes.
	 * 
	 * @param cityGate
	 *            the city gate
	 * @param ano
	 *            the ano
	 * @param mes
	 *            the mes
	 * @return the collection<? extends city gate medicao>
	 */
	@SuppressWarnings("unchecked")
	private Collection<? extends CityGateMedicao> listarCityGateMedicaoPorAnoMes(CityGate cityGate, Integer ano, Integer mes) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" and cityGate.chavePrimaria = :idCityGate ");
		hql.append(" and year(dataMedicao) = :ano ");
		hql.append(" and month(dataMedicao) = :mes ");
		hql.append(" order by dataMedicao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCityGate", cityGate.getChavePrimaria());
		query.setInteger("ano", ano);
		query.setInteger("mes", mes);

		Collection<CityGateMedicao> listaCityGateMedicaoEmAberto = query.list();

		// Verifica se o último registro é menor
		// do que a data corrente, para
		// completa a lista se for o caso
		DateTime dataReferencia = null;
		if (listaCityGateMedicaoEmAberto != null && !listaCityGateMedicaoEmAberto.isEmpty()) {
			List<CityGateMedicao> lista = new ArrayList<CityGateMedicao>(listaCityGateMedicaoEmAberto);
			CityGateMedicao ultimoCityGateMedicao = lista.get(lista.size() - 1);
			dataReferencia = new DateTime(ultimoCityGateMedicao.getDataMedicao());
		}
		
		DateTime dataLimite = new DateTime();
		dataLimite = dataLimite.withDayOfMonth(1);
		dataLimite = dataLimite.withYear(ano);
		dataLimite = dataLimite.withMonthOfYear(mes);
		dataLimite = dataLimite.withHourOfDay(0);
		dataLimite = dataLimite.withMinuteOfHour(0);
		dataLimite = dataLimite.withSecondOfMinute(0);
		dataLimite = dataLimite.withMillisOfSecond(0);
		dataLimite = dataLimite.plusMonths(1);
		dataLimite = dataLimite.minusDays(1);

		if (dataReferencia == null) {
			dataReferencia = dataLimite.withDayOfMonth(1);
		}

		if (dataLimite.compareTo(dataReferencia) > 0) {
			if (listaCityGateMedicaoEmAberto != null && !listaCityGateMedicaoEmAberto.isEmpty()) {
				dataReferencia = dataReferencia.plusDays(1);
			}
			//dataReferencia = dataReferencia.plusDays(1);
			
			while (dataLimite.compareTo(dataReferencia) >= 0 &&  mes.equals(dataReferencia.getMonthOfYear())
							&& dataReferencia.compareTo(new DateTime()) <= 0) {
				CityGateMedicao cityGateMedicao = (CityGateMedicao) criar();
				cityGateMedicao.setDataMedicao(dataReferencia.toDate());
				cityGateMedicao.setCityGate(cityGate);
				
				if(listaCityGateMedicaoEmAberto !=null){
					listaCityGateMedicaoEmAberto.add(cityGateMedicao);
				}

				dataReferencia = dataReferencia.plusDays(1);
			}
		}

		return listaCityGateMedicaoEmAberto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.
	 * ControladorCityGateMedicao
	 * #listarCityGateMedicaoPorData
	 * (java.util.Date)
	 */
	@Override
	public Collection<CityGateMedicao> listarCityGateMedicaoPorData(Date data) {

		DateTime dateTime = new DateTime(data);
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" and year(dataMedicao) = :ano ");
		hql.append(" and month(dataMedicao) = :mes ");
		hql.append(" and day(dataMedicao) = :dia ");
		hql.append(" order by dataMedicao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setInteger("ano", dateTime.getYear());
		query.setInteger("mes", dateTime.getMonthOfYear());
		query.setInteger("dia", dateTime.getDayOfMonth());

		return query.list();
	}

	/**
	 * Listar city gate medicao em aberto.
	 * 
	 * @param cityGate
	 *            the city gate
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<CityGateMedicao> listarCityGateMedicaoEmAberto(CityGate cityGate) throws NegocioException {

		Collection<CityGateMedicao> listaCityGateMedicaoEmAberto = new ArrayList<CityGateMedicao>();

		// 1. Consulta menor data em aberto para o
		// city gate
		Date maiorDataCityGate = null;
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select max(dataMedicao) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" and cityGate.chavePrimaria = :idCityGate ");
		hql.append(" and (medidaPCSSupridora is not null ");
		hql.append("  or medidaPCSFixo is not null ");
		hql.append("  or medidaPCSDistribuidora is not null) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCityGate", cityGate.getChavePrimaria());

		maiorDataCityGate = (Date) query.uniqueResult();

		// 2.1 Caso não exista nenhum registro
		// para o city gate informado, gerar
		// a lista apenas pra o mês corrente.
		if (maiorDataCityGate == null) {
			DateTime dataReferencia = new DateTime();
			dataReferencia = dataReferencia.withDayOfMonth(1);
			while (dataReferencia.getMonthOfYear() == new DateTime().getMonthOfYear() && dataReferencia.compareTo(new DateTime()) <= 0) {
				CityGateMedicao cityGateMedicao = (CityGateMedicao) criar();
				cityGateMedicao.setDataMedicao(dataReferencia.toDate());
				cityGateMedicao.setCityGate(cityGate);
				
				if(listaCityGateMedicaoEmAberto ==null){
					listaCityGateMedicaoEmAberto = new ArrayList<>();
				}
				listaCityGateMedicaoEmAberto.add(cityGateMedicao);

				dataReferencia = dataReferencia.plusDays(1);
			}
		} else {
			// 2.2 Caso contrário, gerar a lista
			// até o data corrente
			hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where habilitado = true ");
			hql.append(" and cityGate.chavePrimaria = :idCityGate ");
			hql.append(" and dataMedicao >= :data ");
			hql.append(" order by dataMedicao ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong("idCityGate", cityGate.getChavePrimaria());

			// Se existe um registro cadastrado cuja a medida da supridora não esteja informada, considerá-lo
			Util.adicionarRestricaoDataSemHora(query, maiorDataCityGate, "data", Boolean.TRUE);

			listaCityGateMedicaoEmAberto = query.list();

			// Verifica se o último registro é menor do que a data corrente, para completa a lista se for o caso
			if (listaCityGateMedicaoEmAberto != null && !listaCityGateMedicaoEmAberto.isEmpty()) {
				List<CityGateMedicao> lista = new ArrayList<CityGateMedicao>(listaCityGateMedicaoEmAberto);
				CityGateMedicao ultimoCityGateMedicao = lista.get(lista.size() - 1);
				DateTime dataReferencia = new DateTime(ultimoCityGateMedicao.getDataMedicao());

				DateTime dataHoje = new DateTime();
				dataHoje = dataHoje.withHourOfDay(0);
				dataHoje = dataHoje.withMinuteOfHour(0);
				dataHoje = dataHoje.withSecondOfMinute(0);
				dataHoje = dataHoje.withMillisOfSecond(0);

				if (dataHoje.compareTo(dataReferencia) > 0) {
					dataReferencia = dataReferencia.plusDays(1);
					while (dataHoje.compareTo(dataReferencia) >= 0 && dataReferencia.compareTo(new DateTime()) <= 0) {
						CityGateMedicao cityGateMedicao = (CityGateMedicao) criar();
						cityGateMedicao.setDataMedicao(dataReferencia.toDate());
						cityGateMedicao.setCityGate(cityGate);

						listaCityGateMedicaoEmAberto.add(cityGateMedicao);

						dataReferencia = dataReferencia.plusDays(1);
					}
				}
			}
		}

		if (listaCityGateMedicaoEmAberto == null || listaCityGateMedicaoEmAberto.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_NAO_EXISTE_DADOS_COMPRA_EM_ABERTO_CITY_GATE, cityGate.getDescricao());
		}

		return listaCityGateMedicaoEmAberto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.
	 * ControladorCityGateMedicao
	 * #atualizarListaCityGateMedicao
	 * (java.util.Collection)
	 */
	@Override
	public boolean atualizarListaCityGateMedicao(Collection<CityGateMedicao> listaCityGateMedicao) throws NegocioException,
					ConcorrenciaException {

		boolean retorno = false;

		if (listaCityGateMedicao != null && !listaCityGateMedicao.isEmpty()) {

			// Preencher janelas de PCS de city
			// gate não preenchidos
			DateTime primeiraDataLista = new DateTime(listaCityGateMedicao.iterator().next().getDataMedicao());

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" select max(dataMedicao)from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" where habilitado = true ");
			hql.append(" and cityGate.chavePrimaria = :idCityGate ");
			hql.append(" and dataMedicao < :data ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setLong("idCityGate", listaCityGateMedicao.iterator().next().getCityGate().getChavePrimaria());
			Util.adicionarRestricaoDataSemHora(query, primeiraDataLista.toDate(), "data", Boolean.TRUE);

			Date ultimaDataCadastrada = (Date) query.uniqueResult();

			Days dias = Days.daysBetween(new DateTime(ultimaDataCadastrada), primeiraDataLista);

			if (dias.getDays() > 1) {
				DateTime dataReferencia = new DateTime(ultimaDataCadastrada).plusDays(1);
				while (primeiraDataLista.compareTo(dataReferencia) >= 0 && dataReferencia.compareTo(primeiraDataLista) <= 0) {
					CityGateMedicao cityGateMedicao = (CityGateMedicao) criar();
					cityGateMedicao.setDataMedicao(dataReferencia.toDate());
					cityGateMedicao.setCityGate(listaCityGateMedicao.iterator().next().getCityGate());

					listaCityGateMedicao.add(cityGateMedicao);

					dataReferencia = dataReferencia.plusDays(1);
				}
			}

			CityGate cityGate = listaCityGateMedicao.iterator().next().getCityGate();
			long chavePrimariaCityGate = cityGate.getChavePrimaria();

			for (CityGateMedicao cityGateMedicao : listaCityGateMedicao) {
				query = null;

				hql = new StringBuilder();
				hql.append(" from ");
				hql.append(getClasseEntidade().getSimpleName());
				hql.append(" where habilitado = true ");
				hql.append(" and cityGate.chavePrimaria = :idCityGate ");
				hql.append(" and dataMedicao between :dataInicial and :dataFinal ");

				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

				query.setLong("idCityGate", chavePrimariaCityGate);
				Date dataMedicao = cityGateMedicao.getDataMedicao();
				Util.adicionarRestricaoDataSemHora(query, dataMedicao, "dataInicial", Boolean.TRUE);
				Util.adicionarRestricaoDataSemHora(query, dataMedicao, "dataFinal", Boolean.FALSE);

				CityGateMedicao cityGateMedicaoAnterior = (CityGateMedicao) query.uniqueResult();

				if (cityGateMedicaoAnterior != null && isCityGateMedicaoAlterado(cityGateMedicaoAnterior, cityGateMedicao)) {

					// verifica se houve alguma
					// alteração
					cityGateMedicaoAnterior.setHabilitado(false);
					// se o cityGateMedicao
					// tiver algum historico
					// historico ele deverá
					// ser
					// recalculado
					cityGateMedicao.setIndicadorAlteracao(true);
					this.atualizar(cityGateMedicaoAnterior);

				}

				if (!retorno) {
					retorno = this.validarDateInicioCityGateMedicaoPressao(cityGate, dataMedicao);
				} else {
					this.validarDateInicioCityGateMedicaoPressao(cityGate, dataMedicao);
				}

				if (cityGateMedicaoAnterior == null || isCityGateMedicaoAlterado(cityGateMedicaoAnterior, cityGateMedicao)) {
					this.inserir(cityGateMedicao);
				}

			}
		}

		return retorno;
	}

	/**
	 * Checks if is city gate medicao alterado.
	 * 
	 * @param cityGateMedicaoAnterior
	 *            the city gate medicao anterior
	 * @param cityGateMedicao
	 *            the city gate medicao
	 * @return true, if is city gate medicao alterado
	 */
	private boolean isCityGateMedicaoAlterado(CityGateMedicao cityGateMedicaoAnterior, CityGateMedicao cityGateMedicao) {

		boolean retorno = false;

		Integer cityGateMedicaoAnteriorMedidaPCSSupridora;
		if (cityGateMedicaoAnterior.getMedidaPCSSupridora() == null) {
			cityGateMedicaoAnteriorMedidaPCSSupridora = 0;
		} else {
			cityGateMedicaoAnteriorMedidaPCSSupridora = cityGateMedicaoAnterior.getMedidaPCSSupridora();
		}
		Integer cityGateMedicaoMedidaPCSSupridora;
		if (cityGateMedicao.getMedidaPCSSupridora() == null) {
			cityGateMedicaoMedidaPCSSupridora = 0;
		} else {
			cityGateMedicaoMedidaPCSSupridora = cityGateMedicao.getMedidaPCSSupridora();
		}

		Integer cityGateMedicaoAnteriorMedidaPCSDistribuidora;
		if (cityGateMedicaoAnterior.getMedidaPCSDistribuidora() == null) {
			cityGateMedicaoAnteriorMedidaPCSDistribuidora = 0;
		} else {
			cityGateMedicaoAnteriorMedidaPCSDistribuidora = cityGateMedicaoAnterior.getMedidaPCSDistribuidora();
		}
		Integer cityGateMedicaoMedidaPCSDistribuidora;
		if (cityGateMedicao.getMedidaPCSDistribuidora() == null) {
			cityGateMedicaoMedidaPCSDistribuidora = 0;
		} else {
			cityGateMedicaoMedidaPCSDistribuidora = cityGateMedicao.getMedidaPCSDistribuidora();
		}

		Integer cityGateMedicaoAnteriorMedidaPCSFixo;
		if (cityGateMedicaoAnterior.getMedidaPCSFixo() == null) {
			cityGateMedicaoAnteriorMedidaPCSFixo = 0;
		} else {
			cityGateMedicaoAnteriorMedidaPCSFixo = cityGateMedicaoAnterior.getMedidaPCSFixo();
		}
		Integer cityGateMedicaoMedidaPCSFixo;
		if (cityGateMedicao.getMedidaPCSFixo() == null) {
			cityGateMedicaoMedidaPCSFixo = 0;
		} else {
			cityGateMedicaoMedidaPCSFixo = cityGateMedicao.getMedidaPCSFixo();
		}

		Integer cityGateMedicaoAnteriorMedidaVolumeSupridora;
		if (cityGateMedicaoAnterior.getMedidaVolumeSupridora() == null) {
			cityGateMedicaoAnteriorMedidaVolumeSupridora = 0;
		} else {
			cityGateMedicaoAnteriorMedidaVolumeSupridora = cityGateMedicaoAnterior.getMedidaVolumeSupridora();
		}
		Integer cityGateMedicaoMedidaVolumeSupridora;
		if (cityGateMedicao.getMedidaVolumeSupridora() == null) {
			cityGateMedicaoMedidaVolumeSupridora = 0;
		} else {
			cityGateMedicaoMedidaVolumeSupridora = cityGateMedicao.getMedidaVolumeSupridora();
		}
		Integer cityGateMedicaoAnteriorMedidaVolumeDistribuidora;
		if (cityGateMedicaoAnterior.getMedidaVolumeDistribuidora() == null) {
			cityGateMedicaoAnteriorMedidaVolumeDistribuidora = 0;
		} else {
			cityGateMedicaoAnteriorMedidaVolumeDistribuidora = cityGateMedicaoAnterior.getMedidaVolumeDistribuidora();
		}
		Integer cityGateMedicaoMedidaVolumeDistribuidora;
		if (cityGateMedicao.getMedidaVolumeDistribuidora() == null) {
			cityGateMedicaoMedidaVolumeDistribuidora = 0;
		} else {
			cityGateMedicaoMedidaVolumeDistribuidora = cityGateMedicao.getMedidaVolumeDistribuidora();
		}

		if (cityGateMedicaoAnteriorMedidaPCSSupridora.compareTo(cityGateMedicaoMedidaPCSSupridora) != 0) {
			retorno = true;
		} else if (cityGateMedicaoAnteriorMedidaPCSDistribuidora.compareTo(cityGateMedicaoMedidaPCSDistribuidora) != 0) {
			retorno = true;
		} else if (cityGateMedicaoAnteriorMedidaPCSFixo.compareTo(cityGateMedicaoMedidaPCSFixo) != 0) {
			retorno = true;
		} else if (cityGateMedicaoAnteriorMedidaVolumeSupridora.compareTo(cityGateMedicaoMedidaVolumeSupridora) != 0) {
			retorno = true;
		} else if (cityGateMedicaoAnteriorMedidaVolumeDistribuidora.compareTo(cityGateMedicaoMedidaVolumeDistribuidora) != 0) {
			retorno = true;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ControladorCityGateMedicao#
	 * permitirAtualizacaoCityGateMedicao(br.com.ggas
	 * .medicao.consumo.CityGateMedicao)
	 */
	@Override
	public boolean permitirAtualizacaoCityGateMedicao(Date dataUltimaLeitura, Date dataMedicao) throws NegocioException {

		boolean retorno = true;
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		if (dataUltimaLeitura != null) {
			Integer permite = Integer.valueOf((String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_PERMITE_ALTERACAO_PCS_CITY_GATE_FATURADO));
			if (permite != null && permite <= 0) {
				DateTime dataPrimeiraPermitida = new DateTime(dataUltimaLeitura);
				dataPrimeiraPermitida = dataPrimeiraPermitida.plusDays(1);
				dataPrimeiraPermitida = dataPrimeiraPermitida.withHourOfDay(0);
				dataPrimeiraPermitida = dataPrimeiraPermitida.withMinuteOfHour(0);
				dataPrimeiraPermitida = dataPrimeiraPermitida.withSecondOfMinute(0);
				dataPrimeiraPermitida = dataPrimeiraPermitida.withMillisOfSecond(0);

				if (dataMedicao.before(dataPrimeiraPermitida.toDate())) {
					retorno = false;
				}
			}

		}

		return retorno;
	}

	/**
	 * Validar date inicio city gate medicao pressao.
	 * 
	 * @param cityGate
	 *            the city gate
	 * @param dataMedicao
	 *            the data medicao
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private boolean validarDateInicioCityGateMedicaoPressao(CityGate cityGate, Date dataMedicao) throws NegocioException {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		boolean retorno = false;

		try {
			Date dataAtual = df.parse(df.format(new Date()));
			if (dataMedicao != null && dataAtual.after(dataMedicao)) {
				HistoricoConsumo historicoConsumoFaturado = this.obterUltimoConsumoHistorico(cityGate, true);

				if (historicoConsumoFaturado != null) {

					HistoricoMedicao historicoMedicao = historicoConsumoFaturado.getHistoricoAtual();

					if (historicoMedicao != null && historicoMedicao.getDataLeituraFaturada() != null) {
						
						Date dataInicio = df.parse(df.format(dataMedicao));
						Date dataLeituraFaturada = df.parse(df.format(historicoMedicao.getDataLeituraFaturada()));

						if (dataInicio.before(dataLeituraFaturada)) {
							throw new NegocioException(ERRO_NEGOCIO_DATAS_ANTERIORES_AO_ULTIMO_FATURAMENTO, false);

						} else {
							HistoricoConsumo historicoConsumoNaoFaturado = this.obterUltimoConsumoHistorico(cityGate, false);
							if (historicoConsumoNaoFaturado != null) {
								HistoricoMedicao historicoMedicaoConsumoNaoFaturado = historicoConsumoNaoFaturado.getHistoricoAtual();
								if (historicoMedicaoConsumoNaoFaturado.getDataLeituraFaturada() != null) {
									dataLeituraFaturada = df.parse(df.format(historicoMedicaoConsumoNaoFaturado
													.getDataLeituraFaturada()));
									if (dataInicio.before(dataLeituraFaturada)) {
										retorno = true;
									}

								}

							}

						}

					}

				}

			}

		} catch (ParseException e) {
			throw new NegocioException(e.getMessage());
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.ControladorCityGateMedicao#obterUltimoConsumoHistorico(br.com.ggas.cadastro.operacional.CityGate,
	 * boolean)
	 */
	@Override
	public HistoricoConsumo obterUltimoConsumoHistorico(CityGate cityGate, boolean consumoFaturado) throws NegocioException {

		HistoricoConsumo historicoConsumo = null;

		StringBuilder hql = new StringBuilder("from HistoricoConsumoImpl historicoConsumo ");
		hql.append("inner join fetch historicoConsumo.pontoConsumo.imovel.quadraFace.rede rede ");
		hql.append("inner join fetch historicoConsumo.historicoAtual ha ");
		hql.append("where rede.chavePrimaria in ( ");
		hql.append(" select rdt.rede.chavePrimaria from RedeDistribuicaoTroncoImpl rdt where rdt.tronco.cityGate.chavePrimaria = :citygate )");
		hql.append("and historicoConsumo.indicadorFaturamento = :consumoFaturado ");
		hql.append("and historicoConsumo.historicoAtual.dataLeituraInformada is not null ");
		hql.append(" and historicoConsumo.habilitado = true ");
		hql.append(" and historicoConsumo.chavePrimaria in ( ");
		hql.append(" select fatura.historicoConsumo.chavePrimaria from  ");
		hql.append(FaturaImpl.class.getSimpleName());
		hql.append(" fatura where fatura.faturaAvulso = false ) ");
		hql.append("order by ha.dataLeituraInformada desc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("citygate", cityGate.getChavePrimaria());
		query.setBoolean("consumoFaturado", consumoFaturado);
		query.setMaxResults(1);
		historicoConsumo = (HistoricoConsumo) query.uniqueResult();

		return historicoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.ControladorCityGateMedicao#obterListaChaveRedes(java.lang.Long)
	 */
	@Override
	public List<Long> obterListaChaveRedes(Long chavePrimariaCityGate) throws NegocioException {

		Criteria criteria = createCriteria(RedeDistribuicaoTronco.class, "redeDistribuicaoTronco");
		criteria.createAlias("redeDistribuicaoTronco.tronco", "tronco");
		criteria.add(Restrictions.eq("tronco.cityGate.chavePrimaria", chavePrimariaCityGate));

		List<RedeDistribuicaoTronco> listaRedeDistribuicaoTronco = criteria.list();

		List<Long> listaChaveRedes = null;
		if (listaRedeDistribuicaoTronco != null && !listaRedeDistribuicaoTronco.isEmpty()) {

			listaChaveRedes = new ArrayList<Long>();
			for (RedeDistribuicaoTronco redeDistribuicaoTronco : listaRedeDistribuicaoTronco) {
				listaChaveRedes.add(redeDistribuicaoTronco.getRede().getChavePrimaria());
			}
		}

		return listaChaveRedes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.ControladorCityGateMedicao#consultarPontosConsumoCityGateMedicao(java.util.Map)
	 */
	@Override
	public Collection<CityGateMedicao> consultarPontosConsumoCityGateMedicao(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if (filtro != null) {

			Long idCityGate = (Long) filtro.get("idCityGate");
			if (idCityGate != null && idCityGate > 0) {
				criteria.add(Restrictions.eq("cityGate.chavePrimaria", idCityGate));
			}

		}
		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.ControladorCityGateMedicao#consultarCityGateMedicaoPorCityGateEData(br.com.ggas.cadastro.operacional.
	 * CityGate, java.util.Date)
	 */
	@Override
	public List<CityGateMedicao> consultarCityGateMedicaoPorCityGateEData(CityGate cityGate, Date data) {

		Criteria criteria = getCriteria();

		criteria.setFetchMode("cityGate", FetchMode.JOIN);

		if (cityGate != null) {
			criteria.createAlias("cityGate", "cityGate");
			criteria.add(Restrictions.eq("cityGate.chavePrimaria", cityGate.getChavePrimaria()));
		}
		if (data != null) {
			criteria.add(Restrictions.eq("dataMedicao", data));
		}
		criteria.add(Restrictions.eq("habilitado", true));

		return criteria.list();
	}
	
	private void validarArquivo(byte[] bytes, String nomeArquivo) throws NegocioException {
		if ( (StringUtils.isEmpty(nomeArquivo)) && 
				(bytes != null)) {
			throw new NegocioException(Constantes.CITY_GATE_ARQUIVO_SELECAO_INVALIDA, true);
		}
	}
	
	private Workbook getWorkbook(String tipoArquivo, byte[] bytes) throws NegocioException {
		Workbook wb = null;
		try {
			if (Constantes.FORMATO_ARQUIVO_XLSX.equals(tipoArquivo)) {
				wb = new XSSFWorkbook(new ByteArrayInputStream(bytes));
			} else if (Constantes.FORMATO_ARQUIVO_XLS.equals(tipoArquivo)) {
				wb = new HSSFWorkbook(new ByteArrayInputStream(bytes));
			} 
			if (wb == null) {
				throw new NegocioException(Constantes.FALHA_FORMATO_IMPORTACAO, true);
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(e);
		}
		return wb;
	}
	
	private Sheet getSheet(byte[] bytes, String tipoArquivo,
			String nomeArquivo) throws NegocioException {
		
		validarArquivo(bytes, nomeArquivo);
		Workbook wb = getWorkbook(tipoArquivo, bytes);
		Sheet sheet = wb.getSheet(Constantes.PLANILHA_MEDICAO_VOLUME);
		if (sheet == null) {
			throw new NegocioException(Constantes.CITY_GATE_PLANILHA_NAO_EXISTE, true);
		}
		return sheet;
	}
	
	@Override
	public PlanilhaVolumePcsVO importarPlanilhaExcel(CityGate cityGate, byte[] bytes, String tipoArquivo,
			String nomeArquivo, Integer diaInicialParam, Integer diaFinalParam, Date dataLeituraFaturada) throws NegocioException {
		
		Sheet sheet = getSheet(bytes, tipoArquivo, nomeArquivo);
		PlanilhaVolumePcsVO vo = new PlanilhaVolumePcsVO();
		
		String periodo = "";
		try {
			periodo = sheet.getRow(Constantes.PLANILHA_MEDICAO_LINHA_PERIODO).getCell(
					Constantes.PLANILHA_MEDICAO_COLUNA_PERIODO).getStringCellValue();
			vo.setMes(periodo.split("/")[0]);
			vo.setAno(periodo.split("/")[1]);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.CITY_GATE_IMPORTACAO_ERRO_CELL_PERIODO + " Período no Arquivo: " + periodo);
		}
		
		Calendar calMedicao = this.extrairPeriodoCalendar(vo);
		
		Integer diaInicial;
		if (diaInicialParam != null) {
			diaInicial = diaInicialParam;
		} else {
			diaInicial = 1;
		}
		
		Integer diaFinal;
		if (diaFinalParam != null) {
			diaFinal = diaFinalParam;
		} else {
			diaFinal = calMedicao.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		
		int linhaDia = Constantes.PLANILHA_MEDICAO_LINHA_INICIO_DIAS;
		int colDia = Constantes.PLANILHA_MEDICAO_COLUNA_DIA;
		int colVol = Constantes.PLANILHA_MEDICAO_COLUNA_VOLUME;
		int colPCS = Constantes.PLANILHA_MEDICAO_COLUNA_PCS;
		
		String dia;
		String vol;
		String pcs;
		while (sheet.getRow(linhaDia) != null &&
				sheet.getRow(linhaDia).getCell(colDia) != null &&
				!("".equals(buscaValorPlanilha(sheet.getRow(linhaDia).getCell(colDia), false))) ) {
			
			try {
				dia = sheet.getRow(linhaDia).getCell(colDia).getStringCellValue().split("-")[1].trim();
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(Constantes.CITY_GATE_IMPORTACAO_ERRO_CELL_DIA + " Erro na linha " + linhaDia + " da Planilha.");
			}
			
			vol = buscaValorPlanilha(sheet.getRow(linhaDia).getCell(colVol), true);
			if (vol == null) {
				vol = "";
			}
			
			pcs = buscaValorPlanilha(sheet.getRow(linhaDia).getCell(colPCS), true);
			if (pcs == null) {
				pcs = "";
			}
			
			if ( (Integer.parseInt(dia) >= diaInicial) && (Integer.parseInt(dia) <= diaFinal) ) {
				calMedicao.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dia));
				if (this.permitirAtualizacaoCityGateMedicao(dataLeituraFaturada,
						calMedicao.getTime())) {
					vo.getLinha().put(Integer.parseInt(dia), new PlanilhaVolumePcsLinhaVO(dia, vol, pcs));
				}
			}
			linhaDia++;
		}
		return vo;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ControladorCityGateMedicaoImpl#
	 * atualizarImportacaoPlanilhaCityGateMedicao(java
	 * .util.Collection)
	 */
	@Override
	public void atualizarImportacaoPlanilhaCityGateMedicao(Collection<CityGateMedicao> listaCityGateMedicoes)
					throws NegocioException {
		for (CityGateMedicao medicao : listaCityGateMedicoes) {
			if (medicao.getChavePrimaria() <= 0) {
				this.inserir(medicao);
			} else {	
				this.getSession().flush();
				this.getSession().clear();
				try {
					this.atualizar(medicao);
				} catch (ConcorrenciaException e) {
					LOG.error(e.getLocalizedMessage(), e);
					throw new NegocioException(e);
				}
			}
		}
	}
	
	private String converteNumericoBrasil(String valor) {
		String val = valor.replace(".","");
		val = val.replace(",",".");
		
		return val;
	}
	
	private String buscaValorPlanilha(Cell celula, Boolean isNumber) {
		String valor = "";
		switch (celula.getCellType()) {
			case Cell.CELL_TYPE_NUMERIC:
				valor = String.valueOf(celula.getNumericCellValue());
				break;
			case Cell.CELL_TYPE_STRING:
				valor = celula.getStringCellValue();
				if (isNumber) {
					valor = converteNumericoBrasil(valor);
				}
				break;
			default:
				break;
		}
		return valor;
	}
	
	@Override
	public Calendar extrairPeriodoCalendar(PlanilhaVolumePcsVO planilha) throws NegocioException {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat
		formatoMes = new SimpleDateFormat("MMM yyyy", new Locale("pt", "BR"));
		try {
			Date dtPeriodoPlanilha = formatoMes.parse(planilha.getMes() + " " + planilha.getAno());
			calendar.setTime(dtPeriodoPlanilha);
		} catch (ParseException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(Constantes.CITY_GATE_IMPORTACAO_ERRO_CELL_PERIODO + 
					" Período na Planilha: " + planilha.getMes() + "/" + planilha.getAno());
		}
		return calendar;
	}

}
