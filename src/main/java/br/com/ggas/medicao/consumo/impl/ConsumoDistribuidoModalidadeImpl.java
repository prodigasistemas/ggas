/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
import br.com.ggas.medicao.consumo.ConsumoDistribuidoModalidade;
import br.com.ggas.medicao.consumo.ModalidadeDataQuantidade;

/**
 * Classe Consumo Distribuido Modalidade.
 * 
 * 
 * @author gmatos
 * 
 */
public class ConsumoDistribuidoModalidadeImpl implements ConsumoDistribuidoModalidade {

	private static final long serialVersionUID = -5712380427036739028L;

	private ContratoPontoConsumoModalidade contratoPontoConsumoModalidade;

	private ContratoPontoConsumoPenalidade penalidadeToPComMenorPeriodicidade;

	private ContratoPontoConsumoPenalidade penalidadeSoPComMenorPeriodicidade;

	private ContratoPontoConsumoPenalidade penalidadeToPComMaiorPeriodicidade;

	private ContratoPontoConsumoPenalidade penalidadeSoPComMaiorPeriodicidade;

	private ContratoPontoConsumoPenalidade penalidadePeriodicidadeToP;

	private ContratoPontoConsumoPenalidade penalidadePeriodicidadeSoP;

	private ContratoPontoConsumoPenalidade penalidadePeriodicidadeRetiradaMaior;

	private ContratoPontoConsumoPenalidade penalidadeRetiradaMaiorComMenorPeriodicidade;

	private ContratoPontoConsumoPenalidade penalidadeRetiradaMaiorComMaiorPeriodicidade;

	private ContratoPontoConsumoPenalidade penalidadePeriodicidadeRetiradaMenor;

	private ContratoPontoConsumoPenalidade penalidadeRetiradaMenorComMenorPeriodicidade;

	private ContratoPontoConsumoPenalidade penalidadeRetiradaMenorComMaiorPeriodicidade;

	private Boolean retiradaAutomatica;

	private BigDecimal volumeQDPMedia;

	private BigDecimal volumeQDCMedia;

	private BigDecimal volumeQDP;

	private BigDecimal volumeQDC;

	private BigDecimal volumeQDR;

	private BigDecimal volumeQR;

	private BigDecimal volumeQL;

	private BigDecimal volumeQNR;

	private Collection<ModalidadeDataQuantidade> modalidadeDataQuantidades = new HashSet<ModalidadeDataQuantidade>();

	private PontoConsumo pontoConsumo;

	/**
	 * @return the penalidadePeriodicidadeToP
	 */
	@Override
	public ContratoPontoConsumoPenalidade getPenalidadePeriodicidadeToP() {

		return penalidadePeriodicidadeToP;
	}

	/**
	 * @param penalidadePeriodicidadeToP
	 *            the penalidadePeriodicidadeToP to set
	 */
	@Override
	public void setPenalidadePeriodicidadeToP(ContratoPontoConsumoPenalidade penalidadePeriodicidadeToP) {

		this.penalidadePeriodicidadeToP = penalidadePeriodicidadeToP;
	}

	/**
	 * @return the penalidadePeriodicidadeSoP
	 */
	@Override
	public ContratoPontoConsumoPenalidade getPenalidadePeriodicidadeSoP() {

		return penalidadePeriodicidadeSoP;
	}

	/**
	 * @param penalidadePeriodicidadeSoP
	 *            the penalidadePeriodicidadeSoP to set
	 */
	@Override
	public void setPenalidadePeriodicidadeSoP(ContratoPontoConsumoPenalidade penalidadePeriodicidadeSoP) {

		this.penalidadePeriodicidadeSoP = penalidadePeriodicidadeSoP;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ConsumoDistribuidoModalidade
	 * #getPenalidadeSoPComMenorPeriodicidade()
	 */
	@Override
	public ContratoPontoConsumoPenalidade getPenalidadeSoPComMenorPeriodicidade() {

		return penalidadeSoPComMenorPeriodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ConsumoDistribuidoModalidade
	 * #setPenalidadeSoPComMenorPeriodicidade
	 * (br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoPenalidade)
	 */
	@Override
	public void setPenalidadeSoPComMenorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeSoPComMenorPeriodicidade) {

		this.penalidadeSoPComMenorPeriodicidade = penalidadeSoPComMenorPeriodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #getContratoPontoConsumoModalidade()
	 */
	@Override
	public ContratoPontoConsumoModalidade getContratoPontoConsumoModalidade() {

		return contratoPontoConsumoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #setContratoPontoConsumoModalidade
	 * (br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoModalidade)
	 */
	@Override
	public void setContratoPontoConsumoModalidade(ContratoPontoConsumoModalidade contratoPontoConsumoModalidade) {

		this.contratoPontoConsumoModalidade = contratoPontoConsumoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #getPenalidadeComMenorPeriodicidade()
	 */
	@Override
	public ContratoPontoConsumoPenalidade getPenalidadeToPComMenorPeriodicidade() {

		return penalidadeToPComMenorPeriodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #setPenalidadeComMenorPeriodicidade
	 * (br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoPenalidade)
	 */
	@Override
	public void setPenalidadeToPComMenorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeComMenorPeriodicidade) {

		this.penalidadeToPComMenorPeriodicidade = penalidadeComMenorPeriodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #getVolumeQDPMedia()
	 */
	@Override
	public BigDecimal getVolumeQDPMedia() {

		return volumeQDPMedia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #setVolumeQDPMedia(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDPMedia(BigDecimal volumeQDPMedia) {

		this.volumeQDPMedia = volumeQDPMedia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #getVolumeQDCMedia()
	 */
	@Override
	public BigDecimal getVolumeQDCMedia() {

		return volumeQDCMedia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #setVolumeQDCMedia(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDCMedia(BigDecimal volumeQDCMedia) {

		this.volumeQDCMedia = volumeQDCMedia;
	}

	@Override
	public BigDecimal getVolumeQDP() {

		return volumeQDP;
	}

	@Override
	public void setVolumeQDP(BigDecimal volumeQDP) {

		this.volumeQDP = volumeQDP;
	}

	@Override
	public BigDecimal getVolumeQDC() {

		return volumeQDC;
	}

	@Override
	public void setVolumeQDC(BigDecimal volumeQDC) {

		this.volumeQDC = volumeQDC;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade#getVolumeQDR()
	 */
	@Override
	public BigDecimal getVolumeQDR() {

		return volumeQDR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #setVolumeQDR(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDR(BigDecimal volumeQDR) {

		this.volumeQDR = volumeQDR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade#getVolumeQR()
	 */
	@Override
	public BigDecimal getVolumeQR() {

		return volumeQR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #setVolumeQR(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQR(BigDecimal volumeQR) {

		this.volumeQR = volumeQR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade#getVolumeQL()
	 */
	@Override
	public BigDecimal getVolumeQL() {

		return volumeQL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #setVolumeQL(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQL(BigDecimal volumeQL) {

		this.volumeQL = volumeQL;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade#getVolumeQNR()
	 */
	@Override
	public BigDecimal getVolumeQNR() {

		return volumeQNR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #setVolumeQNR(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQNR(BigDecimal volumeQNR) {

		this.volumeQNR = volumeQNR;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #getRetiradaAutomatica()
	 */
	@Override
	public Boolean getRetiradaAutomatica() {

		return retiradaAutomatica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #setRetiradaAutomatica(java.lang.Boolean)
	 */
	@Override
	public void setRetiradaAutomatica(Boolean retiradaAutomatica) {

		this.retiradaAutomatica = retiradaAutomatica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #getModalidadeDataQuantidades()
	 */
	@Override
	public Collection<ModalidadeDataQuantidade> getModalidadeDataQuantidades() {

		return modalidadeDataQuantidades;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ConsumoDistribuidoModalidade
	 * #setModalidadeDataQuantidades
	 * (java.util.Collection)
	 */
	@Override
	public void setModalidadeDataQuantidades(Collection<ModalidadeDataQuantidade> modalidadeDataQuantidades) {

		this.modalidadeDataQuantidades = modalidadeDataQuantidades;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof ConsumoDistribuidoModalidadeImpl)) {
			return false;
		}
		if (this.contratoPontoConsumoModalidade != null) {
			ConsumoDistribuidoModalidadeImpl that = (ConsumoDistribuidoModalidadeImpl) obj;
			return new EqualsBuilder().append(this.contratoPontoConsumoModalidade.getChavePrimaria(),
							that.contratoPontoConsumoModalidade.getChavePrimaria()).isEquals();
		} else {
			return super.equals(obj);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		if (this.contratoPontoConsumoModalidade != null) {
			return new HashCodeBuilder(3, 5).append(this.contratoPontoConsumoModalidade.getChavePrimaria()).toHashCode();
		} else {
			return super.hashCode();
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ConsumoDistribuidoModalidade
	 * #getPenalidadeToPComMaiorPeriodicidade()
	 */
	@Override
	public ContratoPontoConsumoPenalidade getPenalidadeToPComMaiorPeriodicidade() {

		return penalidadeToPComMaiorPeriodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ConsumoDistribuidoModalidade
	 * #setPenalidadeToPComMaiorPeriodicidade
	 * (br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoPenalidade)
	 */
	@Override
	public void setPenalidadeToPComMaiorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeToPComMaiorPeriodicidade) {

		this.penalidadeToPComMaiorPeriodicidade = penalidadeToPComMaiorPeriodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ConsumoDistribuidoModalidade
	 * #getPenalidadeSoPComMaiorPeriodicidade()
	 */
	@Override
	public ContratoPontoConsumoPenalidade getPenalidadeSoPComMaiorPeriodicidade() {

		return penalidadeSoPComMaiorPeriodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.
	 * ConsumoDistribuidoModalidade
	 * #setPenalidadeSoPComMaiorPeriodicidade
	 * (br.com.ggas.contrato.contrato.
	 * ContratoPontoConsumoPenalidade)
	 */
	@Override
	public void setPenalidadeSoPComMaiorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeSoPComMaiorPeriodicidade) {

		this.penalidadeSoPComMaiorPeriodicidade = penalidadeSoPComMaiorPeriodicidade;
	}

	@Override
	public ContratoPontoConsumoPenalidade getPenalidadePeriodicidadeRetiradaMaior() {

		return penalidadePeriodicidadeRetiradaMaior;
	}

	@Override
	public void setPenalidadePeriodicidadeRetiradaMaior(ContratoPontoConsumoPenalidade penalidadePeriodicidadeRetiradaMaior) {

		this.penalidadePeriodicidadeRetiradaMaior = penalidadePeriodicidadeRetiradaMaior;
	}

	@Override
	public ContratoPontoConsumoPenalidade getPenalidadeRetiradaMaiorComMenorPeriodicidade() {

		return penalidadeRetiradaMaiorComMenorPeriodicidade;
	}

	@Override
	public void setPenalidadeRetiradaMaiorComMenorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeRetiradaMaiorComMenorPeriodicidade) {

		this.penalidadeRetiradaMaiorComMenorPeriodicidade = penalidadeRetiradaMaiorComMenorPeriodicidade;
	}

	@Override
	public ContratoPontoConsumoPenalidade getPenalidadeRetiradaMaiorComMaiorPeriodicidade() {

		return penalidadeRetiradaMaiorComMaiorPeriodicidade;
	}

	@Override
	public void setPenalidadeRetiradaMaiorComMaiorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeRetiradaMaiorComMaiorPeriodicidade) {

		this.penalidadeRetiradaMaiorComMaiorPeriodicidade = penalidadeRetiradaMaiorComMaiorPeriodicidade;
	}

	@Override
	public ContratoPontoConsumoPenalidade getPenalidadePeriodicidadeRetiradaMenor() {

		return penalidadePeriodicidadeRetiradaMenor;
	}

	@Override
	public void setPenalidadePeriodicidadeRetiradaMenor(ContratoPontoConsumoPenalidade penalidadePeriodicidadeRetiradaMenor) {

		this.penalidadePeriodicidadeRetiradaMenor = penalidadePeriodicidadeRetiradaMenor;
	}

	@Override
	public ContratoPontoConsumoPenalidade getPenalidadeRetiradaMenorComMenorPeriodicidade() {

		return penalidadeRetiradaMenorComMenorPeriodicidade;
	}

	@Override
	public void setPenalidadeRetiradaMenorComMenorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeRetiradaMenorComMenorPeriodicidade) {

		this.penalidadeRetiradaMenorComMenorPeriodicidade = penalidadeRetiradaMenorComMenorPeriodicidade;
	}

	@Override
	public ContratoPontoConsumoPenalidade getPenalidadeRetiradaMenorComMaiorPeriodicidade() {

		return penalidadeRetiradaMenorComMaiorPeriodicidade;
	}

	@Override
	public void setPenalidadeRetiradaMenorComMaiorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeRetiradaMenorComMaiorPeriodicidade) {

		this.penalidadeRetiradaMenorComMaiorPeriodicidade = penalidadeRetiradaMenorComMaiorPeriodicidade;
	}

	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

}
