/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorHistoricoConsumoImpl representa uma ControladorHistoricoConsumo no sistema.
 *
 * @since 01/10/2009
 *
 */

package br.com.ggas.medicao.consumo.impl;

import static br.com.ggas.web.contrato.contrato.ContratoAction.ADITADO;
import static br.com.ggas.web.contrato.contrato.ContratoAction.ATIVO;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DateType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.ProcessoDocumento;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ClienteImovel;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoCityGate;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.TipoRelacionamentoClienteImovel;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoCityGateImpl;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.localidade.ControladorLocalidade;
import br.com.ggas.cadastro.localidade.ControladorSetorComercial;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.Rede;
import br.com.ggas.cadastro.localidade.RedeDistribuicaoTronco;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.ControladorRede;
import br.com.ggas.cadastro.operacional.Tronco;
import br.com.ggas.cadastro.operacional.impl.CityGateImpl;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSAmostragem;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPCSIntervalo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.anomalia.HistoricoAnomaliaFaturamento;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.InfraestruturaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.AnormalidadeSegmentoParametro;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.anormalidade.impl.AnormalidadeConsumoImpl;
import br.com.ggas.medicao.anormalidade.negocio.ProcessadorAcaoAnormalidadeLeitura;
import br.com.ggas.medicao.consumo.CityGateMedicao;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.FaixaConsumoVariacao;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.ImovelPCSZ;
import br.com.ggas.medicao.consumo.IntervaloPCS;
import br.com.ggas.medicao.consumo.TemperaturaPressaoMedicao;
import br.com.ggas.medicao.consumo.TipoComposicaoConsumo;
import br.com.ggas.medicao.consumo.TipoConsumo;
import br.com.ggas.medicao.consumo.UnidadeConversao;
import br.com.ggas.medicao.cromatografia.dominio.Cromatografia;
import br.com.ggas.medicao.cromatografia.negocio.ControladorCromatografia;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.LogAnaliseLeituraConsumoTemp;
import br.com.ggas.medicao.leitura.SituacaoLeitura;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.leitura.TipoMedicao;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.InformacaoClientePontoConsumo;
import br.com.ggas.util.Agrupador;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.ThrowableRunnable;
import br.com.ggas.util.Util;
import br.com.ggas.web.faturamento.leitura.dto.ConsumoHistoricoDTO;
import br.com.ggas.web.medicao.consumo.HistoricoConsumoVO;
import groovy.util.Eval;

class ControladorHistoricoConsumoImpl extends ControladorNegocioImpl implements ControladorHistoricoConsumo {

	private static final int CONSTANTE_NUMERO_ZERO = 0;

	private static final int CONSTANTE_NUMERO_CEM = 100;

	private static final int INCREMENTO_DATA_INICIO_REFERENCIA = 2;

	private static final int TAMANHO_MAXIMO_LISTA = 9999;

	private static final String PONTO_CONSUMO_CHAVE_PRIMARIA = "pontoConsumo.chavePrimaria";

	private static final String PT_CONS = "PT_CONS";

	private static final int ESCALA = 8;

	private static final int CONSTANTE_NUMERO_UM = 1;

	private static final int CONSTANTE_NUMERO_DOIS = 2;

	private static final int CONSTANTE_NUMERO_TRES = 3;
	
	private static final int CONSTANTE_NUMERO_QUATRO = 4;
	
	private static final int DOZE = 12;

	private static final String INDICADOR_FATURAMENTO = "indicadorFaturamento";

	private static final String INDICADOR_CONSUMO_CICLO = "indicadorConsumoCiclo";

	private static final String NUMERO_CICLO = "numeroCiclo";

	private static final String CONSTANTE_ANO_MES_FATURAMENTO = "anoMesFaturamento";

	private static final String CONSTANTE_ID_GRUPO_FATURAMENTO = "idGrupoFaturamento";

	private static final String CONSTANTE_ID_ROTA = "idRota";

	private static final String ERRO_NEGOCIO_DIAS_CONSUMO_FORA_PERIODICIDADE = "ERRO_NEGOCIO_DIAS_CONSUMO_FORA_PERIODICIDADE";

	private static final String CODIGO_ANORMALIDADE_CONSUMO_ZERO = "CODIGO_ANORMALIDADE_CONSUMO_PONTO_CONSUMO_ZERO";

	private static final String ERRO_NEGOCIO_RELATORIO_SEM_DADOS = "ERRO_NEGOCIO_RELATORIO_SEM_DADOS";

	private static final String RELATORIO_CONSISTIR_LEITURA_CONSUMO = "relatorioAnaliseConsistirLeituraConsumo.jasper";
	
	private static final String FROM = " from ";

	/**
	 * @deprecated
	 */
	@Deprecated
	private static final String CODIGO_TIPO_COMPOSICAO_CONSUMO_PADRAO = "CODIGO_TIPO_COMPOSICAO_CONSUMO_PADRAO";

	private static Integer PARAMETRO_PONTO_CONSUMO = 1;

	private static Integer PARAMETRO_DISTRIBUIDORA = 2;

	private static Integer PARAMETRO_SUPRIDORA = 3;

	private static Integer PARAMETRO_CITY_GATE = 4;

	private static final String CICLO = "ciclo";

	private static final String REFERENCIA = "referencia";

	private static final String PONTO_CONSUMO = "pontoConsumo";

	private static final String HISTORICO_MEDICAO_ANTERIOR = "historicoMedicaoAnterior";

	private static final String CONSUMO_MEDIO = "consumoMedio";

	private static final String CONSUMO_APURADO_MEDIO = "consumoApuradoMedio";

	private static final String ATRIBUTO_SELECAO_PCS_SUPRIDORA =
			"cityGateMedicao.medidaPCSSupridora, cityGateMedicao.medidaVolumeSupridora";

	private static final String ATRIBUTO_SELECAO_PCS_DISTRIBUIDORA =
			"cityGateMedicao.medidaPCSDistribuidora," + " cityGateMedicao.medidaVolumeDistribuidora";

	private static final String ATRIBUTO_SELECAO_VOLUME_PCS_FIXO = "cityGateMedicao.medidaVolumeDistribuidora";

	private static final String ERRO_NEGOCIO_CONTRATO_NAO_POSSUI_PERIODICIDADE = "ERRO_NEGOCIO_CONTRATO_NAO_POSSUI_PERIODICIDADE";

	private static final int MODALIDADE_MEDICAO_INEXISTENTE = -1;

	private static final Logger LOG = Logger.getLogger(ControladorHistoricoConsumoImpl.class);

	private ControladorParametroSistema controladorParametrosSistema;

	private Map<Long, TipoConsumo> tipoConsumoCache = new HashMap<>();

	private Map<String, UnidadeConversao> unidadeConversaoCache = new HashMap<>();

	private Map<Long, IntervaloPCS> intervaloPCSCache = new HashMap<>();

	@Autowired
	private ControladorCromatografia controladorCromatografia;

	@Autowired
	private ProcessadorAcaoAnormalidadeLeitura processadorAcaoAnormalidadeLeitura;

	private Integer anoMesFaturamento;

	private Integer numeroCiclo;

	private static final Comparator<CityGateMedicao> COMPARATOR_CITY_GATE_MEDICAO = new Comparator<CityGateMedicao>() {

		@Override
		public int compare(CityGateMedicao cg1, CityGateMedicao cg2) {

			return -cg1.getDataMedicao().compareTo(cg2.getDataMedicao());
		}
	};

	private static final Integer MAX_IN = 9999;

	static {
		// Hack para executar o calculo de
		// formulas mais rápido
		Eval.me(StringUtils.EMPTY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.geral.negocio.impl. ControladorNegocioImpl# getClasseEntidade()
	 */

	private Class<?> getClasseEntidadeFaixaConsumoVariacao() {

		return ServiceLocator.getInstancia().getClassPorID(FaixaConsumoVariacao.BEAN_ID_FAIXA_CONSUMO_VARIACAO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	private Class<?> getClasseTipoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(TipoConsumo.BEAN_ID_TIPO_CONSUMO);
	}

	private Class<?> getClasseHistoricoAnomaliaFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoAnomaliaFaturamento.BEAN_ID_HISTORICO_ANOMALIA_FATURAMENTO);
	}

	private Class<?> getClasseHistoricoMedicao() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoMedicao.BEAN_ID_MEDICAO_HISTORICO);
	}

	private Class<?> getClasseAnomalidadeConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(AnormalidadeConsumo.BEAN_ID_ANORMALIDADE_CONSUMO);
	}

	private Class<?> getClasseUnidadeConversao() {

		return ServiceLocator.getInstancia().getClassPorID(UnidadeConversao.BEAN_ID_UNIDADE_CONVERSAO);
	}

	private Class<?> getClasseImovelPCSZ() {

		return ServiceLocator.getInstancia().getClassPorID(ImovelPCSZ.BEAN_ID_IMOVEL_PCS_Z);
	}

	private Class<?> getClasseCityGateMedicao() {

		return ServiceLocator.getInstancia().getClassPorID(CityGateMedicao.BEAN_ID_CITY_GATE_MEDICAO);
	}

	private Class<?> getClasseContratoPontoConsumoPCSAmostragem() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoPCSAmostragem.BEAN_ID_CONTRATO_PONTO_CONSUMO_PCS_AMOSTRAGEM);
	}

	private Class<?> getClasseContratoPontoConsumoPCSIntervalo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumoPCSIntervalo.BEAN_ID_CONTRATO_PONTO_CONSUMO_PCS_INTERVALO);
	}

	private Class<?> getClasseIntervaloPCS() {

		return ServiceLocator.getInstancia().getClassPorID(IntervaloPCS.BEAN_ID_INTERVALO_PCS);
	}

	private Class<?> getClassePontoConsumoCityGate() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumoCityGate.BEAN_ID_PONTO_CONSUMO_CITY_GATE);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	public void setControladorParametroSistema(ControladorParametroSistema controladorParametroSistema) {

		this.controladorParametrosSistema = controladorParametroSistema;
	}

	/**
	 * Consulta um tipo de consumo por chave primária. Caso o tipo de consumo já tenha sido consultado, obtém o mesmo do
	 * {@code tipoConsumoCache}.
	 * 
	 * @param chavePrimaria
	 * @return tipoConsumo
	 */
	@Override
	public TipoConsumo obterTipoConsumo(Long chavePrimaria) throws NegocioException {
		TipoConsumo tipoConsumo;
		if (tipoConsumoCache.containsKey(chavePrimaria)) {
			tipoConsumo = tipoConsumoCache.get(chavePrimaria);
		} else {
			tipoConsumo = (TipoConsumo) this.obter(chavePrimaria, this.getClasseTipoConsumo());
			if (tipoConsumo != null) {
				tipoConsumoCache.put(tipoConsumo.getChavePrimaria(), tipoConsumo);
			}
		}
		return tipoConsumo;
	}

	/**
	 * Consulta um IntervaloPCS por chave primária. Caso o IntervaloPCS já tenha sido consultado, obtém o mesmo do
	 * {@code intervaloPCSCache}.
	 * 
	 * @param chavePrimaria
	 * @return tipoConsumo
	 */
	@Override
	public IntervaloPCS obterIntervaloPCS(Long chavePrimaria) throws NegocioException {
		IntervaloPCS intervaloPCS = null;
		if (intervaloPCSCache.containsKey(chavePrimaria)) {
			intervaloPCS = intervaloPCSCache.get(chavePrimaria);
		} else {
			intervaloPCS = (IntervaloPCS) this.obter(chavePrimaria, this.getClasseIntervaloPCS());
			if (intervaloPCS != null) {
				intervaloPCSCache.put(intervaloPCS.getChavePrimaria(), intervaloPCS);
			}
		}
		return intervaloPCS;
	}

	private Class<?> getClasseTipoComposicaoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(TipoComposicaoConsumo.BEAN_ID_TIPO_COMPOSICAO_CONSUMO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#obterTipoComposicaoConsumo(java.lang.Long)
	 */
	@Override
	public TipoComposicaoConsumo obterTipoComposicaoConsumo(Long chavePrimaria) throws NegocioException {

		return (TipoComposicaoConsumo) this.obter(chavePrimaria, this.getClasseTipoComposicaoConsumo());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# listarFaixasConsumoPorSegmento (br.com.ggas.cadastro.imovel.Segmento)
	 */
	@Override
	public Collection<FaixaConsumoVariacao> listarFaixasConsumoPorSegmentoFaixaConsumo(Segmento segmento, BigDecimal consumoMedio) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeFaixaConsumoVariacao().getSimpleName());
		hql.append(" faixa where ");
		hql.append(" habilitado = true ");
		hql.append(" and faixa.segmento.chavePrimaria = ?");
		hql.append(" and ? between faixa.faixaInferior and faixa.faixaSuperior");
		hql.append(" order by faixa.faixaInferior");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CONSTANTE_NUMERO_ZERO, segmento.getChavePrimaria());
		query.setBigDecimal(CONSTANTE_NUMERO_UM, consumoMedio);

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# obterFaixaConsumoPorSegmentoFaixaConsumo
	 * (br.com.ggas.cadastro.imovel.Segmento, java.math.BigDecimal)
	 */
	@Override
	public FaixaConsumoVariacao obterFaixaConsumoPorSegmentoFaixaConsumo(Segmento segmento, BigDecimal consumoMedio) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeFaixaConsumoVariacao().getSimpleName());
		hql.append(" faixa where ");
		hql.append(" habilitado = true ");
		hql.append(" and faixa.segmento.chavePrimaria = ?");
		hql.append(" and ? between faixa.faixaInferior and faixa.faixaSuperior");
		hql.append(" order by faixa.faixaInferior");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CONSTANTE_NUMERO_ZERO, segmento.getChavePrimaria());
		query.setBigDecimal(CONSTANTE_NUMERO_UM, consumoMedio);

		return (FaixaConsumoVariacao) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#obterPorPontoConsumo(br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public HistoricoConsumo obterPorPontoConsumo(PontoConsumo pontoConsumo) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico where ");
		hql.append(" historico.habilitado = true ");
		hql.append(" and historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and historico.chavePrimaria = (select MAX(h.chavePrimaria) ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" h where ");
		hql.append(" h.habilitado = true ");
		hql.append(" and h.pontoConsumo.chavePrimaria = :idPontoConsumo)");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idPontoConsumo", pontoConsumo.getChavePrimaria());
		return (HistoricoConsumo) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# calcularConsumoMedio(br.com.ggas.cadastro.imovel.PontoConsumo,
	 * br.com.ggas.contrato.ContratoPontoConsumo, int, int)
	 */
	@Override
	public Map<String, BigDecimal> calcularConsumoMedio(PontoConsumo pontoConsumo, ContratoPontoConsumo contrato, int referenciaAtual,
			int cicloAtual, int diasConsumoAtual) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		if (pontoConsumo == null) {
			throw new NegocioException(Constantes.CAMPOS_OBRIGATORIOS);
		}

		Map<String, BigDecimal> consumos = new HashMap<String, BigDecimal>();
		BigDecimal consumo = BigDecimal.ZERO;
		BigDecimal consumoApurado = BigDecimal.ZERO;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		int numeroCiclos = CONSTANTE_NUMERO_ZERO;
		String codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL);

		TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));

		if (pontoConsumo.getSegmento().getNumeroCiclos() != null) {
			numeroCiclos = pontoConsumo.getSegmento().getNumeroCiclos();
		}

		int quantidadeCiclo = CONSTANTE_NUMERO_ZERO;
		if (contrato != null) {

			if (contrato.getPeriodicidade() != null) {
				quantidadeCiclo = contrato.getPeriodicidade().getQuantidadeCiclo();
			}
		} else {

			if (pontoConsumo.getRamoAtividade().getPeriodicidade() != null) {

				quantidadeCiclo = pontoConsumo.getRamoAtividade().getPeriodicidade().getQuantidadeCiclo();

			} else {

				quantidadeCiclo = pontoConsumo.getSegmento().getPeriodicidade().getQuantidadeCiclo();

			}

		}

		Collection<HistoricoConsumo> colecaoHistoricoConsumo =
				this.obterUltimosHistoricosConsumo(pontoConsumo, referenciaAtual, cicloAtual, numeroCiclos, quantidadeCiclo);
		if (colecaoHistoricoConsumo != null && !colecaoHistoricoConsumo.isEmpty()) {

			// Somar os volumes de consumos retornados e dividir pela qtd de
			// consumos retornados considerando a
			BigDecimal somaConsumo = BigDecimal.ZERO;
			BigDecimal somaConsumoApurado = BigDecimal.ZERO;

			int quantidadeDiasConsumo = CONSTANTE_NUMERO_ZERO;
			for (HistoricoConsumo historicoConsumo : colecaoHistoricoConsumo) {

				boolean isConsumoReal = historicoConsumo.getTipoConsumo().getChavePrimaria() == tipoConsumo.getChavePrimaria();

				consumoApurado = historicoConsumo.getConsumoApurado();
				consumo = historicoConsumo.getConsumo();

				// No cálculo da média de consumo, não devem ser considerados
				// consumos iguais a zero, ou consumo que não tenham sido reais.
				if (consumo != null && (consumo.compareTo(BigDecimal.ZERO) != CONSTANTE_NUMERO_ZERO && isConsumoReal)) {

					somaConsumo = somaConsumo.add(consumo);
				}

				if (consumoApurado != null && (consumoApurado.compareTo(BigDecimal.ZERO) != CONSTANTE_NUMERO_ZERO && isConsumoReal)) {

					somaConsumoApurado = somaConsumoApurado.add(consumoApurado);
				}
				
				if (historicoConsumo.getDiasConsumo() != null && isConsumoReal) {
					quantidadeDiasConsumo += historicoConsumo.getDiasConsumo().intValue();
				}
			}

			if (quantidadeDiasConsumo != CONSTANTE_NUMERO_ZERO) {
				String parametroArredondamento = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_ARREDONDAMENTO_CONSUMO);

				consumos.put("consumoMedioDiario",
						somaConsumo.divide(new BigDecimal(quantidadeDiasConsumo), ESCALA, Integer.parseInt(parametroArredondamento)));

				consumos.put("consumoMedio",
						somaConsumo.divide(new BigDecimal(quantidadeDiasConsumo), ESCALA, Integer.parseInt(parametroArredondamento))
								.multiply(new BigDecimal(diasConsumoAtual)));

				consumos.put("consumoMedioApuradoDiario", somaConsumoApurado.divide(new BigDecimal(quantidadeDiasConsumo), ESCALA,
						Integer.parseInt(parametroArredondamento)));

				consumos.put("consumoMedioApurado",
						somaConsumoApurado.divide(new BigDecimal(quantidadeDiasConsumo), ESCALA, Integer.parseInt(parametroArredondamento))
								.multiply(new BigDecimal(diasConsumoAtual)));
			}
		}
		return consumos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#gerarRelatorioAnaliseConsumo(java.lang.String[], java.util.Map)
	 */
	@Override
	public byte[] gerarRelatorioAnaliseConsumo(String[] chavesPrimariasAux, Map<String, Object> filtro) throws NegocioException {

		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();

		Collection<PontoConsumo> listaPontoConsumo = controladorPontoConsumo.listarPontosConsumo(chavesPrimariasAux);

		Collection<AnaliseConsumoVO> listaAnaliseConsumoVO = new ArrayList<AnaliseConsumoVO>();
		for (PontoConsumo pontoConsumo : listaPontoConsumo) {

			HistoricoConsumo ultimoHistoricoConsumo = obterUltimoHistoricoConsumo(pontoConsumo);

			AnaliseConsumoVO analiseConsumoVO = new AnaliseConsumoVO();
			analiseConsumoVO.setLocalidade(controladorPontoConsumo.obterLocalidadePontoConsumo(pontoConsumo));
			if ((pontoConsumo.getRota() != null) && (pontoConsumo.getRota().getLeiturista() != null)) {
				analiseConsumoVO.setLeiturista(pontoConsumo.getRota().getLeiturista().getFuncionario().getNome());
			}

			if ((pontoConsumo.getRota() != null) && (pontoConsumo.getRota().getSetorComercial() != null)) {
				analiseConsumoVO.setSetor(pontoConsumo.getRota().getSetorComercial().getDescricao());
			} else {
				analiseConsumoVO.setSetor(" ");
			}

			if (pontoConsumo.getRota() != null) {
				analiseConsumoVO.setGrupo(pontoConsumo.getRota().getGrupoFaturamento().getDescricao());
			}

			analiseConsumoVO.setDescricaoPontoConsumo(pontoConsumo.getDescricao());
			analiseConsumoVO.setMatriculaImovelAssociado(String.valueOf(pontoConsumo.getImovel().getChavePrimaria()));
			analiseConsumoVO.setSegmento(pontoConsumo.getSegmento().getDescricaoAbreviada());
			analiseConsumoVO.setSituacaoPontoConsumo(pontoConsumo.getSituacaoConsumo().getDescricao());
			if (pontoConsumo.getInstalacaoMedidor() != null) {
				analiseConsumoVO.setMedidorInstaladoPontoConsumo(pontoConsumo.getInstalacaoMedidor().getMedidor().getNumeroSerie());
				analiseConsumoVO.setDataInstalacaoMedidor(
						Util.converterDataParaStringSemHora(pontoConsumo.getInstalacaoMedidor().getData(), Constantes.FORMATO_DATA_BR));
			}
			analiseConsumoVO.setNome(preencherNomeClienteAnaliseConsumoVO(pontoConsumo.getImovel()));
			analiseConsumoVO.setEnderecoPontoConsumo(pontoConsumo.getEnderecoFormatado());
			analiseConsumoVO.setQuadra(String.valueOf(pontoConsumo.getImovel().getQuadraFace().getQuadra().getNumeroQuadra()));
			analiseConsumoVO.setQuadraFace(String.valueOf(pontoConsumo.getImovel().getQuadraFace().getQuadraFaceFormatada()));
			if (pontoConsumo.getRota() != null) {
				analiseConsumoVO.setRota(pontoConsumo.getRota().getNumeroRota());
			}
			if (pontoConsumo.getNumeroSequenciaLeitura() != null) {
				analiseConsumoVO.setSequenciaLeituraRota(String.valueOf(pontoConsumo.getNumeroSequenciaLeitura()));
			}

			if (ultimoHistoricoConsumo != null) {
				if ((ultimoHistoricoConsumo.getHistoricoAtual() != null)
						&& (ultimoHistoricoConsumo.getHistoricoAtual().getDataLeituraInformada() != null)) {
					analiseConsumoVO.setDataLeituraInformada(Util.converterDataParaStringSemHora(
							ultimoHistoricoConsumo.getHistoricoAtual().getDataLeituraInformada(), Constantes.FORMATO_DATA_BR));
				}

				if ((ultimoHistoricoConsumo.getHistoricoAnterior() != null)
						&& (ultimoHistoricoConsumo.getHistoricoAnterior().getDataLeituraInformada() != null)) {
					analiseConsumoVO.setDataAnteriorLeituraInformada(Util.converterDataParaStringSemHora(
							ultimoHistoricoConsumo.getHistoricoAnterior().getDataLeituraInformada(), Constantes.FORMATO_DATA_BR));
				}
				analiseConsumoVO.preencherUltimasMedicoes(obterListaSubReportUltimasMedicoes(pontoConsumo, ultimoHistoricoConsumo));
			}

			listaAnaliseConsumoVO.add(analiseConsumoVO);
		}
		return RelatorioUtil.gerarRelatorioPDF(listaAnaliseConsumoVO, prepararParametroRelatorio(filtro), "relatorioAnaliseConsumo.jasper");
	}

	/**
	 * Preparar parametro relatorio.
	 *
	 * @param filtro the filtro
	 * @return the map
	 * @throws NumberFormatException the number format exception
	 * @throws NegocioException the negocio exception
	 */
	private Map<String, Object> prepararParametroRelatorio(Map<String, Object> filtro) throws NegocioException {
		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();
		ControladorCliente controladorCliente = ServiceLocator.getInstancia().getControladorCliente();
		ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();
		ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();
		ControladorEmpresa controladorEmpresa = ServiceLocator.getInstancia().getControladorEmpresa();

		Map<String, Object> parametros = new HashMap<String, Object>();
		Long idCliente = (Long) filtro.get("idCliente");
		if ((idCliente != null) && (idCliente > CONSTANTE_NUMERO_ZERO)) {
			Cliente cliente = (Cliente) controladorCliente.obter(idCliente);
			parametros.put("cliente", cliente.getNome());
		}

		colocarParametrosDoImovel(filtro, parametros);
		colocarParametrosRelacionadosAoCadastro(filtro, parametros);

		Long[] idsAnormalidadeLeitura = (Long[]) filtro.get("idsAnormalidadeLeitura");
		if (idsAnormalidadeLeitura != null && idsAnormalidadeLeitura.length > CONSTANTE_NUMERO_ZERO) {
			StringBuilder stringBuilder = new StringBuilder();
			String anormalidades = null;
			for (int i = CONSTANTE_NUMERO_ZERO; i < idsAnormalidadeLeitura.length; i++) {
				AnormalidadeLeitura anormalidadeLeitura = (AnormalidadeLeitura) controladorAnormalidade
						.obter(idsAnormalidadeLeitura[i]);
				stringBuilder.append(anormalidadeLeitura.getDescricao());
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			anormalidades = stringBuilder.toString();
			if (anormalidades.length() > CONSTANTE_NUMERO_ZERO) {
				parametros.put("anormalidadeLeitura", anormalidades.substring(CONSTANTE_NUMERO_ZERO,
						stringBuilder.toString().length() - CONSTANTE_NUMERO_DOIS));
			}
		}

		Long idTipoConsumo = (Long) filtro.get("idTipoConsumo");
		if ((idTipoConsumo != null) && (idTipoConsumo > CONSTANTE_NUMERO_ZERO)) {
			TipoConsumo tipoConsumo = obterTipoConsumo(idTipoConsumo);
			parametros.put("tipoConsumo", tipoConsumo.getDescricao());
		}

		Long idSituacaoPontoConsumo = (Long) filtro.get("idSituacaoPontoConsumo");
		if (idSituacaoPontoConsumo != null && idSituacaoPontoConsumo > CONSTANTE_NUMERO_ZERO) {
			SituacaoConsumo situacaoConsumo = controladorImovel.obterSituacaoConsumo(idSituacaoPontoConsumo);
			parametros.put("situacaoPontoConsumo", situacaoConsumo.getDescricao());
		}

		Long[] idsAnormalidadeConsumo = (Long[]) filtro.get("idsAnormalidadeConsumo");
		if (idsAnormalidadeConsumo != null && idsAnormalidadeConsumo.length > CONSTANTE_NUMERO_ZERO) {
			StringBuilder stringBuilder = new StringBuilder();
			String anormalidades = null;
			for (int i = CONSTANTE_NUMERO_ZERO; i < idsAnormalidadeConsumo.length; i++) {
				AnormalidadeConsumo anormalidadeConsumo = (AnormalidadeConsumo) controladorAnormalidade
						.obter(idsAnormalidadeConsumo[i]);
				stringBuilder.append(anormalidadeConsumo.getDescricao());
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			anormalidades = stringBuilder.toString();
			if (anormalidades.length() > CONSTANTE_NUMERO_ZERO) {
				parametros.put("anormalidadeConsumo", anormalidades.substring(CONSTANTE_NUMERO_ZERO,
						stringBuilder.toString().length() - CONSTANTE_NUMERO_DOIS));
			}
		}

		Long idGrupoFaturamento = (Long) filtro.get(CONSTANTE_ID_GRUPO_FATURAMENTO);
		if (idGrupoFaturamento != null && idGrupoFaturamento > CONSTANTE_NUMERO_ZERO) {
			GrupoFaturamento grupoFaturamento = controladorRota.obterGrupoFaturamento(idGrupoFaturamento);
			parametros.put("grupoFaturamento", grupoFaturamento.getDescricao());
		}

		Long[] idsRota = (Long[]) filtro.get("idsRota");
		if (CollectionUtils.isEmpty(Arrays.asList(idsRota))) {
			StringBuilder stringBuilder = new StringBuilder();
			for (Long idRota : idsRota) {
				Rota rota = (Rota) controladorRota.obter(idRota);
				stringBuilder.append(rota.getNumeroRota());
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			String rotas = stringBuilder.toString();
			if (stringBuilder.length() > CONSTANTE_NUMERO_ZERO) {
				parametros.put("rota", rotas.substring(CONSTANTE_NUMERO_ZERO, rotas.length() - CONSTANTE_NUMERO_DOIS));
			}
		}

		String consumoLido = (String) filtro.get("consumoLido");
		if (!StringUtils.isEmpty(consumoLido)) {
			parametros.put("consumoLido", consumoLido);
		}

		Map<String, Object> filtroEmpresa = new HashMap<String, Object>();
		filtroEmpresa.put("principal", Boolean.TRUE);
		Collection<Empresa> listaEmpresas = controladorEmpresa.consultarEmpresas(filtroEmpresa);
		for (Empresa empresa : listaEmpresas) {
			if (empresa.getLogoEmpresa() != null) {
				parametros.put("imagem", Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(empresa.getChavePrimaria()));
			}
		}
		return parametros;
	}

	/**
	 * Metodo complementar ao metodo {@code prepararParametroRelatorio}
	 *
	 * @param filtro
	 * @param parametros
	 */
	private void colocarParametrosDoImovel(Map<String, Object> filtro, Map<String, Object> parametros) {
		String cepImovel = (String) filtro.get("cepImovel");
		if (!StringUtils.isEmpty(cepImovel)) {
			parametros.put("CEP", cepImovel);
		}

		Long matriculaImovel = (Long) filtro.get("matriculaImovel");
		if ((matriculaImovel != null) && (matriculaImovel > CONSTANTE_NUMERO_ZERO)) {
			parametros.put("matriculaImovel", String.valueOf(matriculaImovel));
		}

		Boolean indicadorCondominio = (Boolean) filtro.get("indicadorCondominio");
		if (indicadorCondominio != null) {
			if (Boolean.TRUE.equals(indicadorCondominio)) {
				parametros.put("ehImovelCondominio", "Sim");
			} else {
				parametros.put("ehImovelCondominio", "Não");
			}
		}

		String nomeImovel = (String) filtro.get("nomeImovel");
		if (!StringUtils.isEmpty(nomeImovel)) {
			parametros.put("nomeFantasia", nomeImovel);
		}

		String complemento = (String) filtro.get("complementoImovel");
		if (!StringUtils.isEmpty(complemento)) {
			complemento = (complemento.replace('%', ' ')).trim();
			parametros.put("complemento", complemento);
		}

		String numeroImovel = (String) filtro.get("numeroImovel");
		if (!StringUtils.isEmpty(numeroImovel)) {
			parametros.put("numeroImovel", numeroImovel);
		}
	}

	/**
	 * Metodo complementar ao metodo {@code prepararParametroRelatorio}
	 *
	 * @param filtro
	 * @param parametros
	 * @throws NegocioException
	 */
	private void colocarParametrosRelacionadosAoCadastro(Map<String, Object> filtro, Map<String, Object> parametros)
			throws NegocioException {
		ControladorLocalidade controladorLocalidade = ServiceLocator.getInstancia().getControladorLocalidade();
		ControladorSegmento controladorSegmento = ServiceLocator.getInstancia().getControladorSegmento();
		ControladorSetorComercial controladorSetorComercial = ServiceLocator.getInstancia().getControladorSetorComercial();

		Long idLocalidade = (Long) filtro.get("idLocalidade");
		if (idLocalidade != null && idLocalidade > CONSTANTE_NUMERO_ZERO) {
			Localidade localidade = (Localidade) controladorLocalidade.obter(idLocalidade);
			parametros.put("localidade", localidade.getDescricao());
		}

		Long idSegmento = (Long) filtro.get("idSegmento");
		if (idSegmento != null && idSegmento > CONSTANTE_NUMERO_ZERO) {
			Segmento segmento = (Segmento) controladorSegmento.obter(idSegmento);
			parametros.put("segmento", segmento.getDescricao());
		}

		Long idSetorComercial = (Long) filtro.get("idSetorComercial");
		if (idSetorComercial != null && idSetorComercial > CONSTANTE_NUMERO_ZERO) {
			SetorComercial setorComercial = (SetorComercial) controladorSetorComercial.obter(idSetorComercial);
			parametros.put("setorComercial", setorComercial.getDescricao());
		}
	}

	/**
	 * Obter lista sub report ultimas medicoes.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param ultimoHistoricoConsumo the ultimo historico consumo
	 * @return the collection
	 */
	private Collection<SubReportAnaliseConsumoVO> obterListaSubReportUltimasMedicoes(PontoConsumo pontoConsumo,
			HistoricoConsumo ultimoHistoricoConsumo) {

		Collection<SubReportAnaliseConsumoVO> listaSubReports = new ArrayList<SubReportAnaliseConsumoVO>();
		listaSubReports.add(preencherSubReport(ultimoHistoricoConsumo));

		Integer anoMesFaturamentoNumeroCiclo =
				(ultimoHistoricoConsumo.getAnoMesFaturamento() * 10) + ultimoHistoricoConsumo.getNumeroCiclo();
		Collection<HistoricoConsumo> listaSeisUltimasMedicoes =
				obterUltimosHistoricosConsumo(pontoConsumo, anoMesFaturamentoNumeroCiclo, 5, true);

		for (HistoricoConsumo historicoConsumo : listaSeisUltimasMedicoes) {
			listaSubReports.add(preencherSubReport(historicoConsumo));
		}
		return listaSubReports;
	}

	/**
	 * Preencher sub report.
	 *
	 * @param historicoConsumo the historico consumo
	 * @return the sub report analise consumo vo
	 */
	private SubReportAnaliseConsumoVO preencherSubReport(HistoricoConsumo historicoConsumo) {

		SubReportAnaliseConsumoVO subReportAnaliseConsumoVO = new SubReportAnaliseConsumoVO();

		subReportAnaliseConsumoVO
				.setReferencia(String.copyValueOf(historicoConsumo.getAnoMesFaturamento().toString().toCharArray(),
						CONSTANTE_NUMERO_QUATRO, CONSTANTE_NUMERO_DOIS)
						+ "/"
						+ String.copyValueOf(historicoConsumo.getAnoMesFaturamento().toString().toCharArray(),
								CONSTANTE_NUMERO_ZERO, CONSTANTE_NUMERO_QUATRO)
						+ "-" + historicoConsumo.getNumeroCiclo());

		if (historicoConsumo.getConsumo() != null) {
			subReportAnaliseConsumoVO.setLeituraMedidaInformada(String.valueOf(historicoConsumo.getConsumo()));
		} else {
			subReportAnaliseConsumoVO.setLeituraMedidaInformada(" - ");
		}

		if (historicoConsumo.getFatorCorrecao() != null) {
			subReportAnaliseConsumoVO.setFatorCorrecao(String.valueOf(historicoConsumo.getFatorCorrecao()));
		} else {
			subReportAnaliseConsumoVO.setFatorCorrecao(" - ");
		}

		if (historicoConsumo.getFatorPTZ() != null) {
			subReportAnaliseConsumoVO.setFatorPTZ(String.valueOf(historicoConsumo.getFatorPTZ()));
		} else {
			subReportAnaliseConsumoVO.setFatorPTZ(" - ");
		}

		if (historicoConsumo.getFatorPCS() != null) {
			subReportAnaliseConsumoVO.setFatorPCS(String.valueOf(historicoConsumo.getFatorPCS()));
		} else {
			subReportAnaliseConsumoVO.setFatorPCS(" - ");
		}
		if (historicoConsumo.getConsumoApurado() != null) {
			subReportAnaliseConsumoVO.setConsumoAtualApurado(String.valueOf(historicoConsumo.getConsumoApurado()));
		} else {
			subReportAnaliseConsumoVO.setConsumoAtualApurado(" - ");
		}
		if (historicoConsumo.getConsumoApuradoMedio() != null) {
			subReportAnaliseConsumoVO.setConsumoMedio(String.valueOf(historicoConsumo.getConsumoApuradoMedio()));
		} else {
			subReportAnaliseConsumoVO.setConsumoMedio(" - ");
		}
		if (historicoConsumo.getAnormalidadeConsumo() != null) {
			subReportAnaliseConsumoVO.setAnormalidadeConsumoIdentificada(
					historicoConsumo.getAnormalidadeConsumo().getDescricaoAbreviada());
		} else {
			subReportAnaliseConsumoVO.setAnormalidadeConsumoIdentificada(" - ");
		}
		if (historicoConsumo.getHistoricoAtual() != null) {
			if (historicoConsumo.getHistoricoAtual().getAnormalidadeLeituraFaturada() != null) {
				subReportAnaliseConsumoVO.setAnormalidadeLeituraIdentificada(
						historicoConsumo.getHistoricoAtual().getAnormalidadeLeituraFaturada().getDescricaoAbreviada());
			} else {
				subReportAnaliseConsumoVO.setAnormalidadeLeituraIdentificada(" - ");
			}

			if (historicoConsumo.getHistoricoAtual().getHistoricoInstalacaoMedidor() != null
					&& historicoConsumo.getHistoricoAtual().getHistoricoInstalacaoMedidor().getMedidor() != null
					&& historicoConsumo.getHistoricoAtual().getHistoricoInstalacaoMedidor().getMedidor()
							.getNumeroSerie() != null) {

				subReportAnaliseConsumoVO.setMedidor(historicoConsumo.getHistoricoAtual()
						.getHistoricoInstalacaoMedidor().getMedidor().getNumeroSerie());
			} else {
				subReportAnaliseConsumoVO.setMedidor(" - ");
			}

			if (historicoConsumo.getHistoricoAtual().getNumeroLeituraInformada() != null) {
				subReportAnaliseConsumoVO.setLeituraInformada(
						String.valueOf(historicoConsumo.getHistoricoAtual().getNumeroLeituraInformada()));
			} else {
				subReportAnaliseConsumoVO.setLeituraInformada(" - ");
			}

		} else {
			subReportAnaliseConsumoVO.setAnormalidadeLeituraIdentificada(" - ");
			subReportAnaliseConsumoVO.setMedidor(" - ");
		}

		return subReportAnaliseConsumoVO;
	}

	/**
	 * Preencher nome cliente analise consumo vo.
	 *
	 * @param imovel the imovel
	 * @return the string
	 */
	private String preencherNomeClienteAnaliseConsumoVO(Imovel imovel) {

		String nome = "";

		if (!StringUtils.isEmpty(imovel.getNome())) {
			nome = imovel.getNome();
		} else {
			Collection<ClienteImovel> listaClienteImovel = imovel.getListaClienteImovel();
			boolean existeCliente = false;
			if (listaClienteImovel != null && !listaClienteImovel.isEmpty()) {
				for (ClienteImovel clienteImovel : listaClienteImovel) {
					if (clienteImovel.getTipoRelacionamentoClienteImovel()
							.getChavePrimaria() == TipoRelacionamentoClienteImovel.TIPO_USUARIO) {
						nome = clienteImovel.getCliente().getNome();
						existeCliente = true;
						break;
					}
				}
			}
			if (!existeCliente) {
				nome = "NÃO INFORMADO";
			}
		}
		return nome;
	}

	/**
	 * Obter ultimos historicos consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param referenciaAtual the referencia atual
	 * @param cicloAtual the ciclo atual
	 * @param numeroCiclos the numero ciclos
	 * @param quantidadeCiclo the quantidade ciclo
	 * @return the collection
	 */
	private Collection<HistoricoConsumo> obterUltimosHistoricosConsumo(PontoConsumo pontoConsumo, int referenciaAtual, int cicloAtual,
			int numeroCiclos, int quantidadeCiclo) {

		Map<String, Integer> referenciaCicloAnterior = Util.regredirReferenciaCiclo(referenciaAtual, cicloAtual, quantidadeCiclo);

		Map<String, Integer> referenciaCicloMinimo =
				Util.regredirMultiplosReferenciaCiclo(referenciaAtual, cicloAtual, quantidadeCiclo, numeroCiclos);
		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico WHERE ");
		hql.append(" historico.habilitado = true ");
		hql.append(" and historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" AND historico.indicadorConsumoCiclo = true ");
		hql.append(" AND (concat(str(historico.anoMesFaturamento),str(historico.numeroCiclo))) <> :referenciaAtual ");
		hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
		query.setString("referenciaAtual", String.valueOf(referenciaAtual) + String.valueOf(cicloAtual));
		query.setMaxResults(numeroCiclos);

		return query.list();
	}

	/**
	 * Obter ultimos historicos consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param anoMesFaturamentoNumeroCiclo the ano mes faturamento numero ciclo
	 * @param qtdConsumos the qtd consumos
	 * @param addWhereIndicadorConsumoCiclo the add where indicador consumo ciclo
	 * @return the collection
	 */
	private Collection<HistoricoConsumo> obterUltimosHistoricosConsumo(PontoConsumo pontoConsumo, Integer anoMesFaturamentoNumeroCiclo,
			Integer qtdConsumos, boolean addWhereIndicadorConsumoCiclo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" WHERE ");
		hql.append(" historico.habilitado = true ");
		hql.append(" and historico.pontoConsumo.chavePrimaria = :idPontoConsumo AND");
		hql.append(" (historico.anoMesFaturamento*10+historico.numeroCiclo) < :anoMesFaturamentoNumeroCiclo ");

		if (addWhereIndicadorConsumoCiclo) {
			hql.append(" AND historico.indicadorConsumoCiclo = true ");
		}

		hql.append(" ORDER BY historico.anoMesFaturamento desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
		query.setInteger("anoMesFaturamentoNumeroCiclo", anoMesFaturamentoNumeroCiclo);
		query.setMaxResults(qtdConsumos);
		return query.list();
	}

	/**
	 * Obter ultimos historicos consumo com leitura faturada.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @return the historico consumo
	 */
	@SuppressWarnings("unchecked")
	private HistoricoConsumo obterUltimosHistoricosConsumoComLeituraFaturada(PontoConsumo pontoConsumo) {
		return queryObterUltimosHistoricosConsumoComLeituraFaturada(pontoConsumo,true);
	}

	/**
	 * Obter ultimos historicos consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @return the historico consumo
	 */
	@SuppressWarnings("unchecked")
	private HistoricoConsumo obterUltimosHistoricosConsumo(PontoConsumo pontoConsumo) {
		return queryObterUltimosHistoricosConsumoComLeituraFaturada(pontoConsumo,false);
	}
	
	private HistoricoConsumo queryObterUltimosHistoricosConsumoComLeituraFaturada(PontoConsumo pontoConsumo, boolean faturada) {

		HistoricoConsumo retorno = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" WHERE ");
		hql.append(" historico.habilitado = true ");
		hql.append(" and historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");
		hql.append(" and historico.indicadorFaturamento = true ");
		hql.append(" and historico.habilitado = true ");
		if(faturada){ 
			hql.append(" and historico.historicoAnterior.numeroLeituraFaturada is not null  ");
		}
		hql.append(" ORDER BY historico.anoMesFaturamento desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());
		Collection<HistoricoConsumo> listaHistoricoConsumo = query.list();

		if (listaHistoricoConsumo != null && listaHistoricoConsumo.size() > CONSTANTE_NUMERO_UM) {
			retorno = listaHistoricoConsumo.iterator().next();
		}

		return retorno;
	}

	/**
	 * Obter ultimo historico consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @return the historico consumo
	 */
	public HistoricoConsumo obterUltimoHistoricoConsumo(PontoConsumo pontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" inner join fetch historico.historicoAtual histAtual");
		hql.append(" left join fetch historico.historicoAnterior histAnterior");
		hql.append(" WHERE ");
		hql.append(" historico.habilitado = true ");
		hql.append(" and historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" AND (historico.anoMesFaturamento + historico.numeroCiclo) = ");
		hql.append("  (SELECT MAX(hist.anoMesFaturamento + hist.numeroCiclo) ");
		hql.append("   FROM ");
		hql.append(getClasseEntidade().getSimpleName()).append(" hist ");
		hql.append("   WHERE ");
		hql.append("   hist.habilitado = true ");
		hql.append("   and hist.pontoConsumo.chavePrimaria = :idPontoConsumo )");
		hql.append(" AND historico.indicadorConsumoCiclo = true ");

		hql.append(" order by histAtual.dataLeituraInformada desc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());

		return (HistoricoConsumo) Util.primeiroElemento(query.list());
	}

	private HistoricoConsumo obterHistoricoConsumoPorDataMI(Long idPontoConsumo, Date data, Long idMedidor) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select historicoConsumo ");
		hql.append(" FROM ");
		hql.append(" HistoricoConsumoImpl historicoConsumo ");
		hql.append(" inner join historicoConsumo.historicoAtual historicoMedicao ");
		hql.append(" WHERE ");
		hql.append(" historicoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and (historicoMedicao.dataLeituraInformada <= :dataUltimaHora ")
				.append("and historicoMedicao.dataLeituraInformada >= :dataPrimeiraHora) ");
		hql.append(" and historicoConsumo.medidor.chavePrimaria = :idMedidor ");
		hql.append(" and (historicoConsumo.indicadorConsumoCiclo = false or historicoConsumo.indicadorConsumoCiclo is null) ");
		hql.append(" and historicoConsumo.indicadorMedicaoIndependenteAtual = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idMedidor", idMedidor);
		query.setLong("idPontoConsumo", idPontoConsumo);
		Util.adicionarRestricaoDataSemHora(query, data, "dataUltimaHora", Boolean.FALSE);
		Util.adicionarRestricaoDataSemHora(query, data, "dataPrimeiraHora", Boolean.TRUE);
		query.setMaxResults(CONSTANTE_NUMERO_UM);

		return (HistoricoConsumo) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo. ControladorHistoricoConsumo #obterHistoricoConsumoPorData (java.lang.Long, java.util.Date)
	 */
	@Override
	public HistoricoConsumo obterHistoricoConsumoPorData(Long idPontoConsumo, Date data) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select historicoConsumo ");
		hql.append(" FROM ");
		hql.append(" HistoricoConsumoImpl historicoConsumo ");
		hql.append(" inner join historicoConsumo.historicoAtual historicoMedicao ");
		hql.append(" WHERE ");
		hql.append(" historicoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and historicoMedicao.dataLeituraInformada >= :data ");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo = true ");
		hql.append(" and historicoConsumo.habilitado = true ");
		hql.append(" order by historicoMedicao.dataLeituraInformada desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo);
		Util.adicionarRestricaoDataSemHora(query, data, "data", Boolean.FALSE);
		query.setMaxResults(CONSTANTE_NUMERO_UM);

		return (HistoricoConsumo) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# consultarHistoricoConsumo(java.lang.Long)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumo(Long chavePrimariaPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo");
		hql.append(" left join fetch historico.historicoAtual historicoAtual");
		hql.append(" left join fetch historico.anormalidadeConsumo anormalidadeConsumo");
		hql.append(" inner join fetch historico.tipoConsumo tipoConsumo");
		hql.append(" where pontoConsumo.chavePrimaria = ? ");
		hql.append(" and historico.habilitado = true ");
		hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CONSTANTE_NUMERO_ZERO, chavePrimariaPontoConsumo);

		return query.list();
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# consultarHistoricoConsumo(java.lang.Long)
	 */
	@Override
	public Collection<HistoricoConsumoVO> consultarHistoricoConsumoLeituraMovimento(Long chavePrimariaPontoConsumo) throws NegocioException {

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT cohi.cohi_dt_am_faturamento as anoMesFaturamento, ");
		sql.append(" cohi.cohi_nr_ciclo as numeroCiclo, ");
		sql.append(" mehi.mehi_tm_leitura_anterior as dataLeituraAnterior, ");
		sql.append(" mehi.mehi_md_leitura_anterior as numeroLeituraAnterior, ");
		sql.append(" mehi.mehi_tm_leitura_informada as dataLeituraInformada, ");
		sql.append(" mehi.mehi_md_leitura_informada as numeroLeituraInformada, ");
		sql.append(" cohi.cohi_md_consumo as consumo, ");
		sql.append(" cohi.cohi_qn_dias_consumo as diasConsumo, ");
		sql.append(" cohi.cohi_nr_fator_pcs as fatorPCS, ");
		sql.append(" cohi.cohi_nr_fator_ptz as fatorPTZ, ");
		sql.append(" cohi.cohi_md_consumo_medido as consumoMedido, ");
		sql.append(" cohi.cohi_nr_fator_correcao as fatorCorrecao, ");
		sql.append(" cotp.cotp_ds as descricaoTipoConsumo, ");
		sql.append(" lemo.lemo_ds_imagem_coletor as urlFoto, ");
		sql.append(" lemo.lemo_cd as idLeituraMovimento, ");
		sql.append(" lemo.rota_cd as idRota ");

		
		sql.append(" FROM ");
		sql.append(" consumo_historico cohi ");
		sql.append(" inner join ponto_consumo pocn on pocn.pocn_cd = cohi.pocn_cd ");
		sql.append(" inner join medicao_historico mehi on mehi.mehi_cd = cohi.mehi_cd_atual ");
		sql.append(" inner join consumo_tipo cotp on cotp.cotp_cd = cohi.cotp_cd ");
		sql.append(" left join leitura_movimento lemo on (lemo.lemo_dt_am_faturamento = cohi.cohi_dt_am_faturamento ");
		sql.append(" and lemo.pocn_cd = cohi.pocn_cd) ");
		sql.append(" WHERE cohi.pocn_cd = :idPontoConsumo ");
		sql.append(" and cohi.cohi_in_uso = 1 and cohi.cohi_in_consumo_ciclo = 1 ");
		sql.append(" order by cohi.cohi_dt_am_faturamento desc, cohi.cohi_nr_ciclo desc ");
		
		SQLQuery query = createSQLQuery(sql.toString());
		query.setLong("idPontoConsumo", chavePrimariaPontoConsumo);
		
		query.addScalar("anoMesFaturamento", IntegerType.INSTANCE);
		query.addScalar("numeroCiclo", IntegerType.INSTANCE);
		query.addScalar("urlFoto", StringType.INSTANCE);
		query.addScalar("dataLeituraAnterior", DateType.INSTANCE);
		query.addScalar("dataLeituraInformada", DateType.INSTANCE);
		query.addScalar("numeroLeituraAnterior", BigDecimalType.INSTANCE);
		query.addScalar("numeroLeituraInformada", BigDecimalType.INSTANCE);
		query.addScalar("consumo", BigDecimalType.INSTANCE);
		query.addScalar("diasConsumo", IntegerType.INSTANCE);
		query.addScalar("fatorPCS", BigDecimalType.INSTANCE);
		query.addScalar("fatorPTZ", BigDecimalType.INSTANCE);
		query.addScalar("consumoMedido", BigDecimalType.INSTANCE);
		query.addScalar("fatorCorrecao", BigDecimalType.INSTANCE);
		query.addScalar("descricaoTipoConsumo", StringType.INSTANCE);
		query.addScalar("idRota", LongType.INSTANCE);
		query.addScalar("idLeituraMovimento", LongType.INSTANCE);

		
		query.setResultTransformer(Transformers.aliasToBean(HistoricoConsumoVO.class));	
		
		return query.list();


	}

	/**
	 * Consultar historico consumo por anormalidade consumo.
	 *
	 * @param chavePrimariaPontoConsumo the chave primaria ponto consumo
	 * @param status the status
	 * @param idsAnormalidadeConsumo the ids anormalidade consumo
	 * @return the collection
	 */
	private Collection<HistoricoConsumo> consultarHistoricoConsumoPorAnormalidadeConsumo(Long chavePrimariaPontoConsumo, Boolean status,
			Long... idsAnormalidadeConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo");
		hql.append(" left join fetch historico.anormalidadeConsumo anormalidadeConsumo");
		hql.append(" where historico.pontoConsumo.chavePrimaria = :chavePrimariaPontoConsumo ");
		hql.append(" and historico.anormalidadeConsumo.chavePrimaria in (:idsAnormalidadeConsumo) ");
		hql.append(" and historico.habilitado = :status ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaPontoConsumo", chavePrimariaPontoConsumo);
		query.setParameterList("idsAnormalidadeConsumo", idsAnormalidadeConsumo);
		query.setBoolean("status", status);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarHistoricoConsumoPorHistoricoMedicao(java.lang.Long)
	 */
	@Override
	public HistoricoConsumo consultarHistoricoConsumoPorHistoricoMedicao(Long chaveHistoricoMedicao) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" left join fetch historico.historicoAnterior historicoAnterior");
		hql.append(" left join fetch historico.historicoAtual historicoAtual");

		hql.append(" where historicoAtual.chavePrimaria = :chaveHistoricoMedicao ");
		hql.append(" and historico.habilitado = true ");
		hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveHistoricoMedicao", chaveHistoricoMedicao);

		query.setMaxResults(CONSTANTE_NUMERO_UM);
		Collection<HistoricoConsumo> historicos = query.list();

		HistoricoConsumo retorno = null;
		if (!historicos.isEmpty()) {
			retorno = historicos.iterator().next();
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# consultarHistoricoConsumo(java.lang.Long, java.lang.Integer,
	 * java.lang.Integer, boolean)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumo(Long idPontoConsumo, Integer anoMesFaturamento, Integer numeroCiclo,
			Boolean indicadorConsumoCiclo, Boolean indicadorFaturamento) throws NegocioException {

		return this.consultarHistoricoConsumo(idPontoConsumo, anoMesFaturamento, numeroCiclo, indicadorConsumoCiclo, indicadorFaturamento,
				null);
	}

	/**
	 * Consulta os históricos de consumo por chave primária do ponto de consumo, por ano/mês faturamento, numero do ciclo de faturamento e
	 * indicador de faturamento.
	 *
	 * @param chavesPontoConsumo
	 * @param anoMesFaturamento
	 * @param numeroCiclo
	 * @param indicadorFaturamento
	 * @return mapa de lista de HistoricoConsumo por chave de PontoConsumo
	 */
	@Override
	public Map<Long, List<HistoricoConsumo>> consultarHistoricoConsumoPorAnoMesFaturamentoEnumeroCiclo(Long[] chavesPontoConsumo,
			Integer anoMesFaturamento, Integer numeroCiclo, Boolean indicadorFaturamento) {

		Map<Long, List<HistoricoConsumo>> mapaHistoricoPorChavePontoConsumo = new HashMap<>();
		if (chavesPontoConsumo != null && chavesPontoConsumo.length > CONSTANTE_NUMERO_ZERO) {
			StringBuilder hql = new StringBuilder();

			hql.append(" select pontoConsumo.chavePrimaria, historico from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" historico ");
			hql.append(" left join fetch historico.historicoAtual historicoAtual ");
			hql.append(" left join fetch historico.anormalidadeConsumo anormalidadeConsumo ");
			hql.append(" inner join fetch historico.tipoConsumo tipoConsumo ");
			hql.append(" inner join historico.pontoConsumo pontoConsumo ");

			hql.append(" where ");
			Map<String, List<Long>> mapaPropriedades =
					HibernateHqlUtil.adicionarClausulaIn(hql, PONTO_CONSUMO_CHAVE_PRIMARIA, PT_CONS, Arrays.asList(chavesPontoConsumo));

			if (anoMesFaturamento != null) {
				hql.append(" and historico.anoMesFaturamento = :anoMesFaturamento ");
			}
			if (numeroCiclo != null) {
				hql.append(" and historico.numeroCiclo = :numeroCiclo ");
			}

			hql.append(" and historico.indicadorConsumoCiclo = :indicadorConsumoCiclo ");

			if (indicadorFaturamento != null) {
				hql.append(" and historico.indicadorFaturamento = :indicadorFaturamento ");
			}

			hql.append(" and historico.habilitado = true ");
			hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc,")
					.append(" historicoAtual.dataLeituraInformada asc, historico.chavePrimaria asc ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

			if (anoMesFaturamento != null) {
				query.setInteger(CONSTANTE_ANO_MES_FATURAMENTO, anoMesFaturamento);
			}
			if (numeroCiclo != null) {
				query.setInteger(NUMERO_CICLO, numeroCiclo);
			}

			query.setBoolean(INDICADOR_CONSUMO_CICLO, Boolean.TRUE);

			if (indicadorFaturamento != null) {
				query.setBoolean(INDICADOR_FATURAMENTO, indicadorFaturamento);
			}

			mapaHistoricoPorChavePontoConsumo.putAll(montarMapaListaDeHistoricoPorChavesPontoConsumo(query.list()));
		}
		return mapaHistoricoPorChavePontoConsumo;
	}

	/**
	 * Constrói um mapa de históricos de consumo por chave primária do ponto de consumo.
	 *
	 * @param atributos
	 * @return mapa de lista de HistoricoConsumo por chave de PontoConsumo
	 */
	private Map<Long, List<HistoricoConsumo>> montarMapaListaDeHistoricoPorChavesPontoConsumo(List<Object[]> atributos) {

		Map<Long, List<HistoricoConsumo>> mapaHistoricoPorChavePontoConsumo = new HashMap<>();
		List<HistoricoConsumo> lista = null;
		for (Object[] dadosHistoricoConsumo : atributos) {
			Long idPontoConsumo = (Long) dadosHistoricoConsumo[CONSTANTE_NUMERO_ZERO];
			HistoricoConsumo historicoConsumo = (HistoricoConsumo) dadosHistoricoConsumo[CONSTANTE_NUMERO_UM];
			if (!mapaHistoricoPorChavePontoConsumo.containsKey(idPontoConsumo)) {
				lista = new ArrayList<>();
				mapaHistoricoPorChavePontoConsumo.put(idPontoConsumo, lista);
			} else {
				lista = mapaHistoricoPorChavePontoConsumo.get(idPontoConsumo);
			}
			lista.add(historicoConsumo);

		}
		return mapaHistoricoPorChavePontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# consultarHistoricoConsumo(java.lang.Long, java.lang.Integer,
	 * java.lang.Integer, boolean)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumo(Long idPontoConsumo, Integer anoMesFaturamento, Integer numeroCiclo,
			Boolean indicadorConsumoCiclo, Boolean indicadorFaturamento, Long chavePrimariaHistoricoConsumoSintetizador)
			throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo ");
		hql.append(" left join fetch historico.historicoAtual historicoAtual ");
		hql.append(" left join fetch historico.historicoAnterior historicoAnterior ");
		hql.append(" left join fetch historico.anormalidadeConsumo anormalidadeConsumo ");
		hql.append(" inner join fetch historico.tipoConsumo tipoConsumo ");
		hql.append(" left join fetch historicoAtual.historicoInstalacaoMedidor instalacaoMedidor ");
		hql.append(" left join fetch instalacaoMedidor.medidor medidor ");

		hql.append(" where pontoConsumo.chavePrimaria = :idPontoConsumo ");

		if (anoMesFaturamento != null) {
			hql.append(" and historico.anoMesFaturamento = :anoMesFaturamento ");
		}
		if (numeroCiclo != null) {
			hql.append(" and historico.numeroCiclo = :numeroCiclo ");
		}
		if (indicadorConsumoCiclo != null) {
			hql.append(" and historico.indicadorConsumoCiclo = :indicadorConsumoCiclo ");
		}

		if (indicadorFaturamento != null) {
			hql.append(" and historico.indicadorFaturamento = :indicadorFaturamento ");
		}

		if (chavePrimariaHistoricoConsumoSintetizador != null) {
			hql.append(" and historico.historicoConsumoSintetizador.chavePrimaria = :chavePrimariaHistoricoConsumoSintetizador ");
		}

		hql.append(" and historico.habilitado = true ");
		hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc,")
				.append(" historicoAtual.dataLeituraInformada asc, historico.chavePrimaria asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", idPontoConsumo);

		if (anoMesFaturamento != null) {
			query.setInteger(CONSTANTE_ANO_MES_FATURAMENTO, anoMesFaturamento);
		}
		if (numeroCiclo != null) {
			query.setInteger(NUMERO_CICLO, numeroCiclo);
		}
		if (indicadorConsumoCiclo != null) {
			query.setBoolean(INDICADOR_CONSUMO_CICLO, indicadorConsumoCiclo);
		}

		if (indicadorFaturamento != null) {
			query.setBoolean(INDICADOR_FATURAMENTO, indicadorFaturamento);
		}

		if (chavePrimariaHistoricoConsumoSintetizador != null) {
			query.setLong("chavePrimariaHistoricoConsumoSintetizador", chavePrimariaHistoricoConsumoSintetizador);
		}

		return query.list();
	}

	/**
	 * Método responsável por consultar os historicos do ponto de consumo, por referência e ciclo, agrupados por chave de ponto de consumo.
	 *
	 * @param pontosConsumo - {@link Collection}
	 * @param anoMesFaturamento - {@link Integer}
	 * @param numeroCiclo - {@link Integer}
	 * @return históricos de consumo por chave de ponto de consumo - {@link Map}
	 */
	@Override
	public Map<Long, Collection<HistoricoConsumo>> consultarHistoricoConsumo(Collection<PontoConsumo> pontosConsumo,
			Integer anoMesFaturamento, Integer numeroCiclo) {

		Map<Long, Collection<HistoricoConsumo>> historicoPorPontoConsumo = null;

		if (!Util.isNullOrEmpty(pontosConsumo)) {

			StringBuilder hql = new StringBuilder();
			hql.append(" select ");
			hql.append(" pontoConsumo.chavePrimaria as pontoConsumo_chavePrimaria, ");
			hql.append(" historico.diasConsumo as diasConsumo, ");
			hql.append(" historico.chavePrimaria as chavePrimaria, ");
			hql.append(" historicoAtual.indicadorInformadoCliente as ");
			hql.append(" historicoAtual_indicadorInformadoCliente, ");
			hql.append(" medidor.numeroSerie as ");
			hql.append(" historicoAtual_historicoInstalacaoMedidor_medidor_numeroSerie ");
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" historico ");
			hql.append(" inner join historico.pontoConsumo pontoConsumo ");
			hql.append(" left join historico.historicoAtual historicoAtual ");
			hql.append(" left join historicoAtual.historicoInstalacaoMedidor historicoInstalacaoMedidor ");
			hql.append(" left join historicoInstalacaoMedidor.medidor medidor ");
			hql.append(" left join historico.anormalidadeConsumo anormalidadeConsumo ");
			hql.append(" inner join historico.tipoConsumo tipoConsumo ");
			hql.append(" where ");
			Map<String, List<Long>> mapaPropriedades = HibernateHqlUtil.adicionarClausulaIn(hql, PONTO_CONSUMO_CHAVE_PRIMARIA, PT_CONS,
					Util.recuperarChavesPrimarias(pontosConsumo));

			if (anoMesFaturamento != null) {
				hql.append(" and historico.anoMesFaturamento = :anoMesFaturamento ");
			}
			if (numeroCiclo != null) {
				hql.append(" and historico.numeroCiclo = :numeroCiclo ");
			}

			hql.append(" and historico.indicadorConsumoCiclo = :indicadorConsumoCiclo ");
			hql.append(" and historico.indicadorFaturamento = :indicadorFaturamento ");
			hql.append(" and historico.habilitado = true ");
			hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc, ");
			hql.append(" historicoAtual.dataLeituraInformada asc, historico.chavePrimaria asc ");

			Query query = getSession().createQuery(hql.toString());
			query.setResultTransformer(new GGASTransformer(getClasseEntidade(), super.getSessionFactory().getAllClassMetadata(),
					"pontoConsumo_chavePrimaria"));
			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

			if (anoMesFaturamento != null) {
				query.setInteger(CONSTANTE_ANO_MES_FATURAMENTO, anoMesFaturamento);
			}
			if (numeroCiclo != null) {
				query.setInteger(NUMERO_CICLO, numeroCiclo);
			}

			query.setBoolean(INDICADOR_CONSUMO_CICLO, Boolean.TRUE);
			query.setBoolean(INDICADOR_FATURAMENTO, Boolean.TRUE);

			historicoPorPontoConsumo = (Map<Long, Collection<HistoricoConsumo>>) query.uniqueResult();

		}
		return historicoPorPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarHistoricoConsumoFaturavelBatch(java.lang.Long,
	 * java.lang.Integer, java.lang.Integer, java.lang.Boolean)
	 */
	// TKT 4232
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumoFaturavelBatch(Long idPontoConsumo, Integer anoMesFaturamento,
			Integer numeroCiclo, Boolean indicadorConsumoCiclo) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo ");
		hql.append(" left join fetch historico.historicoAtual historicoAtual ");
		hql.append(" left join fetch historico.anormalidadeConsumo anormalidadeConsumo ");
		hql.append(" inner join fetch historico.tipoConsumo tipoConsumo ");

		hql.append(" where pontoConsumo.chavePrimaria = :idPontoConsumo ");

		if (anoMesFaturamento != null) {
			hql.append(" and historico.anoMesFaturamento = :anoMesFaturamento ");
		}

		if (numeroCiclo != null) {
			hql.append(" and historico.numeroCiclo = :numeroCiclo ");
		}

		if (indicadorConsumoCiclo != null) {
			hql.append(" and historico.indicadorConsumoCiclo = :indicadorConsumoCiclo ");
		}

		hql.append(" and historico.habilitado = true ");
		hql.append(" and historico.indicadorFaturamento = false ");
		hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc, historicoAtual.dataLeituraInformada asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", idPontoConsumo);

		if (anoMesFaturamento != null) {
			query.setInteger(CONSTANTE_ANO_MES_FATURAMENTO, anoMesFaturamento);
		}

		if (numeroCiclo != null) {
			query.setInteger(NUMERO_CICLO, numeroCiclo);
		}

		if (indicadorConsumoCiclo != null) {
			query.setBoolean(INDICADOR_CONSUMO_CICLO, indicadorConsumoCiclo);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo#
	 * consultarHistoricoConsumo(java.lang.Long, boolean)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumo(Long chavePrimariaPontoConsumo,
			boolean indicadorConsumoCiclo, String referenciaInicial, String referenciaFinal) throws NegocioException {
		return queryConsultarHistoricoConsumo(chavePrimariaPontoConsumo, indicadorConsumoCiclo, referenciaInicial,
				referenciaFinal, CONSTANTE_NUMERO_ZERO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo#
	 * consultarHistoricoConsumo(java.lang.Long, boolean)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumo(Long chavePrimariaPontoConsumo,
			boolean indicadorConsumoCiclo, String referenciaInicial, String referenciaFinal, Integer quantidadeCiclos)
			throws NegocioException {
		return queryConsultarHistoricoConsumo(chavePrimariaPontoConsumo, indicadorConsumoCiclo, referenciaInicial,
				referenciaFinal, quantidadeCiclos);
	}

	private Collection<HistoricoConsumo> queryConsultarHistoricoConsumo(Long chavePrimariaPontoConsumo,
			boolean indicadorConsumoCiclo, String referenciaInicial, String referenciaFinal, Integer quantidadeCiclos)
			throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo");
		hql.append(" left join fetch pontoConsumo.rota rota");
		hql.append(" inner join fetch rota.tipoLeitura tipoLeitura");
		hql.append(" left join fetch historico.historicoAtual historicoAtual");
		hql.append(" left join fetch historico.anormalidadeConsumo anormalidadeConsumo");
		hql.append(" inner join fetch historico.tipoConsumo tipoConsumo");
		hql.append(" where historico.pontoConsumo.chavePrimaria = ? ");
		if (quantidadeCiclos != CONSTANTE_NUMERO_ZERO) {
			hql.append(" and historico.indicadorConsumoCiclo = ? ");
		}
		hql.append(" and historico.habilitado = true ");

		if ((referenciaInicial == null && referenciaFinal != null)
				|| (referenciaInicial != null && referenciaFinal == null)) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA, false);
		} else if (referenciaInicial != null) {
			hql.append(" and historico.anoMesFaturamento between :referenciaInicial and :referenciaFinal ");
		}

		hql.append(
				" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc, historico.historicoAtual.dataLeituraInformada desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CONSTANTE_NUMERO_ZERO, chavePrimariaPontoConsumo);
		if (quantidadeCiclos != CONSTANTE_NUMERO_ZERO) {
			query.setBoolean(CONSTANTE_NUMERO_UM, indicadorConsumoCiclo);
		}

		if (referenciaInicial != null) {
			query.setInteger("referenciaInicial", Integer.parseInt(referenciaInicial));
			query.setInteger("referenciaFinal", Integer.parseInt(referenciaFinal));
		}

		if (quantidadeCiclos > CONSTANTE_NUMERO_ZERO) {
			query.setMaxResults(quantidadeCiclos + CONSTANTE_NUMERO_UM);
		}
		return query.list();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# consultarHistoricoConsumoPorMesAnoCiclo(java.lang.Long, boolean)
	 */
	@Override
	public HistoricoConsumo consultarHistoricoConsumoPorMesAnoCicloAnterior(Long chavePrimariaPontoConsumo, String anoMes, String ciclo)
			throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo");
		hql.append(" left join fetch pontoConsumo.rota rota");
		hql.append(" inner join fetch rota.tipoLeitura tipoLeitura");
		hql.append(" left join fetch historico.historicoAtual historicoAtual");
		hql.append(" left join fetch historico.anormalidadeConsumo anormalidadeConsumo");
		hql.append(" inner join fetch historico.tipoConsumo tipoConsumo");
		hql.append(" where historico.pontoConsumo.chavePrimaria = ? ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");
		hql.append(" and historico.habilitado = true ");
		hql.append(" and historico.anoMesFaturamento = ? ");
		hql.append(" and historico.numeroCiclo = ? ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CONSTANTE_NUMERO_ZERO, chavePrimariaPontoConsumo);
		query.setString(CONSTANTE_NUMERO_UM, anoMes);
		query.setString(CONSTANTE_NUMERO_DOIS, ciclo);

		return (HistoricoConsumo) query.uniqueResult();
	}

	/**
	 * Consultar historico consumo.
	 *
	 * @param chavesPrimariaPontoConsumo the chaves primaria ponto consumo
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# consultarHistoricoConsumo(java.lang.Long, boolean)
	 */
	public Collection<HistoricoConsumo> consultarHistoricoConsumo(Long[] chavesPrimariaPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo");
		hql.append(" where ");
		Map<String, List<Long>> mapaPropriedades = HibernateHqlUtil.adicionarClausulaIn(hql, "historico.pontoConsumo.chavePrimaria",
				PT_CONS, Arrays.asList(chavesPrimariaPontoConsumo));
		hql.append(" and historico.habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# consultarHistoricoConsumoAnaliseExcecoesLeitura (java.util.Map)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumoAnaliseExcecoesLeitura(Map<String, Object> filtro)
			throws NegocioException {

		Collection<HistoricoConsumo> retorno = new ArrayList<HistoricoConsumo>();

		Class entidadePontoConsumo = ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo ponto");
		hql.append(" left join fetch  ponto.rota rota");
		hql.append(" left join fetch  rota.grupoFaturamento grupoFaturamento");
		hql.append(" inner join fetch ponto.imovel imovel");
		hql.append(" left join fetch imovel.imovelCondominio imovelCondominio ");
		hql.append(" inner join fetch historico.historicoAtual histmed ");
		hql.append(" left join fetch historico.anormalidadeConsumo anorCon ");
		hql.append(" left join fetch histmed.anormalidadeLeituraFaturada anorLei ");
		hql.append(" where historico.habilitado = true ");
		hql.append(" and historico.indicadorFaturamento = false ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");

		hql.append(" and (histmed.anoMesLeitura = grupoFaturamento.anoMesReferencia ");
		hql.append(" and histmed.numeroCiclo >= grupoFaturamento.numeroCiclo ");
		hql.append(" or histmed.anoMesLeitura > grupoFaturamento.anoMesReferencia) ");
		hql.append(" and histmed.dataLeituraInformada in ( ");

		hql.append(" select min(historicoMedicao.dataLeituraInformada) ");
		hql.append(" from ");
		hql.append(getClasseHistoricoMedicao().getSimpleName());
		hql.append(" historicoMedicao,  ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historicoConsumo ");
		hql.append(" where historicoMedicao.chavePrimaria = historicoConsumo.historicoAtual.chavePrimaria ");
		hql.append(" and historicoMedicao.pontoConsumo.chavePrimaria = historico.pontoConsumo.chavePrimaria ");
		hql.append(" and historicoConsumo.habilitado = true");
		hql.append(" and historicoConsumo.indicadorFaturamento = false");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo = true ");
		hql.append(" and (historicoMedicao.anoMesLeitura = grupoFaturamento.anoMesReferencia ");
		hql.append(" and historicoMedicao.numeroCiclo >= grupoFaturamento.numeroCiclo ");
		hql.append(" or historicoMedicao.anoMesLeitura > grupoFaturamento.anoMesReferencia)) ");

		Long idCliente = (Long) filtro.get("idCliente");
		if ((idCliente != null) && (idCliente > CONSTANTE_NUMERO_ZERO)) {
			hql.append(" and :idCliente in (select distinct (clienteImovel.cliente.chavePrimaria) from ");
			hql.append(entidadePontoConsumo.getSimpleName());
			hql.append(" pontoCon ");
			hql.append(" inner join pontoCon.imovel.listaClienteImovel clienteImovel");
			hql.append(" where pontoCon = ponto )");
		}

		String cepImovel = (String) filtro.get("cepImovel");
		if (!StringUtils.isEmpty(cepImovel)) {
			hql.append(" and ponto.cep.cep = :cepImovel ");
		}

		Long matriculaImovel = (Long) filtro.get("matriculaImovel");
		if ((matriculaImovel != null) && (matriculaImovel > CONSTANTE_NUMERO_ZERO)) {
			hql.append(" and imovel.chavePrimaria = :matriculaImovel ");
		}

		Boolean indicadorCondominio = (Boolean) filtro.get("indicadorCondominio");
		if (indicadorCondominio != null) {
			hql.append(" and imovel.condominio = :indicadorCondominio ");
		}

		Long matriculaCondominio = (Long) filtro.get("matriculaCondominio");
		if ((matriculaCondominio != null) && (matriculaCondominio > CONSTANTE_NUMERO_ZERO)) {
			hql.append(" and imovelCondominio.chavePrimaria = :matriculaCondominio ");
		}

		String nomeImovel = (String) filtro.get("nomeImovel");
		if (!StringUtils.isEmpty(nomeImovel)) {
			hql.append(" and upper(imovel.nome) like upper(:nomeImovel) ");
		}

		String complementoImovel = (String) filtro.get("complementoImovel");
		if (!StringUtils.isEmpty(complementoImovel)) {
			hql.append(" and upper(imovel.descricaoComplemento) like upper(:complementoImovel) ");
		}

		String numeroImovel = (String) filtro.get("numeroImovel");
		if (!StringUtils.isEmpty(numeroImovel)) {
			hql.append(" and imovel.numeroImovel like :numeroImovel ");
		}

		Long idLocalidade = (Long) filtro.get("idLocalidade");
		if (idLocalidade != null && idLocalidade > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and imovel.quadraFace.quadra.setorComercial.localidade.chavePrimaria = :idLocalidade ");
		}

		Long idSetorComercial = (Long) filtro.get("idSetorComercial");
		if (idSetorComercial != null && idSetorComercial > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and imovel.quadraFace.quadra.setorComercial.chavePrimaria = :idSetorComercial ");
		}

		Long idGrupoFaturamento = (Long) filtro.get(CONSTANTE_ID_GRUPO_FATURAMENTO);
		if (idGrupoFaturamento != null && idGrupoFaturamento > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and ponto.rota.grupoFaturamento.chavePrimaria = :idGrupoFaturamento ");
		}

		Long idSegmento = (Long) filtro.get("idSegmento");
		if (idSegmento != null && idSegmento > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and ponto.segmento.chavePrimaria = :idSegmento ");
		}

		Long idSituacaoPontoConsumo = (Long) filtro.get("idSituacaoPontoConsumo");
		if (idSituacaoPontoConsumo != null && idSituacaoPontoConsumo > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and ponto.situacaoConsumo.chavePrimaria = :idSituacaoPontoConsumo ");
		}

		Long[] idsAnormalidadeConsumo = (Long[]) filtro.get("idsAnormalidadeConsumo");
		if (idsAnormalidadeConsumo != null && idsAnormalidadeConsumo.length > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and anorCon.chavePrimaria in (:idsAnormalidadeConsumo) ");
		}

		Long[] idsAnormalidadeLeitura = (Long[]) filtro.get("idsAnormalidadeLeitura");
		if (idsAnormalidadeLeitura != null && idsAnormalidadeLeitura.length > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and anorLei.chavePrimaria in (:idsAnormalidadeLeitura) ");
		}

		Long[] idsRota = (Long[]) filtro.get("idsRota");
		if (idsRota != null && idsRota.length > CONSTANTE_NUMERO_ZERO) {
			hql.append(" and ponto.rota.chavePrimaria in (:idsRota) ");
		}

		Long idTipoConsumo = (Long) filtro.get("idTipoConsumo");
		if ((idTipoConsumo != null) && (idTipoConsumo > CONSTANTE_NUMERO_ZERO)) {
			hql.append(" and historico.tipoConsumo.chavePrimaria = :idTipoConsumo ");
		}

		BigDecimal consumoLido = (BigDecimal) filtro.get("consumoLido");
		if (consumoLido != null) {
			hql.append(" and historico.consumo >= :consumoLido ");
		}

		BigDecimal percentualVariacaoConsumo = (BigDecimal) filtro.get("percentualVariacaoConsumo");
		if (percentualVariacaoConsumo != null) {
			hql.append(" and historico.consumoApuradoMedio is not null ");
			hql.append(" and historico.habilitado = true ");
			hql.append(" and historico.consumoApuradoMedio > 0 ");
			hql.append(
					" and (historico.consumo - historico.consumoApuradoMedio)*100/historico.consumoApuradoMedio <= :percentualVariacaoConsumo ");
		}

		Boolean analisada = (Boolean) filtro.get("analisada");
		if (analisada != null) {
			hql.append(" and histmed.analisada = :analisada ");
		}

		Boolean comOcorrencia = (Boolean) filtro.get("comOcorrencia");
		if (comOcorrencia != null) {
			if (comOcorrencia) {
				hql.append(" and (anorCon is not null or anorLei is not null) ");
			} else {
				hql.append(" and (anorCon is null and anorLei is null) ");
			}
		}

		String idPontoConsumoLegado = (String) filtro.get("pontoConsumoLegado");
		if ((idPontoConsumoLegado != null) && (!idPontoConsumoLegado.isEmpty())) {
			hql.append(" and ponto.codigoLegado = :codigoLegado ");
		}

		Boolean indicadorImpedeFaturamento = (Boolean) filtro.get("indicadorImpedeFaturamento");
		if (indicadorImpedeFaturamento != null) {
			hql.append(
					" and ( anorLei.bloquearFaturamento = :indicadorImpedeFaturamento or anorCon.bloquearFaturamento ="
							+ " :indicadorImpedeFaturamento )");
		}
		
		Integer n = 0;
		String numeroCiclo = (String) filtro.get("numeroCiclo");
		Integer anoMesReferencia = (Integer) filtro.get("anoMesReferencia");
	if ((anoMesReferencia != null) && (Integer.parseInt( numeroCiclo ) > n)) {
			hql.append(
					" and historico.anoMesFaturamento = :anoMesReferencia");
			hql.append(
					" and historico.numeroCiclo = :numeroCiclo");
		} 
		else if ((anoMesReferencia != null) && (Integer.parseInt( numeroCiclo ) < n)) {
			throw new NegocioException(
			"E obrigatorio o preenchimento do ciclo ao se preencher o Ano/Mês referência!");
            }

		Date dataLeituraInicio = (Date) filtro.get("dataLeituraInicio");
		Date dataLeituraFim = (Date) filtro.get("dataLeituraFim");
		
			
		if (dataLeituraInicio != null && dataLeituraFim != null) {
			if (dataLeituraFim.after(dataLeituraInicio) || dataLeituraFim.equals(dataLeituraInicio)) {
				hql.append(" and ( histmed.dataLeituraInformada between :dataLeituraInicio and :dataLeituraFim ) ");
			}  else {
			throw new NegocioException(
			"A data do período da leitura final deve ser posterior à data do período da leitura inicial!");
            }
		} 

		hql.append(" order by rota.numeroRota, ponto.numeroSequenciaLeitura )");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if ((idCliente != null) && (idCliente > CONSTANTE_NUMERO_ZERO)) {
			query.setLong("idCliente", idCliente);
		}
		if (!StringUtils.isEmpty(cepImovel)) {
			query.setString("cepImovel", cepImovel);
		}
		if ((matriculaImovel != null) && (matriculaImovel > CONSTANTE_NUMERO_ZERO)) {
			query.setLong("matriculaImovel", matriculaImovel);
		}
		if (indicadorCondominio != null) {
			query.setBoolean("indicadorCondominio", indicadorCondominio);
		}
		if ((matriculaCondominio != null) && (matriculaCondominio > CONSTANTE_NUMERO_ZERO)) {
			query.setLong("matriculaCondominio", matriculaCondominio);
		}
		if (!StringUtils.isEmpty(nomeImovel)) {
			query.setString("nomeImovel", Util.formatarTextoConsulta(nomeImovel));
		}
		if (!StringUtils.isEmpty(complementoImovel)) {
			query.setString("complementoImovel", Util.formatarTextoConsulta(complementoImovel));
		}
		if (!StringUtils.isEmpty(numeroImovel)) {
			query.setString("numeroImovel", numeroImovel);
		}
		if (idLocalidade != null && idLocalidade > CONSTANTE_NUMERO_ZERO) {
			query.setLong("idLocalidade", idLocalidade);
		}
		if (idSetorComercial != null && idSetorComercial > CONSTANTE_NUMERO_ZERO) {
			query.setLong("idSetorComercial", idSetorComercial);
		}
		if (idGrupoFaturamento != null && idGrupoFaturamento > CONSTANTE_NUMERO_ZERO) {
			query.setLong(CONSTANTE_ID_GRUPO_FATURAMENTO, idGrupoFaturamento);
		}
		if (idSegmento != null && idSegmento > CONSTANTE_NUMERO_ZERO) {
			query.setLong("idSegmento", idSegmento);
		}
		if (idSituacaoPontoConsumo != null && idSituacaoPontoConsumo > CONSTANTE_NUMERO_ZERO) {
			query.setLong("idSituacaoPontoConsumo", idSituacaoPontoConsumo);
		}
		if (idsAnormalidadeConsumo != null && idsAnormalidadeConsumo.length > CONSTANTE_NUMERO_ZERO) {
			query.setParameterList("idsAnormalidadeConsumo", idsAnormalidadeConsumo);
		}
		if (idsAnormalidadeLeitura != null && idsAnormalidadeLeitura.length > CONSTANTE_NUMERO_ZERO) {
			query.setParameterList("idsAnormalidadeLeitura", idsAnormalidadeLeitura);
		}
		if (idsRota != null && idsRota.length > CONSTANTE_NUMERO_ZERO) {
			query.setParameterList("idsRota", idsRota);
		}
		if ((idTipoConsumo != null) && (idTipoConsumo > CONSTANTE_NUMERO_ZERO)) {
			query.setLong("idTipoConsumo", idTipoConsumo);
		}
		if (consumoLido != null) {
			query.setBigDecimal("consumoLido", consumoLido);
		}
		if (percentualVariacaoConsumo != null) {
			query.setBigDecimal("percentualVariacaoConsumo", percentualVariacaoConsumo);
		}

		if (analisada != null) {
			query.setBoolean("analisada", analisada);
		}

		if ((idPontoConsumoLegado != null) && (!idPontoConsumoLegado.isEmpty())) {
			query.setString("codigoLegado", idPontoConsumoLegado);
		}

		if (indicadorImpedeFaturamento != null) {
			query.setBoolean("indicadorImpedeFaturamento", indicadorImpedeFaturamento);
		}

		if ((dataLeituraInicio != null && dataLeituraFim != null)
				&& (dataLeituraFim.after(dataLeituraInicio) || dataLeituraFim.equals(dataLeituraInicio))) {
			query.setDate("dataLeituraInicio", dataLeituraInicio);
			query.setDate("dataLeituraFim", dataLeituraFim);
		}

		if ((numeroCiclo != null) && (!numeroCiclo.isEmpty()) && (anoMesReferencia != null)) {
			query.setString("numeroCiclo", numeroCiclo);
			query.setInteger("anoMesReferencia", anoMesReferencia);
		}
		
		Collection<HistoricoConsumo> historicosConsumo = query.list();

		Integer quantidadeAnormalidadeConsumoConsecutivas = (Integer) filtro
				.get("quantidadeAnormalidadeConsumoConsecutivas");
		if (quantidadeAnormalidadeConsumoConsecutivas != null
				&& quantidadeAnormalidadeConsumoConsecutivas > CONSTANTE_NUMERO_ZERO && historicosConsumo != null
				&& idsAnormalidadeConsumo != null) {
			for (HistoricoConsumo historicoConsumo : historicosConsumo) {
				if (existeAnormalidadeConsumoConsecutivasPontoConsumo(
						historicoConsumo.getPontoConsumo().getChavePrimaria(),
						quantidadeAnormalidadeConsumoConsecutivas, idsAnormalidadeConsumo)) {
					retorno.add(historicoConsumo);
				}
			}
		} else {
			if (historicosConsumo != null) {
				retorno.addAll(historicosConsumo);
			}
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# listarTiposConsumo()
	 */
	@Override
	public Collection<TipoConsumo> listarTiposConsumo() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseTipoConsumo().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/**
	 * Existe anormalidade consumo consecutivas ponto consumo.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param quantidade the quantidade
	 * @param idsAnormalidadeConsumo the ids anormalidade consumo
	 * @return true, if successful
	 */
	private boolean existeAnormalidadeConsumoConsecutivasPontoConsumo(Long idPontoConsumo, Integer quantidade,
			Long... idsAnormalidadeConsumo) {

		boolean retorno = false;

		StringBuilder hql = new StringBuilder();
		hql.append(" select anorCon.chavePrimaria, historico.anoMesFaturamento, historico.numeroCiclo from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" left join historico.anormalidadeConsumo anorCon ");
		hql.append(" where historico.habilitado = true and historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo);

		Collection<Object[]> ultimosHistoricos = query.setMaxResults(quantidade).list();
		if (ultimosHistoricos != null && ultimosHistoricos.size() == quantidade) {
			for (Long idAnormalidadeConsumo : idsAnormalidadeConsumo) {
				retorno = true;
				for (Object[] historico : ultimosHistoricos) {
					if (!idAnormalidadeConsumo.equals(historico[CONSTANTE_NUMERO_ZERO])) {
						retorno = false;
						break;
					}
				}
				if (retorno) {
					break;
				}
			}
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# validarPontoConsumoSemHistoricoConsumo(java. util.Collection)
	 */
	@Override
	public void validarPontoConsumoSemHistoricoConsumo(Collection<HistoricoConsumo> listaHistoricoConsumo) throws NegocioException {

		if (listaHistoricoConsumo == null || listaHistoricoConsumo.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_PONTO_DE_CONSUMO_SEM_HISTORICO_CONSUMO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# validarSelecaoGrupoFaturamento(java.lang.Long )
	 */
	@Override
	public void validarSelecaoGrupoFaturamento(Long chavePrimaria) throws NegocioException {

		if (chavePrimaria == null || chavePrimaria <= CONSTANTE_NUMERO_ZERO) {
			throw new NegocioException(ERRO_NEGOCIO_GRUPO_FATURAMENTO_NAO_INFORMADO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# consistirLeituraMedicao(java.util.Collection , java.lang.Integer,
	 * java.lang.Integer)
	 */
	@Override
	public void consistirLeituraMedicao(Collection<PontoConsumo> pontosConsumoTmp, Integer anoMesFaturamento, Integer numeroCiclo,
			StringBuilder logProcessamento, Boolean isFaturaAvulso, HistoricoMedicao historico) throws GGASException {
		
		Collection<PontoConsumo> pontosConsumo = pontosConsumoTmp;
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		ControladorUnidade controladorUnidade = ServiceLocator.getInstancia().getControladorUnidade();

		ControladorRede controladorRede = ServiceLocator.getInstancia().getControladorRede();

		ControladorSegmento controladorSegmento = ServiceLocator.getInstancia().getControladorSegmento();

		ControladorFaixaPressaoFornecimento controladorFaixaPressaoFornecimento =
				ServiceLocator.getInstancia().getControladorFaixaPressaoFornecimento();

		ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();

		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();

		ControladorTributo controladorTributo = ServiceLocator.getInstancia().getControladorTributo();

		ControladorHistoricoMedicao controladorHistoricoMedicao = ServiceLocator.getInstancia().getControladorHistoricoMedicao();

		ControladorCronogramaFaturamento controladorCronogramaFaturamento =
				ServiceLocator.getInstancia().getControladorCronogramaFaturamento();
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorLeituraMovimento controladorLeituraMovimento = (ControladorLeituraMovimento) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);
		
		LogAnaliseLeituraConsumoTemp.getInstance().addInfo("--- INICIO LOG ---");

		Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumoAtivo = null;
		Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumoRecente = null;
		Map<Rota, Collection<PontoConsumo>> mapaPontoConsumoPorRota = null;
		Map<PontoConsumo, HistoricoConsumo> mapaUltimoHistoricoConsumoFaturado = null;
		Map<PontoConsumo, Collection<PontoConsumoTributoAliquota>> mapaPontoConsumoTributoAliquota = null;
		Map<PontoConsumo, BigDecimal> mapaTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente = null;
		Map<PontoConsumo, HistoricoMedicao> mapaUltimoHistoricoMedicao = null;
		Map<PontoConsumo, Collection<HistoricoMedicao>> mapaHistoricoMedicao = null;
		Map<PontoConsumo, Map<CityGate, List<PontoConsumoCityGate>>> mapaPontoConsumoCityGate = null;
		Set<HistoricoMedicao> historicoMedicaoInserido = new HashSet<HistoricoMedicao>();

		Collection<PontoConsumo> collecaoPontosConsumo =
				controladorPontoConsumo.removerPontosConsumoAguardandoAtivacaoComInstalacaoMedidor(pontosConsumo);

		if (CollectionUtils.isNotEmpty(collecaoPontosConsumo)) {

			Long[] chavesPontoConsumo = Util.collectionParaArrayChavesPrimarias(collecaoPontosConsumo);

			if (chavesPontoConsumo != null && chavesPontoConsumo.length > CONSTANTE_NUMERO_ZERO) {
				mapaContratoPontoConsumoAtivo = super.obterMapa(TAMANHO_MAXIMO_LISTA, new ArrayList<>(collecaoPontosConsumo),
						(chavesPrimarias) -> validarPontoConsumo(chavesPrimarias, controladorContrato, "obterContratoAtivoPontoConsumo"));

				mapaContratoPontoConsumoRecente = super.obterMapa(TAMANHO_MAXIMO_LISTA, new ArrayList<>(collecaoPontosConsumo),
						(chavesPrimarias) -> validarPontoConsumo(chavesPontoConsumo, controladorContrato,
								"consultarContratoPontoConsumoPorPontoConsumoRecente"));

				mapaUltimoHistoricoConsumoFaturado = super.obterMapa(TAMANHO_MAXIMO_LISTA, new ArrayList<>(collecaoPontosConsumo),
						(chavesPrimarias) -> validarPontoConsumo(chavesPrimarias, null, "obterUltimoHistoricoConsumoFaturado"));

				mapaPontoConsumoTributoAliquota = super.obterMapa(TAMANHO_MAXIMO_LISTA, new ArrayList<>(collecaoPontosConsumo),
						(chavesPrimarias) -> validarPontoConsumo(chavesPrimarias, controladorTributo,
								"listarTributoAliquotaPorPontoConsumo"));

				mapaTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente = super.obterMapa(TAMANHO_MAXIMO_LISTA,
						new ArrayList<>(collecaoPontosConsumo), (chavesPrimarias) -> validarPontoConsumo(chavesPrimarias, null,
								"consultarTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente"));

				mapaUltimoHistoricoMedicao = super.obterMapa(TAMANHO_MAXIMO_LISTA, new ArrayList<>(collecaoPontosConsumo),
						(chavesPrimarias) -> validarPontoConsumo(chavesPrimarias, controladorHistoricoMedicao,
								"obterUltimoHistoricoMedicaoFaturado"));

				mapaHistoricoMedicao = super.obterMapa(TAMANHO_MAXIMO_LISTA, new ArrayList<>(collecaoPontosConsumo),
						(chavesPrimarias) -> validarPontoConsumo(chavesPrimarias, controladorHistoricoMedicao,
								"consultarHistoricoMedicaoAtual"));

				mapaPontoConsumoCityGate = super.obterMapa(TAMANHO_MAXIMO_LISTA, new ArrayList<>(collecaoPontosConsumo),
						(chavesPrimarias) -> this.obterPontoConsumoCityGatePorPontoConsumo(chavesPrimarias));
			}

			mapaPontoConsumoPorRota = validarPontosDeConsumoPorRota(collecaoPontosConsumo, controladorPontoConsumo);
			
			Map<Rota, Map<String, Integer>> referenciaCicloAtualPorRota =
					this.obterReferenciaCicloAtualPorRota(mapaPontoConsumoPorRota, anoMesFaturamento, numeroCiclo, isFaturaAvulso);

			Map<PontoConsumo, Collection<HistoricoMedicao>> mapaHistoricoMedicaoCicloAtual =
					consultarHistoricoMedicaoNaoFaturadoPorRota(mapaPontoConsumoPorRota, referenciaCicloAtualPorRota);

			List<Rota> rotas = controladorRota.agruparRotasDePontosConsumo(collecaoPontosConsumo);

			Map<Rota, CronogramaRota> mapaUltimoCronogramaRealizadoPorPontoConsumo =
					controladorCronogramaFaturamento.obterUltimoCronogramaRealizadoPorRotas(rotas);

			Map<String, Unidade> unidadesPadraoParaConversao = controladorUnidade.obterUnidadesPadraoTemperaturaPressao();

			Long[] chavesPrimariasRede = controladorRede.obterChavesDeRedesDePontosDeConsumo(collecaoPontosConsumo);
			if (chavesPrimariasRede == null || chavesPrimariasRede.length == CONSTANTE_NUMERO_ZERO) {
				chavesPrimariasRede = new Long[CONSTANTE_NUMERO_UM];
				chavesPrimariasRede[CONSTANTE_NUMERO_ZERO] = 0L;
			}

			Map<Rede, List<Tronco>> mapaTroncosPorRede = controladorRede.consultarTroncosDaRede(chavesPrimariasRede);

			Collection<ContratoPontoConsumo> contratosPontoConsumo = agruparContratoPontoConsumoAtivoOuRecente(
					mapaContratoPontoConsumoAtivo, mapaContratoPontoConsumoRecente, chavesPontoConsumo);

			Long[] chavesContratoPontoConsumo = Util.collectionParaArrayChavesPrimarias(contratosPontoConsumo);
			if (chavesContratoPontoConsumo.length == CONSTANTE_NUMERO_ZERO) {
				chavesContratoPontoConsumo = new Long[CONSTANTE_NUMERO_UM];
				chavesContratoPontoConsumo[CONSTANTE_NUMERO_ZERO] = 0L;
			}

			Map<Long, Collection<ContratoPontoConsumoPCSAmostragem>> mapaContratoPontoConsumoPCSAmostragem =
					super.obterMapa(TAMANHO_MAXIMO_LISTA, new ArrayList<>(contratosPontoConsumo),
							(chavesPrimarias) -> this.consultarContratoPontoConsumoPCSAmostragem(chavesPrimarias));

			Map<Long, Collection<ContratoPontoConsumoPCSIntervalo>> mapaContratoPontoConsumoPCSIntervalo =
					super.obterMapa(TAMANHO_MAXIMO_LISTA, new ArrayList<>(contratosPontoConsumo),
							(chavesPrimarias) -> this.consultarContratoPontoConsumoPCSIntervalo(chavesPrimarias));

			Long[] chavesPrimariasSegmentos = controladorSegmento.obterChavesDeSegmentosDePontosConsumo(collecaoPontosConsumo);
			if (chavesPrimariasSegmentos.length == CONSTANTE_NUMERO_ZERO) {
				chavesPrimariasSegmentos = new Long[CONSTANTE_NUMERO_UM];
				chavesPrimariasSegmentos[CONSTANTE_NUMERO_ZERO] = 0L;
			}

			Map<Long, Collection<FaixaPressaoFornecimento>> mapaFaixaPressaoFornecimentoPorSegmento =
					controladorFaixaPressaoFornecimento.obterFaixaPressaoPressaoFornecimentoPorSegmentos(chavesPrimariasSegmentos);

			Map<String, Integer> referenciaCicloAtual = new HashMap<>();

			for (PontoConsumo pontoConsumo : collecaoPontosConsumo) {
				// caso não seja um ponto de consumo na rota de fiscalização
				if (!(pontoConsumo.getInstalacaoMedidor() != null && pontoConsumo.getSituacaoConsumo() != null && pontoConsumo
						.getSituacaoConsumo().getDescricao().equals(PontoConsumo.SITUACAO_PONTO_CONSUMO_AGUARDANDO_ATIVACAO))) {
					
					LogAnaliseLeituraConsumoTemp.getInstance().addInfo("### Ponto de Consumo..:" + pontoConsumo.getDescricao());

					if (!isFaturaAvulso.equals(true)) {
						if (referenciaCicloAtualPorRota != null) {
							referenciaCicloAtual = referenciaCicloAtualPorRota.get(pontoConsumo.getRota());
							this.anoMesFaturamento = referenciaCicloAtual.get(REFERENCIA);
							this.numeroCiclo = referenciaCicloAtual.get(CICLO);
						}
						LogAnaliseLeituraConsumoTemp.getInstance()
								.addInfo(" ## Referencia/Ciclo atual..:" + anoMesFaturamento + "/" + numeroCiclo);
					} else {
						referenciaCicloAtual.put(REFERENCIA, anoMesFaturamento);
						referenciaCicloAtual.put(CICLO, numeroCiclo);
					}

					Long chavePontoConsumo = pontoConsumo.getChavePrimaria();
					ContratoPontoConsumo contratoPontoConsumo = escolherContratoPontoConsumoAtivoOuRecente(mapaContratoPontoConsumoAtivo,
							mapaContratoPontoConsumoRecente, chavePontoConsumo);

					if (contratoPontoConsumo != null) {
						contratoPontoConsumo.setPontoConsumo(pontoConsumo);
					}

					if (contratoPontoConsumo != null) {
						if (contratoPontoConsumo.getContrato().getSituacao().getChavePrimaria() == ATIVO
								|| contratoPontoConsumo.getContrato().getSituacao().getChavePrimaria() == ADITADO) {
							this.consistirPontoConsumo(pontoConsumo, contratoPontoConsumo, referenciaCicloAtual, logProcessamento,
									isFaturaAvulso, historico, mapaUltimoCronogramaRealizadoPorPontoConsumo,
									mapaUltimoHistoricoConsumoFaturado, mapaPontoConsumoTributoAliquota,
									mapaTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente, mapaUltimoHistoricoMedicao,
									mapaHistoricoMedicao, unidadesPadraoParaConversao, mapaHistoricoMedicaoCicloAtual,
									mapaPontoConsumoCityGate, mapaTroncosPorRede, mapaContratoPontoConsumoPCSAmostragem,
									mapaContratoPontoConsumoPCSIntervalo, mapaFaixaPressaoFornecimentoPorSegmento, historicoMedicaoInserido);
							try {
								getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
							} catch (Exception e) {
								e.printStackTrace();
							}
						} else {
							logProcessamento.append("\r\n#####################################");
							logProcessamento.append("\r\nPonto de Consumo não será persistido:").append(pontoConsumo.getDescricao());
							logProcessamento.append("\r\nGerando anormalidade de CLIENTE SEM CONTRATO para o ponto de consumo");
							String codigoAnormalidadeClienteSemContrato = controladorConstanteSistema
									.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CLIENTE_SEM_CONTRATO);
							AnormalidadeConsumo anormalidadeConsumo = new AnormalidadeConsumoImpl();
							anormalidadeConsumo.setChavePrimaria(Long.parseLong(codigoAnormalidadeClienteSemContrato));

							Collection<LeituraMovimento> leituraMovimentos = controladorLeituraMovimento.consultarMovimentosLeituraPorRota(
									new Long[] { pontoConsumo.getRota().getChavePrimaria() },
									new Long[] { SituacaoLeituraMovimento.PROCESSADO }, anoMesFaturamento, numeroCiclo);

							for (LeituraMovimento leituraMovimento : leituraMovimentos) {
								if (leituraMovimento.getPontoConsumo().getChavePrimaria() == pontoConsumo.getChavePrimaria()) {
									logProcessamento.append("\r\nAtualizando leitura movimento com anormalidade de CLIENTE SEM CONTRATO");
									leituraMovimento.setAnormalidadeConsumo(anormalidadeConsumo);
									controladorLeituraMovimento.atualizar(leituraMovimento);
									logProcessamento.append("\r\nLeitura movimento atualizada com sucesso");
									break;
								}
							}
						}
					}

				}
			}
			
			imprimirLogHistoricosComAnormalidade(anoMesFaturamento, numeroCiclo, logProcessamento, rotas);		
		}
	}


	private void imprimirLogHistoricosComAnormalidade(Integer anoMesFaturamento, Integer numeroCiclo,
			StringBuilder logProcessamento, List<Rota> rotas) {
		for (Rota rota: rotas) {
			List<HistoricoConsumo> historicosComAnormalidade = this.consultarHistoricosComAnormalidadeDeConsumo(rota, anoMesFaturamento, numeroCiclo);

			if (!historicosComAnormalidade.isEmpty()) {
				Map<Boolean, List<HistoricoConsumo>> listaAux = 
						historicosComAnormalidade.stream().collect(Collectors.partitioningBy(e -> e.getHistoricoAtual().isAnalisada()));
				
				for(Map.Entry<Boolean, List<HistoricoConsumo>> entry  : listaAux.entrySet()) {
					if(entry.getKey()) {
						logProcessamento.append("\r\nPontos de consumo com anormalidade de consumo analisadas: " + entry.getValue().size());
					} else {
						logProcessamento.append("\r\nPontos de consumo com anormalidade de consumo não analisadas: " + entry.getValue().size());
					}
					for (HistoricoConsumo historicoComAnormalidade : entry.getValue()) {
						logProcessamento.append("\r\nPonto:" + historicoComAnormalidade.getPontoConsumo().getDescricao());
						logProcessamento.append("\rAnormalidade:" + historicoComAnormalidade.getAnormalidadeConsumo().getDescricao());
					}
					
				}
				
			}
		}
	}
	
	/**
	 * Agrupa as entidades ContratoPontoConsumo com contrato ativo ou recente.
	 *
	 * @param mapaContratoPontoConsumoAtivo
	 * @param mapaContratoPontoConsumoRecente
	 * @param chavesPontoConsumo
	 * @return coleção de ContratoPontoConsumo com contrato ativo ou recente.
	 */
	private Collection<ContratoPontoConsumo> agruparContratoPontoConsumoAtivoOuRecente(
			Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumoAtivo, Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumoRecente,
			Long[] chavesPontoConsumo) {

		Collection<ContratoPontoConsumo> contratosPontoConsumo = new ArrayList<>();
		for (Long chavePontoConsumo : chavesPontoConsumo) {
			ContratoPontoConsumo contratoPontoConsumo = escolherContratoPontoConsumoAtivoOuRecente(mapaContratoPontoConsumoAtivo,
					mapaContratoPontoConsumoRecente, chavePontoConsumo);
			if (!contratosPontoConsumo.contains(contratoPontoConsumo)) {
				contratosPontoConsumo.add(contratoPontoConsumo);
			}
		}
		return contratosPontoConsumo;
	}

	/**
	 * Obtém o Histórico de Medição não faturado por pontos de consumo agrupados por rota.
	 *
	 * @param mapaPontoConsumoPorRota
	 * @param referenciaCicloAtualPorRota
	 * @return coleção de históricos de medição por ponto de consumo
	 * @throws NegocioException
	 */
	private Map<PontoConsumo, Collection<HistoricoMedicao>> consultarHistoricoMedicaoNaoFaturadoPorRota(
			Map<Rota, Collection<PontoConsumo>> mapaPontoConsumoPorRota, Map<Rota, Map<String, Integer>> referenciaCicloAtualPorRota)
			throws NegocioException {

		ControladorHistoricoMedicao controladorHistoricoMedicao = ServiceLocator.getInstancia().getControladorHistoricoMedicao();

		Map<PontoConsumo, Collection<HistoricoMedicao>> mapaHistoricoMedicaoCicloAtual = new HashMap<>();
		for (Entry<Rota, Collection<PontoConsumo>> entry : mapaPontoConsumoPorRota.entrySet()) {

			Rota rota = entry.getKey();
			Collection<PontoConsumo> pontosConsumoDaRota = entry.getValue();

			Map<String, Integer> referenciaCicloAtual = referenciaCicloAtualPorRota.get(rota);
			Long[] chavesPontosPorRota = Util.collectionParaArrayChavesPrimarias(pontosConsumoDaRota);
			Map<PontoConsumo, Collection<HistoricoMedicao>> mapaHistoricoMedicaoDaRota = null;
			if (referenciaCicloAtual != null) {
				mapaHistoricoMedicaoDaRota = controladorHistoricoMedicao.consultarHistoricoMedicaoNaoFaturado(chavesPontosPorRota,
						referenciaCicloAtual.get(REFERENCIA), referenciaCicloAtual.get(CICLO));
				if (mapaHistoricoMedicaoDaRota != null) {
					mapaHistoricoMedicaoCicloAtual.putAll(mapaHistoricoMedicaoDaRota);
				}
			}

		}
		return mapaHistoricoMedicaoCicloAtual;
	}

	/**
	 * Obtém o ContratoPontoConsumo de um contrato ativo. Caso não exista um contrato ativo, se obtém o ContratoPontoConsumo do contrato
	 * recente.
	 *
	 * @param mapaContratoPontoConsumoAtivo
	 * @param mapaContratoPontoConsumoRecente
	 * @param chavePontoConsumo
	 * @return contratoPontoConsumo
	 */
	private ContratoPontoConsumo escolherContratoPontoConsumoAtivoOuRecente(Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumoAtivo,
			Map<Long, ContratoPontoConsumo> mapaContratoPontoConsumoRecente, Long chavePontoConsumo) {

		ContratoPontoConsumo contratoPontoConsumo = null;

		if (mapaContratoPontoConsumoAtivo != null) {
			contratoPontoConsumo = mapaContratoPontoConsumoAtivo.get(chavePontoConsumo);

		}

		if (mapaContratoPontoConsumoRecente != null && contratoPontoConsumo == null) {
			contratoPontoConsumo = mapaContratoPontoConsumoRecente.get(chavePontoConsumo);
		}

		return contratoPontoConsumo;
	}

	/**
	 * Obtém a Referencia/Ciclo por Rota inabilitando um ou mais historicos de Consumo de Pontos de Consumo para uma determinada
	 * Referencia/Ciclo de uma Rota.
	 *
	 * @param mapaPontoConsumoPorRota
	 * @param anoMesFaturamento
	 * @param numeroCiclo
	 * @param isFaturaAvulso
	 * @return referencia/ciclo por rota
	 * @throws NegocioException
	 */
	private Map<Rota, Map<String, Integer>> obterReferenciaCicloAtualPorRota(Map<Rota, Collection<PontoConsumo>> mapaPontoConsumoPorRota,
			Integer anoMesFaturamento, Integer numeroCiclo, Boolean isFaturaAvulso) throws NegocioException {

		ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();
		Map<Rota, Map<String, Integer>> referenciaCicloAtualPorRota = new HashMap<>();

		for (Entry<Rota, Collection<PontoConsumo>> entry : mapaPontoConsumoPorRota.entrySet()) {
			Map<String, Integer> referenciaCicloAtual = new HashMap<>();
			Rota rota = entry.getKey();
			if (!isFaturaAvulso.equals(true)) {
				if (anoMesFaturamento == null && numeroCiclo == null) {
					referenciaCicloAtual = controladorRota.obterReferenciaCicloAtual(rota);
				} else {
					referenciaCicloAtual.put(REFERENCIA, anoMesFaturamento);
					referenciaCicloAtual.put(CICLO, numeroCiclo);
				}
				referenciaCicloAtualPorRota.put(rota, referenciaCicloAtual);

				inativarHistoricoConsumoReferenciaCiclo(entry.getValue(), referenciaCicloAtual.get(REFERENCIA),
						referenciaCicloAtual.get(CICLO));
			}
		}

		return referenciaCicloAtualPorRota;
	}

	/**
	 * Consistir ponto consumo.
	 *
	 * @param pontoConsumo the ponto consumo
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param referenciaCicloAtual the referencia ciclo atual
	 * @param logProcessamento the log processamento
	 * @param isFaturaAvulso the is fatura avulso
	 * @param historico the historico
	 * @throws NumberException the number exception
	 * @throws GGASException the GGASexception
	 */
	private void consistirPontoConsumo(PontoConsumo pontoConsumo, ContratoPontoConsumo contratoPontoConsumo,
			Map<String, Integer> referenciaCicloAtual, StringBuilder logProcessamento, Boolean isFaturaAvulso, HistoricoMedicao historico,
			Map<Rota, CronogramaRota> mapaUltimoCronogramaRealizadoPorPontoConsumo,
			Map<PontoConsumo, HistoricoConsumo> mapaUltimoHistoricoConsumoFaturado,
			Map<PontoConsumo, Collection<PontoConsumoTributoAliquota>> mapaPontoConsumoTributoAliquota,
			Map<PontoConsumo, BigDecimal> mapaTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente,
			Map<PontoConsumo, HistoricoMedicao> mapaUltimoHistoricoMedicao,
			Map<PontoConsumo, Collection<HistoricoMedicao>> mapaHistoricoMedicao, Map<String, Unidade> unidadesPadraoParaConversao,
			Map<PontoConsumo, Collection<HistoricoMedicao>> mapaHistoricoMedicaoCicloAtual,
			Map<PontoConsumo, Map<CityGate, List<PontoConsumoCityGate>>> mapaPontoConsumoCityGate,
			Map<Rede, List<Tronco>> mapaTroncosPorRede,
			Map<Long, Collection<ContratoPontoConsumoPCSAmostragem>> mapaContratoPontoConsumoPCSAmostragem,
			Map<Long, Collection<ContratoPontoConsumoPCSIntervalo>> mapaContratoPontoConsumoPCSIntervalo,
			Map<Long, Collection<FaixaPressaoFornecimento>> mapaFaixaPressaoFornecimentoPorSegmento, Set<HistoricoMedicao> historicoMedicaoInserido) throws GGASException {

		if (logProcessamento == null) {
			logProcessamento = new StringBuilder();
		}

		logProcessamento.append("\r\nConsistindo Ponto de Consumo:").append(pontoConsumo.getDescricao());

		ControladorTributo controladorTributo =
				(ControladorTributo) ServiceLocator.getInstancia().getControladorNegocio(ControladorTributo.BEAN_ID_CONTROLADOR_TRIBUTO);
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);
		ControladorAnormalidade controladorAnormalidade = (ControladorAnormalidade) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorAnormalidade.BEAN_ID_CONTROLADOR_ANORMALIDADE);

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		HistoricoMedicao historicoMedicaoCicloAnterior = null;

		// Recupera a última medição com consumo
		// faturado para o ponto de consumo
		// informado.
		if (mapaUltimoHistoricoMedicao != null && mapaUltimoHistoricoMedicao.containsKey(pontoConsumo)) {
			historicoMedicaoCicloAnterior = mapaUltimoHistoricoMedicao.get(pontoConsumo);
		} else {
			historicoMedicaoCicloAnterior =
					controladorHistoricoMedicao.obterUltimoHistoricoMedicao(pontoConsumo.getChavePrimaria(), null, null, Boolean.TRUE);
		}

		HistoricoConsumo historicoConsumoAnterior;
		if (mapaUltimoHistoricoConsumoFaturado != null && mapaUltimoHistoricoConsumoFaturado.containsKey(pontoConsumo)) {
			historicoConsumoAnterior = mapaUltimoHistoricoConsumoFaturado.get(pontoConsumo);
		} else {
			historicoConsumoAnterior = this.obterUltimoHistoricoConsumoFaturado(pontoConsumo.getChavePrimaria());
		}

		// Verifica se não existir uma medição
		// anterior com consumo faturado para o
		// anoMes e ciclo atual,
		// busca a medição anterior conforme
		// regras já definidas.

		Collection<HistoricoMedicao> historicosMedicaoAux = mapaHistoricoMedicaoCicloAtual.get(pontoConsumo);
		if(historicosMedicaoAux != null && !historicosMedicaoAux.isEmpty()) {
			HistoricoMedicao historicoAux = historicosMedicaoAux.iterator().next();
			if (historicoMedicaoCicloAnterior != null && (historicoMedicaoCicloAnterior.getDataLeituraInformada()
					.before(historicoAux.getHistoricoInstalacaoMedidor().getDataAtivacao()) || historicoMedicaoCicloAnterior.getDataLeituraInformada()
					.before(historicoAux.getHistoricoInstalacaoMedidor().getData()))) {
				historicoMedicaoCicloAnterior = null;
			}
		}
		
		if (historicoMedicaoCicloAnterior == null) {

			if (contratoPontoConsumo != null) {

				LogAnaliseLeituraConsumoTemp.getInstance().addInfo(" ## Consistindo ponto de consumo com contrato ativo.");
				LogAnaliseLeituraConsumoTemp.getInstance()
						.addInfo(" ## Código Contrato Ativo do ponto de consumo..:" + contratoPontoConsumo.getChavePrimaria());

				Integer quantidadeCiclo = null;

				if (contratoPontoConsumo.getPeriodicidade() == null) {

					throw new NegocioException(ERRO_NEGOCIO_CONTRATO_NAO_POSSUI_PERIODICIDADE,
							contratoPontoConsumo.getPontoConsumo().getDescricao());

				}

				if (contratoPontoConsumo.getPeriodicidade() != null) {

					quantidadeCiclo = contratoPontoConsumo.getPeriodicidade().getQuantidadeCiclo();

				}

				if (quantidadeCiclo != null) {
					// Obter referencia/ciclo anterior ao atual
					Map<String, Integer> referenciaCicloAnterior = Util.regredirReferenciaCiclo(referenciaCicloAtual.get(REFERENCIA),
							referenciaCicloAtual.get(CICLO), quantidadeCiclo);
					
					if(quantidadeCiclo >= 4) {
						referenciaCicloAnterior.put(CICLO, referenciaCicloAtual.get(CICLO) - 1);
					}
					// Obter a maior leitura do ciclo anterior
					historicoMedicaoCicloAnterior = controladorHistoricoMedicao.obterUltimoHistoricoMedicao(pontoConsumo.getChavePrimaria(),
							referenciaCicloAnterior.get(REFERENCIA), referenciaCicloAnterior.get(CICLO));
					// Se o hitorico consultado retornar null, obtem a maior leitura da referência anterior
					historicoMedicaoCicloAnterior = obtemMaiorLeituraReferenciaAnterior(pontoConsumo, referenciaCicloAtual,
							controladorHistoricoMedicao, historicoMedicaoCicloAnterior, referenciaCicloAnterior);

				}

				LogAnaliseLeituraConsumoTemp.getInstance().addInfo(" ## Quantidade de ciclos definida no contrato..:" + quantidadeCiclo);

			} else {

				LogAnaliseLeituraConsumoTemp.getInstance().addInfo(" ## Consistindo ponto de consumo sem contrato ativo.");

				ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
						.getInstancia().getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

				CronogramaRota cronogramaRota = null;

				if (pontoConsumo.getRota() != null) {
					if (mapaUltimoCronogramaRealizadoPorPontoConsumo != null
							&& mapaUltimoCronogramaRealizadoPorPontoConsumo.containsKey(pontoConsumo.getRota())) {
						cronogramaRota = mapaUltimoCronogramaRealizadoPorPontoConsumo.get(pontoConsumo.getRota());
					} else {
						cronogramaRota = controladorCronogramaFaturamento
								.obterUltimoCronogramaRealizadoPorPontoConsumo(pontoConsumo.getRota().getChavePrimaria());
					}
				}

				if (cronogramaRota == null) {

					historicoMedicaoCicloAnterior = controladorHistoricoMedicao.obterUltimoHistoricoMedicao(pontoConsumo.getChavePrimaria(),
							referenciaCicloAtual.get(REFERENCIA), referenciaCicloAtual.get(CICLO));

				} else {

					historicoMedicaoCicloAnterior = controladorHistoricoMedicao.obterUltimoHistoricoMedicao(pontoConsumo.getChavePrimaria(),
							cronogramaRota.getAnoMesReferencia(), cronogramaRota.getNumeroCiclo());

				}

			}
		}

		LogAnaliseLeituraConsumoTemp.getInstance().addInfo(
				" ## Referencia/Ciclo anterior ao atual..:" + referenciaCicloAtual.get(REFERENCIA) + "/" + referenciaCicloAtual.get(CICLO));

		HistoricoMedicao historicoMedicaoAnterior = historicoMedicaoCicloAnterior;

		Boolean primeiraMedicao = false;

		if (historicoMedicaoAnterior == null) {
			primeiraMedicao = true;
		}

		if (historicoMedicaoAnterior != null) {
			LogAnaliseLeituraConsumoTemp.getInstance()
					.addInfo(" ## Código maior Leitura do ciclo anterior..:" + historicoMedicaoAnterior.getChavePrimaria());
		} else {
			LogAnaliseLeituraConsumoTemp.getInstance().addInfo(" ## Código maior Leitura do ciclo anterior..:" + " ");
		}

		// Obter os historicos de leitura do ponto
		// de consumo
		Collection<HistoricoMedicao> historicosMedicaoCicloAtual = null;

		if (isFaturaAvulso != null && isFaturaAvulso.equals(false)) {
			if (mapaHistoricoMedicaoCicloAtual != null && mapaHistoricoMedicaoCicloAtual.containsKey(pontoConsumo)) {
				historicosMedicaoCicloAtual = mapaHistoricoMedicaoCicloAtual.get(pontoConsumo);
			} else {
				historicosMedicaoCicloAtual = controladorHistoricoMedicao.consultarHistoricoMedicaoNaoFaturado(
						pontoConsumo.getChavePrimaria(), referenciaCicloAtual.get(REFERENCIA), referenciaCicloAtual.get(CICLO));
			}
		} else if (isFaturaAvulso != null && isFaturaAvulso.equals(true)) {
			historicosMedicaoCicloAtual = new ArrayList<HistoricoMedicao>();
			historicosMedicaoCicloAtual.add(historico);
		}

		if (CollectionUtils.isNotEmpty(historicosMedicaoCicloAtual)) {
			boolean isMedidorIndependente = isMedidorIndependente(historicosMedicaoCicloAtual.iterator().next());

			if (!isMedidorIndependente) {
				LogAnaliseLeituraConsumoTemp.getInstance()
						.addInfo(" ## Quantidade de historicos de leitura do ponto de consumo..:" + historicosMedicaoCicloAtual.size());

				logProcessamento.append("\r\nQuantidade de historicos de leitura do ponto de consumo:")
						.append(historicosMedicaoCicloAtual.size());
			}

		} else {
			LogAnaliseLeituraConsumoTemp.getInstance()
					.addInfo(" ## Quantidade de historicos de leitura do ponto de consumo..:" + CONSTANTE_NUMERO_ZERO);
			logProcessamento.append("\r\nQuantidade de historicos de leitura do ponto de consumo:" + CONSTANTE_NUMERO_ZERO);
		}

		Map<String, Object> mapAtributosConsumo = new HashMap<String, Object>();
		mapAtributosConsumo.put(REFERENCIA, referenciaCicloAtual.get(REFERENCIA));
		mapAtributosConsumo.put(CICLO, referenciaCicloAtual.get(CICLO));
		mapAtributosConsumo.put(PONTO_CONSUMO, pontoConsumo);
		mapAtributosConsumo.put(HISTORICO_MEDICAO_ANTERIOR, historicoMedicaoAnterior);

		// Verificar as leituras
		if (CollectionUtils.isNotEmpty(historicosMedicaoCicloAtual)) {

			boolean leituraInformada = false;
			boolean possuiAnormalidade = false;
			boolean medidorSubtituido = false;

			BigDecimal leituraAtual = BigDecimal.ZERO;
			Set<HistoricoConsumo> historicosConsumoDiarios = new HashSet<HistoricoConsumo>();

			HistoricoMedicao historicoUltimaLeitura =
					new ArrayList<HistoricoMedicao>(historicosMedicaoCicloAtual).get(historicosMedicaoCicloAtual.size() - CONSTANTE_NUMERO_UM);

			// Obter consumo médio do ponto de
			// consumo e consumo apurado
			BigDecimal consumoMedioPonto = null;
			BigDecimal consumoApuradoMedioPonto = null;
			BigDecimal consumoMedioApuradoDiarioPonto = null;

			Collection<HistoricoConsumo> listaInsercaoHitoricoConsumo = new ArrayList<HistoricoConsumo>();
			Map<String, List<HistoricoConsumo>> mapaComposicaoVirtual = new HashMap<String, List<HistoricoConsumo>>();
			
			AnormalidadeConsumo anormalidadeBloqueiaFaturamento  = null;
			// cria os historicos de consumo
			for (HistoricoMedicao historicoMedicao : historicosMedicaoCicloAtual) {

				boolean isMedidorIndependente = isMedidorIndependente(historicoMedicao);

				HistoricoConsumo historicoConsumo = this.adicionarHistoricoConsumo(mapAtributosConsumo);

				if (isMedidorIndependente) {
					historicoConsumo.setHabilitado(Boolean.FALSE);
					historicoConsumo.setIndicadorMedicaoIndependenteAtual(Boolean.TRUE);
				} else {
					historicoConsumo.setHabilitado(Boolean.TRUE);
				}
				// Se for uma fatura avulsa... o historico de medicao atual e anterior são os mesmos
				if (isFaturaAvulso != null && isFaturaAvulso) {
					historicoConsumo.setHistoricoAnterior(historicoMedicao);
				}
				historicoConsumo.setHistoricoAtual(historicoMedicao);
				historicoConsumo.setPontoConsumo(pontoConsumo);

				// Fatura Avulsa só possui 1 dia de consumo
				int numeroDiasConsumo = CONSTANTE_NUMERO_ZERO;
				InstalacaoMedidor instalacaoMedidor = pontoConsumo.getInstalacaoMedidor();
				if (isFaturaAvulso != null && !isFaturaAvulso) {
					// Caso a medição venha de um medidor independente, a intalaçaoMedidor do pontoConsumo não é a utilizada para medição.
					// Deve-se usar a instalacaoMedidor do medidor independente, que é a mesma da historicoMedicao nesse caso.
					if (isMedidorIndependente) {
						numeroDiasConsumo = this.calcularDiasConsumo(historicoMedicao, historicoMedicaoAnterior,
								historicoMedicao.getHistoricoInstalacaoMedidor(), Boolean.FALSE);
					} else {
						numeroDiasConsumo = this.calcularDiasConsumo(historicoMedicao, historicoMedicaoAnterior, instalacaoMedidor,Boolean.FALSE);
					}
				} else {
					numeroDiasConsumo = CONSTANTE_NUMERO_UM;
				}
				Map<String, BigDecimal> consumoMedioPontoMap = this.calcularConsumoMedio(pontoConsumo, contratoPontoConsumo,
						referenciaCicloAtual.get(REFERENCIA), referenciaCicloAtual.get(CICLO), numeroDiasConsumo);

				if (consumoMedioPontoMap != null) {
					if (consumoMedioPontoMap.get("consumoMedio") != null) {
						consumoMedioPonto = consumoMedioPontoMap.get("consumoMedio");
					}
					if (consumoMedioPontoMap.get("consumoMedioApurado") != null) {
						consumoApuradoMedioPonto = consumoMedioPontoMap.get("consumoMedioApurado");
					}
					if (consumoMedioPontoMap.get("consumoMedioApuradoDiario") != null) {
						consumoMedioApuradoDiarioPonto = consumoMedioPontoMap.get("consumoMedioApuradoDiario");
					}
				}

				historicoConsumo.setDiasConsumo(numeroDiasConsumo);
				historicoConsumo.setConsumoMedio(consumoMedioPonto);
				historicoConsumo.setConsumoApuradoMedio(consumoApuradoMedioPonto);

				// Verificar anormalidade
				if (historicoMedicao.getAnormalidadeLeituraInformada() != null) {

					possuiAnormalidade = true;

				} else {

					possuiAnormalidade = false;
					historicoMedicao.setAnormalidadeLeituraFaturada(null);

				}

				// Verificar leitura informada
				if (historicoMedicao.getNumeroLeituraInformada() != null) {

					leituraInformada = true;

				} else {

					leituraInformada = false;

				}
				BigDecimal leituraAnteriorPonto;
				if (mapaHistoricoMedicao != null && mapaHistoricoMedicao.containsKey(pontoConsumo)) {
					Collection<HistoricoMedicao> listaHistoricoMedicao = mapaHistoricoMedicao.get(pontoConsumo);
					leituraAnteriorPonto = this.obterLeituraAnteriorPontoConsumo(listaHistoricoMedicao, historicoMedicao, pontoConsumo);
				} else {
					leituraAnteriorPonto = this.obterLeituraAnteriorPontoConsumo(null, historicoMedicao, pontoConsumo);
				}

				// Obter o consumo anterior

				// Verificar Substituição de
				// Medidor
				// Se não houve subst. passará
				// para o fluxo de leitura
				// informada e anormalidade. Caso
				// contrário, deve-se desviar
				// diretamente para aplicação dos
				// fatores
				if (!isMedidorIndependente) {
					medidorSubtituido = this.verificarSubstituicaoMedidor(historicoMedicaoAnterior, historicoMedicao);
				}

				if (medidorSubtituido) {
					String codigoAnormalidadeMedidorSubsituido = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_MEDIDOR_SUBSTITUIDO);
					final AnormalidadeConsumo anormalidadeMedidorSubstituido =
							controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeMedidorSubsituido));

					final HistoricoMedicao finalHistoricoMedicaoCicloAnterior = historicoMedicaoCicloAnterior;
					aplicarAnormalidadeSeAtiva(anormalidadeMedidorSubstituido, historicoConsumo, () -> {
						// Regra de composição de
						// consumo
						this.aplicarComposicaoConsumo(finalHistoricoMedicaoCicloAnterior, historicoMedicao, historicoConsumo, pontoConsumo);
					});

				} else {

					// Verificar leitura informada
					if (leituraInformada) {

						// Verificação entre leitura anterior e atual
						if (historicoMedicao.getNumeroLeituraInformada() != null) {
							leituraAtual = historicoMedicao.getNumeroLeituraInformada();
						} else {
							leituraAtual = BigDecimal.ZERO;
						}

						if (historicoMedicao.getHistoricoInstalacaoMedidor() != null
								&& historicoMedicao.getHistoricoInstalacaoMedidor().getVazaoCorretor() == null ) {

							if (leituraAtual.compareTo(leituraAnteriorPonto) > CONSTANTE_NUMERO_ZERO) {

								// Se a leitura atual for maior que a leitura
								// anterior (Subfluxo Leitura Maior que Leitura Anterior)
								this.subFluxoLeituraMaiorAnterior(historicoMedicaoAnterior, leituraAnteriorPonto, leituraAtual,
										historicoConsumo, historicoMedicao);

							} else if (leituraAtual.compareTo(leituraAnteriorPonto) == CONSTANTE_NUMERO_ZERO) {

								// Se a leitura atual for igual a leitura
								// anterior (Subfluxo Leitura igual a Leitura Anterior)
								this.subFluxoLeituraIgualAnterior(leituraAnteriorPonto, leituraAtual, historicoConsumo);

							} else if (leituraAtual.compareTo(leituraAnteriorPonto) < CONSTANTE_NUMERO_ZERO) {

								// Se a leitura atual for menor que a leitura
								// anterior (Subfluxo Leitura Menor que Leitura Anterior)
								this.subFluxoLeituraMenorAnterior(historicoMedicaoAnterior, leituraAnteriorPonto, leituraAtual,
										historicoConsumo, historicoMedicao, medidorSubtituido);

							}

							// apos a geração da media no outro faturamento subtrair o volume da fatura que utilizou a media
							if (historicoConsumoAnterior != null) {

								BigDecimal consumoCreditoMedia = historicoConsumoAnterior.getConsumoCreditoMedia();
								if (consumoCreditoMedia != null) {

									BigDecimal resultado = historicoConsumo.getConsumo().subtract(consumoCreditoMedia);
									if (resultado.compareTo(BigDecimal.ZERO) <= CONSTANTE_NUMERO_ZERO) {
										historicoConsumo
												.setConsumoCreditoMedia(consumoCreditoMedia.subtract(historicoConsumo.getConsumo()));
										historicoConsumo.setConsumo(BigDecimal.ZERO);
									} else {
										historicoConsumo.setConsumo(resultado);
										historicoConsumo.setConsumoCreditoMedia(BigDecimal.ZERO);
									}
								}
							}

						} else {
							
							if (leituraAtual.compareTo(leituraAnteriorPonto) < CONSTANTE_NUMERO_ZERO) {

								// Se a leitura atual for menor que a leitura
								// anterior (Subfluxo Leitura Menor que Leitura Anterior)
								this.subFluxoLeituraMenorAnterior(historicoMedicaoAnterior, leituraAnteriorPonto,
										leituraAtual, historicoConsumo, historicoMedicao, medidorSubtituido);
							} else {
								historicoConsumo.setConsumo(historicoMedicao.getConsumoMedidor());
								historicoConsumo.setConsumoMedido(historicoMedicao.getConsumoCorretor());
								historicoConsumo.setFatorPTZ(historicoMedicao.getFatorPTZCorretor());
								String codigoTipoConsumo = controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL);
								historicoConsumo.setTipoConsumo(this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo)));
							}
						}

					} else {

						String codigoTipoConsumo =
								controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL);
						historicoConsumo.setTipoConsumo(this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo)));

						if (!possuiAnormalidade) {

							// atualizar o valor de credito da media para o novo historico de consumo
							if (historicoConsumoAnterior != null) {
								BigDecimal consumoCreditoMedia = historicoConsumoAnterior.getConsumoCreditoMedia();
								if (consumoCreditoMedia != null) {
									if (historicoConsumo.getConsumoCreditoMedia() == null) {
										historicoConsumo.setConsumoCreditoMedia(consumoCreditoMedia);
									} else {
										historicoConsumo
												.setConsumoCreditoMedia(historicoConsumo.getConsumoCreditoMedia().add(consumoCreditoMedia));
									}
								}
							}
							historicoMedicao.setAnormalidadeLeituraFaturada(null);
							this.subFluxoSemLeitura(historicoConsumo, historicoMedicao, consumoMedioPontoMap, contratoPontoConsumo);
						}
					}

					if (possuiAnormalidade) {
						historicoMedicao.setAnormalidadeLeituraFaturada(historicoMedicao.getAnormalidadeLeituraInformada());
						this.subFluxoAnormalidadeLeituraInformada(historicoConsumo, leituraInformada);
					}

					// Atualizar a medição atual
					controladorHistoricoMedicao.atualizarEmBatch(historicoMedicao, getClasseHistoricoMedicao());
				}

				// Aplicar a correção de fatores
				// PCS + PTZ
				// Retornar o consumo corrigido
				// parametros[historico
				// consumo/ contrato] 6, 6.1 sem o
				// uc042
				Date dataLeituraAnteriorInformada;
				if (isMedidorIndependente) {
					dataLeituraAnteriorInformada = controladorHistoricoMedicao.setarDataLeituraAnterior(null,
							historicoMedicao.getPontoConsumo(), historicoMedicao.getHistoricoInstalacaoMedidor().getMedidor());
				} else {
					dataLeituraAnteriorInformada = controladorHistoricoMedicao.setarDataLeituraAnterior(instalacaoMedidor, null, null);
				}

				if (historicoMedicao.getDataLeituraAnterior() != null) {

					dataLeituraAnteriorInformada = historicoMedicao.getDataLeituraAnterior();

				}

				LogAnaliseLeituraConsumoTemp.getInstance()
						.addInfo(" ## Data Leitura Anterior Informada..:" + df.format(dataLeituraAnteriorInformada));

				if (contratoPontoConsumo != null) {
					ConstanteSistema constanteTipoLeituraEletroCorretor = controladorConstanteSistema
							.obterConstantePorCodigo(Constantes.C_TIPO_LEITURA_ELETROCORRETOR);
					
					Collection<Date> datasDoIntervaloLeitura =
							obterIntervaloDatasLeitura(dataLeituraAnteriorInformada, historicoMedicao.getDataLeituraInformada());
					
					// Obter a maior leitura do
					// ciclo anterior
					logProcessamento.append("\rAPLICANDO CORREÇÃO PARA O CONSUMO");
					this.aplicarCorrecaoConsumo(mapaPontoConsumoCityGate, mapaTroncosPorRede, historicoConsumo, contratoPontoConsumo,
							historicoMedicao, dataLeituraAnteriorInformada, historicoMedicao.getDataLeituraInformada(),
							historicoUltimaLeitura.getDataLeituraInformada(), unidadesPadraoParaConversao,
							mapaContratoPontoConsumoPCSAmostragem, mapaContratoPontoConsumoPCSIntervalo,
							mapaFaixaPressaoFornecimentoPorSegmento);
					
					if (contratoPontoConsumo.getPeriodicidade() != null
							&& contratoPontoConsumo.getPeriodicidade().getNumeroMaximoDiasCiclo() != null
							&& contratoPontoConsumo.getPeriodicidade()
									.getNumeroMaximoDiasCiclo() < datasDoIntervaloLeitura.size()) {

						String codigoAnormalidade = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_PERIODO_LEITURA_MAIOR);
						AnormalidadeConsumo anormalidade = controladorAnormalidade
								.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidade));
						aplicarAnormalidadeSeAtiva(anormalidade, historicoConsumo);

					} else if (contratoPontoConsumo.getPeriodicidade() != null
							&& contratoPontoConsumo.getPeriodicidade().getNumeroMinimoDiasCiclo() != null
							&& contratoPontoConsumo.getPeriodicidade()
									.getNumeroMinimoDiasCiclo() > datasDoIntervaloLeitura.size()
							&& !(historicoMedicao.getPontoConsumo().getRota().getTipoLeitura()
									.getChavePrimaria() == Long
											.parseLong(constanteTipoLeituraEletroCorretor.getValor()))) {

						String codigoAnormalidade = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_PERIODO_LEITURA_MENOR);
						AnormalidadeConsumo anormalidade = controladorAnormalidade
								.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidade));
						aplicarAnormalidadeSeAtiva(anormalidade, historicoConsumo);						

					} else {

						// Verificar se o ponto se
						// consumo possui leitura
						// diaria ou periódica
						if (contratoPontoConsumo.getTipoMedicao().getChavePrimaria() == TipoMedicao.CODIGO_PERIODICA) {

							// Validar estouro de
							// consumo, alto e baixo

							primeiraMedicao = false;
							if (!this.verificarMudancaTitularidadeContrato(historicoConsumo) && !primeiraMedicao && !historicoConsumo.possuiAnormalidadeConsumoImpeditiva()) {
								this.verificarFaixaLeituraInformada(historicoConsumo, consumoMedioApuradoDiarioPonto);
							}

						} else {
							// Adicionar o historico
							// de consumo para a
							// totalização do consumo
							// diário. (MEDIDOR INDEPENDENTE NÃO POSSUI SINTETIZADOR DIRETAMENTE)
							if (!isMedidorIndependente) {
								historicosConsumoDiarios.add(historicoConsumo);
							}
						}
					}

					if (contratoPontoConsumo.getTipoMedicao().getChavePrimaria() == TipoMedicao.CODIGO_PERIODICA) {
						historicoConsumo.setIndicadorConsumoCiclo(Boolean.TRUE);
					}
				} else {

					// Adicionar o historico de
					// consumo para a totalização
					// do consumo diário
					// diário. (MEDIDOR INDEPENDENTE NÃO POSSUI SINTETIZADOR DIRETAMENTE)
					if (!isMedidorIndependente) {
						historicosConsumoDiarios.add(historicoConsumo);
					}

					String codigoAnormalidadeConsumo = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_PONTO_CONSUMO_SEM_CONTRATO_ATIVO);
					AnormalidadeConsumo anormalidadeConsumo =
							controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumo));

					aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo);

				}

				// Verificar se houve consumo
				// informado
				if (historicoMedicao.getConsumoInformado() != null) {
					this.aplicarConsumoInformado(historicoConsumo, historicoMedicao.getConsumoInformado());
				}

				if (historicoConsumo.getAnormalidadeConsumo() == null && historicoConsumo.getConsumoApurado() != null
						&& historicoConsumo.getConsumoApurado().compareTo(BigDecimal.ZERO) <= CONSTANTE_NUMERO_ZERO) {

					int qtdConsumoZero = 0;

					controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
					controladorAnormalidade = (ControladorAnormalidade) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorAnormalidade.BEAN_ID_CONTROLADOR_ANORMALIDADE);

					String codigoAnormalidadeConsumoZero = (String) controladorParametrosSistema
							.obterValorDoParametroPorCodigo(CODIGO_ANORMALIDADE_CONSUMO_ZERO);

					AnormalidadeConsumo anormalidadeConsumo = null;
					if (codigoAnormalidadeConsumoZero != null) {
						anormalidadeConsumo = controladorAnormalidade
								.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumoZero));
					}
					
					Collection<HistoricoConsumo> listaUltimosConsumoHistorico = consultarHistoricoAgrupadoConsumo(
							historicoConsumo.getPontoConsumo().getChavePrimaria(), anormalidadeConsumo);

					Iterator<HistoricoConsumo> iteratorListaUltimosConsumoHistorico = listaUltimosConsumoHistorico
							.iterator();
					while (iteratorListaUltimosConsumoHistorico.hasNext()) {
						HistoricoConsumo histTemp = iteratorListaUltimosConsumoHistorico.next();
						
						if (histTemp.getAnoMesFaturamento() == historicoConsumo.getAnoMesFaturamento()
								&& histTemp.getNumeroCiclo() == historicoConsumo.getNumeroCiclo()) {
							continue;
						}
						

						if (histTemp.getConsumoMedido() != null
								&& histTemp.getConsumoMedido().compareTo(BigDecimal.ZERO) == 0) {
							qtdConsumoZero++;
						}

					}
					final int quantidadeFinalConsumoZero = qtdConsumoZero;

					System.out.println("\n\n");
					final ControladorAnormalidade finalControladorAnormalidade = controladorAnormalidade;
					final AnormalidadeConsumo finalAnormalidadeConsumo = anormalidadeConsumo;
					aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo, () -> {

						/**
						 * Enviando para analises imoveis residenciais de acordo com os parametros
						 * configurados.
						 */
						if (pontoConsumo != null) {
							Imovel imovel = Fachada.getInstancia()
									.buscarImovelPorChave(pontoConsumo.getImovel().getChavePrimaria());
							int codigoModalidadeMedicaoImovel = imovel.getModalidadeMedicaoImovel() != null
									? imovel.getModalidadeMedicaoImovel().getCodigo()
									: MODALIDADE_MEDICAO_INEXISTENTE;
							Collection<AnormalidadeSegmentoParametro> parametrizacao = finalControladorAnormalidade
									.obterAnormalidadeSegmentoParametros(finalAnormalidadeConsumo,
											pontoConsumo.getSegmento(), codigoModalidadeMedicaoImovel,
											pontoConsumo.getRamoAtividade());
							// Se encontrou uma ou mais parametrizacao geral
							// ou mais especifica, deve ser enviado para analise

							System.out.println( pontoConsumo.getChavePrimaria() + " -> QTD PARAMETRIZACAO: "+ parametrizacao.size());
							Long quantidadeOcorrencias = 0L;
							if (finalAnormalidadeConsumo != null
									&& finalAnormalidadeConsumo.getNumeroOcorrenciaConsecutivasAnormalidade() != null) {
								quantidadeOcorrencias = finalAnormalidadeConsumo
										.getNumeroOcorrenciaConsecutivasAnormalidade();
							}
							if (quantidadeFinalConsumoZero >= quantidadeOcorrencias
									&& CollectionUtils.isNotEmpty(parametrizacao)) {
								System.out.println("Ponto entrou no if consumo 0 >= ocorrencias");
								enviarHistoricoConsumoParaAnalise(historicoConsumo, parametrizacao);
							} else {
								System.out.println("Ponto entrou no if consumo 0 < ocorrencias");
								historicoConsumo.setAnormalidadeConsumo(null);
							}

						} else {
							historicoConsumo.setAnormalidadeConsumo(null);
						}
					});
					
				}

				// adiciona o consumoMedio para o primeiro historicoConsumo do ciclo, apenas para coletor de dados.
				if (historicoConsumo.getConsumoMedio() == null
						&& (historicosConsumoDiarios == null || historicosConsumoDiarios.isEmpty())) {
					historicoConsumo.setConsumoMedio(historicoConsumo.getConsumo());
				}

				if (isMedidorIndependente) {

					// Se houver historico Consumo para a mesma data, seta setIndicadorMedicaoIndependenteAtual do mesmo para false
					// tornando-o apenas
					// um histórico.
					atualizaHistoricoMIAnterior(pontoConsumo, historicoMedicao);

					String dataLeituraInformadaFormatada =
							Util.converterDataParaStringSemHora(historicoMedicao.getDataLeituraInformada(), Constantes.FORMATO_DATA_BR);

					List<HistoricoConsumo> listaHistoricoConsumo = mapaComposicaoVirtual.get(dataLeituraInformadaFormatada);

					historicoConsumo.setHabilitado(false);
					historicoConsumo.setMedidor(historicoMedicao.getHistoricoInstalacaoMedidor().getMedidor());

					// agrupa consumos de medidores independentes diferentes do mesmo dia em uma lista para posterior calculo da composicao.
					listaHistoricoConsumo = agrupaConsumosParaComposicao(historicoConsumo, listaHistoricoConsumo);

					mapaComposicaoVirtual.put(dataLeituraInformadaFormatada, listaHistoricoConsumo);
				}

				if (Objects.isNull(historicoConsumo.getTipoConsumo())) {
					String codigoTipoConsumo =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_SEM_CONSUMO);
					TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
					historicoConsumo.setTipoConsumo(tipoConsumo);
				}
				
				
				if (historicoConsumo.getAnormalidadeConsumo() == null
						|| (historicoConsumo.getAnormalidadeConsumo() != null
								&& !historicoConsumo.getAnormalidadeConsumo().getBloquearFaturamento())) {
					if (historicoConsumo.getConsumoApurado() == null) {
						String codigoAnormalidade = controladorConstanteSistema
								.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_APURADO_NULO);
						AnormalidadeConsumo anormalidade = controladorAnormalidade
								.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidade));
						aplicarAnormalidadeSeAtiva(anormalidade, historicoConsumo);
					} else if (historicoConsumo.getConsumoApurado().compareTo(BigDecimal.ZERO) < 0) {
						String codigoAnormalidade = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
								Constantes.C_ANORMALIDADE_CONSUMO_APURADO_NEGATIVO);
						AnormalidadeConsumo anormalidade = controladorAnormalidade
								.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidade));
						aplicarAnormalidadeSeAtiva(anormalidade, historicoConsumo);
					}
				}

				if (isMedidorIndependente) {
					// Adicionar historico consumo na lista para posterior insercao
					listaInsercaoHitoricoConsumo.add(historicoConsumo);
				} else if (!historicoMedicaoInserido.contains(historicoConsumo.getHistoricoAtual())){
					
					if (!possuiHistoricoConsumo(historicoConsumo)) {
						this.inserirEmBatch(historicoConsumo, Boolean.TRUE);
					}
					logProcessamento.append("\r ");
					logProcessamento.append("	Historico de consumo:");
					logProcessamento.append(" ( ");

					if (historicoConsumo.getHistoricoAtual() != null) {
						logProcessamento.append(Optional.ofNullable(historicoConsumo.getHistoricoAtual().getDataLeituraInformada())
								.map(df::format).orElse(" Sem data de leitura informada"));
					} else {
						logProcessamento.append("" + ") ");
					}
					logProcessamento.append(" ) - ");
					
					if(historicoConsumo.getConsumoApurado() != null) {
						logProcessamento.append(historicoConsumo.getConsumoApurado().toBigInteger());
					} else {
						logProcessamento.append(historicoConsumo.getConsumoApurado());
					}
				}

				leituraAnteriorPonto = historicoMedicao.getNumeroLeituraInformada();
				historicoMedicaoAnterior = historicoMedicao;
				
				if (historicoConsumo.getAnormalidadeConsumo() != null) {
					logProcessamento.append("\r ");
					logProcessamento.append("	Anormalidade de consumo: ");
					logProcessamento.append(historicoConsumo.getAnormalidadeConsumo().getDescricao());
				}

				atualizarMedicaoSeHouverAnormalidade(historicoMedicao, historicoConsumo);

				if(historicoConsumo.getAnormalidadeConsumo() != null && historicoConsumo.getAnormalidadeConsumo().getBloquearFaturamento() && anormalidadeBloqueiaFaturamento == null) {
					controladorAnormalidade = (ControladorAnormalidade) ServiceLocator.getInstancia()
							.getControladorNegocio(ControladorAnormalidade.BEAN_ID_CONTROLADOR_ANORMALIDADE);
					anormalidadeBloqueiaFaturamento = controladorAnormalidade
							.obterAnormalidadeConsumo(historicoConsumo.getAnormalidadeConsumo().getChavePrimaria());
				}
				historicoMedicaoInserido.add(historicoConsumo.getHistoricoAtual());
			}
			boolean possuiAnormalidadeMI = false;
			if (!mapaComposicaoVirtual.isEmpty()) {
				for (Map.Entry<String, List<HistoricoConsumo>> entry : mapaComposicaoVirtual.entrySet()) {
					Collection<HistoricoConsumo> listaHistoricoConsumo = entry.getValue();
					possuiAnormalidadeMI = validaComposicaoVirtual(pontoConsumo, listaHistoricoConsumo);
				}
			}
			for (HistoricoConsumo historicoConsumoAux : listaInsercaoHitoricoConsumo) {

				this.inserirEmBatch(historicoConsumoAux, Boolean.FALSE);
			}
			

			if (!mapaComposicaoVirtual.isEmpty()) {
				List<HistoricoConsumo> listaOrdenada = new ArrayList<HistoricoConsumo>();
				for (Map.Entry<String, List<HistoricoConsumo>> entry : mapaComposicaoVirtual.entrySet()) {
					listaOrdenada.add(insereConsumoComposicaoVirtual(entry, mapAtributosConsumo, possuiAnormalidadeMI));
				}
				Collections.sort(listaOrdenada, new Comparator<HistoricoConsumo>() {
					@Override
					public int compare(HistoricoConsumo obj1, HistoricoConsumo obj2) {
						int resultado = CONSTANTE_NUMERO_ZERO;
						HistoricoMedicao historicoMedicao1 = obj1.getHistoricoAtual();
						HistoricoMedicao historicoMedicao2 = obj2.getHistoricoAtual();

						if (historicoMedicao1 != null && historicoMedicao2 != null) {
							return historicoMedicao1.getDataLeituraInformada().compareTo(historicoMedicao2.getDataLeituraInformada());
						} else if (historicoMedicao1 != null) {
							resultado = CONSTANTE_NUMERO_UM;
						} else if (historicoMedicao2 != null) {
							resultado = -CONSTANTE_NUMERO_UM;
						}
						return resultado;
					}
				});
				if (!listaOrdenada.isEmpty()) {
					logProcessamento.append("\r\nQuantidade de historicos de leitura do ponto de consumo: ").append(listaOrdenada.size());
				} else {
					logProcessamento.append("\r\nQuantidade de historicos de leitura do ponto de consumo: " + CONSTANTE_NUMERO_ZERO);
				}

				for (HistoricoConsumo historicoConsumoAux : listaOrdenada) {
					logProcessamento.append("\r\n ");
					logProcessamento.append("	Historico de consumo:");
					logProcessamento.append(" ( ");

					String historicoAtual;

					if (historicoConsumoAux.getHistoricoAtual() != null) {
						historicoAtual = df.format(historicoConsumoAux.getHistoricoAtual().getDataLeituraInformada());
					} else {
						historicoAtual = "" + ") ";
					}

					logProcessamento.append(historicoAtual);

					logProcessamento.append(" ) - ");
					
					if(historicoConsumoAux.getConsumoApurado() != null) {
						logProcessamento.append(historicoConsumoAux.getConsumoApurado().toBigInteger());
					} else {
						logProcessamento.append(historicoConsumoAux.getConsumoApurado());
					}
					
				}
				historicosConsumoDiarios.addAll(listaOrdenada);
			}
			Collection<PontoConsumoTributoAliquota> listaPontoConsumoTributoAliquotaDrawback;
			if (mapaPontoConsumoTributoAliquota != null && mapaPontoConsumoTributoAliquota.containsKey(pontoConsumo)) {
				listaPontoConsumoTributoAliquotaDrawback = mapaPontoConsumoTributoAliquota.get(pontoConsumo);
			} else {
				listaPontoConsumoTributoAliquotaDrawback =
						controladorTributo.listarTributoAliquotaPorPontoConsumo(pontoConsumo, Boolean.TRUE, Boolean.TRUE);
			}

			BigDecimal valorDrawback = null;
			if (!Util.isNullOrEmpty(listaPontoConsumoTributoAliquotaDrawback)) {
				valorDrawback = listaPontoConsumoTributoAliquotaDrawback.iterator().next().getValorDrawback();

				BigDecimal valorTotalDrawback;
				if (mapaTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente != null
						&& mapaTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente.containsKey(pontoConsumo)) {
					valorTotalDrawback = mapaTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente.get(pontoConsumo);
				} else {
					valorTotalDrawback =
							this.consultarTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente(pontoConsumo.getChavePrimaria());
				}

				if (valorTotalDrawback != null) {
					BigDecimal diff = valorDrawback.subtract(valorTotalDrawback);

					if (diff.compareTo(BigDecimal.ZERO) > CONSTANTE_NUMERO_ZERO) {
						valorDrawback = diff;
					} else {
						valorDrawback = BigDecimal.ZERO;
					}
				}
			}

			// Adicionar lista de historico
			// consumo totalizado para
			// referencia/ciclo - ponto
			if (historicosConsumoDiarios != null && (!historicosConsumoDiarios.isEmpty())) {

				BigDecimal totalConsumoMedido = BigDecimal.ZERO;
				BigDecimal totalConsumoApurado = BigDecimal.ZERO;
				BigDecimal totalConsumo = BigDecimal.ZERO;

				BigDecimal totalConsumoMedidoDrawback = BigDecimal.ZERO;
				BigDecimal totalConsumoApuradoDrawback = BigDecimal.ZERO;
				BigDecimal totalConsumoDrawback = BigDecimal.ZERO;

				TipoConsumo tipoConsumo = this.obterTipoConsumo(
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL)));
				boolean consumoReal = true;

				Boolean ehConsumoDrawback = Boolean.FALSE;
				for (HistoricoConsumo historicoConsumoCiclo : historicosConsumoDiarios) {

					if (historicoConsumoCiclo.getConsumoApurado() != null) {

						if (valorDrawback != null && totalConsumoApuradoDrawback.compareTo(valorDrawback) < CONSTANTE_NUMERO_ZERO) {

							totalConsumoApuradoDrawback = totalConsumoApuradoDrawback
									.add(historicoConsumoCiclo.getConsumoApurado().setScale(0, RoundingMode.HALF_EVEN));
							BigDecimal diff = totalConsumoApuradoDrawback.subtract(valorDrawback);
							if (diff.compareTo(BigDecimal.ZERO) > CONSTANTE_NUMERO_ZERO) {
								totalConsumoApurado = totalConsumoApurado.add(diff);
								totalConsumoApuradoDrawback = valorDrawback;
								ehConsumoDrawback = Boolean.FALSE;
							} else {
								ehConsumoDrawback = Boolean.TRUE;
							}

						} else {

							totalConsumoApurado = totalConsumoApurado.add(historicoConsumoCiclo.getConsumoApurado())
									.setScale(0, RoundingMode.HALF_EVEN);
							ehConsumoDrawback = Boolean.FALSE;
						}

					}

					if (historicoConsumoCiclo.getConsumoMedido() != null) {

						if (valorDrawback != null && ehConsumoDrawback) {
							totalConsumoMedidoDrawback = totalConsumoMedidoDrawback.add(historicoConsumoCiclo.getConsumoMedido());
						} else {
							totalConsumoMedido = totalConsumoMedido.add(historicoConsumoCiclo.getConsumoMedido());
						}

					}

					if (historicoConsumoCiclo.getConsumo() != null) {

						if (valorDrawback != null && ehConsumoDrawback) {
							totalConsumoDrawback = totalConsumoDrawback.add(historicoConsumoCiclo.getConsumo());
						} else {
							totalConsumo = totalConsumo.add(historicoConsumoCiclo.getConsumo());
						}

					}

					if (historicoConsumoCiclo.getTipoConsumo().getChavePrimaria() != tipoConsumo.getChavePrimaria()) {

						consumoReal = false;

					}

				}

				inserirHistoricoConsumoCiclo(mapAtributosConsumo, historicoUltimaLeitura, totalConsumoMedido, totalConsumoApurado,
						totalConsumo, isFaturaAvulso, pontoConsumo, contratoPontoConsumo, referenciaCicloAtual, consumoApuradoMedioPonto,
						consumoReal, tipoConsumo, primeiraMedicao, consumoMedioApuradoDiarioPonto, historicosConsumoDiarios,
						historicosMedicaoCicloAtual, logProcessamento, Boolean.FALSE, anormalidadeBloqueiaFaturamento);				
				
				if (totalConsumoApuradoDrawback.compareTo(BigDecimal.ZERO) > CONSTANTE_NUMERO_ZERO) {
					inserirHistoricoConsumoCiclo(mapAtributosConsumo, historicoUltimaLeitura, totalConsumoMedidoDrawback,
							totalConsumoApuradoDrawback, totalConsumoDrawback, isFaturaAvulso, pontoConsumo, contratoPontoConsumo,
							referenciaCicloAtual, consumoApuradoMedioPonto, consumoReal, tipoConsumo, primeiraMedicao,
							consumoMedioApuradoDiarioPonto, historicosConsumoDiarios, historicosMedicaoCicloAtual, logProcessamento,
							Boolean.TRUE, anormalidadeBloqueiaFaturamento);
				}

			}

		}

	}
	
	private boolean possuiHistoricoConsumo(HistoricoConsumo historico) {
		
		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" where ");
		hql.append(" historico.pontoConsumo.chavePrimaria = :ponto ");
		hql.append(" AND historico.anoMesFaturamento = :referencia ");
		hql.append(" AND historico.numeroCiclo = :ciclo ");
		hql.append(" AND historico.habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("ponto", historico.getPontoConsumo().getChavePrimaria());
		query.setInteger("referencia", historico.getAnoMesFaturamento());
		query.setInteger("ciclo", historico.getNumeroCiclo());

		@SuppressWarnings("unchecked")
		List<HistoricoConsumo> lista = query.list();
		
		return lista != null && !lista.isEmpty();
	}

	private void atualizarMedicaoSeHouverAnormalidade(HistoricoMedicao historicoMedicao,
			HistoricoConsumo historicoConsumo) throws ConcorrenciaException, NegocioException {
		
		if (historicoConsumo.getAnormalidadeConsumo() != null && historicoMedicao.isAnalisada()) {
			HistoricoConsumo historicoAnterior = this.obterUltimoHistoricoConsumoInativo(historicoConsumo);
			
			if (historicoAnterior == null || (historicoAnterior != null && historicoAnterior.getAnormalidadeConsumo() != null
					&& historicoAnterior.getAnormalidadeConsumo().getChavePrimaria() != historicoConsumo
							.getAnormalidadeConsumo().getChavePrimaria())) {
				historicoMedicao.setAnalisada(false);
				this.atualizar(historicoMedicao);
			}
		}
	}
	
	/**
	 * Consulta o Historico Consumo inativo da referencia
	 * @param historicoConsumo - {@link HistoricoConsumo}
	 * @return HistoricoConsumo
	 */
	@SuppressWarnings("unchecked")
	public HistoricoConsumo obterUltimoHistoricoConsumoInativo(HistoricoConsumo historicoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" WHERE ");
		hql.append(" historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" AND historico.anoMesFaturamento = :referencia");
		hql.append(" AND historico.numeroCiclo = :ciclo ");
		hql.append(" AND historico.habilitado = false ");
		hql.append(" order by historico.ultimaAlteracao desc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", historicoConsumo.getPontoConsumo().getChavePrimaria());
		query.setInteger("referencia", historicoConsumo.getAnoMesFaturamento());
		query.setInteger("ciclo", historicoConsumo.getNumeroCiclo());

		return (HistoricoConsumo) Util.primeiroElemento(query.list());
	}

	private HistoricoMedicao obtemMaiorLeituraReferenciaAnterior(PontoConsumo pontoConsumo, Map<String, Integer> referenciaCicloAtual,
			ControladorHistoricoMedicao controladorHistoricoMedicao, HistoricoMedicao historicoMedicaoCicloAnterior,
			Map<String, Integer> referenciaCicloAnterior) throws NegocioException {
		HistoricoMedicao historicoMedicaoCicloAnteriorAux = historicoMedicaoCicloAnterior;
		if (historicoMedicaoCicloAnteriorAux == null) {
			historicoMedicaoCicloAnteriorAux = controladorHistoricoMedicao.obterUltimoHistoricoMedicao(pontoConsumo.getChavePrimaria(),
					referenciaCicloAnterior.get(REFERENCIA), CONSTANTE_NUMERO_ZERO);
			// descartar o historicoMedicaoAtual
			if (historicoMedicaoCicloAnteriorAux != null
					&& historicoMedicaoCicloAnteriorAux.getAnoMesLeitura().compareTo(referenciaCicloAtual.get(REFERENCIA)) == CONSTANTE_NUMERO_ZERO
					&& historicoMedicaoCicloAnteriorAux.getNumeroCiclo().compareTo(referenciaCicloAtual.get(CICLO)) == CONSTANTE_NUMERO_ZERO) {
				historicoMedicaoCicloAnteriorAux = null;
			}
		}
		return historicoMedicaoCicloAnteriorAux;
	}

	private List<HistoricoConsumo> agrupaConsumosParaComposicao(HistoricoConsumo historicoConsumo,
			List<HistoricoConsumo> listaHistoricoConsumo) {

		if (listaHistoricoConsumo == null) {
			listaHistoricoConsumo = new ArrayList<HistoricoConsumo>();
		}
		if (historicoConsumo.getIndicadorMedicaoIndependenteAtual() != null && historicoConsumo.getIndicadorMedicaoIndependenteAtual()) {
			listaHistoricoConsumo.add(historicoConsumo);
		}
		return listaHistoricoConsumo;
	}

	private void atualizaHistoricoMIAnterior(PontoConsumo pontoConsumo, HistoricoMedicao historicoMedicao)
			throws NegocioException, ConcorrenciaException {

		HistoricoConsumo historicoConsumoAtualMI =
				obterHistoricoConsumoPorDataMI(pontoConsumo.getChavePrimaria(), historicoMedicao.getDataLeituraInformada(),
						historicoMedicao.getHistoricoInstalacaoMedidor().getMedidor().getChavePrimaria());
		if (historicoConsumoAtualMI != null) {
			historicoConsumoAtualMI.setIndicadorMedicaoIndependenteAtual(false);
			atualizar(historicoConsumoAtualMI);
		}
	}

	private HistoricoConsumo insereConsumoComposicaoVirtual(Map.Entry<String, List<HistoricoConsumo>> entry,
			Map<String, Object> mapAtributosConsumo, boolean possuiAnormalidadeMI) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		TipoConsumo tipoConsumo = this.obterTipoConsumo(
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL)));

		HistoricoConsumo historicoConsumoComposicao = adicionarHistoricoConsumo(mapAtributosConsumo);

		if (possuiAnormalidadeMI) {
			ControladorAnormalidade controladorAnormalidade = (ControladorAnormalidade) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorAnormalidade.BEAN_ID_CONTROLADOR_ANORMALIDADE);
			String codigoAnormalidade = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_MEDIDOR_COMPOSICAO_SEM_MEDICAO);

			final AnormalidadeConsumo anormalidadeConsumo =
					controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidade));
			final TipoConsumo finalTipoConsumo = tipoConsumo;
			aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumoComposicao, () -> {
				historicoConsumoComposicao.setTipoConsumo(finalTipoConsumo);
				this.inserirEmBatch(historicoConsumoComposicao, Boolean.TRUE);
			});

		} else {

			boolean consumoReal = true;

			// lista com historicosConsumo pertencentes ao mesmo medidor virtual
			List<HistoricoConsumo> listaHistoricoConsumo = entry.getValue();

			PontoConsumo pontoConsumo = Util.primeiroElemento(listaHistoricoConsumo).getPontoConsumo();
			Medidor medidorVirtual = pontoConsumo.getInstalacaoMedidor().getMedidor();
			String composicaoVirtual = medidorVirtual.getComposicaoVirtual();
			String composicaoVirtualConsumo = composicaoVirtual;
			String composicaoVirtualConsumoMedio = composicaoVirtual;
			String composicaoVirtualConsumoMedido = composicaoVirtual;
			String composicaoVirtualConsumoApurado = composicaoVirtual;
			String composicaoVirtualConsumoApuradoMedio = composicaoVirtual;

			for (int i = CONSTANTE_NUMERO_ZERO; i < listaHistoricoConsumo.size(); i++) {
				HistoricoConsumo historicoConsumoAux = listaHistoricoConsumo.get(i);
				composicaoVirtualConsumo =
						montaComposicaoComConsumo(composicaoVirtualConsumo, historicoConsumoAux, historicoConsumoAux.getConsumo());
				composicaoVirtualConsumoMedio = montaComposicaoComConsumo(composicaoVirtualConsumoMedio, historicoConsumoAux,
						historicoConsumoAux.getConsumoMedio());
				composicaoVirtualConsumoMedido = montaComposicaoComConsumo(composicaoVirtualConsumoMedido, historicoConsumoAux,
						historicoConsumoAux.getConsumoMedio());
				composicaoVirtualConsumoApurado = montaComposicaoComConsumo(composicaoVirtualConsumoApurado, historicoConsumoAux,
						historicoConsumoAux.getConsumoApurado());
				composicaoVirtualConsumoApuradoMedio = montaComposicaoComConsumo(composicaoVirtualConsumoApuradoMedio, historicoConsumoAux,
						historicoConsumoAux.getConsumoApuradoMedio());

				if (historicoConsumoAux.getTipoConsumo().getChavePrimaria() != tipoConsumo.getChavePrimaria()) {
					consumoReal = false;
				}
			}

			BigDecimal consumoTotal = realizaCalculoComposicao(composicaoVirtualConsumo);
			BigDecimal consumoMedioTotal = realizaCalculoComposicao(composicaoVirtualConsumoMedio);
			BigDecimal consumoMedidoTotal = realizaCalculoComposicao(composicaoVirtualConsumoMedido);
			BigDecimal consumoApuradoTotal = realizaCalculoComposicao(composicaoVirtualConsumoApurado);
			BigDecimal consumoApuradoMedidoTotal = realizaCalculoComposicao(composicaoVirtualConsumoApuradoMedio);

			HistoricoConsumo historicoConsumo = Util.primeiroElemento(listaHistoricoConsumo);

			historicoConsumoComposicao.setConsumo(consumoTotal);
			historicoConsumoComposicao.setConsumoMedio(consumoMedioTotal);
			historicoConsumoComposicao.setConsumoMedido(consumoMedidoTotal);
			historicoConsumoComposicao.setConsumoApurado(consumoApuradoTotal);
			historicoConsumoComposicao.setConsumoApuradoMedio(consumoApuradoMedidoTotal);

			historicoConsumoComposicao.setHistoricoAtual(historicoConsumo.getHistoricoAtual());

			historicoConsumoComposicao.setDiasConsumo(historicoConsumo.getDiasConsumo());

			historicoConsumoComposicao.setIndicadorConsumoCiclo(Boolean.FALSE);

			historicoConsumoComposicao.setComposicaoVirtual(composicaoVirtual);
			if (consumoReal) {
				historicoConsumoComposicao.setTipoConsumo(tipoConsumo);
			} else {
				tipoConsumo = this.obterTipoConsumo(
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_ESTIMADO)));
				historicoConsumoComposicao.setTipoConsumo(tipoConsumo);
			}

			historicoConsumoComposicao.setHabilitado(Boolean.TRUE);

			this.inserirEmBatch(historicoConsumoComposicao, Boolean.TRUE);

		}
		return historicoConsumoComposicao;

	}

	private BigDecimal realizaCalculoComposicao(String composicaoVirtualConsumo) {

		BigDecimal consumoNegativo = BigDecimal.ZERO;
		BigDecimal consumoPositivo = BigDecimal.ZERO;
		if (composicaoVirtualConsumo != null && !"".equals(composicaoVirtualConsumo)) {
			if (composicaoVirtualConsumo.contains("#")) {
				String[] listaConsumoComSinal = composicaoVirtualConsumo.split("#");
				for (String valorComSinal : listaConsumoComSinal) {
					if (valorComSinal.contains("-")) {
						consumoNegativo = consumoNegativo.add(new BigDecimal(valorComSinal.substring(CONSTANTE_NUMERO_UM)));
					} else {
						consumoPositivo = consumoPositivo.add(new BigDecimal(valorComSinal.substring(CONSTANTE_NUMERO_UM)));
					}
				}
				consumoNegativo = consumoNegativo.negate();
				return consumoPositivo.add(consumoNegativo);
			} else {
				return new BigDecimal(composicaoVirtualConsumo.substring(CONSTANTE_NUMERO_UM));
			}
		}
		return BigDecimal.ZERO;
	}

	private String montaComposicaoComConsumo(String composicaoVirtual, HistoricoConsumo historicoConsumoAux, BigDecimal consumo) {

		if (consumo != null) {

			String chaveMedidor = String.valueOf(historicoConsumoAux.getMedidor().getChavePrimaria());

			if (composicaoVirtual.contains(chaveMedidor)) {
				composicaoVirtual = composicaoVirtual.replace(chaveMedidor, String.valueOf(consumo));
			}
			return composicaoVirtual;
		}
		return "";
	}

	private boolean validaComposicaoVirtual(PontoConsumo pontoConsumo, Collection<HistoricoConsumo> listaHistoricoConsumo) {

		String composicaoVirtual = pontoConsumo.getInstalacaoMedidor().getMedidor().getComposicaoVirtual();

		// valida a quantidade de medidores com a quantidade de medições
		// Conta
		int contador = CONSTANTE_NUMERO_ZERO;
		for (HistoricoConsumo historicoConsumoAux : listaHistoricoConsumo) {
			String chaveMedidor = String.valueOf(historicoConsumoAux.getMedidor().getChavePrimaria());
			if (composicaoVirtual.contains(chaveMedidor)) {
				contador++;
			}
		}

		// Exemplo de como a composição é formada: -8651#+5466#-2333
		int quantidadeMedidores;
		if (composicaoVirtual.contains("#")) {
			quantidadeMedidores = composicaoVirtual.split("#").length;
		} else {
			quantidadeMedidores = CONSTANTE_NUMERO_UM;
		}

		return contador != quantidadeMedidores;
	}

	private boolean isMedidorIndependente(HistoricoMedicao historicoMedicao) throws GGASException {

		InstalacaoMedidor historicoInstalacaoMedidor = historicoMedicao.getHistoricoInstalacaoMedidor();

		return (historicoInstalacaoMedidor != null && historicoInstalacaoMedidor.getMedidor() != null
				&& historicoInstalacaoMedidor.getMedidor().getModoUso().getChavePrimaria() == Long
						.parseLong(Fachada.getInstancia().obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#inserirHistoricoConsumoCiclo(java.util.Map,
	 * br.com.ggas.medicao.leitura.HistoricoMedicao, java.math.BigDecimal, java.math.BigDecimal, java.math.BigDecimal, java.math.BigDecimal,
	 * java.lang.Boolean, br.com.ggas.cadastro.imovel.PontoConsumo, br.com.ggas.contrato.contrato.ContratoPontoConsumo, java.util.Map,
	 * java.math.BigDecimal, java.lang.Boolean, br.com.ggas.medicao.consumo.TipoConsumo, java.lang.Boolean, java.math.BigDecimal,
	 * java.util.HashSet, java.util.Collection, java.lang.StringBuilder, java.lang.Boolean)
	 */
	@Override
	public void inserirHistoricoConsumoCiclo(Map<String, Object> mapAtributosConsumo, HistoricoMedicao historicoUltimaLeitura,
			BigDecimal totalConsumoMedido, BigDecimal totalConsumoApurado, BigDecimal totalConsumo, Boolean isFaturaAvulso,
			PontoConsumo pontoConsumo, ContratoPontoConsumo contratoPontoConsumo, Map<String, Integer> referenciaCicloAtual,
			BigDecimal consumoApuradoMedioPonto, Boolean consumoReal, TipoConsumo tipoConsumo, Boolean primeiraMedicao,
			BigDecimal consumoMedioApuradoDiarioPonto, Set<HistoricoConsumo> historicosConsumoDiarios,
			Collection<HistoricoMedicao> historicosMedicaoCicloAtual, StringBuilder logProcessamento, Boolean isDrawback,
			AnormalidadeConsumo anormalidadeBloqueiaFaturamento) throws GGASException {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();

		HistoricoConsumo novoHistoricoConsumoCiclo = this.adicionarHistoricoConsumo(mapAtributosConsumo);
		novoHistoricoConsumoCiclo.setHistoricoAtual(historicoUltimaLeitura);
		novoHistoricoConsumoCiclo.setIndicadorConsumoCiclo(Boolean.TRUE);
		novoHistoricoConsumoCiclo.setConsumoMedido(totalConsumoMedido);
		novoHistoricoConsumoCiclo.setConsumoApurado(totalConsumoApurado);
		novoHistoricoConsumoCiclo.setConsumo(totalConsumo);
		novoHistoricoConsumoCiclo.setHabilitado(Boolean.TRUE);

		int numeroDiasConsumo = CONSTANTE_NUMERO_ZERO;
		if (isFaturaAvulso != null && !isFaturaAvulso) {
			numeroDiasConsumo = this.calcularDiasConsumo(novoHistoricoConsumoCiclo.getHistoricoAtual(),
					novoHistoricoConsumoCiclo.getHistoricoAnterior(), pontoConsumo.getInstalacaoMedidor(), novoHistoricoConsumoCiclo.isIndicadorConsumoCiclo());
		} else {
			numeroDiasConsumo = CONSTANTE_NUMERO_UM;
		}

		novoHistoricoConsumoCiclo.setDiasConsumo(numeroDiasConsumo);

		Map<String, BigDecimal> consumoMedioPontoApuradoMap;
		if (contratoPontoConsumo != null) {

			consumoMedioPontoApuradoMap = this.calcularConsumoMedio(pontoConsumo, contratoPontoConsumo,
					referenciaCicloAtual.get(REFERENCIA), referenciaCicloAtual.get(CICLO), numeroDiasConsumo);
			if (consumoMedioPontoApuradoMap != null && consumoMedioPontoApuradoMap.get("consumoMedioApurado") != null) {
				consumoApuradoMedioPonto = consumoMedioPontoApuradoMap.get("consumoMedioApurado");
			}

		}

		novoHistoricoConsumoCiclo.setConsumoApuradoMedio(consumoApuradoMedioPonto);

		if (consumoReal) {

			novoHistoricoConsumoCiclo.setTipoConsumo(tipoConsumo);

		} else {

			tipoConsumo = this.obterTipoConsumo(
					Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_ESTIMADO)));
			novoHistoricoConsumoCiclo.setTipoConsumo(tipoConsumo);

		}

		try {
			// Validar estouro de consumo, alto e baixo e fora de faixa
			if (contratoPontoConsumo != null && !verificarMudancaTitularidadeContrato(novoHistoricoConsumoCiclo) && !primeiraMedicao) {

				this.verificarFaixaLeituraInformada(novoHistoricoConsumoCiclo, consumoMedioApuradoDiarioPonto);
			}

			// Validar Anormailidade composicao sem medicao.
			this.verificarAnormalidadesComposicaoSemMedicao(novoHistoricoConsumoCiclo, historicosConsumoDiarios);

			// Validar Anormalidades
			this.verificarAnormalidadesCicloImcompleto(novoHistoricoConsumoCiclo, historicosConsumoDiarios);

			// Validar Anormalidade de leitura não informada
			this.verificarAnormalidadeLeituraNaoInformada(novoHistoricoConsumoCiclo, historicosConsumoDiarios);

			if (contratoPontoConsumo == null) {

				// Verificar anormalidades de ponto consumo sem contrato
				this.verificarAnormalidadesPontoConsunoSemContrato(novoHistoricoConsumoCiclo, historicosConsumoDiarios);

			}
		} catch (GGASException ex) {
			throw new NegocioException("Falha ao tentar verificar anormalidades de consumo", ex);
		}

		novoHistoricoConsumoCiclo.setIndicadorDrawback(isDrawback);

		 if (novoHistoricoConsumoCiclo.getAnormalidadeConsumo() != null && !novoHistoricoConsumoCiclo.getAnormalidadeConsumo().getBloquearFaturamento() && anormalidadeBloqueiaFaturamento != null) {
             aplicarAnormalidadeSeAtiva(anormalidadeBloqueiaFaturamento, novoHistoricoConsumoCiclo);
           }
		
		// É inserido um novo volume consistido
		this.inserirEmBatch(novoHistoricoConsumoCiclo, Boolean.TRUE);

		for (HistoricoConsumo historicoConsumo : historicosConsumoDiarios) {
			historicoConsumo.setHistoricoConsumoSintetizador(novoHistoricoConsumoCiclo);
			this.atualizarEmBatch(historicoConsumo, getClasseEntidade());
		}

		getHibernateTemplate().getSessionFactory().getCurrentSession().flush();

		HistoricoMedicao historicoMedicao = historicosMedicaoCicloAtual.iterator().next();

		logProcessamento.append("\r\n ");
		logProcessamento.append("	Historico de consumo do ciclo:");
		logProcessamento.append(" ( ").append(df.format(historicoMedicao.getDataLeituraInformada())).append(" - ");

		String historioAtual = "";

		if (novoHistoricoConsumoCiclo.getHistoricoAtual() != null) {
			historioAtual = df.format(novoHistoricoConsumoCiclo.getHistoricoAtual().getDataLeituraInformada());
		} else {
			historioAtual = "";
		}

		logProcessamento.append(historioAtual).append(") ").append(" ) - ").append(novoHistoricoConsumoCiclo.getConsumoApurado());
	} 

	

	

	/**
	 * Obtém a leitura anterior do ponto consumo a partir dos históricos de consumo.
	 *
	 * @param listaHistoricoMedicao
	 * @param historicoMedicao
	 * @param pontoConsumo
	 * @return leitura anterior
	 * @throws NegocioException
	 */
	private BigDecimal obterLeituraAnteriorPontoConsumo(Collection<HistoricoMedicao> listaHistoricoMedicao,
			HistoricoMedicao historicoMedicao, PontoConsumo pontoConsumo) throws NegocioException {

		ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

		BigDecimal leituraAnteriorPonto = BigDecimal.ZERO;
		if (historicoMedicao.getNumeroLeituraAnterior() != null) {

			leituraAnteriorPonto = historicoMedicao.getNumeroLeituraAnterior();

		} else {
			Collection<HistoricoMedicao> listaHistoricoMedicaoDePontoConsumo = null;
			if (listaHistoricoMedicao == null) {
				listaHistoricoMedicaoDePontoConsumo =
						controladorHistoricoMedicao.consultarHistoricoMedicao(pontoConsumo.getChavePrimaria());
			} else {
				listaHistoricoMedicaoDePontoConsumo = listaHistoricoMedicao;
			}

			for (HistoricoMedicao historicoMedicaoAnterior : listaHistoricoMedicaoDePontoConsumo) {
				if (historicoMedicaoAnterior.getChavePrimaria() != historicoMedicao.getChavePrimaria()
						&& historicoMedicaoAnterior.getNumeroLeituraInformada() != null) {
					leituraAnteriorPonto = historicoMedicaoAnterior.getNumeroLeituraInformada();
					break;
				}

			}

			if (leituraAnteriorPonto == null) {
				leituraAnteriorPonto = controladorHistoricoMedicao.setarLeituraAnterior(null, pontoConsumo,
						pontoConsumo.getInstalacaoMedidor().getMedidor());
			}

		}

		return leituraAnteriorPonto;

	}

	/**
	 * Verificar anormalidades ciclo imcompleto.
	 *
	 * @param novoHistoricoConsumoCiclo the novo historico consumo ciclo
	 * @param listaHistoricoConsumo the lista historico consumo
	 * @throws NegocioException the negocio exception
	 */
	/*
	 * Método para verificar se existe anormalidade de ciclo incompleto no consumo diário. Se houver, atualiza o tipo e anormalidade no
	 * consumo do ciclo.
	 */
	private void verificarAnormalidadesCicloImcompleto(HistoricoConsumo novoHistoricoConsumoCiclo,
			Collection<HistoricoConsumo> listaHistoricoConsumo) throws GGASException {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoAnormalidadeConsumo =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_CICLO_INCOMPLETO);

		AnormalidadeConsumo anormalidadeConsumo =
				controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumo));

		for (HistoricoConsumo historicoConsumo : listaHistoricoConsumo) {
			if ((historicoConsumo.getAnormalidadeConsumo() != null)
					&& (historicoConsumo.getAnormalidadeConsumo().getChavePrimaria() == Long.parseLong(codigoAnormalidadeConsumo))) {
				aplicarAnormalidadeSeAtiva(anormalidadeConsumo, novoHistoricoConsumoCiclo, () -> {
					novoHistoricoConsumoCiclo.setTipoConsumo(historicoConsumo.getTipoConsumo());
					novoHistoricoConsumoCiclo.setConsumoApurado(null);
				});

				break;
			}
		}
	}

	private void verificarAnormalidadesComposicaoSemMedicao(HistoricoConsumo novoHistoricoConsumoCiclo,
			Collection<HistoricoConsumo> listaHistoricoConsumo) throws GGASException {

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoAnormalidadeConsumo =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_MEDIDOR_COMPOSICAO_SEM_MEDICAO);

		AnormalidadeConsumo anormalidadeConsumo =
				controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumo));

		for (HistoricoConsumo historicoConsumo : listaHistoricoConsumo) {
			if ((historicoConsumo.getAnormalidadeConsumo() != null)
					&& (historicoConsumo.getAnormalidadeConsumo().getChavePrimaria() == Long.parseLong(codigoAnormalidadeConsumo))) {
				aplicarAnormalidadeSeAtiva(anormalidadeConsumo, novoHistoricoConsumoCiclo, () -> {
					novoHistoricoConsumoCiclo.setTipoConsumo(historicoConsumo.getTipoConsumo());
				});
				break;
			}
		}
	}

	/**
	 * Verificar anormalidade leitura nao informada.
	 *
	 * @param novoHistoricoConsumoCiclo the novo historico consumo ciclo
	 * @param listaHistoricoConsumo the lista historico consumo
	 * @throws NegocioException the negocio exception
	 */
	/*
	 * Método para verificar se existe anormalidade de leitura não informada no consumo diário. Se houver, atualiza o tipo e anormalidade no
	 * consumo do ciclo.
	 */
	private void verificarAnormalidadeLeituraNaoInformada(HistoricoConsumo novoHistoricoConsumoCiclo,
			Collection<HistoricoConsumo> listaHistoricoConsumo) throws GGASException {

		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoAnormalidadeConsumoLeituraNaoInformada =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_LEITURA_NAO_INFORMADA);

		AnormalidadeConsumo anormalidadeConsumo = controladorAnormalidade.

				obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumoLeituraNaoInformada));

		String codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_NAO_APURADO);

		TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));

		for (HistoricoConsumo historicoConsumo : listaHistoricoConsumo) {
			if ((historicoConsumo.getAnormalidadeConsumo() != null) && (historicoConsumo.getAnormalidadeConsumo().getChavePrimaria() == Long
					.parseLong(codigoAnormalidadeConsumoLeituraNaoInformada))) {
				aplicarAnormalidadeSeAtiva(anormalidadeConsumo, novoHistoricoConsumoCiclo, () -> {
					novoHistoricoConsumoCiclo.setTipoConsumo(tipoConsumo);
					novoHistoricoConsumoCiclo.setConsumoApurado(null);
					novoHistoricoConsumoCiclo.setConsumo(null);
				});
				break;
			}
		}
	}

	/**
	 * Verificar anormalidades ponto consuno sem contrato.
	 *
	 * @param novoHistoricoConsumoCiclo the novo historico consumo ciclo
	 * @param listaHistoricoConsumo the lista historico consumo
	 * @throws NegocioException the negocio exception
	 */
	/*
	 * Método para verificar se existe anormalidade de ponto de consumo sem contrato no consumo diário. Se houver, atualiza o tipo e
	 * anormalidade no consumo do ciclo.
	 */
	private void verificarAnormalidadesPontoConsunoSemContrato(HistoricoConsumo novoHistoricoConsumoCiclo,
			Collection<HistoricoConsumo> listaHistoricoConsumo) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();

		String codigoAnormalidadeConsumo = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_PONTO_CONSUMO_SEM_CONTRATO_ATIVO);

		//
		// by
		// jmauricio
		// on
		// 20/11/10
		// 12:53
		AnormalidadeConsumo anormalidadeConsumo =
				controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumo));

		for (HistoricoConsumo historicoConsumo : listaHistoricoConsumo) {
			if ((historicoConsumo.getAnormalidadeConsumo() != null)
					&& (historicoConsumo.getAnormalidadeConsumo().getChavePrimaria() == Long.parseLong(codigoAnormalidadeConsumo))) {
				aplicarAnormalidadeSeAtiva(anormalidadeConsumo, novoHistoricoConsumoCiclo, () -> {
					novoHistoricoConsumoCiclo.setTipoConsumo(historicoConsumo.getTipoConsumo());
				});

				break;
			}
		}
	}

	/**
	 * Método responsável por aplicar o consumo informado a um histórico de consumo.
	 *
	 * @param historicoConsumo the historico consumo
	 * @param consumoInformado the consumo informado
	 * @throws NegocioException the negocio exception
	 */
	private void aplicarConsumoInformado(HistoricoConsumo historicoConsumo, BigDecimal consumoInformado) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		// Atribui o consumo informado como o
		// consumo apurado
		historicoConsumo.setConsumoApurado(consumoInformado);
		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();

		// Atribui o tipo de consumo
		// correspondente a CONSUMO INFORMADO ao
		// histórico de consumo
		String codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_INFORMADO);
		TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
		historicoConsumo.setTipoConsumo(tipoConsumo);

		// Atribui a anormalidade correspondente a
		// CONSUMO INFORMADO ao
		// histórico de consumo
		String codigoAnormalidadeConsumoInformado =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_INFORMADO);

		final AnormalidadeConsumo anormalidadeConsumo =
				controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumoInformado));
		aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# consultarHistoricosConsumo (br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricosConsumo(PontoConsumo pontoConsumo, Integer maxResultados)
			throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" where ");
		hql.append(" historico.habilitado = true ");
		hql.append(" and historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");
		hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if (maxResultados != null) {
			query.setMaxResults(maxResultados);
		}

		query.setLong("idPontoConsumo", pontoConsumo.getChavePrimaria());

		return query.list();
	}

	/**
	 * Calcular dias consumo.
	 *
	 * @param historicoMedicao the historico medicao
	 * @param historicoMedicaoAnterior the historico medicao anterior
	 * @param instalacaoMedidor the instalacao medidor
	 * @return the int
	 * @throws NegocioException the negocio exception
	 */
	private int calcularDiasConsumo(HistoricoMedicao historicoMedicao, HistoricoMedicao historicoMedicaoAnterior,
			InstalacaoMedidor instalacaoMedidor, Boolean indicadorConsumoCiclo) throws NegocioException {

		ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

		int numeroDiasConsumo = CONSTANTE_NUMERO_UM;
		Date dataLeituraAnterior = null;

		if(historicoMedicao.getDataLeituraAnterior() != null && !indicadorConsumoCiclo) {
			dataLeituraAnterior = historicoMedicao.getDataLeituraAnterior();
		}else if (historicoMedicaoAnterior != null) {
			dataLeituraAnterior = historicoMedicaoAnterior.getDataLeituraFaturada();
		} else {
			if (historicoMedicao.getHistoricoInstalacaoMedidor() != null) {
				dataLeituraAnterior = controladorHistoricoMedicao.setarDataLeituraAnterior(null, historicoMedicao.getPontoConsumo(),
						historicoMedicao.getHistoricoInstalacaoMedidor().getMedidor());
			} else if (instalacaoMedidor != null) {
				dataLeituraAnterior = controladorHistoricoMedicao.setarDataLeituraAnterior(instalacaoMedidor, null, null);

			}
		}

		if (dataLeituraAnterior != null && historicoMedicao.getDataLeituraInformada() != null) {

			numeroDiasConsumo = Util.intervaloDatas(dataLeituraAnterior, historicoMedicao.getDataLeituraInformada());

			if (dataLeituraAnterior.compareTo(historicoMedicao.getDataLeituraInformada()) == CONSTANTE_NUMERO_ZERO) {

				numeroDiasConsumo = CONSTANTE_NUMERO_UM;

			} else {

				if (historicoMedicaoAnterior == null) {

					numeroDiasConsumo = numeroDiasConsumo + CONSTANTE_NUMERO_UM;

				}

			}

		}

		return numeroDiasConsumo;

	}

	/**
	 * Correcao fator PTZ e PCS. Caso tipo de Consumo definido seja a média, não se deve aplicar correção ConsumoMedido será atribuído neste
	 * método
	 *
	 * @param historicoConsumo the historico consumo
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param historicoMedicao the historico medicao
	 * @param dataLeituraAnterior the data leitura anterior
	 * @param dataLeituraAtual the data leitura atual
	 * @param dataUltimaLeitura the data ultima leitura
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	private void aplicarCorrecaoConsumo(Map<PontoConsumo, Map<CityGate, List<PontoConsumoCityGate>>> mapaPontoConsumoCityGate,
			Map<Rede, List<Tronco>> mapaTroncosPorRede, HistoricoConsumo historicoConsumo, ContratoPontoConsumo contratoPontoConsumo,
			HistoricoMedicao historicoMedicao, Date dataLeituraAnterior, Date dataLeituraAtual, Date dataUltimaLeitura,
			Map<String, Unidade> unidadesPadraoParaConversao,
			Map<Long, Collection<ContratoPontoConsumoPCSAmostragem>> mapaContratoPontoConsumoPCSAmostragem,
			Map<Long, Collection<ContratoPontoConsumoPCSIntervalo>> mapaContratoPontoConsumoPCSIntervalo,
			Map<Long, Collection<FaixaPressaoFornecimento>> mapaFaixaPressaoFornecimentoPorSegmento) throws GGASException {

		BigDecimal consumo = null;

		if (historicoConsumo.getConsumo() != null) {
			if (contratoPontoConsumo.getNumeroFatorCorrecao() != null
					&& (historicoMedicao.getHistoricoInstalacaoMedidor().getVazaoCorretor() == null
							|| (historicoMedicao.getHistoricoInstalacaoMedidor().getVazaoCorretor() != null
									&& historicoMedicao.getFatorPTZCorretor() == null))) {
				consumo = contratoPontoConsumo.getNumeroFatorCorrecao().multiply(historicoConsumo.getConsumo());
				historicoConsumo.setConsumoMedido(consumo);
				historicoConsumo.setFatorCorrecao(contratoPontoConsumo.getNumeroFatorCorrecao());
			} else {

				if (historicoMedicao.getHistoricoInstalacaoMedidor().getVazaoCorretor() != null) {

					consumo = historicoMedicao.getConsumoCorretor();
					historicoConsumo.setFatorPTZ(historicoMedicao.getFatorPTZCorretor());

					// Obter faixa Pressao fornecimento
					// para verificar
					// a correção do Z
					ControladorUnidade controladorUnidade = ServiceLocator.getInstancia().getControladorUnidade();
					Unidade unidadePadraoPressao = controladorUnidade.obterUnidadePadraoPressaoParaConversao(unidadesPadraoParaConversao);
					BigDecimal pressaoContratada = null;
					Unidade unidadeContratada = unidadePadraoPressao;
					pressaoContratada = obterPressaoContratadaConvertida(contratoPontoConsumo, unidadePadraoPressao);

					PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();
					Segmento segmento = null;
					if (pontoConsumo != null) {
						segmento = pontoConsumo.getSegmento();
					}

					FaixaPressaoFornecimento faixaPressaoFornecimento;
					Collection<FaixaPressaoFornecimento> faixaPressaoFornecimentoPorSegmento = null;
					if (segmento != null && mapaFaixaPressaoFornecimentoPorSegmento != null) {
						faixaPressaoFornecimentoPorSegmento = mapaFaixaPressaoFornecimentoPorSegmento.get(segmento.getChavePrimaria());
						faixaPressaoFornecimento = obterFaixaPressaoFornecimento(contratoPontoConsumo, pressaoContratada, unidadeContratada,
								faixaPressaoFornecimentoPorSegmento);
					} else {
						faixaPressaoFornecimento =
								obterFaixaPressaoFornecimento(contratoPontoConsumo, pressaoContratada, unidadeContratada, segmento);
					}

					BigDecimal fatorZ;
					fatorZ = obterFatorZ(faixaPressaoFornecimento, contratoPontoConsumo, historicoMedicao.getDataLeituraInformada(),
							mapaTroncosPorRede);
					// ------------------------------------------------------

					consumo = consumo.multiply(fatorZ);
					historicoConsumo.setMedidaZ(fatorZ);

				} else {

					// Aplica Fator PTZ
					if (historicoConsumo.getTipoConsumo().isIndicadorCorrecaoPTZ()) {
						DadosPTZImpl dadosPTZ = this.aplicarCorrecaoConsumoFatorPTZ(historicoConsumo, contratoPontoConsumo,
								historicoMedicao, dataLeituraAnterior, dataUltimaLeitura, unidadesPadraoParaConversao, mapaTroncosPorRede,
								mapaFaixaPressaoFornecimentoPorSegmento);

						consumo = dadosPTZ.getConsumoMedido();
						historicoConsumo.setFatorPTZ(dadosPTZ.getFatorPTZ());
						historicoConsumo.setMedidaZ(dadosPTZ.getMedidaZ());
					} else {
						consumo = historicoConsumo.getConsumo();
					}
				}

				historicoConsumo.setConsumoMedido(consumo);

				if (historicoConsumo.getConsumoMedido() != null) {
					// Aplica Fator PCS
					if (historicoConsumo.getTipoConsumo().isIndicadorCorrecaoPCS()) {
						if (dataLeituraAnterior != null && dataLeituraAtual != null) {

							Collection<CityGate> listaCityGates =
									obterCityGateDeTroncosDaRedeOuRedeDistribuicaoTronco(mapaTroncosPorRede, contratoPontoConsumo);
							DadosPCSImpl dadosPCS = this.aplicarCorrecaoConsumoFatorPCS(mapaPontoConsumoCityGate, historicoConsumo,
									contratoPontoConsumo, listaCityGates, dataLeituraAnterior, dataLeituraAtual,
									mapaContratoPontoConsumoPCSAmostragem, mapaContratoPontoConsumoPCSIntervalo);

							historicoConsumo.setMedidaPCS(dadosPCS.getMedidaPCS());
							historicoConsumo.setFatorPCS(dadosPCS.getFatorPCS());
							historicoConsumo.setIntervaloPCS(dadosPCS.getIntervaloPCS());
							historicoConsumo.setLocalAmostragemPCS(dadosPCS.getLocalAmostragemPCS());

							consumo = dadosPCS.getConsumoApurado();

						} else {
							consumo = null;
						}
					} else {
						consumo = historicoConsumo.getConsumo();
					}
				}
			}
		}

		if ((consumo != null) && (consumo.compareTo(BigDecimal.ZERO) >= CONSTANTE_NUMERO_ZERO)) {
			historicoConsumo.setConsumoApurado(consumo);
		} else if (consumo == null) {
			historicoConsumo.setConsumoApurado(null);
		}

	}

	/**
	 * Obtém uma coleção de CityGate dos Troncos de um mapa de Troncos por Rede ou dos Troncos de RedeDistruicaoTronco.
	 *
	 * @param mapaTroncosPorRede
	 * @param rede
	 * @return cityGates
	 */
	private Collection<CityGate> obterCityGateDeTroncosDaRedeOuRedeDistribuicaoTronco(Map<Rede, List<Tronco>> mapaTroncosPorRede,
			ContratoPontoConsumo contratoPontoConsumo) {

		Collection<CityGate> listaCityGates = null;
		PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();

		Rede rede = null;
		if (pontoConsumo != null && pontoConsumo.getImovel() != null && pontoConsumo.getImovel().getQuadraFace() != null) {
			Imovel imovel = pontoConsumo.getImovel();
			rede = imovel.getQuadraFace().getRede();
		}

		if (mapaTroncosPorRede != null && mapaTroncosPorRede.containsKey(rede)) {
			listaCityGates = obterCityGateDeTroncosDaRede(mapaTroncosPorRede, rede);
		} else {
			listaCityGates = obterCityGateDeRedeDistribuicaoTronco(rede);
		}
		return listaCityGates;
	}

	/**
	 * Obtém uma coleção de CityGate dos Troncos de um mapa de Troncos por Rede.
	 * 
	 * @param mapaTroncosPorRede
	 * @param rede
	 * @return cityGates
	 */
	private Collection<CityGate> obterCityGateDeTroncosDaRede(Map<Rede, List<Tronco>> mapaTroncosPorRede, Rede rede) {
		Collection<CityGate> listaCityGates = new ArrayList<CityGate>();

		List<Tronco> troncos = mapaTroncosPorRede.get(rede);
		for (Tronco tronco : troncos) {
			listaCityGates.add(tronco.getCityGate());
		}
		return listaCityGates;
	}

	/**
	 * Obtém uma coleção de CityGate dos Troncos de RedeDistruicaoTronco
	 *
	 * @param rede
	 * @return cityGates
	 */
	private Collection<CityGate> obterCityGateDeRedeDistribuicaoTronco(Rede rede) {

		Collection<CityGate> listaCityGates = new ArrayList<CityGate>();

		Collection<RedeDistribuicaoTronco> listaRedesDistribuicao = null;
		if (rede != null) {
			listaRedesDistribuicao = rede.getListaRedeTronco();
		}
		if (!Util.isNullOrEmpty(listaRedesDistribuicao)) {
			for (RedeDistribuicaoTronco redeDistribuicaoTronco : listaRedesDistribuicao) {
				Tronco tronco = redeDistribuicaoTronco.getTronco();
				if (tronco != null) {
					listaCityGates.add(tronco.getCityGate());
				}
			}
		}
		return listaCityGates;
	}

	/**
	 * Método auxiliar do consistir leitura de medição responsável por aplicar a correção PTZ ao consumo.
	 *
	 * @param historicoConsumo the historico consumo
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param historicoMedicao the historico medicao
	 * @param dataLeituraAnterior the data leitura anterior
	 * @param dataUltimaLeitura the data ultima leitura
	 * @return the dados ptz impl
	 * @throws FormatoInvalidoException the formato invalido exception
	 * @throws NegocioException the negocio exception
	 */
	private DadosPTZImpl aplicarCorrecaoConsumoFatorPTZ(HistoricoConsumo historicoConsumo, ContratoPontoConsumo contratoPontoConsumo,
			HistoricoMedicao historicoMedicao, Date dataLeituraAnterior, Date dataUltimaLeitura,
			Map<String, Unidade> unidadesPadraoParaConversao, Map<Rede, List<Tronco>> mapaTroncosPorRede,
			Map<Long, Collection<FaixaPressaoFornecimento>> mapaFaixaPressaoFornecimentoPorSegmento) throws GGASException {

		BigDecimal consumoMedido = null;
		DadosPTZImpl dadosPTZ = new DadosPTZImpl();

		// Verifica se o Ponto de Consumo tem
		// equipamento de corretor de vazão
		if (historicoMedicao.getHistoricoInstalacaoMedidor().getVazaoCorretor() == null) {
			BigDecimal fatorPTZ = this.obterFatorPTZConsumoSemCorretorVazao(contratoPontoConsumo, historicoMedicao, dataLeituraAnterior,
					dataUltimaLeitura, dadosPTZ, historicoConsumo, unidadesPadraoParaConversao, mapaTroncosPorRede,
					mapaFaixaPressaoFornecimentoPorSegmento);

			// Aplica Fator Z ao consumo
			// bruto(lido)h
			if (historicoConsumo.getConsumo().compareTo(BigDecimal.ZERO) == CONSTANTE_NUMERO_ZERO) {
				consumoMedido = historicoConsumo.getConsumo();
			} else {
				if (fatorPTZ != null) {
					consumoMedido = historicoConsumo.getConsumo().multiply(fatorPTZ);
				}
			}

		} else {
			// Caso exista corretor de vazão no
			// medidor, o consumo lido não
			// precisará ser corrigido. Será
			// buscado no historico de Leitura, se
			// os dados de
			// Consumo Corrigido e PTZ foram
			// informados por integração
			if (historicoMedicao.getConsumoCorretor() != null) {
				consumoMedido = historicoMedicao.getConsumoCorretor();
				dadosPTZ.setFatorPTZ(historicoMedicao.getFatorPTZCorretor());
				if (historicoConsumo.getConsumo() != null && historicoConsumo.getConsumo().compareTo(BigDecimal.ZERO) > CONSTANTE_NUMERO_ZERO
						&& historicoMedicao.getFatorPTZCorretor() == null) {
					dadosPTZ.setFatorPTZ(consumoMedido.divide(historicoConsumo.getConsumo(), CONSTANTE_NUMERO_QUATRO, RoundingMode.HALF_EVEN));
				}
			} else {
				consumoMedido = historicoConsumo.getConsumo();
			}

			// Obter faixa Pressao fornecimento
			// para verificar
			// a correção do Z
			ControladorUnidade controladorUnidade = ServiceLocator.getInstancia().getControladorUnidade();
			Unidade unidadePadraoPressao = controladorUnidade.obterUnidadePadraoPressaoParaConversao(unidadesPadraoParaConversao);

			BigDecimal pressaoContratada = null;
			Unidade unidadeContratada = unidadePadraoPressao;
			pressaoContratada = obterPressaoContratadaConvertida(contratoPontoConsumo, unidadePadraoPressao);

			PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();
			Segmento segmento = null;
			if (pontoConsumo != null) {
				segmento = pontoConsumo.getSegmento();
			}

			FaixaPressaoFornecimento faixaPressaoFornecimento;
			Collection<FaixaPressaoFornecimento> faixaPressaoFornecimentoPorSegmento = null;
			if (segmento != null && mapaFaixaPressaoFornecimentoPorSegmento != null) {
				faixaPressaoFornecimentoPorSegmento = mapaFaixaPressaoFornecimentoPorSegmento.get(segmento.getChavePrimaria());
				faixaPressaoFornecimento = obterFaixaPressaoFornecimento(contratoPontoConsumo, pressaoContratada, unidadeContratada,
						faixaPressaoFornecimentoPorSegmento);
			} else {
				faixaPressaoFornecimento =
						obterFaixaPressaoFornecimento(contratoPontoConsumo, pressaoContratada, unidadeContratada, segmento);
			}

			BigDecimal fatorZ;
			fatorZ = obterFatorZ(faixaPressaoFornecimento, contratoPontoConsumo, historicoMedicao.getDataLeituraInformada(),
					mapaTroncosPorRede);

			consumoMedido = consumoMedido.multiply(fatorZ);
			dadosPTZ.setMedidaZ(fatorZ);
		}

		dadosPTZ.setConsumoMedido(consumoMedido);

		return dadosPTZ;
	}

	/**
	 * Método auxiliar de correção de PTZ responsável por obter o fatorPTZ a ser aplicado ao consumo e atualizar o objeto Dados de PTZ.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param historicoMedicao the historico medicao
	 * @param dataLeituraAnterior the data leitura anterior
	 * @param dataUltimaLeitura the data ultima leitura
	 * @param dadosPTZ the dados ptz
	 * @param historicoConsumo the historico consumo
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	private BigDecimal obterFatorPTZConsumoSemCorretorVazao(ContratoPontoConsumo contratoPontoConsumo, HistoricoMedicao historicoMedicao,
			Date dataLeituraAnterior, Date dataUltimaLeitura, DadosPTZImpl dadosPTZ, HistoricoConsumo historicoConsumo,
			Map<String, Unidade> unidadesPadraoParaConversao, Map<Rede, List<Tronco>> mapaTroncosPorRede,
			Map<Long, Collection<FaixaPressaoFornecimento>> mapaFaixaPressaoFornecimentoPorSegmento) throws GGASException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();
		ControladorUnidade controladorUnidade = ServiceLocator.getInstancia().getControladorUnidade();

		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		Long constanteCorrigiPTNAO = Long
				.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_PT_NAO));
		Long constanteCorrigiPTSimColetor = Long.parseLong(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_COLETOR));
		Long constanteCorrigiPTSimContrato = Long.parseLong(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_CONTRATO));
		Long constanteCorrigiPTSimContratoPressaoColetada = Long.parseLong(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGE_PT_SIM_CONTRATO_PRESSAO_COLETADA));

		// Obtém a temperatura de referência(T1)
		String valorTemperaturaReferencia =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TEMPERATURA_CONDICAO_REFERENCIA);

		// FA3
		if (valorTemperaturaReferencia == null) {
			throw new NegocioException(Constantes.ERRO_PARAMETRO_NEGOCIO, Constantes.PARAMETRO_TEMPERATURA_CONDICAO_REFERENCIA);
		}

		BigDecimal temperaturaReferencia = Util.converterCampoStringParaValorBigDecimal("Temperatura", valorTemperaturaReferencia,
				Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);

		// Obtém a pressão de referência(P1)
		String valorPressaoReferencia =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_PRESSAO_CONDICAO_REFERENCIA);

		// FA3
		if (valorPressaoReferencia == null) {
			throw new NegocioException(Constantes.ERRO_PARAMETRO_NEGOCIO, Constantes.PARAMETRO_PRESSAO_CONDICAO_REFERENCIA);
		}

		BigDecimal pressaoReferencia = Util.converterCampoStringParaValorBigDecimal("Pressão", valorPressaoReferencia,
				Constantes.FORMATO_VALOR_NUMERO, Constantes.LOCALE_PADRAO);

		// Obtém unidade padrão para temperatura
		Unidade unidadePadraoTemperatura = controladorUnidade.obterUnidadePadraoTemperaturaParaConversao(unidadesPadraoParaConversao);

		// Obtém unidade padrão para pressão
		Unidade unidadePadraoPressao = controladorUnidade.obterUnidadePadraoPressaoParaConversao(unidadesPadraoParaConversao);

		// Obtém medida e unidade de pressão
		// contratada
		Unidade unidadeContratada = unidadePadraoPressao;
		BigDecimal pressaoContratada = obterPressaoContratadaConvertida(contratoPontoConsumo, unidadePadraoPressao);
		
		pressaoReferencia = obterPressaoReferenciaConvertida(pressaoReferencia, unidadePadraoPressao);
		
		ControladorImovelPCSZ controladorImovelPCSZ = (ControladorImovelPCSZ) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorImovelPCSZ.BEAN_ID_CONTROLADOR_IMOVEL_PCS_Z);

		BigDecimal fatorPTZ = BigDecimal.ZERO;
		BigDecimal iFatorPTZ = null;
		BigDecimal fatorZ = null;
		DateTime dataInicioReferencia = new DateTime(dataLeituraAnterior);

		// Obtém dados de mudança de pressão
		String valorCodigoOperacaoMudancaPressao =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_OPERACAO_MUDANCA_PRESSAO);

		BigDecimal volumePorDia = BigDecimal.ZERO;
		BigDecimal volumeTotal = BigDecimal.ZERO;
		if (historicoConsumo.getConsumo().compareTo(BigDecimal.ZERO) == CONSTANTE_NUMERO_ZERO) {
			volumePorDia = BigDecimal.ZERO;
		} else {
			volumePorDia =
					historicoConsumo.getConsumo().divide(BigDecimal.valueOf(historicoConsumo.getDiasConsumo()), DOZE, RoundingMode.HALF_EVEN);
		}

		SetorComercial setorComercial = contratoPontoConsumo.getPontoConsumo().getImovel().getQuadraFace().getQuadra().getSetorComercial();

		List<TemperaturaPressaoMedicao> listaTemperaturaPressaoMedicao =
				ServiceLocator.getInstancia().getControladorTemperaturaPressaoMedicao()
						.obterTemperaturaPressaoMedicaoVigente(setorComercial.getMunicipio(), setorComercial.getLocalidade());

		Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoVigente = ServiceLocator.getInstancia()
				.getControladorTemperaturaPressaoMedicao().obterTemperaturaMedicaoPorMunicipio(setorComercial.getMunicipio());

		List<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor = controladorMedidor.obterHistoricoOperacaoMedidor(
				historicoMedicao.getHistoricoInstalacaoMedidor().getMedidor().getChavePrimaria(),
				Long.parseLong(valorCodigoOperacaoMudancaPressao));

		while (dataInicioReferencia.compareTo(new DateTime(historicoMedicao.getDataLeituraInformada())) < CONSTANTE_NUMERO_ZERO) {

			// Obtém dados de imovelPCSZ
			ImovelPCSZ imovelPCSZ = controladorImovelPCSZ.obterImovelPCSZVigente(
					contratoPontoConsumo.getPontoConsumo().getImovel().getChavePrimaria(), dataInicioReferencia.plusDays(CONSTANTE_NUMERO_UM).toDate());

			ImovelPCSZ imovelPCSZSeguinte = null;
			if (imovelPCSZ != null) {
				imovelPCSZSeguinte = controladorImovelPCSZ.obterProximoImovelPCSZVigente(imovelPCSZ.getChavePrimaria(), dataUltimaLeitura);
			}

			HistoricoOperacaoMedidor mudancaPressao = this.selecionarHistoricoOperacaoMedidorPorDataRealizada(listaHistoricoOperacaoMedidor,
					dataInicioReferencia.plusDays(CONSTANTE_NUMERO_UM).toDate(), dataUltimaLeitura);

			// Obtém dados de temperatura e
			// pressão local
			TemperaturaPressaoMedicao temperaturaPressaoVigente = selecionarTemperaturaPressaoMedicaoVigente(listaTemperaturaPressaoVigente,
					dataInicioReferencia.plusDays(CONSTANTE_NUMERO_UM).toDate(), contratoPontoConsumo);

			TemperaturaPressaoMedicao temperaturaPressaoProximaVigente = null;
			if (temperaturaPressaoVigente != null) {
				temperaturaPressaoProximaVigente = this.selecionarTemperaturaPressaoMedicaoPorDataDeInicio(listaTemperaturaPressaoMedicao,
						dataInicioReferencia.plusDays(INCREMENTO_DATA_INICIO_REFERENCIA).toDate(), dataUltimaLeitura);
			}

			// Data mais próxima em que houve
			// mudança. Usada para obter o
			// período mais curto de leitura
			Date dataProximaVigencia =
					obterDataVigenciaMaisProxima(imovelPCSZSeguinte, mudancaPressao, temperaturaPressaoProximaVigente, dataUltimaLeitura);

			if (dataProximaVigencia != null && dataProximaVigencia.compareTo(dataInicioReferencia.toDate()) == CONSTANTE_NUMERO_ZERO) {
				dataProximaVigencia = dataInicioReferencia.plusDays(CONSTANTE_NUMERO_UM).toDate();
			}

			// quantidade de dias utilizado para
			// calcular a media de volume por dia
			BigDecimal qtdDias = new BigDecimal(Util.intervaloDatas(dataInicioReferencia.toDate(), dataProximaVigencia));

			BigDecimal temperaturaLocal = null;
			BigDecimal pressaoLocal = null;
			// Verifica se a unidade de
			// temperatura local é igual a unidade
			// padrão de temperatura para
			// converter caso diferente
			if (unidadePadraoTemperatura.equals(temperaturaPressaoVigente.getUnidadeTemperatura())) {
				temperaturaLocal = temperaturaPressaoVigente.getTemperatura();
			} else {
				temperaturaLocal = converterUnidadeMedida(temperaturaPressaoVigente.getTemperatura(),
						temperaturaPressaoVigente.getUnidadeTemperatura(), unidadePadraoTemperatura);
			}

			// Verifica se a unidade de pressão
			// local é igual a unidade
			// padrão de pressão para converter
			// caso diferente
			if (unidadePadraoPressao.equals(temperaturaPressaoVigente.getUnidadePressaoAtmosferica())) {
				pressaoLocal = temperaturaPressaoVigente.getPressaoAtmosferica();
			} else {
				pressaoLocal = converterUnidadeMedida(temperaturaPressaoVigente.getPressaoAtmosferica(),
						temperaturaPressaoVigente.getUnidadePressaoAtmosferica(), unidadePadraoPressao);
			}

			// A pressão contratada será a vigente
			// no período, considerando
			// possíveis mudanças de pressão.
			BigDecimal pressaoConsiderada = null;
			Unidade unidadeConsiderada = null;
			if (mudancaPressao != null) {
				if (unidadePadraoPressao.equals(mudancaPressao.getUnidadePressaoAnterior())) {
					unidadeConsiderada = mudancaPressao.getUnidadePressaoAnterior();
					pressaoConsiderada = mudancaPressao.getMedidaPressaoAnterior();
				} else {
					unidadeConsiderada = unidadePadraoPressao;
					pressaoConsiderada = converterUnidadeMedida(mudancaPressao.getMedidaPressaoAnterior(),
							mudancaPressao.getUnidadePressaoAnterior(), unidadePadraoPressao);
				}
			} else {
				unidadeConsiderada = unidadeContratada;
				pressaoConsiderada = pressaoContratada;
			}

			PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();
			Segmento segmento = null;
			if (pontoConsumo != null) {
				segmento = pontoConsumo.getSegmento();
			}

			FaixaPressaoFornecimento faixaPressaoFornecimento;
			Collection<FaixaPressaoFornecimento> faixaPressaoFornecimentoPorSegmento = null;
			if (segmento != null && mapaFaixaPressaoFornecimentoPorSegmento != null) {
				faixaPressaoFornecimentoPorSegmento = mapaFaixaPressaoFornecimentoPorSegmento.get(segmento.getChavePrimaria());
				faixaPressaoFornecimento = obterFaixaPressaoFornecimento(contratoPontoConsumo, pressaoConsiderada, unidadeConsiderada,
						faixaPressaoFornecimentoPorSegmento);
			} else {
				faixaPressaoFornecimento =
						obterFaixaPressaoFornecimento(contratoPontoConsumo, pressaoConsiderada, unidadeConsiderada, segmento);
			}

			ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();
			if (faixaPressaoFornecimento.getIndicadorCorrecaoPT().getChavePrimaria() == constanteCorrigiPTSimColetor) {
				String codigoAnormalidade = null;
				if (historicoMedicao.getPressaoInformada() == null) {
					codigoAnormalidade = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE);
				} else if (historicoMedicao.getUnidadePressaoInformada() == null) {
					codigoAnormalidade = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_UNIDADE_PRESSAO_NAO_INFORMADA_MANUALMENTE);
				} else if (historicoMedicao.getTemperaturaInformada() == null) {
					codigoAnormalidade = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE);
				} else if (historicoMedicao.getUnidadeTemperaturaInformada() == null) {
					codigoAnormalidade = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_UNIDADE_TEMPERATURA_NAO_INFORMADA_MANUALMENTE);
				} else {

					if (unidadePadraoPressao.equals(historicoMedicao.getUnidadePressaoInformada())) {
						pressaoConsiderada = historicoMedicao.getPressaoInformada();
					} else {
						pressaoConsiderada = converterUnidadeMedida(historicoMedicao.getPressaoInformada(),
								historicoMedicao.getUnidadePressaoInformada(), unidadePadraoPressao);
					}

					if (unidadePadraoTemperatura.equals(historicoMedicao.getUnidadeTemperaturaInformada())) {
						temperaturaLocal = historicoMedicao.getTemperaturaInformada();
					} else {
						temperaturaLocal = converterUnidadeMedida(historicoMedicao.getTemperaturaInformada(),
								historicoMedicao.getUnidadeTemperaturaInformada(), unidadePadraoTemperatura);
					}

					iFatorPTZ = obterFatorPT(temperaturaReferencia, pressaoReferencia, temperaturaLocal, pressaoLocal, pressaoConsiderada);
				}

				if (codigoAnormalidade != null) {
					final AnormalidadeConsumo anormalidadeConsumo =
							controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidade));
					aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo);
				}

			} else if (faixaPressaoFornecimento.getIndicadorCorrecaoPT().getChavePrimaria() == constanteCorrigiPTSimContrato) {

				iFatorPTZ = obterFatorPT(temperaturaReferencia, pressaoReferencia, temperaturaLocal, pressaoLocal, pressaoConsiderada);

			} else if (faixaPressaoFornecimento.getIndicadorCorrecaoPT()
					.getChavePrimaria() == constanteCorrigiPTSimContratoPressaoColetada) {

				if (contratoPontoConsumo.getMedidaPressaoColetada() != null) {
					pressaoConsiderada = contratoPontoConsumo.getMedidaPressaoColetada();
				}

				iFatorPTZ = obterFatorPT(temperaturaReferencia, pressaoReferencia, temperaturaLocal, pressaoLocal, pressaoConsiderada);

			} else if (faixaPressaoFornecimento.getIndicadorCorrecaoPT().getChavePrimaria() == constanteCorrigiPTNAO) {

				iFatorPTZ = BigDecimal.ONE;
			} else {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CONSTANTE_CORRIGE_PT_MAL_CONFIGURADA, true);
			}

			// Obtém o fator Z da faixas de
			// pressão de fornecimento caso não
			// exista um definido e vigente para o
			// imóvel
			if (imovelPCSZ == null) {
				fatorZ = obterFatorZ(faixaPressaoFornecimento, contratoPontoConsumo, historicoMedicao.getDataLeituraInformada(),
						mapaTroncosPorRede);
			} else {
				fatorZ = imovelPCSZ.getFatorZ();
			}

			// APlicacao do fator Z
			if (fatorZ != null && iFatorPTZ != null) {
				iFatorPTZ = iFatorPTZ.multiply(fatorZ);
				// ((P2/T2) *T1/P1))*Z

			} else {
				fatorZ = BigDecimal.ONE;

			}

			if (iFatorPTZ != null) {
				BigDecimal volumePeriodo = volumePorDia.multiply(qtdDias);
				if (!(volumePeriodo.compareTo(BigDecimal.ZERO) == CONSTANTE_NUMERO_ZERO)) {
					iFatorPTZ = iFatorPTZ.multiply(volumePorDia.multiply(qtdDias));
				}
				fatorPTZ = fatorPTZ.add(iFatorPTZ);
			}

			volumeTotal = volumeTotal.add(volumePorDia.multiply(qtdDias));

			// Atualiza a data de referência para
			// cálculo do período
			dataInicioReferencia = new DateTime(dataProximaVigencia);
		}

		// Divide pelo total de dias de consumo da
		// leitura para obter a média
		// ponderada
		if (volumeTotal.doubleValue() > CONSTANTE_NUMERO_ZERO && fatorPTZ != null) {
			fatorPTZ = fatorPTZ.divide(volumeTotal, CONSTANTE_NUMERO_QUATRO, RoundingMode.HALF_EVEN);
		}

		dadosPTZ.setMedidaZ(fatorZ);
		if (fatorPTZ != null && fatorPTZ.compareTo(BigDecimal.ZERO) != CONSTANTE_NUMERO_ZERO) {
			dadosPTZ.setFatorPTZ(fatorPTZ);
		} else {
			fatorPTZ = null;
		}

		return fatorPTZ;
	}

	private BigDecimal obterPressaoContratadaConvertida(ContratoPontoConsumo contratoPontoConsumo,
			Unidade unidadePadraoPressao) throws NegocioException {
		BigDecimal pressaoContratada;
		if (unidadePadraoPressao.equals(contratoPontoConsumo.getUnidadePressao())) {

			pressaoContratada = contratoPontoConsumo.getMedidaPressao();

		} else {
			pressaoContratada = converterUnidadeMedida(contratoPontoConsumo.getMedidaPressao(), contratoPontoConsumo.getUnidadePressao(),
					unidadePadraoPressao);

		}
		return pressaoContratada;
	}
	
	private BigDecimal obterPressaoReferenciaConvertida(BigDecimal pressaoReferencia,
			Unidade unidadePressaoPadrao) throws NegocioException {
		
		ControladorUnidade controladorUnidade = ServiceLocator.getInstancia().getControladorUnidade();
		
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		String unidadeParametroPressao =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_UNIDADE_PRESSAO_MEDICAO);

		Unidade unidadeParametro = (Unidade) controladorUnidade.obter(Long.parseLong(unidadeParametroPressao));
		
		if (unidadePressaoPadrao.equals(unidadeParametro)) {

			return pressaoReferencia;

		} else {
			return converterUnidadeMedida(pressaoReferencia, unidadeParametro, unidadePressaoPadrao);

		}
	}

	private TemperaturaPressaoMedicao selecionarTemperaturaPressaoMedicaoVigente(
			Collection<TemperaturaPressaoMedicao> listaTemperaturaPressaoVigente, Date dataInicioReferencia,
			ContratoPontoConsumo contratoPontoConsumo) throws NegocioException {

		TemperaturaPressaoMedicao temperaturaPressaoMedicaoLocal = listaTemperaturaPressaoVigente.stream()
				.filter(temperaturaPressaoMedicao -> (temperaturaPressaoMedicao.getDataInicio()).before(dataInicioReferencia)
						|| (temperaturaPressaoMedicao.getDataInicio()).equals(dataInicioReferencia))
				.findFirst().orElse(null);

		if (temperaturaPressaoMedicaoLocal == null || temperaturaPressaoMedicaoLocal.getTemperatura() == null
				|| temperaturaPressaoMedicaoLocal.getUnidadeTemperatura() == null
				|| temperaturaPressaoMedicaoLocal.getPressaoAtmosferica() == null
				|| temperaturaPressaoMedicaoLocal.getUnidadePressaoAtmosferica() == null) {

			if (temperaturaPressaoMedicaoLocal == null || temperaturaPressaoMedicaoLocal.getTemperatura() == null
					|| temperaturaPressaoMedicaoLocal.getUnidadeTemperatura() == null) {
				throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_TEMPERATURA_LOCAL_FORNECIMENTO_NAO_ENCONTRADA,
						contratoPontoConsumo.getPontoConsumo().getChavePrimaria() + " - "
								+ contratoPontoConsumo.getPontoConsumo().getDescricaoFormatada());
			} else {
				throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_PRESSAO_LOCAL_FORNECIMENTO_NAO_ENCONTRADA,
						contratoPontoConsumo.getPontoConsumo().getChavePrimaria() + " - "
								+ contratoPontoConsumo.getPontoConsumo().getDescricaoFormatada());
			}
		}

		return temperaturaPressaoMedicaoLocal;
	}

	private TemperaturaPressaoMedicao selecionarTemperaturaPressaoMedicaoPorDataDeInicio(
			List<TemperaturaPressaoMedicao> listaDetemperaturaPressaoMedicao, Date dataLimiteInferior, Date dataLimiteSuperior) {

		TemperaturaPressaoMedicao temperaturaPressaoMedicao = listaDetemperaturaPressaoMedicao.stream()
				.filter((temperaturaPressao) -> dataLimiteInferior.compareTo(temperaturaPressao.getDataInicio()) <= CONSTANTE_NUMERO_ZERO
						&& dataLimiteSuperior.compareTo(temperaturaPressao.getDataInicio()) >= CONSTANTE_NUMERO_ZERO)
				.findFirst().orElse(null);

		if (temperaturaPressaoMedicao == null || temperaturaPressaoMedicao.getTemperatura() == null
				|| temperaturaPressaoMedicao.getUnidadeTemperatura() == null || temperaturaPressaoMedicao.getPressaoAtmosferica() == null
				|| temperaturaPressaoMedicao.getUnidadePressaoAtmosferica() == null) {

			temperaturaPressaoMedicao = null;
		}

		return temperaturaPressaoMedicao;
	}

	private HistoricoOperacaoMedidor selecionarHistoricoOperacaoMedidorPorDataRealizada(
			List<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor, Date dataLimiteInferior, Date dataLimiteSuperior) {

		return listaHistoricoOperacaoMedidor.stream()
				.filter((historicoOperacao) -> dataLimiteInferior.compareTo(historicoOperacao.getDataRealizada()) < CONSTANTE_NUMERO_ZERO
						&& dataLimiteSuperior.compareTo(historicoOperacao.getDataRealizada()) >= CONSTANTE_NUMERO_ZERO)
				.findFirst().orElse(null);
	}

	/**
	 * Obter data vigencia mais proxima.
	 *
	 * @param imovelPCSZSeguinte the imovel pcsz seguinte
	 * @param historicoMudancaPressao the historico mudanca pressao
	 * @param temperaturaPressaoProximaVigente the temperatura pressao proxima vigente
	 * @param dataUltimaLeitura the data ultima leitura
	 * @return the date
	 */
	private Date obterDataVigenciaMaisProxima(ImovelPCSZ imovelPCSZSeguinte, HistoricoOperacaoMedidor historicoMudancaPressao,
			TemperaturaPressaoMedicao temperaturaPressaoProximaVigente, Date dataUltimaLeitura) {

		Date data = dataUltimaLeitura;

		if (imovelPCSZSeguinte != null && new DateTime(imovelPCSZSeguinte.getDataVigencia())
				.minusDays(CONSTANTE_NUMERO_UM).toDate().before(data)) {
			data = new DateTime(imovelPCSZSeguinte.getDataVigencia()).minusDays(CONSTANTE_NUMERO_UM).toDate();
		}
		if (temperaturaPressaoProximaVigente != null
				&& new DateTime(temperaturaPressaoProximaVigente.getDataInicio()).minusDays(CONSTANTE_NUMERO_UM).toDate().before(data)) {
			data = new DateTime(temperaturaPressaoProximaVigente.getDataInicio()).minusDays(CONSTANTE_NUMERO_UM).toDate();
		}
		if (historicoMudancaPressao != null
				&& (new DateTime(historicoMudancaPressao.getDataRealizada()).minusDays(CONSTANTE_NUMERO_UM).toDate().before(data))) {
			data = new DateTime(historicoMudancaPressao.getDataRealizada()).minusDays(CONSTANTE_NUMERO_UM).toDate();
		}

		return data;
	}

	/**
	 * Método auxiliar para cálculo do fator PTZ responsável por obter o fator Z a ser usado no cálculo.
	 *
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param pressaoAnterior the pressao anterior
	 * @param unidadeAnterior the unidade anterior
	 * @param segmento the segmento
	 * @return the faixa pressao fornecimento
	 * @throws NegocioException the negocio exception
	 */
	@Override
	public FaixaPressaoFornecimento obterFaixaPressaoFornecimento(ContratoPontoConsumo contratoPontoConsumo, BigDecimal pressaoAnterior,
			Unidade unidadeAnterior, Segmento segmento) throws NegocioException {

		ControladorFaixaPressaoFornecimento controladorFaixaPressaoFornecimento =
				ServiceLocator.getInstancia().getControladorFaixaPressaoFornecimento();

		// Obtém o Z da tabela de faixa de pressão
		FaixaPressaoFornecimento faixaPressaoFornecimento =
				controladorFaixaPressaoFornecimento.obterFaixaPressaoFornecimentoPorMedida(pressaoAnterior, unidadeAnterior, segmento);

		ControladorUnidade controladorUnidade =
				(ControladorUnidade) ServiceLocator.getInstancia().getBeanPorID(ControladorUnidade.BEAN_ID_CONTROLADOR_UNIDADE);
		Collection<Unidade> listaUnidades = controladorUnidade.listarUnidadesPressao();

		if (faixaPressaoFornecimento == null) {
			for (Unidade unidade : listaUnidades) {
				BigDecimal valorPressao =
						(converterUnidadeMedida(pressaoAnterior, unidadeAnterior, unidade)).setScale(CONSTANTE_NUMERO_QUATRO, RoundingMode.HALF_UP);
				faixaPressaoFornecimento =
						controladorFaixaPressaoFornecimento.obterFaixaPressaoFornecimentoPorMedida(valorPressao, unidade, segmento);
				if (faixaPressaoFornecimento != null) {
					break;
				}
			}
		}

		// Verifica se existe faixa de pressão
		// para a pressão contratada
		if (faixaPressaoFornecimento == null) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_FAIXA_PRESSAO_NAO_ENCONTRADA,
					new Object[] { contratoPontoConsumo.getPontoConsumo().getChavePrimaria() + " - "
							+ contratoPontoConsumo.getPontoConsumo().getDescricaoFormatada(), pressaoAnterior });
		}

		return faixaPressaoFornecimento;
	}

	/**
	 * Método auxiliar para cálculo do fator PTZ responsável por obter o fator Z a ser usado no cálculo.
	 *
	 * @param contratoPontoConsumo
	 * @param pressaoAnterior
	 * @param unidadeAnterior
	 * @param faixaPressaoFornecimentoPorSegmento
	 * @return faixa pressão de fornecimento
	 * @throws NegocioException
	 */
	@Override
	public FaixaPressaoFornecimento obterFaixaPressaoFornecimento(ContratoPontoConsumo contratoPontoConsumo, BigDecimal pressaoAnterior,
			Unidade unidadeAnterior, Collection<FaixaPressaoFornecimento> faixaPressaoFornecimentoPorSegmento) throws NegocioException {

		ControladorFaixaPressaoFornecimento controladorFaixaPressaoFornecimento =
				ServiceLocator.getInstancia().getControladorFaixaPressaoFornecimento();

		// Obtém o Z da tabela de faixa de pressão
		FaixaPressaoFornecimento faixaPressaoFornecimento =
				controladorFaixaPressaoFornecimento.escolherFaixaPressaoFornecimentoPorUnidadeEmedidaMaximaMinima(
						faixaPressaoFornecimentoPorSegmento, pressaoAnterior, unidadeAnterior);

		ControladorUnidade controladorUnidade =
				(ControladorUnidade) ServiceLocator.getInstancia().getBeanPorID(ControladorUnidade.BEAN_ID_CONTROLADOR_UNIDADE);

		if (faixaPressaoFornecimento == null) {
			Collection<Unidade> listaUnidades = controladorUnidade.listarUnidadesPressao();
			for (Unidade unidade : listaUnidades) {
				BigDecimal valorPressao =
						(converterUnidadeMedida(pressaoAnterior, unidadeAnterior, unidade)).setScale(CONSTANTE_NUMERO_QUATRO, RoundingMode.HALF_UP);
				faixaPressaoFornecimento =
						controladorFaixaPressaoFornecimento.escolherFaixaPressaoFornecimentoPorUnidadeEmedidaMaximaMinima(
								faixaPressaoFornecimentoPorSegmento, valorPressao, unidade);
				if (faixaPressaoFornecimento != null) {
					break;
				}
			}
		}

		// Verifica se existe faixa de pressão
		// para a pressão contratada
		if (faixaPressaoFornecimento == null) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_FAIXA_PRESSAO_NAO_ENCONTRADA,
					new Object[] { contratoPontoConsumo.getPontoConsumo().getChavePrimaria() + " - "
							+ contratoPontoConsumo.getPontoConsumo().getDescricaoFormatada(), pressaoAnterior });
		}

		return faixaPressaoFornecimento;
	}

	/**
	 * Método auxiliar para cálculo do fator PTZ responsável por obter o fator Z a ser usado no cálculo.
	 *
	 * @param faixaPressaoFornecimento the faixa pressao fornecimento
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param dataLeituraInformada the data leitura informada
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	private BigDecimal obterFatorZ(FaixaPressaoFornecimento faixaPressaoFornecimento, ContratoPontoConsumo contratoPontoConsumo,
			Date dataLeituraInformada, Map<Rede, List<Tronco>> mapaTroncosPorRede) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		Long constanteCorrigiZNAO = Long
				.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_Z_NAO));
		Long constanteCorrigiZSimValor = Long.parseLong(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_VALOR));
		Long constanteCorrigiZSimCromatografia = Long.parseLong(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_Z_SIM_CROMATOGRAFIA));

		BigDecimal fatorZ = null;

		if (faixaPressaoFornecimento.getIndicadorCorrecaoZ() != null) {

			if (faixaPressaoFornecimento.getIndicadorCorrecaoZ().getChavePrimaria() == constanteCorrigiZNAO) {
				fatorZ = BigDecimal.ONE;
			} else if (faixaPressaoFornecimento.getIndicadorCorrecaoZ().getChavePrimaria() == constanteCorrigiZSimValor) {

				// Verifica se foi informado o
				// número do fator Z
				if (faixaPressaoFornecimento.getNumeroFatorZ() == null) {
					throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_FAIXA_PRESSAO_SEM_Z_INFORMADO,
							new Object[] {
									contratoPontoConsumo.getPontoConsumo().getChavePrimaria() + " - "
											+ contratoPontoConsumo.getPontoConsumo().getDescricaoFormatada(),
									faixaPressaoFornecimento.getDescricaoFormatada() });
				} else {
					// Atribui o fator Z da faixa
					// de pressão ao Z que será
					// aplicado no cálculo da
					// correção
					fatorZ = faixaPressaoFornecimento.getNumeroFatorZ();
				}

			} else if (faixaPressaoFornecimento.getIndicadorCorrecaoZ().getChavePrimaria() == constanteCorrigiZSimCromatografia) {
				Collection<PontoConsumoCityGate> listaPontoConsumoCityGate =
						controladorPontoConsumo.obterPontoConsumoCityGate(contratoPontoConsumo.getPontoConsumo().getChavePrimaria());
				fatorZ = BigDecimal.ZERO;
				if (listaPontoConsumoCityGate != null && listaPontoConsumoCityGate.size() > CONSTANTE_NUMERO_UM) {
					for (PontoConsumoCityGate pontoConsumoCityGate : listaPontoConsumoCityGate) {
						Cromatografia cromatografiaCadastrada = controladorCromatografia
								.consultarCromatografiaPorCityGateDataLeitura(pontoConsumoCityGate.getCityGate(), dataLeituraInformada);
						BigDecimal fatorCompressibilidade;
						if (cromatografiaCadastrada != null) {
							fatorCompressibilidade = cromatografiaCadastrada.getFatorCompressibilidade();
							fatorZ = fatorZ.add(fatorCompressibilidade
									.multiply(pontoConsumoCityGate.getPercentualParticipacao().divide(new BigDecimal(CONSTANTE_NUMERO_CEM))));
						} else {
							throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_CROMATOGRAFIA_NAO_CADASTRADA_PARA_CITYGATE,
									new Object[] { pontoConsumoCityGate.getCityGate().getChavePrimaria() + " - "
											+ pontoConsumoCityGate.getCityGate().getDescricao() });
						}
					}
				} else {
					Collection<CityGate> listaCityGates =
							obterCityGateDeTroncosDaRedeOuRedeDistribuicaoTronco(mapaTroncosPorRede, contratoPontoConsumo);

					String parametroPercentualParticipacaoCityGate =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PERCENTUAL_PARTICIPACAO_CITY_GATE);
					double percentualCityGate = Integer.parseInt(parametroPercentualParticipacaoCityGate) / (double) listaCityGates.size();
					BigDecimal percentualCityGateB = BigDecimal.valueOf(percentualCityGate);

					for (CityGate cityGate : listaCityGates) {
						Cromatografia cromatografiaCadastrada =
								controladorCromatografia.consultarCromatografiaPorCityGateDataLeitura(cityGate, dataLeituraInformada);
						BigDecimal fatorCompressibilidade;
						if (cromatografiaCadastrada != null) {
							fatorCompressibilidade = cromatografiaCadastrada.getFatorCompressibilidade();

							fatorZ = fatorZ.add(fatorCompressibilidade.multiply(percentualCityGateB.divide(new BigDecimal(CONSTANTE_NUMERO_CEM))));
						} else {
							throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_CROMATOGRAFIA_NAO_CADASTRADA_PARA_CITYGATE,
									new Object[] { cityGate.getChavePrimaria() + " - " + cityGate.getDescricao() });
						}
					}
				}

				if (fatorZ.compareTo(BigDecimal.ZERO) == CONSTANTE_NUMERO_ZERO) {
					throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_CROMATOGRAFIA_NAO_CADASTRADA_PARA_CITYGATE);
				}
			}
		}

		return fatorZ;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#obterFatorPT(java.math.BigDecimal, java.math.BigDecimal,
	 * java.math.BigDecimal, java.math.BigDecimal, java.math.BigDecimal)
	 */
	@Override
	public BigDecimal obterFatorPT(BigDecimal temperaturaReferencia, BigDecimal pressaoReferencia, BigDecimal temperaturaLocal,
			BigDecimal pressaoLocal, BigDecimal pressaoConsiderada) {

		BigDecimal fatorPT;

		// Calcula fatorPTZ
		fatorPT = pressaoConsiderada.add(pressaoLocal);
		// P2
		fatorPT = fatorPT.divide(temperaturaLocal, 12, RoundingMode.HALF_EVEN);
		// P2/T2
		fatorPT = fatorPT.multiply(temperaturaReferencia).divide(pressaoReferencia, 12, RoundingMode.HALF_EVEN);
		// (P2/T2) *T1/P1)

		return fatorPT;
	}

	/**
	 * Obter lista dados local amostragem fixo city gate.
	 *
	 * @param listaCityGateMedicao the lista city gate medicao
	 * @param listaCityGates the lista city gates
	 * @param dataIntervalo the data intervalo
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Collection<DadosLocalAmostragemImpl> obterListaDadosLocalAmostragemFixoCityGate(
			Collection<CityGateMedicao> listaCityGateMedicao, Collection<CityGate> listaCityGates, Date dataIntervalo)
			throws NegocioException {

		Collection<DadosLocalAmostragemImpl> listaDadosLocalCalculo = new ArrayList<DadosLocalAmostragemImpl>();

		for (CityGate cityGate : listaCityGates) {
			List<CityGateMedicao> cityGatesMedicao =
					this.carregarListaMedicaoFixoPorCityGate(listaCityGateMedicao, cityGate, dataIntervalo);
			if (!cityGatesMedicao.isEmpty()) {
				Collections.sort(cityGatesMedicao, COMPARATOR_CITY_GATE_MEDICAO);
				CityGateMedicao medicaoFixoVigente = cityGatesMedicao.get(CONSTANTE_NUMERO_ZERO);

				// Busca o volume da Distribuidora
				// para o Fixo
				DadosLocalAmostragemImpl dadosMedicaoVolumeData =
						this.consultarVolumeCityGateMedicaoDiario(cityGate, dataIntervalo, ATRIBUTO_SELECAO_VOLUME_PCS_FIXO);
				Integer medicaoVolumeData = null;
				if (dadosMedicaoVolumeData != null) {
					medicaoVolumeData = dadosMedicaoVolumeData.getMedidaVolume();
				}

				listaDadosLocalCalculo.add(this.criarDadosLocalAmostragem(medicaoFixoVigente, dataIntervalo, medicaoVolumeData));
			}
		}

		return listaDadosLocalCalculo;
	}

	/**
	 * Carrega uma lista de dados de Medição de PCS Fixo por Citygate, baseada numa data de referencia.
	 *
	 * @param listaCityGateMedicao [obrigatorio]
	 * @param cityGate [obrigatorio]
	 * @param dataIntervalo [obrigatorio]
	 * @return List<CityGateMedicao> - somente com os dados de PCS Fixo vigentes para a data de referencia
	 * @throws NullPointerException se param obrigatorios sejam nulos
	 */
	private List<CityGateMedicao> carregarListaMedicaoFixoPorCityGate(Collection<CityGateMedicao> listaCityGateMedicao, CityGate cityGate,
			Date dataIntervalo) {

		List<CityGateMedicao> medicoesFixoCityGate = new ArrayList<CityGateMedicao>();
		for (CityGateMedicao dadosCityGateMedicao : listaCityGateMedicao) {
			boolean isMesmoCityGate = cityGate.equals(dadosCityGateMedicao.getCityGate());
			if (isMesmoCityGate && Util.compararDatas(dadosCityGateMedicao.getDataMedicao(), dataIntervalo) <= CONSTANTE_NUMERO_ZERO) {
				medicoesFixoCityGate.add(dadosCityGateMedicao);
			}
		}

		return medicoesFixoCityGate;
	}

	/**
	 * retorna um VO de dados de amostragem de PCS Fixo, por city gate e data base. Atribui por "default", o volume da distribuidora como
	 * medida de volume do PCS fixo.
	 *
	 * @param cityGateMedicao the city gate medicao
	 * @param dataIntervalo the data intervalo
	 * @param volumeDistribuidoraData
	 *
	 * @return the dados local amostragem impl
	 * @throws NegocioException the negocio exception
	 */
	private DadosLocalAmostragemImpl criarDadosLocalAmostragem(CityGateMedicao cityGateMedicao, Date dataIntervalo,
			Integer volumeDistribuidoraData) throws NegocioException {

		ControladorEntidadeConteudo controladorEntidadeConteudo = ServiceLocator.getInstancia().getControladorEntidadeConteudo();

		DadosLocalAmostragemImpl dadosLocal = new DadosLocalAmostragemImpl();
		dadosLocal.setData(dataIntervalo);
		dadosLocal.setMedidaPCS(cityGateMedicao.getMedidaPCSFixo());
		dadosLocal.setCityGate(cityGateMedicao.getCityGate());
		dadosLocal.setMedidaVolume(volumeDistribuidoraData);
		controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		EntidadeConteudo localFixoCityGate = (EntidadeConteudo) controladorEntidadeConteudo.obter(4L);
		localFixoCityGate.setChavePrimaria(4L);

		dadosLocal.setLocalAmostragem(localFixoCityGate);
		return dadosLocal;
	}

	/**
	 * Método responsável pela apuração do fator PCS para um consumo informado.
	 *
	 * @param historicoConsumo the historico consumo
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param listaCityGates the lista city gates
	 * @param dataLeituraAnterior the data leitura anterior
	 * @param dataLeituraAtual the data leitura atual
	 * @return - VO com o resultado do Fator Apurado e PCS do periodo
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	private DadosPCSImpl aplicarCorrecaoConsumoFatorPCS(
			Map<PontoConsumo, Map<CityGate, List<PontoConsumoCityGate>>> mapaPontoConsumoCityGate, HistoricoConsumo historicoConsumo,
			ContratoPontoConsumo contratoPontoConsumo, Collection<CityGate> listaCityGates, Date dataLeituraAnterior, Date dataLeituraAtual,
			Map<Long, Collection<ContratoPontoConsumoPCSAmostragem>> mapaContratoPontoConsumoPCSAmostragem,
			Map<Long, Collection<ContratoPontoConsumoPCSIntervalo>> mapaContratoPontoConsumoPCSIntervalo) throws GGASException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long chaveContratoPontoConsumo = contratoPontoConsumo.getChavePrimaria();

		Collection<ContratoPontoConsumoPCSAmostragem> listaLocalAmostragem = null;
		if (mapaContratoPontoConsumoPCSAmostragem != null && mapaContratoPontoConsumoPCSAmostragem.containsKey(chaveContratoPontoConsumo)) {
			listaLocalAmostragem = mapaContratoPontoConsumoPCSAmostragem.get(chaveContratoPontoConsumo);
		} else {
			listaLocalAmostragem = this.consultarContratoPontoConsumoPCSAmostragem(chaveContratoPontoConsumo);
		}

		// Local
		// de
		// amostragem
		// priorizado
		// para
		// o
		// contrato
		Collection<ContratoPontoConsumoPCSIntervalo> listaPCSIntervalo = null;
		if (mapaContratoPontoConsumoPCSIntervalo != null && mapaContratoPontoConsumoPCSIntervalo.containsKey(chaveContratoPontoConsumo)) {
			listaPCSIntervalo = mapaContratoPontoConsumoPCSIntervalo.get(chaveContratoPontoConsumo);
		} else {
			listaPCSIntervalo = this.consultarContratoPontoConsumoPCSIntervalo(chaveContratoPontoConsumo);
		}

		// Intervalo
		// priorizado
		// para
		// o
		// contrato

		Collection<Date> datasDoIntervaloLeitura = obterIntervaloDatasLeitura(dataLeituraAnterior, dataLeituraAtual);
		Collection<Date> datasDadosLocalAmostragem = new HashSet<Date>();
		Collection<DadosLocalAmostragemImpl> dadosLocalAmostragemCalculo = new HashSet<DadosLocalAmostragemImpl>();

		DadosPCSImpl dadosPCS = null;
		Collection<DadosLocalAmostragemImpl> listaDadosLocalAmostragem = null;
		EntidadeConteudo localAmostragemParaCalculo = null;
		Map<Date, Collection<DadosLocalAmostragemImpl>> mapaDadosLocalAmostragemPorData = new HashMap<>();

		Collection<DadosLocalAmostragemImpl> listaDadosLocalAmostragemDistribuidora = this.consultarCityGateMedicao(listaCityGates,
				dataLeituraAnterior, dataLeituraAtual, EntidadeConteudo.DISTRIBUIDORA, ATRIBUTO_SELECAO_PCS_DISTRIBUIDORA);
		Collection<DadosLocalAmostragemImpl> listaDadosLocalAmostragemSupridora = this.consultarCityGateMedicao(listaCityGates,
				dataLeituraAnterior, dataLeituraAtual, EntidadeConteudo.SUPRIDORA, ATRIBUTO_SELECAO_PCS_SUPRIDORA);

		for (ContratoPontoConsumoPCSAmostragem localAmostragem : listaLocalAmostragem) {

			if (localAmostragem.getLocalAmostragemPCS().getChavePrimaria() == PARAMETRO_PONTO_CONSUMO) {
				listaDadosLocalAmostragem =
						this.consultarImovelPCSZ(historicoConsumo.getPontoConsumo().getImovel(), datasDoIntervaloLeitura);

				localAmostragemParaCalculo = this.montarDadosParaCalculoPorPontoConsumo(listaDadosLocalAmostragem, datasDoIntervaloLeitura,
						datasDadosLocalAmostragem, dadosLocalAmostragemCalculo, localAmostragemParaCalculo, localAmostragem);

			} else if (localAmostragem.getLocalAmostragemPCS().getChavePrimaria() == PARAMETRO_DISTRIBUIDORA) {

				listaDadosLocalAmostragem = listaDadosLocalAmostragemDistribuidora;
				localAmostragemParaCalculo = this.montarDadosParaCalculo(listaDadosLocalAmostragem, datasDoIntervaloLeitura,
						datasDadosLocalAmostragem, dadosLocalAmostragemCalculo, localAmostragemParaCalculo, listaCityGates, localAmostragem,
						mapaDadosLocalAmostragemPorData);
			} else if (localAmostragem.getLocalAmostragemPCS().getChavePrimaria() == PARAMETRO_SUPRIDORA) {

				listaDadosLocalAmostragem = listaDadosLocalAmostragemSupridora;
				localAmostragemParaCalculo = this.montarDadosParaCalculo(listaDadosLocalAmostragem, datasDoIntervaloLeitura,
						datasDadosLocalAmostragem, dadosLocalAmostragemCalculo, localAmostragemParaCalculo, listaCityGates, localAmostragem,
						mapaDadosLocalAmostragemPorData);

			} else if (localAmostragem.getLocalAmostragemPCS().getChavePrimaria() == PARAMETRO_CITY_GATE) {
				Collection<CityGateMedicao> listaCityGateMedicaoFixo = this.consultarCityGateMedicaoFixo(listaCityGates, dataLeituraAtual);
				if (listaCityGateMedicaoFixo != null && !listaCityGateMedicaoFixo.isEmpty()) {
					Collection<DadosLocalAmostragemImpl> listaDadosLocal = new ArrayList<DadosLocalAmostragemImpl>();
					for (Date dataIntervalo : datasDoIntervaloLeitura) {
						listaDadosLocal.addAll(
								this.obterListaDadosLocalAmostragemFixoCityGate(listaCityGateMedicaoFixo, listaCityGates, dataIntervalo));

					}
					localAmostragemParaCalculo = this.montarDadosParaCalculo(listaDadosLocal, datasDoIntervaloLeitura,
							datasDadosLocalAmostragem, dadosLocalAmostragemCalculo, localAmostragemParaCalculo, listaCityGates,
							localAmostragem, mapaDadosLocalAmostragemPorData);
				}
			}

			if (datasDoIntervaloLeitura.isEmpty()) {
				break;
			}
		}

		if (localAmostragemParaCalculo == null) {
			localAmostragemParaCalculo = this.obterLocalAmostragemPrioritarioCalculoPCS(listaLocalAmostragem);
		}

		this.prepararVolumeParticipacaoParaCalculo(listaLocalAmostragem, mapaPontoConsumoCityGate, dadosLocalAmostragemCalculo,
				contratoPontoConsumo);

		boolean intervaloPCSReal = datasDoIntervaloLeitura.isEmpty();

		if (intervaloPCSReal) {

			IntervaloPCS intervaloPCS = this.obterIntervaloPCS(IntervaloPCS.INTERVALO_REAL);
			dadosPCS = this.preecherDadosPCS(historicoConsumo, listaCityGates, dadosLocalAmostragemCalculo, localAmostragemParaCalculo,
					intervaloPCS, contratoPontoConsumo.getTipoMedicao());
			return dadosPCS;

		} else {
			if (listaPCSIntervalo.size() < CONSTANTE_NUMERO_DOIS) {
				dadosPCS = new DadosPCSImpl();
				dadosPCS.setLocalAmostragemPCS(localAmostragemParaCalculo);

				this.associarAnomaliaConsumoNaoApurado(historicoConsumo);
				return dadosPCS;
			}

			IntervaloPCS intervaloPCSParaCalculo = this.apurarVolumeIntervaloNaoReal(mapaPontoConsumoCityGate, historicoConsumo,
					contratoPontoConsumo, listaCityGates, dataLeituraAnterior, dataLeituraAtual, listaLocalAmostragem, listaPCSIntervalo,
					dadosLocalAmostragemCalculo, localAmostragemParaCalculo);

			dadosPCS = this.preecherDadosPCS(historicoConsumo, listaCityGates, dadosLocalAmostragemCalculo, localAmostragemParaCalculo,
					intervaloPCSParaCalculo, contratoPontoConsumo.getTipoMedicao());
		}

		String codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_NAO_APURADO);
		if (historicoConsumo.getTipoConsumo().getChavePrimaria() == Long.parseLong(codigoTipoConsumo)) {
			dadosPCS.setFatorPCS(null);
		}

		return dadosPCS;
	}

	/**
	 * Apurar volume intervalo nao real.
	 *
	 * @param historicoConsumo the historico consumo
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @param listaCityGates the lista city gates
	 * @param dataLeituraAnterior the data leitura anterior
	 * @param dataLeituraAtual the data leitura atual
	 * @param listaLocalAmostragem the lista local amostragem
	 * @param listaPCSIntervalo the lista pcs intervalo
	 * @param dadosLocalAmostragemCalculo the dados local amostragem calculo
	 * @param localAmostragemParaCalculo the local amostragem para calculo
	 * @return the intervalo pcs
	 * @throws NegocioException the negocio exception
	 */
	private IntervaloPCS apurarVolumeIntervaloNaoReal(Map<PontoConsumo, Map<CityGate, List<PontoConsumoCityGate>>> mapaPontoConsumoCityGate,
			HistoricoConsumo historicoConsumo, ContratoPontoConsumo contratoPontoConsumo, Collection<CityGate> listaCityGates,
			Date dataLeituraAnterior, Date dataLeituraAtual, Collection<ContratoPontoConsumoPCSAmostragem> listaLocalAmostragem,
			Collection<ContratoPontoConsumoPCSIntervalo> listaPCSIntervalo,
			Collection<DadosLocalAmostragemImpl> dadosLocalAmostragemCalculo, EntidadeConteudo localAmostragemParaCalculo)
			throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ServiceLocator.getInstancia().getControladorParametroSistema();

		IntervaloPCS intervaloPCSParaCalculo = null;

		List<ContratoPontoConsumoPCSIntervalo> listaPCSIntervaloOrdenada = new ArrayList<ContratoPontoConsumoPCSIntervalo>();
		// Transformar um o collection para list
		// para poder usar metodo de ordenação
		// "sort"
		for (ContratoPontoConsumoPCSIntervalo pcs : listaPCSIntervalo) {
			listaPCSIntervaloOrdenada.add(pcs);
		}

		Collections.sort(listaPCSIntervaloOrdenada, new Comparator<ContratoPontoConsumoPCSIntervalo>() {

			@Override
			public int compare(ContratoPontoConsumoPCSIntervalo c1, ContratoPontoConsumoPCSIntervalo c2) {

				return c1.getPrioridade().compareTo(c2.getPrioridade());
			}
		});

		// remoção de registros com volume ou pcs
		// null
		Collection<DadosLocalAmostragemImpl> dadosParaRemocao = new ArrayList<DadosLocalAmostragemImpl>();
		for (DadosLocalAmostragemImpl dadosLocalAmostragem : dadosLocalAmostragemCalculo) {
			if (dadosLocalAmostragem.getMedidaPCS() == null || dadosLocalAmostragem.getMedidaVolume() == null) {
				dadosParaRemocao.add(dadosLocalAmostragem);
			}
		}
		for (DadosLocalAmostragemImpl dadoParaRemocao : dadosParaRemocao) {
			dadosLocalAmostragemCalculo.remove(dadoParaRemocao);
		}

		Integer qtdePCSNulo = CONSTANTE_NUMERO_ZERO;
		Integer qtdDiasAmostragem = CONSTANTE_NUMERO_ZERO;

		Date dataBaseExtensao =
				this.determinarDataAnteriorLeituraExtensaoPCS(contratoPontoConsumo.getTipoMedicao(), dataLeituraAnterior, dataLeituraAtual);

		Map<Long, Collection<CityGateMedicao>> mapaCityGateMedicao =
				this.consultarCityGateRecentesPelaDataBasePorChaveDeCityGate(listaCityGates, dataBaseExtensao);

		for (CityGate cityGate : listaCityGates) {

			for (ContratoPontoConsumoPCSIntervalo contratoPCSIntervalo : listaPCSIntervaloOrdenada) {

				if (contratoPCSIntervalo.getIntervaloPCS().getChavePrimaria() == CONSTANTE_NUMERO_DOIS) {// Reduzido

					Integer qtdeDiasReducao = contratoPCSIntervalo.getTamanho();

					qtdePCSNulo = CONSTANTE_NUMERO_ZERO;
					qtdDiasAmostragem = CONSTANTE_NUMERO_ZERO;
					// contar os registros que
					// contem o pcs ou o volume
					// nulo
					for (DadosLocalAmostragemImpl dadosLocalAmostragem : dadosLocalAmostragemCalculo) {
						if (cityGate.getChavePrimaria() == dadosLocalAmostragem.getCityGate().getChavePrimaria()) {
							qtdDiasAmostragem++;
						}
					}

					qtdePCSNulo = historicoConsumo.getDiasConsumo() - qtdDiasAmostragem;

					if (qtdeDiasReducao == null) {
						qtdeDiasReducao = CONSTANTE_NUMERO_ZERO;
					}

					if (qtdePCSNulo.compareTo(qtdeDiasReducao) <= CONSTANTE_NUMERO_ZERO) {
						// caso consiga fazer a
						// redução
						this.associarAnomaliaConsumoEstimado(historicoConsumo);
						break;
					} else {
						// caso não consiga fazer
						// a redução
						this.associarAnomaliaConsumoNaoApurado(historicoConsumo);
					}

					intervaloPCSParaCalculo = contratoPCSIntervalo.getIntervaloPCS();
				}

				if (contratoPCSIntervalo.getIntervaloPCS().getChavePrimaria() == CONSTANTE_NUMERO_TRES) {// Estendido
					this.associarAnomaliaConsumoEstimado(historicoConsumo);
					Collection<DadosLocalAmostragemImpl> dadosAmostragemExtensao = null;
					if (EntidadeConteudo.PONTO_CONSUMO.equals(localAmostragemParaCalculo.getDescricao())) {
						Collection<DadosLocalAmostragemImpl> dadosLocalAmostragemRecentes = this.consultarImovelPCSZRecentesPelaDataBase(
								historicoConsumo.getPontoConsumo().getImovel(), dataBaseExtensao, qtdePCSNulo);
						dadosLocalAmostragemCalculo.addAll(dadosLocalAmostragemRecentes);

					} else {
						// busca será realizada
						// considerando todos
						// os
						// locais do citygate,
						// priorizando por
						// proximidade e
						// prioridade de busca
						Collection<DadosLocalAmostragemImpl> dadosLocalAmostragemRecentes = new ArrayList<DadosLocalAmostragemImpl>();
						qtdePCSNulo = CONSTANTE_NUMERO_ZERO;
						qtdDiasAmostragem = CONSTANTE_NUMERO_ZERO;
						// contar os registros que
						// contem o pcs ou o
						// volume nulo
						for (DadosLocalAmostragemImpl dadosLocalAmostragem : dadosLocalAmostragemCalculo) {
							if (cityGate.getChavePrimaria() == dadosLocalAmostragem.getCityGate().getChavePrimaria()) {
								qtdDiasAmostragem++;
							}
						}
						qtdePCSNulo = historicoConsumo.getDiasConsumo() - qtdDiasAmostragem;

						if (qtdePCSNulo > CONSTANTE_NUMERO_ZERO) {
							Collection<CityGateMedicao> dadosAmostragemCityGate =
									this.getCityGateMedicao(mapaCityGateMedicao, cityGate, qtdePCSNulo);

							// realiza a "priorização" dos dados de extensão, baseado na prioridade do
							// contrato
							dadosAmostragemExtensao =
									this.priorizarDadosAmostragemExtensaoPCS(dadosAmostragemCityGate, listaLocalAmostragem);
							this.atribuirLocalAmostragemDadosPCS(localAmostragemParaCalculo, dadosAmostragemExtensao);

							dadosLocalAmostragemRecentes.addAll(dadosAmostragemExtensao);
						}

						dadosLocalAmostragemCalculo.addAll(dadosLocalAmostragemRecentes);
					}

					int qtdePCSNuloAposVerificacao = CONSTANTE_NUMERO_ZERO;
					for (DadosLocalAmostragemImpl dadosLocalAmostragem : dadosLocalAmostragemCalculo) {
						if (dadosLocalAmostragem.getMedidaPCS() == null) {
							qtdePCSNuloAposVerificacao++;
						}
					}

					intervaloPCSParaCalculo = contratoPCSIntervalo.getIntervaloPCS();

					if (qtdePCSNuloAposVerificacao == CONSTANTE_NUMERO_ZERO) {
						// revalidar se os volumes
						// obtidos na extensão
						// estão
						// preenchidos
						this.prepararVolumeParticipacaoParaCalculo(listaLocalAmostragem, mapaPontoConsumoCityGate,
								dadosLocalAmostragemCalculo, contratoPontoConsumo);
						break;
					}
				}
			}
			// verificar se houve alguma anomalia
			// por city gate, que impeça a não
			// apuração do tipo de consumo
			String codigoTipoConsumo =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_NAO_APURADO);
			if (historicoConsumo.getTipoConsumo().getChavePrimaria() == Long.parseLong(codigoTipoConsumo)) {
				break;
			}

		}
		return intervaloPCSParaCalculo;
	}

	private Collection<CityGateMedicao> getCityGateMedicao(Map<Long, Collection<CityGateMedicao>> mapaCityGateMedicao, CityGate cityGate,
			Integer qtdePCSNulo) {
		return mapaCityGateMedicao.getOrDefault(cityGate.getChavePrimaria(), Collections.emptyList()).stream().limit(qtdePCSNulo)
				.collect(Collectors.toList());
	}

	/**
	 * Associar anomalia consumo estimado.
	 *
	 * @param historicoConsumo the historico consumo
	 * @throws NegocioException the negocio exception
	 */
	private void associarAnomaliaConsumoEstimado(HistoricoConsumo historicoConsumo) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		TipoConsumo tipoConsumo = null;
		ServiceLocator.getInstancia().getControladorParametroSistema();
		String codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_ESTIMADO);
		tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
		historicoConsumo.setTipoConsumo(tipoConsumo);
	}

	/**
	 * Metodo responsavel por priorizar os dados obtidos pela extensão.
	 *
	 * @param colecaoDadosExtensaoCityGate - Dados de Medição por CityGate, obtidos na extensão do Intervalo de Dados
	 * @param listaPrioridadeLocalAmostragem - Lista ordenada por Prioridade dos Locais Possíveis de Busca de Dados de PCS
	 * @return the collection
	 */
	private Collection<DadosLocalAmostragemImpl> priorizarDadosAmostragemExtensaoPCS(
			Collection<CityGateMedicao> colecaoDadosExtensaoCityGate,
			Collection<ContratoPontoConsumoPCSAmostragem> listaPrioridadeLocalAmostragem) {

		Collection<DadosLocalAmostragemImpl> colecaoDadosAmostragemParaCalculo = new HashSet<DadosLocalAmostragemImpl>();
		for (CityGateMedicao cityGateMedicao : colecaoDadosExtensaoCityGate) {
			DadosLocalAmostragemImpl dadosPCSDiaExtensao = new DadosLocalAmostragemImpl();
			dadosPCSDiaExtensao.setCityGate(cityGateMedicao.getCityGate());
			dadosPCSDiaExtensao.setData(cityGateMedicao.getDataMedicao());
			dadosPCSDiaExtensao.setMedidaPCS(this.obterNumeroPCSPrioritarioExtensao(cityGateMedicao, listaPrioridadeLocalAmostragem));
			dadosPCSDiaExtensao.setMedidaVolume(this.obterVolumePrioridadeExtensao(cityGateMedicao, listaPrioridadeLocalAmostragem));

			colecaoDadosAmostragemParaCalculo.add(dadosPCSDiaExtensao);
		}
		return colecaoDadosAmostragemParaCalculo;
	}

	/**
	 * Metodo responsável por determinar, no dado amostragem de extensao obtido, a medida de PCS por prioridade.
	 *
	 * @param dadoMedicaoCityGate the dado medicao city gate
	 * @param listaPrioridadeLocalAmostragem the lista prioridade local amostragem
	 * @return Integer [Medida do PCS por prioridade]
	 */
	private Integer obterNumeroPCSPrioritarioExtensao(CityGateMedicao dadoMedicaoCityGate,
			Collection<ContratoPontoConsumoPCSAmostragem> listaPrioridadeLocalAmostragem) {

		Integer pcsPrioritario = null;
		Map<Long, Integer> mapaDadosPCSLocal = this.criarMapaDadosPriorizacaoExtensao(dadoMedicaoCityGate, false);

		// Itera na prioridade de locais para
		// determinar o PCS prioritário da
		// Extensão
		for (ContratoPontoConsumoPCSAmostragem contratoPontoConsumoPCSAmostragem : listaPrioridadeLocalAmostragem) {
			pcsPrioritario = mapaDadosPCSLocal.get(contratoPontoConsumoPCSAmostragem.getLocalAmostragemPCS().getChavePrimaria());
			if (pcsPrioritario != null) {
				break;
			}
		}
		// Há a possibilidade da prioridade não
		// determinar o PCS prioritario,
		// neste caso obtem-se o único disponível
		if (pcsPrioritario == null) {
			if (dadoMedicaoCityGate.getMedidaPCSDistribuidora() != null) {
				pcsPrioritario = dadoMedicaoCityGate.getMedidaPCSDistribuidora();
			} else {
				pcsPrioritario = dadoMedicaoCityGate.getMedidaPCSSupridora();
			}

		}
		return pcsPrioritario;
	}

	/**
	 * Obter volume prioridade extensao.
	 *
	 * @param dadoMedicaoCityGate the dado medicao city gate
	 * @param listaPrioridadeLocalAmostragem the lista prioridade local amostragem
	 * @return the integer
	 */
	private Integer obterVolumePrioridadeExtensao(CityGateMedicao dadoMedicaoCityGate,
			Collection<ContratoPontoConsumoPCSAmostragem> listaPrioridadeLocalAmostragem) {

		Integer volumePrioritario = null;
		Map<Long, Integer> mapaDadosPCSLocal = this.criarMapaDadosPriorizacaoExtensao(dadoMedicaoCityGate, true);

		// Itera na prioridade de locais para
		// determinar o PCS prioritário da
		// Extensão
		for (ContratoPontoConsumoPCSAmostragem contratoPontoConsumoPCSAmostragem : listaPrioridadeLocalAmostragem) {
			volumePrioritario = mapaDadosPCSLocal.get(contratoPontoConsumoPCSAmostragem.getLocalAmostragemPCS().getChavePrimaria());
			if (volumePrioritario != null) {
				break;
			}
		}
		// Há a possibilidade da prioridade não
		// determinar o PCS prioritario,
		// neste caso obtem-se o único disponível
		if (volumePrioritario == null) {
			if (dadoMedicaoCityGate.getMedidaVolumeDistribuidora() != null) {
				volumePrioritario = dadoMedicaoCityGate.getMedidaVolumeDistribuidora();
			} else {
				volumePrioritario = dadoMedicaoCityGate.getMedidaVolumeSupridora();
			}
		}
		return volumePrioritario;
	}

	/**
	 * Metodo que prepara um map com dados por Local de Amostragem para ser utilizado na priorizacao dos dados de extensão.
	 *
	 * @param dadoMedicaoCityGate the dado medicao city gate
	 * @param isDadosVolume [se true, retornará o mapa com os dados de volume, senão fará com dados PCS]
	 * @return the map
	 */
	private Map<Long, Integer> criarMapaDadosPriorizacaoExtensao(CityGateMedicao dadoMedicaoCityGate, boolean isDadosVolume) {

		Map<Long, Integer> mapaDadosPCSLocal = new HashMap<Long, Integer>();
		if (isDadosVolume) {
			if (dadoMedicaoCityGate.getMedidaVolumeDistribuidora() != null) {
				mapaDadosPCSLocal.put(EntidadeConteudo.CHAVE_DISTRIBUIDORA, dadoMedicaoCityGate.getMedidaVolumeDistribuidora());
			}
			if (dadoMedicaoCityGate.getMedidaVolumeSupridora() != null) {
				mapaDadosPCSLocal.put(EntidadeConteudo.CHAVE_SUPRIDORA, dadoMedicaoCityGate.getMedidaVolumeSupridora());
			}
		} else {
			if (dadoMedicaoCityGate.getMedidaPCSDistribuidora() != null) {
				mapaDadosPCSLocal.put(EntidadeConteudo.CHAVE_DISTRIBUIDORA, dadoMedicaoCityGate.getMedidaPCSDistribuidora());
			}
			if (dadoMedicaoCityGate.getMedidaPCSSupridora() != null) {
				mapaDadosPCSLocal.put(EntidadeConteudo.CHAVE_SUPRIDORA, dadoMedicaoCityGate.getMedidaPCSSupridora());
			}
		}
		return mapaDadosPCSLocal;
	}

	/**
	 * Obter intervalo datas leitura.
	 *
	 * @param dataLeituraAnterior the data leitura anterior
	 * @param dataLeituraAtual the data leitura atual
	 * @return the collection
	 */
	private Collection<Date> obterIntervaloDatasLeitura(Date dataLeituraAnterior, Date dataLeituraAtual) {

		Collection<Date> colecaoDatas = new HashSet<Date>();
		colecaoDatas.addAll(Util.gerarIntervaloDatas(dataLeituraAnterior, dataLeituraAtual));

		if (colecaoDatas.isEmpty()) {
			colecaoDatas.add(dataLeituraAtual);
		}

		return colecaoDatas;
	}

	/**
	 * Método responsável por verificar que data base deve ser considerada para extensão de PCS. No caso da Medicao Diaria, a busca deve ser
	 * feita a partir da data de Leitura atual
	 *
	 * @param tipoMedicao the tipo medicao
	 * @param dataLeituraAnterior the data leitura anterior
	 * @param dataLeituraAtual the data leitura atual
	 * @return the date
	 */
	private Date determinarDataAnteriorLeituraExtensaoPCS(TipoMedicao tipoMedicao, Date dataLeituraAnterior, Date dataLeituraAtual) {

		Date dataBase = dataLeituraAnterior;
		if (dataBase.after(dataLeituraAtual)) {
			dataBase = dataLeituraAtual;
		}
		if (tipoMedicao.getChavePrimaria() == TipoMedicao.CODIGO_DIARIA) {
			dataBase = dataLeituraAtual;
		}
		return dataBase;
	}

	/**
	 * Método utilizado na extensão de PCS , onde é necessário atribuir o local de amostragem aos dados de extensão obtidos.
	 *
	 * @param localAmostragem the local amostragem
	 * @param listaDadosAmostragemPCS the lista dados amostragem pcs
	 */
	private void atribuirLocalAmostragemDadosPCS(EntidadeConteudo localAmostragem,
			Collection<DadosLocalAmostragemImpl> listaDadosAmostragemPCS) {

		if (listaDadosAmostragemPCS != null) {
			for (DadosLocalAmostragemImpl dadosLocalAmostragemImpl : listaDadosAmostragemPCS) {
				dadosLocalAmostragemImpl.setLocalAmostragem(localAmostragem);
			}
		}
	}

	/**
	 * Método responsável por montar os dados para o cálculo do PCS.
	 *
	 * @param listaDadosLocal the lista dados local
	 * @param datasDoIntervaloLeitura the datas do intervalo leitura
	 * @param datasDadosLocalAmostragem the datas dados local amostragem
	 * @param dadosLocalAmostragemCalculo the dados local amostragem calculo
	 * @param localAmostragemParaCalculo the local amostragem para calculo
	 * @param listaCityGates the lista city gates
	 * @param localAmostragem the local amostragem
	 * @param mapaDatasDadosLocalAmostragem - Mapa com dados já obtidos por Data e Local
	 *
	 * @return the entidade conteudo
	 * @throws GGASException 
	 */
	private EntidadeConteudo montarDadosParaCalculo(Collection<DadosLocalAmostragemImpl> listaDadosLocal,
			Collection<Date> datasDoIntervaloLeitura, Collection<Date> datasDadosLocalAmostragem,
			Collection<DadosLocalAmostragemImpl> dadosLocalAmostragemCalculo, EntidadeConteudo localAmostragemParaCalculo,
			Collection<CityGate> listaCityGates, ContratoPontoConsumoPCSAmostragem localAmostragem,
			Map<Date, Collection<DadosLocalAmostragemImpl>> mapaDatasDadosLocalAmostragem) throws GGASException {

		//

		// by
		// gmatos
		// on
		// 15/10/09
		// 10:22
		EntidadeConteudo localAmostragemRetorno = null;
		 
		ContratoPontoConsumo tempContratoPontoConsumo =
				Fachada.getInstancia().obterContratoPontoConsumo(localAmostragem.getContratoPontoConsumo().getChavePrimaria());
		localAmostragem.setContratoPontoConsumo(tempContratoPontoConsumo);
		
		this.carregarMapaDadosLocalAmostragem(localAmostragem.getLocalAmostragemPCS(), listaDadosLocal, mapaDatasDadosLocalAmostragem,
				listaCityGates, localAmostragem.getContratoPontoConsumo().getTipoMedicao());

		for (Map.Entry<Date, Collection<DadosLocalAmostragemImpl>> entry : mapaDatasDadosLocalAmostragem.entrySet()) {
			Date data = entry.getKey();
			if (mapaDatasDadosLocalAmostragem.containsKey(data)) {

				Collection<DadosLocalAmostragemImpl> listaDadosMapa = mapaDatasDadosLocalAmostragem.get(data);
				dadosLocalAmostragemCalculo.addAll(listaDadosMapa);

				if (listaDadosMapa.size() == listaCityGates.size() && datasDadosLocalAmostragem != null) {
					datasDadosLocalAmostragem.add(data);

					// Armazena o Local para o
					// Cálculo do Fator
					if (localAmostragemParaCalculo == null) {

						localAmostragemRetorno = localAmostragem.getLocalAmostragemPCS();

					} else {

						localAmostragemRetorno = localAmostragemParaCalculo;

					}

					for (Date dataDadosLocalAmostragem : datasDadosLocalAmostragem) {

						datasDoIntervaloLeitura.remove(dataDadosLocalAmostragem);

					}

				}
			}
		}
		return localAmostragemRetorno;
	}

	/**
	 * Método responsável por montar os dados para o cálculo pelo Ponto de Consumo.
	 *
	 * @param listaDadosLocalAmostragem the lista dados local amostragem
	 * @param datasDoIntervaloLeitura the datas do intervalo leitura
	 * @param datasDadosLocalAmostragem the datas dados local amostragem
	 * @param dadosLocalAmostragemCalculo the dados local amostragem calculo
	 * @param localAmostragemParaCalculo the local amostragem para calculo
	 * @param localAmostragem the local amostragem
	 * @return the entidade conteudo
	 */
	private EntidadeConteudo montarDadosParaCalculoPorPontoConsumo(Collection<DadosLocalAmostragemImpl> listaDadosLocalAmostragem,
			Collection<Date> datasDoIntervaloLeitura, Collection<Date> datasDadosLocalAmostragem,
			Collection<DadosLocalAmostragemImpl> dadosLocalAmostragemCalculo, EntidadeConteudo localAmostragemParaCalculo,
			ContratoPontoConsumoPCSAmostragem localAmostragem) {

		EntidadeConteudo localAmostragemRetorno = null;
		for (DadosLocalAmostragemImpl dadosLocalAmostragem : listaDadosLocalAmostragem) {

			dadosLocalAmostragem.setLocalAmostragem(localAmostragem.getLocalAmostragemPCS());
			if (dadosLocalAmostragem.getMedidaPCS() != null) {
				datasDadosLocalAmostragem.add(dadosLocalAmostragem.getData());
				dadosLocalAmostragemCalculo.add(dadosLocalAmostragem);
			}
		}

		if (!datasDadosLocalAmostragem.isEmpty()) {
			// Armazena o Local para o Cálculo do
			// Fator
			if (localAmostragemParaCalculo == null) {
				localAmostragemRetorno = localAmostragem.getLocalAmostragemPCS();
			} else {
				localAmostragemRetorno = localAmostragemParaCalculo;
			}
		}

		for (Date dataLocalAmostragem : datasDadosLocalAmostragem) {
			datasDoIntervaloLeitura.remove(dataLocalAmostragem);
		}

		return localAmostragemRetorno;
	}

	/**
	 * Método responsável por montar um mapa com listas de dados local amostragem para cada data.
	 *
	 * @param localAmostragemPCS the local amostragem pcs
	 * @param listaDadosPCSLocalAmostragem the lista dados pcs local amostragem
	 * @param mapaDatasDadosLocalAmostragem [obrigatorio] - Mapa com dados já obtidos por Data e Local
	 * @param listaCityGates the lista city gates
	 * @param tipoMedicao
	 *
	 */
	private void carregarMapaDadosLocalAmostragem(EntidadeConteudo localAmostragemPCS,
			Collection<DadosLocalAmostragemImpl> listaDadosPCSLocalAmostragem,
			Map<Date, Collection<DadosLocalAmostragemImpl>> mapaDatasDadosLocalAmostragem, Collection<CityGate> listaCityGates,
			TipoMedicao tipoMedicao) {

		for (CityGate cityGate : listaCityGates) {
			for (DadosLocalAmostragemImpl dadosLocalAmostragem : listaDadosPCSLocalAmostragem) {
				dadosLocalAmostragem.setLocalAmostragem(localAmostragemPCS);
				if (mapaDatasDadosLocalAmostragem.containsKey(dadosLocalAmostragem.getData())
						&& dadosLocalAmostragem.getCityGate().getChavePrimaria() == cityGate.getChavePrimaria()) {
					Collection<DadosLocalAmostragemImpl> listaDadosMapa = mapaDatasDadosLocalAmostragem.get(dadosLocalAmostragem.getData());
					if (!listaDadosMapa.contains(dadosLocalAmostragem)) {
						listaDadosMapa.add(dadosLocalAmostragem);
					}
				} else {
					Collection<DadosLocalAmostragemImpl> novaListaDados = new ArrayList<DadosLocalAmostragemImpl>();
					if (listaCityGates.size() == CONSTANTE_NUMERO_UM && tipoMedicao != null
							&& tipoMedicao.getChavePrimaria() == TipoMedicao.CODIGO_DIARIA) {
						if (dadosLocalAmostragem.getMedidaPCS() != null
								&& dadosLocalAmostragem.getCityGate().getChavePrimaria() == cityGate.getChavePrimaria()) {
							novaListaDados.add(dadosLocalAmostragem);
							mapaDatasDadosLocalAmostragem.put(dadosLocalAmostragem.getData(), novaListaDados);
						}
					} else {
						if (dadosLocalAmostragem.getMedidaPCS() != null && dadosLocalAmostragem.getMedidaVolume() != null
								&& dadosLocalAmostragem.getCityGate().getChavePrimaria() == cityGate.getChavePrimaria()) {
							novaListaDados.add(dadosLocalAmostragem);
							mapaDatasDadosLocalAmostragem.put(dadosLocalAmostragem.getData(), novaListaDados);
						}
					}
				}
			}
		}
	}

	/**
	 * Associar anomalia consumo nao apurado.
	 *
	 * @param historicoConsumo the historico consumo
	 * @throws NegocioException the negocio exception
	 */
	private void associarAnomaliaConsumoNaoApurado(HistoricoConsumo historicoConsumo) throws GGASException {

		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_NAO_APURADO);
		TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
		historicoConsumo.setTipoConsumo(tipoConsumo);

		String codigoAnormalidadeConsumo =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_CICLO_INCOMPLETO);
		AnormalidadeConsumo anormalidadeConsumo =
				controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumo));
		aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo);
	}

	/**
	 * Método responsável por garantir que o local de Amostragem para cálculo seja definido. Será considerado o primeiro (em prioridade)
	 * local disponível do contrato do ponto de consumo.
	 *
	 * @param locaisAmostragemPontoConsumo [obrigatorio] - assume-se que a lista é ordenada por prioridade
	 * @return the entidade conteudo
	 * @throws NegocioException the negocio exception
	 * @throws NullPointerException se locaisAmostragemPontoConsumo for nula
	 */
	private EntidadeConteudo obterLocalAmostragemPrioritarioCalculoPCS(
			Collection<ContratoPontoConsumoPCSAmostragem> locaisAmostragemPontoConsumo) throws NegocioException {

		EntidadeConteudo localAmostragemPCS = null;

		if (locaisAmostragemPontoConsumo != null && !locaisAmostragemPontoConsumo.isEmpty()) {
			localAmostragemPCS = locaisAmostragemPontoConsumo.iterator().next().getLocalAmostragemPCS();
		}

		if (localAmostragemPCS == null) {
			throw new NegocioException(ERRO_NEGOCIO_CONTARTO_SEM_LOCAL_AMOSTRAGERM_OU_FATOR_UNICO, true);
		}

		return localAmostragemPCS;
	}

	/**
	 * Método responsável por determinar o fator e medida de PCS a ser utilizada na apuração de um consumo.
	 *
	 * @param historicoConsumo the historico consumo
	 * @param listaCityGates the lista city gates
	 * @param listaDadosLocalAmostragem the lista dados local amostragem
	 * @param localAmostragem the local amostragem
	 * @param intervaloPCS the intervalo pcs
	 * @param tipoMedicao [tipo de Medição do Ponto de Consumo]
	 * @return DadosPCSImpl - VO com dados obtidos no cálculo do PCS
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	private DadosPCSImpl preecherDadosPCS(HistoricoConsumo historicoConsumo, Collection<CityGate> listaCityGates,
			Collection<DadosLocalAmostragemImpl> listaDadosLocalAmostragem, EntidadeConteudo localAmostragem, IntervaloPCS intervaloPCS,
			TipoMedicao tipoMedicao) throws NegocioException, FormatoInvalidoException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		BigDecimal fatorPCS = BigDecimal.ZERO;
		BigDecimal consumoApurado = BigDecimal.ZERO;
		DadosPCSImpl dadosPCS = new DadosPCSImpl();

		ServiceLocator.getInstancia().getControladorParametroSistema();

		String codigoTipoConsumoNaoApurado =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_NAO_APURADO);

		String arredondamentoPCS =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_ARREDONDAMENTO_FATOR_PCS);

		if (historicoConsumo.getTipoConsumo().getChavePrimaria() != Long.parseLong(codigoTipoConsumoNaoApurado)) {
			fatorPCS = this.calcularFatorPCS(listaDadosLocalAmostragem, listaCityGates, localAmostragem, dadosPCS, tipoMedicao);
			fatorPCS = fatorPCS.divide(BigDecimal.ONE, Integer.parseInt(arredondamentoPCS), RoundingMode.HALF_UP);
		}

		// Fator PCS
		dadosPCS.setFatorPCS(fatorPCS);
		// Consumo Apurado
		consumoApurado = fatorPCS.multiply(historicoConsumo.getConsumoMedido());
		dadosPCS.setConsumoApurado(consumoApurado);
		dadosPCS.setLocalAmostragemPCS(localAmostragem);
		dadosPCS.setIntervaloPCS(intervaloPCS);
		return dadosPCS;
	}

	/**
	 * Método responsável pelo cálculo do Fator PCS.
	 *
	 * @param listaDadosLocalAmostragem the lista dados local amostragem
	 * @param listaCityGates the lista city gates
	 * @param localAmostragem the local amostragem
	 * @param dadosPCS the dados pcs
	 * @param tipoMedicao [tipo de Medição do Ponto de Consumo]
	 * @return Fator PCS calculado
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	private BigDecimal calcularFatorPCS(Collection<DadosLocalAmostragemImpl> listaDadosLocalAmostragem, Collection<CityGate> listaCityGates,
			EntidadeConteudo localAmostragem, DadosPCSImpl dadosPCS, TipoMedicao tipoMedicao)
			throws NegocioException, FormatoInvalidoException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		String arredondamentoPCS =
				(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_ARREDONDAMENTO_FATOR_PCS);

		// Calculo da medida PCS
		BigDecimal medidaPCS = this.calcularMedidaPCS(listaDadosLocalAmostragem, listaCityGates, tipoMedicao, localAmostragem);
		dadosPCS.setMedidaPCS(medidaPCS);

		BigDecimal pcsReferencia = this.obterPCSReferencia();
		BigDecimal fatorPCS = medidaPCS.divide(pcsReferencia, Integer.parseInt(arredondamentoPCS), RoundingMode.HALF_UP);

		LogAnaliseLeituraConsumoTemp.getInstance().addInfo("   - PCS Referencia ..:" + pcsReferencia);
		LogAnaliseLeituraConsumoTemp.getInstance().addInfo("   - Fator PCS ..:" + fatorPCS);

		return fatorPCS;
	}

	/**
	 * Método responsável por calcular a medida PCS.
	 *
	 * @param listaDadosLocalAmostragem the lista dados local amostragem
	 * @param listaCityGates the lista city gates
	 * @param tipoMedicao the tipo medicao
	 * @param localAmostragem the local amostragem
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 */
	private BigDecimal calcularMedidaPCS(Collection<DadosLocalAmostragemImpl> listaDadosLocalAmostragem,
			Collection<CityGate> listaCityGates, TipoMedicao tipoMedicao, EntidadeConteudo localAmostragem) throws NegocioException {

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		BigDecimal medidaPCS = BigDecimal.ZERO;
		BigDecimal pcsAcumulado = BigDecimal.ZERO;
		BigDecimal volumeAcumulado = BigDecimal.ZERO;
		BigDecimal volumeProporcional = BigDecimal.ZERO;
		Integer qtdePCS = CONSTANTE_NUMERO_ZERO;

		boolean isDiariaComCityGateUnico = (tipoMedicao.getChavePrimaria() == TipoMedicao.CODIGO_DIARIA)
				&& (listaCityGates.size() == CONSTANTE_NUMERO_UM);
		// Diários
		// com
		// 1
		// citygate
		boolean isPontoConsumo = localAmostragem.getChavePrimaria() == CONSTANTE_NUMERO_UM;
		// Referente
		// a
		// Ponto
		// de
		// Consumo
		boolean isFixoComCityGateUnico = (localAmostragem.getChavePrimaria() == CONSTANTE_NUMERO_QUATRO)
				&& (listaCityGates.size() == CONSTANTE_NUMERO_UM);
		// Fixo
		// com
		// 1
		// citygate

		if (isPontoConsumo) {
			for (DadosLocalAmostragemImpl dadosLocalAmostragem : listaDadosLocalAmostragem) {
				if (dadosLocalAmostragem.getCityGate() == null) {
					pcsAcumulado = pcsAcumulado.add(BigDecimal.valueOf(dadosLocalAmostragem.getMedidaPCS()));
					qtdePCS++;
				}
			}
			if (qtdePCS > CONSTANTE_NUMERO_ZERO) {
				medidaPCS = pcsAcumulado.divide(BigDecimal.valueOf(qtdePCS), 12, RoundingMode.HALF_UP);
			}
		} else if (isDiariaComCityGateUnico || isFixoComCityGateUnico) {
			for (DadosLocalAmostragemImpl dadosLocalAmostragem : listaDadosLocalAmostragem) {
				pcsAcumulado = pcsAcumulado.add(BigDecimal.valueOf(dadosLocalAmostragem.getMedidaPCS()));

				qtdePCS++;
			}
			if (qtdePCS > CONSTANTE_NUMERO_ZERO) {
				medidaPCS = pcsAcumulado.divide(BigDecimal.valueOf(qtdePCS), 12, RoundingMode.HALF_UP);
			}

		} else {

			LogAnaliseLeituraConsumoTemp.getInstance()
					.addInfo("  " + "CITY GATE; DATA DE AMOSTRAGEM; MEDIDA PCS; MEDIDA VOLUME; PARTICIPAÇÃO;"
							+ " Medida PCS * (Medida Volume * Participação); Medida Volume * Participação");

			List<DadosLocalAmostragemImpl> lista = new ArrayList<DadosLocalAmostragemImpl>();

			for (DadosLocalAmostragemImpl dadosLocalAmostragem : listaDadosLocalAmostragem) {

				lista.add(dadosLocalAmostragem);

			}

			Collections.sort(lista, new Comparator<DadosLocalAmostragemImpl>() {

				@Override
				public int compare(DadosLocalAmostragemImpl dadosLocalAmostragem1, DadosLocalAmostragemImpl dadosLocalAmostragem2) {

					if (dadosLocalAmostragem1.getData() != null && dadosLocalAmostragem2.getData() != null) {
						return dadosLocalAmostragem1.getData().compareTo(dadosLocalAmostragem2.getData());
					} else if (dadosLocalAmostragem1.getData() != null) {
						return 1;
					} else {
						return -1;
					}
				}
			});

			for (DadosLocalAmostragemImpl dadosLocalAmostragem : lista) {

				if (dadosLocalAmostragem.getMedidaVolume() == null) {
					throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_VOLUME_NAO_INFORMADO,
							dadosLocalAmostragem.getData());
				}
				BigDecimal resultadoMulti = BigDecimal.valueOf(dadosLocalAmostragem.getMedidaPCS())
						.multiply(BigDecimal.valueOf(dadosLocalAmostragem.getMedidaVolume()))
						.multiply(dadosLocalAmostragem.getParticipacao());

				pcsAcumulado = pcsAcumulado.add(resultadoMulti);

				volumeProporcional =
						BigDecimal.valueOf(dadosLocalAmostragem.getMedidaVolume()).multiply(dadosLocalAmostragem.getParticipacao());

				volumeAcumulado = volumeAcumulado
						.add(BigDecimal.valueOf(dadosLocalAmostragem.getMedidaVolume()).multiply(dadosLocalAmostragem.getParticipacao()));

				LogAnaliseLeituraConsumoTemp.getInstance()
						.addInfo("  " + dadosLocalAmostragem.getCityGate().getDescricao() + ";" + df.format(dadosLocalAmostragem.getData())
								+ ";" + dadosLocalAmostragem.getMedidaPCS() + ";" + dadosLocalAmostragem.getMedidaVolume() + ";"
								+ dadosLocalAmostragem.getParticipacao() + ";" + resultadoMulti + ";" + volumeProporcional);
			}

			if (volumeAcumulado.compareTo(BigDecimal.ZERO) > CONSTANTE_NUMERO_ZERO) {
				medidaPCS = pcsAcumulado.divide(volumeAcumulado, 12, RoundingMode.HALF_UP);
			}

		}

		LogAnaliseLeituraConsumoTemp.getInstance().addInfo("  ##Volume Acumulado..:" + volumeAcumulado);
		LogAnaliseLeituraConsumoTemp.getInstance().addInfo("  ##PCS Acumulado..:" + pcsAcumulado);
		LogAnaliseLeituraConsumoTemp.getInstance().addInfo("  ##Medida PCS ( PCS Acumulado /  Volume Acumulado )..:" + medidaPCS);

		return medidaPCS;
	}

	/**
	 * Preparar volume participacao para calculo.
	 *
	 * @param listaDadosLocalAmostragem the lista dados local amostragem
	 * @param contratoPontoConsumo the contrato ponto consumo
	 */
	private void prepararVolumeParticipacaoParaCalculo(Collection<ContratoPontoConsumoPCSAmostragem> listaLocalAmostragem,
			Map<PontoConsumo, Map<CityGate, List<PontoConsumoCityGate>>> mapaPontoConsumoCityGate,
			Collection<DadosLocalAmostragemImpl> listaDadosLocalAmostragem, ContratoPontoConsumo contratoPontoConsumo) {

		List<ContratoPontoConsumoPCSAmostragem> listaContratoPontoConsumoPCSAmostragem = new ArrayList<ContratoPontoConsumoPCSAmostragem>();

		// Transformar um o collection para list
		// para poder usar metodo de ordenação
		// "sort"
		for (ContratoPontoConsumoPCSAmostragem pcs : listaLocalAmostragem) {
			listaContratoPontoConsumoPCSAmostragem.add(pcs);
		}

		Collections.sort(listaContratoPontoConsumoPCSAmostragem, new Comparator<ContratoPontoConsumoPCSAmostragem>() {

			@Override
			public int compare(ContratoPontoConsumoPCSAmostragem c1, ContratoPontoConsumoPCSAmostragem c2) {

				return c1.getPrioridade().compareTo(c2.getPrioridade());
			}
		});

		for (DadosLocalAmostragemImpl dadosLocal : listaDadosLocalAmostragem) {
			Long chaveLocalAmostragem = dadosLocal.getLocalAmostragem().getChavePrimaria();
			if (chaveLocalAmostragem != 1 && dadosLocal.getMedidaVolume() == null
					&& listaContratoPontoConsumoPCSAmostragem.size() > CONSTANTE_NUMERO_UM) {
				String atributoSelect = "";
				// verificar prioridade para
				// atribuição do PCS
				for (ContratoPontoConsumoPCSAmostragem pcs : listaContratoPontoConsumoPCSAmostragem) {
					if (pcs.getPrioridade() != CONSTANTE_NUMERO_ZERO) {
						// se a prioridade for
						// distribuidora
						if (pcs.getLocalAmostragemPCS().getChavePrimaria() == CONSTANTE_NUMERO_DOIS) {
							atributoSelect = "cityGateMedicao.medidaVolumeDistribuidora";
							// se a prioridade
							// for supridora
							// ou citygate
							// fixo deverá
							// utilizar o
							// volume da
							// supridora
						} else {
							atributoSelect = "cityGateMedicao.medidaVolumeSupridora";

						}

						DadosLocalAmostragemImpl dadoVolume =
								this.consultarVolumeCityGateMedicaoDiario(dadosLocal.getCityGate(), dadosLocal.getData(), atributoSelect);
						if ((dadoVolume != null) && (dadoVolume.getMedidaVolume() != null)) {
							dadosLocal.setMedidaVolume(dadoVolume.getMedidaVolume());
							break;
						}
					}
				}
			}
			PontoConsumoCityGate pontoConsumoCityGate = null;
			PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();
			if (dadosLocal.getCityGate() != null) {
				pontoConsumoCityGate = obterPontoConsumoCityGateDeDadosLocalAmostragem(mapaPontoConsumoCityGate, dadosLocal, pontoConsumo);

			}
			if (pontoConsumoCityGate != null && pontoConsumoCityGate.getPercentualParticipacao() != null) {
				dadosLocal.setParticipacao(pontoConsumoCityGate.getPercentualParticipacao());
			} else {
				dadosLocal.setParticipacao(BigDecimal.ONE);
			}
		}
	}

	/**
	 * Escolhe um PontoConsumoCityGate, do city gate dos dados de local amotragem e ponto de consumo passado por parâmetro, e com data de
	 * vigência menor ou igual que a data dos dados de local amostragem. Caso não exista um PontoConsumoCityGate, realiza a consulta.
	 *
	 * @param mapaPontoConsumoCityGate
	 * @param dadosLocal
	 * @param pontoConsumo
	 * @return
	 */
	private PontoConsumoCityGate obterPontoConsumoCityGateDeDadosLocalAmostragem(
			Map<PontoConsumo, Map<CityGate, List<PontoConsumoCityGate>>> mapaPontoConsumoCityGate, DadosLocalAmostragemImpl dadosLocal,
			PontoConsumo pontoConsumo) {

		PontoConsumoCityGate pontoConsumoCityGate = null;
		CityGate cityGate = dadosLocal.getCityGate();
		if (mapaPontoConsumoCityGate != null && mapaPontoConsumoCityGate.containsKey(pontoConsumo)) {
			pontoConsumoCityGate = escolherPontoConsumoCityGateComDataVigenciaMenorOuIgualQueDadosLocalAmostragem(mapaPontoConsumoCityGate,
					dadosLocal, pontoConsumo);
		} else {
			pontoConsumoCityGate =
					this.obterPontoConsumoCityGate(cityGate.getChavePrimaria(), pontoConsumo.getChavePrimaria(), dadosLocal.getData());
		}
		return pontoConsumoCityGate;
	}

	/**
	 * Escolhe um PontoConsumoCityGate, do city gate dos dados de local amotragem e ponto de consumo passado por parâmetro, e com data de
	 * vigência menor ou igual que a data dos dados de local amostragem.
	 *
	 * @param mapaPontoConsumoCityGate
	 * @param dadosLocal
	 * @param pontoConsumo
	 * @return
	 */
	private PontoConsumoCityGate escolherPontoConsumoCityGateComDataVigenciaMenorOuIgualQueDadosLocalAmostragem(
			Map<PontoConsumo, Map<CityGate, List<PontoConsumoCityGate>>> mapaPontoConsumoCityGate, DadosLocalAmostragemImpl dadosLocal,
			PontoConsumo pontoConsumo) {

		PontoConsumoCityGate pontoConsumoCityGate = null;
		CityGate cityGate = dadosLocal.getCityGate();
		Map<CityGate, List<PontoConsumoCityGate>> mapaCityGate = mapaPontoConsumoCityGate.get(pontoConsumo);
		if (mapaCityGate != null && mapaCityGate.containsKey(cityGate)) {
			List<PontoConsumoCityGate> listaPontoConsumoCityGate = mapaCityGate.get(cityGate);
			for (PontoConsumoCityGate pccg : listaPontoConsumoCityGate) {
				Date dataVigencia = pccg.getDataVigencia();
				if (dataVigencia != null && (dataVigencia.before(dadosLocal.getData()) || dataVigencia.equals(dadosLocal.getData()))) {
					pontoConsumoCityGate = pccg;
					break;
				}
			}
		}
		return pontoConsumoCityGate;
	}

	/**
	 * Obter pcs referencia.
	 *
	 * @return the big decimal
	 * @throws NegocioException the negocio exception
	 * @throws FormatoInvalidoException the formato invalido exception
	 */
	private BigDecimal obterPCSReferencia() throws NegocioException, FormatoInvalidoException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		String pcsReferencia = (String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_PCS_REFERENCIA);

		return Util.converterCampoStringParaValorBigDecimal("Valor PCS", pcsReferencia, Constantes.FORMATO_VALOR_NUMERO,
				Constantes.LOCALE_PADRAO);
	}

	/**
	 * Consultar contrato ponto consumo pcs amostragem.
	 *
	 * @param chaveContratoPontoConsumo the chave contrato ponto consumo
	 * @return the collection
	 */
	private Collection<ContratoPontoConsumoPCSAmostragem> consultarContratoPontoConsumoPCSAmostragem(Long chaveContratoPontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseContratoPontoConsumoPCSAmostragem().getSimpleName());
		hql.append(" contrato WHERE ");
		hql.append(" contrato.contratoPontoConsumo.chavePrimaria = :CHAVE_CONTRATO_PONTO_CONSUMO ");
		hql.append(" ORDER BY contrato.prioridade ASC ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_CONTRATO_PONTO_CONSUMO", chaveContratoPontoConsumo);

		return query.list();
	}

	/**
	 * Obtém uma coleção de ContratoPontoConsumoPCSAmostratem por chaves de ContratoPontoConsumo.
	 * 
	 * @param chavesContratoPontoConsumo
	 * @return mapa de ContratoPontoConsumoPCSAmostragem por chave primária de ContratoPontoConsumo
	 */
	private Map<Long, Collection<ContratoPontoConsumoPCSAmostragem>> consultarContratoPontoConsumoPCSAmostragem(
			Long[] chavesContratoPontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select contratoPontoConsumoPCSAmostragem.contratoPontoConsumo.chavePrimaria, ");
		hql.append(" contratoPontoConsumoPCSAmostragem ");
		hql.append(" FROM ");
		hql.append(getClasseContratoPontoConsumoPCSAmostragem().getSimpleName());
		hql.append(" contratoPontoConsumoPCSAmostragem WHERE ");
		Map<String, List<Long>> mapaPropriedades = HibernateHqlUtil.adicionarClausulaIn(hql,
				"contratoPontoConsumoPCSAmostragem.contratoPontoConsumo.chavePrimaria", "PT_CONS",
				Arrays.asList(chavesContratoPontoConsumo));
		hql.append(" ORDER BY contratoPontoConsumoPCSAmostragem.prioridade ASC ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

		return this.construirMapaContratoPontoConsumoPCSAmostragem(query.list());
	}

	/**
	 * Contrói um mapa de ContratoPontoConsumoPCSAmostragem por chave primária de ContratoPontoConsumo a partir da lista de atributos
	 * passada por parâmetro.
	 * 
	 * @param listaAtributos
	 * @return mapa de ContratoPontoConsumoPCSAmostragem por chave primária de ContratoPontoConsumo
	 */
	private Map<Long, Collection<ContratoPontoConsumoPCSAmostragem>> construirMapaContratoPontoConsumoPCSAmostragem(
			List<Object[]> listaAtributos) {

		Map<Long, Collection<ContratoPontoConsumoPCSAmostragem>> mapaContratoPontoConsumoPCSAmostragem = new HashMap<>();
		Collection<ContratoPontoConsumoPCSAmostragem> listaContratoPontoConsumoPCSAmostragem = null;
		for (Object[] atributos : listaAtributos) {
			Long chaveContratoPontoConsumo = (Long) atributos[CONSTANTE_NUMERO_ZERO];
			ContratoPontoConsumoPCSAmostragem consumoPCSAmostragem = (ContratoPontoConsumoPCSAmostragem) atributos[CONSTANTE_NUMERO_UM];
			if (!mapaContratoPontoConsumoPCSAmostragem.containsKey(chaveContratoPontoConsumo)) {
				listaContratoPontoConsumoPCSAmostragem = new ArrayList<>();
				mapaContratoPontoConsumoPCSAmostragem.put(chaveContratoPontoConsumo, listaContratoPontoConsumoPCSAmostragem);
			} else {
				listaContratoPontoConsumoPCSAmostragem = mapaContratoPontoConsumoPCSAmostragem.get(chaveContratoPontoConsumo);
			}
			listaContratoPontoConsumoPCSAmostragem.add(consumoPCSAmostragem);
		}
		return mapaContratoPontoConsumoPCSAmostragem;
	}

	/**
	 * Consultar contrato ponto consumo pcs intervalo.
	 *
	 * @param chaveContratoPontoConsumo the chave contrato ponto consumo
	 * @return the collection
	 */
	private Collection<ContratoPontoConsumoPCSIntervalo> consultarContratoPontoConsumoPCSIntervalo(Long chaveContratoPontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseContratoPontoConsumoPCSIntervalo().getSimpleName());
		hql.append(" contrato WHERE ");
		hql.append(" contrato.contratoPontoConsumo.chavePrimaria = :CHAVE_CONTRATO_PONTO_CONSUMO ");
		hql.append(" ORDER BY contrato.prioridade ASC ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_CONTRATO_PONTO_CONSUMO", chaveContratoPontoConsumo);
		return query.list();
	}

	/**
	 * Obtém uma coleção de ContratoPontoConsumoPCSIntervalo por chaves de ContratoPontoConsumo.
	 * 
	 * @param chavesContratoPontoConsumo
	 * @return mapa de ContratoPontoConsumoPCSIntervalo por chave primária de ContratoPontoConsumo
	 */
	private Map<Long, Collection<ContratoPontoConsumoPCSIntervalo>> consultarContratoPontoConsumoPCSIntervalo(
			Long[] chavesContratoPontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select contratoPontoConsumoPCSIntervalo.contratoPontoConsumo.chavePrimaria, ");
		hql.append(" contratoPontoConsumoPCSIntervalo ");
		hql.append(" FROM ");
		hql.append(getClasseContratoPontoConsumoPCSIntervalo().getSimpleName());
		hql.append(" contratoPontoConsumoPCSIntervalo WHERE ");
		Map<String, List<Long>> mapaPropriedades = HibernateHqlUtil.adicionarClausulaIn(hql,
				"contratoPontoConsumoPCSIntervalo.contratoPontoConsumo.chavePrimaria", "PT_CONS",
				Arrays.asList(chavesContratoPontoConsumo));
		hql.append(" ORDER BY contratoPontoConsumoPCSIntervalo.prioridade ASC ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		return this.construirMapaContratoPontoConsumoPCSIntervalo(query.list());
	}

	/**
	 * Contrói um mapa de ContratoPontoConsumoPCSIntervalo por chave primária de ContratoPontoConsumo a partir da lista de atributos passada
	 * por parâmetro.
	 * 
	 * @param listaAtributos
	 * @return mapa de ContratoPontoConsumoPCSIntervalo por chave primária de ContratoPontoConsumo
	 */
	private Map<Long, Collection<ContratoPontoConsumoPCSIntervalo>> construirMapaContratoPontoConsumoPCSIntervalo(
			List<Object[]> listaAtributos) {

		Map<Long, Collection<ContratoPontoConsumoPCSIntervalo>> mapaContratoPontoConsumoPCSIntervalo = new HashMap<>();
		Collection<ContratoPontoConsumoPCSIntervalo> listaContratoPontoConsumoPCSIntervalo = null;
		for (Object[] atributos : listaAtributos) {
			Long chaveContratoPontoConsumo = (Long) atributos[CONSTANTE_NUMERO_ZERO];
			ContratoPontoConsumoPCSIntervalo consumoPCSIntervalo = (ContratoPontoConsumoPCSIntervalo) atributos[CONSTANTE_NUMERO_UM];
			if (!mapaContratoPontoConsumoPCSIntervalo.containsKey(chaveContratoPontoConsumo)) {
				listaContratoPontoConsumoPCSIntervalo = new ArrayList<>();
				mapaContratoPontoConsumoPCSIntervalo.put(chaveContratoPontoConsumo, listaContratoPontoConsumoPCSIntervalo);
			} else {
				listaContratoPontoConsumoPCSIntervalo = mapaContratoPontoConsumoPCSIntervalo.get(chaveContratoPontoConsumo);
			}
			listaContratoPontoConsumoPCSIntervalo.add(consumoPCSIntervalo);
		}
		return mapaContratoPontoConsumoPCSIntervalo;
	}

	/**
	 * Consultar volume city gate medicao diario.
	 *
	 * @param cityGate the city gate
	 * @param dataMedicao the data medicao
	 * @param atributoSelect the atributo select
	 * @return the dados local amostragem impl
	 */
	private DadosLocalAmostragemImpl consultarVolumeCityGateMedicaoDiario(CityGate cityGate, Date dataMedicao, String atributoSelect) {

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT new br.com.ggas.medicao.consumo.impl.DadosLocalAmostragemImpl( ");
		hql.append(atributoSelect);
		hql.append(" ) ");
		hql.append(" FROM ");
		hql.append(getClasseCityGateMedicao().getSimpleName());
		hql.append(" cityGateMedicao WHERE ");
		hql.append(" cityGateMedicao.cityGate.chavePrimaria = :CHAVE_CITY_GATE ");
		hql.append(" AND cityGateMedicao.dataMedicao = :DATA_MEDICAO ");
		hql.append(" AND cityGateMedicao.habilitado = :HABILITADO ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_CITY_GATE", cityGate.getChavePrimaria());
		query.setParameter("DATA_MEDICAO", dataMedicao);
		query.setParameter("HABILITADO", Boolean.TRUE);

		return (DadosLocalAmostragemImpl) query.uniqueResult();
	}

	/**
	 * Consultar city gate medicao.
	 *
	 * @param listaCityGates the lista city gates
	 * @param datasPeriodoLeitura the datas periodo leitura
	 * @param dataLeituraAtual the data leitura atual
	 * @param localAmostragem the local amostragem
	 * @param atributoSelect the atributo select
	 * @return the collection
	 */
	private Collection<DadosLocalAmostragemImpl> consultarCityGateMedicao(Collection<CityGate> listaCityGates,
			Date dataLeituraAnterior, Date dataLeituraAtual, String localAmostragem, String atributoSelect) {

		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT new br.com.ggas.medicao.consumo.impl.DadosLocalAmostragemImpl(cityGateMedicao.dataMedicao, ");
		hql.append(atributoSelect);
		hql.append(" ,cityGateMedicao.cityGate) ");
		hql.append(" FROM ");
		hql.append(getClasseCityGateMedicao().getSimpleName());
		hql.append(" cityGateMedicao ");

		hql.append(" WHERE cityGateMedicao.cityGate.chavePrimaria in (:CHAVES_CITY_GATE) ");
		hql.append(" AND cityGateMedicao.dataMedicao <= :DATA_LEITURA_ATUAL ");
		hql.append(" AND cityGateMedicao.dataMedicao between :DATA_LEITURA_ANTERIOR and :DATA_LEITURA_ATUAL");

		if (EntidadeConteudo.SUPRIDORA.equals(localAmostragem)) {
			hql.append(" AND cityGateMedicao.medidaPCSSupridora is not null");
		}
		if (EntidadeConteudo.DISTRIBUIDORA.equals(localAmostragem)) {
			hql.append(" AND cityGateMedicao.medidaPCSDistribuidora is not null");
		}

		if (listaCityGates.size() > CONSTANTE_NUMERO_UM) {
			// Garante
			// que os
			// dados
			// que
			// serão
			// buscados
			// considerarão
			// o
			// volume,
			// pois
			// fará o
			// cálculo
			// ponderado
			hql.append(
					" AND (cityGateMedicao.medidaVolumeSupridora is not null OR cityGateMedicao.medidaVolumeDistribuidora is not null) ");
		}

		hql.append(" AND cityGateMedicao.habilitado = :HABILITADO");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("CHAVES_CITY_GATE", this.obterListaChavesCityGate(listaCityGates).toArray());
		query.setParameter("DATA_LEITURA_ANTERIOR", Util.adicionarDiasData(dataLeituraAnterior, 1));
		query.setParameter("DATA_LEITURA_ATUAL", dataLeituraAtual);
		query.setParameter("HABILITADO", Boolean.TRUE);

		return query.list();
	}

	/**
	 * Obter lista chaves city gate.
	 *
	 * @param listaCityGates the lista city gates
	 * @return the collection
	 */
	private Collection<Long> obterListaChavesCityGate(Collection<CityGate> listaCityGates) {

		Collection<Long> chaves = new ArrayList<Long>();
		for (CityGate cityGate : listaCityGates) {
			chaves.add(cityGate.getChavePrimaria());
		}
		return chaves;
	}

	/**
	 * Consultar city gate medicao fixo.
	 *
	 * @param listaCityGates the lista city gates
	 * @param data the data
	 * @return the collection
	 */
	private Collection<CityGateMedicao> consultarCityGateMedicaoFixo(Collection<CityGate> listaCityGates, Date data) {

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseCityGateMedicao().getSimpleName());
		hql.append(" cityGateMedicao WHERE ");
		hql.append(" cityGateMedicao.cityGate.chavePrimaria in (:CHAVES_CITY_GATE) ");
		hql.append(" AND cityGateMedicao.dataMedicao <= :DATA_LEITURA_ATUAL ");
		hql.append(" AND cityGateMedicao.medidaPCSFixo is not null");
		hql.append(" AND cityGateMedicao.habilitado = :HABILITADO");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("CHAVES_CITY_GATE", this.obterListaChavesCityGate(listaCityGates).toArray());
		query.setParameter("DATA_LEITURA_ATUAL", data);
		query.setParameter("HABILITADO", Boolean.TRUE);

		return query.list();
	}

	/**
	 * Consultar imovel pcsz.
	 *
	 * @param imovel the imovel
	 * @param datas the datas
	 * @return the collection
	 */
	private Collection<DadosLocalAmostragemImpl> consultarImovelPCSZ(Imovel imovel, Collection<Date> datas) {

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT new br.com.ggas.medicao.consumo.impl.DadosLocalAmostragemImpl(pcsz.dataVigencia, pcsz.medidaPCS)");
		hql.append(" FROM ");
		hql.append(getClasseImovelPCSZ().getSimpleName());
		hql.append(" pcsz WHERE ");
		hql.append(" pcsz.imovel.chavePrimaria = :CHAVE_IMOVEL ");

		hql.append(" AND pcsz.dataVigencia in (:INTERVALO_DATAS) ");
		hql.append(" AND pcsz.habilitado = :HABILITADO ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("CHAVE_IMOVEL", imovel.getChavePrimaria());
		query.setParameterList("INTERVALO_DATAS", datas.toArray());
		query.setParameter("HABILITADO", Boolean.TRUE);

		return query.list();
	}

	/**
	 * Consultar imovel pcsz recentes pela data base.
	 *
	 * @param imovel the imovel
	 * @param dataBase the data base
	 * @param qtdePCSNulo the qtde pcs nulo
	 * @return the collection
	 */
	private Collection<DadosLocalAmostragemImpl> consultarImovelPCSZRecentesPelaDataBase(Imovel imovel, Date dataBase,
			Integer qtdePCSNulo) {

		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT new br.com.ggas.medicao.consumo.impl.DadosLocalAmostragemImpl(pcsz.dataVigencia, pcsz.medidaPCS)");
		hql.append(" FROM ");
		hql.append(getClasseImovelPCSZ().getSimpleName());
		hql.append(" pcsz WHERE ");
		hql.append(" pcsz.imovel.chavePrimaria = :CHAVE_IMOVEL ");

		hql.append(" AND pcsz.medidaPCS is not null ");
		hql.append(" AND pcsz.dataVigencia < :DATA_BASE ");
		hql.append(" AND pcsz.habilitado = :HABILITADO ");
		hql.append(" ORDER BY pcsz.dataVigencia DESC ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setMaxResults(qtdePCSNulo);
		query.setParameter("CHAVE_IMOVEL", imovel.getChavePrimaria());
		query.setParameter("DATA_BASE", dataBase);
		query.setParameter("HABILITADO", Boolean.TRUE);

		return query.list();
	}

	/**
	 * Alteração para busca de dia com pelo menos um dado de volume (independente da prioridade).
	 * 
	 * @param listaCityGate - {@link Collection}
	 * @param dataBase - {@link Date}
	 * @return mapa contendo uma coleção de {@link CityGateMedicao} por {@link CityGate}
	 * 
	 *
	 */
	private Map<Long, Collection<CityGateMedicao>> consultarCityGateRecentesPelaDataBasePorChaveDeCityGate(
			Collection<CityGate> listaCityGates, Date dataBase) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select cityGateMedicao.cityGate.chavePrimaria as cityGate_chavePrimaria, cityGateMedicao ");
		hql.append(" FROM ");
		hql.append(getClasseCityGateMedicao().getSimpleName());
		hql.append(" cityGateMedicao WHERE ");
		hql.append(" cityGateMedicao.cityGate.chavePrimaria in (:CHAVE_CITY_GATE) ");
		hql.append(" AND cityGateMedicao.dataMedicao <= :DATA_BASE");
		hql.append(" AND (cityGateMedicao.medidaPCSSupridora is not null OR cityGateMedicao.medidaPCSDistribuidora is not null)");
		hql.append(" AND (cityGateMedicao.medidaVolumeSupridora is not null OR cityGateMedicao.medidaVolumeDistribuidora is not null) ");

		hql.append(" AND cityGateMedicao.habilitado = :HABILITADO");
		hql.append(" ORDER BY cityGateMedicao.dataMedicao DESC ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(new GGASTransformer(getClasseCityGateMedicao(), "cityGate_chavePrimaria"));

		query.setParameterList("CHAVE_CITY_GATE", Util.collectionParaArrayChavesPrimarias(listaCityGates));
		query.setParameter("DATA_BASE", dataBase);
		query.setParameter("HABILITADO", Boolean.TRUE);

		return (Map<Long, Collection<CityGateMedicao>>) query.uniqueResult();
	}

	/**
	 * Método responsável pela conversão de medidas para a unidade definida como padrão para uso do sistema.
	 *
	 * @param valor Valor a ser convertido.
	 * @param unidadeOrigem Unidade Origem.
	 * @param unidadeDestino Unidade Destino, que também é considerada a unidade padrão.
	 * @return Valor da unidade origem convertido para a unidade padrão do sistema.
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 */
	@Override
	public BigDecimal converterUnidadeMedida(BigDecimal valor, Unidade unidadeOrigem, Unidade unidadeDestino) throws NegocioException {

		BigDecimal retorno = BigDecimal.ZERO;

		if (valor == null) {
			throw new NegocioException(ERRO_NEGOCIO_VALOR_CONVERSAO, true);
		} else {

			if (unidadeOrigem.getChavePrimaria() == unidadeDestino.getChavePrimaria()) {
				retorno = valor;
			} else {

				UnidadeConversao unidade = this.consultarUnidadeConversao(unidadeOrigem, unidadeDestino);
				if (unidade == null) {
					throw new NegocioException(ERRO_NEGOCIO_UNIDADE_CONVERSAO, new String[] {unidadeOrigem.getDescricao(), unidadeDestino.getDescricao()});
				}
				try {
					retorno = (BigDecimal) Eval.me("parametro", valor, unidade.getFormula());
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					throw new NegocioException(ERRO_NEGOCIO_UNIDADE_CONVERSAO, new String[] {unidadeOrigem.getDescricao(), unidadeDestino.getDescricao()});
				}
			}
		}

		return retorno;
	}

	/**
	 * Método responsável pela conversão de medidas para uma unidade de conversão
	 *
	 * @param valor - Valor a ser convertido.
	 * @param unidadeConversao - Unidade de Conversão
	 * @return BigDecimal - Valor da unidade origem convertido para a unidade padrão do sistema.
	 * @throws NegocioException - Caso ocorra algum erro na invocação do método.
	 */
	@Override
	public BigDecimal converterUnidadeMedida(BigDecimal valor, UnidadeConversao unidadeConversao) throws NegocioException {

		BigDecimal retorno = BigDecimal.ZERO;

		if (valor == null) {
			throw new NegocioException(ERRO_NEGOCIO_VALOR_CONVERSAO, true);
		} else {

			if (unidadeConversao == null) {
				throw new NegocioException(ERRO_NEGOCIO_UNIDADE_CONVERSAO, true);
			}
			try {
				retorno = (BigDecimal) Eval.me("parametro", valor, unidadeConversao.getFormula());
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(ERRO_NEGOCIO_UNIDADE_CONVERSAO, true);
			}
		}

		return retorno;
	}

	/**
	 * Consulta uma UnidadeConversao por chave primária. Caso a UnidadeConversao já tenha sido consultada, obtém a mesmo de
	 * {@code unidadeConversaoCache}.
	 * 
	 * @param unidadeOrigem
	 * @param unidadeDestino
	 * @return unidade de conversão
	 */
	private UnidadeConversao consultarUnidadeConversao(Unidade unidadeOrigem, Unidade unidadeDestino) {

		String chaveUnidadeOrigem = String.valueOf(unidadeOrigem.getChavePrimaria());
		String chaveUnidadeDestino = String.valueOf(unidadeDestino.getChavePrimaria());
		String chave = chaveUnidadeOrigem.concat(chaveUnidadeDestino);

		UnidadeConversao unidadeConversao = null;
		if (!unidadeConversaoCache.containsKey(chave)) {
			unidadeConversao = obterUnidadeConversao(unidadeOrigem, unidadeDestino);
			unidadeConversaoCache.put(chave, unidadeConversao);
		} else {
			unidadeConversao = unidadeConversaoCache.get(chave);
		}
		return unidadeConversao;
	}

	/**
	 * Consultar unidade de conversao.
	 *
	 * @param unidadeOrigem A unidade de origem
	 * @param unidadeDestino A unidade de destino
	 * @return unidade de conversão
	 */
	public UnidadeConversao obterUnidadeConversao(Unidade unidadeOrigem, Unidade unidadeDestino) {

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClasseUnidadeConversao().getSimpleName());
		hql.append(" unidade WHERE ");
		hql.append(" unidade.unidadeOrigem.chavePrimaria = :CHAVE_UNIDADE_ORIGEM ");
		hql.append(" AND unidade.unidadeDestino.chavePrimaria = :CHAVE_UNIDADE_DESTINO ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("CHAVE_UNIDADE_ORIGEM", unidadeOrigem.getChavePrimaria());
		query.setLong("CHAVE_UNIDADE_DESTINO", unidadeDestino.getChavePrimaria());

		return (UnidadeConversao) query.uniqueResult();
	}

	/**
	 * Sub fluxo sem leitura.
	 *
	 * @param historicoConsumo the historico consumo
	 * @param historicoMedicao the historico medicao
	 * @param consumoMedioPontoMap the consumo medio ponto map
	 * @param contratoPontoConsumo the contrato ponto consumo
	 * @throws NegocioException the negocio exception
	 */
	private void subFluxoSemLeitura(HistoricoConsumo historicoConsumo, HistoricoMedicao historicoMedicao,
			Map<String, BigDecimal> consumoMedioPontoMap, ContratoPontoConsumo contratoPontoConsumo) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();

		String codigoAnormalidadeLeituraNaoInformada =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_LEITURA_NAO_INFORMADA);
		final AnormalidadeConsumo anormalidadeConsumo =
				controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeLeituraNaoInformada));
		
		aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo, () -> {

			String codigoTipoConsumo;
			AcaoAnormalidadeConsumo acaoAnormalidadeConsumo = contratoPontoConsumo.getAcaoAnormalidadeConsumoSemLeitura();
			if (acaoAnormalidadeConsumo != null) {

				int acaoAnormalidade = (int) acaoAnormalidadeConsumo.getChavePrimaria();

				if (AcaoAnormalidadeConsumo.MINIMO == acaoAnormalidade) {

					codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_MINIMO);
					ControladorHistoricoConsumo controladorHistoricoConsumo = (ControladorHistoricoConsumo) ServiceLocator.getInstancia()
							.getBeanPorID(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);
					historicoConsumo.setTipoConsumo(controladorHistoricoConsumo.obterTipoConsumo(Long.valueOf(codigoTipoConsumo)));
					historicoConsumo.setConsumo(contratoPontoConsumo.getMedidaFornecimentoMinMensal().setScale(0, RoundingMode.HALF_EVEN));
					historicoConsumo.getConsumoMinimo();

				} else if (AcaoAnormalidadeConsumo.MEDIA == acaoAnormalidade) {
					BigDecimal consumoMedioPonto = BigDecimal.ZERO;
					if (consumoMedioPontoMap != null && consumoMedioPontoMap.get("consumoMedio") != null) {
						consumoMedioPonto = consumoMedioPontoMap.get("consumoMedio").setScale(0, RoundingMode.HALF_EVEN);
					}

					codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_MEDIA);
					ControladorHistoricoConsumo controladorHistoricoConsumo = (ControladorHistoricoConsumo) ServiceLocator.getInstancia()
							.getBeanPorID(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);
					historicoConsumo.setTipoConsumo(controladorHistoricoConsumo.obterTipoConsumo(Long.valueOf(codigoTipoConsumo)));
					historicoConsumo.setConsumo(consumoMedioPonto);

					if (contratoPontoConsumo.getMedidaFornecimentoMinMensal() != null) {
						if (consumoMedioPonto.compareTo(contratoPontoConsumo.getMedidaFornecimentoMinMensal()) > CONSTANTE_NUMERO_ZERO) {
							if (historicoConsumo.getConsumoCreditoMedia() != null) {
								historicoConsumo.setConsumoCreditoMedia(consumoMedioPonto.add(historicoConsumo.getConsumoCreditoMedia()));
							} else {
								historicoConsumo.setConsumoCreditoMedia(consumoMedioPonto);
							}
						}
					} else {
						historicoConsumo.setConsumoCreditoMedia(consumoMedioPonto);
					}

				} else {
					codigoTipoConsumo =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_NAO_APURADO);
					TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
					historicoConsumo.setTipoConsumo(tipoConsumo);
					historicoConsumo.setConsumo(null);
				}
				
				calcularLeituraAtual(historicoConsumo, historicoMedicao);
				
				
			}

		});

		//

		// // TODO: lbf verificar impactos
		// // historicoMedicao.setNumeroLeituraFaturada(historicoMedicao.getNumeroLeituraAnterior())
		// // historicoMedicao.setNumeroLeituraInformada(historicoMedicao.getNumeroLeituraAnterior())

	}

	private void calcularLeituraAtual(HistoricoConsumo historicoConsumo, HistoricoMedicao historicoMedicao)
			throws ConcorrenciaException, NegocioException {
		BigDecimal leituraAtual = BigDecimal.ZERO;
		BigDecimal leituraAnterior = BigDecimal.ZERO;
		
		if(historicoMedicao.getNumeroLeituraAnterior() != null) {
			leituraAnterior = historicoMedicao.getNumeroLeituraAnterior();
		}
		
		if(historicoConsumo.getConsumo() != null) {
			
			leituraAtual = historicoConsumo.getConsumo().add(leituraAnterior);
			
		} else {
			leituraAtual = leituraAnterior;
		}
		
		historicoMedicao.setNumeroLeituraInformada(leituraAtual);
		
		this.atualizar(historicoMedicao);
	}

	/**
	 * SubFluxo Leitura Maior que a Anterior.
	 *
	 * @param historicoMedicaoCicloAnterior the historico medicao ciclo anterior
	 * @param leituraAnteriorPonto the leitura anterior ponto
	 * @param leituraAtualPonto the leitura atual ponto
	 * @param historicoConsumo the historico consumo
	 * @param historicoMedicao the historico medicao
	 * @throws NegocioException the negocio exception
	 */
	private void subFluxoLeituraMaiorAnterior(HistoricoMedicao historicoMedicaoCicloAnterior, BigDecimal leituraAnteriorPonto,
			BigDecimal leituraAtualPonto, HistoricoConsumo historicoConsumo, HistoricoMedicao historicoMedicao) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		// 2.
		BigDecimal consumoLido = leituraAtualPonto.subtract(leituraAnteriorPonto);
		historicoConsumo.setConsumo(consumoLido);

		// verifica se é 1a Leitura do medidor
		boolean isPrimeiraLeitura =
				this.verificarPrimeiraLeituraMedidor(historicoMedicao.getHistoricoInstalacaoMedidor(), leituraAnteriorPonto);

		TipoConsumo tipoConsumo = null;

		// 4.
		if ((isPrimeiraLeitura) // NOSONAR
				|| (historicoMedicaoCicloAnterior != null
						&& (historicoMedicaoCicloAnterior.getSituacaoLeitura().getChavePrimaria() == SituacaoLeitura.REALIZADA
								|| historicoMedicaoCicloAnterior.getSituacaoLeitura().getChavePrimaria() == SituacaoLeitura.CONFIRMADA))) {
			String codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL);
			tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
		} else {
			String codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_ESTIMADO);
			tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
		}

		historicoConsumo.setTipoConsumo(tipoConsumo);
	}

	/**
	 * Método responsável por verificar se a leitura atual está dentro da faixa esperada.
	 *
	 * @param historicoConsumo Histórico de Consumo
	 * @param consumoMedioApuradoDiarioPonto the consumo medio apurado diario ponto
	 * @throws NegocioException the negocio exception
	 */
	private void verificarFaixaLeituraInformada(HistoricoConsumo historicoConsumo,
			BigDecimal consumoMedioApuradoDiarioPonto) throws GGASException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia()
				.getControladorParametroSistema();
		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();

		if (historicoConsumo.getConsumoApurado() != null) {
			String parametroArredondamento = (String) controladorParametroSistema
					.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_ARREDONDAMENTO_CONSUMO);

			PontoConsumo pontoConsumo = historicoConsumo.getPontoConsumo();

			BigDecimal consumoMedioApuradoAtual = historicoConsumo.calcularConsumoApuradoMedio(parametroArredondamento);

			// Obter variação mínima e máxima
			BigDecimal limiteBaixoConsumoVariacaoConsumo = BigDecimal.ZERO;
			BigDecimal limiteInferiorVariacaoConsumo = BigDecimal.ZERO;
			BigDecimal limiteSuperiorVariacaoConsumo = BigDecimal.ZERO;
			BigDecimal limiteAltoConsumoVariacaoConsumo = BigDecimal.ZERO;
			BigDecimal limiteEstouroConsumoVariacaoConsumo = BigDecimal.ZERO;

			FaixaConsumoVariacao faixaConsumoVariacao = null;
			if (historicoConsumo.getConsumoApuradoMedio() != null) {
				faixaConsumoVariacao = this.obterFaixaConsumoPorSegmentoFaixaConsumo(pontoConsumo.getSegmento(),
						historicoConsumo.getConsumoApuradoMedio().setScale(1, RoundingMode.HALF_UP));
				
				if(faixaConsumoVariacao == null) {
					faixaConsumoVariacao = this.obterMaiorFaixaConsumoPorSegmentoFaixaConsumo(pontoConsumo.getSegmento());
				}
			}

			if (faixaConsumoVariacao != null) {
				limiteBaixoConsumoVariacaoConsumo = consumoMedioApuradoDiarioPonto
						.subtract(consumoMedioApuradoDiarioPonto.multiply(faixaConsumoVariacao.getBaixoConsumo().divide(
								new BigDecimal(CONSTANTE_NUMERO_CEM), CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP)));
				if (limiteBaixoConsumoVariacaoConsumo.compareTo(BigDecimal.ZERO) <= CONSTANTE_NUMERO_ZERO) {
					limiteBaixoConsumoVariacaoConsumo = BigDecimal.ZERO;
				}

				limiteInferiorVariacaoConsumo = consumoMedioApuradoDiarioPonto.subtract(
						consumoMedioApuradoDiarioPonto.multiply(faixaConsumoVariacao.getPercentualInferior().divide(
								new BigDecimal(CONSTANTE_NUMERO_CEM), CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP)));
				if (limiteInferiorVariacaoConsumo.compareTo(BigDecimal.ZERO) <= CONSTANTE_NUMERO_ZERO) {
					limiteInferiorVariacaoConsumo = BigDecimal.ZERO;
				}

				limiteSuperiorVariacaoConsumo = consumoMedioApuradoDiarioPonto.add(
						consumoMedioApuradoDiarioPonto.multiply(faixaConsumoVariacao.getPercentualSuperior().divide(
								new BigDecimal(CONSTANTE_NUMERO_CEM), CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP)));
				limiteAltoConsumoVariacaoConsumo = consumoMedioApuradoDiarioPonto
						.add(consumoMedioApuradoDiarioPonto.multiply(faixaConsumoVariacao.getAltoConsumo().divide(
								new BigDecimal(CONSTANTE_NUMERO_CEM), CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP)));
				limiteEstouroConsumoVariacaoConsumo = consumoMedioApuradoDiarioPonto
						.add(consumoMedioApuradoDiarioPonto.multiply(faixaConsumoVariacao.getEstouroConsumo().divide(
								new BigDecimal(CONSTANTE_NUMERO_CEM), CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP)));

				ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
						.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

				String codigoAnormalidade = null;
				
				if (isConsumoForaFaixa(consumoMedioApuradoAtual, limiteInferiorVariacaoConsumo, limiteSuperiorVariacaoConsumo)) {
					codigoAnormalidade = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_FORA_FAIXA);
				}

				if (isAltoConsumo(consumoMedioApuradoAtual, limiteAltoConsumoVariacaoConsumo)) {
					codigoAnormalidade = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_ALTO);
				}

				if (isEstouroConsumo(consumoMedioApuradoAtual, limiteEstouroConsumoVariacaoConsumo)) {
					codigoAnormalidade = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_ESTOURO);
				}

				if (isBaixoConsumo(consumoMedioApuradoAtual, limiteBaixoConsumoVariacaoConsumo)) {
					codigoAnormalidade = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_BAIXO);
				}

				if (codigoAnormalidade != null  
						&& !possuiAnormalidadeSazonal(historicoConsumo,faixaConsumoVariacao)) {
					
					final AnormalidadeConsumo anormalidadeConsumo = controladorAnormalidade
							.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidade));

					aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo);
				}
				
				
			}

		}

	}
	
	/**
	 * Verifica se o consumo representa anormalidade de BAIXO CONSUMO
	 *
	 * @param consumo Consumo apurado
	 * @param limiteBaixoConsumo o limite para comparacao
	 */
	private boolean isBaixoConsumo(BigDecimal consumo, BigDecimal limiteBaixoConsumo) {
		return consumo.compareTo(limiteBaixoConsumo) < CONSTANTE_NUMERO_ZERO;
	}
	
	/**
	 * Verifica se o consumo representa anormalidade de ALTO CONSUMO
	 *
	 * @param consumo Consumo apurado
	 * @param limiteBaixoConsumo o limite para comparacao
	 */
	private boolean isAltoConsumo(BigDecimal consumo, BigDecimal limiteAltoConsumo) {
		return consumo.compareTo(limiteAltoConsumo) > CONSTANTE_NUMERO_ZERO;
	}
	
	/**
	 * Verifica se o consumo representa anormalidade de ESTOURO CONSUMO
	 *
	 * @param consumo Consumo apurado
	 * @param limiteBaixoConsumo o limite para comparacao
	 */
	private boolean isEstouroConsumo(BigDecimal consumo, BigDecimal limiteEstouro) {
		return consumo.compareTo(limiteEstouro) > CONSTANTE_NUMERO_ZERO;
	}
	
	/**
	 * Verifica se o consumo representa anormalidade de FORA DE FAIXA
	 *
	 * @param consumo Consumo apurado
	 * @param limiteInferiorForaFaixa o limite inferior para comparacao
	 * @param limiteSuperorForaFaixa o limite superior para comparacao
	 */
	private boolean isConsumoForaFaixa(BigDecimal consumo, BigDecimal limiteInferiorForaFaixa, BigDecimal limiteSuperorForaFaixa) {
		return ((consumo.compareTo(limiteSuperorForaFaixa) > CONSTANTE_NUMERO_ZERO)
				|| (consumo.compareTo(limiteInferiorForaFaixa) < CONSTANTE_NUMERO_ZERO));
	}

	/**
	 * SubFluxo Leitura Igual a Anterior.
	 *
	 * @param historicoMedicaoCicloAnterior the historico medicao ciclo anterior
	 * @param leituraAnteriorPonto the leitura anterior ponto
	 * @param leituraAtualPonto the leitura atual ponto
	 * @param historicoConsumo the historico consumo
	 * @throws NegocioException the negocio exception
	 */
	private void subFluxoLeituraIgualAnterior(BigDecimal leituraAnteriorPonto, BigDecimal leituraAtualPonto,
			HistoricoConsumo historicoConsumo) throws NegocioException {

		ServiceLocator.getInstancia().getControladorParametroSistema();

		// 2.
		BigDecimal consumoLido = BigDecimal.ZERO;
		consumoLido = leituraAtualPonto.subtract(leituraAnteriorPonto);
		historicoConsumo.setConsumo(consumoLido);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_SEM_CONSUMO);
		historicoConsumo.setTipoConsumo(this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo)));

	}

	/**
	 * SubFluxo Leitura Menor que a Anterior.
	 *
	 * @param historicoMedicaoCicloAnterior the historico medicao ciclo anterior
	 * @param leituraAnteriorPonto the leitura anterior ponto
	 * @param leituraAtualPonto the leitura atual ponto
	 * @param historicoConsumo the historico consumo
	 * @param historicoMedicao the historico medicao
	 * @param medidorSubtituido the medidor subtituido
	 * @throws NegocioException the negocio exception
	 */
	private void subFluxoLeituraMenorAnterior(HistoricoMedicao historicoMedicaoCicloAnterior, BigDecimal leituraAnteriorPonto,
			BigDecimal leituraAtualPonto, HistoricoConsumo historicoConsumo, HistoricoMedicao historicoMedicao, Boolean medidorSubtituido)
			throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorAnormalidade controladorAnormalidade = (ControladorAnormalidade) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorAnormalidade.BEAN_ID_CONTROLADOR_ANORMALIDADE);
		ConstanteSistema constanteTipoLeituraEletroCorretor = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_TIPO_LEITURA_ELETROCORRETOR);
						
		BigDecimal consumoLido = BigDecimal.ZERO;

		int digitoMedidor = historicoMedicao.getHistoricoInstalacaoMedidor().getMedidor().getDigito();
		BigDecimal calculo = (leituraAtualPonto.add(BigDecimal.valueOf(Math.pow(10, digitoMedidor)))).subtract(leituraAnteriorPonto);

		if (calculo != null) {

			calculo = calculo.setScale(8, RoundingMode.HALF_UP);

		}

		consumoLido = calculo;

		BigDecimal calculoVirada = BigDecimal.valueOf(Math.pow(10, digitoMedidor)).subtract(BigDecimal.valueOf(1));

		if (consumoLido != null && consumoLido.compareTo(BigDecimal.ZERO) < CONSTANTE_NUMERO_ZERO) {

			// leitura atual e menor que a anterior e a leitura atual e maior que a leitura maxima do medidor
			String codigoAnormalidadeConsumoLeituraAtualMaiorLeituraVirada = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_LEITURA_MAIOR_QNT_DIGITOS_MEDIDOR);
			final AnormalidadeConsumo anormalidadeConsumo = controladorAnormalidade
					.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumoLeituraAtualMaiorLeituraVirada));

			aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo, () -> {
				historicoConsumo.setConsumo(null);
				String codigoTipoConsumo =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_SEM_CONSUMO);
				TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
				historicoConsumo.setTipoConsumo(tipoConsumo);
			});

		} else if (consumoLido != null && consumoLido.compareTo(calculoVirada) <= CONSTANTE_NUMERO_ZERO) {

			String codigoAnormalidadeViradaMedidor =
					controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_VIRADA_MEDIDOR);
			final AnormalidadeConsumo anormalidadeConsumo =
					controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeViradaMedidor));

			final BigDecimal finalConsumoLido = consumoLido;
			aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo, () -> {
				TipoConsumo tipoConsumo = null;
				if ((historicoMedicaoCicloAnterior != null
						&& historicoMedicaoCicloAnterior.getSituacaoLeitura().getChavePrimaria() == SituacaoLeitura.REALIZADA
						&& historicoMedicao.getSituacaoLeitura().getChavePrimaria() == SituacaoLeitura.REALIZADA)
						|| (historicoMedicaoCicloAnterior != null
								&& historicoMedicaoCicloAnterior.getSituacaoLeitura().getChavePrimaria() == SituacaoLeitura.CONFIRMADA
								&& historicoMedicao.getSituacaoLeitura().getChavePrimaria() == SituacaoLeitura.CONFIRMADA)) {

					String codigoTipoConsumo =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL);
					tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));

				} else {

					String codigoTipoConsumo =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_ESTIMADO);
					tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));

				}

				if (historicoMedicao.getPontoConsumo().getRota().getTipoLeitura().getChavePrimaria() == Long
						.parseLong(constanteTipoLeituraEletroCorretor.getValor())) {
					historicoConsumo.setAnormalidadeConsumo(null);
				}
				historicoConsumo.setTipoConsumo(tipoConsumo);
				historicoConsumo.setConsumo(finalConsumoLido);
			});

		} else if (medidorSubtituido) {

			// 6.
			String codigoAnormalidadeConsumoLeituraAtualMenorQueAnterior = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_LEITURA_MENOR_QUE_ANTERIOR_SUBSTITUICAO);
			final AnormalidadeConsumo anormalidadeConsumo =
					controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumoLeituraAtualMenorQueAnterior));
			aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo, () -> {
				String codigoTipoConsumo =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_SEM_CONSUMO);
				TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
				historicoConsumo.setTipoConsumo(tipoConsumo);
				historicoConsumo.setConsumo(null);
			});

		} else if (historicoMedicaoCicloAnterior != null
				&& historicoMedicaoCicloAnterior.getSituacaoLeitura().getChavePrimaria() == SituacaoLeitura.REALIZADA
				|| historicoMedicaoCicloAnterior != null
						&& historicoMedicaoCicloAnterior.getSituacaoLeitura().getChavePrimaria() == SituacaoLeitura.CONFIRMADA) {

			// 6.
			String codigoAnormalidadeConsumoLeituraAtualMenorQueAnterior = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_LEITURA_MENOR_QUE_ANTERIOR);
			final AnormalidadeConsumo anormalidadeConsumo =
					controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumoLeituraAtualMenorQueAnterior));
			aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo, () -> {
				historicoConsumo.setConsumo(null);
				String codigoTipoConsumo =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_SEM_CONSUMO);
				TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.parseLong(codigoTipoConsumo));
				historicoConsumo.setTipoConsumo(tipoConsumo);
			});

		} else if (historicoMedicaoCicloAnterior != null
				&& historicoMedicaoCicloAnterior.getSituacaoLeitura().getChavePrimaria() == SituacaoLeitura.NAO_REALIZADA) {

			// 7.
			String codigoAnormalidadeLeituraMenorQueProjetada = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_LEITURA_MENOR_QUE_PROJETADA);
			final AnormalidadeConsumo anormalidadeConsumo =
					controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeLeituraMenorQueProjetada));

			aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo, () -> {
				String codigoTipoConsumo =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_SEM_CONSUMO);
				TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
				historicoConsumo.setConsumo(null);
				historicoConsumo.setTipoConsumo(tipoConsumo);
			});

		} else {

			String codigoAnormalidadeConsumoLeituraAtualMenorQueAnterior = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_CONSUMO_LEITURA_MENOR_QUE_ANTERIOR);
			final AnormalidadeConsumo anormalidadeConsumo =
					controladorAnormalidade.obterAnormalidadeConsumo(Long.parseLong(codigoAnormalidadeConsumoLeituraAtualMenorQueAnterior));
			aplicarAnormalidadeSeAtiva(anormalidadeConsumo, historicoConsumo, () -> {
				historicoConsumo.setConsumo(null);
				String codigoTipoConsumo =
						controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_SEM_CONSUMO);
				TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
				historicoConsumo.setTipoConsumo(tipoConsumo);
			});

		}

	}

	/**
	 * Método responsável por verificar se uma Leitura de Historico de Medição é a mesma da instalação do Medidor.
	 *
	 * @param instalacaoMedidor [obrigatório] - Histórico de Instalação do Medidor atualmente instalado no Ponto de Consumo
	 * @param leituraMedicao [obrigatório] - leitura que será comparada com a leitura de instalacao do medidor
	 * @return true, se a Leitura é de instalação do Medidor
	 * @throws NegocioException the negocio exception
	 */
	private boolean verificarPrimeiraLeituraMedidor(InstalacaoMedidor instalacaoMedidor, BigDecimal leituraMedicao)
			throws NegocioException {

		// FIXME : Esta verificação está errada, uma leitura é considerada como sendo a primeira quandonão existe outra entre a instalação e
		// a atual

		boolean leituraInstalacao = false;

		if (instalacaoMedidor.getLeitura() == null) {
			throw new NegocioException(ERRO_NEGOCIO_HISTORICO_INSTALACAO_LEITURA_NULA, instalacaoMedidor.getChavePrimaria());
		}

		ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

		BigDecimal leitura = controladorHistoricoMedicao.setarLeituraAnterior(instalacaoMedidor, null, null);

		if (leitura.compareTo(leituraMedicao) == CONSTANTE_NUMERO_ZERO) {
			leituraInstalacao = true;
		}
		return leituraInstalacao;
	}

	/**
	 * Verificar substituicao medidor.
	 *
	 * @param historicoMedicaoCicloAnterior the historico medicao ciclo anterior
	 * @param historicoMedicao the historico medicao
	 * @return true, if successful
	 */
	private boolean verificarSubstituicaoMedidor(HistoricoMedicao historicoMedicaoCicloAnterior, HistoricoMedicao historicoMedicao) {

		boolean medidorSubtituido = false;

		if (historicoMedicaoCicloAnterior != null) {

			InstalacaoMedidor instalacaoMedidorAnterior = historicoMedicaoCicloAnterior.getHistoricoInstalacaoMedidor();
			InstalacaoMedidor instalacaoMedidorAtual = historicoMedicao.getHistoricoInstalacaoMedidor();

			if ((instalacaoMedidorAnterior != null && instalacaoMedidorAtual != null) && (instalacaoMedidorAnterior.getMedidor()
					.getChavePrimaria() != instalacaoMedidorAtual.getMedidor().getChavePrimaria())) {

				// Houve substituição do medidor
				medidorSubtituido = true;

			}

		}

		return medidorSubtituido;
	}

	/**
	 * Subfluxo utilizado no Consistir que executa as ações configuradas para a Anormalidade de Leitura atribuída a um historico de Medição.
	 *
	 * @param historicoConsumo [obrigatorio]
	 * @param leituraInformada the leitura informada
	 * @throws NegocioException the negocio exception
	 * @throws NullPointerException se HistoricoAtual não estiver atribuído ao HistoricoConsumo passado.
	 */
	private void subFluxoAnormalidadeLeituraInformada(HistoricoConsumo historicoConsumo, final boolean leituraInformada)
			throws NegocioException {

		AcaoAnormalidadeLeitura acaoAnormalidadeLeitura = null;
		AcaoAnormalidadeConsumo acaoAnormalidadeConsumo = null;

		if (leituraInformada) {
			// determina o consumo baseado no
			// config da anormalidade
			if (historicoConsumo.getHistoricoAtual().getAnormalidadeLeituraFaturada() != null) {
				acaoAnormalidadeConsumo =
						historicoConsumo.getHistoricoAtual().getAnormalidadeLeituraFaturada().getAcaoAnormalidadeComConsumo();
			}
			if (historicoConsumo.getHistoricoAtual().getAnormalidadeLeituraFaturada() != null) {
				acaoAnormalidadeLeitura =
						historicoConsumo.getHistoricoAtual().getAnormalidadeLeituraFaturada().getAcaoAnormalidadeComLeitura();
			}
		} else {

			if (historicoConsumo != null && historicoConsumo.getHistoricoAtual().getAnormalidadeLeituraFaturada() != null) {
				acaoAnormalidadeConsumo =
						historicoConsumo.getHistoricoAtual().getAnormalidadeLeituraFaturada().getAcaoAnormalidadeSemConsumo();
			}
			if (historicoConsumo != null && historicoConsumo.getHistoricoAtual().getAnormalidadeLeituraFaturada() != null) {
				acaoAnormalidadeLeitura =
						historicoConsumo.getHistoricoAtual().getAnormalidadeLeituraFaturada().getAcaoAnormalidadeSemLeitura();
			}
		}
		if (historicoConsumo != null && acaoAnormalidadeConsumo != null && acaoAnormalidadeLeitura != null) {
			this.determinarConsumoLeituraInformadaComAnormalidade(historicoConsumo, acaoAnormalidadeConsumo);
			processadorAcaoAnormalidadeLeitura.aplicarAcaoLeituraNoHistorico(historicoConsumo, acaoAnormalidadeLeitura);
		}
	}

	/**
	 * determina a acao que será tomada para definição do consumo de uma medição com Anormalidade.
	 *
	 * @param historicoConsumo [obrigatorio]
	 * @param acaoAnormalidadeConsumo [obrigatorio]
	 * @throws NegocioException the negocio exception
	 * @throws NullPointerException se historicoMedicao ou acaoAnormalidade é nulo
	 */
	private void determinarConsumoLeituraInformadaComAnormalidade(HistoricoConsumo historicoConsumo,
			AcaoAnormalidadeConsumo acaoAnormalidadeConsumo) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		int acaoAnormalidade = (int) acaoAnormalidadeConsumo.getChavePrimaria();
		String codigoTipoConsumo = null;
		ServiceLocator.getInstancia().getControladorParametroSistema();

		switch (acaoAnormalidade) {
		case AcaoAnormalidadeConsumo.NAO_OCORRE:
			historicoConsumo.setConsumo(historicoConsumo.getConsumoApuradoMedio());
			codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_MEDIA);
			break;
		case AcaoAnormalidadeConsumo.MINIMO:
			// TODO obtencao do Consumo Minimo
			// será pelo Contrato
			historicoConsumo.setConsumo(BigDecimal.ZERO);
			codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_SEM_CONSUMO);
			break;
		case AcaoAnormalidadeConsumo.MEDIA:
			codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_MEDIA);
			break;
		case AcaoAnormalidadeConsumo.NORMAL:
			// Acao Normal não realiza ação,
			// mantido para facilitar
			// compreensão
			break;
		case AcaoAnormalidadeConsumo.MAIOR_ENTRE_MEDIA_CONSUMO:
			if (historicoConsumo.getConsumo().compareTo(historicoConsumo.getConsumoApuradoMedio()) <= CONSTANTE_NUMERO_ZERO) {
				historicoConsumo.setConsumo(historicoConsumo.getConsumoApuradoMedio());
			}
			// caso contrario o consumo já
			// atribuído deve ser mantido
			// FIXME : O tipo será média se a média for maior que o consumo, caso contrário, será normal
			codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_MEDIA);
			break;
		case AcaoAnormalidadeConsumo.MENOR_ENTRE_MEDIA_CONSUMO:
			if (historicoConsumo.getConsumo().compareTo(historicoConsumo.getConsumoApuradoMedio()) > CONSTANTE_NUMERO_ZERO) {
				historicoConsumo.setConsumo(historicoConsumo.getConsumoApuradoMedio());
			}
			// caso contrario o consumo já
			// atribuído deve ser mantido

			// FIXME : O tipo será média se a média for menor que o consumo, caso contrário, será normal
			codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_MEDIA);
			break;
		default:
			// nada a fazer
			break;
		}

		if (codigoTipoConsumo != null) {
			TipoConsumo tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
			historicoConsumo.setTipoConsumo(tipoConsumo);
		}
	}

	/**
	 * realiza um cálculo de composição de consumo baseado nos medidores anterior e atual do ponto de consumo.
	 *
	 * @param historicoMedicaoCicloAnterior the historico medicao ciclo anterior
	 * @param historicoMedicaoCicloAtual the historico medicao ciclo atual
	 * @param historicoConsumo the historico consumo
	 * @param pontoConsumo the ponto consumo
	 * @param consumoMedioPonto the consumo medio ponto
	 * @throws NegocioException the negocio exception
	 */
	private void aplicarComposicaoConsumo(HistoricoMedicao historicoMedicaoCicloAnterior, HistoricoMedicao historicoMedicaoCicloAtual,
			HistoricoConsumo historicoConsumo, PontoConsumo pontoConsumo) throws GGASException {

		String codigoTipoConsumo = null;
		TipoConsumo tipoConsumo = null;
		BigDecimal consumoLido = BigDecimal.ZERO;
		Integer padraoComposicao = null;

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		// Obtém o parâmetro de código da operação
		// de substituição de medidor
		String valorCodigoOperacaoSubstituicao =
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_OPERACAO_SUBSTITUICAO);

		if (StringUtils.isEmpty(valorCodigoOperacaoSubstituicao)) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_OPERACAO_SUBTITUICAO_MEDIDOR_NAO_PARAMETRIZADA, true);
		}

		// Consulta o histórico de operação de
		// subtituição do medidor
		Date dataInicio = historicoMedicaoCicloAnterior.getDataLeituraFaturada();
		Date dataFim = historicoMedicaoCicloAtual.getDataLeituraFaturada();

		HistoricoOperacaoMedidor historicoOperacaoMedidorSubstituicao =
				controladorMedidor.obterHistoricoOperacaoMedidorPorChaveMedidorOperacao(
						historicoMedicaoCicloAnterior.getHistoricoInstalacaoMedidor().getMedidor().getChavePrimaria(),
						Long.parseLong(valorCodigoOperacaoSubstituicao), pontoConsumo.getChavePrimaria(), dataInicio, dataFim);

		// Caso exista o registro de histórico de
		// operação de substituição do
		// medidor, obter o tipo de composição
		// associado ao motivo.
		if (historicoOperacaoMedidorSubstituicao != null) {

			padraoComposicao =
					(int) historicoOperacaoMedidorSubstituicao.getMotivoOperacaoMedidor().getTipoComposicaoConsumo().getChavePrimaria();

		} else {
			// Obtém o parâmetro de código do tipo
			// de composição padrão
			// FIXME: Substituir por consulta na tabela CONSUMO_COMPOSICAO_TIPO onde COCT_IN_PADRAO = 1
			String valorCodigoTipoComposicao =
					(String) controladorParametroSistema.obterValorDoParametroPorCodigo(CODIGO_TIPO_COMPOSICAO_CONSUMO_PADRAO);

			if (StringUtils.isEmpty(valorCodigoTipoComposicao)) {
				throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_TIPO_COMPOSICAO_NAO_PARAMETRIZADO, true);
			}

			padraoComposicao = Integer.valueOf(valorCodigoTipoComposicao);
		}

		// leituras do medidor anterior
		BigDecimal leituraCicloAnterior = BigDecimal.ZERO;
		BigDecimal leituraRetirada = BigDecimal.ZERO;

		// leituras do medidor atual
		BigDecimal leituraInstalacaoAtual = BigDecimal.ZERO;
		BigDecimal leituraCicloAtual = BigDecimal.ZERO;

		ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

		switch (padraoComposicao) {
		case CONSTANTE_NUMERO_UM:

			// Calcula o consumo do novo
			// medidor
			leituraInstalacaoAtual = controladorHistoricoMedicao.setarLeituraAnterior(null, historicoMedicaoCicloAtual.getPontoConsumo(),
					historicoMedicaoCicloAtual.getHistoricoInstalacaoMedidor().getMedidor());
			leituraCicloAtual = historicoMedicaoCicloAtual.getNumeroLeituraInformada();

			if (leituraCicloAtual.compareTo(leituraInstalacaoAtual) < CONSTANTE_NUMERO_ZERO) {

				this.subFluxoLeituraMenorAnterior(historicoMedicaoCicloAnterior, leituraInstalacaoAtual, leituraCicloAtual,
						historicoConsumo, historicoMedicaoCicloAtual, Boolean.TRUE);
				// caso seja virada
				if (historicoConsumo.getConsumo() != null) {
					codigoTipoConsumo =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL_ATUAL);
					tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
					historicoConsumo.setTipoConsumo(tipoConsumo);
				}

			} else {

				consumoLido = leituraCicloAtual.subtract(leituraInstalacaoAtual);
				codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL_ATUAL);
				tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
				historicoConsumo.setTipoConsumo(tipoConsumo);
				historicoConsumo.setConsumo(consumoLido);
			}

			break;

		case CONSTANTE_NUMERO_DOIS:
			// Compõe o consumo pela soma dos
			// consumos em cada medidor

			if (historicoOperacaoMedidorSubstituicao == null) {

				throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_OPERACAO_RETIRADA_SEM_HISTORICO,
						new Object[] { historicoMedicaoCicloAtual.getHistoricoInstalacaoMedidor().getMedidor().getNumeroSerie(),
								historicoConsumo.getPontoConsumo().getChavePrimaria() });

			}

			leituraCicloAnterior = historicoMedicaoCicloAnterior.getNumeroLeituraFaturada();
			BigDecimal consumoCreditoMedia = BigDecimal.ZERO;
			if (leituraCicloAnterior == null) {
				HistoricoConsumo historicoConsumoUltimoleituraReal = this.obterUltimosHistoricosConsumoComLeituraFaturada(pontoConsumo);
				leituraCicloAnterior = historicoConsumoUltimoleituraReal.getHistoricoAnterior().getNumeroLeituraFaturada();
				historicoMedicaoCicloAnterior = historicoConsumoUltimoleituraReal.getHistoricoAnterior();

				HistoricoConsumo historicoConsumoAnterior = this.obterUltimosHistoricosConsumo(pontoConsumo);
				if (historicoConsumoAnterior != null && historicoConsumoAnterior.getConsumoCreditoMedia() != null) {
					consumoCreditoMedia = historicoConsumoAnterior.getConsumoCreditoMedia();
				}
			}

			leituraRetirada = historicoOperacaoMedidorSubstituicao.getNumeroLeitura();

			leituraInstalacaoAtual = controladorHistoricoMedicao.setarLeituraAnterior(null, historicoMedicaoCicloAtual.getPontoConsumo(),
					historicoMedicaoCicloAtual.getHistoricoInstalacaoMedidor().getMedidor());
			leituraCicloAtual = historicoMedicaoCicloAtual.getNumeroLeituraInformada();

			if (leituraRetirada.compareTo(leituraCicloAnterior) < CONSTANTE_NUMERO_ZERO) {

				this.subFluxoLeituraMenorAnterior(historicoMedicaoCicloAnterior, leituraCicloAnterior, leituraRetirada, historicoConsumo,
						historicoMedicaoCicloAnterior, Boolean.TRUE);

				// caso seja virada
				if (historicoConsumo.getConsumo() != null) {

					// Subtrai o valor de
					// leitura anterior
					// (medidor antigo)
					consumoLido = historicoConsumo.getConsumo();

					if (leituraCicloAtual.compareTo(leituraInstalacaoAtual) < CONSTANTE_NUMERO_ZERO) {

						this.subFluxoLeituraMenorAnterior(historicoMedicaoCicloAnterior, leituraInstalacaoAtual, leituraCicloAtual,
								historicoConsumo, historicoMedicaoCicloAtual, Boolean.TRUE);

						// caso seja virada
						if (historicoConsumo.getConsumo() != null) {

							consumoLido = consumoLido.add(historicoConsumo.getConsumo());
							codigoTipoConsumo = controladorConstanteSistema
									.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL_COMPOSTO);
							tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
							historicoConsumo.setTipoConsumo(tipoConsumo);
							historicoConsumo.setConsumo(consumoLido);

						}

					} else {

						consumoLido = consumoLido.add(leituraCicloAtual.subtract(leituraInstalacaoAtual));
						codigoTipoConsumo =
								controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL_COMPOSTO);
						tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
						historicoConsumo.setTipoConsumo(tipoConsumo);
						historicoConsumo.setConsumo(consumoLido);
					}

				}

			} else {

				// Subtrai o valor de leitura
				// anterior (medidor antigo)
				consumoLido = leituraRetirada.subtract(leituraCicloAnterior);

				// verificando se teve algum credito decorrente a media
				consumoLido = consumoLido.subtract(consumoCreditoMedia);
				if (consumoLido.compareTo(BigDecimal.ZERO) < 1) {
					consumoLido = BigDecimal.ZERO;
				}

				if (leituraCicloAtual.compareTo(leituraInstalacaoAtual) < CONSTANTE_NUMERO_ZERO) {

					this.subFluxoLeituraMenorAnterior(historicoMedicaoCicloAnterior, leituraInstalacaoAtual,
							historicoMedicaoCicloAtual.getNumeroLeituraInformada(), historicoConsumo, historicoMedicaoCicloAtual,
							Boolean.TRUE);

					// caso seja virada
					if (historicoConsumo.getConsumo() != null) {

						consumoLido = consumoLido.add(historicoConsumo.getConsumo());
						codigoTipoConsumo =
								controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL_COMPOSTO);
						tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
						historicoConsumo.setTipoConsumo(tipoConsumo);
						historicoConsumo.setConsumo(consumoLido);

					}

				} else {
					// normal
					// Atribui o valor de
					// leitura da retirada do
					// medidor
					if (historicoOperacaoMedidorSubstituicao.getNumeroLeitura() != null) {
						// leituraRetirada = historicoOperacaoMedidorSubstituicao.getNumeroLeitura(); // sem uso
					}

					// Leitura da Instalacao
					// Anterior

					leituraCicloAnterior = historicoMedicaoCicloAnterior.getNumeroLeituraFaturada();

					// Leitura da Instalacao Atual
					BigDecimal leituraAtual =
							controladorHistoricoMedicao.setarLeituraAnterior(null, historicoMedicaoCicloAtual.getPontoConsumo(),
									historicoMedicaoCicloAtual.getHistoricoInstalacaoMedidor().getMedidor());
					leituraInstalacaoAtual = leituraAtual;

					// Adiciona o valor de leitura atual
					consumoLido = consumoLido.add(historicoMedicaoCicloAtual.getNumeroLeituraInformada());

					// Subtrai o valor de leitura da instalação do medidor
					consumoLido = consumoLido.subtract(leituraInstalacaoAtual);

					codigoTipoConsumo =
							controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_REAL_COMPOSTO);
					tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
					historicoConsumo.setTipoConsumo(tipoConsumo);
					historicoConsumo.setConsumo(consumoLido);

				}
			}

			break;

		case CONSTANTE_NUMERO_TRES:
			// // Ajusta o consumo para os
			// dias do ciclo

			leituraInstalacaoAtual = controladorHistoricoMedicao.setarLeituraAnterior(null, historicoMedicaoCicloAtual.getPontoConsumo(),
					historicoMedicaoCicloAtual.getHistoricoInstalacaoMedidor().getMedidor());
			leituraCicloAtual = historicoMedicaoCicloAtual.getNumeroLeituraInformada();

			if (leituraCicloAtual.compareTo(leituraInstalacaoAtual) < CONSTANTE_NUMERO_ZERO) {

				this.subFluxoLeituraMenorAnterior(historicoMedicaoCicloAnterior, leituraInstalacaoAtual, leituraCicloAtual,
						historicoConsumo, historicoMedicaoCicloAtual, Boolean.TRUE);

				if (historicoConsumo.getConsumo() != null) {
					Date dataInstalacaoAtual =
							controladorHistoricoMedicao.setarDataLeituraAnterior(null, historicoMedicaoCicloAtual.getPontoConsumo(),
									historicoMedicaoCicloAtual.getHistoricoInstalacaoMedidor().getMedidor());
					consumoLido = historicoConsumo.getConsumo();

					int numeroDiasInstalacao = 1;
					numeroDiasInstalacao = Util.intervaloDatas(dataInstalacaoAtual, historicoMedicaoCicloAtual.getDataLeituraInformada());

					if (dataInstalacaoAtual.compareTo(historicoMedicaoCicloAtual.getDataLeituraInformada()) == CONSTANTE_NUMERO_ZERO) {
						numeroDiasInstalacao = 1;
					}

					String escalaParametro = (String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ESCALA_CALCULO);
					int escala = Integer.parseInt(escalaParametro);

					consumoLido = consumoLido.divide(new BigDecimal(numeroDiasInstalacao), escala, BigDecimal.ROUND_HALF_UP);
					consumoLido = consumoLido.multiply(new BigDecimal(historicoConsumo.getDiasConsumo()));

					codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_ESTIMADO);
					tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
					historicoConsumo.setTipoConsumo(tipoConsumo);
					historicoConsumo.setConsumo(consumoLido);

				}

			} else {

				Date dataInstalacaoAtual =
						controladorHistoricoMedicao.setarDataLeituraAnterior(null, historicoMedicaoCicloAtual.getPontoConsumo(),
								historicoMedicaoCicloAtual.getHistoricoInstalacaoMedidor().getMedidor());
				consumoLido = leituraCicloAtual.subtract(leituraInstalacaoAtual);

				int numeroDiasInstalacao = 1;
				numeroDiasInstalacao = Util.intervaloDatas(dataInstalacaoAtual, historicoMedicaoCicloAtual.getDataLeituraInformada());

				if (dataInstalacaoAtual.compareTo(historicoMedicaoCicloAtual.getDataLeituraInformada()) == CONSTANTE_NUMERO_ZERO) {
					numeroDiasInstalacao = 1;
				}

				String escalaParametro =
						(String) controladorParametroSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_ESCALA_CALCULO);
				int escala = Integer.parseInt(escalaParametro);

				consumoLido = consumoLido.divide(new BigDecimal(numeroDiasInstalacao), escala, BigDecimal.ROUND_HALF_UP);
				consumoLido = consumoLido.multiply(new BigDecimal(historicoConsumo.getDiasConsumo()));

				codigoTipoConsumo = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_CONSUMO_ESTIMADO);
				tipoConsumo = this.obterTipoConsumo(Long.valueOf(codigoTipoConsumo));
				historicoConsumo.setTipoConsumo(tipoConsumo);
				historicoConsumo.setConsumo(consumoLido);

			}
			break;

		default:
			break;
		}
	}

	/**
	 * Retorna um HistoricoConsumo com atributos que nao serao definidos na Consistencia.
	 *
	 * @param mapAtributosConsumo the map atributos consumo
	 * @return HistoricoConsumo
	 */
	private HistoricoConsumo adicionarHistoricoConsumo(Map<String, Object> mapAtributosConsumo) {

		HistoricoConsumo historicoConsumo = (HistoricoConsumo) this.criar();
		historicoConsumo.setPontoConsumo((PontoConsumo) mapAtributosConsumo.get(PONTO_CONSUMO));
		historicoConsumo.setHistoricoAnterior((HistoricoMedicao) mapAtributosConsumo.get(HISTORICO_MEDICAO_ANTERIOR));
		historicoConsumo.setAnoMesFaturamento((Integer) mapAtributosConsumo.get(REFERENCIA));
		historicoConsumo.setNumeroCiclo((Integer) mapAtributosConsumo.get(CICLO));
		historicoConsumo.setConsumoMedio((BigDecimal) mapAtributosConsumo.get(CONSUMO_MEDIO));
		historicoConsumo.setConsumoApuradoMedio((BigDecimal) mapAtributosConsumo.get(CONSUMO_APURADO_MEDIO));
		// Modificado para o incluir fatura.
		historicoConsumo.setIndicadorFaturamento(Boolean.FALSE);
		historicoConsumo.setUltimaAlteracao(Calendar.getInstance().getTime());
		historicoConsumo.setIndicadorConsumoCiclo(Boolean.FALSE);

		// TODO verificacao se o PC é de Imov.
		// Condominio

		return historicoConsumo;
	}

	/**
	 * Inabilita um ou mais historicos de Consumo dos Pontos de Consumo para uma determinada Referencia/Ciclo.
	 *
	 * @param pontoConsumo [obrigatorio]
	 * @param referenciaFaturamento [obrigatorio]
	 * @param numeroCiclo [obrigatorio]
	 */
	private void inativarHistoricoConsumoReferenciaCiclo(Collection<PontoConsumo> listaPontoConsumo, Integer referenciaFaturamento,
			Integer numeroCiclo) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" update ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" set historico.habilitado = false ");
		hql.append(" where ");
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "historico.pontoConsumo.chavePrimaria", "PT_CONS", Util.recuperarChavesPrimarias(listaPontoConsumo));
		hql.append(" and historico.anoMesFaturamento = :referenciaFaturamento ");
		hql.append(" and historico.numeroCiclo = :numeroCiclo ");
		hql.append(" and historico.indicadorFaturamento = false ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		query.setInteger("referenciaFaturamento", referenciaFaturamento);
		query.setInteger(NUMERO_CICLO, numeroCiclo);

		query.executeUpdate();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#obterReferenciaInicial(java.lang.String, java.lang.String)
	 */
	@Override
	public String obterReferenciaInicial(String anoInicial, String mesInicial) throws NegocioException {

		String referenciaInicial = null;

		boolean anoPreenchido = !StringUtils.isEmpty(anoInicial) && !"-1".equals(anoInicial);
		boolean mesPreenchido = !StringUtils.isEmpty(mesInicial) && !"-1".equals(mesInicial);

		if (anoPreenchido && !mesPreenchido) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_REFERENCIA_MES_ANO, true);
		}

		if (mesPreenchido && !anoPreenchido) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_REFERENCIA_MES_ANO, true);
		}

		if (anoPreenchido && mesPreenchido) {
			referenciaInicial = anoInicial + Util.adicionarZerosEsquerdaNumero(mesInicial.toString(), 2);
		}
		return referenciaInicial;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#obterReferenciaFinal(java.lang.String, java.lang.String)
	 */
	@Override
	public String obterReferenciaFinal(String anoFinal, String mesFinal) throws NegocioException {

		String referenciaFinal = null;

		boolean anoPreenchido = !StringUtils.isEmpty(anoFinal) && !"-1".equals(anoFinal);
		boolean mesPreenchido = !StringUtils.isEmpty(mesFinal) && !"-1".equals(mesFinal);

		if (anoPreenchido && !mesPreenchido) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_REFERENCIA_MES_ANO, true);
		}

		if (mesPreenchido && !anoPreenchido) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_REFERENCIA_MES_ANO, true);
		}

		if (anoPreenchido && mesPreenchido) {
			referenciaFinal = anoFinal + Util.adicionarZerosEsquerdaNumero(mesFinal.toString(), 2);
		}
		return referenciaFinal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# validarIntervaloDatas(java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public void validarIntervaloDatas(String mesInicial, String anoInicial, String mesFinal, String anoFinal) throws GGASException {

		if ((!"".equals(mesInicial)) && (!"".equals(anoInicial)) && (!"".equals(mesFinal)) && (!"".equals(anoFinal))
				&& (!"-1".equals(mesInicial)) && (!"-1".equals(anoInicial)) && (!"-1".equals(mesFinal)) && (!"-1".equals(anoFinal))) {

			String mesDataInicial = mesInicial;
			String mesDataFinal = mesFinal;

			if (mesDataInicial.length() == 1) {
				mesDataInicial = "0" + mesDataInicial;
				//
				// by
				// jmauricio
				// on
				// 02/09/10
				// 10:56
			}

			if (mesDataFinal.length() == 1) {
				mesDataFinal = "0" + mesDataFinal;
				//
				// by
				// jmauricio
				// on
				// 02/09/10
				// 10:57
			}

			String dataInicialString = "01/" + mesDataInicial + "/" + anoInicial;
			Date dataInicial = Util.converterCampoStringParaData("Data", dataInicialString, Constantes.FORMATO_DATA_BR);

			String dataFinalString = "01/" + mesDataFinal + "/" + anoFinal;
			Date dataFinal = Util.converterCampoStringParaData("Data", dataFinalString, Constantes.FORMATO_DATA_BR);

			if (dataFinal.before(dataInicial)) {
				throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_DATA_FINAL_MENOR_DATA_INICIAL, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#validarIntervaloReferencia(java.lang.String, java.lang.String)
	 */
	@Override
	public void validarIntervaloReferencia(String referenciaInicial, String referenciaFinal) throws NegocioException {

		if ((referenciaInicial == null && referenciaFinal != null) || (referenciaInicial != null && referenciaFinal == null)) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA, true);
		}
	}

	/**
	 * Obter ponto consumo city gate.
	 *
	 * @param chaveCityGate the chave city gate
	 * @param chavePontoConsumo the chave ponto consumo
	 * @param dataBase the data base
	 * @return the ponto consumo city gate
	 */
	private PontoConsumoCityGate obterPontoConsumoCityGate(Long chaveCityGate, Long chavePontoConsumo, Date dataBase) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(getClassePontoConsumoCityGate().getSimpleName());
		hql.append(" cgPC WHERE ");
		hql.append(" cgPC.pontoConsumo.chavePrimaria = :CHAVE_PONTO_CONSUMO ");
		hql.append(" AND cgPC.cityGate.chavePrimaria = :CHAVE_CITY_GATE ");
		hql.append(" AND cgPC.dataVigencia <= :DATA_BASE ");
		hql.append(" ORDER BY cgPC.dataVigencia DESC");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setMaxResults(1);
		query.setParameter("CHAVE_PONTO_CONSUMO", chavePontoConsumo);
		query.setParameter("CHAVE_CITY_GATE", chaveCityGate);
		query.setParameter("DATA_BASE", dataBase);
		return (PontoConsumoCityGate) query.uniqueResult();
	}

	/**
	 * Obtém um mapa de uma lista de PontoConsumoCityGate por city gate e ponto de consumo. dos pontos de consumo especificados em
	 * {@code chavesPontosDeConsumo}.
	 * 
	 * @param chavesPontosDeConsumo
	 * @return mapa de PontoConsumoCityGate
	 */
	private Map<PontoConsumo, Map<CityGate, List<PontoConsumoCityGate>>> obterPontoConsumoCityGatePorPontoConsumo(
			Long[] chavesPontosDeConsumo) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo.chavePrimaria,  ");
		hql.append(" cityGate.chavePrimaria, cgPC.chavePrimaria, ");
		hql.append(" cgPC.percentualParticipacao, cgPC.dataVigencia ");
		hql.append(" FROM ");
		hql.append(getClassePontoConsumoCityGate().getSimpleName());
		hql.append(" cgPC ");
		hql.append(" WHERE ");
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontosDeConsumo));
		hql.append(" ORDER BY cgPC.dataVigencia DESC");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

		return this.construirMapaPontoConsumoCityGate(query.list());
	}

	/**
	 * Constrói um mapa de uma lista de PontoConsumoCityGate por City Gate e PontoConsumo a partir da lista de atributos passada por
	 * parâmetro.
	 *
	 * @param listaAtributos
	 * @return mapa de PontoConsumoCityGate
	 */
	private Map<PontoConsumo, Map<CityGate, List<PontoConsumoCityGate>>> construirMapaPontoConsumoCityGate(List<Object[]> listaAtributos) {

		Map<PontoConsumo, Map<CityGate, List<PontoConsumoCityGate>>> mapaPontoConsumoCityGate = new HashMap<>();
		Map<CityGate, List<PontoConsumoCityGate>> mapaCityGate;
		List<PontoConsumoCityGate> listaPontoConsumoCityGate = null;
		for (Object[] atributos : listaAtributos) {
			PontoConsumo pontoConsumo = new PontoConsumoImpl();
			pontoConsumo.setChavePrimaria((long) atributos[CONSTANTE_NUMERO_ZERO]);
			CityGate cityGate = new CityGateImpl();
			cityGate.setChavePrimaria((long) atributos[1]);
			PontoConsumoCityGate pontoConsumoCityGate = new PontoConsumoCityGateImpl();
			pontoConsumoCityGate.setChavePrimaria((long) atributos[2]);
			pontoConsumoCityGate.setPercentualParticipacao((BigDecimal) atributos[CONSTANTE_NUMERO_TRES]);
			pontoConsumoCityGate.setDataVigencia((Date) atributos[CONSTANTE_NUMERO_QUATRO]);
			if (!mapaPontoConsumoCityGate.containsKey(pontoConsumo)) {
				mapaCityGate = new HashMap<>();
				listaPontoConsumoCityGate = new ArrayList<>();
				mapaCityGate.put(cityGate, listaPontoConsumoCityGate);
				mapaPontoConsumoCityGate.put(pontoConsumo, mapaCityGate);
			} else {
				mapaCityGate = mapaPontoConsumoCityGate.get(pontoConsumo);
				if (mapaCityGate != null && !mapaCityGate.containsKey(cityGate)) {
					listaPontoConsumoCityGate = new ArrayList<>();
					mapaCityGate.put(cityGate, listaPontoConsumoCityGate);
				} else if (mapaCityGate != null) {
					listaPontoConsumoCityGate = mapaCityGate.get(cityGate);
				}
			}
			if (listaPontoConsumoCityGate != null) {
				listaPontoConsumoCityGate.add(pontoConsumoCityGate);
			}
		}
		return mapaPontoConsumoCityGate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# listarIntervaloPCS()
	 */
	@Override
	public Collection<IntervaloPCS> listarIntervaloPCS() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseIntervaloPCS().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#obterHistoricoConsumo(java.lang.Long)
	 */
	@Override
	public HistoricoConsumo obterHistoricoConsumo(Long idHistoricoConsumo) {

		StringBuilder hql = new StringBuilder();

		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" WHERE ");
		hql.append(" historico.habilitado = true and historico.chavePrimaria = :idHistoricoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idHistoricoConsumo", idHistoricoConsumo);
		HistoricoConsumo historicoConsumo = (HistoricoConsumo) query.uniqueResult();
		if (historicoConsumo != null) {
			Hibernate.initialize(historicoConsumo.getHistoricoAtual());
		}
		return historicoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarHistoricoConsumoParaSimulacao(java.lang.Long)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumoParaSimulacao(Long chavePrimariaPontoConsumo) throws NegocioException {

		Collection<HistoricoConsumo> listaHistoricoConsumo = null;
		listaHistoricoConsumo = this.consultarHistoricoConsumo(chavePrimariaPontoConsumo, true, null, null);
		if (listaHistoricoConsumo == null || listaHistoricoConsumo.isEmpty()) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_HISTORICO_CONSUMO_NAO_ENCONTRADO_PONTO_CONSUMO, true);
		}
		return listaHistoricoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#obterHistoricoConsumoParaSimulacao(long, java.lang.String[])
	 */
	@Override
	public HistoricoConsumo obterHistoricoConsumoParaSimulacao(long chavePrimaria, String... propriedadesLazy) throws GGASException {

		HistoricoConsumo historicoConsumo = (HistoricoConsumo) this.obter(chavePrimaria, propriedadesLazy);

		if (historicoConsumo.getHistoricoAtual() == null) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_DADOS_MEDICAO, true);
		} else if (historicoConsumo.getHistoricoAnterior() == null
				&& historicoConsumo.getHistoricoAtual().getHistoricoInstalacaoMedidor() == null) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_DADOS_MEDICAO, true);
		}

		if (historicoConsumo.getHistoricoAtual().getDataLeituraFaturada() == null) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_DADOS_MEDICAO, true);
		}

		return historicoConsumo;
	}

	/**
	 * Método responsável por obter o consumo apurado dos pontos de consumo por referência que tem o faturamento agrupado.
	 *
	 * @param anoMesFaturamento the ano mes faturamento
	 * @param ciclo the ciclo
	 * @param listaPontoConsumoAgrupados the lista ponto consumo agrupados
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	@Override
	public BigDecimal obterConsumoApuradoPontoConsumoPorReferencia(int anoMesFaturamento, int ciclo,
			Collection<PontoConsumo> listaPontoConsumoAgrupados) throws GGASException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select sum(consumoApurado) ");
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" WHERE ");
		hql.append(" historico.habilitado = true and historico.pontoConsumo.chavePrimaria in (:idsPontosConsumo) ");
		hql.append(" and historico.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and historico.numeroCiclo = :ciclo ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("idsPontosConsumo", Util.collectionParaArrayChavesPrimarias(listaPontoConsumoAgrupados));
		query.setInteger(CONSTANTE_ANO_MES_FATURAMENTO, anoMesFaturamento);
		query.setInteger("ciclo", ciclo);

		return (BigDecimal) query.uniqueResult();
	}

	/**
	 * Método responsável por obter o consumo dos pontos de consumo por referência que tem o faturamento agrupado.
	 *
	 * @param anoMesFaturamento the ano mes faturamento
	 * @param ciclo the ciclo
	 * @param listaPontoConsumoAgrupados the lista ponto consumo agrupados
	 * @return the big decimal
	 * @throws GGASException the GGAS exception
	 */
	@Override
	public BigDecimal obterConsumoPontoConsumoPorReferencia(int anoMesFaturamento, int ciclo,
			Collection<PontoConsumo> listaPontoConsumoAgrupados) throws GGASException {
		StringBuilder hql = new StringBuilder();

		hql.append(" select sum(consumo) ");
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" WHERE ");
		hql.append(" historico.habilitado = true and historico.pontoConsumo.chavePrimaria in (:idsPontosConsumo) ");
		hql.append(" and historico.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and historico.numeroCiclo = :ciclo ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("idsPontosConsumo", Util.collectionParaArrayChavesPrimarias(listaPontoConsumoAgrupados));
		query.setInteger(CONSTANTE_ANO_MES_FATURAMENTO, anoMesFaturamento);
		query.setInteger("ciclo", ciclo);

		return (BigDecimal) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# obterUltimoHistoricoConsumoNaoFaturado(java. lang.Long)
	 */
	@Override
	public HistoricoConsumo obterHistoricoConsumo(Long idPontoConsumo, Boolean indicadorFaturamento, boolean indicadorConsumoCiclo,
			boolean indicadorUltimo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" left join fetch historico.historicoAnterior historicoAnterior ");
		hql.append(" left join fetch historico.historicoAtual historicoAtual ");
		hql.append(" left join fetch historico.tipoConsumo tipoConsumo ");
		hql.append(" where ");
		hql.append(" historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		if (indicadorFaturamento != null) {
			hql.append(" and historico.indicadorFaturamento = :indicadorFaturamento ");
		}
		hql.append(" and historico.indicadorConsumoCiclo = :indicadorConsumoCiclo ");
		hql.append(" and historico.habilitado = true ");

		if (indicadorUltimo) {
			hql.append(" order by historico.anoMesFaturamento asc, historico.numeroCiclo asc ");
		} else {
			hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo);
		if (indicadorFaturamento != null) {
			query.setBoolean(INDICADOR_FATURAMENTO, indicadorFaturamento);
		}
		query.setBoolean(INDICADOR_CONSUMO_CICLO, indicadorConsumoCiclo);
		query.setMaxResults(1);

		return (HistoricoConsumo) query.uniqueResult();
	}

	/**
	 * Consulta os históricos de consumo não faturados, por chave primária do ponto de consumo, ordenados por ano/mês faturamento e numero do
	 * ciclo de faturamento.
	 *
	 * @param listaChavesPontoConsumo
	 * @return mapa de lista de HistoricoConsumo por chave de PontoConsumo
	 */
	@Override
	public Map<Long, HistoricoConsumo> obterHistoricosDeConsumoNaoFaturados(List<Long> listaChavesPontoConsumo,
			Integer anoMesFaturamento, Integer numeroCiclo) {

		Map<Long, HistoricoConsumo> mapaHistoricoPorChavePontoConsumo = new HashMap<>();

		if (CollectionUtils.isNotEmpty(listaChavesPontoConsumo)) {
			StringBuilder hql = new StringBuilder();
			hql.append(" select pontoConsumo.chavePrimaria, ");
			hql.append(" historico ");
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" historico ");
			hql.append(" left join fetch historico.historicoAnterior historicoAnterior ");
			hql.append(" left join fetch historico.historicoAtual historicoAtual ");
			hql.append(" left join fetch historico.tipoConsumo tipoConsumo ");
			hql.append(" inner join historico.pontoConsumo pontoConsumo ");
			hql.append(" where ");
			hql.append(" pontoConsumo.chavePrimaria IN (:chavesPrimarias) ");
			hql.append(" and historico.indicadorFaturamento = :indicadorFaturamento ");
			hql.append(" and historico.indicadorConsumoCiclo = :indicadorConsumoCiclo ");
			hql.append(" and historico.habilitado = true ");
			
			if (anoMesFaturamento != null) {
				hql.append(" and historico.anoMesFaturamento = :anoMesFaturamento ");
			}
			if (numeroCiclo != null) {
				hql.append(" and historico.numeroCiclo = :numeroCiclo ");
			}
			
			hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setBoolean(INDICADOR_FATURAMENTO, Boolean.FALSE);
			query.setBoolean(INDICADOR_CONSUMO_CICLO, Boolean.TRUE);
			
			if (anoMesFaturamento != null) {
				query.setInteger(CONSTANTE_ANO_MES_FATURAMENTO, anoMesFaturamento);
			}
			if (numeroCiclo != null) {
				query.setInteger(NUMERO_CICLO, numeroCiclo);
			}
			
			query.setParameterList(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS, listaChavesPontoConsumo);
			
			mapaHistoricoPorChavePontoConsumo.putAll(this.montarMapaHistoricoPorChavePontoConsumo(query.list()));
		}

		return mapaHistoricoPorChavePontoConsumo;
	}

	/**
	 * Constrói um mapa de históricos de consumo faturados, por chave primária do ponto de consumo, ordenados por ano/mês faturamento e
	 * numero do ciclo de faturamento.
	 *
	 * @param chavesPontoConsumo
	 * @return mapa de lista de HistoricoConsumo por chave de PontoConsumo
	 */
	private Map<Long, HistoricoConsumo> montarMapaHistoricoPorChavePontoConsumo(List<Object[]> atributos) {

		Map<Long, HistoricoConsumo> mapaHistoricoPorChavePontoConsumo = new HashMap<>();
		for (Object[] dadosHistoricoConsumo : atributos) {
			Long idPontoConsumo = (Long) dadosHistoricoConsumo[CONSTANTE_NUMERO_ZERO];
			HistoricoConsumo historicoConsumo = (HistoricoConsumo) dadosHistoricoConsumo[1];
			if (!mapaHistoricoPorChavePontoConsumo.containsKey(idPontoConsumo)) {
				mapaHistoricoPorChavePontoConsumo.put(idPontoConsumo, historicoConsumo);
			}
		}
		return mapaHistoricoPorChavePontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#obterHistoricoConsumoTodos(java.lang.Long, boolean, boolean, boolean)
	 */
	@Override
	public List<HistoricoConsumo> obterHistoricoConsumoTodos(Long idPontoConsumo, boolean indicadorFaturamento,
			boolean indicadorConsumoCiclo, boolean indicadorUltimo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" left join fetch historico.historicoAnterior historicoAnterior ");
		hql.append(" left join fetch historico.historicoAtual historicoAtual ");
		hql.append(" where ");
		hql.append(" historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and historico.indicadorFaturamento = :indicadorFaturamento ");
		hql.append(" and historico.indicadorConsumoCiclo = :indicadorConsumoCiclo ");
		hql.append(" and historico.habilitado = true ");

		if (indicadorUltimo) {
			hql.append(" order by historico.anoMesFaturamento asc, historico.numeroCiclo asc ");
		} else {
			hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo);
		query.setBoolean(INDICADOR_FATURAMENTO, indicadorFaturamento);
		query.setBoolean(INDICADOR_CONSUMO_CICLO, indicadorConsumoCiclo);

		return query.list();
	}

	@Cacheable("ultimoHistoricoConsumo")
	@Override
	public HistoricoConsumo obterUltimoHistoricoConsumoPorPontoConsumo(Long idPontoConsumo, Boolean isFaturado) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico.historicoAtual as historicoAtual, ");
		hql.append(" historico.consumoApurado as consumoApurado, historico.diasConsumo as diasConsumo, ");
		hql.append(" historico.consumo as consumo ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" where ");
		hql.append(" historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");
		hql.append(" and historico.habilitado = true ");

		if (isFaturado != null) {
			hql.append(" and historico.indicadorFaturamento = :indicadorFaturamento ");
		}

		hql.append(
				" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc, historico.historicoAtual.dataLeituraFaturada desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo);

		if (isFaturado != null) {
			query.setBoolean(INDICADOR_FATURAMENTO, isFaturado);
		}
		HistoricoConsumo retorno = null;

		query.setMaxResults(1);
		query.setResultTransformer(Transformers.aliasToBean(HistoricoConsumoImpl.class));
		Collection<HistoricoConsumo> historicos = query.list();

		if (!historicos.isEmpty()) {
			retorno = historicos.iterator().next();
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# validarPeriodicidadeHistoricoConsumo
	 * (br.com.ggas.medicao.consumo.HistoricoConsumo )
	 */
	@Override
	public void validarPeriodicidadeHistoricoConsumo(HistoricoConsumo historicoConsumo) throws NegocioException {

		PontoConsumo pontoConsumo = historicoConsumo.getPontoConsumo();
		Periodicidade periodicidade = pontoConsumo.getSegmento().getPeriodicidade();

		if (periodicidade.getNumeroMinimoDiasCiclo() != null && periodicidade.getNumeroMaximoDiasCiclo() != null
				&& periodicidade.getNumeroMinimoDiasCiclo() > historicoConsumo.getDiasConsumo()
				|| periodicidade.getNumeroMaximoDiasCiclo() < historicoConsumo.getDiasConsumo()) {
			throw new NegocioException(ERRO_NEGOCIO_DIAS_CONSUMO_FORA_PERIODICIDADE, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#verificaCreditoVolume(br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public HistoricoConsumo verificaCreditoVolume(PontoConsumo pontoConsumo, Integer referenciaFaturamento, Integer cicloFaturamento)
			throws NegocioException {

		HistoricoConsumo retorno = null;

		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, pontoConsumo.getChavePrimaria()));
		criteria.add(Restrictions.eq(CONSTANTE_ANO_MES_FATURAMENTO, referenciaFaturamento));
		criteria.add(Restrictions.eq(NUMERO_CICLO, cicloFaturamento));
		criteria.add(Restrictions.eq(INDICADOR_CONSUMO_CICLO, true));

		List<HistoricoConsumo> historicos = criteria.list();
		if (historicos != null && !historicos.isEmpty()) {
			retorno = historicos.get(CONSTANTE_NUMERO_ZERO);
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo. ControladorHistoricoConsumo #consultarHistoricoConsumosPendentes (java.lang.Integer,
	 * java.lang.Integer, java.util.Collection)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumosPendentes(Integer referencia, Integer ciclo,
			Collection<PontoConsumo> pontosConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select historicoConsumo.anoMesFaturamento as anoMesFaturamento, ");
		hql.append(" historicoConsumo.numeroCiclo as numeroCiclo, ");
		hql.append(" historicoConsumo.chavePrimaria as chavePrimaria ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historicoConsumo ");
		hql.append(" where historicoConsumo.pontoConsumo.chavePrimaria in ( :chavePontoConsumo ) ");
		hql.append(" and historicoConsumo.habilitado is true ");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo is true ");
		hql.append(" and historicoConsumo.indicadorFaturamento is false ");
		hql.append(" and ( ");
		hql.append(" 	historicoConsumo.anoMesFaturamento < :referencia ");
		hql.append("	or (historicoConsumo.anoMesFaturamento = :referencia  and historicoConsumo.numeroCiclo < :ciclo) ");
		hql.append(" ) ");
		hql.append(" order by historicoConsumo.anoMesFaturamento, historicoConsumo.numeroCiclo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidade()));
		query.setParameterList("chavePontoConsumo", Util.collectionParaArrayChavesPrimarias(pontosConsumo));
		query.setInteger("referencia", referencia);
		query.setInteger("ciclo", ciclo);

		return query.list();

	}

	/**
	 * Verificar mudanca titularidade contrato.
	 *
	 * @param historicoConsumo the historico consumo
	 * @return the boolean
	 * @throws NegocioException the negocio exception
	 */
	private Boolean verificarMudancaTitularidadeContrato(HistoricoConsumo historicoConsumo) throws NegocioException {

		Date dataMudanca = null;

		if (historicoConsumo.getHistoricoAnterior() != null) {

			ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);

			dataMudanca = controladorContrato.verificarMudancaTitularidadeContratoPontoConsumo(historicoConsumo.getPontoConsumo(),
					historicoConsumo.getHistoricoAnterior().getDataLeituraFaturada(), // Data
																						// inicial
					historicoConsumo.getHistoricoAtual().getDataLeituraFaturada());
			// Data
			// final

		}

		return dataMudanca != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo. ControladorHistoricoConsumo #pesquisarImovelHistoricoLeituraConsumo (java.util.Map)
	 */
	@Override
	public Collection<Imovel> pesquisarImovelHistoricoLeituraConsumo(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = this.createCriteria(Imovel.class);

		if (filtro.get("matricula") != null) {
			criteria.add(Restrictions.idEq(Long.valueOf((String) filtro.get("matricula"))));
			criteria.createAlias("listaPontoConsumo", "listaPontoConsumo", Criteria.LEFT_JOIN);
		} else {
			criteria.createAlias("listaPontoConsumo", "listaPontoConsumo", Criteria.INNER_JOIN);
		}

		criteria.createAlias("listaPontoConsumo.rota", "rota", Criteria.LEFT_JOIN);
		criteria.createAlias("quadraFace", "quadraFace");
		criteria.createAlias("quadraFace.quadra", "quadra");
		criteria.createAlias("quadra.setorComercial", "setorComercial");
		criteria.createAlias("setorComercial.localidade", "localidade");

		if (filtro.get("idCliente") != null) {
			criteria.createCriteria("listaClienteImovel").createCriteria("cliente")
					.add(Restrictions.eq("chavePrimaria", filtro.get("idCliente")));
		}

		if (filtro.get("nome") != null) {
			criteria.add(Restrictions.ilike("nome", Util.formatarTextoConsulta((String) filtro.get("nome"))));
		}

		if (filtro.get("complementoImovel") != null) {
			criteria.add(Restrictions.ilike("descricaoComplemento", Util.formatarTextoConsulta((String) filtro.get("complementoImovel"))));
		}

		if (filtro.get("numeroImovel") != null) {
			criteria.add(Restrictions.eq("numeroImovel", filtro.get("numeroImovel")));
		}

		if (filtro.get("indicadorCondominioAmbos") != null) {

			String indicadorCondominio = (String) filtro.get("indicadorCondominioAmbos");

			if ("true".equalsIgnoreCase(indicadorCondominio)) {

				criteria.add(Restrictions.eq("condominio", Boolean.TRUE));

			} else if ("false".equalsIgnoreCase(indicadorCondominio)) {

				criteria.add(Restrictions.eq("condominio", Boolean.FALSE));

			} else if ("ambos".equalsIgnoreCase(indicadorCondominio)) {

				criteria.add(Restrictions.or(Restrictions.eq("condominio", Boolean.TRUE), Restrictions.eq("condominio", Boolean.FALSE)));

			}

		}

		if (filtro.get("habilitado") != null) {

			String habilitado = (String) filtro.get("habilitado");

			if ("true".equalsIgnoreCase(habilitado)) {

				criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));

			} else if ("false".equalsIgnoreCase(habilitado)) {

				criteria.add(Restrictions.eq("habilitado", Boolean.FALSE));

			} else if ("ambos".equalsIgnoreCase(habilitado)) {

				criteria.add(Restrictions.or(Restrictions.eq("condominio", Boolean.TRUE), Restrictions.eq("condominio", Boolean.FALSE)));

			}
		}

		if (filtro.get("cepImovel") != null) {
			criteria.createCriteria("quadraFace").createCriteria("endereco").createCriteria("cep")
					.add(Restrictions.eq("cep", filtro.get("cepImovel")));
		}

		if (filtro.get(CONSTANTE_ID_GRUPO_FATURAMENTO) != null) {
			criteria.add(Restrictions.eq("rota.grupoFaturamento.chavePrimaria", filtro.get(CONSTANTE_ID_GRUPO_FATURAMENTO)));
		}

		if (filtro.get(CONSTANTE_ID_ROTA) != null) {
			criteria.add(Restrictions.eq("rota.chavePrimaria", filtro.get(CONSTANTE_ID_ROTA)));
		}

		String idPontoConsumoLegado = (String) filtro.get("pontoConsumoLegado");
		if (idPontoConsumoLegado != null && !idPontoConsumoLegado.isEmpty()) {
			Long idPontoConsumoLegadoLong = Long.valueOf(idPontoConsumoLegado);

			criteria.add(Restrictions.or(Restrictions.eq("listaPontoConsumo.codigoLegado", idPontoConsumoLegado),
					Restrictions.eq("listaPontoConsumo.chavePrimaria", idPontoConsumoLegadoLong)));

		}

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		criteria.addOrder(Order.asc("nome"));
		criteria.addOrder(Order.asc("descricaoComplemento"));

		Collection<Imovel> collImovel = criteria.list();

		if (collImovel != null && !collImovel.isEmpty() && collImovel.size() == 1) {

			Imovel imovel = collImovel.iterator().next();

			if (imovel.getListaPontoConsumo() == null || imovel.getListaPontoConsumo().isEmpty()) {
				throw new NegocioException(ERRO_NEGOCIO_IMOVEL_SEM_PONTO_CONSUMO, true);
			}

		}

		// TODO o trecho de codigo abaixo deve ser
		// retirado assim que ajustar a consulta
		Collection<Imovel> collImovelRemover = new ArrayList<Imovel>();
		if (collImovel != null) {
			for (Imovel imovel : collImovel) {
				Long[] chavesPrimariaPontoConsumo = new Long[imovel.getListaPontoConsumo().size()];
				int i = CONSTANTE_NUMERO_ZERO;
				for (PontoConsumo pontoConsumo : imovel.getListaPontoConsumo()) {
					chavesPrimariaPontoConsumo[i] = pontoConsumo.getChavePrimaria();
					i++;
				}

				Collection<HistoricoConsumo> listaHistoricoConsumo = consultarHistoricoConsumo(chavesPrimariaPontoConsumo);
				if (listaHistoricoConsumo.isEmpty()) {
					collImovelRemover.add(imovel);
				}
			}
			collImovel.removeAll(collImovelRemover);
		}
		// -------------------------------------

		return collImovel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo. ControladorHistoricoConsumo #obterUltimoHistoricoConsumoFaturado (java.lang.Long)
	 */
	@Override
	public HistoricoConsumo obterUltimoHistoricoConsumoFaturado(Long idPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" left join fetch historico.historicoAnterior historicoAnterior ");
		hql.append(" left join fetch historico.historicoAtual historicoAtual ");
		hql.append(" where ");
		hql.append(" historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and historico.indicadorFaturamento = true ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");
		hql.append(" and historico.habilitado = true ");

		hql.append(" order by historicoAtual.dataLeituraFaturada desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo);
		query.setMaxResults(1);

		return (HistoricoConsumo) query.uniqueResult();
	}

	/**
	 * Obtém o último histórico de consumo faturado por ponto de consumo. Obtém os seguintes atributos de histórico de consumo:
	 * {@code chavePrimaria}, {@code consumoCreditoMedia}.
	 *
	 * @param chavesPontoConsumo
	 * @return mapa de HistoricoConsumo por PontoConsumo
	 */
	@Override
	public Map<PontoConsumo, HistoricoConsumo> obterUltimoHistoricoConsumoFaturado(Long[] chavesPontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico.chavePrimaria, pontoConsumo.chavePrimaria, ");
		hql.append(" historico.consumoCreditoMedia ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" left join  historico.historicoAnterior historicoAnterior ");
		hql.append(" left join  historico.historicoAtual historicoAtual ");
		hql.append(" inner join historico.pontoConsumo pontoConsumo ");
		hql.append(" where ");
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontoConsumo));
		
		hql.append(" and historico.indicadorFaturamento = true ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");
		hql.append(" and historico.habilitado = true ");

		hql.append(" order by historicoAtual.dataLeituraFaturada desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

		return this.construirMapaUltimoHistoricoConsumoFaturadoPorPontoConsumo(query.list());
	}
	
	/**
	 * Constrói um mapa de histórico de consumo por ponto de consumo a partir dos atributos passados por parâmetro.
	 * 
	 * @param listaAtributos
	 * @return mapa de HistoricoConsumo por PontoConsumo
	 */
	private Map<PontoConsumo, HistoricoConsumo> construirMapaUltimoHistoricoConsumoFaturadoPorPontoConsumo(List<Object[]> listaAtributos) {
		Map<PontoConsumo, HistoricoConsumo> mapaUltimoHistoricoConsumoFaturado = new HashMap<>();
		for (Object[] atributos : listaAtributos) {
			HistoricoConsumo historicoConsumo = (HistoricoConsumo) criar();
			historicoConsumo.setChavePrimaria((long) atributos[CONSTANTE_NUMERO_ZERO]);
			PontoConsumo pontoConsumo = new PontoConsumoImpl();
			pontoConsumo.setChavePrimaria((long) atributos[1]);
			historicoConsumo.setConsumoCreditoMedia((BigDecimal) atributos[2]);
			if (!mapaUltimoHistoricoConsumoFaturado.containsKey(pontoConsumo)) {
				mapaUltimoHistoricoConsumoFaturado.put(pontoConsumo, historicoConsumo);
			}
		}
		return mapaUltimoHistoricoConsumoFaturado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo. ControladorHistoricoConsumo #listarTipoComposicaoConsumo()
	 */
	@Override
	public Collection<TipoComposicaoConsumo> listarTipoComposicaoConsumo() throws NegocioException {

		Criteria criteria = this.createCriteria(TipoComposicaoConsumo.class);
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo. ControladorHistoricoConsumo #listarTipoComposicaoConsumo()
	 */
	@Override
	public Collection<PontoConsumo> removerPontosConsumoDaListaSemHistoricoConsumo(Collection<PontoConsumo> listaPontosConsumo)
			throws NegocioException {

		Collection<PontoConsumo> listaPontoConsumoComHistorico = new ArrayList<PontoConsumo>();
		for (PontoConsumo pontoConsumo : listaPontosConsumo) {
			Collection<HistoricoConsumo> listaHistoricoConsumo = this.consultarHistoricoConsumo(pontoConsumo.getChavePrimaria());

			if (listaHistoricoConsumo != null && !listaHistoricoConsumo.isEmpty()) {
				listaPontoConsumoComHistorico.add(pontoConsumo);
			}
		}

		return listaPontoConsumoComHistorico;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo. ControladorHistoricoConsumo #verificarViradaMedidor(java.lang.Long, java.math.BigDecimal)
	 */
	@Override
	public Boolean verificarViradaMedidor(Long idPontoConsumo, BigDecimal leitura, Date data) throws NegocioException {

		Boolean retorno = null;

		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 59);

		Criteria criteria = this.createCriteria(HistoricoMedicao.class);
		criteria.setProjection(Projections.max("chavePrimaria"));
		criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, idPontoConsumo));
		criteria.add(Restrictions.le("dataLeituraInformada", cal.getTime()));
		criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));

		Long idHistoricoMedicao = (Long) criteria.uniqueResult();

		if (idHistoricoMedicao != null) {

			criteria = this.createCriteria(HistoricoMedicao.class);
			criteria.add(Restrictions.idEq(idHistoricoMedicao));

			HistoricoMedicao historicoMedicao = (HistoricoMedicao) criteria.uniqueResult();

			if (historicoMedicao.getNumeroLeituraFaturada() != null
					&& leitura.compareTo(historicoMedicao.getNumeroLeituraFaturada()) < CONSTANTE_NUMERO_ZERO) {

				criteria = this.createCriteria(HistoricoConsumo.class);
				criteria.add(Restrictions.eq("historicoAtual.chavePrimaria", historicoMedicao.getChavePrimaria()));
				criteria.add(Restrictions.eq(INDICADOR_CONSUMO_CICLO, Boolean.TRUE));
				criteria.add(Restrictions.eq("habilitado", Boolean.TRUE));

				HistoricoConsumo historicoConsumo = (HistoricoConsumo) criteria.uniqueResult();

				retorno = this.isViradaMedidor(historicoMedicao, leitura, historicoConsumo, data);

			}

		}

		return retorno;
	}

	/**
	 * Checks if is virada medidor.
	 *
	 * @param historicoMedicao the historico medicao
	 * @param leituraAtualPonto the leitura atual ponto
	 * @param historicoConsumo the historico consumo
	 * @param data the data
	 * @return the boolean
	 */
	private Boolean isViradaMedidor(HistoricoMedicao historicoMedicao, BigDecimal leituraAtualPonto, HistoricoConsumo historicoConsumo,
			Date data) {

		Boolean retorno = Boolean.FALSE;
		BigDecimal consumoLido = BigDecimal.ZERO;

		historicoMedicao.setNumeroLeituraFaturada(historicoMedicao.getNumeroLeituraInformada());

		// 2.
		int digitoMedidor = historicoMedicao.getHistoricoInstalacaoMedidor().getMedidor().getDigito();
		BigDecimal calculo = (leituraAtualPonto.add(BigDecimal.valueOf(Math.pow(10, digitoMedidor))))
				.subtract(historicoMedicao.getNumeroLeituraFaturada());

		if (calculo != null) {

			calculo = calculo.setScale(8, RoundingMode.HALF_UP);

		}

		consumoLido = calculo;

		BigDecimal calculoVirada = BigDecimal.valueOf(Math.pow(10, digitoMedidor)).subtract(BigDecimal.valueOf(1));

		if (consumoLido != null && consumoLido.compareTo(calculoVirada) <= CONSTANTE_NUMERO_ZERO) {

			retorno = Boolean.TRUE;
		}

		return retorno;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo. ControladorHistoricoConsumo #ConsultarPendenciaFaturamentoAnormalidade (java.lang.Long)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarPendenciaFaturamentoAnormalidade(Long idPontoConsumo) throws NegocioException {

		// Para verificar se existe pendência de
		// faturamento: deve existir consumo não
		// faturado e
		// anormalidade de faturamento gerada e
		// ainda não tratada.

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico,  ");
		hql.append(getClasseHistoricoAnomaliaFaturamento().getSimpleName()).append(" historicoAnomalia ");
		hql.append(" where ");
		hql.append(" 	 historico.pontoConsumo.chavePrimaria = historicoAnomalia.pontoConsumo.chavePrimaria ");
		hql.append(" and historico.anoMesFaturamento = historicoAnomalia.anoMesFaturamento ");
		hql.append(" and historico.numeroCiclo = historicoAnomalia.numeroCiclo ");
		hql.append(" and historico.habilitado = true ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");
		hql.append(" and historico.indicadorFaturamento = false ");
		hql.append(" and historicoAnomalia.analisada = false ");
		hql.append(" and historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo. ControladorHistoricoConsumo# ConsultarPendenciaFaturamentoAnormalidadeMedicao (java.lang.Long)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarPendenciaFaturamentoAnormalidadeMedicao(Long idPontoConsumo) throws NegocioException {

		// Para verificar pendência de leitura:
		// deve existir consumo não faturado com
		// anormalidade de consumo
		// que bloqueia o faturamento e esteja
		// pendente de análise para algum ponto de
		// consumo do contrato.
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico,  ");
		hql.append(getClasseAnomalidadeConsumo().getSimpleName()).append(" anomalidadeConsumo, ");
		hql.append(getClasseHistoricoMedicao().getSimpleName()).append(" historicoMedicao ");
		hql.append(" where ");
		hql.append(" 	historico.pontoConsumo.chavePrimaria = historicoMedicao.pontoConsumo.chavePrimaria ");
		hql.append(" and historico.historicoAtual.chavePrimaria = historicoMedicao.chavePrimaria ");
		hql.append(" and historico.anormalidadeConsumo.chavePrimaria = anomalidadeConsumo.chavePrimaria ");

		hql.append(" and historico.habilitado = true ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");
		hql.append(" and historico.indicadorFaturamento = false ");
		hql.append(" and historicoMedicao.analisada = false ");
		hql.append(" and anomalidadeConsumo.bloquearFaturamento = true ");

		hql.append(" and historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarHistoricoConsumoPorLeituraMovimento(br.com.ggas.medicao.leitura.
	 * LeituraMovimento )
	 */
	// TKT 4233
	@Override
	public HistoricoConsumo consultarHistoricoConsumoPorLeituraMovimento(LeituraMovimento filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade(), "historico");
		criteria.createAlias("historico.historicoAtual", "historicoAtual", Criteria.INNER_JOIN);

		criteria.add(Restrictions.eq("historico.habilitado", Boolean.TRUE));

		if (filtro != null) {
			if (filtro.getAnoMesFaturamento() != null && filtro.getAnoMesFaturamento() > CONSTANTE_NUMERO_ZERO) {
				criteria.add(Restrictions.eq("historico.anoMesFaturamento", filtro.getAnoMesFaturamento()));
			}
			if (filtro.getCiclo() != null && filtro.getCiclo() > CONSTANTE_NUMERO_ZERO) {
				criteria.add(Restrictions.eq("historico.numeroCiclo", filtro.getCiclo()));
			}
			if (filtro.getPontoConsumo() != null && filtro.getPontoConsumo().getChavePrimaria() > CONSTANTE_NUMERO_ZERO) {
				criteria.add(Restrictions.eq("historico.pontoConsumo.chavePrimaria", filtro.getPontoConsumo().getChavePrimaria()));
			}
			if (filtro.getDataLeitura() != null) {
				criteria.add(Restrictions.eq("historicoAtual.dataLeituraInformada", filtro.getDataLeitura()));
			}
			if (filtro.getValorLeitura() != null) {
				criteria.add(Restrictions.eq("historicoAtual.numeroLeituraInformada", filtro.getValorLeitura()));
			}
		}

		criteria.addOrder(Order.desc("historico.anoMesFaturamento"));
		criteria.addOrder(Order.desc("historico.numeroCiclo"));

		HistoricoConsumo retorno = null;
		Collection<HistoricoConsumo> historicos = criteria.list();
		if (!historicos.isEmpty()) {
			retorno = historicos.iterator().next();
		}

		return retorno;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarHistoricoAgrupadoAnoMes(java.lang.Long, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public Collection<Object> consultarHistoricoAgrupadoAnoMes(Long chavePrimariaContrato, Date dataLeituraMenorInformada,
			Date dataLeituraMaiorInformada) throws NegocioException {

		DetachedCriteria subquery = DetachedCriteria.forClass(getClasseEntidadeContratoPontoConsumo(), "contratoPontoConsumo");
		subquery.add(Restrictions.eq("contratoPontoConsumo.contrato.chavePrimaria", chavePrimariaContrato));
		subquery.createAlias("contratoPontoConsumo.pontoConsumo", "pontoConsumo");
		subquery.setProjection(Projections.property(PONTO_CONSUMO_CHAVE_PRIMARIA));

		Criteria criteria = createCriteria(getClasseEntidade(), "historico");

		criteria.add(Subqueries.propertyIn("historico.pontoConsumo.chavePrimaria", subquery));
		criteria.setFetchMode("historico.anoMesFaturamento", FetchMode.SELECT);

		criteria.createAlias("historico.historicoAtual", "historicoMedicao");

		criteria.add(Restrictions.between("historico.ultimaAlteracao", dataLeituraMenorInformada, dataLeituraMaiorInformada));
		criteria.add(Restrictions.eq("historico.habilitado", Boolean.TRUE));

		criteria.setProjection(Projections.projectionList()
				.add(Projections.groupProperty("historico.anoMesFaturamento").as("anoMesLeitura"))
				.add(Projections.sum("historico.consumo").as("comsumo")).add(Projections.sum("historico.consumoApurado").as("consumoReal"))
				// .add(Projections.avg("historico.fatorPTZCorretor").as("fatorPTZ"))
				.add(Projections.sum("historicoMedicao.numeroLeituraInformada").as("numeroLeituraInformada"))
				.add(Projections.avg("historicoMedicao.fatorPTZCorretor").as("fatorPTZCorretor")));

		criteria.addOrder(Order.asc("historico.anoMesFaturamento"));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarProximoMedicaoHistorico(java.lang.Long, java.util.Date)
	 */
	@Override
	public HistoricoConsumo consultarProximoMedicaoHistorico(Long chavePrimariaPontoConsumo, Date dataFimApuracao) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select historicoConsumo ");
		hql.append(" FROM ");
		hql.append(" HistoricoConsumoImpl historicoConsumo ");
		hql.append(" inner join historicoConsumo.historicoAtual historicoMedicao ");
		hql.append(" WHERE ");
		hql.append(" historicoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and historicoMedicao.dataLeituraInformada >= :data ");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo = true ");
		hql.append(" and historicoConsumo.habilitado = true ");
		hql.append(" order by historicoMedicao.dataLeituraInformada asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", chavePrimariaPontoConsumo);
		Util.adicionarRestricaoDataSemHora(query, dataFimApuracao, "data", Boolean.FALSE);
		query.setMaxResults(1);

		return (HistoricoConsumo) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarHistoricoMedicao(java.lang.Long, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoMedicao(Long chavePrimaria, Date dataLeituraMenorInformada,
			Date dataLeituraMaiorInformada) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" where 1 = 1 ");
		hql.append(" and historico.historicoAtual.dataLeituraInformada ");
		hql.append(" between :dataLeituraMenorInformada and :dataLeituraMaiorInformada");
		hql.append(" and historico.pontoConsumo.chavePrimaria = :chavePrimaria");
		hql.append(" and historico.habilitado = true ");
		hql.append(" order by anoMesFaturamento ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setDate("dataLeituraMenorInformada", dataLeituraMenorInformada);
		query.setDate("dataLeituraMaiorInformada", dataLeituraMaiorInformada);
		query.setLong("chavePrimaria", chavePrimaria);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarUltimoHistoricoConsumo(long)
	 */
	@Override
	public HistoricoConsumo consultarUltimoHistoricoConsumo(long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo");
		hql.append(" left join fetch historico.historicoAtual historicoAtual");
		hql.append(" left join fetch historico.historicoAnterior historicoAnterior");
		hql.append(" left join fetch historico.anormalidadeConsumo anormalidadeConsumo");
		hql.append(" inner join fetch historico.tipoConsumo tipoConsumo");
		hql.append(" where pontoConsumo.chavePrimaria = ? ");
		hql.append(" and historico.habilitado = true ");
		hql.append(" order by historico.ultimaAlteracao desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CONSTANTE_NUMERO_ZERO, chavePrimaria);

		return (HistoricoConsumo) query.list().get(CONSTANTE_NUMERO_ZERO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#inativarHistoricoConsumo(java.lang.Long)
	 */
	@Override
	public void inativarHistoricoConsumo(Long idHistoricoMedicao) throws NegocioException, ConcorrenciaException {

		HistoricoConsumo historico = this.consultarHistoricoConsumoPorHistoricoMedicao(idHistoricoMedicao);

		if (historico.getIndicadorFaturamento().equals(false)) {
			historico.setHabilitado(false);
			atualizar(historico);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarHistoricoConsumoPorPontoConsumo(java.lang.Long,
	 * java.lang.Integer)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumoPorPontoConsumo(Long idPontoConsumo, Integer maxResult) {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" hc ");
		hql.append(" where hc.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" order by hc.chavePrimaria desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("idPontoConsumo", idPontoConsumo);

		if (maxResult != null) {
			query.setMaxResults(maxResult);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# consultarHistoricoConsumo(java.lang.Long)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumoPorHistoricoConsumoSintetizador(
			HistoricoConsumo historicoConsumoSintetizador) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo");
		hql.append(" left join fetch historico.historicoAtual historicoAtual");
		hql.append(" left join fetch historico.anormalidadeConsumo anormalidadeConsumo");
		hql.append(" inner join fetch historico.tipoConsumo tipoConsumo");
		hql.append(" where historico.historicoConsumoSintetizador.chavePrimaria = ? ");
		hql.append(" and historico.habilitado = true ");
		hql.append(" order by historico.historicoAtual.chavePrimaria asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CONSTANTE_NUMERO_ZERO, historicoConsumoSintetizador.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#obterAnteriorHistoricoConsumoFaturado(java.lang.Long, java.lang.Long)
	 */
	@Override
	public HistoricoConsumo obterAnteriorHistoricoConsumoFaturado(Long idPontoConsumo, Long idMedicaoAnterior) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" historico ");
		hql.append(" inner join fetch historico.historicoAtual historicoAtual ");
		hql.append(" where ");
		hql.append(" historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and historico.indicadorFaturamento = true ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");
		hql.append(" and historico.habilitado = true ");
		hql.append(" and historicoAtual.chavePrimaria = :idMedicaoAnterior ");
		hql.append(" order by historicoAtual.dataLeituraFaturada desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo);
		query.setLong("idMedicaoAnterior", idMedicaoAnterior);
		query.setMaxResults(1);

		return (HistoricoConsumo) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#obterHistoricoConsumoPorPontoEData(java.lang.Long, java.util.Date)
	 */
	@Override
	public HistoricoConsumo obterHistoricoConsumoPorPontoEData(Long idPontoConsumo, Date data) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select historicoConsumo ");
		hql.append(" FROM ");
		hql.append(" HistoricoConsumoImpl historicoConsumo ");
		hql.append(" inner join historicoConsumo.historicoAtual historicoMedicao ");
		hql.append(" WHERE ");
		hql.append(" historicoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and trunc(historicoMedicao.dataLeituraInformada) = trunc(:data) ");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo = false ");
		hql.append(" and historicoConsumo.habilitado = true ");
		hql.append(" order by historicoMedicao.dataLeituraInformada desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo);
		query.setDate("data", data);
		query.setMaxResults(1);

		return (HistoricoConsumo) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente(java.lang
	 * .Long)
	 */
	@Override
	public BigDecimal consultarTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente(Long idPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select sum(historicoConsumo.consumoApurado) ");
		hql.append(" FROM ");
		hql.append(" HistoricoConsumoImpl historicoConsumo ");
		hql.append(" inner join historicoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join pontoConsumo.rota rota ");
		hql.append(" inner join rota.grupoFaturamento grupoFaturamento ");
		hql.append(" WHERE ");
		hql.append(" pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and historicoConsumo.anoMesFaturamento = grupoFaturamento.anoMesReferencia ");
		hql.append(" and historicoConsumo.numeroCiclo = grupoFaturamento.numeroCiclo ");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo = :indicadorConsumoCiclo ");
		hql.append(" and historicoConsumo.indicadorDrawback = :indicadorDrawback ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", idPontoConsumo);
		query.setBoolean(INDICADOR_CONSUMO_CICLO, Boolean.TRUE);
		query.setBoolean("indicadorDrawback", Boolean.TRUE);

		return (BigDecimal) query.uniqueResult();
	}

	/**
	 * Obtém a soma do total de consumo apurado agrupado por ponto de consumo.
	 * 
	 * @param chavesPontosConsumo
	 * @return mapa de total de consumo apurado por ponto de consumo
	 */
	@Override
	public Map<PontoConsumo, BigDecimal> consultarTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente(Long[] chavesPontosConsumo) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select pontoConsumo.chavePrimaria, sum(historicoConsumo.consumoApurado) ");
		hql.append(" FROM ");
		hql.append(" HistoricoConsumoImpl historicoConsumo ");
		hql.append(" inner join historicoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join pontoConsumo.rota rota ");
		hql.append(" inner join rota.grupoFaturamento grupoFaturamento ");
		hql.append(" WHERE ");
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontosConsumo));
		hql.append(" and historicoConsumo.anoMesFaturamento = grupoFaturamento.anoMesReferencia ");
		hql.append(" and historicoConsumo.numeroCiclo = grupoFaturamento.numeroCiclo ");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo = :indicadorConsumoCiclo ");
		hql.append(" and historicoConsumo.indicadorDrawback = :indicadorDrawback ");
		hql.append(" group by pontoConsumo.chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		query.setBoolean(INDICADOR_CONSUMO_CICLO, Boolean.TRUE);
		query.setBoolean("indicadorDrawback", Boolean.TRUE);

		return this.construirMapaTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente(query.list());
	}

	/**
	 * Constrói um mapa de total de consumo apurado por ponto de consumo a partir dos atributos passados por parâmetro.
	 * 
	 * @param listaAtributos
	 * @return mapa de total de consumo apurado por ponto de consumo
	 */
	private Map<PontoConsumo, BigDecimal> construirMapaTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente(
			List<Object[]> listaAtributos) {

		Map<PontoConsumo, BigDecimal> mapa = new HashMap<>();
		for (Object[] atributos : listaAtributos) {
			PontoConsumo pontoConsumo = new PontoConsumoImpl();
			pontoConsumo.setChavePrimaria((long) atributos[CONSTANTE_NUMERO_ZERO]);
			BigDecimal somaConsumoApurado = (BigDecimal) atributos[1];
			if (!mapa.containsKey(pontoConsumo)) {
				mapa.put(pontoConsumo, somaConsumoApurado);
			}
		}
		return mapa;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consistirPontoConsumoFaturaComplementar(br.com.ggas.cadastro.imovel.
	 * PontoConsumo , br.com.ggas.contrato.contrato.ContratoPontoConsumo, java.util.Map, java.lang.StringBuilder, java.lang.Boolean,
	 * br.com.ggas.medicao.leitura.HistoricoMedicao)
	 */
	@Override
	public void consistirPontoConsumoFaturaComplementar(PontoConsumo pontoConsumo, ContratoPontoConsumo contratoPontoConsumo,
			Map<String, Integer> referenciaCicloAtual, StringBuilder logProcessamento, Boolean isFaturaAvulso, HistoricoMedicao historico)
			throws GGASException {
		Set<HistoricoMedicao> historicoMedicaoInserido = new HashSet<HistoricoMedicao>();
		
		this.consistirPontoConsumo(pontoConsumo, contratoPontoConsumo, referenciaCicloAtual, logProcessamento, isFaturaAvulso, historico,
				null, null, null, null, null, null, null, null, null, null, null, null, null, historicoMedicaoInserido);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarTotalConsumoApuradoAnoMesGrupoFaturamentoCorrente(java.lang.Long)
	 */
	@Override
	public BigDecimal consultarTotalConsumoApuradoAnoMesGrupoFaturamentoCorrente(Long[] idsPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select sum(historicoConsumo.consumoApurado) ");
		hql.append(" FROM ");
		hql.append(" HistoricoConsumoImpl historicoConsumo ");
		hql.append(" inner join historicoConsumo.pontoConsumo pontoConsumo ");
		hql.append(" inner join pontoConsumo.rota rota ");
		hql.append(" inner join rota.grupoFaturamento grupoFaturamento ");
		hql.append(" WHERE ");
		hql.append(" pontoConsumo.chavePrimaria in (:idsPontoConsumo) ");
		hql.append(" and historicoConsumo.anoMesFaturamento = grupoFaturamento.anoMesReferencia ");
		hql.append(" and historicoConsumo.numeroCiclo = grupoFaturamento.numeroCiclo ");
		hql.append(" and historicoConsumo.indicadorFaturamento = :indicadorFaturamento ");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo = :indicadorConsumoCiclo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("idsPontoConsumo", idsPontoConsumo);
		query.setBoolean(INDICADOR_FATURAMENTO, Boolean.TRUE);
		query.setBoolean(INDICADOR_CONSUMO_CICLO, Boolean.TRUE);

		return (BigDecimal) query.uniqueResult();
	}

	/**
	 * Realiza o agrupamento do historico de consumo por medidores independentes
	 *
	 * @param listaHistoricoConsumo
	 *
	 * @return map com listas agrupadas por cada medidor independente
	 */
	@Override
	public Map<Long, Collection<HistoricoConsumo>> agruparHistoricoConsumoPorMedidorIndependente(
			Collection<HistoricoConsumo> listaHistoricoConsumo) throws NegocioException {
		if (listaHistoricoConsumo != null && !listaHistoricoConsumo.isEmpty()) {
			return Util.agrupar(listaHistoricoConsumo, new Agrupador<HistoricoConsumo, Long>() {

				@Override
				public Long getChaveAgrupadora(HistoricoConsumo elemento) {
					return elemento.getHistoricoAtual().getHistoricoInstalacaoMedidor().getMedidor().getChavePrimaria();
				}
			});
		}
		return null;
	}

	/**
	 * Método para gerar relatório
	 * 
	 * @throws GGASException
	 */
	public StringBuilder criarRelatorioConsistirLeituraConsumo(Map<String, Object> parametros, Processo processo,
			StringBuilder logProcessamento) throws GGASException {

		logProcessamento.append("\r\n Obtendo controladores de parâmetro sistema e de processo de documento.");

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		ControladorProcessoDocumento controladorProcessoDocumento = ServiceLocator.getInstancia().getControladorProcessoDocumento();

		logProcessamento.append("\r\n Controladores obtidos, adicionado numeroCiclo e anoMesFaturamento para os parâmetros para "
				+ "gerar array de byte...");

		parametros.put(CICLO, numeroCiclo);
		parametros.put(CONSTANTE_ANO_MES_FATURAMENTO, anoMesFaturamento);
		byte[] relatorio = gerarRelatorioConsistirLeituraConsumo(parametros, logProcessamento);

		final String nomeRelatorio = "Relatório de Leitura e Consumo ["
				+ Util.converterDataParaStringSemCaracteresEspeciais(Calendar.getInstance().getTime()) + "]";
		if (relatorio != null) {
			logProcessamento.append("\r\n Construindo relatório de Leitura e Consumo.");
			FileOutputStream saida = null;
			File pdf = null;
			String caminhoDiretorio = "";
			try {

				Date dataEmissao = Calendar.getInstance().getTime();

				String caminhoDiretorioFatura = (String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_RELATORIO_LEITURA_CONSUMO);

				caminhoDiretorio = caminhoDiretorioFatura + "/"
						+ Util.converterDataParaStringSemHoraSemBarra(dataEmissao, Constantes.FORMATO_DATA_BR_MES_ANO) + "/";
				pdf = Util.getFile(caminhoDiretorio);
				boolean diretorio = pdf.exists();
				if (!diretorio) {
					diretorio = pdf.mkdirs();
				}

				if (diretorio) {
					logProcessamento.append("\r\n Diretório ");
					logProcessamento.append(pdf.getAbsolutePath());
					logProcessamento.append(" foi criado.");
				}

				final String diretorioDocumento = caminhoDiretorio + nomeRelatorio + Constantes.EXTENSAO_ARQUIVO_PDF;
				pdf = Util.getFile(diretorioDocumento);

				saida = new FileOutputStream(pdf);

				ProcessoDocumento processoDocumento =
						controladorProcessoDocumento.criaEPopulaProcessoDocumento(processo, diretorioDocumento, nomeRelatorio);
				controladorProcessoDocumento.inserirProcessoDocumento(processoDocumento);

				saida.write(relatorio);
				saida.flush();
				saida.close();
			} catch (Exception e) {
				logProcessamento.append("\r\n Erro ao inserir documento ");
				throw new InfraestruturaException(e.getMessage(), e);
			}
		}
		return logProcessamento;
	}

	/**
	 * Método para geração de relatório
	 * 
	 * @param parametros
	 * @return
	 * @throws GGASException
	 */
	private byte[] gerarRelatorioConsistirLeituraConsumo(Map<String, Object> parametros, StringBuilder logProcessamento)
			throws GGASException {

		byte[] relatorioLeituraConsumo;

		ControladorEmpresa controladorEmpresa =
				(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

		if (controladorEmpresa == null) {
			logProcessamento.append("\r\n Controlador empresa NULO");
		} else {
			if (controladorEmpresa.obterEmpresaPrincipal() == null) {
				logProcessamento.append("\r\n Empresa principal NULO");
			} else if (controladorEmpresa.obterEmpresaPrincipal().getLogoEmpresa() != null) {
				logProcessamento.append("\r\n Setando chave primaria da empresa para URL_LOGOMARCA_EMPRESA: ");
				logProcessamento.append(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria());
				parametros.put("imagem",
						Constantes.URL_LOGOMARCA_EMPRESA + String.valueOf(controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria()));
			} else {
				logProcessamento.append("\r\n LogoEmpresa nulo");
			}
		}

		logProcessamento.append("\r\n Obtendo dados do relatório...");
		Collection<AnaliseConsistirLeituraConsumoVO[]> listaConsistirLeituraConsumo = this.obterDadosRelatorio(parametros);

		if (listaConsistirLeituraConsumo != null && !listaConsistirLeituraConsumo.isEmpty()) {
			logProcessamento.append("\r\n Gerando relatório PDF");
			relatorioLeituraConsumo =
					RelatorioUtil.gerarRelatorioPDF(listaConsistirLeituraConsumo, parametros, RELATORIO_CONSISTIR_LEITURA_CONSUMO);
		} else {
			logProcessamento.append("\r\n Dados do relatório nulo ou vazio: ");
			logProcessamento.append(listaConsistirLeituraConsumo);
			throw new NegocioException(ERRO_NEGOCIO_RELATORIO_SEM_DADOS, true);
		}

		return relatorioLeituraConsumo;

	}

	/**
	 * Método para pesquisar dados do relatório de Consistir Leitura e Consumo
	 * 
	 * @param filtro
	 * @return
	 * @throws GGASException
	 */
	private Collection<AnaliseConsistirLeituraConsumoVO[]> obterDadosRelatorio(Map<String, Object> filtro) {

		String idGrupoFaturamento = null;
		Integer ciclo = null;
		String idRota = null;

		idGrupoFaturamento = (String) filtro.get(CONSTANTE_ID_GRUPO_FATURAMENTO);
		idRota = (String) filtro.get(CONSTANTE_ID_ROTA);
		ciclo = this.numeroCiclo;

		StringBuilder hql = new StringBuilder();

		hql.append(" select pontoConsumo.chavePrimaria as cdPontoConsumo, pontoConsumo.descricao as dsPontoConsumo, ");
		hql.append(" segmento.descricao as dsSegmento, historicoConsumo.consumo as consumo,");
		hql.append(" historicoConsumo.medidaPCS as pcs, historicoConsumo.consumoApurado as consumoApurado, ");
		hql.append(" historicoConsumo.consumoApuradoMedio as consumoMedio, rota.numeroRota as dsRota,");
		hql.append(" medicaoHistorico.dataLeituraInformada as dataLeitura, ");
		hql.append(" grupoFaturamento.descricao as dsGrupoFaturamento");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historicoConsumo inner join historicoConsumo.pontoConsumo pontoConsumo");
		hql.append(" inner join historicoConsumo.historicoAtual medicaoHistorico");
		hql.append(" inner join pontoConsumo.segmento segmento");
		hql.append(" inner join pontoConsumo.rota rota");
		hql.append(" inner join rota.grupoFaturamento grupoFaturamento");
		hql.append(" where ");
		hql.append(" historicoConsumo.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and historicoConsumo.numeroCiclo = :ciclo ");
		hql.append(" and rota.grupoFaturamento = :idGrupoFaturamento ");
		if (idRota != null) {
			hql.append(" and rota.chavePrimaria = :idRota ");
		}
		hql.append(" and historicoConsumo.medidaPCS is not null ");
		hql.append(" and historicoConsumo.habilitado is true ");
		hql.append(" order by pontoConsumo.chavePrimaria, medicaoHistorico.dataLeituraInformada");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString()).setResultTransformer(
				new GGASTransformer(AnaliseConsistirLeituraConsumoVO.class, getSessionFactory().getAllClassMetadata()));

		if (idRota != null) {
			query.setString(CONSTANTE_ID_ROTA, idRota);
		}
		query.setString(CONSTANTE_ID_GRUPO_FATURAMENTO, idGrupoFaturamento);
		query.setInteger(CONSTANTE_ANO_MES_FATURAMENTO, anoMesFaturamento);
		query.setInteger(CICLO, ciclo);

		return query.list();
	}

	@Override
	public void enviarHistoricoConsumoParaAnalise(HistoricoConsumo historicoConsumo,
			Collection<AnormalidadeSegmentoParametro> parametrizacao) {
		Iterator<AnormalidadeSegmentoParametro> iterator = parametrizacao.iterator();
		Boolean bloqueiaFaturamento = Boolean.FALSE;
		historicoConsumo.setHabilitado(true);
		historicoConsumo.setIndicadorConsumoCiclo(true);
		historicoConsumo.setIndicadorFaturamento(false);
		
		while(iterator.hasNext()) {
			AnormalidadeSegmentoParametro anormalidadeTemp = iterator.next();
			if(anormalidadeTemp.getBloqueiaFaturamento()) {
				System.out.println("Ponto entrou no if da parametrização para bloquear faturamento");
				bloqueiaFaturamento = Boolean.TRUE;
				break;
			}
		}
		
		if(!bloqueiaFaturamento) {
			System.out.println("Ponto entrou no if da parametrização para nao bloquear faturamento");
			historicoConsumo.setAnormalidadeConsumo(null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# obterHistoricoConsumoPorMedidoresIndependentes(java.lang.Long,
	 * java.lang.Integer, java.lang.Integer)
	 */
	public Collection<HistoricoConsumo> obterHistoricoConsumoPorMedidoresIndependentes(Long idPontoConsumo, Integer anoMesFaturamento,
			Integer numeroCiclo) throws NegocioException {
		Criteria criteria = createCriteria(HistoricoConsumo.class, "hist_consumo");
		criteria.createAlias("hist_consumo.pontoConsumo", "pocn", Criteria.INNER_JOIN);
		criteria.createAlias("hist_consumo.anormalidadeConsumo", "anorm_consumo", Criteria.LEFT_JOIN);
		criteria.createAlias("hist_consumo.tipoConsumo", "tp_consumo", Criteria.INNER_JOIN);
		criteria.createAlias("hist_consumo.historicoAtual", "hist_med", Criteria.LEFT_JOIN);
		criteria.createAlias("hist_med.historicoInstalacaoMedidor", "med_inst", Criteria.INNER_JOIN);
		criteria.createAlias("med_inst.medidor", "medidor", Criteria.INNER_JOIN);

		criteria.add(Restrictions.eq("pocn.chavePrimaria", idPontoConsumo));
		criteria.add(Restrictions.eq("hist_consumo.anoMesFaturamento", anoMesFaturamento));
		criteria.add(Restrictions.eq("hist_consumo.numeroCiclo", numeroCiclo));
		criteria.add(Restrictions.isNull("hist_consumo.historicoConsumoSintetizador.chavePrimaria"));
		criteria.add(Restrictions.eq("hist_consumo.indicadorMedicaoIndependenteAtual", Boolean.TRUE));

		criteria.addOrder(Order.asc("hist_med.dataLeituraInformada"));
		criteria.addOrder(Order.asc("medidor.chavePrimaria"));
		criteria.addOrder(Order.asc("hist_consumo.chavePrimaria"));

		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

		return criteria.list();
	}

	@SuppressWarnings("squid:S1192")
	@Override
	public ConsumoHistoricoDTO obterConsumoHistoricoNoMesFaturamento(Long chavePonto, Integer anoMesFaturamento, Integer numeroCiclo) {
		final Criteria criteria = createCriteria(HistoricoConsumo.class).createAlias("pontoConsumo", "pontoConsumo")
				.createAlias("tipoConsumo", "tipoConsumo").add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, chavePonto))
				.add(Restrictions.eq("anoMesFaturamento", anoMesFaturamento)).add(Restrictions.eq("numeroCiclo", numeroCiclo))
				.setProjection(Projections.projectionList().add(Projections.property("anormalidadeConsumo").as("anormalidadeConsumo"))
						.add(Projections.property("tipoConsumo.descricao").as("tipoConsumo"))
						.add(Projections.property("consumoApurado").as("consumoApurado")));

		criteria.setResultTransformer(Transformers.aliasToBean(ConsumoHistoricoDTO.class));
		return (ConsumoHistoricoDTO) criteria.list().stream().findFirst().orElse(null);

	}

	/**
	 * Realiza o mesmo que o {@link ControladorHistoricoConsumoImpl##aplicarAnormalidadeSeAtiva(AnormalidadeConsumo, HistoricoConsumo,
	 * ThrowableRunnable)}, no entanto sem definir uma ação automática a ser executada
	 * 
	 * @param anormalidade anormalidade a ser aplicada
	 * @param historico historico que tera a anormalidade configurada
	 * @throws GGASException exceção lançada caso haja falha ao executar algum procedimento
	 */
	private void aplicarAnormalidadeSeAtiva(AnormalidadeConsumo anormalidade, HistoricoConsumo historico) throws GGASException {
		this.aplicarAnormalidadeSeAtiva(anormalidade, historico, () -> {});
	}

	/**
	 * Seta a anormalidade informada caso ela esteja habilitada. Caso a anormalidade esteja com a opção de ignorar ação automatica
	 * desativada, o {@param executorAcaoAutomatica} é executado
	 * 
	 * @param anormalidade anormalidade que será setada no historico caso esteja ativa
	 * @param historico histórico a ter sua configuração setada
	 * @param executorAcaoAutomatica ação que deve ser tomada caso a ação automatica não esteja ignorada
	 * @throws GGASException exceção lançada caso haja falha ao executar o executorAcaoAutomatica
	 */
	private void aplicarAnormalidadeSeAtiva(AnormalidadeConsumo anormalidade, HistoricoConsumo historico,
			ThrowableRunnable executorAcaoAutomatica) throws GGASException {

		if (anormalidade != null && BooleanUtils.isTrue(anormalidade.isHabilitado())) {
			historico.setAnormalidadeConsumo(anormalidade);

			if (!anormalidade.isIgnorarAcaoAutomatica()) {
				executorAcaoAutomatica.run();
			}

		}
	}

	/**
	 * grupoFaturamento chave do grupo de faturamento anoMesReferencia refência do ano e mês ciclo número do ciclo de fatumento
	 */
	@Override
	public Long consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamento(Long grupoFaturamento, Integer anoMesReferencia, Integer ciclo) {
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
				Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));

		return (Long) createHQLQuery("SELECT COUNT(p) " + "FROM PontoConsumoImpl p "
				+ "WHERE p.chavePrimaria in (SELECT f.pontoConsumo.chavePrimaria FROM HistoricoConsumoImpl f WHERE f.pontoConsumo.rota.grupoFaturamento.chavePrimaria= :grupoFaturamento AND f.numeroCiclo = :ciclo AND f.anoMesFaturamento = :anoMesReferencia AND f.habilitado = true AND f.indicadorFaturamento = 1)"
				+ "OR p.chavePrimaria in (SELECT l.pontoConsumo.chavePrimaria FROM LeituraMovimentoImpl l WHERE l.pontoConsumo.rota.grupoFaturamento.chavePrimaria= :grupoFaturamento  AND l.ciclo = :ciclo AND l.anoMesFaturamento = :anoMesReferencia AND l.habilitado = true AND l.codigoAnormalidadeLeitura = :codigoAnormalidade)")
						.setParameter("grupoFaturamento", grupoFaturamento)
						.setParameter("anoMesReferencia", anoMesReferencia)
						.setParameter("codigoAnormalidade", codigoAnormalidadeLeitura).setParameter("ciclo", ciclo)
						.uniqueResult();
	}
	
	/**
	 * grupoFaturamento chave do grupo de faturamento anoMesReferencia refência do ano e mês ciclo número do ciclo de fatumento
	 */
	@Override
	public Long consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamentoSemFaturar(Long grupoFaturamento,
			Integer anoMesReferencia, Integer ciclo) {
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
				Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));

		return (Long) createHQLQuery("SELECT COUNT(p) " + "FROM PontoConsumoImpl p "
				+ "WHERE p.chavePrimaria in (SELECT f.pontoConsumo.chavePrimaria FROM HistoricoConsumoImpl f WHERE f.pontoConsumo.rota.grupoFaturamento.chavePrimaria= :grupoFaturamento AND f.numeroCiclo = :ciclo AND f.anoMesFaturamento = :anoMesReferencia AND f.habilitado = true)"
				+ "OR p.chavePrimaria in (SELECT l.pontoConsumo.chavePrimaria FROM LeituraMovimentoImpl l WHERE l.pontoConsumo.rota.grupoFaturamento.chavePrimaria= :grupoFaturamento  AND l.ciclo = :ciclo AND l.anoMesFaturamento = :anoMesReferencia AND l.habilitado = true AND l.codigoAnormalidadeLeitura = :codigoAnormalidade)")
						.setParameter("grupoFaturamento", grupoFaturamento)
						.setParameter("anoMesReferencia", anoMesReferencia)
						.setParameter("codigoAnormalidade", codigoAnormalidadeLeitura).setParameter("ciclo", ciclo)
						.uniqueResult();
	}

	/**
	 * Consultar pontos de consumo onde não exista anormalidade para o supervisorio
	 * 
	 * grupoFaturamento chave do grupo de faturamento anoMesReferencia refência do ano e mês ciclo número do ciclo de fatumento
	 */
	@Override
	public Long consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamentoSupervisorio(Long codigoAtividadeSistema,
			Long grupoFaturamento, Integer anoMesReferencia, Integer ciclo) throws GGASException {
		switch (codigoAtividadeSistema.intValue()) {
		case (int) AtividadeSistema.CODIGO_ATIVIDADE_CONSISTIR_LEITURA:
			return (Long) createHQLQuery(
					"SELECT COUNT(distinct hc.pontoConsumo.chavePrimaria) " + "FROM HistoricoConsumoImpl hc "
							+ "WHERE hc.pontoConsumo.rota.grupoFaturamento.chavePrimaria= :grupoFaturamento "
							+ "AND hc.numeroCiclo = :ciclo " + "AND hc.indicadorConsumoCiclo = 1 "
							+ "AND hc.anoMesFaturamento = :anoMesReferencia " + "AND hc.habilitado = true")
									.setParameter("grupoFaturamento", grupoFaturamento)
									.setParameter("anoMesReferencia", anoMesReferencia).setParameter("ciclo", ciclo)
									.uniqueResult();
		case (int) AtividadeSistema.CODIGO_ATIVIDADE_FATURAR:
			return (Long) createHQLQuery(
					"SELECT COUNT(distinct hc.pontoConsumo.chavePrimaria) " + "FROM HistoricoConsumoImpl hc "
							+ "WHERE hc.pontoConsumo.rota.grupoFaturamento.chavePrimaria= :grupoFaturamento "
							+ "AND hc.numeroCiclo = :ciclo " + "AND hc.anoMesFaturamento = :anoMesReferencia "
							+ "AND hc.indicadorConsumoCiclo = 1 " + "AND hc.indicadorFaturamento = 1 "
							+ "AND hc.habilitado = true").setParameter("grupoFaturamento", grupoFaturamento)
									.setParameter("anoMesReferencia", anoMesReferencia).setParameter("ciclo", ciclo)
									.uniqueResult();
		default:
			throw new GGASException();
		}
	}

	private Map<Rota, Collection<PontoConsumo>> validarPontosDeConsumoPorRota(
			Collection<PontoConsumo> chavesPontoConsumo, ControladorPontoConsumo controladorPontoConsumo) {
		Map<Rota, Collection<PontoConsumo>> lista = new HashMap<>(); 
		List<PontoConsumo> list = new ArrayList<>();
		list.addAll(chavesPontoConsumo);
		
		int total = list.size();
		int totalDivisao = list.size() / MAX_IN;
		
		int ini = CONSTANTE_NUMERO_ZERO;
		int fim =CONSTANTE_NUMERO_ZERO;
		int max = MAX_IN; 
		
		if(chavesPontoConsumo.size() < MAX_IN){
			fim = chavesPontoConsumo.size();
		}else{
			fim = max;
		}

		for (int i = CONSTANTE_NUMERO_ZERO; i <= totalDivisao; i++) { 
			Collection<PontoConsumo> colecao = new ArrayList<>();
			List<PontoConsumo> listTemporaria = new ArrayList<>();
			int cont = CONSTANTE_NUMERO_ZERO;
			//Separa a faixa de valores
			for (int j = ini; j < fim; j++) { 
				listTemporaria.add(list.get(j));
				colecao.add(listTemporaria.get(cont));
				cont++;
			}
			
			if(!colecao.isEmpty()){
				Map<Rota, Collection<PontoConsumo>> m = controladorPontoConsumo.agruparPontosDeConsumoPorRota(colecao);
				lista.putAll(m);
			}

			ini = ini + max;
			fim = fim + max;

			if (i == totalDivisao - 1) { 
				fim = total;
			}
		}
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	private <P, T> Map<P, T> validarPontoConsumo(Long[] chavesPontoConsumo, Object generico, String nomeMetodo) {
		Map<P, T> lista = new HashMap<>();
		Integer max = MAX_IN;
		Integer maximo = CONSTANTE_NUMERO_ZERO;
		int fim = CONSTANTE_NUMERO_ZERO;
		int tamanhoVetor = CONSTANTE_NUMERO_ZERO;

		if (chavesPontoConsumo.length < MAX_IN) {
			fim = tamanhoVetor = chavesPontoConsumo.length;
		} else {
			fim = tamanhoVetor = max;
		}
		int ini = CONSTANTE_NUMERO_ZERO;
		// Quantas vezes o procedimento será executado
		for (int i = CONSTANTE_NUMERO_ZERO; i <= chavesPontoConsumo.length / max; i++) {
			Long[] lstPesq = new Long[tamanhoVetor];
			int cont = CONSTANTE_NUMERO_ZERO;
			// Separa a faixa de valores
			for (int k = ini; k < fim; k++) {
				if (k == cont) {
					lstPesq[cont] = chavesPontoConsumo[k];
				}
				cont++;
			}
			if (lstPesq[i] != null && lstPesq.length != CONSTANTE_NUMERO_ZERO) {
				switch (nomeMetodo) {
				case "obterContratoAtivoPontoConsumo":
					Map<Long, ContratoPontoConsumo> obterContratoAtivoPontoConsumo = ((ControladorContrato) generico)
							.obterContratoAtivoPontoConsumo(lstPesq);
					lista.putAll((Map<? extends P, ? extends T>) obterContratoAtivoPontoConsumo);
					break;
				case "consultarContratoPontoConsumoPorPontoConsumoRecente":
					Map<Long, ContratoPontoConsumo> consultarContratoPontoConsumoPorPontoConsumoRecente = ((ControladorContrato) generico)
							.consultarContratoPontoConsumoPorPontoConsumoRecente(lstPesq);
					lista.putAll((Map<? extends P, ? extends T>) consultarContratoPontoConsumoPorPontoConsumoRecente);
					break;
				case "obterUltimoHistoricoConsumoFaturado":
					Map<PontoConsumo, HistoricoConsumo> obterUltimoHistoricoConsumoFaturado = this
							.obterUltimoHistoricoConsumoFaturado(lstPesq);
					lista.putAll((Map<? extends P, ? extends T>) obterUltimoHistoricoConsumoFaturado);
					break;
				case "listarTributoAliquotaPorPontoConsumo":
					Map<PontoConsumo, Collection<PontoConsumoTributoAliquota>> listarTributoAliquotaPorPontoConsumo = ((ControladorTributo) generico)
							.listarTributoAliquotaPorPontoConsumo(lstPesq, Boolean.TRUE, Boolean.TRUE);
					lista.putAll((Map<? extends P, ? extends T>) listarTributoAliquotaPorPontoConsumo);
					break;
				case "consultarTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente":
					Map<PontoConsumo, BigDecimal> consultarTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente = this
							.consultarTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente(lstPesq);
					lista.putAll(
							(Map<? extends P, ? extends T>) consultarTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente);
					break;
				case "obterUltimoHistoricoMedicaoFaturado":
					Map<PontoConsumo, HistoricoMedicao> obterUltimoHistoricoMedicaoFaturado = ((ControladorHistoricoMedicao) generico)
							.obterUltimoHistoricoMedicaoFaturado(lstPesq);
					lista.putAll((Map<? extends P, ? extends T>) obterUltimoHistoricoMedicaoFaturado);
					break;
				case "consultarHistoricoMedicaoAtual":
					Map<PontoConsumo, Collection<HistoricoMedicao>> consultarHistoricoMedicaoAtual = ((ControladorHistoricoMedicao) generico)
							.consultarHistoricoMedicaoAtual(lstPesq);
					lista.putAll((Map<? extends P, ? extends T>) consultarHistoricoMedicaoAtual);
					break;
				default:
					break;
				}
			}
			ini = ini + fim;
			fim = fim + ini;
			maximo = fim - max;

			if (i == (chavesPontoConsumo.length / maximo) - 1) {
				fim = chavesPontoConsumo.length;
			}
		}
		return lista;
	}
	
	/**
	 * Consulta Historico Agrupado Consumo
	 * @param chavePrimariaPontoConsumo - {@link Long}
	 * @return Colleção Historico Consumo consultado
	 * @throws NegocioException - {@link NegocioException}
	 */
	@SuppressWarnings("unchecked")
	public Collection<HistoricoConsumo> consultarHistoricoAgrupadoConsumo(Long chavePrimariaPontoConsumo, AnormalidadeConsumo anormalidadeConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo as pontoConsumo ");
		hql.append(" left join fetch pontoConsumo.rota as rota ");
		hql.append(" where ");
		hql.append(" historico.pontoConsumo.chavePrimaria = ?");
		
		hql.append(" and historico.chavePrimaria = (select MAX(h.chavePrimaria) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" h where ");
		hql.append(" h.pontoConsumo.chavePrimaria = historico.pontoConsumo.chavePrimaria ");
		hql.append(" and h.anoMesFaturamento = historico.anoMesFaturamento ");
		hql.append(" and h.numeroCiclo = historico.numeroCiclo");
		hql.append(" and h.indicadorConsumoCiclo = true");
		hql.append(" and h.habilitado = true)");

		hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaPontoConsumo);
		
		int maxResults = 5;
		if(anormalidadeConsumo != null) {
			maxResults = anormalidadeConsumo.getNumeroOcorrenciaConsecutivasAnormalidade().intValue();
		}
		query.setMaxResults(maxResults);
		
		return query.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.ControladorPontoConsumo#removerPontosConsumoFaturados(java.util.Collection)
	 */
	@Override
	public void removerPontosConsumoFaturados(List<PontoConsumo> listaCompletaPontosConsumo, Rota rota) {
		List<PontoConsumo> pontosFaturados = new ArrayList<PontoConsumo>();
		
		List<Long> listaChavesPontos = Util.recuperarChavesPrimarias(listaCompletaPontosConsumo);
		
		Map<Long, HistoricoConsumo> mapPontosFaturados = this.obterHistoricosDeConsumoFaturados(listaChavesPontos, 
				rota.getGrupoFaturamento().getAnoMesReferencia(), rota.getGrupoFaturamento().getNumeroCiclo());
		
		Set<Long> listaChavesPontosFaturados = mapPontosFaturados.keySet();
		
		if (!listaChavesPontosFaturados.isEmpty()) {
			for (PontoConsumo pontos : listaCompletaPontosConsumo) {
				if (listaChavesPontosFaturados.contains(pontos.getChavePrimaria())) {
					pontosFaturados.add(pontos);
				}
			}
			listaCompletaPontosConsumo.removeAll(pontosFaturados);
		}
	}
	
	private Map<Long, HistoricoConsumo> obterHistoricosDeConsumoFaturados(List<Long> listaChavesPontoConsumo, Integer anoMesFaturamento, Integer numeroCiclo) {

		Map<Long, HistoricoConsumo> mapaHistoricoPorChavePontoConsumo = new HashMap<>();

		if (CollectionUtils.isNotEmpty(listaChavesPontoConsumo)) {
			StringBuilder hql = new StringBuilder();
			hql.append(" select pontoConsumo.chavePrimaria, ");
			hql.append(" historico ");
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" historico ");
			hql.append(" left join fetch historico.historicoAnterior historicoAnterior ");
			hql.append(" left join fetch historico.historicoAtual historicoAtual ");
			hql.append(" left join fetch historico.tipoConsumo tipoConsumo ");
			hql.append(" inner join historico.pontoConsumo pontoConsumo ");
			hql.append(" where ");
			Map<String, List<Long>> mapaPropriedades = 
					HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", listaChavesPontoConsumo);
			hql.append(" and historico.indicadorFaturamento = :indicadorFaturamento ");
			hql.append(" and historico.habilitado = true ");
			
			if (anoMesFaturamento != null) {
				hql.append(" and historico.anoMesFaturamento = :anoMesFaturamento ");
			}
			if (numeroCiclo != null) {
				hql.append(" and historico.numeroCiclo = :numeroCiclo ");
			}
			
			hql.append(" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

			query.setBoolean(INDICADOR_FATURAMENTO, Boolean.TRUE);
			
			if (anoMesFaturamento != null) {
				query.setInteger(CONSTANTE_ANO_MES_FATURAMENTO, anoMesFaturamento);
			}
			if (numeroCiclo != null) {
				query.setInteger(NUMERO_CICLO, numeroCiclo);
			}
					
			mapaHistoricoPorChavePontoConsumo.putAll(this.montarMapaHistoricoPorChavePontoConsumo(query.list()));
		}

		return mapaHistoricoPorChavePontoConsumo;
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.ControladorHistoricoConsumo#consultarHistoricosComAnormalidadeDeConsumo(java.util.Collection)
	 */
	@Override
	public List<HistoricoConsumo> consultarHistoricosComAnormalidadeDeConsumo(Rota rota, Integer anoMesReferencia, Integer ciclo) {
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select historico ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join historico.pontoConsumo pontoConsumo ");
		hql.append(" left join fetch historico.historicoAnterior historicoAnterior ");
		hql.append(" left join fetch historico.historicoAtual historicoAtual ");
		hql.append(" left join fetch historico.tipoConsumo tipoConsumo ");
		hql.append(" where ");
		hql.append(" pontoConsumo.rota.chavePrimaria = :idRota ");
		hql.append(" and historico.indicadorFaturamento = :indicadorFaturamento ");
		hql.append(" and historico.habilitado = true ");
		hql.append(" and historico.anoMesFaturamento = :anoMesFaturamento ");
		hql.append(" and historico.numeroCiclo = :numeroCiclo ");
		hql.append(" and historico.anormalidadeConsumo is not null ");

		hql.append(" order by historico.pontoConsumo.descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setBoolean(INDICADOR_FATURAMENTO, Boolean.FALSE);
		query.setInteger(CONSTANTE_ANO_MES_FATURAMENTO, anoMesFaturamento);
		query.setInteger(NUMERO_CICLO, numeroCiclo);
		query.setLong(CONSTANTE_ID_ROTA, rota.getChavePrimaria());
		
		return query.list();
	}
	
	/**
	 * grupoFaturamento chave do grupo de faturamento anoMesReferencia refência do ano e mês ciclo número do ciclo de fatumento
	 */
	@Override
	public Long consultarQuantidadeQtdPontoConsumoComanormalidadeBloqueiaFaturamentoNaoAnalisada(Long grupoFaturamento,
			Integer anoMesReferencia, Integer ciclo) {

		StringBuilder hql = new StringBuilder();
		//hql.append("SELECT COUNT(p) FROM PontoConsumoImpl p ");
		//hql.append("WHERE p.chavePrimaria in (");
			//hql.append(" SELECT f.pontoConsumo.chavePrimaria FROM HistoricoConsumoImpl f ");
			hql.append(" SELECT count(f) FROM HistoricoConsumoImpl f ");
			hql.append(" inner join f.anormalidadeConsumo anormalidade ");
			hql.append(" WHERE f.pontoConsumo.rota.grupoFaturamento.chavePrimaria= :grupoFaturamento ");
			hql.append(" AND f.numeroCiclo = :ciclo ");
			hql.append(" AND f.anoMesFaturamento = :anoMesReferencia ");
			hql.append(" AND f.habilitado = true ");
			hql.append(" AND anormalidade.bloquearFaturamento = true ");
			hql.append(" AND f.pontoConsumo.chavePrimaria in (");
				hql.append(" SELECT mh.pontoConsumo.chavePrimaria ");
				hql.append(" FROM HistoricoMedicaoImpl mh ");
				hql.append(" WHERE mh.pontoConsumo.rota.grupoFaturamento.chavePrimaria= :grupoFaturamento  ");
				hql.append(" AND mh.numeroCiclo = :ciclo ");
				hql.append(" AND mh.anoMesLeitura = :anoMesReferencia   ");
				hql.append(" AND mh.habilitado = true ");
				hql.append(" AND mh.analisada = false) ");
		
		return (Long) createHQLQuery(hql.toString())
						.setParameter("grupoFaturamento", grupoFaturamento)
						.setParameter("anoMesReferencia", anoMesReferencia)
						.setParameter("ciclo", ciclo)
						.uniqueResult();
	}
	
	
	@Override
	public Long consultarQuantidadeQtdPontoConsumosAFaturar(Long grupoFaturamento, Integer anoMesReferencia, Integer ciclo) {
		
		return (Long) createHQLQuery("SELECT COUNT(p) " + "FROM PontoConsumoImpl p "
				+ "WHERE p.chavePrimaria in (SELECT f.pontoConsumo.chavePrimaria FROM HistoricoConsumoImpl f WHERE "
				+ "f.pontoConsumo.rota.grupoFaturamento.chavePrimaria= :grupoFaturamento AND f.numeroCiclo = :ciclo AND f.anoMesFaturamento = :anoMesReferencia "
				+ "AND f.habilitado = true AND f.indicadorFaturamento = 0)")
						.setParameter("grupoFaturamento", grupoFaturamento)
						.setParameter("anoMesReferencia", anoMesReferencia)
						.setParameter("ciclo", ciclo)
						.uniqueResult();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo#
	 * consultarHistoricoConsumoParaAnaliseExcecoes(java.lang.Long, boolean)
	 */
	@Override
	public Collection<HistoricoConsumo> consultarHistoricoConsumoParaAnaliseExcecoes(Long chavePrimariaPontoConsumo,
			boolean indicadorConsumoCiclo, String referenciaInicial, String referenciaFinal) throws NegocioException {
		return queryConsultarHistoricoConsumoParaAnaliseExcecoes(chavePrimariaPontoConsumo, referenciaInicial,
				referenciaFinal);
	}
	
	private Collection<HistoricoConsumo> queryConsultarHistoricoConsumoParaAnaliseExcecoes(Long chavePrimariaPontoConsumo, String referenciaInicial, String referenciaFinal)
			throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo");
		hql.append(" left  join fetch pontoConsumo.rota rota");
		hql.append(" inner join fetch rota.tipoLeitura tipoLeitura");
		hql.append(" left  join fetch historico.historicoAtual historicoAtual");
		hql.append(" left  join fetch historico.anormalidadeConsumo anormalidadeConsumo");
		hql.append(" left join fetch historico.historicoAnterior historicoAnterior");
		hql.append(" left  join fetch historico.historicoAtual historicoAtual");
		hql.append(" left  join fetch historicoAtual.listaMedicaoHistoricoComentario comentarios");
		hql.append(" where historico.pontoConsumo.chavePrimaria = ? ");
		hql.append(" and historico.indicadorConsumoCiclo = true ");
		hql.append(" and historico.habilitado = true ");

		if ((referenciaInicial == null && referenciaFinal != null)
				|| (referenciaInicial != null && referenciaFinal == null)) {
			throw new NegocioException(ControladorHistoricoConsumo.ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA, false);
		} else if (referenciaInicial != null) {
			hql.append(" and historico.anoMesFaturamento between :referenciaInicial and :referenciaFinal ");
		}

		hql.append(
				" order by historico.anoMesFaturamento desc, historico.numeroCiclo desc, historico.historicoAtual.dataLeituraInformada desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CONSTANTE_NUMERO_ZERO, chavePrimariaPontoConsumo);

		if (referenciaInicial != null) {
			query.setInteger("referenciaInicial", Integer.parseInt(referenciaInicial));
			query.setInteger("referenciaFinal", Integer.parseInt(referenciaFinal));
		}

		return query.list();
	}
	
	/**
	 * Método responsável por verificar se anormalidade de consumo encontrada é uma anormalidade sazonal
	 *
	 * @param historicoConsumo Histórico de Consumo
	 * @param consumoMedioApuradoDiarioPonto the consumo medio apurado diario ponto
	 * @throws NegocioException the negocio exception
	 */
	private boolean possuiAnormalidadeSazonal(HistoricoConsumo historicoAtual, 
			FaixaConsumoVariacao faixaConsumoVariacao) throws GGASException {
		BigDecimal limiteAltoConsumo = BigDecimal.ZERO;
		BigDecimal limiteBaixoConsumo = BigDecimal.ZERO;
		BigDecimal limiteEstouroConsumo = BigDecimal.ZERO;
		BigDecimal limiteInferiorVariacaoConsumo = BigDecimal.ZERO;
		BigDecimal limiteSuperiorVariacaoConsumo = BigDecimal.ZERO;
		
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia()
				.getControladorParametroSistema();
		
		String parametroArredondamento = (String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_ARREDONDAMENTO_CONSUMO);
		
		HistoricoConsumo historicoAnterior = obterHistoricoConsumoAnoAnterior(historicoAtual);
		
		if (historicoAnterior != null) {
			
			BigDecimal consumoAnterior = historicoAnterior.calcularConsumoApuradoMedio(parametroArredondamento);
			
			if (faixaConsumoVariacao != null) {
				
				limiteBaixoConsumo = consumoAnterior
						.subtract(consumoAnterior.multiply(faixaConsumoVariacao.getBaixoConsumo().divide(
								new BigDecimal(CONSTANTE_NUMERO_CEM), CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP)));
				if (limiteBaixoConsumo.compareTo(BigDecimal.ZERO) <= CONSTANTE_NUMERO_ZERO) {
					limiteBaixoConsumo= BigDecimal.ZERO;
				}
				
				limiteInferiorVariacaoConsumo = consumoAnterior.subtract(
						consumoAnterior.multiply(faixaConsumoVariacao.getPercentualInferior().divide(
								new BigDecimal(CONSTANTE_NUMERO_CEM), CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP)));
				if (limiteInferiorVariacaoConsumo.compareTo(BigDecimal.ZERO) <= CONSTANTE_NUMERO_ZERO) {
					limiteInferiorVariacaoConsumo = BigDecimal.ZERO;
				}
				
				limiteSuperiorVariacaoConsumo = consumoAnterior.add(
						consumoAnterior.multiply(faixaConsumoVariacao.getPercentualSuperior().divide(
								new BigDecimal(CONSTANTE_NUMERO_CEM), CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP)));
				
				limiteAltoConsumo = consumoAnterior
						.add(consumoAnterior.multiply(faixaConsumoVariacao.getAltoConsumo().divide(
								new BigDecimal(CONSTANTE_NUMERO_CEM), CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP)));
				
				limiteEstouroConsumo = consumoAnterior
						.add(consumoAnterior.multiply(faixaConsumoVariacao.getEstouroConsumo().divide(
								new BigDecimal(CONSTANTE_NUMERO_CEM), CONSTANTE_NUMERO_DOIS, RoundingMode.HALF_UP)));
				
				if (historicoAnterior != null && !this.verificarAnormalidadeConsumoSazonalidade(historicoAnterior)
						&& (isAltoConsumo(consumoAnterior, limiteAltoConsumo)
								|| isBaixoConsumo(consumoAnterior, limiteBaixoConsumo)
								|| isEstouroConsumo(consumoAnterior, limiteEstouroConsumo)
								|| isConsumoForaFaixa(consumoAnterior, limiteInferiorVariacaoConsumo, limiteSuperiorVariacaoConsumo))) {
					
					return true ; 
				}
			}
			
		}
		return false;
	}
	
	private HistoricoConsumo obterHistoricoConsumoAnoAnterior(HistoricoConsumo historicoAtual) throws NegocioException {
		Integer referenciaAnoAnterior = Util.diminuirMesEmAnoMes(historicoAtual.getAnoMesFaturamento(), 12); 
		ControladorHistoricoConsumo controladorHistoricoConsumo = ServiceLocator.getInstancia()
				.getControladorHistoricoConsumo();

		Collection listaHistoricoAnterior;
		try {
			listaHistoricoAnterior = controladorHistoricoConsumo.consultarHistoricoConsumoFaturavelBatch(
					historicoAtual.getPontoConsumo().getChavePrimaria(), 
					referenciaAnoAnterior, 
					historicoAtual.getNumeroCiclo(), 
					Boolean.TRUE);

			if (listaHistoricoAnterior != null && !listaHistoricoAnterior.isEmpty()) {
				return (HistoricoConsumo)listaHistoricoAnterior.iterator().next();  
			}
		} catch (NegocioException e) {
			throw new NegocioException("Erro ao tentar obter o consumo do ano anterior", e);
		}
		return null;
	}
	
	public Boolean verificarAnormalidadeConsumoSazonalidade(HistoricoConsumo historico) {
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		
		Long estouroConsumo = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
				Constantes.C_ANORMALIDADE_CONSUMO_ESTOURO));
		Long altoConsumo = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
				Constantes.C_ANORMALIDADE_CONSUMO_ALTO));
		Long baixoConsumo = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
				Constantes.C_ANORMALIDADE_CONSUMO_BAIXO));
		Long foraDeFaixa = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
				Constantes.C_ANORMALIDADE_CONSUMO_FORA_FAIXA));

		
		return historico.possuiAnormalidadeConsumo() 
				&& ((Long)historico.getAnormalidadeConsumo().getChavePrimaria() == estouroConsumo
					|| (Long)historico.getAnormalidadeConsumo().getChavePrimaria() == altoConsumo
					|| (Long)historico.getAnormalidadeConsumo().getChavePrimaria() == baixoConsumo
					|| (Long)historico.getAnormalidadeConsumo().getChavePrimaria() == foraDeFaixa);
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.consumo. ControladorHistoricoConsumo# obterMaiorFaixaConsumoPorSegmentoFaixaConsumo
	 * (br.com.ggas.cadastro.imovel.Segmento)
	 */
	@Override
	public FaixaConsumoVariacao obterMaiorFaixaConsumoPorSegmentoFaixaConsumo(Segmento segmento) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeFaixaConsumoVariacao().getSimpleName());
		hql.append(" faixa where ");
		hql.append(" habilitado = true ");
		hql.append(" and faixa.segmento.chavePrimaria = ?");
		hql.append(" order by faixa.faixaSuperior desc");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(CONSTANTE_NUMERO_ZERO, segmento.getChavePrimaria());
		query.setMaxResults(1);

		return (FaixaConsumoVariacao) query.uniqueResult();
	}
	
}
