/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
/**
 * Interface responsável pela assinatura dos métodos relacionados
 * a entidade ApuracaoPeriodo
 *
 */
public interface ApuracaoPeriodo extends Serializable {

	String BEAN_ID_APURACAO_PERIODO = "apuracaoPeriodo";

	/**
	 * @return the recuperacao
	 */
	public BigDecimal getRecuperacao();

	/**
	 * @return the qDPMedia
	 */
	BigDecimal getVolumeQDPMedia();

	/**
	 * @param qDPMedia
	 *            the qDPMedia to set
	 */
	void setVolumeQDPMedia(BigDecimal qDPMedia);

	/**
	 * @return the qDCMedia
	 */
	BigDecimal getVolumeQDCMedia();

	/**
	 * @param qDCMedia
	 *            the qDCMedia to set
	 */
	void setVolumeQDCMedia(BigDecimal qDCMedia);

	/**
	 * @return the qDRAcumulada
	 */
	BigDecimal getVolumeQDRAcumulada();

	/**
	 * @param qDRAcumulada
	 *            the qDRAcumulada to set
	 */
	void setVolumeQDRAcumulada(BigDecimal qDRAcumulada);

	/**
	 * Checks if is contrato periodo recuperacao.
	 * 
	 * @return the isContratoPeriodoRecuperacao
	 */
	Boolean isContratoPeriodoRecuperacao();

	/**
	 * @param isContratoPeriodoRecuperacao
	 *            the isContratoPeriodoRecuperacao
	 *            to set
	 */
	void setIsContratoPeriodoRecuperacao(Boolean isContratoPeriodoRecuperacao);

	/**
	 * @return the consumoUsadoPeriodoTeste
	 */
	BigDecimal getConsumoUsadoPeriodoTeste();

	/**
	 * @param consumoUsadoPeriodoTeste
	 *            the consumoUsadoPeriodoTeste to
	 *            set
	 */
	void setConsumoUsadoPeriodoTeste(BigDecimal consumoUsadoPeriodoTeste);

	/**
	 * @return the dataVolumeTesteAtingido
	 */
	Date getDataVolumeTesteAtingido();

	/**
	 * @param dataVolumeTesteAtingido
	 *            the dataVolumeTesteAtingido to
	 *            set
	 */
	void setDataVolumeTesteAtingido(Date dataVolumeTesteAtingido);

	/**
	 * @return the modalidadeConsumos
	 */
	Collection<ConsumoDistribuidoModalidade> getConsumoDistribuidoModalidade();

	/**
	 * @param modalidadeConsumos
	 *            the modalidadeConsumos to set
	 */
	void setConsumoDistribuidoModalidade(Collection<ConsumoDistribuidoModalidade> modalidadeConsumos);

}
