package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.util.Collection;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.consumo.ColetorConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.util.Fachada;

/**
 * Classe Coletor Consumo Apurado.
 * 
 *
 */
public class ColetorConsumoApurado implements ColetorConsumo {

	/** The Constant CONSUMO_APURADO. */
	private static final String CONSUMO_APURADO = "consumoApurado";
	
	@Override
	public BigDecimal coletarPorConsulta(int anoMesFaturamento, int ciclo,
			Collection<PontoConsumo> listaPontoConsumoAgrupados) throws GGASException {
		// obter consumo apurado dos
		// pontos de consumo para essa
		// referencia.
		return Fachada.getInstancia().obterConsumoApuradoPontoConsumoPorReferencia(anoMesFaturamento, ciclo,
						listaPontoConsumoAgrupados);
	}

	@Override
	public BigDecimal coletarPorPropriedade(HistoricoConsumo historico) {
		return historico.getConsumoApurado();
	}

	@Override
	public String getChaveMapaDados() {
		return CONSUMO_APURADO;
	}

}
