
package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;

import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.ControladorFaixaConsumoVariacao;
import br.com.ggas.medicao.consumo.FaixaConsumoVariacao;
import br.com.ggas.util.ServiceLocator;

/**
 * Classe da Controladora de Faixa 
 * Consumo Variação
 */
public class ControladorFaixaConsumoVariacaoImpl extends ControladorNegocioImpl implements ControladorFaixaConsumoVariacao {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(FaixaConsumoVariacao.BEAN_ID_FAIXA_CONSUMO_VARIACAO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(FaixaConsumoVariacao.BEAN_ID_FAIXA_CONSUMO_VARIACAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.ControladorFaixaConsumoVariacao#consultarTodosFaixaConsumoVariacao(java.util.Map)
	 */
	@Override
	public Collection<FaixaConsumoVariacao> consultarTodosFaixaConsumoVariacao(Map<String, Object> filtro) {

		StringBuilder hql = new StringBuilder();

		hql.append("from ").append(FaixaConsumoVariacaoImpl.class.getName()).append(" faixaConsumoVariacao where 0=0");

		if(filtro != null) {
			if(filtro.containsKey("segmento")) {
				hql.append(" and faixaConsumoVariacao.segmento.chavePrimaria = :idSegmento");
			}

			if(filtro.containsKey("faixaInferior")) {
				hql.append(" and faixaInferior = :faixaInferior");
			}
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(filtro != null) {
			if(filtro.containsKey("segmento")) {
				query.setParameter("idSegmento", filtro.get("segmento"));
			}

			if(filtro.containsKey("faixaInferior")) {
				query.setParameter("faixaInferior", filtro.get("faixaInferior"));
			}
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.ControladorFaixaConsumoVariacao#obterFaixaConsumoVariacaoNoIntervalo(java.util.Collection,
	 * java.math.BigDecimal)
	 */
	@Override
	public FaixaConsumoVariacao obterFaixaConsumoVariacaoNoIntervalo(Collection<FaixaConsumoVariacao> listaFaixaConsumoVariacao,
					BigDecimal consumo) {

		FaixaConsumoVariacao retorno = null;
		for (FaixaConsumoVariacao faixaConsumoVariacao : listaFaixaConsumoVariacao) {
			if(faixaConsumoVariacao != null && consumo.compareTo(faixaConsumoVariacao.getFaixaInferior()) >= 0
							&& consumo.compareTo(faixaConsumoVariacao.getFaixaSuperior()) <= 0) {
				retorno = faixaConsumoVariacao;
				break;
			}
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.ControladorFaixaConsumoVariacao#validarFaixasZeradasFaixaConsumoVariacao(java.util.List)
	 */
	@Override
	public void validarFaixasZeradasFaixaConsumoVariacao(List<FaixaConsumoVariacao> listaDadosFaixa) throws NegocioException,
					FormatoInvalidoException {

		int countValores = 0;
		for (FaixaConsumoVariacao faixaConsumoVariacao : listaDadosFaixa) {
			if(faixaConsumoVariacao.getFaixaSuperior() == null || faixaConsumoVariacao.getFaixaSuperior().compareTo(BigDecimal.ZERO)==0) {
				countValores++;
			}
		}
		if(countValores > 1) {
			throw new NegocioException(FaixaConsumoVariacao.ERRO_VALOR_FAIXA_FINAL_TARIFA_NAO_INFORMADO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.ControladorFaixaConsumoVariacao#consultarFaixasConsumoVariacao(java.util.Map)
	 */
	@Override
	public Collection<FaixaConsumoVariacao> consultarFaixasConsumoVariacao(Map<String, Object> filtro) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();

		Boolean habilitado = (Boolean) filtro.get("habilitado");
		BigDecimal faixa = (BigDecimal) filtro.get("faixa");

		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where 1=1 ");

		if(faixa != null) {
			hql.append(" and (faixaInferior = :faixa");
			hql.append(" or faixaSuperior = :faixa)");
		}

		if(filtro.get("idSegmento") != null) {
			hql.append(" and segmento.id = ").append(filtro.get("idSegmento")).append(" ");
		}
		if(filtro.get("descricao") != null) {
			hql.append(" and descricao like  '%").append(filtro.get("descricao")).append("%' ");
		}
		if(filtro.get("habilitado") != null) {
			hql.append(" and habilitado = ").append(habilitado);
		}
		if(filtro.get("chavePrimaria") != null) {
			hql.append(" and chavePrimaria = ").append(filtro.get("chavePrimaria"));
		}
		hql.append(" order by faixaInferior");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(faixa != null) {
			query.setParameter("faixa", faixa);
		}

		return (Collection<FaixaConsumoVariacao>) query.list();
	}

}
