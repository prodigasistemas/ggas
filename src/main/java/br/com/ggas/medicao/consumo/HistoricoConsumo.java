/*

 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo;

import java.math.BigDecimal;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.medidor.Medidor;
/**
 * Interface responsável pela assinaura dos métodos relacionados 
 * aos históricos de consumo.
 *
 */
public interface HistoricoConsumo extends EntidadeNegocio, Comparable<HistoricoConsumo>{

	String BEAN_ID_HISTORICO_CONSUMO = "historicoConsumo";

	/**
	 * @return the consumoCondominio
	 */
	HistoricoConsumo getConsumoCondominio();

	/**
	 * @param consumoCondominio
	 *            the consumoCondominio to set
	 */
	void setConsumoCondominio(HistoricoConsumo consumoCondominio);

	/**
	 * @return the anoMesFaturamento
	 */
	Integer getAnoMesFaturamento();

	/**
	 * @return the anoMesFaturamento
	 */
	String getAnoMesFaturamentoFormatado();

	/**
	 * @param anoMesFaturamento
	 *            the anoMesFaturamento to set
	 */
	void setAnoMesFaturamento(Integer anoMesFaturamento);

	/**
	 * @return the sequencia
	 */
	Integer getSequencia();

	/**
	 * @param sequencia
	 *            the sequencia to set
	 */
	void setSequencia(Integer sequencia);

	/**
	 * @return the numeroCiclo
	 */
	Integer getNumeroCiclo();

	/**
	 * @param numeroCiclo
	 *            the numeroCiclo to set
	 */
	void setNumeroCiclo(Integer numeroCiclo);

	/**
	 * @return the consumo
	 */
	BigDecimal getConsumo();

	/**
	 * @param consumo
	 *            the consumo to set
	 */
	void setConsumo(BigDecimal consumo);

	/**
	 * @return the consumoMedido
	 */
	BigDecimal getConsumoMedido();

	/**
	 * @param consumoMedido
	 *            the consumoMedido to set
	 */
	void setConsumoMedido(BigDecimal consumoMedido);

	/**
	 * @return the consumoApurado
	 */
	BigDecimal getConsumoApurado();

	/**
	 * @param consumoApurado
	 *            the consumoApurado to set
	 */
	void setConsumoApurado(BigDecimal consumoApurado);

	/**
	 * @return the historicoAnterior
	 */
	HistoricoMedicao getHistoricoAnterior();

	/**
	 * @param historicoAnterior
	 *            the historicoAnterior to set
	 */
	void setHistoricoAnterior(HistoricoMedicao historicoAnterior);

	/**
	 * @return the historicoAtual
	 */
	HistoricoMedicao getHistoricoAtual();

	/**
	 * @param historicoAtual
	 *            the historicoAtual to set
	 */
	void setHistoricoAtual(HistoricoMedicao historicoAtual);

	/**
	 * @return the indicadorImovelCondominio
	 */
	Boolean getIndicadorImovelCondominio();

	/**
	 * @param indicadorImovelCondominio
	 *            the indicadorImovelCondominio to
	 *            set
	 */
	void setIndicadorImovelCondominio(Boolean indicadorImovelCondominio);

	/**
	 * @return the consumoMedio
	 */
	BigDecimal getConsumoMedio();

	/**
	 * @param consumoMedio
	 *            the consumoMedio to set
	 */
	void setConsumoMedio(BigDecimal consumoMedio);

	/**
	 * @return the consumoMinimo
	 */
	BigDecimal getConsumoMinimo();

	/**
	 * @param consumoMinimo
	 *            the consumoMinimo to set
	 */
	void setConsumoMinimo(BigDecimal consumoMinimo);

	/**
	 * @return the consumoImovelVinculado
	 */
	BigDecimal getConsumoImovelVinculado();

	/**
	 * @param consumoImovelVinculado
	 *            the consumoImovelVinculado to
	 *            set
	 */
	void setConsumoImovelVinculado(BigDecimal consumoImovelVinculado);

	/**
	 * @return the consumoCredito
	 */
	BigDecimal getConsumoCredito();

	/**
	 * @param consumoCredito
	 *            the consumoCredito to set
	 */
	void setConsumoCredito(BigDecimal consumoCredito);

	/**
	 * @return the indicadorFaturamento
	 */
	Boolean getIndicadorFaturamento();

	/**
	 * @param indicadorFaturamento
	 *            the indicadorFaturamento to set
	 */
	void setIndicadorFaturamento(Boolean indicadorFaturamento);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the tipoConsumo
	 */
	TipoConsumo getTipoConsumo();

	/**
	 * @param tipoConsumo
	 *            the tipoConsumo to set
	 */
	void setTipoConsumo(TipoConsumo tipoConsumo);

	/**
	 * @return the anormalidadeConsumo
	 */
	AnormalidadeConsumo getAnormalidadeConsumo();

	/**
	 * @param anormalidadeConsumo
	 *            the anormalidadeConsumo to set
	 */
	void setAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo);

	/**
	 * Método responsável por calcular o
	 * percentual de consumo apurado em
	 * relação ao consumo médio.
	 * 
	 * @return (Consumo Apurado - Consumo
	 *         Médio)*100/Consumo Médio
	 */
	BigDecimal getConsumoApuradoPercentualConsumoMedio();

	/**
	 * @return the diasConsumo
	 */
	Integer getDiasConsumo();

	/**
	 * @param diasConsumo
	 *            the diasConsumo to set
	 */
	void setDiasConsumo(Integer diasConsumo);

	/**
	 * @return the consumoApuradoMedio
	 */
	BigDecimal getConsumoApuradoMedio();

	/**
	 * @param consumoApuradoMedio
	 *            the consumoApuradoMedio to set
	 */
	void setConsumoApuradoMedio(BigDecimal consumoApuradoMedio);

	/**
	 * @return the medidaPCS
	 */
	BigDecimal getMedidaPCS();

	/**
	 * @param medidaPCS
	 *            the medidaPCS to set
	 */
	void setMedidaPCS(BigDecimal medidaPCS);

	/**
	 * @return the medidaZ
	 */
	BigDecimal getMedidaZ();

	/**
	 * @param medidaZ
	 *            the medidaZ to set
	 */
	void setMedidaZ(BigDecimal medidaZ);

	/**
	 * @return the fatorPCS
	 */
	BigDecimal getFatorPCS();

	/**
	 * @param fatorPCS
	 *            the fatorPCS to set
	 */
	void setFatorPCS(BigDecimal fatorPCS);

	/**
	 * @return the fatorPTZ
	 */
	BigDecimal getFatorPTZ();

	/**
	 * @param fatorPTZ
	 *            the fatorPTZ to set
	 */
	void setFatorPTZ(BigDecimal fatorPTZ);

	/**
	 * @return the intervaloPCS
	 */
	IntervaloPCS getIntervaloPCS();

	/**
	 * @param intervaloPCS
	 *            the intervaloPCS to set
	 */
	void setIntervaloPCS(IntervaloPCS intervaloPCS);

	/**
	 * @return the localAmostragemPCS
	 */
	EntidadeConteudo getLocalAmostragemPCS();

	/**
	 * @param localAmostragemPCS
	 *            the localAmostragemPCS to set
	 */
	void setLocalAmostragemPCS(EntidadeConteudo localAmostragemPCS);

	/**
	 * @return the indicadorConsumoCiclo
	 */
	boolean isIndicadorConsumoCiclo();

	/**
	 * @param indicadorConsumoCiclo
	 *            the indicadorConsumoCiclo to set
	 */
	void setIndicadorConsumoCiclo(boolean indicadorConsumoCiclo);

	/**
	 * @return the fatorCorrecao
	 */
	BigDecimal getFatorCorrecao();

	/**
	 * @param fatorCorrecao
	 *            the fatorCorrecao to set
	 */
	void setFatorCorrecao(BigDecimal fatorCorrecao);

	/**
	 * @return BigDecimal - Consumo diário.
	 */
	BigDecimal getConsumoDiario();

	/**
	 * @return HistoricoConsumo - Histórico Consumo Sintetizador.
	 */
	HistoricoConsumo getHistoricoConsumoSintetizador();

	/**
	 * @param historicoConsumoSintetizador - Set Histórico consumo sintetizador.
	 */
	void setHistoricoConsumoSintetizador(HistoricoConsumo historicoConsumoSintetizador);

	/**
	 * @return BigDecimal - Consumo média de credito.
	 */
	BigDecimal getConsumoCreditoMedia();

	/**
	 * @param consumoCreditoMedia - Set Consumo média de credito.
	 */
	void setConsumoCreditoMedia(BigDecimal consumoCreditoMedia);

	/**
	 * @return BigDecimal - Variação Consumo
	 */
	public BigDecimal getVariacaoConsumo();

	/**
	 * @param variacaoConsumo - Set Variação Consumo.
	 */
	public void setVariacaoConsumo(BigDecimal variacaoConsumo);

	/**
	 * @return Boolean - Indicador Draw back.
	 */
	Boolean getIndicadorDrawback();

	/**
	 * @param indicadorDrawback -  Set Indicador drawback.
	 */
	void setIndicadorDrawback(Boolean indicadorDrawback);

	/**
	 * @param medidor - Set Medidor
	 */
	void setMedidor(Medidor medidor);
	
	/**
	 * @return Medidor - Medidor
	 */
	Medidor getMedidor();

	/**
	 * @return String - Composição virtual.
	 */
	String getComposicaoVirtual() ;
	
	/**
	 * @param composicaoVirtual - Set Composição virutual.
	 */
	void setComposicaoVirtual(String composicaoVirtual);
	
	/**
	 * @return Boolean - Indicador medição independente atual.
	 */
	public Boolean getIndicadorMedicaoIndependenteAtual();
	
	/**
	 * @param indicadorMedicaoIndependenteAtual - Set Indicador medição independente atual.
	 */
	public void setIndicadorMedicaoIndependenteAtual(Boolean indicadorMedicaoIndependenteAtual);
	
	/**
	 * @return Boolean - Se possui anormalidade de consumo
	 */
	public Boolean possuiAnormalidadeConsumo();
	
	/**
	 * @return Bigdecimal - Calcula o consumo medio apurado
	 */
	public BigDecimal calcularConsumoApuradoMedio(String parametroArredondamento);

	/**
	 * @return Boolean - Se ppssui anormalidade que bloqueia o faturamento
	 */
	public Boolean possuiAnormalidadeConsumoImpeditiva();
}
