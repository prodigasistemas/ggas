/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.medicao.consumo.ApuracaoPeriodo;
import br.com.ggas.medicao.consumo.ConsumoDistribuidoModalidade;
/**
 * Classe responsável pela implementação dos métodos relacionados a apuração de valores de um período
 *
 */
public class ApuracaoPeriodoImpl implements ApuracaoPeriodo {

	private static final long serialVersionUID = 2800501692788760688L;

	private BigDecimal volumeQDPMedia;

	private BigDecimal volumeQDCMedia;

	private BigDecimal volumeQDRAcumulada;

	private Boolean isContratoPeriodoRecuperacao;

	private BigDecimal consumoUsadoPeriodoTeste;

	private Date dataVolumeTesteAtingido;

	private Collection<ConsumoDistribuidoModalidade> consumoDistribuidoModalidade;

	@Override
	public BigDecimal getRecuperacao() {

		BigDecimal saldo = BigDecimal.ZERO;

		if(consumoDistribuidoModalidade != null) {
			for (ConsumoDistribuidoModalidade iConsumo : consumoDistribuidoModalidade) {
				saldo = saldo.add(iConsumo.getVolumeQR());
			}
		}

		return saldo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo#getQDPMedia()
	 */
	@Override
	public BigDecimal getVolumeQDPMedia() {

		return volumeQDPMedia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo
	 * #setQDPMedia(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDPMedia(BigDecimal qDPMedia) {

		volumeQDPMedia = qDPMedia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo#getQDCMedia()
	 */
	@Override
	public BigDecimal getVolumeQDCMedia() {

		return volumeQDCMedia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo
	 * #setQDCMedia(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDCMedia(BigDecimal qDCMedia) {

		volumeQDCMedia = qDCMedia;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo#getQDRAcumulada()
	 */
	@Override
	public BigDecimal getVolumeQDRAcumulada() {

		return volumeQDRAcumulada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo
	 * #setQDRAcumulada(java.math.BigDecimal)
	 */
	@Override
	public void setVolumeQDRAcumulada(BigDecimal qDRAcumulada) {

		volumeQDRAcumulada = qDRAcumulada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo
	 * #getIsContratoPeriodoRecuperacao()
	 */
	@Override
	public Boolean isContratoPeriodoRecuperacao() {

		return isContratoPeriodoRecuperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo
	 * #setIsContratoPeriodoRecuperacao
	 * (java.lang.Boolean)
	 */
	@Override
	public void setIsContratoPeriodoRecuperacao(Boolean isContratoPeriodoRecuperacao) {

		this.isContratoPeriodoRecuperacao = isContratoPeriodoRecuperacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo
	 * #getConsumoUsadoPeriodoTeste()
	 */
	@Override
	public BigDecimal getConsumoUsadoPeriodoTeste() {

		return consumoUsadoPeriodoTeste;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo
	 * #setConsumoUsadoPeriodoTeste(
	 * java.math.BigDecimal)
	 */
	@Override
	public void setConsumoUsadoPeriodoTeste(BigDecimal consumoUsadoPeriodoTeste) {

		this.consumoUsadoPeriodoTeste = consumoUsadoPeriodoTeste;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.consumo.impl. ApuracaoPeriodo
	 * #getDataVolumeTesteAtingido()
	 */
	@Override
	public Date getDataVolumeTesteAtingido() {
		Date data = null;
		if (this.dataVolumeTesteAtingido != null) {
			data = (Date) dataVolumeTesteAtingido.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo
	 * #setDataVolumeTesteAtingido(java.util.Date)
	 */
	@Override
	public void setDataVolumeTesteAtingido(Date dataVolumeTesteAtingido) {
		if(dataVolumeTesteAtingido != null) {
			this.dataVolumeTesteAtingido = (Date) dataVolumeTesteAtingido.clone();
		} else {
			this.dataVolumeTesteAtingido = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo#getModalidadeConsumos()
	 */
	@Override
	public Collection<ConsumoDistribuidoModalidade> getConsumoDistribuidoModalidade() {

		return consumoDistribuidoModalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * ApuracaoPeriodo
	 * #setModalidadeConsumos(java.util
	 * .Collection)
	 */
	@Override
	public void setConsumoDistribuidoModalidade(Collection<ConsumoDistribuidoModalidade> modalidadeConsumos) {

		this.consumoDistribuidoModalidade = modalidadeConsumos;
	}

}
