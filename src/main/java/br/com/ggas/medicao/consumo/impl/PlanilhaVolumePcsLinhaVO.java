package br.com.ggas.medicao.consumo.impl;

import java.io.Serializable;

/**
 * Vo de Planilha volume PCS
 */
public class PlanilhaVolumePcsLinhaVO implements Serializable {

	private static final long serialVersionUID = 8889163747116920034L;

	private String data;
	private String volume;
	private String pcs;

	/**
	 * Construtor default da entidade
	 */
	public PlanilhaVolumePcsLinhaVO() {
		super();
	}

	/**
	 * Construtor completo da entidade
	 * @param data data
	 * @param volume volume
	 * @param pcs dados de pcs
	 */
	public PlanilhaVolumePcsLinhaVO(String data, String volume, String pcs) {
		super();
		this.data = data;
		this.volume = volume;
		this.pcs = pcs;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getPcs() {
		return pcs;
	}

	public void setPcs(String pcs) {
		this.pcs = pcs;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
