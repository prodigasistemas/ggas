/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.geografico.Microrregiao;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.cadastro.geografico.Regiao;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.consumo.TemperaturaPressaoMedicao;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;

/**
 * 
 * 
 */
class TemperaturaPressaoMedicaoImpl extends EntidadeNegocioImpl implements TemperaturaPressaoMedicao {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = 1372482802277240687L;

	private Regiao regiao;

	private Microrregiao microrregiao;

	private Municipio municipio;

	private Localidade localidade;

	private BigDecimal pressaoAtmosferica;

	private Unidade unidadePressaoAtmosferica;

	private BigDecimal temperatura;

	private Unidade unidadeTemperatura;

	private Date dataInicio;

	private UnidadeFederacao unidadeFederacao;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao#getRegiao()
	 */
	@Override
	public Regiao getRegiao() {

		return regiao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #setRegiao(br.com.ggas
	 * .cadastro.geografico.Regiao)
	 */
	@Override
	public void setRegiao(Regiao regiao) {

		this.regiao = regiao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao#getMicrorregiao()
	 */
	@Override
	public Microrregiao getMicrorregiao() {

		return microrregiao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #setMicrorregiao(br
	 * .com.ggas.cadastro.geografico.Microrregiao)
	 */
	@Override
	public void setMicrorregiao(Microrregiao microrregiao) {

		this.microrregiao = microrregiao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao#getMunicipio()
	 */
	@Override
	public Municipio getMunicipio() {

		return municipio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #setMunicipio(br.com
	 * .ggas.cadastro.geografico.Municipio)
	 */
	@Override
	public void setMunicipio(Municipio municipio) {

		this.municipio = municipio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao#getLocalidade()
	 */
	@Override
	public Localidade getLocalidade() {

		return localidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #setLocalidade(br.com
	 * .ggas.cadastro.localidade.Localidade)
	 */
	@Override
	public void setLocalidade(Localidade localidade) {

		this.localidade = localidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #getPressaoAtmosferica()
	 */
	@Override
	public BigDecimal getPressaoAtmosferica() {

		return pressaoAtmosferica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #setPressaoAtmosferica
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPressaoAtmosferica(BigDecimal pressaoAtmosferica) {

		this.pressaoAtmosferica = pressaoAtmosferica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #getUnidadePressaoAtmosferica()
	 */
	@Override
	public Unidade getUnidadePressaoAtmosferica() {

		return unidadePressaoAtmosferica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #setUnidadePressaoAtmosferica
	 * (br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadePressaoAtmosferica(Unidade unidadePressaoAtmosferica) {

		this.unidadePressaoAtmosferica = unidadePressaoAtmosferica;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao#getTemperatura()
	 */
	@Override
	public BigDecimal getTemperatura() {

		return temperatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #setTemperatura(java.math.BigDecimal)
	 */
	@Override
	public void setTemperatura(BigDecimal temperatura) {

		this.temperatura = temperatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #getUnidadeTemperatura()
	 */
	@Override
	public Unidade getUnidadeTemperatura() {

		return unidadeTemperatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #setUnidadeTemperatura
	 * (br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeTemperatura(Unidade unidadeTemperatura) {

		this.unidadeTemperatura = unidadeTemperatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao#getDataInicio()
	 */
	@Override
	public Date getDataInicio() {

		return dataInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #setDataInicio(java.util.Date)
	 */
	@Override
	public void setDataInicio(Date dataInicio) {

		this.dataInicio = dataInicio;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(localidade == null && microrregiao == null && municipio == null && regiao == null && unidadeFederacao == null) {
			stringBuilder.append(AREA_ABRANGENCIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.dataInicio == null) {
			stringBuilder.append(DATA_INICIO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.temperatura == null) {
			stringBuilder.append(TEMPERATURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.unidadeTemperatura == null) {
			stringBuilder.append(UNIDADE_TEMPERATURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.pressaoAtmosferica == null) {
			stringBuilder.append(PRESSAO_ATMOSFERICA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(this.unidadePressaoAtmosferica == null) {
			stringBuilder.append(UNIDADE_PRESSAO_ATMOSFERICA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #getUnidadeFederacao()
	 */
	@Override
	public UnidadeFederacao getUnidadeFederacao() {

		return unidadeFederacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * TemperaturaPressaoMedicao
	 * #setUnidadeFederacao
	 * (br.com.ggas.cadastro.geografico
	 * .UnidadeFederacao)
	 */
	@Override
	public void setUnidadeFederacao(UnidadeFederacao unidadeFederacao) {

		this.unidadeFederacao = unidadeFederacao;
	}
}
