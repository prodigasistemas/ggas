/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo;

import java.math.BigDecimal;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados 
 * as faixas de variação de consumo
 *
 */
public interface FaixaConsumoVariacao extends EntidadeNegocio {

	String VALOR_MAXIMO_FAIXA = "99999999999999999.99999999";

	String FAIXA_INFERIOR = "FAIXA_CONSUMO_INFERIOR";

	String DESCRICAO = "FAIXA_CONSUMO_DESCRICAO";

	String SEGMENTO = "FAIXA_CONSUMO_SEGMENTO";

	String FAIXA_SUPERIOR = "FAIXA_CONSUMO_SUPERIOR";

	String FAIXA_SUPERIOR_MENOR_QUE_FAIXA_SUPERIOR = "FAIXA_SUPERIOR_MENOR_QUE_FAIXA_SUPERIOR";

	String FAIXA_SUPERIOR_IGUAL_FAIXA_INFERIOR = "FAIXA_SUPERIOR_IGUAL_FAIXA_INFERIOR";

	String PERCENTUAL_INFERIOR = "PERCENTUAL_CONSUMO_INFERIOR";

	String PERCENTUAL_SUPERIOR = "PERCENTUAL_CONSUMO_SUPERIOR";

	String BEAN_ID_FAIXA_CONSUMO_VARIACAO = "faixaConsumoVariacao";

	String ERRO_VALOR_FAIXA_FINAL_TARIFA_NAO_INFORMADO = "ERRO_VALOR_FAIXA_FINAL_TARIFA_NAO_INFORMADO";

	String ERRO_VALOR_MAXIMO_DA_FAIXA = "ERRO_VALOR_MAXIMO_DA_FAIXA";

	String ERRO_FAIXA_VARIACAO_CONSUMO_JA_EXISTE = "ERRO_FAIXA_VARIACAO_CONSUMO_JA_EXISTE";

	String ERRO_FAIXA_VARIACAO_CONSUMO_JA_EXISTENTE = "ERRO_FAIXA_VARIACAO_CONSUMO_JA_EXISTENTE";

	String ERRO_FAIXA_VARIACAO_CONSUMO_MENOR_QUE_ZERO = "ERRO_FAIXA_VARIACAO_CONSUMO_MENOR_QUE_ZERO";

	String MSG_SALVAR_SUCESSO_FAIXA_CONSUMO_VARIACAO = "MSG_SALVAR_SUCESSO_FAIXA_CONSUMO_VARIACAO";

	String MSG_ATUALIZAR_SUCESSO_FAIXA_CONSUMO_VARIACAO = "MSG_ATUALIZAR_SUCESSO_FAIXA_CONSUMO_VARIACAO";

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the faixaInferior
	 */
	BigDecimal getFaixaInferior();

	/**
	 * @param faixaInferior
	 *            the faixaInferior to set
	 */
	void setFaixaInferior(BigDecimal faixaInferior);

	/**
	 * @return the faixaSuperior
	 */
	BigDecimal getFaixaSuperior();

	/**
	 * @param faixaSuperior
	 *            the faixaSuperior to set
	 */
	void setFaixaSuperior(BigDecimal faixaSuperior);

	/**
	 * @return the percentualInferior
	 */
	BigDecimal getPercentualInferior();

	/**
	 * @param percentualInferior
	 *            the percentualInferior to set
	 */
	void setPercentualInferior(BigDecimal percentualInferior);

	/**
	 * @return the percentualSuperior
	 */
	BigDecimal getPercentualSuperior();

	/**
	 * @param percentualSuperior
	 *            the percentualSuperior to set
	 */
	void setPercentualSuperior(BigDecimal percentualSuperior);

	/**
	 * @return the segmento
	 */
	Segmento getSegmento();

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	void setSegmento(Segmento segmento);

	/**
	 * @return BigDecimal - Retorna baixo consumo.
	 */
	BigDecimal getBaixoConsumo();

	/**
	 * @param baixoConsumo - Set baixo consumo.
	 */
	void setBaixoConsumo(BigDecimal baixoConsumo);

	/**
	 * @return BigDecimal - Set alto consumo.
	 */
	BigDecimal getAltoConsumo();

	/**
	 * @param altoConsumo - Set alto consumo.
	 */
	void setAltoConsumo(BigDecimal altoConsumo);

	/**
	 * @return BigDecimal - Retorna Estouro Consumo.
	 */
	BigDecimal getEstouroConsumo();

	/**
	 * @param estouroConsumo - Set Estouro consumo.
	 */
	void setEstouroConsumo(BigDecimal estouroConsumo);

	/**
	 * Clone.
	 * 
	 * @return the object
	 * @throws CloneNotSupportedException
	 *             the clone not supported exception
	 */
	Object clone() throws CloneNotSupportedException;

}
