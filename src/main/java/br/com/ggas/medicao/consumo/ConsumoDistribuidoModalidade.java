/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoPenalidade;
/**
 * Interface responsável pela assintura dos métodos relacionados
 * a ConsumoDistribuidoModalidade
 *
 */
public interface ConsumoDistribuidoModalidade extends Serializable  {

	String BEAN_ID_CONSUMO_DISTRIBUIDO_MODALIDADE = "consumoDistribuidoModalidade";

	/**
	 * @return the contratoPontoConsumoModalidade
	 */
	ContratoPontoConsumoModalidade getContratoPontoConsumoModalidade();

	/**
	 * @param contratoPontoConsumoModalidade
	 *            the
	 *            contratoPontoConsumoModalidade
	 *            to set
	 */
	void setContratoPontoConsumoModalidade(ContratoPontoConsumoModalidade contratoPontoConsumoModalidade);

	/**
	 * @return the penalidadeComMenorPeriodicidade
	 */
	ContratoPontoConsumoPenalidade getPenalidadeToPComMenorPeriodicidade();

	/**
	 * @param penalidadeToPComMenorPeriodicidade
	 *            the
	 *            penalidadeComMenorPeriodicidade
	 *            to set
	 */
	void setPenalidadeToPComMenorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeToPComMenorPeriodicidade);

	/**
	 * @return the volumeQDPMedia
	 */
	BigDecimal getVolumeQDPMedia();

	/**
	 * @param volumeQDPMedia
	 *            the volumeQDPMedia to set
	 */
	void setVolumeQDPMedia(BigDecimal volumeQDPMedia);

	/**
	 * @return the volumeQDCMedia
	 */
	BigDecimal getVolumeQDC();

	/**
	 * @param volumeQDCMedia
	 *            the volumeQDCMedia to set
	 */
	void setVolumeQDC(BigDecimal volumeQDC);

	/**
	 * @return the volumeQDP
	 */
	BigDecimal getVolumeQDP();

	/**
	 * @param volumeQDPMedia
	 *            the volumeQDP to set
	 */
	void setVolumeQDP(BigDecimal volumeQDP);

	/**
	 * @return the volumeQDCMedia
	 */
	BigDecimal getVolumeQDCMedia();

	/**
	 * @param volumeQDCMedia
	 *            the volumeQDCMedia to set
	 */
	void setVolumeQDCMedia(BigDecimal volumeQDCMedia);

	/**
	 * @return the volumeQDR
	 */
	BigDecimal getVolumeQDR();

	/**
	 * @param volumeQDR
	 *            the volumeQDR to set
	 */
	void setVolumeQDR(BigDecimal volumeQDR);

	/**
	 * @return the volumeQR
	 */
	BigDecimal getVolumeQR();

	/**
	 * @param volumeQR
	 *            the volumeQR to set
	 */
	void setVolumeQR(BigDecimal volumeQR);

	/**
	 * @return the volumeQL
	 */
	BigDecimal getVolumeQL();

	/**
	 * @param volumeQL
	 *            the volumeQL to set
	 */
	void setVolumeQL(BigDecimal volumeQL);

	/**
	 * @return the volumeQNR
	 */
	BigDecimal getVolumeQNR();

	/**
	 * @param volumeQNR
	 *            the volumeQNR to set
	 */
	void setVolumeQNR(BigDecimal volumeQNR);

	/**
	 * @return the retiradaAutomatica
	 */
	Boolean getRetiradaAutomatica();

	/**
	 * @param retiradaAutomatica
	 *            the retiradaAutomatica to set
	 */
	void setRetiradaAutomatica(Boolean retiradaAutomatica);

	/**
	 * @return the modalidadeDataQuantidades
	 */
	Collection<ModalidadeDataQuantidade> getModalidadeDataQuantidades();

	/**
	 * @param modalidadeDataQuantidades
	 *            the modalidadeDataQuantidades to
	 *            set
	 */
	void setModalidadeDataQuantidades(Collection<ModalidadeDataQuantidade> modalidadeDataQuantidades);

	/**
	 * @return the
	 *         penalidadeSoPComMenorPeriodicidade
	 */
	public ContratoPontoConsumoPenalidade getPenalidadeSoPComMenorPeriodicidade();

	/**
	 * @param penalidadeSoPComMenorPeriodicidade
	 *            the
	 *            penalidadeSoPComMenorPeriodicidade
	 *            to set
	 */
	public void setPenalidadeSoPComMenorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeSoPComMenorPeriodicidade);

	/**
	 * @return the penalidadePeriodicidadeToP
	 */
	public ContratoPontoConsumoPenalidade getPenalidadePeriodicidadeToP();

	/**
	 * @param penalidadePeriodicidadeToP
	 *            the penalidadePeriodicidadeToP
	 *            to set
	 */
	public void setPenalidadePeriodicidadeToP(ContratoPontoConsumoPenalidade penalidadePeriodicidadeToP);

	/**
	 * @return the penalidadePeriodicidadeSoP
	 */
	public ContratoPontoConsumoPenalidade getPenalidadePeriodicidadeSoP();

	/**
	 * @param penalidadePeriodicidadeSoP
	 *            the penalidadePeriodicidadeSoP
	 *            to set
	 */
	public void setPenalidadePeriodicidadeSoP(ContratoPontoConsumoPenalidade penalidadePeriodicidadeSoP);

	/**
	 * @return the
	 *         penalidadeToPComMaiorPeriodicidade
	 */
	public ContratoPontoConsumoPenalidade getPenalidadeToPComMaiorPeriodicidade();

	/**
	 * @param penalidadeToPComMaiorPeriodicidade
	 *            the
	 *            penalidadeToPComMaiorPeriodicidade
	 *            to set
	 */
	public void setPenalidadeToPComMaiorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeToPComMaiorPeriodicidade);

	/**
	 * @return the
	 *         penalidadeSoPComMaiorPeriodicidade
	 */
	public ContratoPontoConsumoPenalidade getPenalidadeSoPComMaiorPeriodicidade();

	/**
	 * @param penalidadeSoPComMaiorPeriodicidade
	 *            the
	 *            penalidadeSoPComMaiorPeriodicidade
	 *            to set
	 */
	public void setPenalidadeSoPComMaiorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeSoPComMaiorPeriodicidade);

	/**
	 * @return ContratoPontoCunsumoPenalidade - Retorna um objeto com informações sobre uma penalidade
	 * periodicidade retirada maior.
	 */
	ContratoPontoConsumoPenalidade getPenalidadePeriodicidadeRetiradaMaior();

	/**
	 * @param penalidadePeriodicidadeRetiradaMaior - Set Penalidade periodicidade retirada maior.
	 */
	void setPenalidadePeriodicidadeRetiradaMaior(ContratoPontoConsumoPenalidade penalidadePeriodicidadeRetiradaMaior);

	/**
	 * @return ContratoPontoConsumoPenalidade - Retorna um objeto com informações sobre
	 * Penalidade retirada maior com menor periocidade.
	 */
	ContratoPontoConsumoPenalidade getPenalidadeRetiradaMaiorComMenorPeriodicidade();

	/**
	 * @param penalidadeRetiradaMaiorComMenorPeriodicidade - Set penalidade retirada maior com menor periodicidade.
	 */
	void setPenalidadeRetiradaMaiorComMenorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeRetiradaMaiorComMenorPeriodicidade);

	/**
	 * @return ContratoConsumoPenalidade - Retorna um objeto com informações sobre uma Penalidade 
	 * retirada maior com maior periodicidade.
	 */
	ContratoPontoConsumoPenalidade getPenalidadeRetiradaMaiorComMaiorPeriodicidade();

	/**
	 * @param penalidadeRetiradaMaiorComMaiorPeriodicidade - Set Penalidade retirada maior com maior periodicidade.
	 */
	void setPenalidadeRetiradaMaiorComMaiorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeRetiradaMaiorComMaiorPeriodicidade);

	/**
	 * @return ContratoPontoConsumoPenalidade - Retorna um objeto com informações sobre Penalidade periocidade 
	 * retirada menor.
	 */
	ContratoPontoConsumoPenalidade getPenalidadePeriodicidadeRetiradaMenor();

	/**
	 * @param penalidadePeriodicidadeRetiradaMenor - Set penalidade periodicidade retirada menor.
	 */
	void setPenalidadePeriodicidadeRetiradaMenor(ContratoPontoConsumoPenalidade penalidadePeriodicidadeRetiradaMenor);

	/**
	 * @return ContratoPontoConsumoPenalidade - Retorna um objeto com informações sobre
	 * Pernalidade retirada menor com menor periodicidade.
	 */
	ContratoPontoConsumoPenalidade getPenalidadeRetiradaMenorComMenorPeriodicidade();

	/**
	 * @param penalidadeRetiradaMenorComMenorPeriodicidade - Set um objeto com penalidade retirada menor
	 * com menor periodicidade.
	 */
	void setPenalidadeRetiradaMenorComMenorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeRetiradaMenorComMenorPeriodicidade);

	/**
	 * @return ContratoPontoConsumoPenalidade - Retorna um objeto com informações sobre a penalidade
	 * retirada menor com maior periodicidade.
	 */
	ContratoPontoConsumoPenalidade getPenalidadeRetiradaMenorComMaiorPeriodicidade();

	/**
	 * @param penalidadeRetiradaMenorComMaiorPeriodicidade - Set Penalidade retirada menor com a maior periodicidade.
	 */
	void setPenalidadeRetiradaMenorComMaiorPeriodicidade(ContratoPontoConsumoPenalidade penalidadeRetiradaMenorComMaiorPeriodicidade);

}
