/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.consumo.impl.PlanilhaVolumePcsVO;

/**
 * Interface responsável por fornecer os métodos
 * de consulta e persistência
 * relativos a entidade CityGateMedicao.
 */
public interface ControladorCityGateMedicao extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_CITY_GATE_MEDICAO = "controladorCityGateMedicao";

	String ERRO_NEGOCIO_ANO_MES_NAO_INFORMADOS = "ERRO_NEGOCIO_ANO_MES_NAO_INFORMADOS";

	String ERRO_NEGOCIO_NAO_EXISTE_DADOS_COMPRA_EM_ABERTO_CITY_GATE = "ERRO_NEGOCIO_NAO_EXISTE_DADOS_COMPRA_EM_ABERTO_CITY_GATE";

	String ERRO_NEGOCIO_DATAS_ANTERIORES_AO_ULTIMO_FATURAMENTO = "ERRO_NEGOCIO_DATAS_ANTERIORES_AO_ULTIMO_FATURAMENTO";

	String ERRO_NEGOCIO_NAO_EXISTE_REDE_DISTRIBUICAO_LIGADA_CITY_GATE = "ERRO_NEGOCIO_NAO_EXISTE_REDE_DISTRIBUICAO_LIGADA_CITY_GATE";

	/**
	 * Método responsável por listar os anos
	 * disponíveis para manutenção de PCS
	 * de um CityGate informado.
	 * 
	 * @param cityGate
	 *            Um cityGate.
	 * @return Coleção de anos;
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Integer> listarAnosDisponiveisCityGateMedicao(CityGate cityGate) throws NegocioException;

	/**
	 * Método responsável por listar os dados de
	 * medição de um city gate para um
	 * ano e mês informados.
	 * 
	 * @param cityGate
	 *            Um city gate.
	 * @param ano
	 *            Um inteiro que represente um
	 *            ano.
	 * @param mes
	 *            Um inteiro que represente um
	 *            mês.
	 * @return Coleção de dados de medição de city
	 *         gate.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CityGateMedicao> listarCityGateMedicaoManutencao(CityGate cityGate, Integer ano, Integer mes) throws NegocioException;

	/**
	 * Método responsável por atualizar os dados
	 * de medição de city gate.
	 * 
	 * @param listaCityGateMedicao
	 *            Coleção de dados de medição de
	 *            city gate;
	 * @return boolean indicando a necessidade de
	 *         processar consistencia de leituras
	 *         para
	 *         que os dados informados sejam
	 *         considerador durante a apuração de
	 *         volumes
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro de
	 *             concorrência.
	 */
	boolean atualizarListaCityGateMedicao(Collection<CityGateMedicao> listaCityGateMedicao) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por verificar se a
	 * alteração dos dados de medição do
	 * city gate é permitida.
	 * 
	 * @param dataUltimaLeitura
	 *            the data ultima leitura
	 * @param dataMedicao
	 *            the data medicao
	 * @return true caso permitida a alteração.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean permitirAtualizacaoCityGateMedicao(Date dataUltimaLeitura, Date dataMedicao) throws NegocioException;

	/**
	 * Método responsável por listar
	 * CityGateMedicao que tenha a data de medicao
	 * igual ao do
	 * parametro passado.
	 * 
	 * @param data
	 *            the data
	 * @return CityGateMedicao;
	 */
	Collection<CityGateMedicao> listarCityGateMedicaoPorData(Date data);

	/**
	 * Obter ultimo consumo historico.
	 * 
	 * @param cityGate
	 *            the city gate
	 * @param consumoFaturado
	 *            the consumo faturado
	 * @return the historico consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	HistoricoConsumo obterUltimoConsumoHistorico(CityGate cityGate, boolean consumoFaturado) throws NegocioException;

	/**
	 * Obter lista chave redes.
	 * 
	 * @param chavePrimariaCityGate
	 *            the chave primaria city gate
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<Long> obterListaChaveRedes(Long chavePrimariaCityGate) throws NegocioException;

	/**
	 * Criar city gate.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarCityGate();

	/**
	 * Consultar pontos consumo city gate medicao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<CityGateMedicao> consultarPontosConsumoCityGateMedicao(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar
	 * CityGateMedicao através do city gate.
	 * 
	 * @param cityGate
	 *            the city gate
	 * @param data
	 *            the data
	 * @return CityGateMedicao;
	 */
	List<CityGateMedicao> consultarCityGateMedicaoPorCityGateEData(CityGate cityGate, Date data);
	
	/**
	 * Método responsável por importar planilha
	 * com dados do Volume e PCS do CityGate
	 * 
	 * @param cityGate
	 *            the city gate
	 * @param bytes
	 *            O arquivo que será importado
	 * @param tipoArquivo
	 *            O tipo do arquivo
	 * @param nomeArquivo
	 *            the nome arquivo
	 * @param diaInicial
	 *            Dia Inicial pra importacao da planilha
	 * @param diaFinal
	 *            Dia Final pra importacao da planilha
	 * @param dataLeituraFaturada
	 *            Data de Leitura Faturada para verificar se atualiza ou nao a medicao
	 * @return Um objeto representando a planilha importada
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	public PlanilhaVolumePcsVO importarPlanilhaExcel(CityGate cityGate, byte[] bytes, String tipoArquivo, String nomeArquivo, 
			Integer diaInicial, Integer diaFinal, Date dataLeituraFaturada) throws NegocioException;
	
	/**
	 * Persiste os dias importados na planilha na tabela do BD 
	 * 
	 * @param listaCityGateMedicoes
	 *            Coleção de dados de medição de
	 *            volume e pcs;
	 * @return boolean indicando a necessidade de
	 *         processar consistencia de leituras
	 *         para
	 *         que os dados informados sejam
	 *         considerador durante a apuração de
	 *         volumes
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * 
	 */
	public void atualizarImportacaoPlanilhaCityGateMedicao(Collection<CityGateMedicao> listaCityGateMedicoes)
			throws NegocioException;
	
	/**
	 * Transforma o mes e ano do  objeto PlanilhaVolumePcsVO em Calendari
	 * 
	 * @param planilha
	 *            Planilha;
	 * @return Calendar calendar com mes e ano da planilha
	 * @throws NegocioException Caso ocorra algum erro na invocação do método.
	 * 
	 */
	public Calendar extrairPeriodoCalendar(PlanilhaVolumePcsVO planilha) throws NegocioException;
}
