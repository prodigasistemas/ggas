/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo.impl;

import java.util.Collection;
import java.util.Date;

import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.consumo.ImovelPCSZ;
/**
 * Classe responsável pela assinatura dos métodos relacionados ao Controlador de PCSZ do Imóvel
 *
 */
public interface ControladorImovelPCSZ extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_IMOVEL_PCS_Z = "controladorImovelPCSZ";

	String ERRO_NEGOCIO_ANO_MES_NAO_INFORMADOS = "ERRO_NEGOCIO_ANO_MES_NAO_INFORMADOS";

	String SUCESSO_IMOVEL_PCS_Z_ATUALIZADO = "SUCESSO_IMOVEL_PCS_Z_ATUALIZADO";

	String ERRO_NEGOCIO_DATAS_ANTERIORES_AO_ULTIMO_FATURAMENTO = "ERRO_NEGOCIO_DATAS_ANTERIORES_AO_ULTIMO_FATURAMENTO";

	/**
	 * Método responsável por listar os dados de
	 * PCS e Fator Z para o imóvel informado.
	 * 
	 * @param imovel
	 *            Imóvel
	 * @param ano
	 *            Ano
	 * @param mes
	 *            Mês
	 * @return Coleção de ImovelPCSZ
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ImovelPCSZ> listarPCSZImovel(Imovel imovel, Integer ano, Integer mes) throws NegocioException;

	/**
	 * Método responsável por verificar se a
	 * alteração das informações de PCS e Fator Z
	 * é permitida.
	 * 
	 * @param imovelPCSZ
	 *            Um ImovelPCSZ.
	 * @return true Caso permita a alteração.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean permitirAtualizacaoImovelPCSZ(ImovelPCSZ imovelPCSZ) throws NegocioException;

	/**
	 * Método responsável por atualizar os dados
	 * PCS e Fator Z do Imóvel.
	 * 
	 * @param listaImovelPCSZ
	 *            Coleção de dados de PCS e Fator
	 *            Z do Imóvel.
	 * @return boolean indicando a necessidade de
	 *         processar consistencia de leituras
	 *         para
	 *         que os dados informados sejam
	 *         considerador durante a apuração de
	 *         volumes
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             Caso ocorra algum erro de
	 *             concorrência.
	 */
	boolean atualizarListaImovelPCSZ(Collection<ImovelPCSZ> listaImovelPCSZ) throws NegocioException, ConcorrenciaException;

	/**
	 * Método responsável por obter o PCS e o
	 * fator Z vigentes para um imóvel e data
	 * informados.
	 * 
	 * @param chavePrimariaImovel
	 *            A chave primária do imóvel.
	 * @param dataReferencia
	 *            A data a ser usada como
	 *            referência de vigência.
	 * @return Um registro de ImovelPCSZ
	 * @throws NegocioException
	 *             Caso ocorra algum erro de
	 *             concorrência.
	 */
	ImovelPCSZ obterImovelPCSZVigente(Long chavePrimariaImovel, Date dataReferencia) throws NegocioException;

	/**
	 * Método responsável por obter o próximo PCS
	 * e fator Z vigente para um imóvelPCSZ e data
	 * informados.
	 * 
	 * @param chavePrimariaImovelPCSZ
	 *            A chave primária do imóvelPCSZ.
	 * @param dataLimite
	 *            A data a ser usada como limite
	 *            de vigência.
	 * @return Um registro de ImovelPCSZ
	 * @throws NegocioException
	 *             Caso ocorra algum erro de
	 *             concorrência.
	 */
	ImovelPCSZ obterProximoImovelPCSZVigente(Long chavePrimariaImovelPCSZ, Date dataLimite) throws NegocioException;

	/**
	 * Método responsável por verificar se é
	 * permitida a manutenção do fator Z do imóvel
	 * informado.
	 * 
	 * @param chavePrimariaImovel
	 *            A chave primária do imóvel.
	 * @return true, caso permitido.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean permitirManutencaoFatorZ(long chavePrimariaImovel) throws NegocioException;
}
