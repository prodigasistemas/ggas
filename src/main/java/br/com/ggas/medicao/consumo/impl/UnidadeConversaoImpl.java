/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe UnidadeConversaoImpl representa uma UnidadeConversaoImpl no sistema.
 *
 * @since 04/11/2009
 * 
 */

package br.com.ggas.medicao.consumo.impl;

import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.consumo.UnidadeConversao;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.medicao.vazaocorretor.Unidade;

class UnidadeConversaoImpl extends EntidadeNegocioImpl implements UnidadeConversao {

	private static final long serialVersionUID = 5840541545044989085L;

	private ClasseUnidade classeUnidade;

	private Unidade unidadeOrigem;

	private Unidade unidadeDestino;

	private String formula;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * UnidadeConversao#getClasseUnidade()
	 */
	@Override
	public ClasseUnidade getClasseUnidade() {

		return classeUnidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * UnidadeConversao
	 * #setClasseUnidade(br.com.ggas
	 * .medicao.vazaocorretor.ClasseUnidade)
	 */
	@Override
	public void setClasseUnidade(ClasseUnidade classeUnidade) {

		this.classeUnidade = classeUnidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * UnidadeConversao#getUnidadeOrigem()
	 */
	@Override
	public Unidade getUnidadeOrigem() {

		return unidadeOrigem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * UnidadeConversao
	 * #setUnidadeOrigem(br.com.ggas
	 * .medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeOrigem(Unidade unidadeOrigem) {

		this.unidadeOrigem = unidadeOrigem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * UnidadeConversao#getUnidadeDestino()
	 */
	@Override
	public Unidade getUnidadeDestino() {

		return unidadeDestino;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * UnidadeConversao
	 * #setUnidadeDestino(br.com.ggas
	 * .medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadeDestino(Unidade unidadeDestino) {

		this.unidadeDestino = unidadeDestino;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.UnidadeConversao
	 * #getFormula()
	 */
	@Override
	public String getFormula() {

		return formula;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.consumo.UnidadeConversao
	 * #setFormula(java.lang.String)
	 */
	@Override
	public void setFormula(String formula) {

		this.formula = formula;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
