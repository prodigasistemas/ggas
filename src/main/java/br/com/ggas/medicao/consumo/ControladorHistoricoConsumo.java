/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.consumo;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AnormalidadeSegmentoParametro;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.web.faturamento.leitura.dto.ConsumoHistoricoDTO;
import br.com.ggas.web.medicao.consumo.HistoricoConsumoVO;

/**
 * 
 * Interface da classe que trabalha com os cálculos das medições para definir os consumos.
 *
 */
public interface ControladorHistoricoConsumo extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO = "controladorHistoricoConsumo";

	String ERRO_NEGOCIO_PONTO_DE_CONSUMO_SEM_HISTORICO_CONSUMO = "ERRO_NEGOCIO_PONTO_DE_CONSUMO_SEM_HISTORICO_CONSUMO";

	String ERRO_NEGOCIO_GRUPO_FATURAMENTO_NAO_INFORMADO = "ERRO_NEGOCIO_GRUPO_FATURAMENTO_NAO_INFORMADO";

	String ERRO_NEGOCIO_PONTO_CONSUMO_SEM_CONTRATO_ATIVO = "ERRO_NEGOCIO_PONTO_CONSUMO_SEM_CONTRATO_ATIVO";

	String SUCESSO_CONSISTIR_LEITURA_MEDICAO = "SUCESSO_CONSISTIR_LEITURA_MEDICAO";

	String ERRO_NEGOCIO_TIPO_COMPOSICAO_NAO_PARAMETRIZADO = "ERRO_NEGOCIO_TIPO_COMPOSICAO_NAO_PARAMETRIZADO";

	String ERRO_NEGOCIO_OPERACAO_RETIRADA_NAO_PARAMETRIZADA = "ERRO_NEGOCIO_OPERACAO_RETIRADA_NAO_PARAMETRIZADA";

	String ERRO_NEGOCIO_OPERACAO_RETIRADA_SEM_HISTORICO = "ERRO_NEGOCIO_OPERACAO_RETIRADA_SEM_HISTORICO";

	String ERRO_NEGOCIO_OPERACAO_SUBTITUICAO_MEDIDOR_NAO_PARAMETRIZADA = "ERRO_NEGOCIO_OPERACAO_SUBTITUICAO_MEDIDOR_NAO_PARAMETRIZADA";

	String ERRO_NEGOCIO_VALOR_CONVERSAO = "ERRO_NEGOCIO_VALOR_CONVERSAO";

	String ERRO_NEGOCIO_UNIDADE_CONVERSAO = "ERRO_NEGOCIO_UNIDADE_CONVERSAO";

	String ERRO_NEGOCIO_TEMPERATURA_LOCAL_FORNECIMENTO_NAO_ENCONTRADA = "ERRO_NEGOCIO_TEMPERATURA_LOCAL_FORNECIMENTO_NAO_ENCONTRADA";

	String ERRO_NEGOCIO_PRESSAO_LOCAL_FORNECIMENTO_NAO_ENCONTRADA = "ERRO_NEGOCIO_PRESSAO_LOCAL_FORNECIMENTO_NAO_ENCONTRADA";

	String ERRO_NEGOCIO_FAIXA_PRESSAO_NAO_ENCONTRADA = "ERRO_NEGOCIO_FAIXA_PRESSAO_NAO_ENCONTRADA";

	String ERRO_NEGOCIO_FAIXA_PRESSAO_SEM_Z_INFORMADO = "ERRO_NEGOCIO_FAIXA_PRESSAO_SEM_Z_INFORMADO";

	String ERRO_NEGOCIO_PCS_NAO_ENCONTRADO = "ERRO_NEGOCIO_PCS_NAO_ENCONTRADO";

	String ERRO_NEGOCIO_VOLUME_NAO_INFORMADO = "ERRO_NEGOCIO_VOLUME_NAO_INFORMADO";

	String ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA = "ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA";

	String ERRO_NEGOCIO_REFERENCIA_MES_ANO = "ERRO_NEGOCIO_REFERENCIA_MES_ANO";

	String ERRO_NEGOCIO_DATA_FINAL_MENOR_DATA_INICIAL = "ERRO_NEGOCIO_DATA_FINAL_MENOR_DATA_INICIAL";

	String ERRO_NEGOCIO_HISTORICO_INSTALACAO_LEITURA_NULA = "ERRO_NEGOCIO_HISTORICO_INSTALACAO_LEITURA_NULA";

	String ERRO_NEGOCIO_HISTORICO_CONSUMO_NAO_ENCONTRADO_PONTO_CONSUMO = "ERRO_NEGOCIO_HISTORICO_CONSUMO_NAO_ENCONTRADO_PONTO_CONSUMO";

	String ERRO_NEGOCIO_DADOS_MEDICAO = "ERRO_NEGOCIO_DADOS_MEDICAO";

	String ERRO_NEGOCIO_IMOVEL_SEM_PONTO_CONSUMO = "ERRO_NEGOCIO_IMOVEL_SEM_PONTO_CONSUMO";

	String ERRO_NEGOCIO_CONTARTO_SEM_LOCAL_AMOSTRAGERM_OU_FATOR_UNICO = "ERRO_NEGOCIO_CONTARTO_SEM_LOCAL_AMOSTRAGERM_OU_FATOR_UNICO";

	public String ERRO_NEGOCIO_CROMATOGRAFIA_NAO_CADASTRADA_PARA_CITYGATE = "ERRO_NEGOCIO_CROMATOGRAFIA_NAO_CADASTRADA_PARA_CITYGATE";

	/**
	 * retorna a lista de faixas de consumo por
	 * segumento e pelo consumo.
	 * 
	 * @param segmento
	 *            the segmento
	 * @param consumoMedio
	 *            the consumo medio
	 * @return the collection
	 */
	Collection<FaixaConsumoVariacao> listarFaixasConsumoPorSegmentoFaixaConsumo(Segmento segmento, BigDecimal consumoMedio);

	/**
	 * Obter faixa de consumo por segmento e
	 * consumo médio.
	 * 
	 * @param segmento
	 *            Segmento
	 * @param consumoMedio
	 *            Consumo Médio
	 * @return FaixaConsumoVariacao Variação da
	 *         faixa de consumo
	 */
	FaixaConsumoVariacao obterFaixaConsumoPorSegmentoFaixaConsumo(Segmento segmento, BigDecimal consumoMedio);

	/**
	 * obter o historicoConsumo mais recente do
	 * pontoConsumo.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @return the historico consumo
	 */
	HistoricoConsumo obterPorPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave primaria do ponto de
	 *            consumo
	 * @return Uma coleção de histórico
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumo(Long chavePrimariaPontoConsumo) throws NegocioException;

	/**
	 * Consultar historico consumo por historico medicao.
	 * 
	 * @param chaveHistoricoMedicao
	 *            the chave historico medicao
	 * @return the historico consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	HistoricoConsumo consultarHistoricoConsumoPorHistoricoMedicao(Long chaveHistoricoMedicao) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo, agrupado
	 * por referência e ciclo.
	 * param idPontoConsumo A chave do ponto de
	 * consumo
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param anoMesFaturamento
	 *            the ano mes faturamento
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @param indicadorConsumoCiclo
	 *            Indicador de consumo ciclo
	 * @param indicadorFaturamento
	 *            the indicador faturamento
	 * @param chavePrimariaHistoricoConsumoSintetizador
	 *            the chave primaria historico consumo sintetizador
	 * @return Uma coleção de histórico
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumo(Long idPontoConsumo, Integer anoMesFaturamento, Integer numeroCiclo,
					Boolean indicadorConsumoCiclo, Boolean indicadorFaturamento, Long chavePrimariaHistoricoConsumoSintetizador)
					throws NegocioException;

	/**
	 * Consultar historico consumo.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param anoMesFaturamento
	 *            the ano mes faturamento
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @param indicadorConsumoCiclo
	 *            the indicador consumo ciclo
	 * @param indicadorFaturamento
	 *            the indicador faturamento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumo(Long idPontoConsumo, Integer anoMesFaturamento, Integer numeroCiclo,
					Boolean indicadorConsumoCiclo, Boolean indicadorFaturamento) throws NegocioException;

	/**
	 * Consultar historico consumo faturavel batch.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param anoMesFaturamento
	 *            the ano mes faturamento
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @param indicadorConsumoCiclo
	 *            the indicador consumo ciclo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	// TKT 4232
	Collection<HistoricoConsumo> consultarHistoricoConsumoFaturavelBatch(Long idPontoConsumo, Integer anoMesFaturamento,
					Integer numeroCiclo, Boolean indicadorConsumoCiclo) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo, agrupados
	 * por referência e ciclo.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave primaria do ponto de
	 *            consumo
	 * @param indicadorConsumoCiclo
	 *            Indicador de consumo ciclo
	 * @param referenciaInicial
	 *            the referencia inicial
	 * @param referenciaFinal
	 *            the referencia final
	 * @return Uma coleção de histórico
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumo(Long chavePrimariaPontoConsumo, boolean indicadorConsumoCiclo,
					String referenciaInicial, String referenciaFinal) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo, agrupados
	 * por referência e ciclo e limitando a
	 * quantidade de ciclos.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave primaria do ponto de
	 *            consumo
	 * @param indicadorConsumoCiclo
	 *            Indicador de consumo ciclo
	 * @param referenciaInicial
	 *            the referencia inicial
	 * @param referenciaFinal
	 *            the referencia final
	 * @param quantidadeCiclos
	 *            the quantidade ciclos
	 * @return Uma coleção de histórico
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumo(Long chavePrimariaPontoConsumo, boolean indicadorConsumoCiclo,
					String referenciaInicial, String referenciaFinal, Integer quantidadeCiclos) throws NegocioException;
	
	
	/**
	 * Método responsável por consultar o
	 * historico do ponto de consumo pelo ano/mes de
	 * referencia e ciclo do ano anterior
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave primaria do ponto de
	 *            consumo
	 * @param anoMes
	 * 			a referencia do faturamento
	 * 
	 * @param ciclo
	 * 			o numero do ciclo
	 *            
	 * @return HistoricoConsumo
	 * 
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	HistoricoConsumo consultarHistoricoConsumoPorMesAnoCicloAnterior(Long chavePrimariaPontoConsumo, String anoMes,
			String ciclo) throws NegocioException;

	/**
	 * Método responsável por calcular o consumo
	 * médio.
	 * 
	 * @param pontoConsumo
	 *            Ponto de Consumo.
	 * @param contrato
	 *            O contrato ativo
	 * @param referenciaAtual
	 *            A referência atual
	 * @param cicloAtual
	 *            O ciclo atual
	 * @param diasConsumoAtual dias do consumo Atual
	 * @return Consumo Médio.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Map<String, BigDecimal> calcularConsumoMedio(PontoConsumo pontoConsumo, ContratoPontoConsumo contrato, int referenciaAtual,
					int cicloAtual, int diasConsumoAtual) throws NegocioException;

	/**
	 * Método responsável por consultar históricos
	 * de consumo para análise de
	 * exceções de leitura de consumo.
	 * 
	 * @param filtro
	 *            Map contendo os parametros da
	 *            pesquisa.
	 * @return coleção de históricos de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumoAnaliseExcecoesLeitura(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar todos os
	 * tipos de consumo cadastrados.
	 * 
	 * @return Uma coleção com todos os tipos de
	 *         consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TipoConsumo> listarTiposConsumo() throws NegocioException;

	/**
	 * Método reponsável por validar se a colecao
	 * de historico do ponto de consumo está vazia.
	 * 
	 * @param listaHistoricoConsumo
	 *            A lista de historicos
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarPontoConsumoSemHistoricoConsumo(Collection<HistoricoConsumo> listaHistoricoConsumo) throws NegocioException;

	/**
	 * Método responsável por gerar o relatório de
	 * análise de consumo.
	 * 
	 * @param chavesPrimariasAux
	 *            the chaves primarias aux
	 * @param filtro
	 *            the filtro
	 * @return O relatório gerado
	 * @throws NegocioException
	 *             the negocio exception
	 */
	byte[] gerarRelatorioAnaliseConsumo(String[] chavesPrimariasAux, Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por validar a selecao do
	 * grupo de faturamento.
	 * 
	 * @param chavePrimaria
	 *            A chavePrimaria do grupo
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarSelecaoGrupoFaturamento(Long chavePrimaria) throws NegocioException;

	/**
	 * Consistir leitura medicao.
	 * 
	 * @param pontosConsumo
	 *            Uma coleção de pontos de consumo
	 * @param anoMesFaturamento
	 *            the ano mes faturamento
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @param logProcessamento
	 *            the log processamento
	 * @param isFaturaAvulso
	 *            the is fatura avulso
	 * @param historicoMedicao
	 *            the historico medicao
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 */

	/**
	 * Método responsável por consistir as
	 * leituras e medições dos pontos de consumo.
	 * 
	 * @param pontosConsumo {@link Collection}
	 * @param anoMesFaturamento {@link Integer}
	 * @param numeroCiclo {@link Integer}
	 * @param logProcessamento {@link StringBuilder}
	 * @param isFaturaAvulso {@link Boolean}
	 * @param historicoMedicao {@link HistoricoMedicao}
	 * @throws GGASException - {@link GGASException}
	 */
	void consistirLeituraMedicao(Collection<PontoConsumo> pontosConsumo, Integer anoMesFaturamento, Integer numeroCiclo,
					StringBuilder logProcessamento, Boolean isFaturaAvulso, HistoricoMedicao historicoMedicao) throws GGASException;

	/**
	 * Método responsável por consultar os
	 * historicos consumo.
	 * 
	 * @param pontoConsumo
	 *            O Ponto de consumo
	 * @param maxResultados
	 *            O número máximo de resultados da
	 *            consulta. Pode ser nulo, nesse
	 *            caso virão todos.
	 * @return Uma coleção de histórico
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoConsumo> consultarHistoricosConsumo(PontoConsumo pontoConsumo, Integer maxResultados) throws NegocioException;

	/**
	 * Método responsável por consultar um tipo de
	 * composição de consumo pela
	 * chave primária.
	 * 
	 * @param chavePrimaria
	 *            A chave primária do tipo de
	 *            composição de consumo.
	 * @return Um tipo de composição de consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoComposicaoConsumo obterTipoComposicaoConsumo(Long chavePrimaria) throws NegocioException;

	/**
	 * Obter referencia inicial.
	 * 
	 * @param anoInicial
	 *            the ano inicial
	 * @param mesInicial
	 *            the mes inicial
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	String obterReferenciaInicial(String anoInicial, String mesInicial) throws NegocioException;

	/**
	 * Obter referencia final.
	 * 
	 * @param anoFinal
	 *            the ano final
	 * @param mesFinal
	 *            the mes final
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	String obterReferenciaFinal(String anoFinal, String mesFinal) throws NegocioException;

	/**
	 * Validar intervalo referencia.
	 * 
	 * @param referenciaInicial
	 *            the referencia inicial
	 * @param referenciaFinal
	 *            the referencia final
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarIntervaloReferencia(String referenciaInicial, String referenciaFinal) throws NegocioException;

	/**
	 * Método que valida se a data final
	 * selecionada é maior que a data inicial.
	 * 
	 * @param mesInicial
	 *            the mes inicial
	 * @param anoInicial
	 *            the ano inicial
	 * @param mesFinal
	 *            the mes final
	 * @param anoFinal
	 *            the ano final
	 * @throws GGASException {@link GGASException}
	 *             the GGAS exception
	 */
	void validarIntervaloDatas(String mesInicial, String anoInicial, String mesFinal, String anoFinal) throws GGASException;

	/**
	 * Método responsável por listar intervalos de
	 * PCS.
	 * 
	 * @return coleção de intervalos de PCS.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<IntervaloPCS> listarIntervaloPCS() throws NegocioException;

	/**
	 * Método responsável por obter o intervalo do
	 * PCS.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return intervalo de PCS.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	IntervaloPCS obterIntervaloPCS(Long chavePrimaria) throws NegocioException;

	/**
	 * Obter historico consumo.
	 * 
	 * @param idHistoricoConsumo
	 *            the id historico consumo
	 * @return the historico consumo
	 */
	HistoricoConsumo obterHistoricoConsumo(Long idHistoricoConsumo);

	/**
	 * Método responsável por consultar os
	 * históricos do ponto de consumo para
	 * simulação.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave primaria do ponto de
	 *            consumo
	 * @return Uma coleção de histórico
	 * @throws GGASException {@link GGASException}
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumoParaSimulacao(Long chavePrimariaPontoConsumo) throws GGASException;

	/**
	 * Método responsável por obter o histórico de
	 * consumo para simulação do cálculo de
	 * fornecimento de gás.
	 * 
	 * @param chavePrimaria
	 *            Chave do histórico de consumo
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return Historico Consumo.
	 * @throws GGASException {@link GGASException}
	 *             Caso ocorra algum erro.
	 */
	HistoricoConsumo obterHistoricoConsumoParaSimulacao(long chavePrimaria, String... propriedadesLazy) throws GGASException;

	/**
	 * Método responsável por obter o consumo
	 * apurado dos pontos de consumo por
	 * referência que tem o faturamento agrupado.
	 * 
	 * @param anoMesFaturamento
	 *            the ano mes faturamento
	 * @param ciclo
	 *            the ciclo
	 * @param listaPontoConsumoAgrupados
	 *            the lista ponto consumo agrupados
	 * @return the big decimal
	 * @throws GGASException {@link GGASException}
	 *             the GGAS exception
	 */
	BigDecimal obterConsumoApuradoPontoConsumoPorReferencia(int anoMesFaturamento, int ciclo,
					Collection<PontoConsumo> listaPontoConsumoAgrupados) throws GGASException;
	
	/**
	 * Método responsável por obter o consumo
	 * dos pontos de consumo por
	 * referência que tem o faturamento agrupado.
	 * 
	 * @param anoMesFaturamento
	 *            the ano mes faturamento
	 * @param ciclo
	 *            the ciclo
	 * @param listaPontoConsumoAgrupados
	 *            the lista ponto consumo agrupados
	 * @return the big decimal
	 * @throws GGASException {@link GGASException}
	 *             the GGAS exception
	 */
	BigDecimal obterConsumoPontoConsumoPorReferencia(int anoMesFaturamento, int ciclo,
					Collection<PontoConsumo> listaPontoConsumoAgrupados) throws GGASException;

	/**
	 * Método responsável por obter o último
	 * histórico de consumo não faturado.
	 * 
	 * @param idPontoConsumo
	 *            Chave primária do ponto de
	 *            consumo.
	 * @param indicadorFaturamento
	 *            Indicador para os consumos
	 *            faturados ou não.
	 * @param indicadorConsumoCiclo
	 *            Indicador do consumo ciclo.
	 * @param indicadorUltimo
	 *            Indicador para buscar os últimos
	 *            consumos.
	 * @return Um HistoricoConsumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	HistoricoConsumo obterHistoricoConsumo(Long idPontoConsumo, Boolean indicadorFaturamento, boolean indicadorConsumoCiclo,
					boolean indicadorUltimo) throws NegocioException;

	/**
	 * Método responsável por validar a
	 * periodicidade de um histórico de consumo.
	 * 
	 * @param historicoConsumo
	 *            the historico consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarPeriodicidadeHistoricoConsumo(HistoricoConsumo historicoConsumo) throws NegocioException;

	/**
	 * Método responsável por verificar credito de
	 * volume.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param referenciaFaturamento
	 *            the referencia faturamento
	 * @param cicloFaturamento
	 *            the ciclo faturamento
	 * @return historicoConsumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	HistoricoConsumo verificaCreditoVolume(PontoConsumo pontoConsumo, Integer referenciaFaturamento, Integer cicloFaturamento)
					throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * Históricos de Consumo que não foram
	 * faturados e são anteriores a uma
	 * referencia/ciclo para um PontoConsumo.
	 * 
	 * @param referencia
	 *            a referencia
	 * @param ciclo
	 *            o ciclo
	 * @param pontosConsumo
	 *            o ponto de consumo
	 * @return Históricos de Consumo anteriores
	 *         que não foram faturados
	 * @throws NegocioException
	 *             caso ocorra algum erro
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumosPendentes(Integer referencia, Integer ciclo,
					Collection<PontoConsumo> pontosConsumo) throws NegocioException;

	/**
	 * Método responsável por pesquisar todos os
	 * imoveis que possuem historico de leitura e
	 * medicao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return imoveis
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Imovel> pesquisarImovelHistoricoLeituraConsumo(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método para obter o último historico
	 * consumo faturado.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the historico consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	HistoricoConsumo obterUltimoHistoricoConsumoFaturado(Long idPontoConsumo) throws NegocioException;

	/**
	 * Obter ultimo historico consumo por ponto consumo.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param isFaturado
	 *            the is faturado
	 * @return the historico consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	HistoricoConsumo obterUltimoHistoricoConsumoPorPontoConsumo(Long idPontoConsumo, Boolean isFaturado) throws NegocioException;

	/**
	 * Método responsável pela conversão de
	 * medidas para a unidade definida como padrão
	 * para uso do sistema.
	 * 
	 * @param valor
	 *            Valor a ser convertido.
	 * @param unidadeOrigem
	 *            Unidade Origem.
	 * @param unidadeDestino
	 *            Unidade Destino, que também é
	 *            considerada a unidade padrão.
	 * @return Valor da unidade origem convertido
	 *         para a unidade padrão do sistema.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	BigDecimal converterUnidadeMedida(BigDecimal valor, Unidade unidadeOrigem, Unidade unidadeDestino) throws NegocioException;

	/**
	 * Método responsável por listar todos os
	 * tipos de composição consumo.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<TipoComposicaoConsumo> listarTipoComposicaoConsumo() throws NegocioException;

	/**
	 * Método para obter o historico consumo pela
	 * data.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param data
	 *            the data
	 * @return the historico consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	HistoricoConsumo obterHistoricoConsumoPorData(Long idPontoConsumo, Date data) throws NegocioException;

	/**
	 * Método para obter o historico consumo
	 * anterior.
	 * 
	 * @param listaPontosConsumo
	 *            the lista pontos consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */

	/**
	 * Método para obter o historico consumo
	 * atraves da referencia e ciclo
	 * 
	 * @param idPontoConsumo
	 * @param anoMesLeitura
	 * @param numeroCiclo
	 * @return
	 * @throws NegocioException
	 */

	/**
	 * Método para remover da lista os pontos de
	 * consumo sem historico de consumo
	 * 
	 * @param listaPontosConsumo
	 * @return
	 * @throws NegocioException
	 */
	Collection<PontoConsumo> removerPontosConsumoDaListaSemHistoricoConsumo(Collection<PontoConsumo> listaPontosConsumo)
					throws NegocioException;

	/**
	 * Método responsável por verificar se houve virada de medidor.
	 * 
	 * @param idPontoConsumo the id ponto consumo
	 * @param leitura the leitura
	 * @param data the data
	 * @return the boolean
	 * @throws NegocioException the negocio exception
	 */
	Boolean verificarViradaMedidor(Long idPontoConsumo, BigDecimal leitura, Date data) throws NegocioException;

	/**
	 * Método responsável por verificar se houve
	 * virada de medidor.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoConsumo> consultarPendenciaFaturamentoAnormalidadeMedicao(Long idPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por verificar se houve
	 * virada de medidor.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoConsumo> consultarPendenciaFaturamentoAnormalidade(Long idPontoConsumo) throws NegocioException;

	/**
	 * Consultar historico consumo por leitura movimento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the historico consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	HistoricoConsumo consultarHistoricoConsumoPorLeituraMovimento(LeituraMovimento filtro) throws NegocioException;

	/**
	 * Obter fator pt.
	 * 
	 * @param temperaturaReferencia
	 *            the temperatura referencia
	 * @param pressaoReferencia
	 *            the pressao referencia
	 * @param temperaturaLocal
	 *            the temperatura local
	 * @param pressaoLocal
	 *            the pressao local
	 * @param pressaoConsiderada
	 *            the pressao considerada
	 * @return the big decimal
	 */
	BigDecimal obterFatorPT(BigDecimal temperaturaReferencia, BigDecimal pressaoReferencia, BigDecimal temperaturaLocal,
					BigDecimal pressaoLocal, BigDecimal pressaoConsiderada);

	/**
	 * Obter historico consumo todos.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param indicadorFaturamento
	 *            the indicador faturamento
	 * @param indicadorConsumoCiclo
	 *            the indicador consumo ciclo
	 * @param indicadorUltimo
	 *            the indicador ultimo
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<HistoricoConsumo> obterHistoricoConsumoTodos(Long idPontoConsumo, boolean indicadorFaturamento, boolean indicadorConsumoCiclo,
					boolean indicadorUltimo) throws NegocioException;

	/**
	 * Consultar historico agrupado ano mes.
	 * 
	 * @param chavePrimariaContrato
	 *            the chave primaria contrato
	 * @param dataLeituraMenorInformada
	 *            the data leitura menor informada
	 * @param dataLeituraMaiorInformada
	 *            the data leitura maior informada
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Object> consultarHistoricoAgrupadoAnoMes(Long chavePrimariaContrato, Date dataLeituraMenorInformada,
					Date dataLeituraMaiorInformada) throws NegocioException;

	/**
	 * Consultar historico medicao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param dataLeituraMenorInformada
	 *            the data leitura menor informada
	 * @param dataLeituraMaiorInformada
	 *            the data leitura maior informada
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoConsumo> consultarHistoricoMedicao(Long chavePrimaria, Date dataLeituraMenorInformada,
					Date dataLeituraMaiorInformada) throws NegocioException;

	/**
	 * Consultar ultimo historico consumo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the historico consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	HistoricoConsumo consultarUltimoHistoricoConsumo(long chavePrimaria) throws NegocioException;

	/**
	 * Inativar historico consumo.
	 * 
	 * @param idHistoricoMedicao
	 *            the id historico medicao
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void inativarHistoricoConsumo(Long idHistoricoMedicao) throws NegocioException, ConcorrenciaException;

	/**
	 * Consultar historico consumo por ponto consumo.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param maxResult
	 *            the max result
	 * @return the collection
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumoPorPontoConsumo(Long idPontoConsumo, Integer maxResult);

	/**
	 * Consultar historico consumo por historico consumo sintetizador.
	 * 
	 * @param historicoConsumoSintetizador
	 *            the historico consumo sintetizador
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumoPorHistoricoConsumoSintetizador(HistoricoConsumo historicoConsumoSintetizador)
					throws NegocioException;

	/**
	 * Obter tipo consumo.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the tipo consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	TipoConsumo obterTipoConsumo(Long chavePrimaria) throws NegocioException;

	/**
	 * Consultar proximo medicao historico.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            the chave primaria ponto consumo
	 * @param dataFimApuracao
	 *            the data fim apuracao
	 * @return the historico consumo
	 */
	HistoricoConsumo consultarProximoMedicaoHistorico(Long chavePrimariaPontoConsumo, Date dataFimApuracao);

	/**
	 * Obter anterior historico consumo faturado.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param idMedicaoAnterior
	 *            the id medicao anterior
	 * @return the historico consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	HistoricoConsumo obterAnteriorHistoricoConsumoFaturado(Long idPontoConsumo, Long idMedicaoAnterior) throws NegocioException;

	/**
	 * Obter historico consumo por ponto e data.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param data
	 *            the data
	 * @return the historico consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	HistoricoConsumo obterHistoricoConsumoPorPontoEData(Long idPontoConsumo, Date data) throws NegocioException;

	/**
	 * Consultar total consumo apurado ano mes grupo faturamento corrente.
	 * 
	 * @param idsPontoConsumo
	 *            the id ponto consumo
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	BigDecimal consultarTotalConsumoApuradoAnoMesGrupoFaturamentoCorrente(Long[] idsPontoConsumo) throws NegocioException;

	/**
	 * Consultar total drawback consumo apurado ano mes grupo faturamento corrente.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the big decimal
	 * @throws NegocioException
	 *             the negocio exception
	 */
	BigDecimal consultarTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente(Long idPontoConsumo) throws NegocioException;

	/**
	 * Obter faixa pressao fornecimento.
	 * 
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param pressaoAnterior
	 *            the pressao anterior
	 * @param unidadeAnterior
	 *            the unidade anterior
	 * @param segmento
	 *            the segmento
	 * @return the faixa pressao fornecimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	FaixaPressaoFornecimento obterFaixaPressaoFornecimento(ContratoPontoConsumo contratoPontoConsumo, BigDecimal pressaoAnterior,
					Unidade unidadeAnterior, Segmento segmento) throws NegocioException;

	/**
	 * Consistir ponto consumo fatura complementar.
	 * 
	 * @param pontoConsumo {@link PontoConsumo}
	 * @param contratoPontoConsumo {@link ContratoPontoConsumo}
	 * @param referenciaCicloAtual {@link Map}
	 * @param logProcessamento {@link StringBuilder}
	 * @param isFaturaAvulso {@link Boolean}
	 * @param historico {@link HistoricoMedicao}
	 * @throws GGASException {@link GGASException}
	 */
	void consistirPontoConsumoFaturaComplementar(PontoConsumo pontoConsumo, ContratoPontoConsumo contratoPontoConsumo,
					Map<String, Integer> referenciaCicloAtual, StringBuilder logProcessamento, Boolean isFaturaAvulso,
					HistoricoMedicao historico) throws GGASException;

	/**
	 * Inserir historico consumo ciclo.
	 * 
	 * @param mapAtributosConsumo
	 *            the map atributos consumo
	 * @param historicoUltimaLeitura
	 *            the historico ultima leitura
	 * @param totalConsumoMedido
	 *            the total consumo medido
	 * @param totalConsumoApurado
	 *            the total consumo apurado
	 * @param totalConsumo
	 *            the total consumo
	 * @param totalFatorCorrecao
	 *            the total fator correcao
	 * @param isFaturaAvulso
	 *            the is fatura avulso
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param contratoPontoConsumo
	 *            the contrato ponto consumo
	 * @param referenciaCicloAtual
	 *            the referencia ciclo atual
	 * @param consumoApuradoMedioPonto
	 *            the consumo apurado medio ponto
	 * @param consumoReal
	 *            the consumo real
	 * @param tipoConsumo
	 *            the tipo consumo
	 * @param primeiraMedicao
	 *            the primeira medicao
	 * @param consumoMedioApuradoDiarioPonto
	 *            the consumo medio apurado diario ponto
	 * @param historicosConsumoDiarios
	 *            the historicos consumo diarios
	 * @param historicosMedicaoCicloAtual
	 *            the historicos medicao ciclo atual
	 * @param logProcessamento
	 *            the log processamento
	 * @param isDrawback
	 *            the is drawback
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws GGASException 
	 */
	void inserirHistoricoConsumoCiclo(Map<String, Object> mapAtributosConsumo, HistoricoMedicao historicoUltimaLeitura,
			BigDecimal totalConsumoMedido, BigDecimal totalConsumoApurado, BigDecimal totalConsumo,
			Boolean isFaturaAvulso, PontoConsumo pontoConsumo, ContratoPontoConsumo contratoPontoConsumo,
			Map<String, Integer> referenciaCicloAtual, BigDecimal consumoApuradoMedioPonto, Boolean consumoReal,
			TipoConsumo tipoConsumo, Boolean primeiraMedicao, BigDecimal consumoMedioApuradoDiarioPonto,
			Set<HistoricoConsumo> historicosConsumoDiarios, Collection<HistoricoMedicao> historicosMedicaoCicloAtual,
			StringBuilder logProcessamento, Boolean isDrawback, AnormalidadeConsumo anormalidadeBloqueiaFaturamento) throws NegocioException, ConcorrenciaException,
			FormatoInvalidoException, GGASException;
	
	/**
	 * Método responsável pela conversão de
	 * medidas para uma unidade de conversão
	 * 
	 * @param valor - Valor a ser convertido.
	 * @param unidadeConversao - Unidade de Conversão
	 * @return BigDecimal - Valor da unidade origem convertido
	 * para a unidade padrão do sistema.
	 * @throws NegocioException - Caso ocorra algum erro na
	 * invocação do método.
	 */
	BigDecimal converterUnidadeMedida(BigDecimal valor, UnidadeConversao unidadeConversao) throws NegocioException;
	
	/**
	 * Obtém o último histórico de consumo faturado por ponto de consumo.
	 * Obtém os seguintes atributos de histórico de consumo: {@code chavePrimaria},
	 * {@code consumoCreditoMedia}.
	 * 
	 * @param chavesPontoConsumo {@link Long[]}
	 * @return mapa de HistoricoConsumo por PontoConsumo
	 */
	Map<PontoConsumo, HistoricoConsumo> obterUltimoHistoricoConsumoFaturado(Long[] chavesPontoConsumo);

	/**
	 * Obtém a soma do total de consumo apurado agrupado por ponto de consumo.
	 * @param chavesPontosConsumo {@link Long[]}
	 * @return mapa de total de consumo apurado por ponto de consumo
	 */
	Map<PontoConsumo, BigDecimal> consultarTotalDrawbackConsumoApuradoAnoMesGrupoFaturamentoCorrente(Long[] chavesPontosConsumo);

	/**
	 * Método auxiliar para cálculo do fator PTZ
	 * responsável por obter o fator Z
	 * a ser usado no cálculo.
	 * 
	 * @param contratoPontoConsumo {@link ContratoPontoConsumo}
	 * @param pressaoAnterior {@link BigDecimal}
	 * @param unidadeAnterior {@link Unidade}
	 * @param faixaPressaoFornecimentoPorSegmento {@link Collection}
	 * @return faixa pressão de fornecimento
	 * @throws NegocioException {@link NegocioException}
	 */
	FaixaPressaoFornecimento obterFaixaPressaoFornecimento(ContratoPontoConsumo contratoPontoConsumo, BigDecimal pressaoAnterior,
					Unidade unidadeAnterior, Collection<FaixaPressaoFornecimento> faixaPressaoFornecimentoPorSegmento)
									throws NegocioException;
	/**
	 * Consulta os históricos de consumo não faturados, por chave primária do ponto de consumo, ordenados por
	 * ano/mês faturamento e numero do ciclo de faturamento.
	 * 
	 * @param listaChavesPontoConsumo {@link List}
	 * @param anoMesFaturamento {@link Integer}
	 * @param numeroCiclo {@link Integer}
	 * @return mapa de lista de HistoricoConsumo por chave de PontoConsumo
	 */
	Map<Long, HistoricoConsumo> obterHistoricosDeConsumoNaoFaturados(List<Long> listaChavesPontoConsumo,
			Integer anoMesFaturamento, Integer numeroCiclo);

	/**
	 * Consulta os históricos de consumo por chave primária do ponto de consumo, por ano/mês faturamento,
	 * numero do ciclo de faturamento e indicador de faturamento.
	 * 
	 * @param chavesPontoConsumo {@link Long[]}
	 * @param anoMesFaturamento {@link Integer}
	 * @param numeroCiclo {@link Integer}
	 * @param indicadorFaturamento {@link Boolean}
	 * @return mapa de lista de HistoricoConsumo por chave de PontoConsumo
	 */
	Map<Long, List<HistoricoConsumo>> consultarHistoricoConsumoPorAnoMesFaturamentoEnumeroCiclo(Long[] chavesPontoConsumo,
					Integer anoMesFaturamento, Integer numeroCiclo, Boolean indicadorFaturamento);

	/**
	 * Método responsável por consultar os historicos do ponto de consumo, por referência e ciclo, agrupados por chave de ponto de consumo.
	 * 
	 * @param pontosConsumo - {@link Collection}
	 * @param anoMesFaturamento - {@link Integer}
	 * @param numeroCiclo - {@link Integer}
	 * @return históricos de consumo por chave de ponto de consumo - {@link Map}
	 */
	Map<Long, Collection<HistoricoConsumo>> consultarHistoricoConsumo(Collection<PontoConsumo> pontosConsumo, Integer anoMesFaturamento,
			Integer numeroCiclo);
	/**
	 * Realiza o agrupamento do historico de consumo por medidores independentes
	 *
	 *@param listaHistoricoConsumo {@link Collection}
	 *@throws NegocioException {@link NegocioException}
	 *@return map com listas agrupadas por cada medidor independente
	 */
	Map<Long, Collection<HistoricoConsumo>> agruparHistoricoConsumoPorMedidorIndependente(
			Collection<HistoricoConsumo> listaHistoricoConsumo) throws NegocioException;
	
	/**
	 * Obtem o historico de consumo detalhado por medididores independentes
	 *
	 *@param idPontoConsumo {@link Long}
	 *@param anoMesFaturamento {@link Integer}
	 *@param numeroCiclo {@link Integer}
	 *@throws NegocioException {@link NegocioException}
	 *@return colecao com historico de consumo detalhado por medidor independente
	 */
	Collection<HistoricoConsumo> obterHistoricoConsumoPorMedidoresIndependentes(
			Long idPontoConsumo, Integer anoMesFaturamento, Integer numeroCiclo) throws NegocioException;	
	
	/**
	 * Método para gerar Relatório de Consistir Leitura e  Consumo
	 * @param filtro {@link Map}
	 * @param processo {@link Processo}
	 * @param logProcessamento {@link StringBuilder}
	 * @return StringBuilder {@link StringBuilder}
	 * @throws GGASException {@link GGASException}
	 */
	StringBuilder criarRelatorioConsistirLeituraConsumo(Map<String, Object> filtro, Processo processo, StringBuilder logProcessamento)
			throws GGASException;

	/**
	 * Obté o histórico de consumo a partir do mes de faturamento do ponto de consumo
	 * @param chavePonto chave do ponto de consumo
	 * @param anoMesFaturamento ano mes de faturamento
	 * @param numeroCiclo número ciclo
	 * @return retorna o {@link ConsumoHistoricoDTO}
	 */
	ConsumoHistoricoDTO obterConsumoHistoricoNoMesFaturamento(Long chavePonto, Integer anoMesFaturamento, Integer numeroCiclo);

	/** 
	 * grupoFaturamento chave do grupo de faturamento
	 * anoMesReferencia refência do ano e mês 
	 * ciclo número do ciclo de fatumento
	 * 
	 * @param grupoFaturamento id Grupor Faturamento
	 * @param anoMesFaturamento anos Mes Faturamento
	 * @param ciclo número do ciclo de fatumento
	 * @return Long quantidade do Ponto Consumo
	 */
	Long consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamento(Long grupoFaturamento, Integer anoMesFaturamento,Integer ciclo);
	
	/** 
	 * Consultar pontos de consumo onde não exista anormalidade para o supervisorio
	 * 
	 * @param codigoAtividadeSistema chave da atividade a ser feito a consulta
	 * @param grupoFaturamento chave do grupo de faturamento
	 * @param anoMesReferencia refência do ano e mês 
	 * @param ciclo número do ciclo de fatumento
	 * @return quantidade de pontos de consumo - {@link Long}
	 * @throws GGASException - {@link GGASException}
	 */
	Long consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamentoSupervisorio(Long codigoAtividadeSistema, Long grupoFaturamento,
			Integer anoMesReferencia, Integer ciclo) throws GGASException;

	/**
	 * Analise parametrizacao consumo zero
	 * @param historicoConsumo - {@link HistoricoConsumo}
	 * @param parametrizacao - {@link AnormalidadeSegmentoParametro}
	 */
	void enviarHistoricoConsumoParaAnalise(HistoricoConsumo historicoConsumo,
			Collection<AnormalidadeSegmentoParametro> parametrizacao);
	
	/**
	 * Método responsável por remover da lista os pontos de consumo que já
	 * foram faturados
	 * @param pontosConsumo {@link List}}]
	 * @param rota {@link Rota}}
	 */
	void removerPontosConsumoFaturados(List<PontoConsumo> pontosConsumo, Rota rota);
	
	/** 
	 * Consultar historicos com anormalidade de consumo
	 * 
	 * @param rota Rota a ser consultada
	 * @param anoMesReferencia refência do ano e mês 
	 * @param ciclo número do ciclo de fatumento
	 * @return a lista de historicos com anormalidade {@link List}}
	 * @throws GGASException - {@link GGASException}
	 */
	List<HistoricoConsumo> consultarHistoricosComAnormalidadeDeConsumo(Rota rota, Integer anoMesReferencia, Integer ciclo);

	/**
	 * Consultar quantidade de pontos de consumos sem faturar
	 * @param grupoFaturamento - {@link GrupoFaturamento}
	 * @param anoMesReferencia - {@link Integer}
	 * @param ciclo - {@link Integer}s
	 * @return Qtd de pontos - {@link Integer}
	 */
	Long consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamentoSemFaturar(Long grupoFaturamento,
			Integer anoMesReferencia, Integer ciclo);
	
	/**
	 * Consulta o Historico Consumo inativo da referencia
	 * @param historicoConsumo - {@link HistoricoConsumo}
	 * @return HistoricoConsumo
	 */
	HistoricoConsumo obterUltimoHistoricoConsumoInativo(HistoricoConsumo historicoConsumo);

	/**
	 * Consultar quantidade de pontos de consumos com anormalidade
	 * que bloqueia faturamento nao analisados
	 * @param grupoFaturamento - {@link GrupoFaturamento}
	 * @param anoMesReferencia - {@link Integer}
	 * @param ciclo - {@link Integer}
	 * @return Qtd de pontos - {@link Integer}
	 */
	Long consultarQuantidadeQtdPontoConsumoComanormalidadeBloqueiaFaturamentoNaoAnalisada(Long grupoFaturamento,
			Integer anoMesReferencia, Integer ciclo);

	Long consultarQuantidadeQtdPontoConsumosAFaturar(Long grupoFaturamento, Integer anoMesReferencia, Integer ciclo);

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo, agrupados
	 * por referência e ciclo para a tela de
	 * analise de exceções
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave primaria do ponto de
	 *            consumo
	 * @param indicadorConsumoCiclo
	 *            Indicador de consumo ciclo
	 * @param referenciaInicial
	 *            the referencia inicial
	 * @param referenciaFinal
	 *            the referencia final
	 * @return Uma coleção de histórico
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoConsumo> consultarHistoricoConsumoParaAnaliseExcecoes(Long chavePrimariaPontoConsumo, boolean indicadorConsumoCiclo,
					String referenciaInicial, String referenciaFinal) throws NegocioException;

	FaixaConsumoVariacao obterMaiorFaixaConsumoPorSegmentoFaixaConsumo(Segmento segmento);

	Collection<HistoricoConsumoVO> consultarHistoricoConsumoLeituraMovimento(Long chavePrimariaPontoConsumo)
			throws NegocioException;

}
