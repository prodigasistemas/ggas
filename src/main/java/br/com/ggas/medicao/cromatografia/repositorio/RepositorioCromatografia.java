/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/09/2013 15:18:17
 @author vtavares
 */

package br.com.ggas.medicao.cromatografia.repositorio;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.cromatografia.dominio.Cromatografia;
import br.com.ggas.util.Util;

/**
 * Classe Repositorio Cromatografia.
 * 
 *
 */
@Repository
public class RepositorioCromatografia extends RepositorioGenerico {

	/**
	 * Instantiates a new repositorio cromatografia.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioCromatografia(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new Cromatografia();
	}

	@Override
	public Class<Cromatografia> getClasseEntidade() {

		return Cromatografia.class;
	}

	/**
	 * Obter cromatografia.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the cromatografia
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Cromatografia obterCromatografia(long chavePrimaria) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode("cityGate", FetchMode.JOIN);

		if(chavePrimaria != 0) {
			criteria.add(Restrictions.eq("chavePrimaria", chavePrimaria));
		}

		Collection<Cromatografia> list = criteria.list();
		for (Cromatografia cromatografia : list) {
			return cromatografia;
		}
		return null;
	}

	/**
	 * Consultar cromatografia.
	 * 
	 * @param cromatografia
	 *            the cromatografia
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<Cromatografia> consultarCromatografia(Cromatografia cromatografia, Map<String, String> filtro)
					throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode("cityGate", FetchMode.JOIN);

		if(cromatografia.getChavePrimaria() != 0) {
			criteria.add(Restrictions.eq("chavePrimaria", cromatografia.getChavePrimaria()));
		}
		criteria.createAlias("cityGate", "cityGate");
		if(cromatografia.getCityGate() != null) {
			criteria.add(Restrictions.eq("cityGate.chavePrimaria", cromatografia.getCityGate().getChavePrimaria()));
		}
		if(cromatografia.getData() != null) {
			criteria.add(Restrictions.eq("data", cromatografia.getData()));
		}

		if(filtro != null) {
			String descricaoCityGate = filtro.get("descricaoCityGate");
			if(StringUtils.isNotEmpty(descricaoCityGate)) {
				criteria.add(Restrictions.ilike("cityGate.descricao", Util.formatarTextoConsulta(descricaoCityGate)));
			}
		}

		return criteria.list();
	}

	/**
	 * Consultar cromatografia por city gate data leitura.
	 * 
	 * @param cityGate
	 *            the city gate
	 * @param dataleituraInformada
	 *            the dataleitura informada
	 * @return the Cromatografia
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Cromatografia consultarCromatografiaPorCityGateDataLeitura(CityGate cityGate, Date dataleituraInformada)
					throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode("cityGate", FetchMode.JOIN);
		criteria.createAlias("cityGate", "cityGate");
		if(cityGate != null) {
			criteria.add(Restrictions.eq("cityGate.chavePrimaria", cityGate.getChavePrimaria()));
		}
		if(dataleituraInformada != null) {
			criteria.add(Restrictions.le("data", dataleituraInformada));
		}
		criteria.addOrder(Order.desc("data"));
		
		@SuppressWarnings("unchecked")
		Collection<Cromatografia> lista = criteria.list();
		
		if (lista!=null && !lista.isEmpty()){
			return lista.iterator().next();
		}
		
		return null;
	}

	/**
	 * Listar cromatografia.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<Cromatografia> listarCromatografia() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.addOrder(Order.asc("data"));

		return criteria.list();
	}

	/**
	 * Consultar ultima cromatografia.
	 *
	 * @param cityGate the city gate
	 * @return the cromatografia
	 */
	public Cromatografia consultarUltimaCromatografia(CityGate cityGate) {
		Criteria criteria = createCriteria(getClasseEntidade())
				.createAlias("cityGate", "cityGate")
				.add(Restrictions.eq("cityGate.chavePrimaria", cityGate.getChavePrimaria()))
				.addOrder(Order.desc("data"))
				.setFirstResult(0)
				.setMaxResults(1);
		return (Cromatografia) criteria.uniqueResult();
	}

}
