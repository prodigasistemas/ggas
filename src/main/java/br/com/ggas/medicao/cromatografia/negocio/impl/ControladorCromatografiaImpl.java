/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 17/03/2015 10:18:31
 @author lacavalcanti
 */

package br.com.ggas.medicao.cromatografia.negocio.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.cromatografia.dominio.Cromatografia;
import br.com.ggas.medicao.cromatografia.negocio.ControladorCromatografia;
import br.com.ggas.medicao.cromatografia.repositorio.RepositorioCromatografia;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
/**
 * Classe responsável pela implementação dos métodos relacionados a Cromatografia
 *
 */
@Service("controladorCromatografia")
@Transactional
public class ControladorCromatografiaImpl implements ControladorCromatografia {

	@Autowired
	private RepositorioCromatografia repositorioCromatografia;

	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;

	public ControladorPontoConsumo getControladorPontoConsumo() {

		return controladorPontoConsumo;
	}

	public void setControladorPontoConsumo(ControladorPontoConsumo controladorPontoConsumo) {

		this.controladorPontoConsumo = controladorPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.cromatografia.negocio.ControladorCromatografia#inserir(br.com.ggas.medicao.cromatografia.dominio.Cromatografia)
	 */
	@Override
	@Transactional
	public Cromatografia inserir(Cromatografia cromatografia) throws NegocioException {

		validarExistenciaCromatografia(cromatografia);
		BigDecimal somaDosComponentes = BigDecimal.ZERO;
		tornaValoresNulosEmZero(cromatografia);
		somaDosComponentes = cromatografia.getAgua().add(cromatografia.getiButano()).add(cromatografia.getnHeptano())
						.add(cromatografia.getArgonio()).add(cromatografia.getiPentano()).add(cromatografia.getnHexano())
						.add(cromatografia.getDioxidoCarbono()).add(cromatografia.getMetano()).add(cromatografia.getNitrogenio())
						.add(cromatografia.getEtano()).add(cromatografia.getMonoxidoCarbono()).add(cromatografia.getnNonano())
						.add(cromatografia.getHelio()).add(cromatografia.getnButano()).add(cromatografia.getnOctano())
						.add(cromatografia.getHidrogenio()).add(cromatografia.getnDecano()).add(cromatografia.getnPentano())
						.add(cromatografia.getOxigenio()).add(cromatografia.getPropano()).add(cromatografia.getSulfetoHidrogenio());

		validarSomaPercentualComponentesCromatografia(somaDosComponentes);
		repositorioCromatografia.inserir(cromatografia);
		return cromatografia;
	}

	private void validarExistenciaCromatografia(Cromatografia cromatografia) throws NegocioException {
		CityGate cityGate = cromatografia.getCityGate();
		Date data = cromatografia.getData();
		Cromatografia ultimaCromatografiaCadastrada = consultarUltimaCromatografia(cityGate);
		boolean existente = ultimaCromatografiaCadastrada != null;
		if (existente) {
			Date dataUltimaCromatografia = ultimaCromatografiaCadastrada.getData();
			int result = data.compareTo(dataUltimaCromatografia);
			boolean dataVigenteExiste = result <= 0;
			if (dataVigenteExiste) {
				Object[] elementosMensagem = new Object[] {cityGate.getDescricao(), DataUtil
								.converterDataParaString(dataUltimaCromatografia)};
				throw new NegocioException(Constantes.ERRO_NEGOCIO_CROMATOGRAFIA_EXISTENTE, elementosMensagem);
			}
		}
	}

	private void validarSomaPercentualComponentesCromatografia(BigDecimal somaDosComponentes) throws NegocioException {
		BigDecimal cemPorCento = new BigDecimal(100);
		if (cemPorCento.compareTo(somaDosComponentes) != 0) {
			throw new NegocioException(Constantes.ERRO_SOMA_DOS_COMPONENTES, true);
		}
	}

	/**
	 * Torna valores nulos em zero.
	 * 
	 * @param cromatografia
	 *            the cromatografia
	 */
	private void tornaValoresNulosEmZero(Cromatografia cromatografia) {

		if(cromatografia.getAgua() == null) {
			cromatografia.setAgua(new BigDecimal(0));
		}
		if(cromatografia.getiButano() == null) {
			cromatografia.setiButano(new BigDecimal(0));
		}
		if(cromatografia.getnHeptano() == null) {
			cromatografia.setnHeptano(new BigDecimal(0));
		}
		if(cromatografia.getPropano() == null) {
			cromatografia.setPropano(new BigDecimal(0));
		}
		if(cromatografia.getSulfetoHidrogenio() == null) {
			cromatografia.setSulfetoHidrogenio(new BigDecimal(0));
		}
		if(cromatografia.getArgonio() == null) {
			cromatografia.setArgonio(new BigDecimal(0));
		}
		if(cromatografia.getOxigenio() == null) {
			cromatografia.setOxigenio(new BigDecimal(0));
		}
		if(cromatografia.getnPentano() == null) {
			cromatografia.setnPentano(new BigDecimal(0));
		}
		if(cromatografia.getnDecano() == null) {
			cromatografia.setnDecano(new BigDecimal(0));
		}
		if(cromatografia.getHidrogenio() == null) {
			cromatografia.setHidrogenio(new BigDecimal(0));
		}
		if(cromatografia.getnOctano() == null) {
			cromatografia.setnOctano(new BigDecimal(0));
		}
		if(cromatografia.getnButano() == null) {
			cromatografia.setnButano(new BigDecimal(0));
		}
		if(cromatografia.getHelio() == null) {
			cromatografia.setHelio(new BigDecimal(0));
		}
		if(cromatografia.getnNonano() == null) {
			cromatografia.setnNonano(new BigDecimal(0));
		}
		if(cromatografia.getMonoxidoCarbono() == null) {
			cromatografia.setMonoxidoCarbono(new BigDecimal(0));
		}
		if(cromatografia.getEtano() == null) {
			cromatografia.setEtano(new BigDecimal(0));
		}
		if(cromatografia.getNitrogenio() == null) {
			cromatografia.setNitrogenio(new BigDecimal(0));
		}
		if(cromatografia.getMetano() == null) {
			cromatografia.setMetano(new BigDecimal(0));
		}
		if(cromatografia.getDioxidoCarbono() == null) {
			cromatografia.setDioxidoCarbono(new BigDecimal(0));
		}
		if(cromatografia.getnHexano() == null) {
			cromatografia.setnHexano(new BigDecimal(0));
		}
		if(cromatografia.getiPentano() == null) {
			cromatografia.setiPentano(new BigDecimal(0));
		}
		if(cromatografia.getPressao() == null) {
			cromatografia.setPressao(new BigDecimal(0));
		}
		if(cromatografia.getTemperatura() == null) {
			cromatografia.setTemperatura(new BigDecimal(0));
		}
		if(cromatografia.getFatorCompressibilidade() == null) {
			cromatografia.setFatorCompressibilidade(new BigDecimal(0));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.cromatografia.negocio.ControladorCromatografia#consultarCromatografia(br.com.ggas.medicao.cromatografia.dominio
	 * .Cromatografia, java.util.Map)
	 */
	@Override
	public Collection<Cromatografia> consultarCromatografia(Cromatografia cromatografia, Map<String, String> filtro)
					throws NegocioException {

		return repositorioCromatografia.consultarCromatografia(cromatografia, filtro);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.cromatografia.negocio.ControladorCromatografia#consultarCromatografiaPorCityGateDataLeitura(br.com.ggas.cadastro
	 * .operacional.CityGate, java.util.Date)
	 */
	@Override
	public Cromatografia consultarCromatografiaPorCityGateDataLeitura(CityGate cityGate, Date dataleituraInformada)
					throws NegocioException {

		return repositorioCromatografia.consultarCromatografiaPorCityGateDataLeitura(cityGate, dataleituraInformada);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.cromatografia.negocio.ControladorCromatografia#obterCromatografia(long)
	 */
	@Override
	public Cromatografia obterCromatografia(long chavePrimaria) throws NegocioException {

		return this.repositorioCromatografia.obterCromatografia(chavePrimaria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.cromatografia.negocio.ControladorCromatografia#obterTodos()
	 */
	@Override
	public Collection<EntidadeNegocio> obterTodos() throws NegocioException {

		return repositorioCromatografia.obterTodas();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.cromatografia.negocio.ControladorCromatografia#atualizarCromatografia(br.com.ggas.medicao.cromatografia.dominio
	 * .Cromatografia)
	 */
	@Transactional
	@Override
	public void atualizarCromatografia(Cromatografia cromatografia) throws NegocioException, ConcorrenciaException {

		repositorioCromatografia.atualizar(cromatografia, Cromatografia.class);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.cromatografia.negocio.ControladorCromatografia#removerCromatografia(java.lang.Long[])
	 */
	@Transactional
	@Override
	public void removerCromatografia(Long[] chaves) throws NegocioException {

		Cromatografia cromatografia;
		for (Long chave : chaves) {
			if(chave != null) {
				cromatografia = this.obterCromatografia(chave);
				repositorioCromatografia.remover(cromatografia);
			}
		}
	}

	@Override
	public Cromatografia consultarUltimaCromatografia(CityGate cityGate) throws NegocioException {
		return repositorioCromatografia.consultarUltimaCromatografia(cityGate);
	}

}
