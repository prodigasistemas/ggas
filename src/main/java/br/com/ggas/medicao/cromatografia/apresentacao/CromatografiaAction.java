/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/09/2013 15:35:42
 @author vtavares
 */

package br.com.ggas.medicao.cromatografia.apresentacao;

import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.cadastro.operacional.impl.CityGateImpl;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.cromatografia.dominio.Cromatografia;
import br.com.ggas.medicao.cromatografia.negocio.ControladorCromatografia;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.medicao.vazaocorretor.impl.UnidadeImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Classe responsável pelas telas relacionadas a Cromatografia. 
 *
 */
@Controller
public class CromatografiaAction extends GenericAction {

	private static final String DATA_CROMATOGRAFIA = "dataCromatografia";

	private static final String CROMATOGRAFIA2 = "Cromatografia";

	private static final String ID_CITY_GATE = "idCityGate";

	private static final String CROMATOGRAFIA = "cromatografia";

	private static final String CALCULADO = "calculado";

	private static final String LISTA_CITY_GATE = "listaCityGate";

	private static final String DESCRICAO_CITY_GATE = "descricaoCityGate";

	private static final String TELA_EXIBIR_PESQUISA_CROMATOGRAFIA = "exibirPesquisaCromatografia";

	private static final String CITY_GATE_NOME_CLASSE = "br.com.ggas.cadastro.operacional.impl.CityGateImpl";

	private static final Logger LOG = Logger.getLogger(CromatografiaAction.class);

	@Autowired
	private ControladorCromatografia controladorCromatografia;

	private Fachada fachada = Fachada.getInstancia();

	public ControladorCromatografia getControladorCromatografia() {

		return controladorCromatografia;
	}

	public void setControladorCromatografia(ControladorCromatografia controladorCromatografia) {

		this.controladorCromatografia = controladorCromatografia;
	}

	/**
	 * Exibir pesquisa cromatografia.
	 * 
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping(TELA_EXIBIR_PESQUISA_CROMATOGRAFIA)
	public ModelAndView exibirPesquisaCromatografia() {

		return new ModelAndView(TELA_EXIBIR_PESQUISA_CROMATOGRAFIA);
	}

	/**
	 * Pesquisar city gate.
	 * 
	 * @param descricaoCityGate
	 *            the descricao city gate
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarCityGate")
	public ModelAndView pesquisarCityGate(@RequestParam(value = DESCRICAO_CITY_GATE, required = true) String descricaoCityGate)
					throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_CROMATOGRAFIA);
		Map<String, Object> filtro = new HashMap<String, Object>();
		if (descricaoCityGate != null && !StringUtils.isEmpty(descricaoCityGate)) {
			filtro.put("descricao", descricaoCityGate);
		}
		filtro.put("habilitado", true);
		Collection<TabelaAuxiliar> listaTabelaAuxiliar = fachada.pesquisarTabelaAuxiliar(filtro, CITY_GATE_NOME_CLASSE);
		model.addObject(DESCRICAO_CITY_GATE, descricaoCityGate);
		model.addObject(LISTA_CITY_GATE, listaTabelaAuxiliar);
		return model;
	}

	/**
	 * Exibir inclusao cromatografia.
	 * 
	 * @param chavePrimariaCityGate
	 *            the chave primaria city gate
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirInclusaoCromatografia")
	public ModelAndView exibirInclusaoCromatografia(
					@RequestParam(value = "chavePrimariaCityGate", required = true) long chavePrimariaCityGate, HttpServletRequest request)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirInclusaoCromatografia");

		Cromatografia cromatografia = new Cromatografia();

		obterCityGateEPopularEmCromatografia(chavePrimariaCityGate, model, cromatografia);
		BigDecimal pressaoReferencia = obterValorReferencia(Constantes.PARAMETRO_PRESSAO_CONDICAO_REFERENCIA);
		cromatografia.setPressao(pressaoReferencia);
		BigDecimal temperaturaReferencia = obterValorReferencia(Constantes.PARAMETRO_TEMPERATURA_CONDICAO_REFERENCIA);
		BigDecimal temperaturaReferenciaCelsius = converterKelvinParaCelsius(temperaturaReferencia);
		cromatografia.setTemperatura(temperaturaReferenciaCelsius);
		model.addObject(CALCULADO, "false");
		model.addObject(CROMATOGRAFIA, cromatografia);
		model.addObject(ID_CITY_GATE, chavePrimariaCityGate);
		return model;
	}

	private BigDecimal converterKelvinParaCelsius(BigDecimal temperaturaKelvin) throws NegocioException {
		Unidade unidadeOrigem = obterUnidadeTemperaturaPorDescricao("kelvin");
		Unidade unidadeDestino = obterUnidadeTemperaturaPorDescricao("celsius");

		return fachada.converterUnidadeMedida(temperaturaKelvin, unidadeOrigem, unidadeDestino);
	}

	private Unidade obterUnidadeTemperaturaPorDescricao(String descricao) {
		Unidade unidadeFiltro = new UnidadeImpl();
		unidadeFiltro.setDescricao(descricao);
		return fachada.obterUnidade(unidadeFiltro);
	}

	private BigDecimal obterValorReferencia(String nomeParametro) throws GGASException {

		Object valorParametro = fachada.obterValorDoParametroPorCodigo(nomeParametro);
		String valorParametroStr = (String) valorParametro;
		valorParametroStr = valorParametroStr.replace(",", ".");
		return new BigDecimal(valorParametroStr);
	}

	/**
	 * Obter city gate e popular em cromatografia.
	 * 
	 * @param chavePrimariaCityGate
	 *            the chave primaria city gate
	 * @param model
	 *            the model
	 * @param cromatografia
	 *            the cromatografia
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void obterCityGateEPopularEmCromatografia(long chavePrimariaCityGate, ModelAndView model, Cromatografia cromatografia)
					throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		if (chavePrimariaCityGate != 0) {
			filtro.put("chavePrimaria", chavePrimariaCityGate);
			Collection<TabelaAuxiliar> listaTabelaAuxiliar = fachada.pesquisarTabelaAuxiliar(filtro, CITY_GATE_NOME_CLASSE);
			if (model != null) {
				model.addObject(LISTA_CITY_GATE, listaTabelaAuxiliar);
			}
			CityGate cityGate = null;
			for (TabelaAuxiliar tabelaAuxiliar : listaTabelaAuxiliar) {
				cityGate = (CityGateImpl) tabelaAuxiliar;
				if (model != null) {
					model.addObject(DESCRICAO_CITY_GATE, cityGate.getDescricao());
				}
			}
			if (cityGate != null && cromatografia != null) {
				cromatografia.setCityGate(cityGate);
			}
		}

	}

	/**
	 * Incluir cromatografia.
	 * 
	 * @param cromatografia
	 *            the cromatografia
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @param idCityGate
	 *            the id city gate
	 * @param dataCromatografia
	 *            the data cromatografia
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping(value = "incluirCromatografia", method = RequestMethod.POST)
	public ModelAndView incluirCromatografia(@ModelAttribute(CROMATOGRAFIA2) Cromatografia cromatografia, BindingResult result,
					HttpServletRequest request, @RequestParam(ID_CITY_GATE) Long idCityGate,
					@RequestParam(DATA_CROMATOGRAFIA) String dataCromatografia) throws GGASException {

		if (dataCromatografia != null && Util.isDataValida(dataCromatografia, Constantes.FORMATO_DATA_BR)) {
			cromatografia.setData(Util.converterCampoStringParaData("data", dataCromatografia, Constantes.FORMATO_DATA_BR));
		}

		Map<String, Object> filtro = new HashMap<String, Object>();
		if (idCityGate != 0) {
			filtro.put("chavePrimaria", idCityGate);
		}

		Collection<TabelaAuxiliar> listaTabelaAuxiliar = fachada.pesquisarTabelaAuxiliar(filtro, CITY_GATE_NOME_CLASSE);
		for (TabelaAuxiliar cityGate : listaTabelaAuxiliar) {
			cromatografia.setCityGate((CityGate) cityGate);
		}
		ModelAndView model = new ModelAndView("forward:/exibirPesquisaCromatografia");
		try {
			controladorCromatografia.inserir(cromatografia);

			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_INCLUIDA, CROMATOGRAFIA2);
			return pesquisarCromatografia(idCityGate, null, dataCromatografia, request);
		} catch (GGASException e) {

			model = new ModelAndView("exibirInclusaoCromatografia");
			model.addObject(CROMATOGRAFIA, cromatografia);
			model.addObject(CALCULADO, "true");
			model.addObject(DATA_CROMATOGRAFIA, dataCromatografia);
			model.addObject(LISTA_CITY_GATE, listaTabelaAuxiliar);
			model.addObject(ID_CITY_GATE, idCityGate);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Pesquisar cromatografia.
	 * 
	 * @param chavePrimariaCityGate
	 *            the chave primaria city gate
	 * @param descricaoCityGate
	 *            the descricao city gate
	 * @param data
	 *            the data
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarCromatografia")
	public ModelAndView pesquisarCromatografia(@RequestParam(value = "chavePrimariaCityGate", required = false) long chavePrimariaCityGate,
					@RequestParam(value = DESCRICAO_CITY_GATE, required = false) String descricaoCityGate,
					@RequestParam(value = "data", required = false) String data, HttpServletRequest request) throws GGASException {

		ModelAndView model = new ModelAndView(TELA_EXIBIR_PESQUISA_CROMATOGRAFIA);

		Cromatografia cromatografia = new Cromatografia();
		obterCityGateEPopularEmCromatografia(chavePrimariaCityGate, null, cromatografia);

		if (data != null && !("".equals(data))) {
			cromatografia.setData(Util.converterCampoStringParaData("data", data, Constantes.FORMATO_DATA_BR));
		}

		Map<String, String> filtro = new HashMap<String, String>();
		if (StringUtils.isNotEmpty(descricaoCityGate)) {
			filtro.put(DESCRICAO_CITY_GATE, descricaoCityGate);
		}
		Collection<Cromatografia> listaCromatografia = controladorCromatografia.consultarCromatografia(cromatografia, filtro);

		model.addObject("data", data);
		model.addObject(DESCRICAO_CITY_GATE, descricaoCityGate);
		model.addObject(CROMATOGRAFIA, cromatografia);
		model.addObject("listaCromatografia", listaCromatografia);
		return model;
	}

	/**
	 * Exibir alteracao cromatografia.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirAlteracaoCromatografia")
	public ModelAndView exibirAlteracaoCromatografia(@RequestParam("chavePrimaria") Long chavePrimaria, HttpServletRequest request)
					throws GGASException {

		ModelAndView model = new ModelAndView("exibirAlteracaoCromatografia");

		Cromatografia cromatografiaModificada = (Cromatografia) request.getAttribute("cromatografiaModificada");

		if (cromatografiaModificada == null) {
			Cromatografia cromatografia = controladorCromatografia.obterCromatografia(chavePrimaria);
			if (cromatografia != null) {
				long idCityGate = cromatografia.getCityGate().getChavePrimaria();
				obterCityGateEPopularEmCromatografia(idCityGate, model, null);
				model.addObject(ID_CITY_GATE, idCityGate);
				model.addObject(DATA_CROMATOGRAFIA, cromatografia.getDataFormatada());
				model.addObject(CROMATOGRAFIA, cromatografia);
				model.addObject(CALCULADO, "true");
			}
		} else {
			long idCityGate = cromatografiaModificada.getCityGate().getChavePrimaria();
			obterCityGateEPopularEmCromatografia(idCityGate, model, null);
			model.addObject(CALCULADO, "true");
			model.addObject(ID_CITY_GATE, idCityGate);
			model.addObject(DATA_CROMATOGRAFIA, cromatografiaModificada.getDataFormatada());
			model.addObject(CROMATOGRAFIA, cromatografiaModificada);
		}

		return model;
	}

	/**
	 * Alterar cromatografia.
	 * 
	 * @param cromatografia
	 *            the cromatografia
	 * @param result
	 *            the result
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param idCityGate
	 *            the id city gate
	 * @param dataCromatografia
	 *            the data cromatografia
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("alterarCromatografia")
	public ModelAndView alterarCromatografia(@ModelAttribute(CROMATOGRAFIA2) Cromatografia cromatografia, BindingResult result,
					@RequestParam("chavePrimaria") Long chavePrimaria, @RequestParam(ID_CITY_GATE) Long idCityGate,
					@RequestParam(DATA_CROMATOGRAFIA) String dataCromatografia, HttpServletRequest request) throws GGASException {

		ModelAndView model = null;
		try {
			if (dataCromatografia != null && !("".equals(dataCromatografia))) {
				cromatografia.setData(Util.converterCampoStringParaData("data", dataCromatografia, Constantes.FORMATO_DATA_BR));
			}
			obterCityGateEPopularEmCromatografia(idCityGate, null, cromatografia);
			controladorCromatografia.atualizarCromatografia(cromatografia);
			model = this.pesquisarCromatografia(idCityGate, null, dataCromatografia, request);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, CROMATOGRAFIA2);
		} catch (GGASException e) {
			model = new ModelAndView("forward:/exibirAlteracaoCromatografia");
			obterCityGateEPopularEmCromatografia(idCityGate, model, null);
			request.setAttribute("cromatografiaModificada", cromatografia);
			mensagemErroParametrizado(model, e);
		}

		return model;
	}

	/**
	 * Exibir detalhamento cromatografia.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the model and view
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("exibirDetalhamentoCromatografia")
	public ModelAndView exibirDetalhamentoCromatografia(@RequestParam("chavePrimaria") Long chavePrimaria) throws GGASException {

		Long idCromatografia = chavePrimaria;

		ModelAndView model = new ModelAndView("exibirDetalhamentoCromatografia");
		Cromatografia cromatografia = controladorCromatografia.obterCromatografia(idCromatografia);
		if (cromatografia != null) {
			CityGate cityGate = cromatografia.getCityGate();
			List<CityGate> listaCityGate = new ArrayList<CityGate>();
			listaCityGate.add(cityGate);
			model.addObject(CROMATOGRAFIA, cromatografia);
			model.addObject("chavePrimariaCityGate", cityGate.getChavePrimaria());
			model.addObject(LISTA_CITY_GATE, listaCityGate);
		}
		return model;
	}

	/**
	 * Remover cromatografia.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("removerCromatografia")
	public ModelAndView removerCromatografia(@RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request)
					throws NegocioException {

		ModelAndView model = new ModelAndView("forward:/exibirPesquisaCromatografia");
		try {
			controladorCromatografia.removerCromatografia(chavesPrimarias);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_EXCLUIDO, CROMATOGRAFIA2);
		} catch (NegocioException e) {
			LOG.error(e.getMessage(), e);
			try {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_MATERIAL_RELACIONADO, true);
			} catch (NegocioException ex) {
				mensagemErroParametrizado(model, ex);
			}
		}
		return model;
	}

}
