/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 17/03/2015 09:21:20
 @author lacavalcanti
 */

package br.com.ggas.medicao.cromatografia.dominio;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela Cromatografia, especificação das características dos gás.
 *
 */
public class Cromatografia extends EntidadeNegocioImpl {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = 1L;

	private Date data;

	private BigDecimal pcs;

	private BigDecimal pressao;

	private BigDecimal temperatura;

	private BigDecimal agua;

	private BigDecimal iButano;

	private BigDecimal nHeptano;

	private BigDecimal argonio;

	private BigDecimal iPentano;

	private BigDecimal nHexano;

	private BigDecimal dioxidoCarbono;

	private BigDecimal metano;

	private BigDecimal nitrogenio;

	private BigDecimal etano;

	private BigDecimal monoxidoCarbono;

	private BigDecimal nNonano;

	private BigDecimal helio;

	private BigDecimal nButano;

	private BigDecimal nOctano;

	private BigDecimal hidrogenio;

	private BigDecimal nDecano;

	private BigDecimal nPentano;

	private BigDecimal oxigenio;

	private BigDecimal propano;

	private BigDecimal sulfetoHidrogenio;

	private BigDecimal fatorCompressibilidade;

	private CityGate cityGate;

	public Date getData() {
		Date dataTmp = null;
		if(this.data != null) {
			dataTmp = (Date) data.clone();
		}
		return dataTmp;
	}

	public void setData(Date data) {
		if(data != null) {
			this.data = (Date) data.clone();
		} else {
			this.data = null;
		}
	}

	public BigDecimal getPcs() {

		return pcs;
	}

	public void setPcs(BigDecimal pcs) {

		this.pcs = pcs;
	}

	public BigDecimal getPressao() {

		return pressao;
	}

	public void setPressao(BigDecimal pressao) {

		this.pressao = pressao;
	}

	public BigDecimal getTemperatura() {

		return temperatura;
	}

	public void setTemperatura(BigDecimal temperatura) {

		this.temperatura = temperatura;
	}

	public BigDecimal getAgua() {

		return agua;
	}

	public void setAgua(BigDecimal agua) {

		this.agua = agua;
	}

	/**
	 * Gets the i butano.
	 * 
	 * @return the i butano
	 */
	public BigDecimal getiButano() {

		return iButano;
	}

	/**
	 * Sets the i butano.
	 * 
	 * @param iButano
	 *            the new i butano
	 */
	public void setiButano(BigDecimal iButano) {

		this.iButano = iButano;
	}

	/**
	 * Gets the n heptano.
	 * 
	 * @return the n heptano
	 */
	public BigDecimal getnHeptano() {

		return nHeptano;
	}

	/**
	 * Sets the n heptano.
	 * 
	 * @param nHeptano
	 *            the new n heptano
	 */
	public void setnHeptano(BigDecimal nHeptano) {

		this.nHeptano = nHeptano;
	}

	public BigDecimal getArgonio() {

		return argonio;
	}

	public void setArgonio(BigDecimal argonio) {

		this.argonio = argonio;
	}

	/**
	 * Gets the i pentano.
	 * 
	 * @return the i pentano
	 */
	public BigDecimal getiPentano() {

		return iPentano;
	}

	/**
	 * Sets the i pentano.
	 * 
	 * @param iPentano
	 *            the new i pentano
	 */
	public void setiPentano(BigDecimal iPentano) {

		this.iPentano = iPentano;
	}

	/**
	 * Gets the n hexano.
	 * 
	 * @return the n hexano
	 */
	public BigDecimal getnHexano() {

		return nHexano;
	}

	/**
	 * Sets the n hexano.
	 * 
	 * @param nHexano
	 *            the new n hexano
	 */
	public void setnHexano(BigDecimal nHexano) {

		this.nHexano = nHexano;
	}

	public BigDecimal getDioxidoCarbono() {

		return dioxidoCarbono;
	}

	public void setDioxidoCarbono(BigDecimal dioxidoCarbono) {

		this.dioxidoCarbono = dioxidoCarbono;
	}

	public BigDecimal getMetano() {

		return metano;
	}

	public void setMetano(BigDecimal metano) {

		this.metano = metano;
	}

	public BigDecimal getNitrogenio() {

		return nitrogenio;
	}

	public void setNitrogenio(BigDecimal nitrogenio) {

		this.nitrogenio = nitrogenio;
	}

	public BigDecimal getEtano() {

		return etano;
	}

	public void setEtano(BigDecimal etano) {

		this.etano = etano;
	}

	public BigDecimal getMonoxidoCarbono() {

		return monoxidoCarbono;
	}

	public void setMonoxidoCarbono(BigDecimal monoxidoCarbono) {

		this.monoxidoCarbono = monoxidoCarbono;
	}

	/**
	 * Gets the n nonano.
	 * 
	 * @return the n nonano
	 */
	public BigDecimal getnNonano() {

		return nNonano;
	}

	/**
	 * Sets the n nonano.
	 * 
	 * @param nNonano
	 *            the new n nonano
	 */
	public void setnNonano(BigDecimal nNonano) {

		this.nNonano = nNonano;
	}

	public BigDecimal getHelio() {

		return helio;
	}

	public void setHelio(BigDecimal helio) {

		this.helio = helio;
	}

	/**
	 * Gets the n butano.
	 * 
	 * @return the n butano
	 */
	public BigDecimal getnButano() {

		return nButano;
	}

	/**
	 * Sets the n butano.
	 * 
	 * @param nButano
	 *            the new n butano
	 */
	public void setnButano(BigDecimal nButano) {

		this.nButano = nButano;
	}

	/**
	 * Gets the n octano.
	 * 
	 * @return the n octano
	 */
	public BigDecimal getnOctano() {

		return nOctano;
	}

	/**
	 * Sets the n octano.
	 * 
	 * @param nOctano
	 *            the new n octano
	 */
	public void setnOctano(BigDecimal nOctano) {

		this.nOctano = nOctano;
	}

	public BigDecimal getHidrogenio() {

		return hidrogenio;
	}

	public void setHidrogenio(BigDecimal hidrogenio) {

		this.hidrogenio = hidrogenio;
	}

	/**
	 * Gets the n decano.
	 * 
	 * @return the n decano
	 */
	public BigDecimal getnDecano() {

		return nDecano;
	}

	/**
	 * Sets the n decano.
	 * 
	 * @param nDecano
	 *            the new n decano
	 */
	public void setnDecano(BigDecimal nDecano) {

		this.nDecano = nDecano;
	}

	/**
	 * Gets the n pentano.
	 * 
	 * @return the n pentano
	 */
	public BigDecimal getnPentano() {

		return nPentano;
	}

	/**
	 * Sets the n pentano.
	 * 
	 * @param nPentano
	 *            the new n pentano
	 */
	public void setnPentano(BigDecimal nPentano) {

		this.nPentano = nPentano;
	}

	public BigDecimal getOxigenio() {

		return oxigenio;
	}

	public void setOxigenio(BigDecimal oxigenio) {

		this.oxigenio = oxigenio;
	}

	public BigDecimal getPropano() {

		return propano;
	}

	public void setPropano(BigDecimal propano) {

		this.propano = propano;
	}

	public BigDecimal getSulfetoHidrogenio() {

		return sulfetoHidrogenio;
	}

	public void setSulfetoHidrogenio(BigDecimal sulfetoHidrogenio) {

		this.sulfetoHidrogenio = sulfetoHidrogenio;
	}

	public BigDecimal getFatorCompressibilidade() {

		return fatorCompressibilidade;
	}

	public void setFatorCompressibilidade(BigDecimal fatorCompressibilidade) {

		this.fatorCompressibilidade = fatorCompressibilidade;
	}

	public CityGate getCityGate() {

		return cityGate;
	}

	public void setCityGate(CityGate cityGate) {

		this.cityGate = cityGate;
	}

	public String getDataFormatada() {

		if(data != null) {
			return Util.converterDataParaString(data);
		}
		return null;
	}

	public String getFatorFormatado() {

		if(fatorCompressibilidade != null) {
			return fatorCompressibilidade.toString().replace('.', ',');
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(data == null) {
			stringBuilder.append(Constantes.DATA_CROMATOGRAFIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(pressao == null || pressao.compareTo(BigDecimal.valueOf(0L))==0) {
			stringBuilder.append(Constantes.PRESSAO_CROMATOGRAFIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(temperatura == null || temperatura.compareTo(BigDecimal.valueOf(0L))==0) {
			stringBuilder.append(Constantes.TEMPERATURA_CROMATOGRAFIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(fatorCompressibilidade == null || fatorCompressibilidade.compareTo(new BigDecimal(0))==0) {
			stringBuilder.append(Constantes.FATOR_COMPRESSIBILIDADE_CROMATOGRAFIA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(cityGate == null) {
			stringBuilder.append(Constantes.CITY_GATE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {

			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));

		}

		return erros;
	}
}
