/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 17/03/2015 10:15:06
 @author lacavalcanti
 */

package br.com.ggas.medicao.cromatografia.negocio;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.cromatografia.dominio.Cromatografia;

/**
 * Classe Controlador Cromatografia.
 * 
 *
 */
public interface ControladorCromatografia {

	/**
	 * Inserir.
	 * 
	 * @param cromatografia
	 *            the cromatografia
	 * @return the cromatografia
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Cromatografia inserir(Cromatografia cromatografia) throws NegocioException;

	/**
	 * Consultar cromatografia.
	 * 
	 * @param cromatografia
	 *            the cromatografia
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<Cromatografia> consultarCromatografia(Cromatografia cromatografia, Map<String, String> filtro)
					throws NegocioException;

	/**
	 * Consultar cromatografia por city gate data leitura.
	 * 
	 * @param cityGate
	 *            the city gate
	 * @param dataleituraInformada
	 *            the dataleitura informada
	 * @return the Cromatografia
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Cromatografia consultarCromatografiaPorCityGateDataLeitura(CityGate cityGate, Date dataleituraInformada)
					throws NegocioException;

	/**
	 * Obter cromatografia.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the cromatografia
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Cromatografia obterCromatografia(long chavePrimaria) throws NegocioException;

	/**
	 * Obter todos.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeNegocio> obterTodos() throws NegocioException;

	/**
	 * Atualizar cromatografia.
	 * 
	 * @param cromatografia
	 *            the cromatografia
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarCromatografia(Cromatografia cromatografia) throws NegocioException, ConcorrenciaException;

	/**
	 * Remover cromatografia.
	 * 
	 * @param chaves
	 *            the chaves
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerCromatografia(Long[] chaves) throws NegocioException;
	
	/**
	 * Consultar ultima cromatografia.
	 *
	 * @param cityGate the city gate
	 * @return the cromatografia
	 * @throws NegocioException the negocio exception
	 */
	Cromatografia consultarUltimaCromatografia(CityGate cityGate) throws NegocioException;

}
