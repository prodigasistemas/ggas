/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.medidor;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.leitura.MedidorProtecao;
import br.com.ggas.medicao.medidor.impl.FiltroHistoricoOperacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.FaixaTemperaturaTrabalho;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Interface Controlador Medidor.
 * 
 *
 */
public interface ControladorMedidor extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_MEDIDOR = "controladorMedidor";

	String ERRO_NEGOCIO_MEDIDOR_EXISTENTE = "ERRO_NEGOCIO_MEDIDOR_EXISTENTE";

	String FALHA_FORMATO_NUMERO_SERIE = "FALHA_FORMATO_NUMERO_SERIE";

	String ERRO_DATA_CALIBRACAO_MAIOR_DATA_AQUISICAO = "ERRO_DATA_CALIBRACAO_MAIOR_DATA_AQUISICAO";

	String ERRO_DATA_VALIDADE_MENOR_QUE_DATA_ATUAL = "ERRO_DATA_VALIDADE_MENOR_QUE_DATA_ATUAL";

	String ERRO_DATA_AQUISICAO_MAIOR_QUE_DATA_ATUAL = "ERRO_DATA_AQUISICAO_MAIOR_QUE_DATA_ATUAL";

	String ERRO_DATA_AQUISICAO_MAIOR_QUE_DATA_MAX_INSTALACAO = "ERRO_DATA_AQUISICAO_MAIOR_QUE_DATA_MAX_INSTALACAO";

	String ERRO_DATA_MAX_INSTALACAO_MENOR_IGUAL_QUE_DATA_ATUAL = "ERRO_DATA_MAX_INSTALACAO_MENOR_IGUAL_QUE_DATA_ATUAL";

	String ERRO_DATA_MAX_INSTALACAO_MAIOR_QUE_DATA_VALIDADE = "ERRO_DATA_MAX_INSTALACAO_MAIOR_QUE_DATA_VALIDADE";

	String ERRO_ANO_FABRICACAO_SUPERIOR_ANO_ATUAL = "ERRO_ANO_FABRICACAO_SUPERIOR_ANO_ATUAL";

	String ERRO_NAO_EXISTE_MOVIMENTACAO = "ERRO_NAO_EXISTE_MOVIMENTACAO";

	String ERRO_NAO_EXISTE_INSTALACAO_MEDIDOR = "ERRO_NAO_EXISTE_INSTALACAO_MEDIDOR";

	String ERRO_CAPACIDADE_MINIMA_MAIOR_QUE_CAPACIDADE_MAXIMA = "ERRO_CAPACIDADE_MINIMA_MAIOR_QUE_CAPACIDADE_MAXIMA";

	String ERRO_NEGOCIO_NUMERO_TOMBAMENTO_JA_EXISTENTE = "ERRO_NEGOCIO_NUMERO_TOMBAMENTO_JA_EXISTENTE";

	String ERRO_PRESSAO_UNIDADE_NAO_INFORMADAS_CONJUNTAMENTE = "ERRO_PRESSAO_UNIDADE_NAO_INFORMADAS_CONJUNTAMENTE";

	String ERRO_NAO_EXISTE_HISTORICO_OPERACAO_MEDIDOR = "ERRO_NAO_EXISTE_HISTORICO_OPERACAO_MEDIDOR";

	String ERRO_NUMERO_DIGITO_LEITURA_INVALIDO = "ERRO_NUMERO_DIGITO_LEITURA_INVALIDO";

	String ERRO_SISTEMA_SEM_HISTORICO_OPERACAO_MEDIDOR = "ERRO_SISTEMA_SEM_HISTORICO_OPERACAO_MEDIDOR";

	String ERRO_PONTO_CONSUMO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_PONTO_CONSUMO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_IMOVEL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_IMOVEL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_OPERACAO_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_OPERACAO_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_MEDIDOR_CORRETOR_VAZAO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_MEDIDOR_CORRETOR_VAZAO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_MEDIDOR_CORRETOR_VAZAO_INVALIDO_ASSOCIACAO_MEDIDOR = "ERRO_MEDIDOR_CORRETOR_VAZAO_INVALIDO_ASSOCIACAO_MEDIDOR";

	String ERRO_PONTO_CONSUMO_POSSUI_MEDIDOR_INSTALADO_ASSOCIACAO_MEDIDOR = "ERRO_PONTO_CONSUMO_POSSUI_MEDIDOR_INSTALADO_ASSOCIACAO_MEDIDOR";

	String ERRO_IMOVEL_SITUACAO_SEM_REDE_ASSOCIACAO_MEDIDOR = "ERRO_IMOVEL_SITUACAO_SEM_REDE_ASSOCIACAO_MEDIDOR";

	String ERRO_PONTO_CONSUMO_DEVE_POSSUIR_MEDIDOR_INSTALADO_ASSOCIACAO_MEDIDOR = "ERRO_PONTO_CONSUMO_DEVE_POSSUIR_"
					+ "MEDIDOR_INSTALADO_ASSOCIACAO_MEDIDOR";

	String ERRO_PONTO_CONSUMO_DEVE_POSSUIR_CORRETOR_VAZAO_ASSOCIACAO_MEDIDOR = "ERRO_PONTO_CONSUMO_DEVE_POSSUIR_"
					+ "CORRETOR_VAZAO_ASSOCIACAO_MEDIDOR";

	String ERRO_PONTO_CONSUMO_POSSUI_CORRETOR_VAZAO_INSTALADO_ASSOCIACAO_MEDIDOR = "ERRO_PONTO_CONSUMO_POSSUI_CORRETOR_"
					+ "VAZAO_INSTALADO_ASSOCIACAO_MEDIDOR";

	String ERRO_MEDIDOR_INDEPENDENTE_POSSUI_CORRETOR_VAZAO_INSTALADO_ASSOCIACAO = "ERRO_MEDIDOR_INDEPENDENTE_POSSUI_"
					+ "CORRETOR_VAZAO_INSTALADO_ASSOCIACAO";

	String ERRO_PONTO_CONSUMO_POSSUI_MEDIDOR_ATIVO_ASSOCIACAO_MEDIDOR = "ERRO_PONTO_CONSUMO_POSSUI_MEDIDOR_ATIVO_ASSOCIACAO_MEDIDOR";

	String ERRO_PONTO_CONSUMO_NAO_POSSUI_ROTA_ASSOCIACAO_MEDIDOR = "ERRO_PONTO_CONSUMO_NAO_POSSUI_ROTA_ASSOCIACAO_MEDIDOR";

	String ERRO_MEDIDOR_ATUAL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_MEDIDOR_ATUAL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_DATA_NAO_INFORMADA_ASSOCIACAO_MEDIDOR = "ERRO_DATA_NAO_INFORMADA_ASSOCIACAO_MEDIDOR";

	String ERRO_LOCAL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_LOCAL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_PROTECAO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_PROTECAO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_LEITURA_ATUAL_NAO_INFORMADA_ASSOCIACAO_MEDIDOR = "ERRO_LEITURA_ATUAL_NAO_INFORMADA_ASSOCIACAO_MEDIDOR";

	String ERRO_DIGITOS_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_DIGITOS_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_QTDA_DIGITOS_MEDIDOR_MENOR_QTDA_DIGITOS_LEITURA_ASSOCIACAO_MEDIDOR = "ERRO_QTDA_DIGITOS_MEDIDOR_MENOR_"
					+ "QTDA_DIGITOS_LEITURA_ASSOCIACAO_MEDIDOR";

	String ERRO_FUNCIONARIO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_FUNCIONARIO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_DATA_SUBSTITUICAO_CORRETOR_ANTERIOR_INSTALACAO = "ERRO_DATA_SUBSTITUICAO_CORRETOR_ANTERIOR_INSTALACAO";

	String ERRO_DATA_INFERIOR_DATA_AQUISICAO = "ERRO_DATA_INFERIOR_DATA_AQUISICAO";

	String ERRO_MEDIDOR_INDEPENDENTE_NAO_INSTALADO = "ERRO_MEDIDOR_INDEPENDENTE_NAO_INSTALADO";

	String ERRO_MEDIDOR_NAO_FAZ_PARTE_DE_COMPOSICAO = "ERRO_MEDIDOR_NAO_FAZ_PARTE_DE_COMPOSICAO";

	String ERRO_EXISTE_MEDIDOR_INSTALADO_NA_COMPOSICAO = "ERRO_EXISTE_MEDIDOR_INSTALADO_NA_COMPOSICAO";

	String ERRO_MEDIDOR_ANTERIOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_MEDIDOR_ANTERIOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_LEITURA_ANTERIOR_NAO_INFORMADA_ASSOCIACAO_MEDIDOR = "ERRO_LEITURA_ANTERIOR_NAO_INFORMADA_ASSOCIACAO_MEDIDOR";

	String ERRO_DATA_MEDIDOR_CORRETOR_VAZAO_INVALIDA_ASSOCIACAO_MEDIDOR = "ERRO_DATA_MEDIDOR_CORRETOR_VAZAO_INVALIDA_ASSOCIACAO_MEDIDOR";

	String ERRO_LEITURA_ANTERIOR_MENOR_IGUAL_ZERO_ASSOCIACAO_MEDIDOR = "ERRO_LEITURA_ANTERIOR_MENOR_IGUAL_ZERO_ASSOCIACAO_MEDIDOR";

	String ERRO_LEITURA_ANTERIOR_MENOR_ZERO_ASSOCIACAO_MEDIDOR = "ERRO_LEITURA_ANTERIOR_MENOR_ZERO_ASSOCIACAO_MEDIDOR";

	String ERRO_LEITURA_ATUAL_MENOR_IGUAL_ZERO_ASSOCIACAO_MEDIDOR = "ERRO_LEITURA_ATUAL_MENOR_IGUAL_ZERO_ASSOCIACAO_MEDIDOR";

	String ERRO_LEITURA_ATUAL_MENOR_ZERO_ASSOCIACAO_MEDIDOR = "ERRO_LEITURA_ATUAL_MENOR_ZERO_ASSOCIACAO_MEDIDOR";

	String ERRO_MEDIDOR_NAO_DISPONIVEL_PARA_INSTALACAO_ASSOCIACAO_MEDIDOR = "ERRO_MEDIDOR_NAO_DISPONIVEL_PARA_INSTALACAO_ASSOCIACAO_MEDIDOR";

	String ERRO_DATA_MENOR_DATA_INSTALACAO_MEDIDOR_ASSOCIACAO_MEDIDOR = "ERRO_DATA_MENOR_DATA_INSTALACAO_MEDIDOR_ASSOCIACAO_MEDIDOR";

	String ERRO_MEDIDOR_INDEPENDENTE_DESATIVADO = "ERRO_MEDIDOR_INDEPENDENTE_DESATIVADO";

	String ERRO_DATA_MENOR_DATA_ATIVACAO_MEDIDOR_ASSOCIACAO_MEDIDOR = "ERRO_DATA_MENOR_DATA_ATIVACAO_MEDIDOR_ASSOCIACAO_MEDIDOR";

	String ERRO_DATA_MENOR_DATA_BLOQUEIO_MEDIDOR_ASSOCIACAO_MEDIDOR = "ERRO_DATA_MENOR_DATA_BLOQUEIO_MEDIDOR_ASSOCIACAO_MEDIDOR";

	String ERRO_DATA_SUBTITUICAO_MEDIDOR_ANTERIOR_DATA_FATURAMENTO_ASSOCIACAO_MEDIDOR = "ERRO_DATA_SUBTITUICAO_MEDIDOR_ANTERIOR_"
					+ "DATA_FATURAMENTO_ASSOCIACAO_MEDIDOR";

	String ERRO_CORRETOR_VAZAO_ATUAL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_CORRETOR_VAZAO_ATUAL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_CORRETOR_VAZAO_ANTERIOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR = "ERRO_CORRETOR_VAZAO_ANTERIOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR";

	String ERRO_HORA_NAO_INFORMADA_ASSOCIACAO_MEDIDOR = "ERRO_HORA_NAO_INFORMADA_ASSOCIACAO_MEDIDOR";

	String ERRO_HORA_INVALIDA_ASSOCIACAO_MEDIDOR = "ERRO_HORA_INVALIDA_ASSOCIACAO_MEDIDOR";

	String ERRO_DATA_MENOR_DATA_INSTALACAO_CORRETOR_VAZAO_ASSOCIACAO_MEDIDOR = "ERRO_DATA_MENOR_DATA_INSTALACAO_CORRETOR_"
					+ "VAZAO_ASSOCIACAO_MEDIDOR";

	String ERRO_CORRETOR_VAZAO_NAO_DISPONIVEL_PARA_INSTALACAO_ASSOCIACAO_MEDIDOR = "ERRO_CORRETOR_VAZAO_NAO_DISPONIVEL_PARA_"
					+ "INSTALACAO_ASSOCIACAO_MEDIDOR";

	String ERRO_ANO_DATA_ULTIMA_CALIBRACAO_MAIOR_IGUAL_ANO_FABRICACAO_MEDIDOR = "ERRO_ANO_DATA_ULTIMA_CALIBRACAO_MAIOR_"
					+ "IGUAL_ANO_FABRICACAO_MEDIDOR";

	String ERRO_MEDIDOR_INDEPENDENTE_FAZ_PARTE_DE_COMPOSICAO = "ERRO_MEDIDOR_INDEPENDENTE_FAZ_PARTE_DE_COMPOSICAO";

	String ERRO_MEDIDOR_INSTALADO_EM_PONTO_CONSUMO = "ERRO_MEDIDOR_INSTALADO_EM_PONTO_CONSUMO";

	static final Integer CODIGO_TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR = 1;

	static final Integer CODIGO_TIPO_ASSOCIACAO_PONTO_CONSUMO_CORRETOR_VAZAO = 2;

	static final Integer CODIGO_TIPO_ASSOCIACAO_MEDIDOR = 1;

	static final Integer CODIGO_TIPO_ASSOCIACAO_CORRETOR_VAZAO = 2;

	String NOME_TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR = "Medidor";

	String MEDIDOR_LOCAL_ARMAZENAGEM = "Local de Armazenagem";

	String ERRO_NEGOCIO_REMOVER_MARCA_MEDIDOR = "ERRO_NEGOCIO_REMOVER_MARCA_MEDIDOR";

	String ERRO_NEGOCIO_REMOVER_MARCA_CORRETOR = "ERRO_NEGOCIO_REMOVER_MARCA_CORRETOR";

	String ERRO_NEGOCIO_REMOVER_MODELO_MEDIDOR = "ERRO_NEGOCIO_REMOVER_MODELO_MEDIDOR";

	String ERRO_NEGOCIO_REMOVER_MODELO_CORRETOR = "ERRO_NEGOCIO_REMOVER_MODELO_CORRETOR";

	String ERRO_DATA_INSTALACAO_INFERIOR_DATA_ULTIMA_RETIRADA = "ERRO_DATA_INSTALACAO_INFERIOR_DATA_ULTIMA_RETIRADA";

	String ERRO_NEGOCIO_COMPOSICAO_VIRTUAL_CONTEM_MEDIDOR = "ERRO_NEGOCIO_COMPOSICAO_VIRTUAL_CONTEM_MEDIDOR";

	String ERRO_NEGOCIO_MEDIDOR_PONTO_CONSUMO_MESMO_ENDERECO_REMOTO = "ERRO_NEGOCIO_MEDIDOR_PONTO_CONSUMO_MESMO_ENDERECO_REMOTO";

	String ERRO_MEDIDOR_NORMAL_ATIVO_NAO_PODE_SER_ALTERADO_PARA_ATIVO = "ERRO_MEDIDOR_NORMAL_ATIVO_NAO_PODE_SER_ALTERADO_PARA_ATIVO";

	String ERRO_MEDIDOR_VIRTUAL_ATIVO_NAO_PODE_SER_ALTERADO_PARA_NORMAL = "ERRO_MEDIDOR_VIRTUAL_ATIVO_NAO_PODE_SER_ALTERADO_PARA_NORMAL";

	String ERRO_MEDIDOR_INDEPENDENTE_INSTALADO_ATIVO_NAO_PODE_SER_ALTERADO_PARA_VIRTUAL_NORMAL = "ERRO_MEDIDOR_INDEPENDENTE_INSTALADO_"
					+ "ATIVO_NAO_PODE_SER_ALTERADO_PARA_VIRTUAL_NORMAL";

	String NOME_TIPO_ASSOCIACAO_PONTO_CONSUMO_CORRETOR_VAZAO = "Corretor de Vazão";
	
	String ERRO_DATA_INSTALACAO_VAZAO_CORRETOR_ANTERIOR_DATA_INSTALACAO_MEDIDOR =
			"ERRO_DATA_INSTALACAO_VAZAO_CORRETOR_ANTERIOR_DATA_INSTALACAO_MEDIDOR";

	/**
	 * Metódo instancia um tipo de medidor.
	 * @return TipoMedidor
	 */
	TipoMedidor criarTipoMedidor();
	
	/**
	 * Metódo instancia um marca medidor
	 * @return Marcamedidor
	 */
	MarcaMedidor criarMarcaMedidor();
	
	/**
	 * Método responsável por listar os tipos dos
	 * medidores existentes no sistema.
	 * 
	 * @return coleção de tipos dos medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TipoMedidor> listarTipoMedidor() throws NegocioException;

	/**
	 * Método responsável por listar as marcas dos
	 * medidores existentes no sistema.
	 * 
	 * @return coleção as marcas dos medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<MarcaMedidor> listarMarcaMedidor() throws NegocioException;

	/**
	 * Método responsável por listar os modelos
	 * dos medidores existentes no sistema.
	 * 
	 * @return coleção de modelo de medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ModeloMedidor> listarModeloMedidor() throws NegocioException;

	/**
	 * Método responsável por listar FatorK.
	 * 
	 * @return coleção de FatorK.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<FatorK> listarFatorK() throws NegocioException;

	/**
	 * Método responsável por listar as situações
	 * dos medidores existentes no sistema.
	 * 
	 * @return coleção as situações dos medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<SituacaoMedidor> listarSituacaoMedidor() throws NegocioException;

	/**
	 * Método responsável por listar os diâmetros
	 * dos medidores existentes no sistema.
	 * 
	 * @return coleção os diâmetros dos medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<DiametroMedidor> listarDiametroMedidor() throws NegocioException;

	/**
	 * Método responsável por listar as faixas de
	 * temperatura de trabalho existentes no
	 * sistema.
	 * 
	 * @return coleção contendo as faixas de
	 *         temperatura de trabalho
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<FaixaTemperaturaTrabalho> listarFaixaTemperaturaTrabalho() throws NegocioException;

	/**
	 * Método responsável por listar as
	 * capacidades dos medidores existentes no
	 * sistema.
	 * 
	 * @return coleção de capacidades dos
	 *         medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<CapacidadeMedidor> listarCapacidadeMedidor() throws NegocioException;

	/**
	 * Método responsável por listar os corretores
	 * de vazão existentes no sistema.
	 * 
	 * @return coleção contendo os corretores de
	 *         vazão existentes no sistema.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<VazaoCorretor> listarVazaoCorretor() throws NegocioException;

	/**
	 * Método responsável por listar os locais de
	 * armazenagem existentes no sistema.
	 * 
	 * @return coleção contendo os locais de
	 *         armazenagem existentes no sistema.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<MedidorLocalArmazenagem> listarLocalArmazenagem() throws NegocioException;

	/**
	 * Método responsável por obter a marca do
	 * medidor.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Uma marca.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	MarcaMedidor obterMarcaMedidor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o modelo do
	 * medidor.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Um Modelo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	ModeloMedidor obterModeloMedidor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o fator K.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Um Fator K.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	FatorK obterFatorK(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter um medidor.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Um medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Medidor obterMedidor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter um medidor.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return Um medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Medidor obterMedidor(long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * Método responsável por obter uma Faixa
	 * Temperatura Trabalho.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Uma FaixaTemperaturaTrabalho
	 * @throws NegocioException
	 *             the negocio exception
	 */
	FaixaTemperaturaTrabalho obterFaixaTemperaturaTrabalho(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter um corretor de
	 * vazão.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Um corretor de vazão.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	VazaoCorretor obterVazaoCorretor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter um Medidor
	 * Local Armazenagem.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Um Medidor Local Armazenagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	MedidorLocalArmazenagem obterMedidorLocalArmazenagem(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o tipo do
	 * medidor.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Um Tipo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoMedidor obterTipoMedidor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter a situação do
	 * medidor.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return A situação do medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	SituacaoMedidor obterSituacaoMedidor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o diâmetro do
	 * medidor.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return O diâmetro do medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	DiametroMedidor obterDiametroMedidor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter a capacidade
	 * do medidor.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return A capacidade do medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	CapacidadeMedidor obterCapacidadeMedidor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * medidores disponíveis do sistema.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção de medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Medidor> consultarMedidor(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * medidores.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primárias dos
	 *            medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException {@link GGASException}
	 */
	void validarRemoverMedidores(Long[] chavesPrimarias) throws GGASException;

	/**
	 * Método responsável por remover os medidores
	 * selecionados.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primárias dos
	 *            medidores selecionados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerMedidores(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por validar o número de
	 * série do medidor.
	 * 
	 * @param numeroSerie
	 *            Número de Série do medidor.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarNumeroSerie(String numeroSerie) throws NegocioException;

	/**
	 * Obtêm um ou mais medidores através do
	 * número de série.
	 * 
	 * @param numeroSerie
	 *            Número de Série do medidor.
	 * @return Um medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	List<Medidor> obterMedidorPorNumeroSerie(String numeroSerie) throws NegocioException;

	/**
	 * Método responsável por consultar instalação
	 * medidor.
	 * 
	 * @param chaveMedidor
	 *            A chave Primária do medidor.
	 * @param chavePontoConsumo
	 *            A chave Primária do ponto de
	 *            consumo.
	 * @return Uma coleção de instalação medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<InstalacaoMedidor> consultarInstalacaoMedidor(Long chaveMedidor, Long chavePontoConsumo) throws NegocioException;

	/**
	 * Método responsável por consultar o
	 * histórico de operação do medidor.
	 * 
	 * @param chaveMedidor
	 *            A chave Primária do medidor.
	 * @param chavePontoConsumo
	 *            chave Primária do ponto de
	 *            consumo.
	 * @return Uma coleção de histórico de
	 *         operação de medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoOperacaoMedidor> consultarHistoricoOperacaoMedidor(Long chaveMedidor, Long chavePontoConsumo)
					throws NegocioException;

	/**
	 * Método responsável por consultar a
	 * movimentação do medidor pelo filtro
	 * informado.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção de movimentação de
	 *         medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<MovimentacaoMedidor> consultarMovimentacaoMedidor(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se existe
	 * histórico de movimentação para o medidor
	 * selecionado.
	 * 
	 * @param movimentacoes
	 *            Coleção de movimentações do
	 *            histórico de medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void verificarListaMovimentacao(Collection<MovimentacaoMedidor> movimentacoes) throws NegocioException;

	/**
	 * Método responsável por verificar se existe
	 * histórico de instalação de medidor.
	 * 
	 * @param listaInstalacaoMedidor
	 *            Coleção de Instalações de
	 *            Medidores
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void verificarListaHistoricoInstalacaoMedidor(Collection<InstalacaoMedidor> listaInstalacaoMedidor) throws NegocioException;

	/**
	 * Método responsável por verificar se existe
	 * histórico de operação de medidor.
	 * 
	 * @param listaHistoricoOperacaoMedidor
	 *            Coleção de histórico de operação
	 *            de medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void verificarListaHistoricoOperacaoMedidor(Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor) throws NegocioException;

	/**
	 * Método responsável por consultar um
	 * histórico de operação de medidor
	 * pela chave do medidor e da operação.
	 * 
	 * @param chavePrimariaMedidor
	 *            A chave primária do medidor.
	 * @param chavePrimariaOperacao
	 *            A chave primária da operação de
	 *            medidor.
	 * @param chavePontoConsumo
	 * 				the chavePontoConsumo
	 * @param dataInicio
	 * 				the dataInicio
	 * @param dataFim
	 * 				the dataFim
	 * @return Um histórico de operação de
	 *         medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	HistoricoOperacaoMedidor obterHistoricoOperacaoMedidorPorChaveMedidorOperacao(long chavePrimariaMedidor, long chavePrimariaOperacao,
					long chavePontoConsumo, Date dataInicio, Date dataFim) throws NegocioException;

	/**
	 * Método responsável por listar as situações
	 * dos medidores disponíveis para cadastro
	 * existentes no sistema.
	 * 
	 * @return coleção as situações dos medidores.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<SituacaoMedidor> listarSituacaoMedidorCadastro() throws NegocioException;

	/**
	 * Método responsável por consultar históricos
	 * de operação de medidor de
	 * em um período.
	 * 
	 * @param chavePrimariaMedidor
	 *            A chave primária do medidor.
	 * @param chavePrimariaOperacao
	 *            A chave primária da operação de
	 *            medidor.
	 * @param dataInicialPeriodo
	 *            A data inicial do período.
	 * @param dataFinalPeriodo
	 *            A data final do período.
	 * @return Coleção de históricos de operações
	 *         de medidor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoOperacaoMedidor> consultarHistoricoOperacaoMedidor(long chavePrimariaMedidor, long chavePrimariaOperacao,
					Date dataInicialPeriodo, Date dataFinalPeriodo) throws NegocioException;

	/**
	 * Método responsável por verificar se é
	 * permitida a alteração da situação
	 * do medidor.
	 * 
	 * @param chavePrimariaMedidor
	 *            A chave primária do medidor.
	 * @return true caso permita.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean permiteAlteracaoSituacaoMedidor(long chavePrimariaMedidor) throws NegocioException;

	/**
	 * Método responsável por verificar se é
	 * permitida a alteração do número de série
	 * do medidor.
	 * 
	 * @param chavePrimariaMedidor
	 *            A chave primária do medidor.
	 * @return true caso permita.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean permiteAlteracaoNumeroSerieMedidor(long chavePrimariaMedidor) throws NegocioException;

	/**
	 * Método responsável por obter o histórico de operação de medidor.
	 * 
	 * @param chavePrimariaMedidor  A chave primária do medidor.
	 * @param chavePrimariaOperacao A chave primária da operação de medidor.
	 * @return Coleção de históricos de operações de medidor.
	 */
	List<HistoricoOperacaoMedidor> obterHistoricoOperacaoMedidor(long chavePrimariaMedidor, long chavePrimariaOperacao);

	/**
	 * Método responsável por verificar a
	 * existência de historico de operacao dos
	 * medidores no sistema.
	 * 
	 * @param filtro
	 *            com os dados do medidor(es)
	 * @return uma lista de medidores com
	 *         historico de medicao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Medidor> pesquisarMedidorComHistoricoOperacao(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Obter instalacao medidor por corretor vazao.
	 * 
	 * @param chavePrimariaCorretorVazao
	 *            the chave primaria corretor vazao
	 * @return the instalacao medidor
	 * @throws NegocioException
	 *             the negocio exception
	 */
	InstalacaoMedidor obterInstalacaoMedidorPorCorretorVazao(long chavePrimariaCorretorVazao) throws NegocioException;

	/**
	 * Obter instalacao medidor por chave.
	 * 
	 * @param chavePrimariaInstalacaoMedidor
	 *            the chave primaria instalacao medidor
	 * @return the instalacao medidor
	 * @throws NegocioException
	 *             the negocio exception
	 */
	InstalacaoMedidor obterInstalacaoMedidorPorChave(long chavePrimariaInstalacaoMedidor) throws NegocioException;

	/**
	 * Método responsável por listar as operações
	 * de um medidor.
	 * 
	 * @return lista de OperacaoMedidor
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<OperacaoMedidor> listarOperacaoMedidor() throws NegocioException;

	/**
	 * Método responsável por validar os dados
	 * necessarios para realizar a associação de
	 * um medidor.
	 * 
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosAssociacaoMedidor(Map<String, Object> dados) throws NegocioException;

	/**
	 * Método responsável por validar os dados
	 * necessarios para realizar a associação de
	 * um medidor em lote.
	 *
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosAssociacaoMedidorLote(Map<String, Object> dados) throws NegocioException;

	/**
	 * Método responsável por validar os dados
	 * necessarios para realizar a associação de
	 * um corretor de vazao.
	 * 
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosAssociacaoCorretorVazao(Map<String, Object> dados) throws NegocioException;

	/**
	 * Método responsável por validar os dados
	 * necessarios para realizar a associação de
	 * um corretor de vazao ao medidor Independente.
	 * 
	 * @param dados
	 *            the dados
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void validarDadosAssociacaoCorretorVazaoMedidorIndependente(Map<String, Object> dados) throws GGASException;

	/**
	 * Método responsável por obter um
	 * OperacaoMedidor.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return OperacaoMedidor
	 * @throws NegocioException
	 *             the negocio exception
	 */
	OperacaoMedidor obterOperacaoMedidor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por retornar os tipos de
	 * associações.
	 * 
	 * @return Tipos de associações
	 */
	Map<Integer, String> obterTiposAssociacoes();

	/**
	 * Método responsável por retornar se o codigo
	 * da associação é do tipo PontoConsumo /
	 * Medidor.
	 * 
	 * @param codigoAssociacao
	 *            the codigo associacao
	 * @return true, if is associacao ponto consumo medidor
	 */
	boolean isAssociacaoPontoConsumoMedidor(Integer codigoAssociacao);

	/**
	 * Método responsável por retornar se o codigo
	 * da associação é do tipo PontoConsumo /
	 * Corretor de Vazão.
	 * 
	 * @param codigoAssociacao
	 *            the codigo associacao
	 * @return true, if is associacao ponto consumo corretor vazao
	 */
	boolean isAssociacaoPontoConsumoCorretorVazao(Integer codigoAssociacao);

	/**
	 * Método reponsável por retornar uma lista de
	 * medidorLocalInstalacao.
	 * 
	 * @return lista de medidorLocalInstalacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<MedidorLocalInstalacao> listarMedidorLocalInstalacao() throws NegocioException;

	/**
	 * Método reponsável por retornar uma lista de
	 * medidorProtecao.
	 * 
	 * @return lista de medidorProtecao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<MedidorProtecao> listarMedidorProtecao() throws NegocioException;

	/**
	 * Método reponsável por retornar uma lista de
	 * MotivoOperacaoMedidor.
	 * 
	 * @return lista de MotivoOperacaoMedidor
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public List<MotivoOperacaoMedidor> listarMotivoOperacaoMedidor() throws NegocioException;

	/**
	 * Método reponsável por retornar um
	 * InstalacaoMedidor.
	 * 
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @return the instalacao medidor
	 * @throws NegocioException
	 *             the negocio exception
	 */
	InstalacaoMedidor obterInstalacaoMedidorPontoConsumo(long chavePontoConsumo) throws NegocioException;

	/**
	 * Método responsável por retornar um ou mais
	 * VazaoCorretor.
	 * 
	 * @param numeroSerie
	 *            the numero serie
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<VazaoCorretor> obterVazaoCorretorPorNumeroSerie(String numeroSerie) throws NegocioException;

	/**
	 * Método responsável por listar as operações
	 * permitidas por tipo de associação entre
	 * ponto de consumo e medidor / vazaoCorretor.
	 * 
	 * @param codigoAssociacao {@link Integer}
	 * @param status {@link String}
	 * @param medidor {@link String}
	 * @param corretorVazao {@link String}
	 * @return List<OperacaoMedidor> {@link List}
	 * @throws GGASException {@link GGASException}
	 */
	List<OperacaoMedidor> obterOperacoesMedidorPorTipoAssociacao(Integer codigoAssociacao, String status, String medidor,
					String corretorVazao) throws GGASException;

	/**
	 * Método responsável por listar as operações
	 * permitidas por tipo de associação entre
	 * imóvel e medidor / vazaoCorretor.
	 *
	 * @param tipoAssociacao the tipo associacao
	 * @param idImovel a chave primária do imóvel
	 * @return the list
	 * @throws GGASException the GGAS exception
	 */
	List<OperacaoMedidor> obterOperacoesMedidorPorImovelCondonimioIndividual(Integer tipoAssociacao, Long idImovel) throws GGASException;

	/**
	 * Método responsável por inserir um medidor.
	 * 
	 * @param medidor
	 *            the medidor
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 * 				the GGASException
	 */
	long inserirMedidor(Medidor medidor) throws GGASException;

	/**
	 * Inserir medidores em lote.
	 * 
	 * @param medidor
	 *            the medidor
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	long inserirMedidoresEmLote(Medidor medidor) throws NegocioException;

	/**
	 * Método responsável por atualizar os dados
	 * de um medidor.
	 * 
	 * @param medidor
	 *            the medidor
	 * @param localArmazenagemAnterior
	 *            the local armazenagem anterior
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 * 				the GGASException
	 */
	void atualizarMedidor(Medidor medidor, MedidorLocalArmazenagem localArmazenagemAnterior) throws GGASException;

	/**
	 * Método responsável por listar as operações
	 * permitidas por tipo de associação entre
	 * ponto de consumo e medidor / vazaoCorretor.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Medidor> consultarMedidorDisponivel(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por listar os motivos
	 * das movimentação do medidor.
	 * 
	 * @return Collection<
	 *         MotivoMovimentacaoMedidor
	 *         >
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Collection<MotivoMovimentacaoMedidor> listarMotivoMovimentacaoMedidor() throws NegocioException;

	/**
	 * Método responsável por obter as informaçoes
	 * do medidor instalado no ponto de consumo.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Map<String, Object> obterInformacoesMedidor(Long idPontoConsumo) throws NegocioException;

	/**
	 * Validar dados associacao ponto consumo corretor vazao.
	 * 
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDadosAssociacaoPontoConsumoCorretorVazao(Map<String, Object> dados) throws NegocioException;

	/**
	 * Verificar necessidade associacao ponto consumo corretor vazao.
	 * 
	 * @param dados {@link Map}
	 * @return true, if successful {@link boolean}
	 * @throws NegocioException {@link NegocioException}
	 */
	boolean verificarNecessidadeAssociacaoPontoConsumoCorretorVazao(Map<String, Object> dados) throws NegocioException;
	
	/**
	 * Metódo para validar dados associacao ponto consumo medidor.
	 * 
	 * @param dados {@link Map}
	 * @throws GGASException {@link GGASException}
	 */
	void validarDadosAssociacaoPontoConsumoMedidor(Map<String, Object> dados) throws GGASException;

	/**
	 * Inserir dados associacao ponto consumo medidor.
	 * 
	 * @param dados
	 *            the dados
	 * @throws GGASException
	 *             the GGASException
	 */
	public PontoConsumo inserirDadosAssociacaoPontoConsumoMedidor(Map<String, Object> dados) throws GGASException;

	/**
	 * Inserir dados associacao ponto consumo corretor vazao.
	 * 
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	public void inserirDadosAssociacaoPontoConsumoCorretorVazao(Map<String, Object> dados) throws NegocioException, ConcorrenciaException;

	/**
	 * Insere dados da associação de medidor independente ao corretor de vazão.
	 * 
	 * @param dados
	 *            the dados
	 * @throws GGASException
	 *             the GGASException
	 */
	public void inserirDadosAssociacaoCorretorVazaoMedidorIndependente(Map<String, Object> dados) throws GGASException;

	/**
	 * Validar ponto consumo possui medidor.
	 * 
	 * @param dados
	 *            the dados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void validarPontoConsumoPossuiMedidor(Map<String, Object> dados) throws NegocioException;

	/**
	 * Validar medidor existente.
	 * 
	 * @param chavePrimariaMedidor
	 *            the chave primaria medidor
	 * @param numeroSerie
	 *            the numero serie
	 * @param marcaMedidor
	 *            the marca medidor
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void validarMedidorExistente(Long chavePrimariaMedidor, String numeroSerie, Long marcaMedidor) throws NegocioException;

	/**
	 * Método responsável por listar todas as qtde de digitos
	 * possiveis para um corretor de vazao.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Integer> listarNumeroDigitosConsumo() throws NegocioException;

	/**
	 * Método responsável por listar todos os tipos
	 * de dados de endereço remoto para o Sistema Supervisório.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarTipoDadosEnderecoRemoto() throws NegocioException;

	/**
	 * Método responsável por listar todos os tipos
	 * de integração para o Sistema Supervisório.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<EntidadeConteudo> listarTipoIntegracao() throws NegocioException;

	/**
	 * Verificar registros associados.
	 * 
	 * @param idMedidor
	 *            the id medidor
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean verificarRegistrosAssociados(Long idMedidor) throws NegocioException;

	/**
	 * Validar medidor existente por numero serie e marca.
	 * 
	 * @param chavePrimariaMedidor
	 *            the chave primaria medidor
	 * @param numeroSerie
	 *            the numero serie
	 * @param marcaMedidor
	 *            the marca medidor
	 * @return true, if successful
	 */
	boolean validarMedidorExistentePorNumeroSerieEMarca(Long chavePrimariaMedidor, String numeroSerie, Long marcaMedidor);

	/**
	 * Consultar operacao medidor.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the string
	 */
	String consultarOperacaoMedidor(Long chavePrimaria);

	/**
	 * Consultar marca medidor em uso.
	 * 
	 * @param marcaMedidor
	 *            the marca medidor
	 * @return the long
	 */
	Long consultarMarcaMedidorEmUso(Long marcaMedidor);

	/**
	 * Consultar modelo medidor em uso.
	 * 
	 * @param modeloMedidor
	 *            the modelo medidor
	 * @return the long
	 */
	Long consultarModeloMedidorEmUso(Long modeloMedidor);

	/**
	 * Consultar data instalacao ativacao retirada anterior data leitura.
	 * @param dataLeitura data da leitura
	 * @param instalacaoMedidor instalação do medidor
	 * @return data de instalação de ativação
	 * @throws NegocioException lançada caso ocorra falha na operação
	 */
	List<Date[]> consultarDataInstalacaoAtivacaoRetiradaAnteriorDataLeitura(Date dataLeitura, Long instalacaoMedidor) throws NegocioException;

	/**
	 * Verificar status ponto consumo.
	 * 
	 * @param dataRealizacaoLeitura
	 *            the data realizacao leitura
	 * @param pontoConsumoAux
	 *            the ponto consumo aux
	 * @return the map
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Map<String, Object> verificarStatusPontoConsumo(Date dataRealizacaoLeitura, PontoConsumo pontoConsumoAux) throws NegocioException;

	/** 
	 * Consultar data instalação retirada anterior da data de leitura.
	 * 
	 * @param dataLeitura {@link Date}
	 * @param pontoConsumo {@link Long}
	 * @param medidor {@link Long}
	 * @return timeStamp {@link Timestamp}
	 * @throws NegocioException {@link NegocioException}
	 */
	Timestamp consultarDataInstalacaoRetiradaAnteriorDataLeitura(Date dataLeitura, Long pontoConsumo, Long medidor) throws NegocioException;

	/**
	 * Obter historico operacao medidor por periodo.
	 * 
	 * @param chavePrimariaOperacao
	 *            the chave primaria operacao
	 * @param dataInicio
	 *            the data inicio
	 * @param dataFim
	 *            the data fim
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoOperacaoMedidor> obterHistoricoOperacaoMedidorPorPeriodo(Long chavePrimariaOperacao, Date dataInicio, Date dataFim)
					throws NegocioException;

	/**
	 * Obter vazao corretor.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the vazao corretor
	 * @throws NegocioException
	 *             the negocio exception
	 */
	VazaoCorretor obterVazaoCorretor(long chavePrimaria, String... propriedadesLazy) throws NegocioException;

	/**
	 * utilizar obterHistoricoOperacaoMedidor(
	 * FiltroHistoricoOperacaoMedidor filtro).
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @param dataAutorizacao
	 *            the data autorizacao
	 * @param idOperacaoMedidor
	 *            the id operacao medidor
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 * @deprecated utilizar obterHistoricoOperacaoMedidor(
	 *             FiltroHistoricoOperacaoMedidor filtro)
	 */
	@Deprecated
	Collection<HistoricoOperacaoMedidor> obterHistoricoOperacaoMedidor(Long idPontoConsumo, Date dataAutorizacao, Long idOperacaoMedidor)
					throws NegocioException;

	/**
	 * Obtem a lista de HistoricoOperacaoMedidor que atenda as restricoes e ordenacoes
	 * do filtro .
	 * 
	 * @param filtro
	 *            filtro com as restricoes e ordenacao
	 * @return lista que que atenda ao filtro
	 */
	Collection<HistoricoOperacaoMedidor> obterHistoricoOperacaoMedidor(FiltroHistoricoOperacaoMedidor filtro);

	/**
	 * Obtem o HistoricoOperacaoMedidor
	 * de bloqueio mais recente do ponto de consumo passado como parametro.
	 * 
	 * @param idPontoConsumo
	 *            codigo do ponto de consumo a obter o
	 *            ultimo historico de bloqueio
	 * @return o historico do bloqueio mais recente
	 */
	HistoricoOperacaoMedidor obterUltimoBloqueio(Long idPontoConsumo);

	/**
	 * Recupera Medidor Virtual por Medidor Independente.
	 * 
	 * @param medidorIndependente {@link Medidor}
	 * @return Collection
	 * @throws GGASException {@link GGASException}
	 * 
	 */
	Collection<Medidor> consultarMedidorVirtualPorMedidorIndependente(Medidor medidorIndependente) throws GGASException;

	/**
	 * Recupera medidor Independente de acordo com parametros passado como filtro.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<Medidor> consultarMedidorIndependente(Map<String, Object> filtro) throws GGASException;

	/**
	 * Recupera lista de operações para determinada situação do medidor independente.
	 * 
	 * @param codigoAssociacao
	 *            the codigo associacao
	 * @param status
	 *            the status
	 * @param medidor
	 *            the medidor
	 * @param corretorVazao
	 *            the corretor vazao
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	List<OperacaoMedidor> obterOperacoesMedidorIndependentePorTipoAssociacao(Integer codigoAssociacao, String status, String medidor,
					String corretorVazao) throws GGASException;

	/**
	 * Insere Dados da associação do medidor independente.
	 * 
	 * @param dados
	 *            the dados
	 * @throws GGASException
	 *             the GGASException
	 */
	void inserirDadosAssociacaoMedidorIndependente(Map<String, Object> dados) throws GGASException;

	/**
	 * Recupera lista de medidores independentes que fazem parte do medidor virtual passado por parametro.
	 * 
	 * @param medidorVirtual {@link Medidor}}
	 * @return Collection<Medidor> {@link Collection}
	 * @throws NegocioException {@link NegocioException}}
	 */
	Collection<Medidor> listaMedidoresQueCompoemMedidorVirtual(Medidor medidorVirtual) throws NegocioException;

	/**
	 * Verifica se o endereço remoto já existe em outro medidor ou ponto de consumo.
	 * 
	 * @param enderecoRemoto {@link String}
	 * @param chaveMedidor {@link Long}
	 * @param chavePontoConsumo {@link Long}
	 * @throws NegocioException {@link NegocioException}
	 */
	void validarEnderecoRemoto(String enderecoRemoto, Long chaveMedidor, Long chavePontoConsumo) throws NegocioException;

	/**
	 * Obtém atributos de Corretor de Vazão por códigos do Ponto de Consumo no Supervisório
	 * @param codigosPontoConsumoSupervisorio {@link String[]}
	 * @return Map - VazaCorretor por atributo chavePrimaria da entidade PontoConsumo
	 **/
	Map<Long, VazaoCorretor> obterCorretoresDeVazaoPorCodigosDoSupervisorio(String[] codigosPontoConsumoSupervisorio);

	/**
	 * Obter historico operacao medidor .
	 * 
	 * @param filtro the FiltroHistoricoOperacaoMedidor
	 * @return HistoricoOperacaoMedidor
	 */
	HistoricoOperacaoMedidor obterUnicoHistoricoOperacaoMedidor(FiltroHistoricoOperacaoMedidor filtro);
	
	/**
	 * Obtem o codigo e o numero de serie do medidor atraves de sua chave primaria
	 * 
	 * @param idMedidor chave primaria do medidor
	 * @return MedidorVirtualVO
	 */
	public MedidorVirtualVO obterCodigoNumeroSeriePoIdMedidor(Long idMedidor);
	
	/**
	 * Obtem uma lista de medidores que compoem um medidor virtual;
	 * 
	 * @param composicaoVirtual composicao do medidor virtual em conjunto com suas operacoes
	 * @return List<MedidorVirtualVO>
	 */
	public List<MedidorVirtualVO> obterComposicaoMedidorVirtual(String composicaoVirtual);

	/**
	 * Obtém a quantidade de dígitos do medidor da chave de leitura movimento
	 * @param chaveLeituraMovimento chave primaria da {@link br.com.ggas.medicao.leitura.LeituraMovimento}
	 * @return retorna a quantidade de dígitos do medidor, nulo caso não exista
	 */
	Integer obterDigitoMedidorPorLeituraMovimento(Long chaveLeituraMovimento);
	
	/**
	 * Lista a Pressão de Fornecimento de um determinado segmento e de um ponto de consumo 
	 * @param idPontoConsumo chave primaria da {@link br.com.ggas.cadastro.imovel.PontoConsumo}
	 * @return retorna uma lista com as pressões de um ponto de consumo referentes ao segmento 
	 */
	List<FaixaPressaoFornecimento> listarPressaoFornecimentoCampo(Long idPontoConsumo);

	/**
	 * Obtém o contratoPontoConsumo de um ponto de consumo 
	 * @param idPontoConsumo chave primaria da {@link br.com.ggas.cadastro.imovel.PontoConsumo}
	 * @return retorna o ContratoPontoConsumo solicitado
	 */
	ContratoPontoConsumo consultarContratoPontoConsumoPorPontoConsumo(Long idPontoConsumo);

	/**
	 * Obtém o contrato de ContratoPontoConsumo
	 * @param idPontoConsumo chave primaria da {@link br.com.ggas.cadastro.imovel.PontoConsumo}
	 * @return retorna o ContratoPontoConsumo e o Contrato dele 
	 */
	ContratoPontoConsumo consultarContratoPorContratoPontoConsumo(long idPontoConsumo);

	InstalacaoMedidor obterUltimoMedidorInstacaoPontoConsumo(Long chavePontoConsumo);

	/**
	 * Consultar ultimo historico operacao por operacao.
	 *
	 * @param operacaoRetirada the operacao retirada
	 * @param operacaoSubstituicao the operacao substituicao
	 * @param pontoConsumo the ponto consumo
	 * @param isMedidor the is medidor
	 * @param medidor the medidor
	 * @return the date
	 */
	Date consultarUltimoHistoricoOperacaoPorOperacao(Long operacaoRetirada, Long operacaoSubstituicao,
			Long pontoConsumo, Boolean isMedidor, Long medidor);
	/**
	 * Obtém o contrato de ContratoPontoConsumo
	 * @param filtro
	 * @return retorna o ContratoPontoConsumo e o informações do medidor
	 */
	Collection<ContratoPontoConsumoMedidorDTO> consultarContratoPontoConsumoMedidor(Map<String, Object> filtro) throws NegocioException;

	MedidorLocalInstalacao obterMedidorLocalInstalacao(long chavePrimaria) throws NegocioException;

	MedidorProtecao obterMedidorProtecao(long chavePrimaria) throws NegocioException;

	MotivoOperacaoMedidor obterMotivoOperacaoMedidor(long chavePrimaria) throws NegocioException;

}
