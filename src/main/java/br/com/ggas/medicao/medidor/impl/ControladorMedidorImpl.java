/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorMedidorImpl representa uma ControladorMedidorImpl no sistema.
 *
 * @since 17/09/2009
 *
 */

package br.com.ggas.medicao.medidor.impl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.properties.SortOrderEnum;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.joda.time.DateTime;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.funcionario.ControladorFuncionario;
import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoImovel;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte;
import br.com.ggas.cobranca.avisocorte.impl.AvisoCorteImpl;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.medicao.leitura.ControladorInstalacaoMedidor;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.leitura.MedidorProtecao;
import br.com.ggas.medicao.leitura.TipoMedicao;
import br.com.ggas.medicao.leitura.impl.InstalacaoMedidorImpl;
import br.com.ggas.medicao.medidor.CapacidadeMedidor;
import br.com.ggas.medicao.medidor.ContratoPontoConsumoMedidorDTO;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.DiametroMedidor;
import br.com.ggas.medicao.medidor.FatorK;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.MarcaMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MedidorVirtualVO;
import br.com.ggas.medicao.medidor.ModeloMedidor;
import br.com.ggas.medicao.medidor.MotivoMovimentacaoMedidor;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.MovimentacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.medidor.SituacaoMedidor;
import br.com.ggas.medicao.medidor.TipoMedidor;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.FaixaTemperaturaTrabalho;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoOperacao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.Ordenacao;
import br.com.ggas.util.OrdenacaoEspecial;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorMedidorImpl extends ControladorNegocioImpl implements ControladorMedidor {

	private static final String PARAM_CHAVE_PRIMARIA_OPERACAO = "chavePrimariaOperacao";

	private static final String PARAM_CHAVE_PRIMARIA_MEDIDOR = "chavePrimariaMedidor";

	private static final String ENDERECO_REMOTO = "enderecoRemoto";

	private static final String CODIGO_MEDIDOR_SUPERVISORIO = "codigoMedidorSupervisorio";

	private static final String NUMERO_SERIE = "numeroSerie";

	private static final String ID = "id";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String LACRE = "lacre";
	
	private static final String LACRE_DOIS = "lacreDois";
	
	private static final String LACRE_TRES = "lacreTres";

	private static final String VIRTUAL = "Virtual";

	private static final String CHAVE_MEDIDOR_ANTERIOR = "chaveMedidorAnterior";

	private static final String LEITURA_ANTERIOR = "leituraAnterior";

	private static final String CHAVE_MEDIDOR_ATUAL = "chaveMedidorAtual";

	private static final String MEDIDOR_PROTECAO = "medidorProtecao";

	private static final String LEITURA_ATUAL = "leituraAtual";

	private static final String DATA_MEDIDOR = "dataMedidor";

	private static final String CHAVE_CORRETOR_VAZAO_ANTERIOR = "chaveCorretorVazaoAnterior";

	private static final String SUBSTITUICAO = "substituicao";

	private static final String DADOS_AUDITORIA = "dadosAuditoria";

	private static final String MEDIDOR_MOTIVO_OPERACAO = "medidorMotivoOperacao";

	private static final String FUNCIONARIO_CORRETOR_VAZAO = "funcionarioCorretorVazao";

	private static final String LOCAL_INSTALACAO_CORRETOR_VAZAO = "localInstalacaoCorretorVazao";

	private static final String LEITURA_ATUAL_CORRETOR_VAZAO = "leituraAtualCorretorVazao";

	private static final String HORA_CORRETOR_VAZAO = "horaCorretorVazao";

	private static final String DATA_CORRETOR_VAZAO = "dataCorretorVazao";

	private static final String CHAVE_CORRETOR_VAZAO_ATUAL = "chaveCorretorVazaoAtual";

	private static final String CONTEM_CORRETOR_VAZAO = "contemCorretorVazao";

	private static final String SUBSTITUIR_CORRETOR_VAZAO = "substituirCorretorVazao";

	private static final String INSTALAR_CORRETOR_VAZAO = "instalarCorretorVazao";

	private static final String VAZAO_CORRETOR = "vazaoCorretor";

	private static final String MODELO_MEDIDOR = "modeloMedidor";

	private static final String ID_MEDIDOR_INDEPENDENTE = "idMedidorIndependente";

	private static final int INTEGER_OBTER_PRIMEIRO_ITEM = 1;

	private static final Logger LOG = Logger.getLogger(ControladorMedidorImpl.class);

	private static final int SIM = 2;

	private static final String FROM = " from ";

	private static final String WHERE = " where ";

	private static final String PONTO_CONSUMO_CHAVE_PRIMARIA = "pontoConsumo.chavePrimaria";

	private static final String TIPOMEDIDOR_CHAVE_PRImARIA = "tipoMedidor.chavePrimaria";

	private static final String MODELO_CHAVE_PRIMARIA = "modelo.chavePrimaria";

	private static final String MARCAMEDIDOR_CHAVE_PRIMARIA = "marcaMedidor.chavePrimaria";

	private static final String MEDIDOR_CHAVE_PRIMARIA = "medidor.chavePrimaria";

	private static final String MODO_USO_CHAVE_PRIMARIA = "modoUso.chavePrimaria";

	private static final String COLECAO_PAGINADA = "colecaoPaginada";

	private static final String SITUACAO_MEDIDOR_CHAVE_PRIMARIA = "situacaoMedidor.chavePrimaria";
	
	public static final String ID_IMOVEL = "idImovel";

	private ControladorAvisoCorte controladorAvisoCorte =
			(ControladorAvisoCorte) ServiceLocator.getInstancia().getBeanPorID(ControladorAvisoCorte.BEAN_ID_CONTROLADOR_AVISO_CORTE);

	private Fachada fachada = Fachada.getInstancia();

	/**
	 * metodo que cria EntidadeNegocio
	 * 
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#criar()
	 */

	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Medidor.BEAN_ID_MEDIDOR);
	}

	@Override
	public TipoMedidor criarTipoMedidor() {

		return (TipoMedidor) ServiceLocator.getInstancia().getBeanPorID(TipoMedidor.BEAN_ID_TIPO_MEDIDOR);
	}

	@Override
	public MarcaMedidor criarMarcaMedidor() {

		return (MarcaMedidor) ServiceLocator.getInstancia().getBeanPorID(MarcaMedidor.BEAN_ID_MARCA_MEDIDOR);
	}

	/**
	 * metodo que cria uma instancia
	 * 
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Medidor.BEAN_ID_MEDIDOR);
	}

	public Class<?> getClasseEntidadeTipoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(TipoMedidor.BEAN_ID_TIPO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeMarcaMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(MarcaMedidor.BEAN_ID_MARCA_MEDIDOR);
	}

	public Class<?> getClasseEntidadeSituacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(SituacaoMedidor.BEAN_ID_SITUACAO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeDiametroMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(DiametroMedidor.BEAN_ID_DIAMETRO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeFaixaTemperaturaTrabalho() {

		return ServiceLocator.getInstancia().getClassPorID(FaixaTemperaturaTrabalho.BEAN_ID_FAIXA_TEMPERATURA_TRABALHO);
	}

	public Class<?> getClasseEntidadeCapacidadeMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(CapacidadeMedidor.BEAN_ID_CAPACIDADE_MEDIDOR);
	}

	public Class<?> getClasseEntidadeVazaoCorretor() {

		return ServiceLocator.getInstancia().getClassPorID(VazaoCorretor.BEAN_ID_VAZAO_CORRETOR);
	}

	public Class<?> getClasseEntidadeMedidorLocalArmazenagem() {

		return ServiceLocator.getInstancia().getClassPorID(MedidorLocalArmazenagem.BEAN_ID_MEDIDOR_LOCAL_ARMAZENAGEM);
	}

	public Class<?> getClasseEntidadeMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(Medidor.BEAN_ID_MEDIDOR);
	}

	public Class<?> getClasseEntidadeMovimentacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(MovimentacaoMedidor.BEAN_ID_MOVIMENTACAO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeInstalacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(InstalacaoMedidor.BEAN_ID_INSTALACAO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeModeloMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(ModeloMedidor.BEAN_ID_MODELO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeFatorK() {

		return ServiceLocator.getInstancia().getClassPorID(FatorK.BEAN_ID_FATOR_K);
	}

	public Class<?> getClasseEntidadeOperacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(OperacaoMedidor.BEAN_ID_OPERACAO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeHistoricoOperacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoOperacaoMedidor.BEAN_ID_HISTORICO_OPERACAO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeMotivoOperacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(MotivoOperacaoMedidor.BEAN_ID_MOTIVO_OPERACAO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeMotivoMovimentacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(MotivoMovimentacaoMedidor.BEAN_ID_MOTIVO_MOVIMENTACAO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeVazaoCorretorHistoricoOperacao() {

		return ServiceLocator.getInstancia().getClassPorID(VazaoCorretorHistoricoOperacao.BEAN_ID_VAZAO_CORRETOR_HISTORICO_OPERACAO);
	}

	public Class<?> getClasseEntidadePressaoFornecimento() {

		return ServiceLocator.getInstancia().getClassPorID(FaixaPressaoFornecimento.BEAN_ID_FAIXA_PRESSAO_FORNECIMENTO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeTeste(){
		return ContratoPontoConsumo.class;
	}
	
	public Class<?> getClasseEntidadeMedidorLocalInstalacao() {

		return ServiceLocator.getInstancia().getClassPorID(MedidorLocalInstalacao.BEAN_ID_MEDIDOR_LOCAL_INSTALACAO);
	}
	
	public Class<?> getClasseEntidadeMedidorProtecao() {

		return ServiceLocator.getInstancia().getClassPorID(MedidorProtecao.BEAN_ID_MEDIDOR_PROTECAO);
	}
	
	/**
	 * Criar movimentacao medidor.
	 *
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarMovimentacaoMedidor() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(MovimentacaoMedidor.BEAN_ID_MOVIMENTACAO_MEDIDOR);

	}

	private ControladorConstanteSistema getControladorConstante() {
		return ServiceLocator.getInstancia().getControladorConstanteSistema();
	}

	private ControladorPontoConsumo getControladorPontoConsumo() {
		return ServiceLocator.getInstancia().getControladorPontoConsumo();
	}

	private ControladorImovel getControladorImovel() {
		return ServiceLocator.getInstancia().getControladorImovel();
	}

	/**
	 * metodo que Monta uma consulta.
	 *
	 * @param classe the classe
	 * @return the criteria
	 * @throws NegocioException the negocio exception
	 */
	private Criteria montarConsulta(Class<?> classe) throws NegocioException {

		Criteria criteria = createCriteria(classe);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		return criteria;
	}

	/**
	 * metodo que listarTipoMedidor
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarTipoMedidor()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<TipoMedidor> listarTipoMedidor() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeTipoMedidor());
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc(Medidor.ATRIBUTO_DESCRICAO));
		return criteria.list();
	}

	/**
	 * metodo que listarMarcaMedidor
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarMarcaMedidor()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<MarcaMedidor> listarMarcaMedidor() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeMarcaMedidor());
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarModeloMedidor()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ModeloMedidor> listarModeloMedidor() throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeModeloMedidor());
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarFatorK()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<FatorK> listarFatorK() throws NegocioException {

		return montarConsulta(getClasseEntidadeFatorK()).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarSituacaoMedidor()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<SituacaoMedidor> listarSituacaoMedidor() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeSituacaoMedidor().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarSituacaoMedidorCadastro()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<SituacaoMedidor> listarSituacaoMedidorCadastro() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeSituacaoMedidor().getSimpleName());
		hql.append(" where habilitado = true ");
		hql.append(" and usoSistema = false ");
		hql.append(" order by descricao");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarDiametroMedidor()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<DiametroMedidor> listarDiametroMedidor() throws NegocioException {

		return montarConsulta(getClasseEntidadeDiametroMedidor()).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarFaixaTemperaturaTrabalho()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<FaixaTemperaturaTrabalho> listarFaixaTemperaturaTrabalho() throws NegocioException {

		Criteria criteria = montarConsulta(getClasseEntidadeFaixaTemperaturaTrabalho());
		criteria.setFetchMode("unidade", FetchMode.JOIN);
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarCapacidadeMedidor()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<CapacidadeMedidor> listarCapacidadeMedidor() throws NegocioException {
		Collection<CapacidadeMedidor> colecaoCapacidadeMedidor = montarConsulta(getClasseEntidadeCapacidadeMedidor()).list();
		colecaoCapacidadeMedidor = colecaoCapacidadeMedidor.stream()
				.sorted(Comparator.comparingDouble(p -> Double.valueOf(p.getDescricaoAbreviada().replace(",", "."))))
				.collect(Collectors.toList());
		
		return colecaoCapacidadeMedidor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarVazaoCorretor()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<VazaoCorretor> listarVazaoCorretor() throws NegocioException {

		return montarConsulta(getClasseEntidadeVazaoCorretor()).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarLocalArmazenagem()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<MedidorLocalArmazenagem> listarLocalArmazenagem() throws NegocioException {

		return montarConsulta(getClasseEntidadeMedidorLocalArmazenagem()).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterMarcaMedidor(long)
	 */
	@Override
	public MarcaMedidor obterMarcaMedidor(long chavePrimaria) throws NegocioException {

		return (MarcaMedidor) super.obter(chavePrimaria, getClasseEntidadeMarcaMedidor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterModeloMedidor(long)
	 */
	@Override
	public ModeloMedidor obterModeloMedidor(long chavePrimaria) throws NegocioException {

		return (ModeloMedidor) super.obter(chavePrimaria, getClasseEntidadeModeloMedidor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterFatorK(long)
	 */
	@Override
	public FatorK obterFatorK(long chavePrimaria) throws NegocioException {

		return (FatorK) super.obter(chavePrimaria, getClasseEntidadeFatorK());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterMedidor(long)
	 */
	@Override
	public Medidor obterMedidor(long chavePrimaria) throws NegocioException {

		return (Medidor) super.obter(chavePrimaria, getClasseEntidadeMedidor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterMedidor(long, java.lang.String[])
	 */
	@Override
	public Medidor obterMedidor(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (Medidor) super.obter(chavePrimaria, getClasseEntidadeMedidor(), propriedadesLazy);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterFaixaTemperaturaTrabalho(long)
	 */
	@Override
	public FaixaTemperaturaTrabalho obterFaixaTemperaturaTrabalho(long chavePrimaria) throws NegocioException {

		return (FaixaTemperaturaTrabalho) super.obter(chavePrimaria, getClasseEntidadeFaixaTemperaturaTrabalho());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterVazaoCorretor(long)
	 */
	@Override
	public VazaoCorretor obterVazaoCorretor(long chavePrimaria) throws NegocioException {

		return (VazaoCorretor) super.obter(chavePrimaria, getClasseEntidadeVazaoCorretor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterMedidorLocalArmazenagem(long)
	 */
	@Override
	public MedidorLocalArmazenagem obterMedidorLocalArmazenagem(long chavePrimaria) throws NegocioException {

		return (MedidorLocalArmazenagem) super.obter(chavePrimaria, getClasseEntidadeMedidorLocalArmazenagem());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterTipoMedidor(long)
	 */
	@Override
	public TipoMedidor obterTipoMedidor(long chavePrimaria) throws NegocioException {

		return (TipoMedidor) super.obter(chavePrimaria, getClasseEntidadeTipoMedidor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterSituacaoMedidor(long)
	 */
	@Override
	public SituacaoMedidor obterSituacaoMedidor(long chavePrimaria) throws NegocioException {

		return (SituacaoMedidor) super.obter(chavePrimaria, getClasseEntidadeSituacaoMedidor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterDiametroMedidor(long)
	 */
	@Override
	public DiametroMedidor obterDiametroMedidor(long chavePrimaria) throws NegocioException {

		return (DiametroMedidor) super.obter(chavePrimaria, getClasseEntidadeDiametroMedidor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterCapacidadeMedidor(long)
	 */
	@Override
	public CapacidadeMedidor obterCapacidadeMedidor(long chavePrimaria) throws NegocioException {

		return (CapacidadeMedidor) super.obter(chavePrimaria, getClasseEntidadeCapacidadeMedidor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # obterMedidorPorNumeroSerie(java.lang.String )
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Medidor> obterMedidorPorNumeroSerie(String numeroSerie) throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.createAlias(Medidor.MODELO, Medidor.MODELO, Criteria.LEFT_JOIN);
		criteria.createAlias(Medidor.TIPO_MEDIDOR, Medidor.TIPO_MEDIDOR, Criteria.LEFT_JOIN);
		criteria.createAlias(Medidor.MARCA_MEDIDOR, Medidor.MARCA_MEDIDOR, Criteria.LEFT_JOIN);
		criteria.createAlias(Medidor.SITUACAO_MEDIDOR, Medidor.SITUACAO_MEDIDOR, Criteria.LEFT_JOIN);
		criteria.add(Restrictions.eq(Medidor.NUMERO_DE_SERIE, numeroSerie));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #consultarMedidor(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Medidor> consultarMedidor(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String numeroSerie = (String) filtro.get(Medidor.NUMERO_DE_SERIE);
			if (!StringUtils.isEmpty(numeroSerie)) {
				criteria.add(Restrictions.ilike(Medidor.NUMERO_DE_SERIE, Util.formatarTextoConsulta(numeroSerie)));
			}

			String numeroSerieParcial = (String) filtro.get(Medidor.NUMERO_SERIE_PARCIAL);
			if (!StringUtils.isEmpty(numeroSerieParcial)) {
				criteria.add(Restrictions.ilike(Medidor.NUMERO_DE_SERIE, Util.formatarTextoConsulta(numeroSerieParcial)));
			}

			Long idTipo = (Long) filtro.get(Medidor.ID_TIPO);
			if ((idTipo != null) && (idTipo > 0)) {
				criteria.createAlias(Medidor.TIPO_MEDIDOR, Medidor.TIPO_MEDIDOR);
				criteria.add(Restrictions.eq(TIPOMEDIDOR_CHAVE_PRImARIA, idTipo));
			} else {
				criteria.setFetchMode(Medidor.TIPO_MEDIDOR, FetchMode.JOIN);
			}

			Long idMarca = (Long) filtro.get(Medidor.ID_MARCA);
			if ((idMarca != null) && (idMarca > 0)) {
				criteria.createAlias(Medidor.MARCA_MEDIDOR, Medidor.MARCA_MEDIDOR);
				criteria.add(Restrictions.eq(MARCAMEDIDOR_CHAVE_PRIMARIA, idMarca));
			} else {
				criteria.setFetchMode(Medidor.MARCA_MEDIDOR, FetchMode.JOIN);
			}

			Long idModelo = (Long) filtro.get(Medidor.ID_MODELO);
			if ((idModelo != null) && (idModelo > 0)) {
				criteria.createAlias(Medidor.MODELO, Medidor.MODELO);
				criteria.add(Restrictions.eq(MODELO_CHAVE_PRIMARIA, idModelo));
			} else {
				criteria.setFetchMode(Medidor.MODELO, FetchMode.JOIN);
			}

			Long idLocalArmazenagem = (Long) filtro.get("idLocalArmazenagem");
			if ((idLocalArmazenagem != null) && (idLocalArmazenagem > 0)) {
				criteria.createAlias(Medidor.LOCAL_ARMAZENAGEM, Medidor.LOCAL_ARMAZENAGEM);
				criteria.add(Restrictions.eq("localArmazenagem.chavePrimaria", idLocalArmazenagem));
			} else {
				criteria.setFetchMode(Medidor.LOCAL_ARMAZENAGEM, FetchMode.JOIN);
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			Long modoUso = (Long) filtro.get(Medidor.MODO_USO);
			if (modoUso != null) {
				criteria.add(Restrictions.eq(MODO_USO_CHAVE_PRIMARIA, modoUso));
			}

			Long idRota = (Long) filtro.get("idRota");
			if (idRota != null) {
				criteria.createAlias("listaInstalacaoMedidor", Medidor.INSTALACAO_MEDIDOR, CriteriaSpecification.LEFT_JOIN);
				criteria.createAlias("instalacaoMedidor.pontoConsumo", HistoricoOperacaoMedidor.PONTO_CONSUMO,
						CriteriaSpecification.LEFT_JOIN);
				criteria.createAlias("pontoConsumo.rota", "rota", CriteriaSpecification.LEFT_JOIN);
				criteria.add(Restrictions.eq("rota.chavePrimaria", idRota));
			}

			String codigoMedidorSupervisorio = (String) filtro.get(Medidor.CODIGO_MEDIDOR);
			if (codigoMedidorSupervisorio != null) {
				criteria.add(Restrictions.eq(Medidor.CODIGO_MEDIDOR, codigoMedidorSupervisorio));
			}

			// paginacao banco
			boolean paginacaoPadrao = false;
			if (filtro.containsKey(COLECAO_PAGINADA)) {

				ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get(COLECAO_PAGINADA);
				colecaoPaginada.adicionarOrdenacaoEspecial(Medidor.NUMERO_DE_SERIE, new OrdenacaoEspecialNumeroSerie());
				HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

				if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
					paginacaoPadrao = true;
				}

			} else {
				paginacaoPadrao = true;
			}

			if (paginacaoPadrao) {
				criteria.addOrder(Order.asc(Medidor.NUMERO_DE_SERIE));
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #consultarMedidor(java.util.Map)
	 */
	@Override
	@SuppressWarnings({ "unchecked", "unused" })
	public Collection<Medidor> consultarMedidorDisponivel(Map<String, Object> filtro) throws NegocioException {

		long codigoMedidorDisponivel =
				Long.parseLong(getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_DISPONIVEL));

		Criteria criteria = getCriteria();

		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String numeroSerie = (String) filtro.get(Medidor.NUMERO_DE_SERIE);
			if (!StringUtils.isEmpty(numeroSerie)) {
				criteria.add(Restrictions.ilike(Medidor.NUMERO_DE_SERIE, Util.formatarTextoConsulta(numeroSerie)));
			}

			String numeroSerieParcial = (String) filtro.get(Medidor.NUMERO_SERIE_PARCIAL);
			if (!StringUtils.isEmpty(numeroSerieParcial)) {
				criteria.add(Restrictions.ilike(Medidor.NUMERO_DE_SERIE, Util.formatarTextoConsulta(numeroSerieParcial)));
			}

			Long idTipo = (Long) filtro.get(Medidor.ID_TIPO);
			if ((idTipo != null) && (idTipo > 0)) {
				criteria.createAlias(Medidor.TIPO_MEDIDOR, Medidor.TIPO_MEDIDOR);
				criteria.add(Restrictions.eq(TIPOMEDIDOR_CHAVE_PRImARIA, idTipo));
			} else {
				criteria.setFetchMode(Medidor.TIPO_MEDIDOR, FetchMode.JOIN);
			}

			Long idMarca = (Long) filtro.get(Medidor.ID_MARCA);
			if ((idMarca != null) && (idMarca > 0)) {
				criteria.createAlias(Medidor.MARCA_MEDIDOR, Medidor.MARCA_MEDIDOR);
				criteria.add(Restrictions.eq(MARCAMEDIDOR_CHAVE_PRIMARIA, idMarca));
			} else {
				criteria.setFetchMode(Medidor.MARCA_MEDIDOR, FetchMode.JOIN);
			}

			Long idModelo = (Long) filtro.get(Medidor.ID_MODELO);
			if ((idModelo != null) && (idModelo > 0)) {
				criteria.createAlias(Medidor.MODELO, Medidor.MODELO);
				criteria.add(Restrictions.eq(MODELO_CHAVE_PRIMARIA, idModelo));
			} else {
				criteria.setFetchMode(Medidor.MODELO, FetchMode.JOIN);
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			Long modoUso = (Long) filtro.get(Medidor.MODO_USO);
			if (modoUso != null) {
				criteria.add(Restrictions.eq(MODO_USO_CHAVE_PRIMARIA, modoUso));
			}

			String isMedidorVirtual = (String) filtro.get("isMedidorVirtual");
			if (isMedidorVirtual != null && "true".equals(isMedidorVirtual)) {
				criteria.add(Restrictions.isNotNull(Medidor.CODIGO_MEDIDOR));
			}

			criteria.add(Restrictions.eq(SITUACAO_MEDIDOR_CHAVE_PRIMARIA, codigoMedidorDisponivel));
			// Paginação em banco dados
			boolean paginacaoPadrao = false;
			if (filtro.containsKey(COLECAO_PAGINADA)) {
				ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get(COLECAO_PAGINADA);
				HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);
				if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
					paginacaoPadrao = true;
				}
			} else {
				paginacaoPadrao = true;
			}

		}

		return criteria.list();
	}

	/**
	 * metodo para validarRemoverMedidores
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #validarRemoverMedidores(java.lang.Long[])
	 */
	@Override
	public void validarRemoverMedidores(Long[] chavesPrimarias) throws GGASException {

		verificaSeMedidorCompoeMedidorVirtual(chavesPrimarias);

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/**
	 * Verifica se medidor compoe medidor virtual.
	 *
	 * @param chavesPrimarias the chaves primarias
	 * @throws NegocioException the negocio exception
	 */
	private void verificaSeMedidorCompoeMedidorVirtual(Long[] chavesPrimarias) throws GGASException {

		// obtem apenas os medidores virtuais
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(Medidor.MODO_USO, Long.valueOf(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL)));
		Collection<Medidor> listaMedidorVirtual = consultarMedidor(filtro);

		// verifica se algum medidor a ser removido compoe medidor virtual.
		List<String> listaNumeroSerieParaMensagem = new ArrayList<String>();
		for (Medidor medidorVirtual : listaMedidorVirtual) {
			String composicaoVirtual = medidorVirtual.getComposicaoVirtual();
			if (composicaoVirtual != null) {
				String[] arrayComposicaoVirtual;
				if (composicaoVirtual.contains("#")) {
					arrayComposicaoVirtual = composicaoVirtual.split("#");
				} else {
					arrayComposicaoVirtual = new String[1];
					arrayComposicaoVirtual[0] = composicaoVirtual;
				}
				for (String idMedidorComOperacao : arrayComposicaoVirtual) {
					String idMedidor = idMedidorComOperacao.substring(1);
					for (Long chavePrimariaMedidorReal : chavesPrimarias) {
						if ((chavePrimariaMedidorReal != null) && chavePrimariaMedidorReal.equals(Long.valueOf(idMedidor))) {
							Medidor medidor = (Medidor) obter(chavePrimariaMedidorReal);
							if (!listaNumeroSerieParaMensagem.contains(medidor.getNumeroSerie())) {
								listaNumeroSerieParaMensagem.add(medidor.getNumeroSerie());
							}
						}
					}
				}
			}
		}
		if (!listaNumeroSerieParaMensagem.isEmpty()) {
			StringBuilder numerosSerieParaMensagem = new StringBuilder();
			int i = 0;
			for (String numeroSerie : listaNumeroSerieParaMensagem) {
				if (i > 0) {
					numerosSerieParaMensagem.append(", ");
					numerosSerieParaMensagem.append(numeroSerie);
				} else {
					numerosSerieParaMensagem.append(numeroSerie);
				}
				i++;
			}
			throw new NegocioException(ERRO_NEGOCIO_COMPOSICAO_VIRTUAL_CONTEM_MEDIDOR, numerosSerieParaMensagem);
		}
	}

	/**
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #removerMedidores(java.lang.Long[])
	 */
	@Override
	public void removerMedidores(Long[] chavesPrimarias) throws NegocioException {

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS, chavesPrimarias);
			Collection<Medidor> listaMedidor = this.consultarMedidor(filtro);
			if ((listaMedidor != null) && (!listaMedidor.isEmpty())) {
				for (Medidor medidor : listaMedidor) {
					if (!existeMedidorHistoricoOperacao(medidor.getChavePrimaria())) {
						try {
							super.remover(medidor);
						} catch (NegocioException e) {
							LOG.error(e.getMessage(), e);
							throw new NegocioException(Constantes.ERRO_NEGOCIO_MEDIDOR_NAO_PODE_SER_REMOVIDO, true);
						}
					} else {
						throw new NegocioException(Constantes.ERRO_NEGOCIO_MEDIDOR_NAO_PODE_SER_REMOVIDO, true);
					}
				}
			}
		}
	}

	/**
	 * Existe medidor historico operacao.
	 *
	 * @param idMedidor the id medidor
	 * @return the boolean
	 */
	private Boolean existeMedidorHistoricoOperacao(Long idMedidor) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
		hql.append(" historicoOperacaoMedidor ");
		hql.append(" inner join fetch historicoOperacaoMedidor.medidor medidor ");
		hql.append(WHERE);
		hql.append(" medidor.chavePrimaria = :idMedidor ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idMedidor", idMedidor);

		List<HistoricoOperacaoMedidor> list = query.list();

		return !list.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#validarMedidorExistente(java.lang.Long, java.lang.String, java.lang.Long)
	 */
	@Override
	public void validarMedidorExistente(Long chavePrimariaMedidor, String numeroSerie, Long marcaMedidor) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(chavePrimaria) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where numeroSerie = :numeroSerie ");
		hql.append(" and marcaMedidor.chavePrimaria = :marcaMedidor ");

		if (chavePrimariaMedidor != null && chavePrimariaMedidor > 0) {
			hql.append(" and chavePrimaria <> :idMedidor ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString(Medidor.NUMERO_DE_SERIE, numeroSerie);
		if (chavePrimariaMedidor != null && chavePrimariaMedidor > 0) {
			query.setLong("idMedidor", chavePrimariaMedidor); // NOSONAR {não faz sentido criar constante para esta string }
		}

		query.setLong(Medidor.MARCA_MEDIDOR, marcaMedidor);

		Long resultado = (Long) query.uniqueResult();

		if (resultado != null && resultado > 0) {
			MarcaMedidor marca = this.obterMarcaMedidor(marcaMedidor);
			throw new NegocioException(ControladorMedidor.ERRO_NEGOCIO_MEDIDOR_EXISTENTE,
					new Object[] { numeroSerie, marca.getDescricao() });
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#validarMedidorExistentePorNumeroSerieEMarca(java.lang.Long, java.lang.String,
	 * java.lang.Long)
	 */
	@Override
	public boolean validarMedidorExistentePorNumeroSerieEMarca(Long chavePrimariaMedidor, String numeroSerie, Long marcaMedidor) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(chavePrimaria) from "); // NOSONAR {não faz sentido criar constante para esta string }
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where numeroSerie = :numeroSerie ");
		hql.append(" and marcaMedidor.chavePrimaria = :marcaMedidor ");

		if (chavePrimariaMedidor != null && chavePrimariaMedidor > 0) {
			hql.append(" and chavePrimaria <> :idMedidor "); // NOSONAR {não faz sentido criar constante para esta string }
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString(Medidor.NUMERO_DE_SERIE, numeroSerie);
		if (chavePrimariaMedidor != null && chavePrimariaMedidor > 0) {
			query.setLong("idMedidor", chavePrimariaMedidor); // NOSONAR {não faz sentido criar constante para esta string }
		}

		query.setLong(Medidor.MARCA_MEDIDOR, marcaMedidor);

		Long resultado = (Long) query.uniqueResult();

		return resultado != null && resultado > 0;
	}

	/**
	 * Validar medidor numero tombamento existente.
	 *
	 * @param chavePrimariaMedidor the chave primaria medidor
	 * @param tombamento the tombamento
	 * @throws NegocioException the negocio exception
	 */
	private void validarMedidorNumeroTombamentoExistente(Long chavePrimariaMedidor, String tombamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(chavePrimaria) from "); // NOSONAR {não faz sentido criar constante para esta string }
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where tombamento = :numeroTombamento ");

		if (chavePrimariaMedidor > 0) {
			hql.append(" and chavePrimaria <> :idMedidor "); // NOSONAR {não faz sentido criar constante para esta string }
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("numeroTombamento", tombamento);
		if (chavePrimariaMedidor > 0) {
			query.setLong("idMedidor", chavePrimariaMedidor); // NOSONAR {não faz sentido criar constante para esta string }
		}

		Long resultado = (Long) query.uniqueResult();

		if (resultado != null && resultado > 0) {
			throw new NegocioException(ControladorMedidor.ERRO_NEGOCIO_NUMERO_TOMBAMENTO_JA_EXISTENTE, tombamento);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # validarNumeroSerieMedidor(java.lang.String)
	 */
	@Override
	public void validarNumeroSerie(String numeroSerie) throws NegocioException {

		if (!Util.existeNumeroEmString(numeroSerie)) {
			throw new NegocioException(ControladorMedidor.FALHA_FORMATO_NUMERO_SERIE, true);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if (entidadeNegocio.getClass().equals(getClasseEntidadeMedidor())) {
			Medidor medidor = (Medidor) entidadeNegocio;
			validarMedidorExistente(medidor.getChavePrimaria(), medidor.getNumeroSerie(), medidor.getMarcaMedidor().getChavePrimaria());
			validarDatasMedidor(medidor);
			validarMedidorNumeroTombamentoExistente(medidor.getChavePrimaria(), medidor.getTombamento());
			validarNumeroDigitoLeitura(medidor.getDigito());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Medidor medidor = (Medidor) entidadeNegocio;
		validarMedidorExistente(medidor.getChavePrimaria(), medidor.getNumeroSerie(), medidor.getMarcaMedidor().getChavePrimaria());
		validarDatasMedidor(medidor);
		validarMedidorNumeroTombamentoExistente(medidor.getChavePrimaria(), medidor.getTombamento());
		validarNumeroDigitoLeitura(medidor.getDigito());
	}

	/**
	 * Validar datas medidor.
	 *
	 * @param medidor the medidor
	 * @throws NegocioException the negocio exception
	 */
	private void validarDatasMedidor(Medidor medidor) throws NegocioException {

		Date dataAquisicao = medidor.getDataAquisicao();
		Date dataMaximaInstalacao = medidor.getDataMaximaInstalacao();
		Calendar calendar = Calendar.getInstance();

		if (dataAquisicao != null) {
			// Data de aquisição deve ser menor
			// que a data atual
			if (dataAquisicao.compareTo(calendar.getTime()) > 0) {
				throw new NegocioException(ControladorMedidor.ERRO_DATA_AQUISICAO_MAIOR_QUE_DATA_ATUAL, dataAquisicao);
			}

			// Data de aquisição deve ser menor ou
			// igual a data max de
			// instalação
			if (dataMaximaInstalacao != null && dataAquisicao.compareTo(dataMaximaInstalacao) > 0) {
				throw new NegocioException(ControladorMedidor.ERRO_DATA_AQUISICAO_MAIOR_QUE_DATA_MAX_INSTALACAO, dataAquisicao);
			}

			// Valida se o ano de aquisição é
			// maior que o ano de fabricação
			DateTime dataAnoAquisicao = new DateTime(medidor.getDataAquisicao());
			int anoAquisicao = dataAnoAquisicao.getYear();
			Integer anoFabricacao = medidor.getAnoFabricacao();
			if ((anoFabricacao != null) && (anoFabricacao.intValue() > anoAquisicao)) {
				throw new NegocioException(ControladorMedidor.ERRO_ANO_FABRICACAO_SUPERIOR_ANO_ATUAL, anoAquisicao);
			}

			// Valida se a pressão máxima de
			// trabalho foi informada
			// conjuntamente com unidade
			if ((medidor.getPressaoMaximaTrabalho() != null && medidor.getUnidadePressaoMaximaTrabalho() == null)
					|| (medidor.getPressaoMaximaTrabalho() == null && medidor.getUnidadePressaoMaximaTrabalho() != null)) {
				throw new NegocioException(ControladorMedidor.ERRO_PRESSAO_UNIDADE_NAO_INFORMADAS_CONJUNTAMENTE, false);
			}
		}

		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		if (dataMaximaInstalacao != null && dataMaximaInstalacao.compareTo(calendar.getTime()) <= 0) {
			// Data máxima de instalação deve ser maior que a data atual.

			throw new NegocioException(ControladorMedidor.ERRO_DATA_MAX_INSTALACAO_MENOR_IGUAL_QUE_DATA_ATUAL, dataAquisicao);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#inserir(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public long inserir(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Long chavePrimaria = null;
		boolean inserirEmLote = false;
		Medidor medidor = (Medidor) entidadeNegocio;
		String numeroSerie = medidor.getNumeroSerie();
		String numeroInicial = medidor.getNumeroInicial();
		Integer quantidadeLote = medidor.getQuantidadeLote();
		String prefixo = medidor.getPrefixo();
		String sufixo = medidor.getSufixo();

		if (!StringUtils.isEmpty(numeroInicial) && quantidadeLote != null && quantidadeLote > 0 && StringUtils.isEmpty(numeroSerie)) {
			medidor.setNumeroSerie(gerarNumeroSerial(prefixo, sufixo, numeroInicial.length(), String.valueOf(numeroInicial)));
			inserirEmLote = true;
		}

		if (!inserirEmLote) {
			medidor.getMovimentacoes().add(criarMovimentacaoMedidor(medidor));
			medidor.setUltimaAlteracao(new Date());

			try {
				String chaveModoUsoIndependente = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE);

				if (chaveModoUsoIndependente.equals(String.valueOf(medidor.getModoUso().getChavePrimaria()))) {
					medidor.setSituacaoAssociacaoMedidorIndependente(
							fachada.obterEntidadeConteudo(Long.parseLong(fachada.obterValorConstanteSistemaPorCodigo(
									Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_AGUARDANDO_ATIVACAO))));
				}
			} catch (GGASException e) {
				LOG.info(e.getMessage());
				throw new NegocioException("ERRO_INESPERADO", true);
			}

			chavePrimaria = super.inserir(medidor);
		}

		if (inserirEmLote) {
			if (quantidadeLote != null && quantidadeLote <= 1 && inserirEmLote) {
				throw new NegocioException(Constantes.QUANTIDADE_MINIMA_INFORMADA_NAO_PODE_SER_MENOR_QUE_2, true);
			} else {

				chavePrimaria = this.inserirMedidoresEmLote(medidor);

			}
		}

		return chavePrimaria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#inserirMedidoresEmLote(br.com.ggas.medicao.medidor.Medidor)
	 */
	@Override
	public long inserirMedidoresEmLote(Medidor medidor) throws NegocioException {

		Long chavePrimaria = null;
		String numeroInicial = medidor.getNumeroInicial();
		Integer contadorNumeroInicial = Integer.valueOf(numeroInicial);
		Integer quantidadeLote = medidor.getQuantidadeLote();
		String prefixo = medidor.getPrefixo();
		String sufixo = medidor.getSufixo();
		Query query = null;
		Medidor novoMedidor = null;

		if (contadorNumeroInicial != null && contadorNumeroInicial >= 0 && quantidadeLote != null && quantidadeLote > 1) {

			while (quantidadeLote > 0) {
				novoMedidor = (Medidor) this.criar();

				Util.copyProperties(novoMedidor, medidor);

				novoMedidor
						.setNumeroSerie(gerarNumeroSerial(prefixo, sufixo, numeroInicial.length(), String.valueOf(contadorNumeroInicial)));

				contadorNumeroInicial++;

				query = getHibernateTemplate().getSessionFactory().getCurrentSession()
						.createQuery(this.getHQLConsultaMedidorExistentePorSerieMarca());
				query.setString(0, novoMedidor.getNumeroSerie());
				query.setLong(1, novoMedidor.getMarcaMedidor().getChavePrimaria());

				Long chavePrimariaMedidorExistente = (Long) query.uniqueResult();

				if (chavePrimariaMedidorExistente != null) {
					LOG.info("### Erro: inserirMedidoresEmLote Medidor existente com o numero de serie " + novoMedidor.getNumeroSerie());
				} else {
					novoMedidor.setMovimentacoes(new HashSet<MovimentacaoMedidor>());
					MovimentacaoMedidor movimentacaoMedidor = criarMovimentacaoMedidor(novoMedidor);
					novoMedidor.getMovimentacoes().add(movimentacaoMedidor);
					chavePrimaria = super.inserir(novoMedidor);
					getHibernateTemplate().getSessionFactory().getCurrentSession().flush();
					quantidadeLote--;

				}

			}
		} else {
			chavePrimaria = medidor.getChavePrimaria();
		}

		return chavePrimaria;
	}

	/**
	 * Criar movimentacao medidor.
	 *
	 * @param medidor the medidor
	 * @return the movimentacao medidor
	 * @throws NegocioException the negocio exception
	 */
	public MovimentacaoMedidor criarMovimentacaoMedidor(Medidor medidor) throws NegocioException {

		Calendar calendar = Calendar.getInstance();

		// Inserir Historico Medição
		MovimentacaoMedidor movimentacaoMedidor = (MovimentacaoMedidor) criarMovimentacaoMedidor();
		movimentacaoMedidor.setDataMovimento(calendar.getTime());
		movimentacaoMedidor.setHabilitado(true);
		movimentacaoMedidor.setDadosAuditoria(medidor.getDadosAuditoria());
		movimentacaoMedidor.setMedidor(medidor);
		movimentacaoMedidor.setUltimaAlteracao(new Date());

		long codigoMotivoMovimentacaoEntrada = Long
				.parseLong(getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_MOTIVO_MOVIMENTACAO_MEDIDOR_ENTRADA));

		MotivoMovimentacaoMedidor motivoMovimentacaoMedidor =
				(MotivoMovimentacaoMedidor) this.obter(codigoMotivoMovimentacaoEntrada, MotivoMovimentacaoMedidorImpl.class);
		movimentacaoMedidor.setMotivoMovimentacaoMedidor(motivoMovimentacaoMedidor);

		movimentacaoMedidor.setLocalArmazenagemDestino(medidor.getLocalArmazenagem());

		return movimentacaoMedidor;
	}

	/**
	 * Gerar numero serial.
	 *
	 * @param prefixo the prefixo
	 * @param sufixo the sufixo
	 * @param tamanho the tamanho
	 * @param numeroInicial the numero inicial
	 * @return the string
	 */
	private String gerarNumeroSerial(String prefixo, String sufixo, int tamanho, String numeroInicial) {

		StringBuilder numeroSerie = new StringBuilder();
		if (!StringUtils.isEmpty(prefixo)) {
			numeroSerie.append(prefixo);
		}
		if (!StringUtils.isEmpty(numeroInicial)) {
			Integer numeroInicialSemFormatacao = Integer.valueOf(numeroInicial);
			numeroSerie.append(Util.adicionarZerosEsquerdaNumero(String.valueOf(numeroInicialSemFormatacao), tamanho));
		}
		if (!StringUtils.isEmpty(sufixo)) {
			numeroSerie.append(sufixo);
		}

		return numeroSerie.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #consultarInstalacaoMedidor(java.lang.Long, java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<InstalacaoMedidor> consultarInstalacaoMedidor(Long chaveMedidor, Long chavePontoConsumo) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeInstalacaoMedidor());
		if (chaveMedidor != null && chaveMedidor > 0) {
			criteria.add(Restrictions.eq(MEDIDOR_CHAVE_PRIMARIA, chaveMedidor));
		}
		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, chavePontoConsumo));
		}

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # consultarHistoricoOperacaoMedidor(java.lang .Long, java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoOperacaoMedidor> consultarHistoricoOperacaoMedidor(Long chaveMedidor, Long chavePontoConsumo)
			throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeHistoricoOperacaoMedidor());
		if (chaveMedidor != null && chaveMedidor > 0) {
			criteria.createAlias(HistoricoOperacaoMedidor.MEDIDOR, HistoricoOperacaoMedidor.MEDIDOR);
			criteria.add(Restrictions.eq(MEDIDOR_CHAVE_PRIMARIA, chaveMedidor));
		} else {
			criteria.setFetchMode(HistoricoOperacaoMedidor.MEDIDOR, FetchMode.JOIN);
		}
		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			criteria.createAlias(HistoricoOperacaoMedidor.PONTO_CONSUMO, HistoricoOperacaoMedidor.PONTO_CONSUMO);
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, chavePontoConsumo));
		} else {
			criteria.setFetchMode(HistoricoOperacaoMedidor.PONTO_CONSUMO, FetchMode.JOIN);
			criteria.setFetchMode("pontoConsumo.imovel", FetchMode.JOIN);
		}
		criteria.setFetchMode("medidorLocalInstalacao", FetchMode.JOIN);
		criteria.setFetchMode("motivoOperacaoMedidor", FetchMode.JOIN);
		criteria.setFetchMode(InstalacaoMedidor.FUNCIONARIO, FetchMode.JOIN);
		criteria.setFetchMode("unidadePressaoAnterior", FetchMode.JOIN);
		criteria.setFetchMode(HistoricoOperacaoMedidor.OPERACAO_MEDIDOR, FetchMode.JOIN);
		criteria.setFetchMode(HistoricoOperacaoMedidor.LOCAL_INSTALACAO_MEDIDOR, FetchMode.JOIN);

		criteria.addOrder(Order.desc(HistoricoOperacaoMedidor.DATA_REALIZADA)).addOrder(Order.desc("ultimaAlteracao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # validarMedidorComHistoricoOperacao(java.util .Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Medidor> pesquisarMedidorComHistoricoOperacao(Map<String, Object> filtro) throws NegocioException {

		DetachedCriteria dc = DetachedCriteria.forClass(getClasseEntidadeHistoricoOperacaoMedidor(), "historico");
		dc.createCriteria("historico.medidor", HistoricoOperacaoMedidor.MEDIDOR);
		dc.setProjection(Projections.property(HistoricoOperacaoMedidor.MEDIDOR));

		Criteria criteria = createCriteria(getClasseEntidade());

		if (filtro != null) {

			String numeroSerie = (String) filtro.get(Medidor.NUMERO_DE_SERIE);
			if (!StringUtils.isEmpty(numeroSerie)) {
				criteria.add(Restrictions.ilike(Medidor.NUMERO_DE_SERIE, Util.formatarTextoConsulta(numeroSerie)));
			}

			String numeroSerieParcial = (String) filtro.get(Medidor.NUMERO_SERIE_PARCIAL);
			if (!StringUtils.isEmpty(numeroSerieParcial)) {
				criteria.add(Restrictions.ilike(Medidor.NUMERO_DE_SERIE, Util.formatarTextoConsulta(numeroSerieParcial)));
			}

			Long idTipo = (Long) filtro.get(Medidor.ID_TIPO);
			if ((idTipo != null) && (idTipo > 0)) {
				criteria.createAlias(Medidor.TIPO_MEDIDOR, Medidor.TIPO_MEDIDOR, CriteriaSpecification.LEFT_JOIN);
				criteria.add(Restrictions.eq(TIPOMEDIDOR_CHAVE_PRImARIA, idTipo));
			} else {
				criteria.createAlias(Medidor.TIPO_MEDIDOR, Medidor.TIPO_MEDIDOR, CriteriaSpecification.LEFT_JOIN);
			}

			Long idMarca = (Long) filtro.get(Medidor.ID_MARCA);
			if ((idMarca != null) && (idMarca > 0)) {
				criteria.createAlias(Medidor.MARCA_MEDIDOR, Medidor.MARCA_MEDIDOR, CriteriaSpecification.LEFT_JOIN);
				criteria.add(Restrictions.eq(MARCAMEDIDOR_CHAVE_PRIMARIA, idMarca));
			} else {
				criteria.createAlias(Medidor.MARCA_MEDIDOR, Medidor.MARCA_MEDIDOR, CriteriaSpecification.LEFT_JOIN);
			}

			Long idModelo = (Long) filtro.get(Medidor.ID_MODELO);
			if ((idModelo != null) && (idModelo > 0)) {
				criteria.createAlias(Medidor.MODELO, Medidor.MODELO, CriteriaSpecification.LEFT_JOIN);
				criteria.add(Restrictions.eq(MODELO_CHAVE_PRIMARIA, idModelo));
			} else {
				criteria.createAlias(Medidor.MODELO, Medidor.MODELO, CriteriaSpecification.LEFT_JOIN);
			}

			criteria.add(Property.forName(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA).in(dc));

			// Paginação em banco dados
			boolean paginacaoPadrao = false;
			if (filtro.containsKey(COLECAO_PAGINADA)) {

				ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get(COLECAO_PAGINADA);
				HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

				if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
					paginacaoPadrao = true;
				}

			} else {
				paginacaoPadrao = true;
			}

			if (paginacaoPadrao) {
				criteria.addOrder(Order.asc(Medidor.NUMERO_DE_SERIE));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # consultarMovimentacaoMedidor(java.util.Map)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<MovimentacaoMedidor> consultarMovimentacaoMedidor(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeMovimentacaoMedidor());
		if (filtro != null) {
			Long idMedidor = (Long) filtro.get(Medidor.ID_MEDIDOR);
			if (idMedidor != null && idMedidor > 0) {
				criteria.createAlias(HistoricoOperacaoMedidor.MEDIDOR, HistoricoOperacaoMedidor.MEDIDOR);
				criteria.add(Restrictions.eq(MEDIDOR_CHAVE_PRIMARIA, idMedidor));
			}
			criteria.setFetchMode("motivoMovimentacaoMedidor", FetchMode.JOIN);
			criteria.setFetchMode("localArmazenagemOrigem", FetchMode.JOIN);
			criteria.setFetchMode("localArmazenagemDestino", FetchMode.JOIN);
			criteria.setFetchMode(InstalacaoMedidor.FUNCIONARIO, FetchMode.JOIN);
			criteria.addOrder(Order.desc("dataMovimento"));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # verificarListaMovimentacao(java.util.Collection )
	 */
	@Override
	public void verificarListaMovimentacao(Collection<MovimentacaoMedidor> movimentacoes) throws NegocioException {

		if (movimentacoes == null || movimentacoes.isEmpty()) {
			throw new NegocioException(ControladorMedidor.ERRO_NAO_EXISTE_MOVIMENTACAO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # verificarListaHistoricoInstalacaoMedidor(java .util.Collection)
	 */
	@Override
	public void verificarListaHistoricoInstalacaoMedidor(Collection<InstalacaoMedidor> listaInstalacaoMedidor) throws NegocioException {

		if (listaInstalacaoMedidor == null || listaInstalacaoMedidor.isEmpty()) {
			throw new NegocioException(ControladorMedidor.ERRO_NAO_EXISTE_INSTALACAO_MEDIDOR, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seebr.com.ggas.medicao.medidor. ControladorMedidor# verificarListaHistoricoOperacaoMedidor(java. util.Collection)
	 */
	@Override
	public void verificarListaHistoricoOperacaoMedidor(Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor)
			throws NegocioException {

		if ((listaHistoricoOperacaoMedidor == null) || (listaHistoricoOperacaoMedidor.isEmpty())) {
			throw new NegocioException(ControladorMedidor.ERRO_NAO_EXISTE_INSTALACAO_MEDIDOR, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # obterHistoricoOperacaoMedidorPorChaveMedidorOperacao (long, long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HistoricoOperacaoMedidor obterHistoricoOperacaoMedidorPorChaveMedidorOperacao(long chavePrimariaMedidor,
			long chavePrimariaOperacao, long chavePontoConsumo, Date dataInicio, Date dataFim) throws NegocioException {

		HistoricoOperacaoMedidor retorno = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
		hql.append(" historico "); // NOSONAR {não faz sentido criar constante para esta string }
		hql.append(" where historico.medidor.chavePrimaria = :chavePrimariaMedidor ");
		hql.append(" and historico.operacaoMedidor.chavePrimaria = :chavePrimariaOperacao ");
		hql.append(" and historico.pontoConsumo.chavePrimaria = :chavePontoConsumo ");

		hql.append(" and historico.dataRealizada between :dataInicialPeriodo and :dataFinalPeriodo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(PARAM_CHAVE_PRIMARIA_MEDIDOR, chavePrimariaMedidor);
		query.setLong(PARAM_CHAVE_PRIMARIA_OPERACAO, chavePrimariaOperacao);
		query.setLong("chavePontoConsumo", chavePontoConsumo);

		Util.adicionarRestricaoDataSemHora(query, dataInicio, "dataInicialPeriodo", Boolean.TRUE); // NOSONAR {não faz sentido criar
																									// constante para esta string }
		Util.adicionarRestricaoDataSemHora(query, dataFim, "dataFinalPeriodo", Boolean.FALSE); // NOSONAR {não faz sentido criar constante
																								// para esta string }

		Collection<HistoricoOperacaoMedidor> lista = query.list();

		if (lista != null && lista.iterator().hasNext()) {
			retorno = lista.iterator().next();
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #consultarHistoricoOperacaoMedidor(long, long, java.util.Date, java.util.Date)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<HistoricoOperacaoMedidor> consultarHistoricoOperacaoMedidor(long chavePrimariaMedidor, long chavePrimariaOperacao,
			Date dataInicialPeriodo, Date dataFinalPeriodo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
		hql.append(" historico "); // NOSONAR {não faz sentido criar constante para esta string }
		hql.append(" where historico.medidor.chavePrimaria = :chavePrimariaMedidor ");
		hql.append(" and historico.operacaoMedidor.chavePrimaria = :chavePrimariaOperacao ");
		hql.append(" and historico.dataRealizada between :dataInicialPeriodo and :dataInicialPeriodo ");
		hql.append(" order by historico.dataRealizada ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(PARAM_CHAVE_PRIMARIA_MEDIDOR, chavePrimariaMedidor);
		query.setLong(PARAM_CHAVE_PRIMARIA_OPERACAO, chavePrimariaOperacao);

		Util.adicionarRestricaoDataSemHora(query, dataInicialPeriodo, "dataInicialPeriodo", Boolean.TRUE); // NOSONAR {não faz sentido criar
																											// constante para esta string }
		Util.adicionarRestricaoDataSemHora(query, dataFinalPeriodo, "dataFinalPeriodo", Boolean.FALSE); // NOSONAR {não faz sentido criar
																										// constante para esta string }

		return query.list();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #permiteAlteracaoSituacaoMedidor(long)
	 */
	@Override
	public boolean permiteAlteracaoSituacaoMedidor(long chavePrimariaMedidor) throws NegocioException {

		boolean permiteAlteracao = true;

		StringBuilder hql = new StringBuilder();
		hql.append(" select situacaoMedidor.usoSistema from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where chavePrimaria = :chavePrimariaMedidor ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(PARAM_CHAVE_PRIMARIA_MEDIDOR, chavePrimariaMedidor);

		Boolean usoSistema = (Boolean) query.uniqueResult();

		if (usoSistema != null && usoSistema) {
			permiteAlteracao = false;
		}

		if (permiteAlteracao) {
			Integer quantidadePontos = obterQuantidadeMedidoresInstalados(chavePrimariaMedidor);
			if (quantidadePontos != null && quantidadePontos > 0) {
				permiteAlteracao = false;
			}
		}

		return permiteAlteracao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #permiteAlteracaoNumeroSerieMedidor(long)
	 */
	@Override
	public boolean permiteAlteracaoNumeroSerieMedidor(long chavePrimariaMedidor) throws NegocioException {

		boolean permiteAlteracao = true;

		Integer quantidadePontos = obterQuantidadeMedidoresInstalados(chavePrimariaMedidor);
		if ((quantidadePontos != null) && (quantidadePontos > 0)) {
			permiteAlteracao = false;
		}

		return permiteAlteracao;
	}

	/**
	 * Obter quantidade medidores instalados.
	 *
	 * @param chavePrimariaMedidor the chave primaria medidor
	 * @return the integer
	 * @throws NegocioException the negocio exception
	 */
	private Integer obterQuantidadeMedidoresInstalados(long chavePrimariaMedidor) throws NegocioException {

		Long resultado = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT count(ponto) FROM ");
		hql.append(getControladorPontoConsumo().getClasseEntidade().getSimpleName()).append(" ponto ");
		hql.append(WHERE);
		hql.append(" ponto.instalacaoMedidor.medidor.chavePrimaria = :chavePrimariaMedidor ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(PARAM_CHAVE_PRIMARIA_MEDIDOR, chavePrimariaMedidor);

		resultado = (Long) query.uniqueResult();

		return resultado.intValue();
	}

	@Override
	public List<HistoricoOperacaoMedidor> obterHistoricoOperacaoMedidor(long chavePrimariaMedidor, long chavePrimariaOperacao) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
		hql.append(" where medidor.chavePrimaria = :chavePrimariaMedidor ");
		hql.append(" and operacaoMedidor.chavePrimaria = :chavePrimariaOperacao ");
		hql.append(" order by dataRealizada ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(PARAM_CHAVE_PRIMARIA_MEDIDOR, chavePrimariaMedidor);
		query.setLong(PARAM_CHAVE_PRIMARIA_OPERACAO, chavePrimariaOperacao);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#obterInstalacaoMedidorPorChave(long)
	 */
	@Override
	public InstalacaoMedidor obterInstalacaoMedidorPorChave(long chavePrimariaInstalacaoMedidor) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeInstalacaoMedidor().getSimpleName());
		hql.append(" instalacaoMedidor ");
		hql.append(" inner join fetch instalacaoMedidor.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch instalacaoMedidor.pontoConsumo.imovel imovel ");
		hql.append(" inner join fetch instalacaoMedidor.medidor medidor ");
		hql.append(" where instalacaoMedidor.chavePrimaria = pontoConsumo.instalacaoMedidor.chavePrimaria ");
		hql.append(" and instalacaoMedidor.medidor.chavePrimaria = :chavePrimariaMedidor ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(PARAM_CHAVE_PRIMARIA_MEDIDOR, chavePrimariaInstalacaoMedidor);

		return (InstalacaoMedidor) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#obterInstalacaoMedidorPorCorretorVazao(long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public InstalacaoMedidor obterInstalacaoMedidorPorCorretorVazao(long chavePrimariaCorretorVazao) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeInstalacaoMedidor().getSimpleName());
		hql.append(" instalacaoMedidor ");
		hql.append(" inner join fetch instalacaoMedidor.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch instalacaoMedidor.pontoConsumo.imovel imovel ");
		hql.append(" inner join fetch instalacaoMedidor.vazaoCorretor vazaoCorretor ");
		hql.append(" inner join fetch instalacaoMedidor.medidor medidor ");
		hql.append(WHERE);
		hql.append(" instalacaoMedidor.vazaoCorretor.chavePrimaria = :chavePrimariaVazaoCorretor ");
		hql.append(" order by instalacaoMedidor.chavePrimaria desc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaVazaoCorretor", chavePrimariaCorretorVazao);

		List<InstalacaoMedidor> lista = query.list();

		if (lista != null && !lista.isEmpty()) {

			return Util.primeiroElemento(lista);

		} else {

			return null;

		}

	}

	/**
	 * Validar numero digito leitura.
	 *
	 * @param digito the digito
	 * @throws NegocioException the negocio exception
	 */
	private void validarNumeroDigitoLeitura(Integer digito) throws NegocioException {

		if (digito != null && digito <= 0) {
			throw new NegocioException(ERRO_NUMERO_DIGITO_LEITURA_INVALIDO, false);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarOperacaoMedidor()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OperacaoMedidor> listarOperacaoMedidor() throws NegocioException {

		Criteria criteria = this.createCriteria(OperacaoMedidor.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#validarDadosAssociacaoMedidor(java.util.Map)
	 */
	@Override
	public void validarDadosAssociacaoMedidor(Map<String, Object> dados) throws NegocioException {

		if (dados.get(Medidor.ID_PONTO_CONSUMO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_PONTO_CONSUMO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_OPERACAO_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO));
		long codigoOperacao = ((Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR)).longValue();

		if (codigoOperacao == OperacaoMedidor.CODIGO_INSTALACAO) {

			if (pontoConsumo.getImovel().getSituacaoImovel().getChavePrimaria() == SituacaoImovel.SITUACAO_POTENCIAL) {
				throw new NegocioException(ControladorMedidor.ERRO_IMOVEL_SITUACAO_SEM_REDE_ASSOCIACAO_MEDIDOR, true);
			}

			if (pontoConsumo.getInstalacaoMedidor() != null) {
				throw new NegocioException(ControladorMedidor.ERRO_PONTO_CONSUMO_POSSUI_MEDIDOR_INSTALADO_ASSOCIACAO_MEDIDOR, true);
			}

		} else if ((codigoOperacao == OperacaoMedidor.CODIGO_SUBSTITUICAO) || (codigoOperacao == OperacaoMedidor.CODIGO_RETIRADA)
				|| (codigoOperacao == OperacaoMedidor.CODIGO_ATIVACAO) || (codigoOperacao == OperacaoMedidor.CODIGO_BLOQUEIO)
				|| (codigoOperacao == OperacaoMedidor.CODIGO_REATIVACAO) || (codigoOperacao == OperacaoMedidor.CODIGO_BLOQUEIO_REATIVACAO)) {

			if (pontoConsumo.getInstalacaoMedidor() == null) {
				throw new NegocioException(ControladorMedidor.ERRO_PONTO_CONSUMO_DEVE_POSSUIR_MEDIDOR_INSTALADO_ASSOCIACAO_MEDIDOR, true);
			} else {

				if ((pontoConsumo.getInstalacaoMedidor().getDataAtivacao() != null)
						&& (codigoOperacao == OperacaoMedidor.CODIGO_ATIVACAO)) {
					throw new NegocioException(ControladorMedidor.ERRO_PONTO_CONSUMO_POSSUI_MEDIDOR_ATIVO_ASSOCIACAO_MEDIDOR, true);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#validarDadosAssociacaoMedidor(java.util.Map)
	 */
	@Override
	public void validarDadosAssociacaoMedidorLote(Map<String, Object> dados) throws NegocioException {

		if (dados.get(ID_IMOVEL) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_IMOVEL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_OPERACAO_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		Imovel imovel = (Imovel) getControladorImovel().obter((Long) dados.get(ID_IMOVEL));
		long codigoOperacao = ((Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR)).longValue();

		if (codigoOperacao == OperacaoMedidor.CODIGO_INSTALACAO
				&& imovel.getSituacaoImovel().getChavePrimaria() == SituacaoImovel.SITUACAO_POTENCIAL) {
			throw new NegocioException(ControladorMedidor.ERRO_IMOVEL_SITUACAO_SEM_REDE_ASSOCIACAO_MEDIDOR, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#validarDadosAssociacaoCorretorVazao(java.util.Map)
	 */
	@Override
	public void validarDadosAssociacaoCorretorVazao(Map<String, Object> dados) throws NegocioException {

		if (dados.get(Medidor.ID_PONTO_CONSUMO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_PONTO_CONSUMO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_OPERACAO_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		PontoConsumo pontoConsumo = (PontoConsumo) dados.get(HistoricoOperacaoMedidor.PONTO_CONSUMO);
		if (pontoConsumo == null) {
			pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO));
		}

		long codigoOperacao = ((Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR)).longValue();

		if (codigoOperacao == OperacaoMedidor.CODIGO_INSTALACAO && pontoConsumo.getInstalacaoMedidor() != null
				&& pontoConsumo.getInstalacaoMedidor().getVazaoCorretor() != null) {
			throw new NegocioException(ControladorMedidor.ERRO_PONTO_CONSUMO_POSSUI_CORRETOR_VAZAO_INSTALADO_ASSOCIACAO_MEDIDOR, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#validarDadosAssociacaoCorretorVazaoMedidorIndependente(java.util.Map)
	 */
	@Override
	public void validarDadosAssociacaoCorretorVazaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		if (dados.get(Medidor.ID_OPERACAO_MEDIDOR_INDEPENDENTE) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_OPERACAO_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}
		Medidor medidor = fachada.obterMedidor((Long) dados.get(ID_MEDIDOR_INDEPENDENTE));
		long codigoOperacao = ((Long) dados.get(Medidor.ID_OPERACAO_MEDIDOR_INDEPENDENTE)).longValue();

		if (codigoOperacao == OperacaoMedidor.CODIGO_INSTALACAO && medidor.getInstalacaoMedidor() != null
				&& medidor.getInstalacaoMedidor().getVazaoCorretor() != null) {
			throw new NegocioException(ControladorMedidor.ERRO_MEDIDOR_INDEPENDENTE_POSSUI_CORRETOR_VAZAO_INSTALADO_ASSOCIACAO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#validarPontoConsumoPossuiMedidor(java.util.Map)
	 */
	@Override
	public void validarPontoConsumoPossuiMedidor(Map<String, Object> dados) throws NegocioException {

		PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO));

		dados.put(HistoricoOperacaoMedidor.PONTO_CONSUMO, pontoConsumo);

		if (pontoConsumo.getInstalacaoMedidor() == null) {
			throw new NegocioException(ControladorMedidor.ERRO_PONTO_CONSUMO_DEVE_POSSUIR_MEDIDOR_INSTALADO_ASSOCIACAO_MEDIDOR, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterOperacaoMedidor(long)
	 */
	@Override
	public OperacaoMedidor obterOperacaoMedidor(long chavePrimaria) throws NegocioException {

		Criteria criteria = this.createCriteria(OperacaoMedidor.class);
		criteria.add(Restrictions.idEq(chavePrimaria));

		return (OperacaoMedidor) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterTiposAssociacoes()
	 */
	@Override
	public Map<Integer, String> obterTiposAssociacoes() {

		Map<Integer, String> associacoes = new HashMap<Integer, String>();
		associacoes.put(CODIGO_TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR, NOME_TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR);
		associacoes.put(CODIGO_TIPO_ASSOCIACAO_PONTO_CONSUMO_CORRETOR_VAZAO, NOME_TIPO_ASSOCIACAO_PONTO_CONSUMO_CORRETOR_VAZAO);

		return associacoes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #isAssociacaoPontoConsumoMedidor(java.lang. Integer)
	 */
	@Override
	public boolean isAssociacaoPontoConsumoMedidor(Integer codigoAssociacao) {

		return codigoAssociacao != null && codigoAssociacao.equals(CODIGO_TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # isAssociacaoPontoConsumoCorretorVazao(java. lang.Integer)
	 */
	@Override
	public boolean isAssociacaoPontoConsumoCorretorVazao(Integer codigoAssociacao) {

		return codigoAssociacao != null && codigoAssociacao.equals(CODIGO_TIPO_ASSOCIACAO_PONTO_CONSUMO_CORRETOR_VAZAO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarMedidorLocalInstalacao()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MedidorLocalInstalacao> listarMedidorLocalInstalacao() throws NegocioException {

		Criteria criteria = this.createCriteria(MedidorLocalInstalacao.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarMedidorProtecao()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MedidorProtecao> listarMedidorProtecao() throws NegocioException {

		Criteria criteria = this.createCriteria(MedidorProtecao.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarMotivoOperacaoMedidor()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<MotivoOperacaoMedidor> listarMotivoOperacaoMedidor() throws NegocioException {

		Criteria criteria = this.createCriteria(MotivoOperacaoMedidor.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterInstalacaoMedidorPontoConsumo(long)
	 */
	@Override
	public InstalacaoMedidor obterInstalacaoMedidorPontoConsumo(long chavePontoConsumo) throws NegocioException {

		Criteria criteria = this.createCriteria(PontoConsumo.class);
		criteria.createAlias(Medidor.INSTALACAO_MEDIDOR, Medidor.INSTALACAO_MEDIDOR, Criteria.LEFT_JOIN);

		criteria.createAlias("instalacaoMedidor.medidor", HistoricoOperacaoMedidor.MEDIDOR, Criteria.LEFT_JOIN);
		criteria.createAlias("medidor.tipoMedidor", Medidor.TIPO_MEDIDOR, Criteria.LEFT_JOIN);
		criteria.createAlias("medidor.modelo", MODELO_MEDIDOR, Criteria.LEFT_JOIN);
		criteria.createAlias("medidor.marcaMedidor", Medidor.MARCA_MEDIDOR, Criteria.LEFT_JOIN);

		criteria.createAlias("instalacaoMedidor.vazaoCorretor", VAZAO_CORRETOR, Criteria.LEFT_JOIN);
		criteria.createAlias("vazaoCorretor.marcaCorretor", "marcaCorretor", Criteria.LEFT_JOIN);
		criteria.createAlias("vazaoCorretor.modelo", "modeloCorretorVazao", Criteria.LEFT_JOIN);
		criteria.add(Restrictions.idEq(chavePontoConsumo));

		PontoConsumo pontoConsumo = (PontoConsumo) criteria.uniqueResult();

		return pontoConsumo.getInstalacaoMedidor();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # obterVazaoCorretorPorNumeroSerie(java.lang. String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<VazaoCorretor> obterVazaoCorretorPorNumeroSerie(String numeroSerie) throws NegocioException {

		Criteria criteria = this.createCriteria(VazaoCorretor.class);
		criteria.createAlias(Medidor.MODELO, Medidor.MODELO, Criteria.LEFT_JOIN);
		criteria.createAlias(VazaoCorretor.MARCA_CORRETOR, VazaoCorretor.MARCA_CORRETOR, Criteria.LEFT_JOIN);
		criteria.createAlias(Medidor.SITUACAO_MEDIDOR, Medidor.SITUACAO_MEDIDOR, Criteria.LEFT_JOIN);
		criteria.createAlias(Medidor.LOCAL_ARMAZENAGEM, Medidor.LOCAL_ARMAZENAGEM, Criteria.LEFT_JOIN);
		criteria.add(Restrictions.eq(Medidor.NUMERO_DE_SERIE, numeroSerie));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#inserirDadosAssociacaoPontoConsumoCorretorVazao(java.util.Map)
	 */
	@Override
	public void inserirDadosAssociacaoPontoConsumoCorretorVazao(Map<String, Object> dados) throws NegocioException, ConcorrenciaException {

		Long idOperacaoMedidor = (Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR);

		this.validarPontoConsumoPossuiMedidor(dados);
		this.validarDadosAssociacaoCorretorVazao(dados);

		if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_INSTALACAO) {
			this.validarDataInstalacaoInferiorDataUltimaRetirada(dados, Boolean.FALSE);
			this.validarDadosInstalacaoCorretorVazao(dados);
			this.inserirDadosInstalacaoCorretorVazao(dados);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_SUBSTITUICAO) {
			this.validarDadosSubstituicaoCorretorVazao(dados);
			this.inserirDadosSubstituicaoCorretorVazao(dados);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_RETIRADA) {
			this.validarDadosRetiradaCorretorVazao(dados);
			this.inserirDadosRetiradaCorretorVazao(dados);
		}
	}

	/**
	 * Insere dados da associação de medidor independente ao corretor de vazão.
	 *
	 * @param dados the dados
	 * @throws GGASException the GGASException
	 */
	@Override
	public void inserirDadosAssociacaoCorretorVazaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		Long idOperacaoMedidor = (Long) dados.get(Medidor.ID_OPERACAO_MEDIDOR_INDEPENDENTE);

		this.validarDadosAssociacaoCorretorVazaoMedidorIndependente(dados);

		if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_INSTALACAO) {
			this.validarDataInstalacaoInferiorDataUltimaRetirada(dados, Boolean.FALSE);
			this.validarDadosInstalacaoCorretorVazao(dados);
			this.inserirDadosInstalacaoCorretorVazaoMedidorIndependente(dados);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_SUBSTITUICAO) {
			this.validarDadosSubstituicaoCorretorVazao(dados);
			try {
				this.inserirDadosSubstituicaoCorretorVazaoMedidorIndependente(dados);
			} catch (Exception e) {

				throw new GGASException(e);
			}
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_RETIRADA) {
			this.validarDadosRetiradaCorretorVazao(dados);
			this.inserirDadosRetiradaCorretorVazaoMedidorIndependente(dados);
		}
	}

	/**
	 * Inserir dados associacao ponto consumo medidor.
	 *
	 * @param dados the dados
	 * @throws GGASException the GGASException
	 */
	@Override
	public PontoConsumo inserirDadosAssociacaoPontoConsumoMedidor(Map<String, Object> dados) throws GGASException {
		
		PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO));

		Long idOperacaoMedidor = (Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR);

		this.validarDadosAssociacaoMedidor(dados);

		if (idOperacaoMedidor.longValue() != OperacaoMedidor.CODIGO_ATIVACAO) {
			this.validarDadosAssociacaoCorretorVazao(dados);
		}

		if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_INSTALACAO) {
			this.validarDadosInstalacaoMedidor(dados);
			Boolean instalarCorretorVazao = (Boolean) dados.get(INSTALAR_CORRETOR_VAZAO);
			if (instalarCorretorVazao == null || instalarCorretorVazao) {
				this.validarDadosInstalacaoCorretorVazao(dados);
			}
			this.inserirDadosInstalacaoMedidor(dados, pontoConsumo);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_INSTALACAO_ATIVACAO) {
			this.validarDadosInstalacaoMedidor(dados);
			Boolean instalarCorretorVazao = (Boolean) dados.get(INSTALAR_CORRETOR_VAZAO);
			if (instalarCorretorVazao == null || instalarCorretorVazao) {
				this.validarDadosInstalacaoCorretorVazao(dados);
			}
			this.inserirDadosInstalacaoMedidor(dados, pontoConsumo);
			dados.put(CHAVE_MEDIDOR_ANTERIOR, dados.get(CHAVE_MEDIDOR_ATUAL));
			dados.put(LEITURA_ANTERIOR, dados.get(LEITURA_ATUAL));

			// <input type="hidden" name="chaveMedidorAnterior" value="297">
			// <input type="hidden" name="numeroDigitosMedidorAnterior" value="5" >
			this.validarDadosAtivacaoMedidor(dados);
			this.inserirDadosAtivacaoMedidor(dados, pontoConsumo);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_SUBSTITUICAO) {
			this.validarDadosSubstituicaoMedidor(dados);
			Boolean substituirCorretorVazao = (Boolean) dados.get(SUBSTITUIR_CORRETOR_VAZAO);
			if (substituirCorretorVazao != null && substituirCorretorVazao) {
				this.validarDadosSubstituicaoCorretorVazao(dados);
			}
			try {
				this.inserirDadosSubstituicaoMedidor(dados, pontoConsumo);
			} catch (Exception e) {

				throw new GGASException(e);
			}
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_ATIVACAO) {
			this.validarDadosAtivacaoMedidor(dados);
			this.inserirDadosAtivacaoMedidor(dados, pontoConsumo);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_RETIRADA) {
			this.validarDadosRetiradaMedidor(dados);
			Boolean contemCorretorVazao = (Boolean) dados.get(CONTEM_CORRETOR_VAZAO);
			if (contemCorretorVazao == null || contemCorretorVazao) {
				this.validarDadosRetiradaCorretorVazao(dados);
			}

			this.inserirDadosRetiradaMedidor(dados, pontoConsumo);
		}
		
		
		System.out.println("--> situacao FINAL: " + pontoConsumo.getSituacaoConsumo().getChavePrimaria());
		if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_BLOQUEIO) {
			this.validarDadosBloqueioMedidor(dados);
			this.inserirDadosBloqueioMedidor(dados, pontoConsumo, Boolean.FALSE);
		}
		
		if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_REATIVACAO) {
			this.validarDadosReativacaoMedidor(dados);
			this.inserirDadosReativacaoMedidor(dados, pontoConsumo, Boolean.FALSE);
		}
		
		if(idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_BLOQUEIO_REATIVACAO) {
			this.validarDadosBloqueioMedidor(dados);
			this.inserirDadosBloqueioReativacaoMedidor(dados, pontoConsumo);
		}
		
		return pontoConsumo;
	}

	private void inserirDadosBloqueioReativacaoMedidor(Map<String, Object> dados, PontoConsumo pontoConsumo) throws GGASException {
		this.inserirDadosBloqueioMedidor(dados, pontoConsumo, Boolean.TRUE);
		
		dados.put(MEDIDOR_MOTIVO_OPERACAO, 2l);
		
		this.inserirDadosReativacaoMedidor(dados, pontoConsumo, Boolean.TRUE);
		
	}

	/**
	 * Insere Dados da associação do medidor independente.
	 *
	 * @param dados the dados
	 * @throws GGASException the GGASException
	 */
	@Override
	public void inserirDadosAssociacaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		Long idOperacaoMedidorIndependente = (Long) dados.get(Medidor.ID_OPERACAO_MEDIDOR_INDEPENDENTE);

		if (idOperacaoMedidorIndependente.longValue() == OperacaoMedidor.CODIGO_INSTALACAO) {
			this.validarDadosInstalacaoMedidor(dados);
			Boolean instalarCorretorVazao = (Boolean) dados.get(INSTALAR_CORRETOR_VAZAO);
			if (instalarCorretorVazao == null || instalarCorretorVazao) {
				this.validarDadosInstalacaoCorretorVazao(dados);
			}
			try {
				this.inserirDadosInstalacaoMedidorIndependente(dados);
			} catch (Exception e) {

				throw new GGASException(e);
			}
		} else if (idOperacaoMedidorIndependente.longValue() == OperacaoMedidor.CODIGO_RETIRADA) {
			this.validarDadosRetiradaMedidorIndependente(dados);
			Boolean contemCorretorVazao = (Boolean) dados.get(CONTEM_CORRETOR_VAZAO);
			if (contemCorretorVazao == null || contemCorretorVazao) {
				this.validarDadosRetiradaCorretorVazao(dados);
			}
			this.inserirDadosRetiradaMedidorIndependente(dados);
		} else if (idOperacaoMedidorIndependente.longValue() == OperacaoMedidor.CODIGO_ATIVACAO) {
			this.validarDadosAtivacaoReativacaoMedidorIndependente(dados);
			this.inserirDadosAtivacaoMedidorIndependente(dados);
		} else if (idOperacaoMedidorIndependente.longValue() == OperacaoMedidor.CODIGO_SUBSTITUICAO) {
			this.validarDadosSubstituicaoMedidorIndependente(dados);
			Boolean substituirCorretorVazao = (Boolean) dados.get(SUBSTITUIR_CORRETOR_VAZAO);
			if (substituirCorretorVazao != null && substituirCorretorVazao) {
				this.validarDadosSubstituicaoCorretorVazao(dados);
			}
			try {
				this.inserirDadosSubstituicaoMedidorIndependente(dados);
			} catch (Exception e) {

				throw new GGASException(e);
			}
		} else if (idOperacaoMedidorIndependente.longValue() == OperacaoMedidor.CODIGO_BLOQUEIO) {
			this.validarDadosBloqueioMedidorIndependente(dados);
			this.inserirDadosBloqueioMedidorIndependente(dados);
		} else if (idOperacaoMedidorIndependente.longValue() == OperacaoMedidor.CODIGO_REATIVACAO) {
			this.validarDadosAtivacaoReativacaoMedidorIndependente(dados);
			this.inserirDadosReativacaoMedidorIndependente(dados);
		} else if (idOperacaoMedidorIndependente.longValue() == OperacaoMedidor.CODIGO_INSTALACAO_ATIVACAO) {
			this.validarDadosInstalacaoMedidor(dados);
			Boolean instalarCorretorVazao = (Boolean) dados.get(INSTALAR_CORRETOR_VAZAO);
			if (instalarCorretorVazao == null || instalarCorretorVazao) {
				this.validarDadosInstalacaoCorretorVazao(dados);
			}
			this.inserirDadosInstalacaoMedidorIndependente(dados);

			this.validarDadosAtivacaoReativacaoMedidorIndependente(dados);
			this.inserirDadosAtivacaoMedidorIndependente(dados);
		}
	}

	/**
	 * Inserir dados instalacao corretor vazao.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	private void inserirDadosInstalacaoCorretorVazao(Map<String, Object> dados) throws NegocioException, ConcorrenciaException {

		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorVazaoCorretor controladorVazaoCorretor = (ControladorVazaoCorretor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorVazaoCorretor.BEAN_ID_CONTROLADOR_VAZAO_CORRETOR);
		ControladorInstalacaoMedidor controladorInstalacaoMedidor = (ControladorInstalacaoMedidor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorInstalacaoMedidor.BEAN_ID_CONTROLADOR_INSTALACAO_MEDIDOR);
		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
		ControladorIntegracao controladorIntegracao =
				(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		// ********************** Dados
		// ***********************
		VazaoCorretor vazaoCorretor = (VazaoCorretor) controladorVazaoCorretor.obter((Long) dados.get(CHAVE_CORRETOR_VAZAO_ATUAL));

		Date data = (Date) dados.get(DATA_CORRETOR_VAZAO);
		String hora = (String) dados.get(HORA_CORRETOR_VAZAO);
		BigDecimal leituraAtualCorretorVazao = new BigDecimal((String) dados.get(LEITURA_ATUAL_CORRETOR_VAZAO));

		Date dataHora = this.obterDataHoraMedidorCorretorVazao(data, hora);

		MedidorLocalInstalacao localInstalacaoCorretorVazao =
				(MedidorLocalInstalacao) controladorInstalacaoMedidor.criarMedidorLocalInstalacao();
		localInstalacaoCorretorVazao.setChavePrimaria((Long) dados.get(LOCAL_INSTALACAO_CORRETOR_VAZAO));

		Funcionario funcionarioCorretorVazao = (Funcionario) controladorFuncionario.criar();
		funcionarioCorretorVazao.setChavePrimaria((Long) dados.get(FUNCIONARIO_CORRETOR_VAZAO));

		MotivoOperacaoMedidor motivoOperacaoMedidor =
				(MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);

		OperacaoMedidor operacaoMedidor = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_INSTALACAO, OperacaoMedidorImpl.class);

		PontoConsumo pontoConsumo = (PontoConsumo) dados.get(HistoricoOperacaoMedidor.PONTO_CONSUMO);
		if (pontoConsumo == null) {
			pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO));
		}

		InstalacaoMedidor instalacaoMedidor = pontoConsumo.getInstalacaoMedidor();

		instalacaoMedidor.setDataCorretorVazao(data);
		instalacaoMedidor.setVazaoCorretor(vazaoCorretor);

		Long idVazaoCorretorOperacaoHistorico = this.inserirHistoricoOperacaoCorretorVazao(vazaoCorretor,
				instalacaoMedidor.getPontoConsumo(), instalacaoMedidor, operacaoMedidor, localInstalacaoCorretorVazao,
				leituraAtualCorretorVazao, dataHora, funcionarioCorretorVazao, motivoOperacaoMedidor);

		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(instalacaoMedidor.getPontoConsumo());

		if (contratoPontoConsumo == null) {
			contratoPontoConsumo = controladorContrato
					.consultarContratoPontoConsumoPorPontoConsumoRecente(instalacaoMedidor.getPontoConsumo().getChavePrimaria());
		}

		if (contratoPontoConsumo != null) {

			TipoMedicao tipoMedicaoDiaria = this.criarTipoMedicao();
			tipoMedicaoDiaria.setChavePrimaria(TipoMedicao.CODIGO_DIARIA);

			contratoPontoConsumo.setTipoMedicao(tipoMedicaoDiaria);
			contratoPontoConsumo.setDadosAuditoria((DadosAuditoria) dados.get(DADOS_AUDITORIA));

			getHibernateTemplate().getSessionFactory().getCurrentSession().clear();
			controladorContrato.atualizarContratoPontoConsumo(contratoPontoConsumo);

		}

		vazaoCorretor.setSituacaoMedidor(operacaoMedidor.getSituacaoMedidor());
		ParametroSistema codigoDestinoInstalacao = controladorParametroSistema.obterParametroPorCodigo(Constantes.P_LOCAL_BEM_INSTALACAO);
		vazaoCorretor.setLocalArmazenagem((MedidorLocalArmazenagem) ServiceLocator.getInstancia().getBeanPorIDeSeteChavePrimaria(
				MedidorLocalArmazenagem.BEAN_ID_MEDIDOR_LOCAL_ARMAZENAGEM, Long.valueOf(codigoDestinoInstalacao.getValor())));
		controladorVazaoCorretor.atualizar(vazaoCorretor);

		ConstanteSistema constanteMovimentacaoBens =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_INTEGRACAO_MOVIMENTACAO_BENS);

		List<String> listaSistemasIntegrantes =
				controladorIntegracao.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteMovimentacaoBens.getValor()));

		if (!listaSistemasIntegrantes.isEmpty()) {
			controladorIntegracao.gerarMovimentacaoVazaoCorretor(vazaoCorretor.getChavePrimaria(), idVazaoCorretorOperacaoHistorico,
					instalacaoMedidor.getPontoConsumo(), true);
		}
	}

	/**
	 * Inserir dados instalacao corretor vazao medidor independente.
	 *
	 * @param dados the dados
	 * @throws GGASException the GGASException
	 */
	private void inserirDadosInstalacaoCorretorVazaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		ControladorVazaoCorretor controladorVazaoCorretor = (ControladorVazaoCorretor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorVazaoCorretor.BEAN_ID_CONTROLADOR_VAZAO_CORRETOR);
		ControladorInstalacaoMedidor controladorInstalacaoMedidor = (ControladorInstalacaoMedidor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorInstalacaoMedidor.BEAN_ID_CONTROLADOR_INSTALACAO_MEDIDOR);
		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorMedidor controladorMedidor =
				(ControladorMedidor) ServiceLocator.getInstancia().getBeanPorID(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);
		ControladorIntegracao controladorIntegracao =
				(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

		// ********************** Dados
		VazaoCorretor vazaoCorretor;

		vazaoCorretor = (VazaoCorretor) controladorVazaoCorretor.obter((Long) dados.get(CHAVE_CORRETOR_VAZAO_ATUAL));

		Date data = (Date) dados.get(DATA_CORRETOR_VAZAO);
		String hora = (String) dados.get(HORA_CORRETOR_VAZAO);
		BigDecimal leituraAtualCorretorVazao = new BigDecimal((String) dados.get(LEITURA_ATUAL_CORRETOR_VAZAO));

		Date dataHora = this.obterDataHoraMedidorCorretorVazao(data, hora);

		MedidorLocalInstalacao localInstalacaoCorretorVazao =
				(MedidorLocalInstalacao) controladorInstalacaoMedidor.criarMedidorLocalInstalacao();
		localInstalacaoCorretorVazao.setChavePrimaria((Long) dados.get(LOCAL_INSTALACAO_CORRETOR_VAZAO));

		Funcionario funcionarioCorretorVazao = (Funcionario) controladorFuncionario.criar();
		funcionarioCorretorVazao.setChavePrimaria((Long) dados.get(FUNCIONARIO_CORRETOR_VAZAO));

		MotivoOperacaoMedidor motivoOperacaoMedidor =
				(MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);

		// ***************************************************

		OperacaoMedidor operacaoMedidor = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_INSTALACAO, OperacaoMedidorImpl.class);

		Medidor medidor = obterMedidor((long) dados.get(ID_MEDIDOR_INDEPENDENTE));
		InstalacaoMedidor instalacaoMedidor;
		if (dados.containsKey(SUBSTITUICAO) && (Boolean) dados.get(SUBSTITUICAO)) {
			instalacaoMedidor = (InstalacaoMedidor) dados.get(Medidor.INSTALACAO_MEDIDOR);
		} else {
			instalacaoMedidor = medidor.getInstalacaoMedidor();
		}

		instalacaoMedidor.setDataCorretorVazao(data);
		instalacaoMedidor.setVazaoCorretor(vazaoCorretor);

		Long idVazaoCorretorOperacaoHistorico =
				this.inserirHistoricoOperacaoCorretorVazao(vazaoCorretor, null, instalacaoMedidor, operacaoMedidor,
						localInstalacaoCorretorVazao, leituraAtualCorretorVazao, dataHora, funcionarioCorretorVazao, motivoOperacaoMedidor);

		vazaoCorretor.setSituacaoMedidor(operacaoMedidor.getSituacaoMedidor());
		ParametroSistema codigoDestinoInstalacao = controladorParametroSistema.obterParametroPorCodigo(Constantes.P_LOCAL_BEM_INSTALACAO);
		vazaoCorretor.setLocalArmazenagem((MedidorLocalArmazenagem) ServiceLocator.getInstancia().getBeanPorIDeSeteChavePrimaria(
				MedidorLocalArmazenagem.BEAN_ID_MEDIDOR_LOCAL_ARMAZENAGEM, Long.valueOf(codigoDestinoInstalacao.getValor())));
		controladorMedidor.atualizar(medidor);

		ConstanteSistema constanteMovimentacaoBens =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_INTEGRACAO_MOVIMENTACAO_BENS);

		List<String> listaSistemasIntegrantes =
				controladorIntegracao.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteMovimentacaoBens.getValor()));

		if (!listaSistemasIntegrantes.isEmpty()) {
			controladorIntegracao.gerarMovimentacaoVazaoCorretor(vazaoCorretor.getChavePrimaria(), idVazaoCorretorOperacaoHistorico, null,
					true);
		}

	}

	/**
	 * Inserir dados substituicao corretor vazao.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	private void inserirDadosSubstituicaoCorretorVazao(Map<String, Object> dados) throws NegocioException, ConcorrenciaException {

		this.inserirDadosRetiradaCorretorVazao(dados);
		this.inserirDadosInstalacaoCorretorVazao(dados);

	}

	/**
	 * Inserir dados substituicao corretor vazao medidor independente.
	 *
	 * @param dados the dados
	 * @throws GGASException
	 * @throws Exception the exception
	 */
	private void inserirDadosSubstituicaoCorretorVazaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		this.inserirDadosRetiradaCorretorVazaoMedidorIndependente(dados);
		this.inserirDadosInstalacaoCorretorVazaoMedidorIndependente(dados);

	}

	/**
	 * Inserir dados retirada corretor vazao.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	private void inserirDadosRetiradaCorretorVazao(Map<String, Object> dados) throws NegocioException, ConcorrenciaException {

		ControladorVazaoCorretor controladorVazaoCorretor = (ControladorVazaoCorretor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorVazaoCorretor.BEAN_ID_CONTROLADOR_VAZAO_CORRETOR);
		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
		ControladorContrato controladorContrato =
				(ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorIntegracao controladorIntegracao =
				(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

		// ********************** Dados
		VazaoCorretor vazaoCorretor = (VazaoCorretor) controladorVazaoCorretor.obter((Long) dados.get(CHAVE_CORRETOR_VAZAO_ANTERIOR));

		Date data = (Date) dados.get(DATA_CORRETOR_VAZAO);
		String hora = (String) dados.get(HORA_CORRETOR_VAZAO);

		Date dataHora = this.obterDataHoraMedidorCorretorVazao(data, hora);

		Funcionario funcionarioCorretorVazao = (Funcionario) controladorFuncionario.criar();
		funcionarioCorretorVazao.setChavePrimaria((Long) dados.get(FUNCIONARIO_CORRETOR_VAZAO));

		MotivoOperacaoMedidor motivoOperacaoMedidor =
				(MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);
		// ***************************************************

		vazaoCorretor.setSituacaoMedidor(motivoOperacaoMedidor.getSituacaoMedidor());
		vazaoCorretor.setLocalArmazenagem(buscarCodigoLocalArmazenagemRetirada());
		controladorVazaoCorretor.atualizar(vazaoCorretor);

		OperacaoMedidor operacaoMedidor =
				(OperacaoMedidor) this.obter((Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR), OperacaoMedidorImpl.class);

		PontoConsumo pontoConsumo =
				(PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO), Medidor.INSTALACAO_MEDIDOR);

		InstalacaoMedidor instalacaoMedidor = pontoConsumo.getInstalacaoMedidor();

		instalacaoMedidor.setDataCorretorVazao(null);
		instalacaoMedidor.setVazaoCorretor(null);

		Long idVazaoCorretorOperacaoHistorico = this.inserirHistoricoOperacaoCorretorVazao(vazaoCorretor, pontoConsumo, instalacaoMedidor,
				operacaoMedidor, null, null, dataHora, funcionarioCorretorVazao, motivoOperacaoMedidor);

		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

		if (contratoPontoConsumo == null) {
			contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(pontoConsumo.getChavePrimaria());
		}

		if (contratoPontoConsumo != null) {
			TipoMedicao tipoMedicaoPeriodica = this.criarTipoMedicao();
			tipoMedicaoPeriodica.setChavePrimaria(TipoMedicao.CODIGO_PERIODICA);

			contratoPontoConsumo.setTipoMedicao(tipoMedicaoPeriodica);
			contratoPontoConsumo.setDadosAuditoria((DadosAuditoria) dados.get(DADOS_AUDITORIA));

			getHibernateTemplate().clear();
			controladorContrato.atualizarContratoPontoConsumo(contratoPontoConsumo);
		}

		ConstanteSistema constanteMovimentacaoBens =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_INTEGRACAO_MOVIMENTACAO_BENS);

		List<String> listaSistemasIntegrantes =
				controladorIntegracao.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteMovimentacaoBens.getValor()));

		if (!listaSistemasIntegrantes.isEmpty()) {
			controladorIntegracao.gerarMovimentacaoVazaoCorretor(vazaoCorretor.getChavePrimaria(), idVazaoCorretorOperacaoHistorico,
					pontoConsumo, false);
		}

	}

	/**
	 * Inserir dados retirada corretor vazao medidor independente.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	private void inserirDadosRetiradaCorretorVazaoMedidorIndependente(Map<String, Object> dados)
			throws NegocioException, ConcorrenciaException {

		ControladorVazaoCorretor controladorVazaoCorretor = (ControladorVazaoCorretor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorVazaoCorretor.BEAN_ID_CONTROLADOR_VAZAO_CORRETOR);
		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
		ControladorMedidor controladorMedidor =
				(ControladorMedidor) ServiceLocator.getInstancia().getBeanPorID(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);
		ControladorIntegracao controladorIntegracao =
				(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

		// ********************** Dados
		VazaoCorretor vazaoCorretor = (VazaoCorretor) controladorVazaoCorretor.obter((Long) dados.get(CHAVE_CORRETOR_VAZAO_ANTERIOR));

		Date data = (Date) dados.get(DATA_CORRETOR_VAZAO);
		String hora = (String) dados.get(HORA_CORRETOR_VAZAO);

		Date dataHora = this.obterDataHoraMedidorCorretorVazao(data, hora);

		Funcionario funcionarioCorretorVazao = (Funcionario) controladorFuncionario.criar();
		funcionarioCorretorVazao.setChavePrimaria((Long) dados.get(FUNCIONARIO_CORRETOR_VAZAO));

		MotivoOperacaoMedidor motivoOperacaoMedidor =
				(MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);
		// ***************************************************

		vazaoCorretor.setSituacaoMedidor(motivoOperacaoMedidor.getSituacaoMedidor());
		vazaoCorretor.setLocalArmazenagem(buscarCodigoLocalArmazenagemRetirada());
		controladorVazaoCorretor.atualizar(vazaoCorretor);

		OperacaoMedidor operacaoMedidor =
				(OperacaoMedidor) this.obter((Long) dados.get(Medidor.ID_OPERACAO_MEDIDOR_INDEPENDENTE), OperacaoMedidorImpl.class);
		Medidor medidor = (Medidor) controladorMedidor.obter((long) dados.get(ID_MEDIDOR_INDEPENDENTE));
		InstalacaoMedidor instalacaoMedidor;
		if (dados.containsKey(SUBSTITUICAO) && (Boolean) dados.get(SUBSTITUICAO)) {
			instalacaoMedidor = (InstalacaoMedidor) dados.get(Medidor.INSTALACAO_MEDIDOR);
		} else {
			instalacaoMedidor = medidor.getInstalacaoMedidor();
		}

		instalacaoMedidor.setDataCorretorVazao(null);
		instalacaoMedidor.setVazaoCorretor(null);

		Long idVazaoCorretorOperacaoHistorico = this.inserirHistoricoOperacaoCorretorVazao(vazaoCorretor, null, instalacaoMedidor,
				operacaoMedidor, null, null, dataHora, funcionarioCorretorVazao, motivoOperacaoMedidor);

		ConstanteSistema constanteMovimentacaoBens =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_INTEGRACAO_MOVIMENTACAO_BENS);

		List<String> listaSistemasIntegrantes =
				controladorIntegracao.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteMovimentacaoBens.getValor()));

		if (!listaSistemasIntegrantes.isEmpty()) {
			controladorIntegracao.gerarMovimentacaoVazaoCorretor(vazaoCorretor.getChavePrimaria(), idVazaoCorretorOperacaoHistorico, null,
					false);
		}
	}

	/**
	 * Inserir dados instalacao medidor.
	 *
	 * @param dados the dados
	 * @param pontoConsumo 
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	private void inserirDadosInstalacaoMedidor(Map<String, Object> dados, PontoConsumo pontoConsumo) throws NegocioException, ConcorrenciaException {

		ControladorInstalacaoMedidor controladorInstalacaoMedidor = (ControladorInstalacaoMedidor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorInstalacaoMedidor.BEAN_ID_CONTROLADOR_INSTALACAO_MEDIDOR);
		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
		ControladorImovel controladorImovel =
				(ControladorImovel) ServiceLocator.getInstancia().getBeanPorID(ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
		ControladorIntegracao controladorIntegracao =
				(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		

		// ********************** Dados
		// ***********************
		Date data = (Date) dados.get(DATA_MEDIDOR);

		BigDecimal leitura = null;
		if (dados.get(LEITURA_ATUAL) != null) {
			leitura = BigDecimal.valueOf(Double.valueOf((String) dados.get(LEITURA_ATUAL)));
		}

		MedidorLocalInstalacao localInstalacaoMedidor = null;
		if (dados.get(HistoricoOperacaoMedidor.LOCAL_INSTALACAO_MEDIDOR) != null) {
			localInstalacaoMedidor = (MedidorLocalInstalacao) controladorInstalacaoMedidor.criarMedidorLocalInstalacao();
			localInstalacaoMedidor.setChavePrimaria((Long) dados.get(HistoricoOperacaoMedidor.LOCAL_INSTALACAO_MEDIDOR));
		}

		MedidorProtecao medidorProtecao = null;
		if (dados.get(MEDIDOR_PROTECAO) != null) {
			medidorProtecao = (MedidorProtecao) controladorInstalacaoMedidor.criarMedidorProtecao();
			medidorProtecao.setChavePrimaria((Long) dados.get(MEDIDOR_PROTECAO));
		}

		Funcionario funcionario = (Funcionario) controladorFuncionario.criar();
		funcionario.setChavePrimaria((Long) dados.get(InstalacaoMedidor.FUNCIONARIO));

		MotivoOperacaoMedidor motivoOperacaoMedidor =
				(MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);
		// ***************************************************

		Medidor medidor = this.obterMedidor((Long) dados.get(CHAVE_MEDIDOR_ATUAL));

		InstalacaoMedidor instalacaoMedidor = (InstalacaoMedidor) controladorInstalacaoMedidor.criar();
		instalacaoMedidor.setMedidor(medidor);
		instalacaoMedidor.setPontoConsumo(pontoConsumo);
		instalacaoMedidor.setData(data);

		instalacaoMedidor.setLocalInstalacao(localInstalacaoMedidor);
		instalacaoMedidor.setProtecao(medidorProtecao);
		instalacaoMedidor.setLeitura(leitura);

		if (dados.containsKey(SUBSTITUICAO) && (Boolean) dados.get(SUBSTITUICAO)) {

			Criteria criteria = this.createCriteria(InstalacaoMedidor.class);
			criteria.setProjection(Projections.max(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA));
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, pontoConsumo.getChavePrimaria()));
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));

			Long id = (Long) criteria.uniqueResult();

			if (id != null) {

				criteria = this.createCriteria(InstalacaoMedidor.class);
				criteria.createAlias(VAZAO_CORRETOR, VAZAO_CORRETOR, Criteria.LEFT_JOIN);
				criteria.add(Restrictions.idEq(id));

				InstalacaoMedidor ultimaInstalacaoMedidor = (InstalacaoMedidor) criteria.uniqueResult();

				if (ultimaInstalacaoMedidor != null) {
					instalacaoMedidor.setVazaoCorretor(ultimaInstalacaoMedidor.getVazaoCorretor());
					instalacaoMedidor.setDataCorretorVazao(ultimaInstalacaoMedidor.getDataCorretorVazao());
				}
			}
		}

		controladorInstalacaoMedidor.inserir(instalacaoMedidor);

		pontoConsumo.setInstalacaoMedidor(instalacaoMedidor);
		
		OperacaoMedidor operacaoMedidor = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_INSTALACAO, OperacaoMedidorImpl.class);

		Long idMedidorOperacaoHistorico = this.inserirHistoricoOperacaoMedidor(operacaoMedidor, data, data, medidor, localInstalacaoMedidor,
				leitura, funcionario, motivoOperacaoMedidor, pontoConsumo, instalacaoMedidor, null, null, null, Boolean.FALSE);

		SituacaoImovel situacaoInterligado = (SituacaoImovel) controladorImovel.criarSituacaoImovel();
		situacaoInterligado.setChavePrimaria(SituacaoImovel.SITUACAO_INTERLIGADO);

		Imovel imovel = pontoConsumo.getImovel();
		imovel.setSituacaoImovel(situacaoInterligado);
		imovel.setDadosAuditoria((DadosAuditoria) dados.get(DADOS_AUDITORIA));

		controladorImovel.atualizar(imovel);

		medidor.setSituacaoMedidor(operacaoMedidor.getSituacaoMedidor());
		ParametroSistema codigoDestinoInstalacao = controladorParametroSistema.obterParametroPorCodigo(Constantes.P_LOCAL_BEM_INSTALACAO);
		medidor.setLocalArmazenagem((MedidorLocalArmazenagem) ServiceLocator.getInstancia().getBeanPorIDeSeteChavePrimaria(
				MedidorLocalArmazenagem.BEAN_ID_MEDIDOR_LOCAL_ARMAZENAGEM, Long.valueOf(codigoDestinoInstalacao.getValor())));
		this.atualizar(medidor);

		Boolean instalarCorretorVazao = (Boolean) dados.get(INSTALAR_CORRETOR_VAZAO);
		if (instalarCorretorVazao != null && instalarCorretorVazao) {
			this.inserirDadosInstalacaoCorretorVazao(dados);
		}

		ConstanteSistema constanteMovimentacaoBens =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_INTEGRACAO_MOVIMENTACAO_BENS);

		List<String> listaSistemasIntegrantes =
				controladorIntegracao.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteMovimentacaoBens.getValor()));

		if (!listaSistemasIntegrantes.isEmpty()) {
			controladorIntegracao.gerarMovimentacaoMedidor(medidor.getChavePrimaria(), idMedidorOperacaoHistorico, pontoConsumo, true);
		}
		
	}

	/**
	 * Inserir dados instalacao medidor independente.
	 *
	 * @param dados the dados
	 * @throws Exception the exception
	 */
	private void inserirDadosInstalacaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		ControladorInstalacaoMedidor controladorInstalacaoMedidor = (ControladorInstalacaoMedidor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorInstalacaoMedidor.BEAN_ID_CONTROLADOR_INSTALACAO_MEDIDOR);
		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
		ControladorIntegracao controladorIntegracao =
				(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
		ControladorParametroSistema controladorParametroSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		// ********************** Dados
		// ***********************
		Date data = (Date) dados.get(DATA_MEDIDOR);

		BigDecimal leitura = BigDecimal.valueOf(Double.valueOf((String) dados.get(LEITURA_ATUAL)));

		MedidorLocalInstalacao localInstalacaoMedidor = (MedidorLocalInstalacao) controladorInstalacaoMedidor.criarMedidorLocalInstalacao();
		localInstalacaoMedidor.setChavePrimaria((Long) dados.get(HistoricoOperacaoMedidor.LOCAL_INSTALACAO_MEDIDOR));

		MedidorProtecao medidorProtecao = (MedidorProtecao) controladorInstalacaoMedidor.criarMedidorProtecao();
		medidorProtecao.setChavePrimaria((Long) dados.get(MEDIDOR_PROTECAO));

		Funcionario funcionario = (Funcionario) controladorFuncionario.criar();
		funcionario.setChavePrimaria((Long) dados.get(InstalacaoMedidor.FUNCIONARIO));

		MotivoOperacaoMedidor motivoOperacaoMedidor =
				(MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);
		// ***************************************************

		Medidor medidor = instanciaMedidorPorOperacaoMedidor(dados);

		InstalacaoMedidor instalacaoMedidor;

		if (dados.containsKey(SUBSTITUICAO) && (Boolean) dados.get(SUBSTITUICAO)) {
			instalacaoMedidor = (InstalacaoMedidor) dados.get(Medidor.INSTALACAO_MEDIDOR);
		} else {
			instalacaoMedidor = (InstalacaoMedidor) controladorInstalacaoMedidor.criar();
		}
		instalacaoMedidor.setMedidor(medidor);
		instalacaoMedidor.setData(data);
		instalacaoMedidor.setLocalInstalacao(localInstalacaoMedidor);
		instalacaoMedidor.setProtecao(medidorProtecao);
		instalacaoMedidor.setLeitura(leitura);

		if (dados.containsKey(SUBSTITUICAO) && (Boolean) dados.get(SUBSTITUICAO)) {
			controladorInstalacaoMedidor.atualizar(instalacaoMedidor);
		} else {
			controladorInstalacaoMedidor.inserir(instalacaoMedidor);
		}

		dados.put(Medidor.INSTALACAO_MEDIDOR, instalacaoMedidor);

		OperacaoMedidor operacaoMedidor = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_INSTALACAO, OperacaoMedidorImpl.class);

		Long idMedidorOperacaoHistorico = inserirHistoricoOperacaoMedidor(operacaoMedidor, data, data, medidor, localInstalacaoMedidor,
				leitura, funcionario, motivoOperacaoMedidor, null, instalacaoMedidor, null, null, null, Boolean.FALSE);

		medidor.setSituacaoMedidor(operacaoMedidor.getSituacaoMedidor());
		ParametroSistema codigoDestinoInstalacao = controladorParametroSistema.obterParametroPorCodigo(Constantes.P_LOCAL_BEM_INSTALACAO);
		medidor.setLocalArmazenagem((MedidorLocalArmazenagem) ServiceLocator.getInstancia().getBeanPorIDeSeteChavePrimaria(
				MedidorLocalArmazenagem.BEAN_ID_MEDIDOR_LOCAL_ARMAZENAGEM, Long.valueOf(codigoDestinoInstalacao.getValor())));
		medidor.setInstalacaoMedidor(instalacaoMedidor);
		this.atualizar(medidor);

		dados.put("medidorIndependente", medidor);

		Boolean instalarCorretorVazao = (Boolean) dados.get(INSTALAR_CORRETOR_VAZAO);
		if (instalarCorretorVazao != null && instalarCorretorVazao) {
			this.inserirDadosInstalacaoCorretorVazaoMedidorIndependente(dados);
		}

		ConstanteSistema constanteMovimentacaoBens =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_INTEGRACAO_MOVIMENTACAO_BENS);

		List<String> listaSistemasIntegrantes =
				controladorIntegracao.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteMovimentacaoBens.getValor()));

		if (!listaSistemasIntegrantes.isEmpty()) {
			controladorIntegracao.gerarMovimentacaoMedidor(medidor.getChavePrimaria(), idMedidorOperacaoHistorico, null, true);
		}
	}

	/**
	 * Inserir dados substituicao medidor.
	 *
	 * @param dados the dados
	 * @throws GGASException the exception
	 */
	private void inserirDadosSubstituicaoMedidor(Map<String, Object> dados, PontoConsumo pontoConsumo) throws GGASException {

		dados.put(SUBSTITUICAO, Boolean.TRUE);

		this.inserirDadosRetiradaMedidor(dados, pontoConsumo);
		this.inserirDadosInstalacaoMedidor(dados, pontoConsumo);

		dados.put(LEITURA_ANTERIOR, dados.get(LEITURA_ATUAL));
		dados.put(CHAVE_MEDIDOR_ANTERIOR, dados.get(CHAVE_MEDIDOR_ATUAL));

		this.inserirDadosAtivacaoMedidor(dados, pontoConsumo);

		Boolean substituirCorretorVazao = (Boolean) dados.get(SUBSTITUIR_CORRETOR_VAZAO);
		if (substituirCorretorVazao != null && substituirCorretorVazao) {
			this.inserirDadosSubstituicaoCorretorVazao(dados);
		}
	}

	/**
	 * Inserir dados substituicao medidor independente.
	 *
	 * @param dados the dados
	 * @throws GGASException the exception
	 */
	private void inserirDadosSubstituicaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		dados.put(SUBSTITUICAO, Boolean.TRUE);

		boolean ativarMedidor = medidorIndependenteAnteriorEstavaAtivo(dados);

		this.inserirDadosRetiradaMedidorIndependente(dados);
		this.inserirDadosInstalacaoMedidorIndependente(dados);

		dados.put(LEITURA_ANTERIOR, dados.get(LEITURA_ATUAL));
		dados.put(ID_MEDIDOR_INDEPENDENTE, dados.get(CHAVE_MEDIDOR_ATUAL));

		if (ativarMedidor) {
			this.inserirDadosAtivacaoMedidorIndependente(dados);
		}

		Boolean substituirCorretorVazao = (Boolean) dados.get(SUBSTITUIR_CORRETOR_VAZAO);
		if (substituirCorretorVazao != null && substituirCorretorVazao) {
			this.inserirDadosSubstituicaoCorretorVazaoMedidorIndependente(dados);
		}
	}

	/**
	 * Medidor independente anterior estava ativo.
	 *
	 * @param dados the dados
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 * @throws GGASException the GGAS exception
	 */
	private boolean medidorIndependenteAnteriorEstavaAtivo(Map<String, Object> dados) throws GGASException {

		Medidor medidor = obterMedidor((Long) dados.get(ID_MEDIDOR_INDEPENDENTE));

		long idSituacaoAssociacaoMI = medidor.getSituacaoAssociacaoMedidorIndependente().getChavePrimaria();

		long idSituacaoAssociacaoMIAtivo =
				Long.parseLong(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_ATIVO));
		if (idSituacaoAssociacaoMI == idSituacaoAssociacaoMIAtivo) {
			return true;
		}
		return false;
	}

	/**
	 * Inserir dados retirada medidor.
	 *
	 * @param dados the dados
	 * @param pontoConsumo 
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	private void inserirDadosRetiradaMedidor(Map<String, Object> dados, PontoConsumo pontoConsumo) throws NegocioException, ConcorrenciaException {

		ControladorImovel controladorImovel =
				(ControladorImovel) ServiceLocator.getInstancia().getBeanPorID(ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
		ControladorIntegracao controladorIntegracao =
				(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

		// ********************** Dados
		// ***********************
		Date data = (Date) dados.get(DATA_MEDIDOR);

		Object leituraAnterior = dados.get(LEITURA_ANTERIOR);
		BigDecimal leitura = null;
		if (leituraAnterior != null) {
			leitura = BigDecimal.valueOf(Double.valueOf((String) leituraAnterior));
		}

		Funcionario funcionario = (Funcionario) controladorFuncionario.criar();
		funcionario.setChavePrimaria((Long) dados.get(InstalacaoMedidor.FUNCIONARIO));

		MotivoOperacaoMedidor motivoOperacaoMedidor =
				(MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);
		// ***************************************************

		InstalacaoMedidor instalacaoMedidor = pontoConsumo.getInstalacaoMedidor();
		if (instalacaoMedidor != null && instalacaoMedidor.getVazaoCorretor() != null
				&& (dados.get(SUBSTITUICAO) != null && Boolean.FALSE.equals(dados.get(SUBSTITUICAO)) || dados.get(SUBSTITUICAO) == null)) {

			this.inserirDadosRetiradaCorretorVazao(dados);
		}

		pontoConsumo.setInstalacaoMedidor(null);
		
		SituacaoConsumo situacaoConsumoAguardandoAtivacao = (SituacaoConsumo) getControladorPontoConsumo().criarSituacaoConsumo();

		situacaoConsumoAguardandoAtivacao.setChavePrimaria(Long.parseLong(
				getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO)));
		
		Long idOperacaoMedidor = (Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR);
		if(idOperacaoMedidor.longValue() != OperacaoMedidor.CODIGO_SUBSTITUICAO) {
			pontoConsumo.setSituacaoConsumo(situacaoConsumoAguardandoAtivacao);
		}
		
		

		// atualiza o imovel para factivel apos a retirada do medidor
		Imovel imovel = pontoConsumo.getImovel();
		imovel.setSituacaoImovel(controladorImovel.obterSituacaoImovel(
				Long.parseLong(getControladorConstante().obterConstantePorCodigo(Constantes.C_IMOVEL_FACTIVEL).getValor())));
		controladorImovel.atualizar(imovel);

		Medidor medidor = this.obterMedidor((Long) dados.get(CHAVE_MEDIDOR_ANTERIOR));

		medidor.setSituacaoMedidor(motivoOperacaoMedidor.getSituacaoMedidor());

		medidor.setLocalArmazenagem(buscarCodigoLocalArmazenagemRetirada());

		this.atualizar(medidor);

		OperacaoMedidor operacaoMedidor =
				(OperacaoMedidor) this.obter((Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR), OperacaoMedidorImpl.class);

		Long idMedidorOperacaoHistorico = this.inserirHistoricoOperacaoMedidor(operacaoMedidor, data, data, medidor, null, leitura,
				funcionario, motivoOperacaoMedidor, pontoConsumo, instalacaoMedidor, null, null, null, Boolean.FALSE);

		ConstanteSistema constanteMovimentacaoBens =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_INTEGRACAO_MOVIMENTACAO_BENS);

		List<String> listaSistemasIntegrantes =
				controladorIntegracao.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteMovimentacaoBens.getValor()));

		if (!listaSistemasIntegrantes.isEmpty()) {
			controladorIntegracao.gerarMovimentacaoMedidor(medidor.getChavePrimaria(), idMedidorOperacaoHistorico, pontoConsumo, false);
		}
		
	}

	/**
	 * Inserir dados retirada medidor independente.
	 *
	 * @param dados the dados
	 * @throws GGASException the GGASException
	 */
	private void inserirDadosRetiradaMedidorIndependente(Map<String, Object> dados) throws GGASException {

		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
		ControladorIntegracao controladorIntegracao =
				(ControladorIntegracao) ServiceLocator.getInstancia().getBeanPorID(ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

		// ********************** Dados
		// ***********************
		Date data = (Date) dados.get(DATA_MEDIDOR);

		BigDecimal leitura = BigDecimal.valueOf(Double.valueOf((String) dados.get(LEITURA_ANTERIOR)));

		Funcionario funcionario = (Funcionario) controladorFuncionario.criar();
		funcionario.setChavePrimaria((Long) dados.get(InstalacaoMedidor.FUNCIONARIO));

		MotivoOperacaoMedidor motivoOperacaoMedidor =
				(MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);
		// ***************************************************

		Medidor medidor = obterMedidor((Long) dados.get(ID_MEDIDOR_INDEPENDENTE));

		InstalacaoMedidor instalacaoMedidor = medidor.getInstalacaoMedidor();
		if (instalacaoMedidor != null && instalacaoMedidor.getVazaoCorretor() != null
				&& (dados.get(SUBSTITUICAO) != null && Boolean.FALSE.equals(dados.get(SUBSTITUICAO)) || dados.get(SUBSTITUICAO) == null)) {

			this.inserirDadosRetiradaCorretorVazaoMedidorIndependente(dados);
		}

		// Para ser usado em caso de substituição do medidor
		dados.put(Medidor.INSTALACAO_MEDIDOR, medidor.getInstalacaoMedidor());

		medidor.setInstalacaoMedidor(null);

		EntidadeConteudo situacaoAssociacaoMedidorIndependente = fachada.obterEntidadeConteudo(Long.parseLong(
				fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_AGUARDANDO_ATIVACAO)));
		medidor.setSituacaoAssociacaoMedidorIndependente(situacaoAssociacaoMedidorIndependente);

		medidor.setSituacaoMedidor(motivoOperacaoMedidor.getSituacaoMedidor());

		medidor.setLocalArmazenagem(buscarCodigoLocalArmazenagemRetirada());

		this.atualizar(medidor);

		OperacaoMedidor operacaoMedidor =
				(OperacaoMedidor) this.obter((Long) dados.get(Medidor.ID_OPERACAO_MEDIDOR_INDEPENDENTE), OperacaoMedidorImpl.class);

		Long idMedidorOperacaoHistorico = this.inserirHistoricoOperacaoMedidor(operacaoMedidor, data, data, medidor, null, leitura,
				funcionario, motivoOperacaoMedidor, null, instalacaoMedidor, null, null, null, Boolean.FALSE);

		ConstanteSistema constanteMovimentacaoBens =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_INTEGRACAO_MOVIMENTACAO_BENS);

		List<String> listaSistemasIntegrantes =
				controladorIntegracao.gerarListaSistemasIntegrantesPorFuncao(Long.valueOf(constanteMovimentacaoBens.getValor()));

		if (!listaSistemasIntegrantes.isEmpty()) {
			controladorIntegracao.gerarMovimentacaoMedidor(medidor.getChavePrimaria(), idMedidorOperacaoHistorico, null, false);
		}
	}

	/**
	 * Inserir dados ativacao medidor.
	 *
	 * @param dados the dados
	 * @param pontoConsumo 
	 * @throws GGASException the GGASException
	 */
	private void inserirDadosAtivacaoMedidor(Map<String, Object> dados, PontoConsumo pontoConsumo) throws GGASException {

		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);

		ControladorInstalacaoMedidor controladorInstalacaoMedidor =
				(ControladorInstalacaoMedidor) ServiceLocator.getInstancia().getBeanPorID(ControladorInstalacaoMedidor.BEAN_ID_CONTROLADOR_INSTALACAO_MEDIDOR);

		// ********************** Dados
		// ***********************
		Date data = (Date) dados.get(DATA_MEDIDOR);

		BigDecimal leitura = null;
		if (dados.get(LEITURA_ANTERIOR) != null) {
			leitura = BigDecimal.valueOf(Double.valueOf((String) dados.get(LEITURA_ANTERIOR)));
		}

		Funcionario funcionario = (Funcionario) controladorFuncionario.criar();
		funcionario.setChavePrimaria((Long) dados.get(InstalacaoMedidor.FUNCIONARIO));
		// ***************************************************
		SituacaoConsumo situacaoConsumo = (SituacaoConsumo) getControladorPontoConsumo().criarSituacaoConsumo();
		situacaoConsumo.setChavePrimaria(
				Long.parseLong(getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_ATIVO)));
		
		Long idOperacaoMedidor = (Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR);
		if(idOperacaoMedidor.longValue() != OperacaoMedidor.CODIGO_SUBSTITUICAO) {
			pontoConsumo.setSituacaoConsumo(situacaoConsumo);
		}
		
		Medidor medidor = this.obterMedidor((Long) dados.get(CHAVE_MEDIDOR_ANTERIOR));

		OperacaoMedidor operacaoMedidor = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_ATIVACAO, OperacaoMedidorImpl.class);
		
		MotivoOperacaoMedidor motivoOperacaoMedidor = null;
		if(dados.get(MEDIDOR_MOTIVO_OPERACAO) != null) {
			motivoOperacaoMedidor = (MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);
		}

		InstalacaoMedidor instalacaoMedidor = pontoConsumo.getInstalacaoMedidor();
		instalacaoMedidor.setDataAtivacao(data);
		instalacaoMedidor.setLeituraAtivacao(leitura);
		instalacaoMedidor.setFuncionario(funcionario);
		
		this.inserirHistoricoOperacaoMedidor(operacaoMedidor, data, data, medidor, null, leitura, funcionario, motivoOperacaoMedidor, pontoConsumo,
				instalacaoMedidor, null, null, null, Boolean.FALSE);

	}

	/**
	 * Recupera medidores que compoem medidor virtual.
	 *
	 * @param medidor the medidor
	 * @return the list
	 */
	private List<Long> recuperaMedidoresQueCompoemMedidorVirtual(Medidor medidor) {

		List<Long> listaIdMedidorIndependente = new ArrayList<Long>();
		String composicaoVirtual = medidor.getComposicaoVirtual();
		if (composicaoVirtual != null) {
			String[] arrayComposicaoVirtual = composicaoVirtual.split("#");
			for (String idMedidorComOperacao : arrayComposicaoVirtual) {
				Long idMedidorIndependente = Long.valueOf(idMedidorComOperacao.substring(1));
				if (!listaIdMedidorIndependente.contains(idMedidorIndependente)) {
					listaIdMedidorIndependente.add(idMedidorIndependente);
				}
			}
		}
		return listaIdMedidorIndependente;
	}

	@Override
	public Collection<Medidor> listaMedidoresQueCompoemMedidorVirtual(Medidor medidorVirtual) throws NegocioException {

		List<Long> listaIdsMedidorIndependente = recuperaMedidoresQueCompoemMedidorVirtual(medidorVirtual);
		Long[] listaChaveMedidorIndependente = new Long[listaIdsMedidorIndependente.size()];
		int count = 0;
		for (Long idMedidor : listaIdsMedidorIndependente) {
			listaChaveMedidorIndependente[count] = idMedidor;
			count++;
		}
		Map<String, Object> filtro = new HashMap<String, Object>();

		filtro.put(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS, listaChaveMedidorIndependente);

		return consultarMedidor(filtro);
	}

	/**
	 * Inserir dados ativacao medidor independente.
	 *
	 * @param dados the dados
	 * @throws GGASException the GGASException
	 */
	private void inserirDadosAtivacaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		Date data = (Date) dados.get(DATA_MEDIDOR);

		Medidor medidor = this.obterMedidor((Long) dados.get(ID_MEDIDOR_INDEPENDENTE));

		EntidadeConteudo situacaoAssociacaoMedidorIndependente = fachada.obterEntidadeConteudo(
				Long.parseLong(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_ATIVO)));
		medidor.setSituacaoAssociacaoMedidorIndependente(situacaoAssociacaoMedidorIndependente);

		OperacaoMedidor operacaoMedidor = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_ATIVACAO, OperacaoMedidorImpl.class);
		
		MotivoOperacaoMedidor motivoOperacaoMedidor = null;
		if(dados.get(MEDIDOR_MOTIVO_OPERACAO) != null) {
			motivoOperacaoMedidor = (MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);
		}
		
		InstalacaoMedidor instalacaoMedidor = medidor.getInstalacaoMedidor();

		instalacaoMedidor.setDataAtivacao(data);

		atualizar(medidor);

		this.inserirHistoricoOperacaoMedidor(operacaoMedidor, data, data, medidor, null, instalacaoMedidor.getLeitura(),
				instalacaoMedidor.getFuncionario(), motivoOperacaoMedidor, null, instalacaoMedidor, null, null, null, Boolean.FALSE);

	}

	/**
	 * Inserir historico operacao medidor.
	 *
	 * @param operacaoMedidor the operacao medidor
	 * @param dataPlanejada the data planejada
	 * @param dataRealizada the data realizada
	 * @param medidor the medidor
	 * @param localInstalacaoMedidor the local instalacao medidor
	 * @param leitura the leitura
	 * @param funcionario the funcionario
	 * @param motivoOperacaoMedidor the motivo operacao medidor
	 * @param pontoConsumo the ponto consumo
	 * @param instalacaoMedidor the instalacao medidor
	 * @param lacre the lacre
	 * @return the long
	 * @throws NegocioException the negocio exception
	 */
	private Long inserirHistoricoOperacaoMedidor(OperacaoMedidor operacaoMedidor, Date dataPlanejada, Date dataRealizada, Medidor medidor,
			MedidorLocalInstalacao localInstalacaoMedidor, BigDecimal leitura, Funcionario funcionario,
			MotivoOperacaoMedidor motivoOperacaoMedidor, PontoConsumo pontoConsumo, InstalacaoMedidor instalacaoMedidor, String lacre, String lacreDois, String lacreTres,  Boolean isOperacaoConjunta)
			throws NegocioException {

		HistoricoOperacaoMedidor historicoOperacaoMedidor = this.criarHistoricoOperacaoMedidor();
		historicoOperacaoMedidor.setOperacaoMedidor(operacaoMedidor);
		historicoOperacaoMedidor.setDataPlanejada(dataPlanejada);
		historicoOperacaoMedidor.setDataRealizada(dataRealizada);
		historicoOperacaoMedidor.setMedidor(medidor);
		historicoOperacaoMedidor.setMedidorLocalInstalacao(localInstalacaoMedidor);
		historicoOperacaoMedidor.setDescricao(operacaoMedidor.getDescricao());
		historicoOperacaoMedidor.setNumeroLeitura(leitura);
		historicoOperacaoMedidor.setFuncionario(funcionario);
		historicoOperacaoMedidor.setMotivoOperacaoMedidor(motivoOperacaoMedidor);
		historicoOperacaoMedidor.setPontoConsumo(pontoConsumo);
		historicoOperacaoMedidor.setInstalacaoMedidor(instalacaoMedidor);
		historicoOperacaoMedidor.setLacre(lacre);
		historicoOperacaoMedidor.setLacreDois(lacreDois);
		historicoOperacaoMedidor.setLacreTres(lacreTres);
		historicoOperacaoMedidor.setIndicadorOperacaoConjunta(isOperacaoConjunta);

		return super.inserir(historicoOperacaoMedidor);
	}

	/**
	 * Inserir historico operacao corretor vazao.
	 *
	 * @param vazaoCorretor the vazao corretor
	 * @param pontoConsumo the ponto consumo
	 * @param instalacaoMedidor the instalacao medidor
	 * @param operacaoMedidor the operacao medidor
	 * @param localInstalacaoCorretorVazao the local instalacao corretor vazao
	 * @param leituraAtualCorretorVazao the leitura atual corretor vazao
	 * @param dataHora the data hora
	 * @param funcionarioCorretorVazao the funcionario corretor vazao
	 * @param motivoOperacaoMedidor the motivo operacao medidor
	 * @return the long
	 * @throws NegocioException the negocio exception
	 */
	private Long inserirHistoricoOperacaoCorretorVazao(VazaoCorretor vazaoCorretor, PontoConsumo pontoConsumo,
			InstalacaoMedidor instalacaoMedidor, OperacaoMedidor operacaoMedidor, MedidorLocalInstalacao localInstalacaoCorretorVazao,
			BigDecimal leituraAtualCorretorVazao, Date dataHora, Funcionario funcionarioCorretorVazao,
			MotivoOperacaoMedidor motivoOperacaoMedidor) throws NegocioException {

		VazaoCorretorHistoricoOperacao vazaoCorretorHistoricoOperacao = this.criarVazaoCorretorHistoricoOperacao();
		vazaoCorretorHistoricoOperacao.setVazaoCorretor(vazaoCorretor);
		vazaoCorretorHistoricoOperacao.setPontoConsumo(pontoConsumo);
		vazaoCorretorHistoricoOperacao.setMedidor(instalacaoMedidor.getMedidor());
		vazaoCorretorHistoricoOperacao.setOperacaoMedidor(operacaoMedidor);
		vazaoCorretorHistoricoOperacao.setLocalInstalacao(localInstalacaoCorretorVazao);
		vazaoCorretorHistoricoOperacao.setLeitura(leituraAtualCorretorVazao);
		vazaoCorretorHistoricoOperacao.setDataRealizada(dataHora);
		vazaoCorretorHistoricoOperacao.setFuncionario(funcionarioCorretorVazao);
		vazaoCorretorHistoricoOperacao.setMotivoOperacaoMedidor(motivoOperacaoMedidor);
		vazaoCorretorHistoricoOperacao.setInstalacaoMedidor(instalacaoMedidor);

		return super.inserir(vazaoCorretorHistoricoOperacao);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#validarDadosAssociacaoPontoConsumoMedidor(java.util.Map)
	 */
	@Override
	public void validarDadosAssociacaoPontoConsumoMedidor(Map<String, Object> dados) throws GGASException {

		Long idOperacaoMedidor = (Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR);

		if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_INSTALACAO) {
			this.validarDadosInstalacaoMedidor(dados);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_SUBSTITUICAO) {
			this.validarDadosSubstituicaoMedidor(dados);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_ATIVACAO) {
			this.validarDadosAtivacaoMedidor(dados);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_RETIRADA) {
			this.validarDadosRetiradaMedidor(dados);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#verificarNecessidadeAssociacaoPontoConsumoCorretorVazao(java.util.Map)
	 */
	@Override
	public boolean verificarNecessidadeAssociacaoPontoConsumoCorretorVazao(Map<String, Object> dados) throws NegocioException {

		boolean retorno = Boolean.TRUE;
		Long idOperacaoMedidor = (Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR);

		if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_INSTALACAO) {
			retorno = this.verificarNecessidadeInstalacaoCorretorVazao(dados);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_SUBSTITUICAO) {
			retorno = this.verificarNecessidadeSubstituicaoCorretorVazao(dados);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_RETIRADA) {
			retorno = this.verificarNecessidadeRetiradaCorretorVazao(dados);
		}

		return retorno;
	}

	/**
	 * Verificar necessidade instalacao corretor vazao.
	 *
	 * @param dados the dados
	 * @return true, if successful
	 */
	private boolean verificarNecessidadeInstalacaoCorretorVazao(Map<String, Object> dados) {

		Boolean instalarCorretorVazao = (Boolean) dados.get(INSTALAR_CORRETOR_VAZAO);
		if (instalarCorretorVazao != null && !instalarCorretorVazao) {
			return false;
		}

		return true;
	}

	/**
	 * Verificar necessidade substituicao corretor vazao.
	 *
	 * @param dados the dados
	 * @return true, if successful
	 */
	private boolean verificarNecessidadeSubstituicaoCorretorVazao(Map<String, Object> dados) {

		Boolean contemCorretorVazao = (Boolean) dados.get(CONTEM_CORRETOR_VAZAO);
		Boolean substituirCorretorVazao = (Boolean) dados.get(SUBSTITUIR_CORRETOR_VAZAO);

		if ((substituirCorretorVazao != null && !substituirCorretorVazao) || (contemCorretorVazao != null && !contemCorretorVazao)) {
			return false;
		}

		return true;
	}

	/**
	 * Verificar necessidade retirada corretor vazao.
	 *
	 * @param dados the dados
	 * @return true, if successful
	 */
	private boolean verificarNecessidadeRetiradaCorretorVazao(Map<String, Object> dados) {

		Boolean contemCorretorVazao = (Boolean) dados.get(CONTEM_CORRETOR_VAZAO);

		if (contemCorretorVazao != null && !contemCorretorVazao) {
			return false;
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#validarDadosAssociacaoPontoConsumoCorretorVazao(java.util.Map)
	 */
	@Override
	public void validarDadosAssociacaoPontoConsumoCorretorVazao(Map<String, Object> dados) throws NegocioException {

		Long idOperacaoMedidor = (Long) dados.get(VazaoCorretor.ID_OPERACAO_MEDIDOR);

		if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_INSTALACAO) {
			this.validarDadosInstalacaoCorretorVazao(dados);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_SUBSTITUICAO) {
			this.validarDadosSubstituicaoCorretorVazao(dados);
		} else if (idOperacaoMedidor.longValue() == OperacaoMedidor.CODIGO_RETIRADA) {
			this.validarDadosRetiradaCorretorVazao(dados);
		}
	}

	/**
	 * Validar dados instalacao corretor vazao.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosInstalacaoCorretorVazao(Map<String, Object> dados) throws NegocioException {

		this.validarDadosEmComumInstalacaoSubstituicaoCorretorVazao(dados);
	}

	/**
	 * Validar dados substituicao corretor vazao.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosSubstituicaoCorretorVazao(Map<String, Object> dados) throws NegocioException {

		this.validarDadosEmComumInstalacaoSubstituicaoCorretorVazao(dados);

		validarDataSubstituicaoAnteriorDataInstalacaoCorretor(dados);

		if (dados.get(MEDIDOR_MOTIVO_OPERACAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(CHAVE_CORRETOR_VAZAO_ANTERIOR) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_CORRETOR_VAZAO_ANTERIOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}
	}

	private void validarDataSubstituicaoAnteriorDataInstalacaoCorretor(Map<String, Object> dados) throws NegocioException {
		PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO));
		Date dataSubstituicao = (Date) dados.get(DATA_CORRETOR_VAZAO);
		if (dataSubstituicao.before(pontoConsumo.getInstalacaoMedidor().getDataCorretorVazao())) {
			throw new NegocioException(ERRO_DATA_SUBSTITUICAO_CORRETOR_ANTERIOR_INSTALACAO, true);
		}
	}

	/**
	 * Validar dados em comum instalacao substituicao corretor vazao.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosEmComumInstalacaoSubstituicaoCorretorVazao(Map<String, Object> dados) throws NegocioException {

		ControladorVazaoCorretor controladorVazaoCorretor = (ControladorVazaoCorretor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorVazaoCorretor.BEAN_ID_CONTROLADOR_VAZAO_CORRETOR);
		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		VazaoCorretor vazaoCorretor = null;

		if (dados.get(CHAVE_CORRETOR_VAZAO_ATUAL) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_CORRETOR_VAZAO_ATUAL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		} else {

			Long codigoVazaoCorretorProntoInstalacao =
					Long.valueOf(getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_PRONTO_INSTALAR));

			vazaoCorretor = (VazaoCorretor) controladorVazaoCorretor.obter((Long) dados.get(CHAVE_CORRETOR_VAZAO_ATUAL));

			if (!codigoVazaoCorretorProntoInstalacao.equals(vazaoCorretor.getSituacaoMedidor().getChavePrimaria())) {
				throw new NegocioException(ControladorMedidor.ERRO_CORRETOR_VAZAO_NAO_DISPONIVEL_PARA_INSTALACAO_ASSOCIACAO_MEDIDOR, true);
			}

		}

		this.validarDataCorretorVazaoAnteriorDataMedidor(dados);

		this.validarDataMedidorCorretorVazao((Date) dados.get(DATA_CORRETOR_VAZAO));

		this.validarHoraMedidorCorretorVazao((String) dados.get(HORA_CORRETOR_VAZAO));

		if (dados.get(LEITURA_ATUAL_CORRETOR_VAZAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ATUAL_NAO_INFORMADA_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(LOCAL_INSTALACAO_CORRETOR_VAZAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_LOCAL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(FUNCIONARIO_CORRETOR_VAZAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_FUNCIONARIO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

	}

	/**
	 * Validar dados retirada corretor vazao.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosRetiradaCorretorVazao(Map<String, Object> dados) throws NegocioException {

		InstalacaoMedidor instanlacaoMedidor;
		Long idPontoConsumo = (Long) dados.get(Medidor.ID_PONTO_CONSUMO);
		if (idPontoConsumo == null) {
			Long idMedidorIndependente = (Long) dados.get(ID_MEDIDOR_INDEPENDENTE);
			Medidor medidor = (Medidor) obter(idMedidorIndependente);
			instanlacaoMedidor = medidor.getInstalacaoMedidor();
		} else {
			PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter(idPontoConsumo);
			instanlacaoMedidor = pontoConsumo.getInstalacaoMedidor();
		}

		if (dados.get(CHAVE_CORRETOR_VAZAO_ANTERIOR) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_CORRETOR_VAZAO_ATUAL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		Date dataRetirada = (Date) dados.get(DATA_CORRETOR_VAZAO);
		String hora = (String) dados.get(HORA_CORRETOR_VAZAO);
		dataRetirada = this.obterDataHoraMedidorCorretorVazao(dataRetirada, hora);

		this.validarDataMedidorCorretorVazao(dataRetirada);

		Date dataInstalacao = instanlacaoMedidor.getDataCorretorVazao();

		if (dataRetirada.compareTo(dataInstalacao) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_INSTALACAO_CORRETOR_VAZAO_ASSOCIACAO_MEDIDOR, true);
		}

		this.validarHoraMedidorCorretorVazao((String) dados.get(HORA_CORRETOR_VAZAO));

		if (dados.get(LEITURA_ATUAL_CORRETOR_VAZAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ATUAL_NAO_INFORMADA_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(FUNCIONARIO_CORRETOR_VAZAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_FUNCIONARIO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(MEDIDOR_MOTIVO_OPERACAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}
	}

	/**
	 * Validar dados instalacao medidor.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosInstalacaoMedidor(Map<String, Object> dados) throws NegocioException {

		this.validarDadosEmComumInstalacaoSubstituicaoMedidor(dados);
	}

	/**
	 * Validar dados ativacao medidor.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosAtivacaoMedidor(Map<String, Object> dados) throws GGASException {

		PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO));
		Medidor medidor = this.obterMedidor((Long) dados.get(CHAVE_MEDIDOR_ANTERIOR));

		this.validarDadosEmComumAtivacaoRetiradaMedidor(dados);
		this.validarDataInferiorDataAquisicao(dados, medidor);

		Long chaveModoUsoVirtual = Long.valueOf(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));
		if (chaveModoUsoVirtual.equals(medidor.getModoUso().getChavePrimaria())) {
			List<Long> listaIdMedidorIndependente = recuperaMedidoresQueCompoemMedidorVirtual(medidor);
			if (!listaIdMedidorIndependente.isEmpty()) {
				StringBuilder listaMedidorNaoAtivo = new StringBuilder();
				for (Long idMedidorIndependente : listaIdMedidorIndependente) {
					Medidor medidorIndependente = (Medidor) obter(idMedidorIndependente);
					Long codSituacaoMIativo = Long.valueOf(getControladorConstante()
							.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_ATIVO));
					if (codSituacaoMIativo != null && medidorIndependente != null
							&& medidorIndependente.getSituacaoAssociacaoMedidorIndependente() != null && !codSituacaoMIativo
									.equals(medidorIndependente.getSituacaoAssociacaoMedidorIndependente().getChavePrimaria())) {
						listaMedidorNaoAtivo.append(medidorIndependente.getNumeroSerie());
						listaMedidorNaoAtivo.append(", ");
					}
				}

				if (listaMedidorNaoAtivo.toString().length() > 0) {
					throw new NegocioException(ControladorMedidor.ERRO_MEDIDOR_INDEPENDENTE_DESATIVADO,
							listaMedidorNaoAtivo.toString().substring(0, listaMedidorNaoAtivo.toString().length() - 1));
				}
			}
		}

		Date dataAtivacao = (Date) dados.get(DATA_MEDIDOR);
		Date dataInstalacao = pontoConsumo.getInstalacaoMedidor().getData();

		if (dataAtivacao.compareTo(dataInstalacao) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_INSTALACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}
	}

	/**
	 * Validar dados ativacao reativacao medidor independente.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosAtivacaoReativacaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		Long idMedidorIndependente;

		if (dados.get(CHAVE_MEDIDOR_ANTERIOR) != null) {
			idMedidorIndependente = (Long) dados.get(CHAVE_MEDIDOR_ANTERIOR);
		} else {
			idMedidorIndependente = (Long) dados.get(ID_MEDIDOR_INDEPENDENTE);
		}

		Medidor medidorIndependente = this.obterMedidor(idMedidorIndependente);
		if (medidorIndependente.getInstalacaoMedidor() == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MEDIDOR_INDEPENDENTE_NAO_INSTALADO, medidorIndependente.getNumeroSerie());
		}

		if (!verificaSeMedidorFazParteDeComposicao(idMedidorIndependente)) {
			throw new NegocioException(ControladorMedidor.ERRO_MEDIDOR_NAO_FAZ_PARTE_DE_COMPOSICAO, true);
		}
		validarDataInferiorDataAquisicao(dados, medidorIndependente);
	}

	/**
	 * Validar dados retirada medidor.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosRetiradaMedidor(Map<String, Object> dados) throws GGASException {

		PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO));

		this.validarDadosEmComumAtivacaoRetiradaMedidor(dados);

		Date dataRetirada = (Date) dados.get(DATA_MEDIDOR);
		Date dataInstalacao = pontoConsumo.getInstalacaoMedidor().getData();
		Date dataAtivacao = pontoConsumo.getInstalacaoMedidor().getDataAtivacao();

		validaMedidorIndependenteInstaladoEmComposicao(dados, getControladorConstante());

		if (dataRetirada.compareTo(dataInstalacao) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_INSTALACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if ((dataAtivacao != null) && (dataRetirada.compareTo(dataAtivacao) < 0)) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_ATIVACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(MEDIDOR_MOTIVO_OPERACAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}
	}

	private void validaMedidorIndependenteInstaladoEmComposicao(Map<String, Object> dados,
			ControladorConstanteSistema controladorConstanteSistema) throws GGASException {

		Long chavePrimariaMedidor = (Long) dados.get(CHAVE_MEDIDOR_ANTERIOR);
		Medidor medidor = this.obterMedidor(chavePrimariaMedidor);
		Long chaveModoUsoVirtual = Long.valueOf(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));
		if (chaveModoUsoVirtual.equals(medidor.getModoUso().getChavePrimaria())) {
			List<Long> listaIdMedidorIndependente = recuperaMedidoresQueCompoemMedidorVirtual(medidor);
			if (!listaIdMedidorIndependente.isEmpty()) {
				for (Long idMedidorIndependente : listaIdMedidorIndependente) {
					Medidor medidorIndependente = (Medidor) obter(idMedidorIndependente);
					Long codSituacaoMIativo = Long.valueOf(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_ATIVO));
					Long codSituacaoMIaguardandoAtivacao = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
							Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_AGUARDANDO_ATIVACAO));
					Long codigoMedidorInstalado =
							Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_INSTALADO));

					if (codSituacaoMIativo.equals(medidorIndependente.getSituacaoAssociacaoMedidorIndependente().getChavePrimaria())
							|| (codSituacaoMIaguardandoAtivacao
									.equals(medidorIndependente.getSituacaoAssociacaoMedidorIndependente().getChavePrimaria())
									&& codigoMedidorInstalado.equals(medidorIndependente.getSituacaoMedidor().getChavePrimaria()))) {
						Collection<Medidor> listaMedidorVirtual = consultarMedidorVirtualPorMedidorIndependente(medidorIndependente);
						for (Medidor medidorVirtualAux : listaMedidorVirtual) {
							if (medidorVirtualAux.getChavePrimaria() != chavePrimariaMedidor) {
								throw new NegocioException(ControladorMedidor.ERRO_EXISTE_MEDIDOR_INSTALADO_NA_COMPOSICAO, true);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Validar dados retirada medidor independente.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosRetiradaMedidorIndependente(Map<String, Object> dados) throws GGASException {

		Long idMedidorIndependente = (Long) dados.get(ID_MEDIDOR_INDEPENDENTE);
		Medidor medidor = obterMedidor(idMedidorIndependente);

		this.validarDadosEmComumAtivacaoRetiradaMedidor(dados);

		Date dataRetirada = (Date) dados.get(DATA_MEDIDOR);
		Date dataInstalacao = medidor.getInstalacaoMedidor().getData();
		Date dataAtivacao = medidor.getInstalacaoMedidor().getDataAtivacao();

		verificaSeMedidorCompoeMedidorVirtual(new Long[] { idMedidorIndependente });

		if (dataRetirada.compareTo(dataInstalacao) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_INSTALACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if ((dataAtivacao != null) && (dataRetirada.compareTo(dataAtivacao) < 0)) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_ATIVACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(MEDIDOR_MOTIVO_OPERACAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}
	}

	/**
	 * Validar dados substituicao medidor.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosSubstituicaoMedidor(Map<String, Object> dados) throws NegocioException {

		PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO));

		this.validarDadosEmComumInstalacaoSubstituicaoMedidor(dados);

		Date dataRetirada = (Date) dados.get(DATA_MEDIDOR);
		Date dataInstalacao = pontoConsumo.getInstalacaoMedidor().getData();
		Date dataAtivacao = pontoConsumo.getInstalacaoMedidor().getDataAtivacao();

		if (dataRetirada.compareTo(dataInstalacao) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_INSTALACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if ((dataAtivacao != null) && (dataRetirada.compareTo(dataAtivacao) < 0)) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_ATIVACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(MEDIDOR_MOTIVO_OPERACAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(CHAVE_MEDIDOR_ANTERIOR) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MEDIDOR_ANTERIOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(LEITURA_ANTERIOR) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ANTERIOR_NAO_INFORMADA_ASSOCIACAO_MEDIDOR, true);
		} else {

			Medidor medidor = this.obterMedidor((Long) dados.get(CHAVE_MEDIDOR_ANTERIOR));

			if (medidor.getDigito() == null) {
				throw new NegocioException(ControladorMedidor.ERRO_DIGITOS_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
			} else if (medidor.getDigito().intValue() < ((String) dados.get(LEITURA_ANTERIOR)).length()) {
				throw new NegocioException(ControladorMedidor.ERRO_QTDA_DIGITOS_MEDIDOR_MENOR_QTDA_DIGITOS_LEITURA_ASSOCIACAO_MEDIDOR,
						true);
			}

			validarLeituraAnteriorMenorOuIgualAZero(dados, pontoConsumo);
		}

		this.validarDataSubstituicaoMedidor(dados);

	}

	private void validarLeituraAnteriorMenorOuIgualAZero(Map<String, Object> dados, PontoConsumo pontoConsumo) throws NegocioException {
		if (Integer.parseInt((String) dados.get(LEITURA_ANTERIOR)) < 0) {
			if (!getControladorPontoConsumo().isPontoConsumoAguardandoAtivacao(pontoConsumo)) {
				throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ATUAL_MENOR_IGUAL_ZERO_ASSOCIACAO_MEDIDOR, true);
			}
		}
	}

	/**
	 * Validar dados substituicao medidor independente.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosSubstituicaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		Long idMedidorIndependente = (Long) dados.get(ID_MEDIDOR_INDEPENDENTE);
		Medidor medidor = this.obterMedidor(idMedidorIndependente);

		this.validarDadosEmComumInstalacaoSubstituicaoMedidor(dados);

		verificaSeMedidorCompoeMedidorVirtual(new Long[] { idMedidorIndependente });

		Date dataInstalacao = medidor.getInstalacaoMedidor().getData();
		Date dataRetirada = (Date) dados.get(DATA_MEDIDOR);
		Date dataAtivacao = medidor.getInstalacaoMedidor().getDataAtivacao();

		if (dataRetirada.compareTo(dataInstalacao) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_INSTALACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if ((dataAtivacao != null) && (dataRetirada.compareTo(dataAtivacao) < 0)) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_ATIVACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(MEDIDOR_MOTIVO_OPERACAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(CHAVE_MEDIDOR_ANTERIOR) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MEDIDOR_ANTERIOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(LEITURA_ANTERIOR) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ANTERIOR_NAO_INFORMADA_ASSOCIACAO_MEDIDOR, true);
		} else {
			if (medidor.getDigito() == null) {
				throw new NegocioException(ControladorMedidor.ERRO_DIGITOS_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
			} else if (medidor.getDigito().intValue() < ((String) dados.get(LEITURA_ATUAL)).length()) {
				throw new NegocioException(ControladorMedidor.ERRO_QTDA_DIGITOS_MEDIDOR_MENOR_QTDA_DIGITOS_LEITURA_ASSOCIACAO_MEDIDOR,
						true);
			} else if (Integer.parseInt((String) dados.get(LEITURA_ATUAL)) < 0) {
				throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ATUAL_MENOR_ZERO_ASSOCIACAO_MEDIDOR, true);
			}
		}

		this.validarDataSubstituicaoMedidorIndependente(dados);

	}

	/**
	 * Instancia medidor por operacao medidor.
	 *
	 * @param dados the dados
	 * @return the medidor
	 * @throws NegocioException the negocio exception
	 */
	private Medidor instanciaMedidorPorOperacaoMedidor(Map<String, Object> dados) throws NegocioException {

		Medidor medidor;
		Long idOperacaoMedidorIndependente = (Long) dados.get(Medidor.ID_OPERACAO_MEDIDOR_INDEPENDENTE);
		if (idOperacaoMedidorIndependente.equals(OperacaoMedidor.CODIGO_SUBSTITUICAO)) {
			medidor = this.obterMedidor((Long) dados.get(CHAVE_MEDIDOR_ATUAL));
		} else {
			medidor = this.obterMedidor((Long) dados.get(ID_MEDIDOR_INDEPENDENTE));
		}
		return medidor;
	}

	/**
	 * Validar dados em comum instalacao substituicao medidor.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosEmComumInstalacaoSubstituicaoMedidor(Map<String, Object> dados) throws NegocioException {

		Medidor medidor = null;

		Boolean isAssociacaoMedidorIndependente = (Boolean) dados.get("isAssociacaoMedidorIndependente");
		if (isAssociacaoMedidorIndependente != null && isAssociacaoMedidorIndependente) {
			medidor = instanciaMedidorPorOperacaoMedidor(dados);

			validarMedidorProntoParaInstalacao(dados);
			validarDataInstalacaoInferiorDataUltimaRetirada(dados, Boolean.TRUE);
			validarDataMedidorCorretorVazao((Date) dados.get(DATA_MEDIDOR));

			if (dados.get(LEITURA_ATUAL) == null) {
				throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ATUAL_NAO_INFORMADA_ASSOCIACAO_MEDIDOR, true);
			} else {
				validarDadosLeituraMedidor(dados, medidor);
			}
		} else {
			if (dados.get(CHAVE_MEDIDOR_ATUAL) == null) {
				throw new NegocioException(ControladorMedidor.ERRO_MEDIDOR_ATUAL_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
			} else {
				validarMedidorProntoParaInstalacao(dados);
			}

			validarDataMedidorCorretorVazao((Date) dados.get(DATA_MEDIDOR));
			validarDataInstalacaoInferiorDataUltimaRetirada(dados, Boolean.TRUE);

			medidor = this.obterMedidor((Long) dados.get(CHAVE_MEDIDOR_ATUAL));
			if (dados.get(Medidor.MODO_USO) != null && !VIRTUAL.equals(dados.get(Medidor.MODO_USO))) {
				if (dados.get(LEITURA_ATUAL) == null) {
					throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ATUAL_NAO_INFORMADA_ASSOCIACAO_MEDIDOR, true);
				} else {
					validarDadosLeituraMedidor(dados, medidor);
				}
			}
		}

		validarDataInferiorDataAquisicao(dados, medidor);

		if (dados.get(InstalacaoMedidor.FUNCIONARIO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_FUNCIONARIO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(MEDIDOR_MOTIVO_OPERACAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

	}

	private void validarDataInferiorDataAquisicao(Map<String, Object> dados, Medidor medidor) throws NegocioException {

		Date dataNovaInstalacao = (Date) dados.get(DATA_MEDIDOR);
		Date dataAquisicao = medidor.getDataAquisicao();

		if (dataNovaInstalacao.compareTo(dataAquisicao) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_INFERIOR_DATA_AQUISICAO, true);
		}
	}

	/**
	 * Validar dados leitura medidor.
	 *
	 * @param dados the dados
	 * @param medidor the medidor
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosLeituraMedidor(Map<String, Object> dados, Medidor medidor) throws NegocioException {

		if (medidor.getDigito() == null) {
			throw new NegocioException(ControladorMedidor.ERRO_DIGITOS_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		} else if (medidor.getDigito().intValue() < ((String) dados.get(LEITURA_ATUAL)).length()) {
			throw new NegocioException(ControladorMedidor.ERRO_QTDA_DIGITOS_MEDIDOR_MENOR_QTDA_DIGITOS_LEITURA_ASSOCIACAO_MEDIDOR, true);
		} else if (Integer.parseInt((String) dados.get(LEITURA_ATUAL)) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ATUAL_MENOR_ZERO_ASSOCIACAO_MEDIDOR, true);
		}
	}

	/**
	 * Validar medidor pronto para instalacao.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarMedidorProntoParaInstalacao(Map<String, Object> dados) throws NegocioException {

		Medidor medidor = null;

		Long codigoMedidorProntoInstalacao =
				Long.valueOf(getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_PRONTO_INSTALAR));

		Long chaveMedidorAtual = (Long) dados.get(CHAVE_MEDIDOR_ATUAL);
		if (!Util.isEmpty(chaveMedidorAtual)) {
			medidor = this.obterMedidor(chaveMedidorAtual);
		} else {
			Long idMedidorIndependente = (Long) dados.get(ID_MEDIDOR_INDEPENDENTE);
			if (!Util.isEmpty(idMedidorIndependente)) {
				medidor = this.obterMedidor(idMedidorIndependente);
			}
		}

		if (medidor != null && medidor.getSituacaoMedidor() != null
				&& !codigoMedidorProntoInstalacao.equals(medidor.getSituacaoMedidor().getChavePrimaria())) {
			throw new NegocioException(ControladorMedidor.ERRO_MEDIDOR_NAO_DISPONIVEL_PARA_INSTALACAO_ASSOCIACAO_MEDIDOR, true);
		}
	}

	/**
	 * Validar dados em comum ativacao retirada medidor.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosEmComumAtivacaoRetiradaMedidor(Map<String, Object> dados) throws NegocioException {

		if (dados.get(CHAVE_MEDIDOR_ANTERIOR) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MEDIDOR_ANTERIOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		this.validarDataMedidorCorretorVazao((Date) dados.get(DATA_MEDIDOR));

		if (dados.get(LEITURA_ANTERIOR) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ANTERIOR_NAO_INFORMADA_ASSOCIACAO_MEDIDOR, true);
		}
		
		if (dados.get(Medidor.MODO_USO) != null && !VIRTUAL.equals(dados.get(Medidor.MODO_USO))) {
			if (dados.get(LEITURA_ANTERIOR) == null) {
				throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ANTERIOR_NAO_INFORMADA_ASSOCIACAO_MEDIDOR, true);
			} else {

				Medidor medidor = this.obterMedidor((Long) dados.get(CHAVE_MEDIDOR_ANTERIOR));

				if (medidor.getDigito() == null) {
					throw new NegocioException(ControladorMedidor.ERRO_DIGITOS_MEDIDOR_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
				} else if (medidor.getDigito().intValue() < ((String) dados.get(LEITURA_ANTERIOR)).length()) {
					throw new NegocioException(ControladorMedidor.ERRO_QTDA_DIGITOS_MEDIDOR_MENOR_QTDA_DIGITOS_LEITURA_ASSOCIACAO_MEDIDOR,
							true);
				} else if (Integer.parseInt((String) dados.get(LEITURA_ANTERIOR)) < 0) {
					throw new NegocioException(ControladorMedidor.ERRO_LEITURA_ANTERIOR_MENOR_ZERO_ASSOCIACAO_MEDIDOR, true);
				}

			}

		}

		if (dados.get(InstalacaoMedidor.FUNCIONARIO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_FUNCIONARIO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

	}

	/**
	 * Validar data medidor corretor vazao.
	 *
	 * @param dataMedidorCorretor the data medidor corretor
	 * @throws NegocioException the negocio exception
	 */
	private void validarDataMedidorCorretorVazao(Date dataMedidorCorretor) throws NegocioException {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

		try {
			Date dataAtual = df.parse(df.format(new Date()));

			if (dataMedidorCorretor == null) {
				throw new NegocioException(ControladorMedidor.ERRO_DATA_NAO_INFORMADA_ASSOCIACAO_MEDIDOR, true);
			} else {
				if (dataMedidorCorretor.compareTo(dataAtual) > 0) {
					throw new NegocioException(ControladorMedidor.ERRO_DATA_MEDIDOR_CORRETOR_VAZAO_INVALIDA_ASSOCIACAO_MEDIDOR, true);
				}
			}

		} catch (ParseException e) {
			throw new NegocioException("ERRO_INESPERADO", true);
		}

	}

	/**
	 * Valida se a data de instalação do Corretor de Vazão é posterior a data de instalação do medidor.
	 *
	 * @param dados
	 * @throws NegocioException
	 */
	private void validarDataCorretorVazaoAnteriorDataMedidor(Map<String, Object> dados) throws NegocioException {
		Date dataCorretor = (Date) dados.get(DATA_CORRETOR_VAZAO);
		Date dataMedidor = (Date) dados.get(DATA_MEDIDOR);
		if (dataMedidor == null) {
			Collection<InstalacaoMedidor> instalacaoMedidor =
					consultarInstalacaoMedidor((Long) dados.get(ID_MEDIDOR_INDEPENDENTE), (Long) dados.get(Medidor.ID_PONTO_CONSUMO));
			dataMedidor = instalacaoMedidor.iterator().next().getData();
		}
		if (dataCorretor != null && dataCorretor.before(dataMedidor)) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_INSTALACAO_VAZAO_CORRETOR_ANTERIOR_DATA_INSTALACAO_MEDIDOR, true);
		}
	}

	/**
	 * Validar hora medidor corretor vazao.
	 *
	 * @param campo the campo
	 * @throws NegocioException the negocio exception
	 */
	private void validarHoraMedidorCorretorVazao(String campo) throws NegocioException {

		if (campo == null) {
			throw new NegocioException(ControladorMedidor.ERRO_HORA_NAO_INFORMADA_ASSOCIACAO_MEDIDOR, true);
		} else {

			try {

				Integer hora = Integer.valueOf(campo.substring(0, 2));
				Integer minutos = Integer.valueOf(campo.substring(3, 5));

				if ((hora > 23) || (minutos > 59)) {
					throw new NegocioException(ControladorMedidor.ERRO_HORA_INVALIDA_ASSOCIACAO_MEDIDOR, true);
				}

			} catch (Exception e) {
				LOG.error(e.getStackTrace(), e);
				throw new NegocioException(ControladorMedidor.ERRO_HORA_INVALIDA_ASSOCIACAO_MEDIDOR, true);
			}

		}

	}

	/**
	 * Validar data substituicao medidor.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDataSubstituicaoMedidor(Map<String, Object> dados) throws NegocioException {

		Criteria criteria = this.createCriteria(HistoricoMedicao.class);
		criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, dados.get(Medidor.ID_PONTO_CONSUMO)));
		criteria.setProjection(Projections.max("dataLeituraFaturada"));

		Date dataFaturamento = (Date) criteria.uniqueResult();

		if (dataFaturamento != null && dataFaturamento.compareTo((Date) dados.get(DATA_MEDIDOR)) > 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_SUBTITUICAO_MEDIDOR_ANTERIOR_DATA_FATURAMENTO_ASSOCIACAO_MEDIDOR, true);
		}
	}

	/**
	 * Validar data substituicao medidor independente.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDataSubstituicaoMedidorIndependente(Map<String, Object> dados) throws NegocioException {

		Criteria criteria = this.createCriteria(HistoricoMedicao.class);

		criteria.createAlias("historicoInstalacaoMedidor", "historicoInstalacaoMedidor");
		criteria.createAlias("historicoInstalacaoMedidor.medidor", HistoricoOperacaoMedidor.MEDIDOR);

		criteria.add(Restrictions.eq(MEDIDOR_CHAVE_PRIMARIA, dados.get(ID_MEDIDOR_INDEPENDENTE)));
		criteria.setProjection(Projections.max("dataLeituraFaturada"));

		Date dataFaturamento = (Date) criteria.uniqueResult();

		if (dataFaturamento != null && dataFaturamento.compareTo((Date) dados.get(DATA_MEDIDOR)) > 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_SUBTITUICAO_MEDIDOR_ANTERIOR_DATA_FATURAMENTO_ASSOCIACAO_MEDIDOR, true);
		}
	}

	/**
	 * Obter data hora medidor corretor vazao.
	 *
	 * @param data the data
	 * @param hora the hora
	 * @return the date
	 * @throws NegocioException the negocio exception
	 */
	private Date obterDataHoraMedidorCorretorVazao(Date data, String hora) throws NegocioException {

		SimpleDateFormat df1 = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		SimpleDateFormat df2 = new SimpleDateFormat(Constantes.FORMATO_DATA_HORA_SEM_SEGUNDOS_BR);

		try {
			return df2.parse(df1.format(data) + " " + hora);
		} catch (ParseException e) {
			throw new NegocioException("ERRO_INESPERADO", true);
		}

	}

	/**
	 * Criar historico operacao medidor.
	 *
	 * @return the historico operacao medidor
	 */
	private HistoricoOperacaoMedidor criarHistoricoOperacaoMedidor() {

		return (HistoricoOperacaoMedidor) ServiceLocator.getInstancia()
				.getBeanPorID(HistoricoOperacaoMedidor.BEAN_ID_HISTORICO_OPERACAO_MEDIDOR);
	}

	/**
	 * Criar vazao corretor historico operacao.
	 *
	 * @return the vazao corretor historico operacao
	 */
	private VazaoCorretorHistoricoOperacao criarVazaoCorretorHistoricoOperacao() {

		return (VazaoCorretorHistoricoOperacao) ServiceLocator.getInstancia()
				.getBeanPorID(VazaoCorretorHistoricoOperacao.BEAN_ID_VAZAO_CORRETOR_HISTORICO_OPERACAO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # obterOperacoesMedidorPorTipoAssociacao(java .lang.Integer)
	 */
	@Override
	public List<OperacaoMedidor> obterOperacoesMedidorPorTipoAssociacao(Integer codigoAssociacao, String status, String numeroSerieMedidor,
			String corretorVazao) throws GGASException {
		List<OperacaoMedidor> listaOperacoes = this.listarOperacaoMedidor();
		List<OperacaoMedidor> listaOperacoesPermitidas = new ArrayList<OperacaoMedidor>();
		ControladorImovel controladorImovel =
				(ControladorImovel) ServiceLocator.getInstancia().getControladorNegocio(ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
		ControladorMedidor controladorMedidor =
				(ControladorMedidor) ServiceLocator.getInstancia().getControladorNegocio(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);
		// status
		ConstanteSistema cAguardandoAtivacao =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_AGUARDANDO_ATIVACAO);
		ConstanteSistema cAtivo = getControladorConstante().obterConstantePorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_ATIVO);
		ConstanteSistema cSuspenso = getControladorConstante().obterConstantePorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_SUSPENSO);
		getControladorConstante().obterConstantePorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_SUPRIMIDO_TOTAL);
		getControladorConstante().obterConstantePorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_SUPRIMIDO_PARCIAL);
		ConstanteSistema cCortado = getControladorConstante().obterConstantePorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_SUPRIMIDO_DEFINITIVO);
		getControladorConstante().obterConstantePorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_BLOQUEADO_TECNICAMENTE);
		ConstanteSistema cBloqueado = getControladorConstante().obterConstantePorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_BLOQUEADO);

		// status
		SituacaoConsumo statusAguardandoAtivacao = controladorImovel.obterSituacaoConsumo(Long.parseLong(cAguardandoAtivacao.getValor()));
		SituacaoConsumo statusAtivo = controladorImovel.obterSituacaoConsumo(Long.parseLong(cAtivo.getValor()));
		SituacaoConsumo statusBloqueado = controladorImovel.obterSituacaoConsumo(Long.parseLong(cBloqueado.getValor()));
		SituacaoConsumo statusSupenso = controladorImovel.obterSituacaoConsumo(Long.parseLong(cSuspenso.getValor()));
		SituacaoConsumo statusCortado = controladorImovel.obterSituacaoConsumo(Long.parseLong(cCortado.getValor()));

		// operação medidor
		ConstanteSistema cInstalacao = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_INSTALACAO);
		ConstanteSistema cAtivacao = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_ATIVACAO);
		ConstanteSistema cSubstituicao = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_SUBSTITUICAO);
		ConstanteSistema cRetirada = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_RETIRADA);
		ConstanteSistema cBloqueio = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_BLOQUEIO);
		ConstanteSistema cReativacao = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_REATIVACAO);
		ConstanteSistema cInstalacaoAtivacao =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_INSTALACAO_ATIVACAO);
		ConstanteSistema cBloqueioReativacao =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_BLOQUEIO_REATIVACAO);


		// operação medidor
		OperacaoMedidor operacaoInstalacao = controladorMedidor.obterOperacaoMedidor(Long.parseLong(cInstalacao.getValor()));
		OperacaoMedidor operacaoAtivacao = controladorMedidor.obterOperacaoMedidor(Long.parseLong(cAtivacao.getValor()));
		OperacaoMedidor operacaoSubstituicao = controladorMedidor.obterOperacaoMedidor(Long.parseLong(cSubstituicao.getValor()));
		OperacaoMedidor operacaoRetirada = controladorMedidor.obterOperacaoMedidor(Long.parseLong(cRetirada.getValor()));
		OperacaoMedidor operacaoBloqueio = controladorMedidor.obterOperacaoMedidor(Long.parseLong(cBloqueio.getValor()));
		OperacaoMedidor operacaoReativacao = controladorMedidor.obterOperacaoMedidor(Long.parseLong(cReativacao.getValor()));
		OperacaoMedidor operacaoInstalacaoAtivacao =
				controladorMedidor.obterOperacaoMedidor(Long.parseLong(cInstalacaoAtivacao.getValor()));
		OperacaoMedidor operacaoBloqueioReativacao =
				controladorMedidor.obterOperacaoMedidor(Long.parseLong(cBloqueioReativacao.getValor()));		

		Long chaveModoUsoVirtual = Long.valueOf(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL));
		List<Medidor> listaMedidor = obterMedidorPorNumeroSerie(numeroSerieMedidor);
		Medidor medidor = null;
		if (!listaMedidor.isEmpty()) {
			medidor = Util.primeiroElemento(listaMedidor);
		}
		if (codigoAssociacao.equals(CODIGO_TIPO_ASSOCIACAO_PONTO_CONSUMO_MEDIDOR)) {

			for (OperacaoMedidor operacaoMedidor : listaOperacoes) {

				if ((operacaoMedidor.getChavePrimaria() == operacaoInstalacao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoSubstituicao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoAtivacao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoRetirada.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoBloqueio.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoReativacao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoInstalacaoAtivacao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoBloqueioReativacao.getChavePrimaria())) {

					if (status.equals(statusAguardandoAtivacao.getDescricao()) && "".equals(numeroSerieMedidor)) {
						if (operacaoMedidor.getDescricao().equals(operacaoInstalacao.getDescricao())
								|| operacaoMedidor.getDescricao().equals(operacaoInstalacaoAtivacao.getDescricao())) {
							listaOperacoesPermitidas.add(operacaoMedidor);
						}
					} else if (status.equals(statusAguardandoAtivacao.getDescricao()) && !"".equals(numeroSerieMedidor)) {
						if ((operacaoMedidor.getDescricao().equals(operacaoAtivacao.getDescricao()))
								|| (operacaoMedidor.getDescricao().equals(operacaoSubstituicao.getDescricao())
										&& (medidor != null && !chaveModoUsoVirtual.equals(medidor.getModoUso().getChavePrimaria())))
								|| (operacaoMedidor.getDescricao().equals(operacaoRetirada.getDescricao()))) {
							listaOperacoesPermitidas.add(operacaoMedidor);
						}
					} else {
						if (operacaoMedidor.getDescricao().equals(operacaoRetirada.getDescricao())
								|| (operacaoMedidor.getDescricao().equals(operacaoSubstituicao.getDescricao())
										&& (medidor != null && !chaveModoUsoVirtual.equals(medidor.getModoUso().getChavePrimaria())))) {
							listaOperacoesPermitidas.add(operacaoMedidor);
						}
					}
					if (status.equals(statusAtivo.getDescricao()) && !"".equals(numeroSerieMedidor)
							&& (operacaoMedidor.getDescricao().equals(operacaoBloqueio.getDescricao()) || operacaoMedidor.getDescricao().equals(operacaoBloqueioReativacao.getDescricao()))) {
						listaOperacoesPermitidas.add(operacaoMedidor);
					}
					
					if ((status.equals(statusBloqueado.getDescricao()) || status.equals(statusSupenso.getDescricao())
							|| status.equals(statusCortado.getDescricao())) && !"".equals(numeroSerieMedidor)
							&& operacaoMedidor.getDescricao().equals(operacaoReativacao.getDescricao())) {
						listaOperacoesPermitidas.add(operacaoMedidor);
					}
					// só entra neste if quando chamado pela ServicoTipoAction método obterOperacoes
					if (status.isEmpty() && numeroSerieMedidor.isEmpty() && !listaOperacoesPermitidas.contains(operacaoMedidor)) {
						listaOperacoesPermitidas.add(operacaoMedidor);
					}
				}

			}

		} else if (codigoAssociacao.equals(CODIGO_TIPO_ASSOCIACAO_PONTO_CONSUMO_CORRETOR_VAZAO)) {

			for (OperacaoMedidor operacaoMedidor : listaOperacoes) {

				if ((operacaoMedidor.getChavePrimaria() == operacaoInstalacao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoSubstituicao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoAtivacao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoRetirada.getChavePrimaria())) {

					if (!"".equals(numeroSerieMedidor) && "".equals(corretorVazao)) {
						if (operacaoMedidor.getDescricao().equals(operacaoInstalacao.getDescricao())) {
							listaOperacoesPermitidas.add(operacaoMedidor);
						}
					} else if (!"".equals(numeroSerieMedidor) && !"".equals(corretorVazao)
							&& (operacaoMedidor.getDescricao().equals(operacaoRetirada.getDescricao())
									|| operacaoMedidor.getDescricao().equals(operacaoSubstituicao.getDescricao()))) {

						listaOperacoesPermitidas.add(operacaoMedidor);
					}

					// só entra neste if quando chamado pela ServicoTipoAction método obterOperacoes
					verificaOperacaoMedidor(status, numeroSerieMedidor, listaOperacoesPermitidas, operacaoAtivacao, operacaoMedidor);
				}

			}

		}

		return listaOperacoesPermitidas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # obterOperacoesMedidorPorTipoAssociacao(java .lang.Integer)
	 */
	@Override
	public List<OperacaoMedidor> obterOperacoesMedidorPorImovelCondonimioIndividual(Integer codigoAssociacao, Long idImovel)
			throws GGASException {
		List<OperacaoMedidor> listaOperacoesPermitidas = new ArrayList<>();
		ControladorMedidor controladorMedidor =
				(ControladorMedidor) ServiceLocator.getInstancia().getControladorNegocio(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);
		// operação medidor
		ConstanteSistema cInstalacao = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_INSTALACAO);
		ConstanteSistema cAtivacao = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_ATIVACAO);
		ConstanteSistema cInstalacaoAtivacao =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_INSTALACAO_ATIVACAO);

		// operação medidor
		OperacaoMedidor operacaoInstalacao = controladorMedidor.obterOperacaoMedidor(Long.parseLong(cInstalacao.getValor()));
		OperacaoMedidor operacaoAtivacao = controladorMedidor.obterOperacaoMedidor(Long.parseLong(cAtivacao.getValor()));
		OperacaoMedidor operacaoInstalacaoAtivacao =
				controladorMedidor.obterOperacaoMedidor(Long.parseLong(cInstalacaoAtivacao.getValor()));

		List<PontoConsumo> pontosFilhos = getControladorPontoConsumo().listarPontoDeConsumoFilhoDeImovelCondominio(idImovel);
		if (CODIGO_TIPO_ASSOCIACAO_MEDIDOR.equals(codigoAssociacao)) {
			for (PontoConsumo ponto : pontosFilhos) {
				if (ponto.getInstalacaoMedidor() == null && !listaOperacoesPermitidas.contains(operacaoInstalacao)) {
					listaOperacoesPermitidas.add(operacaoInstalacao);
					listaOperacoesPermitidas.add(operacaoInstalacaoAtivacao);
				}
				if (ponto.getInstalacaoMedidor() != null && !listaOperacoesPermitidas.contains(operacaoAtivacao)
						&& SituacaoConsumo.AGUARDANDO_ATIVACAO_DB.equals(ponto.getSituacaoConsumo().getDescricao())) {
					listaOperacoesPermitidas.add(operacaoAtivacao);
				}
			}
		} else if (CODIGO_TIPO_ASSOCIACAO_CORRETOR_VAZAO.equals(codigoAssociacao)) {
			for (PontoConsumo ponto : pontosFilhos) {
				if (ponto.getInstalacaoMedidor() != null && !listaOperacoesPermitidas.contains(operacaoInstalacao)) {
					listaOperacoesPermitidas.add(operacaoInstalacao);
				}
			}

		}
		return listaOperacoesPermitidas;
	}

	private void verificaOperacaoMedidor(String status, String numeroSerieMedidor, List<OperacaoMedidor> listaOperacoesPermitidas,
			OperacaoMedidor operacaoAtivacao, OperacaoMedidor operacaoMedidor) {
		if (status.isEmpty() && numeroSerieMedidor.isEmpty() && operacaoMedidor.getChavePrimaria() != operacaoAtivacao.getChavePrimaria()
				&& !listaOperacoesPermitidas.contains(operacaoMedidor)) {
			listaOperacoesPermitidas.add(operacaoMedidor);
		}
	}

	/**
	 * Criar tipo medicao.
	 *
	 * @return the tipo medicao
	 */
	private TipoMedicao criarTipoMedicao() {

		return (TipoMedicao) ServiceLocator.getInstancia().getBeanPorID(TipoMedicao.BEAN_ID_TIPO_MEDICAO);
	}

	/**
	 * Validar dados inclusao alteracao medidor.
	 *
	 * @param medidor the medidor
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosInclusaoAlteracaoMedidor(Medidor medidorLocal) throws GGASException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (medidorLocal.getIndicadorLote() == null) {
			stringBuilder.append(Medidor.INDICADOR_LOTE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);

		} else {
			if (medidorLocal.getIndicadorLote() == SIM) {
				if (StringUtils.isEmpty(medidorLocal.getNumeroInicial())) {
					stringBuilder.append(Medidor.NUMERO_INICIAL);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
				if ((medidorLocal.getQuantidadeLote() == null) || (medidorLocal.getQuantidadeLote() <= 0)) {
					stringBuilder.append(Medidor.QUANTIDADE_LOTE);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			} else {
				if (StringUtils.isEmpty(medidorLocal.getNumeroSerie())) {
					stringBuilder.append(Medidor.NUMERO_SERIE);
					stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
				}
			}
		}

		if (medidorLocal.getAnoFabricacao() != null && medidorLocal.getDataUltimaCalibracao() != null) {

			Calendar cal = Calendar.getInstance();
			cal.setTime(medidorLocal.getDataUltimaCalibracao());

			Integer anoUltimaCalibracao = cal.get(Calendar.YEAR);

			if (!(anoUltimaCalibracao >= medidorLocal.getAnoFabricacao())) {

				throw new NegocioException(ERRO_ANO_DATA_ULTIMA_CALIBRACAO_MAIOR_IGUAL_ANO_FABRICACAO_MEDIDOR, true);

			}

		}

		long chavePrimaria = medidorLocal.getChavePrimaria();
		if (chavePrimaria <= 0 && medidorLocal.getLocalArmazenagem() == null) {
			stringBuilder.append(MEDIDOR_LOCAL_ARMAZENAGEM);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		String chaveModoUsoIndependente = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_INDEPENDENTE);
		String chaveModoUsoVirtual = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL);
		String chaveModoUsoNormal = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_NORMAL);

		if (!chaveModoUsoIndependente.equals(String.valueOf(medidorLocal.getModoUso().getChavePrimaria()))
				&& verificaSeMedidorFazParteDeComposicao(chavePrimaria)) {
			throw new NegocioException(ERRO_MEDIDOR_INDEPENDENTE_FAZ_PARTE_DE_COMPOSICAO, true);
		}

		if (chaveModoUsoIndependente.equals(String.valueOf(medidorLocal.getModoUso().getChavePrimaria()))) {
			InstalacaoMedidor instalacaoMedidor = medidorLocal.getInstalacaoMedidor();
			if (instalacaoMedidor != null && instalacaoMedidor.getPontoConsumo() != null) {
				throw new NegocioException(ERRO_MEDIDOR_INSTALADO_EM_PONTO_CONSUMO, true);
			}
			if (medidorLocal.getCodigoMedidorSupervisorio() == null || "".equals(medidorLocal.getCodigoMedidorSupervisorio())) {
				stringBuilder.append(Medidor.CODIGO_MEDIDOR_SUPERVISORIO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
		}

		if (!chaveModoUsoVirtual.equals(String.valueOf(medidorLocal.getModoUso().getChavePrimaria())) && medidorLocal.getDigito() == null) {
			stringBuilder.append(Medidor.NUMERO_DIGITO_LEITURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (chaveModoUsoVirtual.equals(String.valueOf(medidorLocal.getModoUso().getChavePrimaria()))
				&& StringUtils.isEmpty(medidorLocal.getComposicaoVirtual())) {
			stringBuilder.append(Medidor.COMPOSICAO_VIRTUAL);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (chaveModoUsoIndependente.equals(String.valueOf(medidorLocal.getModoUso().getChavePrimaria()))
				|| chaveModoUsoNormal.equals(String.valueOf(medidorLocal.getModoUso().getChavePrimaria()))) {
			medidorLocal.setComposicaoVirtual(null);
		}

		// alteração
		if (chavePrimaria > 0) {
			Medidor medidor = fachada.obterMedidor(chavePrimaria, "movimentacoes");

			// Se o medidor for normal e estiver ativo, ele não pode ser alterado para virtual.
			if (chaveModoUsoNormal.equals(String.valueOf(medidor.getModoUso().getChavePrimaria())) && medidor.getInstalacaoMedidor() != null
					&& medidor.getInstalacaoMedidor().getPontoConsumo() != null
					&& getControladorPontoConsumo().isPontoConsumoAtivo(medidor.getInstalacaoMedidor().getPontoConsumo())
					&& chaveModoUsoVirtual.equals(String.valueOf(medidorLocal.getModoUso().getChavePrimaria()))) {
				throw new NegocioException(ERRO_MEDIDOR_NORMAL_ATIVO_NAO_PODE_SER_ALTERADO_PARA_ATIVO, true);
			}
			// Se o medidor for virtual e estiver ativo, ele não pode ser alterado para normal.
			if (chaveModoUsoVirtual.equals(String.valueOf(medidor.getModoUso().getChavePrimaria()))
					&& medidor.getInstalacaoMedidor() != null && medidor.getInstalacaoMedidor().getPontoConsumo() != null
					&& getControladorPontoConsumo().isPontoConsumoAtivo(medidor.getInstalacaoMedidor().getPontoConsumo())
					&& chaveModoUsoNormal.equals(String.valueOf(medidorLocal.getModoUso().getChavePrimaria()))) {
				throw new NegocioException(ERRO_MEDIDOR_VIRTUAL_ATIVO_NAO_PODE_SER_ALTERADO_PARA_NORMAL, true);
			}

			// Se o medidor for independente e estiver instalado ou ativo, ele não pode ser alterado para virtual ou normal.
			Long codigoMIAtivo = Long.valueOf(getControladorConstante()
					.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_ATIVO));
			Long codigoMIAguardandoAtivacao = Long.valueOf(getControladorConstante()
					.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_AGUARDANDO_ATIVACAO));

			if (medidor.getInstalacaoMedidor() != null
					&& chaveModoUsoIndependente.equals(String.valueOf(medidor.getModoUso().getChavePrimaria()))) {
				long idSituacaoAssociacaoMedidor = medidor.getSituacaoAssociacaoMedidorIndependente().getChavePrimaria();
				if (chaveModoUsoVirtual.equals(String.valueOf(medidorLocal.getModoUso().getChavePrimaria()))
						|| chaveModoUsoNormal.equals(String.valueOf(medidorLocal.getModoUso().getChavePrimaria()))) {
					if (medidor.getInstalacaoMedidor() != null && codigoMIAguardandoAtivacao.equals(idSituacaoAssociacaoMedidor)) {
						throw new NegocioException(ERRO_MEDIDOR_INDEPENDENTE_INSTALADO_ATIVO_NAO_PODE_SER_ALTERADO_PARA_VIRTUAL_NORMAL,
								true);
					}

					if (medidor.getInstalacaoMedidor() != null && codigoMIAtivo.equals(idSituacaoAssociacaoMedidor)) {
						throw new NegocioException(ERRO_MEDIDOR_INDEPENDENTE_INSTALADO_ATIVO_NAO_PODE_SER_ALTERADO_PARA_VIRTUAL_NORMAL,
								true);
					}
				}
			}
		}

		if (medidorLocal.getCodigoMedidorSupervisorio() != null) {
			validarEnderecoRemoto(medidorLocal.getCodigoMedidorSupervisorio(), medidorLocal.getChavePrimaria(), null);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - Constantes.LIMITE_CAMPO));
		}

	}

	private boolean verificaSeMedidorFazParteDeComposicao(long chavePrimaria) throws GGASException {

		// obtem apenas os medidores virtuais
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(Medidor.MODO_USO, Long.valueOf(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL)));
		Collection<Medidor> listaMedidorVirtual = consultarMedidor(filtro);

		// verifica se medidor compoe medidor virtual.
		for (Medidor medidorVirtual : listaMedidorVirtual) {
			String composicaoVirtual = medidorVirtual.getComposicaoVirtual();
			if (composicaoVirtual != null) {
				String[] arrayComposicaoVirtual;
				if (composicaoVirtual.contains("#")) {
					arrayComposicaoVirtual = composicaoVirtual.split("#");
				} else {
					arrayComposicaoVirtual = new String[1];
					arrayComposicaoVirtual[0] = composicaoVirtual;
				}
				for (String idMedidorComOperacao : arrayComposicaoVirtual) {
					String idMedidor = idMedidorComOperacao.substring(1);
					if (chavePrimaria == Long.parseLong(idMedidor)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # atualizarMedidor(br.com.ggas.medicao.medidor .Medidor)
	 */
	@Override
	public void atualizarMedidor(Medidor medidor, MedidorLocalArmazenagem localArmazenagemAnterior) throws GGASException {

		this.validarDadosInclusaoAlteracaoMedidor(medidor);
		if (medidor.getLocalArmazenagem() != null && localArmazenagemAnterior != null
				&& medidor.getLocalArmazenagem().getChavePrimaria() != localArmazenagemAnterior.getChavePrimaria()) {
			medidor.getMovimentacoes()
					.add(criarMovimentacaoTransferenciaMedidor(medidor, localArmazenagemAnterior, medidor.getLocalArmazenagem()));
		}
		this.atualizar(medidor);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor # inserirMedidor(br.com.ggas.medicao.medidor. Medidor)
	 */
	@Override
	public long inserirMedidor(Medidor medidor) throws GGASException {

		long chaveMedidor;
		Calendar.getInstance();

		this.validarDadosInclusaoAlteracaoMedidor(medidor);
		chaveMedidor = this.inserir(medidor);

		return chaveMedidor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#listarMotivoMovimentacaoMedidor()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<MotivoMovimentacaoMedidor> listarMotivoMovimentacaoMedidor() throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidadeMotivoMovimentacaoMedidor().getSimpleName());
		hql.append(" motivoMovimentacaoMedidor ");
		hql.append(WHERE);
		hql.append(" motivoMovimentacaoMedidor.habilitado = true ");
		hql.append(" order by descricao asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterInformacoesMedidor(java.lang.Long)
	 */
	@Override
	public Map<String, Object> obterInformacoesMedidor(Long idPontoConsumo) throws NegocioException {

		Map<String, Object> dados = null;

		Criteria criteria = this.createCriteria(PontoConsumo.class);
		criteria.createAlias(Medidor.INSTALACAO_MEDIDOR, Medidor.INSTALACAO_MEDIDOR, Criteria.LEFT_JOIN);
		criteria.createAlias("instalacaoMedidor.medidor", HistoricoOperacaoMedidor.MEDIDOR, Criteria.LEFT_JOIN);
		criteria.createAlias("medidor.modelo", Medidor.MODELO, Criteria.LEFT_JOIN);
		criteria.add(Restrictions.idEq(idPontoConsumo));

		PontoConsumo pontoConsumo = (PontoConsumo) criteria.uniqueResult();

		if (pontoConsumo.getInstalacaoMedidor() != null) {
			SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
			Medidor medidor = pontoConsumo.getInstalacaoMedidor().getMedidor();

			dados = new LinkedHashMap<String, Object>();

			if (medidor.getModelo() != null) {
				dados.put(MODELO_MEDIDOR, medidor.getModelo().getDescricao());
			} else {
				dados.put(MODELO_MEDIDOR, "");
			}

			if (medidor.getNumeroSerie() != null) {
				dados.put(Medidor.NUMERO_DE_SERIE, medidor.getNumeroSerie());
			} else {
				dados.put(Medidor.NUMERO_DE_SERIE, "");
			}

			if (pontoConsumo.getInstalacaoMedidor().getData() != null) {
				dados.put("dataInstalacao", df.format(pontoConsumo.getInstalacaoMedidor().getData()));
			} else {
				dados.put("dataInstalacao", "");
			}

			if (pontoConsumo.getInstalacaoMedidor().getDataAtivacao() != null) {
				dados.put("dataAtivacao", df.format(pontoConsumo.getInstalacaoMedidor().getDataAtivacao()));
			} else {
				dados.put("dataAtivacao", "");
			}

		}

		return dados;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#listarNumeroDigitosConsumo()
	 */
	@Override
	public Collection<Integer> listarNumeroDigitosConsumo() throws NegocioException {

		List<Integer> numeros = new ArrayList<Integer>();

		for (int i = 1; i <= Constantes.QTD_MAXIMO_DIGITOS_CORRETOR_VAZAO_COMBO_PARAMETRO_SISTEMA; i++) {
			numeros.add(i);
		}

		return numeros;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#listarTipoDadosEnderecoRemoto()
	 */
	@Override
	public Collection<EntidadeConteudo> listarTipoDadosEnderecoRemoto() throws NegocioException {

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		return controladorEntidadeConteudo.obterListaTipoAtributo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#listarTipoIntegracao()
	 */
	@Override
	public Collection<EntidadeConteudo> listarTipoIntegracao() throws NegocioException {

		ControladorEntidadeConteudo controladorEntidadeConteudo = (ControladorEntidadeConteudo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		return controladorEntidadeConteudo.obterListaTipoIntegracao();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#verificarRegistrosAssociados(java.lang.Long)
	 */
	@Override
	public boolean verificarRegistrosAssociados(Long idMedidor) throws NegocioException {

		boolean retorno = Boolean.FALSE;
		StringBuilder hql = new StringBuilder();

		hql.append(" select count(*) from "); // NOSONAR {não faz sentido criar constante para esta string }
		hql.append(getClasseEntidadeInstalacaoMedidor().getSimpleName());
		hql.append(" MI  where MI.medidor.chavePrimaria = ?");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idMedidor);
		Long result = (Long) query.uniqueResult();
		if (result >= 1) {
			retorno = Boolean.TRUE;
		} else {
			hql = new StringBuilder();

			hql.append(" select count(*) from "); // NOSONAR {não faz sentido criar constante para esta string }
			hql.append(getClasseEntidadeMovimentacaoMedidor().getSimpleName());
			hql.append(" MM  where MM.medidor.chavePrimaria = ?");
			hql.append(" and (select chavePrimaria from ");
			hql.append(getClasseEntidadeMotivoMovimentacaoMedidor().getSimpleName());
			hql.append(" where descricao like 'Aquisição') <> MM.motivoMovimentacaoMedidor.chavePrimaria ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong(0, idMedidor);
			result = (Long) query.uniqueResult();
			if (result >= 1) {
				retorno = Boolean.TRUE;
			} else {
				hql = new StringBuilder();

				hql.append(" select count(*) from "); // NOSONAR {não faz sentido criar constante para esta string }
				hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
				hql.append(" MOH  where MOH.medidor.chavePrimaria = ?");
				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
				query.setLong(0, idMedidor);
				result = (Long) query.uniqueResult();

				if (result >= 1) {
					retorno = Boolean.TRUE;
				} else {
					hql = new StringBuilder();

					hql.append(" select count(*) from "); // NOSONAR {não faz sentido criar constante para esta string }
					hql.append(getClasseEntidadeVazaoCorretorHistoricoOperacao().getSimpleName());
					hql.append(" VCOH  where VCOH.medidor.chavePrimaria = ?");
					query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
					query.setLong(0, idMedidor);
					result = (Long) query.uniqueResult();

					if (result >= 1) {
						retorno = Boolean.TRUE;
					}
				}
			}
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#consultarOperacaoMedidor(java.lang.Long)
	 */
	@Override
	public String consultarOperacaoMedidor(Long chavePrimaria) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select descricao from ");
		hql.append(getClasseEntidadeOperacaoMedidor().getSimpleName());
		hql.append(" where chavePrimaria = :chavePrimaria");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);

		return (String) query.uniqueResult();
	}

	/**
	 * Buscar codigo local armazenagem retirada.
	 *
	 * @return the medidor local armazenagem
	 */
	private MedidorLocalArmazenagem buscarCodigoLocalArmazenagemRetirada() {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeMedidorLocalArmazenagem().getSimpleName());
		hql.append(" where oficina = true");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (MedidorLocalArmazenagem) query.setMaxResults(1).uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#consultarMarcaMedidorEmUso(java.lang.Long)
	 */
	@Override
	public Long consultarMarcaMedidorEmUso(Long marcaMedidor) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select marcaMedidor.chavePrimaria from ");
		hql.append(getClasseEntidadeMedidor().getSimpleName());
		hql.append(" where marcaMedidor = :marcaMedidor");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(Medidor.MARCA_MEDIDOR, marcaMedidor);

		return (Long) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#consultarModeloMedidorEmUso(java.lang.Long)
	 */
	@Override
	public Long consultarModeloMedidorEmUso(Long modeloMedidor) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select modelo.chavePrimaria from ");
		hql.append(getClasseEntidadeMedidor().getSimpleName());
		hql.append(" where modelo = :modeloMedidor");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(MODELO_MEDIDOR, modeloMedidor);

		return (Long) query.uniqueResult();
	}

	private String getHQLConsultaMedidorExistentePorSerieMarca() {

		StringBuilder hql = new StringBuilder();
		hql.append(" select medidor.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" medidor where ");
		hql.append(" medidor.numeroSerie = ?");
		hql.append(" and medidor.marcaMedidor = ?");

		return hql.toString();
	}

	/**
	 * Consultar ultimo historico operacao por operacao.
	 *
	 * @param operacaoRetirada the operacao retirada
	 * @param operacaoSubstituicao the operacao substituicao
	 * @param pontoConsumo the ponto consumo
	 * @param isMedidor the is medidor
	 * @param medidor the medidor
	 * @return the date
	 */
	@Override
	public Date consultarUltimoHistoricoOperacaoPorOperacao(Long operacaoRetirada, Long operacaoSubstituicao, Long pontoConsumo,
			Boolean isMedidor, Long medidor) {

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select max(dataRealizada) from ");

		if (isMedidor != null && isMedidor) {

			hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
		} else {

			hql.append(getClasseEntidadeVazaoCorretorHistoricoOperacao().getSimpleName());
		}

		hql.append(WHERE);

		if (!Util.isEmpty(pontoConsumo)) {
			hql.append(" pontoConsumo.chavePrimaria = :pontoConsumo ");
		}
		if (!Util.isEmpty(medidor)) {
			hql.append(" medidor.chavePrimaria = :medidor ");
		}

		hql.append(" and (operacaoMedidor.chavePrimaria = :operacaoRetirada or operacaoMedidor.chavePrimaria = :operacaoSubstituicao) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (!Util.isEmpty(pontoConsumo)) {
			query.setLong("pontoConsumo", pontoConsumo); // NOSONAR {não faz sentido criar constante para esta string }
		}

		if (!Util.isEmpty(medidor)) {
			query.setLong("medidor", medidor); // NOSONAR {não faz sentido criar constante para esta string }
		}

		query.setLong("operacaoRetirada", operacaoRetirada); // NOSONAR {não faz sentido criar constante para esta string }
		query.setLong("operacaoSubstituicao", operacaoSubstituicao);

		return (Date) query.uniqueResult();
	}

	/**
	 * Validar data instalacao inferior data ultima retirada.
	 *
	 * @param dados the dados
	 * @param isMedidor the is medidor
	 * @throws NegocioException the negocio exception
	 */
	private void validarDataInstalacaoInferiorDataUltimaRetirada(Map<String, Object> dados, Boolean isMedidor) throws NegocioException {

		OperacaoMedidor operacaoRetirada = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_RETIRADA, OperacaoMedidorImpl.class);
		OperacaoMedidor operacaoSubstituicao = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_SUBSTITUICAO, OperacaoMedidorImpl.class);

		Long idMedidorIndependente = (Long) dados.get(ID_MEDIDOR_INDEPENDENTE);
		Long idPontoConsumo = (Long) dados.get(Medidor.ID_PONTO_CONSUMO);

		Date dataRetirada = this.consultarUltimoHistoricoOperacaoPorOperacao(operacaoRetirada.getChavePrimaria(),
				operacaoSubstituicao.getChavePrimaria(), idPontoConsumo, isMedidor, idMedidorIndependente);

		Date dataNovaInstalacao = null;

		if (isMedidor != null && isMedidor) {

			dataNovaInstalacao = (Date) dados.get(DATA_MEDIDOR);
		} else {

			Date data = (Date) dados.get(DATA_CORRETOR_VAZAO);
			String hora = (String) dados.get(HORA_CORRETOR_VAZAO);
			dataNovaInstalacao = this.obterDataHoraMedidorCorretorVazao(data, hora);
		}

		if (dataRetirada != null && dataNovaInstalacao.compareTo(dataRetirada) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_INSTALACAO_INFERIOR_DATA_ULTIMA_RETIRADA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#consultarDataInstalacaoAtivacaoRetiradaAnteriorDataLeitura(java.util.Date,
	 * java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Date[]> consultarDataInstalacaoAtivacaoRetiradaAnteriorDataLeitura(Date dataLeitura, Long instalacaoMedidor)
			throws NegocioException {
		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select instalacao.dataRealizada,  ");
		hql.append(" (select ativacao.dataRealizada from ");
		hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
		hql.append(" ativacao ");
		hql.append(" where instalacao.instalacaoMedidor.chavePrimaria = ativacao.instalacaoMedidor.chavePrimaria "
				+ "and instalacao.dataRealizada <= ativacao.dataRealizada ");
		hql.append(" and ativacao.operacaoMedidor.chavePrimaria = :operacaoAtivacao and ativacao.dataRealizada <= :dataLeitura ) , ");

		hql.append(" (select retirada.dataRealizada ");
		hql.append(FROM);
		hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
		hql.append(" retirada ");
		hql.append(" where instalacao.instalacaoMedidor.chavePrimaria = retirada.instalacaoMedidor.chavePrimaria "
				+ "and instalacao.dataRealizada <= retirada.dataRealizada ");
		hql.append(" and (retirada.operacaoMedidor.chavePrimaria = :operacaoRetirada or retirada.operacaoMedidor.chavePrimaria "
				+ "= :operacaoSubstituicao) and retirada.dataRealizada <= :dataLeitura) ");

		hql.append(FROM);
		hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
		hql.append(" instalacao ");
		hql.append(WHERE);
		hql.append(" instalacao.instalacaoMedidor.chavePrimaria = :instalacaoMedidor ");
		hql.append(" and instalacao.operacaoMedidor.chavePrimaria = :operacaoInstalacao ");
		hql.append(" and instalacao.dataRealizada <= :dataLeitura ");
		hql.append(" order by instalacao.dataRealizada desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setDate("dataLeitura", dataLeitura);
		query.setLong("instalacaoMedidor", instalacaoMedidor);
		query.setLong("operacaoInstalacao", OperacaoMedidor.CODIGO_INSTALACAO);
		query.setLong("operacaoAtivacao", OperacaoMedidor.CODIGO_ATIVACAO);
		query.setLong("operacaoSubstituicao", OperacaoMedidor.CODIGO_SUBSTITUICAO);
		query.setLong("operacaoRetirada", OperacaoMedidor.CODIGO_RETIRADA);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#verificarStatusPontoConsumo(java.util.Date,
	 * br.com.ggas.cadastro.imovel.PontoConsumo)
	 */
	@Override
	public Map<String, Object> verificarStatusPontoConsumo(Date dataRealizacaoLeitura, PontoConsumo pontoConsumoAux)
			throws NegocioException {

		Map<String, Object> mapaInstalacaoMedidor = new LinkedHashMap<String, Object>();

		if (pontoConsumoAux != null) {

			InstalacaoMedidor instalacaoMedidor = obterInstalacaoMedidorPontoConsumo(pontoConsumoAux.getChavePrimaria());

			// verifica o status do medidor no momento que a leitura foi feita
			List<Date[]> listaDatasOperacaoMedidor = this.consultarDataInstalacaoAtivacaoRetiradaAnteriorDataLeitura(dataRealizacaoLeitura,
					instalacaoMedidor.getChavePrimaria());

			if (listaDatasOperacaoMedidor != null && !listaDatasOperacaoMedidor.isEmpty()) {

				Object[] arrayDatas = listaDatasOperacaoMedidor.get(0);
				Date instalacao = (Date) arrayDatas[0];
				Date ativacao = (Date) arrayDatas[1];
				Date retirada = (Date) arrayDatas[2];

				// se não tiver medidor instalado ou se tiver medidor instalado mas, tiver sido retirado ou se tiver medidor instalado mas,
				// não tiver ativado então, o ponto de consumo está inativo.
				if (instalacao == null || ativacao == null || retirada != null) {
					mapaInstalacaoMedidor.put("isStatusAtivo", Boolean.FALSE);
				} else {
					// se o ponto de consumo estiver ativo pega o medidor que está instalado
					mapaInstalacaoMedidor.put("isStatusAtivo", Boolean.TRUE);
					mapaInstalacaoMedidor.put(Medidor.INSTALACAO_MEDIDOR, instalacaoMedidor);
				}
			}
		}

		return mapaInstalacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#consultarDataInstalacaoRetiradaAnteriorDataLeitura(java.util.Date,
	 * java.lang.Long)
	 */
	@Override
	public Timestamp consultarDataInstalacaoRetiradaAnteriorDataLeitura(Date dataLeitura, Long pontoConsumo, Long medidor)
			throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select max(vaCoHistOper.dataRealizada)  ");
		hql.append(FROM);
		hql.append(getClasseEntidadeVazaoCorretorHistoricoOperacao().getSimpleName());
		hql.append(" vaCoHistOper ");
		hql.append(" where vaCoHistOper.operacaoMedidor.chavePrimaria = :operacaoInstalacao ");

		if (pontoConsumo != null) {
			hql.append(" and vaCoHistOper.pontoConsumo.chavePrimaria = :chavePrimariaPontoConsumo ");
		}
		if (medidor != null) {
			hql.append(" and vaCoHistOper.medidor.chavePrimaria = :chavePrimariaMedidor ");
		}

		hql.append(" and vaCoHistOper.dataRealizada <= :dataLeitura ");
		hql.append(" and not exists (select vaCoHistOperAux.dataRealizada  ");
		hql.append(FROM);
		hql.append(getClasseEntidadeVazaoCorretorHistoricoOperacao().getSimpleName());
		hql.append(" vaCoHistOperAux ");
		hql.append(" where vaCoHistOperAux.operacaoMedidor.chavePrimaria = :operacaoRetirada ");

		if (pontoConsumo != null) {
			hql.append(" and vaCoHistOperAux.pontoConsumo.chavePrimaria = vaCoHistOper.pontoConsumo.chavePrimaria ");
		}
		if (medidor != null) {
			hql.append(" and vaCoHistOperAux.medidor.chavePrimaria = vaCoHistOper.medidor.chavePrimaria ");
		}

		hql.append(" and vaCoHistOperAux.vazaoCorretor.chavePrimaria = vaCoHistOper.vazaoCorretor.chavePrimaria ");
		hql.append(" and vaCoHistOperAux.dataRealizada >= vaCoHistOper.dataRealizada ");
		hql.append(" and vaCoHistOperAux.dataRealizada <= :dataLeitura) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setDate("dataLeitura", dataLeitura);

		if (pontoConsumo != null) {
			query.setLong("chavePrimariaPontoConsumo", pontoConsumo);
		}
		if (medidor != null) {
			query.setLong(PARAM_CHAVE_PRIMARIA_MEDIDOR, medidor);
		}

		query.setLong("operacaoInstalacao", OperacaoMedidor.CODIGO_INSTALACAO);
		query.setLong("operacaoRetirada", OperacaoMedidor.CODIGO_RETIRADA); // NOSONAR {não faz sentido criar constante para esta string }

		return (Timestamp) query.uniqueResult();

	}

	/**
	 * Validar dados reativacao medidor.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosReativacaoMedidor(Map<String, Object> dados) throws NegocioException {

		getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO));

		Date dataReativacao = (Date) dados.get(DATA_MEDIDOR);
		Long idMedidorIndependente = (Long) dados.get(ID_MEDIDOR_INDEPENDENTE);
		Long idPontoConsumo = (Long) dados.get(Medidor.ID_PONTO_CONSUMO);

		Date dataBloqueio = this.consultarUltimoHistoricoOperacaoPorOperacao(OperacaoMedidor.CODIGO_BLOQUEIO,
				OperacaoMedidor.CODIGO_BLOQUEIO, idPontoConsumo, true, idMedidorIndependente);

		if (dados.get(InstalacaoMedidor.FUNCIONARIO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_FUNCIONARIO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dataReativacao.compareTo(dataBloqueio) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_BLOQUEIO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(MEDIDOR_MOTIVO_OPERACAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

	}

	/**
	 * Validar dados bloqueio medidor.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosBloqueioMedidor(Map<String, Object> dados) throws NegocioException {

		PontoConsumo pontoConsumo = (PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO));

		Date dataBloqueio = (Date) dados.get(DATA_MEDIDOR);
		Date dataInstalacao = pontoConsumo.getInstalacaoMedidor().getData();
		Date dataAtivacao = pontoConsumo.getInstalacaoMedidor().getDataAtivacao();

		if (dados.get(InstalacaoMedidor.FUNCIONARIO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_FUNCIONARIO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dataBloqueio.compareTo(dataInstalacao) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_INSTALACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if ((dataAtivacao != null) && (dataBloqueio.compareTo(dataAtivacao) < 0)) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_ATIVACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(MEDIDOR_MOTIVO_OPERACAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

	}

	/**
	 * Validar dados bloqueio medidor independente.
	 *
	 * @param dados the dados
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosBloqueioMedidorIndependente(Map<String, Object> dados) throws NegocioException {

		Medidor medidor = obterMedidor((Long) dados.get(ID_MEDIDOR_INDEPENDENTE));

		Date dataBloqueio = (Date) dados.get(DATA_MEDIDOR);
		Date dataInstalacao = medidor.getInstalacaoMedidor().getData();
		Date dataAtivacao = medidor.getInstalacaoMedidor().getDataAtivacao();

		if (dados.get(InstalacaoMedidor.FUNCIONARIO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_FUNCIONARIO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

		if (dataBloqueio.compareTo(dataInstalacao) < 0) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_INSTALACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if ((dataAtivacao != null) && (dataBloqueio.compareTo(dataAtivacao) < 0)) {
			throw new NegocioException(ControladorMedidor.ERRO_DATA_MENOR_DATA_ATIVACAO_MEDIDOR_ASSOCIACAO_MEDIDOR, true);
		}

		if (dados.get(MEDIDOR_MOTIVO_OPERACAO) == null) {
			throw new NegocioException(ControladorMedidor.ERRO_MOTIVO_NAO_INFORMADO_ASSOCIACAO_MEDIDOR, true);
		}

	}

	/**
	 * Inserir dados bloqueio medidor.
	 *
	 * @param dados the dados
	 * @param pontoConsumo 
	 * @throws NegocioException the negocio exception
	 * @throws ConcorrenciaException the concorrencia exception
	 */
	private void inserirDadosBloqueioMedidor(Map<String, Object> dados, PontoConsumo pontoConsumo, Boolean isOperacaoConjunta) throws NegocioException, ConcorrenciaException {

		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
		
		Date data = (Date) dados.get(DATA_MEDIDOR);
		String lacre = (String) dados.get(LACRE);
		String lacreDois = (String) dados.get(LACRE_DOIS);
		String lacreTres = (String) dados.get(LACRE_TRES);

		BigDecimal leitura = BigDecimal.valueOf(Double.valueOf((String) dados.get(LEITURA_ANTERIOR)));

		Funcionario funcionario = (Funcionario) controladorFuncionario.criar();
		funcionario.setChavePrimaria((Long) dados.get(InstalacaoMedidor.FUNCIONARIO));

		SituacaoConsumo situacaoConsumo = (SituacaoConsumo) getControladorPontoConsumo().criarSituacaoConsumo();

		Medidor medidor = this.obterMedidor((Long) dados.get(CHAVE_MEDIDOR_ANTERIOR));
		OperacaoMedidor operacaoMedidor = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_BLOQUEIO, OperacaoMedidorImpl.class);

		MotivoOperacaoMedidor motivoOperacaoMedidor = null;
		if(dados.get(MEDIDOR_MOTIVO_OPERACAO) != null) {
			motivoOperacaoMedidor = (MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);
		}
		
		if(motivoOperacaoMedidor != null) {
			if(motivoOperacaoMedidor.getDescricao().equals("A PEDIDO DO CLIENTE")) {
				situacaoConsumo.setChavePrimaria(
						Long.parseLong(getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_SUSPENSO)));
			} else if (motivoOperacaoMedidor.getDescricao().equals("FALTA DE PAGAMENTO")) {
				situacaoConsumo.setChavePrimaria(
						Long.parseLong(getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_SUPRIMIDO_DEFINITIVO)));
			} else {
				situacaoConsumo.setChavePrimaria(
						Long.parseLong(getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_BLOQUEADO)));
			}
		}
		
		pontoConsumo.setSituacaoConsumo(situacaoConsumo);
		InstalacaoMedidor instalacaoMedidor = pontoConsumo.getInstalacaoMedidor();

		atualizarAvisoDataCorte(data, pontoConsumo);

		this.inserirHistoricoOperacaoMedidor(operacaoMedidor, data, data, medidor, null, leitura, funcionario, motivoOperacaoMedidor, pontoConsumo,
				instalacaoMedidor, lacre, lacreDois, lacreTres, isOperacaoConjunta);

	}

	/**
	 * Inserir dados bloqueio medidor independente.
	 *
	 * @param dados the dados
	 * @throws GGASException the GGASException
	 */
	private void inserirDadosBloqueioMedidorIndependente(Map<String, Object> dados) throws GGASException {

		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);

		Date data = (Date) dados.get(DATA_MEDIDOR);
		String lacre = (String) dados.get(LACRE);
		String lacreDois = (String) dados.get(LACRE_DOIS);
		String lacreTres = (String) dados.get(LACRE_TRES);
		
		BigDecimal leitura = BigDecimal.valueOf(Double.valueOf((String) dados.get(LEITURA_ANTERIOR)));

		Funcionario funcionario = (Funcionario) controladorFuncionario.criar();
		funcionario.setChavePrimaria((Long) dados.get(InstalacaoMedidor.FUNCIONARIO));

		Medidor medidor = this.obterMedidor((Long) dados.get(ID_MEDIDOR_INDEPENDENTE));

		EntidadeConteudo situacaoAssociacaoMedidorIndependente = fachada.obterEntidadeConteudo(Long
				.parseLong(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_BLOQUEADO)));
		medidor.setSituacaoAssociacaoMedidorIndependente(situacaoAssociacaoMedidorIndependente);

		atualizar(medidor);

		OperacaoMedidor operacaoMedidor = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_BLOQUEIO, OperacaoMedidorImpl.class);
		
		MotivoOperacaoMedidor motivoOperacaoMedidor = null;
		if(dados.get(MEDIDOR_MOTIVO_OPERACAO) != null) {
			motivoOperacaoMedidor = (MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);
		}

		InstalacaoMedidor instalacaoMedidor = medidor.getInstalacaoMedidor();

		this.inserirHistoricoOperacaoMedidor(operacaoMedidor, data, data, medidor, null, leitura, funcionario, motivoOperacaoMedidor, null,
				instalacaoMedidor, lacre, lacreDois, lacreTres, Boolean.FALSE);

	}

	/**
	 * Atualizar aviso data corte.
	 *
	 * @param data the data
	 * @param pontoConsumo the ponto consumo
	 * @throws ConcorrenciaException the concorrencia exception
	 * @throws NegocioException the negocio exception
	 */
	private void atualizarAvisoDataCorte(Date data, PontoConsumo pontoConsumo) throws ConcorrenciaException, NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(Medidor.ID_PONTO_CONSUMO, pontoConsumo.getChavePrimaria());
		Collection<AvisoCorte> avisos = controladorAvisoCorte.listar(filtro);
		for (AvisoCorte avisoCorte : avisos) {
			avisoCorte.setDataCorte(data);

			controladorAvisoCorte.atualizar(avisoCorte, AvisoCorteImpl.class);
		}
	}

	/**
	 * Inserir dados reativacao medidor.
	 * 
	 * @param dados the dados
	 * @param pontoConsumo 
	 * @throws GGASException the exception
	 */
	private void inserirDadosReativacaoMedidor(Map<String, Object> dados, PontoConsumo pontoConsumo,  Boolean isOperacaoConjunta) throws GGASException {

		ControladorFuncionario controladorFuncionario =
				(ControladorFuncionario) ServiceLocator.getInstancia().getBeanPorID(ControladorFuncionario.BEAN_ID_CONTROLADOR_FUNCIONARIO);
		
		ControladorInstalacaoMedidor controladorInstalacaoMedidor =
				(ControladorInstalacaoMedidor) ServiceLocator.getInstancia().getBeanPorID(ControladorInstalacaoMedidor.BEAN_ID_CONTROLADOR_INSTALACAO_MEDIDOR);

		Date data = (Date) dados.get(DATA_MEDIDOR);
		String lacre = (String) dados.get(LACRE);
		String lacreDois = (String) dados.get(LACRE_DOIS);
		String lacreTres = (String) dados.get(LACRE_TRES);

		BigDecimal leitura = BigDecimal.valueOf(Double.valueOf((String) dados.get(LEITURA_ANTERIOR)));

		Funcionario funcionario = (Funcionario) controladorFuncionario.criar();
		funcionario.setChavePrimaria((Long) dados.get(InstalacaoMedidor.FUNCIONARIO));

		SituacaoConsumo situacaoConsumo = (SituacaoConsumo) getControladorPontoConsumo().criarSituacaoConsumo();
		situacaoConsumo.setChavePrimaria(
				Long.parseLong(getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_ATIVO)));

		pontoConsumo.setSituacaoConsumo(situacaoConsumo);

		Medidor medidor = this.obterMedidor((Long) dados.get(CHAVE_MEDIDOR_ANTERIOR));

		OperacaoMedidor operacaoMedidor = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_REATIVACAO, OperacaoMedidorImpl.class);

		MotivoOperacaoMedidor motivoOperacaoMedidor = null;
		if(dados.get(MEDIDOR_MOTIVO_OPERACAO) != null) {
			motivoOperacaoMedidor = (MotivoOperacaoMedidor) this.obter((Long) dados.get(MEDIDOR_MOTIVO_OPERACAO), MotivoOperacaoMedidorImpl.class);
		}
		
		InstalacaoMedidor instalacaoMedidor = pontoConsumo.getInstalacaoMedidor();
		instalacaoMedidor.setDataAtivacao(data);
		instalacaoMedidor.setLeituraAtivacao(leitura);
		instalacaoMedidor.setFuncionario(funcionario);
		controladorInstalacaoMedidor.atualizarSaveOrUpdate(instalacaoMedidor, InstalacaoMedidorImpl.class);
		
		this.inserirHistoricoOperacaoMedidor(operacaoMedidor, data, data, medidor, null, leitura, funcionario, motivoOperacaoMedidor, pontoConsumo,
				instalacaoMedidor, lacre, lacreDois, lacreTres, isOperacaoConjunta);

	}

	/**
	 * Inserir dados reativacao medidor independente.
	 *
	 * @param dados the dados
	 * @throws GGASException the GGASException
	 */
	private void inserirDadosReativacaoMedidorIndependente(Map<String, Object> dados) throws GGASException {

		Date data = (Date) dados.get(DATA_MEDIDOR);
		String lacre = (String) dados.get(LACRE);
		String lacreDois = (String) dados.get(LACRE_DOIS);
		String lacreTres = (String) dados.get(LACRE_TRES);

		Medidor medidor = this.obterMedidor((Long) dados.get(CHAVE_MEDIDOR_ANTERIOR));

		EntidadeConteudo situacaoAssociacaoMedidorIndependente = fachada.obterEntidadeConteudo(
				Long.parseLong(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_ATIVO)));
		medidor.setSituacaoAssociacaoMedidorIndependente(situacaoAssociacaoMedidorIndependente);

		OperacaoMedidor operacaoMedidor = (OperacaoMedidor) this.obter(OperacaoMedidor.CODIGO_REATIVACAO, OperacaoMedidorImpl.class);

		InstalacaoMedidor instalacaoMedidor = medidor.getInstalacaoMedidor();
		instalacaoMedidor.setDataAtivacao(data);

		atualizar(medidor);

		this.inserirHistoricoOperacaoMedidor(operacaoMedidor, data, data, medidor, null, instalacaoMedidor.getLeitura(),
				instalacaoMedidor.getFuncionario(), null, null, instalacaoMedidor, lacre, lacreDois, lacreTres, Boolean.FALSE);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#obterHistoricoOperacaoMedidorPorPeriodo(java.lang.Long, java.util.Date,
	 * java.util.Date)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<HistoricoOperacaoMedidor> obterHistoricoOperacaoMedidorPorPeriodo(Long chavePrimariaOperacao, Date dataInicio,
			Date dataFim) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeHistoricoOperacaoMedidor().getSimpleName());
		hql.append(" historico "); // NOSONAR {não faz sentido criar constante para esta string }
		hql.append(" where historico.operacaoMedidor.chavePrimaria = :chavePrimariaOperacao ");
		hql.append(" and historico.dataRealizada between :dataInicialPeriodo and :dataFinalPeriodo ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(PARAM_CHAVE_PRIMARIA_OPERACAO, chavePrimariaOperacao);

		Util.adicionarRestricaoDataSemHora(query, dataInicio, "dataInicialPeriodo", Boolean.TRUE); // NOSONAR {não faz sentido criar
																									// constante para esta string }
		Util.adicionarRestricaoDataSemHora(query, dataFim, "dataFinalPeriodo", Boolean.FALSE); // NOSONAR {não faz sentido criar constante
																								// para esta string }

		return query.list();

	}

	/**
	 * Criar movimentacao transferencia medidor.
	 *
	 * @param medidor the medidor
	 * @param localArmazenagemOrigem the local armazenagem origem
	 * @param localArmazenagemDestino the local armazenagem destino
	 * @return the movimentacao medidor
	 * @throws NegocioException the negocio exception
	 */
	public MovimentacaoMedidor criarMovimentacaoTransferenciaMedidor(Medidor medidor, MedidorLocalArmazenagem localArmazenagemOrigem,
			MedidorLocalArmazenagem localArmazenagemDestino) throws NegocioException {

		Calendar calendar = Calendar.getInstance();

		MovimentacaoMedidor movimentacaoMedidor = (MovimentacaoMedidor) criarMovimentacaoMedidor();
		movimentacaoMedidor.setDataMovimento(calendar.getTime());
		movimentacaoMedidor.setHabilitado(true);
		movimentacaoMedidor.setDadosAuditoria(medidor.getDadosAuditoria());
		movimentacaoMedidor.setMedidor(medidor);
		movimentacaoMedidor.setUltimaAlteracao(new Date());

		long codigoMotivoMovimentacao = Long.parseLong(
				getControladorConstante().obterValorConstanteSistemaPorCodigo(Constantes.C_MOTIVO_MOVIMENTACAO_MEDIDOR_TRANSFERENCIA));

		MotivoMovimentacaoMedidor motivoMovimentacaoMedidor =
				(MotivoMovimentacaoMedidor) this.obter(codigoMotivoMovimentacao, MotivoMovimentacaoMedidorImpl.class);
		movimentacaoMedidor.setMotivoMovimentacaoMedidor(motivoMovimentacaoMedidor);

		movimentacaoMedidor.setLocalArmazenagemOrigem(localArmazenagemOrigem);
		movimentacaoMedidor.setLocalArmazenagemDestino(localArmazenagemDestino);

		return movimentacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#obterVazaoCorretor(long, java.lang.String[])
	 */
	@Override
	public VazaoCorretor obterVazaoCorretor(long chavePrimaria, String... propriedadesLazy) throws NegocioException {

		return (VazaoCorretor) super.obter(chavePrimaria, getClasseEntidadeVazaoCorretor(), propriedadesLazy);
	}

	/**
	 * Obter historico operacao medidor.
	 *
	 * @param idPontoConsumo the id ponto consumo
	 * @param dataAutorizacao the data autorizacao
	 * @param idOperacaoMedidor the id operacao medidor
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 * @deprecated utilizar obterHistoricoOperacaoMedidor(FiltroHistoricoOperacaoMedidor filtro)
	 */
	@Override
	@Deprecated
	public Collection<HistoricoOperacaoMedidor> obterHistoricoOperacaoMedidor(Long idPontoConsumo, Date dataAutorizacao,
			Long idOperacaoMedidor) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeHistoricoOperacaoMedidor());

		criteria.createAlias(HistoricoOperacaoMedidor.PONTO_CONSUMO, HistoricoOperacaoMedidor.PONTO_CONSUMO);
		criteria.createAlias(HistoricoOperacaoMedidor.OPERACAO_MEDIDOR, HistoricoOperacaoMedidor.OPERACAO_MEDIDOR);

		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, idPontoConsumo));
		}

		if (dataAutorizacao != null) {

			criteria.add(Restrictions.ge(HistoricoOperacaoMedidor.DATA_REALIZADA, DataUtil.gerarDataHmsZerados(dataAutorizacao)));
		}

		if (idOperacaoMedidor != null) {
			criteria.add(Restrictions.eq("operacaoMedidor.chavePrimaria", idOperacaoMedidor));

		}

		criteria.addOrder(Order.asc(HistoricoOperacaoMedidor.DATA_REALIZADA));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#obterUltimoBloqueio(java.lang.Long)
	 */
	@Override
	public HistoricoOperacaoMedidor obterUltimoBloqueio(Long idPontoConsumo) {

		FiltroHistoricoOperacaoMedidor filtro = new FiltroHistoricoOperacaoMedidor();
		filtro.setIdPontoConsumo(idPontoConsumo);
		filtro.setIdOperacaoMedidor(OperacaoMedidor.CODIGO_BLOQUEIO);
		filtro.setOrdenacaoDataRealizada(Ordenacao.DESCENDENTE);
		filtro.setLimiteResultados(INTEGER_OBTER_PRIMEIRO_ITEM);
		return obterUnicoHistoricoOperacaoMedidor(filtro);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#obterHistoricoOperacaoMedidor(br.com.ggas.medicao.medidor.impl.
	 * FiltroHistoricoOperacaoMedidor)
	 */
	@Override
	public Collection<HistoricoOperacaoMedidor> obterHistoricoOperacaoMedidor(FiltroHistoricoOperacaoMedidor filtro) {

		Criteria criteria = montarHibernateCriteria(filtro);
		return criteria.list();
	}

	/**
	 * Obter unico historico operacao medidor.
	 *
	 * @param filtro the filtro
	 * @return the historico operacao medidor
	 */
	@Override
	public HistoricoOperacaoMedidor obterUnicoHistoricoOperacaoMedidor(FiltroHistoricoOperacaoMedidor filtro) {

		Criteria criteria = montarHibernateCriteria(filtro);
		return (HistoricoOperacaoMedidor) criteria.uniqueResult();
	}

	/**
	 * Montar hibernate criteria.
	 *
	 * @param filtro the filtro
	 * @return the criteria
	 */
	private Criteria montarHibernateCriteria(FiltroHistoricoOperacaoMedidor filtro) {

		Criteria criteria = createCriteria(getClasseEntidadeHistoricoOperacaoMedidor());

		criteria.createAlias(HistoricoOperacaoMedidor.PONTO_CONSUMO, HistoricoOperacaoMedidor.PONTO_CONSUMO);
		criteria.createAlias(HistoricoOperacaoMedidor.OPERACAO_MEDIDOR, HistoricoOperacaoMedidor.OPERACAO_MEDIDOR);
		
		Long idPontoConsumo = filtro.getIdPontoConsumo();
		if ((idPontoConsumo != null) && (idPontoConsumo > 0)) {
			criteria.add(Restrictions.eq(PONTO_CONSUMO_CHAVE_PRIMARIA, idPontoConsumo));
		}

		Date dataAutorizacao = filtro.getDataAutorizacao();
		if (dataAutorizacao != null) {
			criteria.add(Restrictions.ge(HistoricoOperacaoMedidor.DATA_REALIZADA, dataAutorizacao));
		}

		Long idOperacaoMedidor = filtro.getIdOperacaoMedidor();
		if (idOperacaoMedidor != null) {
			criteria.add(Restrictions.eq("operacaoMedidor.chavePrimaria", idOperacaoMedidor));
		}
		
		Long idMedidorDiferente = filtro.getIdMedidorDiferente();
		if(idMedidorDiferente != null) {
			criteria.add(Restrictions.ne("medidor.chavePrimaria", idMedidorDiferente));
		}

		Ordenacao ordenacaoDataRealizada = filtro.getOrdenacaoDataRealizada();
		if (ordenacaoDataRealizada != null) {
			if (ordenacaoDataRealizada == Ordenacao.ASCENDENTE) {
				criteria.addOrder(Order.asc(HistoricoOperacaoMedidor.DATA_REALIZADA));
			} else if (ordenacaoDataRealizada == Ordenacao.DESCENDENTE) {
				criteria.addOrder(Order.desc(HistoricoOperacaoMedidor.DATA_REALIZADA));
			}
		}
		
		Ordenacao ordenacaoDataAlteracao = filtro.getOrdenacaoDataAlteracao();
		if(ordenacaoDataAlteracao != null) {
			if (ordenacaoDataAlteracao == Ordenacao.ASCENDENTE) {
				criteria.addOrder(Order.asc("ultimaAlteracao"));
			} else if (ordenacaoDataAlteracao == Ordenacao.DESCENDENTE) {
				criteria.addOrder(Order.desc("ultimaAlteracao"));
			}
		}

		Integer indicadorUnicoItem = filtro.getLimiteResultados();
		if (indicadorUnicoItem != null) {
			criteria.setMaxResults(indicadorUnicoItem);
		}
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#consultarMedidorIndependente(java.util.Map)
	 */
	@Override
	public Collection<Medidor> consultarMedidorIndependente(Map<String, Object> filtro) throws GGASException {

		Criteria criteria = getCriteria();

		if (filtro != null) {

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			String numeroSerie = (String) filtro.get(Medidor.NUMERO_DE_SERIE);
			if (!StringUtils.isEmpty(numeroSerie)) {
				criteria.add(Restrictions.ilike(Medidor.NUMERO_DE_SERIE, Util.formatarTextoConsulta(numeroSerie)));
			}

			String numeroSerieParcial = (String) filtro.get(Medidor.NUMERO_SERIE_PARCIAL);
			if (!StringUtils.isEmpty(numeroSerieParcial)) {
				criteria.add(Restrictions.ilike(Medidor.NUMERO_DE_SERIE, Util.formatarTextoConsulta(numeroSerieParcial)));
			}

			Long idTipo = (Long) filtro.get(Medidor.ID_TIPO);
			if ((idTipo != null) && (idTipo > 0)) {
				criteria.createAlias(Medidor.TIPO_MEDIDOR, Medidor.TIPO_MEDIDOR);
				criteria.add(Restrictions.eq(TIPOMEDIDOR_CHAVE_PRImARIA, idTipo));
			} else {
				criteria.setFetchMode(Medidor.TIPO_MEDIDOR, FetchMode.JOIN);
			}

			Long idMarca = (Long) filtro.get(Medidor.ID_MARCA);
			if ((idMarca != null) && (idMarca > 0)) {
				criteria.createAlias(Medidor.MARCA_MEDIDOR, Medidor.MARCA_MEDIDOR);
				criteria.add(Restrictions.eq(MARCAMEDIDOR_CHAVE_PRIMARIA, idMarca));
			} else {
				criteria.setFetchMode(Medidor.MARCA_MEDIDOR, FetchMode.JOIN);
			}

			Long idModelo = (Long) filtro.get(Medidor.ID_MODELO);
			if ((idModelo != null) && (idModelo > 0)) {
				criteria.createAlias(Medidor.MODELO, Medidor.MODELO);
				criteria.add(Restrictions.eq(MODELO_CHAVE_PRIMARIA, idModelo));
			} else {
				criteria.setFetchMode(Medidor.MODELO, FetchMode.JOIN);
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}

			Long modoUso = (Long) filtro.get(Medidor.MODO_USO);
			if (modoUso != null) {
				criteria.add(Restrictions.eq(MODO_USO_CHAVE_PRIMARIA, modoUso));
			}

			/* ***************************************Situação Medidor**************************************** */
			String codigoSituacaoProntoPraInstalar = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_PRONTO_INSTALAR);
			String codigoSituacaoInstalado = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_INSTALADO);
			SituacaoMedidor situacaoMedidorProntoPraInstalar =
					fachada.obterSituacaoMedidor(Long.parseLong(codigoSituacaoProntoPraInstalar));
			SituacaoMedidor situacaoMedidorInstalado = fachada.obterSituacaoMedidor(Long.parseLong(codigoSituacaoInstalado));
			/* *********************************************************************************************** */
			Boolean instaladoOuProntoPraInstalar = (Boolean) filtro.get("InstaladorOuProntoPraInstalar");
			if (instaladoOuProntoPraInstalar != null && instaladoOuProntoPraInstalar) {
				criteria.add(Restrictions.disjunction()
						.add(Restrictions.eq(SITUACAO_MEDIDOR_CHAVE_PRIMARIA, situacaoMedidorProntoPraInstalar.getChavePrimaria()))
						.add(Restrictions.eq(SITUACAO_MEDIDOR_CHAVE_PRIMARIA, situacaoMedidorInstalado.getChavePrimaria())));
			}

			if (filtro.containsKey(COLECAO_PAGINADA)) {
				ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get(COLECAO_PAGINADA);
				HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);
				if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
					// Possivel Implementação Futura.
				}
			} else {
				// Possivel Implementação Futura.
			}

		}
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.medicao.medidor.ControladorMedidor#consultarMedidorVirtualPorMedidorIndependente(br.com.ggas.medicao.medidor.Medidor)
	 */
	@Override
	public Collection<Medidor> consultarMedidorVirtualPorMedidorIndependente(Medidor medidorIndependente) throws GGASException {

		// Obtem apenas os medidores virtuais
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(Medidor.MODO_USO, Long.valueOf(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL)));
		Collection<Medidor> listaMedidor = new ArrayList<Medidor>();
		Collection<Medidor> listaMedidorVirtual = consultarMedidor(filtro);

		for (Medidor medidorVirtual : listaMedidorVirtual) {
			String composicaoVirtual = medidorVirtual.getComposicaoVirtual();
			if (composicaoVirtual != null) {
				String[] arrayComposicaoVirtual = composicaoVirtual.split("#");
				for (String idMedidorComOperacao : arrayComposicaoVirtual) {
					String idMedidor = idMedidorComOperacao.substring(1);
					long chavePrimaria = medidorIndependente.getChavePrimaria();
					if (Long.valueOf(idMedidor).equals(chavePrimaria)) {
						listaMedidor.add(medidorVirtual);
					}
				}
			}
		}
		return listaMedidor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor#obterOperacoesMedidorIndependentePorTipoAssociacao(java.lang.Integer,
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<OperacaoMedidor> obterOperacoesMedidorIndependentePorTipoAssociacao(Integer codigoAssociacao, String status, String medidor,
			String corretorVazao) throws GGASException {

		List<OperacaoMedidor> listaOperacoes = this.listarOperacaoMedidor();
		List<OperacaoMedidor> listaOperacoesPermitidas = new ArrayList<OperacaoMedidor>();

		// operação medidor
		ConstanteSistema cInstalacao = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_INSTALACAO);
		ConstanteSistema cSubstituicao = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_SUBSTITUICAO);
		ConstanteSistema cRetirada = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_RETIRADA);
		ConstanteSistema cBloqueio = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_BLOQUEIO);
		ConstanteSistema cAtivacao = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_ATIVACAO);
		ConstanteSistema cReativacao = getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_REATIVACAO);
		ConstanteSistema cInstalacaoAtivacao =
				getControladorConstante().obterConstantePorCodigo(Constantes.C_MEDIDOR_OPERACAO_INSTALACAO_ATIVACAO);

		// operação medidor
		OperacaoMedidor operacaoInstalacao = obterOperacaoMedidor(Long.parseLong(cInstalacao.getValor()));
		OperacaoMedidor operacaoSubstituicao = obterOperacaoMedidor(Long.parseLong(cSubstituicao.getValor()));
		OperacaoMedidor operacaoRetirada = obterOperacaoMedidor(Long.parseLong(cRetirada.getValor()));
		OperacaoMedidor operacaoBloqueio = obterOperacaoMedidor(Long.parseLong(cBloqueio.getValor()));
		OperacaoMedidor operacaoAtivacao = obterOperacaoMedidor(Long.parseLong(cAtivacao.getValor()));
		OperacaoMedidor operacaoReativacao = obterOperacaoMedidor(Long.parseLong(cReativacao.getValor()));
		OperacaoMedidor operacaoInstalacaoAtivacao = obterOperacaoMedidor(Long.parseLong(cInstalacaoAtivacao.getValor()));

		// Situação do Medidor
		String codigoSituacaoDisponivel = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_DISPONIVEL);
		String codigoSituacaoInstalado = fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_INSTALADO);
		SituacaoMedidor situacaoMedidorDisponivel = fachada.obterSituacaoMedidor(Long.parseLong(codigoSituacaoDisponivel));
		SituacaoMedidor situacaoMedidorInstalado = fachada.obterSituacaoMedidor(Long.parseLong(codigoSituacaoInstalado));

		// Status Medidor Independente
		EntidadeConteudo statusMIAguardandoAtivacao = fachada.obterEntidadeConteudo(Long.parseLong(
				fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_AGUARDANDO_ATIVACAO)));
		EntidadeConteudo statusMIAtivo = fachada.obterEntidadeConteudo(
				Long.parseLong(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_ATIVO)));
		EntidadeConteudo statusMIBloqueado = fachada.obterEntidadeConteudo(Long
				.parseLong(fachada.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_ASSOCIACAO_MEDIDOR_INDEPENDENTE_BLOQUEADO)));

		List<Medidor> listaMedidor = obterMedidorPorNumeroSerie(medidor);
		Medidor medidorIndependente = Util.primeiroElemento(listaMedidor);

		if (codigoAssociacao.equals(CODIGO_TIPO_ASSOCIACAO_MEDIDOR)) {

			for (OperacaoMedidor operacaoMedidor : listaOperacoes) {

				if ((operacaoMedidor.getChavePrimaria() == operacaoInstalacao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoSubstituicao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoRetirada.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoBloqueio.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoAtivacao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoReativacao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoInstalacaoAtivacao.getChavePrimaria())) {

					if (statusMIAguardandoAtivacao.getDescricao().equals(status)) {
						if (situacaoMedidorDisponivel.getChavePrimaria() == medidorIndependente.getSituacaoMedidor().getChavePrimaria()) {
							if (operacaoMedidor.getDescricao().equals(operacaoInstalacao.getDescricao())
									|| operacaoMedidor.getDescricao().equals(operacaoInstalacaoAtivacao.getDescricao())) {
								listaOperacoesPermitidas.add(operacaoMedidor);
							}
						} else if (situacaoMedidorInstalado.getChavePrimaria() == medidorIndependente.getSituacaoMedidor()
								.getChavePrimaria()
								&& (operacaoMedidor.getDescricao().equals(operacaoSubstituicao.getDescricao())
										|| (operacaoMedidor.getDescricao().equals(operacaoRetirada.getDescricao()))
										|| (operacaoMedidor.getDescricao().equals(operacaoAtivacao.getDescricao())))) {

							listaOperacoesPermitidas.add(operacaoMedidor);
						}
					} else if (statusMIAtivo.getDescricao().equals(status)) {
						if (operacaoMedidor.getDescricao().equals(operacaoRetirada.getDescricao())
								|| (operacaoMedidor.getDescricao().equals(operacaoSubstituicao.getDescricao()))
								|| operacaoMedidor.getDescricao().equals(operacaoBloqueio.getDescricao())) {
							listaOperacoesPermitidas.add(operacaoMedidor);
						}
					} else if (statusMIBloqueado.getDescricao().equals(status)
							&& (operacaoMedidor.getDescricao().equals(operacaoSubstituicao.getDescricao()))
							|| operacaoMedidor.getDescricao().equals(operacaoRetirada.getDescricao())
							|| operacaoMedidor.getDescricao().equals(operacaoReativacao.getDescricao())) {
						listaOperacoesPermitidas.add(operacaoMedidor);
					}
				}

			}

		} else if (codigoAssociacao.equals(CODIGO_TIPO_ASSOCIACAO_CORRETOR_VAZAO)) {

			for (OperacaoMedidor operacaoMedidor : listaOperacoes) {

				if ((operacaoMedidor.getChavePrimaria() == operacaoInstalacao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoSubstituicao.getChavePrimaria())
						|| (operacaoMedidor.getChavePrimaria() == operacaoRetirada.getChavePrimaria())) {
					if (statusMIAguardandoAtivacao.getDescricao().equals(status)) {
						if (situacaoMedidorInstalado.getChavePrimaria() == medidorIndependente.getSituacaoMedidor().getChavePrimaria()
								&& "".equals(corretorVazao)) {
							if (operacaoMedidor.getDescricao().equals(operacaoInstalacao.getDescricao())) {
								listaOperacoesPermitidas.add(operacaoMedidor);
							}
						} else if (situacaoMedidorInstalado.getChavePrimaria() == medidorIndependente.getSituacaoMedidor()
								.getChavePrimaria() && !"".equals(corretorVazao)
								&& (operacaoMedidor.getDescricao().equals(operacaoRetirada.getDescricao())
										|| operacaoMedidor.getDescricao().equals(operacaoSubstituicao.getDescricao()))) {

							listaOperacoesPermitidas.add(operacaoMedidor);
						}
					} else if (statusMIAtivo.getDescricao().equals(status)) {
						if ("".equals(corretorVazao)) {
							if (operacaoMedidor.getDescricao().equals(operacaoInstalacao.getDescricao())) {
								listaOperacoesPermitidas.add(operacaoMedidor);
							}
						} else {
							if (operacaoMedidor.getDescricao().equals(operacaoRetirada.getDescricao())
									|| operacaoMedidor.getDescricao().equals(operacaoSubstituicao.getDescricao())) {
								listaOperacoesPermitidas.add(operacaoMedidor);
							}
						}
					}
				}
			}
		}

		return listaOperacoesPermitidas;
	}

	/**
	 * Classe privada reponsável por criar uma ordenação especial por endereço.
	 *
	 * @author gmatos
	 */
	private class OrdenacaoEspecialNumeroSerie implements OrdenacaoEspecial {

		@Override
		public void createAlias(Criteria criteria) {
			// Metódo não é suportado nessa classe.
		}

		@Override
		public void addOrder(Criteria criteria, SortOrderEnum sortDirection) {

			if (SortOrderEnum.ASCENDING.equals(sortDirection)) {
				criteria.addOrder(Order.asc(Medidor.NUMERO_DE_SERIE));
			} else {
				criteria.addOrder(Order.desc(Medidor.NUMERO_DE_SERIE));
			}

		}

		@Override
		public String addOrder(String hqlAuxiliar, SortOrderEnum sortDirection, Object classe) {

			return null;
		}

	}

	@Override
	public void validarEnderecoRemoto(String enderecoRemoto, Long chaveMedidor, Long chavePontoConsumo) throws NegocioException {

		if (!StringUtils.isEmpty(enderecoRemoto)) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(Medidor.CODIGO_MEDIDOR, enderecoRemoto);
			Collection<Medidor> listaMedidor = consultarMedidor(filtro);

			filtro = new HashMap<String, Object>();
			filtro.put("codigoPontoConsumoSupervisorio", enderecoRemoto);
			Collection<PontoConsumo> listaPontoConsumo = getControladorPontoConsumo().consultarPontosConsumo(filtro);

			if (!listaPontoConsumo.isEmpty()) {
				for (PontoConsumo pontoConsumo : listaPontoConsumo) {
					if (chavePontoConsumo != null) {
						if (pontoConsumo.getChavePrimaria() != chavePontoConsumo) {
							throw new NegocioException(ERRO_NEGOCIO_MEDIDOR_PONTO_CONSUMO_MESMO_ENDERECO_REMOTO, true);
						}
					} else {
						throw new NegocioException(ERRO_NEGOCIO_MEDIDOR_PONTO_CONSUMO_MESMO_ENDERECO_REMOTO, true);
					}
				}
			}
			if (!listaMedidor.isEmpty()) {
				for (Medidor medidor : listaMedidor) {
					if (chaveMedidor != null) {
						if (medidor.getChavePrimaria() != chaveMedidor) {
							throw new NegocioException(ERRO_NEGOCIO_MEDIDOR_PONTO_CONSUMO_MESMO_ENDERECO_REMOTO, true);
						}
					} else {
						throw new NegocioException(ERRO_NEGOCIO_MEDIDOR_PONTO_CONSUMO_MESMO_ENDERECO_REMOTO, true);
					}
				}
			}
		}
	}

	/**
	 * Obtém atributos de Corretor de Vazão por códigos do Ponto de Consumo no Supervisório
	 * 
	 * @param codigosPontoConsumoSupervisorio
	 * @return Map - VazaCorretor por atributo chavePrimaria da entidade PontoConsumo
	 **/
	@Override
	public Map<Long, VazaoCorretor> obterCorretoresDeVazaoPorCodigosDoSupervisorio(String[] codigosPontoConsumoSupervisorio) {

		Map<Long, VazaoCorretor> mapaVazaoCorretor = new HashMap<>();

		if (codigosPontoConsumoSupervisorio != null && codigosPontoConsumoSupervisorio.length > 0) {
			StringBuilder hql = new StringBuilder();
			hql.append(" SELECT  pontoConsumo.chavePrimaria, instalacaoMedidor.vazaoCorretor ");
			hql.append(FROM);
			hql.append(getClasseEntidadeInstalacaoMedidor().getSimpleName());
			hql.append(" AS instalacaoMedidor ");
			hql.append(" JOIN instalacaoMedidor.pontoConsumo AS pontoConsumo ");
			hql.append(" WHERE pontoConsumo.codigoPontoConsumoSupervisorio IN (:codigos) ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			query.setParameterList("codigos", codigosPontoConsumoSupervisorio);
			List<Object[]> listaDeAtributos = query.list();
			for (Object[] atributos : listaDeAtributos) {
				Long chavePrimariaPontoConsumo = (Long) atributos[0];
				VazaoCorretor vazaoCorretor = (VazaoCorretor) atributos[1];
				if (!mapaVazaoCorretor.containsKey(chavePrimariaPontoConsumo)) {
					mapaVazaoCorretor.put(chavePrimariaPontoConsumo, vazaoCorretor);
				}
			}
		}

		return mapaVazaoCorretor;
	}

	private boolean isPontoConsumoAtivo(Map<String, Object> dados) throws NegocioException {
		return getControladorPontoConsumo()
				.isPontoConsumoAtivo((PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO)));
	}
	
	private boolean isPontoConsumoAguardandoAtivacao(Map<String, Object> dados) throws NegocioException {
		return getControladorPontoConsumo()
				.isPontoConsumoAguardandoAtivacao((PontoConsumo) getControladorPontoConsumo().obter((Long) dados.get(Medidor.ID_PONTO_CONSUMO)));
	}

	/**
	 * Obtem uma lista de medidores que compoem um medidor virtual;
	 *
	 * @param composicaoVirtual composicao do medidor virtual em conjunto com suas operacoes
	 * @return List<MedidorVirtualVO>
	 */
	@Override
	public List<MedidorVirtualVO> obterComposicaoMedidorVirtual(String composicaoVirtual) {
		List<MedidorVirtualVO> listaRetorno = new ArrayList<MedidorVirtualVO>();

		if (StringUtils.isNotBlank(composicaoVirtual)) {
			String[] listaOperadorCodigo = composicaoVirtual.split("#");

			for (String operadorCodigo : listaOperadorCodigo) {

				Integer size = operadorCodigo.length();
				if (size > 0) {
					Long idMedidor = Long.valueOf(operadorCodigo.substring(1, size));
					String operador = operadorCodigo.substring(0, 1);

					MedidorVirtualVO medidor = obterCodigoNumeroSeriePoIdMedidor(idMedidor);
					medidor.setOperacao(operador);
					listaRetorno.add(medidor);
				}
			}
		}

		return listaRetorno;
	}

	/**
	 * Obtem o codigo e o numero de serie do medidor atraves de sua chave primaria
	 *
	 * @param idMedidor chave primaria do medidor
	 * @return MedidorVirtualVO
	 */
	@Override
	public MedidorVirtualVO obterCodigoNumeroSeriePoIdMedidor(Long idMedidor) {
		Criteria criteria = createCriteria(getClasseEntidadeMedidor());
		criteria.add(Restrictions.eq(CHAVE_PRIMARIA, idMedidor));

		criteria.setProjection(Projections.projectionList().add(Projections.property(CHAVE_PRIMARIA).as(ID))
				.add(Projections.property(NUMERO_SERIE).as(NUMERO_SERIE))
				.add(Projections.property(CODIGO_MEDIDOR_SUPERVISORIO).as(ENDERECO_REMOTO)));

		criteria.setResultTransformer(Transformers.aliasToBean(MedidorVirtualVO.class));

		return (MedidorVirtualVO) criteria.uniqueResult();
	}

	@Override
	public Integer obterDigitoMedidorPorLeituraMovimento(Long chaveLeituraMovimento) {
		return (Integer) createHQLQuery(
				"select m.digito from LeituraMovimentoImpl l join l.instalacaoMedidor i join i.medidor m where l.chavePrimaria = :chave")
						.setParameter("chave", chaveLeituraMovimento).list().stream().findFirst().orElse(null);
	}

	/**
	 * Lista a Pressão de Fornecimento de um determinado segmento e de um ponto de consumo
	 *
	 * @param idPontoConsumo chave primaria do Ponto de Consumo
	 * @return List<FaixaPressaoFornecimento>
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FaixaPressaoFornecimento> listarPressaoFornecimentoCampo(Long idPontoConsumo) {
		ContratoPontoConsumo contr = consultarContratoPontoConsumoPorPontoConsumo(idPontoConsumo);
		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadePressaoFornecimento().getSimpleName());
		hql.append(" pre ");
		hql.append(" inner join fetch pre.unidadePressao unidadePressa ");
		hql.append(" inner join fetch pre.segmento segmento ");
		hql.append(WHERE);
		hql.append(" pre.unidadePressao = ");
		hql.append(contr.getFaixaPressaoFornecimento().getUnidadePressao().getChavePrimaria());
		hql.append(" and ");
		hql.append(" pre.segmento = ");
		hql.append(contr.getFaixaPressaoFornecimento().getSegmento().getChavePrimaria());
		hql.append(" order by pre.medidaMinimo asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		return query.list();
	}

	/**
	 * Obtém o contratoPontoConsumo de um Ponto de consumo
	 *
	 * @param idPontoConsumo chave primaria do Ponto de Consumo
	 * @return ContratoPontoConsumo
	 */
	@Override
	public ContratoPontoConsumo consultarContratoPontoConsumoPorPontoConsumo(Long idPontoConsumo) {
		StringBuilder hql = new StringBuilder();
		hql.append("select contratoPontoConsumo FROM ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPontoConsumo ");
		hql.append(" inner join contratoPontoConsumo.pontoConsumo pontoConsumo");
		hql.append(" inner join contratoPontoConsumo.unidadePressao unidade");
		hql.append(" inner join contratoPontoConsumo.faixaPressaoFornecimento faixaPressaoFornecimento");
		hql.append(" where contratoPontoConsumo.pontoConsumo.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimaria", idPontoConsumo);
		query.setMaxResults(1);
		return (ContratoPontoConsumo) query.uniqueResult();
	}

	/**
	 * Obtém o contrato de ContratoPontoConsumo
	 *
	 * @param idPontoConsumo chave primaria do Ponto de Consumo
	 * @return ContratoPontoConsumo
	 */
	@Override
	public ContratoPontoConsumo consultarContratoPorContratoPontoConsumo(long idPontoConsumo) {
		ContratoPontoConsumo cpc = consultarContratoPontoConsumoPorPontoConsumo(idPontoConsumo);
		cpc.getChavePrimaria();
		cpc.getContrato().getChavePrimaria();
		cpc.getPontoConsumo().getChavePrimaria();

		StringBuilder hql2 = new StringBuilder();
		hql2.append("select contratoPontoConsumo ");
		hql2.append(FROM);
		hql2.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql2.append(" contratoPontoConsumo ");
		hql2.append(" inner join fetch contratoPontoConsumo.contrato contrato ");
		hql2.append(" inner join fetch contratoPontoConsumo.pontoConsumo pontoConsumo ");
		hql2.append(" where contratoPontoConsumo.pontoConsumo.chavePrimaria = :chavePrimaria ");
		hql2.append(" and contratoPontoConsumo.contrato.habilitado = :contcdInUso");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql2.toString());
		query.setLong("chavePrimaria", cpc.getPontoConsumo().getChavePrimaria());
		query.setLong("contcdInUso", 1);
		query.setMaxResults(1);
		return (ContratoPontoConsumo) query.uniqueResult();
	}
	
	@Override
	public InstalacaoMedidor obterUltimoMedidorInstacaoPontoConsumo(Long chavePontoConsumo) {
		StringBuilder hql = new StringBuilder();
		
		hql.append("select instalacaoMedidor ");
		hql.append(FROM);
		hql.append(getClasseEntidadeInstalacaoMedidor().getSimpleName());
		hql.append(" instalacaoMedidor ");
		hql.append(" where instalacaoMedidor.pontoConsumo.chavePrimaria = :chavePrimaria ");
		hql.append(" ORDER BY instalacaoMedidor.chavePrimaria DESC ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimaria", chavePontoConsumo);
		query.setMaxResults(1);
		return (InstalacaoMedidor) query.uniqueResult();
		
		
	}

	@Override
	public Collection<ContratoPontoConsumoMedidorDTO> consultarContratoPontoConsumoMedidor(Map<String, Object> filtro)
			throws NegocioException {
	    StringBuilder sql = new StringBuilder(); 
	    sql.append("WITH medpressao AS (SELECT mi.MEDI_CD medi, " +
					"cpc.POCN_CD pocn, cpc.COPC_MD_PRESSAO pressao, cpc.UNID_CD_PRESSAO unidpressao, " +
					"cpc.COPC_MD_VAZAO_INSTANTANEA vazao " +
					"FROM CONTRATO_PONTO_CONSUMO cpc " +
					"INNER JOIN MEDIDOR_INSTALACAO mi ON  mi.POCN_CD = cpc.POCN_CD " +
					"INNER JOIN CONTRATO c ON c.CONT_CD  = cpc.CONT_CD AND c.COSI_CD = 1 " +
					"INNER JOIN PONTO_CONSUMO pc ON cpc.POCN_CD = pc.POCN_CD " +
					"WHERE pc.MEIN_CD = mi.MEIN_CD ) " +
                    "SELECT DISTINCT " + 
                    "m.MEDI_CD AS chavePrimaria, " +
                    "m.MEDI_IN_USO AS habilitado, " +
                    "m.MEDI_NR_SERIE AS numeroSerie, " + 
                   	"mt.METI_DS AS tipoMedidor, " +
                    "mm.MEMD_DS AS modeloMedidor, " +
                    "ma.MEMA_DS AS marcaMedidor, " +
                    "mla.MELA_DS AS localMedidor, " +
                    "ms.pressao AS pressao, " +
                    "ms.vazao AS vazao, " +
                    "unidade.UNID_DS AS unidadePressao, " +
                    "m.MEDI_MD_PRESSAO AS medidaPressaoMedidor, " +
                    "m.MEDI_MD_VAZAO_INSTANTANEA AS medidaVazaoInstantaneaMedidor, " +
                    "unidade2.UNID_DS AS unidadePressaoMedidor " +
                    "FROM MEDIDOR m " +
					"LEFT JOIN medpressao ms ON ms.medi = m.MEDI_CD " +
					"INNER JOIN MEDIDOR_TIPO mt ON mt.METI_CD = m.METI_CD " +
					"INNER JOIN MEDIDOR_MODELO mm ON mm.MEMD_CD = m.MEMD_CD " +
					"INNER JOIN MEDIDOR_MARCA ma ON ma.MEMA_CD = m.MEMA_CD " +
					"INNER JOIN MEDIDOR_LOCAL_ARMAZENAGEM mla ON mla.MELA_CD = m.MELA_CD " +
					"LEFT JOIN UNIDADE unidade2 ON unidade2.UNID_CD = m.UNID_CD_PRESSAO " + // ;/
					"LEFT JOIN UNIDADE unidade ON ms.unidpressao = unidade.UNID_CD " +
					"LEFT JOIN (SELECT mi.MEDI_CD,mi.POCN_CD FROM MEDIDOR_INSTALACAO mi GROUP BY mi.MEDI_CD, mi.POCN_CD ) mi ON m.MEDI_CD = mi.MEDI_CD " +
					"LEFT JOIN (SELECT pc.POCN_CD FROM PONTO_CONSUMO pc GROUP BY pc.POCN_CD) pc ON pc.POCN_CD = mi.POCN_CD " +
					"LEFT JOIN (SELECT * FROM (SELECT cpc.*, ROW_NUMBER() OVER (PARTITION BY POCN_CD ORDER BY CONT_CD DESC) " +
					"rn FROM CONTRATO_PONTO_CONSUMO cpc) sub_cpc WHERE sub_cpc.rn = 1) cpc ON cpc.POCN_CD = pc.POCN_CD " +
					"LEFT JOIN (SELECT c.CONT_CD FROM CONTRATO c GROUP BY c.CONT_CD ) c ON cpc.CONT_CD = c.CONT_CD " +
					"WHERE 1=1 ");

	    Map<String, Object> params = new HashMap<>();

	    String numeroSerie = (String) filtro.get(Medidor.NUMERO_DE_SERIE);
	    if (!StringUtils.isEmpty(numeroSerie)) {
	        sql.append(" AND m.MEDI_NR_SERIE = :numeroSerie ");
	        params.put("numeroSerie", Util.formatarTextoConsulta(numeroSerie));
	    }

	    String numeroSerieParcial = (String) filtro.get(Medidor.NUMERO_SERIE_PARCIAL);
	    if (!StringUtils.isEmpty(numeroSerieParcial)) {
	        sql.append(" AND m.MEDI_NR_SERIE LIKE :numeroSerieParcial ");
	        params.put("numeroSerieParcial", "%"+numeroSerieParcial+"%");
	    }

	    Long idTipo = (Long) filtro.get(Medidor.ID_TIPO);
	    if (idTipo != null && idTipo > 0) {
	        sql.append(" AND m.METI_CD = :idTipo ");
	        params.put("idTipo", idTipo);
	    }

	    Long idMarca = (Long) filtro.get(Medidor.ID_MARCA);
	    if (idMarca != null && idMarca > 0) {
	        sql.append(" AND m.MEMA_CD = :idMarca ");
	        params.put("idMarca", idMarca);
	    } 

	    Long idModelo = (Long) filtro.get(Medidor.ID_MODELO);
	    if (idModelo != null && idModelo > 0) {
	        sql.append(" AND m.MEMD_CD = :idModelo ");
	        params.put("idModelo", idModelo);
	    } 

	    Long idLocalArmazenagem = (Long) filtro.get("idLocalArmazenagem");
	    if (idLocalArmazenagem != null && idLocalArmazenagem > 0) {
	        sql.append(" AND m.MELA_CD = :idLocalArmazenagem ");
	        params.put("idLocalArmazenagem", idLocalArmazenagem);
	    } 

	    Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
	    if (habilitado != null) {
	        sql.append(" AND m.MEDI_IN_USO = :habilitado ");
	        params.put("habilitado", habilitado ? 1 : 0);
	    }

	    Long modoUso = (Long) filtro.get(Medidor.MODO_USO);
	    if (modoUso != null) {
	        sql.append(" AND m.ENCO_CD_MODO_USO = :modoUso ");
	        params.put("modoUso", modoUso);
	    }

	    Long idRota = (Long) filtro.get("idRota");
	    if (idRota != null) {
	        sql.append(" AND pc.ROTA_CD = :idRota ");
	        params.put("idRota", idRota);
	    }

	    String codigoMedidorSupervisorio = (String) filtro.get(Medidor.CODIGO_MEDIDOR);
	    if (codigoMedidorSupervisorio != null) {
	        sql.append(" AND m.MEDI_CD_SUPERVISORIO = :codigoMedidorSupervisorio ");
	        params.put("codigoMedidorSupervisorio", codigoMedidorSupervisorio);
	    }

	    Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(sql.toString())
	            .addScalar("chavePrimaria", LongType.INSTANCE)
	            .addScalar("habilitado", IntegerType.INSTANCE)
	            .addScalar("numeroSerie", StringType.INSTANCE)
	            .addScalar("tipoMedidor", StringType.INSTANCE)
	            .addScalar("modeloMedidor", StringType.INSTANCE)
	            .addScalar("marcaMedidor", StringType.INSTANCE)
	            .addScalar("localMedidor", StringType.INSTANCE)
	            .addScalar("pressao", DoubleType.INSTANCE)
	            .addScalar("vazao", DoubleType.INSTANCE)
	            .addScalar("unidadePressao", StringType.INSTANCE)
	            .addScalar("medidaPressaoMedidor", BigDecimalType.INSTANCE)
	            .addScalar("medidaVazaoInstantaneaMedidor", BigDecimalType.INSTANCE)
	            .addScalar("unidadePressaoMedidor", StringType.INSTANCE)
	            .setResultTransformer(Transformers.aliasToBean(ContratoPontoConsumoMedidorDTO.class));

	    
	    for (Map.Entry<String, Object> param : params.entrySet()) {
	        query.setParameter(param.getKey(), param.getValue());
	    }

	    // Pagination
	    boolean paginacaoPadrao = false;
	    if (filtro.containsKey(COLECAO_PAGINADA)) {
	        ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get(COLECAO_PAGINADA);
	        colecaoPaginada.adicionarOrdenacaoEspecial(Medidor.NUMERO_DE_SERIE, new OrdenacaoEspecialNumeroSerie());
	        HibernateHqlUtil.paginarConsultaPorHQL(colecaoPaginada, query, ContratoPontoConsumoMedidorDTO.class, sql.toString());

	        
			if (colecaoPaginada != null) {

				if (colecaoPaginada.getIndex() == 1) {
					query.setFirstResult(0);
				} else {
					query.setFirstResult(colecaoPaginada.getIndex());
				}

				query.setMaxResults(colecaoPaginada.getObjectsPerPage());
			}
	        
	        if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
	            paginacaoPadrao = true;
	        }
	    } else {
	        paginacaoPadrao = true;
	    }

	    return query.list();
	    
        }
	
	
		/*
		 * (non-Javadoc)
		 * 
		 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterMedidorLocalArmazenagem(long)
		 */
		@Override
		public MedidorLocalInstalacao obterMedidorLocalInstalacao(long chavePrimaria) throws NegocioException {
	
			return (MedidorLocalInstalacao) super.obter(chavePrimaria, getClasseEntidadeMedidorLocalInstalacao());
		}
		
		
		/*
		 * (non-Javadoc)
		 * 
		 * @see br.com.ggas.medicao.medidor.ControladorMedidor #obterMedidorLocalArmazenagem(long)
		 */
		@Override
		public MedidorProtecao obterMedidorProtecao(long chavePrimaria) throws NegocioException {
	
			return (MedidorProtecao) super.obter(chavePrimaria, getClasseEntidadeMedidorProtecao());
		}
              
		/*
		 * (non-Javadoc)
		 * 
		 * @see br.com.ggas.medicao.medidor.ControladorMedidor #listarMotivoOperacaoMedidor()
		 */
		@SuppressWarnings("unchecked")
		@Override
		public MotivoOperacaoMedidor obterMotivoOperacaoMedidor(long chavePrimaria) throws NegocioException {

			StringBuilder hql = new StringBuilder();
			hql.append("select motivoOperacaMedidor FROM ");
			hql.append(getClasseEntidadeMotivoOperacaoMedidor().getSimpleName());
			hql.append(" motivoOperacaMedidor ");
			hql.append(" where motivoOperacaMedidor.chavePrimaria = :chavePrimaria ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong("chavePrimaria", chavePrimaria);
			query.setMaxResults(1);
			return (MotivoOperacaoMedidor) query.uniqueResult();
		
		}
		
}
