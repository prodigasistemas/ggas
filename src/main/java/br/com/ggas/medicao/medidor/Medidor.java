/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.medidor;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.FaixaTemperaturaTrabalho;
import br.com.ggas.medicao.vazaocorretor.Unidade;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao medidor
 *
 */
public interface Medidor extends EntidadeNegocio {

	String BEAN_ID_MEDIDOR = "medidor";

	int COM_LOTE = 1;

	int SEM_LOTE = 2;

	String MEDIDOR_CAMPO_STRING = "Medidor";

	String MEDIDORES = "Medidor(es)";

	String TIPO = "Tipo";

	String MARCA = "Marca";

	String SITUACAO = "Situação";

	String NUMERO_INICIAL = "Numero Inicial";

	String NUMERO_SERIE = "Número de série";

	String QUANTIDADE_LOTE = "Quantidade";

	String CAPACIDADE_MINIMA = "Capacidade Mínima";

	String CAPACIDADE_MAXIMA = "Capacidade máxima";

	String ANO_FABRICACAO = "Ano fabricação";

	String DATA_AQUISICAO = "Data Aquisição";

	String ANO_CALIBRACAO = "Ano calibração";

	String DATA_ULTIMA_CALIBRACAO = "Data ultima calibração";

	String INDICADOR_LOTE = "Indicador de Lote";

	String DATA_MAXIMA_INSTALACAO = "Data Máxima instalação";

	String NUMERO_DIGITO_LEITURA = "Número de Dígitos para Leitura";

	String FATOR_K = "Fator K";

	String PRESSAO_MAXIMA_TRABALHO = "Pressão Máxima Trabalho";

	String CODIGO_MEDIDOR_SUPERVISORIO = "Endereço Remoto";
	
	String COMPOSICAO_VIRTUAL = "Composição virtual";
	
	String ATRIBUTO_DESCRICAO = "descricao";
	
	String MODELO = "modelo";
	
	String TIPO_MEDIDOR = "tipoMedidor";
	
	String MARCA_MEDIDOR = "marcaMedidor";
	
	String SITUACAO_MEDIDOR = "situacaoMedidor";
	
	String NUMERO_DE_SERIE = "numeroSerie";
	
	String NUMERO_SERIE_PARCIAL = "numeroSerieParcial";
	
	String ID_TIPO ="idTipo";
	
	String ID_MARCA = "idMarca";
	
	String ID_MODELO = "idModelo";
	
	String LOCAL_ARMAZENAGEM =  "localArmazenagem";
	
	String MODO_USO = "modoUso";
	
	String INSTALACAO_MEDIDOR = "instalacaoMedidor";
	
	String CODIGO_MEDIDOR = "codigoMedidorSupervisorio";
	
	String ID_MEDIDOR = "idMedidor";
	
	String ID_PONTO_CONSUMO = "idPontoConsumo";
	
	String ID_OPERACAO_MEDIDOR_INDEPENDENTE = "idOperacaoMedidorIndependente";
	

	/**
	 * @return Retorna uma coleção de Movimentações
	 */
	public Collection<MovimentacaoMedidor> getMovimentacoes();

	/**
	 * @param movimentacoes, Set movimentações. 
	 */
	public void setMovimentacoes(Collection<MovimentacaoMedidor> movimentacoes);

	/**
	 * @return the numeroSerie
	 */
	String getNumeroSerie();

	/**
	 * @param numeroSerie
	 *            the numeroSerie to set
	 */
	void setNumeroSerie(String numeroSerie);

	/**
	 * @return the anoFabricacao
	 */
	Integer getAnoFabricacao();

	/**
	 * @param anoFabricacao
	 *            the anoFabricacao to set
	 */
	void setAnoFabricacao(Integer anoFabricacao);

	/**
	 * @return the dataAquisicao
	 */
	Date getDataAquisicao();

	/**
	 * @param dataAquisicao
	 *            the dataAquisicao to set
	 */
	void setDataAquisicao(Date dataAquisicao);

	/**
	 * @return the dataMaximaInstalacao
	 */
	Date getDataMaximaInstalacao();

	/**
	 * @param dataMaximaInstalacao
	 *            the dataMaximaInstalacao to set
	 */
	void setDataMaximaInstalacao(Date dataMaximaInstalacao);

	/**
	 * @return the dataUltimaCalibracao
	 */
	Date getDataUltimaCalibracao();

	/**
	 * @param dataUltimaCalibracao
	 */
	void setDataUltimaCalibracao(Date dataUltimaCalibracao);

	/**
	 * @return the anoCalibracao
	 */
	Integer getAnoCalibracao();

	/**
	 * @param anoCalibracao
	 *            the anoCalibracao to set
	 */
	void setAnoCalibracao(Integer anoCalibracao);

	/**
	 * @return the tombamento
	 */
	String getTombamento();

	/**
	 * @param tombamento
	 *            the tombamento to set
	 */
	void setTombamento(String tombamento);

	/**
	 * @return the digito
	 */
	Integer getDigito();

	/**
	 * @param digito
	 *            the digito to set
	 */
	void setDigito(Integer digito);

	/**
	 * @return the leituraAcumulada
	 */
	Integer getLeituraAcumulada();

	/**
	 * @param leituraAcumulada
	 *            the leituraAcumulada to set
	 */
	void setLeituraAcumulada(Integer leituraAcumulada);

	/**
	 * @return the numeroInicial
	 */
	String getNumeroInicial();

	/**
	 * @param numeroInicial
	 *            the numeroInicial to set
	 */
	void setNumeroInicial(String numeroInicial);

	/**
	 * @return the quantidadeLote
	 */
	Integer getQuantidadeLote();

	/**
	 * @param quantidadeLote
	 *            the quantidadeLote to set
	 */
	void setQuantidadeLote(Integer quantidadeLote);

	/**
	 * @return the prefixo
	 */
	String getPrefixo();

	/**
	 * @param prefixo
	 *            the prefixo to set
	 */
	void setPrefixo(String prefixo);

	/**
	 * @return the sufixo
	 */
	String getSufixo();

	/**
	 * @param sufixo
	 *            the sufixo to set
	 */
	void setSufixo(String sufixo);

	/**
	 * @return the situacaoMedidor
	 */
	SituacaoMedidor getSituacaoMedidor();

	/**
	 * @param situacaoMedidor
	 *            the situacaoMedidor to set
	 */
	void setSituacaoMedidor(SituacaoMedidor situacaoMedidor);

	/**
	 * @return the tipoMedidor
	 */
	TipoMedidor getTipoMedidor();

	/**
	 * @param tipoMedidor
	 *            the tipoMedidor to set
	 */
	void setTipoMedidor(TipoMedidor tipoMedidor);

	/**
	 * @return the marcaMedidor
	 */
	MarcaMedidor getMarcaMedidor();

	/**
	 * @param marcaMedidor
	 *            the marcaMedidor to set
	 */
	void setMarcaMedidor(MarcaMedidor marcaMedidor);

	/**
	 * @return the diametroMedidor
	 */
	DiametroMedidor getDiametroMedidor();

	/**
	 * @param diametroMedidor
	 *            the diametroMedidor to set
	 */
	void setDiametroMedidor(DiametroMedidor diametroMedidor);

	/**
	 * @return the capacidadeMinima
	 */
	CapacidadeMedidor getCapacidadeMinima();

	/**
	 * @param capacidadeMinima
	 *            the capacidadeMinima to set
	 */
	void setCapacidadeMinima(CapacidadeMedidor capacidadeMinima);

	/**
	 * @return the capacidadeMaxima
	 */
	CapacidadeMedidor getCapacidadeMaxima();

	/**
	 * @param capacidadeMaxima
	 *            the capacidadeMaxima to set
	 */
	void setCapacidadeMaxima(CapacidadeMedidor capacidadeMaxima);

	/**
	 * @return the localArmazenagem
	 */
	MedidorLocalArmazenagem getLocalArmazenagem();

	/**
	 * @param localArmazenagem
	 *            the localArmazenagem to set
	 */
	void setLocalArmazenagem(MedidorLocalArmazenagem localArmazenagem);

	/**
	 * @return the faixaTemperaturaTrabalho
	 */
	FaixaTemperaturaTrabalho getFaixaTemperaturaTrabalho();

	/**
	 * @param faixaTemperaturaTrabalho
	 *            the faixaTemperaturaTrabalho to
	 *            set
	 */
	void setFaixaTemperaturaTrabalho(FaixaTemperaturaTrabalho faixaTemperaturaTrabalho);

	/**
	 * @return the pressaoMaximaTrabalho
	 */
	BigDecimal getPressaoMaximaTrabalho();

	/**
	 * @param pressaoMaximaTrabalho
	 *            the pressaoMaximaTrabalho to set
	 */
	void setPressaoMaximaTrabalho(BigDecimal pressaoMaximaTrabalho);

	/**
	 * @return the indicadorLote
	 */
	Integer getIndicadorLote();

	/**
	 * @param indicadorLote
	 *            the indicadorLote to set
	 */
	void setIndicadorLote(Integer indicadorLote);

	/**
	 * @return the unidadePressaoMaximaTrabalho
	 */
	Unidade getUnidadePressaoMaximaTrabalho();

	/**
	 * @param unidadePressaoMaximaTrabalho
	 *            the unidadePressaoMaximaTrabalho
	 *            to set
	 */
	void setUnidadePressaoMaximaTrabalho(Unidade unidadePressaoMaximaTrabalho);

	/**
	 * @return the modelo
	 */
	ModeloMedidor getModelo();

	/**
	 * @param modelo
	 *            the modelo to set
	 */
	void setModelo(ModeloMedidor modelo);

	/**
	 * @return the fatorK
	 */
	FatorK getFatorK();

	/**
	 * @param fatorK
	 *            the fatorK to set
	 */
	/**
	 * @param Set fatorK
	 */
	void setFatorK(FatorK fatorK);

	/**
	 * @return EntidadeConteudo - Retorna objeto Entidade contéudo com informações sobre modo de uso.
	 */
	public EntidadeConteudo getModoUso();

	/**
	 * @param modoUso - Set modo de uso.
	 */
	public void setModoUso(EntidadeConteudo modoUso);
	
	/**
	 * @return String - Retorna Composição Virtual.
	 */
	String getComposicaoVirtual();

	/**
	 * @param composicaoVirtual - Set Composição Virtual.
	 */
	void setComposicaoVirtual(String composicaoVirtual);

	/**
	 * @return String - Retorna Código Medidor Supervisório.
	 */
	String getCodigoMedidorSupervisorio();

	/**
	 * @param codigoMedidorSupervisorio - Set código medidor supervisório.
	 */
	void setCodigoMedidorSupervisorio(String codigoMedidorSupervisorio);

	/**
	 * @return InstalacaoMedidor - Retorna objeto Instalação Medidor.
	 */
	public InstalacaoMedidor getInstalacaoMedidor();

	/**
	 * @return Collection Instalação medidor - Retorna uma coleção de Instalação Medidor.
	 */
	public Collection<InstalacaoMedidor> getListaInstalacaoMedidor();

	/**
	 * @param listaInstalacaoMedidor - Set Lista instalação médidor.
	 */
	public void setListaInstalacaoMedidor(Collection<InstalacaoMedidor> listaInstalacaoMedidor);

	/**
	 * @param instalacaoMedidor - Set instalação do médidor.
	 */
	public void setInstalacaoMedidor(InstalacaoMedidor instalacaoMedidor);

	/**
	 * @return EntidadeConteudo - Retorna Objeto com a Situação Associação MEdidor Independente.
	 */
	public EntidadeConteudo getSituacaoAssociacaoMedidorIndependente();

	/**
	 * @param situacaoAssociacaoMedidorIndependente - Set situação associação medidor independente.
	 */
	public void setSituacaoAssociacaoMedidorIndependente(EntidadeConteudo situacaoAssociacaoMedidorIndependente);
	
	/**
	 * @return BigDecimal - Medida pressão.
	 */
	public BigDecimal getMedidaPressao();
	
	/**
	 * @param medidaPressao - Set medidaPressao.
	 */
	public void setMedidaPressao(BigDecimal medidaPressao);
	
	/**
	 * @return the unidadePressao
	 */
	Unidade getUnidadePressao();

	/**
	 * @param unidadePressao
	 *            the unidadePressao to set
	 */
	void setUnidadePressao(Unidade unidadePressao);
	
	/**
	 * @return the medidaVazaoInstantanea
	 */
	BigDecimal getMedidaVazaoInstantanea();
	
	/**
	 * @param medidaVazaoInstantanea
	 *            the medidaVazaoInstantanea
	 *            to set
	 */
	void setMedidaVazaoInstantanea(BigDecimal medidaVazaoInstantanea);

}
