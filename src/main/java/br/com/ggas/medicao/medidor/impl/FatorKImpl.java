/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe FatorKImpl representa uma FatorKImpl no sistema.
 *
 * @since 17/11/2009
 * 
 */

package br.com.ggas.medicao.medidor.impl;

import java.math.BigDecimal;
import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.medidor.FatorK;
import br.com.ggas.medicao.vazaocorretor.Unidade;

/**
 * Classe FatorKImpl.
 * 
 *
 */
public class FatorKImpl extends EntidadeNegocioImpl implements FatorK {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3934974956161162073L;

	private BigDecimal fatorK;

	private Unidade unidade;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.FatorK
	 * #getFatorK()
	 */
	@Override
	public BigDecimal getFatorK() {

		return fatorK;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.FatorK
	 * #setFatorK(java.math.BigDecimal)
	 */
	@Override
	public void setFatorK(BigDecimal fatorK) {

		this.fatorK = fatorK;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.FatorK
	 * #getUnidade()
	 */
	@Override
	public Unidade getUnidade() {

		return unidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.FatorK
	 * #setUnidade
	 * (br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidade(Unidade unidade) {

		this.unidade = unidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
