
package br.com.ggas.medicao.medidor.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.util.Ordenacao;

/**
 * Classe Filtro Historico Operacao Medidor.
 * 
 *
 */
public class FiltroHistoricoOperacaoMedidor {

	private static final String KEY_LIMITE_RESULTADOS = "limiteResultados";

	private static final String KEY_ID_PONTO_CONSUMO = "idPontoConsumo";

	private static final String KEY_DATA_AUTORIZACAO = "dataAutorizacao";

	private static final String KEY_ID_OPERACAO_MEDIDOR = "idOperacaoMedidor";
	
	private static final String KEY_ID_MEDIDOR = "idMedidor";

	private static final String KEY_ORDENACAO_DATA_REALIZADA = "ordenacaoDataRealizada";
	
	private static final String KEY_ORDENACAO_ULTIMA_ALTERACAO = "ordenacaoUltimaAlteracao";

	private Map<String, Object> filtro;

	/**
	 * Instantiates a new filtro historico operacao medidor.
	 */
	public FiltroHistoricoOperacaoMedidor() {

		this.filtro = new HashMap<String, Object>();
	}

	public Long getIdPontoConsumo() {

		return (Long) filtro.get(KEY_ID_PONTO_CONSUMO);
	}

	public void setIdPontoConsumo(Long idPontoConsumo) {

		filtro.put(KEY_ID_PONTO_CONSUMO, idPontoConsumo);
	}

	public Date getDataAutorizacao() {

		return (Date) filtro.get(KEY_DATA_AUTORIZACAO);
	}

	public void setDataAutorizacao(Date dataAutorizacao) {

		filtro.put(KEY_DATA_AUTORIZACAO, dataAutorizacao);
	}

	public Long getIdOperacaoMedidor() {

		return (Long) filtro.get(KEY_ID_OPERACAO_MEDIDOR);
	}

	public void setIdOperacaoMedidor(Long idOperacaoMedidor) {

		filtro.put(KEY_ID_OPERACAO_MEDIDOR, idOperacaoMedidor);
	}
	
	public Long getIdMedidor() {

		return (Long) filtro.get(KEY_ID_MEDIDOR);
	}

	public void setIdMedidor(Long idMedidor) {

		filtro.put(KEY_ID_MEDIDOR, idMedidor);
	}

	public Ordenacao getOrdenacaoDataRealizada() {

		return (Ordenacao) filtro.get(KEY_ORDENACAO_DATA_REALIZADA);
	}

	public void setOrdenacaoDataRealizada(Ordenacao ordenacaoDataRealizada) {

		filtro.put(KEY_ORDENACAO_DATA_REALIZADA, ordenacaoDataRealizada);
	}
	
	public Ordenacao getOrdenacaoDataAlteracao() {

		return (Ordenacao) filtro.get(KEY_ORDENACAO_ULTIMA_ALTERACAO);
	}

	public void setOrdenacaoDataAlteracao(Ordenacao ordenacaoUltimaAlteracao) {

		filtro.put(KEY_ORDENACAO_ULTIMA_ALTERACAO, ordenacaoUltimaAlteracao);
	}

	public Integer getLimiteResultados() {

		return (Integer) filtro.get(KEY_LIMITE_RESULTADOS);
	}

	public void setLimiteResultados(Integer limiteResultados) {

		filtro.put(KEY_LIMITE_RESULTADOS, limiteResultados);
	}
	
	public void setIdMedidorDiferente(Long idMedidorDiferente) {
		filtro.put("idMedidorDiferente", idMedidorDiferente);
	}
	
	public Long getIdMedidorDiferente() {
        return (Long) filtro.get("idMedidorDiferente");
    }

}
