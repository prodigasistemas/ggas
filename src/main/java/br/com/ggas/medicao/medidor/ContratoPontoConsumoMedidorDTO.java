package br.com.ggas.medicao.medidor;

import java.io.Serializable;
import java.math.BigDecimal;

import br.com.ggas.contrato.contrato.ContratoPontoConsumo;

public class ContratoPontoConsumoMedidorDTO {

    private Long chavePrimaria;
    private Integer habilitado;
    private String numeroSerie;
    private String tipoMedidor;
    private String modeloMedidor;
    private String marcaMedidor;
    private String localMedidor;
    private Double pressao;
    private Double vazao;
    private String unidadePressao;
    private BigDecimal medidaPressaoMedidor;
    private BigDecimal medidaVazaoInstantaneaMedidor;
    private String unidadePressaoMedidor;

    public Long getChavePrimaria() {
        return chavePrimaria;
    }

    public void setChavePrimaria(Long chavePrimaria) {
        this.chavePrimaria = chavePrimaria;
    }

    public Integer getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Integer habilitado) {
        this.habilitado = habilitado;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getTipoMedidor() {
        return tipoMedidor;
    }

    public void setTipoMedidor(String tipoMedidor) {
        this.tipoMedidor = tipoMedidor;
    }

    public String getModeloMedidor() {
        return modeloMedidor;
    }

    public void setModeloMedidor(String modeloMedidor) {
        this.modeloMedidor = modeloMedidor;
    }

    public String getMarcaMedidor() {
        return marcaMedidor;
    }

    public void setMarcaMedidor(String marcaMedidor) {
        this.marcaMedidor = marcaMedidor;
    }

    public String getLocalMedidor() {
        return localMedidor;
    }

    public void setLocalMedidor(String localMedidor) {
        this.localMedidor = localMedidor;
    }

    public Double getPressao() {
        return pressao;
    }

    public void setPressao(Double pressao) {
        this.pressao = pressao;
    }

    public Double getVazao() {
        return vazao;
    }

    public void setVazao(Double vazao) {
        this.vazao = vazao;
    }

	public String getUnidadePressao() {
		return unidadePressao;
	}

	public void setUnidadePressao(String unidadePressao) {
		this.unidadePressao = unidadePressao;
	}

	public BigDecimal getMedidaPressaoMedidor() {
		return medidaPressaoMedidor;
	}

	public void setMedidaPressaoMedidor(BigDecimal medidaPressaoMedidor) {
		this.medidaPressaoMedidor = medidaPressaoMedidor;
	}

	public BigDecimal getMedidaVazaoInstantaneaMedidor() {
		return medidaVazaoInstantaneaMedidor;
	}

	public void setMedidaVazaoInstantaneaMedidor(BigDecimal medidaVazaoInstantaneaMedidor) {
		this.medidaVazaoInstantaneaMedidor = medidaVazaoInstantaneaMedidor;
	}

	public String getUnidadePressaoMedidor() {
		return unidadePressaoMedidor;
	}

	public void setUnidadePressaoMedidor(String unidadePressaoMedidor) {
		this.unidadePressaoMedidor = unidadePressaoMedidor;
	}
	
	
}

