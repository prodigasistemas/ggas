/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.medidor.impl;

import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.consumo.TipoComposicaoConsumo;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.SituacaoMedidor;

/**
 * Classe que implementa a interface de motivo
 * para uma operação de medidor.
 */
public class MotivoOperacaoMedidorImpl extends EntidadeNegocioImpl implements MotivoOperacaoMedidor {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4862116979306519522L;

	private String descricao;

	private String descricaoAbreviada;

	private TipoComposicaoConsumo tipoComposicaoConsumo;

	private SituacaoMedidor situacaoMedidor;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MotivoOperacaoMedidor#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MotivoOperacaoMedidor
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MotivoOperacaoMedidor
	 * #getDescricaoAbreviada()
	 */
	@Override
	public String getDescricaoAbreviada() {

		return descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MotivoOperacaoMedidor
	 * #setDescricaoAbreviada(java.lang.String)
	 */
	@Override
	public void setDescricaoAbreviada(String descricaoAbreviada) {

		this.descricaoAbreviada = descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MotivoOperacaoMedidor
	 * #getTipoComposicaoConsumo()
	 */
	@Override
	public TipoComposicaoConsumo getTipoComposicaoConsumo() {

		return tipoComposicaoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MotivoOperacaoMedidor
	 * #setTipoComposicaoConsumo
	 * (br.com.ggas.medicao
	 * .consumo.TipoComposicaoConsumo)
	 */
	@Override
	public void setTipoComposicaoConsumo(TipoComposicaoConsumo tipoComposicaoConsumo) {

		this.tipoComposicaoConsumo = tipoComposicaoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.
	 * MotivoOperacaoMedidor#getSituacaoMedidor()
	 */
	@Override
	public SituacaoMedidor getSituacaoMedidor() {

		return this.situacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.
	 * MotivoOperacaoMedidor
	 * #setSituacaoMedidor(br.
	 * com.ggas.medicao.medidor.SituacaoMedidor)
	 */
	@Override
	public void setSituacaoMedidor(SituacaoMedidor situacaoMedidor) {

		this.situacaoMedidor = situacaoMedidor;
	}

}
