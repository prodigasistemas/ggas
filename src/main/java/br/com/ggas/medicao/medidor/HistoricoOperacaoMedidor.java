/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.medidor;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.vazaocorretor.Unidade;

/**
 * Interface que representa um histórico de
 * operação de medidor.
 */
public interface HistoricoOperacaoMedidor extends EntidadeNegocio {

	String BEAN_ID_HISTORICO_OPERACAO_MEDIDOR = "historicoOperacaoMedidor";
	
	String PONTO_CONSUMO = "pontoConsumo";
	
	String MEDIDOR =  "medidor";
	
	String OPERACAO_MEDIDOR = "operacaoMedidor";
	
	String LOCAL_INSTALACAO_MEDIDOR = "localInstalacaoMedidor";
	
	String DATA_REALIZADA = "dataRealizada";
	
	String ID_MEDIDOR = "idMedidor";
	
	String MOTIVO_OPERACAO_MEDIDOR = "motivoOperacaoMedidor";

	/**
	 * @return the operacaoMedidor
	 */
	OperacaoMedidor getOperacaoMedidor();

	/**
	 * @param operacaoMedidor
	 *            the operacaoMedidor to set
	 */
	void setOperacaoMedidor(OperacaoMedidor operacaoMedidor);

	/**
	 * @return the dataPlanejada
	 */
	Date getDataPlanejada();

	/**
	 * @param dataPlanejada
	 *            the dataPlanejada to set
	 */
	void setDataPlanejada(Date dataPlanejada);

	/**
	 * @return the dataRealizada
	 */
	Date getDataRealizada();

	/**
	 * @param dataRealizada
	 *            the dataRealizada to set
	 */
	void setDataRealizada(Date dataRealizada);

	/**
	 * @return the medidor
	 */
	Medidor getMedidor();

	/**
	 * @param medidor
	 *            the medidor to set
	 */
	void setMedidor(Medidor medidor);

	/**
	 * @return the medidorLocalInstalacao
	 */
	MedidorLocalInstalacao getMedidorLocalInstalacao();

	/**
	 * @param medidorLocalInstalacao
	 *            the medidorLocalInstalacao to
	 *            set
	 */
	void setMedidorLocalInstalacao(MedidorLocalInstalacao medidorLocalInstalacao);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the numeroLeitura
	 */
	BigDecimal getNumeroLeitura();

	/**
	 * @param numeroLeitura
	 *            the numeroLeitura to set
	 */
	void setNumeroLeitura(BigDecimal numeroLeitura);

	/**
	 * @return the funcionario
	 */
	Funcionario getFuncionario();

	/**
	 * @param funcionario
	 *            the funcionario to set
	 */
	void setFuncionario(Funcionario funcionario);

	/**
	 * @return the motivoOperacaoMedidor
	 */
	MotivoOperacaoMedidor getMotivoOperacaoMedidor();

	/**
	 * @param motivoOperacaoMedidor
	 *            the motivoOperacaoMedidor to set
	 */
	void setMotivoOperacaoMedidor(MotivoOperacaoMedidor motivoOperacaoMedidor);

	/**
	 * @return the medidaPressaoAnterior
	 */
	BigDecimal getMedidaPressaoAnterior();

	/**
	 * @param medidaPressaoAnterior
	 *            the medidaPressaoAnterior to set
	 */
	void setMedidaPressaoAnterior(BigDecimal medidaPressaoAnterior);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the unidadePressaoAnterior
	 */
	Unidade getUnidadePressaoAnterior();

	/**
	 * @param unidadePressaoAnterior
	 *            the unidadePressaoAnterior to
	 *            set
	 */
	void setUnidadePressaoAnterior(Unidade unidadePressaoAnterior);

	/**
	 * @return the instalacaoMedidor
	 */
	InstalacaoMedidor getInstalacaoMedidor();

	/**
	 * @param instalacaoMedidor
	 *            the instalacaoMedidor to set
	 */
	void setInstalacaoMedidor(InstalacaoMedidor instalacaoMedidor);

	/**
	 * @return String - Retorna lacre.
	 */
	public String getLacre();

	/**
	 * @param lacre - Set lacre.
	 */
	public void setLacre(String lacre);
	
	
	public String getLacreDois();
	/**
	 * @return String - Retorna lacre dois.
	 */
	
	public void setLacreDois(String lacreDois);
	/**
	 * @param lacre - Seta o lacre 2.
	 */
	
	public String getLacreTres();
	/**
	 * @return String - Retorna lacre tres.
	 */
	
	public void setLacreTres(String lacreTres);
	/**
	 * @param lacre - Seta o lacre tres.
	 */

	Boolean getIndicadorOperacaoConjunta();

	void setIndicadorOperacaoConjunta(Boolean indicadorOperacaoConjunta);
	

}
