/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe MovimentacaoMedidorImpl representa uma MovimentacaoMedidorImpl no sistema.
 *
 * @since 21/10/2009
 * 
 */

package br.com.ggas.medicao.medidor.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MotivoMovimentacaoMedidor;
import br.com.ggas.medicao.medidor.MovimentacaoMedidor;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe Movimentacao Medidor Impl.
 * 
 *
 */
public class MovimentacaoMedidorImpl extends EntidadeNegocioImpl implements MovimentacaoMedidor {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3004521942781927309L;

	private Date dataMovimento;

	private Medidor medidor;

	private MotivoMovimentacaoMedidor motivoMovimentacaoMedidor;

	private String descricaoParecer;

	private MedidorLocalArmazenagem localArmazenagemOrigem;

	private MedidorLocalArmazenagem localArmazenagemDestino;

	private Funcionario funcionario;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor#getDataMovimento()
	 */
	@Override
	public Date getDataMovimento() {
		Date data = null;
		if (this.dataMovimento != null) {
			data = (Date) dataMovimento.clone();
		}
		return data;
	}

	@Override
	public String getDataMovimentoFormatada() {

		return Util.converterDataParaStringSemHora(dataMovimento, Constantes.FORMATO_DATA_BR);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.impl. MovimentacaoMedidor
	 * #setDataMovimento(java.util.Date)
	 */
	@Override
	public void setDataMovimento(Date dataMovimento) {
		if (dataMovimento != null) {
			this.dataMovimento = (Date) dataMovimento.clone();
		} else {
			this.dataMovimento = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor#getMedidor()
	 */
	@Override
	public Medidor getMedidor() {

		return medidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor
	 * #setMedidor(br.com.ggas.medicao
	 * .medidor.Medidor)
	 */
	@Override
	public void setMedidor(Medidor medidor) {

		this.medidor = medidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor
	 * #getMotivoMovimentacaoMedidor()
	 */
	@Override
	public MotivoMovimentacaoMedidor getMotivoMovimentacaoMedidor() {

		return motivoMovimentacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor
	 * #setMotivoMovimentacaoMedidor
	 * (br.com.ggas.medicao
	 * .medidor.MotivoMovimentacaoMedidor)
	 */
	@Override
	public void setMotivoMovimentacaoMedidor(MotivoMovimentacaoMedidor motivoMovimentacaoMedidor) {

		this.motivoMovimentacaoMedidor = motivoMovimentacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor#getDescricaoParecer()
	 */
	@Override
	public String getDescricaoParecer() {

		return descricaoParecer;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor
	 * #setDescricaoParecer(java.lang.String)
	 */
	@Override
	public void setDescricaoParecer(String descricaoParecer) {

		this.descricaoParecer = descricaoParecer;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor
	 * #getLocalArmazenagemOrigem()
	 */
	@Override
	public MedidorLocalArmazenagem getLocalArmazenagemOrigem() {

		return localArmazenagemOrigem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor
	 * #setLocalArmazenagemOrigem
	 * (br.com.ggas.cadastro
	 * .operacional.MedidorLocalArmazenagem)
	 */
	@Override
	public void setLocalArmazenagemOrigem(MedidorLocalArmazenagem localArmazenagemOrigem) {

		this.localArmazenagemOrigem = localArmazenagemOrigem;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor
	 * #getLocalArmazenagemDestino()
	 */
	@Override
	public MedidorLocalArmazenagem getLocalArmazenagemDestino() {

		return localArmazenagemDestino;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor
	 * #setLocalArmazenagemDestino
	 * (br.com.ggas.cadastro
	 * .operacional.MedidorLocalArmazenagem)
	 */
	@Override
	public void setLocalArmazenagemDestino(MedidorLocalArmazenagem localArmazenagemDestino) {

		this.localArmazenagemDestino = localArmazenagemDestino;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor#getFuncionario()
	 */
	@Override
	public Funcionario getFuncionario() {

		return funcionario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * MovimentacaoMedidor
	 * #setFuncionario(br.com.ggas
	 * .cadastro.funcionario.Funcionario)
	 */
	@Override
	public void setFuncionario(Funcionario funcionario) {

		this.funcionario = funcionario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
