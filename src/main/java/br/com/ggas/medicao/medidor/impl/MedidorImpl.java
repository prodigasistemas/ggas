/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe MedidorImpl representa uma MedidorImpl no sistema.
 *
 * @since 17/09/2009
 * 
 */

package br.com.ggas.medicao.medidor.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.medidor.CapacidadeMedidor;
import br.com.ggas.medicao.medidor.DiametroMedidor;
import br.com.ggas.medicao.medidor.FatorK;
import br.com.ggas.medicao.medidor.MarcaMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.ModeloMedidor;
import br.com.ggas.medicao.medidor.MovimentacaoMedidor;
import br.com.ggas.medicao.medidor.SituacaoMedidor;
import br.com.ggas.medicao.medidor.TipoMedidor;
import br.com.ggas.medicao.vazaocorretor.FaixaTemperaturaTrabalho;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;

/**
 * Classe MedidorImpl.
 * 
 *
 */
public class MedidorImpl extends EntidadeNegocioImpl implements Medidor {

	private static final long serialVersionUID = -8295251598415871427L;

	private String numeroSerie;

	private Integer anoFabricacao;

	private Date dataAquisicao;

	private Date dataMaximaInstalacao;

	private Date dataUltimaCalibracao;

	private Integer anoCalibracao;

	private String tombamento;

	private Integer digito;

	private Integer leituraAcumulada;

	private String numeroInicial;

	private Integer quantidadeLote;

	private String prefixo;

	private String sufixo;

	private SituacaoMedidor situacaoMedidor;

	private TipoMedidor tipoMedidor;

	private MarcaMedidor marcaMedidor;

	private DiametroMedidor diametroMedidor;

	private CapacidadeMedidor capacidadeMinima;

	private CapacidadeMedidor capacidadeMaxima;

	private MedidorLocalArmazenagem localArmazenagem;

	private FaixaTemperaturaTrabalho faixaTemperaturaTrabalho;

	private BigDecimal pressaoMaximaTrabalho;

	private Integer indicadorLote;

	private Unidade unidadePressaoMaximaTrabalho;

	private ModeloMedidor modelo;

	private FatorK fatorK;

	private EntidadeConteudo modoUso;

	/**
	 * Usado em outras classes.
	 */
	private String composicaoVirtual;

	private String codigoMedidorSupervisorio;

	private Collection<InstalacaoMedidor> listaInstalacaoMedidor;

	private Collection<MovimentacaoMedidor> movimentacoes = new HashSet<MovimentacaoMedidor>();

	private EntidadeConteudo situacaoAssociacaoMedidorIndependente;
	
	private BigDecimal medidaPressao;

	private Unidade unidadePressao;
	
	private BigDecimal medidaVazaoInstantanea;
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.Medidor#getFatorK
	 * ()
	 */
	@Override
	public FatorK getFatorK() {

		return fatorK;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.Medidor#setFatorK
	 * (br.com.ggas.medicao.medidor.impl.FatorK)
	 */
	@Override
	public void setFatorK(FatorK fatorK) {

		this.fatorK = fatorK;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.Medidor#getModelo
	 * ()
	 */
	@Override
	public ModeloMedidor getModelo() {

		return modelo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.Medidor#setModelo
	 * (
	 * br.com.ggas.medicao.medidor.impl.ModeloMedidor
	 * )
	 */
	@Override
	public void setModelo(ModeloMedidor modelo) {

		this.modelo = modelo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.Medidor#
	 * getIndicadorLote()
	 */
	@Override
	public Integer getIndicadorLote() {

		return indicadorLote;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.Medidor#
	 * setIndicadorLote(java.lang.Integer)
	 */
	@Override
	public void setIndicadorLote(Integer indicadorLote) {

		this.indicadorLote = indicadorLote;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.Medidor#getPrefixo
	 * ()
	 */
	@Override
	public String getPrefixo() {

		return prefixo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.Medidor#setPrefixo
	 * (java.lang.String)
	 */
	@Override
	public void setPrefixo(String prefixo) {

		this.prefixo = prefixo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.Medidor#getSufixo
	 * ()
	 */
	@Override
	public String getSufixo() {

		return sufixo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.Medidor#setSufixo
	 * (java.lang.String)
	 */
	@Override
	public void setSufixo(String sufixo) {

		this.sufixo = sufixo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getNumeroSerie()
	 */
	@Override
	public String getNumeroSerie() {

		return numeroSerie;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setNumeroSerie(java.lang.String)
	 */
	@Override
	public void setNumeroSerie(String numeroSerie) {

		this.numeroSerie = numeroSerie;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getAnoFabricacao()
	 */
	@Override
	public Integer getAnoFabricacao() {

		return anoFabricacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setAnoFabricacao(java.util.Date)
	 */
	@Override
	public void setAnoFabricacao(Integer anoFabricacao) {

		this.anoFabricacao = anoFabricacao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.impl.Medidor #getDataAquisicao()
	 */
	@Override
	public Date getDataAquisicao() {
		Date data = null;
		if (this.dataAquisicao != null) {
			data = (Date) dataAquisicao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setDataAquisicao(java.util.Date)
	 */
	@Override
	public void setDataAquisicao(Date dataAquisicao) {
		if(dataAquisicao != null){
			this.dataAquisicao = (Date) dataAquisicao.clone();
		} else {
			this.dataAquisicao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.impl.Medidor #getDataMaximaInstalacao()
	 */
	@Override
	public Date getDataMaximaInstalacao() {
		Date data = null;
		if (this.dataMaximaInstalacao != null) {
			data = (Date) dataMaximaInstalacao.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setDataMaximaInstalacao(java.util.Date)
	 */
	@Override
	public void setDataMaximaInstalacao(Date dataMaximaInstalacao) {
		if(dataMaximaInstalacao != null){
			this.dataMaximaInstalacao = (Date) dataMaximaInstalacao.clone();
		} else {
			this.dataMaximaInstalacao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getAnoCalibracao()
	 */
	@Override
	public Integer getAnoCalibracao() {

		return anoCalibracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setAnoCalibracao(java.lang.Integer)
	 */
	@Override
	public void setAnoCalibracao(Integer anoCalibracao) {

		this.anoCalibracao = anoCalibracao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getTombamento()
	 */
	@Override
	public String getTombamento() {

		return tombamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setTombamento(java.lang.String)
	 */
	@Override
	public void setTombamento(String tombamento) {

		this.tombamento = tombamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getDigito()
	 */
	@Override
	public Integer getDigito() {

		return digito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setDigito(java.lang.Integer)
	 */
	@Override
	public void setDigito(Integer digito) {

		this.digito = digito;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getLeituraAcumulada()
	 */
	@Override
	public Integer getLeituraAcumulada() {

		return leituraAcumulada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setLeituraAcumulada(java.lang.Integer)
	 */
	@Override
	public void setLeituraAcumulada(Integer leituraAcumulada) {

		this.leituraAcumulada = leituraAcumulada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.Medidor#
	 * getNumeroInicial()
	 */
	@Override
	public String getNumeroInicial() {

		return numeroInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.Medidor#
	 * setNumeroInicial(java.lang.String)
	 */
	@Override
	public void setNumeroInicial(String numeroInicial) {

		this.numeroInicial = numeroInicial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.Medidor#
	 * getQuantidadeLote()
	 */
	@Override
	public Integer getQuantidadeLote() {

		return quantidadeLote;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.Medidor#
	 * setQuantidadeLote(java.lang.Integer)
	 */
	@Override
	public void setQuantidadeLote(Integer quantidadeLote) {

		this.quantidadeLote = quantidadeLote;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getSituacaoMedidor()
	 */
	@Override
	public SituacaoMedidor getSituacaoMedidor() {

		return situacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setSituacaoMedidor
	 * (br.com.ggas.medicao.medidor
	 * .SituacaoMedidor)
	 */
	@Override
	public void setSituacaoMedidor(SituacaoMedidor situacaoMedidor) {

		this.situacaoMedidor = situacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getTipoMedidor()
	 */
	@Override
	public TipoMedidor getTipoMedidor() {

		return tipoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setTipoMedidor
	 * (br.com.ggas.medicao.medidor.TipoMedidor)
	 */
	@Override
	public void setTipoMedidor(TipoMedidor tipoMedidor) {

		this.tipoMedidor = tipoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getMarcaMedidor()
	 */
	@Override
	public MarcaMedidor getMarcaMedidor() {

		return marcaMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setMarcaMedidor
	 * (br.com.ggas.medicao.medidor.MarcaMedidor)
	 */
	@Override
	public void setMarcaMedidor(MarcaMedidor marcaMedidor) {

		this.marcaMedidor = marcaMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getDiametroMedidor()
	 */
	@Override
	public DiametroMedidor getDiametroMedidor() {

		return diametroMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setDiametroMedidor
	 * (br.com.ggas.medicao.medidor
	 * .DiametroMedidor)
	 */
	@Override
	public void setDiametroMedidor(DiametroMedidor diametroMedidor) {

		this.diametroMedidor = diametroMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getCapacidadeMinima()
	 */
	@Override
	public CapacidadeMedidor getCapacidadeMinima() {

		return capacidadeMinima;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setCapacidadeMinima
	 * (br.com.ggas.medicao.medidor
	 * .CapacidadeMedidor)
	 */
	@Override
	public void setCapacidadeMinima(CapacidadeMedidor capacidadeMinima) {

		this.capacidadeMinima = capacidadeMinima;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getCapacidadeMaxima()
	 */
	@Override
	public CapacidadeMedidor getCapacidadeMaxima() {

		return capacidadeMaxima;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setCapacidadeMaxima
	 * (br.com.ggas.medicao.medidor
	 * .CapacidadeMedidor)
	 */
	@Override
	public void setCapacidadeMaxima(CapacidadeMedidor capacidadeMaxima) {

		this.capacidadeMaxima = capacidadeMaxima;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getLocalArmazenagem()
	 */
	@Override
	public MedidorLocalArmazenagem getLocalArmazenagem() {

		return localArmazenagem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setLocalArmazenagem
	 * (br.com.ggas.cadastro.operacional
	 * .MedidorLocalArmazenagem)
	 */
	@Override
	public void setLocalArmazenagem(MedidorLocalArmazenagem localArmazenagem) {

		this.localArmazenagem = localArmazenagem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #getFaixaTemperaturaTrabalho()
	 */
	@Override
	public FaixaTemperaturaTrabalho getFaixaTemperaturaTrabalho() {

		return faixaTemperaturaTrabalho;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.medidor.impl.Medidor
	 * #setFaixaTemperaturaTrabalho
	 * (br.com.ggas.medicao
	 * .corretorvazao.FaixaTemperaturaTrabalho)
	 */
	@Override
	public void setFaixaTemperaturaTrabalho(FaixaTemperaturaTrabalho faixaTemperaturaTrabalho) {

		this.faixaTemperaturaTrabalho = faixaTemperaturaTrabalho;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.Medidor#
	 * getPressaoMaximaTrabalho()
	 */
	@Override
	public BigDecimal getPressaoMaximaTrabalho() {

		return pressaoMaximaTrabalho;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.Medidor#
	 * setPressaoMaximaTrabalho
	 * (java.math.BigDecimal)
	 */
	@Override
	public void setPressaoMaximaTrabalho(BigDecimal pressaoMaximaTrabalho) {

		this.pressaoMaximaTrabalho = pressaoMaximaTrabalho;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.Medidor#
	 * getUnidadePressaoMaximaTrabalho()
	 */
	@Override
	public Unidade getUnidadePressaoMaximaTrabalho() {

		return unidadePressaoMaximaTrabalho;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.Medidor#
	 * setUnidadePressaoMaximaTrabalho
	 * (br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadePressaoMaximaTrabalho(Unidade unidadePressaoMaximaTrabalho) {

		this.unidadePressaoMaximaTrabalho = unidadePressaoMaximaTrabalho;
	}

	@Override
	public Date getDataUltimaCalibracao() {
		Date data = null;
		if (this.dataUltimaCalibracao != null) {
			data = (Date) dataUltimaCalibracao.clone();
		}
		return data;
	}

	@Override
	public void setDataUltimaCalibracao(Date dataUltimaCalibracao) {
		if(dataUltimaCalibracao != null){
			this.dataUltimaCalibracao = (Date) dataUltimaCalibracao.clone();
		} else {
			this.dataUltimaCalibracao = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (tipoMedidor == null) {
			stringBuilder.append(TIPO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if(modelo == null) {
			stringBuilder.append(MODELO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		
		if (marcaMedidor == null) {
			stringBuilder.append(MARCA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (situacaoMedidor == null) {
			stringBuilder.append(SITUACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (capacidadeMinima == null) {
			stringBuilder.append(CAPACIDADE_MINIMA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (capacidadeMaxima == null) {
			stringBuilder.append(CAPACIDADE_MAXIMA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if (dataAquisicao == null) {
			stringBuilder.append(DATA_AQUISICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
		}

		return erros;

	}

	@Override
	public Collection<MovimentacaoMedidor> getMovimentacoes() {

		return movimentacoes;
	}

	@Override
	public void setMovimentacoes(Collection<MovimentacaoMedidor> movimentacoes) {

		this.movimentacoes = movimentacoes;
	}

	@Override
	public EntidadeConteudo getModoUso() {

		return modoUso;
	}

	@Override
	public void setModoUso(EntidadeConteudo modoUso) {

		this.modoUso = modoUso;
	}

	@Override
	public String getComposicaoVirtual() {

		return composicaoVirtual;
	}

	@Override
	public void setComposicaoVirtual(String composicaoVirtual) {

		this.composicaoVirtual = composicaoVirtual;
	}

	@Override
	public String getCodigoMedidorSupervisorio() {

		return codigoMedidorSupervisorio;
	}

	@Override
	public void setCodigoMedidorSupervisorio(String codigoMedidorSupervisorio) {

		this.codigoMedidorSupervisorio = codigoMedidorSupervisorio;
	}

	@Override
	public Collection<InstalacaoMedidor> getListaInstalacaoMedidor() {

		return listaInstalacaoMedidor;
	}

	@Override
	public void setListaInstalacaoMedidor(Collection<InstalacaoMedidor> listaInstalacaoMedidor) {

		this.listaInstalacaoMedidor = listaInstalacaoMedidor;
	}

	@Override
	public InstalacaoMedidor getInstalacaoMedidor() {

		if (listaInstalacaoMedidor != null && !listaInstalacaoMedidor.isEmpty()) {
			return Collections.max(listaInstalacaoMedidor, Comparator.comparing(InstalacaoMedidor::getData));
		}
		return null;
	}

	@Override
	public void setInstalacaoMedidor(InstalacaoMedidor instalacaoMedidor) {

		if (listaInstalacaoMedidor == null) {
			listaInstalacaoMedidor = new ArrayList<InstalacaoMedidor>();
		}
		listaInstalacaoMedidor.clear();
		if (instalacaoMedidor != null) {
			listaInstalacaoMedidor.add(instalacaoMedidor);
		}
	}

	@Override
	public EntidadeConteudo getSituacaoAssociacaoMedidorIndependente() {

		return situacaoAssociacaoMedidorIndependente;
	}

	@Override
	public void setSituacaoAssociacaoMedidorIndependente(EntidadeConteudo situacaoAssociacaoMedidorIndependente) {

		this.situacaoAssociacaoMedidorIndependente = situacaoAssociacaoMedidorIndependente;
	}
	
	@Override
	public BigDecimal getMedidaPressao() {
		return medidaPressao;
	}
	@Override
	public void setMedidaPressao(BigDecimal medidaPressao) {
		this.medidaPressao = medidaPressao;
	}
	@Override
	public Unidade getUnidadePressao() {
		return unidadePressao;
	}
	@Override
	public void setUnidadePressao(Unidade unidadePressao) {
		this.unidadePressao = unidadePressao;
	}
	@Override
	public BigDecimal getMedidaVazaoInstantanea() {
		return medidaVazaoInstantanea;
	}
	@Override
	public void setMedidaVazaoInstantanea(BigDecimal medidaVazaoInstantanea) {
		this.medidaVazaoInstantanea = medidaVazaoInstantanea;
	}
	
}
