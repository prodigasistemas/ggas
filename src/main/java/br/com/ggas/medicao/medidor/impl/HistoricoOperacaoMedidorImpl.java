/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.medidor.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.Unidade;

/**
 * Classe que implementa a interface de histórico
 * de operação de medidor.
 */
public class HistoricoOperacaoMedidorImpl extends EntidadeNegocioImpl implements HistoricoOperacaoMedidor {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 5782208551518286529L;

	private OperacaoMedidor operacaoMedidor;

	private Date dataPlanejada;

	private Date dataRealizada;

	private Medidor medidor;

	private MedidorLocalInstalacao medidorLocalInstalacao;

	private String descricao;

	private BigDecimal numeroLeitura;

	private Funcionario funcionario;

	private MotivoOperacaoMedidor motivoOperacaoMedidor;

	private BigDecimal medidaPressaoAnterior;

	private PontoConsumo pontoConsumo;

	private Unidade unidadePressaoAnterior;

	private InstalacaoMedidor instalacaoMedidor;

	private String lacre;
	
	private String lacreDois;
	
	private String lacreTres;
	
	private Boolean indicadorOperacaoConjunta;

	/**
	 * @return the medidaPressaoAnterior
	 */
	@Override
	public BigDecimal getMedidaPressaoAnterior() {

		return medidaPressaoAnterior;
	}

	/**
	 * @param medidaPressaoAnterior
	 *            the medidaPressaoAnterior to set
	 */
	@Override
	public void setMedidaPressaoAnterior(BigDecimal medidaPressaoAnterior) {

		this.medidaPressaoAnterior = medidaPressaoAnterior;
	}

	/**
	 * @return the pontoConsumo
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return the unidadePressaoAnterior
	 */
	@Override
	public Unidade getUnidadePressaoAnterior() {

		return unidadePressaoAnterior;
	}

	/**
	 * @param unidadePressaoAnterior
	 *            the unidadePressaoAnterior to
	 *            set
	 */
	@Override
	public void setUnidadePressaoAnterior(Unidade unidadePressaoAnterior) {

		this.unidadePressaoAnterior = unidadePressaoAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #getOperacaoMedidor()
	 */
	@Override
	public OperacaoMedidor getOperacaoMedidor() {

		return operacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setOperacaoMedidor(
	 * br.com.ggas.medicao.medidor
	 * .OperacaoMedidor)
	 */
	@Override
	public void setOperacaoMedidor(OperacaoMedidor operacaoMedidor) {

		this.operacaoMedidor = operacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getDataPlanejada()
	 */
	@Override
	public Date getDataPlanejada() {
		Date data = null;
		if (this.dataPlanejada != null) {
			data = (Date) dataPlanejada.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.impl. HistoricoOperacaoMedidor
	 * #setDataPlanejada(java.util.Date)
	 */
	@Override
	public void setDataPlanejada(Date dataPlanejada) {
		if (dataPlanejada != null) {
			this.dataPlanejada = (Date) dataPlanejada.clone();
		} else {
			this.dataPlanejada = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getDataRealizada()
	 */
	@Override
	public Date getDataRealizada() {
		Date data = null;
		if (this.dataRealizada != null) {
			data = (Date) dataRealizada.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.impl. HistoricoOperacaoMedidor
	 * #setDataRealizada(java.util.Date)
	 */
	@Override
	public void setDataRealizada(Date dataRealizada) {
		if (dataRealizada != null) {
			this.dataRealizada = (Date) dataRealizada.clone();
		} else {
			this.dataRealizada = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getMedidor()
	 */
	@Override
	public Medidor getMedidor() {

		return medidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setMedidor(br.com.ggas
	 * .medicao.medidor.Medidor)
	 */
	@Override
	public void setMedidor(Medidor medidor) {

		this.medidor = medidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #getMedidorLocalInstalacao()
	 */
	@Override
	public MedidorLocalInstalacao getMedidorLocalInstalacao() {

		return medidorLocalInstalacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setMedidorLocalInstalacao
	 * (br.com.ggas.medicao
	 * .leitura.MedidorLocalInstalacao)
	 */
	@Override
	public void setMedidorLocalInstalacao(MedidorLocalInstalacao medidorLocalInstalacao) {

		this.medidorLocalInstalacao = medidorLocalInstalacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getNumeroLeitura()
	 */
	@Override
	public BigDecimal getNumeroLeitura() {

		return numeroLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setNumeroLeitura(java.math.BigDecimal)
	 */
	@Override
	public void setNumeroLeitura(BigDecimal numeroLeitura) {

		this.numeroLeitura = numeroLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor#getFuncionario()
	 */
	@Override
	public Funcionario getFuncionario() {

		return funcionario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setFuncionario(br.com
	 * .ggas.cadastro.funcionario.Funcionario)
	 */
	@Override
	public void setFuncionario(Funcionario funcionario) {

		this.funcionario = funcionario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #getMotivoOperacaoMedidor()
	 */
	@Override
	public MotivoOperacaoMedidor getMotivoOperacaoMedidor() {

		return motivoOperacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.medidor.impl.
	 * HistoricoOperacaoMedidor
	 * #setMotivoOperacaoMedidor
	 * (br.com.ggas.medicao
	 * .medidor.impl.MotivoOperacaoMedidor)
	 */
	@Override
	public void setMotivoOperacaoMedidor(MotivoOperacaoMedidor motivoOperacaoMedidor) {

		this.motivoOperacaoMedidor = motivoOperacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getInstalacaoMedidor()
	 */
	@Override
	public InstalacaoMedidor getInstalacaoMedidor() {

		return instalacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #setInstalacaoMedidor
	 * (br.com.ggas.medicao.leitura
	 * .InstalacaoMedidor)
	 */
	@Override
	public void setInstalacaoMedidor(InstalacaoMedidor instalacaoMedidor) {

		this.instalacaoMedidor = instalacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public String getLacre() {

		return lacre;
	}

	@Override
	public void setLacre(String lacre) {

		this.lacre = lacre;
	}
	
	@Override
	public String getLacreDois() {
		return lacreDois;
	}

	@Override
	public void setLacreDois(String lacreDois) {
		this.lacreDois = lacreDois;
		
	}

	@Override
	public String getLacreTres() {
		return lacreTres;
	}

	@Override
	public void setLacreTres(String lacreTres) {
		this.lacreTres = lacreTres;
		
	}

	@Override
	public Boolean getIndicadorOperacaoConjunta() {
		return indicadorOperacaoConjunta;
	}

	@Override
	public void setIndicadorOperacaoConjunta(Boolean indicadorOperacaoConjunta) {
		this.indicadorOperacaoConjunta = indicadorOperacaoConjunta;
	}

}
