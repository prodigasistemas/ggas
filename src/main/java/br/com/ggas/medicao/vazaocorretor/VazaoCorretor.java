/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.vazaocorretor;

import java.util.Collection;

import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.medidor.SituacaoMedidor;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao Corretor de Vazão 
 *
 */
public interface VazaoCorretor extends EntidadeNegocio {

	String BEAN_ID_VAZAO_CORRETOR = "vazaoCorretor";

	String CORRETORES_VAZAO = "CORRETORES_VAZAO";

	String CORRETOR_VAZAO = "CORRETOR_VAZAO";

	String ANO_FABRICACAO = "CORRETOR_ANO_FABRICACAO";

	String NUMERO_SERIE = "CORRETOR_NUMERO_SERIE";

	String NUMERO_SITUACAO = "CORRETOR_SITUACAO";

	String MARCA = "CORRETOR_MARCA";

	String NUMERO_DIGITOS_LEITURA = "CORRETOR_NUMERO_DIGITOS_LEITURA";
	
	String MARCA_CORRETOR = "marcaCorretor";
	
	String ID_OPERACAO_MEDIDOR = "idOperacaoMedidor";

	/**
	 * @return the numeroSerie
	 */
	String getNumeroSerie();

	/**
	 * @param numeroSerie
	 *            the numeroSerie to set
	 */
	void setNumeroSerie(String numeroSerie);

	/**
	 * @return the marcaCorretor
	 */
	MarcaCorretor getMarcaCorretor();

	/**
	 * @param marcaCorretor
	 *            the marcaCorretor to set
	 */
	void setMarcaCorretor(MarcaCorretor marcaCorretor);

	/**
	 * @return the numeroAnoFabricacao
	 */
	Integer getNumeroAnoFabricacao();

	/**
	 * @param numeroAnoFabricacao
	 *            the numeroAnoFabricacao to set
	 */
	void setNumeroAnoFabricacao(Integer numeroAnoFabricacao);

	/**
	 * @return the tipoMostrador
	 */
	TipoMostrador getTipoMostrador();

	/**
	 * @param tipoMostrador
	 *            the tipoMostrador to set
	 */
	void setTipoMostrador(TipoMostrador tipoMostrador);

	/**
	 * @return the correcaoPressao
	 */
	Boolean getCorrecaoPressao();

	/**
	 * @param correcaoPressao
	 *            the correcaoPressao to set
	 */
	void setCorrecaoPressao(Boolean correcaoPressao);

	/**
	 * @return the correcaoTemperatura
	 */
	Boolean getCorrecaoTemperatura();

	/**
	 * @param correcaoTemperatura
	 *            the correcaoTemperatura to set
	 */
	void setCorrecaoTemperatura(Boolean correcaoTemperatura);

	/**
	 * @return the controleVazao
	 */
	Boolean getControleVazao();

	/**
	 * @param controleVazao
	 *            the controleVazao to set
	 */
	void setControleVazao(Boolean controleVazao);

	/**
	 * @return the linearizacaoFatorK
	 */
	Boolean getLinearizacaoFatorK();

	/**
	 * @param linearizacaoFatorK
	 *            the linearizacaoFatorK to set
	 */
	void setLinearizacaoFatorK(Boolean linearizacaoFatorK);

	/**
	 * @return the tipoTransdutorPressao
	 */
	TipoTransdutorPressao getTipoTransdutorPressao();

	/**
	 * @param tipoTransdutorPressao
	 *            the tipoTransdutorPressao to set
	 */
	void setTipoTransdutorPressao(TipoTransdutorPressao tipoTransdutorPressao);

	/**
	 * @return the tipoTransdutorTemperatura
	 */
	TipoTransdutorTemperatura getTipoTransdutorTemperatura();

	/**
	 * @param tipoTransdutorTemperatura
	 *            the tipoTransdutorTemperatura to
	 *            set
	 */
	void setTipoTransdutorTemperatura(TipoTransdutorTemperatura tipoTransdutorTemperatura);

	/**
	 * @return the protocoloComunicacao
	 */
	ProtocoloComunicacao getProtocoloComunicacao();

	/**
	 * @return the numeroAnoFabricacao
	 */
	Integer getNumeroDigitos();

	/**
	 * @param numeroDigitos - Set número de digitos.
	 */
	void setNumeroDigitos(Integer numeroDigitos);

	/**
	 * @return the pressaoMaximaTransdutor
	 */
	PressaoMaximaTransdutor getPressaoMaximaTransdutor();

	/**
	 * @param pressaoMaximaTransdutor
	 *            the pressaoMaximaTransdutor to
	 *            set
	 */
	void setPressaoMaximaTransdutor(PressaoMaximaTransdutor pressaoMaximaTransdutor);

	/**
	 * @return the temperaturaMaximaTransdutor
	 */
	TemperaturaMaximaTransdutor getTemperaturaMaximaTransdutor();

	/**
	 * @param temperaturaMaximaTransdutor
	 *            the temperaturaMaximaTransdutor
	 *            to set
	 */
	void setTemperaturaMaximaTransdutor(TemperaturaMaximaTransdutor temperaturaMaximaTransdutor);

	/**
	 * @param protocoloComunicacao
	 *            the protocoloComunicacao to set
	 */
	void setProtocoloComunicacao(ProtocoloComunicacao protocoloComunicacao);

	/**
	 * @return the modelo
	 */
	ModeloCorretor getModelo();

	/**
	 * @param modelo
	 *            the modelo to set
	 */
	void setModelo(ModeloCorretor modelo);

	/**
	 * @return situacaoMedidor
	 */
	SituacaoMedidor getSituacaoMedidor();

	/**
	 * @param situacaoMedidor
	 */
	void setSituacaoMedidor(SituacaoMedidor situacaoMedidor);

	/**
	 * @return String - Retorna tombamento.
	 */
	String getTombamento();

	/**
	 * @param tombamento - Set tombamento.
	 */
	void setTombamento(String tombamento);

	/**
	 * @return the localArmazenagem
	 */
	MedidorLocalArmazenagem getLocalArmazenagem();

	/**
	 * @param localArmazenagem
	 *            the localArmazenagem to set
	 */
	void setLocalArmazenagem(MedidorLocalArmazenagem localArmazenagem);

	/**
	 * @return Collection<VazaoCorretorHistoricoMovimentacao> - Retorna uma coleção de  vazão corretor histórico movimentação.
	 */
	Collection<VazaoCorretorHistoricoMovimentacao> getMovimentacoes();

	/**
	 * @param movimentacoes - Set movimentações.
	 */
	void setMovimentacoes(Collection<VazaoCorretorHistoricoMovimentacao> movimentacoes);

}
