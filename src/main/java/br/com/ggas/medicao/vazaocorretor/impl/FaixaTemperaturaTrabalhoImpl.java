/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe FaixaTemperaturaTrabalhoImpl representa uma FaixaTemperaturaTrabalhoImpl no sistema.
 *
 * @since 17/09/2009
 * 
 */

package br.com.ggas.medicao.vazaocorretor.impl;

import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.vazaocorretor.FaixaTemperaturaTrabalho;
import br.com.ggas.medicao.vazaocorretor.Unidade;

/**
 * Classe responsável por implementar os métodos relacionados as Faixas de Temperaturas de Trabalho.
 * 
 */
public class FaixaTemperaturaTrabalhoImpl extends EntidadeNegocioImpl implements FaixaTemperaturaTrabalho {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7506669126905967935L;

	private String descricao;

	private Integer medidaMinimo;

	private Integer medidaMaximo;

	private Unidade unidade;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaTemperaturaTrabalho#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaTemperaturaTrabalho
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaTemperaturaTrabalho#getMedidaMinimo()
	 */
	@Override
	public Integer getMedidaMinimo() {

		return medidaMinimo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaTemperaturaTrabalho
	 * #setMedidaMinimo(java.lang.Integer)
	 */
	@Override
	public void setMedidaMinimo(Integer medidaMinimo) {

		this.medidaMinimo = medidaMinimo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaTemperaturaTrabalho#getMedidaMaximo()
	 */
	@Override
	public Integer getMedidaMaximo() {

		return medidaMaximo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaTemperaturaTrabalho
	 * #setMedidaMaximo(java.lang.Integer)
	 */
	@Override
	public void setMedidaMaximo(Integer medidaMaximo) {

		this.medidaMaximo = medidaMaximo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * FaixaTemperaturaTrabalho#getUnidade()
	 */
	@Override
	public Unidade getUnidade() {

		return unidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * FaixaTemperaturaTrabalho
	 * #setUnidade(br.com.ggas
	 * .medicao.vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidade(Unidade unidade) {

		this.unidade = unidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public String getDescricaoFormatada() {

		StringBuilder descricaoFormatada = new StringBuilder();

		String descricaoUnidade;
		
		if (this.getUnidade().getDescricaoAbreviada() == null) {
			descricaoUnidade = this.getUnidade().getDescricao();
		} else {
			descricaoUnidade = this.getUnidade().getDescricaoAbreviada();
		}
		
		
		descricaoFormatada.append(this.getMedidaMinimo()).append(" ").append(descricaoUnidade);
		descricaoFormatada.append(" a ");
		descricaoFormatada.append(this.getMedidaMaximo()).append(" ").append(descricaoUnidade);

		return descricaoFormatada.toString();
	}

}
