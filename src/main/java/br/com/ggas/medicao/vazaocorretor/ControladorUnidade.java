/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.vazaocorretor;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.consumo.UnidadeConversao;

public interface ControladorUnidade extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_UNIDADE = "controladorUnidade";

	String ERRO_NEGOCIO_UNIDADE_PADRAO_INEXISTENTE = "ERRO_NEGOCIO_UNIDADE_PADRAO_INEXISTENTE";

	String ERRO_NEGOCIO_UNIDADE_PADRAO_NAO_UNICA = "ERRO_NEGOCIO_UNIDADE_PADRAO_NAO_UNICA";

	String ERRO_NEGOCIO_REMOVER_UNIDADE_CLASSE = "ERRO_NEGOCIO_REMOVER_UNIDADE_CLASSE";

	/**
	 * Método responsável por listar todas as
	 * unidades da classe pressão.
	 * 
	 * @return Coleção de unidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<Unidade> listarUnidadesPressao() throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * unidades da classe extensao.
	 * 
	 * @return Coleção de unidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<Unidade> listarUnidadesExtensao() throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * unidades da classe temperatura.
	 * 
	 * @return Coleção de unidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<Unidade> listarUnidadesTemperatura() throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * unidades da classe vazão.
	 * 
	 * @return Coleção de unidades.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<Unidade> listarUnidadesVazao() throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * unidades da classe volume.
	 * 
	 * @return Coleção de unidades de volume.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<Unidade> listarUnidadesVolume() throws NegocioException;

	/**
	 * Método responsável por consultar a unidade
	 * padrão de um classe informada;.
	 * 
	 * @param classeUnidade
	 *            Uma classe de unidade.
	 * @return Uma unidade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Unidade obterUnidadePadraoPorClasse(ClasseUnidade classeUnidade) throws NegocioException;

	/**
	 * Método responsável por consultar a unidade
	 * padrão para a classe
	 * temperatura;.
	 * 
	 * @return Uma unidade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Unidade obterUnidadePadraoTemperatura() throws NegocioException;

	/**
	 * Método responsável por consultar a unidade
	 * padrão para a classe
	 * pressão;.
	 * 
	 * @return Uma unidade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Unidade obterUnidadePadraoPressao() throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * unidades da classe quantidade.
	 * 
	 * @return Coleção de unidades de quantidade
	 * @throws NegocioException
	 *             Caso ocorra algum erro.
	 */
	Collection<Unidade> listarUnidadesQuantidade() throws NegocioException;

	/**
	 * Método responsavel por listar as classes
	 * unidade.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ClasseUnidade> listarClasseUnidade() throws NegocioException;

	/**
	 * Listar unidade.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ClasseUnidade> listarUnidade(Long chavePrimaria) throws NegocioException;

	/**
	 * Listar unidade habilitada.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<ClasseUnidade> listarUnidadeHabilitada(Long chavePrimaria) throws NegocioException;

	/**
	 * Lista unidades ordenadas.
	 * 
	 * @return the collection
	 */
	Collection<Unidade> listaUnidadesOrdenadas();

	/**
	 * Consultar unidade conversao.
	 * 
	 * @param unidadeDestino
	 *            the unidade destino
	 * @param unidadeOrigem
	 *            the unidade origem
	 * @return the unidade conversao
	 */
	UnidadeConversao consultarUnidadeConversao(Long unidadeDestino, Long unidadeOrigem);

	/**
	 * Recupera do banco a unidade de medida em conformidade com o filtro.
	 *
	 * @param filtro Um objeto Unidade usado como modelo na consulta
	 * @return Um objeto Unidade correspondente com o modelo, ou null
	 * @throws org.hibernate.NonUniqueResultException se o filtro for genérico demais
	 */
	Unidade obterUnidade(Unidade filtro);

	/**
	 * Obtém a unidade de Conversão utilizada para conversão de Celsius 
	 * para a unidade padrão de temperatura
	 * @return UnidadeConversao - Unidade de Conversão de Celsius para Padrão de Temperatura
	 * @throws NegocioException - {@link NegocioException}
	 */
	UnidadeConversao obterUnidadeConversaoDeCelsiusParaPadrao() throws NegocioException;
	/**
	 * Obtém as unidades padrão para conversão de temperatura e pressão.
	 * @return mapa de Unidade por constantes de ClasseUnidade
	 * @throws NegocioException - {@link NegocioException}
	 */
	Map<String, Unidade> obterUnidadesPadraoTemperaturaPressao() throws NegocioException;

	/**
	 * Obtém a unidade padrão para conversão de temperatura, do 
	 * mapa de Unidade por constantes de ClasseUnidade. Caso 
	 * não exista a unidade no mapa, realiza a consulta.
	 * @param unidadesPadraoParaConversao - {@link Map}
	 * @return Unidade - unidade de temperatura
	 * @throws NegocioException - {@link NegocioException}
	 */
	Unidade obterUnidadePadraoTemperaturaParaConversao(Map<String, Unidade> unidadesPadraoParaConversao) throws NegocioException;

	/**
	 * Obtém a unidade padrão para conversão de pressão, do 
	 * mapa de Unidade por constantes de ClasseUnidade. Caso 
	 * não exista a unidade no mapa, realiza a consulta.
	 * @param unidadesPadraoParaConversao - {@link Map}
	 * @return Unidade - unidade de pressão
	 * @throws NegocioException - {@link NegocioException}
	 */
	Unidade obterUnidadePadraoPressaoParaConversao(Map<String, Unidade> unidadesPadraoParaConversao) throws NegocioException;

}
