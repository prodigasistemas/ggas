/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.vazaocorretor.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.impl.SegmentoImpl;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimentoVO;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.ServiceLocator;

class ControladorFaixaPressaoFornecimentoImpl extends ControladorNegocioImpl implements ControladorFaixaPressaoFornecimento {

	private static final String ERRO_NEGOCIO_FAIXA_PRESSAO_EXISTENTE = "ERRO_NEGOCIO_FAIXA_PRESSAO_EXISTENTE";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(FaixaPressaoFornecimento.BEAN_ID_FAIXA_PRESSAO_FORNECIMENTO);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(FaixaPressaoFornecimento.BEAN_ID_FAIXA_PRESSAO_FORNECIMENTO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeSegmento() {

		return ServiceLocator.getInstancia().getClassPorID(Segmento.BEAN_ID_SEGMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * ControladorFaixaPressaoFornecimento
	 * #obterFaixaPressaoFornecimentoPorMedida
	 * (java.math.BigDecimal)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public FaixaPressaoFornecimento obterFaixaPressaoFornecimentoPorMedida(BigDecimal medida, Unidade unidade, Segmento segmento)
					throws NegocioException {

		FaixaPressaoFornecimento retorno = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" faixa ");
		hql.append(" left join fetch faixa.segmento segmento ");
		hql.append(" inner join fetch faixa.unidadePressao unidade ");
		hql.append(" where faixa.medidaMinimo <= :medida ");
		hql.append(" and faixa.medidaMaximo >= :medida ");
		hql.append(" and unidade.chavePrimaria = :unidade ");
		hql.append(" and segmento.chavePrimaria = :segmento ");
		hql.append(" and faixa.habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setBigDecimal("medida", medida);
		query.setLong("unidade", unidade.getChavePrimaria());
		query.setLong("segmento", segmento.getChavePrimaria());
		query.setMaxResults(1);

		Collection<FaixaPressaoFornecimento> resultado = query.list();

		if(resultado != null && !resultado.isEmpty()) {
			retorno = resultado.iterator().next();
		}

		return retorno;
	}
	

	/**
	 * Escolhe uma faixa de pressão, da lista de faixas de pressão,
	 * que possua a unidade de pressão igual a {@code unidade}, passada por
	 * parâmetro, e que tenha a unidade minima menor ou igual e unidade máxima
	 * maior ou igual que a {@code medida} do parâmetro.
	 * 
	 * @param listaFaixaPressaoFornecimento
	 * @param medida
	 * @param unidade
	 * @return faixa de pressão de fornecimento
	 */
	@Override
	public FaixaPressaoFornecimento escolherFaixaPressaoFornecimentoPorUnidadeEmedidaMaximaMinima(
					Collection<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento, BigDecimal medida, Unidade unidade) {

		FaixaPressaoFornecimento retorno = null;

		for (FaixaPressaoFornecimento faixaPressaoFornecimento : listaFaixaPressaoFornecimento) {
			Unidade unidadeFaixa = faixaPressaoFornecimento.getUnidadePressao();
			if (unidadeFaixa != null && unidadeFaixa.equals(unidade)) {
				BigDecimal medidaMinimo = faixaPressaoFornecimento.getMedidaMinimo();
				BigDecimal medidaMaximo = faixaPressaoFornecimento.getMedidaMaximo();
				if ((medidaMinimo != null && (medidaMinimo.compareTo(medida) == 0 || medidaMinimo.compareTo(medida) < 0))
								&& (medidaMaximo != null && (medidaMaximo.compareTo(medida) == 0 || medidaMaximo.compareTo(medida) > 0))) {
					retorno = faixaPressaoFornecimento;
					break;
				}
			}
		}

		return retorno;
	}
	
	/**
	 * Obtém faixas de pressão por chaves primárias
	 * de Segmento.
	 * @param chavesPrimariasSegmentos
	 * @return mapa de faixas de pressão por chave de Segmento
	 */
	@Override
	public Map<Long, Collection<FaixaPressaoFornecimento>> obterFaixaPressaoPressaoFornecimentoPorSegmentos(Long[] chavesPrimariasSegmentos) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select segmento.chavePrimaria, ");
		hql.append(" faixa ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" faixa ");
		hql.append(" left join fetch faixa.segmento segmento ");
		hql.append(" where segmento.chavePrimaria IN (:segmentos) ");
		hql.append(" and faixa.habilitado = true ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("segmentos", chavesPrimariasSegmentos);

		return this.construirMapaFaixaPressaoFornecimentoPorSegmento( query.list() );
	}
	
	/**
	 * Constrói uma mapa de faixas de pressão por chaves primárias
	 * de Segmento, a partir da lista de atributos passada por 
	 * parâmetro.
	 * @param listaAtributos
	 * @return mapa de faixas de pressão por chaves de Segmento
	 */
	private Map<Long, Collection<FaixaPressaoFornecimento>> construirMapaFaixaPressaoFornecimentoPorSegmento(List<Object[]> listaAtributos) {
		Map<Long, Collection<FaixaPressaoFornecimento>> mapaFaixaPressaoFornecimento = new HashMap<>();
		Collection<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento = null;
		for (Object[] atributos : listaAtributos ) {
			Long chaveSegmento = (Long) atributos[0];
			FaixaPressaoFornecimento faixaPressaoFornecimento = (FaixaPressaoFornecimento) atributos[1];
			if (!mapaFaixaPressaoFornecimento.containsKey(chaveSegmento)) {
				listaFaixaPressaoFornecimento = new ArrayList<>();
				mapaFaixaPressaoFornecimento.put(chaveSegmento, listaFaixaPressaoFornecimento);
			} else {
				listaFaixaPressaoFornecimento = mapaFaixaPressaoFornecimento.get(chaveSegmento);
			}
			listaFaixaPressaoFornecimento.add(faixaPressaoFornecimento);
		}
		return mapaFaixaPressaoFornecimento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.
	 * ControladorFaixaPressaoFornecimento
	 * #listarFaixaPressaoPorSegmento(
	 * br.com.ggas.cadastro.imovel.Segmento)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<FaixaPressaoFornecimento> listarFaixaPressaoFornecimentoPorSegmento(Segmento segmento) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select faixa from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" faixa ");
		hql.append(" inner join fetch faixa.segmento segmento ");
		hql.append(" inner join fetch faixa.unidadePressao unidade ");
		hql.append(" where segmento.chavePrimaria = :chavePrimariaSegmento ");
		hql.append(" and faixa.habilitado = true ");
		hql.append(" order by unidade.descricao, faixa.medidaMinimo");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaSegmento", segmento.getChavePrimaria());

		Collection<FaixaPressaoFornecimento> resultado = query.list();

		if(resultado == null || resultado.isEmpty()) {
			hql = new StringBuilder();
			hql.append(" select faixa from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" faixa ");
			hql.append(" left outer join fetch faixa.segmento segmento ");
			hql.append(" inner join fetch faixa.unidadePressao unidade ");
			hql.append(" where segmento.chavePrimaria is null ");
			hql.append(" and faixa.habilitado = true ");
			hql.append(" order by unidade.descricao, faixa.medidaMinimo");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

			resultado = query.list();
		}

		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento#listarFaixaPressaoFornecimentoSemSegmentoAssociado()
	 */
	@Override
	public Collection<FaixaPressaoFornecimento> listarFaixaPressaoFornecimentoSemSegmentoAssociado() {

		StringBuilder hql = new StringBuilder();

		hql.append(" select faixa from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" faixa ");
		hql.append(" left outer join fetch faixa.segmento segmento ");
		hql.append(" inner join fetch faixa.unidadePressao unidade ");
		hql.append(" where segmento.chavePrimaria is null ");
		hql.append(" and faixa.habilitado = true ");
		hql.append(" order by unidade.descricao, faixa.medidaMinimo");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.
	 * ControladorFaixaPressaoFornecimento
	 * #listarTodasFaixasPressaoFornecimento
	 * ()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<FaixaPressaoFornecimento> listarTodasFaixasPressaoFornecimento() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select faixa from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" faixa ");
		hql.append(" left join fetch faixa.segmento segmento ");
		hql.append(" inner join fetch faixa.unidadePressao unidade ");
		hql.append(" where faixa.habilitado = true ");
		hql.append(" order by segmento.descricao, unidade.descricao, faixa.medidaMinimo");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.
	 * ControladorFaixaPressaoFornecimento
	 * #obterFaixaPressaoFornecimento(
	 * br.com.ggas.cadastro.imovel.Segmento,
	 * java.math.BigDecimal,
	 * br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public FaixaPressaoFornecimento obterFaixaPressaoFornecimento(Segmento segmento, BigDecimal medidaMinimo, Unidade unidadePressao)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select faixa from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" faixa ");
		hql.append(" left join fetch faixa.segmento segmento ");
		hql.append(" inner join fetch faixa.unidadePressao unidade ");
		hql.append(" where faixa.habilitado = true ");
		hql.append(" and unidade.chavePrimaria = :chaveUnidade");
		hql.append(" and faixa.medidaMinimo = :medidaMinimo");
		hql.append(" and segmento.chavePrimaria = :chaveSegmento");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chaveUnidade", unidadePressao.getChavePrimaria());
		query.setBigDecimal("medidaMinimo", medidaMinimo);
		query.setLong("chaveSegmento", segmento.getChavePrimaria());
		query.setMaxResults(1);

		FaixaPressaoFornecimento faixaRetorno = (FaixaPressaoFornecimento) query.uniqueResult();

		if(faixaRetorno == null) {
			hql = new StringBuilder();
			hql.append(" select faixa from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" faixa ");
			hql.append(" left join fetch faixa.segmento segmento ");
			hql.append(" inner join fetch faixa.unidadePressao unidade ");
			hql.append(" where faixa.habilitado = true ");
			hql.append(" and unidade.chavePrimaria = :chaveUnidade");
			hql.append(" and faixa.medidaMinimo = :medidaMinimo");
			hql.append(" and segmento is null");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong("chaveUnidade", unidadePressao.getChavePrimaria());
			query.setBigDecimal("medidaMinimo", medidaMinimo);
			query.setMaxResults(1);

			faixaRetorno = (FaixaPressaoFornecimento) query.uniqueResult();
		}

		return faixaRetorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento#obterFaixaPressaoFornecimento(long)
	 */
	@Override
	public FaixaPressaoFornecimento obterFaixaPressaoFornecimento(long chavePrimaria) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(" select faixa from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" faixa ");
		hql.append(" inner join fetch faixa.segmento segmento ");
		hql.append(" inner join fetch faixa.unidadePressao unidade ");
		hql.append(" where ");
		hql.append(" faixa.chavePrimaria = :chavePrimaria ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);

		return (FaixaPressaoFornecimento) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento#consultarFaixaPressaoFornecimento(java.util.Map)
	 */
	@Override
	public Collection<FaixaPressaoFornecimento> consultarFaixaPressaoFornecimento(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}

			Long idSegmento = (Long) filtro.get("idSegmento");
			if(idSegmento != null && idSegmento > 0) {
				criteria.createAlias("segmento", "segmento");
				criteria.add(Restrictions.eq("segmento.chavePrimaria", idSegmento));
			} else if(idSegmento != null && idSegmento.compareTo(-1L) == 0) {
				criteria.createCriteria("segmento", "segmento", Criteria.LEFT_JOIN);
				criteria.add(Restrictions.isNull("segmento"));
			} 

			Long idUnidadePressao = (Long) filtro.get("idUnidadePressao");
			if(idUnidadePressao != null && idUnidadePressao > 0) {
				criteria.createAlias("unidadePressao", "unidadePressao");
				criteria.add(Restrictions.eq("unidadePressao.chavePrimaria", idUnidadePressao));
			}

			BigDecimal medidaMinimo = (BigDecimal) filtro.get("medidaMinimo");
			if(medidaMinimo != null) {
				criteria.add(Restrictions.eq("medidaMinimo", medidaMinimo));
			}

			BigDecimal medidaMaximo = (BigDecimal) filtro.get("medidaMaximo");
			if(medidaMaximo != null) {
				criteria.add(Restrictions.eq("medidaMaximo", medidaMaximo));
			}

			Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			if(habilitado != null) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado));
			}
		}

		// Paginação em banco dados
		boolean paginacaoPadrao = false;
		if(filtro != null && filtro.containsKey("colecaoPaginada")) {
			ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");

			HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);

			if(StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
				paginacaoPadrao = true;
			}

		} else {
			paginacaoPadrao = true;
		}

		if(paginacaoPadrao) {
			criteria.addOrder(Order.asc("medidaMinimo"));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento#inserirFaixaPressaoFornecimento(br.com.ggas.medicao.vazaocorretor
	 * .FaixaPressaoFornecimento)
	 */
	@Override
	public void inserirFaixaPressaoFornecimento(FaixaPressaoFornecimento faixaPressaoFornecimento) throws NegocioException {

		super.inserir(faixaPressaoFornecimento);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento#validarFaixasPressaoFornecimento(java.util.List)
	 */
	@Override
	public void validarFaixasPressaoFornecimento(List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento) throws NegocioException {

		for (FaixaPressaoFornecimento faixaPressaoFornecimento : listaFaixaPressaoFornecimento) {

			if(faixaPressaoFornecimento.getMedidaMinimo() == null || faixaPressaoFornecimento.getMedidaMaximo() == null) {
				throw new NegocioException(Constantes.ERRO_FALTA_PREENCHER_VALORES_DA_FAIXA, true);
			}

			if(faixaPressaoFornecimento.getMedidaMaximo().compareTo(faixaPressaoFornecimento.getMedidaMinimo()) < 0) {
				throw new NegocioException(Constantes.ERRO_MEDIDA_MAXIMA_MENOR, true);
			}

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento#removerFaixasPressaoFornecimento(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerFaixasPressaoFornecimento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		this.remover(chavesPrimarias, dadosAuditoria);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento#atualizarFaixasPressaoFornecimento(br.com.ggas.medicao.
	 * vazaocorretor.FaixaPressaoFornecimento)
	 */
	@Override
	public void atualizarFaixasPressaoFornecimento(FaixaPressaoFornecimento faixaPressaoFornecimento) throws NegocioException,
					ConcorrenciaException {

		this.atualizar(faixaPressaoFornecimento);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento#verificarFaixaPressaoExistente(br.com.ggas.medicao.vazaocorretor
	 * .FaixaPressaoFornecimento)
	 */
	@Override
	public void verificarFaixaPressaoExistente(FaixaPressaoFornecimento faixaPressaoFornecimento) throws NegocioException {

		Map<String, Object> erros = faixaPressaoFornecimento.validarDados();

		if((erros != null) && (!erros.isEmpty())) {
			throw new NegocioException(erros);
		}

		Map<String, Object> filtro = new HashMap<String, Object>();

		Long chavePrimaria = faixaPressaoFornecimento.getChavePrimaria();
		if(faixaPressaoFornecimento.getSegmento() != null) {
			filtro.put("idSegmento", faixaPressaoFornecimento.getSegmento().getChavePrimaria());
			filtro.put("idUnidadePressao", faixaPressaoFornecimento.getUnidadePressao().getChavePrimaria());
		}

		filtro.put("medidaMinimo", faixaPressaoFornecimento.getMedidaMinimo());
		filtro.put("medidaMaximo", faixaPressaoFornecimento.getMedidaMaximo());

		Collection<FaixaPressaoFornecimento> faixaPressao = null;

		faixaPressao = this.consultarFaixaPressaoFornecimento(filtro);

		if(faixaPressao != null && !faixaPressao.isEmpty()) {
			FaixaPressaoFornecimento faixaExistente = faixaPressao.iterator().next();

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(faixaExistente);

			if(faixaExistente.getChavePrimaria() != chavePrimaria) {
				String descricaoSegmento = "";

				if (faixaExistente.getSegmento() != null && !"".equals(faixaExistente.getSegmento().getDescricao())) {
					descricaoSegmento = "e Segmento" + faixaExistente.getSegmento().getDescricao();
				}

				throw new NegocioException(ERRO_NEGOCIO_FAIXA_PRESSAO_EXISTENTE,
								new Object[] {faixaExistente.getMedidaMinimo(), descricaoSegmento, faixaExistente.getUnidadePressao()
												.getDescricao()});
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento#listarFaixasPressaoFaturamentoRelacionadas(java.util.Map)
	 */
	@Override
	public List<FaixaPressaoFornecimento> listarFaixasPressaoFaturamentoRelacionadas(Map<String, Object> filtro) throws NegocioException {

		Long idSegmento = (Long) filtro.get("idSegmento");
		Long idUnidadePressao = (Long) filtro.get("idUnidadePressao");

		StringBuilder hql = new StringBuilder();

		hql.append(" select faixaPressao from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" contratoPontoConsumo ");
		hql.append(" inner join contratoPontoConsumo.faixaPressaoFornecimento faixaPressao ");
		hql.append(" where ");
		hql.append(" 1 = 1 ");

		if(idSegmento != null && idSegmento > 0) {
			hql.append(" and faixaPressao.segmento.chavePrimaria = :idSegmento ");
		}

		if(idSegmento != null && idSegmento > 0) {
			hql.append(" and faixaPressao.unidadePressao.chavePrimaria = :idUnidadePressao ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(idSegmento != null && idSegmento > 0) {
			query.setParameter("idSegmento", idSegmento);
		}
		if(idSegmento != null && idSegmento > 0) {
			query.setParameter("idUnidadePressao", idUnidadePressao);
		}

		return query.list();
	}

	/**
	 * Listar faixa pressao fornecimento sem segmento distinct.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public List<FaixaPressaoFornecimentoVO> listarFaixaPressaoFornecimentoSemSegmentoDistinct(Map<String, Object> filtro)
					throws NegocioException {

		Long idUnidadePressao = (Long) filtro.get("idUnidadePressao");
		Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);

		List<FaixaPressaoFornecimentoVO> listaFaixaPressaoFornecimentoVO = new ArrayList<FaixaPressaoFornecimentoVO>();

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct faixa.unidadePressao ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" faixa ");
		hql.append(" where faixa.segmento is null");

		if(idUnidadePressao != null) {
			hql.append(" and faixa.unidadePressao.chavePrimaria = :idUnidadePressao ");
		}

		if(habilitado != null) {
			hql.append(" and faixa.habilitado = :habilitado ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(idUnidadePressao != null) {
			query.setParameter("idUnidadePressao", idUnidadePressao);
		}
		if(habilitado != null) {
			query.setParameter(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado);
		}

		List<Object> resultado = (ArrayList<Object>) query.list();

		for (Iterator<Object> iterator = resultado.iterator(); iterator.hasNext();) {
			Unidade unidadePressao = (Unidade) iterator.next();

			FaixaPressaoFornecimentoVO faixaPressaoFornecimentoVO = new FaixaPressaoFornecimentoVO();

			faixaPressaoFornecimentoVO.setUnidadePressao(unidadePressao);
			faixaPressaoFornecimentoVO.setHabilitado(true);

			listaFaixaPressaoFornecimentoVO.add(faixaPressaoFornecimentoVO);

			int quantidadeRegistrosInativos = 0;
			List<FaixaPressaoFornecimento> lista = (List<FaixaPressaoFornecimento>) this.consultarFaixaPressaoFornecimento(filtro);
			for (FaixaPressaoFornecimento faixaPressaoFornecimento : lista) {
				if(!faixaPressaoFornecimento.isHabilitado()) {
					quantidadeRegistrosInativos++;
				}
			}

			if(quantidadeRegistrosInativos == lista.size()) {
				faixaPressaoFornecimentoVO.setHabilitado(false);
			}
		}

		return listaFaixaPressaoFornecimentoVO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento#listarFaixaPressaoFornecimento(java.util.Map)
	 */
	@Override
	public List<FaixaPressaoFornecimentoVO> listarFaixaPressaoFornecimento(Map<String, Object> filtro) throws NegocioException {

		Long idSegmento = (Long) filtro.get("idSegmento");
		Long idUnidadePressao = (Long) filtro.get("idUnidadePressao");
		Boolean habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);

		StringBuilder hql = new StringBuilder();

		hql.append(" select distinct faixa.segmento, faixa.unidadePressao ");
		hql.append(" from UnidadeImpl as und, SegmentoImpl as seg, FaixaPressaoFornecimentoImpl as faixa ");
		hql.append(" where faixa.unidadePressao.chavePrimaria = und.chavePrimaria ");
		hql.append(" and faixa.segmento.chavePrimaria = seg.chavePrimaria");

		if(idSegmento != null) {
			hql.append(" and seg.chavePrimaria = :idSegmento ");
		}

		if(idUnidadePressao != null) {
			hql.append(" and und.chavePrimaria = :idUnidadePressao ");
		}
		if(habilitado != null) {
			hql.append(" and faixa.habilitado = :habilitado ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(idSegmento != null) {
			query.setParameter("idSegmento", idSegmento);
		}
		if(idUnidadePressao != null) {
			query.setParameter("idUnidadePressao", idUnidadePressao);
		}
		if(habilitado != null) {
			query.setParameter(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado);
		}

		List<Object> listaObjetoFaixaPressaoFornecimento = (ArrayList<Object>) query.list();

		List<FaixaPressaoFornecimentoVO> listaFaixaPressaoFornecimentoVO = new ArrayList<FaixaPressaoFornecimentoVO>();

		int cont = 0, quantidadeRegistrosInativos = 0;
		for (Iterator<Object> iterator = listaObjetoFaixaPressaoFornecimento.iterator(); iterator.hasNext();) {
			Object[] arrayObjeto = (Object[]) iterator.next();
			FaixaPressaoFornecimentoVO faixaPressaoFornecimentoVO = new FaixaPressaoFornecimentoVO();
			for (int i = 0; i < arrayObjeto.length; i++) {

				if(arrayObjeto[i].getClass().getSimpleName().equals(SegmentoImpl.class.getSimpleName())) {

					Segmento segmento = (Segmento) arrayObjeto[i];
					faixaPressaoFornecimentoVO.setSegmento(segmento);
					faixaPressaoFornecimentoVO.setHabilitado(true);

					listaFaixaPressaoFornecimentoVO.add(faixaPressaoFornecimentoVO);

				}
				if(arrayObjeto[i].getClass().getSimpleName().equals(UnidadeImpl.class.getSimpleName())) {
					Unidade unidadePressao = (Unidade) arrayObjeto[i];
					listaFaixaPressaoFornecimentoVO.get(cont).setUnidadePressao(unidadePressao);

				}
			}
			cont++;

			List<FaixaPressaoFornecimento> lista = (List<FaixaPressaoFornecimento>) this.consultarFaixaPressaoFornecimento(filtro);

			for (FaixaPressaoFornecimento faixaPressaoFornecimento : lista) {
				if(!faixaPressaoFornecimento.isHabilitado()) {
					quantidadeRegistrosInativos++;
				}
			}
			if(quantidadeRegistrosInativos == lista.size()) {
				faixaPressaoFornecimentoVO.setHabilitado(false);
			}

		}

		if(idSegmento == null) {
			listaFaixaPressaoFornecimentoVO.addAll(listarFaixaPressaoFornecimentoSemSegmentoDistinct(filtro));
		}

		return listaFaixaPressaoFornecimentoVO;
	}

	/* (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorFaixaPressaoFornecimento#obterFaixasPressaoFornecimento(br.com.ggas.cadastro.imovel.Segmento, br.com.ggas.medicao.vazaocorretor.Unidade)
	 */
	@Override
	public Collection<FaixaPressaoFornecimento> obterFaixasPressaoFornecimento(Segmento segmento,
			Unidade unidadePressao) throws NegocioException {
	
		Map<String, Object> filtro = new HashMap<>();
		if( segmento != null ){
			filtro.put("idSegmento", segmento.getChavePrimaria());			
		}
		filtro.put("idUnidadePressao", unidadePressao.getChavePrimaria());
	
		return consultarFaixaPressaoFornecimento(filtro);
	}

}
