/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe FaixaPressaoFornecimentoImpl representa uma FaixaPressaoFornecimentoImpl no sistema.
 *
 * @since 17/09/2009
 * 
 */

package br.com.ggas.medicao.vazaocorretor.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável por implementar os métodos relacionados as Faixas de Pressão de Fornecimento
 * 
 */
public class FaixaPressaoFornecimentoImpl extends EntidadeNegocioImpl implements FaixaPressaoFornecimento {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 4958036955231010714L;

	private static final String UNIDADE_PRESSAO = "UNIDADE_PRESSAO";

	private BigDecimal medidaMinimo;

	private BigDecimal medidaMaximo;

	private BigDecimal numeroFatorZ;

	private BigDecimal numeroFatorCorrecaoPTZPCS;

	private Unidade unidadePressao;

	private Segmento segmento;

	private EntidadeConteudo entidadeConteudo;

	// atributo não persitido para indicar vinculo com a entida contratoPontoConsumo
	private boolean indicadorFaixaVinculada;

	private EntidadeConteudo indicadorCorrecaoPT;

	private EntidadeConteudo indicadorCorrecaoZ;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaPressaoFornecimento#getMedidaMinimo()
	 */
	@Override
	public BigDecimal getMedidaMinimo() {

		return medidaMinimo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaPressaoFornecimento
	 * #setMedidaMinimo(java.math.BigDecimal)
	 */
	@Override
	public void setMedidaMinimo(BigDecimal medidaMinimo) {

		this.medidaMinimo = medidaMinimo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaPressaoFornecimento#getMedidaMaximo()
	 */
	@Override
	public BigDecimal getMedidaMaximo() {

		return medidaMaximo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaPressaoFornecimento
	 * #setMedidaMaximo(java.math.BigDecimal)
	 */
	@Override
	public void setMedidaMaximo(BigDecimal medidaMaximo) {

		this.medidaMaximo = medidaMaximo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaPressaoFornecimento#getNumeroFatorZ()
	 */
	@Override
	public BigDecimal getNumeroFatorZ() {

		return numeroFatorZ;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * FaixaPressaoFornecimento
	 * #setNumeroFatorZ(java.math.BigDecimal)
	 */
	@Override
	public void setNumeroFatorZ(BigDecimal numeroFatorZ) {

		this.numeroFatorZ = numeroFatorZ;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * FaixaPressaoFornecimento
	 * #getNumeroFatorCorrecaoPTZPCS()
	 */
	@Override
	public BigDecimal getNumeroFatorCorrecaoPTZPCS() {

		return numeroFatorCorrecaoPTZPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.
	 * FaixaPressaoFornecimento
	 * #setNumeroFatorCorrecaoPTZPCS
	 * (java.math.BigDecimal
	 * )
	 */
	@Override
	public void setNumeroFatorCorrecaoPTZPCS(BigDecimal numeroFatorCorrecaoPTZPCS) {

		this.numeroFatorCorrecaoPTZPCS = numeroFatorCorrecaoPTZPCS;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * FaixaPressaoFornecimento
	 * #getUnidadePressao()
	 */
	@Override
	public Unidade getUnidadePressao() {

		return unidadePressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.
	 * FaixaPressaoFornecimento
	 * #setUnidadePressao(br.com.ggas.medicao
	 * .vazaocorretor.Unidade)
	 */
	@Override
	public void setUnidadePressao(Unidade unidadePressao) {

		this.unidadePressao = unidadePressao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * FaixaPressaoFornecimento#getSegmento()
	 */
	@Override
	public Segmento getSegmento() {

		return segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.
	 * FaixaPressaoFornecimento
	 * #setSegmento(br.com.ggas.cadastro
	 * .imovel.Segmento)
	 */
	@Override
	public void setSegmento(Segmento segmento) {

		this.segmento = segmento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * FaixaPressaoFornecimento
	 * #getDescricaoFormatada()
	 */
	@Override
	public String getDescricaoFormatada() {

		StringBuilder descricaoFormatada = new StringBuilder();

		descricaoFormatada.append(Util.converterCampoValorDecimalParaString("", this.getMedidaMinimo(), Constantes.LOCALE_PADRAO,
						Constantes.QUANTIDADE_CASAS_VALOR_DECIMAL));
		descricaoFormatada.append(" ").append(unidadePressao.getDescricaoAbreviada());

		return descricaoFormatada.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (unidadePressao == null) {
			stringBuilder.append(UNIDADE_PRESSAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0 && unidadePressao == null) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
		}

		return erros;
	}

	@Override
	public void setEntidadeConteudo(EntidadeConteudo entidadeConteudo) {

		this.entidadeConteudo = entidadeConteudo;
	}

	@Override
	public EntidadeConteudo getEntidadeConteudo() {

		return this.entidadeConteudo;
	}

	@Override
	public boolean isIndicadorFaixaVinculada() {

		return indicadorFaixaVinculada;
	}

	@Override
	public void setIndicadorFaixaVinculada(boolean indicadorFaixaVinculada) {

		this.indicadorFaixaVinculada = indicadorFaixaVinculada;
	}

	@Override
	public EntidadeConteudo getIndicadorCorrecaoPT() {

		return indicadorCorrecaoPT;
	}

	@Override
	public void setIndicadorCorrecaoPT(EntidadeConteudo indicadorCorrecaoPT) {

		this.indicadorCorrecaoPT = indicadorCorrecaoPT;
	}

	@Override
	public EntidadeConteudo getIndicadorCorrecaoZ() {

		return indicadorCorrecaoZ;
	}

	@Override
	public void setIndicadorCorrecaoZ(EntidadeConteudo indicadorCorrecaoZ) {

		this.indicadorCorrecaoZ = indicadorCorrecaoZ;
	}

}
