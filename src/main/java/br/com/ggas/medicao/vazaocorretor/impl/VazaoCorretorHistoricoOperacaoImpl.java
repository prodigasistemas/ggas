/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe VazaoCorretorImpl representa uma VazaoCorretorImpl no sistema.
 *
 * @since 17/09/2009
 * 
 */

package br.com.ggas.medicao.vazaocorretor.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.MotivoOperacaoMedidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoOperacao;

/**
 * Classe responsável pela implementação dos métodos relacionados aos Históricos de Operações do Corretor de Vazão 
 *
 */
public class VazaoCorretorHistoricoOperacaoImpl extends EntidadeNegocioImpl implements VazaoCorretorHistoricoOperacao {

	/** serialVersionUID */
	private static final long serialVersionUID = -7306404636902246554L;

	private VazaoCorretor vazaoCorretor;

	private Medidor medidor;

	private PontoConsumo pontoConsumo;

	private OperacaoMedidor operacaoMedidor;

	private MotivoOperacaoMedidor motivoOperacaoMedidor;

	private MedidorLocalInstalacao localInstalacao;

	private Date dataRealizada;

	private Funcionario funcionario;

	private BigDecimal leitura;

	private InstalacaoMedidor instalacaoMedidor;

	/**
	 * @return the medidor
	 */
	@Override
	public Medidor getMedidor() {

		return medidor;
	}

	/**
	 * @param medidor
	 *            the medidor to set
	 */
	@Override
	public void setMedidor(Medidor medidor) {

		this.medidor = medidor;
	}

	/**
	 * @return the pontoConsumo
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return the localInstalacao
	 */
	@Override
	public MedidorLocalInstalacao getLocalInstalacao() {

		return localInstalacao;
	}

	/**
	 * @param localInstalacao
	 *            the localInstalacao to set
	 */
	@Override
	public void setLocalInstalacao(MedidorLocalInstalacao localInstalacao) {

		this.localInstalacao = localInstalacao;
	}

	/**
	 * @return the vazaoCorretor
	 */
	@Override
	public VazaoCorretor getVazaoCorretor() {

		return vazaoCorretor;
	}

	/**
	 * @param vazaoCorretor
	 *            the vazaoCorretor to set
	 */
	@Override
	public void setVazaoCorretor(VazaoCorretor vazaoCorretor) {

		this.vazaoCorretor = vazaoCorretor;
	}

	/**
	 * @return the operacaoMedidor
	 */
	@Override
	public OperacaoMedidor getOperacaoMedidor() {

		return operacaoMedidor;
	}

	/**
	 * @param operacaoMedidor
	 *            the operacaoMedidor to set
	 */
	@Override
	public void setOperacaoMedidor(OperacaoMedidor operacaoMedidor) {

		this.operacaoMedidor = operacaoMedidor;
	}

	/**
	 * @return the motivoOperacaoMedidor
	 */
	@Override
	public MotivoOperacaoMedidor getMotivoOperacaoMedidor() {

		return motivoOperacaoMedidor;
	}

	/**
	 * @param motivoOperacaoMedidor
	 *            the motivoOperacaoMedidor to set
	 */
	@Override
	public void setMotivoOperacaoMedidor(MotivoOperacaoMedidor motivoOperacaoMedidor) {

		this.motivoOperacaoMedidor = motivoOperacaoMedidor;
	}

	/**
	 * Validar dados.
	 * 
	 * @return the medidorLocalInstalacao
	 */

	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public Date getDataRealizada() {
		Date data = null;
		if (this.dataRealizada != null) {
			data = (Date) dataRealizada.clone();
		}
		return data;
	}

	@Override
	public void setDataRealizada(Date dataRealizada) {
		if (dataRealizada != null) {
			this.dataRealizada = (Date) dataRealizada.clone();
		} else {
			this.dataRealizada = null;
		}
	}

	@Override
	public Funcionario getFuncionario() {

		return funcionario;
	}

	@Override
	public void setFuncionario(Funcionario funcionario) {

		this.funcionario = funcionario;
	}

	/**
	 * @return the leitura
	 */
	@Override
	public BigDecimal getLeitura() {

		return leitura;
	}

	/**
	 * @param leitura
	 *            the leitura to set
	 */
	@Override
	public void setLeitura(BigDecimal leitura) {

		this.leitura = leitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #getInstalacaoMedidor()
	 */
	@Override
	public InstalacaoMedidor getInstalacaoMedidor() {

		return instalacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.cadastro.imovel.PontoConsumo
	 * #setInstalacaoMedidor
	 * (br.com.ggas.medicao.leitura
	 * .InstalacaoMedidor)
	 */
	@Override
	public void setInstalacaoMedidor(InstalacaoMedidor instalacaoMedidor) {

		this.instalacaoMedidor = instalacaoMedidor;
	}

}
