/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.vazaocorretor;

import java.math.BigDecimal;

import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assinatura dos métodos relacionados 
 * as Faixas de Fornecimento de Pressão
 *
 */
public interface FaixaPressaoFornecimento extends EntidadeNegocio {

	String BEAN_ID_FAIXA_PRESSAO_FORNECIMENTO = "faixaPressaoFornecimento";

	String FAIXA_PRESSAO_FORNECIMENTO = "Pressão de Fornecimento";

	/**
	 * @return the medidaMinimo
	 */
	BigDecimal getMedidaMinimo();

	/**
	 * @param medidaMinimo
	 *            the medidaMinimo to set
	 */
	void setMedidaMinimo(BigDecimal medidaMinimo);

	/**
	 * @return the medidaMaximo
	 */
	BigDecimal getMedidaMaximo();

	/**
	 * @param medidaMaximo
	 *            the medidaMaximo to set
	 */
	void setMedidaMaximo(BigDecimal medidaMaximo);

	/**
	 * @return the numeroFatorZ
	 */
	BigDecimal getNumeroFatorZ();

	/**
	 * @param numeroFatorZ
	 *            the numeroFatorZ to set
	 */
	void setNumeroFatorZ(BigDecimal numeroFatorZ);

	/**
	 * @param numeroFatorCorrecaoPTZPCS
	 *            the numeroFatorCorrecaoPTZPCS to
	 *            set
	 */
	void setNumeroFatorCorrecaoPTZPCS(BigDecimal numeroFatorCorrecaoPTZPCS);

	/**
	 * @return the unidadePressao
	 */
	Unidade getUnidadePressao();

	/**
	 * @param unidadePressao
	 *            the unidadePressao to set
	 */
	void setUnidadePressao(Unidade unidadePressao);

	/**
	 * @return the segmento
	 */
	Segmento getSegmento();

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	void setSegmento(Segmento segmento);

	/**
	 * @return a descrição formatada da faixa
	 */
	String getDescricaoFormatada();

	/**
	 * @return the numeroFatorCorrecaoPTZPCS
	 */
	BigDecimal getNumeroFatorCorrecaoPTZPCS();

	/**
	 * @return EntidadeConteudo - Retorna objeto entidade conteúdo.
	 */
	EntidadeConteudo getEntidadeConteudo();

	/**
	 * @param entidadeConteudo - Set Entidade conteúdo.
	 */
	void setEntidadeConteudo(EntidadeConteudo entidadeConteudo);

	/**
	 * @return Boolean - Retorna indicador faixa vinculada.
	 */
	boolean isIndicadorFaixaVinculada();

	/**
	 * @param indicadorFaixaVinculada - Set Indicador faixa vinculada.
	 */
	void setIndicadorFaixaVinculada(boolean indicadorFaixaVinculada);

	/**
	 * @return EntidadeConteudo - Retorna objeto entidade conteudo com informações sobre indicador
	 * correção pt.
	 */
	EntidadeConteudo getIndicadorCorrecaoPT();

	/**
	 * @param indicadorCorrecaoPT - Set indicador correção Pt.
	 */
	void setIndicadorCorrecaoPT(EntidadeConteudo indicadorCorrecaoPT);

	/**
	 * @return EntidadeConteudo - Retorna um objeto com informações sobre Indicador correzão z.
	 */
	EntidadeConteudo getIndicadorCorrecaoZ();

	/**
	 * @param indicadorCorrecaoZ - Set Indicador Correção Z.
	 */
	void setIndicadorCorrecaoZ(EntidadeConteudo indicadorCorrecaoZ);

}
