/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.vazaocorretor;

import java.io.Serializable;

import br.com.ggas.cadastro.imovel.Segmento;
/**
 * Classe responsável pela transferência de dados entre as telas relacionadas 
 * a Faixa de Pressão de Fornecimento
 *
 */
public class FaixaPressaoFornecimentoVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4459326449641645645L;

	private boolean habilitado;
	private Segmento segmento;
	private Unidade unidadePressao;
	private String[] medidaMinimo;
	private String[] medidaMaximo;
	private String[] entidadeConteudo;
	private String[] numeroFatorCorrecaoPTZPCS;
	private String[] indicadorCorrecaoPT;
	private String[] indicadorCorrecaoZ;
	private String[] numeroFatorZ;
	
	public boolean isHabilitado() {
		return habilitado;
	}
	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
	public Segmento getSegmento() {
		return segmento;
	}
	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}
	public Unidade getUnidadePressao() {
		return unidadePressao;
	}
	public void setUnidadePressao(Unidade unidadePressao) {
		this.unidadePressao = unidadePressao;
	}
	public String[] getMedidaMinimo() {
		return medidaMinimo;
	}
	public void setMedidaMinimo(String[] medidaMinimo) {
		if(medidaMinimo != null) {
			this.medidaMinimo = medidaMinimo.clone();
		}
	}
	public String[] getMedidaMaximo() {
		return medidaMaximo;
	}
	public void setMedidaMaximo(String[] medidaMaximo) {
		this.medidaMaximo = medidaMaximo;
	}
	public String[] getEntidadeConteudo() {
		return entidadeConteudo;
	}
	public void setEntidadeConteudo(String[] entidadeConteudo) {
		this.entidadeConteudo = entidadeConteudo;
	}
	public String[] getNumeroFatorCorrecaoPTZPCS() {
		return numeroFatorCorrecaoPTZPCS;
	}
	public void setNumeroFatorCorrecaoPTZPCS(String[] numeroFatorCorrecaoPTZPCS) {
		this.numeroFatorCorrecaoPTZPCS = numeroFatorCorrecaoPTZPCS;
	}
	public String[] getIndicadorCorrecaoPT() {
		return indicadorCorrecaoPT;
	}
	public void setIndicadorCorrecaoPT(String[] indicadorCorrecaoPT) {
		this.indicadorCorrecaoPT = indicadorCorrecaoPT;
	}
	public String[] getIndicadorCorrecaoZ() {
		return indicadorCorrecaoZ;
	}
	public void setIndicadorCorrecaoZ(String[] indicadorCorrecaoZ) {
		this.indicadorCorrecaoZ = indicadorCorrecaoZ;
	}
	public String[] getNumeroFatorZ() {
		return numeroFatorZ;
	}
	public void setNumeroFatorZ(String[] numeroFatorZ) {
		this.numeroFatorZ = numeroFatorZ;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
