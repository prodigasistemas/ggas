/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe TipoMostradorImpl representa uma TipoMostradorImpl no sistema.
 *
 * @since 22/09/2009
 * 
 */

package br.com.ggas.medicao.vazaocorretor.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import br.com.ggas.geral.dominio.impl.EntidadeDominioImpl;
import br.com.ggas.medicao.vazaocorretor.TipoMostrador;

/**
 * 
 *
 */
class TipoMostradorImpl extends EntidadeDominioImpl implements TipoMostrador {

	/**
     * 
     */
	private static final long serialVersionUID = -5464235351640407241L;

	private static final Map<Integer, String> NOMES;

	private static final String ANALOGICO_NOME = "Analógico";

	private static final String DIGITAL_NOME = "Digital";

	private static final List<TipoMostradorImpl> TODOS;

	static {
		NOMES = new HashMap<Integer, String>();
		NOMES.put(ANALOGICO, ANALOGICO_NOME);
		NOMES.put(DIGITAL, DIGITAL_NOME);

		TODOS = new ArrayList<TipoMostradorImpl>();
		TODOS.add(new TipoMostradorImpl(ANALOGICO));
		TODOS.add(new TipoMostradorImpl(DIGITAL));
	}

	/**
	 * Instantiates a new tipo mostrador impl.
	 */
	public TipoMostradorImpl() {

		super();
	}

	/**
	 * Instantiates a new tipo mostrador impl.
	 * 
	 * @param codigo
	 *            the codigo
	 */
	public TipoMostradorImpl(int codigo) {

		super();
		this.setCodigo(codigo);
		// by gmatos
		// on 15/10/09
		// 10:21
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.dominio.impl.
	 * EntidadeDominioImpl#setCodigo(int)
	 */
	@Override
	public void setCodigo(int codigo) {

		super.setCodigo(codigo);

		if(codigo == ANALOGICO) {
			super.setDescricao(DESCRICAO_ANALOGICO);
		} else if(codigo == DIGITAL) {
			super.setDescricao(DESCRICAO_DIGITAL);
		}
	}

	/**
	 * Lista todos os Tipos Mostrador ordenados
	 * por código.
	 * 
	 * @return Collection Uma coleção de
	 *         instâncias de TipoMostrador.
	 */
	@SuppressWarnings({"rawtypes"})
	public static Collection listar() {

		return TODOS;
	}

	/**
	 * Recupera o tipo vigencia com o código
	 * informado.
	 * 
	 * @param codigo
	 *            O código do tipo vigencia
	 *            desejado.
	 * @return TipoMostrador O tipo mostrador que
	 *         possui o código informado.
	 */
	public static TipoMostrador consultar(Integer codigo) {

		TipoMostrador retorno = null;
		TipoMostrador status;
		Iterator<?> it = TODOS.iterator();
		while(it.hasNext()) {
			status = (TipoMostrador) it.next();
			if(status.getCodigo() == codigo.intValue()) {
				retorno = status;
				break;
			}
		}
		return retorno;
	}
}
