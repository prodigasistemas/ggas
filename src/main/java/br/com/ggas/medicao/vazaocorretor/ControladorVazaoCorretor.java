/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.vazaocorretor;

import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * Interface que define o controlador de vazão
 */
public interface ControladorVazaoCorretor extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_VAZAO_CORRETOR = "controladorVazaoCorretor";

	String ERRO_NEGOCIO_CORRETOR_VAZAO_JA_EXISTENTE = "ERRO_NEGOCIO_CORRETOR_VAZAO_JA_EXISTENTE";

	String MEDIDOR_LOCAL_ARMAZENAGEM = "MEDIDOR_LOCAL_ARMAZENAGEM";

	String MEDIDOR_MARCA = "MEDIDOR_MARCA";

	String MEDIDOR_NUMERO_SERIE = "MEDIDOR_NUMERO_SERIE";

	String MEDIDOR_SITUACAO = "MEDIDOR_SITUACAO";
	
	String MEDIDOR_NUMERO_DIGITO_LEITURA = "MEDIDOR_NUMERO_DIGITO_LEITURA";
	
	/**
	 * Método responsável por consultar as marcas
	 * dos corretores de vazão
	 * cadastrados.
	 * 
	 * @return Uma coleção de corretor de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<MarcaCorretor> consultarMarcasDosCorretoresDeVazaoCadastrados() throws NegocioException;

	/**
	 * Método resposável por consultar se a marca do corretor de vazão está sendo utilizada.
	 * 
	 * @param marcaCorretor
	 *            the marca corretor
	 * @return the long
	 */
	Long consultarMarcaCorretorEmUso(Long marcaCorretor);

	/**
	 * Método resposável por consultar se o modelo do corretor de vazão está sendo utilizado.
	 * 
	 * @param modeloCorretor
	 *            the modelo corretor
	 * @return the long
	 */
	Long consultarModeloCorretorEmUso(Long modeloCorretor);

	/**
	 * Método responsável por consultar o tipo
	 * mostrador dos corretores de vazão
	 * cadastrados.
	 * 
	 * @return Uma coleção de tipo mostrador.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TipoMostrador> consultarTipoMostradorDosCorretoresDeVazaoCadastrados() throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * corretores de vazão disponíveis do sistema.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção de corretores de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<VazaoCorretor> consultarVazaoCorretor(Map<String, Object> filtro) throws NegocioException;
	
	/**
	 * Método responsável por consultar as
	 * marcas dos corretores de vazão disponíveis do sistema.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção de marcas de corretores de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<MarcaCorretor> consultarMarcaCorretor(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a exclusão
	 * de corretores de vazão.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primárias dos
	 *            corretores de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarExcluirVazaoCorretor(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por excluir os
	 * corretores de vazão selecionados.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primárias dos
	 *            corretores de vazão
	 *            selecionados.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerVazaoCorretor(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por listar as marcas dos
	 * corretores de vazão do sistema.
	 * 
	 * @return coleção de marcas dos corretores de
	 *         vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<MarcaCorretor> listarMarcaVazaoCorretor() throws NegocioException;

	/**
	 * Método responsável por listar os modelos
	 * dos corretores de vazão do sistema.
	 * 
	 * @return coleção de modelos dos corretores
	 *         de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ModeloCorretor> listarModeloVazaoCorretor() throws NegocioException;

	/**
	 * Método responsável por listar os tipos
	 * mostrador dos corretores de vazão do
	 * sistema.
	 * 
	 * @return coleção de tipo mostrador do
	 *         corretor de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TipoMostrador> listarTipoMostrador() throws NegocioException;

	/**
	 * Método responsável por listar os protocolos
	 * de comunicação dos corretores de vazão do
	 * sistema.
	 * 
	 * @return coleção de protocolos de
	 *         comunicação dos corretores de
	 *         vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<ProtocoloComunicacao> listarProtocoloComunicacao() throws NegocioException;

	/**
	 * Método responsável por listar os numero de digitos
	 * para leitura dos corretores de vazão do
	 * sistema.
	 * 
	 * @return coleção de protocolos de
	 *         comunicação dos corretores de
	 *         vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Integer> listarNumeroDigitosLeitura() throws NegocioException;

	/**
	 * Método responsável por listar os Tipos de
	 * Transdutor de Pressao dos corretores de
	 * vazão do sistema.
	 * 
	 * @return coleção de Tipos de Transdutor de
	 *         Pressao dos corretores de vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TipoTransdutorPressao> listarTipoTransdutorPressao() throws NegocioException;

	/**
	 * Método responsável por listar os Tipos de
	 * Transdutor de Temperatura dos corretores de
	 * vazão do sistema.
	 * 
	 * @return coleção de Tipos de Transdutor de
	 *         Temperatura dos corretores de
	 *         vazão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TipoTransdutorTemperatura> listarTipoTransdutorTemperatura() throws NegocioException;

	/**
	 * Método responsável por listar as
	 * Temperaturas Máximas do Transdutor.
	 * 
	 * @return Coleção de Temperaturas Máximas do
	 *         Transdutor.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<TemperaturaMaximaTransdutor> listarTemperaturaMaximaTransdutor() throws NegocioException;

	/**
	 * Método responsável por listar as Pressões
	 * Máximas do Transdutor.
	 * 
	 * @return Coleção de Pressões Máximas do
	 *         Transdutor.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<PressaoMaximaTransdutor> listarPressaoMaximaTransdutor() throws NegocioException;

	/**
	 * Método responsável por obter a marca do
	 * corretor de vazão.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Uma marca.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	MarcaCorretor obterMarcaVazaoCorretor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o modelo do
	 * corretor de vazão.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Um modelo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	ModeloCorretor obterModeloCorretor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter a pressão
	 * máxima do trandutor.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Uma Pressão Máxima do Transdutor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	PressaoMaximaTransdutor obterPressaoMaximaTransdutor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter a temperatura
	 * máxima do trandutor.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return Uma Temperatura Máxima do
	 *         Transdutor.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TemperaturaMaximaTransdutor obterTemperaturaMaximaTransdutor(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o tipo
	 * mostrador.
	 * 
	 * @param codigoTipoMostrador
	 *            the codigo tipo mostrador
	 * @return Um tipo mostrador
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoMostrador obterTipoMostrador(Integer codigoTipoMostrador) throws NegocioException;

	/**
	 * Método responsável por obter o protocolo de
	 * comunicação.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return o protocolo de comunicação.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	ProtocoloComunicacao obterProtocoloComunicacao(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o numero de digitos de leitura.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return o protocolo de comunicação.
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Integer obterNumeroDigitosLeitura(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o tipo de
	 * transdutor de pressão.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return o tipo de transdutor de pressão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoTransdutorPressao obterTipoTransdutorPressao(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por obter o tipo de
	 * transdutor de temperatura.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria
	 * @return o tipo de transdutor de
	 *         temperatura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoTransdutorTemperatura obterTipoTransdutorTemperatura(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por verificar se é
	 * permitida a alteração do número de série
	 * do corretor de vazão.
	 * 
	 * @param chavePrimariaVazaoCorretor
	 *            the chave primaria vazao corretor
	 * @return true caso permita.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	boolean permiteAlteracaoNumeroSerieVazaoCorretor(long chavePrimariaVazaoCorretor) throws GGASException;

	/**
	 * Listar historico corretor vazao.
	 * 
	 * @param idCorretorVazao
	 *            the id corretor vazao
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<VazaoCorretorHistoricoOperacao> listarHistoricoCorretorVazao(Long idCorretorVazao) throws NegocioException;

	/**
	 * Criar vazao corretor historico operacao.
	 * 
	 * @return VazaoCorretorHistoricoOperacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	VazaoCorretorHistoricoOperacao criarVazaoCorretorHistoricoOperacao() throws NegocioException;

	/**
	 * Listar vazao corretor historico movimentacao.
	 * 
	 * @param idCorretorVazao
	 *            the id corretor vazao
	 * @return VazaoCorretorHistoricoMovimentacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<VazaoCorretorHistoricoMovimentacao> listarVazaoCorretorHistoricoMovimentacao(Long idCorretorVazao) throws NegocioException;

	/**
	 * Método responsável por verificar se é
	 * permitido alterar a situacao do corretor de
	 * vazao.
	 * 
	 * @param chavePrimariaVazaoCorretor
	 *            the chave primaria vazao corretor
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean permiteAlteracaoSituacaoCorretorVazao(long chavePrimariaVazaoCorretor) throws NegocioException;

	/**
	 * Método responsável por listar as operações
	 * permitidas por tipo de associação entre
	 * ponto de consumo e medidor / vazaoCorretor.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<VazaoCorretor> consultarVazaoCorretorDisponivel(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar historico corretor vazao.
	 * 
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @param codigoOperacaoMedidorInstalacao
	 *            the codigo operacao medidor instalacao
	 * @param codigoOperacaoMedidorRetirada
	 *            the codigo operacao medidor retirada
	 * @param chaveCorretorVazao
	 *            the chave corretor vazao
	 * @param dataLeitura
	 *            the data leitura
	 * @param chaveMedidor
	 *            chaveMedidor
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<VazaoCorretorHistoricoOperacao> consultarHistoricoCorretorVazao(Long chavePontoConsumo,
					Long codigoOperacaoMedidorInstalacao, Long codigoOperacaoMedidorRetirada, Long chaveCorretorVazao, Date dataLeitura,
					Long chaveMedidor) throws NegocioException;

	/**
	 * Validar corretor vazao sem ponto consumo.
	 * 
	 * @param listaVazaoCorretor
	 *            the lista vazao corretor
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarCorretorVazaoSemPontoConsumo(Collection<VazaoCorretorHistoricoOperacao> listaVazaoCorretor) throws NegocioException;

	/**
	 * Verificar registros associados.
	 * 
	 * @param idVazaoCorretor
	 *            the id vazao corretor
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean verificarRegistrosAssociados(Long idVazaoCorretor) throws NegocioException;

	/**
	 * Consultar historico operacao vazao corretor.
	 * 
	 * @param chaveVazaoCorretor
	 *            the chave vazao corretor
	 * @param chavePontoConsumo
	 *            the chave ponto consumo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<VazaoCorretorHistoricoOperacao> consultarHistoricoOperacaoVazaoCorretor(Long chaveVazaoCorretor, Long chavePontoConsumo)
					throws NegocioException;

	/**
	 * Validar vazao corretor existente por numero serie e marca.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param numeroSerie
	 *            the numero serie
	 * @param marcaCorretor
	 *            the marca corretor
	 * @return true, if successful
	 */
	boolean validarVazaoCorretorExistentePorNumeroSerieEMarca(Long chavePrimaria, String numeroSerie, Long marcaCorretor);

	/**
	 * Inserir corretor vazao.
	 * 
	 * @param entidadeNegocio
	 *            the entidade negocio
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 */
	long inserirCorretorVazao(EntidadeNegocio entidadeNegocio) throws NegocioException;
	
	
	/**
	 * Gera uma movimentação de aquisição para o histórico de Movimentação do Corretor de Vazão.
	 * 
	 * @param vazaoCorretor
	 *            the vazao corretor
	 * @return the vazao corretor historico movimentacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	VazaoCorretorHistoricoMovimentacao criarMovimentacaoCorretorVazao(VazaoCorretor vazaoCorretor) throws NegocioException;

	/**
	 * Criar movimentacao transferencia corretor vazao.
	 * 
	 * @param vazaoCorretor
	 *            the vazao corretor
	 * @param localArmazenagemOrigem
	 *            the local armazenagem origem
	 * @param localArmazenagemDestino
	 *            the local armazenagem destino
	 * @return the vazao corretor historico movimentacao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	VazaoCorretorHistoricoMovimentacao criarMovimentacaoTransferenciaCorretorVazao(VazaoCorretor vazaoCorretor,
					MedidorLocalArmazenagem localArmazenagemOrigem, MedidorLocalArmazenagem localArmazenagemDestino)
					throws NegocioException;



	/**
	 * Consulta uma coleção de entidades VazaCorretorHistoricoOperacao, 
	 * obtendo os seguintes atributos: {@code leitura}, {@code dataRealizada},
	 * {@code operacaoMedidor}, {@code vazaoCorretor}
	 * 
	 * @param chavePontoConsumo - {@link Long}
	 * @param codigoOperacaoMedidorInstalacao - {@link Long}
	 * @param codigoOperacaoMedidorRetirada - {@link Long}
	 * @param chaveCorretorVazao - {@link Long}
	 * @param dataLeitura - {@link Date}
	 * @param chaveMedidor - {@link Long}
	 * @return coleção de VazaoCorretorHistoricoOperacao
	 * @throws NegocioException
	 *             the negocio exception
	 **/
	Collection<VazaoCorretorHistoricoOperacao> consultarHistoricoOperacaoCorretorVazao(Long chavePontoConsumo,
					Long codigoOperacaoMedidorInstalacao, Long codigoOperacaoMedidorRetirada, Long chaveCorretorVazao, Date dataLeitura,
					Long chaveMedidor) throws NegocioException;
	/**
	 * Validar dados de alteração do corretor de vazao
	 * 
	 * @param vazaoCorretor - {@link VazaoCorretor}
	 * @throws NegocioException 
	 * 			   The negocio exception
	 */
	void validarDadosAlteracaoVazaoCorretor(VazaoCorretor vazaoCorretor) throws NegocioException;
}
