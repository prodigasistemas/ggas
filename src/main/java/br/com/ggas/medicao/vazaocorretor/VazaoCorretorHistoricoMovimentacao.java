/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.vazaocorretor;

import java.util.Date;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.medidor.MotivoMovimentacaoMedidor;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Histórico de Movimentação do Corretor de Vazão
 *
 */
public interface VazaoCorretorHistoricoMovimentacao extends EntidadeNegocio {

	String BEAN_ID_VAZAO_CORRETOR_HISTORICO_MOVIMENTACAO = "vazaoCorretorHistoricoMovimentacao";

	/**
	 * @return the dataMovimento
	 */
	Date getDataMovimento();

	/**
	 * @return the DataMovimentoFormatada
	 */
	String getDataMovimentoFormatada();

	/**
	 * @param dataMovimento
	 *            the dataMovimento to set
	 */
	void setDataMovimento(Date dataMovimento);

	/**
	 * @return the medidor
	 */
	VazaoCorretor getVazaoCorretor();

	/**
	 * @param vazaoCorretor, Set Vazão corretor.
	 */
	void setVazaoCorretor(VazaoCorretor vazaoCorretor);

	/**
	 * @return the motivoMovimentacaoMedidor
	 */
	MotivoMovimentacaoMedidor getMotivoMovimentacaoMedidor();

	/**
	 * @param motivoMovimentacaoMedidor
	 *            the motivoMovimentacaoMedidor to
	 *            set
	 */
	void setMotivoMovimentacaoMedidor(MotivoMovimentacaoMedidor motivoMovimentacaoMedidor);

	/**
	 * @return the descricaoParecer
	 */
	String getDescricaoParecer();

	/**
	 * @param descricaoParecer
	 *            the descricaoParecer to set
	 */
	void setDescricaoParecer(String descricaoParecer);

	/**
	 * @return the localArmazenagemOrigem
	 */
	MedidorLocalArmazenagem getLocalArmazenagemOrigem();

	/**
	 * @param localArmazenagemOrigem
	 *            the localArmazenagemOrigem to
	 *            set
	 */
	void setLocalArmazenagemOrigem(MedidorLocalArmazenagem localArmazenagemOrigem);

	/**
	 * @return the localArmazenagemDestino
	 */
	MedidorLocalArmazenagem getLocalArmazenagemDestino();

	/**
	 * @param localArmazenagemDestino
	 *            the localArmazenagemDestino to
	 *            set
	 */
	void setLocalArmazenagemDestino(MedidorLocalArmazenagem localArmazenagemDestino);

	/**
	 * @return the funcionario
	 */
	Funcionario getFuncionario();

	/**
	 * @param funcionario
	 *            the funcionario to set
	 */
	void setFuncionario(Funcionario funcionario);

}
