/*
 * Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 * 
 * Este programa é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 * 
 * O GGAS é distribuído na expectativa de ser útil, mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de COMERCIALIZAÇÃO ou de
 * ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR. Consulte a Licença Pública Geral GNU para obter mais detalhes.
 * 
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa; se não, escreva para Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 * 
 * 
 * Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 * 
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 * 
 * GGAS is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; version 2 of the License.
 * 
 * GGAS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.medicao.vazaocorretor.impl;

import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.dominio.EntidadeDominio;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.bens.CorretorVazaoMovimentacao;
import br.com.ggas.medicao.leitura.ControladorInstalacaoMedidor;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.medidor.MotivoMovimentacaoMedidor;
import br.com.ggas.medicao.medidor.impl.MotivoMovimentacaoMedidorImpl;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.MarcaCorretor;
import br.com.ggas.medicao.vazaocorretor.ModeloCorretor;
import br.com.ggas.medicao.vazaocorretor.PressaoMaximaTransdutor;
import br.com.ggas.medicao.vazaocorretor.ProtocoloComunicacao;
import br.com.ggas.medicao.vazaocorretor.TemperaturaMaximaTransdutor;
import br.com.ggas.medicao.vazaocorretor.TipoMostrador;
import br.com.ggas.medicao.vazaocorretor.TipoTransdutorPressao;
import br.com.ggas.medicao.vazaocorretor.TipoTransdutorTemperatura;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoMovimentacao;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoOperacao;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.HibernateCriteriaUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ControladorVazaoCorretorImpl extends ControladorNegocioImpl implements ControladorVazaoCorretor {

	private static final String PONTO_CONSUMO = "pontoConsumo";

	private static final String VAZAO_CORRETOR = "vazaoCorretor";

	private static final String MODELO = "modelo";

	private static final String NUMERO_SERIE = "numeroSerie";

	private static final String CHAVE_PRIMARIA = "chavePrimaria";

	private static final String CHAVES_PRIMARIAS = "chavesPrimarias";

	private static final String MARCA_CORRETOR = "marcaCorretor";

	private static final String HABILITADO = "habilitado";

	private static final String DESCRICAO = "descricao";

	private static final String ATRIBUTO_CODIGO_OPERACAO_MEDIDOR_INSTALACAO = "codigoOperacaoMedidorInstalacao";

	private ControladorPontoConsumo controladorPontoConsumo;

	private ControladorInstalacaoMedidor controladorInstalacaoMedidor;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(VazaoCorretor.BEAN_ID_VAZAO_CORRETOR);
	}

	/**
	 * @param controladorPontoConsumo the controladorPontoConsumo to set
	 */

	public void setControladorPontoConsumo(ControladorPontoConsumo controladorPontoConsumo) {

		this.controladorPontoConsumo = controladorPontoConsumo;
	}

	/**
	 * @return the controladorInstalacaoMedidor
	 */
	public ControladorInstalacaoMedidor getControladorInstalacaoMedidor() {

		return controladorInstalacaoMedidor;
	}

	/**
	 * @param controladorInstalacaoMedidor the controladorInstalacaoMedidor to set
	 */

	public void setControladorInstalacaoMedidor(ControladorInstalacaoMedidor controladorInstalacaoMedidor) {

		this.controladorInstalacaoMedidor = controladorInstalacaoMedidor;
	}

	/**
	 * Criar tipo mostrador.
	 * 
	 * @return the entidade dominio
	 */
	public EntidadeDominio criarTipoMostrador() {

		return (EntidadeDominio) ServiceLocator.getInstancia().getBeanPorID(TipoMostrador.BEAN_ID_TIPO_MOSTRADOR);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl. ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(VazaoCorretor.BEAN_ID_VAZAO_CORRETOR);
	}

	public Class<?> getClasseEntidadeMarcaVazaoCorretor() {

		return ServiceLocator.getInstancia().getClassPorID(MarcaCorretor.BEAN_ID_MARCA_CORRETOR);
	}

	public Class<?> getClasseEntidadeVazaoCorretorHistoricoOperacao() {

		return ServiceLocator.getInstancia().getClassPorID(VazaoCorretorHistoricoOperacao.BEAN_ID_VAZAO_CORRETOR_HISTORICO_OPERACAO);
	}

	public Class<?> getClasseEntidadeInstalacaoMedidor() {

		return ServiceLocator.getInstancia().getClassPorID(InstalacaoMedidor.BEAN_ID_INSTALACAO_MEDIDOR);
	}

	public Class<?> getClasseEntidadeProtocoloComunicacao() {

		return ServiceLocator.getInstancia().getClassPorID(ProtocoloComunicacao.BEAN_ID_PROTOCOLO_COMUNICACAO);
	}

	public Class<?> getClasseEntidadeTipoTransdutorPressao() {

		return ServiceLocator.getInstancia().getClassPorID(TipoTransdutorPressao.BEAN_ID_TIPO_TRANSDUTOR_PRESSAO);
	}

	public Class<?> getClasseEntidadeTipoTransdutorTemperatura() {

		return ServiceLocator.getInstancia().getClassPorID(TipoTransdutorTemperatura.BEAN_ID_TIPO_TRANSDUTOR_TEMPERATURA);
	}

	public Class<?> getClasseEntidadeTemperaturaMaximaTransdutor() {

		return ServiceLocator.getInstancia().getClassPorID(TemperaturaMaximaTransdutor.BEAN_ID_TEMPERATURA_MAXIMA_TRANSDUTOR);
	}

	public Class<?> getClasseEntidadePressaoMaximaTransdutor() {

		return ServiceLocator.getInstancia().getClassPorID(PressaoMaximaTransdutor.BEAN_ID_PRESSAO_MAXIMA_TRANSDUTOR);
	}

	public Class<?> getClasseEntidadeModeloCorretor() {

		return ServiceLocator.getInstancia().getClassPorID(ModeloCorretor.BEAN_ID_MODELO_CORRETOR);
	}

	public Class<?> getClasseEntidadeVazaoCorretorHistoricoMovimentacao() {

		return ServiceLocator.getInstancia()
				.getClassPorID(VazaoCorretorHistoricoMovimentacao.BEAN_ID_VAZAO_CORRETOR_HISTORICO_MOVIMENTACAO);
	}

	public Class<?> getClasseEntidadeVazaoCorretorMovimentacao() {

		return ServiceLocator.getInstancia().getClassPorID(CorretorVazaoMovimentacao.BEAN_ID_CORRETOR_VAZAO_MOVIMENTACAO);
	}

	public Class<?> getClasseEntidadeParametroSistema() {

		return ServiceLocator.getInstancia().getClassPorID(ParametroSistema.BEAN_ID_PARAMETRO_SISTEMA);
	}

	/**
	 * Criar movimentacao vazao corretor.
	 * 
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarMovimentacaoVazaoCorretor() {

		return (EntidadeNegocio) ServiceLocator.getInstancia()
				.getBeanPorID(VazaoCorretorHistoricoMovimentacao.BEAN_ID_VAZAO_CORRETOR_HISTORICO_MOVIMENTACAO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #consultarMarcasDosCorretoresDeVazaoCadastrados ()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<MarcaCorretor> consultarMarcasDosCorretoresDeVazaoCadastrados() throws NegocioException {

		Criteria criteria = createCriteria(MarcaCorretor.class);
		criteria.setProjection(Projections.property(DESCRICAO));
		criteria.add(Restrictions.eq(HABILITADO, Boolean.TRUE));
		criteria.setProjection(Projections.groupProperty(DESCRICAO).as("marca")).addOrder(Order.asc("marca"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# consultarMarcaCorretorEmUso(java.lang.Long)
	 */
	@Override
	public Long consultarMarcaCorretorEmUso(Long marcaCorretor) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select marcaCorretor.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where marcaCorretor = :marcaCorretor");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(MARCA_CORRETOR, marcaCorretor);

		return (Long) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# consultarModeloCorretorEmUso(java.lang.Long)
	 */
	@Override
	public Long consultarModeloCorretorEmUso(Long modeloCorretor) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select modelo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where modelo = :modeloCorretor");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("modeloCorretor", modeloCorretor);

		return (Long) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor# consultarTipoMostradorDosCorretoresDeVazaoCadastrados ()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<TipoMostrador> consultarTipoMostradorDosCorretoresDeVazaoCadastrados() throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.setProjection(Projections.property("tipoMostrador"));
		criteria.add(Restrictions.eq(HABILITADO, Boolean.TRUE));
		criteria.setProjection(Projections.groupProperty("tipoMostrador").as("tipo")).addOrder(Order.asc("tipo"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #consultarVazaoCorretor(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<VazaoCorretor> consultarVazaoCorretor(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			String numeroSerie = (String) filtro.get(NUMERO_SERIE);
			if (!StringUtils.isEmpty(numeroSerie)) {
				criteria.add(Restrictions.eq(NUMERO_SERIE, numeroSerie));
			}

			String numeroSerieParcial = (String) filtro.get("numeroSerieParcial");
			if (!StringUtils.isEmpty(numeroSerieParcial)) {
				criteria.add(Restrictions.ilike(NUMERO_SERIE, Util.formatarTextoConsulta(numeroSerieParcial)));
			}

			Long idMarca = (Long) filtro.get("idMarca");
			if ((idMarca != null) && (idMarca > 0)) {
				criteria.createAlias(MARCA_CORRETOR, MARCA_CORRETOR);
				criteria.add(Restrictions.eq("marcaCorretor.chavePrimaria", idMarca));
			} else {
				criteria.setFetchMode(MARCA_CORRETOR, FetchMode.JOIN);
			}

			Long idModelo = (Long) filtro.get("idModelo");
			if ((idModelo != null) && (idModelo > 0)) {
				criteria.createAlias(MODELO, MODELO);
				criteria.add(Restrictions.eq("modelo.chavePrimaria", idModelo));
			} else {
				criteria.setFetchMode(MODELO, FetchMode.JOIN);
			}

			Integer codigoTipoMostrador = (Integer) filtro.get("codigoTipoMostrador");
			if (codigoTipoMostrador != null) {
				criteria.add(Restrictions.eq("tipoMostrador.codigo", codigoTipoMostrador));
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}
			Boolean corretorVirtual = (Boolean) filtro.get("corretorVirtual");
			if (corretorVirtual != null) {
				criteria.add(Restrictions.eq("vazaoCorretorVirtual", corretorVirtual));
			}
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# consultarHistoricoOperacaoVazaoCorretor(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<VazaoCorretorHistoricoOperacao> consultarHistoricoOperacaoVazaoCorretor(Long chaveVazaoCorretor,
			Long chavePontoConsumo) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeVazaoCorretorHistoricoOperacao());
		if (chaveVazaoCorretor != null && chaveVazaoCorretor > 0) {
			criteria.createAlias("VazaoCorretor", VAZAO_CORRETOR);
			criteria.add(Restrictions.eq("VazaoCorretor.chavePrimaria", chaveVazaoCorretor));
		} else {
			criteria.setFetchMode(VAZAO_CORRETOR, FetchMode.JOIN);
		}
		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			criteria.createAlias(PONTO_CONSUMO, PONTO_CONSUMO);
			criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", chavePontoConsumo));
		} else {
			criteria.setFetchMode(PONTO_CONSUMO, FetchMode.JOIN);
			criteria.setFetchMode("pontoConsumo.imovel", FetchMode.JOIN);
		}
		criteria.setFetchMode("medidorLocalInstalacao", FetchMode.JOIN);
		criteria.setFetchMode("motivoOperacaoMedidor", FetchMode.JOIN);
		criteria.setFetchMode("funcionario", FetchMode.JOIN);
		criteria.setFetchMode("unidadePressaoAnterior", FetchMode.JOIN);
		criteria.setFetchMode("operacaoMedidor", FetchMode.JOIN);
		criteria.setFetchMode("localInstalacaoMedidor", FetchMode.JOIN);
		criteria.setFetchMode("leitura", FetchMode.JOIN);

		criteria.addOrder(Order.desc("dataRealizada"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.medidor.ControladorMedidor #consultarMedidor(java.util.Map)
	 */
	@Override
	@SuppressWarnings({ "unchecked", "unused" })
	public Collection<VazaoCorretor> consultarVazaoCorretorDisponivel(Map<String, Object> filtro) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long codigoMedidorDisponivel =
				Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MEDIDOR_DISPONIVEL));

		Criteria criteria = getCriteria();

		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			String numeroSerie = (String) filtro.get(NUMERO_SERIE);
			if (!StringUtils.isEmpty(numeroSerie)) {
				criteria.add(Restrictions.ilike(NUMERO_SERIE, Util.formatarTextoConsulta(numeroSerie)));
			}

			String numeroSerieParcial = (String) filtro.get("numeroSerieParcial");
			if (!StringUtils.isEmpty(numeroSerieParcial)) {
				criteria.add(Restrictions.ilike(NUMERO_SERIE, Util.formatarTextoConsulta(numeroSerieParcial)));
			}

			String tombamento = (String) filtro.get("tombamento");
			if (!StringUtils.isEmpty(tombamento)) {
				criteria.add(Restrictions.ilike("tombamento", Util.formatarTextoConsulta(tombamento)));
			}

			Long idMarca = (Long) filtro.get("idMarca");
			if ((idMarca != null) && (idMarca > 0)) {
				criteria.createAlias(MARCA_CORRETOR, MARCA_CORRETOR);
				criteria.add(Restrictions.eq("marcaCorretor.chavePrimaria", idMarca));
			} else {
				criteria.setFetchMode(MARCA_CORRETOR, FetchMode.JOIN);
			}

			Long idModelo = (Long) filtro.get("idModelo");
			if ((idModelo != null) && (idModelo > 0)) {
				criteria.createAlias(MODELO, MODELO);
				criteria.add(Restrictions.eq("modelo.chavePrimaria", idModelo));
			} else {
				criteria.setFetchMode(MODELO, FetchMode.JOIN);
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}
			Boolean corretorVirtual = (Boolean) filtro.get("corretorVirtual");
			if (corretorVirtual != null) {
				criteria.add(Restrictions.eq("vazaoCorretorVirtual", corretorVirtual));
			}

			criteria.add(Restrictions.eq("situacaoMedidor.chavePrimaria", codigoMedidorDisponivel));
			// Paginação em banco dados
			boolean paginacaoPadrao = false;
			if (filtro.containsKey("colecaoPaginada")) {
				ColecaoPaginada colecaoPaginada = (ColecaoPaginada) filtro.get("colecaoPaginada");
				HibernateCriteriaUtil.paginarConsultaPorCriteria(colecaoPaginada, criteria);
				if (StringUtils.isEmpty(colecaoPaginada.getSortCriterion())) {
					paginacaoPadrao = true;
				}
			} else {
				paginacaoPadrao = true;
			}

		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #validarExcluirVazaoCorretor (java.lang.Long[])
	 */
	@Override
	public void validarExcluirVazaoCorretor(Long[] chavesPrimarias) throws NegocioException {

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# validarCorretorVazaoSemPontoConsumo(java.util.Collection)
	 */
	@Override
	public void validarCorretorVazaoSemPontoConsumo(Collection<VazaoCorretorHistoricoOperacao> listaVazaoCorretor) throws NegocioException {

		if (listaVazaoCorretor == null || listaVazaoCorretor.isEmpty()) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CORRETOR_VAZAO_SEM_HISTORICO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #permiteAlteracaoNumeroSerieVazaoCorretor (long)
	 */
	@Override
	public boolean permiteAlteracaoNumeroSerieVazaoCorretor(long chavePrimariaVazaoCorretor) throws GGASException {

		boolean permiteAlteracao = true;

		Integer quantidade = obterQuantidadeCorretoresLigadosMedidoresAtivos(chavePrimariaVazaoCorretor);
		if ((quantidade != null) && (quantidade > 0)) {
			permiteAlteracao = false;
		}

		return permiteAlteracao;
	}

	/**
	 * Método que obtem a quantidade de corretores de vazão que estão lincados a medidores ativos.
	 * 
	 * @param chavePrimariaVazaoCorretor the chave primaria vazao corretor
	 * @return quantidade de corretores de vazão lincados a medidores ativos.
	 * @throws NegocioException the negocio exception
	 */
	private Integer obterQuantidadeCorretoresLigadosMedidoresAtivos(long chavePrimariaVazaoCorretor) throws NegocioException {

		Long resultado = null;

		this.controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		this.controladorInstalacaoMedidor = (ControladorInstalacaoMedidor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorInstalacaoMedidor.BEAN_ID_CONTROLADOR_INSTALACAO_MEDIDOR);

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT count(ponto) ");
		hql.append(" FROM ").append(this.controladorPontoConsumo.getClasseEntidade().getSimpleName()).append(" ponto ");
		hql.append(" WHERE ponto.instalacaoMedidor.medidor.chavePrimaria in ");
		hql.append(" 	(SELECT instalacaoMedidor.medidor.chavePrimaria ");
		hql.append(" 	 FROM ").append(this.controladorInstalacaoMedidor.getClasseEntidade().getSimpleName()).append(" instalacaoMedidor ");
		hql.append(" 	 WHERE instalacaoMedidor.vazaoCorretor.chavePrimaria = :chavePrimariaVazaoCorretor) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaVazaoCorretor", chavePrimariaVazaoCorretor);

		resultado = (Long) query.uniqueResult();

		return resultado.intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #removerVazaoCorretor(java.lang.Long[])
	 */
	@Override
	public void removerVazaoCorretor(Long[] chavesPrimarias) throws NegocioException {

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVES_PRIMARIAS, chavesPrimarias);
			Collection<VazaoCorretor> listaVazao = this.consultarVazaoCorretor(filtro);
			if ((listaVazao != null) && (!listaVazao.isEmpty())) {
				for (VazaoCorretor vazaoCorretor : listaVazao) {
					if (existeVazaoCorretorHistoricoOperacao(vazaoCorretor.getChavePrimaria()).equals(false)) {
						super.remover(vazaoCorretor);
					} else {
						throw new NegocioException(Constantes.ERRO_NEGOCIO_CORRETOR_NAO_PODE_SER_REMOVIDO, true);
					}
				}
			}
		}
	}

	/**
	 * Existe vazao corretor historico operacao.
	 * 
	 * @param idCorretorVazao the id corretor vazao
	 * @return the boolean
	 */
	private Boolean existeVazaoCorretorHistoricoOperacao(Long idCorretorVazao) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeVazaoCorretorHistoricoOperacao().getSimpleName());
		hql.append(" vazaoCorretorHistoricoOperacao ");
		hql.append(" inner join fetch vazaoCorretorHistoricoOperacao.vazaoCorretor vazaoCorretor ");
		hql.append(" where ");
		hql.append(" vazaoCorretor.chavePrimaria = :idCorretorVazao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCorretorVazao", idCorretorVazao);

		List<VazaoCorretorHistoricoOperacao> list = query.list();

		if (list.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Montar consulta.
	 * 
	 * @param classe the classe
	 * @return the criteria
	 * @throws NegocioException the negocio exception
	 */
	private Criteria montarConsulta(Class<?> classe) throws NegocioException {

		Criteria criteria = createCriteria(classe);
		criteria.add(Restrictions.eq(HABILITADO, Boolean.TRUE));
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #listarTipoMostrador()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<TipoMostrador> listarTipoMostrador() throws NegocioException {

		return TipoMostradorImpl.listar();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #listarMarcaVazaoCorretor()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<MarcaCorretor> listarMarcaVazaoCorretor() throws NegocioException {

		Criteria criteria = montarConsulta(getClasseEntidadeMarcaVazaoCorretor());
		criteria.addOrder(Order.asc(DESCRICAO));
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# listarModeloVazaoCorretor()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ModeloCorretor> listarModeloVazaoCorretor() throws NegocioException {

		Criteria criteria = montarConsulta(getClasseEntidadeModeloCorretor());
		criteria.addOrder(Order.asc(DESCRICAO));
		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# listarProtocoloComunicacao()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ProtocoloComunicacao> listarProtocoloComunicacao() throws NegocioException {

		return montarConsulta(getClasseEntidadeProtocoloComunicacao()).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# listarNumeroDigitosLeitura()
	 */
	/**
	 * @deprecated
	 */
	@Override
	@Deprecated
	public Collection<Integer> listarNumeroDigitosLeitura() throws NegocioException {

		// FIXME: Utilizar o método de busca de parâmetros
		List<Integer> retorno = new ArrayList<Integer>();

		String hql = "from ParametroSistemaImpl where codigo = 'NUMERO_MAXIMO_DIGITOS_CORRETOR'";
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);

		ParametroSistema parametroSistema = (ParametroSistema) query.uniqueResult();
		Integer maxDigitos = Integer.valueOf(parametroSistema.getValor());

		for (int i = 1; i <= maxDigitos; i++) {
			retorno.add(i);
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #listarTipoTransdutorPressao()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<TipoTransdutorPressao> listarTipoTransdutorPressao() throws NegocioException {

		return montarConsulta(getClasseEntidadeTipoTransdutorPressao()).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #listarTipoTransdutorTemperatura()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<TipoTransdutorTemperatura> listarTipoTransdutorTemperatura() throws NegocioException {

		return montarConsulta(getClasseEntidadeTipoTransdutorTemperatura()).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #listarTemperaturaMaximaTransdutor()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<TemperaturaMaximaTransdutor> listarTemperaturaMaximaTransdutor() throws NegocioException {

		return montarConsulta(getClasseEntidadeTemperaturaMaximaTransdutor()).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# listarPressaoMaximaTransdutor()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<PressaoMaximaTransdutor> listarPressaoMaximaTransdutor() throws NegocioException {

		return montarConsulta(getClasseEntidadePressaoMaximaTransdutor()).list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #obterMarcaVazaoCorretor(long)
	 */
	@Override
	public MarcaCorretor obterMarcaVazaoCorretor(long chavePrimaria) throws NegocioException {

		return (MarcaCorretor) super.obter(chavePrimaria, getClasseEntidadeMarcaVazaoCorretor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #obterModeloCorretor(long)
	 */
	@Override
	public ModeloCorretor obterModeloCorretor(long chavePrimaria) throws NegocioException {

		return (ModeloCorretor) super.obter(chavePrimaria, getClasseEntidadeModeloCorretor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #obterTipoMostrador(java.lang.Integer)
	 */
	@Override
	public TipoMostrador obterTipoMostrador(Integer codigoTipoMostrador) throws NegocioException {

		return TipoMostradorImpl.consultar(codigoTipoMostrador);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #obterProtocoloComunicacao(long)
	 */
	@Override
	public ProtocoloComunicacao obterProtocoloComunicacao(long chavePrimaria) throws NegocioException {

		return (ProtocoloComunicacao) super.obter(chavePrimaria, getClasseEntidadeProtocoloComunicacao());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #obterNumeroDigitosLeitura(long)
	 */
	@Override
	public Integer obterNumeroDigitosLeitura(long chavePrimaria) throws NegocioException {

		// TODO implementar logica

		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# obterTipoTransdutorPressao(long)
	 */
	@Override
	public TipoTransdutorPressao obterTipoTransdutorPressao(long chavePrimaria) throws NegocioException {

		return (TipoTransdutorPressao) super.obter(chavePrimaria, getClasseEntidadeTipoTransdutorPressao());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# obterTipoTransdutorTemperatura(long)
	 */
	@Override
	public TipoTransdutorTemperatura obterTipoTransdutorTemperatura(long chavePrimaria) throws NegocioException {

		return (TipoTransdutorTemperatura) super.obter(chavePrimaria, getClasseEntidadeTipoTransdutorTemperatura());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor#obterPressaoMaximaTransdutor(long)
	 */
	@Override
	public PressaoMaximaTransdutor obterPressaoMaximaTransdutor(long chavePrimaria) throws NegocioException {

		return (PressaoMaximaTransdutor) super.obter(chavePrimaria, getClasseEntidadePressaoMaximaTransdutor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #obterTemperaturaMaximaTransdutor(long)
	 */
	@Override
	public TemperaturaMaximaTransdutor obterTemperaturaMaximaTransdutor(long chavePrimaria) throws NegocioException {

		return (TemperaturaMaximaTransdutor) super.obter(chavePrimaria, getClasseEntidadeTemperaturaMaximaTransdutor());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com. ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		validarCorretorVazaoExistente((VazaoCorretor) entidadeNegocio);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br. com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		validarCorretorVazaoExistente((VazaoCorretor) entidadeNegocio);
		validarDadosAlteracaoVazaoCorretor((VazaoCorretor) entidadeNegocio);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# inserirCorretorVazao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public long inserirCorretorVazao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Long chavePrimaria = null;
		VazaoCorretor corretor = (VazaoCorretor) entidadeNegocio;
		corretor.getNumeroSerie();

		this.validarDadosInclusaoAlteracaoVazaoCorretor(corretor);

		corretor.getMovimentacoes().add(criarMovimentacaoCorretorVazao(corretor));
		corretor.setUltimaAlteracao(new Date());

		chavePrimaria = super.inserir(corretor);

		return chavePrimaria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# criarMovimentacaoCorretorVazao(br.com.ggas.medicao.vazaocorretor.
	 * VazaoCorretor )
	 */
	@Override
	public VazaoCorretorHistoricoMovimentacao criarMovimentacaoCorretorVazao(VazaoCorretor vazaoCorretor) throws NegocioException {

		Calendar calendar = Calendar.getInstance();

		// Inserir Historico Movimentação Vazão Corretor
		VazaoCorretorHistoricoMovimentacao movimentacaoVazaoCorretor =
				(VazaoCorretorHistoricoMovimentacao) criarMovimentacaoVazaoCorretor();
		movimentacaoVazaoCorretor.setDataMovimento(calendar.getTime());
		movimentacaoVazaoCorretor.setHabilitado(true);
		movimentacaoVazaoCorretor.setDadosAuditoria(vazaoCorretor.getDadosAuditoria());
		movimentacaoVazaoCorretor.setVazaoCorretor(vazaoCorretor);
		movimentacaoVazaoCorretor.setUltimaAlteracao(new Date());

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		long codigoMotivoMovimentacaoEntrada = Long
				.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MOTIVO_MOVIMENTACAO_MEDIDOR_ENTRADA));

		MotivoMovimentacaoMedidor motivoMovimentacaoMedidor =
				(MotivoMovimentacaoMedidor) this.obter(codigoMotivoMovimentacaoEntrada, MotivoMovimentacaoMedidorImpl.class);
		movimentacaoVazaoCorretor.setMotivoMovimentacaoMedidor(motivoMovimentacaoMedidor);

		movimentacaoVazaoCorretor.setLocalArmazenagemDestino(vazaoCorretor.getLocalArmazenagem());

		return movimentacaoVazaoCorretor;
	}

	/**
	 * Validar dados inclusao alteracao vazao corretor.
	 * 
	 * @param vazaoCorretor the vazao corretor
	 * @throws NegocioException the negocio exception
	 */
	private void validarDadosInclusaoAlteracaoVazaoCorretor(VazaoCorretor vazaoCorretor) throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (vazaoCorretor.getChavePrimaria() <= 0) {
			if (vazaoCorretor.getNumeroSerie() == null) {
				stringBuilder.append(MEDIDOR_NUMERO_SERIE);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if (vazaoCorretor.getMarcaCorretor() == null) {
				stringBuilder.append(MEDIDOR_MARCA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if (vazaoCorretor.getSituacaoMedidor() == null) {
				stringBuilder.append(MEDIDOR_SITUACAO);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if (vazaoCorretor.getNumeroDigitos() == null) {
				stringBuilder.append(MEDIDOR_NUMERO_DIGITO_LEITURA);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}
			if (vazaoCorretor.getLocalArmazenagem() == null) {
				stringBuilder.append(MEDIDOR_LOCAL_ARMAZENAGEM);
				stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
			}

		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
		}

	}

	/**
	 * Validar corretor vazao existente.
	 * 
	 * @param vazaoCorretor the vazao corretor
	 * @throws NegocioException the negocio exception
	 */
	private void validarCorretorVazaoExistente(VazaoCorretor vazaoCorretor) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT count(chavePrimaria) FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" WHERE numeroSerie = :numeroSerie ");

		if (vazaoCorretor.getMarcaCorretor() != null) {
			hql.append(" AND marcaCorretor.chavePrimaria = :codMarcaCorretor ");
		}

		if (vazaoCorretor.getChavePrimaria() > 0) {
			hql.append(" and chavePrimaria <> :idVazaoCorretor ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString(NUMERO_SERIE, vazaoCorretor.getNumeroSerie());

		if (vazaoCorretor.getMarcaCorretor() != null) {
			query.setLong("codMarcaCorretor", vazaoCorretor.getMarcaCorretor().getChavePrimaria());
		}

		if (vazaoCorretor.getChavePrimaria() > 0) {
			query.setLong("idVazaoCorretor", vazaoCorretor.getChavePrimaria());
		}

		Long resultado = (Long) query.uniqueResult();

		if (resultado != null && resultado > 0) {
			throw new NegocioException(ControladorVazaoCorretor.ERRO_NEGOCIO_CORRETOR_VAZAO_JA_EXISTENTE,
					vazaoCorretor.getNumeroSerie().toString());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# validarVazaoCorretorExistentePorNumeroSerieEMarca(java.lang.Long,
	 * java.lang.String, java.lang.Long)
	 */
	@Override
	public boolean validarVazaoCorretorExistentePorNumeroSerieEMarca(Long chavePrimaria, String numeroSerie, Long marcaCorretor) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(chavePrimaria) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where numeroSerie = :numeroSerie ");
		hql.append(" and marcaCorretor.chavePrimaria = :marcaCorretor ");

		if (chavePrimaria != null && chavePrimaria > 0) {
			hql.append(" and chavePrimaria <> :chavePrimaria ");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString(NUMERO_SERIE, numeroSerie);
		if (chavePrimaria != null && chavePrimaria > 0) {
			query.setLong("idMedidor", chavePrimaria);
		}

		query.setLong(MARCA_CORRETOR, marcaCorretor);

		Long resultado = (Long) query.uniqueResult();

		return resultado != null && resultado > 0;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# listarHistoricoCorretorVazao(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<VazaoCorretorHistoricoOperacao> listarHistoricoCorretorVazao(Long idCorretorVazao) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeVazaoCorretorHistoricoOperacao().getSimpleName());
		hql.append(" vazaoCorretorHistoricoOperacao ");
		hql.append(" inner join fetch vazaoCorretorHistoricoOperacao.vazaoCorretor vazaoCorretor ");
		hql.append(" inner join fetch vazaoCorretorHistoricoOperacao.operacaoMedidor operacaoMedidor ");
		hql.append(" inner join fetch vazaoCorretorHistoricoOperacao.motivoOperacaoMedidor motivoOperacaoMedidor ");
		hql.append(" inner join fetch vazaoCorretorHistoricoOperacao.pontoConsumo pontoConsumo ");
		hql.append(" inner join fetch vazaoCorretorHistoricoOperacao.funcionario ");
		hql.append(" inner join fetch pontoConsumo.imovel imovel ");
		hql.append(" inner join fetch vazaoCorretorHistoricoOperacao.medidor medidor ");
		hql.append(" where ");
		hql.append(" vazaoCorretor.chavePrimaria = :idCorretorVazao ");
		hql.append(" order by ");
		hql.append(" vazaoCorretorHistoricoOperacao.dataRealizada desc");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idCorretorVazao", idCorretorVazao);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# listarVazaoCorretorHistoricoMovimentacao(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<VazaoCorretorHistoricoMovimentacao> listarVazaoCorretorHistoricoMovimentacao(Long idCorretorVazao)
			throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeVazaoCorretorHistoricoMovimentacao());
		if (idCorretorVazao != null) {

			if (idCorretorVazao > 0) {
				criteria.createAlias(VAZAO_CORRETOR, VAZAO_CORRETOR);
				criteria.add(Restrictions.eq("vazaoCorretor.chavePrimaria", idCorretorVazao));
			}
			criteria.setFetchMode("motivoMovimentacaoMedidor", FetchMode.JOIN);
			criteria.setFetchMode("localArmazenagemOrigem", FetchMode.JOIN);
			criteria.setFetchMode("localArmazenagemDestino", FetchMode.JOIN);
			criteria.setFetchMode("funcionario", FetchMode.JOIN);
			criteria.addOrder(Order.desc("dataMovimento"));
		}

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #criarVazaoCorretorHistoricoOperacao()
	 */
	@Override
	public VazaoCorretorHistoricoOperacao criarVazaoCorretorHistoricoOperacao() throws NegocioException {

		return (VazaoCorretorHistoricoOperacao) ServiceLocator.getInstancia()
				.getBeanPorID(VazaoCorretorHistoricoOperacao.BEAN_ID_VAZAO_CORRETOR_HISTORICO_OPERACAO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor. ControladorVazaoCorretor #permiteAlteracaoSituacaoCorretorVazao (long)
	 */
	@Override
	public boolean permiteAlteracaoSituacaoCorretorVazao(long chavePrimariaCorretorVazao) throws NegocioException {

		boolean permiteAlteracao = true;

		StringBuilder hql = new StringBuilder();
		hql.append(" select situacaoMedidor.usoSistema from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where chavePrimaria = :chavePrimariaCorretorVazao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaCorretorVazao", chavePrimariaCorretorVazao);

		Boolean usoSistema = (Boolean) query.uniqueResult();

		if (usoSistema != null && usoSistema) {
			permiteAlteracao = false;
		}

		if (permiteAlteracao) {
			Integer quantidadePontos = obterQuantidadeVazaoCorretorInstalados(chavePrimariaCorretorVazao);
			if (quantidadePontos != null && quantidadePontos > 0) {
				permiteAlteracao = false;
			}
		}

		return permiteAlteracao;

	}

	/**
	 * Obter quantidade vazao corretor instalados.
	 * 
	 * @param chavePrimariaCorretorVazao the chave primaria corretor vazao
	 * @return the integer
	 * @throws NegocioException the negocio exception
	 */
	private Integer obterQuantidadeVazaoCorretorInstalados(long chavePrimariaCorretorVazao) throws NegocioException {

		ControladorPontoConsumo ctlPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		Long resultado = 0L;
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT count(ponto) FROM ");
		hql.append(ctlPontoConsumo.getClasseEntidade().getSimpleName()).append(" ponto ");
		hql.append(" WHERE ");
		hql.append(" ponto.instalacaoMedidor.vazaoCorretor.chavePrimaria = :chavePrimariaCorretorVazao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimariaCorretorVazao", chavePrimariaCorretorVazao);

		resultado = (Long) query.uniqueResult();
		return resultado.intValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# consultarHistoricoCorretorVazao(java.lang.Long, java.lang.Long,
	 * java.lang.Long, java.lang.Long, java.util.Date)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<VazaoCorretorHistoricoOperacao> consultarHistoricoCorretorVazao(Long chavePontoConsumo,
			Long codigoOperacaoMedidorInstalacao, Long codigoOperacaoMedidorRetirada, Long chaveCorretorVazao, Date dataLeitura,
			Long chaveMedidor) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeVazaoCorretorHistoricoOperacao().getSimpleName());
		hql.append(" vazaoCorretorHistoricoOperacao ");
		hql.append(" inner join fetch vazaoCorretorHistoricoOperacao.operacaoMedidor operacaoMedidor ");
		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			hql.append(" inner join fetch vazaoCorretorHistoricoOperacao.pontoConsumo pontoConsumo ");
		}
		hql.append(" inner join fetch vazaoCorretorHistoricoOperacao.medidor medidor ");
		hql.append(" where ");

		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			hql.append(" pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		}

		if (chaveMedidor != null && chaveMedidor > 0) {
			hql.append(" medidor.chavePrimaria = :chaveMedidor ");
		}

		if (codigoOperacaoMedidorInstalacao != null && codigoOperacaoMedidorRetirada != null) {
			hql.append(" and operacaoMedidor.chavePrimaria in (:codigoOperacaoMedidorInstalacao, :codigoOperacaoMedidorRetirada) ");
		}

		if (codigoOperacaoMedidorInstalacao != null && codigoOperacaoMedidorRetirada == null) {

			hql.append(" and operacaoMedidor.chavePrimaria = :codigoOperacaoMedidorInstalacao ");
		}

		if (chaveCorretorVazao != null) {
			hql.append(" and vazaoCorretorHistoricoOperacao.vazaoCorretor.chavePrimaria = :chaveCorretorVazao ");
		}

		if (dataLeitura != null) {

			hql.append(" and vazaoCorretorHistoricoOperacao.dataRealizada < :dataLeitura ");
			hql.append(" order by vazaoCorretorHistoricoOperacao.dataRealizada desc ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			query.setLong("chavePontoConsumo", chavePontoConsumo);
		}

		if (codigoOperacaoMedidorInstalacao != null && codigoOperacaoMedidorRetirada != null) {

			query.setLong(ATRIBUTO_CODIGO_OPERACAO_MEDIDOR_INSTALACAO, codigoOperacaoMedidorInstalacao);
			query.setLong("codigoOperacaoMedidorRetirada", codigoOperacaoMedidorRetirada);
		}

		if (codigoOperacaoMedidorInstalacao != null) {

			query.setLong(ATRIBUTO_CODIGO_OPERACAO_MEDIDOR_INSTALACAO, codigoOperacaoMedidorInstalacao);
		}

		if (chaveCorretorVazao != null) {

			query.setLong("chaveCorretorVazao", chaveCorretorVazao);
		}

		if (dataLeitura != null) {
			query.setDate("dataLeitura", dataLeitura);
		}

		if (chaveMedidor != null && chaveMedidor > 0) {
			query.setLong("chaveMedidor", chaveMedidor);
		}
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# verificarRegistrosAssociados(java.lang.Long)
	 */
	@Override
	public boolean verificarRegistrosAssociados(Long idVazaoCorretor) throws NegocioException {

		boolean retorno = false;

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		StringBuilder hql = new StringBuilder();

		hql.append(" select count(*) from ");
		hql.append(getClasseEntidadeInstalacaoMedidor().getSimpleName());
		hql.append(" MI  where MI.vazaoCorretor.chavePrimaria = ?");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idVazaoCorretor);
		Long result = (Long) query.uniqueResult();
		if (result >= 1) {
			retorno = true;
		} else {
			hql = new StringBuilder();

			hql.append(" select count(*) from ");
			hql.append(getClasseEntidadeVazaoCorretorMovimentacao().getSimpleName());
			hql.append(" VCM  where VCM.corretorVazao.chavePrimaria = ?");
			hql.append(" and (select valor from ");
			hql.append(getClasseEntidadeParametroSistema().getSimpleName());
			hql.append(" where codigo = '")
					.append(controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_MOTIVO_MOVIMENTACAO_MEDIDOR_ENTRADA))
					.append("') <> VCM.motivoMovimentacao.chavePrimaria ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong(0, idVazaoCorretor);
			result = (Long) query.uniqueResult();
			if (result >= 1) {
				retorno = true;
			} else {
				hql = new StringBuilder();

				hql.append(" select count(*) from ");
				hql.append(getClasseEntidadeVazaoCorretorHistoricoOperacao().getSimpleName());
				hql.append(" VCHO  where VCHO.vazaoCorretor.chavePrimaria = ?");
				query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
				query.setLong(0, idVazaoCorretor);
				result = (Long) query.uniqueResult();

				if (result >= 1) {
					retorno = true;
				}
			}
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor# criarMovimentacaoTransferenciaCorretorVazao(br.com.ggas.medicao.
	 * vazaocorretor .VazaoCorretor, br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem,
	 * br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem)
	 */
	@Override
	public VazaoCorretorHistoricoMovimentacao criarMovimentacaoTransferenciaCorretorVazao(VazaoCorretor vazaoCorretor,
			MedidorLocalArmazenagem localArmazenagemOrigem, MedidorLocalArmazenagem localArmazenagemDestino) throws NegocioException {

		Calendar calendar = Calendar.getInstance();

		// Inserir Historico Movimentação Vazão Corretor
		VazaoCorretorHistoricoMovimentacao movimentacaoVazaoCorretor =
				(VazaoCorretorHistoricoMovimentacao) criarMovimentacaoVazaoCorretor();
		movimentacaoVazaoCorretor.setDataMovimento(calendar.getTime());
		movimentacaoVazaoCorretor.setHabilitado(true);
		movimentacaoVazaoCorretor.setDadosAuditoria(vazaoCorretor.getDadosAuditoria());
		movimentacaoVazaoCorretor.setVazaoCorretor(vazaoCorretor);
		movimentacaoVazaoCorretor.setUltimaAlteracao(new Date());

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		long codigoMotivoMovimentacao = Long.parseLong(
				controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_MOTIVO_MOVIMENTACAO_MEDIDOR_TRANSFERENCIA));

		MotivoMovimentacaoMedidor motivoMovimentacaoMedidor =
				(MotivoMovimentacaoMedidor) this.obter(codigoMotivoMovimentacao, MotivoMovimentacaoMedidorImpl.class);
		movimentacaoVazaoCorretor.setMotivoMovimentacaoMedidor(motivoMovimentacaoMedidor);

		movimentacaoVazaoCorretor.setLocalArmazenagemOrigem(localArmazenagemOrigem);
		movimentacaoVazaoCorretor.setLocalArmazenagemDestino(localArmazenagemDestino);

		return movimentacaoVazaoCorretor;
	}

	/**
	 * Validar dados alteracao vazao corretor.
	 * 
	 * @param vazaoCorretor the vazao corretor
	 * @throws NegocioException the negocio exception
	 */
	public void validarDadosAlteracaoVazaoCorretor(VazaoCorretor vazaoCorretor) throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (vazaoCorretor.getLocalArmazenagem() == null) {
			stringBuilder.append(MEDIDOR_LOCAL_ARMAZENAGEM);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));

		}

	}

	@Override
	public Collection<MarcaCorretor> consultarMarcaCorretor(Map<String, Object> filtro) throws NegocioException {
		Criteria criteria = createCriteria(MarcaCorretor.class);

		if (filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(CHAVES_PRIMARIAS);
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(CHAVE_PRIMARIA, chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get(CHAVE_PRIMARIA);
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(CHAVE_PRIMARIA, chavePrimaria));
			}

			String descricao = (String) filtro.get(DESCRICAO);
			if (!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(DESCRICAO, descricao, MatchMode.ANYWHERE));
			}

			String descricaoAbreviada = (String) filtro.get("descricaoAbreviada");
			if (!StringUtils.isEmpty(descricaoAbreviada)) {
				criteria.add(Restrictions.ilike("descricaoAbreviada", descricaoAbreviada, MatchMode.ANYWHERE));
			}
		}

		return criteria.list();
	}

	/**
	 * Consulta uma coleção de entidades VazaCorretorHistoricoOperacao, obtendo os seguintes atributos: {@code leitura},
	 * {@code dataRealizada}, {@code operacaoMedidor}, {@code vazaoCorretor}
	 * 
	 * @param chavePontoConsumo
	 * @param codigoOperacaoMedidorInstalacao
	 * @param codigoOperacaoMedidorRetirada
	 * @param chaveCorretorVazao
	 * @param dataLeitura
	 * @param chaveMedidor
	 * @return coleção de VazaoCorretorHistoricoOperacao
	 **/
	@Override
	public Collection<VazaoCorretorHistoricoOperacao> consultarHistoricoOperacaoCorretorVazao(Long chavePontoConsumo,
			Long codigoOperacaoMedidorInstalacao, Long codigoOperacaoMedidorRetirada, Long chaveCorretorVazao, Date dataLeitura,
			Long chaveMedidor) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select ");
		hql.append(" vazaoCorretorHistoricoOperacao.leitura as leitura, ");
		hql.append(" vazaoCorretorHistoricoOperacao.dataRealizada as dataRealizada, ");
		hql.append(" operacaoMedidor as operacaoMedidor, ");
		hql.append(" vazaoCorretorHistoricoOperacao.vazaoCorretor as vazaoCorretor ");
		hql.append(" from ");
		hql.append(getClasseEntidadeVazaoCorretorHistoricoOperacao().getSimpleName());
		hql.append(" vazaoCorretorHistoricoOperacao ");
		hql.append(" inner join vazaoCorretorHistoricoOperacao.operacaoMedidor operacaoMedidor ");
		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			hql.append(" inner join vazaoCorretorHistoricoOperacao.pontoConsumo pontoConsumo ");
		}
		hql.append(" inner join vazaoCorretorHistoricoOperacao.medidor medidor ");
		hql.append(" where ");

		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			hql.append(" pontoConsumo.chavePrimaria = :chavePontoConsumo ");
		}

		if (chaveMedidor != null && chaveMedidor > 0) {
			hql.append(" medidor.chavePrimaria = :chaveMedidor ");
		}

		if (codigoOperacaoMedidorInstalacao != null && codigoOperacaoMedidorRetirada != null) {

			hql.append(" and operacaoMedidor.chavePrimaria in (:codigoOperacaoMedidorInstalacao, :codigoOperacaoMedidorRetirada) ");
		}

		if (codigoOperacaoMedidorInstalacao != null && codigoOperacaoMedidorRetirada == null) {

			hql.append(" and operacaoMedidor.chavePrimaria = :codigoOperacaoMedidorInstalacao ");
		}

		if (chaveCorretorVazao != null) {

			hql.append(" and vazaoCorretorHistoricoOperacao.vazaoCorretor.chavePrimaria = :chaveCorretorVazao ");
		}

		if (dataLeitura != null) {

			hql.append(" and vazaoCorretorHistoricoOperacao.dataRealizada < :dataLeitura ");
			hql.append(" order by vazaoCorretorHistoricoOperacao.dataRealizada desc ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidadeVazaoCorretorHistoricoOperacao()));

		if (chavePontoConsumo != null && chavePontoConsumo > 0) {
			query.setLong("chavePontoConsumo", chavePontoConsumo);
		}

		if (codigoOperacaoMedidorInstalacao != null && codigoOperacaoMedidorRetirada != null) {

			query.setLong(ATRIBUTO_CODIGO_OPERACAO_MEDIDOR_INSTALACAO, codigoOperacaoMedidorInstalacao);
			query.setLong("codigoOperacaoMedidorRetirada", codigoOperacaoMedidorRetirada);
		}

		if (codigoOperacaoMedidorInstalacao != null) {

			query.setLong(ATRIBUTO_CODIGO_OPERACAO_MEDIDOR_INSTALACAO, codigoOperacaoMedidorInstalacao);
		}

		if (chaveCorretorVazao != null) {

			query.setLong("chaveCorretorVazao", chaveCorretorVazao);
		}

		if (dataLeitura != null) {
			query.setDate("dataLeitura", dataLeitura);
		}

		if (chaveMedidor != null && chaveMedidor > 0) {
			query.setLong("chaveMedidor", chaveMedidor);
		}
		return query.list();
	}

}
