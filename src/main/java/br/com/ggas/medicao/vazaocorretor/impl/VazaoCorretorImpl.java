/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe VazaoCorretorImpl representa uma VazaoCorretorImpl no sistema.
 *
 * @since 17/09/2009
 * 
 */

package br.com.ggas.medicao.vazaocorretor.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.medidor.SituacaoMedidor;
import br.com.ggas.medicao.vazaocorretor.MarcaCorretor;
import br.com.ggas.medicao.vazaocorretor.ModeloCorretor;
import br.com.ggas.medicao.vazaocorretor.PressaoMaximaTransdutor;
import br.com.ggas.medicao.vazaocorretor.ProtocoloComunicacao;
import br.com.ggas.medicao.vazaocorretor.TemperaturaMaximaTransdutor;
import br.com.ggas.medicao.vazaocorretor.TipoMostrador;
import br.com.ggas.medicao.vazaocorretor.TipoTransdutorPressao;
import br.com.ggas.medicao.vazaocorretor.TipoTransdutorTemperatura;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoMovimentacao;
import br.com.ggas.util.Constantes;

public class VazaoCorretorImpl extends EntidadeNegocioImpl implements VazaoCorretor {

	/** serialVersionUID */
	private static final long serialVersionUID = -7306404636902246554L;

	private String numeroSerie;

	private MarcaCorretor marcaCorretor;

	private Integer numeroAnoFabricacao;

	private TipoMostrador tipoMostrador;

	private Boolean correcaoPressao;

	private Boolean correcaoTemperatura;

	private Boolean controleVazao;

	private Boolean linearizacaoFatorK;

	private TipoTransdutorPressao tipoTransdutorPressao;

	private TipoTransdutorTemperatura tipoTransdutorTemperatura;

	private ProtocoloComunicacao protocoloComunicacao;

	private Integer numeroDigitos;

	private PressaoMaximaTransdutor pressaoMaximaTransdutor;

	private TemperaturaMaximaTransdutor temperaturaMaximaTransdutor;

	private ModeloCorretor modelo;

	private SituacaoMedidor situacaoMedidor;

	private String tombamento;

	private MedidorLocalArmazenagem localArmazenagem;

	private Collection<VazaoCorretorHistoricoMovimentacao> movimentacoes = new HashSet<VazaoCorretorHistoricoMovimentacao>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.VazaoCorretor
	 * #getModelo()
	 */
	@Override
	public ModeloCorretor getModelo() {

		return modelo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.VazaoCorretor
	 * #
	 * setModelo(br.com.ggas.medicao.vazaocorretor
	 * .impl.ModeloCorretor)
	 */
	@Override
	public void setModelo(ModeloCorretor modelo) {

		this.modelo = modelo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor#getNumeroSerie()
	 */
	@Override
	public String getNumeroSerie() {

		return numeroSerie;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor
	 * #setNumeroSerie(java.lang.String)
	 */
	@Override
	public void setNumeroSerie(String numeroSerie) {

		this.numeroSerie = numeroSerie;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor#getMarcaCorretor()
	 */
	@Override
	public MarcaCorretor getMarcaCorretor() {

		return marcaCorretor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor
	 * #setMarcaCorretor(br.com.ggas.medicao
	 * .corretorvazao.MarcaCorretor)
	 */
	@Override
	public void setMarcaCorretor(MarcaCorretor marcaCorretor) {

		this.marcaCorretor = marcaCorretor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor#getNumeroAnoFabricacao()
	 */
	@Override
	public Integer getNumeroAnoFabricacao() {

		return numeroAnoFabricacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor
	 * #setNumeroAnoFabricacao(java.lang.Integer)
	 */
	@Override
	public void setNumeroAnoFabricacao(Integer numeroAnoFabricacao) {

		this.numeroAnoFabricacao = numeroAnoFabricacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor#getTipoMostrador()
	 */
	@Override
	public TipoMostrador getTipoMostrador() {

		return tipoMostrador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor
	 * #setTipoMostrador(java.lang.Integer)
	 */
	@Override
	public void setTipoMostrador(TipoMostrador tipoMostrador) {

		this.tipoMostrador = tipoMostrador;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.VazaoCorretor
	 * #getCorrecaoPressao()
	 */
	@Override
	public Boolean getCorrecaoPressao() {

		return correcaoPressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.VazaoCorretor
	 * #setCorrecaoPressao(java.lang.Boolean)
	 */
	@Override
	public void setCorrecaoPressao(Boolean correcaoPressao) {

		this.correcaoPressao = correcaoPressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.VazaoCorretor
	 * #getCorrecaoTemperatura()
	 */
	@Override
	public Boolean getCorrecaoTemperatura() {

		return correcaoTemperatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.VazaoCorretor
	 * #setCorrecaoTemperatura(java.lang.Boolean)
	 */
	@Override
	public void setCorrecaoTemperatura(Boolean correcaoTemperatura) {

		this.correcaoTemperatura = correcaoTemperatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.VazaoCorretor
	 * #getControleVazao()
	 */
	@Override
	public Boolean getControleVazao() {

		return controleVazao;
	}

	/**
	 * @param controleVazao
	 *            the controleVazao to set
	 */
	@Override
	public void setControleVazao(Boolean controleVazao) {

		this.controleVazao = controleVazao;
	}

	/**
	 * @return the linearizacaoFatorK
	 */
	@Override
	public Boolean getLinearizacaoFatorK() {

		return linearizacaoFatorK;
	}

	/**
	 * @param linearizacaoFatorK
	 *            the linearizacaoFatorK to set
	 */
	@Override
	public void setLinearizacaoFatorK(Boolean linearizacaoFatorK) {

		this.linearizacaoFatorK = linearizacaoFatorK;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor#getTipoTransdutorPressao()
	 */
	@Override
	public TipoTransdutorPressao getTipoTransdutorPressao() {

		return tipoTransdutorPressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor
	 * #setTipoTransdutorPressao(java.
	 * lang.Integer)
	 */
	@Override
	public void setTipoTransdutorPressao(TipoTransdutorPressao tipoTransdutorPressao) {

		this.tipoTransdutorPressao = tipoTransdutorPressao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor
	 * #getTipoTransdutorTemperatura()
	 */
	@Override
	public TipoTransdutorTemperatura getTipoTransdutorTemperatura() {

		return tipoTransdutorTemperatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.impl.
	 * VazaoCorretor
	 * #setTipoTransdutorTemperatura(java
	 * .lang.Integer)
	 */
	@Override
	public void setTipoTransdutorTemperatura(TipoTransdutorTemperatura tipoTransdutorTemperatura) {

		this.tipoTransdutorTemperatura = tipoTransdutorTemperatura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.VazaoCorretor
	 * #getProtocoloComunicacao()
	 */
	@Override
	public ProtocoloComunicacao getProtocoloComunicacao() {

		return protocoloComunicacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.vazaocorretor.VazaoCorretor
	 * #
	 * setProtocoloComunicacao(br.com.ggas.medicao
	 * .vazaocorretor.ProtocoloComunicacao)
	 */
	@Override
	public void setProtocoloComunicacao(ProtocoloComunicacao protocoloComunicacao) {

		this.protocoloComunicacao = protocoloComunicacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(numeroSerie)) {
			stringBuilder.append(NUMERO_SERIE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(marcaCorretor == null) {
			stringBuilder.append(MARCA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(situacaoMedidor == null) {
			stringBuilder.append(NUMERO_SITUACAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
		}
		return erros;
	}

	/**
	 * @return the pressaoMaximaTransdutor
	 */
	@Override
	public PressaoMaximaTransdutor getPressaoMaximaTransdutor() {

		return pressaoMaximaTransdutor;
	}

	/**
	 * @param pressaoMaximaTransdutor
	 *            the pressaoMaximaTransdutor to
	 *            set
	 */
	@Override
	public void setPressaoMaximaTransdutor(PressaoMaximaTransdutor pressaoMaximaTransdutor) {

		this.pressaoMaximaTransdutor = pressaoMaximaTransdutor;
	}

	/**
	 * @return the temperaturaMaximaTransdutor
	 */
	@Override
	public TemperaturaMaximaTransdutor getTemperaturaMaximaTransdutor() {

		return temperaturaMaximaTransdutor;
	}

	/**
	 * @param temperaturaMaximaTransdutor
	 *            the temperaturaMaximaTransdutor
	 *            to set
	 */
	@Override
	public void setTemperaturaMaximaTransdutor(TemperaturaMaximaTransdutor temperaturaMaximaTransdutor) {

		this.temperaturaMaximaTransdutor = temperaturaMaximaTransdutor;
	}

	@Override
	public SituacaoMedidor getSituacaoMedidor() {

		return situacaoMedidor;
	}

	@Override
	public void setSituacaoMedidor(SituacaoMedidor situacaoMedidor) {

		this.situacaoMedidor = situacaoMedidor;
	}

	@Override
	public Integer getNumeroDigitos() {

		return numeroDigitos;
	}

	@Override
	public void setNumeroDigitos(Integer numeroDigitos) {

		this.numeroDigitos = numeroDigitos;
	}

	@Override
	public String getTombamento() {

		return tombamento;
	}

	@Override
	public void setTombamento(String tombamento) {

		this.tombamento = tombamento;
	}

	@Override
	public MedidorLocalArmazenagem getLocalArmazenagem() {

		return localArmazenagem;
	}

	@Override
	public void setLocalArmazenagem(MedidorLocalArmazenagem localArmazenagem) {

		this.localArmazenagem = localArmazenagem;
	}

	@Override
	public Collection<VazaoCorretorHistoricoMovimentacao> getMovimentacoes() {

		return movimentacoes;
	}

	@Override
	public void setMovimentacoes(Collection<VazaoCorretorHistoricoMovimentacao> movimentacoes) {

		this.movimentacoes = movimentacoes;
	}

}
