/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.vazaocorretor;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

public interface ControladorFaixaPressaoFornecimento extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_FAIXA_PRESSAO_FORNECIMENTO = "controladorFaixaPressaoFornecimento";

	/**
	 * Obter faixa pressao fornecimento por medida.
	 * 
	 * @param medida
	 *            the medida
	 * @param unidade
	 *            the unidade
	 * @param segmento
	 *            the segmento
	 * @return the faixa pressao fornecimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	FaixaPressaoFornecimento obterFaixaPressaoFornecimentoPorMedida(BigDecimal medida, Unidade unidade, Segmento segmento)
					throws NegocioException;

	/**
	 * Método responsável por listar as faixas de
	 * pressão de fornecimento filtradas por
	 * segmento.
	 * 
	 * @param segmento
	 *            Um segmento.
	 * @return Coleção de faixas de pressão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<FaixaPressaoFornecimento> listarFaixaPressaoFornecimentoPorSegmento(Segmento segmento) throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * faixas de pressão de fornecimento do
	 * sistema.
	 * 
	 * @return Coleção de faixas de pressão.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<FaixaPressaoFornecimento> listarTodasFaixasPressaoFornecimento() throws NegocioException;

	/**
	 * Método responsável por obter a faixa de
	 * pressão de fornecimento a partir de um
	 * segmento, uma medida mínima e uma
	 * unidade de pressão.
	 * 
	 * @param segmento
	 *            O segmento.
	 * @param medidaMinimo
	 *            A medida mínima.
	 * @param unidadePressao
	 *            Unidade da pressão.
	 * @return A faixa de pressão de fornecimento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	FaixaPressaoFornecimento obterFaixaPressaoFornecimento(Segmento segmento, BigDecimal medidaMinimo, Unidade unidadePressao)
					throws NegocioException;

	/**
	 * Consultar faixa pressao fornecimento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<FaixaPressaoFornecimento> consultarFaixaPressaoFornecimento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Obter faixa pressao fornecimento.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the faixa pressao fornecimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	FaixaPressaoFornecimento obterFaixaPressaoFornecimento(long chavePrimaria) throws NegocioException;

	/**
	 * Inserir faixa pressao fornecimento.
	 * 
	 * @param faixaPressaoFornecimento
	 *            the faixa pressao fornecimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirFaixaPressaoFornecimento(FaixaPressaoFornecimento faixaPressaoFornecimento) throws NegocioException;

	/**
	 * Validar faixas pressao fornecimento.
	 * 
	 * @param listaFaixaPressaoFornecimento
	 *            the lista faixa pressao fornecimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarFaixasPressaoFornecimento(List<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento) throws NegocioException;

	/**
	 * Remover faixas pressao fornecimento.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerFaixasPressaoFornecimento(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Atualizar faixas pressao fornecimento.
	 * 
	 * @param faixaPressaoFornecimento
	 *            the faixa pressao fornecimento
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarFaixasPressaoFornecimento(FaixaPressaoFornecimento faixaPressaoFornecimento) throws NegocioException,
					ConcorrenciaException;

	/**
	 * Verificar faixa pressao existente.
	 * 
	 * @param faixaPressaoFornecimento
	 *            the faixa pressao fornecimento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void verificarFaixaPressaoExistente(FaixaPressaoFornecimento faixaPressaoFornecimento) throws NegocioException;

	/**
	 * Listar faixas pressao faturamento relacionadas.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<FaixaPressaoFornecimento> listarFaixasPressaoFaturamentoRelacionadas(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Listar faixa pressao fornecimento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the list
	 * @throws NegocioException
	 *             the negocio exception
	 */
	List<FaixaPressaoFornecimentoVO> listarFaixaPressaoFornecimento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Listar faixa pressao fornecimento sem segmento associado.
	 * 
	 * @return the collection
	 */
	Collection<FaixaPressaoFornecimento> listarFaixaPressaoFornecimentoSemSegmentoAssociado();
	/**
	 * Obtém faixas de pressão por chaves primárias
	 * de Segmento.
	 * @param chavesPrimariasSegmentos - {@link Long} Array
	 * @return mapa de faixas de pressão por chave de Segmento
	 */
	Map<Long, Collection<FaixaPressaoFornecimento>> obterFaixaPressaoPressaoFornecimentoPorSegmentos(Long[] chavesPrimariasSegmentos);

	/**
	 * Escolhe uma faixa de pressão, da lista de faixas de pressão,
	 * que possua a unidade de pressão igual a {@code unidade}, passada por 
	 * parâmetro, e que tenha a unidade minima menor ou igual e unidade máxima
	 * maior ou igual que a {@code medida} do parâmetro.
	 * @param listaFaixaPressaoFornecimento - {@link Collection}
	 * @param medida - {@link BigDecimal}
	 * @param unidade - {@link Unidade}
	 * @return faixa de pressão de fornecimento
	 */
	FaixaPressaoFornecimento escolherFaixaPressaoFornecimentoPorUnidadeEmedidaMaximaMinima(
					Collection<FaixaPressaoFornecimento> listaFaixaPressaoFornecimento, BigDecimal medida, Unidade unidade);

	
	/**
	 * Obtém todas as faixas de pressão com os respectivos Segmeto e Unidade de Pressão
	 * @param segmento - {@link Segmento}
	 * @param unidadePressao - {@link Unidade}
	 * @return colecaoFaixaPressaoFornecimento - {@link Collection}
	 * @throws NegocioException - {@link NegocioException}
	 */
	Collection<FaixaPressaoFornecimento> obterFaixasPressaoFornecimento(Segmento segmento, Unidade unidadePressao) throws NegocioException;

}
