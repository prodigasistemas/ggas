	/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.vazaocorretor.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.consumo.UnidadeConversao;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Constantes.TipoUnidade;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorUnidadeImpl extends ControladorNegocioImpl implements ControladorUnidade {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Unidade.BEAN_ID_UNIDADE);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Unidade.BEAN_ID_UNIDADE);
	}

	public Class<?> getClasseEntidadeClasseUnidade() {

		return ServiceLocator.getInstancia().getClassPorID(ClasseUnidade.BEAN_ID_CLASSE_UNIDADE);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * ControladorUnidade#listarUnidadesExtensao()
	 */
	@Override
	public Collection<Unidade> listarUnidadesExtensao() throws NegocioException {

		// FIXME: Parâmetro substituido por constante
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valorCodigoClasseUnidadeExtensao = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_UNIDADE_COMPRIMENTO);

		return listarUnidadesPorChaveClasseUnidade(Long.parseLong(valorCodigoClasseUnidadeExtensao));
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * ControladorUnidade#listarUnidadesPressao()
	 */
	@Override
	public Collection<Unidade> listarUnidadesPressao() throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valorCodigoClasseUnidadePressao = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_UNIDADE_PRESSAO);

		return listarUnidadesPorChaveClasseUnidade(Long.parseLong(valorCodigoClasseUnidadePressao));
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * ControladorUnidade
	 * #listarUnidadesTemperatura()
	 */
	@Override
	public Collection<Unidade> listarUnidadesTemperatura() throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valorCodigoClasseUnidadeTemperatura = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_UNIDADE_TEMPERATURA);

		return listarUnidadesPorChaveClasseUnidade(Long.parseLong(valorCodigoClasseUnidadeTemperatura));
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * ControladorUnidade#listarUnidadesVazao()
	 */
	@Override
	public Collection<Unidade> listarUnidadesVazao() throws NegocioException {

		// FIXME: Parâmetro substituido por constante
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valorCodigoClasseUnidadeVazao = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_UNIDADE_VAZAO);

		return listarUnidadesPorChaveClasseUnidade(Long.parseLong(valorCodigoClasseUnidadeVazao));
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * ControladorUnidade#listarUnidadesVolume()
	 */
	@Override
	public Collection<Unidade> listarUnidadesVolume() throws NegocioException {

		// FIXME: Parâmetro substituido por constante
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valorCodigoClasseUnidadeVolume = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_UNIDADE_VOLUME);

		return listarUnidadesPorChaveClasseUnidade(Long.parseLong(valorCodigoClasseUnidadeVolume));
	}

	/**
	 * Listar unidades por chave classe unidade.
	 * 
	 * @param chavePrimariaClasseUnidade
	 *            the chave primaria classe unidade
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private Collection<Unidade> listarUnidadesPorChaveClasseUnidade(long chavePrimariaClasseUnidade) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where classeUnidade.chavePrimaria = :idClasseUnidade ");
		hql.append(" and habilitado = true ");
		hql.append(" order by descricao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idClasseUnidade", chavePrimariaClasseUnidade);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorUnidade#consultarUnidadeConversao(java.lang.Long, java.lang.Long)
	 */
	@Override
	public UnidadeConversao consultarUnidadeConversao(Long unidadeDestino, Long unidadeOrigem) {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(" UnidadeConversaoImpl ");
		hql.append(" where unidadeOrigem.chavePrimaria = :idUnidadeOrigem ");
		hql.append(" and unidadeDestino.chavePrimaria = :idUnidadeDestino ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idUnidadeOrigem", unidadeOrigem);
		query.setLong("idUnidadeDestino", unidadeDestino);

		return (UnidadeConversao) query.uniqueResult();
	}
	

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * ControladorUnidade
	 * #obterUnidadePadraoPorClasse
	 * (br.com.ggas.medicao
	 * .vazaocorretor.ClasseUnidade)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Unidade obterUnidadePadraoPorClasse(ClasseUnidade classeUnidade) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" where classeUnidade.chavePrimaria = :idClasseUnidade ");
		hql.append(" and indicadorPadrao = :indicadorPadrao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idClasseUnidade", classeUnidade.getChavePrimaria());
		query.setBoolean("indicadorPadrao", Boolean.TRUE);

		List<Unidade> unidades = query.list();

		if(unidades == null || unidades.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_UNIDADE_PADRAO_INEXISTENTE, classeUnidade.getDescricao());
		} else {
			if(unidades.size() > 1) {
				throw new NegocioException(ERRO_NEGOCIO_UNIDADE_PADRAO_NAO_UNICA, classeUnidade.getDescricao());
			}
		}

		return Util.primeiroElemento(unidades);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * ControladorUnidade
	 * #obterUnidadePadraoPressao()
	 */
	@Override
	public Unidade obterUnidadePadraoPressao() throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valorCodigoClasseUnidadePressao = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_UNIDADE_PRESSAO);

		ClasseUnidade classeUnidade = (ClasseUnidade) obter(Long.parseLong(valorCodigoClasseUnidadePressao), 
						getClasseEntidadeClasseUnidade());

		return this.obterUnidadePadraoPorClasse(classeUnidade);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * ControladorUnidade
	 * #obterUnidadePadraoTemperatura()
	 */
	@Override
	public Unidade obterUnidadePadraoTemperatura() throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valorCodigoClasseUnidadeTemperatura = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_UNIDADE_TEMPERATURA);

		ClasseUnidade classeUnidade = (ClasseUnidade) obter(Long.parseLong(valorCodigoClasseUnidadeTemperatura),
						getClasseEntidadeClasseUnidade());

		return this.obterUnidadePadraoPorClasse(classeUnidade);
	}
	
	/**
	 * Recupera do banco a unidade de medida em conformidade com o filtro.
	 *
	 * @param filtro Um objeto Unidade usado como modelo na consulta
	 * @return Um objeto Unidade correspondente com o modelo, ou null
	 * @throws org.hibernate.NonUniqueResultException se o filtro for genérico demais
	 */
	@Override
	public Unidade obterUnidade(Unidade filtro) {
		Example exemplo = Example.create(filtro)
				.enableLike(MatchMode.EXACT)
				.ignoreCase()
				.excludeProperty("indicadorPadrao")
				.excludeProperty(EntidadeNegocio.ATRIBUTO_HABILITADO)
				.excludeProperty("versao");
		
		Criteria criteria = this.createCriteria(getClasseEntidade());
		criteria.add(exemplo);
		return (Unidade) criteria.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * ControladorUnidade
	 * #listarUnidadesQuantidade()
	 */
	@Override
	public Collection<Unidade> listarUnidadesQuantidade() throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String valorCodigoClasseUnidadeQuantidade = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_UNIDADE_QUANTIDADE);

		return listarUnidadesPorChaveClasseUnidade(Long.parseLong(valorCodigoClasseUnidadeQuantidade));
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.
	 * ControladorUnidade#listarClasseUnidade()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ClasseUnidade> listarClasseUnidade() throws NegocioException {

		Criteria criteria = this.createCriteria(ClasseUnidade.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorUnidade#listarUnidade(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ClasseUnidade> listarUnidade(Long chavePrimaria) throws NegocioException {

		Criteria criteria = this.createCriteria(Unidade.class);
		criteria.add(Restrictions.eq("classeUnidade.chavePrimaria", chavePrimaria));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorUnidade#listarUnidadeHabilitada(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<ClasseUnidade> listarUnidadeHabilitada(Long chavePrimaria) throws NegocioException {

		Criteria criteria = this.createCriteria(Unidade.class);
		criteria.add(Restrictions.eq("classeUnidade.chavePrimaria", chavePrimaria));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.vazaocorretor.ControladorUnidade#listaUnidadesOrdenadas()
	 */
	@Override
	public Collection<Unidade> listaUnidadesOrdenadas() {

		Criteria criteria = this.createCriteria(Unidade.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc(TabelaAuxiliar.ATRIBUTO_DESCRICAO));

		return criteria.list();
	}

	/**
	 * Obtém a unidade de Conversão utilizada para conversão de Celsius 
	 * para a unidade padrão de temperatura
	 * @return UnidadeConversao - Unidade de Conversão de Celsius para Padrão de Temperatura
	 * @throws NegocioException
	 */
	@Override
	public UnidadeConversao obterUnidadeConversaoDeCelsiusParaPadrao() throws NegocioException  {
		Unidade unidadePadraoTemperatura = this.obterUnidadePadraoTemperatura();
		Unidade unidadeCelsius = (Unidade) criar();
		unidadeCelsius.setChavePrimaria(Long.parseLong(TipoUnidade.CELSIUS.getValor()));
		return this.consultarUnidadeConversao(unidadePadraoTemperatura.getChavePrimaria(), unidadeCelsius.getChavePrimaria());
	}
	
	/**
	 * Obtém as unidades padrão para conversão de temperatura e pressão.
	 * @return mapa de Unidade por constantes de ClasseUnidade
	 */
	@Override
	public Map<String, Unidade> obterUnidadesPadraoTemperaturaPressao() throws NegocioException {
		Map<String, Unidade> unidadesPadraoParaConversao = new HashMap<>();
		Unidade unidadePadraoTemperatura = this.obterUnidadePadraoTemperatura();
		unidadesPadraoParaConversao.put(ClasseUnidade.TEMPERATURA, unidadePadraoTemperatura);
		Unidade unidadePadraoPressao = this.obterUnidadePadraoPressao();
		unidadesPadraoParaConversao.put(ClasseUnidade.PRESSAO, unidadePadraoPressao);
		return unidadesPadraoParaConversao;
	}
	
	/**
	 * Obtém a unidade padrão para conversão de temperatura, do 
	 * mapa de Unidade por constantes de ClasseUnidade. Caso 
	 * não exista a unidade no mapa, realiza a consulta.
	 * @param unidadesPadraoParaConversao
	 * @return Unidade - unidade de temperatura
	 * @throws NegocioException
	 */
	@Override
	public Unidade obterUnidadePadraoTemperaturaParaConversao(Map<String, Unidade> unidadesPadraoParaConversao) throws NegocioException {

		Unidade unidadePadraoTemperatura;
		if (unidadesPadraoParaConversao != null && unidadesPadraoParaConversao.containsKey(ClasseUnidade.TEMPERATURA)) {
			unidadePadraoTemperatura = unidadesPadraoParaConversao.get(ClasseUnidade.TEMPERATURA);
		} else {
			unidadePadraoTemperatura = this.obterUnidadePadraoTemperatura();
		}
		return unidadePadraoTemperatura;
	}
	
	/**
	 * Obtém a unidade padrão para conversão de pressão, do 
	 * mapa de Unidade por constantes de ClasseUnidade. Caso 
	 * não exista a unidade no mapa, realiza a consulta.
	 * @param unidadesPadraoParaConversao
	 * @return Unidade - unidade de pressão
	 * @throws NegocioException
	 */
	@Override
	public Unidade obterUnidadePadraoPressaoParaConversao(Map<String, Unidade> unidadesPadraoParaConversao) throws NegocioException {

		Unidade unidadePadraoPressao;
		if (unidadesPadraoParaConversao != null && unidadesPadraoParaConversao.containsKey(ClasseUnidade.PRESSAO)) {
			unidadePadraoPressao = unidadesPadraoParaConversao.get(ClasseUnidade.PRESSAO);
		} else {
			unidadePadraoPressao = this.obterUnidadePadraoPressao();
		}
		return unidadePadraoPressao;
	}
	

}
