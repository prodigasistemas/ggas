/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe AnormalidadeConsumoImpl representa uma AnormalidadeConsumoImpl no sistema.
 *
 * @since 30/09/2009
 * 
 */

package br.com.ggas.medicao.anormalidade.impl;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.util.Constantes;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe responsável por representar uma anormalidade no consumo.
 *
 */
public class AnormalidadeConsumoImpl extends EntidadeNegocioImpl implements AnormalidadeConsumo {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = 3611358607009826442L;

	private String descricao;

	private String descricaoAbreviada;

	private Boolean bloquearFaturamento;

	private Long numeroOcorrenciaConsecutivasAnormalidade;

	private String mensagemConsumo;

	private String regra;

	private boolean ignorarAcaoAutomatica;

	/**
	 * @return the numeroOcorrenciaConsecutivasAnormalidade
	 */
	@Override
	public Long getNumeroOcorrenciaConsecutivasAnormalidade() {

		return numeroOcorrenciaConsecutivasAnormalidade;
	}

	/**
	 * @param numeroOcorrenciaConsecutivasAnormalidade
	 *            the numeroOcorrenciaConsecutivasAnormalidade to set
	 */
	@Override
	public void setNumeroOcorrenciaConsecutivasAnormalidade(Long numeroOcorrenciaConsecutivasAnormalidade) {

		this.numeroOcorrenciaConsecutivasAnormalidade = numeroOcorrenciaConsecutivasAnormalidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeConsumo#getMensagemConsumo()
	 */
	@Override
	public String getMensagemConsumo() {
		return mensagemConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeConsumo
	 * #setMensagemConsumo(java.lang.String)
	 */
	@Override
	public void setMensagemConsumo(String mensagemConsumo) {
		this.mensagemConsumo = mensagemConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * AnormalidadeConsumo#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * AnormalidadeConsumo
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * AnormalidadeConsumo#getDescricaoAbreviada()
	 */
	@Override
	public String getDescricaoAbreviada() {

		return descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.consumo.impl.
	 * AnormalidadeConsumo
	 * #setDescricaoAbreviada(java.lang.String)
	 */
	@Override
	public void setDescricaoAbreviada(String descricaoAbreviada) {

		this.descricaoAbreviada = descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #getBloquearFaturamento()
	 */
	@Override
	public Boolean getBloquearFaturamento() {

		return bloquearFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setBloquearFaturamento(java.lang.Boolean)
	 */
	@Override
	public void setBloquearFaturamento(Boolean bloquearFaturamento) {

		this.bloquearFaturamento = bloquearFaturamento;
	}

	@Override
	public String getRegra() {
		return regra;
	}

	@Override
	public void setRegra(String regra) {
		this.regra = regra;
	}

	@Override
	public boolean isIgnorarAcaoAutomatica() {
		return ignorarAcaoAutomatica;
	}

	@Override
	public void setIgnorarAcaoAutomatica(boolean ignorarAcaoAutomatica) {
		this.ignorarAcaoAutomatica = ignorarAcaoAutomatica;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(bloquearFaturamento == null) {
			stringBuilder.append(BLOQUEAR_FATURAMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}
		return erros;
	}

}
