package br.com.ggas.medicao.anormalidade;

import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assintura dos métodos relacionados a AnormalidadeSegmentoParametro
 *
 */
public interface AnormalidadeSegmentoParametro extends EntidadeNegocio {

	/**
	 * @param anormalidadeConsumo
	 */
	public void setAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo);

	/**
	 * @return anormalidadeConsumo
	 */
	public AnormalidadeConsumo getAnormalidadeConsumo();

	/**
	 * @param segmento
	 */
	public void setSegmento(Segmento segmento);

	/**
	 * @return segmento
	 */
	public Segmento getSegmento();

	/**
	 * @param modalidadeMedicaoImovel
	 */
	public void setModalidadeMedicao(ModalidadeMedicaoImovel modalidadeMedicaoImovel);

	/**
	 * @return modalidadeMedicaoImovel
	 */
	public ModalidadeMedicaoImovel getModalidadeMedicao();

	/**
	 * @return quantidadeDeRecorrencia
	 */
	public Integer getQuantidadeDeRecorrencia();

	/**
	 * @param quantidadeDeRecorrencia
	 */
	public void setQuantidadeDeRecorrencia(Integer quantidadeDeRecorrencia);

	/**
	 * @return bloqueiaFaturamento
	 */
	public Boolean getBloqueiaFaturamento();

	/**
	 * @param bloqueiaFaturamento
	 */
	public void setBloqueiaFaturamento(Boolean bloqueiaFaturamento);
	
	/**
	 * @param ramoAtividade
	 */
	public void setRamoAtividade(RamoAtividade ramoAtividade);
	
	/**
	 * @return
	 */
	public RamoAtividade getRamoAtividade();
}
