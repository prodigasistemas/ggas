/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.anormalidade.negocio;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.dominio.TipoAcaoAnormalidadeLeitura;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.util.ServiceLocator;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Responsável por executar uma ação de anormalidade de leitura
 * @author jose.victor@logiquesistemas.com.br
 */
@Service
public class ProcessadorAcaoAnormalidadeLeitura {

	/**
	 * Executa a ação da anormalidade de leitura no historio informado.
	 * Atualiza o valor da nova leitura informada no histórico atual do {@link HistoricoConsumo} informado
	 * @param historico historico de consumo
	 * @param acao ação a ser executada
	 *
	 * @throws NegocioException lançada caso ocorra alguma falha de execução de acesso ao banco
	 */
	public void aplicarAcaoLeituraNoHistorico(HistoricoConsumo historico, AcaoAnormalidadeLeitura acao) throws NegocioException {

		final TipoAcaoAnormalidadeLeitura tipoAcao = TipoAcaoAnormalidadeLeitura.fromCodigoAcao((int) acao.getChavePrimaria());
		final ExecutorAcaoAnormalidade bean = ServiceLocator.getInstancia().getBean(tipoAcao.getImplementacaoAcao());

		final BigDecimal novoValorParaLeitura = bean.executarAcaoLeituraInformada(historico);
		historico.getHistoricoAtual().setNumeroLeituraInformada(novoValorParaLeitura);
	}

}
