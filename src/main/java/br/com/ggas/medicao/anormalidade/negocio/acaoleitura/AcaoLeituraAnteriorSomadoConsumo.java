/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.anormalidade.negocio.acaoleitura;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.anormalidade.negocio.ExecutorAcaoAnormalidade;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Implementação da execução da Anormalidade de Leitura para o tipo
 * {@link br.com.ggas.medicao.anormalidade.dominio.TipoAcaoAnormalidadeLeitura#ANTERIOR_SOMADO_AO_CONSUMO}
 *
 * Obtém o valor da leitura anterior do historico somada ao consumo atual
 *
 * @author jose.victor@logiquesistemas.com.br
 */
@Service
@SuppressWarnings("common-java:InsufficientCommentDensity")
public class AcaoLeituraAnteriorSomadoConsumo implements ExecutorAcaoAnormalidade {

	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BigDecimal executarAcaoLeituraInformada(HistoricoConsumo historico) throws NegocioException {
		int formaArredondamento = Integer.parseInt((String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_TIPO_ARREDONDAMENTO_CONSUMO));
		MathContext contextoArredondamento = new MathContext(0, RoundingMode.valueOf(formaArredondamento));

		BigDecimal numeroLeituraAnterior = BigDecimal.ZERO;
		BigDecimal numeroConsumoAtual = BigDecimal.ZERO;

		if (historico.getHistoricoAtual().getNumeroLeituraAnterior() != null) {
			numeroLeituraAnterior = historico.getHistoricoAtual().getNumeroLeituraAnterior();
		}

		if (historico.getConsumo() != null) {
			numeroConsumoAtual = historico.getConsumo();
		}

		return numeroLeituraAnterior.add(numeroConsumoAtual, contextoArredondamento);
	}

}
