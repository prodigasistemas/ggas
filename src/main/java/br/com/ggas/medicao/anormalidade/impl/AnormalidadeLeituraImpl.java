/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.anormalidade.impl;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.util.Constantes;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Classe responsável por representar uma anormalidade de leitura.
 *
 */
public class AnormalidadeLeituraImpl extends EntidadeNegocioImpl implements AnormalidadeLeitura {

	private static final int LIMITE_CAMPO_OBRIGATORIO = 2;

	private static final long serialVersionUID = 4837425204512967110L;

	private String descricao;

	private String descricaoAbreviada;

	private String mensagemLeitura;

	private String mensagemManutencao;

	private String mensagemPrevencao;

	private String regra;

	private Boolean aceitaLeitura;

	private Boolean relativoMedidor;

	private Boolean bloquearFaturamento;

	private Boolean exigeFoto;

	private AcaoAnormalidadeConsumo acaoAnormalidadeSemConsumo;

	private AcaoAnormalidadeConsumo acaoAnormalidadeComConsumo;

	private AcaoAnormalidadeLeitura acaoAnormalidadeSemLeitura;

	private AcaoAnormalidadeLeitura acaoAnormalidadeComLeitura;

	/**
	 * @return the relativoMedidor
	 */
	@Override
	public Boolean getRelativoMedidor() {

		return relativoMedidor;
	}

	/**
	 * @param relativoMedidor
	 *            the relativoMedidor to set
	 */
	@Override
	public void setRelativoMedidor(Boolean relativoMedidor) {

		this.relativoMedidor = relativoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura#getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura#getDescricaoAbreviada()
	 */
	@Override
	public String getDescricaoAbreviada() {

		return descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setDescricaoAbreviada(java.lang.String)
	 */
	@Override
	public void setDescricaoAbreviada(String descricaoAbreviada) {

		this.descricaoAbreviada = descricaoAbreviada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura#getMensagemLeitura()
	 */
	@Override
	public String getMensagemLeitura() {

		return mensagemLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setMensagemLeitura(java.lang.String)
	 */
	@Override
	public void setMensagemLeitura(String mensagemLeitura) {

		this.mensagemLeitura = mensagemLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura#getMensagemManutencao()
	 */
	@Override
	public String getMensagemManutencao() {

		return mensagemManutencao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setMensagemManutencao(java.lang.String)
	 */
	@Override
	public void setMensagemManutencao(String mensagemManutencao) {

		this.mensagemManutencao = mensagemManutencao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura#getMensagemPrevencao()
	 */
	@Override
	public String getMensagemPrevencao() {

		return mensagemPrevencao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setMensagemPrevencao(java.lang.String)
	 */
	@Override
	public void setMensagemPrevencao(String mensagemPrevencao) {

		this.mensagemPrevencao = mensagemPrevencao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura#getAceitaLeitura()
	 */
	@Override
	public Boolean getAceitaLeitura() {

		return aceitaLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setAceitaLeitura(java.lang.Boolean)
	 */
	@Override
	public void setAceitaLeitura(Boolean aceitaLeitura) {

		this.aceitaLeitura = aceitaLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #getAcaoAnormalidadeSemConsumo()
	 */
	@Override
	public AcaoAnormalidadeConsumo getAcaoAnormalidadeSemConsumo() {

		return acaoAnormalidadeSemConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setAcaoAnormalidadeSemConsumo
	 * (br.com.ggas.medicao
	 * .anormalidade.AcaoAnormalidadeConsumo)
	 */
	@Override
	public void setAcaoAnormalidadeSemConsumo(AcaoAnormalidadeConsumo acaoAnormalidadeSemConsumo) {

		this.acaoAnormalidadeSemConsumo = acaoAnormalidadeSemConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #getAcaoAnormalidadeComConsumo()
	 */
	@Override
	public AcaoAnormalidadeConsumo getAcaoAnormalidadeComConsumo() {

		return acaoAnormalidadeComConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setAcaoAnormalidadeComConsumo
	 * (br.com.ggas.medicao
	 * .anormalidade.AcaoAnormalidadeConsumo)
	 */
	@Override
	public void setAcaoAnormalidadeComConsumo(AcaoAnormalidadeConsumo acaoAnormalidadeComConsumo) {

		this.acaoAnormalidadeComConsumo = acaoAnormalidadeComConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #getAcaoAnormalidadeSemLeitura()
	 */
	@Override
	public AcaoAnormalidadeLeitura getAcaoAnormalidadeSemLeitura() {

		return acaoAnormalidadeSemLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setAcaoAnormalidadeSemLeitura
	 * (br.com.ggas.medicao
	 * .anormalidade.AcaoAnormalidadeLeitura)
	 */
	@Override
	public void setAcaoAnormalidadeSemLeitura(AcaoAnormalidadeLeitura acaoAnormalidadeSemLeitura) {

		this.acaoAnormalidadeSemLeitura = acaoAnormalidadeSemLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #getAcaoAnormalidadeComLeitura()
	 */
	@Override
	public AcaoAnormalidadeLeitura getAcaoAnormalidadeComLeitura() {

		return acaoAnormalidadeComLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setAcaoAnormalidadeComLeitura
	 * (br.com.ggas.medicao
	 * .anormalidade.AcaoAnormalidadeLeitura)
	 */
	@Override
	public void setAcaoAnormalidadeComLeitura(AcaoAnormalidadeLeitura acaoAnormalidadeComLeitura) {

		this.acaoAnormalidadeComLeitura = acaoAnormalidadeComLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #getBloquearFaturamento()
	 */
	@Override
	public Boolean getBloquearFaturamento() {

		return bloquearFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * AnormalidadeLeitura
	 * #setBloquearFaturamento(java.lang.Boolean)
	 */
	@Override
	public void setBloquearFaturamento(Boolean bloquearFaturamento) {

		this.bloquearFaturamento = bloquearFaturamento;
	}

	@Override
	public Boolean getExigeFoto() {
		return exigeFoto;
	}

	@Override
	public void setExigeFoto(Boolean exigeFoto) {
		this.exigeFoto = exigeFoto;
	}

	@Override
	public void setRegra(String regra) {
		this.regra = regra;
	}

	@Override
	public String getRegra() {
		return this.regra;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.EntidadeNegocio
	 * #validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(descricao)) {
			stringBuilder.append(DESCRICAO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(relativoMedidor == null) {
			stringBuilder.append(RELATIVO_MEDIDOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(bloquearFaturamento == null) {
			stringBuilder.append(BLOQUEAR_FATURAMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(acaoAnormalidadeSemConsumo == null) {
			stringBuilder.append(ACAO_ANORMALIDADE_SEM_CONSUMO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(acaoAnormalidadeComConsumo == null) {
			stringBuilder.append(ACAO_ANORMALIDADE_COM_CONSUMO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(acaoAnormalidadeSemLeitura == null) {
			stringBuilder.append(ACAO_ANORMALIDADE_SEM_LEITURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(acaoAnormalidadeComLeitura == null) {
			stringBuilder.append(ACAO_ANORMALIDADE_COM_LEITURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS,
					camposObrigatorios.substring(0, stringBuilder.toString().length() - LIMITE_CAMPO_OBRIGATORIO));
		}
		return erros;
	}

}
