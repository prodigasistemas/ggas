/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.anormalidade;

import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados
 * a AnormalidadeLeitura
 *
 */
public interface AnormalidadeLeitura extends EntidadeNegocio {

	String BEAN_ID_ANORMALIDADE_LEITURA = "anormalidadeLeitura";

	String ANORMALIDADE_LEITURA = "ANORMALIDADE_LEITURA";

	String DESCRICAO = "ANORMALIDADE_CONSUMO_DESCRICAO";

	String ACAO_ANORMALIDADE_SEM_CONSUMO = "ACAO_ANORMALIDADE_SEM_CONSUMO";

	String ACAO_ANORMALIDADE_COM_CONSUMO = "ACAO_ANORMALIDADE_COM_CONSUMO";

	String ACAO_ANORMALIDADE_SEM_LEITURA = "ACAO_ANORMALIDADE_SEM_LEITURA";

	String ACAO_ANORMALIDADE_COM_LEITURA = "ACAO_ANORMALIDADE_COM_LEITURA";

	String RELATIVO_MEDIDOR = "RELATIVO_MEDIDOR";

	String GERA_ORDEM_SERVICO = "GERA_ORDEM_SERVICO";

	String BLOQUEAR_FATURAMENTO = "BLOQUEAR_FATURAMENTO";

	/**
	 * @return the relativoMedidor
	 */
	Boolean getRelativoMedidor();

	/**
	 * @param relativoMedidor
	 *            the relativoMedidor to set
	 */
	void setRelativoMedidor(Boolean relativoMedidor);

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the descricaoAbreviada
	 */
	String getDescricaoAbreviada();

	/**
	 * @param descricaoAbreviada
	 *            the descricaoAbreviada to set
	 */
	void setDescricaoAbreviada(String descricaoAbreviada);

	/**
	 * @return the mensagemLeitura
	 */
	String getMensagemLeitura();

	/**
	 * @param mensagemLeitura
	 *            the mensagemLeitura to set
	 */
	void setMensagemLeitura(String mensagemLeitura);

	/**
	 * @return the mensagemManutencao
	 */
	String getMensagemManutencao();

	/**
	 * @param mensagemManutencao
	 *            the mensagemManutencao to set
	 */
	void setMensagemManutencao(String mensagemManutencao);

	/**
	 * @return the mensagemPrevencao
	 */
	String getMensagemPrevencao();

	/**
	 * @param mensagemPrevencao
	 *            the mensagemPrevencao to set
	 */
	void setMensagemPrevencao(String mensagemPrevencao);

	/**
	 * @return the aceitaLeitura
	 */
	Boolean getAceitaLeitura();

	/**
	 * @param aceitaLeitura
	 *            the aceitaLeitura to set
	 */
	void setAceitaLeitura(Boolean aceitaLeitura);

	/**
	 * @return the acaoAnormalidadeSemConsumo
	 */
	AcaoAnormalidadeConsumo getAcaoAnormalidadeSemConsumo();

	/**
	 * @param acaoAnormalidadeSemConsumo
	 *            the acaoAnormalidadeSemConsumo
	 *            to set
	 */
	void setAcaoAnormalidadeSemConsumo(AcaoAnormalidadeConsumo acaoAnormalidadeSemConsumo);

	/**
	 * @return the acaoAnormalidadeComConsumo
	 */
	AcaoAnormalidadeConsumo getAcaoAnormalidadeComConsumo();

	/**
	 * @param acaoAnormalidadeComConsumo
	 *            the acaoAnormalidadeComConsumo
	 *            to set
	 */
	void setAcaoAnormalidadeComConsumo(AcaoAnormalidadeConsumo acaoAnormalidadeComConsumo);

	/**
	 * @return the acaoAnormalidadeSemLeitura
	 */
	AcaoAnormalidadeLeitura getAcaoAnormalidadeSemLeitura();

	/**
	 * @param acaoAnormalidadeSemLeitura
	 *            the acaoAnormalidadeSemLeitura
	 *            to set
	 */
	void setAcaoAnormalidadeSemLeitura(AcaoAnormalidadeLeitura acaoAnormalidadeSemLeitura);

	/**
	 * @return the acaoAnormalidadeComLeitura
	 */
	AcaoAnormalidadeLeitura getAcaoAnormalidadeComLeitura();

	/**
	 * @param acaoAnormalidadeComLeitura
	 *            the acaoAnormalidadeComLeitura
	 *            to set
	 */
	void setAcaoAnormalidadeComLeitura(AcaoAnormalidadeLeitura acaoAnormalidadeComLeitura);

	/**
	 * @return the bloquearFaturamento
	 */
	Boolean getBloquearFaturamento();

	/**
	 * @param bloquearFaturamento
	 *            the bloquearFaturamento to set
	 */
	void setBloquearFaturamento(Boolean bloquearFaturamento);

	/**
	 * @return true se exige foto da anormalidade ao realizar a leitura e false caso contrário
	 */
	Boolean getExigeFoto();

	/**
	 * Set para o indicador de exigencia de foto da anormalidade ao realizar a leitura e false caso contrário
	 * @param exigeFoto true se exige foto da anormalidade ao realizar a leitura e false caso contrário
	 */
	void setExigeFoto(Boolean exigeFoto);

	/**
	 * Configura a regra
	 * @param regra atributo regra
	 */
	void setRegra(String regra);

	/**
	 * Obtém a descrição da regra em que a anormalidade é aplicada
	 * @return valor da regra
	 */
	String getRegra();

}
