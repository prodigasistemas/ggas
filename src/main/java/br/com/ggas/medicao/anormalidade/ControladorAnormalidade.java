/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.anormalidade;

import java.util.Collection;
import java.util.Map;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados
 * ao ControladorAnormalidade
 * 
 */
public interface ControladorAnormalidade extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_ANORMALIDADE = "controladorAnormalidade";

	String ERRO_NEGOCIO_ANORMALIDADE_EXISTENTE = "ERRO_NEGOCIO_ANORMALIDADE_EXISTENTE";

	/**
	 * Método responsável por listas as
	 * anormalidades de leitura.
	 * 
	 * @return coleção de anormalidades de
	 *         leitura.
	 */
	Collection<AnormalidadeLeitura> listarAnormalidadesLeitura();

	/**
	 * lista anormalidades de leitura somente com chave e descrição
	 * @return retorna a lista de anormalidades de leitura
	 */
	Collection<AnormalidadeLeitura> listarAnormalidadesLeituraLazy();

	/**
	 * Método responsável por listas as
	 * anormalidades de consumo.
	 * 
	 * @return coleção de anormalidades de
	 *         consumo.
	 */
	Collection<AnormalidadeConsumo> listarAnormalidadesConsumo();

	/**
	 * Método responsável por retornar a
	 * anormalidade leitura pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da anormalidade
	 *            leitura.
	 * @return Uma anormalidade Leitura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	AnormalidadeLeitura obterAnormalidadeLeitura(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por retornar a
	 * anormalidade consumo pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da anormalidade
	 *            consumo.
	 * @return Uma anormalidade consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	AnormalidadeConsumo obterAnormalidadeConsumo(long chavePrimaria) throws NegocioException;

	/**
	 * Método que lista a acão anormalidade
	 * leitura.
	 * 
	 * @return listaAcaoAnormalidadeLeitura
	 */
	Collection<AcaoAnormalidadeLeitura> listarAcaoAnormalidadeLeitura();

	/**
	 * Método que lista a acão anormalidade
	 * leitura.
	 * 
	 * @return listaAcaoAnormalidadeLeitura
	 */
	Collection<AcaoAnormalidadeLeitura> listarAcaoAnormalidadeComLeitura();

	/**
	 * Método que lista a acão anormalidade
	 * leitura.
	 * 
	 * @return listaAcaoAnormalidadeLeitura
	 */
	Collection<AcaoAnormalidadeLeitura> listarAcaoAnormalidadeSemLeitura();

	/**
	 * Método que lista a acão anormalidade
	 * consumo.
	 * 
	 * @return listaAcaoAnormalidadeConsumo
	 */
	Collection<AcaoAnormalidadeConsumo> listarAcaoAnormalidadeConsumo();

	/**
	 * Método que lista a acão anormalidade
	 * consumo.
	 * 
	 * @return listaAcaoAnormalidadeConsumo
	 */
	Collection<AcaoAnormalidadeConsumo> listarAcaoAnormalidadeConsumoComLeitura();

	/**
	 * Método que lista a acão anormalidade
	 * consumo.
	 * 
	 * @return listaAcaoAnormalidadeConsumo
	 */
	Collection<AcaoAnormalidadeConsumo> listarAcaoAnormalidadeConsumoSemLeitura();

	/**
	 * Método responsável por criar uma
	 * anormalidade leitura.
	 * return AnormalidadeLeitura
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarAnormalidadeLeitura();

	/**
	 * Método responsável por criar uma
	 * anormalidade consumo.
	 * return AnormalidadeConsumo
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarAnormalidadeConsumo();

	/**
	 * Método responsável por retornar uma ação
	 * anormalidade consumo pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da anormalidade
	 *            consumo.
	 * @return Uma ação anormalidade consumo.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	AcaoAnormalidadeConsumo obterAcaoAnormalidadeConsumo(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por retornar uma ação
	 * anormalidade leitura pela chavePrimaria
	 * passada por parâmetro.
	 * 
	 * @param chavePrimaria
	 *            A chave primária da anormalidade
	 *            consumo.
	 * @return Uma ação anormalidade leitura.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	AcaoAnormalidadeLeitura obterAcaoAnormalidadeLeitura(long chavePrimaria) throws NegocioException;

	/**
	 * Consultar anormalidades consumo.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<AnormalidadeConsumo> consultarAnormalidadesConsumo(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Consultar anormalidades leitura.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<AnormalidadeLeitura> consultarAnormalidadesLeitura(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Remover anormalidades consumo.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerAnormalidadesConsumo(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Remover anormalidades leitura.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerAnormalidadesLeitura(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Atualizar anormalidade leitura.
	 * 
	 * @param anormalidadeLeitura
	 *            the anormalidade leitura
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarAnormalidadeLeitura(AnormalidadeLeitura anormalidadeLeitura) throws NegocioException, ConcorrenciaException;

	/**
	 * Atualizar anormalidade consumo.
	 * 
	 * @param anormalidadeConsumo
	 *            the anormalidade consumo
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void atualizarAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo) throws NegocioException, ConcorrenciaException;
	
	/**
	 * Metodo que carrega todas os {@code AnormalidadeSegmentoParametro} associados a uma {@code AnormalidadeConsumo}
	 * 
	 * @param anormalidadeConsumo anormalidade de consumo
	 * @return colecao de anormalidadeSegmentoParametros da anormalidade
	 * @throws NegocioException caso ocorra falha na operação
	 */
	Collection<AnormalidadeSegmentoParametro> listarAnormalidadeSegmentoParametros(AnormalidadeConsumo anormalidadeConsumo)
			throws NegocioException;
	
	/**
	 * Metodo que carrega os {@code AnormalidadeSegmentoParametro} associados a uma {@code AnormalidadeConsumo}
	 * 
	 * @param anormalidadeConsumo anormlidadeConsumo a quem pertence a parametrizacao
	 * @param segmento segmento do prontoConsumo
	 * @param codigoModalidadeMedicao tipo da modalidade de medicao do imovel do ponto consumo
	 * @param ramoAtividade ramo de atividade do ponto consumo
	 * @return Collection<AnormalidadeSegmentoParametro> {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	Collection<AnormalidadeSegmentoParametro> obterAnormalidadeSegmentoParametros(AnormalidadeConsumo anormalidadeConsumo,
			Segmento segmento, int codigoModalidadeMedicao, RamoAtividade ramoAtividade) throws NegocioException;

}
