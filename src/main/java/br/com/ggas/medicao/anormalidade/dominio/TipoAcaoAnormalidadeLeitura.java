/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.anormalidade.dominio;

import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.negocio.acaoleitura.AcaoLeituraAnterior;
import br.com.ggas.medicao.anormalidade.negocio.acaoleitura.AcaoLeituraAnteriorSomadoConsumo;
import br.com.ggas.medicao.anormalidade.negocio.acaoleitura.AcaoLeituraAnteriorSomadoMedia;
import br.com.ggas.medicao.anormalidade.negocio.acaoleitura.AcaoLeituraNormal;
import br.com.ggas.medicao.anormalidade.negocio.ExecutorAcaoAnormalidade;

import java.util.Arrays;

/**
 * Informa os tipos de ação de anormalidades de leitura implementados e suportados pelo GGAS
 * @author jose.victor@logiquesistemas.com.br
 */
public enum TipoAcaoAnormalidadeLeitura {

	NORMAL(AcaoAnormalidadeLeitura.NORMAL, AcaoLeituraNormal.class),
	INFORMADA(AcaoAnormalidadeLeitura.INFORMADA, AcaoLeituraNormal.class),
	ANTERIOR(AcaoAnormalidadeLeitura.ANTERIOR, AcaoLeituraAnterior.class),
	ANTERIOR_SOMADO_AO_CONSUMO(AcaoAnormalidadeLeitura.ANTERIOR_SOMADO_AO_CONSUMO, AcaoLeituraAnteriorSomadoConsumo.class),
	ANTERIOR_SOMADO_A_MEDIA(AcaoAnormalidadeLeitura.ANTERIOR_SOMADO_A_MEDIA, AcaoLeituraAnteriorSomadoMedia.class);

	private int codigoAcao;
	private Class<? extends ExecutorAcaoAnormalidade> implementacaoAcao;

	/**
	 * Construtor padrão do tipo acao anormalidade leitura
	 * @param codigoAcao codigo da acao
	 * @param implementacaoAcao implementacao do {@link ExecutorAcaoAnormalidade}
	 */
	TipoAcaoAnormalidadeLeitura(int codigoAcao, Class<? extends ExecutorAcaoAnormalidade> implementacaoAcao) {
		this.codigoAcao = codigoAcao;
		this.implementacaoAcao = implementacaoAcao;
	}

	/**
	 * Obtém o enum {@link TipoAcaoAnormalidadeLeitura} referente a partir do codigo da ação de anormalidade de leitura
	 * @param codigoAcao codigo da ação de anormalidade de leitura
	 * @return retorna o enum ou null caso não exista
	 */
	public static TipoAcaoAnormalidadeLeitura fromCodigoAcao(int codigoAcao) {
		return Arrays.stream(TipoAcaoAnormalidadeLeitura.values())
				.filter(t -> t.getCodigoAcao() == codigoAcao)
				.findFirst().orElse(null);
	}

	/**
	 * Obtém o código da ação
	 * @return retorna o codigo da ação;
	 */
	public int getCodigoAcao() {
		return codigoAcao;
	}

	/**
	 * Obtém o tipo da implementação do {@link ExecutorAcaoAnormalidade} que implementa ação informada
	 * @return a classe da implementação do executor
	 */
	public Class<? extends ExecutorAcaoAnormalidade> getImplementacaoAcao() {
		return implementacaoAcao;
	}
}
