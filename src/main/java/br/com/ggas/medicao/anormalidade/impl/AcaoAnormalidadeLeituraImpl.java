/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.anormalidade.impl;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeLeitura;

import java.util.Map;

/**
 * 
 * Entidade de ação da anormalidade de leitura
 */
public class AcaoAnormalidadeLeituraImpl extends EntidadeNegocioImpl implements AcaoAnormalidadeLeitura {

	/**
	 * serial
	 */
	private static final long serialVersionUID = 4837425204512967110L;

	private String descricao;

	private boolean indicadorComLeitura;

	private boolean indicadorSemLeitura;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.AnormalidadeLeitura
	 * #isIndicadorComLeitura()
	 */
	public boolean isIndicadorComLeitura() {

		return indicadorComLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.AnormalidadeLeitura
	 * #setIndicadorComLeitura(boolean)
	 */
	public void setIndicadorComLeitura(boolean indicadorComLeitura) {

		this.indicadorComLeitura = indicadorComLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.AnormalidadeLeitura
	 * #isIndicadorSemLeitura()
	 */
	public boolean isIndicadorSemLeitura() {

		return indicadorSemLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.AnormalidadeLeitura
	 * #setIndicadorSemLeitura(boolean)
	 */
	public void setIndicadorSemLeitura(boolean indicadorSemLeitura) {

		this.indicadorSemLeitura = indicadorSemLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.AnormalidadeLeitura
	 * #getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.AnormalidadeLeitura
	 * #setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.geral.negocio.EntidadeNegocio
	 * #validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
