package br.com.ggas.medicao.anormalidade.impl;

import java.util.Map;

import br.com.ggas.cadastro.imovel.ModalidadeMedicaoImovel;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AnormalidadeSegmentoParametro;

/**
 * Implementação da entidade de anormalidade de segmento
 */
public class AnormalidadeSegmentoParametroImpl extends EntidadeNegocioImpl implements AnormalidadeSegmentoParametro {

	private static final long serialVersionUID = -7924349657649553648L;
	private AnormalidadeConsumo anormalidadeConsumo;
	private Segmento segmento;
	private ModalidadeMedicaoImovel modalidadeMedicao;
	private Integer quantidadeDeRecorrencia;
	private Boolean bloqueiaFaturamento;
	private RamoAtividade ramoAtividade;

	public AnormalidadeConsumo getAnormalidadeConsumo() {
		return anormalidadeConsumo;
	}

	public void setAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo) {
		this.anormalidadeConsumo = anormalidadeConsumo;
	}

	public Segmento getSegmento() {
		return segmento;
	}

	public void setSegmento(Segmento segmento) {
		this.segmento = segmento;
	}

	public ModalidadeMedicaoImovel getModalidadeMedicao() {
		return modalidadeMedicao;
	}

	public void setModalidadeMedicao(ModalidadeMedicaoImovel parametroSistema) {
		this.modalidadeMedicao = parametroSistema;
	}
	
	public Integer getQuantidadeDeRecorrencia() {
		return quantidadeDeRecorrencia;
	}

	public void setQuantidadeDeRecorrencia(Integer quantidadeDeRecorrencia) {
		this.quantidadeDeRecorrencia = quantidadeDeRecorrencia;
	}

	public Boolean getBloqueiaFaturamento() {
		return bloqueiaFaturamento;
	}

	public void setBloqueiaFaturamento(Boolean bloqueiaFaturamento) {
		this.bloqueiaFaturamento = bloqueiaFaturamento;
	}

	@Override
	public Map<String, Object> validarDados() {
		return null;
	}

	public RamoAtividade getRamoAtividade() {
		return ramoAtividade;
	}

	public void setRamoAtividade(RamoAtividade ramoAtividade) {
		this.ramoAtividade = ramoAtividade;
	}
}
