/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.anormalidade.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AcaoAnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.AnormalidadeSegmentoParametro;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

/**
 * 
 * 
 */
class ControladorAnormalidadeImpl extends ControladorNegocioImpl implements ControladorAnormalidade {
	
	public static final long CHAVE_PRIMARIA_INEXISTENTE = -1l;

	private Map<Long, AnormalidadeConsumo> anormalidadeConsumoCache = new HashMap<>();
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return null;
	}

	@Override
	public Class<?> getClasseEntidade() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #criarAnormalidadeLeitura()
	 */
	@Override
	public EntidadeNegocio criarAnormalidadeLeitura() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AnormalidadeLeitura.BEAN_ID_ANORMALIDADE_LEITURA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #criarAnormalidadeConsumo()
	 */
	@Override
	public EntidadeNegocio criarAnormalidadeConsumo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AnormalidadeConsumo.BEAN_ID_ANORMALIDADE_CONSUMO);
	}

	public Class<?> getClasseEntidadeAnormalidadeLeitura() {

		return ServiceLocator.getInstancia().getClassPorID(AnormalidadeLeitura.BEAN_ID_ANORMALIDADE_LEITURA);
	}

	public Class<?> getClasseEntidadeAnormalidadeConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(AnormalidadeConsumo.BEAN_ID_ANORMALIDADE_CONSUMO);
	}

	/**
	 * Criar acao anormalidade leitura.
	 * 
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarAcaoAnormalidadeLeitura() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AcaoAnormalidadeLeitura.BEAN_ID_ACAO_ANORMALIDADE_LEITURA);
	}

	public Class<?> getClasseEntidadeAcaoAnormalidadeLeitura() {

		return ServiceLocator.getInstancia().getClassPorID(AcaoAnormalidadeLeitura.BEAN_ID_ACAO_ANORMALIDADE_LEITURA);
	}

	/**
	 * Criar acao anormalidade consumo.
	 * 
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarAcaoAnormalidadeConsumo() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(AcaoAnormalidadeConsumo.BEAN_ID_ACAO_ANORMALIDADE_CONSUMO);
	}

	public Class<?> getClasseEntidadeAcaoAnormalidadeConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(AcaoAnormalidadeConsumo.BEAN_ID_ACAO_ANORMALIDADE_CONSUMO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.ControladorAnormalidade#consultarAnormalidadesConsumo(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AnormalidadeConsumo> consultarAnormalidadesConsumo(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeAnormalidadeConsumo());
		preencherCriteria(filtro, criteria, true);
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.ControladorAnormalidade#consultarAnormalidadesLeitura(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AnormalidadeLeitura> consultarAnormalidadesLeitura(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidadeAnormalidadeLeitura());
		preencherCriteria(filtro, criteria, false);
		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {
		if (!(entidadeNegocio instanceof AnormalidadeSegmentoParametroImpl)) {
			validarAnormalidadeExistente(entidadeNegocio);
		}
	}

	/**
	 * Preencher criteria.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param criteria
	 *            the criteria
	 * @param consumo
	 *            the consumo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void preencherCriteria(Map<String, Object> filtro, Criteria criteria, boolean consumo) throws NegocioException {

		if(filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			String descricao = (String) filtro.get(TabelaAuxiliar.ATRIBUTO_DESCRICAO);
			if(!StringUtils.isEmpty(descricao)) {
				criteria.add(Restrictions.ilike(TabelaAuxiliar.ATRIBUTO_DESCRICAO, Util.formatarTextoConsulta(descricao)));
			}
			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}
			if(!consumo) {
				Long idAcaoAnormalidadeSemConsumo = (Long) filtro.get("idAcaoAnormalidadeSemConsumo");
				if((idAcaoAnormalidadeSemConsumo != null) && (idAcaoAnormalidadeSemConsumo > 0)) {
					criteria.createAlias("acaoAnormalidadeSemConsumo", "acaoAnormalidadeSemConsumo");
					criteria.add(Restrictions.eq("acaoAnormalidadeSemConsumo.chavePrimaria", idAcaoAnormalidadeSemConsumo));
				} else {
					criteria.setFetchMode("acaoAnormalidadeSemConsumo", FetchMode.JOIN);
				}
				Long idAcaoAnormalidadeComConsumo = (Long) filtro.get("idAcaoAnormalidadeComConsumo");
				if((idAcaoAnormalidadeComConsumo != null) && (idAcaoAnormalidadeComConsumo > 0)) {
					criteria.createAlias("acaoAnormalidadeComConsumo", "acaoAnormalidadeComConsumo");
					criteria.add(Restrictions.eq("acaoAnormalidadeComConsumo.chavePrimaria", idAcaoAnormalidadeComConsumo));
				} else {
					criteria.setFetchMode("acaoAnormalidadeComConsumo", FetchMode.JOIN);
				}
				Long idAcaoAnormalidadeSemLeitura = (Long) filtro.get("idAcaoAnormalidadeSemLeitura");
				if((idAcaoAnormalidadeSemLeitura != null) && (idAcaoAnormalidadeSemLeitura > 0)) {
					criteria.createAlias("acaoAnormalidadeSemLeitura", "acaoAnormalidadeSemLeitura");
					criteria.add(Restrictions.eq("acaoAnormalidadeSemLeitura.chavePrimaria", idAcaoAnormalidadeSemLeitura));
				} else {
					criteria.setFetchMode("acaoAnormalidadeSemLeitura", FetchMode.JOIN);
				}
				Long idAcaoAnormalidadeComLeitura = (Long) filtro.get("idAcaoAnormalidadeComLeitura");
				if((idAcaoAnormalidadeComLeitura != null) && (idAcaoAnormalidadeComLeitura > 0)) {
					criteria.createAlias("acaoAnormalidadeComLeitura", "acaoAnormalidadeComLeitura");
					criteria.add(Restrictions.eq("acaoAnormalidadeComLeitura.chavePrimaria", idAcaoAnormalidadeComLeitura));
				} else {
					criteria.setFetchMode("acaoAnormalidadeComLeitura", FetchMode.JOIN);
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #listarAnormalidadesConsumo()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AnormalidadeConsumo> listarAnormalidadesConsumo() {

		Query query = criarQueryParaCombos(getClasseEntidadeAnormalidadeConsumo().getSimpleName());
		return (Collection<AnormalidadeConsumo>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #listarAnormalidadesLeitura()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AnormalidadeLeitura> listarAnormalidadesLeitura() {

		Query query = criarQueryParaCombos(getClasseEntidadeAnormalidadeLeitura().getSimpleName());
		return (Collection<AnormalidadeLeitura>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #listarAcaoAnormalidadeLeitura()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AcaoAnormalidadeLeitura> listarAcaoAnormalidadeLeitura() {

		Query query = criarQueryParaCombosComLeitura(getClasseEntidadeAcaoAnormalidadeLeitura().getSimpleName());
		return (Collection<AcaoAnormalidadeLeitura>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #listarAcaoAnormalidadeLeitura()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AcaoAnormalidadeLeitura> listarAcaoAnormalidadeComLeitura() {

		Query query = criarQueryParaCombosComLeitura(getClasseEntidadeAcaoAnormalidadeLeitura().getSimpleName());
		return (Collection<AcaoAnormalidadeLeitura>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #listarAcaoAnormalidadeSemLeitura()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AcaoAnormalidadeLeitura> listarAcaoAnormalidadeSemLeitura() {

		Query query = criarQueryParaCombosSemLeitura(getClasseEntidadeAcaoAnormalidadeLeitura().getSimpleName());
		return (Collection<AcaoAnormalidadeLeitura>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #listarAcaoAnormalidadeConsumo()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AcaoAnormalidadeConsumo> listarAcaoAnormalidadeConsumo() {

		Query query = criarQueryParaCombos(getClasseEntidadeAcaoAnormalidadeConsumo().getSimpleName());
		return (Collection<AcaoAnormalidadeConsumo>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #listarAcaoAnormalidadeConsumo()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AcaoAnormalidadeConsumo> listarAcaoAnormalidadeConsumoComLeitura() {

		Query query = criarQueryParaComboConsumoComLeitura(getClasseEntidadeAcaoAnormalidadeConsumo().getSimpleName());
		return (Collection<AcaoAnormalidadeConsumo>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #listarAcaoAnormalidadeConsumo()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<AcaoAnormalidadeConsumo> listarAcaoAnormalidadeConsumoSemLeitura() {

		Query query = criarQueryParaComboConsumoSemLeitura(getClasseEntidadeAcaoAnormalidadeConsumo().getSimpleName());
		return (Collection<AcaoAnormalidadeConsumo>) query.list();
	}

	/**
	 * Criar query para combos.
	 * 
	 * @param classe
	 *            the classe
	 * @return the query
	 */
	private Query criarQueryParaCombos(String classe) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe);
		hql.append(" where habilitado = true ");
		hql.append(" order by descricao ");

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
	}

	/**
	 * Criar query para combo consumo com leitura.
	 * 
	 * @param classe
	 *            the classe
	 * @return the query
	 */
	private Query criarQueryParaComboConsumoComLeitura(String classe) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe);
		hql.append(" where habilitado = true ");
		hql.append(" and indicadorComLeitura = true");
		hql.append(" order by descricao ");

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
	}

	/**
	 * Criar query para combo consumo sem leitura.
	 * 
	 * @param classe
	 *            the classe
	 * @return the query
	 */
	private Query criarQueryParaComboConsumoSemLeitura(String classe) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe);
		hql.append(" where habilitado = true ");
		hql.append(" and indicadorSemLeitura = true");
		hql.append(" order by descricao ");

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
	}

	/**
	 * Criar query para combos com leitura.
	 * 
	 * @param classe
	 *            the classe
	 * @return the query
	 */
	private Query criarQueryParaCombosComLeitura(String classe) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe);
		hql.append(" where habilitado = true ");
		hql.append(" and indicadorComLeitura = true");
		hql.append(" order by descricao ");

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
	}

	/**
	 * Criar query para combos sem leitura.
	 * 
	 * @param classe
	 *            the classe
	 * @return the query
	 */
	private Query criarQueryParaCombosSemLeitura(String classe) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(classe);
		hql.append(" where habilitado = true ");
		hql.append(" and indicadorSemLeitura = true");
		hql.append(" order by descricao ");

		return getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
	}

	/**
	 * Consulta uma anormalidade de consumo por chave primária. 
	 * Caso a anormalidade já tenha sido consultada, obtém a 
	 * mesma do {@code anormalidadeConsumoCache}.
	 * @param chavePrimaria
	 * @return anormalidadeConsumo
	 */
	@Override
	public AnormalidadeConsumo obterAnormalidadeConsumo(long chavePrimaria) throws NegocioException {
		AnormalidadeConsumo anormalidadeConsumo;
		if ( anormalidadeConsumoCache.containsKey(chavePrimaria)) {
			anormalidadeConsumo = anormalidadeConsumoCache.get(chavePrimaria);
		} else {
			anormalidadeConsumo = (AnormalidadeConsumo) super.obter(chavePrimaria, this.getClasseEntidadeAnormalidadeConsumo());
			if ( anormalidadeConsumo != null ) {
				anormalidadeConsumoCache.put(anormalidadeConsumo.getChavePrimaria(), anormalidadeConsumo);
			}
		}
		return anormalidadeConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #obterAnormalidadeLeitura(long)
	 */
	@Override
	public AnormalidadeLeitura obterAnormalidadeLeitura(long chavePrimaria) throws NegocioException {

		return (AnormalidadeLeitura) super.obter(chavePrimaria, this.getClasseEntidadeAnormalidadeLeitura());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #obterAcaoAnormalidadeConsumo(long)
	 */
	@Override
	public AcaoAnormalidadeConsumo obterAcaoAnormalidadeConsumo(long chavePrimaria) throws NegocioException {

		return (AcaoAnormalidadeConsumo) super.obter(chavePrimaria, this.getClasseEntidadeAcaoAnormalidadeConsumo());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #obterAcaoAnormalidadeLeitura(long)
	 */
	@Override
	public AcaoAnormalidadeLeitura obterAcaoAnormalidadeLeitura(long chavePrimaria) throws NegocioException {

		return (AcaoAnormalidadeLeitura) super.obter(chavePrimaria, this.getClasseEntidadeAcaoAnormalidadeLeitura());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #atualizarAnormalidadeLeitura
	 * (br.com.ggas.medicao
	 * .anormalidade.AnormalidadeLeitura)
	 */
	@Override
	public void atualizarAnormalidadeLeitura(AnormalidadeLeitura anormalidadeLeitura) throws NegocioException, ConcorrenciaException {

		super.atualizar(anormalidadeLeitura, getClasseEntidadeAnormalidadeLeitura());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.
	 * ControladorAnormalidade
	 * #atualizarAnormalidadeConsumo
	 * (br.com.ggas.medicao
	 * .anormalidade.AnormalidadeConsumo)
	 */
	@Override
	public void atualizarAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo) throws NegocioException, ConcorrenciaException {

		super.atualizar(anormalidadeConsumo, getClasseEntidadeAnormalidadeConsumo());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.ControladorAnormalidade#removerAnormalidadesConsumo(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerAnormalidadesConsumo(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		removerAnormalidades(chavesPrimarias, dadosAuditoria, getClasseEntidadeAnormalidadeConsumo());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.anormalidade.ControladorAnormalidade#removerAnormalidadesLeitura(java.lang.Long[],
	 * br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void removerAnormalidadesLeitura(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		removerAnormalidades(chavesPrimarias, dadosAuditoria, getClasseEntidadeAnormalidadeLeitura());
	}

	/**
	 * Remover anormalidades.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param classe
	 *            the classe
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private void removerAnormalidades(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria, Class<?> classe) throws NegocioException {

		if((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" FROM ");
			hql.append(classe.getSimpleName());
			hql.append(" WHERE ");
			hql.append(" chavePrimaria in (:chavesPrimarias) ");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("chavesPrimarias", chavesPrimarias);
			Collection<EntidadeNegocio> entidades = query.list();
			if((entidades != null) && (!entidades.isEmpty())) {
				for (EntidadeNegocio entidade : entidades) {
					(entidade).setDadosAuditoria(dadosAuditoria);
					this.remover(entidade);
				}
			}
		}
	}

	/**
	 * Método responsável por validar um
	 * [Tipo(consumo/leitura) Anormalidade] para
	 * não haver insercao repetida.
	 * 
	 * @param entidadeNegocio
	 *            the entidade negocio
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	private void validarAnormalidadeExistente(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select count(chavePrimaria) FROM ");
		
		if (entidadeNegocio instanceof AnormalidadeConsumo) {
			hql.append(getClasseEntidadeAnormalidadeConsumo().getSimpleName());
		} else {
			hql.append(getClasseEntidadeAnormalidadeLeitura().getSimpleName());
		}
		
		hql.append(" WHERE ");
		hql.append(" lower(descricao) = lower(:descricao) ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(entidadeNegocio instanceof AnormalidadeConsumo) {
			AnormalidadeConsumo anormalidadeConsumo = (AnormalidadeConsumo) entidadeNegocio;
			query.setParameter(TabelaAuxiliar.ATRIBUTO_DESCRICAO, anormalidadeConsumo.getDescricao());
		} else {
			AnormalidadeLeitura anormalidadeLeitura = (AnormalidadeLeitura) entidadeNegocio;
			query.setParameter(TabelaAuxiliar.ATRIBUTO_DESCRICAO, anormalidadeLeitura.getDescricao());
		}

		Long quantidade = (Long) query.uniqueResult();

		if(quantidade != null && quantidade > 0) {
			throw new NegocioException(ControladorAnormalidade.ERRO_NEGOCIO_ANORMALIDADE_EXISTENTE, true);
		}
	}
	
	@SuppressWarnings("unchecked")
	public Collection<AnormalidadeSegmentoParametro> listarAnormalidadeSegmentoParametros(AnormalidadeConsumo anormalidadeConsumo)
			throws NegocioException {
		return getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createQuery("SELECT a FROM AnormalidadeSegmentoParametroImpl a JOIN FETCH a.segmento LEFT JOIN FETCH a.ramoAtividade "
						+ " WHERE a.anormalidadeConsumo = :anormalidadeConsumo and a.habilitado = true")
				.setLong("anormalidadeConsumo", anormalidadeConsumo.getChavePrimaria()).list();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<AnormalidadeSegmentoParametro> obterAnormalidadeSegmentoParametros(AnormalidadeConsumo anormalidadeConsumo,
			Segmento segmento, int codigoModalidadeMedicao, RamoAtividade ramoAtividade) throws NegocioException {
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select a FROM AnormalidadeSegmentoParametroImpl a ");
		hql.append(" inner join a.anormalidadeConsumo ac ");
		hql.append(" left join a.ramoAtividade ra ");
		hql.append(" left join a.segmento sg ");
		hql.append(" where ");
		hql.append(" a.habilitado = true ");
		if (anormalidadeConsumo.getChavePrimaria() != 0L) {
			hql.append(" and ac.chavePrimaria = :anormalidadeConsumo ");
		}
		if (segmento.getChavePrimaria() != 0L) {
			hql.append(" and sg.chavePrimaria = :segmento ");
		}

		if (ramoAtividade.getChavePrimaria() != 0L) {
			hql.append(" and ra.chavePrimaria = :ramoAtividade ");
		}
		if (codigoModalidadeMedicao > 0) {
			hql.append(" and a.modalidadeMedicao = :modalidadeMedicao ");
		}
				
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (anormalidadeConsumo.getChavePrimaria() != 0L) {
			query.setLong("anormalidadeConsumo", anormalidadeConsumo.getChavePrimaria());

		}
		if (segmento.getChavePrimaria() != 0L) {
			query.setLong("segmento", segmento.getChavePrimaria());
		}

		if (ramoAtividade.getChavePrimaria() != 0L) {
			query.setLong("ramoAtividade", ramoAtividade.getChavePrimaria());
		}
		if (codigoModalidadeMedicao > 0) {
			query.setInteger("modalidadeMedicao", codigoModalidadeMedicao);
		}

		return query.list();

	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<AnormalidadeLeitura> listarAnormalidadesLeituraLazy() {
		return createHQLQuery(
				"select a.chavePrimaria as chavePrimaria, a.descricao as descricao from "
						+ getClasseEntidadeAnormalidadeLeitura().getSimpleName() + " a where habilitado = true order by a.descricao ")
				.setResultTransformer(Transformers.aliasToBean(getClasseEntidadeAnormalidadeLeitura()))
				.list();
	}
}
