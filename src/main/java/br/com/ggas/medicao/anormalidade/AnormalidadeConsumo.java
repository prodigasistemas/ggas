/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.anormalidade;

import br.com.ggas.geral.negocio.EntidadeNegocio;
/**
 * Interface responsável pela assintura dos métodos relacionados
 * a AnormalidadeConsumo
 *
 */
public interface AnormalidadeConsumo extends EntidadeNegocio {

	String BEAN_ID_ANORMALIDADE_CONSUMO = "anormalidadeConsumo";

	String ANORMALIDADE_CONSUMO = "ANORMALIDADE_CONSUMO";

	String DESCRICAO = "ANORMALIDADE_CONSUMO_DESCRICAO";

	String BLOQUEAR_FATURAMENTO = "BLOQUEAR_FATURAMENTO";

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the descricaoAbreviada
	 */
	String getDescricaoAbreviada();

	/**
	 * @param descricaoAbreviada
	 *            the descricaoAbreviada to set
	 */
	void setDescricaoAbreviada(String descricaoAbreviada);

	/**
	 * @return the bloquearFaturamento
	 */
	Boolean getBloquearFaturamento();

	/**
	 * @param bloquearFaturamento - Set Bloquear faturamento.
	 */
	void setBloquearFaturamento(Boolean bloquearFaturamento);

	/**
	 * @return the numeroOcorrenciaConsecutivasAnormalidade
	 */
	Long getNumeroOcorrenciaConsecutivasAnormalidade();

	/**
	 * @param numeroOcorrenciaConsecutivasAnormalidade
	 *            the numeroOcorrenciaConsecutivasAnormalidade to set
	 */
	void setNumeroOcorrenciaConsecutivasAnormalidade(Long numeroOcorrenciaConsecutivasAnormalidade);

	/**
	 * @return the mensagemConsumo
	 */
	String getMensagemConsumo();

	/**
	 * @param mensagemConsumo
	 *            the mensagemConsumo to set
	 */
	void setMensagemConsumo(String mensagemConsumo);

	/**
	 * Configura a regra
	 * @param regra atributo regra
	 */
	void setRegra(String regra);

	/**
	 * Obtém a descrição da regra em que a anormalidade é aplicada
	 * @return valor da regra
	 */
	String getRegra();

	/**
	 * Configura a opção de ignorar ou não a ação automática
	 * @param ignorarAcaoAutomatica se irá ignorar ou não a ação automática
	 */
	void setIgnorarAcaoAutomatica(boolean ignorarAcaoAutomatica);

	/**
	 * Caso true a ação automática da anormalidade de consumo não é aplicada, por exemplo, caso a anormalidade
	 * de virada de medidor estiver com este atributo true, ela será identificada, no entanto, a ação de computar
	 * o valor não será aplicado
	 *
	 * @return se deve ou não ignorar a ação automática
	 */
	boolean isIgnorarAcaoAutomatica();

}
