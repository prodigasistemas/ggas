/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.anormalidade.negocio;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.HistoricoConsumo;

import java.math.BigDecimal;

/**
 * Define a interface de executores de ação de anormalidades (tanto de leitura quando de consumo)
 * {@link br.com.ggas.medicao.anormalidade.dominio.TipoAcaoAnormalidadeLeitura}
 * @author jose.victor@logiquesistemas.com.br
 */
public interface ExecutorAcaoAnormalidade {

	/**
	 * Executa uma ação a partir das informações do histórico
	 * @param historico histórico a ser avaliado
	 * @return retorna o novo valor da ação (para anormalidade de leitura sera uma nova leitura, já para anormalidades de consumo será
	 * um novo consumo)
	 * @throws NegocioException exceção lançada caso haja falha na execução da ação
	 */
	BigDecimal executarAcaoLeituraInformada(HistoricoConsumo historico) throws NegocioException;
}
