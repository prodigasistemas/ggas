/*
 * 
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 
 */

package br.com.ggas.medicao.rota.impl;

import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.web.medicao.rota.PontoConsumoRotaVO;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * Classe responsável por aplicar as regras de 
 * ordenação referentes aos pontos de consumo 
 * associados à rota.
 * 
 * 
 * 
 * @author lcavalcanti
 *
 */
public class RegrasOrdenacaoPontoConsumoRota {

	/**
	 * 
	 * Método responsável por ordenar uma lista de pontos
	 * de consumo.
	 * 
	 * @param listaPontoConsumoRota - {@link List}
	 * 
	 * @return listaPontoConsumo - {@link List}
	 * 
	 */
	public List<PontoConsumo> ordenaListaPontoConsumo(List<PontoConsumo> listaPontoConsumoRota) {
		
		List<PontoConsumo> listaPontoConsumo = new LinkedList<>();
		
		if(listaPontoConsumoRota!=null){
			
			ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();

			List<PontoConsumoRotaVO> listaPontoConsumoVO =
					controladorPontoConsumo.converterListaPontoConsumo(listaPontoConsumoRota, null, null);
			controladorPontoConsumo.ordernarListaPontoConsumoRotaVOPorDescricao(listaPontoConsumoVO);
	
			listaPontoConsumo = converteListaPontoConsumoVOOrdenada(listaPontoConsumoRota, listaPontoConsumoVO);

		}
		
		return listaPontoConsumo;
	}
	
	/**
	 * 
	 * Converte listaPontoConsumoVO em 
	 * uma lista de pontoConsumo.
	 * 
	 * @param listaPontoConsumoParaOrdenacao - {@link List}
	 * @param listaPontoConsumoVO - {@link List}
	 * @return listaOrdenada - {@link List}
	 * 
	 */
	public List<PontoConsumo> converteListaPontoConsumoVOOrdenada(List<PontoConsumo> listaPontoConsumoParaOrdenacao,
			List<PontoConsumoRotaVO> listaPontoConsumoVO) {
		List<PontoConsumo> listaOrdenada = new LinkedList<>();
		for (PontoConsumoRotaVO pontoConsumoVO : listaPontoConsumoVO) {
			for (PontoConsumo pontoConsumo : listaPontoConsumoParaOrdenacao) {
				if(pontoConsumoVO.getIdPontoConsumo().equals(pontoConsumo.getChavePrimaria())){
					listaOrdenada.add(pontoConsumo);
				}
			}
		}
		listaPontoConsumoParaOrdenacao.clear();
		return listaOrdenada;
	}
	

	/**
	 * 
	 * Seta numero sequencia nos pontos de consumo.
	 * 
	 * @param idPontosConsumo - {@link Long} Array
	 * @param pontosConsumo - {@link Map}
	 * 
	 */
	public void insereNumeroSequenciaPontoConsumo(Long[] idPontosConsumo, Map<Long, Object> pontosConsumo) {

		if (idPontosConsumo != null && idPontosConsumo.length > 0) {
			for (int i = 0; i < idPontosConsumo.length; i++) {
				PontoConsumo pontoConsumoRota = (PontoConsumo) pontosConsumo.get(idPontosConsumo[i]);
				if (pontoConsumoRota != null) {
					pontoConsumoRota.setNumeroSequenciaLeitura(i + 1);
				}
			}
		}
	}
	

	/**
	 * 
	 * Seta numero sequencia nos pontos consumo VO.
	 * 
	 * @param listaPontoConsumo - {@link List}
	 * 
	 */
	public void insereNumeroSequenciaPontoConsumoVO(List<PontoConsumoRotaVO> listaPontoConsumo) {

		for (int i = 0; i < listaPontoConsumo.size(); i++) {
			PontoConsumoRotaVO ponto = listaPontoConsumo.get(i);
			if (ponto != null) {
				ponto.setNumeroSequenciaLeitura(i + 1);
			}

		}
	}
	
	/**
	 * 
	 * Método responsável por ordenar partes da lista 
	 * de ponto de consumo onde tais pontos de consumo
	 * pertencem a imóveis condomínio. 
	 * Esse método não altera a ordenação já aplicada 
	 * aos pontos de consumo, apenas reordena os que 
	 * são do mesmo imóvel condomínio através do número
	 * de sequência do imóvel de cada ponto de consumo.
	 * 
	 * @param listaPontoConsumoOrdenada - {@link List}
	 * 
	 */
	public void ordenaImoveisLote(List<PontoConsumo> listaPontoConsumoOrdenada) {
		ListMultimap <Long, PontoConsumo> mapaPontoConsumoCondominio = ArrayListMultimap.create();
		ListMultimap <Long, Integer> mapaIndicePontoConsumoListaOrdenada = ArrayListMultimap.create();
		
		int contador = 0;
		for (PontoConsumo pontoConsumo : listaPontoConsumoOrdenada) {
			if(pontoConsumo.getImovel().getImovelCondominio()!=null){
				mapaPontoConsumoCondominio.put(pontoConsumo.getImovel().getImovelCondominio().getChavePrimaria(), pontoConsumo);
				mapaIndicePontoConsumoListaOrdenada.put(pontoConsumo.getImovel().getImovelCondominio().getChavePrimaria(), contador);
			}
			contador++;
		}
		
		for (Long idCondominio : mapaPontoConsumoCondominio.keySet()) {
			
			List<PontoConsumo> listaPontoConsumoCondominio =  mapaPontoConsumoCondominio.get(idCondominio);
			List<Integer> listaIndicePontoConsumo = mapaIndicePontoConsumoListaOrdenada.get(idCondominio);

			listaIndicePontoConsumo.sort(Comparator.naturalOrder());
			listaPontoConsumoCondominio.sort(Comparator.comparing(o -> {
				int valor = 0;
				if (o.getImovel() != null && o.getImovel().getNumeroSequenciaLeitura() != null) {
					valor = o.getImovel().getNumeroSequenciaLeitura();
				}
				return valor;
			}));

			contador = 0;
			for (PontoConsumo pontoConsumo : listaPontoConsumoCondominio) {
				listaPontoConsumoOrdenada.set(listaIndicePontoConsumo.get(contador), pontoConsumo);
				contador++;
			}
		}
	}
}
