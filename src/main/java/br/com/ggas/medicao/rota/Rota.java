/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.rota;

import java.util.Collection;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.leitura.Leiturista;

public interface Rota extends EntidadeNegocio {

	String BEAN_ID_ROTA = "rota";

	String ROTA_CAMPO_STRING = "ROTA";

	String ROTAS = "ROTAS";

	String PERIODICIDADE = "ROTA_PERIODICIDADE";

	String NUMERO = "ROTA_NUMERO";

	String SETOR_COMERCIAL = "ROTA_SETOR_COMERCIAL";

	String TIPO_LEITURA = "ROTA_TIPO_LEITURA";

	String EMPRESA = "ROTA_EMPRESA";

	String GRUPO_FATURAMENTO = "GRUPO_FATURAMENTO";

	String NUMERO_MAXIMO_PONTOS_CONSUMO = "ROTA_NUMERO_MAXIMO_PONTOS_CONSUMO";

	/**
	 * @return the tipoLeitura
	 */
	TipoLeitura getTipoLeitura();

	/**
	 * @param tipoLeitura
	 *            the tipoLeitura to set
	 */
	void setTipoLeitura(TipoLeitura tipoLeitura);

	/**
	 * @return the empresa
	 */
	Empresa getEmpresa();

	/**
	 * @param empresa
	 *            the empresa to set
	 */
	void setEmpresa(Empresa empresa);

	/**
	 * @return the grupoFaturamento
	 */
	GrupoFaturamento getGrupoFaturamento();

	/**
	 * @param grupoFaturamento
	 *            the grupoFaturamento to set
	 */
	void setGrupoFaturamento(GrupoFaturamento grupoFaturamento);

	/**
	 * @return the setorComercial
	 */
	SetorComercial getSetorComercial();

	/**
	 * @param setorComercial
	 *            the setorComercial to set
	 */
	void setSetorComercial(SetorComercial setorComercial);

	/**
	 * @return the numeroRota
	 */
	String getNumeroRota();

	/**
	 * @param numeroRota
	 *            the numeroRota to set
	 */
	void setNumeroRota(String numeroRota);

	/**
	 * @return the leiturista
	 */
	Leiturista getLeiturista();

	/**
	 * @param leiturista
	 *            the leiturista to set
	 */
	void setLeiturista(Leiturista leiturista);

	/**
	 * @return the periodicidade
	 */
	Periodicidade getPeriodicidade();

	/**
	 * @param periodicidade
	 *            the periodicidade to set
	 */
	void setPeriodicidade(Periodicidade periodicidade);

	/**
	 * @return the cronogramas
	 */
	Collection<CronogramaRota> getCronogramas();

	/**
	 * @param cronogramas
	 *            the cronogramas to set
	 */
	void setCronogramas(Collection<CronogramaRota> cronogramas);

	/**
	 * @return the pontosConsumo
	 */
	Collection<PontoConsumo> getPontosConsumo();

	/**
	 * @param pontosConsumo
	 *            the pontosConsumo to set
	 */
	void setPontosConsumo(Collection<PontoConsumo> pontosConsumo);

	/**
	 * @return the numeroMaximoPontosConsumo
	 */
	Integer getNumeroMaximoPontosConsumo();

	/**
	 * @param numeroMaximoPontosConsumo
	 *            the numeroMaximoPontosConsumo to
	 *            set
	 */
	void setNumeroMaximoPontosConsumo(Integer numeroMaximoPontosConsumo);

	/**
	 * @return {@link Leiturista}
	 */
	Leiturista getLeituristaSuplente();

	/**
	 * @param leituristaSuplente
	 */
	void setLeituristaSuplente(Leiturista leituristaSuplente);
}
