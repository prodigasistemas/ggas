/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.rota;

import java.util.Date;

import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao cronograma de Rota
 * 
 *
 */
public interface CronogramaRota extends EntidadeNegocio {

	String BEAN_ID_CRONOGRAMA_ROTA = "cronogramaRota";

	String CRONOGRAMA_ROTA = "CRONOGRAMA_ROTA_ROTULO";

	String CRONOGRAMAS_ROTA = "CRONOGRAMAS_ROTA_ROTULO";

	String DATA_PREVISTA = "CRONOGRAMA_ROTA_DATA_PREVISTA";

	/**
	 * @return Data realizada formatada.
	 */
	public String getDataRealizadaFormatada();

	/**
	 * @return Data prevista formatada.
	 */
	public String getDataPrevistaFormatada();

	/**
	 * @return the dataPrevista
	 */
	Date getDataPrevista();

	/**
	 * @param dataPrevista
	 *            the dataPrevista to set
	 */
	void setDataPrevista(Date dataPrevista);

	/**
	 * @return the dataRealizada
	 */
	Date getDataRealizada();

	/**
	 * @param dataRealizada
	 *            the dataRealizada to set
	 */
	void setDataRealizada(Date dataRealizada);

	/**
	 * @return the rota
	 */
	Rota getRota();

	/**
	 * @param rota
	 *            the rota to set
	 */
	void setRota(Rota rota);

	/**
	 * @return the anoMesReferencia
	 */
	Integer getAnoMesReferencia();

	/**
	 * @param anoMesReferencia
	 *            the anoMesReferencia to set
	 */
	void setAnoMesReferencia(Integer anoMesReferencia);

	/**
	 * @return the numeroCiclo
	 */
	Integer getNumeroCiclo();

	/**
	 * @param numeroCiclo
	 *            the numeroCiclo to set
	 */
	void setNumeroCiclo(Integer numeroCiclo);

	/**
	 * @return the cronogramaAtividadeFaturamento
	 */
	CronogramaAtividadeFaturamento getCronogramaAtividadeFaturamento();

	/**
	 * @param cronogramaAtividadeFaturamento
	 *            the
	 *            cronogramaAtividadeFaturamento
	 *            to set
	 */
	void setCronogramaAtividadeFaturamento(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento);

}
