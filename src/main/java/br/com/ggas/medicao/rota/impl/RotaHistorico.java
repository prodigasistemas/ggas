
package br.com.ggas.medicao.rota.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Rota;

public class RotaHistorico extends EntidadeNegocioImpl {

	private Leiturista leiturista;

	private Leiturista leituristaSuplente;

	private Rota rota;

	private CronogramaRota cronogramaRota;
	
	private Date dataCadastro;
	
	
	public Leiturista getLeiturista() {

		return leiturista;
	}
	
	public void setLeiturista(Leiturista leiturista) {

		this.leiturista = leiturista;
	}

	public Leiturista getLeituristaSuplente() {
	
		return leituristaSuplente;
	}

	public void setLeituristaSuplente(Leiturista leituristaSuplente) {
	
		this.leituristaSuplente = leituristaSuplente;
	}
	
	public Rota getRota() {
	
		return rota;
	}
	
	public void setRota(Rota rota) {
	
		this.rota = rota;
	}
	
	public CronogramaRota getCronogramaRota() {
	
		return cronogramaRota;
	}
	
	public void setCronogramaRota(CronogramaRota cronogramaRota) {
	
		this.cronogramaRota = cronogramaRota;
	}

	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/**
	 * @return the dataCadastro
	 */
	public Date getDataCadastro() {
		Date data = null;
		if (this.dataCadastro != null) {
			data = (Date) dataCadastro.clone();
		}
		return data;
	}

	/**
	 * @param dataCadastro
	 *            the dataCadastro to set
	 */
	public void setDataCadastro(Date dataCadastro) {
		if (dataCadastro != null) {
			this.dataCadastro = (Date) dataCadastro.clone();
		} else {
			this.dataCadastro = null;
		}
	}
}
