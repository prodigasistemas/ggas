/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorRotaImpl representa uma ControladorRotaImpl no sistema.
 *
 * @since 17/09/2009
 */

package br.com.ggas.medicao.rota.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.cadastro.localidade.ControladorQuadra;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.geral.tabelaAuxiliar.TabelaAuxiliar;
import br.com.ggas.integracao.supervisorio.ControladorSupervisorio;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.rota.TipoRota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorRotaImpl extends ControladorNegocioImpl implements ControladorRota {

	private static final String HABILITADO = "habilitado";

	private static final String NUMERO_ROTA = "numeroRota";

	private static final String FATURAMENTO_GRUPO = "grupoFaturamento";

	private static ServiceLocator serviceLocator = ServiceLocator.getInstancia();

	private static final Logger LOG = Logger.getLogger(ControladorRotaImpl.class);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Rota.BEAN_ID_ROTA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Rota.BEAN_ID_ROTA);
	}

	public Class<?> getClasseEntidadeTipoLeitura() {

		return ServiceLocator.getInstancia().getClassPorID(TipoLeitura.BEAN_ID_TIPO_LEITURA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#criarTipoLeitura()
	 */
	@Override
	public EntidadeNegocio criarTipoLeitura() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(TipoLeitura.BEAN_ID_TIPO_LEITURA);
	}

	public Class<?> getClasseEntidadeLeiturista() {

		return ServiceLocator.getInstancia().getClassPorID(Leiturista.BEAN_ID_LEITURISTA);
	}

	public Class<?> getClasseEntidadeCronogramaAtividadeFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(CronogramaAtividadeFaturamento.BEAN_ID_CRONOGRAMA_ATIVIDADE_FATURAMENTO);
	}

	public Class<?> getClasseEntidadeCronogramaFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(CronogramaFaturamento.BEAN_ID_CRONOGRAMA_FATURAMENTO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#criarPeriodicidade()
	 */
	@Override
	public EntidadeNegocio criarPeriodicidade() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Periodicidade.BEAN_ID_PERIODICIDADE);
	}

	public Class<?> getClasseEntidadePeriodicidade() {

		return ServiceLocator.getInstancia().getClassPorID(Periodicidade.BEAN_ID_PERIODICIDADE);
	}

	public Class<?> getClasseEntidadeTipoRota() {

		return ServiceLocator.getInstancia().getClassPorID(TipoRota.BEAN_ID_TIPO_ROTA);
	}

	public Class<?> getClasseEntidadeGrupoFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(GrupoFaturamento.BEAN_ID_GRUPO_FATURAMENTO);
	}

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadeRotahistorico() {

		return RotaHistorico.class;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #criarCronogramaRota()
	 */
	@Override
	public EntidadeNegocio criarCronogramaRota() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(CronogramaRota.BEAN_ID_CRONOGRAMA_ROTA);
	}

	public Class<?> getClasseEntidadeCronogramaRota() {

		return ServiceLocator.getInstancia().getClassPorID(CronogramaRota.BEAN_ID_CRONOGRAMA_ROTA);
	}

	/**
	 * Montar criteria.
	 *
	 * @param classe
	 *            the classe
	 * @return the criteria
	 */
	private Criteria montarCriteria(Class<?> classe) {

		Criteria criteria = createCriteria(classe);
		criteria.add(Restrictions.eq(HABILITADO, Boolean.TRUE));
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#listarTiposLeitura()
	 */
	@Override
	public Collection<TipoLeitura> listarTiposLeitura() throws NegocioException {

		Criteria criteria = this.montarCriteria(getClasseEntidadeTipoLeitura());

		Order ordenacao = Order.asc("descricao");
		criteria.addOrder(ordenacao);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #listarLeituristas()
	 */
	@Override
	public Collection<Leiturista> listarLeituristas() throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeLeiturista().getSimpleName());
		hql.append(" where ");
		hql.append(" habilitado = true ");
		hql.append(" order by funcionario.nome ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #listarPeriodicidadesRotas()
	 */
	@Override
	public Collection<Periodicidade> listarPeriodicidades() throws NegocioException {

		Criteria criteria = this.montarCriteria(getClasseEntidadePeriodicidade());

		Order ordenacao = Order.asc("descricao");
		criteria.addOrder(ordenacao);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #listarTiposRotas()
	 */
	@Override
	public Collection<TipoRota> listarTiposRotas() throws NegocioException {

		Criteria criteria = this.montarCriteria(getClasseEntidadeTipoRota());

		Order ordenacao = Order.asc("descricao");
		criteria.addOrder(ordenacao);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #listarGruposFaturamentoRotas()
	 */
	@Override
	public Collection<GrupoFaturamento> listarGruposFaturamentoRotas() throws NegocioException {

		Criteria criteria = this.montarCriteria(getClasseEntidadeGrupoFaturamento());

		Order ordenacao = Order.asc("descricao");
		criteria.addOrder(ordenacao);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #consultarRota(java.util.Map)
	 */
	@Override
	public Collection<Rota> consultarRota(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();
		filtroRotas(filtro, criteria);

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #obterTipoRota(java.lang.Long)
	 */
	@Override
	public TipoRota obterTipoRota(Long chave) throws NegocioException {

		return (TipoRota) super.obter(chave, getClasseEntidadeTipoRota());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #obterGrupoFaturamento(java.lang.Long)
	 */
	@Override
	public GrupoFaturamento obterGrupoFaturamento(Long chave) throws NegocioException {

		return (GrupoFaturamento) super.obter(chave, getClasseEntidadeGrupoFaturamento(), "periodicidade");
	}

	/**
	 * Consulta o grupo de faturamento por chave primária, obtendo os seguintes atributos:
	 * {@code chavePrimaria}, {@code descricao}.
	 * @param chave
	 * @return grupo de faturamento
	 */
	@Override
	public GrupoFaturamento obterAtributosGrupoFaturamento(Long chave) {
		StringBuilder hql = new StringBuilder();
		hql.append(" select chavePrimaria as chavePrimaria, descricao as descricao ");
		hql.append(" from ");
		hql.append(getClasseEntidadeGrupoFaturamento().getSimpleName());
		hql.append(" grupoFaturamento ");
		hql.append(" where grupoFaturamento.chavePrimaria = ?");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidadeGrupoFaturamento()));
		query.setParameter(0, chave);
		return (GrupoFaturamento) query.uniqueResult();
	}

	/**
	  * Consulta a rota por chave primária, obtendo os seguintes atributos:
	  * {@code chavePrimaria}.
	  * @param chave
	  * @return rota
	  */
	@Override
	public Rota obterAtributosRota(Long chave) {
		StringBuilder hql = new StringBuilder();
		hql.append(" select rota.chavePrimaria as chavePrimaria, ");
		hql.append(" rota.numeroRota as numeroRota, ");
		hql.append(" rota.periodicidade as periodicidade, ");
		hql.append(" rota.grupoFaturamento as grupoFaturamento ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota ");
		hql.append(" left join rota.periodicidade periodicidade ");
		hql.append(" inner join rota.grupoFaturamento periodicidade ");
		hql.append(" where rota.chavePrimaria = ? ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidade()));
		query.setParameter(0, chave);
		return (Rota) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #obterLeiturista(java.lang.Long)
	 */
	@Override
	public Leiturista obterLeiturista(Long chave) throws NegocioException {

		return (Leiturista) super.obter(chave, getClasseEntidadeLeiturista());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #obterPeriodicidade(java.lang.Long)
	 */
	@Override
	public Periodicidade obterPeriodicidade(Long chave) throws NegocioException {

		return (Periodicidade) super.obter(chave, getClasseEntidadePeriodicidade());
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #obterTipoLeitura(java.lang.Long)
	 */
	@Override
	public TipoLeitura obterTipoLeitura(Long chave) throws NegocioException {

		return (TipoLeitura) super.obter(chave, getClasseEntidadeTipoLeitura());
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if (entidadeNegocio instanceof Rota) {
			Rota rota = (Rota) entidadeNegocio;
			validarRotaExistente(rota);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		if (entidadeNegocio instanceof Rota) {
			Rota rota = (Rota) entidadeNegocio;
			validarRotaExistente(rota);
			validarRotaEmProcessoLeitura(rota);
		}

	}

	/**
	 * Validar rota existente.
	 *
	 * @param rota
	 *            the rota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarRotaExistente(Rota rota) throws NegocioException {

		Long chavePrimariaRota = rota.getChavePrimaria();
		String numeroRota = rota.getNumeroRota();

		if (!StringUtils.isEmpty(numeroRota)) {

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(this.getHQLConsultaRotaExistente());
			query.setString(0, numeroRota);

			Long chavePrimariaExistente = (Long) query.uniqueResult();
			if (chavePrimariaExistente != null && chavePrimariaExistente.longValue() != chavePrimariaRota.longValue()) {
				throw new NegocioException(ControladorRota.ERRO_NEGOCIO_ROTA_EXISTENTE, numeroRota);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#validarRotaEmProcessoLeitura(br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public void validarRotaEmProcessoLeitura(Rota rota) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select cronogramaRota1 from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName()).append(" cronogramaRota1, ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName()).append(" cronogramaAtividadeFaturamento1 ");
		hql.append(" where cronogramaRota1.rota.chavePrimaria = :idRota");
		hql.append(" and cronogramaAtividadeFaturamento1.chavePrimaria = cronogramaRota1.cronogramaAtividadeFaturamento.chavePrimaria ");
		hql.append(" and cronogramaAtividadeFaturamento1.atividadeSistema.chavePrimaria = :idAtividadeRetornar ");
		hql.append(" and cronogramaRota1.dataRealizada is null ");

		hql.append(" and exists (  ");

		hql.append("select cronogramaRota2 from ").append(getClasseEntidadeCronogramaRota().getSimpleName()).append(" cronogramaRota2, ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName()).append(" cronogramaAtividadeFaturamento2 ");
		hql.append(" where cronogramaRota2.rota.chavePrimaria = :idRota");
		hql.append(" and cronogramaAtividadeFaturamento2.cronogramaFaturamento.chavePrimaria = ")
				.append("cronogramaAtividadeFaturamento1.cronogramaFaturamento.chavePrimaria ");
		hql.append(" and cronogramaRota2.cronogramaAtividadeFaturamento.chavePrimaria = cronogramaAtividadeFaturamento2.chavePrimaria ");
		hql.append(" and cronogramaAtividadeFaturamento2.atividadeSistema.chavePrimaria = :idAtividadeGerar ");
		hql.append(" and cronogramaRota2.dataRealizada is not null ");
		hql.append(" )");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idRota", rota.getChavePrimaria());
		query.setLong("idAtividadeRetornar", AtividadeSistema.CODIGO_ATIVIDADE_RETORNAR_LEITURA);
		query.setLong("idAtividadeGerar", AtividadeSistema.CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA);

		Collection<CronogramaRota> cronogramasRota = query.list();
		if (cronogramasRota != null && !cronogramasRota.isEmpty()) {
			throw new NegocioException(ControladorRota.ERRO_NEGOCIO_ROTA_EM_PROCESSO_LEITURA, rota.getNumeroRota());
		}
	}

	private String getHQLConsultaRotaExistente() {

		StringBuilder hql = new StringBuilder();
		hql.append(" select rota.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota where ");
		hql.append(" rota.numeroRota = ? ");
		hql.append(" and rota.habilitado = true ");

		return hql.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#validarRemoverRotas(java.lang.Long[])
	 */
	@Override
	public void validarRemoverRotas(Long[] chavesPrimarias) throws NegocioException {

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#removerRotas(java.lang.Long[])
	 */
	@Override
	public void removerRotas(Long[] chavesPrimarias) throws NegocioException {

		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
						.getInstancia().getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);
		ControladorFatura controladorFatura = (ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorLeituraMovimento controladorLeituraMovimento = (ControladorLeituraMovimento) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);

		if ((chavesPrimarias != null) && (chavesPrimarias.length > 0)) {

			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put("chavesPrimarias", chavesPrimarias);
			Collection<Rota> listaRota = this.consultarRota(filtro);

			if ((listaRota != null) && (!listaRota.isEmpty())) {
				for (Rota rota : listaRota) {
					filtro.put("chaveRota", rota.getChavePrimaria());
					Collection<Fatura> listaFatura = controladorFatura.consultarFaturasPorRota(filtro);
					Collection<LeituraMovimento> listaLeituraMovimento = controladorLeituraMovimento.consultarMovimentosLeituraPorRota(
									new Long[] {rota.getChavePrimaria()}, null, null, null);

					if ((rota.getPontosConsumo() == null || rota.getPontosConsumo().isEmpty())
									&& (listaFatura == null || listaFatura.isEmpty())
									&& (listaLeituraMovimento == null || listaLeituraMovimento.isEmpty())) {
						controladorCronogramaFaturamento.processarExclusaoRota(rota.getGrupoFaturamento().getChavePrimaria(), rota);
						super.remover(rota, true);

					} else if (rota.getPontosConsumo() != null && !rota.getPontosConsumo().isEmpty()) {
						throw new NegocioException(Constantes.ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_PONTOS_ASSOCIADOS, rota.getNumeroRota());

					} else if (listaFatura != null && !listaFatura.isEmpty()) {
						throw new NegocioException(Constantes.ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_FATURAS_ASSOCIADOS, rota.getNumeroRota());

					} else if (listaLeituraMovimento != null && !listaLeituraMovimento.isEmpty()) {
						throw new NegocioException(Constantes.ERRO_NAO_POSSIVEL_EXCLUIR_ROTA_LEITURA_MOVIMENTO_ASSOCIADOS,
										rota.getNumeroRota());

					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * listarRotasGeracaoDadosLeituraPorSetorComercial
	 * (java.lang.Long)
	 */

	@Override
	public Collection<Rota> listarRotasGeracaoDadosLeituraPorSetorComercial(Long idSetorComercial) {

		ControladorLeituraMovimento controladorLeituraMovimento = (ControladorLeituraMovimento) serviceLocator
						.getControladorNegocio(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);

		String aliasRota = "rota";
		String aliasLeitura = "leitura";

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" ").append(aliasRota);
		hql.append(" where ");
		hql.append(" ").append(aliasRota).append(".chavePrimaria not in (");
		hql.append("    select ").append(aliasLeitura).append(".rota.chavePrimaria ");
		hql.append("    from ");
		hql.append(controladorLeituraMovimento.getClasseEntidade().getSimpleName()).append(" ").append(aliasLeitura);
		hql.append("    where ").append(aliasLeitura).append(".situacaoLeitura.chavePrimaria <> 4");
		hql.append("    ) ");

		if (idSetorComercial != null) {
			hql.append(" and ").append(aliasRota).append(".setorComercial.chavePrimaria = ?");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if (idSetorComercial != null) {
			query.setLong(0, idSetorComercial);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * listarRotasGeracaoDadosLeitura(java.lang
	 * .Long, java.lang.Long)
	 */
	@Override
	public Collection<Rota> listarRotasGeracaoDadosLeitura(Long idGrupoFaturamento, Long idPeriodicidade, Date dataPrevista)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct rota from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" rota, ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName()).append(" cronogramaAtividadeFaturamento, ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName()).append(" cronogramaRota ");

		String condicao = "";
		if ((idGrupoFaturamento != null) && (idGrupoFaturamento > 0)) {
			condicao += " rota.grupoFaturamento.chavePrimaria = :idGrupoFaturamento";
		}

		if ((idPeriodicidade != null) && (idPeriodicidade > 0)) {
			if (condicao.length() > 0) {
				condicao += " and ";
			}
			condicao += " rota.periodicidade.chavePrimaria = :idPeriodicidade";
		}

		if (dataPrevista != null) {
			if (condicao.length() > 0) {
				condicao += " and ";
			}
			condicao += " rota.cronogramas.dataPrevista > :dataPrevista ";
		}

		if (condicao.length() > 0) {
			hql.append(" where ");
			hql.append(condicao);
			hql.append(" and rota.habilitado = true");
		} else {
			hql.append(" where rota.habilitado = true");
		}

		hql.append(" and rota.cronogramas.dataRealizada is null");
		hql.append(" and cronogramaRota.rota = rota ");
		hql.append(" and cronogramaRota.cronogramaAtividadeFaturamento = cronogramaAtividadeFaturamento ");
		hql.append(" and cronogramaRota.cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = 1 ");
		hql.append(" and cronogramaAtividadeFaturamento.dataInicio <= :dataAtual");

		hql.append(" order by rota.numeroRota ");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if ((idGrupoFaturamento != null) && (idGrupoFaturamento > 0)) {
			query.setLong("idGrupoFaturamento", idGrupoFaturamento);
		}

		if ((idPeriodicidade != null) && (idPeriodicidade > 0)) {
			query.setLong("idPeriodicidade", idPeriodicidade);
		}

		if (dataPrevista != null) {
			query.setDate("dataPrevista", dataPrevista);
		}

		query.setDate("dataAtual", new Date());

		Collection<Rota> rotas = query.list();

		Collections.sort((List) rotas, new Comparator<Rota>(){

			@Override
			public int compare(Rota o1, Rota o2) {

				return o1.getNumeroRota().compareTo(o2.getNumeroRota());
			}
		});

		return rotas;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * listarRotasGeracaoDadosLeituraCronogramaEmAtraso
	 * (java.util.Collection)
	 */
	@Override
	public Collection<Rota> listarRotasGeracaoDadosLeituraCronogramaEmAtraso(Collection<Rota> rotasFiltro) throws NegocioException {

		Collection<Rota> rotasEmAlerta = new ArrayList<Rota>();

		if ((rotasFiltro != null) && (!rotasFiltro.isEmpty())) {

			Long[] idRotasFiltro = new Long[rotasFiltro.size()];

			Rota rotaFiltro = null;
			for (int i = 0; i < rotasFiltro.size(); i++) {
				rotaFiltro = ((List<Rota>) rotasFiltro).get(i);
				idRotasFiltro[i] = rotaFiltro.getChavePrimaria();
			}

			Query query = null;

			// Segunda condição para rota estar em
			// alerta
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" rota ");
			hql.append(" where ");
			hql.append(" rota.chavePrimaria in (:idRotasFiltro) ");
			hql.append(" and rota.cronogramas.dataPrevista < :dataAtual and rota.cronogramas.dataRealizada is null ");
			hql.append(" and rota.habilitado = true");

			Date dataAtual = Calendar.getInstance().getTime();
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("idRotasFiltro", idRotasFiltro);
			query.setDate("dataAtual", dataAtual);

			Collection<Rota> rotas = query.list();
			if (rotas != null && !rotas.isEmpty()) {
				rotasEmAlerta.addAll(rotas);
			}

			// Terceira condição para rota estar
			// em alerta
			hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" rota ");
			hql.append(" where ");
			hql.append(" rota.chavePrimaria in (:idRotasFiltro) ");
			hql.append(" and rota.cronogramas.dataRealizada is null ");
			hql.append(" and rota.habilitado = true");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("idRotasFiltro", idRotasFiltro);

			rotas = query.list();
			if ((rotas != null) && (!rotas.isEmpty())) {

				ControladorLeituraMovimento controladorLeituraMovimento = (ControladorLeituraMovimento) serviceLocator
								.getControladorNegocio(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);

				Collection<LeituraMovimento> leiturasMovimentoNaoProcessados = controladorLeituraMovimento
								.consultarMovimentosLeituraNaoProcessados(rotas);

				if ((leiturasMovimentoNaoProcessados != null) && (!leiturasMovimentoNaoProcessados.isEmpty())) {

					for (LeituraMovimento leituraMovimento : leiturasMovimentoNaoProcessados) {
						if (rotas.contains(leituraMovimento.getRota())) {
							Rota rota = leituraMovimento.getRota();

							hql = new StringBuilder();
							hql.append(" from ");
							hql.append(getClasseEntidadeCronogramaRota().getSimpleName()).append(" cronograma ");
							hql.append(" where cronograma.rota.chavePrimaria = ?");
							hql.append(" order by cronograma.dataPrevista ");

							query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

							query.setLong(0, rota.getChavePrimaria());

							List<CronogramaRota> cronogramas = query.list();

							if ((cronogramas != null) && (!cronogramas.isEmpty())) {

								CronogramaRota cronogramaRota = cronogramas.get(0);

								if (!leituraMovimento.getAnoMesFaturamento().equals(cronogramaRota.getAnoMesReferencia())
												&& !leituraMovimento.getCiclo().equals(cronogramaRota.getNumeroCiclo())) {

									DateTime dataPrevista = new DateTime(leituraMovimento.getDataProximaLeituraPrevista());

									if (dataPrevista.toDate().compareTo(dataAtual) < 0) {
										rotasEmAlerta.add(rota);
									}
								}
							}
						}
					}
				}
			}
		}

		this.ordenarLista(rotasEmAlerta);

		return rotasEmAlerta;
	}

	/**
	 * Ordenar lista.
	 *
	 * @param rotasEmAlerta
	 *            the rotas em alerta
	 */
	private void ordenarLista(Collection<Rota> rotasEmAlerta) {

		Collections.sort((List) rotasEmAlerta, new Comparator<Rota>(){

			@Override
			public int compare(Rota o1, Rota o2) {

				return o1.getNumeroRota().compareTo(o2.getNumeroRota());
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#listarRotasGeracaoDadosLeituraSemCronograma(java.lang.Long, java.lang.Long,
	 * java.util.Date)
	 */
	@Override
	public Collection<Rota> listarRotasGeracaoDadosLeituraSemCronograma(Long idGrupoFaturamento, Long idPeriodicidade, Date dataPrevista)
					throws NegocioException {

		Query query = null;

		Map filtro = new HashMap<String, Object>();
		filtro.put("grupoFaturamento", idGrupoFaturamento);
		this.consultarRota(filtro);

		// Primeira condição para rota estar em
		// alerta
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota ");

		String condicao = "";
		if ((idGrupoFaturamento != null) && (idGrupoFaturamento > 0)) {
			condicao += " rota.grupoFaturamento.chavePrimaria = :idGrupoFaturamento";
		}

		if ((idPeriodicidade != null) && (idPeriodicidade > 0)) {
			if (condicao.length() > 0) {
				condicao += " and ";
			}
			condicao += " rota.periodicidade.chavePrimaria = :idPeriodicidade";
		}

		if (dataPrevista != null) {
			if (condicao.length() > 0) {
				condicao += " and ";
			}
			condicao += " rota.cronogramas.dataPrevista > :dataPrevista ";
		}

		if (condicao.length() > 0) {
			hql.append(" where ");
			hql.append(condicao);
			hql.append(" and rota.habilitado = true");
		} else {
			hql.append(" where rota.habilitado = true");
		}

		hql.append(" and not exists (select cronogramaRota from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName()).append(" cronogramaRota ");
		hql.append(" inner join cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" inner join cronogramaAtividadeFaturamento.atividadeSistema atividadeSistema ");
		hql.append(" where cronogramaRota.dataRealizada is null ");
		hql.append(" and atividadeSistema.chavePrimaria = 1 ");

		if ((idGrupoFaturamento != null) && (idGrupoFaturamento > 0)) {
			hql.append(" and cronogramaRota.rota = rota ");
			hql.append(" and cronogramaRota.anoMesReferencia = :referencia ");
			hql.append(" and cronogramaRota.numeroCiclo = :ciclo )");
		} else {
			hql.append(" and cronogramaRota.rota = rota )");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		if ((idGrupoFaturamento != null) && (idGrupoFaturamento > 0)) {
			query.setLong("idGrupoFaturamento", idGrupoFaturamento);
			GrupoFaturamento grupoFaturamento = obterGrupoFaturamento(idGrupoFaturamento);
			query.setParameter("referencia", grupoFaturamento.getAnoMesReferencia());
			query.setLong("ciclo", grupoFaturamento.getNumeroCiclo());
		}

		if ((idPeriodicidade != null) && (idPeriodicidade > 0)) {
			query.setLong("idPeriodicidade", idPeriodicidade);
		}

		if (dataPrevista != null) {
			query.setDate("dataPrevista", dataPrevista);
		}

		Collection<Rota> rotas = query.list();

		Collections.sort((List) rotas, new Comparator<Rota>(){

			@Override
			public int compare(Rota o1, Rota o2) {

				return o1.getNumeroRota().compareTo(o2.getNumeroRota());
			}
		});

		return rotas;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * validarCronogramaRota(br.com.procenge.ggas
	 * .medicao.rota.CronogramaRota)
	 */
	@Override
	public void validarCronogramaRota(CronogramaRota cronogramaRota) throws NegocioException {

		if (cronogramaRota.getDataPrevista() == null) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, CronogramaRota.DATA_PREVISTA);
		} else {
			DateTime dataAtual = new DateTime();
			dataAtual = dataAtual.withHourOfDay(0);
			dataAtual = dataAtual.withMinuteOfHour(0);
			dataAtual = dataAtual.withSecondOfMinute(0);
			dataAtual = dataAtual.withMillisOfSecond(0);
			if (cronogramaRota.getDataPrevista().before(dataAtual.toDate())) {
				throw new NegocioException(ControladorRota.ERRO_NEGOCIO_DATA_MENOR_ATUAL, true);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#listarCronogramasRotaRealizadasPorRota(long)
	 */
	@Override
	public Collection<CronogramaRota> listarCronogramasRotaRealizadasPorRota(long idRota) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" where dataRealizada is not null ");
		hql.append(" and rota.chavePrimaria = ?");
		hql.append(" order by dataPrevista");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, idRota);

		return query.list();
	}

	/**
	 * Obter proximo ano mes referencia cronograma rota.
	 *
	 * @param idRota
	 *            the id rota
	 * @param cronogramasRota
	 *            the cronogramas rota
	 * @return the integer
	 * @throws NegocioException
	 *             the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * obterProximoAnoMesReferenciaCronogramaRota
	 * (long,
	 * java.util.Collection)
	 */
	public Integer obterProximoAnoMesReferenciaCronogramaRota(long idRota, Collection<CronogramaRota> cronogramasRota)
					throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		DateTime proximaDataPrevista = new DateTime();
		Integer proximoAnoMesReferencia = Integer.valueOf((String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO));

		Rota rota = (Rota) obter(idRota);

		List<CronogramaRota> listaCronogramas = new ArrayList<CronogramaRota>(cronogramasRota);

		Collections.sort(listaCronogramas, new Comparator<CronogramaRota>(){

			@Override
			public int compare(CronogramaRota o1, CronogramaRota o2) {

				return o1.getDataPrevista().compareTo(o2.getDataPrevista());
			}
		});

		if (!listaCronogramas.isEmpty()) {
			CronogramaRota ultimoCronograma = listaCronogramas.get(listaCronogramas.size() - 1);
			if (proximoAnoMesReferencia < ultimoCronograma.getAnoMesReferencia()) {
				if (ultimoCronograma.getNumeroCiclo() < rota.getPeriodicidade().getQuantidadeCiclo()) {
					proximoAnoMesReferencia = ultimoCronograma.getAnoMesReferencia();
				} else {
					proximoAnoMesReferencia = Util.adicionarMesEmAnoMes(ultimoCronograma.getAnoMesReferencia());
				}

			}
			if (proximaDataPrevista.toDate().compareTo(ultimoCronograma.getDataPrevista()) < 0) {
				proximaDataPrevista = new DateTime(ultimoCronograma.getDataPrevista());
				proximaDataPrevista.plusDays(1);
			}
		}

		return proximoAnoMesReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * validarCronogramaRotaEmAlerta
	 * (br.com.procenge
	 * .ggas.medicao.rota.CronogramaRota,
	 * long)
	 */
	@Override
	public boolean validarCronogramaRotaEmAlerta(CronogramaRota cronogramaRota, long idRota) {

		boolean emAlerta = false;

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(cronograma.chavePrimaria) from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName()).append(" cronograma ");
		hql.append(", ").append(getClasseEntidade().getSimpleName()).append(" rota ");
		hql.append(" where rota.chavePrimaria = ?");
		hql.append(" and cronograma.rota <> rota ");
		hql.append(" and cronograma.rota.leiturista = rota.leiturista ");
		hql.append(" and cronograma.dataPrevista = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, idRota);
		query.setDate(1, cronogramaRota.getDataPrevista());

		Long resultado = (Long) query.uniqueResult();
		if (resultado != null && resultado > 0) {
			emAlerta = true;
		}

		return emAlerta;
	}

	/**
	 * Listar rotas cronograma em alerta.
	 *
	 * @param cronogramaRota
	 *            the cronograma rota
	 * @return the collection
	 */
	@Override
	public Collection<Rota> listarRotasCronogramaEmAlerta(CronogramaRota cronogramaRota) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct cronograma.rota from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName()).append(" cronograma ");
		hql.append(" where cronograma.rota.chavePrimaria <> ?");
		hql.append(" and cronograma.rota.leiturista = ( select leiturista from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" rota where rota.chavePrimaria = ?) ");
		hql.append(" and cronograma.dataPrevista = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, cronogramaRota.getRota().getChavePrimaria());
		query.setLong(1, cronogramaRota.getRota().getChavePrimaria());
		query.setDate(2, cronogramaRota.getDataPrevista());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * listarRotasRemanejamento(java.lang.Long)
	 */
	@Override
	public Collection<Rota> listarRotasRemanejamento(Rota rota) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota ");
		hql.append(" where rota.chavePrimaria <> :chavePrimaria ");
		hql.append(" and rota.periodicidade.chavePrimaria = :periodicidade ");
		hql.append(" and rota.habilitado = true ");
		hql.append(" order by rota.numeroRota ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chavePrimaria", rota.getChavePrimaria());
		query.setLong("periodicidade", rota.getPeriodicidade().getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * listarRotasRemanejamentoPeriodicidade(br
	 * .com.procenge.ggas.medicao.rota.Rota, long)
	 */
	@Override
	public Collection<Rota> listarRotasRemanejamentoPeriodicidade(long idRotaMantida, long idPeriodicidade) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota ");
		hql.append(" where rota.chavePrimaria <> :chavePrimaria ");
		hql.append(" and rota.periodicidade.chavePrimaria = :periodicidade ");
		hql.append(" and rota.habilitado = true ");
		hql.append(" order by rota.numeroRota ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("chavePrimaria", idRotaMantida);
		query.setLong("periodicidade", idPeriodicidade);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * listarRotasRemanejamento(java.lang.Long)
	 */
	@Override
	public Rota buscarRota(Long chavePrimaria) throws NegocioException {

		Rota rota = null;

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" rota ");
		hql.append(" inner join fetch rota.periodicidade periodicidade ");
		hql.append(" left join fetch rota.leiturista leiturista ");
		hql.append(" left join fetch leiturista.funcionario funcionario ");
		hql.append(" where rota.chavePrimaria = :idChave ");

		hql.append(" order by rota.numeroRota ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idChave", chavePrimaria);

		Collection<Rota> rotas = query.list();
		if (rotas != null && !rotas.isEmpty()) {
			rota = rotas.iterator().next();
		}

		return rota;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * atualizar(br.com.ggas.medicao.
	 * rota.Rota,
	 * java.util.Collection)
	 */
	@Override
	public void atualizar(Rota rota, Collection<PontoConsumo> pontosConsumoRemanejados) throws NegocioException {

		Set<Rota> rotasRemanejamento = new HashSet<Rota>();
		Collection<PontoConsumo> pontosConsumoDesassociados = new ArrayList<PontoConsumo>();

		// Itera os pontos de consumo para
		// preencher as rotas que foram
		// alteradas pelo remanejamento
		for (PontoConsumo pontoConsumo : pontosConsumoRemanejados) {
			if (pontoConsumo.getRota() != null) {
				pontoConsumo.setNumeroSequenciaLeitura(null);
				if (rotasRemanejamento.contains(pontoConsumo.getRota())) {
					for (Rota rotaRemanejamento : rotasRemanejamento) {
						if (rotaRemanejamento.equals(pontoConsumo.getRota())) {
							pontoConsumo.setRota(rotaRemanejamento);
							// Altera o ponto de
							// consumo caso já
							// esteja
							// associado a rota
							if (rotaRemanejamento.getPontosConsumo().contains(pontoConsumo)) {
								this.alterarDadosPontoConsumoAssociadoRota(pontoConsumo, rotaRemanejamento);
							} else {
									// Adiciona o
									// ponto de
									// consumo a
									// rota no
									// caso de um
									// remanejamento
								rotaRemanejamento.getPontosConsumo().add(pontoConsumo);
							}
							break;
						}
					}
				} else {
					Rota rotaRemanejamento = (Rota) obter(pontoConsumo.getRota().getChavePrimaria(), "pontosConsumo");
					if (rotaRemanejamento.getPontosConsumo() == null) {
						rotaRemanejamento.setPontosConsumo(new HashSet<PontoConsumo>());
					}
					// Altera o ponto de consumo
					// caso já esteja associado a
					// rota
					if (rotaRemanejamento.getPontosConsumo().contains(pontoConsumo)) {
						this.alterarDadosPontoConsumoAssociadoRota(pontoConsumo, rotaRemanejamento);
					} else {
						// Adiciona o ponto de
						// consumo a rota no
						// caso de
						// um remanejamento
						pontoConsumo.setRota(rotaRemanejamento);
						rotaRemanejamento.getPontosConsumo().add(pontoConsumo);
					}
					rotasRemanejamento.add(rotaRemanejamento);
				}
			} else {
				throw new NegocioException(ControladorRota.ERRO_NEGOCIO_ROTAS_NAO_REMANEJADAS, true);
			}
		}

		for (Rota rotaRemanejamento : rotasRemanejamento) {
			getHibernateTemplate().saveOrUpdate(rotaRemanejamento);
		}

		if (!rota.isHabilitado()) {

			for (PontoConsumo ponto : rota.getPontosConsumo()) {
				ponto.setNumeroSequenciaLeitura(null);
				ponto.setRota(null);
				pontosConsumoDesassociados.add(ponto);
			}

			rota.getPontosConsumo().clear();

		}

		if (!pontosConsumoDesassociados.isEmpty()) {
			this.getHibernateTemplate().saveOrUpdate(pontosConsumoDesassociados);
		}
		
		getHibernateTemplate().merge(rota);
	}

	/**
	 * Método responsável por alterar os dados de
	 * associação de um ponto de
	 * consumo a uma rota.
	 *
	 * @param pontoConsumoAssociado
	 *            Um ponto de consumo.
	 * @param rota
	 *            Uma rota.
	 */
	private void alterarDadosPontoConsumoAssociadoRota(PontoConsumo pontoConsumoAssociado, Rota rota) {

		for (PontoConsumo pontoConsumoAlterar : rota.getPontosConsumo()) {
			if (pontoConsumoAlterar.equals(pontoConsumoAssociado)) {
				pontoConsumoAlterar.setNumeroSequenciaLeitura(pontoConsumoAssociado.getNumeroSequenciaLeitura());
				break;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * atualizarCronogramaRotaGeracaoPorPontosConsumo
	 * (java.util.Collection)
	 */
	@Override
	public void atualizarCronogramaPorPontosConsumo(Collection<PontoConsumo> pontosConsumo, long chavePrimariaAtividadeSistema,
					Collection<Rota> listRota, StringBuilder logProcesso) throws NegocioException {

		ControladorLeituraMovimento controladorLeituraMovimento = (ControladorLeituraMovimento) serviceLocator
						.getControladorNegocio(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);

		ControladorProcesso controladorProcesso = (ControladorProcesso) serviceLocator
						.getControladorNegocio(ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);

		AtividadeSistema atividadeSistema = controladorProcesso.obterAtividadeSistema(chavePrimariaAtividadeSistema);

		// listar as rotas
		Collection<CronogramaAtividadeFaturamento> cronogramaAtividadeFaturamento = new ArrayList<CronogramaAtividadeFaturamento>();
		Map<Rota, List<PontoConsumo>> mapaRotas = new HashMap<Rota, List<PontoConsumo>>();
		Rota rota = null;

		Date dataRealizacao = Calendar.getInstance().getTime();

		// Identifica a lista de rotas processadas
		// a partir da lista dos pontos de consumo
		// passada
		for (PontoConsumo pontoConsumo : pontosConsumo) {
			rota = pontoConsumo.getRota();
			if (mapaRotas.containsKey(rota)) {
				mapaRotas.get(rota).add(pontoConsumo);
			} else {
				List<PontoConsumo> listaPontosConsumo = new ArrayList<PontoConsumo>();
				listaPontosConsumo.add(pontoConsumo);
				if (pontoConsumo.getRota() != null) {
					mapaRotas.put(pontoConsumo.getRota(), listaPontosConsumo);
				}
			}
		}

		Iterator it = mapaRotas.entrySet().iterator();
		while (it.hasNext()) {

			Map.Entry pairs = (Map.Entry) it.next();
			rota = (Rota) pairs.getKey();

			// metodo de
			// validarCronogramaRotaEmAlerta(cronogramaRota,
			// idRota)
			// primeiro em relação a data atual
			CronogramaRota cronogramaRota = this.obterCronogramaRotaAtual(rota, atividadeSistema.getChavePrimaria(), false, true);

			// se nao tiver cronogramaAtual,
			// levantar uma excessao
			if (cronogramaRota != null) {

				// salva o cronograma da atividade
				// para o grupo de faturamento
				if (!cronogramaAtividadeFaturamento.contains(cronogramaRota.getCronogramaAtividadeFaturamento())) {
					cronogramaAtividadeFaturamento.add(cronogramaRota.getCronogramaAtividadeFaturamento());
				}

				// Identifica a data da realizacao
				// da leitura para atualizar o
				// cronograma
				if (atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_REALIZAR_LEITURA) {
					Long[] chaveRota = new Long[1];
					chaveRota[0] = cronogramaRota.getRota().getChavePrimaria();
					Collection<LeituraMovimento> movimentos = controladorLeituraMovimento.consultarMovimentosLeituraPorRota(chaveRota,
									new Long[]{SituacaoLeituraMovimento.PROCESSADO}, cronogramaRota.getAnoMesReferencia(),
									cronogramaRota.getNumeroCiclo());

					if (movimentos != null && !movimentos.isEmpty()) {
						dataRealizacao = (movimentos.iterator().next()).getDataLeitura();
					}

					ControladorSupervisorio controladorSupervisorio = (ControladorSupervisorio) ServiceLocator.getInstancia().getBeanPorID(
									ControladorSupervisorio.BEAN_ID_CONTROLADOR_SUPERVISORIO);

					// Monta filtro para a consulta
					Map<String, Object> filtro = new HashMap<String, Object>();
					filtro.put("dataReferencia", cronogramaRota.getAnoMesReferencia());
					filtro.put("numeroCiclo", cronogramaRota.getNumeroCiclo());
					filtro.put("chaveRota", chaveRota);
					filtro.put("habilitado", Boolean.TRUE);
					filtro.put("indicadorProcessado", Boolean.TRUE);

					// Monta a ordenação da consulta
					String ordenacao = "dataRealizacaoLeitura desc ";

					Collection<SupervisorioMedicaoDiaria> medicoesDiarias = controladorSupervisorio.consultarSupervisorioMedicaoDiaria(
									filtro, ordenacao);

					if (medicoesDiarias != null && !medicoesDiarias.isEmpty()) {
						dataRealizacao = (medicoesDiarias.iterator().next()).getDataRealizacaoLeitura();
					}

				}

				this.verificarCronogramaRota(rota, cronogramaRota, atividadeSistema, dataRealizacao, logProcesso);
			}

		}

		// Caso seja enviada a lista de rotas
		// processadas
		if (pontosConsumo.isEmpty() && listRota != null && !(listRota.isEmpty())) {
			for (Rota rotaUnica : listRota) {
				CronogramaRota cronogramaRota = this.obterCronogramaRotaAtual(rotaUnica, atividadeSistema.getChavePrimaria(), false, true);
				Date dataAtual = Calendar.getInstance().getTime();
				if (cronogramaRota != null && cronogramaRota.getDataRealizada() == null
								&& dataAtual.compareTo(cronogramaRota.getDataPrevista()) >= 0) {
					this.verificarCronogramaRota(rotaUnica, cronogramaRota, atividadeSistema, dataRealizacao, logProcesso);
					cronogramaAtividadeFaturamento.add(cronogramaRota.getCronogramaAtividadeFaturamento());
				}
			}
		}

		// qual o cronograma a ser atualizado de
		// cada rota
		for (CronogramaAtividadeFaturamento listaCronogramaAtividadeFaturamento : cronogramaAtividadeFaturamento) {
			// metodo de
			// validarCronogramaRotaEmAlerta(cronogramaRota,
			// idRota)
			// primeiro em relação a data atual
			// se nao tiver cronogramaAtual,
			// levantar uma excessao
			if (listaCronogramaAtividadeFaturamento != null) {
				// metodo pra ver se todos os
				// cronogramas de rota do grupo já
				// estão processados$$$
				boolean atualizar = this.verificarRotasNaoProcessadas(listaCronogramaAtividadeFaturamento);
				if (atualizar) {
					// atualiza a data de geração
					// do cronograma para data
					// atual
					listaCronogramaAtividadeFaturamento.setDataRealizacao(dataRealizacao);
					getHibernateTemplate().getSessionFactory().getCurrentSession().update(listaCronogramaAtividadeFaturamento);
					if (logProcesso != null) {
						logProcesso.append("\n\rCronograma do Grupo atualizado com sucesso.");
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * atualizarCronogramaRotaGeracaoPorPontosConsumo
	 * (java.util.Collection)
	 */
	@Override
	public void atualizarCronogramaPorRota(long chAtividadeSistema, Collection<Rota> lRota, StringBuilder logProcesso)
					throws NegocioException {

		ControladorLeituraMovimento controladorLeituraMovimento = (ControladorLeituraMovimento) serviceLocator
						.getControladorNegocio(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);

		ControladorProcesso controladorProcesso = (ControladorProcesso) serviceLocator
						.getControladorNegocio(ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);


		AtividadeSistema atividadeSistema = controladorProcesso.obterAtividadeSistema(chAtividadeSistema);

		Collection<CronogramaAtividadeFaturamento> cronogramaAtividadeFaturamento = new ArrayList<CronogramaAtividadeFaturamento>();

		Date dataRealizacao = Calendar.getInstance().getTime();


		for (Rota rota : lRota) {

			// metodo de
			// validarCronogramaRotaEmAlerta(cronogramaRota,
			// idRota)
			// primeiro em relação a data atual
			CronogramaRota cronogramaRota = this.obterCronogramaRotaAtual(rota, atividadeSistema.getChavePrimaria(), false, true);

			// se nao tiver cronogramaAtual,
			// levantar uma excessao
			if (cronogramaRota != null) {

				// salva o cronograma da atividade
				// para o grupo de faturamento
				if (!cronogramaAtividadeFaturamento.contains(cronogramaRota.getCronogramaAtividadeFaturamento())) {
					cronogramaAtividadeFaturamento.add(cronogramaRota.getCronogramaAtividadeFaturamento());
				}

				// Identifica a data da realizacao
				// da leitura para atualizar o
				// cronograma
				if (atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_REALIZAR_LEITURA) {
					Long[] rotaAtual = new Long[1];
					rotaAtual[0] = cronogramaRota.getRota().getChavePrimaria();
					Collection<LeituraMovimento> movimentos = controladorLeituraMovimento.consultarMovimentosLeituraPorRota(rotaAtual,
									new Long[]{SituacaoLeituraMovimento.PROCESSADO}, cronogramaRota.getAnoMesReferencia(),
									cronogramaRota.getNumeroCiclo());

					if (movimentos != null && !movimentos.isEmpty()) {
						for (LeituraMovimento leituraMovimento : movimentos) {
							dataRealizacao = leituraMovimento.getDataLeitura();
							break;
						}
					}
				} else {
					escreverNovaLinha(logProcesso, "Chave primária da atividade de sistema ("
							+atividadeSistema.getChavePrimaria()+") é diferente de "
							+AtividadeSistema.CODIGO_ATIVIDADE_REALIZAR_LEITURA);
				}
				this.verificarCronogramaRota(rota, cronogramaRota, atividadeSistema, dataRealizacao, logProcesso);
			}
		}

		// Atualiza cronograma do grupo de
		// faturamento
		for (CronogramaAtividadeFaturamento listaCronogramaAtividadeFaturamento : cronogramaAtividadeFaturamento) {
			if (listaCronogramaAtividadeFaturamento != null) {
				// metodo pra ver se todos os
				// cronogramas de rota do grupo já
				// estão processados$$$
				boolean atualizar = this.verificarRotasNaoProcessadas(listaCronogramaAtividadeFaturamento);
				if (atualizar) {
					// atualiza a data de geração
					// do cronograma para data
					// atual
					listaCronogramaAtividadeFaturamento.setDataRealizacao(dataRealizacao);
					getHibernateTemplate().getSessionFactory().getCurrentSession().update(listaCronogramaAtividadeFaturamento);
					logProcesso.append("\rCronograma do grupo ATUALIZADO.");
				} else {
					logProcesso.append("\rCronograma do grupo NÃO FOI ATUALIZADO.");
				}
			}
		}
	}

	/**
	 * Verifica o cronograma para a atividade de Consistir Leitura e Faturar Grupo, atualizando
	 * a data de realização do cronograma de rotas e do cronograma de atividade
	 * do faturamento.
	 * @param mapaCronogramaRotaPorRota
	 * @param atividadeSistema
	 * @param logProcesso
	 * @param rotaPossuiPontosConsumo
	 */
	@Override
	public void atualizarCronogramaPorRota(Map<Rota, Collection<CronogramaRota>> mapaCronogramaRotaPorRota,
					AtividadeSistema atividadeSistema, StringBuilder logProcesso, Map<Rota, Boolean> rotaPossuiPontosConsumo)
			throws NegocioException {

		Collection<CronogramaAtividadeFaturamento> cronogramasDeAtividadeDoFaturamento = new ArrayList<CronogramaAtividadeFaturamento>();

		Date dataRealizacao = Calendar.getInstance().getTime();

		Collection<CronogramaRota> cronogramasDeRotasVerificados = new ArrayList<>();

		escreverAposDuasLinha(logProcesso, "Percorrendo mapa de cronogramas por rota");

		for (Entry<Rota, Collection<CronogramaRota>> entry : mapaCronogramaRotaPorRota.entrySet()) {
			Rota rota = entry.getKey();

			escreverNovaLinha(logProcesso, "Chave do mapa sendo iterado é a seguinte rota: "+rota.getNumeroRota());

			Collection<CronogramaRota> cronogramas = entry.getValue();

			CronogramaRota cronogramaRota = cronogramas.iterator().next();

			if ( cronogramaRota != null ) {
				Boolean rotaPossuiPontos = rotaPossuiPontosConsumo(rota, rotaPossuiPontosConsumo);
				if (rotaPossuiPontos || (!rotaPossuiPontos && (cronogramaRota.getDataRealizada() == null
								&& dataRealizacao.compareTo(cronogramaRota.getDataPrevista()) >= 0))) {

					cronogramasDeAtividadeDoFaturamento = adicionarCronogramasDeAtividadeFaturamento(
									cronogramaRota, cronogramasDeAtividadeDoFaturamento);
					cronogramasDeRotasVerificados = this.verificarCronogramaRota(rota, cronogramasDeRotasVerificados,
									cronogramaRota, atividadeSistema, logProcesso);
				} else {
					escreverNovaLinha(logProcesso, "Rota não possui pontos ou cronograma já foi realizado ou a data prevista do cronograma "
							+ "é menor do que hoje");
					escreverNovaLinha(logProcesso, "Rota possui pontos: "+rotaPossuiPontos);
					escreverNovaLinha(logProcesso, "Data realização do cronograma: "+cronogramaRota.getDataRealizada());
					escreverNovaLinha(logProcesso, "Data hoje: "+dataRealizacao);
				}
			} else {
				escreverAposDuasLinha(logProcesso, "cronogramaRota NULO da rota: "+rota.getNumeroRota());
			}
		}

		if (cronogramasDeRotasVerificados != null && !cronogramasDeRotasVerificados.isEmpty()) {
			this.atualizarDataRealizacaoCronogramaRota(cronogramasDeRotasVerificados, dataRealizacao);
		} else {
			escreverNovaLinha(logProcesso, "Não foi possível atualizar data de realização dos cronogramas, lista de cronogramas "
					+ "verificados NULA");
		}

		Collection<CronogramaAtividadeFaturamento> cronogramasAtualizados = new HashSet<>();

		escreverNovaLinha(logProcesso, "Percorrendo cronogramas de atividades do faturamento...");
		for (CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento : cronogramasDeAtividadeDoFaturamento) {
			if (cronogramaAtividadeFaturamento != null) {

				boolean atualizar = this.verificarRotasNaoProcessadas(cronogramaAtividadeFaturamento);
				if (atualizar) {
					escreverNovaLinha(logProcesso, "Adicionado cronograma na lista de cronogramas atualizados para realizar atualização: "
							+cronogramaAtividadeFaturamento.getLabel());
					cronogramasAtualizados.add(cronogramaAtividadeFaturamento);
				} else {
					escreverNovaLinha(logProcesso, "Cronograma não precisa ser atualizado: "+cronogramaAtividadeFaturamento.getLabel());
				}
			} else {
				escreverNovaLinha(logProcesso, "Cronograma iterado nulo");
			}
		}

		this.atualizarDataRealizacaoCronogramaAtividadeFaturamento(cronogramasAtualizados, dataRealizacao);

		for (CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento : cronogramasDeAtividadeDoFaturamento) {
			if(cronogramasAtualizados.contains(cronogramaAtividadeFaturamento)) {
				escreverNovaLinha(logProcesso, "\n\rCronograma do Grupo atualizado com sucesso.");
			} else {
				escreverNovaLinha(logProcesso, "\n\rNao foi possivel atualizar o Cronograma do Grupo.");
			}
		}
	}

	/**
	 * Adiciona o CronogramaAtividadeFaturamento do CronogramaRota especificado
	 * no parâmetro à coleção de CronogramaAtividadeFaturamento
	 * @param cronograma
	 * @param cronogramasDeAtividadeDoFaturamento
	 * @return cronogramasDeAtividadeDoFaturamento
	 */
	private Collection<CronogramaAtividadeFaturamento> adicionarCronogramasDeAtividadeFaturamento(CronogramaRota cronogramaRota,
					Collection<CronogramaAtividadeFaturamento> cronogramasDeAtividadeDoFaturamento) {

		if (!cronogramasDeAtividadeDoFaturamento.contains(cronogramaRota.getCronogramaAtividadeFaturamento())) {
			cronogramasDeAtividadeDoFaturamento.add(cronogramaRota.getCronogramaAtividadeFaturamento());
		}
		return cronogramasDeAtividadeDoFaturamento;
	}
	/**
	 * Retorna o valor booleano do mapa de booleanos por Rota,
	 * caso a rota especificada no parâmetro não esteja no mapa,
	 * ou o mapa tenha valor nulo, assume-se o retorno como true.
	 *
	 * @param rota
	 * @param rotaPossuiPontosConsumo
	 * @return rota possui pontos de consumo
	 */
	private Boolean rotaPossuiPontosConsumo(Rota rota, Map<Rota, Boolean> rotaPossuiPontosConsumo) {
		Boolean possuiPontos = Boolean.TRUE;
		if (rotaPossuiPontosConsumo != null && rotaPossuiPontosConsumo.containsKey(rota)) {
			possuiPontos = rotaPossuiPontosConsumo.get(rota);
		}
		return possuiPontos;
	}

	/**
	 * Verifica se é possível atualizar o cronograma da rota
	 * para a atividade de Consistir Leitura e Faturar Grupo.
	 * @param rota
	 * @param cronogramasDeRotasVerificados
	 * @param cronogramaRota
	 * @param atividadeSistema
	 * @param logProcesso
	 * @return coleção de cronogramas de rota verificados
	 * @throws NegocioException
	 */
	private Collection<CronogramaRota> verificarCronogramaRota(Rota rota,
					Collection<CronogramaRota> cronogramasDeRotasVerificados, CronogramaRota cronogramaRota,
					AtividadeSistema atividadeSistema, StringBuilder logProcesso) throws NegocioException {

		ControladorLeituraMovimento controladorLeituraMovimento = (ControladorLeituraMovimento) serviceLocator
						.getControladorNegocio(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia()
						.getBeanPorID(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		ConstanteSistema constanteSistemaTipoLeituraEletroCorretor = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_TIPO_LEITURA_ELETROCORRETOR);

		ControladorFatura controladorFatura = (ControladorFatura) serviceLocator.getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Long chaveTipoLeituraEletrocorretor = null;
		try {
			chaveTipoLeituraEletrocorretor = Util.converterCampoStringParaValorLong("Tipo Leitura Eletrocorretor",
							constanteSistemaTipoLeituraEletroCorretor.getValor());
		} catch (FormatoInvalidoException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException(e);
		}
		Boolean atualizar = Boolean.FALSE;

		if (atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_CONSISTIR_LEITURA) {

			if (rota.getTipoLeitura().getChavePrimaria() == chaveTipoLeituraEletrocorretor) {

				atualizar = controladorLeituraMovimento.verificarConsistirLeituraCorretorVazao(rota, cronogramaRota.getAnoMesReferencia(),
								cronogramaRota.getNumeroCiclo());
			} else {

				atualizar = controladorLeituraMovimento.verificarConsistirLeitura(rota, cronogramaRota.getAnoMesReferencia(),
								cronogramaRota.getNumeroCiclo());
			}
		} else if (atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_SIMULAR_FATURAMENTO) {

			atualizar = Boolean.TRUE;

		} else if (atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_FATURAR) {

			if (rota.getTipoLeitura().getChavePrimaria() == chaveTipoLeituraEletrocorretor) {

				atualizar = controladorLeituraMovimento.verificarFaturarGrupoCorretorVazao(rota, cronogramaRota.getAnoMesReferencia(),
								cronogramaRota.getNumeroCiclo());
			} else {
				atualizar = controladorFatura.verificarFaturamentoPorRota(rota, cronogramaRota.getAnoMesReferencia(),
								cronogramaRota.getNumeroCiclo());
			}
		}

		if (atualizar) {
			cronogramasDeRotasVerificados.add(cronogramaRota);
			escreverNovaLinha(logProcesso, "Cronograma da Rota "+rota.getNumeroRota()+" para a atividade "+atividadeSistema.getDescricao()
					+" atualizado com sucesso.");
		} else {
			escreverNovaLinha(logProcesso, "Nao foi possivel atualizar o cronograma da Rota " + rota.getNumeroRota()+" para a atividade "
					+atividadeSistema.getDescricao());
		}

		return cronogramasDeRotasVerificados;
	}


	/**
	 * Atualiza a data de realização de uma coleção de CronogramaRota
	 * @param cronogramasDeRotas
	 * @param dataRealizada
	 */
	private void atualizarDataRealizacaoCronogramaRota(Collection<CronogramaRota> cronogramasDeRotas, Date dataRealizada) {
		if (cronogramasDeRotas != null && !cronogramasDeRotas.isEmpty()) {
			StringBuilder hql = new StringBuilder();
			hql.append(" update ");
			hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
			hql.append(" as cronogramaRota ");
			hql.append(" set cronogramaRota.dataRealizada = :dataRealizada");
			hql.append(" where cronogramaRota in (:cronogramasDeRotas) ");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("cronogramasDeRotas", cronogramasDeRotas);
			query.setTimestamp("dataRealizada", dataRealizada);
			query.executeUpdate();
		}

	}

	/**
	 * Atualiza a data de realização de uma coleção de
	 * CronogramaAtividadeFaturamento
	 *
	 * @param cronogramasDeRotas
	 * @param dataRealizada
	 */
	private void atualizarDataRealizacaoCronogramaAtividadeFaturamento(
			Collection<CronogramaAtividadeFaturamento> cronogramasDeAtividade, Date dataRealizada) {

		if (!Util.isNullOrEmpty(cronogramasDeAtividade)) {
			StringBuilder hql = new StringBuilder();
			hql.append(" update ");
			hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
			hql.append(" as cronogramaAtividadeFaturamento ");
			hql.append(" set cronogramaAtividadeFaturamento.dataRealizacao = :dataRealizada");
			hql.append(" where cronogramaAtividadeFaturamento.chavePrimaria in (:chavesPrimariasCronogramasDeAtividade) ");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("chavesPrimariasCronogramasDeAtividade",
					Util.collectionParaArrayChavesPrimarias(cronogramasDeAtividade));
			query.setTimestamp("dataRealizada", dataRealizada);
			query.executeUpdate();
		}
	}

	/**
	 * Obtém cronogramas de rota, por chaves primárias de rotas e que possuem
	 * cronograma de atividade da atividade especificada no parâmetro. 
	 * Dados trazidos pelo objeto CronogramaRota 
	 * chavePrimaria de rota 
	 * numeroRota de rota
	 * tipoLeitura 
	 * periodicidade 
	 * chavePrimaria de grupo de faturamento
	 * cronogramaAtividadeFaturamento 
	 * chavePrimaria de cronograma 
	 * anoMesReferencia de cronograma
	 * numeroCiclo de cronograma 
	 * dataRealizada de cronograma
	 * dataPrevista de cronograma
	 * 
	 * @param chavesPrimarias
	 * @param atividadeSistema
	 * @return mapa de cronogramas de rota por rota
	 */
	@Override
	public Map<Rota, Collection<CronogramaRota>> obterMapaCronogramaRotaPorRotas(Long[] chavesPrimarias, AtividadeSistema atividadeSistema) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select rota.chavePrimaria, rota.numeroRota, tipoLeitura, ");
		hql.append(" periodicidade, grupoFaturamento.chavePrimaria, ");
		hql.append(" cronogramaAtividadeFaturamento, cronograma.chavePrimaria, ");
		hql.append(" cronograma.anoMesReferencia, cronograma.numeroCiclo, ");
		hql.append(" cronograma.dataRealizada, cronograma.dataPrevista ");
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" as cronograma ");
		hql.append(" inner join cronograma.rota rota ");
		hql.append(" inner join rota.grupoFaturamento grupoFaturamento ");
		hql.append(" left join rota.periodicidade periodicidade ");
		hql.append(" left join rota.tipoLeitura tipoLeitura ");
		hql.append(" inner join cronograma.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" inner join cronogramaAtividadeFaturamento.atividadeSistema atividadeSistema ");
		hql.append(" where rota.chavePrimaria in (:chavesPrimarias)");
		hql.append(" and atividadeSistema.chavePrimaria = :idAtividade");
		hql.append(" and cronogramaAtividadeFaturamento.dataInicio <= :dataAtual");
		hql.append(" and cronograma.anoMesReferencia = grupoFaturamento.anoMesReferencia");
		hql.append(" and cronograma.numeroCiclo = grupoFaturamento.numeroCiclo");
		hql.append(" and cronograma.dataRealizada is null");
		hql.append(" order by cronograma.chavePrimaria asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameterList("chavesPrimarias", chavesPrimarias);
		query.setLong("idAtividade", atividadeSistema.getChavePrimaria());
		query.setDate("dataAtual", Calendar.getInstance().getTime());

		return this.construirMapaCronogramaRotaPorRota(query.list());
	}

	/**
	 * Constrói um mapa de cronogramas de rota por rota a partir
	 * da lista de atributos passada por parâmetro.
	 *
	 * @param listaAtributos
	 * @return mapa de cronogramas de rota por rota
	 */
	private Map<Rota, Collection<CronogramaRota>> construirMapaCronogramaRotaPorRota(List<Object[]> listaAtributos) {

		ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia()
						.getBeanPorID(ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);

		Map<Rota, Collection<CronogramaRota>> mapaCronogramaRotaPorRota = new HashMap<>();
		for (Object[] atributos : listaAtributos) {
			Rota rota = (Rota) this.criar();
			rota.setChavePrimaria((long) atributos[0]);
			rota.setNumeroRota((String) atributos[1]);
			rota.setTipoLeitura((TipoLeitura) atributos[2]);
			rota.setPeriodicidade((Periodicidade) atributos[3]);
			GrupoFaturamento grupoFaturamento = (GrupoFaturamento) controladorImovel.criarGrupoFaturamento();
			grupoFaturamento.setChavePrimaria((long) atributos[4]);
			rota.setGrupoFaturamento(grupoFaturamento);
			CronogramaRota cronogramaRota = (CronogramaRota) criarCronogramaRota();
			cronogramaRota.setCronogramaAtividadeFaturamento((CronogramaAtividadeFaturamento) atributos[5]);
			cronogramaRota.setChavePrimaria((long) atributos[6]);
			cronogramaRota.setAnoMesReferencia((Integer) atributos[7]);
			cronogramaRota.setNumeroCiclo((Integer) atributos[8]);
			cronogramaRota.setDataRealizada((Date) atributos[9]);
			cronogramaRota.setDataPrevista((Date) atributos[10]);
			cronogramaRota.setRota(rota);

			if (mapaCronogramaRotaPorRota.containsKey(rota)) {
				mapaCronogramaRotaPorRota.get(rota).add(cronogramaRota);
			} else {
				Collection<CronogramaRota> listaCronogramaRota = new ArrayList<>();
				listaCronogramaRota.add(cronogramaRota);
				if (cronogramaRota.getRota() != null) {
					mapaCronogramaRotaPorRota.put(cronogramaRota.getRota(), listaCronogramaRota);
				}
			}
		}
		return mapaCronogramaRotaPorRota;

	}

	/**
	 * Verificar cronograma rota.
	 *
	 * @param rota
	 *            the rota
	 * @param cronogramaRota
	 *            the cronograma rota
	 * @param atividadeSistema
	 *            the atividade sistema
	 * @param dataRealizacao
	 *            the data realizacao
	 * @param logProcesso
	 *            the log processo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void verificarCronogramaRota(Rota rota, CronogramaRota cronogramaRota, AtividadeSistema atividadeSistema, Date dataRealizacao,
					StringBuilder logProcesso) throws NegocioException {

		ControladorLeituraMovimento controladorLeituraMovimento = (ControladorLeituraMovimento) serviceLocator
						.getControladorNegocio(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);

		ControladorFatura controladorFatura = (ControladorFatura) serviceLocator
						.getControladorNegocio(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator.getInstancia().getBeanPorID(
						ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ConstanteSistema constanteSistemaTipoLeituraEletroCorretor = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_TIPO_LEITURA_ELETROCORRETOR);

		Long chaveTipoLeituraEletrocorretor = null;
		try {
			chaveTipoLeituraEletrocorretor = Util.converterCampoStringParaValorLong("Tipo Leitura Eletrocorretor",
							constanteSistemaTipoLeituraEletroCorretor.getValor());
		} catch(FormatoInvalidoException e) {
			escreverNovaLinha(logProcesso, "Erro ao obter a chave do tipo de leitura do eletrocorretor: "
					+constanteSistemaTipoLeituraEletroCorretor.getValor());
			LOG.error(e.getMessage(), e);
			throw new NegocioException(e);
		}

		boolean atualizar = false;
		// metodo pra ver se todos os pontos de
		// consumo da rota já estão em leitura
		// movimento para o
		// anoMesRef e ciclo

		if (atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA) {

			atualizar = controladorLeituraMovimento.verificarGerarDadosParaLeitura(rota, cronogramaRota.getAnoMesReferencia(),
							cronogramaRota.getNumeroCiclo());

		} else if (atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_REALIZAR_LEITURA
						|| atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_RETORNAR_LEITURA) {

			if (rota.getTipoLeitura().getChavePrimaria() == chaveTipoLeituraEletrocorretor) {
				atualizar = controladorLeituraMovimento.verificarRetornoLeituraCorretorVazao(rota, cronogramaRota.getAnoMesReferencia(),
								cronogramaRota.getNumeroCiclo());
			}else{
				atualizar = controladorLeituraMovimento.verificarRetornoLeitura(rota, cronogramaRota.getAnoMesReferencia(),
						cronogramaRota.getNumeroCiclo(), logProcesso);
			}

		} else if (atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_CONSISTIR_LEITURA) {

			if (rota.getTipoLeitura().getChavePrimaria() == chaveTipoLeituraEletrocorretor) {

				atualizar = controladorLeituraMovimento.verificarConsistirLeituraCorretorVazao(rota, cronogramaRota.getAnoMesReferencia(),
								cronogramaRota.getNumeroCiclo());

			} else {

				atualizar = controladorLeituraMovimento.verificarConsistirLeitura(rota, cronogramaRota.getAnoMesReferencia(),
								cronogramaRota.getNumeroCiclo());

			}

		} else if (atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_EMITIR_FATURA) {

			atualizar = controladorLeituraMovimento.verificarEmitirFatura(rota, cronogramaRota, chaveTipoLeituraEletrocorretor);

		} else if (atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_SIMULAR_FATURAMENTO) {

			atualizar = true;

		} else if (atividadeSistema.getChavePrimaria() == AtividadeSistema.CODIGO_ATIVIDADE_FATURAR) {

			if (rota.getTipoLeitura().getChavePrimaria() == chaveTipoLeituraEletrocorretor) {
				atualizar = controladorLeituraMovimento.verificarFaturarGrupoCorretorVazao(rota, cronogramaRota.getAnoMesReferencia(),
								cronogramaRota.getNumeroCiclo());
			} else {
				atualizar = controladorFatura.verificarFaturamentoPorRota(rota, cronogramaRota.getAnoMesReferencia(),
								cronogramaRota.getNumeroCiclo());
			}

		}
		if (atualizar) {
			// atualiza a data de geração do
			// cronograma para data atual
			cronogramaRota.setDataRealizada(dataRealizacao);
			getHibernateTemplate().getSessionFactory().getCurrentSession().update(cronogramaRota);
			logProcesso.append("\n\rCronograma da Rota ").append(rota.getNumeroRota()).append(" para a atividade ")
					.append(atividadeSistema.getDescricao()).append(" atualizado com sucesso.");
		} else {
			logProcesso.append("\n\rNão foi possivel atualizar o cronograma da Rota ").append(rota.getNumeroRota())
					.append(" para a atividade ").append(atividadeSistema.getDescricao());
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#obterCronogramaFaturamentoAtual(br.com.ggas.cadastro.imovel.GrupoFaturamento,
	 * br.com.ggas.faturamento.cronograma.AtividadeSistema)
	 */
	@Override
	public CronogramaAtividadeFaturamento obterCronogramaFaturamentoAtual(GrupoFaturamento grupoFaturamento,
					AtividadeSistema atividadeSistema) throws NegocioException {

		CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = null;

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select cronograma from ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName()).append(" cronograma ");
		hql.append(" where cronograma.cronogramaFaturamento.grupoFaturamento.chavePrimaria = :idGrupo");
		hql.append(" and cronograma.atividadeSistema.chavePrimaria = :idAtividade");
		hql.append(" and cronograma.dataInicio =  ");
		hql.append("          (select min(dataInicio) from ").append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append("             where dataInicio <= :dataAtual");
		hql.append("             and cronogramaFaturamento.grupoFaturamento.chavePrimaria = :idGrupo ");
		hql.append(" and         atividadeSistema.chavePrimaria = :idAtividade");
		hql.append("             and dataRealizacao is null) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idGrupo", grupoFaturamento.getChavePrimaria());
		query.setLong("idAtividade", atividadeSistema.getChavePrimaria());
		query.setDate("dataAtual", Calendar.getInstance().getTime());

		Collection<CronogramaAtividadeFaturamento> cronogramaFaturamento = query.list();
		if (cronogramaFaturamento != null && !cronogramaFaturamento.isEmpty()) {
			cronogramaAtividadeFaturamento = cronogramaFaturamento.iterator().next();
		}
		return cronogramaAtividadeFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#obterCronogramaRotaAtual(br.com.ggas.medicao.rota.Rota, java.lang.Long, boolean,
	 * boolean)
	 */
	@Override
	public CronogramaRota obterCronogramaRotaAtual(Rota rota, Long chavePrimaria, boolean realizado, boolean verificaIniciada)
					throws NegocioException {

		CronogramaRota cronogramaRota = null;

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select cronograma from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName()).append(" cronograma ");
		hql.append(" inner join cronograma.rota rota ");
		hql.append(" inner join rota.grupoFaturamento grupoFaturamento ");
		hql.append(" inner join fetch cronograma.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" inner join fetch cronogramaAtividadeFaturamento.atividadeSistema atividadeSistema ");
		hql.append(" where rota.chavePrimaria = :idRota");
		hql.append(" and atividadeSistema.chavePrimaria = :idAtividade");
		if (verificaIniciada) {
			hql.append(" and cronogramaAtividadeFaturamento.dataInicio <= :dataAtual");
		}
		hql.append(" and cronograma.anoMesReferencia = grupoFaturamento.anoMesReferencia");
		hql.append(" and cronograma.numeroCiclo = grupoFaturamento.numeroCiclo");
		if (realizado) {
			hql.append(" and cronograma.dataRealizada is not null");
		} else {
			hql.append(" and cronograma.dataRealizada is null");
		}
		hql.append(" order by cronograma.chavePrimaria asc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idRota", rota.getChavePrimaria());
		query.setLong("idAtividade", chavePrimaria);
		if (verificaIniciada) {
			query.setDate("dataAtual", Calendar.getInstance().getTime());
		}

		Collection<CronogramaRota> cronogramasRota = query.list();
		if (cronogramasRota != null && !cronogramasRota.isEmpty()) {
			cronogramaRota = cronogramasRota.iterator().next();
		}
		return cronogramaRota;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CronogramaRota obterCronogramaRotaAtualNaoFaturado(Rota rota) {
		final Long chaveProcessado = 4L;
		return (CronogramaRota) getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(
				"select cronograma from " + getClasseEntidadeCronogramaRota().getSimpleName() + " cronograma "
				+ "inner join cronograma.rota rota inner join rota.grupoFaturamento grupoFaturamento "
				+ "inner join fetch cronograma.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento "
				+ "inner join fetch cronogramaAtividadeFaturamento.atividadeSistema atividadeSistema "

				+ "where rota.chavePrimaria = :idRota "
				+ "and cronograma.anoMesReferencia = grupoFaturamento.anoMesReferencia "
				+ "and cronograma.numeroCiclo = grupoFaturamento.numeroCiclo "
				+ "and cronograma.dataRealizada is null "

			   
				+ "order by cronograma.chavePrimaria"
		)
				.setLong("idRota", rota.getChavePrimaria())
				.list().stream().findFirst().orElse(null);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#verificarRotasNaoProcessadas(br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento
	 * )
	 */
	@Override
	public boolean verificarRotasNaoProcessadas(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento) {

		// de todos os pontos de consumo ativos da
		// rota, verificar se eles
		// tem ocorrencia em leitura movimento
		// para
		// ref e ciclo do cronograma atual

		Query query = null;
		StringBuilder hql = new StringBuilder();
		// select cronogramarota
		hql.append(" select count(*) from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota ");
		hql.append(" inner join cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" where ");
		hql.append(" cronogramaRota.cronogramaAtividadeFaturamento.chavePrimaria = :idCronogramaAtividade ");
		hql.append(" and cronogramaRota.dataRealizada is null");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idCronogramaAtividade", cronogramaAtividadeFaturamento.getChavePrimaria());

		Long total = (Long) query.uniqueResult();
		boolean atualiza = false;
		if (total != null) {
			atualiza = total == 0;
		}
		return atualiza;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * listarRotasPorGrupoFaturamento(java.lang
	 * .Long)
	 */
	@Override
	public Collection<Rota> listarRotasPorGrupoFaturamento(Long chavePrimariaGrupoFaturamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota ");
		hql.append(" inner join fetch rota.periodicidade periodicidade");
		hql.append(" left join fetch rota.leiturista leiturista ");
		hql.append(" where rota.grupoFaturamento.chavePrimaria = ? ");
		hql.append(" order by rota.numeroRota ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, chavePrimariaGrupoFaturamento);

		return query.list();
	}
	/**
	  * Consulta as rotas de um grupo de faturamento por chave primária do grupo,
	  * obtendo os seguintes atributos da rota:
	  * {@code chavePrimaria}.
	  * @param chavePrimariaGrupoFaturamento
	  * @return rota
	  */
	@Override
	public Collection<Rota> listarAtributosRotasPorGrupoFaturamento(Long chavePrimariaGrupoFaturamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select rota.chavePrimaria as chavePrimaria ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota ");
		hql.append(" inner join rota.periodicidade periodicidade");
		hql.append(" left join rota.leiturista leiturista ");
		hql.append(" where rota.grupoFaturamento.chavePrimaria = ? ");
		hql.append(" order by rota.numeroRota ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidade()));
		query.setLong(0, chavePrimariaGrupoFaturamento);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * obterCronogramaRotaReferenciaCicloAtual
	 * (br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public CronogramaRota obterCronogramaRotaReferenciaCicloAtual(Rota rota) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" where rota.chavePrimaria = :CHAVE_ROTA ");
		hql.append(" and dataRealizada is not null ");
		hql.append(" and dataRealizada <= :DATA_ATUAL ");
		hql.append(" order by dataRealizada desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setMaxResults(1);

		query.setParameter("CHAVE_ROTA", rota.getChavePrimaria());
		query.setParameter("DATA_ATUAL", Calendar.getInstance().getTime());

		return (CronogramaRota) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.cadastro.imovel.
	 * ControladorPontoConsumo#
	 * consultarPontoConsumoPorGrupoFaturamento
	 * (br.com.ggas.cadastro.imovel.GrupoFaturamento
	 * )
	 */
	@Override
	public Collection<Rota> consultarRotaPorGrupoFaturamento(GrupoFaturamento grupoFaturamento) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select rota ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota ");
		hql.append(" where rota.habilitado = true ");
		hql.append(" and rota.grupoFaturamento.chavePrimaria = ? ");
		hql.append(" order by rota.numeroRota ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, grupoFaturamento.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 */
	@Override
	public Collection<Rota> obterRotasCronogramaFaturamento(GrupoFaturamento grupoFaturamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota ");
		hql.append(" inner join fetch rota.periodicidade periodicidade");
		hql.append(" left join fetch rota.leiturista leiturista ");
		hql.append(" where rota.grupoFaturamento.chavePrimaria = ? ");
		hql.append(" and rota.chavePrimaria in ");
		hql.append(" ( ");
		hql.append(" select distinct cronogramaRota.rota.chavePrimaria ");
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName()).append(" cronogramaRota ");
		hql.append(" inner join cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" inner join cronogramaAtividadeFaturamento.cronogramaFaturamento cronogramaFaturamento ");
		hql.append(" where cronogramaFaturamento.anoMesFaturamento = :anoMes ");
		hql.append(" and cronogramaFaturamento.numeroCiclo = :ciclo ");
		hql.append(" and cronogramaFaturamento.grupoFaturamento.chavePrimaria = :idGrupofaturamento ");
		hql.append(" ) ");
		hql.append(" order by rota.numeroRota ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, grupoFaturamento.getChavePrimaria());
		query.setLong("anoMes", grupoFaturamento.getAnoMesReferencia());
		query.setLong("ciclo", grupoFaturamento.getNumeroCiclo());
		query.setLong("idGrupofaturamento", grupoFaturamento.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * obterUltimoCronogramaDaRota(br.com.procenge
	 * .ggas.medicao.rota.Rota)
	 */
	@Override
	public CronogramaRota obterUltimoCronogramaDaRota(Rota rota) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronograma where ");
		hql.append(" cronograma.rota.chavePrimaria = :CHAVE_ROTA ");
		hql.append(" and cronograma.dataPrevista =  ");
		hql.append("            (select max(dataPrevista) from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append("             where ");
		hql.append("             rota.chavePrimaria = :CHAVE_ROTA ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setMaxResults(1);
		query.setParameter("CHAVE_ROTA", rota.getChavePrimaria());

		return (CronogramaRota) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preRemocao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preRemocao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Rota rota = (Rota) entidadeNegocio;
		Collection<PontoConsumo> pontosConsumoDesassociados = new ArrayList<PontoConsumo>();

		if (rota.getPontosConsumo() != null && !rota.getPontosConsumo().isEmpty()) {
			for (PontoConsumo ponto : rota.getPontosConsumo()) {
				ponto.setNumeroSequenciaLeitura(null);
				ponto.setRota(null);
				pontosConsumoDesassociados.add(ponto);
			}
			rota.getPontosConsumo().clear();

			this.getHibernateTemplate().saveOrUpdate(pontosConsumoDesassociados);
		}

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * obterDataPrevistaDoCronograma(java.lang
	 * .Long)
	 */
	@Override
	public Date obterDataPrevistaDoCronograma(Long idRota, AtividadeSistema atividadeSistema) throws NegocioException {

		Date dataPrevista = new Date();

		Rota rota = null;
		rota = (Rota) this.obter(idRota);

		CronogramaRota cronogramaRota = this.obterCronogramaRotaAtual(rota, atividadeSistema.getChavePrimaria(), false, true);

		if (cronogramaRota != null && cronogramaRota.getDataPrevista() != null && dataPrevista.before(cronogramaRota.getDataPrevista())) {
			dataPrevista = cronogramaRota.getDataPrevista();
		}

		return dataPrevista;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * validarCronogramasRotaManutencao(java.util
	 * .Collection)
	 */
	/**
	 * @deprecated
	 */
	@Deprecated
	@Override
	public void validarCronogramasRotaManutencao(Collection<CronogramaRota> listaCronogramaRota) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Collection<CronogramaRota> listaCronogramasEmAberto = new ArrayList<CronogramaRota>();
		if (listaCronogramaRota != null && !listaCronogramaRota.isEmpty()) {
			for (CronogramaRota cronogramaRota : listaCronogramaRota) {
				if (cronogramaRota.getDataRealizada() == null) {
					listaCronogramasEmAberto.add(cronogramaRota);
				}
			}
		}

		Integer quantidadeMinimaCronogramaGerados = Integer.valueOf((String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_MINIMA_CRONOGRAMAS_ABERTOS));

		if (listaCronogramasEmAberto.size() < quantidadeMinimaCronogramaGerados) {
			throw new NegocioException(ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ABERTOS_INFERIOR_MINIMO, quantidadeMinimaCronogramaGerados);
		}

		validarQuantidadeCronogramasEmAberto(listaCronogramasEmAberto.size(), quantidadeMinimaCronogramaGerados);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * obterUltimoCronogramaRotaRealizado(long)
	 */
	@Override
	public CronogramaRota obterUltimoCronogramaRotaRealizado(long chavePrimariaRota) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" where dataRealizada is not null ");
		hql.append(" and rota.chavePrimaria = :idRota ");
		hql.append(" order by dataPrevista desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idRota", chavePrimariaRota);
		query.setMaxResults(1);

		return (CronogramaRota) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #validarAlteracaoRotaInativa(int, int)
	 */
	@Override
	public void validarAlteracaoRotaInativa(Rota rota, Long[] idPontosConsumoAssociados) throws NegocioException {

		if (!rota.isHabilitado() && (idPontosConsumoAssociados.length != 0)) {
			throw new NegocioException(ControladorRota.ERRO_NEGOCIO_ROTA_INATIVA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.contrato.contrato.
	 * ControladorContrato
	 * #validarRemoverContratos(java.lang
	 * .Long[])
	 */
	@Override
	public void validarRemoverCronogramaRota(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException {

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
						.getInstancia().getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);

		StringBuilder hql = new StringBuilder();
		hql.append(" select cronogramaRota from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota ");
		hql.append(" inner join fetch cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" inner join fetch cronogramaAtividadeFaturamento.cronogramaFaturamento cronogramaFaturamento ");
		hql.append(" where cronogramaFaturamento.chavePrimaria in (:chavesPrimarias) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("chavesPrimarias", chavesPrimarias);

		Collection<CronogramaRota> listaCronogramaRota = query.list();

		if (listaCronogramaRota != null && !listaCronogramaRota.isEmpty()) {
			for (CronogramaRota cronogramaRota : listaCronogramaRota) {

				if (cronogramaRota.getCronogramaAtividadeFaturamento().getDataRealizacao() != null) {
					removerCronogramaRotaPorChave(cronogramaRota, true);
				} else {
					removerCronogramaRotaPorChave(cronogramaRota, false);
				}
			}
		}

		controladorCronogramaFaturamento.removerCronogramaAtvidadeFaturamento(chavesPrimarias, dadosAuditoria);

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * listarCronogramasRotaRealizadasPorRota(
	 * long)
	 */
	@Override
	public Collection<CronogramaRota> listarCronogramasRotasPorAtividade(long idAtividade, long chavePrimaria) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota ");
		hql.append(" inner join fetch cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" inner join fetch cronogramaRota.cronogramaAtividadeFaturamento.cronogramaFaturamento cronogramaFaturamento ");
		hql.append(" inner join fetch cronogramaRota.rota rota ");
		hql.append(" left join fetch cronogramaRota.rota.leiturista leiturista ");
		hql.append(" inner join fetch cronogramaRota.rota.periodicidade periodicidade ");
		hql.append(" where cronogramaRota.cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :idAtividade");
		hql.append(" and cronogramaFaturamento.chavePrimaria = :idCronogramaFaturamento");
		hql.append(" order by ");
		hql.append(" rota.numeroRota ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idAtividade", idAtividade);
		query.setLong("idCronogramaFaturamento", chavePrimaria);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#consultarCronogramasRotasPorAtividadeECronogramaFaturamento(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public Collection<CronogramaRota> consultarCronogramasRotasPorAtividadeECronogramaFaturamento(Long idAtividadeSistema,
					Long idCronogramaFaturamento) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota ");
		hql.append(" inner join fetch cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" inner join fetch cronogramaRota.rota rota ");
		hql.append(" left join fetch cronogramaRota.rota.leiturista leiturista ");
		hql.append(" inner join fetch cronogramaRota.rota.periodicidade periodicidade ");
		hql.append(" where cronogramaRota.cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :idAtividadeSistema");
		hql.append(" and ");
		hql.append(" cronogramaRota.cronogramaAtividadeFaturamento.cronogramaFaturamento.chavePrimaria = :idCronogramaFaturamento");
		hql.append(" order by ");
		hql.append(" cronogramaRota.rota.numeroRota ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idAtividadeSistema", idAtividadeSistema);
		query.setLong("idCronogramaFaturamento", idCronogramaFaturamento);

		Collection listaCronogramaRota = query.list();

		Collection<CronogramaRota> listaCronogramaRotaAux = null;

		listaCronogramaRotaAux = new ArrayList(new HashSet(listaCronogramaRota));

		return listaCronogramaRotaAux;

	}

	/**
	 * Remover cronograma rota por chave.
	 *
	 * @param cronogramaRota
	 *            the cronograma rota
	 * @param fisicamente
	 *            the fisicamente
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void removerCronogramaRotaPorChave(CronogramaRota cronogramaRota, boolean fisicamente) throws NegocioException {

		super.remover(cronogramaRota, getClasseEntidadeCronogramaRota(), fisicamente);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#quantidadeRotasPorGrupoFaturamento(java.lang.Long)
	 */
	@Override
	public int quantidadeRotasPorGrupoFaturamento(Long idGrupoFaturamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(rota.chavePrimaria) from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" rota ");
		hql.append(" where grupoFaturamento.chavePrimaria = ? ");
		hql.append(" and rota.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idGrupoFaturamento);

		Long total = (Long) query.uniqueResult();

		return total.intValue();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * validarQuantidadeCronogramasEmAberto(int,
	 * int)
	 */
	@Override
	public void validarQuantidadeCronogramasEmAberto(Integer quantidadeExistente, Integer quantidadeAdicional) throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Integer quantidadeMaximaCronogramasAbertos = Integer.valueOf((String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_QUANTIDADE_MAXIMA_CRONOGRAMAS_ABERTOS));

		if (quantidadeExistente == null) {
			throw new NegocioException(ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_EXISTENTES, true);
		}

		if (quantidadeAdicional == null) {
			throw new NegocioException(ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ADICIONAIS, true);
		} else {
			if (quantidadeAdicional == 0) {
				throw new NegocioException(ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ADICIONAIS_ZERO, true);
			}
		}

		if ((quantidadeExistente + quantidadeAdicional) > quantidadeMaximaCronogramasAbertos) {
			throw new NegocioException(ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ABERTOS_EXCEDIDO, quantidadeMaximaCronogramasAbertos);
		}

	}

	/*
	 * (non-Javadoc)
	 * @seebr.com.ggas.faturamento.fatura.
	 * ControladorFatura#
	 * listarFaturaItemPorTarifa(java.lang.Long)
	 */
	@Override
	public Collection<GrupoFaturamento> listarGrupoFaturamento(Long idPeriodicidade, Long idTipoLeitura) throws NegocioException {

		Collection<GrupoFaturamento> retorno = null;

		if (idPeriodicidade != null && idTipoLeitura != null) {
			StringBuilder hql = new StringBuilder();

			hql.append(" select grupoFaturamento from ");
			hql.append(getClasseEntidadeGrupoFaturamento().getSimpleName());
			hql.append(" grupoFaturamento ");
			hql.append(" inner join grupoFaturamento.periodicidade periodicidade ");
			hql.append(" inner join grupoFaturamento.tipoLeitura tipoLeitura ");
			hql.append(" where ");
			hql.append(" periodicidade.chavePrimaria = :idPeriodicidade ");
			hql.append(" and tipoLeitura.chavePrimaria = :idTipoLeitura ");

			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameter("idPeriodicidade", idPeriodicidade);
			query.setParameter("idTipoLeitura", idTipoLeitura);

			retorno = query.list();
		}

		if (retorno == null) {
			retorno = new ArrayList<GrupoFaturamento>();
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * obterPeriodicidadePorGrupoFaturamento(java
	 * .lang.Long)
	 */
	@Override
	public Periodicidade obterPeriodicidadePorGrupoFaturamento(Long idGrupoFaturamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select grupoFaturamento.periodicidade  from ");
		hql.append(getClasseEntidadeGrupoFaturamento().getSimpleName()).append(" grupoFaturamento ");
		hql.append(" where grupoFaturamento.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, idGrupoFaturamento);

		return (Periodicidade) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * consultarUltimoCronogramaRotaRealizado(
	 * java.lang.Long, java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public CronogramaRota consultarCronogramaRotaAtividade(Long idRota, Long idAtividadeSistema,
					Map<String, Integer> referenciaCicloAnterior) {

		Integer ciclo = referenciaCicloAnterior.get("ciclo");
		Integer anoMes = referenciaCicloAnterior.get("referencia");
		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota ");
		hql.append(" inner join fetch cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" inner join fetch cronogramaRota.rota rota ");
		hql.append(" where rota.chavePrimaria = :idRota");
		hql.append(" and ");
		hql.append(" cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :idAtividadeSistema");
		hql.append(" and ");
		hql.append(" cronogramaRota.anoMesReferencia = :idAnoMes");
		hql.append(" and ");
		hql.append(" cronogramaRota.numeroCiclo = :idCiclo");
		hql.append(" order by cronogramaRota.dataPrevista desc ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setMaxResults(1);

		query.setLong("idAtividadeSistema", idAtividadeSistema);
		query.setLong("idRota", idRota);
		query.setInteger("idAnoMes", anoMes);
		query.setInteger("idCiclo", ciclo);

		return (CronogramaRota) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #listarCiclos()
	 */
	@Override
	public Collection<Integer> listarCiclos() {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct periodicidade.quantidadeCiclo ");
		hql.append(" from ");
		hql.append(getClasseEntidadePeriodicidade().getSimpleName());
		hql.append(" periodicidade ");
		hql.append(" order by periodicidade.quantidadeCiclo ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * incluirRota(br.com.ggas.medicao
	 * .rota.Rota)
	 */
	@Override
	public Long inserirRota(Rota rota, Map<Long, Object> pontosConsumoAssociados) throws NegocioException, IllegalAccessException,
					InvocationTargetException, ConcorrenciaException {

		ControladorCronogramaFaturamento controladorCronogramaFaturamento = (ControladorCronogramaFaturamento) ServiceLocator
						.getInstancia().getControladorNegocio(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO);
		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		Long idRota = this.inserir(rota);

		if (pontosConsumoAssociados == null || pontosConsumoAssociados.isEmpty()) {
			rota.getPontosConsumo().clear();
		} else {
			rota.getPontosConsumo().clear();
			Set<Entry<Long, Object>> entrySet = pontosConsumoAssociados.entrySet();
			Iterator iterator = entrySet.iterator();
 			while (iterator.hasNext()) { 
				Map.Entry<Long, PontoConsumo> entryPontoConsumo = (Map.Entry<Long, PontoConsumo>) iterator.next();

				ArrayList<PontoConsumo> pontoConsumoLista = (ArrayList<PontoConsumo>) entryPontoConsumo.getValue();
				if(pontoConsumoLista != null){
				for (PontoConsumo pConsumo : pontoConsumoLista) {
					pConsumo.setRota(rota);
					controladorPontoConsumo.atualizar(pConsumo);
				}
				}
			} 
		}

		controladorCronogramaFaturamento.processarInclusaoRota(rota.getGrupoFaturamento().getChavePrimaria(), idRota);

		return idRota;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * obterCronogramasRota(br.com.procenge.ggas
	 * .medicao.rota.Rota, java.lang.Integer,
	 * java.lang.Integer)
	 */
	@Override
	public List<CronogramaRota> obterAtributosCronogramasRota(Rota rota, String ciclo, String referencia) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(
				" select cronogramaRota.cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria"
				+ " as cronogramaAtividadeFaturamento_atividadeSistema_chavePrimaria, ");
		hql.append(" cronogramaRota.dataPrevista as dataPrevista ");
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota ");
		hql.append(" left join cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento");
		hql.append(" inner join cronogramaAtividadeFaturamento.atividadeSistema atividadeSistema ");
		hql.append(" where cronogramaRota.rota.chavePrimaria = :cdRota ");
		hql.append(" and cronogramaRota.anoMesReferencia = :referencia ");
		hql.append(" and cronogramaRota.numeroCiclo = :ciclo ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(new GGASTransformer(getClasseEntidadeCronogramaRota(),
				super.getSessionFactory().getAllClassMetadata()));
		query.setLong("cdRota", rota.getChavePrimaria());
		query.setString("referencia", referencia);
		query.setString("ciclo", ciclo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * obterRotaPorPontoConsumo(java.lang.Long)
	 */
	@Override
	public Rota obterRotaPorPontoConsumo(Long idPontoConsumo) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo.rota ");
		hql.append(" from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where pontoConsumo.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, idPontoConsumo);

		return (Rota) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#listarRotasPorSetorComercialDaQuadra(java.lang.Long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Rota> listarRotasPorSetorComercialDaQuadra(Long idQuadra) throws NegocioException {

		ControladorQuadra controladorQuadra = (ControladorQuadra) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorQuadra.BEAN_ID_CONTROLADOR_QUADRA);

		Query query = null;
		Collection<Rota> retorno;

		Quadra quadra = (Quadra) controladorQuadra.obter(idQuadra);

		StringBuilder hql = new StringBuilder();
		hql.append("from RotaImpl rota where rota.setorComercial.chavePrimaria = :idSetorComercial");
		hql.append(" and rota.habilitado = true");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idSetorComercial", quadra.getSetorComercial().getChavePrimaria());

		retorno = query.list();

		if (retorno.isEmpty()) {

			hql = new StringBuilder();
			hql.append("from RotaImpl rota where rota.habilitado = true");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			retorno = query.list();

		}

		return retorno;

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#validarRota(java.lang.Long)
	 */
	@Override
	public void validarRota(Long rota) throws NegocioException {

		if (rota == null || rota == -1) {
			throw new NegocioException(ControladorRota.ERRO_ROTA_NAO_INFORMADA, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#atualizarAssociacaoPontoConsumoRota(java.util.List, java.util.List, java.lang.Long)
	 */
	@Override
	public void atualizarAssociacaoPontoConsumoRota(List<PontoConsumo> listaPontoConsumo,
					List<PontoConsumo> listaPontoConsumoRemoverAssociacao, Long chaveRota) throws ConcorrenciaException, NegocioException {

		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);

		Rota rota = (Rota) obter(chaveRota);

		if (listaPontoConsumo != null) {
			for (PontoConsumo pontoConsumo : listaPontoConsumo) {
				pontoConsumo.setRota(rota);
				controladorPontoConsumo.atualizar(pontoConsumo);
			}
		}

		if (listaPontoConsumoRemoverAssociacao != null) {
			for (PontoConsumo pontoConsumo : listaPontoConsumoRemoverAssociacao) {
				pontoConsumo.setRota(null);
				controladorPontoConsumo.atualizar(pontoConsumo);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#verificarCronogramaRotaRealizado(br.com.ggas.medicao.rota.Rota, java.lang.Integer,
	 * java.lang.Integer, java.lang.Long)
	 */
	@Override
	public boolean verificarCronogramaRotaRealizado(Rota rota, Integer anoMesReferencia, Integer numeroCiclo, Long idAtividadeSistema)
					throws NegocioException {

		// de todos os pontos de consumo ativos da rota, verificar se eles tem ocorrencia em leitura movimento
		// para ref e ciclo do cronograma atual

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append("select count(cronogramaRota) from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append("		cronogramaRota ");
		hql.append("		where cronogramaRota.rota.chavePrimaria = :idRota ");
		hql.append("		and cronogramaRota.dataRealizada is not  null ");
		hql.append("		and cronogramaRota.cronogramaAtividadeFaturamento.chavePrimaria in ( ");
		hql.append("				select cronogramaAtividadeFaturamento.chavePrimaria from ");
		hql.append(getClasseEntidadeCronogramaAtividadeFaturamento().getSimpleName());
		hql.append("				cronogramaAtividadeFaturamento ");
		hql.append("				where cronogramaAtividadeFaturamento.atividadeSistema.chavePrimaria = :idAtividadeSistema ");
		hql.append("				and cronogramaAtividadeFaturamento.cronogramaFaturamento.chavePrimaria in ( ");
		hql.append("						select cronogramaFaturamento.chavePrimaria from ");
		hql.append(getClasseEntidadeCronogramaFaturamento().getSimpleName());
		hql.append("						cronogramaFaturamento ");
		hql.append("						where cronogramaFaturamento.anoMesFaturamento = :anoMesReferencia ");
		hql.append("						and cronogramaFaturamento.numeroCiclo = :ciclo ");
		hql.append("						and cronogramaFaturamento.grupoFaturamento.chavePrimaria = :idGrupoFaturamento ");
		hql.append("				) ");
		hql.append("		) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idRota", rota.getChavePrimaria());
		query.setParameter("anoMesReferencia", anoMesReferencia);
		query.setParameter("ciclo", numeroCiclo);
		query.setParameter("idGrupoFaturamento", rota.getGrupoFaturamento().getChavePrimaria());
		query.setParameter("idAtividadeSistema", idAtividadeSistema);

		Long total = (Long) query.uniqueResult();

		return total > 0;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#listarGrupoFaturamentoDadosMedicao(br.com.ggas.constantesistema.ConstanteSistema)
	 */
	@Override
	public Collection<GrupoFaturamento> listarGrupoFaturamentoDadosMedicao(ConstanteSistema idTipoLeitura) throws NegocioException {

		Collection<GrupoFaturamento> retorno = null;
		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select grupoFaturamento from ");
		hql.append(getClasseEntidadeGrupoFaturamento().getSimpleName());
		hql.append(" grupoFaturamento ");
		hql.append(" inner join grupoFaturamento.tipoLeitura tipoLeitura ");
		hql.append(" where ");
		hql.append("  tipoLeitura.chavePrimaria <> :idTipoLeitura ");
		hql.append(" order by grupoFaturamento.descricao ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("idTipoLeitura", Long.valueOf(idTipoLeitura.getValor()));

		retorno = query.list();

		if (retorno == null) {
			retorno = new ArrayList<GrupoFaturamento>();
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#listarRotaPorLeiturista(br.com.ggas.medicao.leitura.Leiturista)
	 */
	@Override
	public Collection<Rota> listarRotaPorLeiturista(Leiturista leiturista) {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota ");
		hql.append(" inner join fetch rota.leiturista leiturista ");
		hql.append(" where ");
		hql.append(" leiturista.chavePrimaria = :idLeiturista ");
		hql.append(" order by rota.numeroRota ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setParameter("idLeiturista", leiturista.getChavePrimaria());

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#obterHistoricoRotaMaisRecente(java.util.Map)
	 */
	@Override
	public Collection<RotaHistorico> obterHistoricoRotaMaisRecente(Map<String, Object> filtro, Boolean data) {

		Long chaveRota = null;
		Long chaveCronograma = null;
		Date dataCadastro = null;
		if(filtro != null){
			chaveRota = (Long) filtro.get("chaveRota");
			chaveCronograma = (Long) filtro.get("chaveCronograma");
			dataCadastro = (Date) filtro.get("dataCadastro");
		}

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeRotahistorico().getSimpleName());
		hql.append(" rotaHistorico ");
		hql.append(" inner join fetch rotaHistorico.leiturista leiturista ");
		hql.append(" inner join fetch leiturista.funcionario funcionario ");
		hql.append(" where ");
		if (chaveRota != null && chaveRota > 0) {
			hql.append(" rotaHistorico.rota.chavePrimaria = ?  ");
		}
		if(chaveCronograma != null && chaveCronograma > 0){
			hql.append(" and rotaHistorico.cronogramaRota.chavePrimaria = ?  ");
		}
		if(data != null && data && dataCadastro != null){
			hql.append(" and rotaHistorico.dataCadastro < :dataCadastro  ");
		}
		if(data == null && dataCadastro != null){
			hql.append(" and rotaHistorico.dataCadastro <= :dataCadastro  ");
		}
		hql.append(" order by rotaHistorico.dataCadastro desc  ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (chaveRota != null && chaveRota > 0) {
			query.setLong(0, chaveRota);
		}
		if(chaveCronograma != null && chaveCronograma > 0){
			query.setLong(1, chaveCronograma);
		}
		if(dataCadastro != null){
			query.setTimestamp("dataCadastro", dataCadastro);
		}

		return (Collection<RotaHistorico>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#listarRotahistoricoPorCronogramaRota(java.lang.Long)
	 */
	@Override
	public Collection<RotaHistorico> listarRotahistoricoPorRota(Long chaveRota) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select rotaHistorico from ");
		hql.append(getClasseEntidadeRotahistorico().getSimpleName());
		hql.append(" rotaHistorico ");
		hql.append(" inner join fetch rotaHistorico.rota rota ");
		hql.append(" inner join fetch rota.leiturista leiturista ");
		hql.append(" inner join fetch leiturista.funcionario funcionario ");
		hql.append(" inner join fetch rota.leituristaSuplente leituristaSuplente ");
		hql.append(" inner join fetch leituristaSuplente.funcionario funcionarioSuplente ");
		hql.append(" where ");
		hql.append(" rotaHistorico.rota.chavePrimaria = ? ");
		hql.append(" order by rotaHistorico.dataCadastro desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong(0, chaveRota);

		return (Collection<RotaHistorico>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#listarRotahistorico()
	 */
	@Override
	public Collection<RotaHistorico> listarRotahistorico() {

		StringBuilder hql = new StringBuilder();

		hql.append(" from ");
		hql.append(getClasseEntidadeRotahistorico().getSimpleName());
		hql.append(" rotaHistorico ");
		hql.append(" inner join fetch rotaHistorico.leiturista leiturista ");
		hql.append(" inner join fetch leiturista.funcionario funcionario ");
		hql.append(" order by rotaHistorico.ultimaAlteracao desc and ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		return (Collection<RotaHistorico>) query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#salvarRotaHistorico(java.util.Collection)
	 */
	@Override
	public boolean salvarRotaHistorico(Collection<RotaHistorico> listaRotaHistorico) throws NegocioException {

		for (RotaHistorico rotaHistorico : listaRotaHistorico) {
			this.inserir(rotaHistorico);
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#obterCronogramaRota(long)
	 */
	@Override
	public CronogramaRota obterCronogramaRota(long chavePrimaria) {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName());
		hql.append(" cronogramaRota ");
		hql.append(" where cronogramaRota.chavePrimaria = :chavePrimaria");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("chavePrimaria", chavePrimaria);

		return (CronogramaRota) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.ControladorRota#atualizarRotaHistorico(br.com.ggas.medicao.rota.impl.RotaHistorico)
	 */
	@Override
	public boolean atualizarRotaHistorico(RotaHistorico rotaHistorico) throws NegocioException, ConcorrenciaException {

		this.atualizar(rotaHistorico);

		return true;
	}

	/**
	 * Obtém o número ciclo e o ano mês de referência do grupo de faturamento da {@code rota}
	 * especificada no parâmetro.
	 * @param rota
	 * @param grupoFaturamento
	 * @return mapa de mês referência por número ciclo
	 **/
	@Override
	public Map<String, Integer> obterReferenciaCicloAtual(Rota rota) throws NegocioException {

		Map<String, Integer> referenciaCiclo = new HashMap<String, Integer>();
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		if (rota != null && rota.getGrupoFaturamento() != null) {
			referenciaCiclo.put("ciclo", rota.getGrupoFaturamento().getNumeroCiclo());
			referenciaCiclo.put("referencia", rota.getGrupoFaturamento().getAnoMesReferencia());
		} else {
			referenciaCiclo.put("ciclo", 1);
			referenciaCiclo.put("referencia", Integer.valueOf((String) controladorParametroSistema
							.obterValorDoParametroPorCodigo(LeituraMovimento.REFERENCIA_FATURAMENTO)));
		}
		return referenciaCiclo;
	}

	/**
	 * Agrupa as rotas dos pontos de consumo
	 * especificados no parâmetro.
	 *
	 * @param pontosConsumo
	 * @return rotas dos pontos de consumo
	 **/
	@Override
	public List<Rota> agruparRotasDePontosConsumo(Collection<PontoConsumo> pontosConsumo) {
		List<Rota> rotas = new ArrayList<>();
		for (PontoConsumo pontoConsumo : pontosConsumo) {
			if (!rotas.contains(pontoConsumo.getRota())) {
				rotas.add(pontoConsumo.getRota());
			}
		}
		return rotas;
	}


	/**
	 * Obtém uma rota por grupo de faturamento e pelo atributo
	 * {@code numeroRota}.
	 *
	 * @param numeroRota
	 *            - {@link String}
	 * @param grupoFaturamento
	 *            - {@link GrupoFaturamento}
	 * @return {@link Rota}
	 */
	@Override
	public Rota obterRotaDeGrupoFaturamentoPorNumero(String numeroRota, GrupoFaturamento grupoFaturamento) {

		Criteria criteria = getCriteria();
		Rota rota = null;
		if (StringUtils.isNotBlank(numeroRota) && Util.isAllNotNull(grupoFaturamento)) {
			criteria.add(Restrictions.eq(NUMERO_ROTA, numeroRota));
			criteria.add(Restrictions.eq(FATURAMENTO_GRUPO, grupoFaturamento));
			rota = (Rota) criteria.uniqueResult();
		}

		return rota;
	}

	/**
	 * Obtém um grupo de faturamento pelo atributo {@code descricao}.
	 *
	 * @param descricaoGrupo
	 *            - {@link String}
	 * @return {@link GrupoFaturamento}
	 */
	@Override
	public GrupoFaturamento obterGrupoPorDescricao(String descricaoGrupo) {

		Criteria criteria = createCriteria(getClasseEntidadeGrupoFaturamento());
		GrupoFaturamento grupoFaturamento = null;

		if (StringUtils.isNotBlank(descricaoGrupo)) {
			criteria.add(Restrictions.eq(TabelaAuxiliar.ATRIBUTO_DESCRICAO, descricaoGrupo));
			grupoFaturamento = (GrupoFaturamento) criteria.uniqueResult();
		}

		return grupoFaturamento;
	}

	/**
	 * Adiciona cronograma de rota como um valor para um rota como chave.
	 *
	 * @param cronogramaPorRota - {@link Map}
	 * @param cronogramaRota - {@link CronogramaRota}
	 * @param rota - {@link Rota}
	 */
	@Override
	public void mapearCronogramaRota(Map<Rota, CronogramaRota> cronogramaPorRota,
			CronogramaRota cronogramaRota, Rota rota) {

		cronogramaPorRota.put(rota, cronogramaRota);
	}

	private static void escreverAposDuasLinha(StringBuilder logProcessamento, String texto) {
		if (logProcessamento != null) {
			logProcessamento.append("\n");
			logProcessamento.append("\n");
			logProcessamento.append(texto);
		}
	}

	private static void escreverNovaLinha(StringBuilder logProcessamento, String texto) {
		if (logProcessamento != null) {
			logProcessamento.append("\n");
			logProcessamento.append(texto);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #consultarRota(java.util.Map)
	 */
	@Override
	public Collection<Long> consultarChaveRota(Map<String, Object> filtro) {

		Criteria criteria = createCriteria(Rota.class);
		criteria.setProjection(Projections.property("chavePrimaria"));
		
		filtroRotas(filtro, criteria);
		
		return criteria.list();
	}

	private void filtroRotas(Map<String, Object> filtro, Criteria criteria) {
	
		if (filtro != null) {
			Long[] chavesPrimarias = (Long[]) filtro.get("chavesPrimarias");
			if (chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
			}

			Long chavePrimaria = (Long) filtro.get("chavePrimaria");
			if (chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq("chavePrimaria", chavePrimaria));
			}

			Long idTipoLeitura = (Long) filtro.get("idTipoLeitura");
			if ((idTipoLeitura != null) && (idTipoLeitura > 0)) {
				criteria.createAlias("tipoLeitura", "tipoLeitura");
				criteria.add(Restrictions.eq("tipoLeitura.chavePrimaria", idTipoLeitura));
			} else {
				criteria.setFetchMode("tipoLeitura", FetchMode.JOIN);
			}

			Long idEmpresa = (Long) filtro.get("idEmpresa");
			if ((idEmpresa != null) && (idEmpresa > 0)) {
				criteria.createAlias("empresa", "empresa");
				criteria.add(Restrictions.eq("empresa.chavePrimaria", idEmpresa));
			} else {
				criteria.setFetchMode("empresa", FetchMode.JOIN);
			}

			Long idLeiturista = (Long) filtro.get("idLeiturista");
			if ((idLeiturista != null) && (idLeiturista > 0)) {
				criteria.createAlias("leiturista", "leiturista");
				criteria.add(Restrictions.eq("leiturista.chavePrimaria", idLeiturista));
			} else {
				criteria.setFetchMode("leiturista", FetchMode.JOIN);
			}

			Long idPeriodicidade = (Long) filtro.get("idPeriodicidade");
			if ((idPeriodicidade != null) && (idPeriodicidade > 0)) {
				criteria.createAlias("periodicidade", "periodicidade");
				criteria.add(Restrictions.eq("periodicidade.chavePrimaria", idPeriodicidade));
			} else {
				criteria.setFetchMode("periodicidade", FetchMode.JOIN);
			}

			Long idSetorComercial = (Long) filtro.get("idSetorComercial");
			if ((idSetorComercial != null) && (idSetorComercial > 0)) {
				criteria.createAlias("setorComercial", "setorComercial");
				criteria.add(Restrictions.eq("setorComercial.chavePrimaria", idSetorComercial));
			} else {
				criteria.setFetchMode("setorComercial", FetchMode.JOIN);
			}

			String numeroRota = (String) filtro.get(NUMERO_ROTA);
			if (!StringUtils.isEmpty(numeroRota)) {
				criteria.add(Restrictions.like(NUMERO_ROTA, numeroRota, MatchMode.ANYWHERE));
			}

			Long idTipoRota = (Long) filtro.get("idTipoRota");
			if ((idTipoRota != null) && (idTipoRota > 0)) {
				criteria.createAlias("tipoRota", "tipoRota");
				criteria.add(Restrictions.eq("tipoRota.chavePrimaria", idTipoRota));
			} else {
				criteria.setFetchMode("tipoRota", FetchMode.JOIN);
			}

			Long idGrupoFaturamento = (Long) filtro.get("idGrupoFaturamento");
			if ((idGrupoFaturamento != null) && (idGrupoFaturamento > 0)) {
				criteria.createAlias("grupoFaturamento", "grupoFaturamento");
				criteria.add(Restrictions.eq("grupoFaturamento.chavePrimaria", idGrupoFaturamento));
			} else {
				criteria.setFetchMode("grupoFaturamento", FetchMode.JOIN);
			}

			Boolean habilitado = (Boolean) filtro.get(HABILITADO);
			if (habilitado != null) {
				criteria.add(Restrictions.eq(HABILITADO, habilitado));
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #obterRotasCronogramaFaturamentoNew(GrupoFaturamento)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Rota> obterNumerosRotasCronogramaFaturamento(GrupoFaturamento grupoFaturamento) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select rota.chavePrimaria as chavePrimaria, ");
		hql.append(" rota.numeroRota as numeroRota ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota ");
		hql.append(" inner join rota.periodicidade periodicidade");
		hql.append(" left join rota.leiturista leiturista ");
		hql.append(" where rota.grupoFaturamento.chavePrimaria = :chaveGrupo ");
		hql.append(" and rota.chavePrimaria in ");
		hql.append(" ( ");
		hql.append(" select distinct cronogramaRota.rota.chavePrimaria ");
		hql.append(" from ");
		hql.append(getClasseEntidadeCronogramaRota().getSimpleName()).append(" cronogramaRota ");
		hql.append(" inner join cronogramaRota.cronogramaAtividadeFaturamento cronogramaAtividadeFaturamento ");
		hql.append(" inner join cronogramaAtividadeFaturamento.cronogramaFaturamento cronogramaFaturamento ");
		hql.append(" where cronogramaFaturamento.anoMesFaturamento = :anoMes ");
		hql.append(" and cronogramaFaturamento.numeroCiclo = :ciclo ");
		hql.append(" and cronogramaFaturamento.grupoFaturamento.chavePrimaria = :idGrupofaturamento ");
		hql.append(" ) ");
		hql.append(" order by rota.numeroRota ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidade()));

		query.setLong("chaveGrupo", grupoFaturamento.getChavePrimaria());
		query.setLong("anoMes", grupoFaturamento.getAnoMesReferencia());
		query.setLong("ciclo", grupoFaturamento.getNumeroCiclo());
		query.setLong("idGrupofaturamento", grupoFaturamento.getChavePrimaria());
		
		return query.list();
	}
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota
	 * #obterRotasCronogramaFaturamentoNew(GrupoFaturamento)
	 */
	@Override
	public GrupoFaturamento obterDescricaoGrupoFaturamento(Long chave) {
		StringBuilder hql = new StringBuilder();
		hql.append(" select chavePrimaria as chavePrimaria, descricaoAbreviada as descricaoAbreviada ");
		hql.append(" from ");
		hql.append(getClasseEntidadeGrupoFaturamento().getSimpleName());
		hql.append(" grupoFaturamento ");
		hql.append(" where grupoFaturamento.chavePrimaria = ?");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setResultTransformer(Transformers.aliasToBean(getClasseEntidadeGrupoFaturamento()));
		query.setParameter(0, chave);
		return (GrupoFaturamento) query.uniqueResult();
	}
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * listarRotasRemanejamento(java.lang.Long)
	 */
	@Override
	public Rota buscarNumeroRota(Long chavePrimaria) throws NegocioException {

		Rota rota = null;

		rota = (Rota) super.obter(chavePrimaria, getClasseEntidade());

		return rota;
	}
	
	@Override
	public void atualizarCronogamaPorRota(Rota rota, long codigoAtividadeCronograma) throws NegocioException {
		try {
			ControladorProcesso controladorProcesso = (ControladorProcesso) ServiceLocator.getInstancia().getControladorNegocio(
					ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO);
			Long[] chaveRota = {rota.getChavePrimaria()}; 
			
			AtividadeSistema atividadeSistema = controladorProcesso.obterAtividadeSistema(codigoAtividadeCronograma);
			
			Map<Rota, Collection<CronogramaRota>> mapaCronogramaRotaPorRota = this
					.obterMapaCronogramaRotaPorRotas(chaveRota, atividadeSistema);

			this.atualizarCronogramaPorRota(mapaCronogramaRotaPorRota, atividadeSistema, null, null);
		} catch (NegocioException e) {
			LOG.error("Erroao atualizar o cronograma", e);
			throw new NegocioException(e);
		}

	}
	
	@Override
	public Rota obterPorId(Long idRota) throws GGASException {
		return (Rota) super.obter(idRota, getClasseEntidade());
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.ControladorRota#
	 * listarRotasPorGrupoFaturamento(java.lang
	 * .Long)
	 */
	@Override
	public Collection<Rota> listarRotasPorTipoLeitura(String tipoLeitura) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" rota ");
		hql.append(" inner join fetch rota.periodicidade periodicidade");
		hql.append(" inner join fetch rota.tipoLeitura tipoLeitura ");
		hql.append(" where tipoLeitura.descricao = :tipoLeitura ");
		hql.append(" order by rota.numeroRota ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setString("tipoLeitura", tipoLeitura);

		return query.list();
	}
}
