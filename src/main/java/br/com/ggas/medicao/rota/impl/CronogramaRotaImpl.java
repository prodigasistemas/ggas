/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.rota.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe responsável por implementar os métodos relacionados ao Cronograma de Rota
 *
 */
public class CronogramaRotaImpl extends EntidadeNegocioImpl implements CronogramaRota {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6837463226826034694L;

	private Date dataPrevista;

	private Date dataRealizada;

	private Integer anoMesReferencia;

	private Integer numeroCiclo;

	private Rota rota;

	private CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento
	 * #getAnoMesFaturamentoFormatado()
	 */
	@Override
	public String getDataRealizadaFormatada() {

		if(dataRealizada != null) {
			return Util.converterDataParaStringSemHora(dataRealizada, Constantes.FORMATO_DATA_BR);
		} else {
			return String.valueOf("");
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.faturamento.cronograma.impl
	 * .CronogramaFaturamento
	 * #getAnoMesFaturamentoFormatado()
	 */
	@Override
	public String getDataPrevistaFormatada() {

		if(dataPrevista != null) {
			return Util.converterDataParaStringSemHora(dataPrevista, Constantes.FORMATO_DATA_BR);
		} else {
			return String.valueOf("");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.rota.CronogramaRota #getDataPrevista()
	 */
	@Override
	public Date getDataPrevista() {
		Date data = null;
		if (this.dataPrevista != null) {
			data = (Date) dataPrevista.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.rota.CronogramaRota
	 * #setDataPrevista(java.util.Date)
	 */
	@Override
	public void setDataPrevista(Date dataPrevista) {
		if (dataPrevista != null) {
			this.dataPrevista = (Date) dataPrevista.clone();
		} else {
			this.dataPrevista = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.rota.CronogramaRota #getDataRealizada()
	 */
	@Override
	public Date getDataRealizada() {
		Date data = null;
		if (this.dataRealizada != null) {
			data = (Date) dataRealizada.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.rota.CronogramaRota
	 * #setDataRealizada(java.util.Date)
	 */
	@Override
	public void setDataRealizada(Date dataRealizada) {
		if (dataRealizada != null) {
			this.dataRealizada = (Date) dataRealizada.clone();
		} else {
			this.dataRealizada = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #getRota()
	 */
	@Override
	public Rota getRota() {

		return rota;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #setRota(br.com.ggas.medicao.rota.Rota)
	 */
	@Override
	public void setRota(Rota rota) {

		this.rota = rota;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #getDataGeracao()
	 */

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #setDataGeracao(java.util.Date)
	 */

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #getDataRetorno()
	 */

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #setDataRetorno(java.util.Date)
	 */

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #getAnoMesReferencia()
	 */
	@Override
	public Integer getAnoMesReferencia() {

		return anoMesReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #setAnoMesReferencia(java.lang.Integer)
	 */
	@Override
	public void setAnoMesReferencia(Integer anoMesReferencia) {

		this.anoMesReferencia = anoMesReferencia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #getNumeroCiclo()
	 */
	@Override
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #setNumeroCiclo(java.lang.Integer)
	 */
	@Override
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #getCronogramaAtividadeFaturamento()
	 */
	@Override
	public CronogramaAtividadeFaturamento getCronogramaAtividadeFaturamento() {

		return cronogramaAtividadeFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.CronogramaRota
	 * #setCronogramaAtividadeFaturamento
	 * (br.com.ggas.faturamento.cronograma.
	 * CronogramaAtividadeFaturamento)
	 */
	@Override
	public void setCronogramaAtividadeFaturamento(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento) {

		this.cronogramaAtividadeFaturamento = cronogramaAtividadeFaturamento;
	}

}
