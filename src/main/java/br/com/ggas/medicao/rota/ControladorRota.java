/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.rota;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.arrecadacao.ArrecadadorCarteiraCobranca;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaFaturamento;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.rota.impl.RotaHistorico;
/**
 * Classe responsável pela assinatura dos métodos relacionados ao Controlador de Rota
 *
 */
public interface ControladorRota extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_ROTA = "controladorRota";

	String ERRO_NEGOCIO_ROTA_EXISTENTE = "ERRO_NEGOCIO_ROTA_EXISTENTE";

	String ERRO_NEGOCIO_ROTA_INATIVA = "ERRO_NEGOCIO_ROTA_INATIVA";

	String ERRO_NEGOCIO_DATA_MENOR_ATUAL = "ERRO_NEGOCIO_DATA_MENOR_ATUAL";

	String ERRO_NEGOCIO_ROTAS_NAO_REMANEJAVEIS = "ERRO_NEGOCIO_ROTAS_NAO_REMANEJAVEIS";

	String ERRO_NEGOCIO_ROTAS_NAO_REMANEJADAS = "ERRO_NEGOCIO_ROTAS_NAO_REMANEJADAS";

	String ERRO_NEGOCIO_ROTA_EM_PROCESSO_LEITURA = "ERRO_NEGOCIO_ROTA_EM_PROCESSO_LEITURA";

	String ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO = "ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO";

	String ERRO_NEGOCIO_ANO_MES_ANTERIOR_REFERENCIA_FATURAMENTO = "ERRO_NEGOCIO_ANO_MES_ANTERIOR_REFERENCIA_FATURAMENTO";

	String ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ABERTOS_INFERIOR_MINIMO = "ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ABERTOS_INFERIOR_MINIMO";

	String ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ABERTOS_EXCEDIDO = "ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ABERTOS_EXCEDIDO";

	String ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ADICIONAIS_ZERO = "ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ADICIONAIS_ZERO";

	String ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ADICIONAIS = "ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_ADICIONAIS";

	String ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_EXISTENTES = "ERRO_NEGOCIO_QUANTIDADE_CRONOGRAMAS_EXISTENTES";

	String ERRO_NEGOCIO_NUMERO_CICLOS_INVALIDOS = "ERRO_NEGOCIO_NUMERO_CICLOS_INVALIDOS";

	String ERRO_ROTA_NAO_INFORMADA = "ERRO_ROTA_NAO_INFORMADA";

	/**
	 * Metodo responsável por criar um entidade de
	 * negocio do tipo Rota Cronograma.
	 * 
	 * @return Uma Entidade de Negócio
	 */
	EntidadeNegocio criarCronogramaRota();

	/**
	 * Método responsável por consultar os tipos
	 * de leituras das rotas cadastrados no
	 * sistema.
	 * 
	 * @return Uma coleção de tipo de leitura
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TipoLeitura> listarTiposLeitura() throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * leituristas das rotas cadastrados no
	 * sistema.
	 * 
	 * @return Uma coleção de leituristas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Leiturista> listarLeituristas() throws NegocioException;

	/**
	 * Método responsável por consultar as
	 * periodicidades das rotas cadastrados no
	 * sistema.
	 * 
	 * @return Uma coleção de periodicidades
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Periodicidade> listarPeriodicidades() throws NegocioException;

	/**
	 * Método responsável por consultar tipos das
	 * rotas cadastrados no sistema.
	 * 
	 * @return Uma coleção de tipos das rotas
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<TipoRota> listarTiposRotas() throws NegocioException;

	/**
	 * Método responsável por consultar grupos de
	 * faturamento das rotas cadastrados no
	 * sistema.
	 * 
	 * @return Uma coleção de grupos de
	 *         faturamento das rotas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<GrupoFaturamento> listarGruposFaturamentoRotas() throws NegocioException;

	/**
	 * Método responsável por consultar as rotas
	 * pelo filtro informado.
	 * 
	 * @param filtro
	 *            O filtro com os parâmetros para
	 *            a pesquisa.
	 * @return Uma coleção de rotas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Rota> consultarRota(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Método responsável por obter um tipo
	 * leitura pela chave passada por parâmetro.
	 * 
	 * @param chave
	 *            the chave
	 * @return TipoLeitura
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoLeitura obterTipoLeitura(Long chave) throws NegocioException;

	/**
	 * Método responsável por obter um grupo
	 * faturamento pela chave passada por
	 * parâmetro.
	 * 
	 * @param chave
	 *            the chave
	 * @return GrupoFaturamento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	GrupoFaturamento obterGrupoFaturamento(Long chave) throws NegocioException;

	/**
	 * Método responsável por obter um leiturista
	 * pela chave passada por parâmetro.
	 * 
	 * @param chave
	 *            the obterLeiturista
	 * @return Leiturista
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Leiturista obterLeiturista(Long chave) throws NegocioException;

	/**
	 * Método responsável por obter a
	 * periodicidade da rota pela chave passada
	 * por parâmetro.
	 * 
	 * @param chave
	 *            the chave
	 * @return Periodicidade
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Periodicidade obterPeriodicidade(Long chave) throws NegocioException;

	/**
	 * Método responsável por obter o tipo da rota
	 * pela chave passada por parâmetro.
	 * 
	 * @param chave
	 *            the chave
	 * @return TipoRota
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	TipoRota obterTipoRota(Long chave) throws NegocioException;

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a remoção de
	 * rotas.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primárias das rotas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarRemoverRotas(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por remover a(s) rota(s)
	 * selecionada(s).
	 * 
	 * @param chavesPrimarias
	 *            A(s) chave(s) primária(s) da(s)
	 *            rota(s) selecionada(s).
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void removerRotas(Long[] chavesPrimarias) throws NegocioException;

	/**
	 * Método responsável por listar as rotas para
	 * geração de movimentos de
	 * leitura por setor comercial(opcional).
	 * 
	 * @param idSetorComercial
	 *            A chave primária de um setor
	 *            comercial.
	 * @return coleção de rotas.
	 */
	Collection<Rota> listarRotasGeracaoDadosLeituraPorSetorComercial(Long idSetorComercial);

	/**
	 * Método responsável por listar todas as
	 * Rotas cadastradas no Sistema
	 * associadas a um Grupo Faturamento e uma
	 * Periodicidade informada.
	 * 
	 * @param idGrupoFaturamento
	 *            Chave primária do Grupo
	 *            Faturamento.
	 * @param idPeriodicidade
	 *            Chave primária da Periodicidade.
	 * @param dataPrevista
	 *            the data prevista
	 * @return Uma coleção de Rotas.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Rota> listarRotasGeracaoDadosLeitura(Long idGrupoFaturamento, Long idPeriodicidade, Date dataPrevista)
					throws NegocioException;

	/**
	 * Método reponsável por listar todas as Rotas
	 * que possuem cronograma em atraso.
	 * 
	 * @param rotasFiltro
	 *            the rotas filtro
	 * @return Uma coleção de rotas em atraso.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Rota> listarRotasGeracaoDadosLeituraCronogramaEmAtraso(Collection<Rota> rotasFiltro) throws NegocioException;

	/**
	 * Método reponsável por listar todas as Rotas
	 * que não possuem cronograma.
	 * 
	 * @param idGrupoFaturamento
	 *            the id grupo faturamento
	 * @param idPeriodicidade
	 *            the id periodicidade
	 * @param dataPrevista
	 *            the data prevista
	 * @return Uma coleção de rotas em atraso.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<Rota> listarRotasGeracaoDadosLeituraSemCronograma(Long idGrupoFaturamento, Long idPeriodicidade, Date dataPrevista)
					throws NegocioException;

	/**
	 * Método responsável por validar um
	 * cronograma de rota a ser cadastrada.
	 * 
	 * @param cronogramaRota
	 *            Um cronograma de rota.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarCronogramaRota(CronogramaRota cronogramaRota) throws NegocioException;

	/**
	 * Método responsável por listar os
	 * cronogramas que já foram realizadas de
	 * uma rota.
	 * 
	 * @param idRota
	 *            Chave primária da rota.
	 * @return coleção de cronogramas de rota já
	 *         realizadas.
	 */
	Collection<CronogramaRota> listarCronogramasRotaRealizadasPorRota(long idRota);

	/**
	 * Método responsável por validar se o
	 * cronograma está em alerta. O alerta
	 * de cronogroma informa que existe outro
	 * cronograma com a mesma data
	 * prevista e mesmo leiturista para uma rota
	 * diferente.
	 * 
	 * @param cronogramaRota
	 *            Cronograma de rota a ser
	 *            válidado
	 * @param idRota
	 *            Chave primária da rota.
	 * @return true, caso exista.
	 */
	boolean validarCronogramaRotaEmAlerta(CronogramaRota cronogramaRota, long idRota);

	/**
	 * Método responsável por listar as rotas para
	 * remanejamento de pontos de
	 * consumo. A rota que está sendo mantida não
	 * será retornada e só serão listadas
	 * as rotas com a mesma periodicidade da rota
	 * passada por parâmetro.
	 * 
	 * @param rota
	 *            the rota
	 * @return Uma coleção de Rotas.
	 */
	Collection<Rota> listarRotasRemanejamento(Rota rota);

	/**
	 * Método responsável por listar as rotas para
	 * remanejamento de pontos de
	 * consumo. A rota que está sendo mantida não
	 * será retornada e só serão listadas
	 * as rotas com a periodicidade passada por
	 * parâmetro.
	 * 
	 * @param idRotaMantida
	 *            the id rota mantida
	 * @param idPeriodicidade
	 *            the id periodicidade
	 * @return Uma coleção de Rotas.
	 */
	Collection<Rota> listarRotasRemanejamentoPeriodicidade(long idRotaMantida, long idPeriodicidade);

	/**
	 * Método responsável por atualizar uma
	 * entidade.
	 * 
	 * @param rota
	 *            Rota a ser alterada no sistema.
	 * @param pontosConsumoRemanejados
	 *            Pontos de consumo remanejados.
	 * @return chavePrimeria Chave da rota.
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de
	 *             negócio
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 */
	void atualizar(Rota rota, Collection<PontoConsumo> pontosConsumoRemanejados) throws ConcorrenciaException, NegocioException,
					IllegalAccessException, InvocationTargetException;

	/**
	 * Método responsavel por atualizar a data de
	 * geração do CronogramaRota das rotas dos
	 * pontos de
	 * consumo.
	 * 
	 * @param pontosConsumo
	 *            the pontos consumo
	 * @param atividadeSistema
	 *            the atividade sistema
	 * @param listRota
	 *            the list rota
	 * @param logProcesso
	 *            the log processo
	 * @throws NegocioException
	 *             the negocio exception
	 */

	void atualizarCronogramaPorPontosConsumo(Collection<PontoConsumo> pontosConsumo, long atividadeSistema, Collection<Rota> listRota,
					StringBuilder logProcesso) throws NegocioException;

	/**
	 * Método responsável por listar as rotas
	 * associadas a um grupo de
	 * faturamento informado.
	 * 
	 * @param chavePrimariaGrupoFaturamento
	 *            Chave primária do grupo de
	 *            faturamento.
	 * @return Uma coleção de Rotas.
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	Collection<Rota> listarRotasPorGrupoFaturamento(Long chavePrimariaGrupoFaturamento) throws NegocioException;

	/**
	 * Método responsável por obter o cronograma
	 * com referencia e ciclo atual.
	 * 
	 * @param rota
	 *            Uma rota
	 * @return Um cronograma
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	CronogramaRota obterCronogramaRotaReferenciaCicloAtual(Rota rota) throws NegocioException;

	/**
	 * Método responsável por obter o ultimo
	 * cronograma da rota.
	 * 
	 * @param rota
	 *            Uma rota
	 * @return Um cronograma
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio
	 */
	CronogramaRota obterUltimoCronogramaDaRota(Rota rota) throws NegocioException;

	/**
	 * Metodo para obter o cronograma atual
	 * (cronograma mais proximo depois de hoje).
	 * 
	 * @param rota
	 *            the rota
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param realizado
	 *            the realizado
	 * @param verificaIniciada
	 *            the verifica iniciada
	 * @return Um cronograma
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CronogramaRota obterCronogramaRotaAtual(Rota rota, Long chavePrimaria, boolean realizado, boolean verificaIniciada)
					throws NegocioException;

	/**
	 * Obtém o cronograma da rota atual que ainda não foi faturado (que não possui leituras movimento com status de leitura 4 - processado
	 * Este cronograma é útil para o processo de gerar dados
	 * @param rota rota cujo cronograma referencia
	 * @return retorna o {@link CronogramaRota} referente
	 */
	CronogramaRota obterCronogramaRotaAtualNaoFaturado(Rota rota);

	/**
	 * Obtém a lista de rotas que estão em Alerta
	 * em relação a um determinado cronograma/Rota.
	 * 
	 * @param cronogramaRota
	 *            [obrigatorio]
	 * @return the collection
	 */
	Collection<Rota> listarRotasCronogramaEmAlerta(CronogramaRota cronogramaRota);

	/**
	 * Método responsável por obter a data
	 * prevista do roteiro baseado
	 * no cronograma da primeira rota adicionada
	 * ao roteiro.
	 * 
	 * @param idRota
	 *            Chave primária da rota.
	 * @param atividadeSistema
	 *            the atividade sistema
	 * @return data prevista
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	Date obterDataPrevistaDoCronograma(Long idRota, AtividadeSistema atividadeSistema) throws NegocioException;

	/**
	 * Método responsável por validar uma coleção
	 * de cronogramas de rotas a ser
	 * mantida.
	 * 
	 * @param listaCronogramaRota
	 *            Uma coleção de cronogramas de
	 *            rotas.
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	void validarCronogramasRotaManutencao(Collection<CronogramaRota> listaCronogramaRota) throws NegocioException;

	/**
	 * Método responsável por consulta o último
	 * cronograma realizado da rota
	 * informada.
	 * 
	 * @param chavePrimariaRota
	 *            Chave primária da rota.
	 * @return Um cronograma de rota.
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	CronogramaRota obterUltimoCronogramaRotaRealizado(long chavePrimariaRota) throws NegocioException;

	/**
	 * Método responsável por validar a quantidade
	 * de cronogramas em aberto de uma rota.
	 * 
	 * @param quantidadeExistente
	 *            Quantidade de cronogramas em
	 *            aberto já existentes.
	 * @param quantidadeAdicional
	 *            Quantidade de cronogramas em
	 *            aberto a serem adicionados.
	 * @throws NegocioException
	 *             Caso ocorra alguma violação nas
	 *             regras de negócio.
	 */
	void validarQuantidadeCronogramasEmAberto(Integer quantidadeExistente, Integer quantidadeAdicional) throws NegocioException;

	/**
	 * Método responsável por validar a alteração
	 * de rotas inativas.
	 * 
	 * @param rota
	 *            the rota
	 * @param idPontosConsumoAssociados
	 *            the id pontos consumo associados
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarAlteracaoRotaInativa(Rota rota, Long[] idPontosConsumoAssociados) throws NegocioException;

	/**
	 * Buscar rota.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the rota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Rota buscarRota(Long chavePrimaria) throws NegocioException;

	/**
	 * Consultar rota por grupo faturamento.
	 * 
	 * @param grupoFaturamento
	 *            the grupo faturamento
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Rota> consultarRotaPorGrupoFaturamento(GrupoFaturamento grupoFaturamento) throws NegocioException;

	/**
	 * Método responsável por obter cronograma do
	 * faturamento atual.
	 * 
	 * @param grupoFaturamento
	 *            the grupo faturamento
	 * @param atividadeSistema
	 *            the atividade sistema
	 * @return the cronograma atividade faturamento
	 * @throws NegocioException
	 *             the negocio exception
	 */
	CronogramaAtividadeFaturamento obterCronogramaFaturamentoAtual(GrupoFaturamento grupoFaturamento, AtividadeSistema atividadeSistema)
					throws NegocioException;

	/**
	 * Verificar rotas nao processadas.
	 * 
	 * @param cronogramaAtividadeFaturamento
	 *            the cronograma atividade faturamento
	 * @return true, if successful
	 */
	boolean verificarRotasNaoProcessadas(CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento);

	/**
	 * Validar remover cronograma rota.
	 * 
	 * @param chavesPrimarias
	 *            the chaves primarias
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarRemoverCronogramaRota(Long[] chavesPrimarias, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Listar cronogramas rotas por atividade.
	 * 
	 * @param idAtividade
	 *            the id atividade
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the collection
	 */
	Collection<CronogramaRota> listarCronogramasRotasPorAtividade(long idAtividade, long chavePrimaria);

	/**
	 * Quantidade rotas por grupo faturamento.
	 * 
	 * @param idGrupoFaturamento
	 *            the id grupo faturamento
	 * @return the int
	 * @throws NegocioException
	 *             the negocio exception
	 */
	int quantidadeRotasPorGrupoFaturamento(Long idGrupoFaturamento) throws NegocioException;

	/**
	 * Método responsável por obter a
	 * periodicidade de um grupo de grupo de
	 * faturamento.
	 * 
	 * @param idGrupoFaturamento
	 *            id do grupo de faturamento
	 * @return periodicidade
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Periodicidade obterPeriodicidadePorGrupoFaturamento(Long idGrupoFaturamento) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * cronogramas rota da atividade de sistema de
	 * um cronograma
	 * faturamento baseado pelo
	 * id do cronograma e da atividade de sistema.
	 * 
	 * @param idAtividadeSistema
	 *            the id atividade sistema
	 * @param idCronogramaFaturamento
	 *            the id cronograma faturamento
	 * @return the collection
	 */
	Collection<CronogramaRota> consultarCronogramasRotasPorAtividadeECronogramaFaturamento(Long idAtividadeSistema,
					Long idCronogramaFaturamento);

	/**
	 * Método responsável por consultar o último
	 * Cronograma Rota a partir de uma Rota e da
	 * Atividade
	 * de Sistema.
	 * 
	 * @param idRota
	 *            the id rota
	 * @param idAtividadeSistema
	 *            the id atividade sistema
	 * @param referenciaCicloAnterior
	 *            the referencia ciclo anterior
	 * @return the cronograma rota
	 */
	CronogramaRota consultarCronogramaRotaAtividade(Long idRota, Long idAtividadeSistema, Map<String, Integer> referenciaCicloAnterior);

	/**
	 * Método responsável por consultar o último
	 * Cronograma Rota a partir de uma Rota e da
	 * Atividade
	 * de Sistema.
	 * 
	 * @param idPeriodicidade
	 *            the id periodicidade
	 * @param idTipoLeitura
	 *            the id tipo leitura
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<GrupoFaturamento> listarGrupoFaturamento(Long idPeriodicidade, Long idTipoLeitura) throws NegocioException;

	/**
	 * Método responsável por listar todos os
	 * possíveis ciclos do sistema.
	 * 
	 * @return lista de ciclos do sistema
	 */
	Collection<Integer> listarCiclos();

	/**
	 * Método responsável por inserir a rota.
	 * 
	 * @param rota
	 *            the rota
	 * @param pontosConsumoAssociados
	 *            the pontos consumo associados
	 * @return the long
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws IllegalAccessException
	 *             the illegal access exception
	 * @throws InvocationTargetException
	 *             the invocation target exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	Long inserirRota(Rota rota, Map<Long, Object> pontosConsumoAssociados) throws NegocioException, IllegalAccessException,
					InvocationTargetException, ConcorrenciaException;

	/**
	 * Método reponsável por obter o Cronograma
	 * baseado na Rota, Ciclo e Referência.
	 * 
	 * @param rota
	 *            Rota
	 * @param ciclo
	 *            Ciclo
	 * @param referencia
	 *            Referência
	 * @return CronogramaRota
	 */
	List<CronogramaRota> obterAtributosCronogramasRota(Rota rota, String ciclo, String referencia);

	/**
	 * Método responsavel por validar uma rota em
	 * processo de leitura.
	 * 
	 * @param rota
	 *            the rota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarRotaEmProcessoLeitura(Rota rota) throws NegocioException;

	/**
	 * Método para obter a rota a partir do ponto
	 * de consumo.
	 * 
	 * @param idPontoConsumo
	 *            the id ponto consumo
	 * @return the rota
	 */
	Rota obterRotaPorPontoConsumo(Long idPontoConsumo);

	/**
	 * Método para obter a rota do cronograma rota
	 * a partir do Grupo de Faturamento.
	 * 
	 * @param grupodFaturamento
	 *            the grupod faturamento
	 * @return Collection<Rota>
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Rota> obterRotasCronogramaFaturamento(GrupoFaturamento grupodFaturamento) throws NegocioException;

	/**
	 * Método para atualizar cronograma a partir
	 * da Rota.
	 * 
	 * @param chAtividadeSistema
	 *            , Collection<Rota> lRota,
	 *            StringBuilder logProcesso
	 * @param lRota
	 *            the l rota
	 * @param logProcesso
	 *            the log processo
	 * @throws NegocioException
	 *             the negocio exception
	 */

	void atualizarCronogramaPorRota(long chAtividadeSistema, Collection<Rota> lRota, StringBuilder logProcesso) throws NegocioException;

	/**
	 * Criar periodicidade.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarPeriodicidade();

	/**
	 * Criar tipo leitura.
	 * 
	 * @return the entidade negocio
	 */
	EntidadeNegocio criarTipoLeitura();

	/**
	 * Listar rotas por setor comercial da quadra.
	 * 
	 * @param idQuadra
	 *            the id quadra
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Rota> listarRotasPorSetorComercialDaQuadra(Long idQuadra) throws NegocioException;

	/**
	 * Validar rota.
	 * 
	 * @param rota
	 *            the rota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarRota(Long rota) throws NegocioException;

	/**
	 * Atualizar associacao ponto consumo rota.
	 * 
	 * @param listaPontoConsumo
	 *            the lista ponto consumo
	 * @param listaPontoConsumoRemoverAssociacao
	 *            the lista ponto consumo remover associacao
	 * @param chaveRota
	 *            the chave rota
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void atualizarAssociacaoPontoConsumoRota(List<PontoConsumo> listaPontoConsumo, List<PontoConsumo> listaPontoConsumoRemoverAssociacao,
					Long chaveRota) throws ConcorrenciaException, NegocioException;

	/**
	 * Verificar cronograma rota realizado.
	 * 
	 * @param rota
	 *            the rota
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @param idAtividadeSistema
	 *            the id atividade sistema
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public boolean verificarCronogramaRotaRealizado(Rota rota, Integer anoMesReferencia, Integer numeroCiclo, Long idAtividadeSistema)
					throws NegocioException;

	/**
	 * Listar grupo faturamento dados medicao.
	 * 
	 * @param idTipoLeituras
	 *            the id tipo leituras
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<GrupoFaturamento> listarGrupoFaturamentoDadosMedicao(ConstanteSistema idTipoLeituras) throws NegocioException;

	/**
	 * Listar rota por leiturista.
	 * 
	 * @param leiturista
	 *            the leiturista
	 * @return the collection
	 */
	Collection<Rota> listarRotaPorLeiturista(Leiturista leiturista);

	/**
	 * Obtém o registro mais recente do histórico de rota filtrando por: rotacronograma e rota.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param data
	 * 			the data
	 * @return {@link RotaHistorico}
	 */
	Collection<RotaHistorico> obterHistoricoRotaMaisRecente(Map<String, Object> filtro, Boolean data);

	/**
	 * Listar rotahistorico por cronograma rota.
	 * 
	 * @return {@link Collection}<RotaHistorico>
	 */
	Collection<RotaHistorico> listarRotahistorico();

	/**
	 * Salvar rota historico.
	 * 
	 * @param listaRotaHistorico
	 *            the lista rota historico
	 * @return boolean
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean salvarRotaHistorico(Collection<RotaHistorico> listaRotaHistorico) throws NegocioException;

	/**
	 * Obter cronograma rota.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return {@link CronogramaRota}
	 */
	CronogramaRota obterCronogramaRota(long chavePrimaria);

	/**
	 * Atualizar rota historico.
	 * 
	 * @param rotaHistorico
	 *            the rota historico
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	boolean atualizarRotaHistorico(RotaHistorico rotaHistorico) throws NegocioException, ConcorrenciaException;

	/**
	 * Consulta uma coleção de {@link RotaHistorico} por chave primária de rota.
	 * @param chaveRota - {@link Long}
	 * @return colecaoRotaHistorico - {@link Collection}
	 */
	Collection<RotaHistorico> listarRotahistoricoPorRota(Long chaveRota);

	/**
	 * Consulta o grupo de faturamento por chave primária, obtendo os seguintes atributos:
	 * {@code chavePrimaria}, {@code descricao}.
	 * @param chave
	 * 			the chave
	 * @return grupo de faturamento
	 */
	GrupoFaturamento obterAtributosGrupoFaturamento(Long chave);
	
	/**
	  * Consulta a rota por chave primária, obtendo os seguintes atributos:
	  * {@code chavePrimaria}.
	  * @param chave
	  * 		the chave
	  * @return rota
	  */
	Rota obterAtributosRota(Long chave);
	
	/**
	  * Consulta as rotas de um grupo de faturamento por chave primária do grupo, 
	  * obtendo os seguintes atributos da rota:
	  * {@code chavePrimaria}.
	  * @param chavePrimariaGrupoFaturamento
	  * 				the chavePrimariaGrupoFaturamento
	  * @return rota
	  * @throws NegocioException
	  * 				the NegocioException
	  * 
	  */
	Collection<Rota> listarAtributosRotasPorGrupoFaturamento(Long chavePrimariaGrupoFaturamento) throws NegocioException;


	/**
	 * Verifica o cronograma para a atividade de Consistir Leitura e Faturar Grupo, atualizando
	 * a data de realização do cronograma de rotas e do cronograma de atividade
	 * do faturamento.
	 * @param mapaCronogramaRotaPorRota
	 * 			the mapaCronogramaRotaPorRota
	 * @param atividadeSistema
	 * 			the atividadeSistema
	 * @param logProcesso
	 * 			the logProcesso
	 * @param rotaPossuiPontosConsumo
	 * 			the rotaPossuiPontosConsumo
	 * @throws NegocioException
	 * 					the NegocioException
	 */
	void atualizarCronogramaPorRota(Map<Rota, Collection<CronogramaRota>> mapaCronogramaRotaPorRota, AtividadeSistema atividadeSistema,
					StringBuilder logProcesso, Map<Rota, Boolean> rotaPossuiPontosConsumo) throws NegocioException;

	/**
	 * Obtém cronogramas de rota, por chaves primárias de rotas
	 * e que possuem cronograma de atividade da atividade
	 * especificada no parâmetro.
	 * 
	 * @param chavesPrimarias
	 * 					the chavesPrimarias
	 * @param atividadeSistema
	 * 					the atividadeSistema
	 * @return mapa de cronogramas de rota por rota
	 */
	Map<Rota, Collection<CronogramaRota>> obterMapaCronogramaRotaPorRotas(Long[] chavesPrimarias, AtividadeSistema atividadeSistema);

	
	
	/**
	 * Obtém o número ciclo e o ano mês de referência do grupo de faturamento da {@code rota}
	 * especificada no parâmetro.
	 * @param rota
	 * 			the rota
	 * @param grupoFaturamento
	 * 			the grupoFaturamento
	 * @return mapa de mês referência por número ciclo 
	 * @throws NegocioException
	 * 						the NegocioException
	 **/
	Map<String, Integer> obterReferenciaCicloAtual(Rota rota) throws NegocioException;

	/**
	 * Agrupa as rotas dos pontos de consumo
	 * especificados no parâmetro.
	 * 
	 * @param pontosConsumo
	 * 					the pontosConsumo
	 * @return rotas dos pontos de consumo
	 **/
	List<Rota> agruparRotasDePontosConsumo(Collection<PontoConsumo> pontosConsumo);

	
	/**
	 * Obtém um grupo de faturamento pelo atributo {@code descricao}.
	 * 
	 * @param descricaoGrupo - {@link String}
	 * @return {@link GrupoFaturamento}
	 */
	GrupoFaturamento obterGrupoPorDescricao(String descricaoGrupo);

	/**
	 * Obtém uma rota por grupo de faturamento e pelo atributo
	 * {@code numeroRota}.
	 * 
	 * @param numeroRota
	 *            - {@link String}
	 * @param grupoFaturamento
	 *            - {@link GrupoFaturamento}
	 * @return {@link Rota}
	 */
	Rota obterRotaDeGrupoFaturamentoPorNumero(String numeroRota, GrupoFaturamento grupoFaturamento);

	/**
	 * Adiciona cronograma de rota como um valor para um rota como chave.
	 * 
	 * @param cronogramaPorRota - {@link Map}
	 * @param cronogramaRota - {@link CronogramaRota}
	 * @param rota - {@link Rota}
	 */
	void mapearCronogramaRota(Map<Rota, CronogramaRota> cronogramaPorRota, CronogramaRota cronogramaRota, Rota rota);

	/**
	 * Método consultar chave rota.
	 * 
	 * @param filtro {@link Map}
	 * @return Collection {@link Collection}
	 */
	Collection<Long> consultarChaveRota(Map<String, Object> filtro);
	
	/**
	 * Método responsável para obtenção de chave primaria de rota e numero de rota em Faturar Grupo
	 * 
	 * @param grupoFaturamento {@link GrupoFaturamento}
	 * @return Collection {@link Collection}
	 * @throws NegocioException {@link NegocioException}
	 */
	public Collection<Rota> obterNumerosRotasCronogramaFaturamento(GrupoFaturamento grupoFaturamento) throws NegocioException;

	/**
	 * Consulta o grupo de faturamento por chave primária, obtendo os seguintes atributos:
	 * {@code chavePrimaria}, {@code descricaoAbreviada}.
	 * @param chave {@link Long}
	 * @return grupo de faturamento
	 */
	GrupoFaturamento obterDescricaoGrupoFaturamento(Long chave);

	/**
	 * Consulta responsável por trazer uma rota trazendo os seguintes atributos:
	 * {@code cahvePrimaria}, {@code numeroRota}.
	 * @param chavePrimaria {@link Long}
	 * @return Rota {@link Rota}
	 * @throws NegocioException {@link NegocioException}
	 */
	Rota buscarNumeroRota(Long chavePrimaria) throws NegocioException;

	/**
	 * Método para atualizar cronograma a partir
	 * da Rota.
	 * 
	 * @param rota {@link Rota}
	 * @param codigoAtividadeConsistirLeitura 
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void atualizarCronogamaPorRota(Rota rota, long codigoAtividadeConsistirLeitura) throws NegocioException;
	
	/**
	 * Retorna a rota com o id passado como parametro
	 * @param idRota
	 * @return rota {@link Rota}
	 * @throws GGASException
	 */
	Rota obterPorId(Long idRota) throws GGASException;

	Collection<Rota> listarRotasPorTipoLeitura(String tipoLeitura) throws NegocioException;
	
}
