/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe RotaImpl representa uma RotaImpl no sistema.
 *
 * @since 25/08/2009
 * 
 */

package br.com.ggas.medicao.rota.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Constantes;

/**
 * Classe responsável pela implementação dos métodos relacionados 
 * a Rota
 *
 */
public class RotaImpl extends EntidadeNegocioImpl implements Rota {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -2049531491273658493L;

	private TipoLeitura tipoLeitura;

	private Empresa empresa;

	private GrupoFaturamento grupoFaturamento;

	private SetorComercial setorComercial;

	private String numeroRota;

	private Leiturista leiturista;

	private Integer numeroMaximoPontosConsumo;

	private Periodicidade periodicidade;

	private Collection<CronogramaRota> cronogramas = new TreeSet<CronogramaRota>();

	private Collection<PontoConsumo> pontosConsumo = new HashSet<PontoConsumo>();

	private Leiturista leituristaSuplente;

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * getTipoLeitura()
	 */
	@Override
	public TipoLeitura getTipoLeitura() {

		return tipoLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * setTipoLeitura
	 * (br.com.ggas.cadastro.imovel.TipoLeitura)
	 */
	@Override
	public void setTipoLeitura(TipoLeitura tipoLeitura) {

		this.tipoLeitura = tipoLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.impl.Rota#getEmpresa
	 * ()
	 */
	@Override
	public Empresa getEmpresa() {

		return empresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.impl.Rota#setEmpresa
	 * (br.com.ggas.cadastro.empresa.Empresa)
	 */
	@Override
	public void setEmpresa(Empresa empresa) {

		this.empresa = empresa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * getGrupoFaturamento()
	 */
	@Override
	public GrupoFaturamento getGrupoFaturamento() {

		return grupoFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * setGrupoFaturamento
	 * (br.com.ggas.cadastro.imovel
	 * .GrupoFaturamento)
	 */
	@Override
	public void setGrupoFaturamento(GrupoFaturamento grupoFaturamento) {

		this.grupoFaturamento = grupoFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * getSetorComercial()
	 */
	@Override
	public SetorComercial getSetorComercial() {

		return setorComercial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * setSetorComercial
	 * (br.com.ggas.cadastro.localidade
	 * .SetorComercial)
	 */
	@Override
	public void setSetorComercial(SetorComercial setorComercial) {

		this.setorComercial = setorComercial;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * getNumeroRota()
	 */
	@Override
	public String getNumeroRota() {

		return numeroRota;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * setNumeroRota(java.lang.String)
	 */
	@Override
	public void setNumeroRota(String numeroRota) {

		this.numeroRota = numeroRota;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * getLeiturista()
	 */
	@Override
	public Leiturista getLeiturista() {

		return leiturista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * setLeiturista
	 * (br.com.ggas.cadastro.imovel.Leiturista)
	 */
	@Override
	public void setLeiturista(Leiturista leiturista) {

		this.leiturista = leiturista;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * getPeriodicidade()
	 */
	@Override
	public Periodicidade getPeriodicidade() {

		return periodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.impl.Rota#
	 * setPeriodicidade
	 * (br.com.ggas.medicao.rota.Periodicidade)
	 */
	@Override
	public void setPeriodicidade(Periodicidade periodicidade) {

		this.periodicidade = periodicidade;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.Rota#getCronogramas
	 * ()
	 */
	@Override
	public Collection<CronogramaRota> getCronogramas() {

		return cronogramas;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.Rota#setCronogramas
	 * (java.util.Collection)
	 */
	@Override
	public void setCronogramas(Collection<CronogramaRota> cronogramas) {

		this.cronogramas = cronogramas;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.Rota#getPontosConsumo
	 * ()
	 */
	@Override
	public Collection<PontoConsumo> getPontosConsumo() {

		return pontosConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.rota.Rota#setPontosConsumo
	 * (java.util.Collection)
	 */
	@Override
	public void setPontosConsumo(Collection<PontoConsumo> pontosConsumo) {

		this.pontosConsumo = pontosConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.Rota#
	 * getNumeroMaximoPontosConsumo()
	 */
	@Override
	public Integer getNumeroMaximoPontosConsumo() {

		return numeroMaximoPontosConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.rota.Rota#
	 * setNumeroMaximoPontosConsumo
	 * (java.lang.Integer)
	 */
	@Override
	public void setNumeroMaximoPontosConsumo(Integer numeroMaximoPontosConsumo) {

		this.numeroMaximoPontosConsumo = numeroMaximoPontosConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(periodicidade == null) {
			stringBuilder.append(PERIODICIDADE);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(tipoLeitura == null) {
			stringBuilder.append(TIPO_LEITURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(numeroRota)) {
			stringBuilder.append(NUMERO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(grupoFaturamento == null) {
			stringBuilder.append(GRUPO_FATURAMENTO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}


		if(empresa == null) {
			stringBuilder.append(EMPRESA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
		}

		return erros;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return numeroRota;
	}

	@Override
	public Leiturista getLeituristaSuplente() {

		return leituristaSuplente;
	}

	@Override
	public void setLeituristaSuplente(Leiturista leituristaSuplente) {

		this.leituristaSuplente = leituristaSuplente;
	}

}
