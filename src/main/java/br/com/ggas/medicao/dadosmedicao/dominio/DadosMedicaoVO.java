/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/09/2014 11:21:14
 @author vtavares
 */

package br.com.ggas.medicao.dadosmedicao.dominio;

import br.com.ggas.medicao.leitura.impl.LeituraMovimentoImpl;
/**
 * Classe utilizada para transferência de dados entre as telas de dados de medição
 *
 */
public class DadosMedicaoVO extends LeituraMovimentoImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1744644016461555646L;

	private String referenciaCiclo;

	private Long chaveRota;

	private String numeroRota;

	private String descricaoAnormalidade;

	private Long quantidadeAnormalidade;

	private String descricaoGrupoFaturamento;

	private Long quantidadePontoConsumo;

	private Long processado;

	private String dataLeituraInicio;

	private String dataLeituraFim;

	private String mesAnoReferencia;

	private Long chaveAnormalidadeLeitura;

	private Long chaveGrupoFaturamento;

	private String confirmacaoLeitura;

	private String indicadorProcessado;

	public String getReferenciaCiclo() {

		return referenciaCiclo;
	}

	public void setReferenciaCiclo(String referenciaCiclo) {

		this.referenciaCiclo = referenciaCiclo;
	}

	public Long getChaveRota() {

		return chaveRota;
	}

	public void setChaveRota(Long chaveRota) {

		this.chaveRota = chaveRota;
	}

	@Override
	public String getNumeroRota() {

		return numeroRota;
	}

	@Override
	public void setNumeroRota(String numeroRota) {

		this.numeroRota = numeroRota;
	}

	public String getDescricaoAnormalidade() {

		return descricaoAnormalidade;
	}

	public void setDescricaoAnormalidade(String descricaoAnormalidade) {

		this.descricaoAnormalidade = descricaoAnormalidade;
	}

	public String getDescricaoGrupoFaturamento() {

		return descricaoGrupoFaturamento;
	}

	public void setDescricaoGrupoFaturamento(String descricaoGrupoFaturamento) {

		this.descricaoGrupoFaturamento = descricaoGrupoFaturamento;
	}

	public Long getQuantidadePontoConsumo() {

		return quantidadePontoConsumo;
	}

	public void setQuantidadePontoConsumo(Long quantidadePontoConsumo) {

		this.quantidadePontoConsumo = quantidadePontoConsumo;
	}

	public Long getProcessado() {

		return processado;
	}

	public void setProcessado(Long processado) {

		this.processado = processado;
	}

	public String getDataLeituraInicio() {

		return dataLeituraInicio;
	}

	public void setDataLeituraInicio(String dataLeituraInicio) {

		this.dataLeituraInicio = dataLeituraInicio;
	}

	public String getDataLeituraFim() {

		return dataLeituraFim;
	}

	public void setDataLeituraFim(String dataLeituraFim) {

		this.dataLeituraFim = dataLeituraFim;
	}

	public String getMesAnoReferencia() {

		return mesAnoReferencia;
	}

	public void setMesAnoReferencia(String mesAnoReferencia) {

		this.mesAnoReferencia = mesAnoReferencia;
	}

	public Long getChaveAnormalidadeLeitura() {

		return chaveAnormalidadeLeitura;
	}

	public void setChaveAnormalidadeLeitura(Long chaveAnormalidadeLeitura) {

		this.chaveAnormalidadeLeitura = chaveAnormalidadeLeitura;
	}

	public Long getChaveGrupoFaturamento() {

		return chaveGrupoFaturamento;
	}

	public void setChaveGrupoFaturamento(Long chaveGrupoFaturamento) {

		this.chaveGrupoFaturamento = chaveGrupoFaturamento;
	}

	public String getConfirmacaoLeitura() {

		return confirmacaoLeitura;
	}

	public void setConfirmacaoLeitura(String confirmacaoLeitura) {

		this.confirmacaoLeitura = confirmacaoLeitura;
	}

	public Long getQuantidadeAnormalidade() {

		return quantidadeAnormalidade;
	}

	public void setQuantidadeAnormalidade(Long quantidadeAnormalidade) {

		this.quantidadeAnormalidade = quantidadeAnormalidade;
	}

	public String getIndicadorProcessado() {

		return indicadorProcessado;
	}

	public void setIndicadorProcessado(String indicadorProcessado) {

		this.indicadorProcessado = indicadorProcessado;
	}

}
