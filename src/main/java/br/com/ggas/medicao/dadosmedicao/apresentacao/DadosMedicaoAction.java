/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/09/2014 10:54:11
 @author vtavares
 */

package br.com.ggas.medicao.dadosmedicao.apresentacao;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.geral.apresentacao.GenericAction;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.dadosmedicao.dominio.DadosMedicaoVO;
import br.com.ggas.medicao.dadosmedicao.negocio.ControladorDadosMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.ControladorLeiturista;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.medicao.leitura.DadosRegistrarLeiturasAnormalidades;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.medicao.leitura.AnaliseExcecaoLeituraVO;

/**
 * Classe Dados Medicao Action.
 *
 * @author lcavalcanti
 *
 *
 */
@Controller
public class DadosMedicaoAction extends GenericAction {

	private static final String LISTA_GRUPO_FATURAMENTO = "listaGrupoFaturamento";

	private static final String LISTA_ROTA = "listaRota";

	private static final String LISTA_ANORMALIDADE_LEITURA = "listaAnormalidadeLeitura";

	private static final String LISTA_LEITURA_MOVIMENTO = "listaLeituraMovimento";

	private static final String LISTA_LEITURISTA = "listaLeiturista";

	private static final String CONFIRMACAO_LEITURA = "confirmacaoLeitura";

	private static final String LEITURA_MOVIMENTO = "leituraMovimento";

	private static final String HABILITADO = "habilitado";

	private static final String ID_ROTA = "idRota";

	private static final String PROCESSADO = "processado";
	
	private static final String NAO_PROCESSADO = "naoProcessado";
	
	private static final String SITUACAO_LEITURA_MOVIMENTO = "situacaoLeituraMovimento";
	
	private static final String ANO_MES_FATURAMENTO = "anoMesFaturamento";
	
	private static final String NUMERO_SERIE_MEDIDOR = "numeroSerieMedidor";

	private static final String SITUACAO_LEITURA = "situacaoLeitura";
	
	private static final String ID_IMOVEL = "idImovel";

	private static final String DATA_LEITURA_FILTRO = "dataLeituraFiltro";

	@Autowired
	@Qualifier("controladorRota")
	private ControladorRota controladorRota;

	@Autowired
	@Qualifier("controladorAnormalidade")
	private ControladorAnormalidade controladorAnormalidade;

	@Autowired
	@Qualifier("controladorLeituraMovimento")
	private ControladorLeituraMovimento controladorLeituraMovimento;

	@Autowired
	@Qualifier("controladorLeiturista")
	private ControladorLeiturista controladorLeiturista;

	@Autowired
	private ControladorDadosMedicao controladorDadosMedicao;

	@Autowired
	@Qualifier("controladorConstanteSistema")
	private ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	@Qualifier("controladorAcesso")
	private ControladorAcesso controladorAcesso;
	
	@Autowired
	@Qualifier("controladorCronogramaFaturamento")
	private ControladorCronogramaFaturamento controladorCronogramaFaturamento;

	/**
	 * Exibir pesquisa dados medicao.
	 * 
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("exibirPesquisaDadosMedicao")
	public ModelAndView exibirPesquisaDadosMedicao() throws NegocioException {

		ModelAndView model = new ModelAndView("exibirPesquisaDadosMedicao");

		this.carregarDados(model);

		model.addObject(CONFIRMACAO_LEITURA, "");
		model.addObject("indicadorProcessado", "");

		return model;
	}

	/**
	 * 
	 * Exibir detalhamento dados medicao.
	 * 
	 * @param chaveRota
	 * 				the chaveRota 
	 * @param anoMesFaturamento
	 * 				the anoMesFaturamento
	 * @param processado
	 * 				the processado
	 * @param situacaoLeituraMovimento
	 * 				the situacaoLeituraMovimento
	 * @param idImovel
	 * 				the idImovel
	 * @param numeroSerieMedidor
	 * 				the numeroSerieMedidor
	 * @param dataLeituraFiltro
	 * 				the dataLeituraFiltro
	 * @param request
	 * 				the request
	 * @return ModelAndView
	 * @throws ParseException
	 * 				the ParseException
	 * @throws GGASException
	 * 				the ParseException
	 */
	@RequestMapping("exibirDetalhamentoDadosMedicao")
	public ModelAndView exibirDetalhamentoDadosMedicao(@RequestParam("idRota") Long chaveRota,
					@RequestParam("anoMesFaturamento") Integer anoMesFaturamento, 
					@RequestParam("processado") String processado,
					@RequestParam("situacaoLeituraMovimento") String situacaoLeituraMovimento, 
					@RequestParam(ID_IMOVEL) Long idImovel,
					@RequestParam(NUMERO_SERIE_MEDIDOR) String numeroSerieMedidor, 
					@RequestParam(DATA_LEITURA_FILTRO) String dataLeituraFiltro,
					HttpServletRequest request)
					throws ParseException, GGASException {

		ModelAndView model = new ModelAndView("exibirDetalhamentoDadosMedicao");
		

		Map<String, Object> filtro = montaFiltro(processado, idImovel, numeroSerieMedidor, dataLeituraFiltro, model);
		
		Collection<LeituraMovimento> listaLeituraMovimento =
				controladorDadosMedicao.consultarLeituraMovimentoPorRota(chaveRota, anoMesFaturamento, null, filtro);
		this.carregarDataNovaLeitura(listaLeituraMovimento);

		LeituraMovimento leituraMovimento =
				controladorDadosMedicao.consultarLeituraMovimentoPorRota(chaveRota, anoMesFaturamento, null, null).iterator().next();

		filtro = new HashMap<String, Object>();
		filtro.put(HABILITADO, Boolean.TRUE);
		Collection<Leiturista> listaLeiturista = controladorLeiturista.consultarLeituristas(filtro);

		if (idImovel != null) {
			Imovel imovel = Fachada.getInstancia().buscarImovelPorChave(idImovel);
			model.addObject("nomeFantasiaImovel", imovel.getNome());
			model.addObject("matriculaImovel", imovel.getChavePrimaria());
			model.addObject("numeroImovel", imovel.getNumeroImovel());
			model.addObject("cidadeImovel", imovel.getQuadraFace().getEndereco().getCep().getNomeMunicipio());
			model.addObject("condominio", String.valueOf(imovel.getCondominio()));
			model.addObject(ID_IMOVEL, idImovel);
		}
		model.addObject(DATA_LEITURA_FILTRO, dataLeituraFiltro);
		model.addObject(NUMERO_SERIE_MEDIDOR, numeroSerieMedidor);
		model.addObject(LEITURA_MOVIMENTO, leituraMovimento);
		model.addObject(LISTA_LEITURISTA, listaLeiturista);
		model.addObject(LISTA_LEITURA_MOVIMENTO, listaLeituraMovimento);

		this.carregarDados(model);

		model.addObject(PROCESSADO, processado);
		model.addObject(SITUACAO_LEITURA_MOVIMENTO, situacaoLeituraMovimento);
		model.addObject(ID_ROTA, chaveRota);
		model.addObject(ANO_MES_FATURAMENTO, anoMesFaturamento);

		return model;
	}

	private Map<String, Object> montaFiltro(String situacaoLeituraMovimento, Long idImovel, String numeroSerieMedidor,
			String dataLeituraFiltro, ModelAndView model) {
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(SITUACAO_LEITURA, obtemChavesSituacaoLeitura(situacaoLeituraMovimento));
		filtro.put(ID_IMOVEL, idImovel);
		filtro.put(NUMERO_SERIE_MEDIDOR, numeroSerieMedidor);
		if (dataLeituraFiltro != null && !"".equals(dataLeituraFiltro)) {
			try {
				filtro.put(DATA_LEITURA_FILTRO,
						Util.converterCampoStringParaData(DATA_LEITURA_FILTRO, dataLeituraFiltro, Constantes.FORMATO_DATA_BR));
			} catch (GGASException e) {
				LOG.error(e.getMessage(), e);
				mensagemErroParametrizado(model, new NegocioException(Constantes.ERRO_DATA_INVALIDADE, true));
			}
		}
		return filtro;
	}

	/**
	 * Alterar dados medicao.
	 * 
	 * @param processado
	 *            the processado
	 * @param chaveRota
	 *            the chave rota
	 * @param anoMesFaturamento
	 *            the ano mes faturamento
	 * @param chaveLeiturista
	 *            the chave leiturista
	 * @param dataLeitura
	 *            the data leitura
	 * @param dataLeituraAnterior
	 *            the data leitura anterior
	 * @param valorLeitura
	 *            the valor leitura
	 * @param valorPressao
	 *            the valor pressao
	 * @param valorUnidadePressao
	 *            the valor unidade pressao
	 * @param valorTemperatura
	 *            the valor temperatura
	 * @param valorUnidadeTemperatura
	 *            the valor unidade temperatura
	 * @param anormaLidadeLeituraAnterior
	 *            the anorma lidade leitura anterior
	 * @param observacoesCadastrais
	 *            the observacoes cadastrais
	 * @param anormalidadeLeitura - {@link Long} Array
	 * @param situacaoLeituraMovimento - {@link String}
	 * @param request
	 *           	 the request
	 * @param chavesPrimarias
	 * 				the chavesPrimarias
	 * @param idImovel
	 * 				the idImovel
	 * @param s	ituacaoLeituraMovimento
	 * 				the situacaoLeituraMovimento
	 * @param numeroSerieMedidor
	 * 				the numeroSerieMedidor
	 * @param dataLeituraFiltro
	 * 				the dataLeituraFiltro
	 * @return the model and view
	 * @throws ParseException
	 *             the parse exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("alterarDadosMedicao")
	public ModelAndView alterarDadosMedicao(@RequestParam("processado") String processado, @RequestParam("chaveRota") Long chaveRota,
					@RequestParam("anoMesFaturamento") Integer anoMesFaturamento, @RequestParam("leiturista") Long chaveLeiturista,
					@RequestParam("chavesPrimarias") Long[] chavesPrimarias,
					@RequestParam("dataLeitura") String[] dataLeitura, @RequestParam("dataLeituraAnterior") String[] dataLeituraAnterior,
					@RequestParam("valorLeitura") Float[] valorLeitura, @RequestParam("valorPressao") BigDecimal[] valorPressao,
					@RequestParam("valorTemperatura") BigDecimal[] valorTemperatura,
					@RequestParam("anormalidadeLeitura") Long[] anormalidadeLeitura,
					@RequestParam("observacoesCadastrais") String[] observacoesCadastrais,
					@RequestParam("situacaoLeituraMovimento") String situacaoLeituraMovimento, 
					@RequestParam(ID_IMOVEL) Long idImovel,
					@RequestParam(NUMERO_SERIE_MEDIDOR) String numeroSerieMedidor, 
					@RequestParam(DATA_LEITURA_FILTRO) String dataLeituraFiltro, HttpServletRequest request)
					throws ParseException, GGASException {

		ModelAndView model = new ModelAndView("exibirDetalhamentoDadosMedicao");

		try {

			controladorLeituraMovimento.atualizarColecao(chaveRota, anoMesFaturamento, chaveLeiturista,
							dataLeitura, valorLeitura, valorPressao, valorTemperatura,
							anormalidadeLeitura, observacoesCadastrais, chavesPrimarias);
			
			model = this.exibirDetalhamentoDadosMedicao(chaveRota, anoMesFaturamento, processado, situacaoLeituraMovimento, idImovel,
					numeroSerieMedidor, dataLeituraFiltro, request);
			mensagemSucesso(model, Constantes.SUCESSO_ENTIDADE_ALTERADA, "Dados de Medição");
			
		} catch(GGASException e) {
			model = this.exibirDetalhamentoDadosMedicao(chaveRota, anoMesFaturamento, processado, situacaoLeituraMovimento, idImovel,
					numeroSerieMedidor, dataLeituraFiltro, request);
			mensagemErroParametrizado(model, e);
		} finally {
			model.addObject("chaveLeiturista", chaveLeiturista);
		}

		return model;
	}
	
	
	@RequestMapping(value = "alterarDadosMedicaoAJAX", 
					method = RequestMethod.POST, 
					consumes = {MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public Map<String,Object> alterarDadosMedicaoAJAX(@RequestBody JSONDadosLeituraMovimento json)
					throws ParseException, GGASException {
		try {
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
						
			if (json.anormalidadeLeitura != 35 && !"".equals(json.dataLeitura)) {
				Date dataLeitura = formato.parse(json.dataLeitura);
				Date dataLeituraAnterior = formato.parse(json.dataLeituraAnterior);
				dataLeitura.before(dataLeituraAnterior);
				if (dataLeitura.before(dataLeituraAnterior) || dataLeitura.equals(dataLeituraAnterior)) {
					return Maps.newHashMap(ImmutableMap.of("erro",
							"Data de leitura não pode ser igual ou anterior a data de leitura anterior!"));
				}
			} else {
				json.dataLeitura = null;
			}
			
			controladorLeituraMovimento.atualizarColecao(json.chaveRota, 
														 json.anoMesFaturamento, 
														 json.chaveLeiturista,
														 new String[]{ json.dataLeitura }, 
														 new Float[] {json.valorLeitura}, 
														 new BigDecimal[]{json.valorPressao}, 
														 new BigDecimal[]{json.valorTemperatura},
														 new Long[] {json.anormalidadeLeitura}, 
														 new String[] {json.observacoesCadastrais}, 
														 new Long[] {json.chavePrimaria});
			return Maps.newHashMap(ImmutableMap.of( "ok","Leitura gravada!"));
		} catch (ParseException pe) {
			return Maps.newHashMap(ImmutableMap.of( "erro",pe));
		} catch (GGASException ge) {
			return Maps.newHashMap(ImmutableMap.of( "erro",ge));
		}
	}

	/**
	 * Carregar data nova leitura.
	 * 
	 * @param listaLeituraMovimento
	 *            the lista leitura movimento
	 */
	private void carregarDataNovaLeitura(Collection<LeituraMovimento> listaLeituraMovimento) {

		for (LeituraMovimento leituraMovimento : listaLeituraMovimento) {
			Date dataLeitura = leituraMovimento.getDataLeitura();
			if(dataLeitura != null) {
				String dataNovaLeitura = Util.converterDataParaStringSemHora(dataLeitura, Constantes.FORMATO_DATA_HORA_BR);
				leituraMovimento.setDataNovaLeitura(dataNovaLeitura);
			}
		}

	}

	/**
	 * Pesquisar dados medicao.
	 * 
	 * @param dadosMedicaoVO
	 *            the dados medicao vo
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws ParseException
	 *             the parse exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@RequestMapping("pesquisarDadosMedicao")
	public ModelAndView pesquisarDadosMedicao(@ModelAttribute("DadosMedicaoVO") DadosMedicaoVO dadosMedicaoVO, BindingResult result,
					HttpServletRequest request) throws ParseException, GGASException {

		ModelAndView model = new ModelAndView("exibirPesquisaDadosMedicao");

		try {

			Collection<DadosMedicaoVO> listaLeituraMovimento = controladorDadosMedicao.consultarLeituraMovimento(dadosMedicaoVO);
			model.addObject(LISTA_LEITURA_MOVIMENTO, listaLeituraMovimento);

			this.carregarDados(model);
		} catch(NegocioException e) {
			mensagemErroParametrizado(model, e);
		} catch(GGASException e) {
			mensagemErroParametrizado(model, e);
		}

		String confirmacaoLeitura = request.getParameter(CONFIRMACAO_LEITURA);
		if(confirmacaoLeitura == null) {
			model.addObject(CONFIRMACAO_LEITURA, "true");
		} else {
			model.addObject(CONFIRMACAO_LEITURA, confirmacaoLeitura);
		}

		model.addObject(LEITURA_MOVIMENTO, dadosMedicaoVO);
		this.carregarDados(model);

		return model;
	}
	
	@RequestMapping("encerrarRota")
	public ModelAndView encerrarRota(@ModelAttribute("DadosMedicaoVO") DadosMedicaoVO dadosMedicaoVO,
			BindingResult result, @RequestParam("chavesPrimarias") Long[] chavesPrimarias, HttpServletRequest request,
			HttpServletResponse response) throws GGASException, IOException, ParseException {
		
		StringBuilder logProcessamento = new StringBuilder();
		Operacao operacao = controladorAcesso.obterOperacaoSistemaPelaDescricao("Registrar Leitura");
		AtividadeSistema atividadeSistema = null;
		Collection<DadosRegistrarLeiturasAnormalidades> dados = null;
		ModelAndView model = new ModelAndView("exibirPesquisaDadosMedicao");
		
		if(operacao != null) {
			atividadeSistema = controladorCronogramaFaturamento.obterAtividadeSistemaPorOperacao(operacao.getChavePrimaria());
		}
		
		
		try {
			if(atividadeSistema != null) {
				dados = controladorLeituraMovimento.registrarLeituraAnormalidade(chavesPrimarias, atividadeSistema, null, new Long[0], logProcessamento, getDadosAuditoria(request).getUsuario(), Boolean.TRUE);
			}
			
			if(dados != null && !dados.isEmpty()) {
				byte[] bytes = RelatorioUtil.gerarRelatorioPDF(dados, null, "relatorioRegistrarLeiturasAnormalidades.jasper");
				
				if (bytes != null) {
					ServletOutputStream servletOutputStream = response.getOutputStream();
					response.setContentType("application/pdf");
					response.setContentLength(bytes.length);
					response.addHeader("Content-Disposition", "attachment; filename=relatorioExtratoDebito.pdf");
					servletOutputStream.write(bytes, 0, bytes.length);
					servletOutputStream.flush();
					servletOutputStream.close();
				} 
	
			}
		} catch(GGASException e ) {
			mensagemErroParametrizado(model, e);
		}
		
		mensagemSucesso(model, "A(s) Rota(s) apta(s) foram encerradas com sucesso");
		
		return model;
	}

	/**
	 * Carregar dados.
	 * 
	 * @param model
	 *            the model
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void carregarDados(ModelAndView model) throws NegocioException {

		ConstanteSistema tipoLeituraEletroCorretor = controladorConstanteSistema
						.obterConstantePorCodigo(Constantes.C_TIPO_LEITURA_ELETROCORRETOR);

		model.addObject(LISTA_GRUPO_FATURAMENTO, controladorRota.listarGrupoFaturamentoDadosMedicao(tipoLeituraEletroCorretor));

		model.addObject(LISTA_ANORMALIDADE_LEITURA, controladorAnormalidade.listarAnormalidadesLeitura());
	}

	/**
	 * Carregar rota por grupo faturamento.
	 * 
	 * @param chaveGrupoFaturamento
	 *            the chave grupo faturamento
	 * @param chaveRota
	 *            the chave rota
	 * @return the model and view
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@RequestMapping("carregarRotaPorGrupoFaturamento")
	public ModelAndView carregarRotaPorGrupoFaturamento(@RequestParam("chaveGrupoFaturamento") Long chaveGrupoFaturamento,
					@RequestParam("chaveRotaGrupoFaturamento") Long chaveRota) throws NegocioException {

		ModelAndView model = new ModelAndView("divDadosMedicaoRota");

		Collection<Rota> listaRota = controladorRota.listarRotasPorGrupoFaturamento(chaveGrupoFaturamento);

		model.addObject(LISTA_ROTA, listaRota);
		model.addObject(ID_ROTA, chaveRota);

		return model;
	}
	
	private Long[] obtemChavesSituacaoLeitura(String situacaoLeituraMovimento){
		Long[] chavesSituacaoNaoProcessado =
				{ SituacaoLeituraMovimento.GERADO, SituacaoLeituraMovimento.EM_LEITURA, SituacaoLeituraMovimento.LEITURA_RETORNADA };
		Long[] chavesSituacaoProcessado={SituacaoLeituraMovimento.PROCESSADO};
		Long[] retorno = null;
 		
		if(PROCESSADO.equals(situacaoLeituraMovimento)){
			retorno = chavesSituacaoProcessado;
		}else if(NAO_PROCESSADO.equals(situacaoLeituraMovimento)){
			retorno = chavesSituacaoNaoProcessado;
		}
		return retorno;
	}

}
