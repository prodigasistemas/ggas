package br.com.ggas.medicao.dadosmedicao.apresentacao;

import java.io.Serializable;
import java.math.BigDecimal;

public class JSONDadosLeituraMovimento implements Serializable {
	/**
	 * 
	 */
	protected static final long serialVersionUID = -279757216716103029L;
	protected Long chaveRota;
	protected Integer anoMesFaturamento;
	protected Long chaveLeiturista;
	protected String dataLeitura;
	protected String dataLeituraAnterior;
	protected Float valorLeitura;
	protected BigDecimal valorPressao;
	protected BigDecimal valorTemperatura;
	protected Long anormalidadeLeitura;
	protected String observacoesCadastrais;
	protected Long chavePrimaria;
	
	public Long getChaveRota() {
		return chaveRota;
	}
	public void setChaveRota(Long chaveRota) {
		this.chaveRota = chaveRota;
	}
	public Integer getAnoMesFaturamento() {
		return anoMesFaturamento;
	}
	public void setAnoMesFaturamento(Integer anoMesFaturamento) {
		this.anoMesFaturamento = anoMesFaturamento;
	}
	public Long getChaveLeiturista() {
		return chaveLeiturista;
	}
	public void setChaveLeiturista(Long chaveLeiturista) {
		this.chaveLeiturista = chaveLeiturista;
	}
	public String getDataLeitura() {
		return dataLeitura;
	}
	public void setDataLeitura(String dataLeitura) {
		this.dataLeitura = dataLeitura;
	}
	public String getDataLeituraAnterior() {
		return dataLeituraAnterior;
	}
	public void setDataLeituraAnterior(String dataLeituraAnterior) {
		this.dataLeituraAnterior = dataLeituraAnterior;
	}
	public Float getValorLeitura() {
		return valorLeitura;
	}
	public void setValorLeitura(Float valorLeitura) {
		this.valorLeitura = valorLeitura;
	}
	public BigDecimal getValorPressao() {
		return valorPressao;
	}
	public void setValorPressao(BigDecimal valorPressao) {
		this.valorPressao = valorPressao;
	}
	public BigDecimal getValorTemperatura() {
		return valorTemperatura;
	}
	public void setValorTemperatura(BigDecimal valorTemperatura) {
		this.valorTemperatura = valorTemperatura;
	}
	public Long getAnormalidadeLeitura() {
		return anormalidadeLeitura;
	}
	public void setAnormalidadeLeitura(Long anormalidadeLeitura) {
		this.anormalidadeLeitura = anormalidadeLeitura;
	}
	public String getObservacoesCadastrais() {
		return observacoesCadastrais;
	}
	public void setObservacoesCadastrais(String observacoesCadastrais) {
		this.observacoesCadastrais = observacoesCadastrais;
	}
	public Long getChavePrimaria() {
		return chavePrimaria;
	}
	public void setChavePrimaria(Long chavePrimaria) {
		this.chavePrimaria = chavePrimaria;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
