/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/09/2014 10:53:46
 @author vtavares
 */

package br.com.ggas.medicao.dadosmedicao.negocio.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.anormalidade.impl.AnormalidadeLeituraImpl;
import br.com.ggas.medicao.dadosmedicao.dominio.DadosMedicaoVO;
import br.com.ggas.medicao.dadosmedicao.negocio.ControladorDadosMedicao;
import br.com.ggas.medicao.dadosmedicao.repositorio.RepositorioDadosMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.ControladorLeiturista;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe Controlador Dados Medicao.
 * 
 *
 */
@Transactional
@Service("controladorDadosMedicao")
public class ControladorDadosMedicaoImpl implements ControladorDadosMedicao {

	@Autowired
	@Qualifier("controladorAnormalidade")
	private ControladorAnormalidade controladorAnormalidade;

	@Autowired
	@Qualifier("controladorLeiturista")
	private ControladorLeiturista controladorLeiturista;

	@Autowired
	@Qualifier("controladorLeituraMovimento")
	private ControladorLeituraMovimento controladorLeituraMovimento;

	@Autowired
	@Qualifier("controladorUnidade")
	private ControladorUnidade controladorUnidade;

	@Autowired
	private RepositorioDadosMedicao repositorioDadosMedicao;

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.dadosmedicao.negocio.ControladorDadosMedicao#consultarLeituraMovimento(br.com.ggas.medicao.dadosmedicao.dominio.
	 * DadosMedicaoVO
	 * )
	 */
	@Override
	public Collection<DadosMedicaoVO> consultarLeituraMovimento(DadosMedicaoVO dadosMedicaoVO) throws ParseException, GGASException {

		this.validarDadosConsulta(dadosMedicaoVO);
		Collection<Object[]> consulta = repositorioDadosMedicao.consultarLeituraMovimento(dadosMedicaoVO);

		return popularDadosMedicaoVO(consulta);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.dadosmedicao.negocio.ControladorDadosMedicao#consultarLeituraMovimentoPorRota(java.lang.Long, java.lang.Integer, java.lang.Long[], java.util.Map)
	 */
	@Override
	public Collection<LeituraMovimento> consultarLeituraMovimentoPorRota(Long chaveRota, Integer anoMesFaturamento, Long[] chavesPrimarias,
			Map<String, Object> filtro) {

		return repositorioDadosMedicao.consultarLeituraMovimentoPorRota(chaveRota, anoMesFaturamento, chavesPrimarias, filtro);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.dadosmedicao.negocio.ControladorDadosMedicao#validarDadosConsulta(br.com.ggas.medicao.dadosmedicao.dominio.
	 * DadosMedicaoVO)
	 */
	@Override
	public void validarDadosConsulta(DadosMedicaoVO dadosMedicao) throws GGASException {

		String dataLeituraInicio = dadosMedicao.getDataLeituraInicio();
		String dataLeituraFim = dadosMedicao.getDataLeituraFim();
		String mesAno = dadosMedicao.getMesAnoReferencia();

		Date dataInicio = null;
		if(dataLeituraInicio != null && !dataLeituraInicio.isEmpty()) {
			dataInicio = Util.converterCampoStringParaData("Período da Leitura", dataLeituraInicio, Constantes.FORMATO_DATA_BR);
		}

		Date dataFim = null;
		if(dataLeituraFim != null && !dataLeituraFim.isEmpty()) {
			dataFim = Util.converterCampoStringParaData("Período da Leitura", dataLeituraFim, Constantes.FORMATO_DATA_BR);
		}

		if(mesAno != null && !mesAno.isEmpty()) {
			mesAno = mesAno.replace("/", "").replace("_", "");
			boolean retorno = Util.validarMesAno(mesAno);

			if(!retorno) {
				throw new NegocioException(Constantes.ERRO_NEGOCIO_ANO_MES_INVALIDO, true);
			}
		}

		Util.validarIntervaloDatas(dataInicio, dataFim);

		Date dataAtual = Calendar.getInstance().getTime();
		if(dataInicio != null && dataInicio.compareTo(dataAtual) > 0) {

			throw new NegocioException(Constantes.ERRO_DATA_INFORMADA_MAIOR_ATUAL, true);
		}

		if(dataFim != null && dataFim.compareTo(dataAtual) > 0) {

			throw new NegocioException(Constantes.ERRO_DATA_INFORMADA_MAIOR_ATUAL, true);
		}

	}

	/**
	 * Popular dados medicao vo.
	 * 
	 * @param listaDadosMedicaoConsulta
	 *            the lista dados medicao consulta
	 * @return the collection
	 */
	private Collection<DadosMedicaoVO> popularDadosMedicaoVO(Collection<Object[]> listaDadosMedicaoConsulta) {

		Collection<DadosMedicaoVO> listaDadosMedicao = new ArrayList<DadosMedicaoVO>();

		for (Object[] objeto : listaDadosMedicaoConsulta) {
			DadosMedicaoVO dadosMedicao = new DadosMedicaoVO();
			Long quantidadeAnormalidade = (Long) objeto[0];
			String mesAno = objeto[1].toString();
			mesAno = mesAno.substring(4, 6) + "/" + mesAno.substring(0, 4);
			String referenciaCiclo = mesAno + "-" + objeto[2].toString();
			String descricaoGrupoFaturamento = (String) objeto[3];
			String numeroRota = (String) objeto[4];
			Long quantidadePontoConsumo = (Long) objeto[5];
			Long processado = (Long) objeto[6];
			Long chaveRota = (Long) objeto[7];

			dadosMedicao.setQuantidadeAnormalidade(quantidadeAnormalidade);
			dadosMedicao.setReferenciaCiclo(referenciaCiclo);
			dadosMedicao.setDescricaoGrupoFaturamento(descricaoGrupoFaturamento);
			dadosMedicao.setNumeroRota(numeroRota);
			dadosMedicao.setQuantidadePontoConsumo(quantidadePontoConsumo);
			dadosMedicao.setProcessado(processado);
			dadosMedicao.setChaveRota(chaveRota);
			dadosMedicao.setAnoMesFaturamento((Integer) objeto[1]);

			listaDadosMedicao.add(dadosMedicao);

		}

		return listaDadosMedicao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.dadosmedicao.negocio.ControladorDadosMedicao#carregarDadosAlterados(java.lang.Long, java.lang.Integer,
	 * java.lang.Long,
	 * java.lang.String[], java.lang.Integer[], java.math.BigDecimal[], java.lang.Long[], java.math.BigDecimal[], java.lang.Long[],
	 * java.lang.Long[],
	 * java.lang.String[])
	 */
	@Override
	public Collection<LeituraMovimento> carregarDadosAlterados(Long chaveRota, Integer anoMesFaturamento, Long chaveLeiturista,
					String[] dataLeitura, Integer[] valorLeitura, BigDecimal[] valorPressao, Long[] valorUnidadePressao, 
					BigDecimal[] valorTemperatura, Long[] valorUnidadeTemperatura, Long[] anormaLidadeLeituraAnterior, 
					String[] observacoesCadastrais, Long[] chavesPrimarias) throws GGASException {

		Collection<LeituraMovimento> listaLeituraMovimento = 
						this.consultarLeituraMovimentoPorRota(chaveRota, anoMesFaturamento, chavesPrimarias, null);

		SituacaoLeituraMovimento situacaoLeituraMovimento = controladorLeituraMovimento
						.obterSituacaoLeituraMovimento(SituacaoLeituraMovimento.LEITURA_RETORNADA);

		int cont = 0;
		for (LeituraMovimento leituraMovimento : listaLeituraMovimento) {

			SituacaoLeituraMovimento situacaoLeitura = leituraMovimento.getSituacaoLeitura();

			if(situacaoLeitura != null && situacaoLeitura.getChavePrimaria() != SituacaoLeituraMovimento.PROCESSADO) {
				
				leituraMovimento.setSituacaoLeitura(situacaoLeituraMovimento);

				String data = null;
				if(dataLeitura.length > 0 && cont < dataLeitura.length) {
					data = dataLeitura[cont];
					if(Util.isDataValida(data, Constantes.FORMATO_DATA_BR)) {
						data = data.replace("/", "").replace("_", "");
						if(data.length() == 8) {
							Date date = Util.converterCampoStringParaData("Data da Leitura", dataLeitura[cont], Constantes.FORMATO_DATA_BR);
							leituraMovimento.setDataLeitura(date);
						}
					}
				}

				Integer valor = null;
				if(valorLeitura.length > 0 && cont < valorLeitura.length) {
					valor = valorLeitura[cont];
				}

				Long chaveUnidadePressao = null;
				if(valorUnidadePressao.length > 0 && cont < valorUnidadePressao.length) {
					chaveUnidadePressao = valorUnidadePressao[cont];
				}

				Long chaveUnidadeTemperatura = null;
				if(valorUnidadeTemperatura.length > 0 && cont < valorUnidadeTemperatura.length) {
					chaveUnidadeTemperatura = valorUnidadeTemperatura[cont];
				}

				Long chaveAnormalidade = null;
				if(anormaLidadeLeituraAnterior.length > 0 && cont < anormaLidadeLeituraAnterior.length) {
					chaveAnormalidade = anormaLidadeLeituraAnterior[cont];
				}

				String obs = null;
				if(observacoesCadastrais.length > 0 && cont < observacoesCadastrais.length) {
					obs = observacoesCadastrais[cont];
				}

				leituraMovimento.setDataNovaLeitura(data);
				leituraMovimento.setObservacoesCadastrais(obs);

				if(valor != null) {
					BigDecimal bigDecimal = new BigDecimal(valor);
					leituraMovimento.setValorLeitura(bigDecimal);
				} else {
					leituraMovimento.setValorLeitura(null);
				}

				if((valorPressao.length > 0 && cont < valorPressao.length) && valorPressao[cont] != null) {

					leituraMovimento.setPressaoInformada(valorPressao[cont]);
				}

				if(chaveUnidadePressao != null && chaveUnidadePressao > 0) {
					Unidade unidadePressao = (Unidade) controladorUnidade.obter(chaveUnidadePressao);
					leituraMovimento.setUnidadePressaoInformada(unidadePressao);
				} else {
					leituraMovimento.setUnidadePressaoInformada(null);
				}

				if((valorTemperatura.length > 0 && cont < valorTemperatura.length) && valorTemperatura[cont] != null) {

					leituraMovimento.setTemperaturaInformada(valorTemperatura[cont]);
				}

				if(chaveUnidadeTemperatura != null && chaveUnidadeTemperatura > 0) {
					Unidade unidadeTemperatura = (Unidade) controladorUnidade.obter(chaveUnidadeTemperatura);
					leituraMovimento.setUnidadeTemperaturaInformada(unidadeTemperatura);
				} else {
					leituraMovimento.setUnidadeTemperaturaInformada(null);
				}

				if(chaveAnormalidade != null && chaveAnormalidade > 0) {
					AnormalidadeLeitura anormalidadeLeitura = controladorAnormalidade.obterAnormalidadeLeitura(chaveAnormalidade);
					leituraMovimento.setAnormalidadeLeituraAnterior(anormalidadeLeitura);
				} else {
					leituraMovimento.setAnormalidadeLeituraAnterior(null);
				}
				if(chaveLeiturista != null && chaveLeiturista > 0) {
					Leiturista leiturista = (Leiturista) controladorLeiturista.obter(chaveLeiturista);
					leituraMovimento.setLeiturista(leiturista);
				} else {
					leituraMovimento.setLeiturista(null);
				}
				cont++;
			}
		}

		return listaLeituraMovimento;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.dadosmedicao.negocio.ControladorDadosMedicao#obterArquivoLeituraMovimentoPorLeiturista(br.com.ggas.medicao.leitura
	 * .Leiturista
	 * , java.lang.String, java.lang.String)
	 */
	@Override
	public String obterArquivoLeituraMovimentoPorLeiturista(Leiturista leiturista, String dataLeituraPrevista, String numeroRota)
					throws GGASException {

		Long chaveLeiturista = leiturista.getChavePrimaria();

		Collection<LeituraMovimento> listaLeituraMovimento = repositorioDadosMedicao.consultarLeituraMovimentoPorLeiturista(
						chaveLeiturista, dataLeituraPrevista, numeroRota);

		SituacaoLeituraMovimento situacaoLeituraMovimento = controladorLeituraMovimento
						.obterSituacaoLeituraMovimento(SituacaoLeituraMovimento.EM_LEITURA);
		for (LeituraMovimento leituraMovimento : listaLeituraMovimento) {
			leituraMovimento.setSituacaoLeitura(situacaoLeituraMovimento);
			controladorLeituraMovimento.atualizar(leituraMovimento);
		}

		@SuppressWarnings("unchecked")
		Collection<AnormalidadeLeitura> listaAnormalidade = (Collection<AnormalidadeLeitura>) controladorAnormalidade.obterTodas(
						AnormalidadeLeituraImpl.class, true);

		Document document = new Document();

		Element elementImportacao = new Element("importacao");

		for (LeituraMovimento leituraMovimento : listaLeituraMovimento) {

			Long chaveLeituraMovimento = leituraMovimento.getChavePrimaria();
			String rota = leituraMovimento.getNumeroRota();
			Integer anoMesFaturamento = leituraMovimento.getAnoMesFaturamento();
			Integer ciclo = leituraMovimento.getCiclo();
			Long chavePontoConsumo = leituraMovimento.getPontoConsumo().getChavePrimaria();
			BigDecimal leituraAnteriror = leituraMovimento.getLeituraAnterior();
			String numeroSerieMedidor = leituraMovimento.getNumeroSerieMedidor();
			String endereco = leituraMovimento.getEndereco();
			SituacaoConsumo situacaoConsumo = leituraMovimento.getSituacaoPontoConsumo();
			Long chaveSituacaoConsumo = situacaoConsumo.getChavePrimaria();
			String descricaoSituacaoConsumo = situacaoConsumo.getDescricao();
			Imovel imovel = leituraMovimento.getImovel();
			PontoConsumo pontoConsumo = leituraMovimento.getPontoConsumo();
			Long chaveImovel = imovel.getChavePrimaria();
			String leituraPrevista = Util.converterDataParaStringSemHora(leituraMovimento.getDataLeituraPrevista(),
							Constantes.FORMATO_DATA_BR);
			String local = leituraMovimento.getLocalInstalacaoMedidor();
			Integer qtdDigitosMedidor = leituraMovimento.getInstalacaoMedidor().getMedidor().getDigito();
			BigDecimal minimoLeituraEsperada = BigDecimal.ZERO;
			BigDecimal maximoLeituraEsperada = BigDecimal.ZERO;
			
			if(leituraMovimento.getMinimoLeituraEsperada() != null) {
				minimoLeituraEsperada = leituraMovimento.getMinimoLeituraEsperada().setScale(BigDecimal.ROUND_HALF_UP);
			}
			
			if(leituraMovimento.getMaximoLeituraEsperada() != null) {
				maximoLeituraEsperada = leituraMovimento.getMaximoLeituraEsperada().setScale(BigDecimal.ROUND_HALF_UP);
			}
			String descricaoComplemento = "";
			if(imovel.getDescricaoComplemento() != null) {
				descricaoComplemento = imovel.getDescricaoComplemento();
			}

			Element elementLeitura = new Element("leitura");

			elementLeitura.setAttribute("chaveLeituraMovimento", chaveLeituraMovimento.toString());
			elementLeitura.setAttribute("chaveLeiturista", chaveLeiturista.toString());
			elementLeitura.setAttribute("anoMesFaturamento", anoMesFaturamento.toString());
			elementLeitura.setAttribute("ciclo", ciclo.toString());
			elementLeitura.setAttribute("pontoConsumo", chavePontoConsumo.toString());
			elementLeitura.setAttribute("matriculaImovel", chaveImovel.toString());
			elementLeitura.setAttribute("nomeImovel", imovel.getNome() + " :: " + pontoConsumo.getDescricao());
			elementLeitura.setAttribute("numeroRota", rota);
			
			if (leituraAnteriror != null) {
				elementLeitura.setAttribute("leituraAnteriror", leituraAnteriror.toString());
			} else {
				elementLeitura.setAttribute("leituraAnteriror", "");
			}
			
			elementLeitura.setAttribute("numeroSerieMedidor", numeroSerieMedidor);
			elementLeitura.setAttribute("endereco", endereco);
			elementLeitura.setAttribute("enderecoComplemento", descricaoComplemento);
			elementLeitura.setAttribute("chaveSituacaoConsumo", chaveSituacaoConsumo.toString());
			elementLeitura.setAttribute("descricaoSituacaoConsumo", descricaoSituacaoConsumo);
			elementLeitura.setAttribute("dataLeituraPrevista", leituraPrevista);
			elementLeitura.setAttribute("local", local);
			elementLeitura.setAttribute("qtdDigitosMedidor", qtdDigitosMedidor.toString());
			elementLeitura.setAttribute("minimoLeituraEsperada", minimoLeituraEsperada.toString());
			elementLeitura.setAttribute("maximoLeituraEsperada", maximoLeituraEsperada.toString());

			elementImportacao.addContent(elementLeitura);
		}

		for (AnormalidadeLeitura anormalidadeLeitura : listaAnormalidade) {
			Long chaveAnormalidade = anormalidadeLeitura.getChavePrimaria();
			String descricaoAnormalidade = anormalidadeLeitura.getDescricao();

			Element elementAnormalidade = new Element("anormalidade");

			elementAnormalidade.setAttribute("chaveAnormalidade", chaveAnormalidade.toString());
			elementAnormalidade.setAttribute("descricaoAnormalidade", descricaoAnormalidade);

			elementImportacao.addContent(elementAnormalidade);
		}

		document.setRootElement(elementImportacao);

		XMLOutputter xout = new XMLOutputter();
		xout.setFormat(Format.getPrettyFormat());

		return xout.outputString(document);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.dadosmedicao.negocio.ControladorDadosMedicao#consultarLeituraMovimentoPorLeiturista(java.lang.Long,
	 * java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public Collection<LeituraMovimento> consultarLeituraMovimentoPorLeiturista(Long chaveLeiturista, String dataLeituraPrevista,
					String numeroRota) throws GGASException {

		return repositorioDadosMedicao.consultarLeituraMovimentoPorLeiturista(chaveLeiturista, dataLeituraPrevista, numeroRota);
	}

}
