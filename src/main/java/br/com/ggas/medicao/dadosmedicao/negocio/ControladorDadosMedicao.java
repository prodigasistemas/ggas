/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/09/2014 10:53:28
 @author vtavares
 */

package br.com.ggas.medicao.dadosmedicao.negocio;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Collection;
import java.util.Map;

import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.dadosmedicao.dominio.DadosMedicaoVO;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.Leiturista;
/**
 * Interface responsável pela assinatura dos métodos relacionados ao Controlador dos dados de medição
 *
 */
public interface ControladorDadosMedicao {

	/**
	 * Consultar leitura movimento.
	 * 
	 * @param dadosMedicaoVO
	 *            the dados medicao vo
	 * @return the collection
	 * @throws ParseException
	 *             the parse exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<DadosMedicaoVO> consultarLeituraMovimento(DadosMedicaoVO dadosMedicaoVO) throws ParseException, GGASException;

	/**
	 * Validar dados consulta.
	 * 
	 * @param dadosMedicao
	 *            the dados medicao
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void validarDadosConsulta(DadosMedicaoVO dadosMedicao) throws GGASException;

	/**
	 * Consultar leitura movimento por rota.
	 * 
	 * @param chaveRota
	 * 				the chaveRota
	 * @param anoMesFaturamento
	 * 				the anoMesFaturamento
	 * @param chavesPrimarias
	 * 				the chavesPrimarias
	 * @param filtro - {@link Map}
	 * @return Collection
	 * 
	 */
	Collection<LeituraMovimento> consultarLeituraMovimentoPorRota(Long chaveRota, Integer anoMesFaturamento, Long[] chavesPrimarias,
			Map<String, Object> filtro);

	/**
	 * Carregar dados alterados.
	 * 
	 * @param chaveRota
	 *            the chave rota
	 * @param anoMesFaturamento
	 *            the ano mes faturamento
	 * @param chaveLeiturista
	 *            the chave leiturista
	 * @param dataLeitura
	 *            the data leitura
	 * @param valorLeitura
	 *            the valor leitura
	 * @param valorPressao
	 *            the valor pressao
	 * @param valorUnidadePressao
	 *            the valor unidade pressao
	 * @param valorTemperatura
	 *            the valor temperatura
	 * @param valorUnidadeTemperatura
	 *            the valor unidade temperatura
	 * @param anormaLidadeLeituraAnterior
	 *            the anorma lidade leitura anterior
	 * @param observacoesCadastrais
	 *            the observacoes cadastrais
	 * @param chavesPrimarias 
 * 				  the chavesPrimarias
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<LeituraMovimento> carregarDadosAlterados(Long chaveRota, Integer anoMesFaturamento, Long chaveLeiturista,
					String[] dataLeitura, Integer[] valorLeitura, BigDecimal[] valorPressao, Long[] valorUnidadePressao, 
					BigDecimal[] valorTemperatura, Long[] valorUnidadeTemperatura, Long[] anormaLidadeLeituraAnterior, 
					String[] observacoesCadastrais, Long[] chavesPrimarias) throws GGASException;

	/**
	 * Obter arquivo leitura movimento por leiturista.
	 * 
	 * @param leiturista
	 *            the leiturista
	 * @param dataLeituraPrevista
	 *            the data leitura prevista
	 * @param numeroRota
	 *            the numero rota
	 * @return the string
	 * @throws GGASException
	 *             the GGAS exception
	 */
	String obterArquivoLeituraMovimentoPorLeiturista(Leiturista leiturista, String dataLeituraPrevista, String numeroRota)
					throws GGASException;

	/**
	 * Consultar leitura movimento por leiturista.
	 * 
	 * @param chaveLeiturista
	 *            the chave leiturista
	 * @param dataLeituraPrevista
	 *            the data leitura prevista
	 * @param numeroRota
	 *            the numero rota
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<LeituraMovimento> consultarLeituraMovimentoPorLeiturista(Long chaveLeiturista, String dataLeituraPrevista, String numeroRota)
					throws GGASException;

}
