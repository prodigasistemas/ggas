/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 @since 10/09/2014 10:54:37
 @author vtavares
 */

package br.com.ggas.medicao.dadosmedicao.repositorio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.ggas.geral.RepositorioGenerico;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.dadosmedicao.dominio.DadosMedicaoVO;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.leitura.impl.LeituraMovimentoImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * Classe Repositorio Dados Medicao.
 * 
 *
 */
@Repository
public class RepositorioDadosMedicao extends RepositorioGenerico {
	
	private static final String CHAVE_SITUACAO_LEITURA = "situacaoLeitura.chavePrimaria";
	
	private static final String SITUACAO_LEITURA = "situacaoLeitura";
	
	private static final String NUMERO_SERIE_MEDIDOR = "numeroSerieMedidor";

	private static final String ID_IMOVEL = "idImovel";

	private static final String DATA_LEITURA_FILTRO = "dataLeituraFiltro";

	/**
	 * Instantiates a new repositorio dados medicao.
	 * 
	 * @param sessionFactory
	 *            the session factory
	 */
	@Autowired
	public RepositorioDadosMedicao(SessionFactory sessionFactory) {

		setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.RepositorioGenerico#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return new LeituraMovimentoImpl();
	}

	@Override
	public Class<?> getClasseEntidade() {

		return LeituraMovimentoImpl.class;
	}

	/**
	 * Consultar leitura movimento.
	 * 
	 * @param dadosMedicaoVO
	 *            the dados medicao vo
	 * @return the collection
	 * @throws ParseException
	 *             the parse exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<Object[]> consultarLeituraMovimento(DadosMedicaoVO dadosMedicaoVO) throws ParseException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select ");
		hql.append(" count(anormalidadeLeituraAnterior.chavePrimaria) as qtdAnormalidade, ");
		hql.append(" leituraMovimento.anoMesFaturamento, ");
		hql.append(" leituraMovimento.ciclo, ");
		hql.append(" grupoFaturamento.descricao, ");
		hql.append(" rota.numeroRota, ");
		hql.append(" count(rota.chavePrimaria) as qtdPontoConsumo, ");
		hql.append(" (sum(situacaoLeitura.chavePrimaria))/(count(rota.chavePrimaria)), ");
		hql.append(" rota.chavePrimaria ");
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" leituraMovimento ");
		hql.append(" inner join leituraMovimento.rota rota ");
		hql.append(" inner join leituraMovimento.pontoConsumo pontoConsumo ");
		hql.append(" inner join leituraMovimento.grupoFaturamento grupoFaturamento ");
		hql.append(" left join  leituraMovimento.anormalidadeLeituraAnterior anormalidadeLeituraAnterior ");
		hql.append(" left join  leituraMovimento.situacaoLeitura situacaoLeitura ");
		hql.append(" where 1=1 ");

		Long rota = dadosMedicaoVO.getChaveRota();
		if(rota != null && rota > 0) {
			hql.append(" and rota.chavePrimaria = :chaveRota");
		}

		Long anormalidadeLeitura = dadosMedicaoVO.getChaveAnormalidadeLeitura();
		if(anormalidadeLeitura != null && anormalidadeLeitura > 0) {
			hql.append(" and leituraMovimento.codigoAnormalidadeLeitura = :chaveAnormalidadeLeitura ");
		}

		Long grupoFaturamento = dadosMedicaoVO.getChaveGrupoFaturamento();
		if(grupoFaturamento != null && grupoFaturamento > 0) {
			hql.append(" and grupoFaturamento.chavePrimaria = :chaveGrupoFaturamento ");
		}

		String mesAnoReferencia = dadosMedicaoVO.getMesAnoReferencia();
		if(mesAnoReferencia != null && !mesAnoReferencia.isEmpty()) {
			hql.append(" and leituraMovimento.anoMesFaturamento like :anoMesFaturamento ");
		}

		Integer ciclo = dadosMedicaoVO.getCiclo();
		if(ciclo != null && ciclo > 0) {
			hql.append(" and leituraMovimento.ciclo = :ciclo");
		}

		String indicadorConfirmacaoLeitura = dadosMedicaoVO.getConfirmacaoLeitura();
		if(indicadorConfirmacaoLeitura != null && !indicadorConfirmacaoLeitura.isEmpty()) {
			hql.append(" and leituraMovimento.indicadorConfirmacaoLeitura = :indicadorConfirmacaoLeitura ");
		}

		if((dadosMedicaoVO.getDataLeituraInicio() != null && !dadosMedicaoVO.getDataLeituraInicio().isEmpty())
						&& (dadosMedicaoVO.getDataLeituraFim() != null && !dadosMedicaoVO.getDataLeituraFim().isEmpty())) {
			hql.append(" and (leituraMovimento.dataLeitura between :dataLeituraInicio and :dataLeituraFim) ");
		}

		String indicadorProcessado = dadosMedicaoVO.getIndicadorProcessado();
		if(indicadorProcessado != null && !indicadorProcessado.isEmpty()) {
			if ("4".equals(dadosMedicaoVO.getIndicadorProcessado())) {
				hql.append(" and situacaoLeitura.chavePrimaria = :processado ");
			} else {
				hql.append(" and situacaoLeitura.chavePrimaria != 4 ");
			}
		}

		hql.append(" group by leituraMovimento.anoMesFaturamento, leituraMovimento.ciclo, "
						+ "grupoFaturamento.descricao, rota.numeroRota, rota.chavePrimaria ");
		hql.append(" order by leituraMovimento.anoMesFaturamento, leituraMovimento.ciclo, grupoFaturamento.descricao, rota.numeroRota ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(rota != null && rota > 0) {
			query.setParameter("chaveRota", rota);
		}

		if(anormalidadeLeitura != null && anormalidadeLeitura > 0) {
			query.setParameter("chaveAnormalidadeLeitura", anormalidadeLeitura);
		}

		if(grupoFaturamento != null && grupoFaturamento > 0) {
			query.setParameter("chaveGrupoFaturamento", grupoFaturamento);
		}

		if(mesAnoReferencia != null && !mesAnoReferencia.isEmpty()) {
			mesAnoReferencia = mesAnoReferencia.replace("_", "");
			String anoMes = Util.converterMesAnoEmAnoMes(mesAnoReferencia);
			query.setParameter("anoMesFaturamento", Integer.parseInt(anoMes));
		}

		if(ciclo != null && ciclo > 0) {
			query.setParameter("ciclo", ciclo);
		}

		if(indicadorConfirmacaoLeitura != null && !indicadorConfirmacaoLeitura.isEmpty()) {
			query.setParameter("indicadorConfirmacaoLeitura", Boolean.parseBoolean(indicadorConfirmacaoLeitura));
		}

		SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
		if((dadosMedicaoVO.getDataLeituraInicio() != null && !dadosMedicaoVO.getDataLeituraInicio().isEmpty())
						&& (dadosMedicaoVO.getDataLeituraFim() != null && !dadosMedicaoVO.getDataLeituraFim().isEmpty())) {

			Date dataLeituraInicio = formatoData.parse(dadosMedicaoVO.getDataLeituraInicio());
			Date dataLeituraFim = formatoData.parse(dadosMedicaoVO.getDataLeituraFim());

			Util.adicionarRestricaoDataSemHora(query, dataLeituraInicio, "dataLeituraInicio", Boolean.TRUE);
			Util.adicionarRestricaoDataSemHora(query, dataLeituraFim, "dataLeituraFim", Boolean.FALSE);

		}
		if (indicadorProcessado != null && !indicadorProcessado.isEmpty()
				&& "4".equals(dadosMedicaoVO.getIndicadorProcessado())) {
			query.setParameter("processado", Long.parseLong(indicadorProcessado));
		}

		return query.list();
	}

	/**
	 *  Consultar leitura movimento por rota.
	 * 
	 * @param chaveRota
	 * 				the chaveRota
	 * @param anoMesFaturamento
	 * 				the anoMesFaturamento
	 * @param chavesPrimarias
	 * 				the chavesPrimarias
	 * @param filtro
	 * 				the filtro
	 * @return Collection
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Collection<LeituraMovimento> consultarLeituraMovimentoPorRota(Long chaveRota, Integer anoMesFaturamento,
					Long[] chavesPrimarias, Map<String, Object> filtro) {
		

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode("rota", FetchMode.JOIN);
		criteria.setFetchMode("anormalidadeLeitura", FetchMode.JOIN);
		criteria.setFetchMode("grupoFaturamento", FetchMode.JOIN);
		criteria.setFetchMode("pontoConsumo", FetchMode.JOIN);
		criteria.setFetchMode(SITUACAO_LEITURA, FetchMode.JOIN);
		criteria.setFetchMode("instalacaoMedidor", FetchMode.JOIN);
		criteria.setFetchMode("imovel", FetchMode.JOIN);
		criteria.createAlias("instalacaoMedidor.medidor", "medidor", Criteria.INNER_JOIN);		
		
		
		if (chavesPrimarias!=null && chavesPrimarias.length>0) {
			criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
		}
		
		if(chaveRota != null) {
			criteria.add(Restrictions.eq("rota.chavePrimaria", chaveRota));
		}

		if(anoMesFaturamento != null) {
			criteria.add(Restrictions.eq("anoMesFaturamento", anoMesFaturamento));
		}
		
		aplicaFiltro(filtro, criteria);
		
		criteria.addOrder(Order.asc("sequencialLeitura"));

		return criteria.list();
	}

	private void aplicaFiltro(Map<String, Object> filtro, Criteria criteria) {
		Long[] situacaoLeitura = null;
		String numeroSerieMedidor = null;
		Long idImovel = null;
		Date dataLeituraFiltro = null;
		if (filtro != null) {
			situacaoLeitura = (Long[]) filtro.get(SITUACAO_LEITURA);
			numeroSerieMedidor = (String) filtro.get(NUMERO_SERIE_MEDIDOR);
			idImovel = (Long) filtro.get(ID_IMOVEL);
			dataLeituraFiltro = (Date) filtro.get(DATA_LEITURA_FILTRO);
		}

		if(situacaoLeitura != null) {
			criteria.add(Restrictions.in(CHAVE_SITUACAO_LEITURA, situacaoLeitura));
		}
		
		if(numeroSerieMedidor != null && !"".equals(numeroSerieMedidor)) {
			criteria.add(Restrictions.eq("medidor.numeroSerie", numeroSerieMedidor));
		}
		
		if(idImovel != null) {
			criteria.add(Restrictions.eq("imovel.chavePrimaria", idImovel));
		}
		
		if(dataLeituraFiltro != null) {
			criteria.add(Restrictions.eq("dataLeitura", dataLeituraFiltro));
		}
	}

	/**
	 * Consultar leitura movimento por leiturista.
	 * 
	 * @param chaveLeiturista
	 *            the chave leiturista
	 * @param dataLeituraPrevista
	 *            the data leitura prevista
	 * @param numeroRota
	 *            the numero rota
	 * @return the collection
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@SuppressWarnings("unchecked")
	public Collection<LeituraMovimento> consultarLeituraMovimentoPorLeiturista(Long chaveLeiturista, String dataLeituraPrevista,
					String numeroRota) throws GGASException {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode("imovel", FetchMode.JOIN);
		criteria.setFetchMode("leiturista", FetchMode.JOIN);
		criteria.setFetchMode("situacaoPontoConsumo", FetchMode.JOIN);
		criteria.setFetchMode(SITUACAO_LEITURA, FetchMode.JOIN);
		criteria.setFetchMode("instalacaoMedidor", FetchMode.JOIN);
		criteria.setFetchMode("instalacaoMedidor.medidor", FetchMode.JOIN);

		criteria.add(Restrictions.eq("leiturista.chavePrimaria", chaveLeiturista));

		if(dataLeituraPrevista != null && !dataLeituraPrevista.isEmpty()) {
			Date dataPrevista = Util.converterCampoStringParaData("Data Leitura Prevista", dataLeituraPrevista, Constantes.FORMATO_DATA_BR);
			criteria.add(Restrictions.eq("dataLeituraPrevista", dataPrevista));
		}

		if(numeroRota != null && !numeroRota.isEmpty()) {
			criteria.add(Restrictions.eq("numeroRota", numeroRota));
		}

		Criterion restricao1 = Restrictions.eq(CHAVE_SITUACAO_LEITURA, SituacaoLeituraMovimento.GERADO);
		Criterion restricao2 = Restrictions.eq(CHAVE_SITUACAO_LEITURA, SituacaoLeituraMovimento.EM_LEITURA);
		criteria.add(Restrictions.or(restricao1, restricao2));
		
		criteria.addOrder(Order.asc("sequencialLeitura"));

		return criteria.list();
	}

}
