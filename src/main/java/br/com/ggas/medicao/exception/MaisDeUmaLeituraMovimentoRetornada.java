package br.com.ggas.medicao.exception;

/**
 * Exceção lançada para os casos em que mais de uma leitura movimento é retornada pelo código legado e o status para realizar a medição
 */
public class MaisDeUmaLeituraMovimentoRetornada extends Exception {

	/**
	 * Construtor da classe.
	 * 
	 * @param mensagem
	 * 				the mensagem
	 * 
	 */
	public MaisDeUmaLeituraMovimentoRetornada(String mensagem) {
		super(mensagem);
	}
}
