package br.com.ggas.medicao.exception;

/**
 * Exceção utilizada para os casos em que vários pontos de consumo estão utilizando o mesmo código legado
 */
public class CodigoLegadoEmVariosPontosConsumo extends Exception {

	/**
	 * Construtor da classe.
	 * 
	 * @param mensagem
	 * 				the mensagem
	 * 
	 */
	public CodigoLegadoEmVariosPontosConsumo(String mensagem) {
		super(mensagem);
	}
}
