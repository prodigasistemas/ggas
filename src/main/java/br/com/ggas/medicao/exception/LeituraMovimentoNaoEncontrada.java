package br.com.ggas.medicao.exception;

/**
 * Exceção lançada para os casos em que a leitura movimento não é encontrado com base no código legado
 */
public class LeituraMovimentoNaoEncontrada extends Exception {

	/**
	 * Construtor da classe.
	 * 
	 * @param mensagem
	 * 				the mensagem
	 * 
	 */
	public LeituraMovimentoNaoEncontrada(String mensagem) {
		super(mensagem);
	}
}
