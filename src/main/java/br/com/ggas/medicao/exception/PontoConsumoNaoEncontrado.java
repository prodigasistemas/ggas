package br.com.ggas.medicao.exception;

/**
 * Exceção lançada quando o ponto de consumo não é encontrado com base no código legado
 */
public class PontoConsumoNaoEncontrado extends Exception {

	/**
	 * Construtor da classe.
	 * 
	 * @param mensagem
	 * 				the mensagem
	 * 
	 */
	public PontoConsumoNaoEncontrado(String mensagem) {
		super(mensagem);
	}
}
