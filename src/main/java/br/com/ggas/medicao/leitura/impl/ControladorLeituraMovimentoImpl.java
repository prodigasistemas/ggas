/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * A classe ControladorLeituraMovimentoImpl representa uma ControladorLeituraMovimentoImpl no sistema.
 *
 * @since 29/09/2009
 *
 */

package br.com.ggas.medicao.leitura.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.jdom.Element;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;

import com.google.common.collect.ImmutableMap;

import br.com.ggas.api.dto.RotaDTO;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.endereco.ControladorEndereco;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.imovel.ContatoImovel;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.localidade.impl.LocalidadeImpl;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.anomalia.HistoricoAnomaliaFaturamento;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.impl.CronogramaAtividadeFaturamentoImpl;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.InfraestruturaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.supervisorio.ControladorSupervisorio;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.batch.GerarDadosPontoConsumoVO;
import br.com.ggas.medicao.batch.GerarDadosVO;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.ControladorTemperaturaPressaoMedicao;
import br.com.ggas.medicao.consumo.FaixaConsumoVariacao;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.TemperaturaPressaoMedicao;
import br.com.ggas.medicao.exception.CodigoLegadoEmVariosPontosConsumo;
import br.com.ggas.medicao.exception.LeituraMovimentoNaoEncontrada;
import br.com.ggas.medicao.exception.MaisDeUmaLeituraMovimentoRetornada;
import br.com.ggas.medicao.exception.PontoConsumoNaoEncontrado;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.ControladorLeiturista;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.leitura.OrigemLeituraMovimento;
import br.com.ggas.medicao.leitura.SituacaoLeitura;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.medidor.impl.FiltroHistoricoOperacaoMedidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.vazaocorretor.ControladorUnidade;
import br.com.ggas.medicao.vazaocorretor.ControladorVazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.FaixaPressaoFornecimento;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretorHistoricoOperacao;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.relatorio.medicao.leitura.DadosRegistrarLeiturasAnormalidades;
import br.com.ggas.util.BooleanUtil;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Fachada;
import br.com.ggas.util.GGASAliasToBeanNestedResultTransformer;
import br.com.ggas.util.Ordenacao;
import br.com.ggas.util.Pair;
import br.com.ggas.util.RelatorioUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.faturamento.leitura.dto.AnormalidadeDTO;
import br.com.ggas.web.faturamento.leitura.dto.LeituraMovimentoDTO;
import br.com.ggas.web.faturamento.leitura.dto.PesquisaLeituraMovimento;

/**
 * The Class ControladorLeituraMovimentoImpl.
 */
public class ControladorLeituraMovimentoImpl extends ControladorNegocioImpl implements ControladorLeituraMovimento {

	private static final int CHAVE_TIPO_LEITURA_COLETOR_DADOS = 3;
	public static final String CONTRATOS_ATIVOS_CARREGADOS = " contratos ativos carregados";
	public static final String ROTA_POSSUI_CRONOGRAMA_CAPTURANDO_SEQUENCIA_DO_PONTO_DE_CONSUMO = " rota possui cronograma, capturando sequência do ponto de consumo";
	public static final String TEMPERATURA_INFORMADA = "temperaturaInformada";
	public static final String PRESSAO_INFORMADA = "pressaoInformada";
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ControladorLeituraMovimentoImpl.class);

	/** The Constant CICLO. */
	private static final String CICLO = "ciclo";

	/** The Constant REFERENCIA. */
	private static final String REFERENCIA = "referencia";
	public static final String CHAVE_PRIMARIA = "chavePrimaria";
	public static final String DATA_GERACAO = "dataGeracao";
	public static final String ANO_MES_FATURAMENTO = "anoMesFaturamento";
	public static final String PONTO_CONSUMO_CHAVE_PRIMARIA = "pontoConsumo.chavePrimaria";
	public static final String PONTO_CONSUMO_DESCRICAO = "pontoConsumo.descricao";
	public static final String ROTA_CHAVE_PRIMARIA = "rota.chavePrimaria";
	public static final String ROTA_NUMERO_ROTA = "rota.numeroRota";
	public static final String SEQUENCIAL_LEITURA = "sequencialLeitura";
	public static final String ENDERECO = "endereco";
	public static final String COMPLEMENTO_EMDERECO = "complementoEmdereco";
	public static final String BAIRRO = "bairro";
	public static final String MUNICIPIO = "municipio";
	public static final String UF = "uf";
	public static final String CEP = "cep";
	public static final String NUMERO_SERIE_MEDIDOR = "numeroSerieMedidor";
	public static final String LOCAL_INSTALACAO_MEDIDOR = "localInstalacaoMedidor";
	public static final String DATA_LEITURA_PREVISTA = "dataLeituraPrevista";
	public static final String LOCAL_INSTALACAO = "localInstalacao";
	public static final String COMPLEMENTO_ENDERECO = "complementoEndereco";
	public static final String SEQUENCIA_LEITURA = "sequenciaLeitura";
	public static final String ROTA = "rota";
	public static final String ID_ROTA = "idRota";
	public static final String DESCRICAO_PONTO_CONSUMO = "descricaoPontoConsumo";
	public static final String ID_PONTO_CONSUMO = "idPontoConsumo";
	public static final String ID = "id";
	public static final String SITUACAO_LEITURA = "situacaoLeitura";
	public static final String PONTO_CONSUMO = "pontoConsumo";
	public static final String NOME = "nome";
	public static final String SITUACAO_LEITURA_CHAVE_PRIMARIA = "situacaoLeitura.chavePrimaria";
	public static final String CORRIGE_PT = "corrigePT";
	public static final String CODIGO_ANORMALIDADE_LEITURA = "codigoAnormalidadeLeitura";
	public static final String VALOR_LEITURA = "valorLeitura";
	public static final String DATA_LEITURA = "dataLeitura";
	public static final String ORIGEM = "origem";
	public static final String ID_LEITURISTA = "idLeiturista";
	public static final String ROTA_LEITURISTA = "rota.leiturista.chavePrimaria";
	public static final String DATA = "Data";
	public static final String RELATORIO_GERACAO_LEITURAS_JASPER = "relatorioGeracaoLeituras.jasper";
	public static final String RELATORIO_GERACAO_LEITURAS = "RelatorioGeracaoLeituras";
	public static final String DATA_REFERENCIA = "dataReferencia";
	public static final String NUMERO_CICLO = "numeroCiclo";
	public static final String CHAVE_ROTA = "chaveRota";
	public static final String INDICADOR_INTEGRADO = "indicadorIntegrado";
	public static final String INDICADOR_PROCESSADO = "indicadorProcessado";
	public static final String DATA_REALIZACAO_LEITURA = "dataRealizacaoLeitura ";
	public static final String MODO_USO = "modoUso";
	public static final String CODIGO_MEDIDOR_SUPERVISORIO = "codigoMedidorSupervisorio";
	public static final String LISTA_CODIGO_SUPERVISORIO = "listaCodigoSupervisorio";
	public static final int QTD_MES = 2;
	public static final int TAMANHO_TOTAL = 2;
	public static final long DIA_EM_MILISSEGUNDOS = 86400000L;
	public static final String SITUACAO_DO_CONTRATO = "situacaoContrato";
	public static final String SITUACAO_CONTRATO_ATIVO = "situacaoContratoAtivo";
	public static final String GRUPO_FATURAMENTO = "grupoFaturamento";
	public static final int QTD_DIGITOS_PARA_REMOVER = 2;
	private static final int MAX_IN = 900;
	public static final String NAO_INFORMADO = "não informado";

	private final ResourceBundleMessageSource mensagens = (ResourceBundleMessageSource) ServiceLocator.getInstancia().getBeanPorID("messageSource");
	
	@Autowired
	private ControladorParametroSistema controladorParametroSistema;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(LeituraMovimento.BEAN_ID_LEITURA_MOVIMENTO);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(LeituraMovimento.BEAN_ID_LEITURA_MOVIMENTO);
	}

	/**
	 * Gets the classe entidade fatura.
	 *
	 * @return the classe entidade fatura
	 */
	public Class<?> getClasseEntidadeFatura() {

		return ServiceLocator.getInstancia().getClassPorID(Fatura.BEAN_ID_FATURA);
	}

	/**
	 * Gets the classe entidade grupo faturamento.
	 *
	 * @return the classe entidade grupo faturamento
	 */
	public Class<?> getClasseEntidadeGrupoFaturamento() {

		return ServiceLocator.getInstancia().getClassPorID(GrupoFaturamento.BEAN_ID_GRUPO_FATURAMENTO);
	}

	/**
	 * Gets the classe entidade historico consumo.
	 *
	 * @return the classe entidade historico consumo
	 */
	public Class<?> getClasseEntidadeHistoricoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	/**
	 * Gets the classe entidade historico anomalia faturamento.
	 *
	 * @return the classe entidade historico anomalia faturamento
	 */
	public Class<?> getClasseEntidadeHistoricoAnomaliaFaturamento() {

		return ServiceLocator.getInstancia()
				.getClassPorID(HistoricoAnomaliaFaturamento.BEAN_ID_HISTORICO_ANOMALIA_FATURAMENTO);
	}

	/**
	 * Gets the classe entidade rota.
	 *
	 * @return the classe entidade rota
	 */
	public Class<?> getClasseEntidadeRota() {

		return ServiceLocator.getInstancia().getClassPorID(Rota.BEAN_ID_ROTA);
	}

	/**
	 * Gets the classe entidade historico medicao.
	 *
	 * @return the classe entidade historico medicao
	 */
	public Class<?> getClasseEntidadeHistoricoMedicao() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoMedicao.BEAN_ID_MEDICAO_HISTORICO);
	}

	/**
	 * Gets the classe entidade contrato ponto consumo.
	 *
	 * @return the classe entidade contrato ponto consumo
	 */
	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	/**
	 * Gets the classe entidade ponto consumo.
	 *
	 * @return the classe entidade ponto consumo
	 */
	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	private ControladorPontoConsumo getControladorPontoConsumo() {
		return ServiceLocator.getInstancia().getControladorPontoConsumo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura. ControladorLeituraMovimento
	 * #criarLeiturasMovimento(java .util.Collection, java.util.Date,
	 * br.com.ggas.controleacesso.Usuario, java.util.Map)
	 */
	@Override
	public Collection<LeituraMovimento> criarLeiturasMovimento(Collection<PontoConsumo> pontosConsumo,
			Date dataPrevistaLeitura,

			Usuario usuario, Map<String, Integer> sequencias, AtividadeSistema atividadeSistema, Processo processo,
			StringBuilder logProcessamento) throws GGASException {

		ControladorProcessoDocumento controladorProcessoDocumento = ServiceLocator.getInstancia()
				.getControladorProcessoDocumento();

		ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();

		ControladorProcesso controladorProcesso = ServiceLocator.getInstancia().getControladorProcesso();

		ControladorHistoricoMedicao controladorHistoricoMedicao = ServiceLocator.getInstancia()
				.getControladorHistoricoMedicao();

		Collection<LeituraMovimento> colecaoLeituraMovimento = new ArrayList<>();
		Map<Rota, String> mapRotas = new HashMap<>();
		Map<Rota, List<LeituraMovimento>> mapRotasLeituraMovimento = new HashMap<>();
		Collection<GerarDadosVO> listaGerarDadosVO = new ArrayList<>();
		GerarDadosVO gerarDadosVO;
		Rota rota;
		CronogramaRota cronogramaRota = null;
		String dataPrevistaS = null;
		Date dataPrevistaD = null;
		boolean possuiCronograma;
		Collection<GerarDadosPontoConsumoVO> listaGerarDadosPontoConsumo;

		escreverNovaLinha(logProcessamento, "Carregando os contratos ativos e os respectivos pontos de consumo...");
		Map<Long, ContratoPontoConsumo> contratoAtivoPorPontoConsumo = validarContratoAtivoPontoConsumo(
				Util.collectionParaArrayChavesPrimarias(pontosConsumo), controladorContrato);

		escreverNovaLinha(logProcessamento, String.valueOf(contratoAtivoPorPontoConsumo.size()));
		escreverLinha(logProcessamento, CONTRATOS_ATIVOS_CARREGADOS);

		escreverNovaLinha(logProcessamento, "Carregando último histórico de medição faturado...");
		Map<Long, Collection<HistoricoMedicao>> historicoPorPontoConsumo = controladorHistoricoMedicao
				.obterAtributosDoUltimoHistoricoMedicaoFaturado(Util.collectionParaArrayChavesPrimarias(pontosConsumo));
		escreverNovaLinha(logProcessamento, String.valueOf(historicoPorPontoConsumo.size()));
		escreverLinha(logProcessamento, " históricos carregados da última medição");

		for (PontoConsumo pontoConsumo : pontosConsumo) {
			rota = (Rota) controladorRota.obter(pontoConsumo.getRota().getChavePrimaria());
			possuiCronograma = false;

			final String chavePrimariaRota = "[" + rota.getChavePrimaria() + "]";

			if (!mapRotas.containsKey(rota)) {			

				cronogramaRota = controladorRota.obterCronogramaRotaAtualNaoFaturado(rota);
				if (cronogramaRota != null) {
					String dataPrevistaCronograma = cronogramaRota.getDataPrevista().toString();

					mapRotas.put(rota, dataPrevistaCronograma);
					dataPrevistaS = mapRotas.get(rota);
					dataPrevistaS = dataPrevistaS.substring(8, 10) + "/" + dataPrevistaS.substring(5, 7) + "/"
							+ dataPrevistaS.substring(0, 4);
					dataPrevistaD = Util.converterCampoStringParaData(DATA, dataPrevistaS, Constantes.FORMATO_DATA_BR);

					possuiCronograma = true;
				}
			} else {
				dataPrevistaS = mapRotas.get(rota);
				dataPrevistaS = dataPrevistaS.substring(8, 10) + "/" + dataPrevistaS.substring(5, 7) + "/"
						+ dataPrevistaS.substring(0, 4);
				dataPrevistaD = Util.converterCampoStringParaData(DATA, dataPrevistaS, Constantes.FORMATO_DATA_BR);
				possuiCronograma = true;
			}

			if (possuiCronograma) {
				Integer sequencia = sequencias.get(Long.toString(pontoConsumo.getChavePrimaria()));

				LeituraMovimento leituraMovimento = this.preencherLeituraMovimento(pontoConsumo,
						usuario.getChavePrimaria(), dataPrevistaD, sequencia, cronogramaRota,
						contratoAtivoPorPontoConsumo, historicoPorPontoConsumo);

				if (leituraMovimento.getNumeroSerieMedidor() != null) {
					colecaoLeituraMovimento.add(leituraMovimento);
				} else {
					escreverNovaLinha(logProcessamento, chavePrimariaRota + ". Ponto de Consumo: "
							+ pontoConsumo.getDescricao() + " número de série do medidor está NULO");
				}

				ConstanteSistema tipoLeituraPlanilha =
								ServiceLocator.getInstancia().getControladorConstanteSistema()
												.obterConstantePorCodigo(Constantes.C_TIPO_LEITURA_PLANILHA);

				if (tipoLeituraPlanilha != null
						&& rota.getTipoLeitura().getChavePrimaria() == Long.parseLong(tipoLeituraPlanilha.getValor())) {

					if (pontoConsumo.getImovel().getImovelCondominio() == null) {
						escreverNovaLinha(logProcessamento,
								" imóvel condomínio do imóvel do ponto de consumo está nulo, "
										+ "GerarDadosVO será criado do imóvel");
						gerarDadosVO = new GerarDadosVO();
						gerarDadosVO.setImovel(pontoConsumo.getImovel());
						gerarDadosVO.setGrupoFaturamento(rota.getGrupoFaturamento().getDescricao());
						gerarDadosVO
								.setRotaCiclo(rota.getNumeroRota() + "/" + rota.getGrupoFaturamento().getNumeroCiclo());
						gerarDadosVO.setDataLeitura(dataPrevistaS);
						gerarDadosVO.setDetalhesImovel(pontoConsumo.getImovel().getEnderecoFormatado());
						gerarDadosVO.setMatricula(String.valueOf(pontoConsumo.getImovel().getChavePrimaria()));
					} else {
						escreverNovaLinha(logProcessamento,
								" imóvel condomínio do imóvel do ponto de consumo está preenchido, "
										+ "GerarDadosVO será criado do imóvelCondominio");
						gerarDadosVO = new GerarDadosVO();
						gerarDadosVO.setImovel(pontoConsumo.getImovel().getImovelCondominio());
						gerarDadosVO.setGrupoFaturamento(rota.getGrupoFaturamento().getDescricao());
						gerarDadosVO
								.setRotaCiclo(rota.getNumeroRota() + "/" + rota.getGrupoFaturamento().getNumeroCiclo());
						gerarDadosVO.setDataLeitura(dataPrevistaS);
						gerarDadosVO.setDetalhesImovel(
								pontoConsumo.getImovel().getImovelCondominio().getEnderecoFormatado());
						gerarDadosVO.setMatricula(
								String.valueOf(pontoConsumo.getImovel().getImovelCondominio().getChavePrimaria()));
					}
					escreverNovaLinha(logProcessamento,
							" gerarDadosVO foi gerado, total de pontos está sendo atualizado para: "
									+ pontosConsumo.size());
					gerarDadosVO.setTotalPontos(String.valueOf(pontosConsumo.size()));

					if (listaGerarDadosVO.contains(gerarDadosVO)) {
						escreverNovaLinha(logProcessamento, " gerarDadosVO já está contido na lista listaGerarDadosVO");
					} else {
						escreverNovaLinha(logProcessamento,
								" gerarDadosVO NÃO está contido na lista listaGerarDadosVO, "
										+ "adicionando item na lista...");
						listaGerarDadosVO.add(gerarDadosVO);
					}

					logProcessamento.append("\n\rPontos de consumo: ");
					logProcessamento.append("\n\r");
					logProcessamento.append(pontoConsumo.getChavePrimaria());
					logProcessamento.append(" - ");
					logProcessamento.append(pontoConsumo.getDescricao());

				}

			}
		}

		for (GerarDadosVO gerarDados : listaGerarDadosVO) {
			listaGerarDadosPontoConsumo = new ArrayList<>();
			Map<String, Object> mapaAtributos = new HashMap<>();
			for (PontoConsumo pontoConsumo : pontosConsumo) {
				if ((pontoConsumo.getImovel().getImovelCondominio() != null && gerarDados.getImovel()
						.getChavePrimaria() == pontoConsumo.getImovel().getImovelCondominio().getChavePrimaria())
						|| gerarDados.getImovel().getChavePrimaria() == pontoConsumo.getImovel().getChavePrimaria()) {
					Integer seq = sequencias.get(Long.toString(pontoConsumo.getChavePrimaria()));

					LeituraMovimento leitura = this.preencherLeituraMovimento(pontoConsumo, usuario.getChavePrimaria(),
							dataPrevistaD, seq, cronogramaRota, contratoAtivoPorPontoConsumo, historicoPorPontoConsumo);

					GerarDadosPontoConsumoVO gerarDadosPontoConsumoVO = new GerarDadosPontoConsumoVO();
					gerarDadosPontoConsumoVO.setDetalhesPontoConsumo(pontoConsumo.getDescricaoFormatada());
					if (leitura.getLeituraAnterior() == null) {
						gerarDadosPontoConsumoVO.setLeituraAnterior("0");
					} else {
						gerarDadosPontoConsumoVO.setLeituraAnterior(String.valueOf(leitura.getLeituraAnterior()));
					}
					if (leitura.getInstalacaoMedidor() != null) {
						gerarDadosPontoConsumoVO
								.setNumeroMedidor(leitura.getInstalacaoMedidor().getMedidor().getNumeroSerie());
						gerarDadosPontoConsumoVO.setMarcaMedidor(
								leitura.getInstalacaoMedidor().getMedidor().getMarcaMedidor().getDescricao());
					}
					gerarDadosPontoConsumoVO.setMatricula(String.valueOf(pontoConsumo.getImovel().getChavePrimaria()));

					if (!mapaAtributos.containsKey(String.valueOf(pontoConsumo.getChavePrimaria()))) {

						listaGerarDadosPontoConsumo.add(gerarDadosPontoConsumoVO);
						mapaAtributos.put(CHAVE_PRIMARIA + pontoConsumo.getChavePrimaria(),
								pontoConsumo.getChavePrimaria());
					}
				}
			}
			gerarDados.setListaGerarDadosPontoConsumo(listaGerarDadosPontoConsumo);
		}

		if (!listaGerarDadosVO.isEmpty()) {

			Map<String, Object> parametrosJasper = new HashMap<>();

			ControladorEmpresa controladorEmpresa = (ControladorEmpresa) ServiceLocator.getInstancia()
					.getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);

			if (controladorEmpresa.obterEmpresaPrincipal().getLogoEmpresa() != null) {
				Object obj = Constantes.URL_LOGOMARCA_EMPRESA
						+ controladorEmpresa.obterEmpresaPrincipal().getChavePrimaria();
				if (obj != null) {
					parametrosJasper.put("imagem", obj);
				}
			}

			byte[] bytes = RelatorioUtil.gerarRelatorioPDF(listaGerarDadosVO, parametrosJasper,
					RELATORIO_GERACAO_LEITURAS_JASPER);

			if (bytes != null) {

				ByteArrayInputStream byteArrayInputStream;
				try {
					byteArrayInputStream = new ByteArrayInputStream(bytes);

					ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator
							.getInstancia()
							.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

					// verifica se o diretorio ja existe,
					// se nao existe cria.
					String pathDiretorioDestino = (String) controladorParametrosSistema
							.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_DIRETORIO_RELATORIO_PDF);

					File diretorioDestino = Util.getFile(pathDiretorioDestino);
					if (!diretorioDestino.exists() && !diretorioDestino.mkdirs()) {
						throw new GGASException(
								ResourceBundle.getBundle(Constantes.ARQUIVO_MENSAGENS, Constantes.LOCALE_PADRAO),
								Constantes.PARAMETRO_ERRO_SISTEMA_IMPOSSIVEL_CRIAR_DIRETORIO);

					}
					String nomeDocumento = RELATORIO_GERACAO_LEITURAS + processo.getChavePrimaria()
							+ Calendar.getInstance().getTime().getTime();
					String diretorioDocumento = pathDiretorioDestino + nomeDocumento + Constantes.EXTENSAO_ARQUIVO_PDF;

					controladorProcesso.salvarRelatorio(processo, controladorProcessoDocumento, byteArrayInputStream,
							nomeDocumento, diretorioDocumento);
				} catch (Exception e) {
					throw new InfraestruturaException(e.getMessage(), e);
				}

			}
		}

		boolean temCronogramaAtualizadoDaRota = false;
		logProcessamento.append("\n\r");
		for (Map.Entry<Rota, List<LeituraMovimento>> entry : mapRotasLeituraMovimento.entrySet()) {
			if (entry.getKey().getPontosConsumo().size() == entry.getValue().size()) {
				temCronogramaAtualizadoDaRota = true;
				logProcessamento.append("\nCronograma da Rota ").append(entry.getKey().getNumeroRota())
						.append(" para a atividade GERAR DADOS PARA LEITURA atualizado com sucesso.");
			} else {
				logProcessamento.append("\nCronograma da Rota ").append(entry.getKey().getNumeroRota())
						.append(" para a atividade GERAR DADOS PARA LEITURA NÃO FOI ATUALIZADO");
			}
		}

		return colecaoLeituraMovimento;
	}

	/**
	 * Preencher leitura movimento.
	 *
	 * @param pontoConsumo                 the ponto consumo
	 * @param usuario                      the usuario
	 * @param leituraPrevista              the leitura prevista
	 * @param sequencia                    the sequencia
	 * @param cronogramaRota               the cronograma rota
	 * @param contratoAtivoPorPontoConsumo - {@link Map}
	 * @param historicoPorPontoConsumo     - {@link Map}
	 * @return the leitura movimento
	 * @throws NegocioException the negocio exception
	 */
	private LeituraMovimento preencherLeituraMovimento(PontoConsumo pontoConsumo, Long usuario, Date leituraPrevista,
			Integer sequencia, CronogramaRota cronogramaRota,
			Map<Long, ContratoPontoConsumo> contratoAtivoPorPontoConsumo,
			Map<Long, Collection<HistoricoMedicao>> historicoPorPontoConsumo) throws NegocioException {

		LeituraMovimento leituraMovimento = criarNovaLeituraOuObterExistente(pontoConsumo, cronogramaRota);

		ControladorUsuario controladorUsuario = ServiceLocator.getInstancia().getControladorUsuario();

		Imovel imovel = pontoConsumo.getImovel();
		Rota rota = pontoConsumo.getRota();
		leituraMovimento.setDataLeituraPrevista(leituraPrevista);
		leituraMovimento.setDataGeracao(Calendar.getInstance().getTime());
		leituraMovimento.setPontoConsumo(pontoConsumo);
		leituraMovimento.setDescicaoPontoConsumo(pontoConsumo.getDescricao());
		leituraMovimento.setObservacaoPontoConsumo(pontoConsumo.getObservacao());
		leituraMovimento.setSituacaoPontoConsumo(pontoConsumo.getSituacaoConsumo());
		leituraMovimento.setLatitude(pontoConsumo.getLatitudeGrau());
		leituraMovimento.setLongitude(pontoConsumo.getLongitudeGrau());

		leituraMovimento.setSequencialLeitura(sequencia);
		leituraMovimento.setImovel(imovel);

		if (pontoConsumo.getSegmento() != null) {
			leituraMovimento.setSegmento(pontoConsumo.getSegmento().getDescricao());
		}
		if (pontoConsumo.getInstalacaoMedidor() != null) {
			InstalacaoMedidor instalacaoMedidor = pontoConsumo.getInstalacaoMedidor();
			Medidor medidor = instalacaoMedidor.getMedidor();

			if (leituraMovimento.getLeituraAnterior() == null
					|| leituraMovimento.getLeituraAnterior().compareTo(BigDecimal.ZERO) == 0) {
				leituraMovimento.setLeituraAnterior(instalacaoMedidor.getLeituraAtivacao());
			}

			leituraMovimento.setInstalacaoMedidor(instalacaoMedidor);
			leituraMovimento.setNumeroSerieMedidor(medidor.getNumeroSerie());
			leituraMovimento.setDataInstalacaoMedidor(instalacaoMedidor.getData());
			if (instalacaoMedidor.getLocalInstalacao() != null) {
				leituraMovimento.setLocalInstalacaoMedidor(instalacaoMedidor.getLocalInstalacao().getDescricao());
			}
		}
		this.preencherLeituraComInfoImovel(leituraMovimento, imovel);
		this.preencherLeituraComInfoRota(leituraMovimento, rota);
		this.preencherLeituraComInfoCliente(leituraMovimento, pontoConsumo, contratoAtivoPorPontoConsumo);
		this.preencherLeituraComInfoContatoImovel(leituraMovimento, imovel);
		this.preencherLeituraComInfoHistoricoMedicao(leituraMovimento, cronogramaRota, historicoPorPontoConsumo);
		this.preencherLeituraComInfoHistoricoConsumo(pontoConsumo, leituraMovimento);
		this.preencherLeituraComInfoEnderecoImovel(leituraMovimento, imovel, pontoConsumo);
		this.preencherLeituraComInfoTemperaturaPressao(leituraMovimento, pontoConsumo, contratoAtivoPorPontoConsumo);

		boolean isVariacaoValida = leituraMovimento.getMinimoVariacaoConsumo() != null
				&& leituraMovimento.getMaximoVariacaoConsumo() != null;
		if (leituraMovimento.getLeituraAnterior() != null && isVariacaoValida) {
			leituraMovimento.setMinimoLeituraEsperada(
					leituraMovimento.getLeituraAnterior().add(leituraMovimento.getMinimoVariacaoConsumo()));
			leituraMovimento.setMaximoLeituraEsperada(
					leituraMovimento.getLeituraAnterior().add(leituraMovimento.getMaximoVariacaoConsumo()));
		}

		Date dataPrevistaProximaLeitura = this.obterDataPrevistaProximaLeitura(rota, leituraPrevista, leituraMovimento);

		leituraMovimento.setDataProximaLeituraPrevista(dataPrevistaProximaLeitura);

		Usuario user = (Usuario) controladorUsuario.obter(usuario);
		leituraMovimento.setUsuario(user);

		SituacaoLeituraMovimento situacaoLeituraMovimento = this
				.obterSituacaoLeituraMovimento(SituacaoLeituraMovimento.GERADO);
		leituraMovimento.setSituacaoLeitura(situacaoLeituraMovimento);
		leituraMovimento.setOrigem(OrigemLeituraMovimento.GERAR_DADOS.name());
		return leituraMovimento;
	}

	private LeituraMovimento criarNovaLeituraOuObterExistente(PontoConsumo pontoConsumo,
			CronogramaRota cronogramaRota) {
		LeituraMovimento leituraMovimento = (LeituraMovimento) this.criar();
		final LeituraMovimento leituraJaExistente = this.consultarPorSituacaoAnoMesCicloPontoConsumo(null, pontoConsumo,
				cronogramaRota.getAnoMesReferencia(), cronogramaRota.getNumeroCiclo());

		if (leituraJaExistente != null) {
			leituraMovimento = leituraJaExistente;
		}

		return leituraMovimento;
	}

	/**
	 * Preencher leitura com info historico consumo.
	 *
	 * @param pontoConsumo     the ponto consumo
	 * @param leituraMovimento the leitura movimento
	 */
	private void preencherLeituraComInfoHistoricoConsumo(PontoConsumo pontoConsumo, LeituraMovimento leituraMovimento) {

		ControladorHistoricoConsumo controladorHistoricoConsumo = ServiceLocator.getInstancia()
				.getControladorHistoricoConsumo();

		HistoricoConsumo historicoConsumo = controladorHistoricoConsumo.obterPorPontoConsumo(pontoConsumo);

		if (historicoConsumo != null) {
			BigDecimal consumoMedio = historicoConsumo.getConsumoMedio();

			if (consumoMedio != null) {
				leituraMovimento.setConsumoMedio(consumoMedio);
			} else {
				leituraMovimento.setConsumoMedio(BigDecimal.ZERO);
			}

			// variacao
			Collection<FaixaConsumoVariacao> faixasVariacao = controladorHistoricoConsumo
					.listarFaixasConsumoPorSegmentoFaixaConsumo(pontoConsumo.getSegmento(), consumoMedio);
			if (!faixasVariacao.isEmpty()) {
				FaixaConsumoVariacao faixaConsumoVariacaoAtual = faixasVariacao.iterator().next();
				BigDecimal minimoVariacaoConsumo = consumoMedio
						.multiply((faixaConsumoVariacaoAtual.getPercentualInferior()
								.divide(new BigDecimal(100), 2, RoundingMode.HALF_UP).subtract(BigDecimal.ONE)).abs());
				BigDecimal maximoVariacaoConsumo = consumoMedio
						.multiply(faixaConsumoVariacaoAtual.getPercentualSuperior()
								.divide(new BigDecimal(100), 2, RoundingMode.HALF_UP).add(BigDecimal.ONE));
				leituraMovimento.setMinimoVariacaoConsumo(minimoVariacaoConsumo);
				leituraMovimento.setMaximoVariacaoConsumo(maximoVariacaoConsumo);
			}

			leituraMovimento.setConsumoAnterior(historicoConsumo.getConsumoMedido());
		} else {
			leituraMovimento.setConsumoMedio(BigDecimal.ZERO);
		}
	}

	/**
	 * Preencher leitura com info historico medicao.
	 *
	 * @param leituraMovimento         the leitura movimento
	 * @param cronogramaRota           the cronograma rota
	 * @param historicoPorPontoConsumo - {@link Map}
	 * @throws NegocioException the negocio exception
	 */
	private void preencherLeituraComInfoHistoricoMedicao(LeituraMovimento leituraMovimento,
			CronogramaRota cronogramaRota, Map<Long, Collection<HistoricoMedicao>> historicoPorPontoConsumo)
			throws NegocioException {

		Integer referencia = cronogramaRota.getAnoMesReferencia();
		Integer ciclo = cronogramaRota.getNumeroCiclo();

		ControladorHistoricoMedicao controladorHistoricoMedicao = ServiceLocator.getInstancia()
				.getControladorHistoricoMedicao();

		HistoricoMedicao historicoMedicao = Util
				.primeiroElemento(historicoPorPontoConsumo.get(leituraMovimento.getPontoConsumo().getChavePrimaria()));

		if (historicoMedicao != null) {
			Date dataLeituraAnterior = historicoMedicao.getDataLeituraFaturada();
			if (dataLeituraAnterior == null) {
				dataLeituraAnterior = historicoMedicao.getDataLeituraInformada();
			}
			BigDecimal numeroLeituraAnterior = historicoMedicao.getNumeroLeituraFaturada();
			if (numeroLeituraAnterior == null) {
				numeroLeituraAnterior = historicoMedicao.getNumeroLeituraInformada();
			}

			leituraMovimento.setDataLeituraAnterior(dataLeituraAnterior);
			leituraMovimento.setLeituraAnterior(numeroLeituraAnterior);

			leituraMovimento.setAnormalidadeLeituraAnterior(historicoMedicao.getAnormalidadeLeituraFaturada());
			leituraMovimento.setSituacaoLeituraAnterior(historicoMedicao.getSituacaoLeitura().getDescricao());
		} else {

			if (leituraMovimento.getInstalacaoMedidor() != null) {

				Date dataLeitura = controladorHistoricoMedicao.setarDataLeituraAnterior(null,
						leituraMovimento.getPontoConsumo(), leituraMovimento.getInstalacaoMedidor().getMedidor());
				BigDecimal leitura = controladorHistoricoMedicao.setarLeituraAnterior(null,
						leituraMovimento.getPontoConsumo(), leituraMovimento.getInstalacaoMedidor().getMedidor());

				leituraMovimento.setDataLeituraAnterior(dataLeitura);
				leituraMovimento.setLeituraAnterior(leitura);
			}
		}

		leituraMovimento.setAnoMesFaturamento(referencia);
		leituraMovimento.setCiclo(ciclo);

	}

	/**
	 * Método responsável por obter a data prevista da próxima leitura.[RN015].
	 *
	 * @param rota              Rota
	 * @param dtPrevistaLeitura Data Prevista de Leitura.
	 * @param leituraMovimento  the leitura movimento
	 * @return CronogramaRota
	 */
	@SuppressWarnings({ "squid:S1192", "unchecked", "rawtypes" })
	private Date obterDataPrevistaProximaLeitura(Rota rota, Date dtPrevistaLeitura, LeituraMovimento leituraMovimento) {

		CronogramaRota cronogramaRota = null;
		Date dataPrevistaProximaLeitura = null;

		Class entidadeCronogramaRota = ServiceLocator.getInstancia()
				.getClassPorID(CronogramaRota.BEAN_ID_CRONOGRAMA_ROTA);

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" FROM ");
		hql.append(entidadeCronogramaRota.getSimpleName());
		hql.append(" cronograma ");
		hql.append(" WHERE cronograma.habilitado = true ");
		hql.append(" AND cronograma.rota.chavePrimaria = :idRota ");
		hql.append(" AND cronograma.dataPrevista > :dataPrevistaLeitura ");
		hql.append(
				" AND (concat(str(cronograma.anoMesReferencia), str(cronograma.numeroCiclo))) > :referenciaCicloAtual ");
		hql.append(" ORDER BY cronograma.dataPrevista ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idRota", rota.getChavePrimaria());
		query.setDate("dataPrevistaLeitura", dtPrevistaLeitura);
		query.setString("referenciaCicloAtual",
				String.valueOf(leituraMovimento.getAnoMesFaturamento()) + String.valueOf(leituraMovimento.getCiclo()));

		Collection<CronogramaRota> colecaoCronogramaRota = query.list();

		if (colecaoCronogramaRota != null && !colecaoCronogramaRota.isEmpty()) {
			cronogramaRota = (CronogramaRota) Util.primeiroElemento(query.list());
			dataPrevistaProximaLeitura = cronogramaRota.getDataPrevista();
		}

		return dataPrevistaProximaLeitura;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura. ControladorLeituraMovimento#
	 * consultarMovimentosLeituraNaoProcessados(java .lang.Long[])
	 */
	@Override
	@SuppressWarnings({ "squid:S1192", "unchecked" })
	public Collection<LeituraMovimento> consultarMovimentosLeituraNaoProcessados(Collection<Rota> rotas)
			throws NegocioException {

		Long[] idRotas = null;
		if (rotas != null) {
			idRotas = new Long[rotas.size()];
		}

		if ((rotas != null) && (idRotas != null) && (!rotas.isEmpty())) {
			Rota rota = null;
			for (int i = 0; i < rotas.size(); i++) {
				rota = ((List<Rota>) rotas).get(i);
				idRotas[i] = rota.getChavePrimaria();
			}
		}

		Collection<LeituraMovimento> listaLeituraMovimento = new ArrayList<LeituraMovimento>();

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" leituraMovimento ");
		hql.append(" where ");
		hql.append(" leituraMovimento.situacaoLeitura <> 4 ");

		if ((rotas != null) && (!rotas.isEmpty())) {
			hql.append(" and leituraMovimento.rota.chavePrimaria in (:idRotas)");
		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if ((rotas != null) && (!rotas.isEmpty())) {
			query.setParameterList("idRotas", idRotas);
		}

		listaLeituraMovimento = query.list();

		return listaLeituraMovimento;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura. ControladorLeituraMovimento#
	 * consultarPorSituacaoAnoMesCicloPontoConsumo
	 * (br.com.ggas.medicao.leitura.LeituraMovimento )
	 */
	@Override
	public LeituraMovimento consultarPorSituacaoAnoMesCicloPontoConsumo(LeituraMovimento leituraMovimento) {

		LeituraMovimento retorno = null;
		Criteria criteria = getCriteria();

		if (leituraMovimento != null) {
			if (leituraMovimento.getSituacaoLeitura() != null
					&& leituraMovimento.getSituacaoLeitura().getChavePrimaria() > 0) {
				criteria.add(Restrictions.eq("situacaoLeitura.chavePrimaria",
						leituraMovimento.getSituacaoLeitura().getChavePrimaria()));
			}

			if (leituraMovimento.getPontoConsumo() != null
					&& leituraMovimento.getPontoConsumo().getChavePrimaria() > 0) {
				criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria",
						leituraMovimento.getPontoConsumo().getChavePrimaria()));
			}

			if (leituraMovimento.getAnoMesFaturamento() != null && leituraMovimento.getAnoMesFaturamento() > 0) {
				criteria.add(Restrictions.eq("anoMesFaturamento", leituraMovimento.getAnoMesFaturamento()));
			}

			if (leituraMovimento.getCiclo() != null && leituraMovimento.getCiclo() > 0) {
				criteria.add(Restrictions.eq(CICLO, leituraMovimento.getCiclo()));
			}
		}

		Collection<LeituraMovimento> listaLeituraMovimento = criteria.list();

		if (listaLeituraMovimento.size() == 1) {
			retorno = listaLeituraMovimento.iterator().next();
		}

		return retorno;
	}

	private LeituraMovimento consultarPorSituacaoAnoMesCicloPontoConsumo(SituacaoLeituraMovimento situacao,
			PontoConsumo ponto, Integer anoMesFaturamento, Integer ciclo) {
		LeituraMovimento leitura = new LeituraMovimentoImpl();
		leitura.setSituacaoLeitura(situacao);
		leitura.setPontoConsumo(ponto);
		leitura.setAnoMesFaturamento(anoMesFaturamento);
		leitura.setCiclo(ciclo);
		return this.consultarPorSituacaoAnoMesCicloPontoConsumo(leitura);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura. ControladorLeituraMovimento
	 * #inserirLeiturasMovimento (java.util.Collection)
	 */
	@Override
	public void inserirLeiturasMovimento(final Collection<LeituraMovimento> leiturasMovimentos)
			throws NegocioException {

		// TODO - Ver a forma de inserir usando
		// StatelessSession.
		List<LeituraMovimento> listaOrdenada = Util.ordenarColecaoPorAtributo(leiturasMovimentos, "sequencialLeitura",
				true);
		for (LeituraMovimento leituraMovimento : listaOrdenada) {
			leituraMovimento.setUltimaAlteracao(Calendar.getInstance().getTime());
			// FIXME: Substituir pelo método do controlador negócio, conforme o caso
			// (inserir(), atualizar() ou remover())
			if (leituraMovimento.getChavePrimaria() > 0L) {
				try {
					super.atualizar(leituraMovimento);
				} catch (ConcorrenciaException e) {
					throw new NegocioException(e);
				}
			} else {
				super.inserir(leituraMovimento);
			}
		}

	}

	/**
	 * Preencher leitura com info contato imovel.
	 *
	 * @param leituraMovimento the leitura movimento
	 * @param imovel           the imovel
	 * @throws NegocioException the negocio exception
	 */
	private void preencherLeituraComInfoContatoImovel(LeituraMovimento leituraMovimento, Imovel imovel)
			throws NegocioException {

		ControladorImovel controladorImovel = (ControladorImovel) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL);
		String dadosContato = "";
		Collection<ContatoImovel> listaContatosImovel = controladorImovel
				.listarContatoImovelPorChaveImovel(imovel.getChavePrimaria());
		if (listaContatosImovel != null && !listaContatosImovel.isEmpty()) {

			for (ContatoImovel contatoImovel : listaContatosImovel) {

				if (contatoImovel.isPrincipal()) {
					leituraMovimento.setNomeContato(contatoImovel.getNome());

					if (contatoImovel.getFone() != null) {
						dadosContato = contatoImovel.getCodigoDDD() + contatoImovel.getFone();
					} else {
						dadosContato = contatoImovel.getEmail();
					}

					leituraMovimento.setDadosContato(dadosContato);

					break;
				}
			}
		}
	}

	/**
	 * Preencher leitura com info cliente.
	 *
	 * @param leituraMovimento             the leitura movimento
	 * @param contratoAtivoPorPontoConsumo - {@link Map}
	 * @param pontoConsumo                 the ponto consumo
	 */
	private void preencherLeituraComInfoCliente(LeituraMovimento leituraMovimento, PontoConsumo pontoConsumo,
			Map<Long, ContratoPontoConsumo> contratoAtivoPorPontoConsumo) {

		ContratoPontoConsumo contratoPontoConsumo = contratoAtivoPorPontoConsumo.get(pontoConsumo.getChavePrimaria());

		boolean existeCliente = false;
		if (contratoPontoConsumo != null && contratoPontoConsumo.getContrato().getClienteAssinatura() != null) {

			leituraMovimento.setNomeCliente(contratoPontoConsumo.getContrato().getClienteAssinatura().getNome());
			existeCliente = true;
		}
		if (!existeCliente) {
			leituraMovimento.setNomeCliente("NÃO INFORMADO");
		}

	}

	/**
	 * Preenche o objeto LeituraMovimento com os dados de temperatura e pressão
	 * contratualmente acordadas
	 * 
	 * @param leituraMovimento             O objeto que será preenchido
	 * @param pontoConsumo                 O ponto de consumo de referência
	 * @param contratoAtivoPorPontoConsumo A lista de contratos ativos
	 */
	private void preencherLeituraComInfoTemperaturaPressao(LeituraMovimento leituraMovimento, PontoConsumo pontoConsumo,
			Map<Long, ContratoPontoConsumo> contratoAtivoPorPontoConsumo) {

		ContratoPontoConsumo contratoPontoConsumo = contratoAtivoPorPontoConsumo.get(pontoConsumo.getChavePrimaria());

		if (contratoPontoConsumo != null) {
			leituraMovimento.setPressaoInformada(contratoPontoConsumo.getMedidaPressao());
			leituraMovimento.setUnidadePressaoInformada(contratoPontoConsumo.getUnidadePressao());
			ControladorTemperaturaPressaoMedicao controladorTemperaturaPressaoMedicao = (ControladorTemperaturaPressaoMedicao) ServiceLocator
					.getInstancia()
					.getBeanPorID(ControladorTemperaturaPressaoMedicao.BEAN_ID_CONTROLADOR_TEMPERATURA_PRESSAO_MEDICAO);
			TemperaturaPressaoMedicao temperaturaPressaoMedicao = controladorTemperaturaPressaoMedicao
					.obterTemperaturaMedicaoVigente(pontoConsumo.getCep().getMunicipio(), new LocalidadeImpl(),
							leituraMovimento.getDataLeituraPrevista());
			ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
					.getInstancia()
					.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
			String chaveCorrigePtNao = controladorConstanteSistema
					.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_PT_NAO);
			if (contratoPontoConsumo.getFaixaPressaoFornecimento() != null
					&& contratoPontoConsumo.getFaixaPressaoFornecimento().getIndicadorCorrecaoPT() != null) {
				final String chavePrimariaStr = String.valueOf(
						contratoPontoConsumo.getFaixaPressaoFornecimento().getIndicadorCorrecaoPT().getChavePrimaria());
				final boolean isCorrigePt = !chaveCorrigePtNao.equals(chavePrimariaStr);
				leituraMovimento.setCorrigePT(isCorrigePt);
			}
			if (temperaturaPressaoMedicao != null) {
				leituraMovimento.setTemperaturaInformada(temperaturaPressaoMedicao.getTemperatura());
				leituraMovimento.setUnidadeTemperaturaInformada(temperaturaPressaoMedicao.getUnidadeTemperatura());
			}
		}
	}

	/**
	 * Preencher leitura com info endereco imovel.
	 *
	 * @param leituraMovimento the leitura movimento
	 * @param imovel           the imovel
	 * @param pontoConsumo     the ponto consumo
	 */
	private void preencherLeituraComInfoEnderecoImovel(LeituraMovimento leituraMovimento, Imovel imovel,
			PontoConsumo pontoConsumo) {

		if (imovel != null && imovel.getQuadraFace() != null) {

			String enderecoFormatado;

			Endereco endereco = imovel.getQuadraFace().getEndereco();
			if (pontoConsumo.getCep() == null) {
				// o numero do endereço nao é o
				// numero da quadraFace, e sim do
				// imovel
				ControladorEndereco controladorEndereco = (ControladorEndereco) ServiceLocator.getInstancia()
						.getBeanPorID(ControladorEndereco.BEAN_ID_CONTROLADOR_ENDERECO);
				Endereco endFormatadoImovel = controladorEndereco.criarEndereco();
				endFormatadoImovel.setCep(endereco.getCep());
				endFormatadoImovel.setEnderecoReferencia(endereco.getEnderecoReferencia());
				endFormatadoImovel.setNumero(imovel.getNumeroImovel());
				endFormatadoImovel.setComplemento(imovel.getDescricaoComplemento());

				enderecoFormatado = endFormatadoImovel.getEnderecoFormatado();
			} else {
				enderecoFormatado = pontoConsumo.getEnderecoFormatado();
			}

			leituraMovimento.setEndereco(enderecoFormatado);

			if (!StringUtils.isEmpty(endereco.getComplemento())) {
				leituraMovimento.setComplementoEmdereco(endereco.getComplemento());
			}

			leituraMovimento.setCep(endereco.getCep().getCep());

			if (!StringUtils.isEmpty(endereco.getCep().getBairro())) {
				leituraMovimento.setBairro(endereco.getCep().getBairro());
			}

			if (!StringUtils.isEmpty(endereco.getCep().getNomeMunicipio())) {
				leituraMovimento.setMunicipio(endereco.getCep().getNomeMunicipio());
			}

			if (!StringUtils.isEmpty(endereco.getCep().getUf())) {
				leituraMovimento.setUf(endereco.getCep().getUf());
			}
		}

	}

	/**
	 * Preencher leitura com info rota.
	 *
	 * @param leituraMovimento the leitura movimento
	 * @param rota             the rota
	 */
	private void preencherLeituraComInfoRota(LeituraMovimento leituraMovimento, Rota rota) {

		if (rota != null) {
			leituraMovimento.setRota(rota);
			leituraMovimento.setNumeroRota(rota.getNumeroRota());
			leituraMovimento.setEmpresaResponsavel(rota.getEmpresa());
			leituraMovimento.setLeiturista(rota.getLeiturista());
			leituraMovimento.setTipoLeitura(rota.getTipoLeitura());
			leituraMovimento.setGrupoFaturamento(rota.getGrupoFaturamento());

			if (rota.getSetorComercial() != null) {
				leituraMovimento.setNumeroSetorComercial(rota.getSetorComercial().getCodigo());
			}

		}
	}

	/**
	 * Preencher leitura com info imovel.
	 *
	 * @param leituraMovimento the leitura movimento
	 * @param imovel           the imovel
	 */
	private void preencherLeituraComInfoImovel(LeituraMovimento leituraMovimento, Imovel imovel) {

		if (imovel != null) {
			leituraMovimento.setNumeroLoteImovel(imovel.getNumeroLote());
			leituraMovimento.setSubLoteImovel(imovel.getNumeroSublote());

			if (imovel.getPerfilImovel() != null) {
				leituraMovimento.setPerfilImovel(imovel.getPerfilImovel().getDescricao());
			}

			if (imovel.getQuadraFace() != null) {

				leituraMovimento.setNumeroQuadra(imovel.getQuadraFace().getQuadra().getNumeroQuadra());

				if (imovel.getQuadraFace().getEndereco() != null) {
					leituraMovimento
							.setNumeroFaceQuadra(Integer.valueOf(imovel.getQuadraFace().getEndereco().getNumero()));
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura. ControladorLeituraMovimento
	 * #validarGerarDadosDeLeitura (java.lang.Long[])
	 */
	@Override
	public void validarGerarDadosDeLeitura(Long[] chavesPrimarias, String dataPrevistaLeitura,
			AtividadeSistema atividadeSistema) throws NegocioException {

		ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();

		if (chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}

		if (dataPrevistaLeitura == null || dataPrevistaLeitura.length() == 0) {
			throw new NegocioException(ControladorLeituraMovimento.ERRO_NEGOCIO_DATA_LEITURA_NAO_PREENCHIDA, true);
		} else {
			try {
				Date dataPrevista = Util.converterCampoStringParaData("CRONOGRAMA_ROTA_DATA_PREVISTA",
						dataPrevistaLeitura, Constantes.FORMATO_DATA_BR);
				DateMidnight dataAtual = new DateMidnight(Calendar.getInstance().getTime());
				if (dataPrevista.before(dataAtual.toDate())) {
					throw new NegocioException(ControladorLeituraMovimento.ERRO_NEGOCIO_DATA_LEITURA_MENOR, true);
				}
			} catch (GGASException e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(e.getChaveErro(), ControladorLeituraMovimento.GERAR_DADOS_DATA_LEITURA);
			}
		}
		// Validar se as rotas tem cronograma
		// gerado.
		String[] chaves = new String[chavesPrimarias.length];
		for (int i = 0; i < chavesPrimarias.length; i++) {
			chaves[i] = Long.toString(chavesPrimarias[i]);
		}

		Collection<PontoConsumo> pontosConsumo = controladorPontoConsumo.listarPontosConsumo(chaves);

		Collection<Rota> rotas = new ArrayList<Rota>();
		for (PontoConsumo pontoConsumo : pontosConsumo) {
			if (!rotas.contains(pontoConsumo.getRota())) {
				rotas.add(pontoConsumo.getRota());
			}
		}

		Collection<String> numeroRotasCollection = new ArrayList<String>();
		// Rotas
		for (Rota rota : rotas) {
			// primeiro em relação a data atual
			CronogramaRota cronogramaRota = controladorRota.obterCronogramaRotaAtual(rota,
					atividadeSistema.getChavePrimaria(), false, true);

			if (cronogramaRota == null) {
				numeroRotasCollection.add(rota.getNumeroRota());
			}
		}
		if (!numeroRotasCollection.isEmpty()) {
			String numeros = Util.arrayParaString(numeroRotasCollection.toArray(), ",");

			throw new NegocioException(ControladorLeituraMovimento.ERRO_ROTA_SEM_CRONOGRAMA, numeros);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura. ControladorLeituraMovimento
	 * #registrarLeituraAnormalidade (java.lang.Long[],
	 * br.com.ggas.faturamento.cronograma .AtividadeSistema, java.lang.Long)
	 */
	@Override
	public Collection<DadosRegistrarLeiturasAnormalidades> registrarLeituraAnormalidade(Long[] rotas,
			AtividadeSistema atividadeSistema, Long chavePrimaria, Long[] leituras, StringBuilder logProcessamento, Usuario usuarioLogado, Boolean isEncerrarRota)
			throws GGASException {

		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();
		ControladorPontoConsumo controladorPontoConsumo = ServiceLocator.getInstancia().getControladorPontoConsumo();
		ControladorHistoricoMedicao controladorHistoricoMedicao = ServiceLocator.getInstancia()
				.getControladorHistoricoMedicao();
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

		SimpleDateFormat format = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);

		Collection<DadosRegistrarLeiturasAnormalidades> relatorios = new ArrayList<DadosRegistrarLeiturasAnormalidades>();
		Long idSituacaoRealizada = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_LEITURA_REALIZADA));
		Long idSituacaoNaoRealizada = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_LEITURA_NAO_REALIZADA));
		Long idSituacaoConfirmada = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_SITUACAO_LEITURA_CONFIRMADA));

		SituacaoLeitura leituraNaoRealizada = this.obterSituacaoLeitura(idSituacaoNaoRealizada);
		SituacaoLeitura leituraConfirmada = this.obterSituacaoLeitura(idSituacaoConfirmada);
		SituacaoLeitura leituraRealizada = this.obterSituacaoLeitura(idSituacaoRealizada);
		Boolean isRegistraSemLeituraRetornada = BooleanUtil.converterStringCharParaBooleano(controladorParametroSistema
				.obterParametroPorCodigo(Constantes.PARAMETRO_REGISTRA_HISTORICO_MEDICAO).getValor());

		for (int j = 0; j < rotas.length; j++) {

			Collection<LeituraMovimento> movimentos;
			Collection<PontoConsumo> listaPontoConsumoComLeituraBloqueio;
			Rota rota = (Rota) controladorRota.obter(rotas[j]);
			final String chavePrimariaRota = "[" + rota.getChavePrimaria() + "]";

			escreverNovaLinha(logProcessamento,
					"Buscando cronograma da rota que não tenha sido realizada e verifica se foi iniciada");

			CronogramaRota cronogramaRota = controladorRota.obterCronogramaRotaAtual(rota,
					atividadeSistema.getChavePrimaria(), false, true);

			Long[] aRray = new Long[1];
			aRray[0] = Long.valueOf(rota.getChavePrimaria());
			Long[] chaveRota = null;
			chaveRota = aRray;

			SortedMap<String, String> sortedMap = new TreeMap<String, String>();
			Collection<HistoricoMedicao> historicoMedicoes = new ArrayList<HistoricoMedicao>();
			Collection<PontoConsumo> lpontoConsumo = new ArrayList<PontoConsumo>();
			Map<Rota, String> mapRotas = new HashMap<Rota, String>();
			Collection<HistoricoMedicao> listaHistoricoMedicao = null;
			CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = null;

			if (cronogramaRota != null) {

				escreverNovaLinha(logProcessamento, "Cronograma encontrado, com o tipo de leitura: "
						+ cronogramaRota.getRota().getTipoLeitura().getDescricao());

				if (cronogramaRota.getRota().getTipoLeitura().getChavePrimaria() == Long
						.parseLong(Constantes.TipoLeitura.ELETROCORRETOR.getValor())) {

					ControladorSupervisorio controladorSupervisorio = (ControladorSupervisorio) ServiceLocator
							.getInstancia()
							.getControladorNegocio(ControladorSupervisorio.BEAN_ID_CONTROLADOR_SUPERVISORIO);

					// Monta filtro para a consulta
					Map<String, Object> filtro = new HashMap<String, Object>();
					filtro.put(DATA_REFERENCIA, cronogramaRota.getAnoMesReferencia());
					filtro.put(NUMERO_CICLO, cronogramaRota.getNumeroCiclo());
					filtro.put(CHAVE_ROTA, chaveRota);
					filtro.put(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE);
					filtro.put(INDICADOR_INTEGRADO, Boolean.TRUE);
					filtro.put(INDICADOR_PROCESSADO, Boolean.FALSE);

					// Monta a ordenação da consulta
					String ordenacao = "codigoPontoConsumoSupervisorio," + DATA_REALIZACAO_LEITURA;

					Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria = controladorSupervisorio
							.consultarSupervisorioMedicaoDiaria(filtro, ordenacao);

					// Adiciona à listaSupervisorioMedicaoDiaria as medicoes que vem de medidores
					// independentes.
					filtro = new HashMap<>();
					filtro.put(ID_ROTA, rota.getChavePrimaria());
					filtro.put(MODO_USO, Long.valueOf(Fachada.getInstancia()
							.obterValorConstanteSistemaPorCodigo(Constantes.C_MODO_USO_MEDIDOR_VIRTUAL)));
					Collection<Medidor> listaMedidorVirtual = controladorMedidor.consultarMedidor(filtro);
					Collection<PontoConsumo> listaPontoConsumoMI = new ArrayList<PontoConsumo>();

					for (Medidor medidorVirtual : listaMedidorVirtual) {

						String[] arrayEnderecoRemoto = listarCodigoSupervisorioMedidorVirtual(controladorMedidor,
								medidorVirtual);

						List<Object> listaContadorValidacao = controladorSupervisorio
								.validarComposicaoVirtualPorCodSupervisorio(arrayEnderecoRemoto,
										cronogramaRota.getNumeroCiclo(), cronogramaRota.getAnoMesReferencia());

						boolean possuiAnormalidade = validaQuantidadeRegistrosMI(relatorios, cronogramaRota,
								controladorSupervisorio, ordenacao, listaSupervisorioMedicaoDiaria, controladorMedidor,
								arrayEnderecoRemoto, listaContadorValidacao);

						if (possuiAnormalidade && medidorVirtual.getInstalacaoMedidor() != null) {

							listaPontoConsumoMI.add(medidorVirtual.getInstalacaoMedidor().getPontoConsumo());
						}
					}

					if (listaSupervisorioMedicaoDiaria != null && !listaSupervisorioMedicaoDiaria.isEmpty()) {
						logProcessamento.append("\r\n Quantidade de Medições Diárias: ")
								.append(listaSupervisorioMedicaoDiaria.size());
					} else {
						logProcessamento.append(
								"\r\n Não há medições disponíveis, verificar consolidação e integração dos dados do Supervisório. ");
					}

					cronogramaAtividadeFaturamento = cronogramaRota.getCronogramaAtividadeFaturamento();

					if (listaSupervisorioMedicaoDiaria != null && !listaSupervisorioMedicaoDiaria.isEmpty()) {
						Map<PontoConsumo, String> mapaPeriodicidade = new HashMap<>();
						SupervisorioMedicaoDiaria supervisorioAnterior = null;
						Map<String, PontoConsumo> mapaPontoConsumo = new HashMap<String, PontoConsumo>();
						for (SupervisorioMedicaoDiaria supervisorio : listaSupervisorioMedicaoDiaria) {
							Collection<PontoConsumo> listaPontoConsumo = new ArrayList<PontoConsumo>();

							Medidor medidorIndependente = null;

							if (supervisorioAnterior != null && !supervisorio.getCodigoPontoConsumoSupervisorio()
									.equals(supervisorioAnterior.getCodigoPontoConsumoSupervisorio())) {
								supervisorioAnterior = null;
							}
							if (Boolean.TRUE.equals(supervisorio.getIndicadorMedidor())) {
								filtro = new HashMap<String, Object>();
								filtro.put(CODIGO_MEDIDOR_SUPERVISORIO,
										supervisorio.getCodigoPontoConsumoSupervisorio());
								medidorIndependente = controladorMedidor.consultarMedidor(filtro).iterator().next();

								listaMedidorVirtual = controladorMedidor
										.consultarMedidorVirtualPorMedidorIndependente(medidorIndependente);
								for (Medidor medidorVirtual : listaMedidorVirtual) {
									if (medidorVirtual.getInstalacaoMedidor() != null) {
										PontoConsumo pontoConsumo = medidorVirtual.getInstalacaoMedidor()
												.getPontoConsumo();

										// caso exita anormalidade para esse ponto de consumo, suas medições não serão
										// processadas.
										if (!listaPontoConsumoMI.contains(pontoConsumo)) {
											listaPontoConsumo.add(pontoConsumo);
										}
									}
								}
							} else if (mapaPontoConsumo.containsKey(supervisorio.getCodigoPontoConsumoSupervisorio())) {
								listaPontoConsumo
										.add(mapaPontoConsumo.get(supervisorio.getCodigoPontoConsumoSupervisorio()));

							} else {
								PontoConsumo pontoConsumo = controladorPontoConsumo
										.listarPontoConsumoPorCodigoSupervisorio(
												supervisorio.getCodigoPontoConsumoSupervisorio());
								mapaPontoConsumo.put(supervisorio.getCodigoPontoConsumoSupervisorio(), pontoConsumo);
								listaPontoConsumo.add(pontoConsumo);
							}

							for (PontoConsumo pontoConsumo : listaPontoConsumo) {

								if (!mapaPeriodicidade.containsKey(pontoConsumo)) {
									ContratoPontoConsumo contratoPonto = controladorContrato
											.consultarContratoPontoConsumoPorPontoConsumo(
													pontoConsumo.getChavePrimaria());
									String mensagem = "";
									if (rota.getGrupoFaturamento().getPeriodicidade()
											.getChavePrimaria() != contratoPonto.getPeriodicidade()
													.getChavePrimaria()) {
										mensagem = "\r\n Ponto de Consumo: " + pontoConsumo.getDescricao()
												+ "\r\nPeriodicidade do Contrato ("
												+ contratoPonto.getPeriodicidade().getDescricao()
												+ ") diferente do Grupo de Faturamento ("
												+ rota.getGrupoFaturamento().getPeriodicidade().getDescricao() + ")";
									}

									mapaPeriodicidade.put(pontoConsumo, mensagem);
								}

								if (pontoConsumo != null) {
									if (!mapRotas.containsKey(pontoConsumo.getRota())) {

										String dataLeitura = "";
										if (supervisorio.getDataRealizacaoLeitura() != null) {
											dataLeitura = Util.converterDataParaStringSemHora(
													supervisorio.getDataRealizacaoLeitura(),
													Constantes.FORMATO_DATA_US);
										}
										mapRotas.put(pontoConsumo.getRota(),
												cronogramaRota.getChavePrimaria() + "_" + dataLeitura);
									}

									boolean possuiErro = this.validarMovimentoLeituraDiariaSupervisorio(supervisorio,
											pontoConsumo, relatorios);
									if (!possuiErro) {
										HistoricoMedicao historicoMedicao;
										if (Boolean.TRUE.equals(supervisorio.getIndicadorMedidor())) {
											historicoMedicao = this.popularHistoricoMedicaoMI(supervisorio,
													supervisorioAnterior, medidorIndependente, pontoConsumo);
										} else {
											historicoMedicao = this.popularHistoricoMedicao(supervisorio,
													supervisorioAnterior, pontoConsumo);
										}

										String chave = pontoConsumo.getDescricao() + " "
												+ historicoMedicao.getDataLeituraInformada();
										String valor = pontoConsumo.getDescricao() + " \n\r Data:  "
												+ format.format(historicoMedicao.getDataLeituraInformada())
												+ " - Leitura: " + historicoMedicao.getNumeroLeituraInformada();
										sortedMap.put(chave, valor);

										historicoMedicao.setSituacaoLeitura(leituraRealizada);
										historicoMedicoes.add(historicoMedicao);

										controladorSupervisorio.alterarSupervisorioMedicaoDiaria(supervisorio);
										lpontoConsumo.add(historicoMedicao.getPontoConsumo());

									}
									supervisorioAnterior = supervisorio;
								}
							}
						}
						for (Map.Entry<String, String> mapa : sortedMap.entrySet()) {
							logProcessamento.append("\r\n Ponto de Consumo: ").append(mapa.getValue());
						}

						for (Map.Entry<PontoConsumo, String> mapa : mapaPeriodicidade.entrySet()) {

							if (!"".equals(mapa.getValue())) {
								logProcessamento.append(mapa.getValue());
							}

						}
					}
				} else {
					if (leituras.length > 0) {
						escreverNovaLinha(logProcessamento,
								chavePrimariaRota
										+ " consultando movimentos de leitura pelas chaves fornecidas. Leituras: "
										+ Arrays.toString(leituras));
						movimentos = this.consultarMovimentosLeituraPorChaveLeituraMovimento(leituras);
					} else {
						escreverNovaLinha(logProcessamento,
								"Consultando movimentos de leitura para a referencia: "
										+ cronogramaRota.getAnoMesReferencia() + "/" + cronogramaRota.getNumeroCiclo()
										+ ". Para a rota: " + rota.getNumeroRota());

						movimentos = this.consultarMovimentosLeituraPorRota(chaveRota,
								new Long[] { SituacaoLeituraMovimento.LEITURA_RETORNADA },
								cronogramaRota.getAnoMesReferencia(), cronogramaRota.getNumeroCiclo())
								.stream()
								.sorted(Comparator.comparing(LeituraMovimento::getDescicaoPontoConsumo))
								.collect(Collectors.toList());
					}

					montaLogPontosConsumoComLeituraNaoRetornada(logProcessamento, cronogramaRota, chaveRota);
					
					
					if (movimentos != null) {
						removerMovimentosNaoFaturar(movimentos, usuarioLogado);
						logProcessamento.append(
								"\r\n Quantidade de Movimento de Leitura Retornadas e aptos para Registrar Leitura: ")
								.append(movimentos.size());
					} else {
						logProcessamento.append("\r\n Nenhum movimento encontrado. ");
					}

					cronogramaAtividadeFaturamento = cronogramaRota.getCronogramaAtividadeFaturamento();
					if (movimentos != null && !movimentos.isEmpty()) {
						for (LeituraMovimento leituraMovimento : movimentos) {

							if (!mapRotas.containsKey(leituraMovimento.getRota())) {

								String dataLeitura = "";
								if (leituraMovimento.getDataLeitura() != null) {
									dataLeitura = Util.converterDataParaStringSemHora(leituraMovimento.getDataLeitura(),
											Constantes.FORMATO_DATA_US);
								}
								mapRotas.put(leituraMovimento.getRota(),
										cronogramaRota.getChavePrimaria() + "_" + dataLeitura);
							}

							try {
								this.validarMovimentoLeitura(leituraMovimento, relatorios);

								HistoricoMedicao historicoMedicao = this.popularHistoricoMedicao(
										leituraMovimento.getPontoConsumo(), leituraMovimento.getDataLeitura(),
										leituraMovimento.getValorLeitura(), leituraMovimento.getAnoMesFaturamento(),
										leituraMovimento.getCiclo(), leituraMovimento.getLeiturista(),
										leituraMovimento.getPressaoInformada(),
										leituraMovimento.getTemperaturaInformada(),
										leituraMovimento.getUnidadePressaoInformada(),
										leituraMovimento.getUnidadeTemperaturaInformada(),
										leituraMovimento.getCodigoAnormalidadeLeitura());

								if (historicoMedicao.getAnormalidadeLeituraInformada() != null) {
									logProcessamento.append("\r\n Ponto de Consumo ")
											.append(leituraMovimento.getPontoConsumo().getDescricao()).append(" (")
											.append(leituraMovimento.getPontoConsumo().getChavePrimaria())
											.append(") com anormalidade de leitura ")
											.append(historicoMedicao.getAnormalidadeLeituraInformada().getDescricao());
								}

								SituacaoLeitura situacaoLeitura = null;
								BigDecimal valorLeitura = leituraMovimento.getValorLeitura();
								Long anormalidadeLeitura = leituraMovimento.getCodigoAnormalidadeLeitura();
								if (valorLeitura == null && anormalidadeLeitura == null) {
									situacaoLeitura = leituraNaoRealizada;
								} else if (leituraMovimento.getIndicadorConfirmacaoLeitura() != null
										&& leituraMovimento.getIndicadorConfirmacaoLeitura()) {
									situacaoLeitura = leituraConfirmada;
								} else {
									situacaoLeitura = leituraRealizada;
								}

								historicoMedicao.setSituacaoLeitura(situacaoLeitura);
								historicoMedicoes.add(historicoMedicao);

								this.alterarSituacaoLeituraMovimento(leituraMovimento, usuarioLogado);
								PontoConsumo pontoConsumo = historicoMedicao.getPontoConsumo();
								lpontoConsumo.add(pontoConsumo);

								ContratoPontoConsumo contratoPontoConsumo = controladorContrato
										.consultarContratoPontoConsumoPorPontoConsumo(pontoConsumo.getChavePrimaria());
								if (contratoPontoConsumo != null) {
									Contrato contrato = contratoPontoConsumo.getContrato();
									Cliente cliente = contrato.getClienteAssinatura();
									String log = logProcessamento.toString();

									if (!log.contains(pontoConsumo.getDescricao())) {
										logProcessamento.append("\r\n Cliente/Ponto de Consumo: ")
												.append(cliente.getNome()).append("/")
												.append(pontoConsumo.getDescricao());
									}
								} else {
									logProcessamento.append("\r\n Cliente/Ponto de Consumo: ").append("Sem Contrato")
											.append("/").append(pontoConsumo.getDescricao());
								}

								if (historicoMedicao.getDataLeituraInformada() != null) {
									escreverNovaLinha(logProcessamento, chavePrimariaRota + " Data Leitura : "
											+ format.format(historicoMedicao.getDataLeituraInformada()));
								} else {
									escreverNovaLinha(logProcessamento,
											chavePrimariaRota + " Data Leitura do histórico de medição está NULO");
								}

								if (historicoMedicao.getNumeroLeituraInformada() != null) {
									escreverNovaLinha(logProcessamento, chavePrimariaRota + "Leitura : "
											+ historicoMedicao.getNumeroLeituraInformada());
								} else {
									escreverNovaLinha(logProcessamento,
											chavePrimariaRota + " Número de Leitura do histórico de medição está NULO");
								}
							} catch (NegocioException e) {
								LOG.error(e.getMessage(), e);
								if (leituraMovimento.getPontoConsumo() != null) {
									logProcessamento.append("\r\nPonto de Consumo: ")
											.append(leituraMovimento.getPontoConsumo().getDescricao());
								}
								escreverNovaLinha(logProcessamento, chavePrimariaRota + e.getMessage());
							}

						}
					}

					// Pontos de consumo que estao bloqueados, nao foram para leitura mas possuem
					// consumo de bloqueio
					listaPontoConsumoComLeituraBloqueio = controladorPontoConsumo
							.consultarPontoConsumoBloqueadoComLeituraBloqueio(rota,
									cronogramaRota.getAnoMesReferencia(), cronogramaRota.getNumeroCiclo());
					if (listaPontoConsumoComLeituraBloqueio != null && !listaPontoConsumoComLeituraBloqueio.isEmpty()) {
						for (PontoConsumo pontoConsumoBloqueado : listaPontoConsumoComLeituraBloqueio) {

							FiltroHistoricoOperacaoMedidor filtro = new FiltroHistoricoOperacaoMedidor();
							filtro.setIdPontoConsumo(pontoConsumoBloqueado.getChavePrimaria());
							filtro.setOrdenacaoDataRealizada(Ordenacao.DESCENDENTE);
							Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor = controladorMedidor
									.obterHistoricoOperacaoMedidor(filtro);
							HistoricoOperacaoMedidor historicoOperacaoMedidor = listaHistoricoOperacaoMedidor.iterator()
									.next();

							HistoricoMedicao historicoMedicaoCicloAnterior = controladorHistoricoMedicao
									.obterUltimoHistoricoMedicao(pontoConsumoBloqueado.getChavePrimaria(), null, null,
											Boolean.TRUE);

							if (historicoOperacaoMedidor != null && historicoMedicaoCicloAnterior != null
									&& historicoMedicaoCicloAnterior.getNumeroLeituraInformada() != null
									&& historicoOperacaoMedidor.getNumeroLeitura()
											.compareTo(historicoMedicaoCicloAnterior.getNumeroLeituraInformada()) > 0) {
								logProcessamento.append("\r\n Ponto de consumo com leitura de bloqueio: ")
										.append(pontoConsumoBloqueado.getDescricao());
								logProcessamento.append("\r\n  Leitura : ")
										.append(historicoOperacaoMedidor.getNumeroLeitura());
								String codigoAnormalidadeLeitura = controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(
												Constantes.C_ANORMALIDADE_LEITURA_BLOQUEIO_UTILIZADA);
								HistoricoMedicao historicoMedicao = this.popularHistoricoMedicao(pontoConsumoBloqueado,
										historicoOperacaoMedidor.getDataRealizada(),
										historicoOperacaoMedidor.getNumeroLeitura(),
										cronogramaAtividadeFaturamento.getCronogramaFaturamento()
												.getAnoMesFaturamento(),
										cronogramaAtividadeFaturamento.getCronogramaFaturamento().getNumeroCiclo(),
										null, null, null, null, null, Long.valueOf(codigoAnormalidadeLeitura));

								historicoMedicao.setSituacaoLeitura(leituraRealizada);
								historicoMedicoes.add(historicoMedicao);

							}

						}
					}
					// verifica os pontos de consumo que não tiveram medição e foram gerados leitura
					// movimento
					if (Util.compararDatas(cronogramaAtividadeFaturamento.getDataFim(),
							Calendar.getInstance().getTime()) < 0) {

						if (!mapRotas.containsKey(rota)) {
							String dataLeitura = "";
							dataLeitura = Util.converterDataParaStringSemHora(Calendar.getInstance().getTime(),
									Constantes.FORMATO_DATA_US);
							mapRotas.put(rota, cronogramaRota.getChavePrimaria() + "_" + dataLeitura);
						}

						Collection<PontoConsumo> listaPontoConsumo = controladorPontoConsumo
								.listarPontosConsumoPorRotaLeituraMovimento(rota.getChavePrimaria(),
										cronogramaAtividadeFaturamento);

						for (PontoConsumo pontoConsumo : listaPontoConsumo) {

							listaHistoricoMedicao = controladorHistoricoMedicao.consultarHistoricoMedicao(
									pontoConsumo.getChavePrimaria(),
									cronogramaAtividadeFaturamento.getCronogramaFaturamento().getAnoMesFaturamento(),
									cronogramaAtividadeFaturamento.getCronogramaFaturamento().getNumeroCiclo());

							boolean temHistoricoMedicao = false;

							if (listaHistoricoMedicao != null && !listaHistoricoMedicao.isEmpty()) {
								for (HistoricoMedicao historicoMedicao : listaHistoricoMedicao) {
									if (historicoMedicao.getAnoMesLeitura()
											.equals(cronogramaAtividadeFaturamento.getCronogramaFaturamento()
													.getAnoMesFaturamento())
											&& historicoMedicao.getNumeroCiclo().equals(cronogramaAtividadeFaturamento
													.getCronogramaFaturamento().getNumeroCiclo())) {
										temHistoricoMedicao = true;
									}
								}
							}

							// Só gerar 'historico
							// medicao' se não existir
							// e se não tem corretor
							// de
							// vazão.
							if ((isRegistraSemLeituraRetornada || isEncerrarRota) && !temHistoricoMedicao
									&& (pontoConsumo.getInstalacaoMedidor() != null
											&& pontoConsumo.getInstalacaoMedidor().getVazaoCorretor() == null
											&& pontoConsumo.getHabilitado())) {

								HistoricoMedicao historicoMedicaoNaoRealizada = null;
								historicoMedicaoNaoRealizada = (HistoricoMedicao) controladorHistoricoMedicao.criar();

								historicoMedicaoNaoRealizada.setNumeroCiclo(
										cronogramaAtividadeFaturamento.getCronogramaFaturamento().getNumeroCiclo());
								historicoMedicaoNaoRealizada.setAnoMesLeitura(cronogramaAtividadeFaturamento
										.getCronogramaFaturamento().getAnoMesFaturamento());
								historicoMedicaoNaoRealizada.setSequenciaLeitura(1);
								historicoMedicaoNaoRealizada.setDataProcessamento(Calendar.getInstance().getTime());
								historicoMedicaoNaoRealizada.setUltimaAlteracao(Calendar.getInstance().getTime());
								historicoMedicaoNaoRealizada.setHabilitado(Boolean.TRUE);
								historicoMedicaoNaoRealizada.setSituacaoLeitura(leituraNaoRealizada);
								historicoMedicaoNaoRealizada.setPontoConsumo(pontoConsumo);

								historicoMedicaoNaoRealizada
										.setHistoricoInstalacaoMedidor(pontoConsumo.getInstalacaoMedidor());
								Map<String, Integer> referenciaCicloAnterior = Util.regredirReferenciaCiclo(
										historicoMedicaoNaoRealizada.getAnoMesLeitura(),
										historicoMedicaoNaoRealizada.getNumeroCiclo(),
										rota.getPeriodicidade().getQuantidadeCiclo());

								// Obter a maior leitura do ciclo anterior.
								HistoricoMedicao historicoMedicaoCicloAnterior = controladorHistoricoMedicao
										.obterUltimoHistoricoMedicao(pontoConsumo.getChavePrimaria(),
												referenciaCicloAnterior.get(REFERENCIA),
												referenciaCicloAnterior.get(CICLO));
								if (historicoMedicaoCicloAnterior != null) {
									historicoMedicaoNaoRealizada.setAnormalidadeLeituraAnterior(
											historicoMedicaoCicloAnterior.getAnormalidadeLeituraFaturada());
									historicoMedicaoNaoRealizada.setNumeroLeituraAnterior(
											historicoMedicaoCicloAnterior.getNumeroLeituraFaturada());
									historicoMedicaoNaoRealizada.setDataLeituraAnterior(
											historicoMedicaoCicloAnterior.getDataLeituraFaturada());
									historicoMedicaoNaoRealizada
											.setCreditoSaldo(historicoMedicaoCicloAnterior.getCreditoVolume());
								} else if (pontoConsumo.getInstalacaoMedidor() != null) {
									Date dataLeitura = controladorHistoricoMedicao.setarDataLeituraAnterior(null,
											pontoConsumo, pontoConsumo.getInstalacaoMedidor().getMedidor());
									BigDecimal leitura = controladorHistoricoMedicao.setarLeituraAnterior(null,
											pontoConsumo, pontoConsumo.getInstalacaoMedidor().getMedidor());

									historicoMedicaoNaoRealizada.setNumeroLeituraAnterior(leitura);
									historicoMedicaoNaoRealizada.setDataLeituraAnterior(dataLeitura);
								}

								historicoMedicaoNaoRealizada.setDataLeituraInformada(cronogramaRota.getDataPrevista());

								historicoMedicoes.add(historicoMedicaoNaoRealizada);

							}
							lpontoConsumo.add(pontoConsumo);
						}
					}
				}

			}

			if (!historicoMedicoes.isEmpty()) {
				for (HistoricoMedicao historicoMedicao : historicoMedicoes) {
					super.inserir(historicoMedicao);
				}
			} else if (cronogramaAtividadeFaturamento != null) {
				logProcessamento
						.append("\r\n Nesse processo não foi gerado Medição Histórico para a rota: " + rota.getNumeroRota()
								+ " para o Ano/Mês: ")
						.append(cronogramaAtividadeFaturamento.getCronogramaFaturamento().getAnoMesFaturamentoMascara())
						.append(" e Ciclo: ")
						.append(cronogramaAtividadeFaturamento.getCronogramaFaturamento().getNumeroCiclo());

			} else {
				escreverNovaLinha(logProcessamento, " Todos os pontos de consumo da rota: "
						+ rota.getNumeroRota() + " foram executados com sucesso. ");
			}

			if (!lpontoConsumo.isEmpty()) {
				controladorRota.atualizarCronogramaPorPontosConsumo(lpontoConsumo,
						AtividadeSistema.CODIGO_ATIVIDADE_REALIZAR_LEITURA, null, logProcessamento);
				controladorRota.atualizarCronogramaPorPontosConsumo(lpontoConsumo,
						AtividadeSistema.CODIGO_ATIVIDADE_RETORNAR_LEITURA, null, logProcessamento);
			} else {
				Collection<Rota> listRota = new HashSet<Rota>();
				listRota.add(rota);
				
				controladorRota.atualizarCronogramaPorPontosConsumo(lpontoConsumo,
						AtividadeSistema.CODIGO_ATIVIDADE_REALIZAR_LEITURA, listRota, logProcessamento);
				controladorRota.atualizarCronogramaPorPontosConsumo(lpontoConsumo,
						AtividadeSistema.CODIGO_ATIVIDADE_RETORNAR_LEITURA, listRota, logProcessamento);
			}
		}
		return relatorios;
	}

	@Override
	public void removerMovimentosNaoFaturar(Collection<LeituraMovimento> movimentos, Usuario usuarioLogado) throws NegocioException {
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();
		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));
		Collection<LeituraMovimento> removerLeituras = new HashSet<LeituraMovimento>();
		for (LeituraMovimento leitura : movimentos) {
			if (leitura.getCodigoAnormalidadeLeitura() != null
					&& leitura.getCodigoAnormalidadeLeitura().compareTo(codigoAnormalidadeLeitura) == 0) {
				alterarSituacaoLeituraMovimento(leitura, usuarioLogado);
				removerLeituras.add(leitura);
			}
		}

		movimentos.removeAll(removerLeituras);
	}

	private void montaLogPontosConsumoComLeituraNaoRetornada(StringBuilder logProcessamento,
			CronogramaRota cronogramaRota, Long[] chaveRota) throws NegocioException {

		Collection<LeituraMovimento> movimentos;
		movimentos = this.consultarMovimentosLeituraPorRota(chaveRota,
				new Long[] { SituacaoLeituraMovimento.GERADO, SituacaoLeituraMovimento.EM_LEITURA },
				cronogramaRota.getAnoMesReferencia(), cronogramaRota.getNumeroCiclo())
				.stream()
				.sorted(Comparator.comparing(LeituraMovimento::getDescicaoPontoConsumo))
				.collect(Collectors.toList());

		if (movimentos != null && !movimentos.isEmpty()) {
			escreverAposDuasLinha(logProcessamento, "Quantidade de pontos de consumo sem leitura retornada: " + movimentos.size());
			for (LeituraMovimento leituraMovimento : movimentos) {
				escreverNovaLinha(logProcessamento,
						" Descrição da leitura movimento: " + leituraMovimento.getDescicaoPontoConsumo());
			}
		} else if (movimentos == null){
			escreverAposDuasLinha(logProcessamento, " Movimentos carregado nulo ou vazio: " + movimentos);
		}
	}

	/**
	 * Valida se a quantidade de registros do supervisório, para todos os medidores
	 * independentes que compoem a formula do medidor virtual, dentro de um mesmo
	 * ciclo, é igual.
	 *
	 * @param relatorios                     the relatorios
	 * @param cronogramaRota                 the cronograma rota
	 * @param controladorSupervisorio        the controlador supervisorio
	 * @param ordenacao                      the ordenacao
	 * @param listaSupervisorioMedicaoDiaria the lista supervisorio medicao diaria
	 * @param controladorMedidor             the controlador medidor
	 * @param arrayEnderecoRemoto            the array endereco remoto
	 * @param listaContadorValidacao         the lista contador validacao
	 * @return true, if successful
	 * @throws GGASException the GGAS exception
	 */
	private boolean validaQuantidadeRegistrosMI(Collection<DadosRegistrarLeiturasAnormalidades> relatorios,
			CronogramaRota cronogramaRota, ControladorSupervisorio controladorSupervisorio, String ordenacao,
			Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiaria, ControladorMedidor controladorMedidor,
			String[] arrayEnderecoRemoto, List<Object> listaContadorValidacao) throws GGASException {

		Map<String, Object> filtro;
		boolean possuiIrregularidade = false;
		for (Object object : listaContadorValidacao) {
			Object[] arrayObjeto = (Object[]) object;
			Long contador = (Long) arrayObjeto[1];
			if (arrayEnderecoRemoto.length != contador) {
				possuiIrregularidade = true;
			}
		}

		filtro = new HashMap<>();
		filtro.put(LISTA_CODIGO_SUPERVISORIO, arrayEnderecoRemoto);
		filtro.put(DATA_REFERENCIA, cronogramaRota.getAnoMesReferencia());
		filtro.put(NUMERO_CICLO, cronogramaRota.getNumeroCiclo());
		filtro.put(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE);
		filtro.put(INDICADOR_INTEGRADO, Boolean.TRUE);

		Collection<SupervisorioMedicaoDiaria> listaSupervisorioMedicaoDiariaMI = controladorSupervisorio
				.consultarSupervisorioMedicaoDiaria(filtro, ordenacao);

		if (possuiIrregularidade) {
			for (SupervisorioMedicaoDiaria superMediAux : listaSupervisorioMedicaoDiariaMI) {
				String ocorrencia = "Não contem todas as medições para a composição virtual.";
				filtro = new HashMap<>();
				filtro.put(CODIGO_MEDIDOR_SUPERVISORIO, superMediAux.getCodigoPontoConsumoSupervisorio());
				Collection<Medidor> listaMedidorAux = controladorMedidor.consultarMedidor(filtro);
				this.setarInformacoesRelatorioMI(superMediAux, listaMedidorAux.iterator().next(), relatorios,
						ocorrencia);
				if (listaSupervisorioMedicaoDiaria.contains(superMediAux)) {
					listaSupervisorioMedicaoDiaria.remove(superMediAux);
				}
			}
		} else {
			if (!listaSupervisorioMedicaoDiariaMI.isEmpty()) {
				for (SupervisorioMedicaoDiaria superMediAux : listaSupervisorioMedicaoDiariaMI) {
					if (!listaSupervisorioMedicaoDiaria.contains(superMediAux)) {
						listaSupervisorioMedicaoDiaria.add(superMediAux);
					}
				}
			}
		}
		return possuiIrregularidade;
	}

	/**
	 * Lista os endereços remoto dos medidores independentes que fazem parte da
	 * composição do medidor virtual.
	 *
	 * @param controladorMedidor the controlador medidor
	 * @param medidorVirtual     the medidor virtual
	 * @return the string[]
	 * @throws NegocioException the negocio exception
	 */
	private String[] listarCodigoSupervisorioMedidorVirtual(ControladorMedidor controladorMedidor,
			Medidor medidorVirtual) throws NegocioException {

		Collection<Medidor> listaMedidorIndependente = controladorMedidor
				.listaMedidoresQueCompoemMedidorVirtual(medidorVirtual);

		Collection<String> listaEnderecoRemotoMI = new ArrayList<String>();
		for (Medidor medidor : listaMedidorIndependente) {
			listaEnderecoRemotoMI.add(medidor.getCodigoMedidorSupervisorio());
		}

		return listaEnderecoRemotoMI.toArray(new String[listaEnderecoRemotoMI.size()]);
	}

	/**
	 * Alterar situacao leitura movimento.
	 *
	 * @param leituraMovimento the leitura movimento
	 * @throws NegocioException the negocio exception
	 */
	private void alterarSituacaoLeituraMovimento(LeituraMovimento leituraMovimento, Usuario usuarioLogado) throws NegocioException {

		SituacaoLeituraMovimento situacaoLeituraMovimento = this
				.obterSituacaoLeituraMovimento(SituacaoLeituraMovimento.PROCESSADO);
		leituraMovimento.setSituacaoLeitura(situacaoLeituraMovimento);
		leituraMovimento.setUltimaAlteracao(Calendar.getInstance().getTime());
		// FIXME: Substituir pelo método do controlador negócio, conforme o caso
		// (inserir(), atualizar() ou remover())
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(leituraMovimento);
		inserirHistoricoLeituraMovimento(leituraMovimento, usuarioLogado);
	}

	/**
	 * Gets the classe entidade situacao leitura movimento.
	 *
	 * @return the classe entidade situacao leitura movimento
	 */
	public Class<?> getClasseEntidadeSituacaoLeituraMovimento() {

		return ServiceLocator.getInstancia().getClassPorID(SituacaoLeituraMovimento.BEAN_ID_SITUACAO_LEITURA_MOVIMENTO);
	}

	/**
	 * Gets the classe entidade situacao leitura.
	 *
	 * @return the classe entidade situacao leitura
	 */
	public Class<?> getClasseEntidadeSituacaoLeitura() {

		return ServiceLocator.getInstancia().getClassPorID(SituacaoLeitura.BEAN_ID_SITUACAO_LEITURA);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura. ControladorLeituraMovimento
	 * #obterSituacaoLeituraMovimento (long)
	 */
	@Override
	public SituacaoLeituraMovimento obterSituacaoLeituraMovimento(long chavePrimaria) throws NegocioException {

		return (SituacaoLeituraMovimento) super.obter(chavePrimaria, getClasseEntidadeSituacaoLeituraMovimento());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.medicao.leitura.ControladorLeituraMovimento#obterSituacaoLeitura(
	 * long)
	 */
	@Override
	public SituacaoLeitura obterSituacaoLeitura(long chavePrimaria) throws NegocioException {

		return (SituacaoLeitura) super.obter(chavePrimaria, getClasseEntidadeSituacaoLeitura());
	}

	/**
	 * Popular historico medicao.
	 *
	 * @return the historico medicao
	 * @throws NegocioException the negocio exception
	 */
	private HistoricoMedicao popularHistoricoMedicao(PontoConsumo pontoConsumo, Date dataLeitura,
			BigDecimal valorLeitura, Integer anoMesFaturamento, Integer ciclo, Leiturista leiturista,
			BigDecimal pressaoInformada, BigDecimal temperaturaInformada, Unidade unidadePressaoInformada,
			Unidade unidadeTemperaturaInformada, Long codigoAnormalidadeLeituraMovimento) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();
		ControladorHistoricoMedicao controladorHistoricoMedicao = ServiceLocator.getInstancia()
				.getControladorHistoricoMedicao();
		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

		Date dataLeituraInformada = dataLeitura;
		BigDecimal leituraInformada = valorLeitura;
		AnormalidadeLeitura anormalidadeLeituraBloqueio = null;
		if (!getControladorPontoConsumo().isPontoConsumoAtivo(pontoConsumo)) {

			FiltroHistoricoOperacaoMedidor filtro = new FiltroHistoricoOperacaoMedidor();
			filtro.setIdPontoConsumo(pontoConsumo.getChavePrimaria());
			filtro.setOrdenacaoDataRealizada(Ordenacao.DESCENDENTE);
			Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor = controladorMedidor
					.obterHistoricoOperacaoMedidor(filtro);
			HistoricoOperacaoMedidor historicoOperacaoMedidor = listaHistoricoOperacaoMedidor.iterator().next();

			Date dataLeituraHistoricoOperacaoMedidor = historicoOperacaoMedidor.getDataRealizada();
			BigDecimal leituraHistoricoOperacaoMedidor = historicoOperacaoMedidor.getNumeroLeitura();

			// leitura de bloqueio coletada antes da leitura de campo
			if (dataLeituraHistoricoOperacaoMedidor.compareTo(dataLeituraInformada) <= 0) {
				// leitura informada deveria ser igual a leitura de bloqueio
				if (leituraInformada.compareTo(leituraHistoricoOperacaoMedidor) != 0) {
					// gerar anormalidade
					String codigoAnormalidadeLeitura = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
							Constantes.C_ANORMALIDADE_LEITURA_CONSUMO_DEPOIS_LEITURA_BLOQUEIO);
					anormalidadeLeituraBloqueio = controladorAnormalidade
							.obterAnormalidadeLeitura(Long.parseLong(codigoAnormalidadeLeitura));
				} else {
					leituraInformada = leituraHistoricoOperacaoMedidor;
					dataLeituraInformada = dataLeituraHistoricoOperacaoMedidor;
					// gerar anormalidade
					String codigoAnormalidadeLeitura = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_BLOQUEIO_UTILIZADA);
					anormalidadeLeituraBloqueio = controladorAnormalidade
							.obterAnormalidadeLeitura(Long.parseLong(codigoAnormalidadeLeitura));
				}
			} else {
				// leitura de bloqueio depois
				if (leituraHistoricoOperacaoMedidor.compareTo(leituraInformada) < 0) {
					// gerar anormalidade
					String codigoAnormalidadeLeitura = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
							Constantes.C_ANORMALIDADE_LEITURA_CONSUMO_DEPOIS_LEITURA_BLOQUEIO);
					anormalidadeLeituraBloqueio = controladorAnormalidade
							.obterAnormalidadeLeitura(Long.parseLong(codigoAnormalidadeLeitura));
				} else {
					leituraInformada = leituraHistoricoOperacaoMedidor;
					dataLeituraInformada = dataLeituraHistoricoOperacaoMedidor;
					// gerar anormalidade
					String codigoAnormalidadeLeitura = controladorConstanteSistema
							.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_BLOQUEIO_UTILIZADA);
					anormalidadeLeituraBloqueio = controladorAnormalidade
							.obterAnormalidadeLeitura(Long.parseLong(codigoAnormalidadeLeitura));
				}
			}
		}

		HistoricoMedicao historicoMedicao = (HistoricoMedicao) controladorHistoricoMedicao.criar();
		historicoMedicao.setAnoMesLeitura(anoMesFaturamento);
		historicoMedicao.setNumeroCiclo(ciclo);
		historicoMedicao.setSequenciaLeitura(1);
		historicoMedicao.setDataLeituraInformada(dataLeituraInformada);
		historicoMedicao.setNumeroLeituraInformada(leituraInformada);
		historicoMedicao.setLeiturista(leiturista);
		historicoMedicao.setPontoConsumo(pontoConsumo);

		historicoMedicao.setPressaoInformada(pressaoInformada);
		historicoMedicao.setTemperaturaInformada(temperaturaInformada);
		historicoMedicao.setUnidadePressaoInformada(unidadePressaoInformada);
		historicoMedicao.setUnidadeTemperaturaInformada(unidadeTemperaturaInformada);

		if (codigoAnormalidadeLeituraMovimento != null && codigoAnormalidadeLeituraMovimento > 0) {
			AnormalidadeLeitura anormalidadeLeitura = controladorAnormalidade
					.obterAnormalidadeLeitura(codigoAnormalidadeLeituraMovimento);
			historicoMedicao.setAnormalidadeLeituraInformada(anormalidadeLeitura);
			historicoMedicao.setAnormalidadeLeituraFaturada(anormalidadeLeitura);
		}

		if (anormalidadeLeituraBloqueio != null) {
			historicoMedicao.setAnormalidadeLeituraInformada(anormalidadeLeituraBloqueio);
			historicoMedicao.setAnormalidadeLeituraFaturada(anormalidadeLeituraBloqueio);
		}

		this.verificarStatusMedidorNoPontoConsumo(null, null, dataLeitura, pontoConsumo, historicoMedicao);

		historicoMedicao.setDataProcessamento(Calendar.getInstance().getTime());
		historicoMedicao.setUltimaAlteracao(Calendar.getInstance().getTime());
		historicoMedicao.setHabilitado(Boolean.TRUE);
		historicoMedicao.setPontoConsumo(pontoConsumo);

		Map<String, Integer> anoMesReferenciaAnterior = Util.regredirReferenciaCiclo(anoMesFaturamento, ciclo,
				pontoConsumo.getRota().getPeriodicidade().getQuantidadeCiclo());

		HistoricoMedicao historicoMedicaoCicloAnterior = controladorHistoricoMedicao.obterUltimoHistoricoMedicao(
				pontoConsumo.getChavePrimaria(), anoMesReferenciaAnterior.get(REFERENCIA),
				anoMesReferenciaAnterior.get(CICLO), Boolean.TRUE);

		if (historicoMedicaoCicloAnterior != null) {
			popularHistoricoMedicaoAnterior(historicoMedicao, historicoMedicaoCicloAnterior, pontoConsumo);
		} else {

			Date dataLeituraAnterior = controladorHistoricoMedicao.setarDataLeituraAnterior(null, pontoConsumo,
					pontoConsumo.getInstalacaoMedidor().getMedidor());
			BigDecimal leitura = controladorHistoricoMedicao.setarLeituraAnterior(null, pontoConsumo,
					pontoConsumo.getInstalacaoMedidor().getMedidor());

			historicoMedicaoCicloAnterior = controladorHistoricoMedicao
					.obterUltimoHistoricoMedicao(pontoConsumo.getChavePrimaria(), null, null, Boolean.TRUE);

			if (historicoMedicaoCicloAnterior != null
					&& historicoMedicaoCicloAnterior.getDataLeituraInformada().compareTo(dataLeituraAnterior) > 0) {
				popularHistoricoMedicaoAnterior(historicoMedicao, historicoMedicaoCicloAnterior, pontoConsumo);
			} else {

				historicoMedicao.setNumeroLeituraAnterior(leitura);
				historicoMedicao.setDataLeituraAnterior(dataLeituraAnterior);
			}
		}

		return historicoMedicao;
	}

	private void popularHistoricoMedicaoAnterior(HistoricoMedicao historicoMedicao,
			HistoricoMedicao historicoMedicaoCicloAnterior, PontoConsumo pontoConsumo) throws NegocioException {

		ControladorHistoricoConsumo controladorHistoricoConsumo = ServiceLocator.getInstancia()
				.getControladorHistoricoConsumo();
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();
		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();

		historicoMedicao
				.setAnormalidadeLeituraAnterior(historicoMedicaoCicloAnterior.getAnormalidadeLeituraInformada());
		historicoMedicao.setNumeroLeituraAnterior(historicoMedicaoCicloAnterior.getNumeroLeituraFaturada());
		historicoMedicao.setDataLeituraAnterior(historicoMedicaoCicloAnterior.getDataLeituraFaturada());

		BigDecimal saldoCredito = BigDecimal.ZERO;

		HistoricoConsumo historicoConsumo = controladorHistoricoConsumo
				.consultarHistoricoConsumoPorHistoricoMedicao(historicoMedicaoCicloAnterior.getChavePrimaria());
		if (historicoMedicaoCicloAnterior.getCreditoVolume() != null) {
			saldoCredito = historicoMedicaoCicloAnterior.getCreditoVolume();

			if (historicoConsumo != null && historicoConsumo.getConsumoCredito() != null) {
				saldoCredito = saldoCredito.subtract(historicoConsumo.getConsumoCredito());
			}
		}

		String codigoAnormalidadeLeitura = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_BLOQUEIO_UTILIZADA);
		if (getControladorPontoConsumo().isPontoConsumoAtivo(pontoConsumo)
				&& historicoMedicaoCicloAnterior.getAnormalidadeLeituraFaturada() != null
				&& historicoMedicaoCicloAnterior.getAnormalidadeLeituraFaturada().getChavePrimaria() == Long
						.parseLong(codigoAnormalidadeLeitura)) {

			FiltroHistoricoOperacaoMedidor filtro = new FiltroHistoricoOperacaoMedidor();
			filtro.setIdPontoConsumo(pontoConsumo.getChavePrimaria());
			filtro.setOrdenacaoDataRealizada(Ordenacao.DESCENDENTE);
			Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor = controladorMedidor
					.obterHistoricoOperacaoMedidor(filtro);

			HistoricoOperacaoMedidor historicoOperacaoMedidorReativacao = null;
			HistoricoOperacaoMedidor historicoOperacaoMedidorBloqueio = null;
			Iterator<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidorIterator = listaHistoricoOperacaoMedidor
					.iterator();
			if (listaHistoricoOperacaoMedidorIterator.hasNext()) {
				historicoOperacaoMedidorReativacao = listaHistoricoOperacaoMedidorIterator.next();
			}
			if (listaHistoricoOperacaoMedidorIterator.hasNext()) {
				historicoOperacaoMedidorBloqueio = listaHistoricoOperacaoMedidorIterator.next();
			}

			if (historicoOperacaoMedidorReativacao != null && historicoOperacaoMedidorBloqueio != null
					&& historicoOperacaoMedidorReativacao.getOperacaoMedidor()
							.getChavePrimaria() == OperacaoMedidor.CODIGO_REATIVACAO
					&& historicoOperacaoMedidorBloqueio.getOperacaoMedidor()
							.getChavePrimaria() == OperacaoMedidor.CODIGO_BLOQUEIO
					&& historicoOperacaoMedidorReativacao.getNumeroLeitura() != historicoOperacaoMedidorBloqueio
							.getNumeroLeitura()) {

				String codigoAnormalidadeLeituraBloqueioDifAtivo = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_ATIVACAO_DIF_BLOQUEIO);

				AnormalidadeLeitura anormalidadeLeituraBloqueioDifAtivo = controladorAnormalidade
						.obterAnormalidadeLeitura(Long.parseLong(codigoAnormalidadeLeituraBloqueioDifAtivo));
				historicoMedicao.setAnormalidadeLeituraFaturada(anormalidadeLeituraBloqueioDifAtivo);
			}

		}

		historicoMedicao.setCreditoSaldo(saldoCredito);
	}

	/**
	 * Popular historico medicao.
	 *
	 * @param supervisorio         the supervisorio
	 * @param supervisorioAnterior the supervisorio anterior
	 * @param pontoConsumo         the ponto consumo
	 * @return the historico medicao
	 * @throws NegocioException the negocio exception
	 */
	private HistoricoMedicao popularHistoricoMedicao(SupervisorioMedicaoDiaria supervisorio,
			SupervisorioMedicaoDiaria supervisorioAnterior, PontoConsumo pontoConsumo) throws NegocioException {

		ControladorHistoricoConsumo controladorHistoricoConsumo = ServiceLocator.getInstancia()
				.getControladorHistoricoConsumo();
		ControladorHistoricoMedicao controladorHistoricoMedicao = ServiceLocator.getInstancia()
				.getControladorHistoricoMedicao();
		ServiceLocator.getInstancia().getControladorParametroSistema();

		ServiceLocator.getInstancia().getControladorNegocio(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);

		HistoricoMedicao historicoMedicao = (HistoricoMedicao) controladorHistoricoMedicao.criar();
		historicoMedicao.setAnoMesLeitura(supervisorio.getDataReferencia());
		historicoMedicao.setNumeroCiclo(supervisorio.getNumeroCiclo());
		historicoMedicao.setSequenciaLeitura(1);
		historicoMedicao.setDataLeituraInformada(supervisorio.getDataRealizacaoLeitura());
		historicoMedicao.setNumeroLeituraInformada(supervisorio.getLeituraComCorrecaoFatorPTZ());

		this.verificarStatusMedidorNoPontoConsumo(null, null, supervisorio.getDataRealizacaoLeitura(), pontoConsumo,
				historicoMedicao);

		historicoMedicao.setDataProcessamento(Calendar.getInstance().getTime());
		historicoMedicao.setUltimaAlteracao(Calendar.getInstance().getTime());
		historicoMedicao.setHabilitado(Boolean.TRUE);
		historicoMedicao.setPontoConsumo(pontoConsumo);
		historicoMedicao.setNumeroLeituraCorretor(supervisorio.getLeituraSemCorrecaoFatorPTZ());
		historicoMedicao.setConsumoCorretor(supervisorio.getConsumoComCorrecaoFatorPTZ());
		historicoMedicao.setConsumoMedidor(supervisorio.getConsumoSemCorrecaoFatorPTZ());
		historicoMedicao.setFatorPTZCorretor(supervisorio.getFatorPTZ());

		if (supervisorioAnterior != null) {
			historicoMedicao.setNumeroLeituraAnterior(supervisorioAnterior.getLeituraComCorrecaoFatorPTZ());
			historicoMedicao.setDataLeituraAnterior(supervisorioAnterior.getDataRealizacaoLeitura());
		} else {
			HistoricoMedicao historicoMedicaoCicloAnterior = controladorHistoricoMedicao
					.obterUltimoHistoricoMedicao(pontoConsumo.getChavePrimaria(), null, null, null);

			if (historicoMedicaoCicloAnterior != null) {
				historicoMedicao.setAnormalidadeLeituraAnterior(
						historicoMedicaoCicloAnterior.getAnormalidadeLeituraInformada());
				historicoMedicao.setNumeroLeituraAnterior(historicoMedicaoCicloAnterior.getNumeroLeituraInformada());
				historicoMedicao.setDataLeituraAnterior(historicoMedicaoCicloAnterior.getDataLeituraInformada());

				BigDecimal saldoCredito = BigDecimal.ZERO;

				HistoricoConsumo historicoConsumo = controladorHistoricoConsumo
						.consultarHistoricoConsumoPorHistoricoMedicao(historicoMedicaoCicloAnterior.getChavePrimaria());
				if (historicoMedicaoCicloAnterior.getCreditoVolume() != null) {
					saldoCredito = historicoMedicaoCicloAnterior.getCreditoVolume();

					if (historicoConsumo != null && historicoConsumo.getConsumoCredito() != null) {
						saldoCredito = saldoCredito.subtract(historicoConsumo.getConsumoCredito());
					}
				}

				historicoMedicao.setCreditoSaldo(saldoCredito);
			} else {

				ServiceLocator.getInstancia()
						.getBeanPorID(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

				ControladorVazaoCorretor controladorVazaoCorretor = (ControladorVazaoCorretor) ServiceLocator
						.getInstancia().getBeanPorID(ControladorVazaoCorretor.BEAN_ID_CONTROLADOR_VAZAO_CORRETOR);

				ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
						.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

				String codigoOperacaoMedidorInstalacao = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_OPERACAO_INSTALACAO);

				Date dataLeitura = null;
				BigDecimal leitura = null;

				if (pontoConsumo.getInstalacaoMedidor() != null
						&& pontoConsumo.getInstalacaoMedidor().getVazaoCorretor() != null) {
					VazaoCorretor vazaoCorretor = pontoConsumo.getInstalacaoMedidor().getVazaoCorretor();
					Collection<VazaoCorretorHistoricoOperacao> vazaoCorretorHistoricoOperacao = controladorVazaoCorretor
							.consultarHistoricoCorretorVazao(pontoConsumo.getChavePrimaria(),
									Long.valueOf(codigoOperacaoMedidorInstalacao), null,
									vazaoCorretor.getChavePrimaria(), null, null);
					dataLeitura = vazaoCorretorHistoricoOperacao.iterator().next().getDataRealizada();
					leitura = vazaoCorretorHistoricoOperacao.iterator().next().getLeitura();
				}

				historicoMedicao.setNumeroLeituraAnterior(leitura);
				historicoMedicao.setDataLeituraAnterior(dataLeitura);
			}
		}

		return historicoMedicao;
	}

	/**
	 * Popular historico medicao mi.
	 *
	 * @param supervisorio         the supervisorio
	 * @param supervisorioAnterior the supervisorio anterior
	 * @param medidor              the medidor
	 * @param pontoConsumo         the ponto consumo
	 * @return the historico medicao
	 * @throws GGASException the GGAS exception
	 */
	private HistoricoMedicao popularHistoricoMedicaoMI(SupervisorioMedicaoDiaria supervisorio,
			SupervisorioMedicaoDiaria supervisorioAnterior, Medidor medidor, PontoConsumo pontoConsumo)
			throws GGASException {

		ControladorHistoricoConsumo controladorHistoricoConsumo = ServiceLocator.getInstancia()
				.getControladorHistoricoConsumo();
		ControladorHistoricoMedicao controladorHistoricoMedicao = ServiceLocator.getInstancia()
				.getControladorHistoricoMedicao();
		ServiceLocator.getInstancia().getControladorParametroSistema();

		ServiceLocator.getInstancia().getControladorNegocio(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);

		HistoricoMedicao historicoMedicao = (HistoricoMedicao) controladorHistoricoMedicao.criar();
		historicoMedicao.setAnoMesLeitura(supervisorio.getDataReferencia());
		historicoMedicao.setNumeroCiclo(supervisorio.getNumeroCiclo());
		historicoMedicao.setSequenciaLeitura(1);
		historicoMedicao.setDataLeituraInformada(supervisorio.getDataRealizacaoLeitura());
		historicoMedicao.setNumeroLeituraInformada(supervisorio.getLeituraComCorrecaoFatorPTZ());

		this.verificarStatusMedidorNoPontoConsumoMI(null, null, supervisorio.getDataRealizacaoLeitura(), medidor,
				historicoMedicao);

		historicoMedicao.setDataProcessamento(Calendar.getInstance().getTime());
		historicoMedicao.setUltimaAlteracao(Calendar.getInstance().getTime());
		historicoMedicao.setHabilitado(Boolean.TRUE);
		historicoMedicao.setPontoConsumo(pontoConsumo);
		historicoMedicao.setNumeroLeituraCorretor(supervisorio.getLeituraSemCorrecaoFatorPTZ());
		historicoMedicao.setConsumoCorretor(supervisorio.getConsumoComCorrecaoFatorPTZ());
		historicoMedicao.setConsumoMedidor(supervisorio.getConsumoSemCorrecaoFatorPTZ());
		historicoMedicao.setFatorPTZCorretor(supervisorio.getFatorPTZ());

		if (supervisorioAnterior != null) {
			historicoMedicao.setNumeroLeituraAnterior(supervisorioAnterior.getLeituraComCorrecaoFatorPTZ());
			historicoMedicao.setDataLeituraAnterior(supervisorioAnterior.getDataRealizacaoLeitura());
		} else {
			HistoricoMedicao historicoMedicaoCicloAnterior = controladorHistoricoMedicao
					.obterUltimoHistoricoMedicao(medidor, null, null, null);

			if (historicoMedicaoCicloAnterior != null) {
				historicoMedicao.setAnormalidadeLeituraAnterior(
						historicoMedicaoCicloAnterior.getAnormalidadeLeituraInformada());
				historicoMedicao.setNumeroLeituraAnterior(historicoMedicaoCicloAnterior.getNumeroLeituraInformada());
				historicoMedicao.setDataLeituraAnterior(historicoMedicaoCicloAnterior.getDataLeituraInformada());

				BigDecimal saldoCredito = BigDecimal.ZERO;

				HistoricoConsumo historicoConsumo = controladorHistoricoConsumo
						.consultarHistoricoConsumoPorHistoricoMedicao(historicoMedicaoCicloAnterior.getChavePrimaria());
				if (historicoMedicaoCicloAnterior.getCreditoVolume() != null) {
					saldoCredito = historicoMedicaoCicloAnterior.getCreditoVolume();

					if (historicoConsumo != null && historicoConsumo.getConsumoCredito() != null) {
						saldoCredito = saldoCredito.subtract(historicoConsumo.getConsumoCredito());
					}
				}

				historicoMedicao.setCreditoSaldo(saldoCredito);
			} else {

				ServiceLocator.getInstancia()
						.getBeanPorID(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

				ControladorVazaoCorretor controladorVazaoCorretor = (ControladorVazaoCorretor) ServiceLocator
						.getInstancia().getBeanPorID(ControladorVazaoCorretor.BEAN_ID_CONTROLADOR_VAZAO_CORRETOR);

				ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
						.getInstancia()
						.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

				String codigoOperacaoMedidorInstalacao = controladorConstanteSistema
						.obterValorConstanteSistemaPorCodigo(Constantes.C_OPERACAO_INSTALACAO);

				Date dataLeitura = null;
				BigDecimal leitura = null;

				if (medidor.getInstalacaoMedidor() != null
						&& medidor.getInstalacaoMedidor().getVazaoCorretor() != null) {
					VazaoCorretor vazaoCorretor = medidor.getInstalacaoMedidor().getVazaoCorretor();
					Collection<VazaoCorretorHistoricoOperacao> vazaoCorretorHistoricoOperacao = controladorVazaoCorretor
							.consultarHistoricoCorretorVazao(null, Long.valueOf(codigoOperacaoMedidorInstalacao), null,
									vazaoCorretor.getChavePrimaria(), null, medidor.getChavePrimaria());
					dataLeitura = vazaoCorretorHistoricoOperacao.iterator().next().getDataRealizada();
					leitura = vazaoCorretorHistoricoOperacao.iterator().next().getLeitura();
				}

				historicoMedicao.setNumeroLeituraAnterior(leitura);
				historicoMedicao.setDataLeituraAnterior(dataLeitura);
			}
		}

		return historicoMedicao;
	}

	/**
	 * Validar movimento leitura.
	 *
	 * @param leituraMovimento the leitura movimento
	 * @param relatorios       the relatorios
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	private boolean validarMovimentoLeitura(LeituraMovimento leituraMovimento,
			Collection<DadosRegistrarLeiturasAnormalidades> relatorios) throws NegocioException {

		// FA2 - Data Hora de Leitura informada
		// inválida
		boolean temErro1 = this.verificarDataHoraLeituraInvalida(leituraMovimento, relatorios);
		if (temErro1) {
			throw new NegocioException("Data Hora de Leitura informada inválida");
		}

		// FA3 - Anormalidade de Leitura inativa
		boolean temErro2 = this.verificarAnormalidadeLeituraInativa(leituraMovimento, relatorios);
		if (temErro2) {
			throw new NegocioException("Anormalidade de Leitura inativa");
		}

		// FA5 - Medidor do Movimento de Leitura não é o mais recente associado ao Ponto
		// de Consumo
		boolean temErro3 = this.verificarMovimentoLeituraComPontoConsumo(leituraMovimento, relatorios);
		if (temErro3) {
			throw new NegocioException(
					"Medidor do Movimento de Leitura não é o mais recente associado ao Ponto de Consumo");
		}

		ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator
				.getInstancia().getBeanPorID(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);
		Collection<HistoricoMedicao> listaHistoricoMedicao = null;

		listaHistoricoMedicao = controladorHistoricoMedicao.consultarHistoricoMedicao(
				leituraMovimento.getPontoConsumo().getChavePrimaria(), leituraMovimento.getAnoMesFaturamento(),
				leituraMovimento.getCiclo());
		// FA5 - Medidor do Movimento de Leitura não é o mais recente associado ao Ponto
		// de Consumo
		boolean temErro4 = this.verificarMovimentoLeituraExistente(leituraMovimento, relatorios, listaHistoricoMedicao);
		if (temErro4) {
			throw new NegocioException("Leitura movimento não encontrada");
		}

		// erro caso não tenha medidor instalado e ativado na época da leitura
		boolean temErro5 = this.verificarStatusMedidorNoPontoConsumo(leituraMovimento, relatorios,
				leituraMovimento.getDataLeitura(), leituraMovimento.getPontoConsumo(), null);
		if (temErro5) {
			throw new NegocioException("Não tem medidor instalado e ativado na época da leitura");
		}

		// Verifica se o ponto de consumo não tem contrato associado, a leitura atual é
		// igual a
		// leitura anterior e o medidor do ponto de consumo está instalado. Caso atenda
		// a esses critérios, não deve gerar histórico de
		// medição.
		boolean temErro6 = this.verificarCondicoesRotaFiscalizacao(leituraMovimento, relatorios, listaHistoricoMedicao);
		if (temErro6) {
			throw new NegocioException(
					"Verifica se o ponto de consumo não tem contrato associado, a leitura atual é igual a "
							+ "leitura anterior e o medidor do ponto de consumo está instalado");
		}
		return false;
	}

	/**
	 * Verificar condicoes rota fiscalizacao.
	 *
	 * @param leituraMovimento      the leitura movimento
	 * @param relatorios            the relatorios
	 * @param listaHistoricoMedicao the lista historico medicao
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	private boolean verificarCondicoesRotaFiscalizacao(LeituraMovimento leituraMovimento,
			Collection<DadosRegistrarLeiturasAnormalidades> relatorios,
			Collection<HistoricoMedicao> listaHistoricoMedicao) throws NegocioException {

		boolean temErro = false;
		ControladorContrato controladorContrato = ServiceLocator.getInstancia().getControladorContrato();
		// Obtendo o contrato ativo do ponto
		// de consumo.
		ContratoPontoConsumo contratoPontoConsumo = controladorContrato
				.obterContratoAtivoPontoConsumo(leituraMovimento.getPontoConsumo());

		// Para obter o contrato na inclusão
		// de fatura de encerramento (quando
		// também se trabalha com contrato
		// ADITADO e ALTERADO).
		if (contratoPontoConsumo == null) {
			contratoPontoConsumo = controladorContrato.consultarContratoPontoConsumoPorPontoConsumoRecente(
					leituraMovimento.getPontoConsumo().getChavePrimaria());
		}
		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ConstanteSistema cAtivo = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_STATUS_PONTO_CONSUMO_ATIVO);

		// Se o ponto de consumo não tiver contrato associado, a leitura atual for
		// diferente da anterior e o
		// medidor estiver apenas instalado, deverá gerar anormalidade de Inicio de
		// Consumo.
		if (listaHistoricoMedicao != null && !listaHistoricoMedicao.isEmpty()) {
			for (HistoricoMedicao historicoMedicao : listaHistoricoMedicao) {
				if (historicoMedicao.getConsumoInformado() != null) {
					if (((historicoMedicao.getConsumoInformado().compareTo(leituraMovimento.getValorLeitura()) != 0)
							&& leituraMovimento.getPontoConsumo().getSituacaoConsumo().getChavePrimaria() != Long
									.parseLong(cAtivo.getValor()))
							|| (historicoMedicao.getConsumoInformado()
									.compareTo(leituraMovimento.getValorLeitura()) != 0)
									&& leituraMovimento.getPontoConsumo().getSituacaoConsumo()
											.getChavePrimaria() == Long.parseLong(cAtivo.getValor())
									&& contratoPontoConsumo == null) {
						String ocorrencia = "Ocorreu um erro durante o processamento, verifique o status do ponto de consumo e do contrato";
						this.setarInformacoesRelatorio(leituraMovimento, relatorios, ocorrencia);
						temErro = true;
						break;
						// não gera historico medição
					} else if ((historicoMedicao.getConsumoInformado()
							.compareTo(leituraMovimento.getValorLeitura()) == 0)
							&& leituraMovimento.getPontoConsumo().getSituacaoConsumo().getChavePrimaria() != Long
									.parseLong(cAtivo.getValor())) {
						temErro = true;
						break;
					}
				}
			}
		} else {

			if ((contratoPontoConsumo == null)
					&& (leituraMovimento.getValorLeitura() != null
							&& leituraMovimento.getValorLeitura().compareTo(BigDecimal.valueOf(0)) > 0)
					&& leituraMovimento.getPontoConsumo().getSituacaoConsumo().getChavePrimaria() == Long
							.parseLong(cAtivo.getValor())) {

				String ocorrencia = "Ocorreu um erro durante o processamento, verifique o status do ponto de consumo e do contrato";
				this.setarInformacoesRelatorio(leituraMovimento, relatorios, ocorrencia);
				temErro = true;
			}
		}

		return temErro;
	}

	/**
	 * Verificar status medidor no ponto consumo.
	 *
	 * @param leituraMovimento      the leitura movimento
	 * @param relatorios            the relatorios
	 * @param dataRealizacaoLeitura the data realizacao leitura
	 * @param pontoConsumo          the ponto consumo
	 * @param historicoMedicao      the historico medicao
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	private boolean verificarStatusMedidorNoPontoConsumo(LeituraMovimento leituraMovimento,
			Collection<DadosRegistrarLeiturasAnormalidades> relatorios, Date dataRealizacaoLeitura,
			PontoConsumo pontoConsumo, HistoricoMedicao historicoMedicao) throws NegocioException {

		Boolean temErro = Boolean.FALSE;

		ControladorMedidor controladorMedidor = (ControladorMedidor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);

		// verifica como estava o status do ponto de consumo na data que a leitura foi
		// realizada
		Map<String, Object> mapaInstalacaoMedidor = controladorMedidor
				.verificarStatusPontoConsumo(dataRealizacaoLeitura, pontoConsumo);

		InstalacaoMedidor instalacaoMedidor = null;
		if (mapaInstalacaoMedidor != null && !mapaInstalacaoMedidor.isEmpty()) {

			Boolean isStatusAtivo = (Boolean) mapaInstalacaoMedidor.get("isStatusAtivo");

			if (isStatusAtivo != null && isStatusAtivo) {

				if (historicoMedicao != null) {

					instalacaoMedidor = (InstalacaoMedidor) mapaInstalacaoMedidor.get("instalacaoMedidor");
					historicoMedicao.setHistoricoInstalacaoMedidor(instalacaoMedidor);
				}
			} else {

				if (leituraMovimento != null && relatorios != null) {

					String ocorrencia = "Medidor não estava instalado ou ativado na data da leitura.";
					this.setarInformacoesRelatorio(leituraMovimento, relatorios, ocorrencia);
					temErro = Boolean.TRUE;
				}
			}
		} else {
			String ocorrencia = "Medidor não estava instalado ou ativado na data da leitura.";
			this.setarInformacoesRelatorio(leituraMovimento, relatorios, ocorrencia);
			temErro = Boolean.TRUE;
		}

		return temErro;
	}

	/**
	 * Verificar status medidor no ponto consumo mi.
	 *
	 * @param leituraMovimento      the leitura movimento
	 * @param relatorios            the relatorios
	 * @param dataRealizacaoLeitura the data realizacao leitura
	 * @param medidor               the medidor
	 * @param historicoMedicao      the historico medicao
	 * @return true, if successful
	 * @throws GGASException the GGAS exception
	 */
	private boolean verificarStatusMedidorNoPontoConsumoMI(LeituraMovimento leituraMovimento,
			Collection<DadosRegistrarLeiturasAnormalidades> relatorios, Date dataRealizacaoLeitura, Medidor medidor,
			HistoricoMedicao historicoMedicao) throws GGASException {

		Boolean temErro = Boolean.FALSE;

		ControladorMedidor controladorMedidor = (ControladorMedidor) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorMedidor.BEAN_ID_CONTROLADOR_MEDIDOR);

		Collection<Medidor> listaMedidorVirtual = controladorMedidor
				.consultarMedidorVirtualPorMedidorIndependente(medidor);
		// verifica como estava o status do ponto de consumo na data que a leitura foi
		// realizada
		PontoConsumo pontoConsumo = listaMedidorVirtual.iterator().next().getInstalacaoMedidor().getPontoConsumo();
		Map<String, Object> mapaInstalacaoMedidor = controladorMedidor
				.verificarStatusPontoConsumo(dataRealizacaoLeitura, pontoConsumo);

		if (mapaInstalacaoMedidor != null && !mapaInstalacaoMedidor.isEmpty()) {
			Boolean isStatusAtivo = (Boolean) mapaInstalacaoMedidor.get("isStatusAtivo");

			if (isStatusAtivo != null && isStatusAtivo) {
				if (historicoMedicao != null) {

					historicoMedicao.setHistoricoInstalacaoMedidor(medidor.getInstalacaoMedidor());
				}
			} else {
				if (leituraMovimento != null && relatorios != null) {

					String ocorrencia = "Medidor não estava instalado ou ativado na data da leitura.";
					this.setarInformacoesRelatorio(leituraMovimento, relatorios, ocorrencia);
					temErro = Boolean.TRUE;
				}
			}
		}

		return temErro;
	}

	/**
	 * Validar movimento leitura diaria supervisorio.
	 *
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @param pontoConsumo              the ponto consumo
	 * @param relatorios                the relatorios
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	private boolean validarMovimentoLeituraDiariaSupervisorio(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
			PontoConsumo pontoConsumo, Collection<DadosRegistrarLeiturasAnormalidades> relatorios)
			throws NegocioException {

		Boolean erro1 = this.validarDataMedidorInstalacao(supervisorioMedicaoDiaria, pontoConsumo, relatorios);

		Boolean erro2 = this.verificarMovimentoLeituraExistente(supervisorioMedicaoDiaria, pontoConsumo, relatorios);

		return erro1 || erro2;
	}

	/**
	 * Se o Medidor contido no Movimento de Leitura a ser Registrado não tem
	 * associação com um ponto de consumo ou está associado a um ponto de consumo
	 * diferente do existente no Movimento de Leitura.
	 *
	 * @param leituraMovimento the leitura movimento
	 * @param relatorios       the relatorios
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	private boolean verificarMovimentoLeituraComPontoConsumo(LeituraMovimento leituraMovimento,
			Collection<DadosRegistrarLeiturasAnormalidades> relatorios) throws NegocioException {

		boolean temErro = false;
		ControladorImovel controladorImovel = ServiceLocator.getInstancia().getControladorImovel();

		PontoConsumo pontoConsumo = controladorImovel.obterPontoConsumoPorChaveMedidor(
				leituraMovimento.getInstalacaoMedidor().getMedidor().getChavePrimaria());

		boolean isAssociacaoDiferente = (pontoConsumo != null)
				&& (pontoConsumo.getChavePrimaria() != leituraMovimento.getPontoConsumo().getChavePrimaria());

		if (pontoConsumo == null || isAssociacaoDiferente) {
			String ocorrencia = "Medidor não tem associação com o ponto de consumo.";
			this.setarInformacoesRelatorio(leituraMovimento, relatorios, ocorrencia);
			temErro = true;
		}
		return temErro;
	}

	/**
	 * Se no Subfluxo de Registro de Leituras, a Anormalidade Leitura informada de
	 * Campos não for encontrada ou estiver com situação ‘inativa’.
	 *
	 * @param leituraMovimento the leitura movimento
	 * @param relatorios       the relatorios
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	private boolean verificarAnormalidadeLeituraInativa(LeituraMovimento leituraMovimento,
			Collection<DadosRegistrarLeiturasAnormalidades> relatorios) throws NegocioException {

		boolean temErro = false;
		boolean anormalidadeNaoEncontrada = false;
		boolean isAnormalidadeInativa = false;

		ControladorAnormalidade controladorAnormalidade = ServiceLocator.getInstancia().getControladorAnormalidade();

		if (leituraMovimento.getCodigoAnormalidadeLeitura() != null) {
			Map<String, Object> filtro = new HashMap<String, Object>();
			filtro.put(CHAVE_PRIMARIA, leituraMovimento.getCodigoAnormalidadeLeitura());
			Collection<AnormalidadeLeitura> listaAnormalidade = controladorAnormalidade
					.consultarAnormalidadesLeitura(filtro);

			if (listaAnormalidade == null || listaAnormalidade.isEmpty()) {
				anormalidadeNaoEncontrada = true;
			} else {
				AnormalidadeLeitura anormalidade = listaAnormalidade.iterator().next();
				isAnormalidadeInativa = (anormalidade != null) && (!anormalidade.isHabilitado());
			}

			if (anormalidadeNaoEncontrada || isAnormalidadeInativa) {
				String ocorrencia = "Anormalidade da leitura inativa.";
				this.setarInformacoesRelatorio(leituraMovimento, relatorios, ocorrencia);
				temErro = true;
			}
		}
		return temErro;
	}

	/**
	 * Se no Subfluxo de Registro de Leituras, a Data/Hora de Leitura informada
	 * contida no Movimento de Leitura do Ponto de Consumo for inválida, for menor
	 * que 2 referências anteriores da Referência Atual de Faturamento ou maior que
	 * a Data Atual.
	 *
	 * @param leituraMovimento the leitura movimento
	 * @param relatorios       the relatorios
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	private boolean verificarDataHoraLeituraInvalida(final LeituraMovimento leituraMovimento,
			Collection<DadosRegistrarLeiturasAnormalidades> relatorios) throws NegocioException {

		boolean temErro = false;
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia()
				.getControladorParametroSistema();

		if (leituraMovimento.getDataLeitura() != null) {
			LeituraMovimento leituraMovimentoLocal = (LeituraMovimento) obter(leituraMovimento.getChavePrimaria());
			String dataConvertida = DataUtil.converterDataParaString(leituraMovimentoLocal.getDataLeitura(), true);

			DateTime dataLeitura = new DateTime(leituraMovimentoLocal.getDataLeitura());
			Integer anoMesFaturamento = null;
			try {
				String anoMes = String.valueOf(
						controladorParametroSistema.obterValorDoParametroPorCodigo(PARAMETRO_REFERENCIA_FATURAMENTO));
				anoMesFaturamento = Integer.valueOf(anoMes);

			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				throw new NegocioException(ControladorLeituraMovimento.ERRO_NEGOCIO_PARAMETRO_NAO_ENCONTRADO,
						LeituraMovimento.REFERENCIA_FATURAMENTO);
			}

			String ano = String.valueOf(dataLeitura.getYear());
			String mes = Util.adicionarZerosEsquerdaNumero(String.valueOf(dataLeitura.getMonthOfYear()), TAMANHO_TOTAL);
			Integer dataReferenciaLeitura = Integer.valueOf(ano + mes);
			Integer referenciaAnterior = Util.diminuirMesEmAnoMes(anoMesFaturamento, QTD_MES);

			boolean isDataInvalida = !Util.isDataValida(dataConvertida, Constantes.FORMATO_DATA_HORA_BR);
			boolean isReferenciaMenor = dataReferenciaLeitura <= referenciaAnterior;
			boolean isMaiorDataAtual = leituraMovimentoLocal.getDataLeitura().after(Calendar.getInstance().getTime());
			boolean isDataleituraAnteriorMaiorDataLeituraAtual = leituraMovimentoLocal.getDataLeituraAnterior()
					.after(leituraMovimentoLocal.getDataLeitura());

			if (isDataInvalida || isReferenciaMenor || isMaiorDataAtual || isDataleituraAnteriorMaiorDataLeituraAtual) {
				String ocorrencia = "Data e hora da leitura inválida.";
				this.setarInformacoesRelatorio(leituraMovimentoLocal, relatorios, ocorrencia);
				temErro = true;
			}
		}
		return temErro;
	}

	/**
	 * Verificar movimento leitura existente.
	 *
	 * @param leituraMovimento      the leitura movimento
	 * @param relatorios            the relatorios
	 * @param listaHistoricoMedicao the lista historico medicao
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	private boolean verificarMovimentoLeituraExistente(LeituraMovimento leituraMovimento,
			Collection<DadosRegistrarLeiturasAnormalidades> relatorios,
			Collection<HistoricoMedicao> listaHistoricoMedicao) {

		boolean temErro = false;
		if (listaHistoricoMedicao != null && !listaHistoricoMedicao.isEmpty()) {
			for (HistoricoMedicao historicoMedicao : listaHistoricoMedicao) {
				if (historicoMedicao.getDataLeituraInformada().compareTo(leituraMovimento.getDataLeitura()) == 0) {
					String ocorrencia = "Medição já retornada.";
					this.setarInformacoesRelatorio(leituraMovimento, relatorios, ocorrencia);
					temErro = true;
					break;
				}
			}
		}

		return temErro;
	}

	/**
	 * Verificar movimento leitura existente.
	 *
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @param pontoConsumo              the ponto consumo
	 * @param relatorios                the relatorios
	 * @return true, if successful
	 * @throws NegocioException the negocio exception
	 */
	private boolean verificarMovimentoLeituraExistente(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
			PontoConsumo pontoConsumo, Collection<DadosRegistrarLeiturasAnormalidades> relatorios)
			throws NegocioException {

		ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator
				.getInstancia().getBeanPorID(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);
		boolean temErro = false;
		Collection<HistoricoMedicao> listaHistoricoMedicao = null;

		listaHistoricoMedicao = controladorHistoricoMedicao.consultarHistoricoMedicao(pontoConsumo.getChavePrimaria(),
				supervisorioMedicaoDiaria.getDataReferencia(), supervisorioMedicaoDiaria.getNumeroCiclo());
		if (listaHistoricoMedicao != null && !listaHistoricoMedicao.isEmpty()) {
			for (HistoricoMedicao historicoMedicao : listaHistoricoMedicao) {
				if (historicoMedicao.getDataLeituraInformada()
						.compareTo(supervisorioMedicaoDiaria.getDataRealizacaoLeitura()) == 0) {
					String ocorrencia = "Medição já retornada.";
					this.setarInformacoesRelatorio(supervisorioMedicaoDiaria, pontoConsumo, relatorios, ocorrencia);
					temErro = true;
					break;
				}
			}
		}

		return temErro;
	}

	/**
	 * Validar a data de instalacao do medidor
	 *
	 * @param supervisorioMedicaoDiaria
	 * @param pontoConsumo
	 * @param relatorios
	 * @return
	 * @throws NegocioException
	 */
	private boolean validarDataMedidorInstalacao(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
			PontoConsumo pontoConsumo, Collection<DadosRegistrarLeiturasAnormalidades> relatorios)
			throws NegocioException {

		boolean temErro = false;

		InputStream stream = Constantes.class.getClassLoader().getResourceAsStream(Constantes.MENSAGEM_PROPRIEDADES);
		Properties propriedades = new Properties();
		try {
			propriedades.load(stream);
		} catch (IOException e) {
			throw new NegocioException(e);
		}

		if (pontoConsumo.getInstalacaoMedidor() != null) {
			if (pontoConsumo.getInstalacaoMedidor().getData() != null && pontoConsumo.getInstalacaoMedidor().getData()
					.compareTo(supervisorioMedicaoDiaria.getDataRealizacaoLeitura()) > 0) {
				String ocorrencia = (String) propriedades
						.get(Constantes.ERRO_NEGOCIO_DATA_INSTALACAO_MEDIDOR_MAIOR_DATA_LEITURA);
				this.setarInformacoesRelatorio(supervisorioMedicaoDiaria, pontoConsumo, relatorios, ocorrencia);
				temErro = true;

			}
			if (pontoConsumo.getInstalacaoMedidor().getDataAtivacao() != null && pontoConsumo.getInstalacaoMedidor()
					.getDataAtivacao().compareTo(supervisorioMedicaoDiaria.getDataRealizacaoLeitura()) > 0) {
				String ocorrencia = (String) propriedades
						.get(Constantes.ERRO_NEGOCIO_DATA_ATIVACAO_MEDIDOR_MAIOR_DATA_LEITURA);
				this.setarInformacoesRelatorio(supervisorioMedicaoDiaria, pontoConsumo, relatorios, ocorrencia);
				temErro = true;

			}
			if (pontoConsumo.getInstalacaoMedidor().getDataCorretorVazao() != null
					&& pontoConsumo.getInstalacaoMedidor().getDataCorretorVazao()
							.compareTo(supervisorioMedicaoDiaria.getDataRealizacaoLeitura()) > 0) {
				String ocorrencia = (String) propriedades
						.get(Constantes.ERRO_NEGOCIO_DATA_INSTALACAO_CORRETOR_MAIOR_DATA_LEITURA);
				this.setarInformacoesRelatorio(supervisorioMedicaoDiaria, pontoConsumo, relatorios, ocorrencia);
				temErro = true;

			}
		}

		return temErro;
	}

	/**
	 * Setar informacoes relatorio.
	 *
	 * @param leituraMovimento the leitura movimento
	 * @param relatorios       the relatorios
	 * @param ocorrencia       the ocorrencia
	 */
	private void setarInformacoesRelatorio(LeituraMovimento leituraMovimento,
			Collection<DadosRegistrarLeiturasAnormalidades> relatorios, String ocorrencia) {

		DadosRegistrarLeiturasAnormalidades dadoAnormalidade = new DadosRegistrarLeiturasAnormalidades();

		if (leituraMovimento.getLeiturista() != null) {
			dadoAnormalidade.setMatriculaFuncionario(leituraMovimento.getLeiturista().getFuncionario().getMatricula());
		}
		dadoAnormalidade.setMedidor(leituraMovimento.getInstalacaoMedidor().getMedidor().getNumeroSerie());
		if (leituraMovimento.getAnormalidadeConsumo() != null) {
			dadoAnormalidade.setCodigoAnormalidade(leituraMovimento.getAnormalidadeConsumo().getDescricaoAbreviada());
		}
		if (leituraMovimento.getValorLeitura() != null) {
			dadoAnormalidade.setLeitura(String.valueOf(leituraMovimento.getValorLeitura()));
		}
		if (leituraMovimento.getIndicadorConfirmacaoLeitura() != null) {
			dadoAnormalidade.setIndicadorConfirmacao(
					converterValorConfirmacao(leituraMovimento.getIndicadorConfirmacaoLeitura()));
		}
		if (!StringUtils.isEmpty(ocorrencia)) {
			dadoAnormalidade.setErrosTxt(ocorrencia);
		}
		dadoAnormalidade.setGrupoFaturamento(String.valueOf(leituraMovimento.getGrupoFaturamento().getChavePrimaria()));
		dadoAnormalidade.setAnoMesReferencia(leituraMovimento.getAnoMesFaturamentoFormatado());
		dadoAnormalidade.setCiclo(String.valueOf(leituraMovimento.getCiclo()));
		if (!StringUtils.isEmpty(leituraMovimento.getNumeroRota())) {
			dadoAnormalidade.setRota(leituraMovimento.getNumeroRota());
		}

		if (leituraMovimento.getDataLeitura() != null) {
			String dataLei = Util.converterDataParaStringSemHora(leituraMovimento.getDataLeitura(),
					Constantes.FORMATO_DATA_BR);
			dadoAnormalidade.setDataLeitura(dataLei);
		}

		dadoAnormalidade.setMatriculaImovel(String.valueOf(leituraMovimento.getImovel().getChavePrimaria()));
		dadoAnormalidade.setPontoConsumo(leituraMovimento.getPontoConsumo().getDescricao());
		
		
		InstalacaoMedidor instalacaoMedidor = leituraMovimento.getInstalacaoMedidor();
		
		if(instalacaoMedidor != null) {

			String dataInstalacaoMedidor = "";
			String dataAtivacaoMedidor = "";

			if (instalacaoMedidor.getData() != null) {
				dataInstalacaoMedidor = Util.converterDataParaStringSemHora(instalacaoMedidor.getData(),
						Constantes.FORMATO_DATA_BR);
			}

			if (instalacaoMedidor.getDataAtivacao() != null) {
				dataAtivacaoMedidor = Util.converterDataParaStringSemHora(instalacaoMedidor.getDataAtivacao(),
						Constantes.FORMATO_DATA_BR);
			}
			dadoAnormalidade.setDataInstalacaoMedidor(dataInstalacaoMedidor);
			dadoAnormalidade.setDataAtivacaoMedidor(dataAtivacaoMedidor);
		}
		
		relatorios.add(dadoAnormalidade);
	}

	/**
	 * Setar informacoes relatorio.
	 *
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @param pontoConsumo              the ponto consumo
	 * @param relatorios                the relatorios
	 * @param ocorrencia                the ocorrencia
	 */
	private void setarInformacoesRelatorio(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria,
			PontoConsumo pontoConsumo, Collection<DadosRegistrarLeiturasAnormalidades> relatorios, String ocorrencia) {

		DadosRegistrarLeiturasAnormalidades dadoAnormalidade = new DadosRegistrarLeiturasAnormalidades();

		dadoAnormalidade.setMedidor(pontoConsumo.getInstalacaoMedidor().getMedidor().getNumeroSerie());
		if (supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ() != null) {
			dadoAnormalidade.setLeitura(String.valueOf(supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ()));
		}
		if (!StringUtils.isEmpty(ocorrencia)) {
			dadoAnormalidade.setErrosTxt(ocorrencia);
		}
		if (pontoConsumo.getRota() != null && pontoConsumo.getRota().getGrupoFaturamento() != null) {
			dadoAnormalidade.setGrupoFaturamento(
					String.valueOf(pontoConsumo.getRota().getGrupoFaturamento().getChavePrimaria()));
		}
		dadoAnormalidade.setAnoMesReferencia(supervisorioMedicaoDiaria.getDataReferenciaFormatado());
		dadoAnormalidade.setCiclo(String.valueOf(supervisorioMedicaoDiaria.getNumeroCiclo()));
		if (pontoConsumo.getRota() != null) {
			dadoAnormalidade.setRota(pontoConsumo.getRota().getNumeroRota());
		}

		if (supervisorioMedicaoDiaria.getDataRealizacaoLeitura() != null) {
			String dataLei = Util.converterDataParaStringSemHora(supervisorioMedicaoDiaria.getDataRealizacaoLeitura(),
					Constantes.FORMATO_DATA_BR);
			dadoAnormalidade.setDataLeitura(dataLei);
		}

		dadoAnormalidade.setMatriculaImovel(String.valueOf(pontoConsumo.getImovel().getChavePrimaria()));
		dadoAnormalidade.setPontoConsumo(pontoConsumo.getDescricao());
		relatorios.add(dadoAnormalidade);
	}

	/**
	 * Setar informacoes relatorio mi.
	 *
	 * @param supervisorioMedicaoDiaria the supervisorio medicao diaria
	 * @param medidor                   the medidor
	 * @param relatorios                the relatorios
	 * @param ocorrencia                the ocorrencia
	 * @throws GGASException the GGAS exception
	 */
	private void setarInformacoesRelatorioMI(SupervisorioMedicaoDiaria supervisorioMedicaoDiaria, Medidor medidor,
			Collection<DadosRegistrarLeiturasAnormalidades> relatorios, String ocorrencia) throws GGASException {

		DadosRegistrarLeiturasAnormalidades dadoAnormalidade = new DadosRegistrarLeiturasAnormalidades();

		dadoAnormalidade.setMedidor(medidor.getNumeroSerie());
		if (supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ() != null) {
			dadoAnormalidade.setLeitura(String.valueOf(supervisorioMedicaoDiaria.getLeituraComCorrecaoFatorPTZ()));
		}
		if (!StringUtils.isEmpty(ocorrencia)) {
			dadoAnormalidade.setErrosTxt(ocorrencia);
		}
		ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();
		Collection<Medidor> listaMedidorVirtual = controladorMedidor
				.consultarMedidorVirtualPorMedidorIndependente(medidor);
		PontoConsumo pontoConsumo = listaMedidorVirtual.iterator().next().getInstalacaoMedidor().getPontoConsumo();
		if (pontoConsumo.getRota() != null && pontoConsumo.getRota().getGrupoFaturamento() != null) {
			dadoAnormalidade.setGrupoFaturamento(
					String.valueOf(pontoConsumo.getRota().getGrupoFaturamento().getChavePrimaria()));
		}
		dadoAnormalidade.setAnoMesReferencia(supervisorioMedicaoDiaria.getDataReferenciaFormatado());
		dadoAnormalidade.setCiclo(String.valueOf(supervisorioMedicaoDiaria.getNumeroCiclo()));
		if (pontoConsumo.getRota() != null) {
			dadoAnormalidade.setRota(pontoConsumo.getRota().getNumeroRota());
		}

		if (supervisorioMedicaoDiaria.getDataRealizacaoLeitura() != null) {
			String dataLei = Util.converterDataParaStringSemHora(supervisorioMedicaoDiaria.getDataRealizacaoLeitura(),
					Constantes.FORMATO_DATA_BR);
			dadoAnormalidade.setDataLeitura(dataLei);
		}

		dadoAnormalidade.setMatriculaImovel(String.valueOf(pontoConsumo.getImovel().getChavePrimaria()));
		dadoAnormalidade.setPontoConsumo(pontoConsumo.getDescricao());
		relatorios.add(dadoAnormalidade);
	}

	/**
	 * Converter valor confirmacao.
	 *
	 * @param confirmacao the confirmacao
	 * @return the string
	 */
	private String converterValorConfirmacao(boolean confirmacao) {

		String retorno = "";
		if (confirmacao) {
			retorno = "Sim";
		} else {
			retorno = "Não";
		}
		return retorno;
	}

	/**
	 * Consulta movimentos de leitura pelas chaves de leitura_movimento
	 * 
	 * @param leituras as chaves das leituras que se deseja consultar
	 * @return A coleção de LeituraMovimento obtidas a partir das chaves fornecidas
	 */
	private Collection<LeituraMovimento> consultarMovimentosLeituraPorChaveLeituraMovimento(Long[] leituras) {
		Collection<LeituraMovimento> listaLeituras = new ArrayList<>();
		Criteria criteria = getCriteria();
		if (leituras != null && leituras.length > 0) {
			criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, true));
			criteria.addOrder(Order.desc("dataLeitura"));
			addCriteriaIn(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, new ArrayList<>(Arrays.asList(leituras)), criteria);
			listaLeituras = criteria.list();
		}
		return listaLeituras;
	}

	/**
	 * Converte uma cláusula IN com mais de 1000 elementos em grupos de IN pois o
	 * ORACLE não aceita mais de 1000 elementos no IN
	 * 
	 * @param nomeDoCampo O nome do campo que será afetado pelo IN
	 * @param elementos   Os elementos que farão parte do IN
	 * @param criteria    A criteria que executará o select
	 */
	private void addCriteriaIn(String nomeDoCampo, List<?> elementos, Criteria criteria) {
		Disjunction or = Restrictions.disjunction();
		if (elementos.size() > 1000) {
			while (elementos.size() > 1000) {
				List<?> subList = elementos.subList(0, 1000);
				or.add(Restrictions.in(nomeDoCampo, subList));
				elementos.subList(0, 1000).clear();
			}
		}
		or.add(Restrictions.in(nomeDoCampo, elementos));
		criteria.add(or);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.ControladorLeituraMovimento#
	 * consultarMovimentosLeituraPorRota(java.lang.Long[], java.lang.Long,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<LeituraMovimento> consultarMovimentosLeituraPorRota(Long[] chavesPrimarias,
			Long[] chavesSituacaoLeitura, Integer referencia, Integer ciclo) throws NegocioException {

		Collection<LeituraMovimento> retorno = null;
		if (chavesPrimarias != null && chavesPrimarias.length > 0) {
			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName()).append(" movimento");
			hql.append(" where ");
			hql.append(" movimento.rota.chavePrimaria in (:CHAVES_ROTAS)");
			hql.append(" and movimento.habilitado = true");
			if (referencia != null) {
				hql.append(" and movimento.anoMesFaturamento = :REFERENCIA");
				hql.append(" and movimento.ciclo = :CICLO");
			}

			if (chavesSituacaoLeitura != null && chavesSituacaoLeitura.length > 0) {
				hql.append(" and movimento.situacaoLeitura.chavePrimaria in(:SITUACAO)");
			}

			hql.append(" order by movimento.dataLeitura desc");
			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameterList("CHAVES_ROTAS", chavesPrimarias);
			if (chavesSituacaoLeitura != null && chavesSituacaoLeitura.length > 0) {
				query.setParameterList("SITUACAO", chavesSituacaoLeitura);
			}

			if (referencia != null) {
				query.setInteger("REFERENCIA", referencia);
				query.setInteger("CICLO", ciclo);
			}

			retorno = query.list();
		}

		return retorno;
	}

	/**
	 * Listar grupo faturamento para leitura anormalidade.
	 *
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura. ControladorLeituraMovimento#
	 * listarGrupoFaturamentoParaLeituraAnormalidade( )
	 */
	@SuppressWarnings("unchecked")
	public Collection<GrupoFaturamento> listarGrupoFaturamentoParaLeituraAnormalidade() throws NegocioException {

		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia()
				.getControladorParametroSistema();
		Integer anoMesReferencia = Integer.valueOf((String) controladorParametroSistema
				.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO));

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" select distinct leitura.grupoFaturamento from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" leitura ");
		hql.append(" where ");
		hql.append(" leitura.anoMesFaturamento = ? ");
		hql.append(" and leitura.situacaoLeitura.chavePrimaria = ? ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setInteger(0, anoMesReferencia);
		query.setLong(1, SituacaoLeituraMovimento.LEITURA_RETORNADA);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.ControladorLeituraMovimento#
	 * verificarGerarDadosParaLeitura(br.com.ggas.medicao.rota.Rota,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public boolean verificarGerarDadosParaLeitura(Rota rota, Integer anoMesReferencia, Integer numeroCiclo)
			throws NegocioException {

		// de todos os pontos de consumo ativos da rota, verificar se eles tem
		// ocorrencia em leitura movimento
		// para ref e ciclo do cronograma atual

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select pontoConsumo from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where ");
		hql.append(" pontoConsumo.rota.chavePrimaria = :idRota ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = true");
		hql.append(" and pontoConsumo.habilitado = true");
		hql.append(" and (pontoConsumo.instalacaoMedidor is not null ");
		hql.append(" and pontoConsumo.instalacaoMedidor.vazaoCorretor is null) ");
		hql.append(" and pontoConsumo.chavePrimaria NOT IN (");
		hql.append(" 		select leituraMovimento.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" 		leituraMovimento ");
		hql.append(" 		where ");
		hql.append(" 		leituraMovimento.anoMesFaturamento = :anoMesReferencia ");
		hql.append(" 		and leituraMovimento.ciclo = :ciclo ");
		hql.append(" 		and leituraMovimento.rota.chavePrimaria = :idRota)");
		hql.append(" and pontoConsumo.chavePrimaria IN (");
		hql.append(" 		select contratoPontoConsumo.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
		hql.append(" 		contratoPontoConsumo ");
		hql.append(" 		where ");
		hql.append(" 		contratoPontoConsumo.contrato.habilitado = true ");
		hql.append(" 		and contratoPontoConsumo.contrato.situacao.chavePrimaria = :ativo )");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idRota", rota.getChavePrimaria());
		query.setParameter("anoMesReferencia", anoMesReferencia);
		query.setParameter(CICLO, numeroCiclo);
		query.setLong("ativo", SituacaoContrato.ATIVO);

		Collection<PontoConsumo> listaPontoConsumo = query.list();

		removerPontosConsumoQtdDiasLeitura(listaPontoConsumo, null);
		int total = listaPontoConsumo.size();

		return total == 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.ControladorLeituraMovimento#
	 * removerPontosConsumoQtdDiasLeitura(java.util.Collection)
	 */
	@Override
	public void removerPontosConsumoQtdDiasLeitura(Collection<PontoConsumo> pontosConsumo,
			Map<Rota, CronogramaRota> cronogramaPorRota) throws NegocioException {

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
		ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator
				.getInstancia().getBeanPorID(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

		Collection<PontoConsumo> pontosConsumoRemovidos = new HashSet<PontoConsumo>();
		CronogramaRota cronogramaRota = null;
		if (!pontosConsumo.isEmpty()) {

			Map<Long, Collection<HistoricoMedicao>> historicosPorPontosDeConsumo = controladorHistoricoMedicao
					.obterDataLeituraDeUltimoHistoricoMedicao(pontosConsumo);

			for (PontoConsumo pontoConsumo : pontosConsumo) {
				if (pontoConsumo.getSegmento() != null && pontoConsumo.getInstalacaoMedidor() != null) {
					if (cronogramaPorRota != null) {

						cronogramaRota = cronogramaPorRota.get(pontoConsumo.getRota());
					} else {
						cronogramaRota = controladorRota.obterCronogramaRotaAtual(pontoConsumo.getRota(),
								AtividadeSistema.CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA, false, false);
					}

					HistoricoMedicao historicoMedicao = Util
							.primeiroElemento(historicosPorPontosDeConsumo.get(pontoConsumo.getChavePrimaria()));

					Date dataUltimaleitura;
					if (historicoMedicao == null) {
						dataUltimaleitura = controladorHistoricoMedicao.setarDataLeituraAnterior(null, pontoConsumo,
								pontoConsumo.getInstalacaoMedidor().getMedidor());
					} else {
						dataUltimaleitura = historicoMedicao.getDataLeituraInformada();
					}

					Date dataAtual = new Date();

					if (cronogramaRota.getDataPrevista().before(dataAtual)) {
						dataAtual = cronogramaRota.getDataPrevista();
					}

					if (dataUltimaleitura != null) {
						Long quantidadeDias = Math
								.abs((dataAtual.getTime() - dataUltimaleitura.getTime()) / DIA_EM_MILISSEGUNDOS);
						if (quantidadeDias < pontoConsumo.getSegmento().getNumeroDiasFaturamento()) {
							pontosConsumoRemovidos.add(pontoConsumo);
						}
					}
				}
			}
		}
		if (pontosConsumoRemovidos != null && !pontosConsumoRemovidos.isEmpty()) {
			pontosConsumo.removeAll(pontosConsumoRemovidos);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.ControladorLeituraMovimento#
	 * verificarRetornoLeitura(br.com.ggas.medicao.rota.Rota, java.lang.Integer,
	 * java.lang.Integer)
	 */
	@Override
	public boolean verificarRetornoLeitura(Rota rota, Integer anoMesReferencia, Integer numeroCiclo,
			StringBuilder logProcesso) throws NegocioException {

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();
		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));

		// de todos os pontos de consumo ativos da rota, verificar se eles tem
		// ocorrencia em leitura movimento
		// para ref e ciclo do cronograma atual

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select count(pontoConsumo) from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where ");
		hql.append(" pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.rota.chavePrimaria = :idRota ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = true");
		hql.append(" and pontoConsumo.instalacaoMedidor is not null ");

		hql.append(" and pontoConsumo.chavePrimaria NOT IN (");
		hql.append(" 		select distinct historicoMedicao.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeHistoricoMedicao().getSimpleName());
		hql.append(" 		historicoMedicao ");
		hql.append(" 		where ");
		hql.append(" 		historicoMedicao.anoMesLeitura = :anoMesReferencia ");
		hql.append(" 		and historicoMedicao.numeroCiclo = :ciclo ");
		hql.append(" 		and historicoMedicao.pontoConsumo.rota.chavePrimaria = :idRota) ");

		hql.append(" and pontoConsumo.chavePrimaria IN (");
		hql.append(" 		select leituraMovimento.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" 		leituraMovimento ");
		hql.append(" 		where ");
		hql.append(" 		leituraMovimento.anoMesFaturamento = :anoMesReferencia ");
		hql.append(" 		and leituraMovimento.ciclo = :ciclo ");
		hql.append(" 		and (leituraMovimento.codigoAnormalidadeLeitura != :codigoAnormalidadeLeitura ");
		hql.append(" or leituraMovimento.codigoAnormalidadeLeitura is null) ");
		hql.append(" 		and leituraMovimento.rota.chavePrimaria = :idRota)");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idRota", rota.getChavePrimaria());
		query.setParameter("anoMesReferencia", anoMesReferencia);
		query.setParameter("codigoAnormalidadeLeitura", codigoAnormalidadeLeitura);
		query.setParameter(CICLO, numeroCiclo);

		Boolean ehCronogramaRotaRealizado = controladorRota.verificarCronogramaRotaRealizado(rota, anoMesReferencia,
				numeroCiclo, AtividadeSistema.CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA);

		Long total = (Long) query.uniqueResult();

		return total == 0 && ehCronogramaRotaRealizado && !temPontoConsumoEletrocorretor(rota, logProcesso);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.ControladorLeituraMovimento#
	 * verificarRetornoLeituraCorretorVazao(br.com.ggas.medicao.rota.Rota,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public boolean verificarRetornoLeituraCorretorVazao(Rota rota, Integer anoMesReferencia, Integer numeroCiclo)
			throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String valorSituacaoContrato = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);
		Long valorSituacaoContratoLong = null;
		try {
			valorSituacaoContratoLong = Util.converterCampoStringParaValorLong(SITUACAO_DO_CONTRATO,
					valorSituacaoContrato);
		} catch (FormatoInvalidoException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException();
		}

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select pontoConsumo from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where ");
		hql.append(" pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.rota.chavePrimaria = :idRota ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = true");
		hql.append(" and pontoConsumo.instalacaoMedidor is not null ");
		hql.append(" and ( ");
		// quantidade de medicao menor que o minimo
		hql.append(montarConsultaContadorMedicaoHistoricoPontoConsumo());
		hql.append(" < ");
		hql.append(montarConsultaMinimoMaximoDiasCiclo(Boolean.TRUE, rota, anoMesReferencia));

		hql.append(" or ");
		// quantidade de medicao maior que o maximo
		hql.append(montarConsultaContadorMedicaoHistoricoPontoConsumo());
		hql.append(" > ");
		hql.append(montarConsultaMinimoMaximoDiasCiclo(Boolean.FALSE, rota, anoMesReferencia));
		hql.append(" or ");
		// não tiver medicao
		hql.append(montarConsultaContadorMedicaoHistoricoPontoConsumo());
		hql.append(" is null ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idRota", rota.getChavePrimaria());
		query.setParameter("anoMesReferencia", anoMesReferencia);
		query.setParameter("ciclo", numeroCiclo);
		if (rota.getPeriodicidade() != null && (rota.getPeriodicidade().getConsideraMesCivil() == null
				|| !rota.getPeriodicidade().getConsideraMesCivil())) {
			query.setParameter(SITUACAO_CONTRATO_ATIVO, valorSituacaoContratoLong);
		}

		Collection<PontoConsumo> listaPontoConsumo = query.list();

		boolean atualiza = Boolean.FALSE;
		if (listaPontoConsumo == null || listaPontoConsumo.isEmpty()) {
			atualiza = Boolean.TRUE;
		}

		return atualiza;
	}

	/**
	 * Montar consulta minimo maximo dias ciclo.
	 *
	 * @param ehMinimoDias     the eh minimo dias
	 * @param rota             the rota
	 * @param anoMesReferencia the ano mes referencia
	 * @return the string builder
	 */
	private StringBuilder montarConsultaMinimoMaximoDiasCiclo(Boolean ehMinimoDias, Rota rota,
			Integer anoMesReferencia) {

		StringBuilder hql = new StringBuilder();

		if (rota != null && rota.getPeriodicidade() != null && rota.getPeriodicidade().getConsideraMesCivil() != null
				&& rota.getPeriodicidade().getConsideraMesCivil()) {

			hql.append(DataUtil.quantidadeDiasMes(anoMesReferencia));
		} else {

			if (ehMinimoDias) {
				hql.append(" ( select contratoPontoConsumo.periodicidade.numeroMinimoDiasCiclo ");
			} else {
				hql.append(" ( select contratoPontoConsumo.periodicidade.numeroMaximoDiasCiclo ");
			}
			hql.append(" from ");
			hql.append(getClasseEntidadeContratoPontoConsumo().getSimpleName());
			hql.append(" contratoPontoConsumo ");
			hql.append(" where  ");
			hql.append(" pontoConsumo.chavePrimaria = contratoPontoConsumo.pontoConsumo.chavePrimaria ");
			hql.append(" and contratoPontoConsumo.contrato.situacao.chavePrimaria = :situacaoContratoAtivo ");
			hql.append(" and contratoPontoConsumo.contrato.chavePrimariaPrincipal is null ) ");
		}

		return hql;
	}

	/**
	 * Montar consulta contador medicao historico ponto consumo.
	 *
	 * @return the string builder
	 */
	private StringBuilder montarConsultaContadorMedicaoHistoricoPontoConsumo() {

		StringBuilder hql = new StringBuilder();
		hql.append("( select distinct count (*) from ");
		hql.append(getClasseEntidadeHistoricoMedicao().getSimpleName());
		hql.append(" historicoMedicao ");
		hql.append(" where ");
		hql.append(" pontoConsumo.chavePrimaria = historicoMedicao.pontoConsumo.chavePrimaria ");
		hql.append(" and historicoMedicao.anoMesLeitura = :anoMesReferencia ");
		hql.append(" and historicoMedicao.numeroCiclo = :ciclo ");
		hql.append(" and historicoMedicao.habilitado = true ");
		hql.append(
				" group by historicoMedicao.pontoConsumo.chavePrimaria, historicoMedicao.historicoInstalacaoMedidor.chavePrimaria  ) ");

		return hql;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.ControladorLeituraMovimento#
	 * verificarConsistirLeitura(br.com.ggas.medicao.rota.Rota, java.lang.Integer,
	 * java.lang.Integer)
	 */
	@Override
	public boolean verificarConsistirLeitura(Rota rota, Integer anoMesReferencia, Integer numeroCiclo)
			throws NegocioException {
		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();
		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));
		// de todos os pontos de consumo ativos da rota, verificar se eles tem
		// ocorrencia em leitura movimento
		// para ref e ciclo do cronograma atual
		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" select count(pontoConsumo) from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where ");
		hql.append(" pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.rota.chavePrimaria = :idRota ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = true");
		hql.append(" and pontoConsumo.instalacaoMedidor is not null");
		hql.append(" and (");
		hql.append("     (pontoConsumo.chavePrimaria NOT IN (");
		hql.append(" 		select distinct historicoConsumo.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" 		historicoConsumo ");
		hql.append(" 		where ");
		hql.append(" 		historicoConsumo.anoMesFaturamento = :anoMesReferencia ");
		hql.append(" 		and historicoConsumo.numeroCiclo = :ciclo ");
		hql.append(" 		and historicoConsumo.indicadorConsumoCiclo = true");
		hql.append(" 		and historicoConsumo.habilitado = true");
		hql.append(" 		and historicoConsumo.pontoConsumo.rota.chavePrimaria = :idRota))");
		hql.append(" or ");
		hql.append("     (pontoConsumo.chavePrimaria IN (");
		hql.append(" 		select distinct historicoConsumo.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" 		historicoConsumo ");
		hql.append(" 		where ");
		hql.append(" 		historicoConsumo.anoMesFaturamento = :anoMesReferencia ");
		hql.append(" 		and historicoConsumo.numeroCiclo = :ciclo ");
		hql.append(" 		and historicoConsumo.indicadorConsumoCiclo = true");
		hql.append(" 		and historicoConsumo.habilitado = true");
		hql.append(" 		and historicoConsumo.anormalidadeConsumo.bloquearFaturamento = true");
		hql.append(" 		and historicoConsumo.indicadorFaturamento = false");
		hql.append(" 		and historicoConsumo.historicoAtual.analisada = false");
		hql.append(" 		and historicoConsumo.pontoConsumo.rota.chavePrimaria = :idRota))");
		hql.append(" ) ");
		hql.append(" and pontoConsumo.chavePrimaria IN (");
		hql.append(" 		select leituraMovimento.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" 		leituraMovimento ");
		hql.append(" 		where ");
		hql.append(" 		leituraMovimento.anoMesFaturamento = :anoMesReferencia ");
		hql.append(" 		and leituraMovimento.ciclo = :ciclo ");
		hql.append(" 		and (leituraMovimento.codigoAnormalidadeLeitura != :codigoAnormalidadeLeitura ");
		hql.append(" 		or leituraMovimento.codigoAnormalidadeLeitura is null) ");
		hql.append(" 		and leituraMovimento.rota = :idRota)");
		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idRota", rota.getChavePrimaria());
		query.setParameter("anoMesReferencia", anoMesReferencia);
		query.setParameter("codigoAnormalidadeLeitura", codigoAnormalidadeLeitura);
		query.setParameter(CICLO, numeroCiclo);
		Boolean ehCronogramaRotaRealizado = controladorRota.verificarCronogramaRotaRealizado(rota, anoMesReferencia,
				numeroCiclo, AtividadeSistema.CODIGO_ATIVIDADE_RETORNAR_LEITURA);
		Long total = (Long) query.uniqueResult();
		return total == 0 && ehCronogramaRotaRealizado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.ControladorLeituraMovimento#
	 * verificarConsistirLeituraCorretorVazao(br.com.ggas.medicao.rota.Rota,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public boolean verificarConsistirLeituraCorretorVazao(Rota rota, Integer anoMesReferencia, Integer numeroCiclo)
			throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String valorSituacaoContrato = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);
		Long valorSituacaoContratoLong = null;
		try {
			valorSituacaoContratoLong = Util.converterCampoStringParaValorLong(SITUACAO_DO_CONTRATO,
					valorSituacaoContrato);
		} catch (FormatoInvalidoException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException();
		}

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select count(pontoConsumo) from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where ");
		hql.append(" pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.rota.chavePrimaria = :idRota ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = true");
		hql.append(" and pontoConsumo.instalacaoMedidor is not null ");
		hql.append(" and ( ");

		// quantidade de medicao menor que o minimo
		hql.append(montarConsultaContadorConsumoHistoricoPontoConsumo(Boolean.FALSE));
		hql.append(" < ");
		hql.append(montarConsultaMinimoMaximoDiasCiclo(Boolean.TRUE, rota, anoMesReferencia));
		hql.append(" or ");

		// quantidade de medicao maior que o maximo
		hql.append(montarConsultaContadorConsumoHistoricoPontoConsumo(Boolean.FALSE));
		hql.append(" > ");
		hql.append(montarConsultaMinimoMaximoDiasCiclo(Boolean.FALSE, rota, anoMesReferencia));
		hql.append(" or ");

		// não tiver medicao
		hql.append(montarConsultaContadorConsumoHistoricoPontoConsumo(Boolean.FALSE));
		hql.append(" = 0 ");
		hql.append(" or ");

		// tiver anormalidade impeditiva
		hql.append(montarConsultaContadorConsumoHistoricoPontoConsumoAnormalidadeBloqueioFaturamento());
		hql.append(" > 0 ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idRota", rota.getChavePrimaria());
		query.setParameter("anoMesReferencia", anoMesReferencia);
		query.setParameter(CICLO, numeroCiclo);
		if (rota.getPeriodicidade() != null && (rota.getPeriodicidade().getConsideraMesCivil() == null
				|| !rota.getPeriodicidade().getConsideraMesCivil())) {
			query.setParameter(SITUACAO_CONTRATO_ATIVO, valorSituacaoContratoLong);
		}

		Long quantidade = (Long) query.uniqueResult();
		boolean atualiza = Boolean.FALSE;
		if (quantidade == null || quantidade == 0) {
			atualiza = Boolean.TRUE;
		}

		return atualiza;
	}

	/**
	 * Montar consulta contador consumo historico ponto consumo.
	 *
	 * @param consumoFaturado the consumo faturado
	 * @return the string builder
	 */
	private StringBuilder montarConsultaContadorConsumoHistoricoPontoConsumo(Boolean consumoFaturado) {

		StringBuilder hql = new StringBuilder();
		hql.append("( select count (*) from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" historicoConsumo ");
		hql.append(" where ");
		hql.append(" pontoConsumo.chavePrimaria = historicoConsumo.pontoConsumo.chavePrimaria ");
		hql.append(" and historicoConsumo.anoMesFaturamento = :anoMesReferencia ");
		hql.append(" and historicoConsumo.numeroCiclo = :ciclo ");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo = false ");
		if (consumoFaturado) {
			hql.append(" and historicoConsumo.indicadorFaturamento = true ");
		}
		hql.append(" and historicoConsumo.habilitado = true ) ");

		return hql;
	}

	/**
	 * Montar consulta contador consumo historico ponto consumo anormalidade
	 * bloqueio faturamento.
	 *
	 * @return the string builder
	 */
	private StringBuilder montarConsultaContadorConsumoHistoricoPontoConsumoAnormalidadeBloqueioFaturamento() {

		StringBuilder hql = new StringBuilder();
		hql.append("( select count (*) from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" historicoConsumo ");
		hql.append(" where ");
		hql.append(" pontoConsumo.chavePrimaria = historicoConsumo.pontoConsumo.chavePrimaria ");
		hql.append(" and historicoConsumo.anoMesFaturamento = :anoMesReferencia ");
		hql.append(" and historicoConsumo.numeroCiclo = :ciclo ");
		hql.append(" and historicoConsumo.indicadorConsumoCiclo = true ");
		hql.append(" and historicoConsumo.habilitado = true ");
		hql.append(" and historicoConsumo.historicoAtual.analisada = false");
		hql.append(" and historicoConsumo.anormalidadeConsumo.bloquearFaturamento = true ) ");

		return hql;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.ggas.medicao.leitura.ControladorLeituraMovimento#verificarEmitirFatura
	 * (br.com.ggas.medicao.rota.Rota, br.com.ggas.medicao.rota.CronogramaRota,
	 * java.lang.Long)
	 */
	@Override
	public boolean verificarEmitirFatura(Rota rota, CronogramaRota cronogramaRota, Long chaveTipoLeituraEletrocorretor)
			throws NegocioException {

		ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorRota.BEAN_ID_CONTROLADOR_ROTA);

		boolean atingiuDataLimite = cronogramaRota.getCronogramaAtividadeFaturamento() != null
				&& cronogramaRota.getCronogramaAtividadeFaturamento().getDataFim() != null && cronogramaRota
						.getCronogramaAtividadeFaturamento().getDataFim().before(Calendar.getInstance().getTime());
		
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();

		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));

		// de todos os pontos de consumo ativos da rota, verificar se eles tem
		// ocorrencia em leitura movimento
		// para ref e ciclo do cronograma atual

		StringBuilder hql = new StringBuilder();

		hql.append(" select pontoConsumo from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where ");
		hql.append(" pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.rota.chavePrimaria = :idRota ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = true");
		hql.append(" and pontoConsumo.chavePrimaria NOT IN (");
		hql.append(" 		select distinct fatura.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadeFatura().getSimpleName());
		hql.append(" 			fatura ");
		hql.append(" 			where ");
		hql.append(" 			fatura.anoMesReferencia = :anoMesReferencia ");
		hql.append(" 			and fatura.numeroCiclo = :ciclo )");
		if (atingiuDataLimite) {
			hql.append(" and pontoConsumo.chavePrimaria NOT IN (");
			hql.append(" 		select distinct anomalia.pontoConsumo.chavePrimaria from ");
			hql.append(getClasseEntidadeHistoricoAnomaliaFaturamento().getSimpleName());
			hql.append(" 			anomalia ");
			hql.append(" 			where ");
			hql.append(" 			anomalia.anoMesFaturamento = :anoMesReferencia ");
			hql.append(" 			and anomalia.numeroCiclo = :ciclo )");
		}
		hql.append(" and pontoConsumo.chavePrimaria IN (");
		hql.append(" 		select leituraMovimento.pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" 			leituraMovimento ");
		hql.append(" 			where ");
		hql.append(" 			leituraMovimento.anoMesFaturamento = :anoMesReferencia ");
		hql.append(" 			and leituraMovimento.ciclo = :ciclo ");
		hql.append(" 			and (leituraMovimento.codigoAnormalidadeLeitura is null or "
				+ "leituraMovimento.codigoAnormalidadeLeitura != :codigoAnormalidadeLeitura) ");
		hql.append(" 			and leituraMovimento.rota = :idRota)");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idRota", rota.getChavePrimaria());
		query.setParameter("anoMesReferencia", cronogramaRota.getAnoMesReferencia());
		query.setParameter("ciclo", cronogramaRota.getNumeroCiclo());
		query.setParameter("codigoAnormalidadeLeitura", codigoAnormalidadeLeitura);

		Collection<PontoConsumo> listaPontoConsumo = query.list();

		Boolean ehCronogramaRotaRealizado = controladorRota.verificarCronogramaRotaRealizado(rota,
				cronogramaRota.getAnoMesReferencia(), cronogramaRota.getNumeroCiclo(),
				AtividadeSistema.CODIGO_ATIVIDADE_FATURAR);

		int total = listaPontoConsumo.size();

		return total == 0 && ehCronogramaRotaRealizado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura. ControladorLeituraMovimento
	 * #listarSituacaoLeitura()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<SituacaoLeitura> listarSituacaoLeitura() throws NegocioException {

		Criteria criteria = this.createCriteria(SituacaoLeitura.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.ControladorLeituraMovimento#
	 * consultarLeituraMovimento(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<LeituraMovimento> consultarLeituraMovimento(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = createCriteria(getClasseEntidade());
		if (filtro != null) {

			criteria.createAlias(GRUPO_FATURAMENTO, GRUPO_FATURAMENTO);
			criteria.createAlias(SITUACAO_LEITURA, SITUACAO_LEITURA);

			Long codigoGrupoFaturamento = (Long) filtro.get(GRUPO_FATURAMENTO);
			if (codigoGrupoFaturamento != null && codigoGrupoFaturamento > 0) {
				criteria.add(Restrictions.eq("grupoFaturamento.chavePrimaria", codigoGrupoFaturamento));
			}

			Long chaveSituacaoLeitura = (Long) filtro.get("chaveSituacaoLeitura");
			if (chaveSituacaoLeitura != null) {
				criteria.add(Restrictions.eq("situacaoLeitura.chavePrimaria", chaveSituacaoLeitura));
			}

		}
		return criteria.list();
	}

	@Override
	public LocalDateTime buscarInicioLeituraMovimento(Long idRota, Long idGrupoFaturamento) {
		return DataUtil.toLocalDateTime((Date) createHQLQuery("select l.dataLeitura from LeituraMovimentoImpl l where "
				+ "l.grupoFaturamento.chavePrimaria = :idGrupo "
				+ "and l.rota.chavePrimaria = :idRota order by l.dataLeitura").setMaxResults(1)
						.setParameter("idGrupo", idGrupoFaturamento).setParameter("idRota", idRota).list().stream()
						.filter(Objects::nonNull).findFirst().orElse(null));
	}

	/**
	 * Cria uma lista de LeituraMovimentoDTO a partir da pesquisa de Leitura
	 * Movimento especificada Obs.: Adicionando o ignore para o squid:S1192, pois
	 * não se faz necessário criar constantes para strings de consultas hql.
	 *
	 * @param pesquisa a pesquisa a ser utilizada na criteria
	 * @return A lista com os objetos LeituraMovimentoDTO referente à pesquisa da
	 *         Leitura movimento
	 */
	@SuppressWarnings("squid:S1192")
	@Override
	public Pair<List<LeituraMovimentoDTO>, Long> criarListaLeituraMovimentoDTO(PesquisaLeituraMovimento pesquisa) {
		SQLQuery query = montarQueryLeituraMovimento(pesquisa);
		return preencherLeituras(query);
	}

	/**
	 * SuppressWarning: Posições esperadas do array de consultas
	 * 
	 * @param query query onde serão obtidos os resultados
	 * @return retorna um {@link Pair} das leituras movimentos e seu total (todas as
	 *         leituras da consulta e não o tamanho da lista)
	 */
	@SuppressWarnings("squid:S109")
	private Pair<List<LeituraMovimentoDTO>, Long> preencherLeituras(SQLQuery query) {
		List<Objects[]> results = query.list();
		List<LeituraMovimentoDTO> leituras = new ArrayList<>();
		Long totalRegistros = 0L;
		for (Object[] row : results) {
			LeituraMovimentoDTO leitura = new LeituraMovimentoDTO();
			Optional.ofNullable(row[0])
					.ifPresent(info -> leitura.setChaveLeituraMovimento(Long.parseLong(info.toString())));
			Optional.ofNullable(row[1]).ifPresent(info -> {
				String dataFormatada = new SimpleDateFormat(Constantes.FORMATO_DATA_BR).format(info);
				leitura.setDataLeitura(dataFormatada);
			});
			Optional.ofNullable(row[2]).ifPresent(info -> leitura.setLeituraAtual(Double.valueOf(info.toString())));
			Optional.ofNullable(row[3]).ifPresent(info -> leitura.setLeituraAnterior(Double.valueOf(info.toString())));
			Optional.ofNullable(row[4]).ifPresent(info -> leitura.setObservacaoLeitura(info.toString()));
			Optional.ofNullable(row[5]).ifPresent(info -> leitura.setOrigem(info.toString()));
			Optional.ofNullable(row[6]).ifPresent(info -> leitura.setChavePontoConsumo(Long.valueOf(info.toString())));
			Optional.ofNullable(row[7]).ifPresent(info -> leitura.setPontoConsumo(info.toString()));
			Optional.ofNullable(row[8]).ifPresent(info -> {
				SituacaoLeituraMovimentoImpl situacaoLeitura = new SituacaoLeituraMovimentoImpl();
				situacaoLeitura.setChavePrimaria(Long.parseLong(info.toString()));
				situacaoLeitura.setDescricao(row[9].toString());
				leitura.setSituacaoLeitura(situacaoLeitura);
			});
			Optional.ofNullable(row[10]).ifPresent(info -> leitura.setPressao(Double.valueOf(info.toString())));
			Optional.ofNullable(row[11]).ifPresent(info -> leitura.setTemperatura(Double.valueOf(info.toString())));

			if (row[12] != null && row[13] != null && row[15] != null) {
				Optional.ofNullable(row[12]).ifPresent(info -> {
					AnormalidadeDTO anormalidadeLeitura = new AnormalidadeDTO(Long.valueOf(row[12].toString()),
							row[13].toString(), ((BigDecimal) row[15]).compareTo(BigDecimal.ONE) == 0);
					leitura.setAnormalidadeLeitura(anormalidadeLeitura);
				});
			}

			if (row[16] != null && row[17] != null && row[18] != null) {
				Optional.ofNullable(row[16]).ifPresent(info -> {
					AnormalidadeDTO anormalidadeConsumo = new AnormalidadeDTO(Long.valueOf(row[16].toString()),
							row[17].toString(), ((BigDecimal) row[18]).compareTo(BigDecimal.ONE) == 0);
					leitura.setAnormalidadeConsumo(anormalidadeConsumo);
				});
			}

			Optional.ofNullable(row[19]).ifPresent(info -> leitura.setConsumoApurado(Double.valueOf(info.toString())));
			Optional.ofNullable(row[20]).ifPresent(info -> leitura.setTipoConsumo(info.toString()));
			Optional.ofNullable(row[21]).ifPresent(info -> leitura.setCorrigePT(Integer.valueOf(info.toString())));
			Optional.ofNullable(row[22]).ifPresent(info -> leitura.setObservacaoPontoConsumo(info.toString()));
			Optional.ofNullable(row[23]).ifPresent(info -> leitura.setMesAnoCiclo(info.toString()));
			Optional.ofNullable(row[14]).ifPresent(info -> leitura.setFotoColetor(info.toString()));
			Optional.ofNullable(row[25]).ifPresent(info -> leitura.setChaveRota(Long.valueOf(info.toString())));
			Optional.ofNullable(row[26]).ifPresent(info -> leitura.setSituacaoPontoConsumo(info.toString()));
			
			Optional.ofNullable(row[27]).ifPresent(info -> {
				String dataFormatada = new SimpleDateFormat(Constantes.FORMATO_DATA_BR).format(info);
				leitura.setDataLeituraAnterior(dataFormatada);
			});
			
			Optional.ofNullable(row[28]).ifPresent(info -> leitura.setChaveHistoricoMedicao(Long.valueOf(info.toString())));
			
			totalRegistros = Long.valueOf(row[24].toString());
			leituras.add(leitura);
		}
		return new Pair<>(leituras, totalRegistros);
	}

	/**
	 * Monta a query de pesquisa de leitura movimento
	 * 
	 * @param pesquisa parametro de pesquisa Suppresswarnings: necessario para a
	 *                 query
	 * @return retorna o {@link SQLQuery} montado
	 */
	@SuppressWarnings("squid:S1192")
	private SQLQuery montarQueryLeituraMovimento(PesquisaLeituraMovimento pesquisa) {
		StringBuilder queryNativa = new StringBuilder();
		queryNativa.append(" SELECT " + "    l.LEMO_CD as chavePrimaria," + "    COALESCE(mh.MEHI_TM_LEITURA_INFORMADA, l.LEMO_TM_LEITURA) as dataLeitura,"
				+ "    COALESCE(mh.MEHI_MD_LEITURA_INFORMADA, l.LEMO_MD_LEITURA) as leituraAtual," + "    COALESCE(mh.MEHI_MD_LEITURA_ANTERIOR, l.LEMO_MD_LEITURA_ANTERIOR) as leituraAnterior,"
				+ "    l.LEMO_DS_OBSERVACAO_LEITURA as observacaoLeitura," + "    l.LEMO_DS_ORIGEM as origem,"
				+ "    p.POCN_CD as pontoConsumoChave," + "    p.POCN_DS as pontoConsumoDescricao,"
				+ "    s.LEMS_CD as SitLeituraMovChavePrimaria," + "    s.LEMS_DS as SitLeituraMovChaveDescricao,"
				+ "    l.LEMO_MD_PRESSAO_INFORMADA as pressaoInformada,"
				+ "    l.LEMO_MD_TEMPERATURA_INFORMADA as temperaturaInformada,"
				+ "    l.LEAN_CD as codigoAnormalidadeLeitura," + "    la.LEAN_DS as anormalidadeLeituraDesc,"
				+ "    l.LEMO_DS_IMAGEM_COLETOR as imagemColetor,"
				+ "    la.LEAN_IN_BLOQUEIA_FATURAMENTO as anormalidadeLeituraGravidade," + "    CASE "
				+ "    WHEN h.COAN_CD IS NOT NULL THEN h.COAN_CD "
				+ "    ELSE l.COAN_CD END as anormalidadeConsumoChave,"
				+ "    ca.COAN_DS as anormalidadeConsumoDescricao,"
				+ "    ca.COAN_IN_BLOQUEIA_FATURAMENTO as anormalidadeConsumoGravidade,"
				+ "    h.COHI_MD_CONSUMO_APURADO as consumoApurado," + "    t.COTP_DS as tipoConsumo, "
				+ "    l.LEMO_IN_CORRIGE_PT as corrigePT, " + "    p.POCN_OBSERVACAO as pontoConsumoObservacao,"
				+ "    l.LEMO_DT_AM_FATURAMENTO as mesAnoCiclo, " + "	count(*) over () as Count, "
				+ "    p.ROTA_CD, ps.POCS_DS, COALESCE(mh.MEHI_TM_LEITURA_ANTERIOR, l.LEMO_DT_LEITURA_ANTERIOR) as dataLeituraAnterior, "
				+ "    COALESCE(mh.mehi_cd, null), "
				+ "    COALESCE(p.pocn_cd, null) ");
		queryNativa.append(" FROM LEITURA_MOVIMENTO l ");
		queryNativa.append(" LEFT JOIN PONTO_CONSUMO p ");
		queryNativa.append(" ON l.POCN_CD = p.POCN_CD");
		queryNativa.append(" LEFT JOIN PONTO_CONSUMO_SITUACAO ps ");
		queryNativa.append(" ON p.POCS_CD = ps.POCS_CD");		
		queryNativa.append(" LEFT JOIN LEITURA_MOVIMENTO_SITUACAO s ");
		queryNativa.append(" ON l.LEMS_CD = s.LEMS_CD");
		queryNativa.append(" LEFT JOIN CONSUMO_HISTORICO h");
		queryNativa
				.append(" ON h.COHI_NR_CICLO =l.LEMO_NR_CICLO AND h.COHI_DT_AM_FATURAMENTO = l.LEMO_DT_AM_FATURAMENTO "
						+ "AND h.POCN_CD = l.POCN_CD AND h.COHI_IN_USO = 1");
		queryNativa.append(" LEFT JOIN CONSUMO_TIPO t ");
		queryNativa.append(" ON t.COTP_CD = h.COTP_CD");
		queryNativa.append(" LEFT JOIN LEITURA_ANORMALIDADE la ");
		queryNativa.append("  ON la.LEAN_CD = l.LEAN_CD");
		queryNativa.append(" LEFT JOIN CONSUMO_ANORMALIDADE ca ");
		queryNativa.append(" ON ca.COAN_CD = h.COAN_CD or l.coan_cd = ca.coan_cd");
		queryNativa.append(" LEFT JOIN MEDICAO_HISTORICO mh");
		queryNativa
				.append(" ON mh.MEHI_NR_CICLO = l.LEMO_NR_CICLO AND mh.MEHI_DT_AM_LEITURA = l.LEMO_DT_AM_FATURAMENTO "
						+ "AND mh.POCN_CD = l.POCN_CD AND mh.MEHI_IN_USO = 1");
		ArrayList<String> whereStm = new ArrayList<>();
		HashMap<String, Object> params = new HashMap<>();
		Optional.ofNullable(pesquisa.getGrupoFaturamento()).ifPresent(r -> {
			whereStm.add(" l.FAGR_CD = :grupoFaturamento");
			params.put("grupoFaturamento", r);
		});
		Optional.ofNullable(pesquisa.getSegmento()).filter(StringUtils::isNotBlank).ifPresent(r -> {
			whereStm.add(" lower(l.LEMO_DS_SEGMENTO) like lower(:segmento)");
			params.put("segmento", "%" + r + "%");
		});
		Optional.ofNullable(pesquisa.getRotas()).ifPresent(r -> {
			whereStm.add(" l.ROTA_CD in (:rota)");
			params.put("rota", r);
		});
		Optional.ofNullable(pesquisa.getPontosConsumo()).ifPresent(pontos -> {
			List<Long> chavesPontos = new ArrayList<>();
			pontos.forEach(ponto -> chavesPontos.add(ponto.getChavePrimaria()));
			if (!chavesPontos.isEmpty()) {
				whereStm.add(" p.POCN_CD in :ponto");
				params.put("ponto", chavesPontos);
			}
		});
		Optional.ofNullable(pesquisa.getCiclo()).ifPresent(ciclo -> {
			whereStm.add(" l.LEMO_NR_CICLO = :ciclo");
			params.put("ciclo", ciclo);
		});
		
		Optional.ofNullable(pesquisa.getReferencia()).ifPresent(referencia -> {
			whereStm.add(" l.LEMO_DT_AM_FATURAMENTO = :referencia");
			params.put("referencia", referencia.replace("/", ""));
		});
		
		Optional.ofNullable(pesquisa.getSituacaoLeituras()).ifPresent(r -> {
			whereStm.add(" s.LEMS_CD in (:situacaoLeituras)");
			params.put("situacaoLeituras", r);
		});
		if (pesquisa.getDataInicial() != null && pesquisa.getDataFinal() != null) {
			whereStm.add(" l.LEMO_TM_LEITURA between :dataInicial and :dataFinal");
			params.put("dataInicial", DataUtil.toDate(pesquisa.getDataInicial()));
			params.put("dataFinal", DataUtil.toDate(pesquisa.getDataFinal()));
		}

		if (CollectionUtils.isNotEmpty(pesquisa.getChavesAnormalidades())) {
			whereStm.add(" l.LEAN_CD in (:anormalidadeLeitura)");
			params.put("anormalidadeLeitura", pesquisa.getChavesAnormalidades());
		}
		if (CollectionUtils.isNotEmpty(pesquisa.getChavesAnormalidadesConsumo())) {
			whereStm.add(" h.COAN_CD in (:anormalidadeConsumo)");
			params.put("anormalidadeConsumo", pesquisa.getChavesAnormalidadesConsumo());
		}
		Optional.ofNullable(pesquisa.getAnormalidadeLeituraBloqueiaFaturamento()).ifPresent(r -> {
			if (r) {
				whereStm.add("la.LEAN_IN_BLOQUEIA_FATURAMENTO = 1 and l.LEAN_CD IS NOT NULL");
			} else {
				whereStm.add(
						"(la.LEAN_IN_BLOQUEIA_FATURAMENTO = 0 or la.LEAN_IN_BLOQUEIA_FATURAMENTO IS NULL) and l.LEAN_CD IS NOT NULL ");
			}
		});
		
		Optional.ofNullable(pesquisa.getPossuiAnormalidadeLeitura()).ifPresent(r -> {
			if (r) {
				whereStm.add("l.LEAN_CD IS NOT NULL AND l.LEAN_CD > 0");
			} else {
				whereStm.add("l.LEAN_CD IS NULL OR l.LEAN_CD <= 0 ");
			}
		});

		Optional.ofNullable(pesquisa.getAnormalidadeConsumoBloqueiaFaturamento()).ifPresent(r -> {
			if (r) {
				whereStm.add(" ca.COAN_IN_BLOQUEIA_FATURAMENTO = 1 and h.COAN_CD IS NOT NULL");
			} else {
				whereStm.add(
						" (ca.COAN_IN_BLOQUEIA_FATURAMENTO = 0 OR ca.COAN_IN_BLOQUEIA_FATURAMENTO IS NULL) and h.COAN_CD IS NOT NULL");
			}
		});
		
		Optional.ofNullable(pesquisa.getPossuiAnormalidadeConsumo()).ifPresent(r -> {
			if (r) {
				whereStm.add("h.COAN_CD IS NOT NULL AND h.COAN_CD > 0");
			} else {
				whereStm.add("h.COAN_CD IS NULL OR h.COAN_CD <= 0");
			}
		});

		Optional.ofNullable(whereStm.toString()).filter(StringUtils::isNotBlank)
				.ifPresent(r -> queryNativa.append(" where ").append(String.join(" and ", whereStm)));
		queryNativa.append(" order by l.LEMO_TM_LEITURA, p.POCN_DS");
		SQLQuery query = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createSQLQuery(queryNativa.toString());
		query.setMaxResults(pesquisa.getQtdRegistros());
		query.setFirstResult(pesquisa.getOffset());
		for (Map.Entry<String, Object> entrada : params.entrySet()) {
			String chave = entrada.getKey();
			Object info = entrada.getValue();
			if (info instanceof Collection) {
				query.setParameterList(chave, (List) info);
			} else {
				query.setParameter(chave, info);
			}
		}
		return query;
	}

	/**
	 * Adiciona as propriedades lazy à instância da criteria.
	 *
	 * Obs.: Adicionando o ignore para o squid:S1192, pois não se faz necessário
	 * criar constantes para strings de consultas hql.
	 *
	 * @param criteria a isntância a ser modificada
	 */
	@SuppressWarnings("squid:S1192")
	private void configurarProjectionsLazy(Criteria criteria) {
		final ProjectionList projections = Projections.projectionList();

		projections.add(Projections.property("chavePrimaria"), "chavePrimaria")
				.add(Projections.property("dataLeitura"), "dataLeitura")
				.add(Projections.property("leituraAnterior"), "leituraAnterior")
				.add(Projections.property("observacaoLeitura"), "observacaoLeitura")
				.add(Projections.property("origem"), "origem")
				.add(Projections.property("anoMesFaturamento"), "anoMesFaturamento")
				.add(Projections.property("segmento"), "segmento").add(Projections.property("ciclo"), "ciclo")
				.add(Projections.property("valorLeitura"), "valorLeitura")
				.add(Projections.property("p.chavePrimaria").as("pontoConsumo.chavePrimaria"))
				.add(Projections.property("p.descricao").as("pontoConsumo.descricao"))
				.add(Projections.property("situacaoLeitura.chavePrimaria").as("situacaoLeitura.chavePrimaria"))
				.add(Projections.property("situacaoLeitura.descricao").as("situacaoLeitura.descricao"))
				.add(Projections.property("pressaoInformada"), "pressaoInformada")
				.add(Projections.property("temperaturaInformada"), "temperaturaInformada")
				.add(Projections.property("codigoAnormalidadeLeitura"), "codigoAnormalidadeLeitura")
				.add(Projections.property("anormalidadeConsumo"), "anormalidadeConsumo");

		criteria.setProjection(projections)
				.setResultTransformer(new GGASAliasToBeanNestedResultTransformer(LeituraMovimentoImpl.class,
						ImmutableMap.<Class, Class>builder().put(PontoConsumo.class, PontoConsumoImpl.class)
								.put(SituacaoLeituraMovimento.class, SituacaoLeituraMovimentoImpl.class).build()))
				.createAlias("pontoConsumo", "p", CriteriaSpecification.LEFT_JOIN)
				.createAlias("situacaoLeitura", "situacaoLeitura", CriteriaSpecification.LEFT_JOIN)
				.createAlias("anormalidadeConsumo", "anormalidadeConsumo", CriteriaSpecification.LEFT_JOIN);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.ControladorLeituraMovimento#
	 * verificarFaturarGrupoCorretorVazao(br.com.ggas.medicao.rota.Rota,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public boolean verificarFaturarGrupoCorretorVazao(Rota rota, Integer anoMesReferencia, Integer numeroCiclo)
			throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		String valorSituacaoContrato = controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_CONTRATO_ATIVO);
		Long valorSituacaoContratoLong = null;
		try {
			valorSituacaoContratoLong = Util.converterCampoStringParaValorLong(SITUACAO_DO_CONTRATO,
					valorSituacaoContrato);
		} catch (FormatoInvalidoException e) {
			LOG.error(e.getMessage(), e);
			throw new NegocioException();
		}

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select count(*) from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append(" pontoConsumo ");
		hql.append(" where ");
		hql.append(" pontoConsumo.habilitado = true ");
		hql.append(" and pontoConsumo.rota.chavePrimaria = :idRota ");
		hql.append(" and pontoConsumo.situacaoConsumo.indicadorLeitura = true");
		hql.append(" and pontoConsumo.instalacaoMedidor is not null ");
		hql.append(" and ( ");

		// quantidade de medicao menor que o minimo
		hql.append(montarConsultaContadorConsumoHistoricoPontoConsumo(Boolean.TRUE));
		hql.append(" < ");
		hql.append(montarConsultaMinimoMaximoDiasCiclo(Boolean.TRUE, rota, anoMesReferencia));
		hql.append(" or ");

		// quantidade de medicao maior que o maximo
		hql.append(montarConsultaContadorConsumoHistoricoPontoConsumo(Boolean.TRUE));
		hql.append(" > ");
		hql.append(montarConsultaMinimoMaximoDiasCiclo(Boolean.FALSE, rota, anoMesReferencia));
		hql.append(" ) ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idRota", rota.getChavePrimaria());
		query.setParameter("anoMesReferencia", anoMesReferencia);
		query.setParameter(CICLO, numeroCiclo);
		if (rota.getPeriodicidade() != null && (rota.getPeriodicidade().getConsideraMesCivil() == null
				|| !rota.getPeriodicidade().getConsideraMesCivil())) {
			query.setParameter(SITUACAO_CONTRATO_ATIVO, valorSituacaoContratoLong);
		}

		Long quantidadePontosDeConsumo = (Long) query.uniqueResult();
		boolean atualiza = Boolean.FALSE;
		if (quantidadePontosDeConsumo == null || quantidadePontosDeConsumo == 0) {
			atualiza = Boolean.TRUE;
		}

		return atualiza;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * br.com.ggas.medicao.leitura.ControladorLeituraMovimento#atualizarColecao(java
	 * .lang.Long, java.lang.Integer, java.lang.Long, java.lang.String[],
	 * java.lang.Integer[], java.math.BigDecimal[], java.math.BigDecimal[],
	 * java.lang.Long[], java.lang.String[], java.lang.Long[])
	 */
	@Override
	public void atualizarColecao(Long chaveRota, Integer anoMesFaturamento, Long chaveLeiturista, String[] dataLeitura,
			Float[] valorLeitura, BigDecimal[] valorPressao, BigDecimal[] valorTemperatura,
			Long[] anormalidadeLeitura, String[] observacoesCadastrais, Long[] chavesPrimarias) throws GGASException {

		ControladorUnidade controladorUnidade = (ControladorUnidade) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorUnidade.BEAN_ID_CONTROLADOR_UNIDADE);
		ControladorAnormalidade controladorAnormalidade = (ControladorAnormalidade) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorAnormalidade.BEAN_ID_CONTROLADOR_ANORMALIDADE);
		ControladorLeiturista controladorLeiturista = (ControladorLeiturista) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorLeiturista.BEAN_ID_CONTROLADOR_LEITURISTA);

		Collection<LeituraMovimento> listaLeituraMovimento = this.consultarLeituraMovimentoPorRota(chaveRota,
				anoMesFaturamento, chavesPrimarias, null, null);

		SituacaoLeituraMovimento situacaoLeituraMovimento = this
				.obterSituacaoLeituraMovimento(SituacaoLeituraMovimento.LEITURA_RETORNADA);

		int cont = 0;
		for (LeituraMovimento leituraMovimento : listaLeituraMovimento) {
			leituraMovimento.setOrigem(OrigemLeituraMovimento.DADOS_MEDICAO.name());
			SituacaoLeituraMovimento situacaoLeitura = leituraMovimento.getSituacaoLeitura();

			if (situacaoLeitura != null && situacaoLeitura.getChavePrimaria() != SituacaoLeituraMovimento.PROCESSADO) {

				leituraMovimento.setSituacaoLeitura(situacaoLeituraMovimento);

				String data = null;
				if (dataLeitura.length > 0 && cont < dataLeitura.length) {
					data = dataLeitura[cont];
					if (data != null && Util.isDataValida(data, Constantes.FORMATO_DATA_BR)) {
						data = data.replace("/", "").replace("_", "");
						if (data.length() == 8) {
							Date date = Util.converterCampoStringParaData("Data da Leitura", dataLeitura[cont],
									Constantes.FORMATO_DATA_BR);
							leituraMovimento.setDataLeitura(date);
						} else {
							leituraMovimento.setDataLeitura(null);
						}
					} else {
						leituraMovimento.setDataLeitura(null);
					}
				}

				Float valor = null;
				if (valorLeitura.length > 0 && cont < valorLeitura.length) {
					valor = valorLeitura[cont];
				}

				Long chaveUnidadePressao = Fachada.getInstancia()
						.obterParametroPorCodigo(Constantes.PARAMETRO_UNIDADE_PRESSAO_MEDICAO).getValorLong();

				Long chaveUnidadeTemperatura = Fachada.getInstancia()
						.obterParametroPorCodigo(Constantes.PARAMETRO_UNIDADE_TEMPERATURA_MEDICAO).getValorLong();

				Long chaveAnormalidade = null;
				if (anormalidadeLeitura.length > 0 && cont < anormalidadeLeitura.length) {
					chaveAnormalidade = anormalidadeLeitura[cont];
				}

				String obs = null;
				if (observacoesCadastrais.length > 0 && cont < observacoesCadastrais.length) {
					obs = observacoesCadastrais[cont];
				}

				leituraMovimento.setDataNovaLeitura(data);
				leituraMovimento.setObservacoesCadastrais(obs);

				if (valor != null) {
					BigDecimal bigDecimal = new BigDecimal(valor);
					leituraMovimento.setValorLeitura(bigDecimal);
				} else {
					leituraMovimento.setValorLeitura(null);
				}

				if ((valorPressao.length > 0 && cont < valorPressao.length) && valorPressao[cont] != null) {

					leituraMovimento.setPressaoInformada(valorPressao[cont]);
				}

				if (chaveUnidadePressao != null && chaveUnidadePressao > 0) {
					Unidade unidadePressao = (Unidade) controladorUnidade.obter(chaveUnidadePressao);
					leituraMovimento.setUnidadePressaoInformada(unidadePressao);
				} else {
					leituraMovimento.setUnidadePressaoInformada(null);
				}

				if ((valorTemperatura.length > 0 && cont < valorTemperatura.length) && valorTemperatura[cont] != null) {

					leituraMovimento.setTemperaturaInformada(valorTemperatura[cont]);
				}

				if (chaveUnidadeTemperatura != null && chaveUnidadeTemperatura > 0) {
					Unidade unidadeTemperatura = (Unidade) controladorUnidade.obter(chaveUnidadeTemperatura);
					leituraMovimento.setUnidadeTemperaturaInformada(unidadeTemperatura);
				} else {
					leituraMovimento.setUnidadeTemperaturaInformada(null);
				}

				if (chaveAnormalidade != null && chaveAnormalidade > 0) {
					AnormalidadeLeitura anormalidadeLeituraDados = controladorAnormalidade
							.obterAnormalidadeLeitura(chaveAnormalidade);
					leituraMovimento.setCodigoAnormalidadeLeitura(anormalidadeLeituraDados.getChavePrimaria());
				} else {
					leituraMovimento.setAnormalidadeLeituraAnterior(null);
				}
				if (chaveLeiturista != null && chaveLeiturista > 0) {
					Leiturista leiturista = (Leiturista) controladorLeiturista.obter(chaveLeiturista);
					leituraMovimento.setLeiturista(leiturista);
				} else {
					leituraMovimento.setLeiturista(null);
				}
				cont++;
			}

		}

		validarDatasLeitura(listaLeituraMovimento);
		validarLeituraMovimento(listaLeituraMovimento);
		atualizarColecao(listaLeituraMovimento.toArray(), LeituraMovimentoImpl.class);

	}

	private void validarLeituraMovimento(Collection<LeituraMovimento> listaLeituraMovimento) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema = (ControladorConstanteSistema) ServiceLocator
				.getInstancia()
				.getControladorNegocio(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia()
				.getControladorNegocio(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorUnidade controladorUnidade = (ControladorUnidade) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorUnidade.BEAN_ID_CONTROLADOR_UNIDADE);
		ControladorHistoricoConsumo controladorHistoricoConsumo = (ControladorHistoricoConsumo) ServiceLocator
				.getInstancia().getBeanPorID(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);

		Long constanteCorrigiPTSimColetor = Long.parseLong(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ENTIDADE_CONTEUDO_CORRIGI_PT_SIM_COLETOR));

		String descricaoPontosConsumo = "";
		StringBuilder builder = new StringBuilder();

		for (LeituraMovimento leituraMovimento : listaLeituraMovimento) {

			ContratoPontoConsumo contratoPontoConsumo = controladorContrato
					.consultarContratoPontoConsumoPorPontoConsumoRecente(
							leituraMovimento.getPontoConsumo().getChavePrimaria());

			if (contratoPontoConsumo != null) {
				Unidade unidadePadraoPressao = controladorUnidade.obterUnidadePadraoPressao();
				BigDecimal pressaoContratada = null;
				Unidade unidadeContratada = unidadePadraoPressao;
				if (unidadePadraoPressao.equals(contratoPontoConsumo.getUnidadePressao())) {

					pressaoContratada = contratoPontoConsumo.getMedidaPressao();

				} else {
					pressaoContratada = controladorHistoricoConsumo.converterUnidadeMedida(
							contratoPontoConsumo.getMedidaPressao(), contratoPontoConsumo.getUnidadePressao(),
							unidadePadraoPressao);

				}

				FaixaPressaoFornecimento faixaPressaoFornecimento = controladorHistoricoConsumo
						.obterFaixaPressaoFornecimento(contratoPontoConsumo, pressaoContratada, unidadeContratada,
								contratoPontoConsumo.getPontoConsumo().getSegmento());

				if (faixaPressaoFornecimento.getIndicadorCorrecaoPT().getChavePrimaria() == constanteCorrigiPTSimColetor
						&& (leituraMovimento.getDataLeitura() != null && leituraMovimento.getPressaoInformada() == null
								|| leituraMovimento.getUnidadePressaoInformada() == null
								|| leituraMovimento.getTemperaturaInformada() == null
								|| leituraMovimento.getUnidadeTemperaturaInformada() == null)) {
					builder.append(leituraMovimento.getPontoConsumo().getDescricao());
					builder.append(", ");
				}
			}
		}
		descricaoPontosConsumo = builder.toString();
		if (!"".equals(descricaoPontosConsumo)) {

			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS_PRESSAO_TEMPERATURA,
					descricaoPontosConsumo.substring(0, descricaoPontosConsumo.length() - QTD_DIGITOS_PARA_REMOVER));
		}

	}

	private void validarDatasLeitura(Collection<LeituraMovimento> listaLeituraMovimento) throws GGASException {
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();

		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));

		for (LeituraMovimento leituraMovimento : listaLeituraMovimento) {

			if (leituraMovimento.getDataLeitura() == null && (leituraMovimento.getCodigoAnormalidadeLeitura() == null
					|| leituraMovimento.getCodigoAnormalidadeLeitura().compareTo(codigoAnormalidadeLeitura) != 0)) {
				leituraMovimento.setDataLeitura(leituraMovimento.getDataLeitura());
				throw new NegocioException(Constantes.ERRO_NEGOCIO_DATA_LEITURA_OBRIGAROTIA, true);

			}

			Date dataAtual = Calendar.getInstance().getTime();
			if (leituraMovimento.getDataLeitura() != null
					&& leituraMovimento.getDataLeitura().compareTo(dataAtual) > 0) {
				leituraMovimento.setDataLeitura(leituraMovimento.getDataLeitura());
				throw new NegocioException(ControladorHistoricoMedicao.ERRO_NEGOCIO_DATA_REALIZACAO_LEITURA_INVALIDA,
						Util.converterDataParaStringSemHora(leituraMovimento.getDataLeituraAnterior(),
								Constantes.FORMATO_DATA_HORA_BR));
			}

			if (leituraMovimento.getDataLeitura() != null && leituraMovimento.getDataLeituraAnterior() != null
					&& leituraMovimento.getDataLeitura().compareTo(leituraMovimento.getDataLeituraAnterior()) < 0) {
				leituraMovimento.setDataLeitura(leituraMovimento.getDataLeitura());
				throw new NegocioException(ControladorHistoricoMedicao.ERRO_NEGOCIO_DATA_REALIZACAO_LEITURA_INVALIDA,
						Util.converterDataParaStringSemHora(leituraMovimento.getDataLeituraAnterior(),
								Constantes.FORMATO_DATA_HORA_BR));
			}
		}
	}

	/**
	 * Consultar leitura movimento por rota.
	 *
	 * @param chaveRota         the chave rota
	 * @param anoMesFaturamento the ano mes faturamento
	 * @param chavesPrimarias   the chaves primarias
	 * @param pontoConsumo      the ponto consumo
	 * @param ciclo             the ciclo
	 * @return the collection
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<LeituraMovimento> consultarLeituraMovimentoPorRota(Long chaveRota, Integer anoMesFaturamento,
			Long[] chavesPrimarias, Long pontoConsumo, Integer ciclo) {

		Criteria criteria = createCriteria(getClasseEntidade());

		criteria.setFetchMode("rota", FetchMode.JOIN);
		criteria.setFetchMode("anormalidadeLeitura", FetchMode.JOIN);
		criteria.setFetchMode("grupoFaturamento", FetchMode.JOIN);
		criteria.setFetchMode("pontoConsumo", FetchMode.JOIN);
		criteria.setFetchMode("situacaoLeitura", FetchMode.JOIN);

		if (chavesPrimarias != null && chavesPrimarias.length > 0) {
			criteria.add(Restrictions.in("chavePrimaria", chavesPrimarias));
		}

		if (chaveRota != null) {
			criteria.add(Restrictions.eq("rota.chavePrimaria", chaveRota));
		}

		if (anoMesFaturamento != null) {
			criteria.add(Restrictions.eq("anoMesFaturamento", anoMesFaturamento));
		}

		if (ciclo != null) {
			criteria.add(Restrictions.eq("ciclo", ciclo));
		}

		if (pontoConsumo != null) {
			criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", pontoConsumo));
		}

		return criteria.list();
	}

	private boolean temPontoConsumoEletrocorretor(Rota rota, StringBuilder logProcesso) {
		boolean retorno = false;
		for (PontoConsumo pontoConsumo : rota.getPontosConsumo()) {
			if (pontoConsumo.getInstalacaoMedidor() != null
					&& pontoConsumo.getInstalacaoMedidor().getVazaoCorretor() != null) {
				if (!retorno) {
					logProcesso.append(
							"\n\r Os Pontos de consumo abaixo são Eletrocorretores e não podem estar numa rota de coletores");
				}
				logProcesso.append("\n\r	");
				logProcesso.append(pontoConsumo.getDescricao());
				retorno = true;
			}
		}
		return retorno;
	}

	private static void escreverAposDuasLinha(StringBuilder logProcessamento, String texto) {
		logProcessamento.append("\n");
		logProcessamento.append("\n");
		logProcessamento.append(texto);
	}

	private static void escreverNovaLinha(StringBuilder logProcessamento, String texto) {
		logProcessamento.append("\n");
		logProcessamento.append(texto);
	}

	private static void escreverLinha(StringBuilder logProcessamento, String texto) {
		logProcessamento.append(texto);
	}

	@Override
	public LeituraMovimento consultarLeituraMovimentoPorCodigoLegado(Long codigoLegado)
			throws PontoConsumoNaoEncontrado, LeituraMovimentoNaoEncontrada, MaisDeUmaLeituraMovimentoRetornada,
			CodigoLegadoEmVariosPontosConsumo {

		LeituraMovimento leituraMovimento = null;
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("SELECT count(*) FROM PontoConsumoImpl pc WHERE pc.codigoLegado = :codigoLegado");
			Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameter("codigoLegado", codigoLegado.toString());
			Long pontoConsumoEncontrado = (Long) query.uniqueResult();
			if (pontoConsumoEncontrado == 0) {
				throw new PontoConsumoNaoEncontrado(
						"Ponto de consumo não encontrado para o código legado " + codigoLegado);
			} else if (pontoConsumoEncontrado > 1) {
				throw new CodigoLegadoEmVariosPontosConsumo(
						"Mais de um ponto de consumo com o código legado " + codigoLegado);
			}

			hql = new StringBuilder();
			hql.append("SELECT new LeituraMovimentoImpl(lm.chavePrimaria, lm.pontoConsumo.chavePrimaria, ");
			hql.append(
					"lm.nomeCliente, lm.descicaoPontoConsumo, lm.endereco, lm.anoMesFaturamento, lm.ciclo, situacaoLeitura.chavePrimaria, ");
			hql.append(" lm.valorLeitura, lm.pressaoInformada, lm.temperaturaInformada, lm.dataLeitura)");
			hql.append(" from ");
			hql.append(getClasseEntidade().getSimpleName());
			hql.append(" lm ");
			hql.append(" inner join lm.situacaoLeitura situacaoLeitura ");
			hql.append(" where ");
			hql.append(" situacaoLeitura.chavePrimaria in (:GERADO, :EM_LEITURA, :LEITURA_RETORNADA) ");
			hql.append(" and lm.pontoConsumo.codigoLegado = :codigoLegado ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setParameter("codigoLegado", codigoLegado.toString());
			query.setParameter("GERADO", SituacaoLeituraMovimento.GERADO);
			query.setParameter("EM_LEITURA", SituacaoLeituraMovimento.EM_LEITURA);
			query.setParameter("LEITURA_RETORNADA", SituacaoLeituraMovimento.LEITURA_RETORNADA);
			leituraMovimento = (LeituraMovimento) query.uniqueResult();
			if (leituraMovimento == null) {
				throw new LeituraMovimentoNaoEncontrada(
						"Leitura movimento não encontrada para o código legado " + codigoLegado + " e status "
								+ SituacaoLeituraMovimento.GERADO + "(gerado), " + SituacaoLeituraMovimento.EM_LEITURA
								+ "(em leitura) e " + SituacaoLeituraMovimento.LEITURA_RETORNADA + ".");
			}
			return leituraMovimento;
		} catch (PontoConsumoNaoEncontrado | CodigoLegadoEmVariosPontosConsumo
				| LeituraMovimentoNaoEncontrada pontoConsumoNaoEncontrado) {
			throw pontoConsumoNaoEncontrado;
		} catch (NonUniqueResultException maisDeUmaLeituraMovimentoRetornada) {

			throw new MaisDeUmaLeituraMovimentoRetornada("Mais de uma leitura movimento retornada para o código legado "
					+ codigoLegado + " e status 1 e 2.");
		} catch (Exception e) {
			return leituraMovimento;
		}
	}

	@Override
	public Collection<RotaDTO> consultarRotasAtivas() {

		StringBuilder hql = new StringBuilder("SELECT DISTINCT new br.com.ggas.api.dto.RotaDTO(");
		hql.append("lm.rota.chavePrimaria, lm.rota.numeroRota, lm.rota.leiturista.chavePrimaria, lm.anoMesFaturamento");
		hql.append(", caf.dataInicio, caf.dataFim)");

		hql.append(" FROM ").append(LeituraMovimentoImpl.class.getSimpleName()).append(" lm");
		hql.append(", ").append(CronogramaAtividadeFaturamentoImpl.class.getSimpleName()).append(" caf");

		hql.append(" WHERE lm.situacaoLeitura.chavePrimaria IN (:GERADO, :EM_LEITURA, :LEITURA_RETORNADA)");
		hql.append(" AND lm.rota.habilitado = true");
		hql.append(
				" AND caf.cronogramaFaturamento.grupoFaturamento.chavePrimaria = lm.rota.grupoFaturamento.chavePrimaria");
		hql.append(
				" AND caf.cronogramaFaturamento.grupoFaturamento.chavePrimaria = lm.rota.grupoFaturamento.chavePrimaria");
		hql.append(" AND caf.cronogramaFaturamento.anoMesFaturamento = lm.anoMesFaturamento");
		hql.append(" AND caf.atividadeSistema.chavePrimaria = :ATIVIDADE");

		Query query = createHQLQuery(hql.toString());

		query.setParameter("GERADO", SituacaoLeituraMovimento.GERADO);
		query.setParameter("EM_LEITURA", SituacaoLeituraMovimento.EM_LEITURA);
		query.setParameter("LEITURA_RETORNADA", SituacaoLeituraMovimento.LEITURA_RETORNADA);
		query.setParameter("ATIVIDADE", AtividadeSistema.CODIGO_ATIVIDADE_GERAR_DADOS_LEITURA);

		return query.list();
	}

	@Override
	public Collection<br.com.ggas.api.dto.LeituraMovimentoDTO> consultarLeiturasAtivasPorRota(Long idRota,
			Integer anoMesFaturamento) {

		StringBuilder hql = new StringBuilder("SELECT new br.com.ggas.api.dto.LeituraMovimentoDTO(");
		hql.append("lm.id, lm.dataGeracao, lm.anoMesFaturamento, lm.ciclo");
		hql.append(", lm.pontoConsumo.chavePrimaria, lm.pontoConsumo.descricao, lm.pontoConsumo.observacao");
		hql.append(", lm.rota.chavePrimaria, lm.rota.numeroRota, lm.sequencialLeitura");
		hql.append(", lm.endereco, lm.complementoEmdereco, lm.bairro, lm.municipio, lm.uf, lm.cep");
		hql.append(", lm.numeroSerieMedidor, lm.localInstalacaoMedidor");
		hql.append(", lm.dataLeituraPrevista, lm.situacaoLeitura.chavePrimaria");
		hql.append(", lm.corrigePT, cp.faixaPressaoFornecimento.indicadorCorrecaoPT.descricao");
		hql.append(", lm.origem, lm.dataLeitura");
		hql.append(", lm.valorLeitura, lm.pressaoInformada, lm.temperaturaInformada, lm.codigoAnormalidadeLeitura");
		hql.append(", lm.rota.leiturista.chavePrimaria, lm.observacaoLeitura");
		hql.append(", lm.minimoLeituraEsperada, lm.maximoLeituraEsperada, lm.fotoColetor ");
		hql.append(")");

		hql.append(" FROM ").append(LeituraMovimentoImpl.class.getSimpleName()).append(" lm");
		hql.append(", ").append(ContratoPontoConsumoImpl.class.getSimpleName()).append(" cp");

		hql.append(" WHERE lm.situacaoLeitura.chavePrimaria IN (:GERADO, :EM_LEITURA, :LEITURA_RETORNADA)");
		hql.append(" AND cp.pontoConsumo.chavePrimaria = lm.pontoConsumo.chavePrimaria");
		hql.append(" AND lm.rota.chavePrimaria = :idRota AND lm.anoMesFaturamento = :anoMesFaturamento");

		Query query = createHQLQuery(hql.toString());

		query.setParameter("GERADO", SituacaoLeituraMovimento.GERADO);
		query.setParameter("EM_LEITURA", SituacaoLeituraMovimento.EM_LEITURA);
		query.setParameter("LEITURA_RETORNADA", SituacaoLeituraMovimento.LEITURA_RETORNADA);

		query.setParameter("idRota", idRota);
		query.setParameter("anoMesFaturamento", anoMesFaturamento);

		return query.list();
	}

	@SuppressWarnings("squid:S1192")
	public void atualizarLeiturasAtivasPorRota(Long idRota, Integer anoMesFaturamento) {
		StringBuilder hql = new StringBuilder();
		hql.append(" update ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" set situacaoLeitura = ? ");
		hql.append(" where situacaoLeitura = ? ");
		hql.append(" and rota = ? ");
		hql.append(" and anoMesFaturamento = ? ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, SituacaoLeituraMovimento.EM_LEITURA);
		query.setLong(1, SituacaoLeituraMovimento.GERADO);
		query.setLong(2, idRota);
		query.setLong(3, anoMesFaturamento);
		query.executeUpdate();
	}

	/**
	 * Registrar Leitura
	 * 
	 * @param id          - {@link Long}
	 * @param leitura     - {@link BigDecimal}
	 * @param pressao     - {@link BigDecimal}
	 * @param temperatura - {@link BigDecimal}
	 * @param coorigePT   - {@link Boolean}
	 */
	@SuppressWarnings("squid:S1192")
	public void registrarLeitura(Long id, BigDecimal leitura, BigDecimal pressao, BigDecimal temperatura,
			Boolean coorigePT) {
		StringBuilder hql = new StringBuilder();
		hql.append(" update ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" set situacaoLeitura = :situacao ");
		if (coorigePT != null && coorigePT) {
			hql.append(" , valorLeitura = :valorLeitura ");
			hql.append(" , pressaoInformada = :pressaoInformada ");
			hql.append(" , temperaturaInformada = :temperaturaInformada ");
		}
		hql.append(" where chavePrimaria = :id ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter(":situacao", SituacaoLeituraMovimento.LEITURA_RETORNADA);
		query.setParameter(":id", id);
		if (coorigePT != null && coorigePT) {
			query.setParameter(":valorLeitura", leitura);
			query.setParameter(":pressaoInformada", pressao);
			query.setParameter(":temperaturaInformada", temperatura);
		}
		query.executeUpdate();
	}

	@Override
	public void inserirHistoricoLeituraMovimento(LeituraMovimento leituraMovimento, Usuario usuarioLogado) {
		try {
			HistoricoLeituraMovimento historicoLeituraMovimento = new HistoricoLeituraMovimento();
			historicoLeituraMovimento.setAnormalidadeConsumo(leituraMovimento.getAnormalidadeConsumo());
			historicoLeituraMovimento.setCodigoAnormalidadeLeitura(leituraMovimento.getCodigoAnormalidadeLeitura());
			historicoLeituraMovimento.setDataGeracao(new Date());
			historicoLeituraMovimento.setDataLeitura(leituraMovimento.getDataLeitura());
			historicoLeituraMovimento.setOrigem(leituraMovimento.getOrigem());

			try {
				if (usuarioLogado != null) {
					historicoLeituraMovimento.setUsuario(usuarioLogado);
				} else {
					historicoLeituraMovimento.setUsuario(leituraMovimento.getUsuario());
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				historicoLeituraMovimento.setUsuario(leituraMovimento.getUsuario());
			}

			historicoLeituraMovimento.setPressaoInformada(leituraMovimento.getPressaoInformada());
			historicoLeituraMovimento.setTemperaturaInformada(leituraMovimento.getTemperaturaInformada());
			historicoLeituraMovimento.setSituacaoLeitura(leituraMovimento.getSituacaoLeitura());
			historicoLeituraMovimento.setValorLeitura(leituraMovimento.getValorLeitura());
			historicoLeituraMovimento.setHabilitado(true);
			historicoLeituraMovimento.setUltimaAlteracao(new Date());
			historicoLeituraMovimento.setCodigoLeituraMovimento(leituraMovimento.getChavePrimaria());
			super.inserir(historicoLeituraMovimento);
		} catch (Exception e) {
			logger.error("Erro ao inserir histórico de leitura movimento", e);
		}
	}

	@Override
	public void atualizar(EntidadeNegocio entidadeNegocio) throws NegocioException {
		try {
			super.atualizar(entidadeNegocio);
		} catch (ConcorrenciaException e) {
			throw new NegocioException("Falha ao atualizar a leitura", e);
		}
		

		this.inserirHistoricoLeituraMovimento((LeituraMovimento) entidadeNegocio, null);
	}

	@Override
	public void atualizarColecao(Object[] entidade, Class<?> classe) throws NegocioException {
		if (entidade != null && entidade.length > 0) {
			for (int i = 0; i < entidade.length; i++) {
				this.atualizar((EntidadeNegocio) entidade[i]);
			}
		}
	}

	/**
	 * Consulta a quantidade de pontos de consumos que estão "presos" em uma
	 * determinada etapa do faturamaento
	 * 
	 * @param chaveSituacaoLeitura chave da situação da leitura
	 * @param grupoFaturamento     chave do grupo de faturamento
	 * @param anoMesFaturamento    ano mês faturamento
	 * @param ciclo                número do ciclo
	 * @return retorna a quantidade de pontos de consumo que estão em uma
	 *         determinada situação
	 */
	@Override
	public Long consultarQuantidadePontosConsumoNaAtividade(Long chaveSituacaoLeitura, Long grupoFaturamento,
			Integer anoMesFaturamento, Integer ciclo) {
		return (Long) createHQLQuery(
				"select count(l) from LeituraMovimentoImpl l where l.situacaoLeitura.chavePrimaria = :chaveSituacaoLeitura "
						+ "and l.ciclo = :ciclo and l.grupoFaturamento.chavePrimaria = :grupoFaturamento and l.anoMesFaturamento = :anoMesFaturamento "
						+ "and l.habilitado = true").setParameter("chaveSituacaoLeitura", chaveSituacaoLeitura)
								.setParameter("ciclo", ciclo).setParameter("anoMesFaturamento", anoMesFaturamento)
								.setParameter("grupoFaturamento", grupoFaturamento).list().stream().findFirst()
								.orElse(0L);
	}

	/**
	 * Consulta a quantidade total de pontos de consumos que estão em uma
	 * determinada etapa do faturamaento
	 * 
	 * @param chaveSituacaoLeitura chave da situação da leitura
	 * @param grupoFaturamento     chave do grupo de faturamento
	 * @param anoMesFaturamento    ano mês faturamento
	 * @param ciclo                número do ciclo
	 * @return retorna a quantidade total de pontos de consumo que estão em uma
	 *         determinada situação
	 */
	@Override
	public Long consultarQuantidadePontosConsumoTotaisNaAtividade(Long chaveSituacaoLeitura, Long grupoFaturamento,
			Integer anoMesFaturamento, Integer ciclo) {
		return (Long) createHQLQuery("select count(l) from LeituraMovimentoImpl l where "
				+ "l.ciclo = :ciclo and l.grupoFaturamento.chavePrimaria = :grupoFaturamento and l.anoMesFaturamento = :anoMesFaturamento "
				+ "and l.habilitado = true").setParameter("ciclo", ciclo)
						.setParameter("anoMesFaturamento", anoMesFaturamento)
						.setParameter("grupoFaturamento", grupoFaturamento).list().stream().findFirst().orElse(0L);
	}

	private Map<Long, ContratoPontoConsumo> validarContratoAtivoPontoConsumo(Long[] chavesPontoConsumo,
			ControladorContrato controladorContrato) {
		Map<Long, ContratoPontoConsumo> lista = new HashMap<>();
		Integer max = MAX_IN;
		Integer maximo = 0;
		int fim = 0;
		int tamanhoVetor = 0;

		if (chavesPontoConsumo.length < MAX_IN) {
			fim = tamanhoVetor = chavesPontoConsumo.length;
		} else {
			fim = tamanhoVetor = max;
		}
		int ini = 0;
		// Quantas vezes o procedimento será executado
		for (int i = 0; i <= chavesPontoConsumo.length / max; i++) {
			Long[] lstPesq = new Long[tamanhoVetor];
			int cont = 0;
			// Separa a faixa de valores
			for (int k = ini; k < fim; k++) {
				lstPesq[cont] = chavesPontoConsumo[k];
				cont++;
			}
			if (lstPesq[i] != null && lstPesq.length != 0) {
				Map<Long, ContratoPontoConsumo> m = controladorContrato.obterContratoAtivoPontoConsumo(lstPesq);
				lista.putAll(m);
			}

			ini = ini + fim;
			fim = fim + ini;
			maximo = fim - max;

			if (i == (chavesPontoConsumo.length / maximo) - 1) {
				fim = chavesPontoConsumo.length;
			}
		}
		return lista;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.ControladorLeituraMovimento#
	 * removerPontosConsumoQtdDiasLeitura(java.util.Collection)
	 */
	@Override
	public void removerPontosConsumoSemLeituraMovimento(Collection<PontoConsumo> pontosConsumo, GrupoFaturamento grupo)
			throws NegocioException {

		Collection<PontoConsumo> pontosConsumoRemovidos = new HashSet<PontoConsumo>();
		if (!pontosConsumo.isEmpty() && grupo.getTipoLeitura().getChavePrimaria() == CHAVE_TIPO_LEITURA_COLETOR_DADOS) {

			for (PontoConsumo pontoConsumo : pontosConsumo) {

				if (!existeLeituraMovimento(pontoConsumo.getChavePrimaria(), grupo.getAnoMesReferencia(),
						grupo.getNumeroCiclo())) {
					pontosConsumoRemovidos.add(pontoConsumo);
				}

			}
		}
		if (!pontosConsumoRemovidos.isEmpty()) {
			pontosConsumo.removeAll(pontosConsumoRemovidos);
		}
	}

	/**
	 * Verifica se existe leitura movimento habilitada para o ponto de consumo,
	 * naquela referencia e ciclo
	 * 
	 * @param chavePontoConsumo chave do ponto de consumo
	 * @param anoMesFaturamento ano mês faturamento
	 * @param ciclo             número do ciclo
	 * @return retorna true caso exista leitura movimento e false caso nao exista
	 */
	private boolean existeLeituraMovimento(Long chavePontoConsumo, Integer anoMesFaturamento, Integer numeroCiclo) {
		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select count(movimento) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" movimento ");
		hql.append(" where ");
		hql.append(" movimento.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and movimento.habilitado = true ");
		hql.append(" and movimento.ciclo = :ciclo ");
		hql.append(" and movimento.anoMesFaturamento = :anoMesFaturamento");
		hql.append(" and pontoConsumo.instalacaoMedidor is not null ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idPontoConsumo", chavePontoConsumo);
		query.setParameter("anoMesFaturamento", anoMesFaturamento);
		query.setParameter(CICLO, numeroCiclo);

		Long quantidade = (Long) query.uniqueResult();

		return (quantidade != null && quantidade > 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura. ControladorLeituraMovimento#
	 * consultarMovimentosLeituraNaoProcessados(java .lang.Long[])
	 */

	@Override
	@SuppressWarnings({ "squid:S1192", "unchecked" })
	public Collection<LeituraMovimento> consultarMovimentosLeituraNaoProcessadosPorRota(
			Map<String, Object> parametros) {

		Collection<LeituraMovimento> listaLeituraMovimento = new ArrayList<LeituraMovimento>();

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" leituraMovimento ");
		hql.append(" where ");
		hql.append(" leituraMovimento.situacaoLeitura <> 4 ");
		hql.append(" and leituraMovimento.habilitado = 1 ");
		hql.append(" and leituraMovimento.rota.chavePrimaria = :idRota ");
		hql.append(" and leituraMovimento.anoMesFaturamento = :referencia ");
		hql.append(" and leituraMovimento.ciclo = :ciclo");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idRota", (Long) parametros.get("chaveRota"));
		query.setParameter("referencia", (Integer) parametros.get("anoMesFaturamento"));
		query.setParameter("ciclo", (Integer) parametros.get("numeroCiclo"));

		listaLeituraMovimento = query.list();

		return listaLeituraMovimento;
	}

	@Override
	public boolean consultaLeituraMovimentoComAnormalidadeNaoFaturar(Long chavePontoConsumo, Integer anoMesFaturamento,
			Integer numeroCiclo) {
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();

		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));

		Query query = null;
		StringBuilder hql = new StringBuilder();

		hql.append(" select count(movimento) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" movimento ");
		hql.append(" where ");
		hql.append(" movimento.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and movimento.habilitado = true ");
		hql.append(" and movimento.ciclo = :ciclo ");
		hql.append(" and movimento.anoMesFaturamento = :anoMesFaturamento");
		hql.append(" and pontoConsumo.instalacaoMedidor is not null ");
		hql.append(" and movimento.instalacaoMedidor is not null ");
		hql.append(" and movimento.codigoAnormalidadeLeitura = :codigoAnormalidadeLeitura ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("idPontoConsumo", chavePontoConsumo);
		query.setParameter("anoMesFaturamento", anoMesFaturamento);
		query.setParameter(CICLO, numeroCiclo);
		query.setParameter("codigoAnormalidadeLeitura", codigoAnormalidadeLeitura);

		Long quantidade = (Long) query.uniqueResult();

		return (quantidade != null && quantidade > 0);
	}

	@Override
	public Long consultarQuantidadePontosConsumoConsistirLeitura(Integer anoMesReferencia, Integer ciclo,
			Long grupoFaturamento) {
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();
		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));

		StringBuilder query = new StringBuilder();
		
		query.append("select count(l) from LeituraMovimentoImpl l where (l.codigoAnormalidadeLeitura is null or l.codigoAnormalidadeLeitura != :codigoAnormalidade) ")
		.append("and l.grupoFaturamento.chavePrimaria = :grupoFaturamento and l.ciclo = :ciclo ")
		.append("and l.anoMesFaturamento = :anoMesReferencia and l.habilitado = true ")
		.append("and (l.pontoConsumo.chavePrimaria not in ")
		.append("    	(select c.pontoConsumo.chavePrimaria from HistoricoConsumoImpl c ")
		.append("        where c.habilitado = true and c.anoMesFaturamento = :anoMesReferencia) ")
		.append("   or ((l.pontoConsumo.chavePrimaria in ")
		.append("          (select c.pontoConsumo.chavePrimaria from HistoricoConsumoImpl c where c.habilitado = true and c.anoMesFaturamento = :anoMesReferencia)) ")
		.append("      and ( l.pontoConsumo.chavePrimaria in ")
		.append("          (select f.pontoConsumo.chavePrimaria from FaturaImpl f ")
		.append("           where f.anoMesReferencia = :anoMesReferencia ")
		.append("           and f.rota.grupoFaturamento.chavePrimaria = :grupoFaturamento ")
		.append("           and f.creditoDebitoSituacao.chavePrimaria = :faturaCancelada)) ")
		.append("      and (l.pontoConsumo.chavePrimaria not in ")
		.append("          (select f.pontoConsumo.chavePrimaria from FaturaImpl f ")
		.append("           where f.anoMesReferencia = :anoMesReferencia ")
		.append("           and f.rota.grupoFaturamento.chavePrimaria = :grupoFaturamento ")
		.append("           and f.creditoDebitoSituacao.chavePrimaria = :faturaNormal)))) ");

	return (Long) createHQLQuery(query.toString()).setParameter("anoMesReferencia", anoMesReferencia)
							.setParameter("codigoAnormalidade", codigoAnormalidadeLeitura)
							.setParameter("ciclo", ciclo)
							.setParameter("grupoFaturamento", grupoFaturamento)
							.setParameter("faturaCancelada", CreditoDebitoSituacao.CANCELADA)
							.setParameter("faturaNormal", CreditoDebitoSituacao.NORMAL).list()
							.stream().findFirst().orElse(0L);

	}

	@Override
	public Long consultarQuantidadePontosConsumoRegistrarLeitura(Integer anoMesReferencia, Integer ciclo,
			Long grupoFaturamento) {
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia()
				.getControladorConstanteSistema();
		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));

		return (Long) createHQLQuery(
				"select count(l) from LeituraMovimentoImpl l where (l.codigoAnormalidadeLeitura is null or l.codigoAnormalidadeLeitura != :codigoAnormalidade)"
						+ "and l.grupoFaturamento.chavePrimaria = :grupoFaturamento and l.ciclo = :ciclo "
						+ "and l.anoMesFaturamento = :anoMesReferencia and l.habilitado = true and l.pontoConsumo.chavePrimaria not in "
						+ "(select m.pontoConsumo.chavePrimaria from HistoricoMedicaoImpl m where m.numeroCiclo = :ciclo and m.anoMesLeitura = :anoMesReferencia and m.habilitado = true)")
								.setParameter("anoMesReferencia", anoMesReferencia)
								.setParameter("codigoAnormalidade", codigoAnormalidadeLeitura)
								.setParameter("ciclo", ciclo).setParameter("grupoFaturamento", grupoFaturamento).list()
								.stream().findFirst().orElse(0L);

	}
	
	
	@Override
	public Boolean atualizarLeituraMovimento(Element elemento, StringBuilder logLeiturasFalhas,
			SituacaoLeituraMovimento leituraRetornada, SituacaoLeituraMovimento emLeitura) throws ConcorrenciaException, NegocioException {
		LeituraMovimento leitura = null;
		LeituraMovimento filtro = (LeituraMovimento) this.criar();
		filtro.setSituacaoLeitura(emLeitura);
		
		NegocioException erro = popularLeituraMovimento(elemento, filtro);
		if (erro != null) {
			preencherLogLeiturasFalhas(logLeiturasFalhas, filtro, erro.getMessage());
		} else {
			leitura = this.consultarPorSituacaoAnoMesCicloPontoConsumo(filtro);

			if (leitura != null) {
				leitura.setValorLeitura(filtro.getValorLeitura());
				leitura.setDataLeitura(filtro.getDataLeitura());
				leitura.setCodigoAnormalidadeLeitura(filtro.getCodigoAnormalidadeLeitura());
				leitura.setIndicadorConfirmacaoLeitura(filtro.getIndicadorConfirmacaoLeitura());
				leitura.setOrigem(OrigemLeituraMovimento.XML.name());
				leitura.setSituacaoLeitura(leituraRetornada);
				
				this.atualizar(leitura);
				
				return Boolean.TRUE;
				
			} else {
				preencherLogLeiturasFalhas(logLeiturasFalhas, filtro,
						"Leitura pendente não encontrada para os valores informados.");
			}
		}

		return Boolean.FALSE;
		
	}
	
	
	/**
	 * Preencher log leituras falhas.
	 * 
	 * @param logLeiturasFalhas
	 *            the log leituras falhas
	 * @param filtro
	 *            the filtro
	 * @param erro
	 *            the erro
	 */
	private void preencherLogLeiturasFalhas(StringBuilder logLeiturasFalhas, LeituraMovimento filtro, String erro)  {

		logLeiturasFalhas.append("Dados da leitura importada: \n");
		String ciclo = definirCiclo(filtro);
		String anoMesFaturamento = definirAnoMesFaturamento(filtro);
		logLeiturasFalhas.append(" - Ano mês de faturamento = ").append((anoMesFaturamento));

		
		if (filtro.getCiclo() != null) {
			logLeiturasFalhas.append("   Ciclo = ").append(filtro.getCiclo()).append(" \n");
		} else {
			logLeiturasFalhas.append("   Ciclo = ").append(NAO_INFORMADO).append(" \n");
		}
		
		String pontoConsumo = definirPontoConsumo(filtro);
		logLeiturasFalhas.append(" - Ponto de consumo = ").append((pontoConsumo)).append(" \n");
		logLeiturasFalhas.append("Erro: ").append(erro).append(" \r\n\r\n");
	}
	
	
	private String definirPontoConsumo(LeituraMovimento filtro) {

		if(filtro.getPontoConsumo() != null 
						&& filtro.getPontoConsumo().getChavePrimaria() > 0){
			return String.valueOf(filtro.getPontoConsumo().getChavePrimaria() +" - "+ filtro.getPontoConsumo().getDescricao());
		}else{
			return NAO_INFORMADO;
		}
		
	}

	private String definirCiclo(LeituraMovimento filtro) {

		if(filtro.getAnoMesFaturamento() != null){
			return filtro.getCiclo().toString();
		}else{
			return NAO_INFORMADO;
		}
		
	}
	
	
	/**
	 * Popular leitura movimento.
	 * 
	 * @param elemento
	 *            the elemento
	 * @param leitura
	 *            the leitura
	 * @return the negocio exception
	 */
	private NegocioException popularLeituraMovimento(Element elemento, LeituraMovimento leitura) {

		
		ControladorPontoConsumo controladorPontoConsumo = (ControladorPontoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
				ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO);
		
		NegocioException erro = null;

		try {
			String atributoCiclo = elemento.getAttributeValue("ciclo");
			if(atributoCiclo != null) {
				Integer ciclo = Integer.parseInt(atributoCiclo);
				leitura.setCiclo(ciclo);
			}

			String atributoAnoMes = elemento.getAttributeValue("anoMesFaturamento");
			if(atributoAnoMes != null) {
				Integer anoMes = Integer.parseInt(elemento.getAttributeValue("anoMesFaturamento"));
				leitura.setAnoMesFaturamento(anoMes);
				if(!Util.validarAnoMes(anoMes)) {
					erro = new NegocioException("anoMês inválido.");
				}
			}

			long idPontoConsumo = Long.parseLong(elemento.getAttributeValue("pontoConsumo"));
			PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.criar();
			pontoConsumo.setChavePrimaria(idPontoConsumo);
			leitura.setPontoConsumo(pontoConsumo);
			pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(idPontoConsumo);
			leitura.setPontoConsumo(pontoConsumo);

			BigDecimal valorLeitura; 
			
			if(elemento.getAttributeValue(VALOR_LEITURA) != null && !"".equals(elemento.getAttributeValue(VALOR_LEITURA))){
				valorLeitura = new BigDecimal(elemento.getAttributeValue(VALOR_LEITURA));
			}else{
				valorLeitura = null;
			}
			leitura.setValorLeitura(valorLeitura);

			Long codigoAnormalidade;
			if(elemento.getAttributeValue(CODIGO_ANORMALIDADE_LEITURA) != null 
							&& !"".equals(elemento.getAttributeValue(CODIGO_ANORMALIDADE_LEITURA))){
				codigoAnormalidade = Long.parseLong(elemento.getAttributeValue(CODIGO_ANORMALIDADE_LEITURA));
			}else{
				codigoAnormalidade = null;
			}
			leitura.setCodigoAnormalidadeLeitura(codigoAnormalidade);

			Boolean confirmacao = Boolean.parseBoolean(elemento.getAttributeValue("indicadorConfirmacaoLeitura"));
			leitura.setIndicadorConfirmacaoLeitura(confirmacao);

			Date dataLeitura;

			if (elemento.getAttributeValue(DATA_LEITURA) != null && !"".equals(elemento.getAttributeValue(DATA_LEITURA))) {
				dataLeitura = Util.converterCampoStringParaData("data de leitura", elemento.getAttributeValue(DATA_LEITURA),
								Constantes.FORMATO_DATA_BR);
			} else {
				dataLeitura = Calendar.getInstance().getTime();
			}
							
			leitura.setDataLeitura(dataLeitura);

		} catch (GGASException e) {
			LOG.error(e.getMessage(), e);
			String msg; 
			if(mensagens != null && e.getChaveErro() != null){
				msg = mensagens.getMessage(e.getChaveErro(), e.getParametrosErro(), null);
			}else{
				msg = e.getChaveErro();
			}
			erro = new NegocioException(msg);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			erro = new NegocioException(e.getMessage());
		}

		return erro;
	}
	
	private String definirAnoMesFaturamento(LeituraMovimento filtro) {

		if(filtro.getAnoMesFaturamentoFormatado() != null){
			return filtro.getAnoMesFaturamentoFormatado().toString();
		}else{
			return NAO_INFORMADO;
		}
		
	}
	
	
	/**
	 * Monta a query de pesquisa de leitura movimento
	 * 
	 * @param pesquisa parametro de pesquisa Suppresswarnings: necessario para a
	 *                 query
	 * @return retorna o {@link SQLQuery} montado
	 */
	@SuppressWarnings("squid:S1192")
	private SQLQuery montarQueryHistoricoLeitura(Long chavePontoConsumo) {
		StringBuilder queryNativa = new StringBuilder();
		queryNativa.append("SELECT "
				+ "	mh.MEHI_DT_AM_LEITURA AS referencia, mh.mehi_tm_leitura_informada AS dataAtual, mh.MEHI_TM_LEITURA_ANTERIOR AS dataAnterior, "
				+ "	mh.MEHI_MD_LEITURA_INFORMADA AS leituraAtual, mh.MEHI_MD_LEITURA_ANTERIOR AS leituraAnterior, la.LEAN_CD as codigoAnormalidadeLeitura, la.LEAN_DS AS leituraAnormalidade, la.LEAN_IN_BLOQUEIA_FATURAMENTO as anormalidadeLeituraGravidade, "
				+ "	h.cohi_md_consumo,h.COHI_MD_CONSUMO_APURADO, h.COHI_NR_FATOR_PTZ , h.COHI_NR_FATOR_PCS, h.COHI_QN_DIAS_CONSUMO, ca.COAN_CD, ca.COAN_DS, ca.COAN_IN_BLOQUEIA_FATURAMENTO, count(*) over () as Count ");
		queryNativa.append(" FROM MEDICAO_HISTORICO mh ");
		queryNativa.append(" LEFT JOIN CONSUMO_HISTORICO h ON ");
		queryNativa.append(" h.COHI_NR_CICLO = mh.MEHI_nr_ciclo AND h.COHI_DT_AM_FATURAMENTO = mh.MEHI_DT_AM_LEITURA ");
		queryNativa.append(" AND h.POCN_CD = mh.POCN_CD AND h.COHI_IN_USO = 1 AND h.COHI_IN_CONSUMO_CICLO = 1 ");
		queryNativa.append(" LEFT JOIN LEITURA_ANORMALIDADE la ON la.LEAN_CD = mh.LEAN_CD_INFORMADA ");
		queryNativa.append(" LEFT JOIN CONSUMO_ANORMALIDADE ca ON ca.COAN_CD = h.COAN_CD  ");
		ArrayList<String> whereStm = new ArrayList<>();
		whereStm.add(" mh.MEHI_IN_USO = 1 ");

		HashMap<String, Object> params = new HashMap<>();
		Optional.ofNullable(chavePontoConsumo).ifPresent(r -> {
			whereStm.add(" mh.POCN_CD = :pontoConsumo");
			params.put("pontoConsumo", r);
		});

		Optional.ofNullable(whereStm.toString()).filter(StringUtils::isNotBlank)
				.ifPresent(r -> queryNativa.append(" where ").append(String.join(" and ", whereStm)));
		queryNativa.append(" ORDER BY mh.MEHI_DT_AM_LEITURA desc");
		SQLQuery query = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createSQLQuery(queryNativa.toString());
		for (Map.Entry<String, Object> entrada : params.entrySet()) {
			String chave = entrada.getKey();
			Object info = entrada.getValue();
			if (info instanceof Collection) {
				query.setParameterList(chave, (List) info);
			} else {
				query.setParameter(chave, info);
			}
		}
		return query;
	}
	
	
	@SuppressWarnings("squid:S1192")
	@Override
	public Pair<List<LeituraMovimentoDTO>, Long> criarListaHistoricoLeituraMovimentoDTO(Long chavePontoConsumo) {
		SQLQuery query = montarQueryHistoricoLeitura(chavePontoConsumo);
		return preencherLeiturasHistoricoLeitura(query);
	}
	
	
	private Pair<List<LeituraMovimentoDTO>, Long> preencherLeiturasHistoricoLeitura(SQLQuery query) {
		List<Objects[]> results = query.list();
		List<LeituraMovimentoDTO> leituras = new ArrayList<>();
		Long totalRegistros = 0L;
		for (Object[] row : results) {
			LeituraMovimentoDTO leitura = new LeituraMovimentoDTO();
			Optional.ofNullable(row[0]).ifPresent(info -> leitura.setMesAnoCiclo(info.toString()));
			Optional.ofNullable(row[1]).ifPresent(info -> {
				String dataFormatada = new SimpleDateFormat(Constantes.FORMATO_DATA_BR).format(info);
				leitura.setDataLeitura(dataFormatada);
			});
			Optional.ofNullable(row[2]).ifPresent(info -> {
				String dataFormatada = new SimpleDateFormat(Constantes.FORMATO_DATA_BR).format(info);
				leitura.setDataLeituraAnterior(dataFormatada);
			});
			Optional.ofNullable(row[3]).ifPresent(info -> leitura.setLeituraAtual(Double.valueOf(info.toString())));
			Optional.ofNullable(row[4]).ifPresent(info -> leitura.setLeituraAnterior(Double.valueOf(info.toString())));
			if (row[5] != null && row[6] != null && row[7] != null) {
				Optional.ofNullable(row[5]).ifPresent(info -> {
					AnormalidadeDTO anormalidadeLeitura = new AnormalidadeDTO(Long.valueOf(row[5].toString()),
							row[6].toString(), ((BigDecimal) row[7]).compareTo(BigDecimal.ONE) == 0);
					leitura.setAnormalidadeLeitura(anormalidadeLeitura);
				});
			}
			
			Optional.ofNullable(row[8]).ifPresent(info -> leitura.setConsumo(Double.valueOf(info.toString())));
			Optional.ofNullable(row[9]).ifPresent(info -> leitura.setConsumoApurado(Double.valueOf(info.toString())));
			Optional.ofNullable(row[10]).ifPresent(info -> leitura.setFcPtz(Double.valueOf(info.toString())));
			Optional.ofNullable(row[11]).ifPresent(info -> leitura.setFcPcs(Double.valueOf(info.toString())));
			Optional.ofNullable(row[12]).ifPresent(info -> leitura.setQuantidadeDias(Integer.valueOf(info.toString())));


			if (row[13] != null && row[14] != null && row[15] != null) {
				Optional.ofNullable(row[13]).ifPresent(info -> {
					AnormalidadeDTO anormalidadeConsumo = new AnormalidadeDTO(Long.valueOf(row[13].toString()),
							row[14].toString(), ((BigDecimal) row[15]).compareTo(BigDecimal.ONE) == 0);
					leitura.setAnormalidadeConsumo(anormalidadeConsumo);
				});
			}
			
			totalRegistros = Long.valueOf(row[16].toString());
			leituras.add(leitura);
		}
		return new Pair<>(leituras, totalRegistros);
	}

		
}
