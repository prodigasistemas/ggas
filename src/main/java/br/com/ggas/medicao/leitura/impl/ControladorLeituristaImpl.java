/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.leitura.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.CollectionUtils;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.leitura.ControladorLeiturista;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 *
 */
class ControladorLeituristaImpl extends ControladorNegocioImpl implements ControladorLeiturista {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Leiturista.BEAN_ID_LEITURISTA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Leiturista.BEAN_ID_LEITURISTA);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorLeiturista
	 * #validarRemoverLeituristas
	 * (java.lang.Long[])
	 */
	@Override
	public void validarRemoverLeituristas(Long[] chavesPrimarias) throws NegocioException {

		if(chavesPrimarias == null || chavesPrimarias.length == 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_MULTI_SELECAO_DE_CHAVES, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preInsercao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preInsercao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Leiturista leiturista = (Leiturista) entidadeNegocio;
		validarLeituristaExistente(leiturista);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#preAtualizacao(br.com.ggas.geral.negocio.EntidadeNegocio)
	 */
	@Override
	public void preAtualizacao(EntidadeNegocio entidadeNegocio) throws NegocioException {

		Leiturista leiturista = (Leiturista) entidadeNegocio;
		validarLeituristaExistente(leiturista);
		validarInativacaoLeiturista(leiturista);
	}

	/**
	 * Validar inativacao leiturista.
	 * 
	 * @param leiturista
	 *            the leiturista
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarInativacaoLeiturista(Leiturista leiturista) throws NegocioException {

		ControladorRota controladorRota = ServiceLocator.getInstancia().getControladorRota();
		if(!leiturista.isHabilitado()) {

			Query query = null;
			StringBuilder hql = new StringBuilder();
			hql.append(" select count(chavePrimaria) from ");
			hql.append(controladorRota.getClasseEntidade().getSimpleName());
			hql.append(" where leiturista.chavePrimaria = :idLeiturista");
			hql.append(" and leiturista.habilitado = true ");
			hql.append(" and habilitado = true ");

			query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
			query.setLong("idLeiturista", leiturista.getChavePrimaria());

			Long quantidade = (Long) query.uniqueResult();

			if(quantidade != null && quantidade > 0) {
				throw new NegocioException(ERRO_NEGOCIO_INATIVACAO_LEITURISTA_ASSOCIADO_ROTA, false);
			}
		}
	}

	/**
	 * Validar leiturista existente.
	 * 
	 * @param leiturista
	 *            the leiturista
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private void validarLeituristaExistente(Leiturista leiturista) throws NegocioException {

		Criteria criteria = getCriteria();

		Long idFuncionario = leiturista.getFuncionario().getChavePrimaria();
		if((idFuncionario != null) && (idFuncionario > 0)) {
			criteria.createAlias("funcionario", "funcionario");
			criteria.add(Restrictions.eq("funcionario.chavePrimaria", idFuncionario));
		} else {
			criteria.setFetchMode("funcionario", FetchMode.JOIN);
		}

		List<Leiturista> listaLeituristas = criteria.list();

		if(!listaLeituristas.isEmpty()) {
			Leiturista leituristaExistente = listaLeituristas.get(0);

			Session sessao = getHibernateTemplate().getSessionFactory().getCurrentSession();
			sessao.evict(leituristaExistente);

			if(leituristaExistente.getChavePrimaria() != leiturista.getChavePrimaria()) {
				throw new NegocioException(ControladorLeiturista.ERRO_NEGOCIO_LEITURISTA_EXISTENTE, true);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorLeiturista
	 * #consultarLeituristas(java.util.Map)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Leiturista> consultarLeituristas(Map<String, Object> filtro) throws NegocioException {

		Criteria criteria = getCriteria();

		if(filtro != null) {

			Long[] chavesPrimarias = (Long[]) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
			if(chavesPrimarias != null && chavesPrimarias.length > 0) {
				criteria.add(Restrictions.in(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavesPrimarias));
			}
			Long chavePrimaria = (Long) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA);
			if(chavePrimaria != null && chavePrimaria > 0) {
				criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria));
			}
			Long idFuncionario = (Long) filtro.get("idFuncionario");
			if((idFuncionario != null) && (idFuncionario > 0)) {
				criteria.createAlias("funcionario", "funcionario");
				criteria.add(Restrictions.eq("funcionario.chavePrimaria", idFuncionario));
			} else {
				criteria.setFetchMode("funcionario", FetchMode.JOIN);
			}
			Boolean habilitado = (Boolean) filtro.get("habilitado");
			if(habilitado != null) {
				criteria.add(Restrictions.eq("habilitado", habilitado));
			}

		}

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.ControladorLeiturista#obterListaLeituristaEmOrdemAlfabetica(java.util.HashMap)
	 */
	@Override
	public Collection<Leiturista> obterListaLeituristaEmOrdemAlfabetica(Map<String, Object> filtro) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		List<Long> chavesPrimarias = null;
		if(filtro != null) {
			chavesPrimarias = (List<Long>) filtro.get(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS);
		}
		hql.append("  from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" leiturista ");
		hql.append(" inner join fetch leiturista.funcionario funcionario ");
		hql.append(" where ");
		hql.append(" leiturista.habilitado = true ");
		if(chavesPrimarias != null && !CollectionUtils.isEmpty(chavesPrimarias)) {
			hql.append(" and leiturista.chavePrimaria in (:chavesPrimarias) ");
		}
		hql.append(" order by funcionario.nome asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if(chavesPrimarias != null && !CollectionUtils.isEmpty(chavesPrimarias)) {
			query.setParameterList(EntidadeNegocio.ATRIBUTO_CHAVES_PRIMARIAS, chavesPrimarias);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.ControladorLeiturista#obterLeiturista(long)
	 */
	@Override
	public Leiturista obterLeiturista(long chavePrimaria) throws NegocioException {

		Query query = null;
		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" leiturista ");
		hql.append(" inner join fetch leiturista.funcionario funcionario ");
		hql.append(" where leiturista.chavePrimaria = :idLeiturista");
		hql.append(" and leiturista.habilitado = true ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idLeiturista", chavePrimaria);

		return (Leiturista) query.uniqueResult();

	}
}
