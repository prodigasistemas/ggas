/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.leitura.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela implementação dos métodos relacionados aos Movimentos de Leitura
 *
 */
public class LeituraMovimentoImpl extends EntidadeNegocioImpl implements LeituraMovimento {

	private static final long serialVersionUID = -8190827011085573747L;

	private Date dataGeracao;

	private PontoConsumo pontoConsumo;

	private SituacaoConsumo situacaoPontoConsumo;

	private String descicaoPontoConsumo;

	private String observacaoPontoConsumo;

	private Imovel imovel;

	private String perfilImovel;

	private String segmento;

	private Rota rota;

	private String numeroRota;

	private String numeroSetorComercial;

	private String numeroQuadra;

	private Integer numeroFaceQuadra;

	private Integer numeroLoteImovel;

	private Integer subLoteImovel;

	private Integer sequencialLeitura;

	private Empresa empresaResponsavel;

	private Leiturista leiturista;

	private TipoLeitura tipoLeitura;

	private GrupoFaturamento grupoFaturamento;

	private String nomeCliente;

	private String endereco;

	private String complementoEmdereco;

	private String bairro;

	private String municipio;

	private String uf;

	private String cep;

	private String nomeContato;

	private String dadosContato;

	private String observacoesCadastrais;

	private String numeroSerieMedidor;

	private InstalacaoMedidor instalacaoMedidor;

	private Date dataInstalacaoMedidor;

	private String localInstalacaoMedidor;

	private Date dataLeituraAnterior;

	private Date dataLeituraPrevista;

	private Date dataProximaLeituraPrevista;

	private BigDecimal minimoVariacaoConsumo;

	private BigDecimal maximoVariacaoConsumo;

	private BigDecimal minimoLeituraEsperada;

	private BigDecimal maximoLeituraEsperada;

	private BigDecimal leituraAnterior;

	private AnormalidadeLeitura anormalidadeLeituraAnterior;

	private String situacaoLeituraAnterior;

	private BigDecimal consumoMedio;

	private BigDecimal consumoAnterior;

	private Integer numeroColetor;

	private Date dataLeitura;

	private BigDecimal valorLeitura;

	private Long codigoAnormalidadeLeitura;

	private AnormalidadeConsumo anormalidadeConsumo;

	private SituacaoLeituraMovimento situacaoLeitura;

	private Boolean indicadorConfirmacaoLeitura;

	private Usuario usuario;

	private String observacaoLeitura;

	private Integer ciclo;

	private Integer anoMesFaturamento;

	private String dataNovaLeitura;

	private BigDecimal pressaoInformada;

	private BigDecimal temperaturaInformada;

	private Unidade unidadePressaoInformada;

	private Unidade unidadeTemperaturaInformada;

	private String origem;

	private Boolean corrigePT;

	private BigDecimal latitude;

	private BigDecimal longitude;

	private String fotoColetor;

	private String urlFoto;

	public LeituraMovimentoImpl(){
	}

	/**
	 * Construtor inicializado por parametros.
	 * 
	 * @param chavePrimaria
	 * 					the chavePrimaria
	 * @param chavePrimariaPontoConsumo
	 * 					the chavePrimariaPontoConsumo
	 * @param nomeCliente
	 * 					the nomeCliente
	 * @param descicaoPontoConsumo
	 * 					the descicaoPontoConsumo
	 * @param endereco
	 * 					the endereco
	 * @param anoMesFaturamento
	 * 					the anoMesFaturamento
	 * @param ciclo
	 * 					the ciclo
	 * @param chavePrimariaSituacao
	 * 					the chavePrimariaSituacao
	 * @param valorLeitura
	 * 					the valorLeitura
	 * @param pressaoInformada
	 * 					the pressaoInformada
	 * @param temperaturaInformada
	 * 					the temperaturaInformada
	 * @param dataLeitura
	 * 					the dataLeitura
	 * 
	 */
	public LeituraMovimentoImpl(Long chavePrimaria, Long chavePrimariaPontoConsumo,
			String nomeCliente, String descicaoPontoConsumo, String endereco,
			Integer anoMesFaturamento, Integer ciclo, Long chavePrimariaSituacao,
			BigDecimal valorLeitura, BigDecimal pressaoInformada, BigDecimal temperaturaInformada, Date dataLeitura, String urlFoto) {
		this.setChavePrimaria(chavePrimaria);
		this.nomeCliente = nomeCliente;
		this.descicaoPontoConsumo = descicaoPontoConsumo;
		this.endereco = endereco;
		this.anoMesFaturamento = anoMesFaturamento;
		this.ciclo = ciclo;
		this.pontoConsumo = new PontoConsumoImpl();
		this.pontoConsumo.setChavePrimaria(chavePrimariaPontoConsumo);
		this.situacaoLeitura = new SituacaoLeituraMovimentoImpl();
		this.situacaoLeitura.setChavePrimaria(chavePrimariaSituacao);

		this.valorLeitura = valorLeitura;
		this.pressaoInformada = pressaoInformada;
		this.temperaturaInformada = temperaturaInformada;
		this.dataLeitura = dataLeitura;
		this.urlFoto = urlFoto;
	}

	/**
	 * @return the dataGeracao
	 */
	@Override
	public Date getDataGeracao() {
		Date data = null;
		if (this.dataGeracao != null) {
			data = (Date) dataGeracao.clone();
		}
		return data;
	}

	/**
	 * @param dataGeracao
	 *            the dataGeracao to set
	 */
	@Override
	public void setDataGeracao(Date dataGeracao) {
		if (dataGeracao != null) {
			this.dataGeracao = (Date) dataGeracao.clone();
		} else {
			this.dataGeracao = null;
		}
	}

	/**
	 * @return the pontoConsumo
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/**
	 * @return the situacaoPontoConsumo
	 */
	@Override
	public SituacaoConsumo getSituacaoPontoConsumo() {

		return situacaoPontoConsumo;
	}

	/**
	 * @param situacaoPontoConsumo
	 *            the situacaoPontoConsumo to set
	 */
	@Override
	public void setSituacaoPontoConsumo(SituacaoConsumo situacaoPontoConsumo) {

		this.situacaoPontoConsumo = situacaoPontoConsumo;
	}

	/**
	 * @return the imovel
	 */
	@Override
	public Imovel getImovel() {

		return imovel;
	}

	/**
	 * @param imovel
	 *            the imovel to set
	 */
	@Override
	public void setImovel(Imovel imovel) {

		this.imovel = imovel;
	}

	/**
	 * @return the perfilImovel
	 */
	@Override
	public String getPerfilImovel() {

		return perfilImovel;
	}

	/**
	 * @param perfilImovel
	 *            the perfilImovel to set
	 */
	@Override
	public void setPerfilImovel(String perfilImovel) {

		this.perfilImovel = perfilImovel;
	}

	/**
	 * @return the segmento
	 */
	@Override
	public String getSegmento() {

		return segmento;
	}

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	@Override
	public void setSegmento(String segmento) {

		this.segmento = segmento;
	}

	/**
	 * @return the rota
	 */
	@Override
	public Rota getRota() {

		return rota;
	}

	/**
	 * @param rota
	 *            the rota to set
	 */
	@Override
	public void setRota(Rota rota) {

		this.rota = rota;
	}

	/**
	 * @return the numeroRota
	 */
	@Override
	public String getNumeroRota() {

		return numeroRota;
	}

	/**
	 * @param numeroRota
	 *            the numeroRota to set
	 */
	@Override
	public void setNumeroRota(String numeroRota) {

		this.numeroRota = numeroRota;
	}

	/**
	 * @return the numeroSetorComercial
	 */
	@Override
	public String getNumeroSetorComercial() {

		return numeroSetorComercial;
	}

	/**
	 * @param numeroSetorComercial
	 *            the numeroSetorComercial to set
	 */
	@Override
	public void setNumeroSetorComercial(String numeroSetorComercial) {

		this.numeroSetorComercial = numeroSetorComercial;
	}

	/**
	 * @return the numeroQuadra
	 */
	@Override
	public String getNumeroQuadra() {

		return numeroQuadra;
	}

	/**
	 * @param numeroQuadra
	 *            the numeroQuadra to set
	 */
	@Override
	public void setNumeroQuadra(String numeroQuadra) {

		this.numeroQuadra = numeroQuadra;
	}

	/**
	 * @return the numeroFaceQuadra
	 */
	@Override
	public Integer getNumeroFaceQuadra() {

		return numeroFaceQuadra;
	}

	/**
	 * @param numeroFaceQuadra
	 *            the numeroFaceQuadra to set
	 */
	@Override
	public void setNumeroFaceQuadra(Integer numeroFaceQuadra) {

		this.numeroFaceQuadra = numeroFaceQuadra;
	}

	/**
	 * @return the numeroLoteImovel
	 */
	@Override
	public Integer getNumeroLoteImovel() {

		return numeroLoteImovel;
	}

	/**
	 * @param numeroLoteImovel
	 *            the numeroLoteImovel to set
	 */
	@Override
	public void setNumeroLoteImovel(Integer numeroLoteImovel) {

		this.numeroLoteImovel = numeroLoteImovel;
	}

	/**
	 * @return the subLoteImovel
	 */
	@Override
	public Integer getSubLoteImovel() {

		return subLoteImovel;
	}

	/**
	 * @param subLoteImovel
	 *            the subLoteImovel to set
	 */
	@Override
	public void setSubLoteImovel(Integer subLoteImovel) {

		this.subLoteImovel = subLoteImovel;
	}

	/**
	 * @return the sequencialLeitura
	 */
	@Override
	public Integer getSequencialLeitura() {

		return sequencialLeitura;
	}

	/**
	 * @param sequencialLeitura
	 *            the sequencialLeitura to set
	 */
	@Override
	public void setSequencialLeitura(Integer sequencialLeitura) {

		this.sequencialLeitura = sequencialLeitura;
	}

	/**
	 * @return the empresaResponsavel
	 */
	@Override
	public Empresa getEmpresaResponsavel() {

		return empresaResponsavel;
	}

	/**
	 * @param empresaResponsavel
	 *            the empresaResponsavel to set
	 */
	@Override
	public void setEmpresaResponsavel(Empresa empresaResponsavel) {

		this.empresaResponsavel = empresaResponsavel;
	}

	/**
	 * @return the leiturista
	 */
	@Override
	public Leiturista getLeiturista() {

		return leiturista;
	}

	/**
	 * @param leiturista
	 *            the leiturista to set
	 */
	@Override
	public void setLeiturista(Leiturista leiturista) {

		this.leiturista = leiturista;
	}

	/**
	 * @return the tipoLeitura
	 */
	@Override
	public TipoLeitura getTipoLeitura() {

		return tipoLeitura;
	}

	/**
	 * @param tipoLeitura
	 *            the tipoLeitura to set
	 */
	@Override
	public void setTipoLeitura(TipoLeitura tipoLeitura) {

		this.tipoLeitura = tipoLeitura;
	}

	/**
	 * @return the grupoFaturamento
	 */
	@Override
	public GrupoFaturamento getGrupoFaturamento() {

		return grupoFaturamento;
	}

	/**
	 * @param grupoFaturamento
	 *            the grupoFaturamento to set
	 */
	@Override
	public void setGrupoFaturamento(GrupoFaturamento grupoFaturamento) {

		this.grupoFaturamento = grupoFaturamento;
	}

	/**
	 * @return the nomeCliente
	 */
	@Override
	public String getNomeCliente() {

		return nomeCliente;
	}

	/**
	 * @param nomeCliente
	 *            the nomeCliente to set
	 */
	@Override
	public void setNomeCliente(String nomeCliente) {

		this.nomeCliente = nomeCliente;
	}

	/**
	 * @return the endereco
	 */
	@Override
	public String getEndereco() {

		return endereco;
	}

	/**
	 * @param endereco
	 *            the endereco to set
	 */
	@Override
	public void setEndereco(String endereco) {

		this.endereco = endereco;
	}

	/**
	 * @return the complementoEmdereco
	 */
	@Override
	public String getComplementoEmdereco() {

		return complementoEmdereco;
	}

	/**
	 * @param complementoEmdereco
	 *            the complementoEmdereco to set
	 */
	@Override
	public void setComplementoEmdereco(String complementoEmdereco) {

		this.complementoEmdereco = complementoEmdereco;
	}

	/**
	 * @return the bairro
	 */
	@Override
	public String getBairro() {

		return bairro;
	}

	/**
	 * @param bairro
	 *            the bairro to set
	 */
	@Override
	public void setBairro(String bairro) {

		this.bairro = bairro;
	}

	/**
	 * @return the municipio
	 */
	@Override
	public String getMunicipio() {

		return municipio;
	}

	/**
	 * @param municipio
	 *            the municipio to set
	 */
	@Override
	public void setMunicipio(String municipio) {

		this.municipio = municipio;
	}

	/**
	 * @return the uf
	 */
	@Override
	public String getUf() {

		return uf;
	}

	/**
	 * @param uf
	 *            the uf to set
	 */
	@Override
	public void setUf(String uf) {

		this.uf = uf;
	}

	/**
	 * @return the cep
	 */
	@Override
	public String getCep() {

		return cep;
	}

	/**
	 * @param cep
	 *            the cep to set
	 */
	@Override
	public void setCep(String cep) {

		this.cep = cep;
	}

	/**
	 * @return the nomeContato
	 */
	@Override
	public String getNomeContato() {

		return nomeContato;
	}

	/**
	 * @param nomeContato
	 *            the nomeContato to set
	 */
	@Override
	public void setNomeContato(String nomeContato) {

		this.nomeContato = nomeContato;
	}

	/**
	 * @return the dadosContato
	 */
	@Override
	public String getDadosContato() {

		return dadosContato;
	}

	/**
	 * @param dadosContato
	 *            the dadosContato to set
	 */
	@Override
	public void setDadosContato(String dadosContato) {

		this.dadosContato = dadosContato;
	}

	/**
	 * @return the observacoesCadastrais
	 */
	@Override
	public String getObservacoesCadastrais() {

		return observacoesCadastrais;
	}

	/**
	 * @param observacoesCadastrais
	 *            the observacoesCadastrais to set
	 */
	@Override
	public void setObservacoesCadastrais(String observacoesCadastrais) {

		this.observacoesCadastrais = observacoesCadastrais;
	}

	/**
	 * @return the numeroSerieMedidor
	 */
	@Override
	public String getNumeroSerieMedidor() {

		return numeroSerieMedidor;
	}

	/**
	 * @param numeroSerieMedidor
	 *            the numeroSerieMedidor to set
	 */
	@Override
	public void setNumeroSerieMedidor(String numeroSerieMedidor) {

		this.numeroSerieMedidor = numeroSerieMedidor;
	}

	/**
	 * @return the instalacaoMedidor
	 */
	@Override
	public InstalacaoMedidor getInstalacaoMedidor() {

		return instalacaoMedidor;
	}

	/**
	 * @param instalacaoMedidor
	 *            the instalacaoMedidor to set
	 */
	@Override
	public void setInstalacaoMedidor(InstalacaoMedidor instalacaoMedidor) {

		this.instalacaoMedidor = instalacaoMedidor;
	}

	/**
	 * @return the dataInstalacaoMedidor
	 */
	@Override
	public Date getDataInstalacaoMedidor() {
		Date data = null;
		if (this.dataInstalacaoMedidor != null) {
			data = (Date) dataInstalacaoMedidor.clone();
		}
		return data;
	}

	/**
	 * @param dataInstalacaoMedidor
	 *            the dataInstalacaoMedidor to set
	 */
	@Override
	public void setDataInstalacaoMedidor(Date dataInstalacaoMedidor) {
		if (dataInstalacaoMedidor != null) {
			this.dataInstalacaoMedidor = (Date) dataInstalacaoMedidor.clone();
		} else {
			this.dataInstalacaoMedidor = null;
		}
	}

	/**
	 * @return the localInstalacaoMedidor
	 */
	@Override
	public String getLocalInstalacaoMedidor() {

		return localInstalacaoMedidor;
	}

	/**
	 * @param localInstalacaoMedidor
	 *            the localInstalacaoMedidor to
	 *            set
	 */
	@Override
	public void setLocalInstalacaoMedidor(String localInstalacaoMedidor) {

		this.localInstalacaoMedidor = localInstalacaoMedidor;
	}

	/**
	 * @return the dataLeituraAnterior
	 */
	@Override
	public Date getDataLeituraAnterior() {
		Date data = null;
		if (this.dataLeituraAnterior != null) {
			data = (Date) dataLeituraAnterior.clone();
		}
		return data;
	}

	/**
	 * @param dataLeituraAnterior
	 *            the dataLeituraAnterior to set
	 */
	@Override
	public void setDataLeituraAnterior(Date dataLeituraAnterior) {
		if (dataLeituraAnterior != null) {
			this.dataLeituraAnterior = (Date) dataLeituraAnterior.clone();
		} else {
			this.dataLeituraAnterior = null;
		}
	}

	/**
	 * @return the dataLeituraPrevista
	 */
	@Override
	public Date getDataLeituraPrevista() {
		Date data = null;
		if (this.dataLeituraPrevista != null) {
			data = (Date) dataLeituraPrevista.clone();
		}
		return data;
	}

	/**
	 * @param dataLeituraPrevista
	 *            the dataLeituraPrevista to set
	 */
	@Override
	public void setDataLeituraPrevista(Date dataLeituraPrevista) {
		if (dataLeituraPrevista != null) {
			this.dataLeituraPrevista = (Date) dataLeituraPrevista.clone();
		} else {
			this.dataLeituraPrevista = null;
		}
	}

	/**
	 * @return the dataProximaLeituraPrevista
	 */
	@Override
	public Date getDataProximaLeituraPrevista() {
		Date data = null;
		if (this.dataProximaLeituraPrevista != null) {
			data = (Date) dataProximaLeituraPrevista.clone();
		}
		return data;
	}

	/**
	 * @param dataProximaLeituraPrevista
	 *            the dataProximaLeituraPrevista to set
	 */
	@Override
	public void setDataProximaLeituraPrevista(Date dataProximaLeituraPrevista) {
		if (dataProximaLeituraPrevista != null) {
			this.dataProximaLeituraPrevista = (Date) dataProximaLeituraPrevista.clone();
		} else {
			this.dataProximaLeituraPrevista = null;
		}
	}

	/**
	 * @return the minimoVariacaoConsumo
	 */
	@Override
	public BigDecimal getMinimoVariacaoConsumo() {

		return minimoVariacaoConsumo;
	}

	/**
	 * @param minimoVariacaoConsumo
	 *            the minimoVariacaoConsumo to set
	 */
	@Override
	public void setMinimoVariacaoConsumo(BigDecimal minimoVariacaoConsumo) {

		this.minimoVariacaoConsumo = minimoVariacaoConsumo;
	}

	/**
	 * @return the maximoVariacaoConsumo
	 */
	@Override
	public BigDecimal getMaximoVariacaoConsumo() {

		return maximoVariacaoConsumo;
	}

	/**
	 * @param maximoVariacaoConsumo
	 *            the maximoVariacaoConsumo to set
	 */
	@Override
	public void setMaximoVariacaoConsumo(BigDecimal maximoVariacaoConsumo) {

		this.maximoVariacaoConsumo = maximoVariacaoConsumo;
	}

	/**
	 * @return the minimoLeituraEsperada
	 */
	@Override
	public BigDecimal getMinimoLeituraEsperada() {

		return minimoLeituraEsperada;
	}

	/**
	 * @param minimoLeituraEsperada
	 *            the minimoLeituraEsperada to set
	 */
	@Override
	public void setMinimoLeituraEsperada(BigDecimal minimoLeituraEsperada) {

		this.minimoLeituraEsperada = minimoLeituraEsperada;
	}

	/**
	 * @return the maximoLeituraEsperada
	 */
	@Override
	public BigDecimal getMaximoLeituraEsperada() {

		return maximoLeituraEsperada;
	}

	/**
	 * @param maximoLeituraEsperada
	 *            the maximoLeituraEsperada to set
	 */
	@Override
	public void setMaximoLeituraEsperada(BigDecimal maximoLeituraEsperada) {

		this.maximoLeituraEsperada = maximoLeituraEsperada;
	}

	/**
	 * @return the leituraAnterior
	 */
	@Override
	public BigDecimal getLeituraAnterior() {

		return leituraAnterior;
	}

	/**
	 * @param leituraAnterior
	 *            the leituraAnterior to set
	 */
	@Override
	public void setLeituraAnterior(BigDecimal leituraAnterior) {

		this.leituraAnterior = leituraAnterior;
	}

	/**
	 * @return the anormalidadeLeituraAnterior
	 */
	@Override
	public AnormalidadeLeitura getAnormalidadeLeituraAnterior() {

		return anormalidadeLeituraAnterior;
	}

	/**
	 * @param anormalidadeLeituraAnterior
	 *            the anormalidadeLeituraAnterior
	 *            to set
	 */
	@Override
	public void setAnormalidadeLeituraAnterior(AnormalidadeLeitura anormalidadeLeituraAnterior) {

		this.anormalidadeLeituraAnterior = anormalidadeLeituraAnterior;
	}

	/**
	 * @return the situacaoLeituraAnterior
	 */
	@Override
	public String getSituacaoLeituraAnterior() {

		return situacaoLeituraAnterior;
	}

	/**
	 * @param situacaoLeituraAnterior
	 *            the situacaoLeituraAnterior to set
	 */
	@Override
	public void setSituacaoLeituraAnterior(String situacaoLeituraAnterior) {

		this.situacaoLeituraAnterior = situacaoLeituraAnterior;
	}

	/**
	 * @return the consumoMedio
	 */
	@Override
	public BigDecimal getConsumoMedio() {

		return consumoMedio;
	}

	/**
	 * @param consumoMedio
	 *            the consumoMedio to set
	 */
	@Override
	public void setConsumoMedio(BigDecimal consumoMedio) {

		this.consumoMedio = consumoMedio;
	}

	/**
	 * @return the consumoAnterior
	 */
	@Override
	public BigDecimal getConsumoAnterior() {

		return consumoAnterior;
	}

	/**
	 * @param consumoAnterior
	 *            the consumoAnterior to set
	 */
	@Override
	public void setConsumoAnterior(BigDecimal consumoAnterior) {

		this.consumoAnterior = consumoAnterior;
	}

	/**
	 * @return the numeroColetor
	 */
	@Override
	public Integer getNumeroColetor() {

		return numeroColetor;
	}

	/**
	 * @param numeroColetor
	 *            the numeroColetor to set
	 */
	@Override
	public void setNumeroColetor(Integer numeroColetor) {

		this.numeroColetor = numeroColetor;
	}

	/**
	 * @return the dataLeitura
	 */
	@Override
	public Date getDataLeitura() {
		Date data = null;
		if (this.dataLeitura != null) {
			data = (Date) dataLeitura.clone();
		}
		return data;
	}

	/**
	 * @param dataLeitura
	 *            the dataLeitura to set
	 */
	@Override
	public void setDataLeitura(Date dataLeitura) {
		if(dataLeitura != null){
			this.dataLeitura = (Date) dataLeitura.clone();
		} else {
			this.dataLeitura = null;
		}
	}

	/**
	 * @return the valorLeitura
	 */
	@Override
	public BigDecimal getValorLeitura() {

		return valorLeitura;
	}

	/**
	 * @param valorLeitura
	 *            the valorLeitura to set
	 */
	@Override
	public void setValorLeitura(BigDecimal valorLeitura) {

		this.valorLeitura = valorLeitura;
	}

	/**
	 * @return the codigoAnormalidadeLeitura
	 */
	@Override
	public Long getCodigoAnormalidadeLeitura() {

		return codigoAnormalidadeLeitura;
	}

	/**
	 * @param codigoAnormalidadeLeitura
	 *            the codigoAnormalidadeLeitura to
	 *            set
	 */
	@Override
	public void setCodigoAnormalidadeLeitura(Long codigoAnormalidadeLeitura) {

		this.codigoAnormalidadeLeitura = codigoAnormalidadeLeitura;
	}

	/**
	 * @return the anormalidadeConsumo
	 */
	@Override
	public AnormalidadeConsumo getAnormalidadeConsumo() {

		return anormalidadeConsumo;
	}

	/**
	 * @param anormalidadeConsumo
	 *            the anormalidadeConsumo to set
	 */
	@Override
	public void setAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo) {

		this.anormalidadeConsumo = anormalidadeConsumo;
	}

	/**
	 * @return the situacaoLeitura
	 */
	@Override
	public SituacaoLeituraMovimento getSituacaoLeitura() {

		return situacaoLeitura;
	}

	/**
	 * @param situacaoLeitura
	 *            the situacaoLeitura to set
	 */
	@Override
	public void setSituacaoLeitura(SituacaoLeituraMovimento situacaoLeitura) {

		this.situacaoLeitura = situacaoLeitura;
	}

	/**
	 * @return the indicadorConfirmacaoLeitura
	 */
	@Override
	public Boolean getIndicadorConfirmacaoLeitura() {

		return indicadorConfirmacaoLeitura;
	}

	/**
	 * @param indicadorConfirmacaoLeitura
	 *            the indicadorConfirmacaoLeitura
	 *            to set
	 */
	@Override
	public void setIndicadorConfirmacaoLeitura(Boolean indicadorConfirmacaoLeitura) {

		this.indicadorConfirmacaoLeitura = indicadorConfirmacaoLeitura;
	}

	/**
	 * @return the usuario
	 */
	@Override
	public Usuario getUsuario() {

		return usuario;
	}

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	@Override
	public void setUsuario(Usuario usuario) {

		this.usuario = usuario;
	}

	/**
	 * @return the observacaoLeitura
	 */
	@Override
	public String getObservacaoLeitura() {

		return observacaoLeitura;
	}

	/**
	 * @param observacaoLeitura
	 *            the observacaoLeitura to set
	 */
	@Override
	public void setObservacaoLeitura(String observacaoLeitura) {

		this.observacaoLeitura = observacaoLeitura;
	}

	/**
	 * @return the ciclo
	 */
	@Override
	public Integer getCiclo() {

		return ciclo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.LeituraMovimento
	 * #setCiclo(java.lang.Integer)
	 */
	@Override
	public void setCiclo(Integer ciclo) {

		this.ciclo = ciclo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.LeituraMovimento
	 * #getAnoMesFaturamento()
	 */
	@Override
	public Integer getAnoMesFaturamento() {

		return anoMesFaturamento;
	}

	@Override
	public String getAnoMesFaturamentoFormatado() {

		return Util.formatarAnoMes(anoMesFaturamento);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.LeituraMovimento
	 * #setAnoMesFaturamento(java.lang.Integer)
	 */
	@Override
	public void setAnoMesFaturamento(Integer anoMesFaturamento) {

		this.anoMesFaturamento = anoMesFaturamento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.LeituraMovimento
	 * #getDescicaoPontoConsumo()
	 */
	@Override
	public String getDescicaoPontoConsumo() {

		return descicaoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.LeituraMovimento
	 * #setDescicaoPontoConsumo(java.lang.String)
	 */
	@Override
	public void setDescicaoPontoConsumo(String descicaoPontoConsumo) {

		this.descicaoPontoConsumo = descicaoPontoConsumo;
	}

	@Override
	public String getObservacaoPontoConsumo() {
		return observacaoPontoConsumo;
	}

	@Override
	public void setObservacaoPontoConsumo(String observacaoPontoConsumo) {
		this.observacaoPontoConsumo = observacaoPontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public String getDataNovaLeitura() {

		return dataNovaLeitura;
	}

	@Override
	public void setDataNovaLeitura(String dataNovaLeitura) {

		this.dataNovaLeitura = dataNovaLeitura;
	}

	@Override
	public BigDecimal getPressaoInformada() {

		return pressaoInformada;
	}

	@Override
	public void setPressaoInformada(BigDecimal pressaoInformada) {

		this.pressaoInformada = pressaoInformada;
	}

	@Override
	public BigDecimal getTemperaturaInformada() {

		return temperaturaInformada;
	}

	@Override
	public void setTemperaturaInformada(BigDecimal temperaturaInformada) {

		this.temperaturaInformada = temperaturaInformada;
	}

	@Override
	public Unidade getUnidadePressaoInformada() {

		return unidadePressaoInformada;
	}

	@Override
	public void setUnidadePressaoInformada(Unidade unidadePressaoInformada) {

		this.unidadePressaoInformada = unidadePressaoInformada;
	}

	@Override
	public Unidade getUnidadeTemperaturaInformada() {

		return unidadeTemperaturaInformada;
	}

	@Override
	public void setUnidadeTemperaturaInformada(Unidade unidadeTemperaturaInformada) {

		this.unidadeTemperaturaInformada = unidadeTemperaturaInformada;
	}

	@Override
	public BigDecimal getLatitude() {
		return latitude;
	}

	@Override
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	@Override
	public BigDecimal getLongitude() {
		return longitude;
	}

	@Override
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	@Override
	public String getOrigem() {
		return origem;
	}

	@Override
	public void setOrigem(String origem) {
		this.origem = origem;
	}

	@Override
	public Boolean getCorrigePT() {
		return this.corrigePT;
	}

	@Override
	public void setCorrigePT(Boolean corrigePT) {
		this.corrigePT = corrigePT;
	}

	@Override
	public String getFotoColetor() {
		return this.fotoColetor;
	}

	@Override
	public void setFotoColetor(String fotoColetor) {
		this.fotoColetor = fotoColetor;
		
	}

	@Override
	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
		
	}

	@Override
	public String getUrlFoto() {
		return this.urlFoto;
	}


}
