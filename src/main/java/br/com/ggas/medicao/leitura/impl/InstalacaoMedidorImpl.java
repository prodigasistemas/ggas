/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.leitura.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.leitura.MedidorProtecao;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;

/**
 * Classe de Instalação medidor
 *
 */
public class InstalacaoMedidorImpl extends EntidadeNegocioImpl implements InstalacaoMedidor {

	/**
	 * serial
	 */
	private static final long serialVersionUID = -5811972672573028768L;

	private Date data;

	private Date dataAtivacao;

	private Medidor medidor;

	private MedidorLocalInstalacao localInstalacao;

	private PontoConsumo pontoConsumo;

	private MedidorProtecao protecao;

	private BigDecimal leitura;

	private BigDecimal leituraAtivacao;

	private Date dataCorretorVazao;

	private VazaoCorretor vazaoCorretor;
	
	private Funcionario funcionario;

	/**
	 * @return the dataCorretorVazao
	 */
	@Override
	public Date getDataCorretorVazao() {
		Date dataTmp = null;
		if (this.dataCorretorVazao != null) {
			dataTmp = (Date) this.dataCorretorVazao.clone();
		}
		return dataTmp;
	}

	/**
	 * @param dataCorretorVazao
	 *            the dataCorretorVazao to set
	 */
	@Override
	public void setDataCorretorVazao(Date dataCorretorVazao) {
		if(dataCorretorVazao != null){
			this.dataCorretorVazao = (Date) dataCorretorVazao.clone();
		} else {
			this.dataCorretorVazao = null;
		}
	}

	/**
	 * @return the vazaoCorretor
	 */
	@Override
	public VazaoCorretor getVazaoCorretor() {

		return vazaoCorretor;
	}

	/**
	 * @param vazaoCorretor
	 *            the vazaoCorretor to set
	 */
	@Override
	public void setVazaoCorretor(VazaoCorretor vazaoCorretor) {

		this.vazaoCorretor = vazaoCorretor;
	}

	/**
	 * @return the dataAtivacao
	 */
	@Override
	public Date getDataAtivacao() {
		Date dataTmp = null;
		if (this.dataAtivacao != null) {
			dataTmp = (Date) dataAtivacao.clone();
		}
		return dataTmp;
	}

	/**
	 * @param dataAtivacao
	 *            the dataAtivacao to set
	 */
	@Override
	public void setDataAtivacao(Date dataAtivacao) {
		if(dataAtivacao != null) {
			this.dataAtivacao = (Date) dataAtivacao.clone();
		} else {
			this.dataAtivacao = null;
		}
	}

	/**
	 * @return the leituraAtivacao
	 */
	@Override
	public BigDecimal getLeituraAtivacao() {

		return leituraAtivacao;
	}

	/**
	 * @param leituraAtivacao
	 *            the leituraAtivacao to set
	 */
	@Override
	public void setLeituraAtivacao(BigDecimal leituraAtivacao) {

		this.leituraAtivacao = leituraAtivacao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.impl. InstalacaoMedidor#getData()
	 */
	@Override
	public Date getData() {
		Date dataTmp = null;
		if (this.data != null) {
			dataTmp = (Date) this.data.clone();
		}
		return dataTmp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor#setData(java.util.Date)
	 */
	@Override
	public void setData(Date data) {
		if (data != null) {
			this.data = (Date) data.clone();
		} else {
			this.data = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor#getMedidor()
	 */
	@Override
	public Medidor getMedidor() {

		return medidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor
	 * #setMedidor(br.com.ggas.medicao
	 * .medidor.Medidor)
	 */
	@Override
	public void setMedidor(Medidor medidor) {

		this.medidor = medidor;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor#getLocalInstalacao()
	 */
	@Override
	public MedidorLocalInstalacao getLocalInstalacao() {

		return localInstalacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor
	 * #setLocalInstalacao(br.com.
	 * ggas.medicao.leitura
	 * .MedidorLocalInstalacao)
	 */
	@Override
	public void setLocalInstalacao(MedidorLocalInstalacao localInstalacao) {

		this.localInstalacao = localInstalacao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor#getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor
	 * #setPontoConsumo(br.com.ggas
	 * .cadastro.imovel.PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor#getProtecao()
	 */
	@Override
	public MedidorProtecao getProtecao() {

		return protecao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor
	 * #setProtecao(br.com.ggas.medicao
	 * .leitura.MedidorProtecao)
	 */
	@Override
	public void setProtecao(MedidorProtecao protecao) {

		this.protecao = protecao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor#getLeitura()
	 */
	@Override
	public BigDecimal getLeitura() {

		return leitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor
	 * #setLeitura(java.math.BigDecimal)
	 */
	@Override
	public void setLeitura(BigDecimal leitura) {

		this.leitura = leitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor#getFuncionario()
	 */
	@Override
	public Funcionario getFuncionario() {

		return funcionario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * InstalacaoMedidor
	 * #setFuncionario(br.com.ggas
	 * .cadastro.funcionario.Funcionario)
	 */
	@Override
	public void setFuncionario(Funcionario funcionario) {

		this.funcionario = funcionario;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
