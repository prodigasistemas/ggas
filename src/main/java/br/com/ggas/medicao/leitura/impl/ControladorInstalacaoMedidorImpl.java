/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.leitura.impl;

import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.leitura.ControladorInstalacaoMedidor;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.MedidorLocalInstalacao;
import br.com.ggas.medicao.leitura.MedidorProtecao;
import br.com.ggas.util.ServiceLocator;

/**
 * 
 *
 */
class ControladorInstalacaoMedidorImpl extends ControladorNegocioImpl implements ControladorInstalacaoMedidor {

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(InstalacaoMedidor.BEAN_ID_INSTALACAO_MEDIDOR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(InstalacaoMedidor.BEAN_ID_INSTALACAO_MEDIDOR);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * ControladorInstalacaoMedidor
	 * #criarMedidorLocalInstalacao()
	 */
	@Override
	public EntidadeNegocio criarMedidorLocalInstalacao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(MedidorLocalInstalacao.BEAN_ID_MEDIDOR_LOCAL_INSTALACAO);
	}

	public Class<?> getClasseEntidadeMedidorLocalInstalacao() {

		return ServiceLocator.getInstancia().getClassPorID(MedidorLocalInstalacao.BEAN_ID_MEDIDOR_LOCAL_INSTALACAO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.impl.
	 * ControladorInstalacaoMedidor
	 * #criarMedidorProtecao()
	 */
	@Override
	public EntidadeNegocio criarMedidorProtecao() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(MedidorProtecao.BEAN_ID_MEDIDOR_PROTECAO);
	}

	public Class<?> getClasseEntidadeMedidorProtecao() {

		return ServiceLocator.getInstancia().getClassPorID(MedidorProtecao.BEAN_ID_MEDIDOR_PROTECAO);
	}

}
