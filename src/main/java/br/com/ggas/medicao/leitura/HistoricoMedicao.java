/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.leitura;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.vazaocorretor.Unidade;

/**
 * Classe responsável pela assinatura dos métodos relacionados ao Histórico de medição
 * 
 *
 */
public interface HistoricoMedicao extends EntidadeNegocio {

	String BEAN_ID_MEDICAO_HISTORICO = "medicaoHistorico";

	String ANO_MES_LEITURA = "HISTORICO_MEDICAO_ANO_MES_LEITURA";

	String NUMERO_CICLO = "HISTORICO_MEDICAO_NUMERO_CICLO";

	String NUMERO_LEITURA_FATURADA = "HISTORICO_MEDICAO_NUMERO_LEITURA_FATURADA";

	String DATA_LEITURA_FATURADA = "HISTORICO_MEDICAO_DATA_LEITURA_FATURADA";

	String NUMERO_LEITURA_INFORMADA = "HISTORICO_MEDICAO_NUMERO_LEITURA_INFORMADA";

	String DATA_LEITURA_INFORMADA = "HISTORICO_MEDICAO_DATA_LEITURA_INFORMADA";

	String NUMERO_LEITURA_ANTERIOR = "HISTORICO_MEDICAO_NUMERO_LEITURA_ANTERIOR";

	String DATA_LEITURA_ANTERIOR = "HISTORICO_MEDICAO_DATA_LEITURA_ANTERIOR";

	String ANORMALIDADE_LEITURA_FATURADA = "HISTORICO_MEDICAO_ANORMALIDADE_LEITURA_FATURADA";

	String LEITURISTA = "HISTORICO_MEDICAO_LEITURISTA";

	String CONSUMO_INFORMADO = "HISTORICO_MEDICAO_CONSUMO_INFORMADO";

	String CREDITO_GERADO = "HISTORICO_MEDICAO_CREDITO_GERADO";

	String INFORMADO_PELO_CLIENTE = "HISTORICO_MEDICAO_INDICADOR_INFORMADO_PELO_CLIENTE";

	/**
	 * @return the creditoVolume
	 */
	BigDecimal getCreditoVolume();

	/**
	 * @return the creditoGerado
	 */
	BigDecimal getCreditoGerado();

	/**
	 * @param creditoGerado
	 *            the creditoGerado to set
	 */
	void setCreditoGerado(BigDecimal creditoGerado);

	/**
	 * @return the creditoSaldo
	 */
	BigDecimal getCreditoSaldo();

	/**
	 * @param creditoSaldo
	 *            the creditoSaldo to set
	 */
	void setCreditoSaldo(BigDecimal creditoSaldo);

	/**
	 * @return the analisada
	 */
	boolean isAnalisada();

	/**
	 * @param analisada
	 *            the analisada to set
	 */
	void setAnalisada(boolean analisada);

	/**
	 * @return the anoMesLeitura
	 */
	Integer getAnoMesLeitura();

	/**
	 * @return the anoMesLeitura
	 */
	String getAnoMesLeituraFormatado();

	/**
	 * @param anoMesLeitura
	 *            the anoMesLeitura to set
	 */
	void setAnoMesLeitura(Integer anoMesLeitura);

	/**
	 * @return the sequenciaLeitura
	 */
	Integer getSequenciaLeitura();

	/**
	 * @param sequenciaLeitura
	 *            the sequenciaLeitura to set
	 */
	void setSequenciaLeitura(Integer sequenciaLeitura);

	/**
	 * @return the numeroCiclo
	 */
	Integer getNumeroCiclo();

	/**
	 * @param numeroCiclo
	 *            the numeroCiclo to set
	 */
	void setNumeroCiclo(Integer numeroCiclo);

	/**
	 * @return the dataLeituraInformada
	 */
	Date getDataLeituraInformada();

	/**
	 * @param dataLeituraInformada
	 *            the dataLeituraInformada to set
	 */
	void setDataLeituraInformada(Date dataLeituraInformada);

	/**
	 * @return the numeroLeituraInformada
	 */
	BigDecimal getNumeroLeituraInformada();

	/**
	 * @param numeroLeituraInformada
	 *            the numeroLeituraInformada to
	 *            set
	 */
	void setNumeroLeituraInformada(BigDecimal numeroLeituraInformada);

	/**
	 * @return the dataLeituraFaturada
	 */
	Date getDataLeituraFaturada();

	/**
	 * @param dataLeituraFaturada
	 *            the dataLeituraFaturada to set
	 */
	void setDataLeituraFaturada(Date dataLeituraFaturada);

	/**
	 * @return the numeroLeituraFaturada
	 */
	BigDecimal getNumeroLeituraFaturada();

	/**
	 * @param numeroLeituraFaturada
	 *            the numeroLeituraFaturada to set
	 */
	void setNumeroLeituraFaturada(BigDecimal numeroLeituraFaturada);

	/**
	 * @return the leiturista
	 */
	Leiturista getLeiturista();

	/**
	 * @param leiturista
	 *            the leiturista to set
	 */
	void setLeiturista(Leiturista leiturista);

	/**
	 * @return the anormalidadeLeituraInformada
	 */
	AnormalidadeLeitura getAnormalidadeLeituraInformada();

	/**
	 * @param anormalidadeLeituraInformada
	 *            the anormalidadeLeituraInformada
	 *            to set
	 */
	void setAnormalidadeLeituraInformada(AnormalidadeLeitura anormalidadeLeituraInformada);

	/**
	 * @return the anormalidadeLeituraFaturada
	 */
	AnormalidadeLeitura getAnormalidadeLeituraFaturada();

	/**
	 * @param anormalidadeLeituraFaturada
	 *            the anormalidadeLeituraFaturada
	 *            to set
	 */
	void setAnormalidadeLeituraFaturada(AnormalidadeLeitura anormalidadeLeituraFaturada);

	/**
	 * @return the situacaoLeitura
	 */
	SituacaoLeitura getSituacaoLeitura();

	/**
	 * @param situacaoLeitura
	 *            the situacaoLeitura to set
	 */
	void setSituacaoLeitura(SituacaoLeitura situacaoLeitura);

	/**
	 * @return the historicoInstalacaoMedidor
	 */
	InstalacaoMedidor getHistoricoInstalacaoMedidor();

	/**
	 * @param historicoInstalacaoMedidor
	 *            the historicoInstalacaoMedidor
	 *            to set
	 */
	void setHistoricoInstalacaoMedidor(InstalacaoMedidor historicoInstalacaoMedidor);

	/**
	 * @return the dataProcessamento
	 */
	Date getDataProcessamento();

	/**
	 * @param dataProcessamento
	 *            the dataProcessamento to set
	 */
	void setDataProcessamento(Date dataProcessamento);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the consumoInformado
	 */
	BigDecimal getConsumoInformado();

	/**
	 * @param consumoInformado
	 *            the consumoInformado to set
	 */
	void setConsumoInformado(BigDecimal consumoInformado);

	/**
	 * @return the numeroLeituraAnterior
	 */
	BigDecimal getNumeroLeituraAnterior();

	/**
	 * @param numeroLeituraAnterior
	 *            the numeroLeituraAnterior to set
	 */
	void setNumeroLeituraAnterior(BigDecimal numeroLeituraAnterior);

	/**
	 * @return the dataLeituraAnterior
	 */
	Date getDataLeituraAnterior();

	/**
	 * @param dataLeituraAnterior
	 *            the dataLeituraAnterior to set
	 */
	void setDataLeituraAnterior(Date dataLeituraAnterior);

	/**
	 * @return the anormalidadeLeituraAnterior
	 */
	AnormalidadeLeitura getAnormalidadeLeituraAnterior();

	/**
	 * @param anormalidadeLeituraAnterior
	 *            the anormalidadeLeituraAnterior
	 *            to set
	 */
	void setAnormalidadeLeituraAnterior(AnormalidadeLeitura anormalidadeLeituraAnterior);

	/**
	 * @return the numeroLeituraCorretor
	 */
	BigDecimal getNumeroLeituraCorretor();

	/**
	 * @param numeroLeituraCorretor
	 *            the numeroLeituraCorretor to set
	 */
	void setNumeroLeituraCorretor(BigDecimal numeroLeituraCorretor);

	/**
	 * @return the consumoCorretor
	 */
	BigDecimal getConsumoCorretor();

	/**
	 * @param consumoCorretor
	 *            the consumoCorretor to set
	 */
	void setConsumoCorretor(BigDecimal consumoCorretor);

	/**
	 * @return the consumoMedidor
	 */
	BigDecimal getConsumoMedidor();

	/**
	 * @param consumoMedidor
	 *            the consumoMedidor to set
	 */
	void setConsumoMedidor(BigDecimal consumoMedidor);

	/**
	 * @return the fatorPTZCorretor
	 */
	BigDecimal getFatorPTZCorretor();

	/**
	 * @param fatorPTZCorretor
	 *            the fatorPTZCorretor to set
	 */
	void setFatorPTZCorretor(BigDecimal fatorPTZCorretor);

	/**
	 * @return the indicadorInformadoCliente
	 */
	Boolean getIndicadorInformadoCliente();

	/**
	 * @param indicadorInformadoCliente
	 *            the indicadorInformadoCliente to
	 *            set
	 */
	void setIndicadorInformadoCliente(Boolean indicadorInformadoCliente);

	/**
	 * @return AnoMesCicloFormatado
	 *            the AnoMesCicloFormatado to set
	 */
	Integer getAnoMesCicloFormatado();

	/**
	 * @return BigDecimal - Retorna pressão informada.
	 */
	BigDecimal getPressaoInformada();

	/**
	 * @param pressaoInformada - Set pressão informada.
	 */
	void setPressaoInformada(BigDecimal pressaoInformada);

	/**
	 * @return BigDecimal - Retorna temperatura informada.
	 */
	BigDecimal getTemperaturaInformada();

	/**
	 * @param temperaturaInformada - Set temperatura informada.
	 */
	void setTemperaturaInformada(BigDecimal temperaturaInformada);

	/**
	 * @return Unidade - Retorna um objeto unidade com informações sobre Unidade da pressão informada.
	 */
	Unidade getUnidadePressaoInformada();

	/**
	 * @param unidadePressaoInformada - Set Unidade da pressão informada.
	 */
	void setUnidadePressaoInformada(Unidade unidadePressaoInformada);

	/**
	 * @return Unidade - Retorna objeto unidade com informações sobre Unidade da Temperadura informada.
	 */
	Unidade getUnidadeTemperaturaInformada();

	/**
	 * @param unidadeTemperaturaInformada - Set Unidade temperatura informada.
	 */
	void setUnidadeTemperaturaInformada(Unidade unidadeTemperaturaInformada);

	/**
	 * @return MedicaoHistoricoComentario
	 */
	Collection<MedicaoHistoricoComentario> getListaMedicaoHistoricoComentario();

	/**
	 * @param listaMedicaoHistoricoComentario
	 */
	void setListaMedicaoHistoricoComentario(Collection<MedicaoHistoricoComentario> listaMedicaoHistoricoComentario);
	
	boolean getExisteComentario();
}
