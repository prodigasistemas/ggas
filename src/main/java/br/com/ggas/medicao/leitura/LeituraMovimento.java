/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.medicao.leitura;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.TipoLeitura;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.vazaocorretor.Unidade;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Classe responsável pela assiantura dos meodos relacionados aos movimentos de leitura
 *
 */
public interface LeituraMovimento extends EntidadeNegocio {

	String BEAN_ID_LEITURA_MOVIMENTO = "leituraMovimento";

	String REFERENCIA_FATURAMENTO = "REFERENCIA_FATURAMENTO";

	String CONSUMO_MEDIO = "LEITURA_MOVIMENTO_CONSUMO_MEDIO";

	/**
	 * @return the dataGeracao
	 */
	Date getDataGeracao();

	/**
	 * @param dataGeracao
	 *            the dataGeracao to set
	 */
	void setDataGeracao(Date dataGeracao);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the situacaoPontoConsumo
	 */
	SituacaoConsumo getSituacaoPontoConsumo();

	/**
	 * @param situacaoPontoConsumo
	 *            the situacaoPontoConsumo to set
	 */
	void setSituacaoPontoConsumo(SituacaoConsumo situacaoPontoConsumo);

	/**
	 * @return the imovel
	 */
	Imovel getImovel();

	/**
	 * @param imovel
	 *            the imovel to set
	 */
	void setImovel(Imovel imovel);

	/**
	 * @return the perfilImovel
	 */
	String getPerfilImovel();

	/**
	 * @param perfilImovel
	 *            the perfilImovel to set
	 */
	void setPerfilImovel(String perfilImovel);

	/**
	 * @return the segmento
	 */
	String getSegmento();

	/**
	 * @param segmento
	 *            the segmento to set
	 */
	void setSegmento(String segmento);

	/**
	 * @return the rota
	 */
	Rota getRota();

	/**
	 * @param rota
	 *            the rota to set
	 */
	void setRota(Rota rota);

	/**
	 * @return the numeroRota
	 */
	String getNumeroRota();

	/**
	 * @param numeroRota
	 *            the numeroRota to set
	 */
	void setNumeroRota(String numeroRota);

	/**
	 * @return the numeroSetorComercial
	 */
	String getNumeroSetorComercial();

	/**
	 * @param numeroSetorComercial
	 *            the numeroSetorComercial to set
	 */
	void setNumeroSetorComercial(String numeroSetorComercial);

	/**
	 * @return the numeroQuadra
	 */
	String getNumeroQuadra();

	/**
	 * @param numeroQuadra
	 *            the numeroQuadra to set
	 */
	void setNumeroQuadra(String numeroQuadra);

	/**
	 * @return the numeroFaceQuadra
	 */
	Integer getNumeroFaceQuadra();

	/**
	 * @param numeroFaceQuadra
	 *            the numeroFaceQuadra to set
	 */
	void setNumeroFaceQuadra(Integer numeroFaceQuadra);

	/**
	 * @return the numeroLoteImovel
	 */
	Integer getNumeroLoteImovel();

	/**
	 * @param numeroLoteImovel
	 *            the numeroLoteImovel to set
	 */
	void setNumeroLoteImovel(Integer numeroLoteImovel);

	/**
	 * @return the subLoteImovel
	 */
	Integer getSubLoteImovel();

	/**
	 * @param subLoteImovel
	 *            the subLoteImovel to set
	 */
	void setSubLoteImovel(Integer subLoteImovel);

	/**
	 * @return the sequencialLeitura
	 */
	Integer getSequencialLeitura();

	/**
	 * @param sequencialLeitura
	 *            the sequencialLeitura to set
	 */
	void setSequencialLeitura(Integer sequencialLeitura);

	/**
	 * @return the empresa
	 */
	Empresa getEmpresaResponsavel();

	/**
	 * @param empresaResponsavel - Set Empresa resposável.
	 */
	void setEmpresaResponsavel(Empresa empresaResponsavel);

	/**
	 * @return the leiturista
	 */
	Leiturista getLeiturista();

	/**
	 * @param leiturista
	 *            the leiturista to set
	 */
	void setLeiturista(Leiturista leiturista);

	/**
	 * @return the tipoLeitura
	 */
	TipoLeitura getTipoLeitura();

	/**
	 * @param tipoLeitura
	 *            the tipoLeitura to set
	 */
	void setTipoLeitura(TipoLeitura tipoLeitura);

	/**
	 * @return the grupoFaturamento
	 */
	GrupoFaturamento getGrupoFaturamento();

	/**
	 * @param grupoFaturamento
	 *            the grupoFaturamento to set
	 */
	void setGrupoFaturamento(GrupoFaturamento grupoFaturamento);

	/**
	 * @return the nomeCliente
	 */
	String getNomeCliente();

	/**
	 * @param nomeCliente
	 *            the nomeCliente to set
	 */
	void setNomeCliente(String nomeCliente);

	/**
	 * @return the endereco
	 */
	String getEndereco();

	/**
	 * @param endereco
	 *            the endereco to set
	 */
	void setEndereco(String endereco);

	/**
	 * @return the complementoEmdereco
	 */
	String getComplementoEmdereco();

	/**
	 * @param complementoEmdereco
	 *            the complementoEmdereco to set
	 */
	void setComplementoEmdereco(String complementoEmdereco);

	/**
	 * @return the bairro
	 */
	String getBairro();

	/**
	 * @param bairro
	 *            the bairro to set
	 */
	void setBairro(String bairro);

	/**
	 * @return the municipio
	 */
	String getMunicipio();

	/**
	 * @param municipio
	 *            the municipio to set
	 */
	void setMunicipio(String municipio);

	/**
	 * @return the uf
	 */
	String getUf();

	/**
	 * @param uf
	 *            the uf to set
	 */
	void setUf(String uf);

	/**
	 * @return the cep
	 */
	String getCep();

	/**
	 * @param cep
	 *            the cep to set
	 */
	void setCep(String cep);

	/**
	 * @return the nomeContato
	 */
	String getNomeContato();

	/**
	 * @param nomeContato
	 *            the nomeContato to set
	 */
	void setNomeContato(String nomeContato);

	/**
	 * @return the dadosContato
	 */
	String getDadosContato();

	/**
	 * @param dadosContato
	 *            the dadosContato to set
	 */
	void setDadosContato(String dadosContato);

	/**
	 * @return the observacoesCadastrais
	 */
	String getObservacoesCadastrais();

	/**
	 * @param observacoesCadastrais
	 *            the observacoesCadastrais to set
	 */
	void setObservacoesCadastrais(String observacoesCadastrais);

	/**
	 * @return the numeroSerieMedidor
	 */
	String getNumeroSerieMedidor();

	/**
	 * @param numeroSerieMedidor
	 *            the numeroSerieMedidor to set
	 */
	void setNumeroSerieMedidor(String numeroSerieMedidor);

	/**
	 * @return the InstalacaoMedidor
	 */
	InstalacaoMedidor getInstalacaoMedidor();

	/**
	 * @param instalacaoMedidor - Set instalação medidor.
	 */
	void setInstalacaoMedidor(InstalacaoMedidor instalacaoMedidor);

	/**
	 * @return the dataInstalacaoMedidor
	 */
	Date getDataInstalacaoMedidor();

	/**
	 * @param dataInstalacaoMedidor
	 *            the dataInstalacaoMedidor to set
	 */
	void setDataInstalacaoMedidor(Date dataInstalacaoMedidor);

	/**
	 * @return the localInstalacaoMedidor
	 */
	String getLocalInstalacaoMedidor();

	/**
	 * @param localInstalacaoMedidor
	 *            the localInstalacaoMedidor to
	 *            set
	 */
	void setLocalInstalacaoMedidor(String localInstalacaoMedidor);

	/**
	 * @return the dataLeituraAnterior
	 */
	Date getDataLeituraAnterior();

	/**
	 * @param dataLeituraAnterior
	 *            the dataLeituraAnterior to set
	 */
	void setDataLeituraAnterior(Date dataLeituraAnterior);

	/**
	 * @return the dataLeituraPrevista
	 */
	Date getDataLeituraPrevista();

	/**
	 * @param dataLeituraPrevista
	 *            the dataLeituraPrevista to set
	 */
	void setDataLeituraPrevista(Date dataLeituraPrevista);

	/**
	 * @return the dataProximaLeituraPrevista
	 */
	Date getDataProximaLeituraPrevista();

	/**
	 * @param dataProximaLeituraPrevista
	 *            the dataProximaLeituraPrevista
	 *            to set
	 */
	void setDataProximaLeituraPrevista(Date dataProximaLeituraPrevista);

	/**
	 * @return the minimoVariacaoConsumo
	 */
	BigDecimal getMinimoVariacaoConsumo();

	/**
	 * @param minimoVariacaoConsumo
	 *            the minimoVariacaoConsumo to set
	 */
	void setMinimoVariacaoConsumo(BigDecimal minimoVariacaoConsumo);

	/**
	 * @return the maximoVariacaoConsumo
	 */
	BigDecimal getMaximoVariacaoConsumo();

	/**
	 * @param maximoVariacaoConsumo
	 *            the maximoVariacaoConsumo to set
	 */
	void setMaximoVariacaoConsumo(BigDecimal maximoVariacaoConsumo);

	/**
	 * @return the minimoLeituraEsperada
	 */
	BigDecimal getMinimoLeituraEsperada();

	/**
	 * @param minimoLeituraEsperada
	 *            the minimoLeituraEsperada to set
	 */
	void setMinimoLeituraEsperada(BigDecimal minimoLeituraEsperada);

	/**
	 * @return the maximoLeituraEsperada
	 */
	BigDecimal getMaximoLeituraEsperada();

	/**
	 * @param maximoLeituraEsperada
	 *            the maximoLeituraEsperada to set
	 */
	void setMaximoLeituraEsperada(BigDecimal maximoLeituraEsperada);

	/**
	 * @return the leituraAnterior
	 */
	BigDecimal getLeituraAnterior();

	/**
	 * @param leituraAnterior
	 *            the leituraAnterior to set
	 */
	void setLeituraAnterior(BigDecimal leituraAnterior);

	/**
	 * @return the anormalidadeLeituraAnterior
	 */
	AnormalidadeLeitura getAnormalidadeLeituraAnterior();

	/**
	 * @param anormalidadeLeituraAnterior
	 *            the anormalidadeLeituraAnterior
	 *            to set
	 */
	void setAnormalidadeLeituraAnterior(AnormalidadeLeitura anormalidadeLeituraAnterior);

	/**
	 * @return the situacaoLeituraAnterior
	 */
	String getSituacaoLeituraAnterior();

	/**
	 * @param situacaoLeituraAnterior
	 *            the situacaoLeituraAnterior to
	 *            set
	 */
	void setSituacaoLeituraAnterior(String situacaoLeituraAnterior);

	/**
	 * @return the consumoMedio
	 */
	BigDecimal getConsumoMedio();

	/**
	 * @param consumoMedio
	 *            the consumoMedio to set
	 */
	void setConsumoMedio(BigDecimal consumoMedio);

	/**
	 * @return the consumoAnterior
	 */
	BigDecimal getConsumoAnterior();

	/**
	 * @param consumoAnterior
	 *            the consumoAnterior to set
	 */
	void setConsumoAnterior(BigDecimal consumoAnterior);

	/**
	 * @return the numeroColetor
	 */
	Integer getNumeroColetor();

	/**
	 * @param numeroColetor
	 *            the numeroColetor to set
	 */
	void setNumeroColetor(Integer numeroColetor);

	/**
	 * @return the dataLeitura
	 */
	Date getDataLeitura();

	/**
	 * @param dataLeitura
	 *            the dataLeitura to set
	 */
	void setDataLeitura(Date dataLeitura);

	/**
	 * @return the valorLeitura
	 */
	BigDecimal getValorLeitura();

	/**
	 * @param valorLeitura
	 *            the valorLeitura to set
	 */
	void setValorLeitura(BigDecimal valorLeitura);

	/**
	 * @return the codigoAnormalidadeLeitura
	 */
	Long getCodigoAnormalidadeLeitura();

	/**
	 * @param codigoAnormalidadeLeitura
	 *            the codigoAnormalidadeLeitura to
	 *            set
	 */
	void setCodigoAnormalidadeLeitura(Long codigoAnormalidadeLeitura);

	/**
	 * @return the anormalidadeConsumo
	 */
	AnormalidadeConsumo getAnormalidadeConsumo();

	/**
	 * @param anormalidadeConsumo
	 *            the anormalidadeConsumo to set
	 */
	void setAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo);

	/**
	 * @return the situacaoLeitura
	 */
	SituacaoLeituraMovimento getSituacaoLeitura();

	/**
	 * @param situacaoLeitura
	 *            the situacaoLeitura to set
	 */
	void setSituacaoLeitura(SituacaoLeituraMovimento situacaoLeitura);

	/**
	 * @return the indicadorConfirmacaoLeitura
	 */
	Boolean getIndicadorConfirmacaoLeitura();

	/**
	 * @param indicadorConfirmacaoLeitura
	 *            the indicadorConfirmacaoLeitura
	 *            to set
	 */
	void setIndicadorConfirmacaoLeitura(Boolean indicadorConfirmacaoLeitura);

	/**
	 * @return the usuario
	 */
	Usuario getUsuario();

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	void setUsuario(Usuario usuario);

	/**
	 * @return the observacaoLeitura
	 */
	String getObservacaoLeitura();

	/**
	 * @param observacaoLeitura
	 *            the observacaoLeitura to set
	 */
	void setObservacaoLeitura(String observacaoLeitura);

	/**
	 * @return the ciclo
	 */
	Integer getCiclo();

	/**
	 * @param ciclo
	 *            the ciclo to set
	 */
	void setCiclo(Integer ciclo);

	/**
	 * @return the anoMesFaturamento
	 */
	Integer getAnoMesFaturamento();

	/**
	 * @return String - Retorna Ano, mês faturamento formatado.
	 */
	String getAnoMesFaturamentoFormatado();

	/**
	 * @param anoMesFaturamento
	 *            the anoMesFaturamento to set
	 */
	void setAnoMesFaturamento(Integer anoMesFaturamento);

	/**
	 * @return the descicaoPontoConsumo
	 */
	String getDescicaoPontoConsumo();

	/**
	 * @param descicaoPontoConsumo
	 *            the descicaoPontoConsumo to set
	 */
	void setDescicaoPontoConsumo(String descicaoPontoConsumo);

	String getObservacaoPontoConsumo();

	void setObservacaoPontoConsumo(String observacaoPontoConsumo);

	/**
	 * @return String - Retorna nova data de leitura.
	 */
	String getDataNovaLeitura();

	/**
	 * @param dataNovaLeitura - Set Nova data de leitura.
	 */
	void setDataNovaLeitura(String dataNovaLeitura);

	/**
	 * @return BigDecimal - Retorna pressão informada.
	 */
	BigDecimal getPressaoInformada();

	/**
	 * @param pressaoInformada - Set pressão informada.
	 */
	void setPressaoInformada(BigDecimal pressaoInformada);

	/**
	 * @return BigDecimal - Retorna temperatura informada.
	 */
	BigDecimal getTemperaturaInformada();

	/**
	 * @param temperaturaInformada - Set temperatura informada.
	 */
	void setTemperaturaInformada(BigDecimal temperaturaInformada);

	/**
	 * @return Unidade - Retorna um objeto com unidade pressão informada.
	 */
	Unidade getUnidadePressaoInformada();

	/**
	 * @param unidadePressaoInformada - Set Unidade pressão informada.
	 */
	void setUnidadePressaoInformada(Unidade unidadePressaoInformada);

	/**
	 * @return Unidade - Retorna um objeto com informações sobre Unidade temperatura informada.
	 */
	Unidade getUnidadeTemperaturaInformada();

	/**
	 * @param unidadeTemperaturaInformada - Set unidade temperatura informada.
	 */
	void setUnidadeTemperaturaInformada(Unidade unidadeTemperaturaInformada);

	/**
	 * Obtém a origem da leitura
	 * @return retorna a origem da leitura
	 */
	String getOrigem();

	/**
	 * Configura a origem da leitura movimento
	 * @param origem origem da leitura movimento
	 */
	void setOrigem(String origem);

	/**
	 * Retorna se a leitura permite a correção com PT
 	 * @return true se está habilitado para corrigir com o PT e false caso contrário
	 */
	Boolean getCorrigePT();

	/**
	 * Define se a leitura permite a correção com PT
	 * @param corrigePT true se está habilitado para corrigir com o PT e false caso contrário
	 */
	void setCorrigePT(Boolean corrigePT);

	BigDecimal getLatitude();

	void setLatitude(BigDecimal latitude);

	BigDecimal getLongitude();

	void setLongitude(BigDecimal longitude);
	
	String getFotoColetor();

	void setFotoColetor(String fotoColetor);
	
	void setUrlFoto(String urlFoto);
	
	String getUrlFoto();



}
