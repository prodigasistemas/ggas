/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.leitura;

import br.com.ggas.util.MensagemUtil;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;

/**
 * Informa os tipos possíveis da origem da leitura movimento
 * @author jose.victor@logiquesistemas.com.br
 */
public enum OrigemLeituraMovimento {

	NAO_INFORMADO("origemLeituraMovimento.naoInformado"),
	GERAR_DADOS("origemLeituraMovimento.gerarDados"),
	XML("origemLeituraMovimento.xml"),
	API("origemLeituraMovimento.api"),
	DADOS_MEDICAO("origemLeituraMovimento.dadosMedicao"),
	ANALISE_MEDICAO("origemLeituraMovimento.analiseMedicao");

	private String chaveMensagem;

	OrigemLeituraMovimento(String chaveMensagem) {
		this.chaveMensagem = chaveMensagem;
	}

	/**
	 * Obtém a chave da mensagem
	 * @return chave da mensagem
	 */
	public String getChaveMensagem() {
		return chaveMensagem;
	}

	/**
	 * Identifica uma opção de {@link OrigemLeituraMovimento} a partir de uma string.
	 * A identificação deve casar ou com o name o enum correspondente ou com o valor que a chave de mensagem representa
	 * Caso não seja identificado a origem da leitura, é retornado o enum {@link OrigemLeituraMovimento#NAO_INFORMADO},
	 * portanto este método nunca retorna null.
	 *
	 * @param mensagem mensagem a ser investigada
	 * @return retorna  leitura movimento identificada
	 */
	public static OrigemLeituraMovimento identificarOrigem(String mensagem) {
		return Arrays.stream(OrigemLeituraMovimento.values())
				.filter(origem -> StringUtils.equalsIgnoreCase(origem.name(), mensagem)
						|| StringUtils.equalsIgnoreCase(MensagemUtil.obterMensagem(origem.getChaveMensagem()), mensagem))
				.findFirst().orElse(OrigemLeituraMovimento.NAO_INFORMADO);
	}
}
