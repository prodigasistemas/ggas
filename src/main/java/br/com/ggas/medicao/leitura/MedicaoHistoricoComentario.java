/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.leitura;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * Classe Medicao Historico Comentario.
 * 
 *
 */
public interface MedicaoHistoricoComentario extends EntidadeNegocio {

	String BEAN_ID_MEDICAO_HISTORICO_COMENTARIO = "medicaoHistoricoComentario";

	String DESCRICAO = "MEDICAO_HISTORICO_COMENTARIO_DESCRICAO";

	String NUMERO = "MEDICAO_HISTORICO_COMENTARIO_NUMERO";

	String USUARIO = "MEDICAO_HISTORICO_COMENTARIO_USUARIO";

	String HISTORICO_MEDICAO = "MEDICAO_HISTORICO_COMENTARIO_ID_HISTORICO_MEDICAO";

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the numero
	 */
	Integer getNumero();

	/**
	 * @param numero
	 *            the numero to set
	 */
	void setNumero(Integer numero);

	/**
	 * @return the historicoMedicao
	 */
	HistoricoMedicao getHistoricoMedicao();

	/**
	 * @param historicoMedicao
	 *            the historicoMedicao to set
	 */
	void setHistoricoMedicao(HistoricoMedicao historicoMedicao);

	/**
	 * @return the usuario
	 */
	Usuario getUsuario();

	/**
	 * @param usuario
	 *            the usuario to set
	 */
	void setUsuario(Usuario usuario);

}
