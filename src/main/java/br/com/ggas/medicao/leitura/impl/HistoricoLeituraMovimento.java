/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 *
 */

package br.com.ggas.medicao.leitura.impl;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * Classe responsável pela implementação dos métodos relacionados aos históricos de leituras movimento
 */
public class HistoricoLeituraMovimento extends EntidadeNegocioImpl {

	private static final long serialVersionUID = -8190827011085573747L;

	private BigDecimal valorLeitura;
	private BigDecimal pressaoInformada;
	private BigDecimal temperaturaInformada;

	private Long codigoLeituraMovimento;
	private Long codigoAnormalidadeLeitura;
	private AnormalidadeConsumo anormalidadeConsumo;

	private String origem;

	private Date dataLeitura;
	private Date dataGeracao;

	private Usuario usuario;
	private SituacaoLeituraMovimento situacaoLeitura;

	public HistoricoLeituraMovimento() {
	}

	@Override public Map<String, Object> validarDados() {
		return null;
	}

	public BigDecimal getValorLeitura() {
		return valorLeitura;
	}

	public void setValorLeitura(BigDecimal valorLeitura) {
		this.valorLeitura = valorLeitura;
	}

	public BigDecimal getPressaoInformada() {
		return pressaoInformada;
	}

	public void setPressaoInformada(BigDecimal pressaoInformada) {
		this.pressaoInformada = pressaoInformada;
	}

	public BigDecimal getTemperaturaInformada() {
		return temperaturaInformada;
	}

	public void setTemperaturaInformada(BigDecimal temperaturaInformada) {
		this.temperaturaInformada = temperaturaInformada;
	}

	public Long getCodigoLeituraMovimento() {
		return codigoLeituraMovimento;
	}

	public void setCodigoLeituraMovimento(Long codigoLeituraMovimento) {
		this.codigoLeituraMovimento = codigoLeituraMovimento;
	}

	public Long getCodigoAnormalidadeLeitura() {
		return codigoAnormalidadeLeitura;
	}

	public void setCodigoAnormalidadeLeitura(Long codigoAnormalidadeLeitura) {
		this.codigoAnormalidadeLeitura = codigoAnormalidadeLeitura;
	}

	public AnormalidadeConsumo getAnormalidadeConsumo() {
		return anormalidadeConsumo;
	}

	public void setAnormalidadeConsumo(AnormalidadeConsumo anormalidadeConsumo) {
		this.anormalidadeConsumo = anormalidadeConsumo;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public Date getDataLeitura() {
		return dataLeitura;
	}

	public void setDataLeitura(Date dataLeitura) {
		this.dataLeitura = dataLeitura;
	}

	public Date getDataGeracao() {
		return dataGeracao;
	}

	public void setDataGeracao(Date dataGeracao) {
		this.dataGeracao = dataGeracao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public SituacaoLeituraMovimento getSituacaoLeitura() {
		return situacaoLeitura;
	}

	public void setSituacaoLeitura(SituacaoLeituraMovimento situacaoLeitura) {
		this.situacaoLeitura = situacaoLeitura;
	}
}
