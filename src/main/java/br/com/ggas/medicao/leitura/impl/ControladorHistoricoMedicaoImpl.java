/*

 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.leitura.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.joda.time.DateTime;

import com.google.common.base.Optional;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.fatura.impl.DadosResumoPontoConsumoImpl;
import br.com.ggas.faturamento.tarifa.TarifaPontoConsumo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.MedicaoHistoricoComentario;
import br.com.ggas.medicao.leitura.SituacaoLeitura;
import br.com.ggas.medicao.leitura.TipoMedicao;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.medicao.medidor.HistoricoOperacaoMedidor;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.medidor.impl.FiltroHistoricoOperacaoMedidor;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.HibernateHqlUtil;
import br.com.ggas.util.Ordenacao;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import br.com.ggas.web.medicao.leitura.HistoricoLeituraGSAVO;

/**
 * Controlador Historico Medicao Impl
 * 
 */
class ControladorHistoricoMedicaoImpl extends ControladorNegocioImpl implements ControladorHistoricoMedicao {

	private static final String HISTORICO = " historico ";
	private static final String FROM = " from ";
	private static final String WHERE = " where ";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(HistoricoMedicao.BEAN_ID_MEDICAO_HISTORICO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.
	 * ControladorNegocioImpl#getClasseEntidade()
	 */
	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoMedicao.BEAN_ID_MEDICAO_HISTORICO);
	}

	public Class<?> getClasseEntidadeMedicaoHistoricoComentario() {

		return ServiceLocator.getInstancia().getClassPorID(MedicaoHistoricoComentario.BEAN_ID_MEDICAO_HISTORICO_COMENTARIO);
	}

	public Class<?> getClasseEntidadeHistoricoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(HistoricoConsumo.BEAN_ID_HISTORICO_CONSUMO);
	}

	public Class<?> getClasseEntidadeContratoPontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(ContratoPontoConsumo.BEAN_ID_CONTRATO_PONTO_CONSUMO);
	}

	public Class<?> getClasseEntidadePontoConsumo() {

		return ServiceLocator.getInstancia().getClassPorID(PontoConsumo.BEAN_ID_PONTO_CONSUMO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #obterHistoricoMedicaoPorInstalacaoMedidor
	 * (java.lang.Long)
	 */
	@Override
	public HistoricoMedicao obterHistoricoMedicaoPorInstalacaoMedidor(Long chaveInstalacaoMedidor) {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico where ");
		hql.append(" historico.historicoInstalacaoMedidor.chavePrimaria = :chaveInstalacaoMedidor");
		hql.append(" and historico.habilitado = true");
		hql.append(" and historico.dataLeituraFaturada = (select MAX(h.dataLeituraFaturada) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" h where ");
		hql.append(" h.historicoInstalacaoMedidor.chavePrimaria = :chaveInstalacaoMedidor)");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameter("chaveInstalacaoMedidor", chaveInstalacaoMedidor);

		return (HistoricoMedicao) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #consultarHistoricoMedicao(java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> consultarHistoricoMedicao(Long chavePrimariaPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico where ");
		hql.append(" historico.pontoConsumo.chavePrimaria = ?");
		hql.append(" order by historico.anoMesLeitura desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaPontoConsumo);

		return query.list();
	}

	/**
	 * Obtém uma coleção de históricos de medição por chave primária do ponto
	 * de consumo. (Medição não consistidas)
	 * @param chavesPontoConsumo
	 * @return mapa de históricos de medição por ponto de consumo
	 */
	@Override

	public Map<PontoConsumo, Collection<HistoricoMedicao>> consultarHistoricoMedicaoAtual(Long[] chavesPontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo.chavePrimaria, historico from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join historico.pontoConsumo pontoConsumo ");
		hql.append("  where ");
		hql.append("  historico.habilitado = true and ");
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontoConsumo));
		hql.append(" and concat(str(historico.anoMesLeitura),str(historico.numeroCiclo)) >= ");
		hql.append(" ( select max(concat(str(historicoConsumo.anoMesFaturamento),str(historicoConsumo.numeroCiclo))) from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" historicoConsumo ");
		hql.append(" where ");
		Map<String, List<Long>> mapaPropriedades1 = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "historicoConsumo.pontoConsumo.chavePrimaria", "PT_CONS1", Arrays.asList(chavesPontoConsumo));
		hql.append(" )");
		hql.append(" order by historico.anoMesLeitura desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades1);
		
		return this.consultarHistoricoMedicao(query.list()) ;
	}
	
	/**
	 * Obtém uma coleção de históricos de medição por chave primária do ponto
	 * de consumo.
	 * @param chavesPontoConsumo
	 * @return mapa de históricos de medição por ponto de consumo
	 */
	@Override
	public Map<PontoConsumo, Collection<HistoricoMedicao>> consultarHistoricoMedicao(Long[] chavesPontoConsumo) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo.chavePrimaria, historico ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join historico.pontoConsumo pontoConsumo ");
		hql.append("  where ");
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontoConsumo));
		hql.append(" order by historico.anoMesLeitura desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		
		return this.consultarHistoricoMedicao( query.list() ) ;
	}

	/**
	 * Contrói um mapa de coleção de histórico de medição por ponto de consumo a partir
	 * dos atributos passados por parâmetro.
	 * @param listaAtributos
	 * @return mapa de históricos de medição por ponto de consumo
	 */
	private Map<PontoConsumo, Collection<HistoricoMedicao>> consultarHistoricoMedicao(List<Object[]> listaAtributos) {
		Map<PontoConsumo, Collection<HistoricoMedicao>> mapa = new HashMap<>();
		Collection<HistoricoMedicao> listaHistoricoMedicao;
		for ( Object[] atributos : listaAtributos ) {
			PontoConsumo pontoConsumo = new PontoConsumoImpl();
			pontoConsumo.setChavePrimaria((long) atributos[0]);
			HistoricoMedicao historicoMedicao = (HistoricoMedicao) atributos[1];
			if ( !mapa.containsKey(pontoConsumo) ) {
				listaHistoricoMedicao = new ArrayList<>();
				mapa.put(pontoConsumo, listaHistoricoMedicao);
			} else {
				listaHistoricoMedicao = mapa.get(pontoConsumo);
			}
			listaHistoricoMedicao.add(historicoMedicao);

		}
		return mapa;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #consultarHistoricoAgrupadoMedicao
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> consultarHistoricoAgrupadoMedicao(Long chavePrimariaPontoConsumo, 
			String referenciaInicial, String referenciaFinal) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico ");
		hql.append(" inner join fetch historico.pontoConsumo as pontoConsumo ");
		hql.append(" left join fetch pontoConsumo.rota as rota ");
		hql.append(WHERE);
		hql.append(" historico.pontoConsumo.chavePrimaria = ?");
		
		hql.append(" and historico.chavePrimaria = (select MAX(h.chavePrimaria) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" h where ");
		hql.append(" h.pontoConsumo.chavePrimaria = historico.pontoConsumo.chavePrimaria ");
		hql.append(" and h.anoMesLeitura = historico.anoMesLeitura ");
		hql.append(" and h.numeroCiclo = historico.numeroCiclo )");

		if ((referenciaInicial == null && referenciaFinal != null) || (referenciaInicial != null && referenciaFinal == null)) {
			throw new NegocioException(ControladorHistoricoMedicao.ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA, false);
		} else if (referenciaInicial != null) {
			hql.append(" and historico.anoMesLeitura between :referenciaInicial and :referenciaFinal ");
		}

		hql.append(" order by historico.anoMesLeitura desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaPontoConsumo);

		if ((referenciaInicial != null && StringUtils.isNotEmpty(referenciaInicial)) && 
					(referenciaFinal != null && StringUtils.isNotEmpty(referenciaFinal))) {
			query.setInteger("referenciaInicial", Integer.parseInt(referenciaInicial));
			query.setInteger("referenciaFinal", Integer.parseInt(referenciaFinal));
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #consultarHistoricoAgrupadoMedicao
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> consultarHistoricoAgrupadoMedicao(Long chavePrimariaPontoConsumo, String referenciaInicial,
					String referenciaFinal, Integer quantidadeCiclos) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join fetch historico.pontoConsumo as pontoConsumo ");
		hql.append(" left join fetch pontoConsumo.rota as rota ");
		hql.append(WHERE);
		hql.append(" pontoConsumo.chavePrimaria = ?");
		hql.append(" and historico.chavePrimaria = (select MAX(h.chavePrimaria) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" h where ");
		hql.append(" h.pontoConsumo.chavePrimaria = historico.pontoConsumo.chavePrimaria ");
		hql.append(" and h.anoMesLeitura = historico.anoMesLeitura ");
		hql.append(" and h.numeroCiclo = historico.numeroCiclo )");

		if ((referenciaInicial == null && referenciaFinal != null) || (referenciaInicial != null && referenciaFinal == null)) {
			throw new NegocioException(ControladorHistoricoMedicao.ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA, false);
		} else if (referenciaInicial != null) {
			hql.append(" and historico.anoMesLeitura between :referenciaInicial and :referenciaFinal ");
		}

		hql.append(" order by historico.anoMesLeitura desc, historico.numeroCiclo desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaPontoConsumo);

		if ((referenciaInicial != null && StringUtils.isNotEmpty(referenciaInicial)) && referenciaFinal != null && StringUtils
				.isNotEmpty(referenciaFinal)) {
			query.setInteger("referenciaInicial", Integer.parseInt(referenciaInicial));
			query.setInteger("referenciaFinal", Integer.parseInt(referenciaFinal));
		}

		if (quantidadeCiclos != null && quantidadeCiclos > 0) {
			query.setMaxResults(quantidadeCiclos + 1);
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #validarPontoConsumoSemHistoricoMedicao
	 * (java.util.Collection)
	 */
	@Override
	public void validarPontoConsumoSemHistoricoMedicao(Collection<HistoricoMedicao> listaHistoricoMedicao) throws NegocioException {

		if (listaHistoricoMedicao == null || listaHistoricoMedicao.isEmpty()) {
			throw new NegocioException(ERRO_NEGOCIO_PONTO_DE_CONSUMO_SEM_HISTORICO_MEDICAO, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #obterUltimoHistoricoMedicao
	 * (java.lang.Long, int, int)
	 */
	@Override
	public HistoricoMedicao obterUltimoHistoricoMedicao(Long chavePrimariaPontoConsumo,
					int referencia, int ciclo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo ");
		hql.append(WHERE);
		hql.append(" historico.pontoConsumo.chavePrimaria = ?");
		hql.append(" and historico.anoMesLeitura = ?");
		hql.append(" and historico.habilitado = true");

		if (ciclo != 0) {
			hql.append(" and historico.numeroCiclo = ?");
		}
		hql.append(" order by historico.dataLeituraFaturada desc ");

		Query query = getHibernateTemplate().getSessionFactory()
						.getCurrentSession().createQuery(hql.toString());

		query.setLong(0, chavePrimariaPontoConsumo);
		query.setLong(1, referencia);
		// Caso o ciclo seja 0, retorna o ciclo mais recente da referencia
		if (ciclo != 0) {
			query.setLong(2, ciclo);
		}
		query.setMaxResults(1);
		return (HistoricoMedicao) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #obterUltimoHistoricoMedicao
	 * (java.lang.Long)
	 */
	@Override
	public HistoricoMedicao obterUltimoHistoricoMedicao(Long chavePrimariaPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo ");
		hql.append(WHERE);
		hql.append(" historico.pontoConsumo.chavePrimaria = ?");
		hql.append(" order by historico.dataLeituraFaturada desc, historico.numeroCiclo desc, historico.dataLeituraInformada desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaPontoConsumo);
		query.setMaxResults(1);

		return (HistoricoMedicao) query.uniqueResult();

	}

	/**
	 * Obtém a data de leitura do histórico de medição.
	 *
	 * @param pontosDeConsumo - {@link Collection}
	 * @return históricos por chave de pontos de consumo - {@link Map}
	 */
	public Map<Long, Collection<HistoricoMedicao>> obterDataLeituraDeUltimoHistoricoMedicao(Collection<PontoConsumo> pontosDeConsumo) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select pontoConsumo.chavePrimaria as pontoConsumo_chavePrimaria, ");
		hql.append(" historico.chavePrimaria as chavePrimaria, ");
		hql.append(" historico.dataLeituraInformada as dataLeituraInformada ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join historico.pontoConsumo pontoConsumo ");
		hql.append(WHERE);
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Util.recuperarChavesPrimarias(pontosDeConsumo));
		hql.append(" order by historico.dataLeituraFaturada desc, ");
		hql.append(" historico.numeroCiclo desc, historico.dataLeituraInformada desc ");

		Query query = getSession().createQuery(hql.toString());

		query.setResultTransformer(new GGASTransformer(getClasseEntidade(), getSessionFactory().getAllClassMetadata(),
				"pontoConsumo_chavePrimaria"));
		
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

		return (Map<Long, Collection<HistoricoMedicao>>) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #obterUltimoHistoricoMedicaoFaturado
	 * (java.lang.Long)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public HistoricoMedicao obterUltimoHistoricoMedicao(Long chavePrimariaPontoConsumo, Integer referencia, Integer ciclo,
					Boolean isFaturado) throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo ");
		hql.append(WHERE);
		hql.append(" historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" and historico.habilitado = true ");


		if (referencia != null && referencia > 0) {
			hql.append(" and historico.anoMesLeitura = :referencia ");
		}

		if (ciclo != null && ciclo > 0) {
			hql.append(" and historico.numeroCiclo = :ciclo ");
		}

		// Verificação de faturamento
		if (isFaturado != null) {

			if (isFaturado) {
				hql.append(" and historico.dataLeituraFaturada is not null ");
			}

			hql.append(" and historico.chavePrimaria ");
			if (!isFaturado) {
				hql.append(" not ");
			}
			hql.append(" in ( select historicoConsumo.historicoAtual.chavePrimaria from ");
			hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
			hql.append("	historicoConsumo ");
			hql.append(" 	where ");
			hql.append(" 	historicoConsumo.habilitado = true ");
			hql.append(" 	and historicoConsumo.indicadorFaturamento = true ");
			if (referencia != null && referencia > 0) {
				hql.append(" 	and historico.anoMesLeitura = :referencia ");
			}
			if (ciclo != null && ciclo > 0) {
				hql.append(" 	and historico.numeroCiclo = :ciclo ");
			}
			hql.append(" 	and historicoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo) ");
		}

		// Ordenação
		if (isFaturado != null && isFaturado) {
			hql.append(" order by historico.dataLeituraFaturada desc ");
		} else {
			hql.append(" order by historico.dataLeituraInformada desc ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", chavePrimariaPontoConsumo);

		if (referencia != null && referencia > 0) {
			query.setLong("referencia", referencia);
		}

		if (ciclo != null && ciclo > 0) {
			query.setLong("ciclo", ciclo);
		}

		HistoricoMedicao retorno = null;

		query.setMaxResults(1);
		Collection<HistoricoMedicao> historicos = query.list();
		if (!historicos.isEmpty()) {
			retorno = historicos.iterator().next();
		}

		return retorno;
	}


	/**
	 *
	 * Obtém o último histórico de medição faturado, dos pontos de consumo
	 * especificados no parâmetro.
	 *
	 * @param chavesPontoConsumo
	 * @return mapa de histórico de medição por ponto de consumo
	 */
	@Override
	public Map<PontoConsumo, HistoricoMedicao> obterUltimoHistoricoMedicaoFaturado(Long[] chavesPontoConsumo) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select pontoConsumo.chavePrimaria, historico ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join historico.pontoConsumo as pontoConsumo ");
		hql.append(WHERE);
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontoConsumo));
		hql.append(" and historico.dataLeituraFaturada is not null ");
		hql.append(" and historico.chavePrimaria ");
		hql.append(" in ( select historicoConsumo.historicoAtual.chavePrimaria from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append("	historicoConsumo ");
		hql.append("    inner join historicoConsumo.pontoConsumo as pc ");
		hql.append(" 	where ");
		hql.append(" 	historicoConsumo.habilitado = true ");
		hql.append(" 	and historicoConsumo.indicadorFaturamento = true ");
		hql.append("    and(concat(str(historicoConsumo.anoMesFaturamento),str(historicoConsumo.numeroCiclo))) = ");
		hql.append("    (select max(concat(str(his.anoMesFaturamento),str(his.numeroCiclo))) from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append(" his where his.habilitado = true and ");
		Map<String, List<Long>> mapaPropriedades1 = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "his.pontoConsumo.chavePrimaria", "PT_CONS1", Arrays.asList(chavesPontoConsumo));
		hql.append(" ))");
		hql.append(" order by historico.dataLeituraFaturada desc ");
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades1);
		List<Object[]> objetos = query.list();

		return this.construirMapaUltimoHistoricoMedicao(objetos);
	}
	
	/**
	 * Obtém do último histórico de medição faturado, os atributos: {@code dataLeituraFaturada},
	 * {@code numeroLeituraFaturada}, {@code anormalidadeLeituraFaturada}, {@code dataLeituraInformada},
	 * {@code numeroLeituraInformada}, {@code chavePrimaria}, {@code situacaoLeitura}.
	 *
	 * @param chavesPontoConsumo
	 * @return mapa de histórico de medição por ponto de consumo
	 */
	@Override
	public Map<Long, Collection<HistoricoMedicao>> obterAtributosDoUltimoHistoricoMedicaoFaturado(Long[] chavesPontoConsumo) {

		StringBuilder hql = new StringBuilder();

		hql.append(" select pontoConsumo.chavePrimaria as pontoConsumo_chavePrimaria, ");
		hql.append(" historico.dataLeituraFaturada as dataLeituraFaturada, ");
		hql.append(" historico.numeroLeituraFaturada as numeroLeituraFaturada, ");
		hql.append(" anormalidadeLeituraFaturada as anormalidadeLeituraFaturada, ");
		hql.append(" historico.dataLeituraInformada as dataLeituraInformada, ");
		hql.append(" historico.numeroLeituraInformada as numeroLeituraInformada, ");
		hql.append(" historico.chavePrimaria as chavePrimaria, ");
		hql.append(" situacaoLeitura as situacaoLeitura ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join historico.pontoConsumo as pontoConsumo ");
		hql.append(" left join historico.situacaoLeitura situacaoLeitura ");
		hql.append(" left join historico.anormalidadeLeituraFaturada anormalidadeLeituraFaturada ");
		hql.append(WHERE);
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontoConsumo));
		hql.append(" and historico.dataLeituraFaturada is not null ");
		hql.append(" and historico.chavePrimaria ");
		hql.append(" in ( select historicoConsumo.historicoAtual.chavePrimaria from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append("	historicoConsumo ");
		hql.append("    inner join historicoConsumo.pontoConsumo as pc ");
		hql.append(" 	where ");
		hql.append(" 	historicoConsumo.habilitado = true ");
		hql.append(" 	and historicoConsumo.indicadorFaturamento = true ");
		hql.append(" 	and ");
		HibernateHqlUtil.adicionarClausulaIn(hql, "pc.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontoConsumo));
		hql.append(" ) ");
		
		hql.append(" order by historico.dataLeituraFaturada desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setResultTransformer(new GGASTransformer(getClasseEntidade(),
				super.getSessionFactory().getAllClassMetadata(), "pontoConsumo_chavePrimaria"));

		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);

		return (Map<Long, Collection<HistoricoMedicao>>) query.uniqueResult();
	}

	/**
	 * Constrói um mapa de histórico de medição por ponto de consumo a partir
	 * da lista de atributos passada por parâmetro.
	 * @param listaAtributos
	 * @return mapa de histórico de medição por ponto de consumo
	 */
	private Map<PontoConsumo, HistoricoMedicao> construirMapaUltimoHistoricoMedicao(List<Object[]> listaAtributos) {
		Map<PontoConsumo, HistoricoMedicao> mapa = new HashMap<>();
		for ( Object[] atributos : listaAtributos ) {
			PontoConsumo pontoConsumo = new PontoConsumoImpl();
			pontoConsumo.setChavePrimaria((long) atributos[0]);
			HistoricoMedicao historicoConsumo = (HistoricoMedicao) atributos[1];
			if ( !mapa.containsKey(pontoConsumo) ) {
				mapa.put(pontoConsumo, historicoConsumo);
			}
		}
		return mapa;
	}



	@Override
	public HistoricoMedicao obterUltimoHistoricoMedicao(Medidor medidor, Integer referencia, Integer ciclo, Boolean isFaturado)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();

		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join fetch historico.historicoInstalacaoMedidor historicoInstalacaoMedidor ");
		hql.append(WHERE);
		hql.append(" historico.historicoInstalacaoMedidor.chavePrimaria = :idInstalacaoMedidor ");
		hql.append(" and historico.habilitado = 1 ");


		if (referencia != null && referencia > 0) {
			hql.append(" and historico.anoMesLeitura = :referencia ");
		}

		if (ciclo != null && ciclo > 0) {
			hql.append(" and historico.numeroCiclo = :ciclo ");
		}

		// Verificação de faturamento
		if (isFaturado != null) {
			hql.append(" and historico.chavePrimaria ");
			if (!isFaturado) {
				hql.append(" not ");
			}
			hql.append(" in ( select historicoConsumo.historicoAtual.chavePrimaria from ");
			hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
			hql.append("	historicoConsumo ");
			hql.append(" 	where ");
			hql.append(" 	historicoConsumo.habilitado = true ");
			hql.append(" 	and historicoConsumo.indicadorFaturamento = true ");
			if (referencia != null && referencia > 0) {
				hql.append(" 	and historico.anoMesLeitura = :referencia ");
			}
			if (ciclo != null && ciclo > 0) {
				hql.append(" 	and historico.numeroCiclo = :ciclo ");
			}
			hql.append(" ) ");
		}

		// Ordenação
		if (isFaturado != null && isFaturado) {
			hql.append(" order by historico.dataLeituraFaturada desc ");
		} else {
			hql.append(" order by historico.dataLeituraInformada desc ");
		}

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idInstalacaoMedidor", medidor.getInstalacaoMedidor().getChavePrimaria());

		if (referencia != null && referencia > 0) {
			query.setLong("referencia", referencia);
		}

		if (ciclo != null && ciclo > 0) {
			query.setLong("ciclo", ciclo);
		}

		HistoricoMedicao retorno = null;

		query.setMaxResults(1);
		Collection<HistoricoMedicao> historicos = query.list();
		if (!historicos.isEmpty()) {
			retorno = historicos.iterator().next();
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #obterPrimeiroHistoricoMedicao
	 * (java.lang.Long)
	 */
	@Override
	public HistoricoMedicao obterPrimeiroHistoricoMedicao(Long chavePrimariaPontoConsumo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join fetch historico.pontoConsumo pontoConsumo ");
		hql.append(WHERE);
		hql.append(" historico.pontoConsumo.chavePrimaria = ?");

		hql.append(" order by historico.anoMesLeitura desc, historico.numeroCiclo desc, historico.dataLeituraInformada asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaPontoConsumo);
		query.setMaxResults(1);

		return (HistoricoMedicao) query.uniqueResult();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #consultarHistoricoMedicao(java.lang.Long,
	 * int, int)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> consultarHistoricoMedicao(Long chavePrimariaPontoConsumo, int referencia, int ciclo)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico where ");
		hql.append(" historico.pontoConsumo.chavePrimaria = ?");
		hql.append(" and historico.anoMesLeitura = ?");
		hql.append(" and historico.numeroCiclo = ?");
		hql.append(" order by dataLeituraInformada asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, chavePrimariaPontoConsumo);
		query.setLong(1, referencia);
		query.setLong(2, ciclo);

		return query.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> consultarHistoricoMedicaoPorMedidor(Medidor medidor, int referencia, int ciclo)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico where ");
		hql.append(" historico.historicoInstalacaoMedidor.chavePrimaria = ?");
		hql.append(" and historico.anoMesLeitura = ?");
		hql.append(" and historico.numeroCiclo = ?");
		hql.append(" order by dataLeituraInformada asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(0, medidor.getInstalacaoMedidor().getChavePrimaria());
		query.setLong(1, referencia);
		query.setLong(2, ciclo);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.ControladorHistoricoMedicao#consultarHistoricoMedicaoNaoFaturado(java.lang.Long, int, int)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> consultarHistoricoMedicaoNaoFaturado(Long chavePrimariaPontoConsumo, int referencia, int ciclo)
					throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico where ");
		hql.append(" historico.pontoConsumo.chavePrimaria = :idPontoConsumo");
		hql.append(" and historico.anoMesLeitura = :referencia");
		hql.append(" and historico.numeroCiclo = :ciclo");
		hql.append(" and historico.habilitado = true");

		// Medições que não possuem consumo
		// faturado
		hql.append(" and historico.chavePrimaria not in ( ");
		hql.append(" 	select historicoConsumo.historicoAtual.chavePrimaria from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append("	historicoConsumo ");
		hql.append(" 	where ");
		hql.append(" 	historicoConsumo.habilitado = true ");
		hql.append(" 	and historicoConsumo.indicadorFaturamento = true ");
		hql.append(" 	and historicoConsumo.anoMesFaturamento = :referencia");
		hql.append(" 	and historicoConsumo.numeroCiclo = :ciclo");
		hql.append(" 	and historicoConsumo.pontoConsumo.chavePrimaria = :idPontoConsumo) ");

		hql.append(" order by dataLeituraInformada asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idPontoConsumo", chavePrimariaPontoConsumo);
		query.setLong("referencia", referencia);
		query.setLong("ciclo", ciclo);

		return query.list();
	}
	/**
	 * Obtém os históricos de medição não faturados por ponto de consumo.
	 * @param chavesPontoConsumo
	 * @param referencia
	 * @param ciclo
	 * @return mapa de históricos de medição por ponto de consumo
	 */
	@Override
	public Map<PontoConsumo, Collection<HistoricoMedicao>> consultarHistoricoMedicaoNaoFaturado(Long[] chavesPontoConsumo, int referencia,
					int ciclo) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select pontoConsumo.chavePrimaria, historico ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" inner join historico.pontoConsumo pontoConsumo ");
		hql.append(" inner join historico.historicoInstalacaoMedidor historicoInstalacaoMedidor ");
		hql.append(WHERE);
		Map<String, List<Long>> mapaPropriedades = 
				HibernateHqlUtil.adicionarClausulaIn(hql, "pontoConsumo.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontoConsumo));
		hql.append(" and historico.anoMesLeitura = :referencia");
		hql.append(" and historico.numeroCiclo = :ciclo");
		hql.append(" and historico.habilitado = true");
		hql.append(" and historico.chavePrimaria not in ( ");
		hql.append(" 	select historicoConsumo.historicoAtual.chavePrimaria from ");
		hql.append(getClasseEntidadeHistoricoConsumo().getSimpleName());
		hql.append("	historicoConsumo ");
		hql.append("    inner join historicoConsumo.pontoConsumo pc ");
		hql.append(" 	where ");
		hql.append(" 	historicoConsumo.habilitado = true  ");
		hql.append(" 	and historicoConsumo.indicadorFaturamento = true ");
		hql.append(" 	and historicoConsumo.anoMesFaturamento = :referencia");
		hql.append(" 	and historicoConsumo.numeroCiclo = :ciclo");
		hql.append(" 	and ");
		HibernateHqlUtil.adicionarClausulaIn(hql, "pc.chavePrimaria", "PT_CONS", Arrays.asList(chavesPontoConsumo));
		hql.append(" ) ");

		hql.append(" order by historico.dataLeituraInformada asc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		HibernateHqlUtil.adicionarPropriedadesClausulaIn(query, mapaPropriedades);
		query.setLong("referencia", referencia);
		query.setLong("ciclo", ciclo);


		return this.construirMapaHistoricoConsumoPorPontoConsumo( query.list() );
	}

	/**
	 * Constrói um mapa de históricos de medição por ponto de consumo
	 * a partir da lista de atributos passada por parâmetro.
	 * 
	 * @param listaAtributos
	 * 					the listaAtributos
	 * @return mapa de históricos de medição por ponto de consumo
	 */
	public Map<PontoConsumo, Collection<HistoricoMedicao>> construirMapaHistoricoConsumoPorPontoConsumo(List<Object[]> listaAtributos) {
		Map<PontoConsumo, Collection<HistoricoMedicao>> mapa = new HashMap<>();
		Collection<HistoricoMedicao> medicoes;
		for( Object[] atributos : listaAtributos ) {
			PontoConsumo pontoConsumo = new PontoConsumoImpl();
			pontoConsumo.setChavePrimaria((long) atributos[0]);
			HistoricoMedicao historicoMedicao = (HistoricoMedicao) atributos[1];
			if ( !mapa.containsKey(pontoConsumo) ) {
				medicoes = new ArrayList<>();
				medicoes.add(historicoMedicao);
				mapa.put(pontoConsumo, medicoes);
			} else {
				medicoes = mapa.get(pontoConsumo);
				medicoes.add(historicoMedicao);
			}
		}
		return mapa;
	}



	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #atualizarDadosFaturamentoLeitura
	 * (br.com.ggas.cadastro.imovel.PontoConsumo,
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * ,
	 * br.com.ggas.cadastro.imovel.impl.
	 * ComentarioPontoConsumo)
	 */
	@Override
	public void atualizarDadosFaturamentoLeitura(PontoConsumo pontoConsumo, HistoricoMedicao historicoMedicao,
					MedicaoHistoricoComentario medicaoHistoricoComentario, boolean executarConsistir) throws GGASException {

		this.validarDadosAtualizarDadosFaturamentoLeitura(historicoMedicao);
		ControladorLeituraMovimento controladorLeituraMovimento = ServiceLocator.getInstancia().getControladorLeituraMovimento();

		historicoMedicao.setAnalisada(true);

		controladorLeituraMovimento = (ControladorLeituraMovimento) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO);
		SituacaoLeitura situacaoLeituraRealizada = controladorLeituraMovimento.obterSituacaoLeitura(SituacaoLeitura.REALIZADA);

		historicoMedicao.setSituacaoLeitura(situacaoLeituraRealizada);

		
		if(medicaoHistoricoComentario != null) {
			this.inserirMedicaoHistoricoComentario(medicaoHistoricoComentario, historicoMedicao);
		}

		Boolean isAnormalidadeLeituraNaoFaturar = subFluxoPontoNaoDeveSerFaturado(historicoMedicao);
		
		

		if (executarConsistir && !isAnormalidadeLeituraNaoFaturar) {
			Collection<PontoConsumo> pontosConsumo = new ArrayList<PontoConsumo>();
			pontosConsumo.add(pontoConsumo);
			ControladorHistoricoConsumo controladorHistoricoConsumo = (ControladorHistoricoConsumo) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO);
			
			ControladorRota controladorRota = (ControladorRota) ServiceLocator.getInstancia().getControladorNegocio(
					ControladorRota.BEAN_ID_CONTROLADOR_ROTA);
			
			StringBuilder logProcessamento = new StringBuilder();
			controladorHistoricoConsumo.consistirLeituraMedicao(pontosConsumo, historicoMedicao.getAnoMesLeitura(),
							historicoMedicao.getNumeroCiclo(), logProcessamento, false, historicoMedicao);
			controladorRota.atualizarCronogamaPorRota(pontoConsumo.getRota(), AtividadeSistema.CODIGO_ATIVIDADE_CONSISTIR_LEITURA);
		}
	}

	private Boolean subFluxoPontoNaoDeveSerFaturado(HistoricoMedicao historicoMedicao) throws ConcorrenciaException, NegocioException {
		ControladorConstanteSistema controladorConstanteSistema = ServiceLocator.getInstancia().getControladorConstanteSistema();
		ControladorLeituraMovimento controladorLeituraMovimento = ServiceLocator.getInstancia().getControladorLeituraMovimento();
		ControladorHistoricoConsumo controladorHistoricoConsumo = ServiceLocator.getInstancia().getControladorHistoricoConsumo();
		LeituraMovimento leituraMovimento = null;

		Long codigoAnormalidadeLeitura = Long.valueOf(controladorConstanteSistema
				.obterValorConstanteSistemaPorCodigo(Constantes.C_ANORMALIDADE_LEITURA_NAO_FATURAR_PONTO));
		
		Boolean isAnormalidadeLeituraNaoFaturarPonto = Boolean.FALSE;
		
		Collection<LeituraMovimento> colecaoLeituraMovimento = controladorLeituraMovimento.consultarLeituraMovimentoPorRota(null, historicoMedicao.getAnoMesLeitura(),
				null, historicoMedicao.getPontoConsumo().getChavePrimaria(), historicoMedicao.getNumeroCiclo());
		
		if(colecaoLeituraMovimento.iterator().hasNext()) {
			leituraMovimento = colecaoLeituraMovimento.iterator().next();
			if (historicoMedicao.getAnormalidadeLeituraInformada() != null) {
				leituraMovimento.setCodigoAnormalidadeLeitura(historicoMedicao.getAnormalidadeLeituraInformada().getChavePrimaria());
			}
		}
		
		if(historicoMedicao.getAnormalidadeLeituraInformada() != null && historicoMedicao.getAnormalidadeLeituraInformada().getChavePrimaria() == codigoAnormalidadeLeitura) {
			isAnormalidadeLeituraNaoFaturarPonto = Boolean.TRUE;

			if(leituraMovimento != null) {
				leituraMovimento.setDataLeitura(null);
				leituraMovimento.setValorLeitura(null);
			}
			
			historicoMedicao.setHabilitado(false);
			
			HistoricoConsumo historicoConsumo = controladorHistoricoConsumo.consultarHistoricoConsumoPorHistoricoMedicao(historicoMedicao.getChavePrimaria());
			historicoConsumo.setHabilitado(false);
			
			controladorHistoricoConsumo.atualizar(historicoConsumo);
			
		}
		
		this.atualizar(historicoMedicao);
		
		if(leituraMovimento != null) {
			controladorLeituraMovimento.atualizar(leituraMovimento);
		}
		
		return isAnormalidadeLeituraNaoFaturarPonto;
		
	}

	/**
	 * Inserir medicao historico comentario.
	 *
	 * @param medicaoHistoricoComentario
	 *            the medicao historico comentario
	 * @param historicoMedicao
	 *            the historico medicao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void inserirMedicaoHistoricoComentario(MedicaoHistoricoComentario medicaoHistoricoComentario, HistoricoMedicao historicoMedicao)
					throws NegocioException {

		if (medicaoHistoricoComentario == null || StringUtils.isEmpty(medicaoHistoricoComentario.getDescricao())) {
			if (exigeComentarioAnaliseExcecao()) {
				throw new NegocioException(ERRO_NEGOCIO_COMENTARIO_ALTERACAO_DADOS_LEITURA_NAO_INFORMADO, true);
			}
		} else {
			Integer ultimoNumeroComentario = this.obterUltimoNumeroComentario(historicoMedicao.getChavePrimaria());
			if (ultimoNumeroComentario == null) {
				ultimoNumeroComentario = 0;
			}
			medicaoHistoricoComentario.setNumero(ultimoNumeroComentario + 1);
			this.validarDadosEntidade(medicaoHistoricoComentario);
			this.customizarDescricaoComentario(medicaoHistoricoComentario, historicoMedicao);
			this.inserir(medicaoHistoricoComentario);

		}
	}

	/**
	 * Customizar descricao comentario.
	 *
	 * @param medicaoHistoricoComentario
	 *            the medicao historico comentario
	 * @param historicoMedicao
	 *            the historico medicao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void customizarDescricaoComentario(MedicaoHistoricoComentario medicaoHistoricoComentario, HistoricoMedicao historicoMedicao)
					throws NegocioException {

		SimpleDateFormat df = new SimpleDateFormat(Constantes.FORMATO_DATA_BR);
		StringBuilder sb = new StringBuilder();

		Criteria criteria = this.createCriteria(HistoricoMedicao.class);
		criteria.add(Restrictions.idEq(historicoMedicao.getChavePrimaria()));

		HistoricoMedicao historicoMedicaoBd = (HistoricoMedicao) criteria.uniqueResult();
		this.getSession().evict(historicoMedicaoBd);

		sb.append(medicaoHistoricoComentario.getDescricao());

		sb.append(this.gerarComentarioCampo(HistoricoMedicao.NUMERO_LEITURA_INFORMADA, historicoMedicaoBd.getNumeroLeituraInformada(),
						historicoMedicao.getNumeroLeituraInformada()));

		String dataAtualLeituraInformada = null;
		String dataNovaLeituraInformada = null;

		if (historicoMedicaoBd.getDataLeituraInformada() != null) {
			dataAtualLeituraInformada = "\"" + df.format(historicoMedicaoBd.getDataLeituraInformada()) + "\"";
		}

		if (historicoMedicao.getDataLeituraInformada() != null) {
			dataNovaLeituraInformada = "\"" + df.format(historicoMedicao.getDataLeituraInformada()) + "\"";
		}

		sb.append(this.gerarComentarioCampo(HistoricoMedicao.DATA_LEITURA_INFORMADA, dataAtualLeituraInformada, dataNovaLeituraInformada));

		String anormalidadeInformadaAtual = null;
		String anormalidadeInformadaNova = null;

		if (historicoMedicaoBd.getAnormalidadeLeituraInformada() != null) {
			anormalidadeInformadaAtual = "\"" + historicoMedicaoBd.getAnormalidadeLeituraInformada().getDescricao() + "\"";
		}

		if (historicoMedicao.getAnormalidadeLeituraInformada() != null) {
			anormalidadeInformadaNova = "\"" + historicoMedicao.getAnormalidadeLeituraInformada().getDescricao() + "\"";
		}

		sb.append(this.gerarComentarioCampo(HistoricoMedicao.ANORMALIDADE_LEITURA_FATURADA, anormalidadeInformadaAtual,
						anormalidadeInformadaNova));

		sb.append(this.gerarComentarioCampo(HistoricoMedicao.NUMERO_LEITURA_ANTERIOR, historicoMedicaoBd.getNumeroLeituraAnterior(),
						historicoMedicao.getNumeroLeituraAnterior()));

		String dataAtualLeituraAnteriorInformada = null;
		String dataNovaLeituraAnteriorInformada = null;

		if (historicoMedicaoBd.getDataLeituraAnterior() != null) {
			dataAtualLeituraAnteriorInformada = "\"" + df.format(historicoMedicaoBd.getDataLeituraAnterior()) + "\"";
		}

		if (historicoMedicao.getDataLeituraAnterior() != null) {
			dataNovaLeituraAnteriorInformada = "\"" + df.format(historicoMedicao.getDataLeituraAnterior()) + "\"";
		}

		sb.append(this.gerarComentarioCampo(HistoricoMedicao.DATA_LEITURA_ANTERIOR, dataAtualLeituraAnteriorInformada,
						dataNovaLeituraAnteriorInformada));

		String leituristaAtual = null;
		String leiruristaNovo = null;

		if (historicoMedicaoBd.getLeiturista() != null) {
			leituristaAtual = "\"" + historicoMedicaoBd.getLeiturista().getFuncionario().getNome() + "\"";
		}

		if (historicoMedicao.getLeiturista() != null) {
			leiruristaNovo = "\"" + historicoMedicao.getLeiturista().getFuncionario().getNome() + "\"";
		}

		sb.append(this.gerarComentarioCampo(HistoricoMedicao.LEITURISTA, leituristaAtual, leiruristaNovo));

		sb.append(this.gerarComentarioCampo(HistoricoMedicao.CONSUMO_INFORMADO, historicoMedicaoBd.getConsumoInformado(),
						historicoMedicao.getConsumoInformado()));

		sb.append(this.gerarComentarioCampo(HistoricoMedicao.CREDITO_GERADO, historicoMedicaoBd.getCreditoVolume(),
						historicoMedicao.getCreditoVolume()));

		String indicadorAtual = null;
		String indicadorNovo = null;

		if (historicoMedicaoBd.getIndicadorInformadoCliente() != null) {
			if (historicoMedicaoBd.getIndicadorInformadoCliente().equals(true)) {
				indicadorAtual = "\"" + "SIM" + "\"";
			} else {
				indicadorAtual = "\"" + "NÃO" + "\"";
			}
		}

		if (historicoMedicao.getIndicadorInformadoCliente() != null) {
			if (historicoMedicao.getIndicadorInformadoCliente().equals(true)) {
				indicadorNovo = "\"" + "SIM" + "\"";
			} else {
				indicadorNovo = "\"" + "NÃO" + "\"";
			}
		}

		sb.append(this.gerarComentarioCampo(HistoricoMedicao.INFORMADO_PELO_CLIENTE, indicadorAtual, indicadorNovo));

		medicaoHistoricoComentario.setDescricao(sb.toString());

	}

	/**
	 * Gerar comentario campo.
	 *
	 * @param chaveCampo
	 *            the chave campo
	 * @param valorAtual
	 *            the valor atual
	 * @param novoValor
	 *            the novo valor
	 * @return the string builder
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private StringBuilder gerarComentarioCampo(String chaveCampo, Object valorAtual, Object novoValor) throws NegocioException {

		Properties prop = null;
		StringBuilder sb = new StringBuilder();

		try {
			prop = Util.lerArquivoPropriedades("mensagens.properties");
		} catch (GGASException e) {
			throw new NegocioException(e);
		}

		if ((valorAtual != null) && (novoValor != null)) {
			if (!valorAtual.equals(novoValor)) {
				sb.append(", ").append((prop.get(chaveCampo))).append(" alterado de ").append(valorAtual.toString()).append(" para ")
						.append(novoValor.toString());
			}
		} else {
			if (valorAtual != null) {
				sb.append(", ").append((prop.get(chaveCampo))).append(" alterado de ").append(valorAtual.toString())
						.append(" para não informado");
			} else if (novoValor != null) {
				sb.append(", ").append((prop.get(chaveCampo))).append(" alterado de não informado para ").append(novoValor.toString());
			}
		}

		return sb;
	}

	/**
	 * Obter ultimo numero comentario.
	 *
	 * @param idHistoricoMedicao
	 *            the id historico medicao
	 * @return the integer
	 */
	private Integer obterUltimoNumeroComentario(long idHistoricoMedicao) {

		Integer retorno = 0;

		StringBuilder hql = new StringBuilder();
		hql.append(" select max(numero) from ");
		hql.append(getClasseEntidadeMedicaoHistoricoComentario().getSimpleName());
		hql.append(WHERE);
		hql.append(" historicoMedicao.chavePrimaria = :idHistoricoMedicao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idHistoricoMedicao", idHistoricoMedicao);

		Integer ultimoNumero = (Integer) query.uniqueResult();

		retorno = ultimoNumero;

		return retorno;
	}

	/**
	 * Validar dados atualizar dados faturamento leitura.
	 *
	 * @param historicoMedicao
	 *            the historico medicao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void validarDadosAtualizarDadosFaturamentoLeitura(HistoricoMedicao historicoMedicao) throws NegocioException {

		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if (historicoMedicao.getAnoMesLeitura() == null || historicoMedicao.getAnoMesLeitura() <= 0) {
			stringBuilder.append(HistoricoMedicao.ANO_MES_LEITURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (historicoMedicao.getNumeroCiclo() == null || historicoMedicao.getNumeroCiclo() <= 0) {
			stringBuilder.append(HistoricoMedicao.NUMERO_CICLO);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (historicoMedicao.getDataLeituraInformada() == null) {
			stringBuilder.append(HistoricoMedicao.DATA_LEITURA_FATURADA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (historicoMedicao.getNumeroLeituraAnterior() == null) {
			stringBuilder.append(HistoricoMedicao.NUMERO_LEITURA_ANTERIOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if (historicoMedicao.getDataLeituraAnterior() == null) {
			stringBuilder.append(HistoricoMedicao.DATA_LEITURA_ANTERIOR);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		boolean validaLeiturista = true;
		if (historicoMedicao.getHistoricoInstalacaoMedidor() != null
						&& historicoMedicao.getHistoricoInstalacaoMedidor().getVazaoCorretor() != null) {
			validaLeiturista = false;
		}

		if (validaLeiturista
						&& ((historicoMedicao.getIndicadorInformadoCliente() == null) || (!historicoMedicao.getIndicadorInformadoCliente()))
						&& (historicoMedicao.getLeiturista() == null)) {
			stringBuilder.append(HistoricoMedicao.LEITURISTA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if (camposObrigatorios.length() > 0) {
			throw new NegocioException(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder
							.toString().length() - 2));
		}

		Collection<HistoricoMedicao> collHistoricoMedicaoPontoConsumo = this.consultarHistoricoMedicao(historicoMedicao.getPontoConsumo()
						.getChavePrimaria(), historicoMedicao.getAnoMesLeitura(), historicoMedicao.getNumeroCiclo());

		if (collHistoricoMedicaoPontoConsumo != null && !collHistoricoMedicaoPontoConsumo.isEmpty()) {
			for (HistoricoMedicao historicoMedicaoBd : collHistoricoMedicaoPontoConsumo) {
				if (historicoMedicaoBd.getChavePrimaria() != historicoMedicao.getChavePrimaria()
								&& historicoMedicaoBd.getDataLeituraInformada() != null
								&& historicoMedicaoBd.getDataLeituraInformada().compareTo(historicoMedicao.getDataLeituraInformada()) == 0) {
					throw new NegocioException(ERRO_NEGOCIO_JA_EXISTE_REGISTRO_MEDICAO_PARA_DATA_INFORMADA, true);
				}
			}
		}

		this.validarDatasHistoricoMedicao(historicoMedicao);
		this.validarMedidorHistoricoMedicao(historicoMedicao);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #validarMedidorHistoricoMedicao
	 * (br.com.ggas.
	 * medicao.leitura.HistoricoMedicao)
	 */
	@Override
	public void validarMedidorHistoricoMedicao(HistoricoMedicao historicoMedicao) throws NegocioException {

		// Valida o número de dígitos do medidor
		Medidor medidor = obterMedidorPorHistoricoMedicao(historicoMedicao);
		Integer numeroDigitosMedidor = null;
		if (medidor == null) {
			if(historicoMedicao.getHistoricoInstalacaoMedidor() != null){
				medidor = historicoMedicao.getHistoricoInstalacaoMedidor().getMedidor();
			}
		}else{

		}

		if(medidor != null){
			numeroDigitosMedidor = medidor.getDigito();
		}else{
			numeroDigitosMedidor = null;
		}


		if (numeroDigitosMedidor != null) {

			BigDecimal numeroLeituraInformada = historicoMedicao.getNumeroLeituraInformada();
			if (numeroLeituraInformada != null) {
				Integer leituraInformada = numeroLeituraInformada.intValue();
				if (leituraInformada.toString().length() > numeroDigitosMedidor) {
					throw new NegocioException(
									ERRO_NEGOCIO_LEITURA_EXCEDE_LIMITE_DIGITOS_MEDIDOR,
									new Object[] {HistoricoMedicao.NUMERO_LEITURA_INFORMADA, numeroDigitosMedidor, medidor.getNumeroSerie()});
				}
			}

			BigDecimal numeroLeituraAnterior = historicoMedicao.getNumeroLeituraAnterior();
			if (numeroLeituraAnterior != null) {
				Integer leituraAnterior = numeroLeituraAnterior.intValue();
				if (leituraAnterior.toString().length() > numeroDigitosMedidor) {
					throw new NegocioException(ERRO_NEGOCIO_LEITURA_EXCEDE_LIMITE_DIGITOS_MEDIDOR,
									new Object[] {HistoricoMedicao.NUMERO_LEITURA_ANTERIOR, numeroDigitosMedidor, medidor.getNumeroSerie()});
				}
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #validarDatasHistoricoMedicao
	 * (br.com.ggas.medicao
	 * .leitura.HistoricoMedicao)
	 */
	@Override
	public void validarDataAtualInformada(HistoricoMedicao historicoMedicao) throws NegocioException {

		if (historicoMedicao.getDataLeituraInformada() == null) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_LEITURA_ATUAL_INVALIDA, false);
		}
	}

	@Override
	public void validarLeituraAtualInformada(HistoricoMedicao historicoMedicao) throws NegocioException {

		if (historicoMedicao.getNumeroLeituraCorretor() == null) {
			throw new NegocioException(ERRO_NEGOCIO_LEITURA_ATUAL_INVALIDA, false);
		}
	}


	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.ControladorHistoricoMedicao#validarDatasHistoricoMedicao(br.com.ggas.medicao.leitura.HistoricoMedicao)
	 */
	@Override
	public void validarDatasHistoricoMedicao(HistoricoMedicao historicoMedicao) throws NegocioException {

		// A data de realização da leitura deve
		// ser maior que a data da leitura
		// anterior e menor do que a data corrente
		DateTime dataLeituraInformada = new DateTime(historicoMedicao.getDataLeituraInformada());
		dataLeituraInformada = dataLeituraInformada.withHourOfDay(0);
		dataLeituraInformada = dataLeituraInformada.withMinuteOfHour(0);
		dataLeituraInformada = dataLeituraInformada.withSecondOfMinute(0);
		dataLeituraInformada = dataLeituraInformada.withMillisOfSecond(0);

		DateTime dataCorrente = new DateTime();

		DateTime dataLeituraAnterior = new DateTime();
		if (historicoMedicao.getDataLeituraAnterior() != null) {
			dataLeituraAnterior = new DateTime(historicoMedicao.getDataLeituraAnterior());
		} else {
			if(historicoMedicao.getHistoricoInstalacaoMedidor() != null){
				dataLeituraAnterior = new DateTime(setarDataLeituraAnterior(null,
						historicoMedicao.getPontoConsumo(),
						historicoMedicao.getHistoricoInstalacaoMedidor().getMedidor()));
			}

		}

		dataLeituraAnterior = dataLeituraAnterior.withHourOfDay(0);
		dataLeituraAnterior = dataLeituraAnterior.withMinuteOfHour(0);
		dataLeituraAnterior = dataLeituraAnterior.withSecondOfMinute(0);
		dataLeituraAnterior = dataLeituraAnterior.withMillisOfSecond(0);

		// Valida se a data da leitura anterior é
		// posterior a data corrente.
		if (dataLeituraAnterior != null && dataLeituraAnterior.compareTo(dataCorrente) > 0) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_LEITURA_ANTERIOR_INVALIDA, false);
		}

		// Valida se a data da leitura informada é
		// posterior a data corrrente ou inferior
		// a data da leitura anterior
		if (dataLeituraInformada.compareTo(dataCorrente) > 0
						|| (dataLeituraAnterior != null && dataLeituraAnterior.compareTo(dataLeituraInformada) > 0)) {
			throw new NegocioException(ERRO_NEGOCIO_DATA_REALIZACAO_LEITURA_INVALIDA, Util.converterDataParaStringSemHora(
							dataLeituraAnterior.toDate(), Constantes.FORMATO_DATA_BR));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #setarDataLeituraAnterior
	 * (br.com.ggas.medicao
	 * .leitura.HistoricoMedicao)
	 */
	@Override
	public Date setarDataLeituraAnterior(InstalacaoMedidor instalacaoMedidor,
					PontoConsumo pontoConsumo, Medidor medidor) throws NegocioException {

		Date dataLeitura = null;
		if(pontoConsumo != null && medidor != null){

			ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

			if(pontoConsumo.getDadosResumoPontoConsumo() == null){
				pontoConsumo.setDadosResumoPontoConsumo(new DadosResumoPontoConsumoImpl());
			}
			Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor;
			if(pontoConsumo.getDadosResumoPontoConsumo().getListaHistoricoOperacaMedidor() == null
							|| pontoConsumo.getDadosResumoPontoConsumo().getListaHistoricoOperacaMedidor().isEmpty()){

				FiltroHistoricoOperacaoMedidor filtro = new FiltroHistoricoOperacaoMedidor();
				filtro.setIdPontoConsumo(pontoConsumo.getChavePrimaria());
				filtro.setIdMedidor(medidor.getChavePrimaria());
				filtro.setOrdenacaoDataRealizada(Ordenacao.DESCENDENTE);
				listaHistoricoOperacaoMedidor =
								controladorMedidor.obterHistoricoOperacaoMedidor(filtro);
				pontoConsumo.getDadosResumoPontoConsumo().setListaHistoricoOperacaMedidor(listaHistoricoOperacaoMedidor);
			}else{
				listaHistoricoOperacaoMedidor =
								pontoConsumo.getDadosResumoPontoConsumo().getListaHistoricoOperacaMedidor();
			}


			Optional<HistoricoOperacaoMedidor> historicoOperacaoMedidor = Optional.absent();
			if(listaHistoricoOperacaoMedidor.iterator().hasNext()) {
				historicoOperacaoMedidor = Optional.fromNullable(listaHistoricoOperacaoMedidor.iterator().next());
			}

			if (historicoOperacaoMedidor.isPresent() && historicoOperacaoMedidor.get().getOperacaoMedidor().getChavePrimaria()
					== OperacaoMedidor.CODIGO_REATIVACAO) {
				dataLeitura = historicoOperacaoMedidor.get().getDataRealizada();
			} else {
				dataLeitura = obterDataInstalacaoOuAtivacao(pontoConsumo.getInstalacaoMedidor());
			}

		}else if(instalacaoMedidor != null){
			dataLeitura = obterDataInstalacaoOuAtivacao(instalacaoMedidor);
		}

		return DataUtil.gerarDataHmsZerados(dataLeitura);

	}

	/**
	 * Obtem a data de Instalação ou Ativação do Medidor
	 * @param instalacaoMedidor {@link InstalacaoMedidor}
	 * @return Date {@link Date}
	 */
	public Date obterDataInstalacaoOuAtivacao (InstalacaoMedidor instalacaoMedidor){

		Date dataLeitura = null;

		Date dataInstalacao = instalacaoMedidor.getData();
		BigDecimal leituraInstalacao = instalacaoMedidor.getLeitura();

		BigDecimal leituraAtivacao = instalacaoMedidor.getLeituraAtivacao();
		Date dataAtivacao = instalacaoMedidor.getDataAtivacao();
		if (leituraInstalacao != null && leituraAtivacao != null) {
			if (leituraInstalacao.compareTo(leituraAtivacao) == 0) {
				dataLeitura = dataAtivacao;
			} else {
				if (dataInstalacao.after(dataAtivacao)) {
					dataLeitura = dataInstalacao;
				} else {
					dataLeitura = dataAtivacao;
				}
			}
		} else if (leituraAtivacao != null) {
			dataLeitura = dataAtivacao;
		} else {
			dataLeitura = dataInstalacao;
		}
		return dataLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #setarDataLeituraAnterior
	 * (br.com.ggas.medicao
	 * .leitura.HistoricoMedicao)
	 */
	@Override
	public BigDecimal setarLeituraAnterior(InstalacaoMedidor instalacaoMedidor,
					PontoConsumo pontoConsumo, Medidor medidor) throws NegocioException {

		BigDecimal leitura = null;
		if(pontoConsumo != null && medidor != null){
			ControladorMedidor controladorMedidor = ServiceLocator.getInstancia().getControladorMedidor();

			if(pontoConsumo.getDadosResumoPontoConsumo() == null){
				pontoConsumo.setDadosResumoPontoConsumo(new DadosResumoPontoConsumoImpl());
			}
			Collection<HistoricoOperacaoMedidor> listaHistoricoOperacaoMedidor;
			if(pontoConsumo.getDadosResumoPontoConsumo().getListaHistoricoOperacaMedidor() == null
						|| pontoConsumo.getDadosResumoPontoConsumo().getListaHistoricoOperacaMedidor().isEmpty()){

				FiltroHistoricoOperacaoMedidor filtro = new FiltroHistoricoOperacaoMedidor();
				filtro.setIdPontoConsumo(pontoConsumo.getChavePrimaria());
				filtro.setIdMedidor(medidor.getChavePrimaria());
				filtro.setOrdenacaoDataRealizada(Ordenacao.DESCENDENTE);
				listaHistoricoOperacaoMedidor =
								controladorMedidor.obterHistoricoOperacaoMedidor(filtro);
				pontoConsumo.getDadosResumoPontoConsumo().setListaHistoricoOperacaMedidor(listaHistoricoOperacaoMedidor);
			}else{
				listaHistoricoOperacaoMedidor =
								pontoConsumo.getDadosResumoPontoConsumo().getListaHistoricoOperacaMedidor();
			}

			Optional<HistoricoOperacaoMedidor> historicoOperacaoMedidor = Optional.absent();
			if(listaHistoricoOperacaoMedidor.iterator().hasNext()) {
				historicoOperacaoMedidor = Optional.fromNullable(listaHistoricoOperacaoMedidor.iterator().next());
			}

			if (historicoOperacaoMedidor.isPresent() && historicoOperacaoMedidor.get().getOperacaoMedidor().getChavePrimaria()
					== OperacaoMedidor.CODIGO_REATIVACAO) {

				leitura = historicoOperacaoMedidor.get().getNumeroLeitura();

			}else{

				leitura  = obterLeituraInstalacaoOuAtivacao(pontoConsumo.getInstalacaoMedidor());

			}
		}else if(instalacaoMedidor != null){

			leitura  = obterLeituraInstalacaoOuAtivacao(instalacaoMedidor);

		}

		return leitura;

	}

	/**
	 * @param instalacaoMedidor
	 * @return
	 */
	private BigDecimal obterLeituraInstalacaoOuAtivacao(InstalacaoMedidor instalacaoMedidor){

		BigDecimal leitura;

		BigDecimal leituraInstalacao = instalacaoMedidor.getLeitura();
		BigDecimal leituraAtivacao = instalacaoMedidor.getLeituraAtivacao();

		if (leituraInstalacao == null) {
			leituraInstalacao = BigDecimal.ZERO;
		}
		if (leituraAtivacao == null) {
			leituraAtivacao = BigDecimal.ZERO;
		}

		if (leituraInstalacao.compareTo(leituraAtivacao) == 0) {
			leitura = leituraAtivacao;
		} else {
			leitura = leituraInstalacao;
		}
		return leitura;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #exigeComentarioAnaliseExcecao()
	 */
	@Override
	public boolean exigeComentarioAnaliseExcecao() throws NegocioException {

		boolean retorno = false;
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();

		Integer exigeComentario = Integer.valueOf((String) controladorParametroSistema
						.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_EXIGE_COMENTARIO_ANALISE_EXCECAO));

		if (exigeComentario != null && exigeComentario > 0) {
			retorno = true;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #obterDataUltimaHistoricoMedicaoAnoMes
	 * (java.lang.Integer)
	 */
	@Override
	public Date obterDataUltimaHistoricoMedicaoAnoMes(Integer anoMes) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select max(dataLeituraInformada) from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(WHERE);
		hql.append(" anoMesLeitura = :anoMes ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setInteger("anoMes", anoMes);

		return (Date) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #verificarConcessaoCredito(java.lang.Long,
	 * java.math.BigDecimal)
	 */
	@Override
	public BigDecimal[] verificarConcessaoCredito(Long idHistoricoMedicao, BigDecimal valorLeituraAnteriorModificado)
					throws NegocioException {

		BigDecimal[] valores = new BigDecimal[2];

		HistoricoMedicao historicoMedicao = (HistoricoMedicao) obter(idHistoricoMedicao);
		if (((historicoMedicao != null && historicoMedicao.getNumeroLeituraAnterior() != null) && valorLeituraAnteriorModificado != null)
						&& (valorLeituraAnteriorModificado.compareTo(historicoMedicao.getNumeroLeituraAnterior()) < 0)) {
			BigDecimal saldo = historicoMedicao.getCreditoSaldo();
			if (saldo != null) {
				valores[1] = saldo;
			} else {
				valores[1] = BigDecimal.ZERO;
			}
			BigDecimal diferenca = historicoMedicao.getNumeroLeituraAnterior().subtract(valorLeituraAnteriorModificado);
			valores[0] = diferenca;
			valores[1] = valores[1].add(diferenca);

		}

		return valores;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #calcularCreditoGerado
	 * (br.com.ggas.medicao.leitura
	 * .HistoricoMedicao, java.math.BigDecimal)
	 */
	@Override
	public void calcularCreditoGerado(HistoricoMedicao historicoMedicao, BigDecimal creditoVolume) throws NegocioException {

		BigDecimal creditoGerado = null;
		BigDecimal creditoSaldo = historicoMedicao.getCreditoSaldo();

		if (creditoVolume != null && creditoSaldo != null) {
			creditoGerado = creditoVolume.subtract(creditoSaldo);
			if (creditoGerado.compareTo(BigDecimal.ZERO) == 0) {
				creditoGerado = null;
			} else if (creditoGerado.abs().compareTo(creditoSaldo) == 0) {
				creditoGerado = null;
				creditoSaldo = null;
			} else if (creditoGerado.compareTo(BigDecimal.ZERO) < 0 && creditoGerado.compareTo(creditoSaldo) < 0) {
				creditoGerado = null;
				creditoSaldo = creditoVolume;
			}

		} else if (creditoVolume != null) {
			creditoGerado = creditoVolume;
		}

		historicoMedicao.setCreditoGerado(creditoGerado);
		historicoMedicao.setCreditoSaldo(creditoSaldo);

	}

	/**
	 * Obter medidor por historico medicao.
	 *
	 * @param historicoMedicao
	 *            the historico medicao
	 * @return the medidor
	 */
	private Medidor obterMedidorPorHistoricoMedicao(HistoricoMedicao historicoMedicao) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select historico.pontoConsumo.instalacaoMedidor.medidor from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" historico where ");
		hql.append(" historico.chavePrimaria = :idHistoricoMedicao ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong("idHistoricoMedicao", historicoMedicao.getChavePrimaria());

		return (Medidor) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.ControladorHistoricoMedicao#obterHistoricoMedicaoPorTarifaPontoConsumo(br.com.ggas.faturamento.tarifa
	 * .TarifaPontoConsumo)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> obterHistoricoMedicaoPorTarifaPontoConsumo(TarifaPontoConsumo tarifaPontoConsumo) {

		Criteria criteria = createCriteria(this.getClasseEntidade());

		int dIncio = Util.obterMesAnoData(tarifaPontoConsumo.getDataInicioVigencia(), Calendar.MONTH) + 1;
		dIncio += (Util.obterMesAnoData(tarifaPontoConsumo.getDataInicioVigencia(), Calendar.YEAR) + 1900) * 100;

		int dFim = Util.obterMesAnoData(tarifaPontoConsumo.getDataFimVigencia(), Calendar.MONTH) + 1;
		dFim += (Util.obterMesAnoData(tarifaPontoConsumo.getDataFimVigencia(), Calendar.YEAR) + 1900) * 100;

		criteria.add(Restrictions.between("anoMesLeitura", dIncio, dFim));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #criarMedicaoHistoricoComentario()
	 */
	@Override
	public EntidadeNegocio criarMedicaoHistoricoComentario() {

		return (EntidadeNegocio) ServiceLocator.getInstancia()
						.getBeanPorID(MedicaoHistoricoComentario.BEAN_ID_MEDICAO_HISTORICO_COMENTARIO);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #consultarMedicaoHistoricoComentario
	 * (java.lang.Long, int, int)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<MedicaoHistoricoComentario> consultarMedicaoHistoricoComentario(Long chavePrimariaPontoConsumo, int referencia,
					int ciclo) throws NegocioException {

		Collection<HistoricoMedicao> listaHistoricoMedicao = this.consultarHistoricoMedicao(chavePrimariaPontoConsumo, referencia, ciclo);
		Long[] chavesHistoricoMedicao = Util.collectionParaArrayChavesPrimarias(listaHistoricoMedicao);

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidadeMedicaoHistoricoComentario().getSimpleName());
		hql.append(" comentario ");
		hql.append(" inner join fetch comentario.usuario.funcionario ");
		hql.append(" where comentario.historicoMedicao.chavePrimaria in (:chavesHistoricoMedicao) ");
		hql.append(" order by comentario.historicoMedicao.dataLeituraFaturada asc, comentario.ultimaAlteracao desc ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setParameterList("chavesHistoricoMedicao", chavesHistoricoMedicao);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #existeMedicaoHistoricoComentario
	 * (java.lang.Long, int, int)
	 */
	@Override
	public boolean existeMedicaoHistoricoComentario(Long chavePrimariaPontoConsumo, int referencia, int ciclo) throws GGASException {

		boolean retorno = false;

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(*) ");
		hql.append(FROM);
		hql.append(getClasseEntidadeMedicaoHistoricoComentario().getSimpleName());
		hql.append(" comentario ");
		hql.append(" where comentario.historicoMedicao.chavePrimaria in ( ");
		hql.append(" 	select historico.chavePrimaria ");
		hql.append(" 	from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" 	historico where ");
		hql.append(" 	historico.pontoConsumo.chavePrimaria = :idPontoConsumo ");
		hql.append(" 	and historico.anoMesLeitura = :referencia ");
		hql.append(" 	and historico.numeroCiclo = :ciclo ");
		hql.append("    ) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setLong("idPontoConsumo", chavePrimariaPontoConsumo);
		query.setLong("referencia", referencia);
		query.setLong("ciclo", ciclo);

		Long total = (Long) query.uniqueResult();
		if (total != null && total > 0) {
			retorno = true;
		}

		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #consultarDataLeituraUltimoDiaCicloAnterior
	 * (br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public Date consultarDataLeituraUltimoDiaCicloAnterior(PontoConsumo pontoConsumo, Integer anoMesLeitura, Integer numeroCiclo)
					throws NegocioException {

		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

		Date dataLeituraUltimoDiaCicloAnterior = null;

		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

		if ((contratoPontoConsumo != null) && (contratoPontoConsumo.getPeriodicidade() != null) && (anoMesLeitura != null)
						&& (numeroCiclo != null)) {

			Map<String, Integer> referenciaCicloAnterior = Util.regredirReferenciaCiclo(anoMesLeitura, numeroCiclo, contratoPontoConsumo
							.getPeriodicidade().getQuantidadeCiclo());
			HistoricoMedicao historicoMedicaoCicloAnterior = controladorHistoricoMedicao.obterUltimoHistoricoMedicao(
							pontoConsumo.getChavePrimaria(), referenciaCicloAnterior.get("referencia"),
							referenciaCicloAnterior.get("ciclo"));

			if (historicoMedicaoCicloAnterior != null) {
				dataLeituraUltimoDiaCicloAnterior = historicoMedicaoCicloAnterior.getDataLeituraFaturada();
			}

		}

		return dataLeituraUltimoDiaCicloAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao#
	 * consultarHistoricoMedicaoUltimoDiaCicloAnterior
	 * (br.com.ggas.cadastro.imovel.PontoConsumo,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public HistoricoMedicao consultarHistoricoMedicaoUltimoDiaCicloAnterior(PontoConsumo pontoConsumo, Integer anoMesLeitura,
					Integer numeroCiclo) throws NegocioException {

		ControladorContrato controladorContrato = (ControladorContrato) ServiceLocator.getInstancia().getControladorNegocio(
						ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO);
		ControladorHistoricoMedicao controladorHistoricoMedicao = (ControladorHistoricoMedicao) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO);

		HistoricoMedicao historicoMedicaoCicloAnterior = null;

		ContratoPontoConsumo contratoPontoConsumo = controladorContrato.obterContratoAtivoPontoConsumo(pontoConsumo);

		if ((contratoPontoConsumo != null) && (contratoPontoConsumo.getPeriodicidade() != null) && (anoMesLeitura != null)
						&& (numeroCiclo != null)) {

			Map<String, Integer> referenciaCicloAnterior = Util.regredirReferenciaCiclo(anoMesLeitura, numeroCiclo, contratoPontoConsumo
							.getPeriodicidade().getQuantidadeCiclo());
			historicoMedicaoCicloAnterior = controladorHistoricoMedicao.obterUltimoHistoricoMedicao(pontoConsumo.getChavePrimaria(),
							referenciaCicloAnterior.get("referencia"), referenciaCicloAnterior.get("ciclo"));

		}

		return historicoMedicaoCicloAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.
	 * ControladorHistoricoMedicao
	 * #listarTipoMedicao()
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<TipoMedicao> listarTipoMedicao() throws NegocioException {

		Criteria criteria = this.createCriteria(TipoMedicao.class);
		criteria.add(Restrictions.eq(EntidadeNegocio.ATRIBUTO_HABILITADO, Boolean.TRUE));
		criteria.addOrder(Order.asc("descricao"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.ControladorHistoricoMedicao#listarHistoricoMedicaoPorPontoConsumo(br.com.ggas.cadastro.imovel.PontoConsumo
	 * , java.util.Date)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> listarHistoricoMedicaoPorPontoConsumo(PontoConsumo pontoConsumo, Date dataleitura)
					throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.add(Restrictions.eq("pontoConsumo.chavePrimaria", pontoConsumo.getChavePrimaria()));
		criteria.add(Restrictions.lt("dataLeituraInformada", dataleitura));
		criteria.addOrder(Order.desc("dataLeituraInformada"));

		return criteria.list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> listarHistoricoMedicaoPorMedidor(Medidor medidor, Date dataleitura) throws NegocioException {

		Criteria criteria = getCriteria();
		criteria.createAlias("historicoInstalacaoMedidor", "historicoInstalacaoMedidor", CriteriaSpecification.INNER_JOIN);
		criteria.createAlias("historicoInstalacaoMedidor.medidor", "medidor", CriteriaSpecification.INNER_JOIN);
		criteria.add(Restrictions.eq("medidor.chavePrimaria", medidor.getChavePrimaria()));
		criteria.add(Restrictions.lt("dataLeituraInformada", dataleitura));
		criteria.addOrder(Order.desc("dataLeituraInformada"));

		return criteria.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.ControladorHistoricoMedicao#consultarHistoricoMedicao(java.lang.Long, java.util.Date)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> consultarHistoricoMedicaoPorPontoConsumoOuMedidor(Long chavePrimariaPontoConsumo,
					Date dataRealizacaoLeitura, Medidor medidor) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" hisMed ");

		if (chavePrimariaPontoConsumo != null && chavePrimariaPontoConsumo > 0) {
			hql.append(" where pontoConsumo.chavePrimaria = :idPontoConsumo ");
		} else if (medidor != null) {
			hql.append(" inner join fetch hisMed.historicoInstalacaoMedidor historicoInstalacaoMedidor ");
			hql.append(" inner join fetch historicoInstalacaoMedidor.medidor medidor ");
			hql.append(" where medidor.chavePrimaria = :idMedidor ");
		}

		hql.append(" and (hisMed.dataLeituraFaturada is not null and hisMed.dataLeituraFaturada < :dataRealizacaoLeitura) ");
		hql.append(" or ");
		hql.append(" (hisMed.dataLeituraInformada is not null and hisMed.dataLeituraInformada < :dataRealizacaoLeitura) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (chavePrimariaPontoConsumo != null && chavePrimariaPontoConsumo > 0) {
			query.setLong("idPontoConsumo", chavePrimariaPontoConsumo);
		} else if (medidor != null) {
			query.setLong("idMedidor", medidor.getChavePrimaria());
		}

		query.setDate("dataRealizacaoLeitura", dataRealizacaoLeitura);
		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.ControladorHistoricoMedicao#consultarHistoricoMedicao(java.util.Map, java.lang.String,
	 * java.lang.String[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> consultarHistoricoMedicao(Map<String, Object> filtro, String ordenacao, String... propriedadesLazy)
					throws NegocioException {

		Query query = null;
		Boolean habilitado = null;
		Date dataLeituraMaiorInformada = null;
		Date dataLeituraMenorInformada = null;
		Long chavePrimariaPontoConsumo = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);

		if (!filtro.isEmpty()) {

			habilitado = (Boolean) filtro.get(EntidadeNegocio.ATRIBUTO_HABILITADO);
			dataLeituraMaiorInformada = (Date) filtro.get("dataLeituraMaiorInformada");
			dataLeituraMenorInformada = (Date) filtro.get("dataLeituraMenorInformada");
			chavePrimariaPontoConsumo = (Long) filtro.get("chavePrimariaPontoConsumo");
		}

		hql.append(" where 1 = 1 ");

		if (habilitado != null) {

			hql.append(" and historico.habilitado = :habilitado");
		}

		if (dataLeituraMenorInformada != null) {

			hql.append(" and historico.dataLeituraInformada < :dataLeituraMenorInformada");
		}

		if (dataLeituraMaiorInformada != null) {

			hql.append(" and historico.dataLeituraInformada > :dataLeituraMaiorInformada");
		}

		if (chavePrimariaPontoConsumo != null) {

			hql.append(" and historico.pontoConsumo.chavePrimaria = :chavePrimariaPontoConsumo");
		}

		if (ordenacao != null) {

			hql.append(" order by  ").append(ordenacao);

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (!filtro.isEmpty()) {

			if (habilitado != null) {

				query.setBoolean(EntidadeNegocio.ATRIBUTO_HABILITADO, habilitado);
			}

			if (dataLeituraMenorInformada != null) {

				query.setDate("dataLeituraMenorInformada", dataLeituraMenorInformada);
			}

			if (dataLeituraMaiorInformada != null) {

				query.setDate("dataLeituraMaiorInformada", dataLeituraMaiorInformada);
			}

			if (chavePrimariaPontoConsumo != null) {

				query.setLong("chavePrimariaPontoConsumo", chavePrimariaPontoConsumo);
			}
		}

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.ControladorHistoricoMedicao#consultarHistoricoMedicao(java.lang.Long, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<HistoricoMedicao> consultarHistoricoMedicao(Long chavePrimaria, Date dataLeituraMenorInformada,
					Date dataLeituraMaiorInformada) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(HISTORICO);
		hql.append(" where 1 = 1 ");
		hql.append(" and historico.dataLeituraInformada >= :dataLeituraMenorInformada");
		hql.append(" and historico.dataLeituraInformada <= :dataLeituraMaiorInformada");
		hql.append(" and historico.pontoConsumo.chavePrimaria = :chavePrimaria");
		hql.append(" and historico.habilitado = true ");
		hql.append(" order by anoMesLeitura ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setDate("dataLeituraMenorInformada", dataLeituraMenorInformada);
		query.setDate("dataLeituraMaiorInformada", dataLeituraMaiorInformada);
		query.setLong("chavePrimaria", chavePrimaria);

		return query.list();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.ControladorHistoricoMedicao#consultarHistoricoAgrupadoAnoMes(java.lang.Long, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Object> consultarHistoricoAgrupadoAnoMes(Long chavePrimariaContrato, Date dataLeituraMenorInformada,
					Date dataLeituraMaiorInformada) throws NegocioException {

		DetachedCriteria subquery = DetachedCriteria.forClass(getClasseEntidadeContratoPontoConsumo(), "contratoPontoConsumo");
		subquery.add(Restrictions.eq("contratoPontoConsumo.contrato.chavePrimaria", chavePrimariaContrato));
		subquery.createAlias("contratoPontoConsumo.pontoConsumo", "pontoConsumo");
		subquery.setProjection(Projections.property("pontoConsumo.chavePrimaria"));

		Criteria criteria = createCriteria(getClasseEntidade(), "historico");

		criteria.add(Subqueries.propertyIn("historico.pontoConsumo.chavePrimaria", subquery));
		criteria.setFetchMode("historico.anoMesLeitura", FetchMode.SELECT);

		criteria.add(Restrictions.between("historico.dataLeituraInformada", dataLeituraMenorInformada, dataLeituraMaiorInformada));
		criteria.add(Restrictions.eq("historico.habilitado", Boolean.TRUE));

		criteria.setProjection(Projections.projectionList().add(Projections.groupProperty("historico.anoMesLeitura").as("anoMesLeitura"))
						.add(Projections.sum("historico.numeroLeituraInformada").as("leituraAtual"))
						.add(Projections.sum("historico.numeroLeituraAnterior").as("leituraAnterior"))
						.add(Projections.avg("historico.fatorPTZCorretor").as("fatorPTZ")));

		criteria.addOrder(Order.asc("historico.anoMesLeitura"));

		return criteria.list();

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.ControladorHistoricoMedicao#inativarHistoricoMedicao(java.lang.Long)
	 */
	@Override
	public HistoricoMedicao inativarHistoricoMedicao(Long idHistoricoMedicao) throws NegocioException, ConcorrenciaException {

		HistoricoMedicao historico = (HistoricoMedicao) obter(idHistoricoMedicao);
		historico.setHabilitado(false);

		atualizar(historico);

		ControladorHistoricoConsumo controladorHistoricoConsumo = ServiceLocator.getInstancia().getControladorHistoricoConsumo();
		controladorHistoricoConsumo.inativarHistoricoConsumo(idHistoricoMedicao);

		return historico;
	}

	/**
	 * Obtém a quantidade de entidades de HistoricoMedicao, filtrando a consulta
	 * de acordo com os parametros fornecidos
	 * @param chavePrimariaPontoConsumo
	 * @param dataRealizacaoLeitura
	 * @param medidor
	 * @return Long - quantidade de registros de HistoricoMedicao
	 */
	@Override
	public Long obterQuantidadeDoHistoricoMedicaoPorPontoConsumoOuMedidor(Long chavePrimariaPontoConsumo, Date dataRealizacaoLeitura,
					Medidor medidor) throws NegocioException {

		StringBuilder hql = new StringBuilder();
		hql.append(" select count(hisMed) ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" hisMed ");


		if (chavePrimariaPontoConsumo!=null && chavePrimariaPontoConsumo>0) {
			hql.append(" where pontoConsumo.chavePrimaria = :idPontoConsumo ");
		}else if(medidor!=null){
			hql.append(" inner join fetch hisMed.historicoInstalacaoMedidor historicoInstalacaoMedidor ");
			hql.append(" inner join fetch historicoInstalacaoMedidor.medidor medidor ");
			hql.append(" where medidor.chavePrimaria = :idMedidor ");
		}

		hql.append(" and (hisMed.dataLeituraFaturada is not null and hisMed.dataLeituraFaturada < :dataRealizacaoLeitura) ");
		hql.append(" or ");
		hql.append(" (hisMed.dataLeituraInformada is not null and hisMed.dataLeituraInformada < :dataRealizacaoLeitura) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (chavePrimariaPontoConsumo!=null && chavePrimariaPontoConsumo>0) {
			query.setLong("idPontoConsumo", chavePrimariaPontoConsumo);
		}else if(medidor!=null){
			query.setLong("idMedidor", medidor.getChavePrimaria());
		}

		query.setDate("dataRealizacaoLeitura", dataRealizacaoLeitura);
		return (Long) query.uniqueResult();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.ControladorHistoricoMedicao#obterPontosConsumoNaoRegistrados(java.util.Collection)
	 */
	@Override
	public List<PontoConsumo> obterPontosConsumoNaoRegistrados(List<PontoConsumo> pontosConsumo, Rota rota) {
		List<PontoConsumo> listaPontosNaoRegistrados = new ArrayList<PontoConsumo>();
		List<PontoConsumo> listaPontosRegistrados = this.obterPontosComHistoricoMedicaoAtual(rota);
		
		listaPontosNaoRegistrados.addAll(pontosConsumo);
		
		for (PontoConsumo ponto : listaPontosRegistrados) {
			listaPontosNaoRegistrados.remove(ponto);
		}
			
		return listaPontosNaoRegistrados;
	}
	
	private List<PontoConsumo> obterPontosComHistoricoMedicaoAtual(Rota rota) {
		
		StringBuilder hql = new StringBuilder();
		hql.append(" select medicao.pontoConsumo ");
		hql.append(FROM);
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" medicao ");
		hql.append(" where medicao.anoMesLeitura = :anoMesLeitura ");
		hql.append(" and medicao.numeroCiclo = :numeroCiclo ");
		hql.append(" and medicao.pontoConsumo.chavePrimaria in ( ");
		hql.append(" 	select pontoConsumo.chavePrimaria from ");
		hql.append(getClasseEntidadePontoConsumo().getSimpleName());
		hql.append("    pontoConsumo ");
		hql.append(" 	where pontoConsumo.rota.chavePrimaria = :idRota )");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		query.setInteger("anoMesLeitura", rota.getGrupoFaturamento().getAnoMesReferencia());
		query.setInteger("numeroCiclo", rota.getGrupoFaturamento().getNumeroCiclo());
		query.setLong("idRota", rota.getChavePrimaria());
		return query.list();
	}
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.medicao.leitura.ControladorHistoricoMedicao#obterDataMedicaoAnterior(HistoricoConsumo)
	 */
	@Override
	public Date obterDataMedicaoAnterior(HistoricoConsumo historicoConsumo) {
		HistoricoMedicao historicoMedicao = historicoConsumo.getHistoricoAtual();
		HistoricoMedicao historicoMedicaoCicloAnterior = historicoConsumo.getHistoricoAnterior();
		Date dataAnterior = new Date();
		if (historicoMedicaoCicloAnterior != null && historicoMedicaoCicloAnterior.getDataLeituraInformada() != null) {
			dataAnterior = historicoMedicaoCicloAnterior.getDataLeituraFaturada();
		} else if (historicoMedicao.getPontoConsumo().getInstalacaoMedidor() != null
				&& historicoMedicao.getPontoConsumo().getInstalacaoMedidor().getDataAtivacao() != null) {
			dataAnterior = historicoMedicao.getPontoConsumo().getInstalacaoMedidor().getDataAtivacao();
		} else if (historicoMedicao.getPontoConsumo().getInstalacaoMedidor() != null
				&& historicoMedicao.getPontoConsumo().getInstalacaoMedidor().getData() != null) {
			dataAnterior = historicoMedicao.getPontoConsumo().getInstalacaoMedidor().getData();
		}
		return dataAnterior;
	}
	
	@Override
	public Collection<HistoricoLeituraGSAVO> obterHistoricoMedicaoGSAPorPontoConsumo(PontoConsumo pontoConsumo) {
		
		Collection<HistoricoLeituraGSAVO> historicoLeituraGSA = new ArrayList<>();
		String nomeQuery = "historicoMedicao.medicaoGSA";
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.getNamedQuery(nomeQuery);
		query.setParameter("codigoUnico", pontoConsumo.getCodigoLegado());
		
		
		
		for (Object object : query.list()) {
			Object[] resultado = (Object[]) object;
			HistoricoLeituraGSAVO leituraGSA = new HistoricoLeituraGSAVO();
			
			leituraGSA.setLeituraAtual(resultado[0] != null ? resultado[0].toString() : "");
			leituraGSA.setDataLeituraAtual(resultado[1] != null ? resultado[1].toString() : "");
			leituraGSA.setLeituraAnterior(resultado[2] != null ? resultado[2].toString() : "");
			leituraGSA.setDataLeituraAnterior(resultado[3] != null ? resultado[3].toString() : "");
			leituraGSA.setVolumeMedido(resultado[4] != null ? resultado[4].toString() : "");
			leituraGSA.setVolumeFaturado(resultado[5] != null ? resultado[5].toString() : "");
			leituraGSA.setTipoMedicaoManual(resultado[6] != null ? resultado[6].toString() : "");
			
			historicoLeituraGSA.add(leituraGSA);
			
		}
		
		return historicoLeituraGSA;
		
	}
	
	@Override
	public Collection<HistoricoLeituraGSAVO> obterVolumeFaturadoGSANoPeriodo(PontoConsumo pontoConsumo, String dataInicio, String dataFim) {
		Collection<HistoricoLeituraGSAVO> historicoLeituraGSA = new ArrayList<>();
		String nomeQuery = "historicoMedicao.volumeNoPeriodo";
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.getNamedQuery(nomeQuery);
		query.setParameter("codigoUnico", pontoConsumo.getCodigoLegado());
		query.setParameter("dataInicio", dataInicio);
		query.setParameter("dataFim", dataFim);

		
		for (Object object : query.list()) {
			Object[] resultado = (Object[]) object;
			HistoricoLeituraGSAVO leituraGSA = new HistoricoLeituraGSAVO();
			
			leituraGSA.setVolumeFaturado(resultado[0] != null ? resultado[0].toString() : "");
			leituraGSA.setDataLeituraAtual(resultado[1] != null ? resultado[1].toString() : "");
			
			historicoLeituraGSA.add(leituraGSA);
			
		}
		
		return historicoLeituraGSA;
	}
	
}
