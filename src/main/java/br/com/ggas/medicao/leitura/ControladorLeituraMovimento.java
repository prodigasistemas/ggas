/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.leitura;

import br.com.ggas.api.dto.RotaDTO;
import br.com.ggas.batch.Processo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.FormatoInvalidoException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.medicao.exception.CodigoLegadoEmVariosPontosConsumo;
import br.com.ggas.medicao.exception.LeituraMovimentoNaoEncontrada;
import br.com.ggas.medicao.exception.MaisDeUmaLeituraMovimentoRetornada;
import br.com.ggas.medicao.exception.PontoConsumoNaoEncontrado;
import br.com.ggas.medicao.rota.CronogramaRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.relatorio.medicao.leitura.DadosRegistrarLeiturasAnormalidades;
import br.com.ggas.util.Pair;
import br.com.ggas.web.faturamento.leitura.dto.LeituraMovimentoDTO;
import br.com.ggas.web.faturamento.leitura.dto.PesquisaLeituraMovimento;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.jdom.Element;

/**
 * Interface responsável pela assinatura dos métodos relacionados ao controlador de leitura de movimentos
 *
 */
public interface ControladorLeituraMovimento extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO = "controladorLeituraMovimento";

	String SUCESSO_GERACAO_RETORNAR_LEITURAS = "SUCESSO_GERACAO_RETORNAR_LEITURAS";

	String ERRO_NEGOCIO_PARAMETRO_NAO_ENCONTRADO = "ERRO_NEGOCIO_PARAMETRO_NAO_ENCONTRADO";

	String ERRO_NEGOCIO_DATA_LEITURA_MENOR = "ERRO_NEGOCIO_DATA_LEITURA_MENOR";

	String ERRO_NEGOCIO_DATA_LEITURA_NAO_PREENCHIDA = "ERRO_NEGOCIO_DATA_LEITURA_NAO_PREENCHIDA";

	String GERAR_DADOS_DATA_LEITURA = "GERAR_DADOS_DATA_LEITURA";

	String SUCESSO_GERACAO_DADOS_LEITURA = "SUCESSO_GERACAO_DADOS_LEITURA";

	String ERRO_ROTA_SEM_CRONOGRAMA = "ERRO_ROTA_SEM_CRONOGRAMA";

	String ERRO_DATA_REALIZADA_REGISTRO_LEITURA = "ERRO_DATA_REALIZADA_REGISTRO_LEITURA";

	String PARAMETRO_REFERENCIA_FATURAMENTO = "REFERENCIA_FATURAMENTO";

	/**
	 * Método responsável por criar uma coleção de
	 * Leitura Movimento a partir dos pontos de
	 * consumo recebidos.
	 * 
	 * @param pontosConsumo
	 *            the pontos consumo
	 * @param dataPrevistaLeitura
	 *            A data prevista de leitura
	 * @param usuario
	 *            O usuário
	 * @param sequencias
	 *            As sequencias de rotas
	 * @param atividadeSistema
	 *            the atividade sistema
	 * @param processo
	 *            the processo
	 * @param logProcessamento log do processamento
	 * @return coleção de leitura movimento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Collection<LeituraMovimento> criarLeiturasMovimento(Collection<PontoConsumo> pontosConsumo, Date dataPrevistaLeitura, Usuario usuario,
			Map<String, Integer> sequencias, AtividadeSistema atividadeSistema, Processo processo, StringBuilder logProcessamento)
			throws GGASException;

	/**
	 *	Retorna um par representado pela lista de LeituraMovimentoDTO atendendo aos critérios informados e o total de resultados encontrados
	 * @param pesquisa pesquisa de leitura movimento
	 * @return O par <lista de LeituraMovimentoDTO que atende aos filtros da pesquisa e que estão dentro da paginação informada>
	 *     e <total de resultados encontrados>
	 */
	Pair<List<LeituraMovimentoDTO>, Long> criarListaLeituraMovimentoDTO(PesquisaLeituraMovimento pesquisa);

	/**
	 * Método responsável por consultar Movimentos
	 * de Leitura não processados e que possuem a
	 * rota com chave primária
	 * na lista informada.
	 * 
	 * @param rotas
	 *            the rotas
	 * @return Uma coleção de leituraMovimento.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<LeituraMovimento> consultarMovimentosLeituraNaoProcessados(Collection<Rota> rotas) throws NegocioException;

	/**
	 * Consultar por situacao ano mes ciclo ponto consumo.
	 * 
	 * @param filtro
	 *            Leitura de movimento contendo os
	 *            valores para filtragem.
	 * @return leituraMovimento
	 */
	LeituraMovimento consultarPorSituacaoAnoMesCicloPontoConsumo(LeituraMovimento filtro);

	/**
	 * insere uma lista de LeituraMovimento.
	 * 
	 * @param leiturasMovimentos
	 *            the leituras movimentos
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirLeiturasMovimento(Collection<LeituraMovimento> leiturasMovimentos) throws NegocioException;

	/**
	 * Método responsável por verificar se as
	 * chaves foram selecionadas para a geração de
	 * dados para leitura.
	 * 
	 * @param chavesPrimarias
	 *            As chaves primárias dos pontos
	 *            de consumo.
	 * @param dataPrevistaLeitura
	 *            Data Prevista de Leitura
	 * @param atividadeSistema
	 *            the atividade sistema
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarGerarDadosDeLeitura(Long[] chavesPrimarias, String dataPrevistaLeitura, AtividadeSistema atividadeSistema)
					throws NegocioException;

	/**
	 * Método responsável por realizar o registro
	 * da leitura e anormalidade.
	 * 
	 * @param rotas
	 *            the rotas
	 * @param atividadeSistema
	 *            the atividade sistema
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param leituras as chaves das leituras que se deseja registrar
	 * @param logProcessamento
	 * 					the logProcessamento
	 * @return coleção de dados do registro da
	 *         leitura e anormalidade.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws FormatoInvalidoException
	 *             the formato invalido exception
	 * @throws GGASException 
	 * 						the GGASException
	 * @throws NumberFormatException 
	 * 						the NumberFormatException
	 */
	Collection<DadosRegistrarLeiturasAnormalidades> registrarLeituraAnormalidade(Long[] rotas, AtividadeSistema atividadeSistema,
			Long chavePrimaria, Long[] leituras, StringBuilder logProcessamento, Usuario usuarioLogado, Boolean isEncerrarRota) throws GGASException;

	/**
	 * Método responsável por retornar uma
	 * situação de leitura do movimento.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria da situação
	 * @return Uma dituação de leitura moviumento
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	SituacaoLeituraMovimento obterSituacaoLeituraMovimento(long chavePrimaria) throws NegocioException;

	/**
	 * Método responsável por retornar uma
	 * situação de leitura.
	 * 
	 * @param chavePrimaria
	 *            A chave primaria da situação
	 * @return Uma situação de leitura
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	SituacaoLeitura obterSituacaoLeitura(long chavePrimaria) throws NegocioException;

	/**
	 * Metodo responsavel por verificar se todos
	 * os pontos de consumo da rota
	 * estao com leituraMovimento no mesmo
	 * anoMesReferencia e ciclo.
	 * 
	 * @param rota
	 *            the rota
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean verificarGerarDadosParaLeitura(Rota rota, Integer anoMesReferencia, Integer numeroCiclo) throws NegocioException;

	/**
	 * Metodo responsavel por verificar se todos
	 * os pontos de consumo da rota
	 * estao com leituraMovimento no mesmo
	 * anoMesReferencia e ciclo.
	 * 
	 * @param rota
	 * 			the rota
	 * @param anoMesReferencia
	 * 			the anoMesReferencia
	 * @param numeroCiclo
	 * 			the numeroCiclo
	 * @param logProcesso
	 * 			the logProcesso
	 * @return  boolean
	 * @throws NegocioException
	 * 			the NegocioException
	 */
	boolean verificarRetornoLeitura(Rota rota, Integer anoMesReferencia, Integer numeroCiclo, StringBuilder logProcesso)
			throws NegocioException;

	/**
	 * Metodo responsavel por verificar se todos
	 * os pontos de consumo da rota
	 * estao com leituraMovimento no mesmo
	 * anoMesReferencia e ciclo.
	 * 
	 * @param rota
	 *            the rota
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean verificarRetornoLeituraCorretorVazao(Rota rota, Integer anoMesReferencia, Integer numeroCiclo) throws NegocioException;

	/**
	 * Metodo responsavel por verificar se todos
	 * os pontos de consumo da rota
	 * estao com leituraMovimento no mesmo
	 * anoMesReferencia e ciclo.
	 * 
	 * @param rota
	 *            the rota
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public boolean verificarConsistirLeitura(Rota rota, Integer anoMesReferencia, Integer numeroCiclo) throws NegocioException;

	/**
	 * Metodo responsavel por verificar se todos
	 * os pontos de consumo da rota
	 * estao com leituraMovimento no mesmo
	 * anoMesReferencia e ciclo.
	 * 
	 * @param rota
	 *            the rota
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public boolean verificarConsistirLeituraCorretorVazao(Rota rota, Integer anoMesReferencia, Integer numeroCiclo) throws NegocioException;

	/**
	 * Metodo responsavel por verificar se todos
	 * os pontos de consumo da rota
	 * estao com leituraMovimento no mesmo
	 * anoMesReferencia e ciclo.
	 * 
	 * @param rota
	 *            the rota
	 * @param cronogramaRota
	 *            the cronograma rota
	 * @param chaveTipoLeituraEletrocorretor
	 *            the chave tipo leitura eletrocorretor
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean verificarEmitirFatura(Rota rota, CronogramaRota cronogramaRota, Long chaveTipoLeituraEletrocorretor) throws NegocioException;

	/**
	 * Consultar movimentos leitura por rota.
	 * 
	 * @param chavesPrimarias
	 *            chaves primarias
	 * @param chavesSituacaoLeitura
	 * 			  chaves das situacoes de leitura
	 * @param referencia
	 *            referencia
	 * @param ciclo
	 *            ciclo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<LeituraMovimento> consultarMovimentosLeituraPorRota(Long[] chavesPrimarias, Long[] chavesSituacaoLeitura,
					Integer referencia, Integer ciclo) throws NegocioException;

	/**
	 * Método responsável por listar todas as
	 * Situações Leitura.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<SituacaoLeitura> listarSituacaoLeitura() throws NegocioException;

	/**
	 * Consultar leitura movimento.
	 * 
	 * @param filtro
	 *            the filtro
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<LeituraMovimento> consultarLeituraMovimento(Map<String, Object> filtro) throws NegocioException;

	/**
	 * Busca a data inicial da primeira leitura movimento da rota e do grupo de faturamento
	 * @param idRota id da rota
	 * @param idGrupoFaturamento id do grupo de faturamento
	 * @return retorna a data de inicio da leitura, caso não exista, nulo é retornado
	 */
	LocalDateTime buscarInicioLeituraMovimento(Long idRota, Long idGrupoFaturamento);

	/**
	 * Verificar faturar grupo corretor vazao.
	 * 
	 * @param rota
	 *            the rota
	 * @param anoMesReferencia
	 *            the ano mes referencia
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return true, if successful
	 * @throws NegocioException
	 *             the negocio exception
	 */
	boolean verificarFaturarGrupoCorretorVazao(Rota rota, Integer anoMesReferencia, Integer numeroCiclo) throws NegocioException;

	/**
	 * Remover pontos consumo qtd dias leitura.
	 * 
	 * @param pontosConsumo
	 *            the pontos consumo
	 * @param cronogramaRota
	 * 				the cronogramaRota
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerPontosConsumoQtdDiasLeitura(Collection<PontoConsumo> pontosConsumo,
			Map<Rota, CronogramaRota> cronogramaRota) throws NegocioException;

	/**
	 * Atualizar colecao.
	 * 
	 * @param anormalidadeLeitura - {@link Long} Array
	 * @param chaveRota
	 * 				the chaveRota
	 * @param anoMesFaturamento
	 * 				the anoMesFaturamento
	 * @param chaveLeiturista
	 * 				the chaveLeiturista
	 * @param dataLeitura
	 * 				the dataLeitura
	 * @param valorLeitura
	 * 				the valorLeitura
	 * @param valorPressao
	 * 				the valorPressao
	 * @param valorTemperatura
	 * 				the valorTemperatura
	 * @param anormaLidadeLeituraAnterior
	 * 				the anormaLidadeLeituraAnterior
	 * @param observacoesCadastrais
	 * 				the observacoesCadastrais
	 * @param chavesPrimarias
	 * 				the chavesPrimarias
	 * @throws GGASException
	 * 				the GGASException
	 */
	void atualizarColecao(Long chaveRota, Integer anoMesFaturamento, Long chaveLeiturista,
					String[] dataLeitura, Float[] valorLeitura, BigDecimal[] valorPressao, 
					BigDecimal[] valorTemperatura, Long[] anormalidadeLeitura, String[] observacoesCadastrais, 
					Long[] chavesPrimarias) throws GGASException;
	
	/**
	 * Consultar leitura movimento por rota.
	 *
	 * @param chaveRota the chave rota
	 * @param anoMesFaturamento the ano mes faturamento
	 * @param chavesPrimarias the chaves primarias
	 * @param pontoConsumo the ponto consumo
	 * @param ciclo the ciclo
	 * @return the collection
	 */
	Collection<LeituraMovimento> consultarLeituraMovimentoPorRota(Long chaveRota, Integer anoMesFaturamento,
					Long[] chavesPrimarias, Long pontoConsumo, Integer ciclo);

	/**
	 * Retorna a leitura de movimento com o status igual a 1 e que seja igual ao código legado passado
	 *
	 * @param codigoLegado pk do cliente no SGM
	 * @return a leitura de movimento para a API
	 * @throws PontoConsumoNaoEncontrado
	 * 									the PontoConsumoNaoEncontrado
	 * @throws LeituraMovimentoNaoEncontrada
	 * 									the LeituraMovimentoNaoEncontrada
	 * @throws MaisDeUmaLeituraMovimentoRetornada
	 * 									the MaisDeUmaLeituraMovimentoRetornada
	 * @throws CodigoLegadoEmVariosPontosConsumo
	 * 									the CodigoLegadoEmVariosPontosConsumo
	 */
	LeituraMovimento consultarLeituraMovimentoPorCodigoLegado(Long codigoLegado) throws PontoConsumoNaoEncontrado,
			LeituraMovimentoNaoEncontrada, MaisDeUmaLeituraMovimentoRetornada, CodigoLegadoEmVariosPontosConsumo;

	/**
	 * Consulta todas as rotas ativas
	 * @return A lista de todas as rotas ativas
	 */
	Collection<RotaDTO> consultarRotasAtivas();

	/**
	 * Método chamado sempreq que houver atualização em leitura movimento.
	 * 
	 * @param leituraMovimento - {@link LeituraMovimento}
	 * @param usuarioLogado - {@link usuarioLogado}
	 * @throws NegocioException - {@link NegocioException}
	 */
	void inserirHistoricoLeituraMovimento(LeituraMovimento leituraMovimento, Usuario usuarioLogado) throws NegocioException;

	/**
	 * Obtém a quantidade de pontos de consumo que estão em uma determinada etapa
	 * @param chaveSituacaoLeitura chave da situação da leitura
	 * @param grupoFaturamento chave do grupo de faturamento
	 * @param anoMesFaturamento ano mês faturamento
	 * @param ciclo número do ciclo
	 * @return retorna a quantidade de pontos de consumo que estão na atividade
	 */
	Long consultarQuantidadePontosConsumoNaAtividade(Long chaveSituacaoLeitura, Long grupoFaturamento, Integer anoMesFaturamento,
			Integer ciclo);

	/**
	 * Consulta todas as leituras movimento ativas de uma determinada rota.
	 * @param idRota A chave primária da rota
	 * @param anoMesFaturamento o anoMesFaturamento da rota
	 * @return A lista das leituras ativas da rota informada
	 */
	Collection<br.com.ggas.api.dto.LeituraMovimentoDTO> consultarLeiturasAtivasPorRota(Long idRota, Integer anoMesFaturamento);

	/**
	 * Atualiza todas as leituras movimento ativas de uma determinada rota para o status EM LEITURA
	 * @param idRota A chave primária da rota
	 * @param anoMesFaturamento o anoMesFaturamento da rota
	 */
	void atualizarLeiturasAtivasPorRota(Long idRota, Integer anoMesFaturamento);

	/**
	 * Metódo para consultar quantidade pontos consumo totais na atividade.
	 * 
	 * @param chaveSituacaoLeitura
	 * 							the chaveSituacaoLeitura
	 * @param grupoFaturamento
	 * 							the grupoFaturamento
	 * @param anoMesFaturamento
	 * 							the anoMesFaturamento
	 * @param ciclo
	 * 							the ciclo
	 * @return Long
	 * 
	 */
	Long consultarQuantidadePontosConsumoTotaisNaAtividade(Long chaveSituacaoLeitura, Long grupoFaturamento, Integer anoMesFaturamento,
													 Integer ciclo);
	
	/**
	 * Remover pontos consumo que ja tenham leitura movimento gerados para a referencia e ciclo
	 * 
	 * @param pontosConsumo
	 *            the pontos consumo
	 * @param grupo Grupo que o ponto de consumo pertence
	 * 			
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void removerPontosConsumoSemLeituraMovimento(Collection<PontoConsumo> pontosConsumo,
			GrupoFaturamento grupo) throws NegocioException;

	/**
	 * Remover Movimentos de Leitura que nao sao para faturar
	 * @param movimentos - {@link Collection}
	 * @param usuarioLogado - {@link Usuario}
	 * @throws NegocioException - the NegocioException
	 */
	void removerMovimentosNaoFaturar(Collection<LeituraMovimento> movimentos, Usuario usuarioLogado) throws NegocioException;
	
	/**
	 * Consultar Movimentos de Leitura nao processados por rota
	 * @param movimentos - {@link Collection}
	 * @throws NegocioException - the NegocioException
	 */

	Collection<LeituraMovimento> consultarMovimentosLeituraNaoProcessadosPorRota(Map<String, Object> parametros);

	boolean consultaLeituraMovimentoComAnormalidadeNaoFaturar(Long chavePontoConsumo, Integer anoMesFaturamento,
			Integer numeroCiclo);

	Long consultarQuantidadePontosConsumoConsistirLeitura(Integer anoMesReferencia, Integer ciclo, Long grupoFaturamento);

	Long consultarQuantidadePontosConsumoRegistrarLeitura(Integer anoMesReferencia, Integer ciclo, Long grupoFaturamento);

	Boolean atualizarLeituraMovimento(Element elemento, StringBuilder logLeiturasFalhas,
			SituacaoLeituraMovimento leituraRetornada, SituacaoLeituraMovimento emLeitura)
			throws ConcorrenciaException, NegocioException;

	Pair<List<LeituraMovimentoDTO>, Long> criarListaHistoricoLeituraMovimentoDTO(Long chavePontoConsumo);



}
