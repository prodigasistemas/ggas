/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.leitura;

import java.math.BigDecimal;
import java.util.Date;

import br.com.ggas.cadastro.funcionario.Funcionario;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.vazaocorretor.VazaoCorretor;
/**
 * Interface responsável pela assinatura dos métodos relacionados a instalação de medidor
 *
 */
public interface InstalacaoMedidor extends EntidadeNegocio {

	String BEAN_ID_INSTALACAO_MEDIDOR = "instalacaoMedidor";
	String FUNCIONARIO = "funcionario";

	/**
	 * @return the dataCorretorVazao
	 */
	Date getDataCorretorVazao();

	/**
	 * @param dataCorretorVazao
	 *            the dataCorretorVazao to set
	 */
	void setDataCorretorVazao(Date dataCorretorVazao);

	/**
	 * @return the data
	 */
	Date getData();

	/**
	 * @param data
	 *            the data to set
	 */
	void setData(Date data);

	/**
	 * @return the medidor
	 */
	Medidor getMedidor();

	/**
	 * @param medidor
	 *            the medidor to set
	 */
	void setMedidor(Medidor medidor);

	/**
	 * @return the localInstalacao
	 */
	MedidorLocalInstalacao getLocalInstalacao();

	/**
	 * @param localInstalacao
	 *            the localInstalacao to set
	 */
	void setLocalInstalacao(MedidorLocalInstalacao localInstalacao);

	/**
	 * @return the vazaoCorretor
	 */
	VazaoCorretor getVazaoCorretor();

	/**
	 * @param vazaoCorretor
	 *            the vazaoCorretor to set
	 */
	void setVazaoCorretor(VazaoCorretor vazaoCorretor);

	/**
	 * @return the pontoConsumo
	 */
	PontoConsumo getPontoConsumo();

	/**
	 * @param pontoConsumo
	 *            the pontoConsumo to set
	 */
	void setPontoConsumo(PontoConsumo pontoConsumo);

	/**
	 * @return the protecao
	 */
	MedidorProtecao getProtecao();

	/**
	 * @param protecao
	 *            the protecao to set
	 */
	void setProtecao(MedidorProtecao protecao);

	/**
	 * @return the leitura
	 */
	BigDecimal getLeitura();

	/**
	 * @param leitura
	 *            the leitura to set
	 */
	void setLeitura(BigDecimal leitura);

	/**
	 * @return the funcionario
	 */
	Funcionario getFuncionario();

	/**
	 * @param funcionario
	 *            the funcionario to set
	 */
	void setFuncionario(Funcionario funcionario);

	/**
	 * @return the dataAtivacao
	 */
	Date getDataAtivacao();

	/**
	 * @param dataAtivacao
	 *            the dataAtivacao to set
	 */
	void setDataAtivacao(Date dataAtivacao);

	/**
	 * @return the leituraAtivacao
	 */
	BigDecimal getLeituraAtivacao();

	/**
	 * @param leituraAtivacao
	 *            the leituraAtivacao to set
	 */
	void setLeituraAtivacao(BigDecimal leituraAtivacao);

}
