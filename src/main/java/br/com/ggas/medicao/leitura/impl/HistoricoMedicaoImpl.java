/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.leitura.impl;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.medicao.anormalidade.AnormalidadeLeitura;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.medicao.leitura.Leiturista;
import br.com.ggas.medicao.leitura.MedicaoHistoricoComentario;
import br.com.ggas.medicao.leitura.SituacaoLeitura;
import br.com.ggas.medicao.vazaocorretor.Unidade;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
/**
 * Classe responsável pela implementação dos métodos relacionados
 * aos Históricos de Medição
 *
 */
public class HistoricoMedicaoImpl extends EntidadeNegocioImpl implements HistoricoMedicao {

	private static final long serialVersionUID = 8546570427101043289L;

	private Integer anoMesLeitura;

	private Integer sequenciaLeitura;

	private Integer numeroCiclo;

	private Date dataLeituraInformada;

	private BigDecimal numeroLeituraInformada;

	private Date dataLeituraFaturada;

	private BigDecimal numeroLeituraFaturada;

	private Leiturista leiturista;

	private AnormalidadeLeitura anormalidadeLeituraInformada;

	private AnormalidadeLeitura anormalidadeLeituraFaturada;

	private SituacaoLeitura situacaoLeitura;

	private InstalacaoMedidor historicoInstalacaoMedidor;

	private Date dataProcessamento;

	private PontoConsumo pontoConsumo;

	private boolean analisada;

	private BigDecimal consumoInformado;

	private BigDecimal numeroLeituraAnterior;

	private Date dataLeituraAnterior;

	private AnormalidadeLeitura anormalidadeLeituraAnterior;

	private BigDecimal creditoGerado;

	private BigDecimal creditoSaldo;

	private BigDecimal numeroLeituraCorretor;

	private BigDecimal consumoCorretor;

	private BigDecimal consumoMedidor;

	private BigDecimal fatorPTZCorretor;

	private Boolean indicadorInformadoCliente;

	private BigDecimal pressaoInformada;

	private BigDecimal temperaturaInformada;

	private Unidade unidadePressaoInformada;

	private Unidade unidadeTemperaturaInformada;

	private Collection<MedicaoHistoricoComentario> listaMedicaoHistoricoComentario = new HashSet<MedicaoHistoricoComentario>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getCreditoVolume()
	 */
	@Override
	public BigDecimal getCreditoVolume() {

		BigDecimal retorno = null;
		if(creditoGerado != null && creditoSaldo != null) {
			retorno = creditoGerado.add(creditoSaldo);
		} else if(creditoGerado == null && creditoSaldo != null) {
			retorno = creditoSaldo;
		} else if(creditoGerado != null) {
			retorno = creditoGerado;
		}
		return retorno;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getCreditoGerado()
	 */
	@Override
	public BigDecimal getCreditoGerado() {

		return creditoGerado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #setCreditoGerado(java.math.BigDecimal)
	 */
	@Override
	public void setCreditoGerado(BigDecimal creditoGerado) {

		this.creditoGerado = creditoGerado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getCreditoSaldo()
	 */
	@Override
	public BigDecimal getCreditoSaldo() {

		return creditoSaldo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #setCreditoSaldo(java.math.BigDecimal)
	 */
	@Override
	public void setCreditoSaldo(BigDecimal creditoSaldo) {

		this.creditoSaldo = creditoSaldo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #isAnalisada()
	 */
	@Override
	public boolean isAnalisada() {

		return analisada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #setAnalisada(boolean)
	 */
	@Override
	public void setAnalisada(boolean analisada) {

		this.analisada = analisada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #getAnoMesLeitura()
	 */
	@Override
	public Integer getAnoMesLeitura() {

		return anoMesLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getAnoMesLeituraFormatado()
	 */
	@Override
	public String getAnoMesLeituraFormatado() {

		return Util.formatarAnoMes(anoMesLeitura);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getAnoMesLeituraFormatado()
	 */
	@Override
	public Integer getAnoMesCicloFormatado() {

		String anoMesCiclo = String.valueOf(anoMesLeitura) + String.valueOf(numeroCiclo);
		return Integer.valueOf(anoMesCiclo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #setAnoMesLeitura(java.lang.Integer)
	 */
	@Override
	public void setAnoMesLeitura(Integer anoMesLeitura) {

		this.anoMesLeitura = anoMesLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #getSequenciaLeitura()
	 */
	@Override
	public Integer getSequenciaLeitura() {

		return sequenciaLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #setSequenciaLeitura(java.lang.Integer)
	 */
	@Override
	public void setSequenciaLeitura(Integer sequenciaLeitura) {

		this.sequenciaLeitura = sequenciaLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getNumeroCiclo()
	 */
	@Override
	public Integer getNumeroCiclo() {

		return numeroCiclo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #setNumeroCiclo(java.lang.Integer)
	 */
	@Override
	public void setNumeroCiclo(Integer numeroCiclo) {

		this.numeroCiclo = numeroCiclo;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #getDataLeituraInformada()
	 */
	@Override
	public Date getDataLeituraInformada() {
		Date data = null;
		if (this.dataLeituraInformada != null) {
			data = (Date) dataLeituraInformada.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #setDataLeituraInformada(java.util.Date)
	 */
	@Override
	public void setDataLeituraInformada(Date dataLeituraInformada) {
		if(dataLeituraInformada != null){
			this.dataLeituraInformada = (Date) dataLeituraInformada.clone();
		} else {
			this.dataLeituraInformada = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #getNumeroLeituraInformada()
	 */
	@Override
	public BigDecimal getNumeroLeituraInformada() {

		return numeroLeituraInformada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #
	 * setNumeroLeituraInformada(java.math.BigDecimal
	 * )
	 */
	@Override
	public void setNumeroLeituraInformada(BigDecimal numeroLeituraInformada) {

		this.numeroLeituraInformada = numeroLeituraInformada;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #getDataLeituraFaturada()
	 */
	@Override
	public Date getDataLeituraFaturada() {
		Date data = null;
		if (this.dataLeituraFaturada != null) {
			data = (Date) dataLeituraFaturada.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #setDataLeituraFaturada(java.util.Date)
	 */
	@Override
	public void setDataLeituraFaturada(Date dataLeituraFaturada) {
		if(dataLeituraFaturada != null){
			this.dataLeituraFaturada = (Date) dataLeituraFaturada.clone();
		} else {
			this.dataLeituraFaturada = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #getNumeroLeituraFaturada()
	 */
	@Override
	public BigDecimal getNumeroLeituraFaturada() {

		return numeroLeituraFaturada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #
	 * setNumeroLeituraFaturada(java.math.BigDecimal
	 * )
	 */
	@Override
	public void setNumeroLeituraFaturada(BigDecimal numeroLeituraFaturada) {

		this.numeroLeituraFaturada = numeroLeituraFaturada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #getLeiturista()
	 */
	@Override
	public Leiturista getLeiturista() {

		return leiturista;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #setLeiturista(br.com.ggas.cadastro.imovel.
	 * Leiturista)
	 */
	@Override
	public void setLeiturista(Leiturista leiturista) {

		this.leiturista = leiturista;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #getAnormalidadeLeituraInformada()
	 */
	@Override
	public AnormalidadeLeitura getAnormalidadeLeituraInformada() {

		return anormalidadeLeituraInformada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #
	 * setAnormalidadeLeituraInformada(br.com.ggas
	 * .medicao.leitura.AnormalidadeLeitura)
	 */
	@Override
	public void setAnormalidadeLeituraInformada(AnormalidadeLeitura anormalidadeLeituraInformada) {

		this.anormalidadeLeituraInformada = anormalidadeLeituraInformada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #getAnormalidadeLeituraFaturada()
	 */
	@Override
	public AnormalidadeLeitura getAnormalidadeLeituraFaturada() {

		return anormalidadeLeituraFaturada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #
	 * setAnormalidadeLeituraFaturada(br.com.ggas.
	 * medicao.leitura.AnormalidadeLeitura)
	 */
	@Override
	public void setAnormalidadeLeituraFaturada(AnormalidadeLeitura anormalidadeLeituraFaturada) {

		this.anormalidadeLeituraFaturada = anormalidadeLeituraFaturada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #getSituacaoLeitura()
	 */
	@Override
	public SituacaoLeitura getSituacaoLeitura() {

		return situacaoLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #
	 * setSituacaoLeitura(br.com.ggas.medicao.leitura
	 * .SituacaoLeitura)
	 */
	@Override
	public void setSituacaoLeitura(SituacaoLeitura situacaoLeitura) {

		this.situacaoLeitura = situacaoLeitura;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getHistoricoInstalacaoMedidor()
	 */
	@Override
	public InstalacaoMedidor getHistoricoInstalacaoMedidor() {

		return historicoInstalacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #setHistoricoInstalacaoMedidor(br.com.ggas.
	 * medicao.leitura.InstalacaoMedidor)
	 */
	@Override
	public void setHistoricoInstalacaoMedidor(InstalacaoMedidor historicoInstalacaoMedidor) {

		this.historicoInstalacaoMedidor = historicoInstalacaoMedidor;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #getDataProcessamento()
	 */
	@Override
	public Date getDataProcessamento() {
		Date data = null;
		if(this.dataProcessamento != null) {
			data = (Date) dataProcessamento.clone();
		}
		return data;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.medicao.leitura.MedicaoHistorico
	 * #setDataProcessamento(java.util.Date)
	 */
	@Override
	public void setDataProcessamento(Date dataProcessamento) {
		if (dataProcessamento != null) {
			this.dataProcessamento = (Date) dataProcessamento.clone();
		} else {
			this.dataProcessamento = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getPontoConsumo()
	 */
	@Override
	public PontoConsumo getPontoConsumo() {

		return pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #
	 * setPontoConsumo(br.com.ggas.cadastro.imovel
	 * .PontoConsumo)
	 */
	@Override
	public void setPontoConsumo(PontoConsumo pontoConsumo) {

		this.pontoConsumo = pontoConsumo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getConsumoInformado()
	 */
	@Override
	public BigDecimal getConsumoInformado() {

		return consumoInformado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #setConsumoInformado(java.math.BigDecimal)
	 */
	@Override
	public void setConsumoInformado(BigDecimal consumoInformado) {

		this.consumoInformado = consumoInformado;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getNumeroLeituraAnterior()
	 */
	@Override
	public BigDecimal getNumeroLeituraAnterior() {

		return numeroLeituraAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #
	 * setNumeroLeituraAnterior(java.math.BigDecimal
	 * )
	 */
	@Override
	public void setNumeroLeituraAnterior(BigDecimal numeroLeituraAnterior) {

		this.numeroLeituraAnterior = numeroLeituraAnterior;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getDataLeituraAnterior()
	 */
	@Override
	public Date getDataLeituraAnterior() {
		Date data = null;
		if (this.dataLeituraAnterior != null) {
			data = (Date) dataLeituraAnterior.clone();
		}
		return data;
	}

	@Override
	public void setDataLeituraAnterior(Date dataLeituraAnterior) {
		if(dataLeituraAnterior != null){
			this.dataLeituraAnterior = (Date) dataLeituraAnterior.clone();
		} else {
			this.dataLeituraAnterior = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #getAnormalidadeLeituraAnterior()
	 */
	@Override
	public AnormalidadeLeitura getAnormalidadeLeituraAnterior() {

		return anormalidadeLeituraAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.medicao.leitura.HistoricoMedicao
	 * #
	 * setAnormalidadeLeituraAnterior(br.com.ggas.
	 * medicao.anormalidade.AnormalidadeLeitura)
	 */
	@Override
	public void setAnormalidadeLeituraAnterior(AnormalidadeLeitura anormalidadeLeituraAnterior) {

		this.anormalidadeLeituraAnterior = anormalidadeLeituraAnterior;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder stringBuilder = new StringBuilder();
		String camposObrigatorios = null;

		if(anoMesLeitura == null || anoMesLeitura <= 0) {
			stringBuilder.append(ANO_MES_LEITURA);
			stringBuilder.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = stringBuilder.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, stringBuilder.toString().length() - 2));
		}
		return erros;
	}

	/**
	 * @return the numeroLeituraCorretor
	 */
	@Override
	public BigDecimal getNumeroLeituraCorretor() {

		return numeroLeituraCorretor;
	}

	/**
	 * @param numeroLeituraCorretor
	 *            the numeroLeituraCorretor to set
	 */
	@Override
	public void setNumeroLeituraCorretor(BigDecimal numeroLeituraCorretor) {

		this.numeroLeituraCorretor = numeroLeituraCorretor;
	}

	/**
	 * @return the consumoCorretor
	 */
	@Override
	public BigDecimal getConsumoCorretor() {

		return consumoCorretor;
	}

	/**
	 * @param consumoCorretor
	 *            the consumoCorretor to set
	 */
	@Override
	public void setConsumoCorretor(BigDecimal consumoCorretor) {

		this.consumoCorretor = consumoCorretor;
	}

	/**
	 * @return the consumoMedidor
	 */
	@Override
	public BigDecimal getConsumoMedidor() {

		return consumoMedidor;
	}

	/**
	 * @param consumoMedidor
	 *            the consumoMedidor to set
	 */
	@Override
	public void setConsumoMedidor(BigDecimal consumoMedidor) {

		this.consumoMedidor = consumoMedidor;
	}

	/**
	 * @return the fatorPTZCorretor
	 */
	@Override
	public BigDecimal getFatorPTZCorretor() {

		return fatorPTZCorretor;
	}

	/**
	 * @param fatorPTZCorretor
	 *            the fatorPTZCorretor to set
	 */
	@Override
	public void setFatorPTZCorretor(BigDecimal fatorPTZCorretor) {

		this.fatorPTZCorretor = fatorPTZCorretor;
	}

	/**
	 * @return the indicadorInformadoCliente
	 */
	@Override
	public Boolean getIndicadorInformadoCliente() {

		return indicadorInformadoCliente;
	}

	/**
	 * @param indicadorInformadoCliente
	 *            the indicadorInformadoCliente to
	 *            set
	 */
	@Override
	public void setIndicadorInformadoCliente(Boolean indicadorInformadoCliente) {

		this.indicadorInformadoCliente = indicadorInformadoCliente;
	}

	@Override
	public BigDecimal getPressaoInformada() {

		return pressaoInformada;
	}

	@Override
	public void setPressaoInformada(BigDecimal pressaoInformada) {

		this.pressaoInformada = pressaoInformada;
	}

	@Override
	public BigDecimal getTemperaturaInformada() {

		return temperaturaInformada;
	}

	@Override
	public void setTemperaturaInformada(BigDecimal temperaturaInformada) {

		this.temperaturaInformada = temperaturaInformada;
	}

	@Override
	public Unidade getUnidadePressaoInformada() {

		return unidadePressaoInformada;
	}

	@Override
	public void setUnidadePressaoInformada(Unidade unidadePressaoInformada) {

		this.unidadePressaoInformada = unidadePressaoInformada;
	}

	@Override
	public Unidade getUnidadeTemperaturaInformada() {

		return unidadeTemperaturaInformada;
	}

	@Override
	public void setUnidadeTemperaturaInformada(Unidade unidadeTemperaturaInformada) {

		this.unidadeTemperaturaInformada = unidadeTemperaturaInformada;
	}

	@Override
	public Collection<MedicaoHistoricoComentario> getListaMedicaoHistoricoComentario() {
		return listaMedicaoHistoricoComentario;
	}

	@Override
	public void setListaMedicaoHistoricoComentario(Collection<MedicaoHistoricoComentario> listaMedicaoHistoricoComentario) {
		this.listaMedicaoHistoricoComentario = listaMedicaoHistoricoComentario;
	}

	@Override
	public boolean getExisteComentario() {
		// TODO Auto-generated method stub
		return listaMedicaoHistoricoComentario != null && listaMedicaoHistoricoComentario.size() > 0;
	}


}
