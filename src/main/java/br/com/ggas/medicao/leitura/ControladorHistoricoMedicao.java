/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.leitura;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.faturamento.tarifa.TarifaPontoConsumo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.medidor.Medidor;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.web.medicao.leitura.HistoricoLeituraGSAVO;

/**
 * The Interface ControladorHistoricoMedicao.
 */
public interface ControladorHistoricoMedicao extends ControladorNegocio {

	/** The bean id controlador historico medicao. */
	String BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO = "controladorHistoricoMedicao";

	/** The erro negocio ponto de consumo sem historico medicao. */
	String ERRO_NEGOCIO_PONTO_DE_CONSUMO_SEM_HISTORICO_MEDICAO = "ERRO_NEGOCIO_PONTO_DE_CONSUMO_SEM_HISTORICO_MEDICAO";

	/** The erro negocio comentario alteracao dados leitura nao informado. */
	String ERRO_NEGOCIO_COMENTARIO_ALTERACAO_DADOS_LEITURA_NAO_INFORMADO = "ERRO_NEGOCIO_COMENTARIO_ALTERACAO_DADOS_LEITURA_NAO_INFORMADO";

	/** The erro negocio data realizacao leitura invalida. */
	String ERRO_NEGOCIO_DATA_REALIZACAO_LEITURA_INVALIDA = "ERRO_NEGOCIO_DATA_REALIZACAO_LEITURA_INVALIDA";

	/** The erro negocio referencia nao informada. */
	String ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA = "ERRO_NEGOCIO_REFERENCIA_NAO_INFORMADA";

	/** The erro negocio data leitura anterior invalida. */
	String ERRO_NEGOCIO_DATA_LEITURA_ANTERIOR_INVALIDA = "ERRO_NEGOCIO_DATA_LEITURA_ANTERIOR_INVALIDA";

	/** The erro negocio data leitura atual invalida. */
	String ERRO_NEGOCIO_DATA_LEITURA_ATUAL_INVALIDA = "ERRO_NEGOCIO_DATA_LEITURA_ATUAL_INVALIDA";

	/** The erro negocio data leitura atual invalida. */
	String ERRO_NEGOCIO_LEITURA_ATUAL_INVALIDA = "ERRO_NEGOCIO_LEITURA_ATUAL_INVALIDA";

	/** The erro negocio leitura excede limite digitos medidor. */
	String ERRO_NEGOCIO_LEITURA_EXCEDE_LIMITE_DIGITOS_MEDIDOR = "ERRO_NEGOCIO_LEITURA_EXCEDE_LIMITE_DIGITOS_MEDIDOR";

	/** The erro negocio numero leitura anterior invalida. */
	String ERRO_NEGOCIO_NUMERO_LEITURA_ANTERIOR_INVALIDA = "ERRO_NEGOCIO_NUMERO_LEITURA_ANTERIOR_INVALIDA";

	/** The erro negocio ja existe registro medicao para data informada. */
	String ERRO_NEGOCIO_JA_EXISTE_REGISTRO_MEDICAO_PARA_DATA_INFORMADA = "ERRO_NEGOCIO_JA_EXISTE_REGISTRO_MEDICAO_PARA_DATA_INFORMADA";

	/**
	 * Método responsável por consultar o
	 * histórico de medição pela instalação do
	 * medidor
	 * que possua a maior data de leitura
	 * faturada.
	 * 
	 * @param chaveInstalacaoMedidor
	 *            Chave primária da instalação do
	 *            medidor.
	 * @return the historico medicao
	 */
	HistoricoMedicao obterHistoricoMedicaoPorInstalacaoMedidor(Long chaveInstalacaoMedidor);

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave primaria do ponto de
	 *            consumo
	 * @return Uma coleção de histórico
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoMedicao> consultarHistoricoMedicao(Long chavePrimariaPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo, agrupando
	 * por referência e ciclo.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave primaria do ponto de
	 *            consumo
	 * @param referenciaInicial
	 *            Referência Inicial
	 * @param referenciaFinal
	 *            Referência Final
	 * @return Uma coleção de histórico
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoMedicao> consultarHistoricoAgrupadoMedicao(Long chavePrimariaPontoConsumo, String referenciaInicial,
					String referenciaFinal) throws NegocioException;

	/**
	 * Método responsável por consultar os
	 * historicos do ponto de consumo, agrupando
	 * por referência elimitado por ciclo.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave primaria do ponto de
	 *            consumo
	 * @param referenciaInicial
	 *            Referência Inicial
	 * @param referenciaFinal
	 *            Referência Final
	 * @param quantidadeCiclos
	 *            the quantidade ciclos
	 * @return Uma coleção de histórico
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoMedicao> consultarHistoricoAgrupadoMedicao(Long chavePrimariaPontoConsumo, String referenciaInicial,
					String referenciaFinal, Integer quantidadeCiclos) throws NegocioException;

	/**
	 * Método reponsável por validar se a colecao
	 * de historico do ponto de consumo está vazia.
	 * 
	 * @param listaHistoricoMedicao
	 *            A lista de historicos
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarPontoConsumoSemHistoricoMedicao(Collection<HistoricoMedicao> listaHistoricoMedicao) throws NegocioException;

	/**
	 * Método responsável por obter a ultima
	 * leitura do ponto de consumo no ciclo.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave do ponto de consumo
	 * @param referencia
	 *            A referência
	 * @param ciclo
	 *            O ciclo
	 * @return Uma medição
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	HistoricoMedicao obterUltimoHistoricoMedicao(Long chavePrimariaPontoConsumo, int referencia, int ciclo) throws NegocioException;

	/**
	 * Método responsável por obter a ultima
	 * leitura do ponto de consumo.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave do ponto de consumo
	 * @return Uma medição
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	HistoricoMedicao obterUltimoHistoricoMedicao(Long chavePrimariaPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter o histórico
	 * medição de um ponto de consumo em relação
	 * aos filtros informados.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave do ponto de consumo
	 * @param referencia
	 *            the referencia
	 * @param ciclo
	 *            the ciclo
	 * @param isFaturado
	 *            Indica se a consulta deve
	 *            considerar as medições
	 *            faturadas, não faturadas ou
	 *            ambas.
	 * @return Uma medição
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	HistoricoMedicao obterUltimoHistoricoMedicao(Long chavePrimariaPontoConsumo, Integer referencia, Integer ciclo, Boolean isFaturado)
					throws NegocioException;

	/**
	 * Método responsável por obter a primeiro
	 * leitura do ponto de consumo.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave do ponto de consumo
	 * @return Uma medição
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	HistoricoMedicao obterPrimeiroHistoricoMedicao(Long chavePrimariaPontoConsumo) throws NegocioException;

	/**
	 * Método responsável por obter a ultima
	 * leitura do ponto de consumo no ciclo.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave do ponto de consumo
	 * @param referencia
	 *            A referência
	 * @param ciclo
	 *            O ciclo
	 * @return the collection
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoMedicao> consultarHistoricoMedicao(Long chavePrimariaPontoConsumo, int referencia, int ciclo)
					throws NegocioException;
	
	/**
	 * Método responsável por obter a ultima
	 * leitura do medidor no ciclo.
	 *
	 * @param medidor the medidor
	 * @param referencia            A referência
	 * @param ciclo            O ciclo
	 * @return the collection
	 * @throws NegocioException             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Collection<HistoricoMedicao> consultarHistoricoMedicaoPorMedidor(Medidor medidor, int referencia, int ciclo)
					throws NegocioException;

	/**
	 * Consultar historico medicao nao faturado.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            the chave primaria ponto consumo
	 * @param referencia
	 *            the referencia
	 * @param ciclo
	 *            the ciclo
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoMedicao> consultarHistoricoMedicaoNaoFaturado(Long chavePrimariaPontoConsumo, int referencia, int ciclo)
					throws NegocioException;

	/**
	 * Método responsável por atualizar os dados
	 * de faturamento de um histórico
	 * de ponto consumo e adicionar um comentário
	 * para o histórido de medição.
	 *
	 * @param pontoConsumo            Ponto de consumo impactado.
	 * @param historicoMedicao            O histórico de medição a ser
	 *            alterado.
	 * @param medicaoHistoricoComentario            O comentário de alteração do
	 *            histórico de medição.
	 * @throws GGASException the GGAS exception
	 * @throws NumberFormatException the number format exception
	 */
	void atualizarDadosFaturamentoLeitura(PontoConsumo pontoConsumo, HistoricoMedicao historicoMedicao,
					MedicaoHistoricoComentario medicaoHistoricoComentario, boolean executarConsistir) throws GGASException;

	/**
	 * Método responsável por validar a exigência
	 * de comentário para a análise
	 * de exceção.
	 *
	 * @return true caso for exigido.
	 * @throws NegocioException             Caso ocorra algum erro na
	 *             invocação do método.
	 * @throws NumberFormatException             Caso ocorra algum erro de
	 *             conversão de número.
	 */
	boolean exigeComentarioAnaliseExcecao() throws NegocioException;

	/**
	 * Método responsável por buscar a última data
	 * de leitura do ano/mês
	 * informado.
	 * 
	 * @param anoMes
	 *            Um ano/Mês.
	 * @return Uma data.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Date obterDataUltimaHistoricoMedicaoAnoMes(Integer anoMes) throws NegocioException;

	/**
	 * Método reponsável por verificar se houve
	 * uma concessao de crédito.
	 * 
	 * @param idHistoricoMedicao
	 *            A chave primária do histórico
	 *            medição
	 * @param valorLeituraAnteriorModificado
	 *            O valor modificado da leitura
	 *            anterior
	 * @return Um array com os valores, posição
	 *         [0] a difereça creditada, [1] o
	 *         crrédito (saldo + diferença)
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	BigDecimal[] verificarConcessaoCredito(Long idHistoricoMedicao, BigDecimal valorLeituraAnteriorModificado) throws NegocioException;

	/**
	 * Método responsável por calcular o crédito
	 * derado.
	 * 
	 * @param historicoMedicao
	 *            the historico medicao
	 * @param creditoVolume
	 *            O crédito volume
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void calcularCreditoGerado(HistoricoMedicao historicoMedicao, BigDecimal creditoVolume) throws NegocioException;

	/**
	 * Obter historico medicao por tarifa ponto consumo.
	 * 
	 * @param tarifaPontoConsumo
	 *            the tarifa ponto consumo
	 * @return the collection
	 */
	Collection<HistoricoMedicao> obterHistoricoMedicaoPorTarifaPontoConsumo(TarifaPontoConsumo tarifaPontoConsumo);

	/**
	 * Método que cria a entidade
	 * MedicaoHistoricoComentario.
	 * 
	 * @return Instância de
	 *         MedicaoHistoricoComentario
	 */
	EntidadeNegocio criarMedicaoHistoricoComentario();

	/**
	 * Método responsável por obter todos os
	 * comentários para o ponto de consumo na
	 * referência/ciclo informada.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave do ponto de consumo
	 * @param referencia
	 *            A referência
	 * @param ciclo
	 *            O ciclo
	 * @return Uma coleção de Medição Histórico
	 *         Comentário
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<MedicaoHistoricoComentario> consultarMedicaoHistoricoComentario(Long chavePrimariaPontoConsumo, int referencia, int ciclo)
					throws NegocioException;

	/**
	 * Método responsável por verificar se existe
	 * comentário para os parâmetros informados;.
	 * 
	 * @param chavePrimariaPontoConsumo
	 *            A chave do ponto de consumo
	 * @param referencia
	 *            A referência
	 * @param ciclo
	 *            O ciclo
	 * @return True caso exista, False caso não
	 *         exista.
	 * @throws GGASException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	boolean existeMedicaoHistoricoComentario(Long chavePrimariaPontoConsumo, int referencia, int ciclo) throws GGASException;

	/**
	 * Método responsável por validar o número de
	 * dígitos do medidor.
	 * 
	 * @param historicoMedicao
	 *            Histórico de medição.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarMedidorHistoricoMedicao(HistoricoMedicao historicoMedicao) throws NegocioException;

	/**
	 * Método responsável por validar as datas do
	 * histórico de medição.
	 * 
	 * @param historicoMedicao
	 *            Histórico de medição.
	 * @throws NegocioException
	 *             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	void validarDatasHistoricoMedicao(HistoricoMedicao historicoMedicao) throws NegocioException;

	/**
	 * Método responsável por setar
	 * dataLeituraAnterior do histórico de
	 * medição.
	 *
	 * @param instalacaoMedidor            the instalacao medidor
	 * @param pontoConsumo the ponto consumo
	 * @param medidor the medidor
	 * @return the date
	 * @throws NegocioException             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	Date setarDataLeituraAnterior(InstalacaoMedidor instalacaoMedidor,
					PontoConsumo pontoConsumo, Medidor medidor) throws NegocioException;

	/**
	 * Método responsável por setar
	 * LeituraAnterior do histórico de medição.
	 *
	 * @param instalacaoMedidor            the instalacao medidor
	 * @param pontoConsumo the ponto consumo
	 * @param medidor the medidor
	 * @return the big decimal
	 * @throws NegocioException             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	BigDecimal setarLeituraAnterior(InstalacaoMedidor instalacaoMedidor,
					PontoConsumo pontoConsumo, Medidor medidor) throws NegocioException;

	/**
	 * Método responsável por consultar a data de
	 * leitura do último dia do ciclo anterior.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param anoMesLeitura
	 *            the ano mes leitura
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return dataLeituraUltimoDiaCicloAnterior
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Date consultarDataLeituraUltimoDiaCicloAnterior(PontoConsumo pontoConsumo, Integer anoMesLeitura, Integer numeroCiclo)
					throws NegocioException;

	/**
	 * Validar data atual informada.
	 * 
	 * @param historicoMedicao
	 *            the historico medicao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void validarDataAtualInformada(HistoricoMedicao historicoMedicao) throws NegocioException;

	/**
	 * Metodos validar leitura atual informada.
	 * 
	 * @param historicoMedicao
	 * 						the historicoMedicao
	 * 
	 * @throws NegocioException
	 * 						the NegocioException
	 */
	void validarLeituraAtualInformada(HistoricoMedicao historicoMedicao) throws NegocioException;

	/**
	 * Método responsável por listar todos os
	 * tipos de medicao.
	 * 
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<TipoMedicao> listarTipoMedicao() throws NegocioException;

	/**
	 * Método responsável por consultar
	 * HistoricoMedicaodo do último dia do ciclo
	 * anterior.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param anoMesLeitura
	 *            the ano mes leitura
	 * @param numeroCiclo
	 *            the numero ciclo
	 * @return the historico medicao
	 * @throws NegocioException
	 *             the negocio exception
	 */
	HistoricoMedicao consultarHistoricoMedicaoUltimoDiaCicloAnterior(PontoConsumo pontoConsumo, Integer anoMesLeitura, Integer numeroCiclo)
					throws NegocioException;

	/**
	 * Listar historico medicao por ponto consumo.
	 * 
	 * @param pontoConsumo
	 *            the ponto consumo
	 * @param dataleitura
	 *            the dataleitura
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoMedicao> listarHistoricoMedicaoPorPontoConsumo(PontoConsumo pontoConsumo, Date dataleitura) throws NegocioException;

	/**
	 * Consultar historico medicao.
	 *
	 * @param chavePrimariaPontoConsumo            the chave primaria ponto consumo
	 * @param dataRealizacaoLeitura            the data realizacao leitura
	 * @param medidor the medidor
	 * @return the collection
	 * @throws NegocioException             the negocio exception
	 */
	Collection<HistoricoMedicao> consultarHistoricoMedicaoPorPontoConsumoOuMedidor
		(Long chavePrimariaPontoConsumo, Date dataRealizacaoLeitura, Medidor medidor)
					throws NegocioException;

	/**
	 * Consultar historico medicao.
	 * 
	 * @param filtro
	 *            the filtro
	 * @param ordenacao
	 *            the ordenacao
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoMedicao> consultarHistoricoMedicao(Map<String, Object> filtro, String ordenacao, String... propriedadesLazy)
					throws NegocioException;

	/**
	 * Consultar historico medicao.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @param dataLeituraMenorInformada
	 *            the data leitura menor informada
	 * @param dataLeituraMaiorInformada
	 *            the data leitura maior informada
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<HistoricoMedicao> consultarHistoricoMedicao(Long chavePrimaria, Date dataLeituraMenorInformada,
					Date dataLeituraMaiorInformada) throws NegocioException;

	/**
	 * Consultar historico agrupado ano mes.
	 * 
	 * @param chavePrimariaContrato
	 *            the chave primaria contrato
	 * @param dataLeituraMenorInformada
	 *            the data leitura menor informada
	 * @param dataLeituraMaiorInformada
	 *            the data leitura maior informada
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Object> consultarHistoricoAgrupadoAnoMes(Long chavePrimariaContrato, Date dataLeituraMenorInformada,
					Date dataLeituraMaiorInformada) throws NegocioException;

	/**
	 * Inativar historico medicao.
	 * 
	 * @param idHistoricoMedicao
	 *            the id historico medicao
	 * @return the historico medicao
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	HistoricoMedicao inativarHistoricoMedicao(Long idHistoricoMedicao) throws NegocioException, ConcorrenciaException;

	/**
	 * Consulta lista de historico medicao por medidor.
	 *
	 * @param medidor the medidor
	 * @param dataleitura the dataleitura
	 * @return the collection
	 * @throws NegocioException the negocio exception
	 */
	Collection<HistoricoMedicao> listarHistoricoMedicaoPorMedidor(Medidor medidor, Date dataleitura) throws NegocioException;

	/**
	 * Método responsável por obter a ultima
	 * 
	 * leitura do medidor no ciclo.
	 *
	 * @param medidor the medidor
	 * @param referencia            A referência
	 * @param ciclo            O ciclo
	 * @param isFaturado the is faturado
	 * @return Uma medição
	 * @throws NegocioException             Caso ocorra algum erro na
	 *             invocação do método.
	 */
	HistoricoMedicao obterUltimoHistoricoMedicao(Medidor medidor, Integer referencia, Integer ciclo, Boolean isFaturado)
					throws NegocioException;

	/**
	 * Obtém a quantidade de entidades de HistoricoMedicao, filtrando a consulta
	 * de acordo com os parametros fornecidos.
	 *
	 * @param chavePrimariaPontoConsumo the chave primaria ponto consumo
	 * @param dataRealizacaoLeitura the data realizacao leitura
	 * @param medidor the medidor
	 * @return Long - quantidade de registros de HistoricoMedicao
	 * @throws NegocioException the negocio exception
	 */
	Long obterQuantidadeDoHistoricoMedicaoPorPontoConsumoOuMedidor(Long chavePrimariaPontoConsumo, Date dataRealizacaoLeitura,
		Medidor medidor) throws NegocioException;

	/**
	 * Obtém o último histórico de medição faturado, dos pontos de consumo
	 * especificados no parâmetro.
	 *
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @return mapa de histórico de medição por ponto de consumo
	 */
	Map<PontoConsumo, HistoricoMedicao> obterUltimoHistoricoMedicaoFaturado(Long[] chavesPontoConsumo);

	/**
	 * Obtém os históricos de medição não faturados por ponto de consumo.
	 *
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @param referencia the referencia
	 * @param ciclo the ciclo
	 * @return mapa de históricos de medição por ponto de consumo
	 * @throws NegocioException the negocio exception
	 */
	Map<PontoConsumo, Collection<HistoricoMedicao>> consultarHistoricoMedicaoNaoFaturado(Long[] chavesPontoConsumo, int referencia,
					int ciclo) throws NegocioException;

	/**
	 * Obtém uma coleção de históricos de medição por chave primária do ponto
	 * de consumo.
	 *
	 * @param chavesPontoConsumo the chaves ponto consumo
	 * @return mapa de históricos de medição por ponto de consumo
	 */
	Map<PontoConsumo, Collection<HistoricoMedicao>> consultarHistoricoMedicao(Long[] chavesPontoConsumo);

	/**
	 * Obtém a data de leitura do histórico de medição.
	 * 
	 * @param pontosDeConsumo - {@link Collection}
	 * @return históricos por chave de pontos de consumo - {@link Map}
	 */
	Map<Long, Collection<HistoricoMedicao>> obterDataLeituraDeUltimoHistoricoMedicao(
			Collection<PontoConsumo> pontosDeConsumo);

	/**
	 * Obtém do último histórico de medição faturado, os atributos: {@code dataLeituraFaturada},
	 * {@code numeroLeituraFaturada}, {@code anormalidadeLeituraFaturada}, {@code dataLeituraInformada},
	 * {@code numeroLeituraInformada}, {@code chavePrimaria}, {@code situacaoLeitura}.
	 * 
	 * @param chavesPontoConsumo {@link Long[]}
	 * @return mapa de histórico de medição por ponto de consumo
	 */
	Map<Long, Collection<HistoricoMedicao>> obterAtributosDoUltimoHistoricoMedicaoFaturado(Long[] chavesPontoConsumo);

	/**
	 * Obtém uma coleção de históricos de medição por chave primária do ponto
	 * de consumo. (Medição não consistidas)
	 * @param chavesPontoConsumo {@link Long[]}
	 * @return mapa de históricos de medição por ponto de consumo
	 */
	Map<PontoConsumo, Collection<HistoricoMedicao>> consultarHistoricoMedicaoAtual(Long[] chavesPontoConsumo);

	/**
	 * Método responsável por obter a lista os pontos de consumo que nao
	 * foram registrados
	 * @param pontosConsumo {@link List}}]
	 * @param rota {@link Rota}}
	 * @return Lista de pontos de consumo {@link List}
	 */
	List<PontoConsumo> obterPontosConsumoNaoRegistrados(List<PontoConsumo> pontosConsumo, Rota rota);

	/**
	 * Obtém a data de leitura anterior. Caso nao haja medicao anterior, retorna a data de ativacao/instaalcao do medidor
	 * @param historicoConsumo {@link HistoricoConsumo}
	 * @return retorna a data da medicao anterior 
	 */
	Date obterDataMedicaoAnterior(HistoricoConsumo historicoConsumo);

	Collection<HistoricoLeituraGSAVO> obterHistoricoMedicaoGSAPorPontoConsumo(PontoConsumo pontoConsumo);

	Collection<HistoricoLeituraGSAVO> obterVolumeFaturadoGSANoPeriodo(PontoConsumo pontoConsumo, String dataInicio,
			String dataFim);
}
