/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe;

import java.util.Map;

import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface NfeEmpresa extends EntidadeNegocio {

	final String EMPRESA = "EMPRESA_ROTULO";

	final String BEAN_ID_EMPRESA = "nfeEmpresa";

	// EMPR_NM_FANTASIA
	// VARCHAR2(60),

	final String NOME_FANTASIA = "EMPRESA_NOME_FANTASIA";

	// EMPR_CD_EMPRESA
	// NUMBER(4)
	// not
	// null,

	final String NOME = "EMPRESA_NOME";

	// EMPR_NM_EMPRESA
	// VARCHAR2(60)
	// not
	// null,

	final String COMPLEMENTO = "EMPRESA_COMPLEMENTO";

	// EMPR_DS_COMPL_EMPRESA
	// VARCHAR2(30),

	final String INSCRICAO = "EMPRESA_INSCRICAO";

	// EMPR_NR_INSCR_EMPRESA
	// VARCHAR2(20)
	// not
	// null,

	final String CNPJ = "EMPRESA_CNPJ";

	// EMPR_NR_CNPJ_EMPRESA
	// VARCHAR2(20)
	// not
	// null,

	final String LOGO = "EMPRESA_LOGO";

	// EMPR_MM_LOGO
	// BLOB,

	final String IND_IMPRIME_LOGO = "EMPRESA_IND_IMPRIME_LOGO";

	// EMPR_IN_IMPRIME_LOGO
	// VARCHAR2(1)
	// not
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.EntidadeNegocio#validarDados()
	 */
	// null,
	@Override
	Map<String, Object> validarDados();

	/**
	 * @return Integer - Retorna o Código IBGE UF
	 */
	Integer getCodigoIbgeUF();

	/**
	 * @param codigoIbgeUF - Set Código IBGE - UF
	 */
	void setCodigoIbgeUF(Integer codigoIbgeUF);

	/**
	 * @return String - Retorna Nome fântasia
	 */
	String getNomeFantasia();

	/**
	 * @param nomeFantasia - Set Nome fântasia. 
	 */
	void setNomeFantasia(String nomeFantasia);

	/**
	 * @return String - Retorna Nome.
	 */
	String getNome();

	/**
	 * @param nome - Set nome.
	 */
	void setNome(String nome);

	/**
	 * @return String - Complemento empresa.
	 */
	String getComplementoEmpresa();

	/**
	 * @param complEmpresa - Set Complemento Empresa.
	 */
	void setComplementoEmpresa(String complEmpresa);
	
	/**
	 * @return String - Retorna cnpj.
	 */
	String getCnpj();

	/**
	 * @param cnpj - Set Cnpj.
	 */
	void setCnpj(String cnpj);

	/**
	 * @return byte - Retorna byte de logo.
	 */
	byte[] getLogo();

	/**
	 * @param logo - Set logo.
	 */
	void setLogo(byte[] logo);

	/**
	 * @return String - Retorna Imprime logo.
	 */
	String getImprimeLogo();

	/**
	 * @param imprimeLogo - Set Imprime Logo.
	 */
	void setImprimeLogo(String imprimeLogo);

	/**
	 * @return String - Retorna número inscrição da empresa.
	 */
	String getNumeroInscricaoEmpresa();

	/**
	 * @param numeroInscricaoEmpresa - Set Número inscrição da empresa.
	 */
	void setNumeroInscricaoEmpresa(String numeroInscricaoEmpresa);
}
