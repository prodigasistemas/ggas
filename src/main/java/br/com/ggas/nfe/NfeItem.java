/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe;

import java.util.Map;

import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface NfeItem.
 */
public interface NfeItem extends EntidadeNegocio {

	/** The bean id item. */
	final String BEAN_ID_ITEM = "nfeItem";

	/**
	 * Gets the nfe.
	 *
	 * @return the nfe
	 */
	Nfe getNfe();

	/**
	 * Sets the nfe.
	 *
	 * @param nfe the new nfe
	 */
	void setNfe(Nfe nfe);

	/**
	 * Sets the numero item.
	 *
	 * @param numeroItem the new numero item
	 */
	void setNumeroItem(Integer numeroItem);

	/**
	 * Gets the numero item.
	 *
	 * @return the numero item
	 */
	Integer getNumeroItem();

	/**
	 * Gets the codigo produto.
	 *
	 * @return the codigo produto
	 */
	String getCodigoProduto();

	/**
	 * Sets the codigo produto.
	 *
	 * @param codigoProduto the new codigo produto
	 */
	void setCodigoProduto(String codigoProduto);

	/**
	 * Gets the codigo ean.
	 *
	 * @return the codigo ean
	 */
	String getCodigoEAN();

	/**
	 * Sets the codigo ean.
	 *
	 * @param codigoEAN the new codigo ean
	 */
	void setCodigoEAN(String codigoEAN);

	/**
	 * Gets the descricao produto.
	 *
	 * @return the descricao produto
	 */
	String getDescricaoProduto();

	/**
	 * Sets the descricao produto.
	 *
	 * @param descricaoProduto the new descricao produto
	 */
	void setDescricaoProduto(String descricaoProduto);

	/**
	 * Gets the codigo ncm.
	 *
	 * @return the codigo ncm
	 */
	String getCodigoNCM();

	/**
	 * Sets the codigo ncm.
	 *
	 * @param codigoNCM the new codigo ncm
	 */
	void setCodigoNCM(String codigoNCM);

	/**
	 * Gets the codigo ex tipi.
	 *
	 * @return the codigo ex tipi
	 */
	String getCodigoExTIPI();

	/**
	 * Sets the codigo ex tipi.
	 *
	 * @param codigoExTIPI the new codigo ex tipi
	 */
	void setCodigoExTIPI(String codigoExTIPI);

	/**
	 * Gets the codigo genero.
	 *
	 * @return the codigo genero
	 */
	Integer getCodigoGenero();

	/**
	 * Sets the codigo genero.
	 *
	 * @param codigoGenero the new codigo genero
	 */
	void setCodigoGenero(Integer codigoGenero);

	/**
	 * Gets the cfop.
	 *
	 * @return the cfop
	 */
	Integer getCfop();

	/**
	 * Sets the cfop.
	 *
	 * @param cfop the new cfop
	 */
	void setCfop(Integer cfop);

	/**
	 * Gets the descricao unidade medida tributavel.
	 *
	 * @return the descricao unidade medida tributavel
	 */
	String getDescricaoUnidadeMedidaTributavel();

	/**
	 * Sets the descricao unidade medida tributavel.
	 *
	 * @param descricaoUnidadeMedidaTributavel the new descricao unidade medida tributavel
	 */
	void setDescricaoUnidadeMedidaTributavel(String descricaoUnidadeMedidaTributavel);

	/**
	 * Gets the descricao unidade medida comercializacao.
	 *
	 * @return the descricao unidade medida comercializacao
	 */
	String getDescricaoUnidadeMedidaComercializacao();

	/**
	 * Sets the descricao unidade medida comercializacao.
	 *
	 * @param descricaoUnidadeMedidaComercializacao the new descricao unidade medida comercializacao
	 */
	void setDescricaoUnidadeMedidaComercializacao(String descricaoUnidadeMedidaComercializacao);

	/**
	 * Gets the quantidade tributavel.
	 *
	 * @return the quantidade tributavel
	 */
	Double getQuantidadeTributavel();

	/**
	 * Sets the quantidade tributavel.
	 *
	 * @param quantidadeTrib the new quantidade tributavel
	 */
	void setQuantidadeTributavel(Double quantidadeTrib);

	/**
	 * Gets the quantidade comercializacao.
	 *
	 * @return the quantidade comercializacao
	 */
	Double getQuantidadeComercializacao();

	/**
	 * Sets the quantidade comercializacao.
	 *
	 * @param quantidadeComercializacao the new quantidade comercializacao
	 */
	void setQuantidadeComercializacao(Double quantidadeComercializacao);

	/**
	 * Gets the valor total bruto.
	 *
	 * @return the valor total bruto
	 */
	Double getValorTotalBruto();

	/**
	 * Sets the valor total bruto.
	 *
	 * @param valorTotalBruto the new valor total bruto
	 */
	void setValorTotalBruto(Double valorTotalBruto);

	/**
	 * Gets the valor total frete.
	 *
	 * @return the valor total frete
	 */
	Double getValorTotalFrete();

	/**
	 * Sets the valor total frete.
	 *
	 * @param valorTotalFrete the new valor total frete
	 */
	void setValorTotalFrete(Double valorTotalFrete);

	/**
	 * Gets the valor total seguro.
	 *
	 * @return the valor total seguro
	 */
	Double getValorTotalSeguro();

	/**
	 * Sets the valor total seguro.
	 *
	 * @param valorTotalSeguro the new valor total seguro
	 */
	void setValorTotalSeguro(Double valorTotalSeguro);

	/**
	 * Gets the valor desconto.
	 *
	 * @return the valor desconto
	 */
	Double getValorDesconto();

	/**
	 * Sets the valor desconto.
	 *
	 * @param valorDesconto the new valor desconto
	 */
	void setValorDesconto(Double valorDesconto);

	/**
	 * Gets the ind trib icms.
	 *
	 * @return the ind trib icms
	 */
	String getIndTribICMS();

	/**
	 * Sets the ind trib icms.
	 *
	 * @param indTribICMS the new ind trib icms
	 */
	void setIndTribICMS(String indTribICMS);

	/**
	 * Gets the ind origem icms.
	 *
	 * @return the ind origem icms
	 */
	Integer getIndOrigemICMS();

	/**
	 * Sets the ind origem icms.
	 *
	 * @param indOrigemICMS the new ind origem icms
	 */
	void setIndOrigemICMS(Integer indOrigemICMS);

	/**
	 * Gets the ind mod base calculo icms.
	 *
	 * @return the ind mod base calculo icms
	 */
	Integer getIndModBaseCalculoICMS();

	/**
	 * Sets the ind mod base calculo icms.
	 *
	 * @param indModBaseCalculoICMS the new ind mod base calculo icms
	 */
	void setIndModBaseCalculoICMS(Integer indModBaseCalculoICMS);

	/**
	 * Gets the valor base calculo icms.
	 *
	 * @return the valor base calculo icms
	 */
	Double getValorBaseCalculoICMS();

	/**
	 * Sets the valor base calculo icms.
	 *
	 * @param valorBaseCalculoICMS the new valor base calculo icms
	 */
	void setValorBaseCalculoICMS(Double valorBaseCalculoICMS);

	/**
	 * Gets the valor aliquota icms.
	 *
	 * @return the valor aliquota icms
	 */
	Double getValorAliquotaICMS();

	/**
	 * Sets the valor aliquota icms.
	 *
	 * @param valorAliquotaICMS the new valor aliquota icms
	 */
	void setValorAliquotaICMS(Double valorAliquotaICMS);

	/**
	 * Gets the valor icms.
	 *
	 * @return the valor icms
	 */
	Double getValorICMS();

	/**
	 * Sets the valor icms.
	 *
	 * @param valorICMS the new valor icms
	 */
	void setValorICMS(Double valorICMS);

	/**
	 * Gets the ind mod base calculo icms substituto.
	 *
	 * @return the ind mod base calculo icms substituto
	 */
	Integer getIndModBaseCalculoICMSSubstituto();

	/**
	 * Sets the ind mod base calculo icms substituto.
	 *
	 * @param indModBaseCalculoICMSSubstituto the new ind mod base calculo icms substituto
	 */
	void setIndModBaseCalculoICMSSubstituto(Integer indModBaseCalculoICMSSubstituto);

	/**
	 * Gets the percentual margem icms.
	 *
	 * @return the percentual margem icms
	 */
	Double getPercentualMargemICMS();

	/**
	 * Sets the percentual margem icms.
	 *
	 * @param percentualMargemICMS the new percentual margem icms
	 */
	void setPercentualMargemICMS(Double percentualMargemICMS);

	/**
	 * Gets the percentual reducao icms.
	 *
	 * @return the percentual reducao icms
	 */
	Double getPercentualReducaoICMS();

	/**
	 * Sets the percentual reducao icms.
	 *
	 * @param percentualReducaoICMS the new percentual reducao icms
	 */
	void setPercentualReducaoICMS(Double percentualReducaoICMS);

	/**
	 * Gets the base calculo icms substituto.
	 *
	 * @return the base calculo icms substituto
	 */
	Double getBaseCalculoICMSSubstituto();

	/**
	 * Sets the base calculo icms substituto.
	 *
	 * @param baseCalculoICMSSubstituto the new base calculo icms substituto
	 */
	void setBaseCalculoICMSSubstituto(Double baseCalculoICMSSubstituto);

	/**
	 * Gets the valor aliquota situacao trib icms.
	 *
	 * @return the valor aliquota situacao trib icms
	 */
	Double getValorAliquotaSituacaoTribICMS();

	/**
	 * Sets the valor aliquota situacao trib icms.
	 *
	 * @param valorAliquotaSituacaiICMS the new valor aliquota situacao trib icms
	 */
	void setValorAliquotaSituacaoTribICMS(Double valorAliquotaSituacaiICMS);

	/**
	 * Gets the valor icms situacao.
	 *
	 * @return the valor icms situacao
	 */
	Double getValorICMSSituacao();

	/**
	 * Sets the valor icms situacao.
	 *
	 * @param valorICMSSituacao the new valor icms situacao
	 */
	void setValorICMSSituacao(Double valorICMSSituacao);

	/**
	 * Gets the percentual reducao icms situacao.
	 *
	 * @return the percentual reducao icms situacao
	 */
	Double getPercentualReducaoICMSSituacao();

	/**
	 * Sets the percentual reducao icms situacao.
	 *
	 * @param percentualReducaoICMSSituacao the new percentual reducao icms situacao
	 */
	void setPercentualReducaoICMSSituacao(Double percentualReducaoICMSSituacao);

	/**
	 * Gets the codigo classe ipi.
	 *
	 * @return the codigo classe ipi
	 */
	String getCodigoClasseIPI();

	/**
	 * Sets the codigo classe ipi.
	 *
	 * @param codigoClasseIPI the new codigo classe ipi
	 */
	void setCodigoClasseIPI(String codigoClasseIPI);

	/**
	 * Gets the cpnj ipi.
	 *
	 * @return the cpnj ipi
	 */
	String getCpnjIPI();

	/**
	 * Sets the cpnj ipi.
	 *
	 * @param cpnjIPI the new cpnj ipi
	 */
	void setCpnjIPI(String cpnjIPI);

	/**
	 * Gets the codigo selo ipi.
	 *
	 * @return the codigo selo ipi
	 */
	String getCodigoSeloIPI();

	/**
	 * Sets the codigo selo ipi.
	 *
	 * @param codigoSeloIPI the new codigo selo ipi
	 */
	void setCodigoSeloIPI(String codigoSeloIPI);

	/**
	 * Gets the numero selo ipi.
	 *
	 * @return the numero selo ipi
	 */
	Integer getNumeroSeloIPI();

	/**
	 * Sets the numero selo ipi.
	 *
	 * @param numeroSeloIPI the new numero selo ipi
	 */
	void setNumeroSeloIPI(Integer numeroSeloIPI);

	/**
	 * Gets the codigo enq ipi.
	 *
	 * @return the codigo enq ipi
	 */
	String getCodigoEnqIPI();

	/**
	 * Sets the codigo enq ipi.
	 *
	 * @param codigoEnqIPI the new codigo enq ipi
	 */
	void setCodigoEnqIPI(String codigoEnqIPI);

	/**
	 * Gets the ind situacao trib ipi.
	 *
	 * @return the ind situacao trib ipi
	 */
	String getIndSituacaoTribIPI();

	/**
	 * Sets the ind situacao trib ipi.
	 *
	 * @param indSituacaoTribIPI the new ind situacao trib ipi
	 */
	void setIndSituacaoTribIPI(String indSituacaoTribIPI);

	/**
	 * Gets the base calculo ipi.
	 *
	 * @return the base calculo ipi
	 */
	Double getBaseCalculoIPI();

	/**
	 * Sets the base calculo ipi.
	 *
	 * @param baseCalculoIPI the new base calculo ipi
	 */
	void setBaseCalculoIPI(Double baseCalculoIPI);

	/**
	 * Gets the numero unidade prd ipi.
	 *
	 * @return the numero unidade prd ipi
	 */
	Double getNumeroUnidadePrdIPI();

	/**
	 * Sets the numero unidade prd ipi.
	 *
	 * @param numeroUnidadePrdIPI the new numero unidade prd ipi
	 */
	void setNumeroUnidadePrdIPI(Double numeroUnidadePrdIPI);

	/**
	 * Gets the valor unidade trib ipi.
	 *
	 * @return the valor unidade trib ipi
	 */
	Double getValorUnidadeTribIPI();

	/**
	 * Sets the valor unidade trib ipi.
	 *
	 * @param valorUnidadeTribIPI the new valor unidade trib ipi
	 */
	void setValorUnidadeTribIPI(Double valorUnidadeTribIPI);

	/**
	 * Gets the valor aliquota ipi.
	 *
	 * @return the valor aliquota ipi
	 */
	Double getValorAliquotaIPI();

	/**
	 * Sets the valor aliquota ipi.
	 *
	 * @param valorAliquotaIPI the new valor aliquota ipi
	 */
	void setValorAliquotaIPI(Double valorAliquotaIPI);

	/**
	 * Gets the valor ipi.
	 *
	 * @return the valor ipi
	 */
	Double getValorIPI();

	/**
	 * Sets the valor ipi.
	 *
	 * @param valorIPI the new valor ipi
	 */
	void setValorIPI(Double valorIPI);

	/**
	 * Gets the ind situacao trib pis.
	 *
	 * @return the ind situacao trib pis
	 */
	String getIndSituacaoTribPIS();

	/**
	 * Sets the ind situacao trib pis.
	 *
	 * @param indSituacaoTribPIS the new ind situacao trib pis
	 */
	void setIndSituacaoTribPIS(String indSituacaoTribPIS);

	/**
	 * Gets the base calculo pis.
	 *
	 * @return the base calculo pis
	 */
	Double getBaseCalculoPIS();

	/**
	 * Sets the base calculo pis.
	 *
	 * @param baseCalculoPIS the new base calculo pis
	 */
	void setBaseCalculoPIS(Double baseCalculoPIS);

	/**
	 * Gets the percentual aliquota pis.
	 *
	 * @return the percentual aliquota pis
	 */
	Double getPercentualAliquotaPIS();

	/**
	 * Sets the percentual aliquota pis.
	 *
	 * @param percentualAliquotaPIS the new percentual aliquota pis
	 */
	void setPercentualAliquotaPIS(Double percentualAliquotaPIS);

	/**
	 * Gets the valor pis.
	 *
	 * @return the valor pis
	 */
	Double getValorPIS();

	/**
	 * Sets the valor pis.
	 *
	 * @param valorPIS the new valor pis
	 */
	void setValorPIS(Double valorPIS);

	/**
	 * Gets the quantidade vend pis.
	 *
	 * @return the quantidade vend pis
	 */
	Double getQuantidadeVendPIS();

	/**
	 * Sets the quantidade vend pis.
	 *
	 * @param valorQtdVendPIS the new quantidade vend pis
	 */
	void setQuantidadeVendPIS(Double valorQtdVendPIS);

	/**
	 * Gets the valor aliquota pis.
	 *
	 * @return the valor aliquota pis
	 */
	Double getValorAliquotaPIS();

	/**
	 * Sets the valor aliquota pis.
	 *
	 * @param valorAliquotaPIS the new valor aliquota pis
	 */
	void setValorAliquotaPIS(Double valorAliquotaPIS);

	/**
	 * Gets the ind situacao trib cofins.
	 *
	 * @return the ind situacao trib cofins
	 */
	String getIndSituacaoTribCOFINS();

	/**
	 * Sets the ind situacao trib cofins.
	 *
	 * @param indSituacaoTribCOFINS the new ind situacao trib cofins
	 */
	void setIndSituacaoTribCOFINS(String indSituacaoTribCOFINS);

	/**
	 * Gets the base calculo cofins.
	 *
	 * @return the base calculo cofins
	 */
	Double getBaseCalculoCOFINS();

	/**
	 * Sets the base calculo cofins.
	 *
	 * @param baseCalculoCOFINS the new base calculo cofins
	 */
	void setBaseCalculoCOFINS(Double baseCalculoCOFINS);

	/**
	 * Gets the percentual aliquota cofins.
	 *
	 * @return the percentual aliquota cofins
	 */
	Double getPercentualAliquotaCOFINS();

	/**
	 * Sets the percentual aliquota cofins.
	 *
	 * @param percentualAliquotaCOFINS the new percentual aliquota cofins
	 */
	void setPercentualAliquotaCOFINS(Double percentualAliquotaCOFINS);

	/**
	 * Gets the valor cofins.
	 *
	 * @return the valor cofins
	 */
	Double getValorCOFINS();

	/**
	 * Sets the valor cofins.
	 *
	 * @param valorCOFINS the new valor cofins
	 */
	void setValorCOFINS(Double valorCOFINS);

	/**
	 * Gets the valor aliquota cofins.
	 *
	 * @return the valor aliquota cofins
	 */
	Double getValorAliquotaCOFINS();

	/**
	 * Sets the valor aliquota cofins.
	 *
	 * @param valorAliquotCOFINS the new valor aliquota cofins
	 */
	void setValorAliquotaCOFINS(Double valorAliquotCOFINS);

	/**
	 * Gets the quantidade vend cofins.
	 *
	 * @return the quantidade vend cofins
	 */
	Double getQuantidadeVendCOFINS();

	/**
	 * Sets the quantidade vend cofins.
	 *
	 * @param valorQtdVendCOFINS the new quantidade vend cofins
	 */
	void setQuantidadeVendCOFINS(Double valorQtdVendCOFINS);

	/**
	 * Gets the base calculo issqn.
	 *
	 * @return the base calculo issqn
	 */
	Double getBaseCalculoISSQN();

	/**
	 * Sets the base calculo issqn.
	 *
	 * @param baseCalculoISSQN the new base calculo issqn
	 */
	void setBaseCalculoISSQN(Double baseCalculoISSQN);

	/**
	 * Gets the valor aliquota issqn.
	 *
	 * @return the valor aliquota issqn
	 */
	Double getValorAliquotaISSQN();

	/**
	 * Sets the valor aliquota issqn.
	 *
	 * @param valorAliquotaISSQN the new valor aliquota issqn
	 */
	void setValorAliquotaISSQN(Double valorAliquotaISSQN);

	/**
	 * Gets the valor issqn.
	 *
	 * @return the valor issqn
	 */
	Double getValorISSQN();

	/**
	 * Sets the valor issqn.
	 *
	 * @param valorISSQN the new valor issqn
	 */
	void setValorISSQN(Double valorISSQN);

	/**
	 * Gets the numero ibge municipio issqn.
	 *
	 * @return the numero ibge municipio issqn
	 */
	Integer getNumeroIbgeMunicipioISSQN();

	/**
	 * Sets the numero ibge municipio issqn.
	 *
	 * @param numeroIbgeMunicipioISSQN the new numero ibge municipio issqn
	 */
	void setNumeroIbgeMunicipioISSQN(Integer numeroIbgeMunicipioISSQN);

	/**
	 * Gets the info.
	 *
	 * @return the info
	 */
	String getInfo();

	/**
	 * Sets the info.
	 *
	 * @param info the new info
	 */
	void setInfo(String info);

	/**
	 * Gets the valor unitario.
	 *
	 * @return the valor unitario
	 */
	Double getValorUnitario();

	/**
	 * Sets the valor unitario.
	 *
	 * @param valorUnitario the new valor unitario
	 */
	void setValorUnitario(Double valorUnitario);

	/**
	 * Gets the valor unitario tributavel.
	 *
	 * @return the valor unitario tributavel
	 */
	Double getValorUnitarioTributavel();

	/**
	 * Sets the valor unitario tributavel.
	 *
	 * @param valorUnitarioTributavel the new valor unitario tributavel
	 */
	void setValorUnitarioTributavel(Double valorUnitarioTributavel);

	/**
	 * Gets the codigo lista servico.
	 *
	 * @return the codigo lista servico
	 */
	String getCodigoListaServico();

	/**
	 * Sets the codigo lista servico.
	 *
	 * @param codigoListaServico the new codigo lista servico
	 */
	void setCodigoListaServico(String codigoListaServico);

	/**
	 * Gets the base calculo imposto importacao.
	 *
	 * @return the base calculo imposto importacao
	 */
	Double getBaseCalculoImpostoImportacao();

	/**
	 * Sets the base calculo imposto importacao.
	 *
	 * @param baseCalculoImpostoImportacao the new base calculo imposto importacao
	 */
	void setBaseCalculoImpostoImportacao(Double baseCalculoImpostoImportacao);

	/**
	 * Gets the valor despesas aduaneiras.
	 *
	 * @return the valor despesas aduaneiras
	 */
	Double getValorDespesasAduaneiras();

	/**
	 * Sets the valor despesas aduaneiras.
	 *
	 * @param valorDespesasAduaneiras the new valor despesas aduaneiras
	 */
	void setValorDespesasAduaneiras(Double valorDespesasAduaneiras);

	/**
	 * Gets the valor imposto importacao.
	 *
	 * @return the valor imposto importacao
	 */
	Double getValorImpostoImportacao();

	/**
	 * Sets the valor imposto importacao.
	 *
	 * @param valorImpostoImportacao the new valor imposto importacao
	 */
	void setValorImpostoImportacao(Double valorImpostoImportacao);

	/**
	 * Gets the valor iof.
	 *
	 * @return the valor iof
	 */
	Double getValorIof();

	/**
	 * Sets the valor iof.
	 *
	 * @param valorIof the new valor iof
	 */
	void setValorIof(Double valorIof);

	/**
	 * Gets the codigo ean trib.
	 *
	 * @return the codigo ean trib
	 */
	String getCodigoEanTrib();

	/**
	 * Sets the codigo ean trib.
	 *
	 * @param codigoEanTrib the new codigo ean trib
	 */
	void setCodigoEanTrib(String codigoEanTrib);

	/**
	 * Gets the base calculo pis substituto.
	 *
	 * @return the base calculo pis substituto
	 */
	Double getBaseCalculoPISSubstituto();

	/**
	 * Sets the base calculo pis substituto.
	 *
	 * @param baseCalculoPISSubstituto the new base calculo pis substituto
	 */
	void setBaseCalculoPISSubstituto(Double baseCalculoPISSubstituto);

	/**
	 * Gets the valor paliq pis substituto.
	 *
	 * @return the valor paliq pis substituto
	 */
	Double getValorPaliqPISSubstituto();

	/**
	 * Sets the valor paliq pis substituto.
	 *
	 * @param valorPaliqPISSubstituto the new valor paliq pis substituto
	 */
	void setValorPaliqPISSubstituto(Double valorPaliqPISSubstituto);

	/**
	 * Gets the quantidade pis substituto.
	 *
	 * @return the quantidade pis substituto
	 */
	Double getQuantidadePISSubstituto();

	/**
	 * Sets the quantidade pis substituto.
	 *
	 * @param quantidadePISSubstituto the new quantidade pis substituto
	 */
	void setQuantidadePISSubstituto(Double quantidadePISSubstituto);

	/**
	 * Gets the valor pis substituto.
	 *
	 * @return the valor pis substituto
	 */
	Double getValorPISSubstituto();

	/**
	 * Sets the valor pis substituto.
	 *
	 * @param valorPISSubstituto the new valor pis substituto
	 */
	void setValorPISSubstituto(Double valorPISSubstituto);

	/**
	 * Gets the base calculo cofins substituto.
	 *
	 * @return the base calculo cofins substituto
	 */
	Double getBaseCalculoCOFINSSubstituto();

	/**
	 * Sets the base calculo cofins substituto.
	 *
	 * @param baseCalculoCOFINSSubstituto the new base calculo cofins substituto
	 */
	void setBaseCalculoCOFINSSubstituto(Double baseCalculoCOFINSSubstituto);

	/**
	 * Gets the valor paliq cofins substituto.
	 *
	 * @return the valor paliq cofins substituto
	 */
	Double getValorPaliqCOFINSSubstituto();

	/**
	 * Sets the valor paliq cofins substituto.
	 *
	 * @param valorPaliqCOFINSSubstituto the new valor paliq cofins substituto
	 */
	void setValorPaliqCOFINSSubstituto(Double valorPaliqCOFINSSubstituto);

	/**
	 * Gets the quantidade cofins substituto.
	 *
	 * @return the quantidade cofins substituto
	 */
	Double getQuantidadeCOFINSSubstituto();

	/**
	 * Sets the quantidade cofins substituto.
	 *
	 * @param valorQtdCOFINSSubstituto the new quantidade cofins substituto
	 */
	void setQuantidadeCOFINSSubstituto(Double valorQtdCOFINSSubstituto);

	/**
	 * Gets the valor aliq cofins substituto.
	 *
	 * @return the valor aliq cofins substituto
	 */
	Double getValorAliqCOFINSSubstituto();

	/**
	 * Sets the valor aliq cofins substituto.
	 *
	 * @param valorAliqCOFINSSubstituto the new valor aliq cofins substituto
	 */
	void setValorAliqCOFINSSubstituto(Double valorAliqCOFINSSubstituto);

	/**
	 * Gets the valor cofins substituto.
	 *
	 * @return the valor cofins substituto
	 */
	Double getValorCOFINSSubstituto();

	/**
	 * Sets the valor cofins substituto.
	 *
	 * @param valorCOFINSSubstituto the new valor cofins substituto
	 */
	void setValorCOFINSSubstituto(Double valorCOFINSSubstituto);

	/**
	 * Gets the valor raliq pis substituto.
	 *
	 * @return the valor raliq pis substituto
	 */
	Double getValorRaliqPISSubstituto();

	/**
	 * Sets the valor raliq pis substituto.
	 *
	 * @param valorRaliqPISSubstituto the new valor raliq pis substituto
	 */
	void setValorRaliqPISSubstituto(Double valorRaliqPISSubstituto);

	/**
	 * Gets the valor outros.
	 *
	 * @return the valor outros
	 */
	Double getValorOutros();

	/**
	 * Sets the valor outros.
	 *
	 * @param valorOutros the new valor outros
	 */
	void setValorOutros(Double valorOutros);

	/**
	 * Gets the indicador totalizacao.
	 *
	 * @return the indicador totalizacao
	 */
	Integer getIndicadorTotalizacao();

	/**
	 * Sets the indicador totalizacao.
	 *
	 * @param indicadorTotalizacao the new indicador totalizacao
	 */
	void setIndicadorTotalizacao(Integer indicadorTotalizacao);

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.EntidadeNegocio#validarDados()
	 */
	@Override
	Map<String, Object> validarDados();

	/**
	 * Sets the valor icms desonerado.
	 *
	 * @param valorICMSDesonerado the new valor icms desonerado
	 */
	void setValorICMSDesonerado(Double valorICMSDesonerado);

	/**
	 * Gets the valor icms desonerado.
	 *
	 * @return the valor icms desonerado
	 */
	Double getValorICMSDesonerado();

	/**
	 * Sets the valor icms operacao.
	 *
	 * @param valorICMSOperacao the new valor icms operacao
	 */
	void setValorICMSOperacao(Double valorICMSOperacao);

	/**
	 * Gets the valor icms operacao.
	 *
	 * @return the valor icms operacao
	 */
	Double getValorICMSOperacao();

	/**
	 * Sets the percentual diferimento icms.
	 *
	 * @param percentualDiferimentoICMS the new percentual diferimento icms
	 */
	void setPercentualDiferimentoICMS(Double percentualDiferimentoICMS);

	/**
	 * Gets the percentual diferimento icms.
	 *
	 * @return the percentual diferimento icms
	 */
	Double getPercentualDiferimentoICMS();

	/**
	 * Sets the valor icms diferido.
	 *
	 * @param valorICMSDiferido the new valor icms diferido
	 */
	void setValorICMSDiferido(Double valorICMSDiferido);

	/**
	 * Gets the valor icms diferido.
	 *
	 * @return the valor icms diferido
	 */
	Double getValorICMSDiferido();

	/**
	 * Sets the motivo desoneracao icms.
	 *
	 * @param motivoDesoneracaoICMS the new motivo desoneracao icms
	 */
	void setMotivoDesoneracaoICMS(Integer motivoDesoneracaoICMS);

	/**
	 * Gets the motivo desoneracao icms.
	 *
	 * @return the motivo desoneracao icms
	 */
	Integer getMotivoDesoneracaoICMS();

	/**
	 * Sets the numero recopi.
	 *
	 * @param numeroRECOPI the new numero recopi
	 */
	void setNumeroRECOPI(String numeroRECOPI);

	/**
	 * Gets the numero recopi.
	 *
	 * @return the numero recopi
	 */
	String getNumeroRECOPI();

	/**
	 * Gets the codigo situacao tributaria issqn.
	 *
	 * @return the codigo situacao tributaria issqn
	 */
	String getCodigoSituacaoTributariaISSQN();

	/**
	 * Sets the codigo situacao tributaria issqn.
	 *
	 * @param codigo the new codigo situacao tributaria issqn
	 */
	void setCodigoSituacaoTributariaISSQN(String codigo);

	/**
	 * Sets the valor deducao issqn.
	 *
	 * @param valorDeducaoISSQN the new valor deducao issqn
	 */
	void setValorDeducaoISSQN(Double valorDeducaoISSQN);

	/**
	 * Gets the valor deducao issqn.
	 *
	 * @return the valor deducao issqn
	 */
	Double getValorDeducaoISSQN();

	/**
	 * Sets the valor outras issqn.
	 *
	 * @param valorOutrasISSQN the new valor outras issqn
	 */
	void setValorOutrasISSQN(Double valorOutrasISSQN);

	/**
	 * Gets the valor outras issqn.
	 *
	 * @return the valor outras issqn
	 */
	Double getValorOutrasISSQN();

	/**
	 * Sets the valor desconto incondicionado issqn.
	 *
	 * @param valorDescontoIncondicionadoISSQN the new valor desconto incondicionado issqn
	 */
	void setValorDescontoIncondicionadoISSQN(Double valorDescontoIncondicionadoISSQN);

	/**
	 * Gets the valor desconto incondicionado issqn.
	 *
	 * @return the valor desconto incondicionado issqn
	 */
	Double getValorDescontoIncondicionadoISSQN();

	/**
	 * Sets the valor desconto condicionado issqn.
	 *
	 * @param valorDescontoCondicionadoISSQN the new valor desconto condicionado issqn
	 */
	void setValorDescontoCondicionadoISSQN(Double valorDescontoCondicionadoISSQN);

	/**
	 * Gets the valor desconto condicionado issqn.
	 *
	 * @return the valor desconto condicionado issqn
	 */
	Double getValorDescontoCondicionadoISSQN();

	/**
	 * Sets the valor iss retido.
	 *
	 * @param valorISSRetido the new valor iss retido
	 */
	void setValorISSRetido(Double valorISSRetido);

	/**
	 * Gets the valor iss retido.
	 *
	 * @return the valor iss retido
	 */
	Double getValorISSRetido();

	/**
	 * Sets the indicador exigibilidade issqn.
	 *
	 * @param indicadorExigibilidadeISSQN the new indicador exigibilidade issqn
	 */
	void setIndicadorExigibilidadeISSQN(Integer indicadorExigibilidadeISSQN);

	/**
	 * Gets the indicador exigibilidade issqn.
	 *
	 * @return the indicador exigibilidade issqn
	 */
	Integer getIndicadorExigibilidadeISSQN();

	/**
	 * Sets the codigo servico.
	 *
	 * @param codigoServico the new codigo servico
	 */
	void setCodigoServico(String codigoServico);

	/**
	 * Gets the codigo servico.
	 *
	 * @return the codigo servico
	 */
	String getCodigoServico();

	/**
	 * Sets the codigo municipio servico.
	 *
	 * @param codigoMunicipioServico the new codigo municipio servico
	 */
	void setCodigoMunicipioServico(String codigoMunicipioServico);

	/**
	 * Gets the codigo municipio servico.
	 *
	 * @return the codigo municipio servico
	 */
	String getCodigoMunicipioServico();

	/**
	 * Sets the codigo pais servico.
	 *
	 * @param codigoPaisServico the new codigo pais servico
	 */
	void setCodigoPaisServico(String codigoPaisServico);

	/**
	 * Gets the codigo pais servico.
	 *
	 * @return the codigo pais servico
	 */
	String getCodigoPaisServico();

	/**
	 * Sets the numero processo exigibilidade issqn.
	 *
	 * @param numeroProcessoExigibilidadeISSQN the new numero processo exigibilidade issqn
	 */
	void setNumeroProcessoExigibilidadeISSQN(String numeroProcessoExigibilidadeISSQN);

	/**
	 * Gets the numero processo exigibilidade issqn.
	 *
	 * @return the numero processo exigibilidade issqn
	 */
	String getNumeroProcessoExigibilidadeISSQN();

	/**
	 * Sets the indicador incentivo fiscal issqn.
	 *
	 * @param indicadorIncentivoFiscalISSQN the new indicador incentivo fiscal issqn
	 */
	void setIndicadorIncentivoFiscalISSQN(Integer indicadorIncentivoFiscalISSQN);

	/**
	 * Gets the indicador incentivo fiscal issqn.
	 *
	 * @return the indicador incentivo fiscal issqn
	 */
	Integer getIndicadorIncentivoFiscalISSQN();

	/**
	 * Gets the valor total impostos.
	 *
	 * @return the valor total impostos
	 */
	Double getValorTotalImpostos();

	/**
	 * Sets the valor total impostos.
	 *
	 * @param valorTotalImpostos the new valor total impostos
	 */
	void setValorTotalImpostos(Double valorTotalImpostos);

	/**
	 * Gets the numero pedido compra.
	 *
	 * @return the numero pedido compra
	 */
	String getNumeroPedidoCompra();

	/**
	 * Sets the numero pedido compra.
	 *
	 * @param numeroPedido the new numero pedido compra
	 */
	void setNumeroPedidoCompra(String numeroPedido);

	/**
	 * Gets the numero item pedido compra.
	 *
	 * @return the numero item pedido compra
	 */
	Integer getNumeroItemPedidoCompra();

	/**
	 * Sets the numero item pedido compra.
	 *
	 * @param itemPedido the new numero item pedido compra
	 */
	void setNumeroItemPedidoCompra(Integer itemPedido);

	/**
	 * Gets the codigo fci.
	 *
	 * @return the codigo fci
	 */
	String getCodigoFCI();

	/**
	 * Sets the codigo fci.
	 *
	 * @param codigoFCI the new codigo fci
	 */
	void setCodigoFCI(String codigoFCI);
	
	/**
	 * Gets o codigo CEST
	 *
	 * @return o codigo CEST
	 */

	String getCodigoCEST();

	/**
	 * Sets o codigo CEST
	 *
	 * @param codigoCEST o codigo CEST
	 */
	void setCodigoCEST(String codigoCEST);
	
}
