/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe;

import java.util.Collection;
import java.util.Date;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.EntidadeNegocio;

/**
 * The Interface Nfe.
 */
//interface NotaFiscalEletronica
public interface Nfe extends EntidadeNegocio {

	/** The bean id nfe. */
	final String BEAN_ID_NFE = "nfe";

	/**
	 * Gets the nfe empresa.
	 *
	 * @return the nfe empresa
	 */
	NfeEmpresa getNfeEmpresa();

	/**
	 * Sets the nfe empresa.
	 *
	 * @param nfeEmpresa the new nfe empresa
	 */
	void setNfeEmpresa(NfeEmpresa nfeEmpresa);

	/**
	 * Gets the ind danfe impressa.
	 *
	 * @return the ind danfe impressa
	 */
	Integer getIndDanfeImpressa();

	/**
	 * Sets the ind danfe impressa.
	 *
	 * @param indDanfeImpressa the new ind danfe impressa
	 */
	void setIndDanfeImpressa(Integer indDanfeImpressa);

	/**
	 * Gets the natureza operacao.
	 *
	 * @return the natureza operacao
	 */
	String getNaturezaOperacao();

	/**
	 * Sets the natureza operacao.
	 *
	 * @param naturezaOperacao the new natureza operacao
	 */
	void setNaturezaOperacao(String naturezaOperacao);

	/**
	 * Gets the forma pagamento.
	 *
	 * @return the forma pagamento
	 */
	Integer getFormaPagamento();

	/**
	 * Sets the forma pagamento.
	 *
	 * @param formaPagamento the new forma pagamento
	 */
	void setFormaPagamento(Integer formaPagamento);

	/**
	 * Gets the modelo documento fiscal.
	 *
	 * @return the modelo documento fiscal
	 */
	Integer getModeloDocumentoFiscal();

	/**
	 * Sets the modelo documento fiscal.
	 *
	 * @param modeloDocumentoFiscal the new modelo documento fiscal
	 */
	void setModeloDocumentoFiscal(Integer modeloDocumentoFiscal);

	/**
	 * Gets the serie.
	 *
	 * @return the serie
	 */
	Integer getSerie();

	/**
	 * Sets the serie.
	 *
	 * @param serie the new serie
	 */
	void setSerie(Integer serie);

	/**
	 * Gets the documento fiscal.
	 *
	 * @return the documento fiscal
	 */
	Integer getDocumentoFiscal();

	/**
	 * Sets the documento fiscal.
	 *
	 * @param documentoFiscal the new documento fiscal
	 */
	void setDocumentoFiscal(Integer documentoFiscal);

	/**
	 * Gets the emissao.
	 *
	 * @return the emissao
	 */
	Date getEmissao();

	/**
	 * Sets the emissao.
	 *
	 * @param emissao the new emissao
	 */
	void setEmissao(Date emissao);

	/**
	 * Gets the ind entrada saida.
	 *
	 * @return the ind entrada saida
	 */
	Integer getIndEntradaSaida();

	/**
	 * Sets the ind entrada saida.
	 *
	 * @param indEntradaSaida the new ind entrada saida
	 */
	void setIndEntradaSaida(Integer indEntradaSaida);

	/**
	 * Gets the cnpj.
	 *
	 * @return the cnpj
	 */
	String getCnpj();

	/**
	 * Sets the cnpj.
	 *
	 * @param cnpj the new cnpj
	 */
	void setCnpj(String cnpj);

	/**
	 * Gets the razao social.
	 *
	 * @return the razao social
	 */
	String getRazaoSocial();

	/**
	 * Sets the razao social.
	 *
	 * @param razaoSocial the new razao social
	 */
	void setRazaoSocial(String razaoSocial);

	/**
	 * Gets the nome fantasia.
	 *
	 * @return the nome fantasia
	 */
	String getNomeFantasia();

	/**
	 * Sets the nome fantasia.
	 *
	 * @param nomeFantasia the new nome fantasia
	 */
	void setNomeFantasia(String nomeFantasia);

	/**
	 * Gets the numero pais.
	 *
	 * @return the numero pais
	 */
	String getNumeroPais();

	/**
	 * Sets the numero pais.
	 *
	 * @param numeroPais the new numero pais
	 */
	void setNumeroPais(String numeroPais);

	/**
	 * Gets the pais.
	 *
	 * @return the pais
	 */
	String getPais();

	/**
	 * Sets the pais.
	 *
	 * @param pais the new pais
	 */
	void setPais(String pais);

	/**
	 * Gets the inscricao estadual.
	 *
	 * @return the inscricao estadual
	 */
	String getInscricaoEstadual();

	/**
	 * Sets the inscricao estadual.
	 *
	 * @param inscricaoEstadual the new inscricao estadual
	 */
	void setInscricaoEstadual(String inscricaoEstadual);

	/**
	 * Gets the inscricao municipal.
	 *
	 * @return the inscricao municipal
	 */
	String getInscricaoMunicipal();

	/**
	 * Sets the inscricao municipal.
	 *
	 * @param inscricaoMunicipal the new inscricao municipal
	 */
	void setInscricaoMunicipal(String inscricaoMunicipal);

	/**
	 * Gets the valor produto.
	 *
	 * @return the valor produto
	 */
	Double getValorProduto();

	/**
	 * Sets the valor produto.
	 *
	 * @param valorProduto the new valor produto
	 */
	void setValorProduto(Double valorProduto);

	/**
	 * Gets the valor frete.
	 *
	 * @return the valor frete
	 */
	Double getValorFrete();

	/**
	 * Sets the valor frete.
	 *
	 * @param valorFrete the new valor frete
	 */
	void setValorFrete(Double valorFrete);

	/**
	 * Gets the valor seguro.
	 *
	 * @return the valor seguro
	 */
	Double getValorSeguro();

	/**
	 * Sets the valor seguro.
	 *
	 * @param valorSeguro the new valor seguro
	 */
	void setValorSeguro(Double valorSeguro);

	/**
	 * Gets the valor desconto.
	 *
	 * @return the valor desconto
	 */
	Double getValorDesconto();

	/**
	 * Sets the valor desconto.
	 *
	 * @param valorDesconto the new valor desconto
	 */
	void setValorDesconto(Double valorDesconto);

	/**
	 * Gets the valor ipi.
	 *
	 * @return the valor ipi
	 */
	Double getValorIpi();

	/**
	 * Sets the valor ipi.
	 *
	 * @param valorIpi the new valor ipi
	 */
	void setValorIpi(Double valorIpi);

	/**
	 * Gets the valor outros.
	 *
	 * @return the valor outros
	 */
	Double getValorOutros();

	/**
	 * Sets the valor outros.
	 *
	 * @param valorOutros the new valor outros
	 */
	void setValorOutros(Double valorOutros);

	/**
	 * Gets the valor nota fiscal.
	 *
	 * @return the valor nota fiscal
	 */
	Double getValorNotaFiscal();

	/**
	 * Sets the valor nota fiscal.
	 *
	 * @param valorNotaFiscal the new valor nota fiscal
	 */
	void setValorNotaFiscal(Double valorNotaFiscal);

	/**
	 * Gets the valor ret pis.
	 *
	 * @return the valor ret pis
	 */
	Double getValorRetPis();

	/**
	 * Sets the valor ret pis.
	 *
	 * @param valorRetPis the new valor ret pis
	 */
	void setValorRetPis(Double valorRetPis);

	/**
	 * Gets the valor ret cofins.
	 *
	 * @return the valor ret cofins
	 */
	Double getValorRetCofins();

	/**
	 * Sets the valor ret cofins.
	 *
	 * @param valorRetCofins the new valor ret cofins
	 */
	void setValorRetCofins(Double valorRetCofins);

	/**
	 * Gets the valor ret csll.
	 *
	 * @return the valor ret csll
	 */
	Double getValorRetCsll();

	/**
	 * Sets the valor ret csll.
	 *
	 * @param valorRetCsll the new valor ret csll
	 */
	void setValorRetCsll(Double valorRetCsll);

	/**
	 * Gets the base calculo irrf.
	 *
	 * @return the base calculo irrf
	 */
	Double getBaseCalculoIrrf();

	/**
	 * Sets the base calculo irrf.
	 *
	 * @param baseCalculoIrrf the new base calculo irrf
	 */
	void setBaseCalculoIrrf(Double baseCalculoIrrf);

	/**
	 * Gets the valor irrf.
	 *
	 * @return the valor irrf
	 */
	Double getValorIrrf();

	/**
	 * Sets the valor irrf.
	 *
	 * @param valorIrrf the new valor irrf
	 */
	void setValorIrrf(Double valorIrrf);

	/**
	 * Gets the base calculo ret prev.
	 *
	 * @return the base calculo ret prev
	 */
	Double getBaseCalculoRetPrev();

	/**
	 * Sets the base calculo ret prev.
	 *
	 * @param baseCalculoRetPrev the new base calculo ret prev
	 */
	void setBaseCalculoRetPrev(Double baseCalculoRetPrev);

	/**
	 * Gets the valor ret prev.
	 *
	 * @return the valor ret prev
	 */
	Double getValorRetPrev();

	/**
	 * Sets the valor ret prev.
	 *
	 * @param valorRetPrev the new valor ret prev
	 */
	void setValorRetPrev(Double valorRetPrev);

	/**
	 * Gets the info fisco.
	 *
	 * @return the info fisco
	 */
	String getInfoFisco();

	/**
	 * Sets the info fisco.
	 *
	 * @param infoFisco the new info fisco
	 */
	void setInfoFisco(String infoFisco);

	/**
	 * Gets the info contribuinte.
	 *
	 * @return the info contribuinte
	 */
	String getInfoContribuinte();

	/**
	 * Sets the info contribuinte.
	 *
	 * @param infoContribuinte the new info contribuinte
	 */
	void setInfoContribuinte(String infoContribuinte);

	/**
	 * Gets the numero pais destino.
	 *
	 * @return the numero pais destino
	 */
	String getNumeroPaisDestino();

	/**
	 * Sets the numero pais destino.
	 *
	 * @param numeroPaisDestino the new numero pais destino
	 */
	void setNumeroPaisDestino(String numeroPaisDestino);

	/**
	 * Gets the pais destino.
	 *
	 * @return the pais destino
	 */
	String getPaisDestino();

	/**
	 * Sets the pais destino.
	 *
	 * @param paisDestino the new pais destino
	 */
	void setPaisDestino(String paisDestino);

	/**
	 * Gets the cnae.
	 *
	 * @return the cnae
	 */
	String getCnae();

	/**
	 * Sets the cnae.
	 *
	 * @param cnae the new cnae
	 */
	void setCnae(String cnae);

	/**
	 * Gets the descricao empenho.
	 *
	 * @return the descricao empenho
	 */
	String getDescricaoEmpenho();

	/**
	 * Sets the descricao empenho.
	 *
	 * @param descricaoEmpenho the new descricao empenho
	 */
	void setDescricaoEmpenho(String descricaoEmpenho);

	/**
	 * Gets the descricao contrato.
	 *
	 * @return the descricao contrato
	 */
	String getDescricaoContrato();

	/**
	 * Sets the descricao contrato.
	 *
	 * @param descricaoContrato the new descricao contrato
	 */
	void setDescricaoContrato(String descricaoContrato);

	/**
	 * Gets the ind cancelamento.
	 *
	 * @return the ind cancelamento
	 */
	Integer getIndCancelamento();

	/**
	 * Sets the ind cancelamento.
	 *
	 * @param indCancelamento the new ind cancelamento
	 */
	void setIndCancelamento(Integer indCancelamento);

	/**
	 * Gets the ind fin em.
	 *
	 * @return the ind fin em
	 */
	Integer getIndFinEm();

	/**
	 * Sets the ind fin em.
	 *
	 * @param indFinEm the new ind fin em
	 */
	void setIndFinEm(Integer indFinEm);

	/**
	 * Gets the ind processo nota fiscal.
	 *
	 * @return the ind processo nota fiscal
	 */
	Integer getIndProcessoNotaFiscal();

	/**
	 * Sets the ind processo nota fiscal.
	 *
	 * @param indProcessoNotaFiscal the new ind processo nota fiscal
	 */
	void setIndProcessoNotaFiscal(Integer indProcessoNotaFiscal);

	/**
	 * Gets the ind fmt danfe.
	 *
	 * @return the ind fmt danfe
	 */
	String getIndFmtDanfe();

	/**
	 * Sets the ind fmt danfe.
	 *
	 * @param indFmtDanfe the new ind fmt danfe
	 */
	void setIndFmtDanfe(String indFmtDanfe);

	/**
	 * Gets the ind emit contigencia.
	 *
	 * @return the ind emit contigencia
	 */
	Integer getIndEmitContigencia();

	/**
	 * Sets the ind emit contigencia.
	 *
	 * @param indEmitContigencia the new ind emit contigencia
	 */
	void setIndEmitContigencia(Integer indEmitContigencia);

	/**
	 * Gets the ind nf autorizado.
	 *
	 * @return the ind nf autorizado
	 */
	Integer getIndNfAutorizado();

	/**
	 * Sets the ind nf autorizado.
	 *
	 * @param indNfAutorizado the new ind nf autorizado
	 */
	void setIndNfAutorizado(Integer indNfAutorizado);

	/**
	 * Gets the data entrada sistema.
	 *
	 * @return the data entrada sistema
	 */
	Date getDataEntradaSistema();

	/**
	 * Sets the data entrada sistema.
	 *
	 * @param dataEntradaSistema the new data entrada sistema
	 */
	void setDataEntradaSistema(Date dataEntradaSistema);

	/**
	 * Gets the sis operacao.
	 *
	 * @return the sis operacao
	 */
	String getSisOperacao();

	/**
	 * Sets the sis operacao.
	 *
	 * @param sisOperacao the new sis operacao
	 */
	void setSisOperacao(String sisOperacao);

	/**
	 * Gets the sis status.
	 *
	 * @return the sis status
	 */
	String getSisStatus();

	/**
	 * Sets the sis status.
	 *
	 * @param sisStatus the new sis status
	 */
	void setSisStatus(String sisStatus);

	/**
	 * Gets the sis data status.
	 *
	 * @return the sis data status
	 */
	Date getSisDataStatus();

	/**
	 * Sets the sis data status.
	 *
	 * @param sisDataStatus the new sis data status
	 */
	void setSisDataStatus(Date sisDataStatus);

	/**
	 * Gets the cod regime trib emitente.
	 *
	 * @return the cod regime trib emitente
	 */
	Integer getCodRegimeTribEmitente();

	/**
	 * Sets the cod regime trib emitente.
	 *
	 * @param codRegimeTribEmitente the new cod regime trib emitente
	 */
	void setCodRegimeTribEmitente(Integer codRegimeTribEmitente);

	/**
	 * Gets the lista nfe chave referenciada.
	 *
	 * @return the lista nfe chave referenciada
	 */
	Collection<NfeChaveReferenciada> getListaNfeChaveReferenciada();

	/**
	 * Sets the lista nfe chave referenciada.
	 *
	 * @param listaNfeChaveReferenciada the new lista nfe chave referenciada
	 */
	void setListaNfeChaveReferenciada(Collection<NfeChaveReferenciada> listaNfeChaveReferenciada);

	/**
	 * Gets the lista nfe item.
	 *
	 * @return the lista nfe item
	 */
	Collection<NfeItem> getListaNfeItem();

	/**
	 * Sets the lista nfe item.
	 *
	 * @param listaNfeItem the new lista nfe item
	 */
	void setListaNfeItem(Collection<NfeItem> listaNfeItem);

	/**
	 * Gets the data cancelamento.
	 *
	 * @return the dataCancelamento
	 */
	Date getDataCancelamento();

	/**
	 * Sets the data cancelamento.
	 *
	 * @param dataCancelamento            the dataCancelamento to set
	 */
	void setDataCancelamento(Date dataCancelamento);

	/**
	 * Gets the justificativa cancelamento.
	 *
	 * @return the justificativaCancelamento
	 */
	String getJustificativaCancelamento();

	/**
	 * Sets the justificativa cancelamento.
	 *
	 * @param justificativaCancelamento            the justificativaCancelamento to
	 *            set
	 */
	void setJustificativaCancelamento(String justificativaCancelamento);

	/**
	 * Gets the usuario cancelamento.
	 *
	 * @return the usuarioCancelamento
	 */
	Usuario getUsuarioCancelamento();

	/**
	 * Sets the usuario cancelamento.
	 *
	 * @param usuarioCancelamento            the usuarioCancelamento to set
	 */
	void setUsuarioCancelamento(Usuario usuarioCancelamento);

	/**
	 * Sets the nfe usuario.
	 *
	 * @param nfeUsuario the new nfe usuario
	 */
	void setNfeUsuario(NfeUsuario nfeUsuario);

	/**
	 * Gets the nfe usuario.
	 *
	 * @return the nfe usuario
	 */
	NfeUsuario getNfeUsuario();

	/**
	 * Sets the numero ibge uf.
	 *
	 * @param numeroIbgeUf the new numero ibge uf
	 */
	void setNumeroIbgeUf(String numeroIbgeUf);

	/**
	 * Gets the numero ibge uf.
	 *
	 * @return the numero ibge uf
	 */
	String getNumeroIbgeUf();

	/**
	 * Sets the numero ibge municipio.
	 *
	 * @param numeroIbgeMunicipio the new numero ibge municipio
	 */
	void setNumeroIbgeMunicipio(String numeroIbgeMunicipio);

	/**
	 * Gets the numero ibge municipio.
	 *
	 * @return the numero ibge municipio
	 */
	String getNumeroIbgeMunicipio();

	/**
	 * Sets the numero ibge municipio destino.
	 *
	 * @param numeroIbgeMunicipioDestino the new numero ibge municipio destino
	 */
	void setNumeroIbgeMunicipioDestino(String numeroIbgeMunicipioDestino);

	/**
	 * Gets the numero ibge municipio destino.
	 *
	 * @return the numero ibge municipio destino
	 */
	String getNumeroIbgeMunicipioDestino();

	/**
	 * Sets the numero ibge municipio fg.
	 *
	 * @param numeroIbgeMunicipioFg the new numero ibge municipio fg
	 */
	void setNumeroIbgeMunicipioFg(String numeroIbgeMunicipioFg);

	/**
	 * Gets the numero ibge municipio fg.
	 *
	 * @return the numero ibge municipio fg
	 */
	String getNumeroIbgeMunicipioFg();

	/**
	 * Sets the nome logradouro.
	 *
	 * @param nomeLogradouro the new nome logradouro
	 */
	void setNomeLogradouro(String nomeLogradouro);

	/**
	 * Gets the nome logradouro.
	 *
	 * @return the nome logradouro
	 */
	String getNomeLogradouro();

	/**
	 * Sets the numero logradouro.
	 *
	 * @param numeroLogradouro the new numero logradouro
	 */
	void setNumeroLogradouro(String numeroLogradouro);

	/**
	 * Gets the numero logradouro.
	 *
	 * @return the numero logradouro
	 */
	String getNumeroLogradouro();

	/**
	 * Sets the descricao bairro.
	 *
	 * @param descricaoBairro the new descricao bairro
	 */
	void setDescricaoBairro(String descricaoBairro);

	/**
	 * Gets the descricao bairro.
	 *
	 * @return the descricao bairro
	 */
	String getDescricaoBairro();

	/**
	 * Sets the descricao municipio.
	 *
	 * @param descricaoMunicipio the new descricao municipio
	 */
	void setDescricaoMunicipio(String descricaoMunicipio);

	/**
	 * Gets the descricao municipio.
	 *
	 * @return the descricao municipio
	 */
	String getDescricaoMunicipio();

	/**
	 * Sets the descricao uf.
	 *
	 * @param descricaoUf the new descricao uf
	 */
	void setDescricaoUf(String descricaoUf);

	/**
	 * Gets the descricao uf.
	 *
	 * @return the descricao uf
	 */
	String getDescricaoUf();

	/**
	 * Sets the base calculo icms.
	 *
	 * @param baseCalculoIcms the new base calculo icms
	 */
	void setBaseCalculoIcms(Double baseCalculoIcms);

	/**
	 * Gets the base calculo icms.
	 *
	 * @return the base calculo icms
	 */
	Double getBaseCalculoIcms();

	/**
	 * Sets the valor icms.
	 *
	 * @param valorIcms the new valor icms
	 */
	void setValorIcms(Double valorIcms);

	/**
	 * Gets the valor icms.
	 *
	 * @return the valor icms
	 */
	Double getValorIcms();

	/**
	 * Sets the base calculo icms subst.
	 *
	 * @param baseCalculoIcmsSubst the new base calculo icms subst
	 */
	void setBaseCalculoIcmsSubst(Double baseCalculoIcmsSubst);

	/**
	 * Gets the base calculo icms subst.
	 *
	 * @return the base calculo icms subst
	 */
	Double getBaseCalculoIcmsSubst();

	/**
	 * Sets the valor icms subst.
	 *
	 * @param valorIcmsSubst the new valor icms subst
	 */
	void setValorIcmsSubst(Double valorIcmsSubst);

	/**
	 * Gets the valor icms subst.
	 *
	 * @return the valor icms subst
	 */
	Double getValorIcmsSubst();

	/**
	 * Sets the valor pis.
	 *
	 * @param valorPis the new valor pis
	 */
	void setValorPis(Double valorPis);

	/**
	 * Gets the valor pis.
	 *
	 * @return the valor pis
	 */
	Double getValorPis();

	/**
	 * Sets the valor cofins.
	 *
	 * @param valorCofins the new valor cofins
	 */
	void setValorCofins(Double valorCofins);

	/**
	 * Gets the valor cofins.
	 *
	 * @return the valor cofins
	 */
	Double getValorCofins();

	/**
	 * Sets the destino.
	 *
	 * @param destino the new destino
	 */
	void setDestino(String destino);

	/**
	 * Gets the destino.
	 *
	 * @return the destino
	 */
	String getDestino();

	/**
	 * Sets the logradouro destino.
	 *
	 * @param logradouroDestino the new logradouro destino
	 */
	void setLogradouroDestino(String logradouroDestino);

	/**
	 * Gets the logradouro destino.
	 *
	 * @return the logradouro destino
	 */
	String getLogradouroDestino();

	/**
	 * Sets the numero logradouro destino.
	 *
	 * @param numeroLogradouroDestino the new numero logradouro destino
	 */
	void setNumeroLogradouroDestino(String numeroLogradouroDestino);

	/**
	 * Gets the numero logradouro destino.
	 *
	 * @return the numero logradouro destino
	 */
	String getNumeroLogradouroDestino();

	/**
	 * Sets the complemento destino.
	 *
	 * @param complementoDestino the new complemento destino
	 */
	void setComplementoDestino(String complementoDestino);

	/**
	 * Gets the complemento destino.
	 *
	 * @return the complemento destino
	 */
	String getComplementoDestino();

	/**
	 * Sets the bairro destino.
	 *
	 * @param bairroDestino the new bairro destino
	 */
	void setBairroDestino(String bairroDestino);

	/**
	 * Gets the bairro destino.
	 *
	 * @return the bairro destino
	 */
	String getBairroDestino();

	/**
	 * Sets the municipio destino.
	 *
	 * @param municipioDestino the new municipio destino
	 */
	void setMunicipioDestino(String municipioDestino);

	/**
	 * Gets the municipio destino.
	 *
	 * @return the municipio destino
	 */
	String getMunicipioDestino();

	/**
	 * Sets the uf destino.
	 *
	 * @param ufDestino the new uf destino
	 */
	void setUfDestino(String ufDestino);

	/**
	 * Gets the uf destino.
	 *
	 * @return the uf destino
	 */
	String getUfDestino();

	/**
	 * Sets the cep destino.
	 *
	 * @param cepDestino the new cep destino
	 */
	void setCepDestino(String cepDestino);

	/**
	 * Gets the cep destino.
	 *
	 * @return the cep destino
	 */
	String getCepDestino();

	/**
	 * Gets the numero mensagem c.
	 *
	 * @return the numero mensagem c
	 */
	String getNumeroMensagemC();

	/**
	 * Sets the numero mensagem c.
	 *
	 * @param numeroMensagemC the new numero mensagem c
	 */
	void setNumeroMensagemC(String numeroMensagemC);

	/**
	 * Gets the numero mensagem e.
	 *
	 * @return the numero mensagem e
	 */
	String getNumeroMensagemE();

	/**
	 * Sets the numero mensagem e.
	 *
	 * @param numeroMensagemE the new numero mensagem e
	 */
	void setNumeroMensagemE(String numeroMensagemE);

	/**
	 * Sets the cnpj cpf destino.
	 *
	 * @param cnpjCpfDestino the new cnpj cpf destino
	 */
	void setCnpjCpfDestino(String cnpjCpfDestino);

	/**
	 * Gets the cnpj cpf destino.
	 *
	 * @return the cnpj cpf destino
	 */
	String getCnpjCpfDestino();

	/**
	 * Sets the telefone destino.
	 *
	 * @param telefoneDestino the new telefone destino
	 */
	void setTelefoneDestino(String telefoneDestino);

	/**
	 * Gets the telefone destino.
	 *
	 * @return the telefone destino
	 */
	String getTelefoneDestino();

	/**
	 * Sets the inscricao estadual destino.
	 *
	 * @param inscricaoEstadualDestino the new inscricao estadual destino
	 */
	void setInscricaoEstadualDestino(String inscricaoEstadualDestino);

	/**
	 * Gets the inscricao estadual destino.
	 *
	 * @return the inscricao estadual destino
	 */
	String getInscricaoEstadualDestino();

	/**
	 * Sets the ind forma emissao.
	 *
	 * @param indFormaEmissao the new ind forma emissao
	 */
	void setIndFormaEmissao(String indFormaEmissao);

	/**
	 * Gets the ind forma emissao.
	 *
	 * @return the ind forma emissao
	 */
	String getIndFormaEmissao();

	/**
	 * Sets the entrada saida.
	 *
	 * @param entradaSaida the new entrada saida
	 */
	void setEntradaSaida(Date entradaSaida);

	/**
	 * Gets the entrada saida.
	 *
	 * @return the entrada saida
	 */
	Date getEntradaSaida();

	/**
	 * Sets the complemento logradouro.
	 *
	 * @param complementoLogradouro the new complemento logradouro
	 */
	void setComplementoLogradouro(String complementoLogradouro);

	/**
	 * Gets the complemento logradouro.
	 *
	 * @return the complemento logradouro
	 */
	String getComplementoLogradouro();

	/**
	 * Sets the telefone.
	 *
	 * @param telefone the new telefone
	 */
	void setTelefone(String telefone);

	/**
	 * Gets the telefone.
	 *
	 * @return the telefone
	 */
	String getTelefone();

	/**
	 * Sets the ind ambiente sistema.
	 *
	 * @param indAmbienteSistema the new ind ambiente sistema
	 */
	void setIndAmbienteSistema(Integer indAmbienteSistema);

	/**
	 * Gets the ind ambiente sistema.
	 *
	 * @return the ind ambiente sistema
	 */
	Integer getIndAmbienteSistema();

	/**
	 * Sets the versao xml.
	 *
	 * @param versaoXml the new versao xml
	 */
	void setVersaoXml(String versaoXml);

	/**
	 * Gets the versao xml.
	 *
	 * @return the versao xml
	 */
	String getVersaoXml();

	/**
	 * Sets the codigo lote.
	 *
	 * @param codigoLote the new codigo lote
	 */
	void setCodigoLote(Long codigoLote);

	/**
	 * Gets the codigo lote.
	 *
	 * @return the codigo lote
	 */
	Long getCodigoLote();

	/**
	 * Sets the chave acesso.
	 *
	 * @param chaveAcesso the new chave acesso
	 */
	void setChaveAcesso(String chaveAcesso);

	/**
	 * Gets the chave acesso.
	 *
	 * @return the chave acesso
	 */
	String getChaveAcesso();

	/**
	 * Gets the impressa contingencia.
	 *
	 * @return the impressa contingencia
	 */
	Integer getImpressaContingencia();

	/**
	 * Sets the impressa contingencia.
	 *
	 * @param impressaContingencia the new impressa contingencia
	 */
	void setImpressaContingencia(Integer impressaContingencia);

	/**
	 * Gets the danfe pdf.
	 *
	 * @return the danfe pdf
	 */
	byte[] getDanfePdf();

	/**
	 * Sets the danfe pdf.
	 *
	 * @param danfePdf the new danfe pdf
	 */
	void setDanfePdf(byte[] danfePdf);

	/**
	 * Gets the ds arquivo xml.
	 *
	 * @return the ds arquivo xml
	 */
	byte[] getDsArquivoXml();

	/**
	 * Sets the ds arquivo xml.
	 *
	 * @param dsArquivoXml the new ds arquivo xml
	 */
	void setDsArquivoXml(byte[] dsArquivoXml);

	/**
	 * Gets the cep.
	 *
	 * @return the cep
	 */
	String getCep();

	/**
	 * Sets the cep.
	 *
	 * @param cep the new cep
	 */
	void setCep(String cep);

	/**
	 * Sets the ind presenca.
	 *
	 * @param indPresenca the new ind presenca
	 */
	void setIndPresenca(Integer indPresenca);

	/**
	 * Gets the ind presenca.
	 *
	 * @return the ind presenca
	 */
	Integer getIndPresenca();

	/**
	 * Sets the ind consumidor final.
	 *
	 * @param indConsumidorFinal the new ind consumidor final
	 */
	void setIndConsumidorFinal(Integer indConsumidorFinal);

	/**
	 * Gets the ind consumidor final.
	 *
	 * @return the ind consumidor final
	 */
	Integer getIndConsumidorFinal();

	/**
	 * Sets the ind ie destinatario.
	 *
	 * @param indPresenca the new ind ie destinatario
	 */
	void setIndIEDestinatario(Integer indPresenca);

	/**
	 * Gets the ind ie destinatario.
	 *
	 * @return the ind ie destinatario
	 */
	Integer getIndIEDestinatario();

	/**
	 * Gets the valor icms desonerado.
	 *
	 * @return the valor icms desonerado
	 */
	public Double getValorIcmsDesonerado();

	/**
	 * Sets the valor icms desonerado.
	 *
	 * @param valorIcmsDesonerado the new valor icms desonerado
	 */
	public void setValorIcmsDesonerado(Double valorIcmsDesonerado);

	/**
	 * Sets the ind local destino operacao.
	 *
	 * @param indLocalDestinoOperacao the new ind local destino operacao
	 */
	void setIndLocalDestinoOperacao(Integer indLocalDestinoOperacao);

	/**
	 * Gets the ind local destino operacao.
	 *
	 * @return the ind local destino operacao
	 */
	Integer getIndLocalDestinoOperacao();

	/**
	 * Gets the usuario contigencia.
	 *
	 * @return the usuario contigencia
	 */
	Usuario getUsuarioContigencia();

	/**
	 * Sets the usuario contigencia.
	 *
	 * @param usuarioContigencia the new usuario contigencia
	 */
	void setUsuarioContigencia(Usuario usuarioContigencia);
	
	/**
	 * Gets the Número de Protocolo de Envio.
	 *
	 * @param numeroProtocoloEnvio the new Número de Protocolo de Envio
	 */
	String getNumeroProtocoloEnvio();
	
	/**
	 * Sets the Número de Protocolo de Envio.
	 *
	 * @param numeroProtocoloEnvio the new Número de Protocolo de Envio
	 */	
	void setNumeroProtocoloEnvio(String numeroProtocoloEnvio);
}
