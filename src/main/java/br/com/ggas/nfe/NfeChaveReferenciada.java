/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe;

import java.util.Date;

import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface NfeChaveReferenciada extends EntidadeNegocio {

	final String BEAN_ID_NFE_CHAVE_REF = "nfeChaveReferenciada";

	final String CHAVE_ACESSO = "CHAVE_REFERENCIADA_CHAVE_ACESSO";

	final String NOTA_FISCAL = "CHAVE_REFERENCIADA_NOTA_FISCAL";

	final String EMPRESA = "CHAVE_REFERENCIADA_EMPRESA";

	final String IBGE_UF = "CHAVE_REFERENCIADA_IBGE_UF";

	final String DATA_EMISSAO = "CHAVE_REFERENCIADA_DATA_EMISSAO";

	final String CNPJ = "CHAVE_REFERENCIADA_CNPJ";

	final String NUMERO_MODELO = "CHAVE_REFERENCIADA_NUMERO_MODELO";

	final String SERIE = "CHAVE_REFERENCIADA_SERIE";

	final String DOCUMENTO = "CHAVE_REFERENCIADA_NUMERO_DOCUMENTO";

	/**
	 * @return NfeChaveRerefenciada - Retorna nfe chave referenciada.
	 */
	NfeChaveReferenciada getNfeChaveReferenciada();

	/**
	 * @param nfeChaveReferenciada - Set Nfe chave referenciada.
	 */
	void setNfeChaveReferenciada(NfeChaveReferenciada nfeChaveReferenciada);

	/**
	 * @return String - Retorna Chave de acesso.
	 */
	String getChaveAcesso();

	/**
	 * @param chaveAcesso - Setchave de acesso.
	 */
	void setChaveAcesso(String chaveAcesso);

	/**
	 * @return NFE - Retorna NFE.
	 */
	Nfe getNfe();

	/**
	 * @param nfe - sET NFE.
	 */
	void setNfe(Nfe nfe);

	/**
	 * @return NfeEmpresa - Retorna Nfe empresa. 
	 */
	NfeEmpresa getNfeEmpresa();

	/**
	 * @param nfeEmpresa - Set NFe Empresa.
	 */
	void setNfeEmpresa(NfeEmpresa nfeEmpresa);

	/**
	 * @return Integer - Set Número IBGE UF.
	 */
	Integer getNumeroIbgeUf();

	/**
	 * @param numeroIbgeUf - Set número ibge uf. 
	 */
	void setNumeroIbgeUf(Integer numeroIbgeUf);

	/**
	 * @return Date - Retorna emissão.
	 */
	Date getEmissao();

	/**
	 * @param emissao - Set emissão. 
	 */
	void setEmissao(Date emissao);

	/**
	 * @return String - Retorna Cnpj.
	 */
	String getCnpj();

	/**
	 * @param cnpj - Set cnpj.
	 */
	void setCnpj(String cnpj);

	/**
	 * @return Integer - Retorna número do modelo.
	 */
	Integer getNumeroModelo();

	/**
	 * @param numeroModelo - Set número do modelo.
	 */
	void setNumeroModelo(Integer numeroModelo);

	/**
	 * @return Integer - Retorna número de serie.
	 */
	Integer getNumeroSerie();

	/**
	 * @param numeroSerie - Set número de serie.
	 */
	void setNumeroSerie(Integer numeroSerie);

	/**
	 * @return Integer - Retorna número do documento.
	 */
	Integer getNumeroDocumento();

	/**
	 * @param numeroDocumento - Set Número do documento.
	 */
	void setNumeroDocumento(Integer numeroDocumento);

}
