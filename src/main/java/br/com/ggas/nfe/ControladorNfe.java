/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe;

import java.util.Collection;
import java.util.Date;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.fatura.DadosResumoFatura;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.ControladorNegocio;

/**
 * 
 * 
 */
public interface ControladorNfe extends ControladorNegocio {

	String BEAN_ID_CONTROLADOR_NFE = "controladorNfe";

	/**
	 * Consultar dados nota fiscal eletronica.
	 * 
	 * @param numeroDocFiscal
	 *            the numero doc fiscal
	 * @param numeroSerie
	 *            the numero serie
	 * @param dataEmissao
	 *            the data emissao
	 * @return the nfe
	 */
	Nfe consultarDadosNotaFiscalEletronica(Long numeroDocFiscal, String numeroSerie, Date dataEmissao);

	/**
	 * Inserir nfe cancelamento.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the nfe
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	Nfe inserirNfeCancelamento(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Inserir nfe chave referenciada cancelamento.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param nfe
	 *            the nfe
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	void inserirNfeChaveReferenciadaCancelamento(DocumentoFiscal documentoFiscal, Nfe nfe, DadosAuditoria dadosAuditoria)
					throws NegocioException;

	/**
	 * Emitir nfe saida.
	 * 
	 * @param logProcessamento
	 *            the log processamento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void emitirNfeSaida(StringBuilder logProcessamento, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Emitir nfe entrada.
	 * 
	 * @param logProcessamento
	 *            the log processamento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 * @throws GGASException
	 *             the GGAS exception
	 */
	void emitirNfeEntrada(StringBuilder logProcessamento, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Processar retorno nfe entrada.
	 * 
	 * @param logProcessamento
	 *            the log processamento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void processarRetornoNfeEntrada(StringBuilder logProcessamento, DadosAuditoria dadosAuditoria) throws GGASException;

	/**
	 * Processar retorno nfe saida.
	 * 
	 * @param logProcessamento
	 *            the log processamento
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param dataEmissao
	 * 			  the date emissão nfe           
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void processarRetornoNfeSaida(StringBuilder logProcessamento, DadosAuditoria dadosAuditoria, String dataEmissao) throws GGASException;

	/**
	 * Tratar dados nfe status autorizada.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosResumoFatura
	 *            the dados resumo fatura
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param statusAutorizada
	 *            the status autorizada
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void tratarDadosNfeStatusAutorizada(DocumentoFiscal documentoFiscal, DadosResumoFatura dadosResumoFatura,
					DadosAuditoria dadosAuditoria, EntidadeConteudo statusAutorizada) throws NegocioException, ConcorrenciaException;

	/**
	 * Tratar dados nfe entrada status cancelada.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param statusCancelada
	 *            the status cancelada
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	void tratarDadosNfeEntradaStatusCancelada(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria,
					EntidadeConteudo statusCancelada) throws NegocioException, ConcorrenciaException;

	/**
	 * Consultar nfe.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param serie
	 *            the serie
	 * @param habilitado
	 *            the habilitado
	 * @param ordenacao
	 *            the ordenacao
	 * @param propriedadesLazy
	 *            the propriedades lazy
	 * @return the collection
	 * @throws NegocioException
	 *             the negocio exception
	 */
	Collection<Nfe> consultarNfe(Long documentoFiscal, Long serie, Boolean habilitado, String ordenacao, String... propriedadesLazy)
					throws NegocioException;

	/**
	 * Inserir fatura impressao.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void inserirFaturaImpressao(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria) throws NegocioException;

	/**
	 * Obtem Nfe Usuario
	 * 
	 * @param login -- {@link String}
	 * @return nfeUsuario - {@link NfeUsuario}
	 * @throws NegocioException - {@link NegocioException}
	 */
	NfeUsuario obterNfeUsuario(String login) throws NegocioException;
	
	/**
	 * Consulta Integração de Nota Fiscal por tipo de operação nfe
	 * @param operacao - {@link String}
	 * @param status - {@link String}
	 * @return colecao Nfe - {@link Nfe}
	 * @throws NegocioException the negocio exception
	 */
	Collection<Nfe>consultarIntegracaoNotaFiscalTipoOperacaoNfe(String operacao, String status)throws NegocioException;
	
}
