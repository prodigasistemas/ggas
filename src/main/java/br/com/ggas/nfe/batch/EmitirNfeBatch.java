/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe.batch;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.Batch;
import br.com.ggas.batch.Processo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.nfe.ControladorNfe;
import br.com.ggas.nfe.NfeUsuario;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class EmitirNfeBatch implements Batch {

	private static final String PROCESSO = "processo";

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.batch.Batch#processar(java.util.Map)
	 */
	@Override
	public String processar(Map<String, Object> parametros) throws GGASException {

		Processo processo = (Processo) parametros.get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();


		ControladorParametroSistema controladorParametrosSistema = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

		DadosAuditoria dadosAuditoria = Util.getDadosAuditoria(processo.getUsuario(), processo.getOperacao(),
						(String) controladorParametrosSistema.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_SERVIDOR_IP));

		ControladorNfe controladorNfe = (ControladorNfe) ServiceLocator.getInstancia().getBeanPorID(
						ControladorNfe.BEAN_ID_CONTROLADOR_NFE);
		
		boolean impedeProcessamento = validaDados(logProcessamento, dadosAuditoria, controladorNfe);

		if(!impedeProcessamento){
			controladorNfe.emitirNfeSaida(logProcessamento, dadosAuditoria);
			controladorNfe.emitirNfeEntrada(logProcessamento, dadosAuditoria);
		}


		return logProcessamento.toString();

	}

	private boolean validaDados(StringBuilder logProcessamento, DadosAuditoria dadosAuditoria, ControladorNfe controladorNfe)
			throws NegocioException {
		
		boolean retorno = false;
		
		NfeUsuario nfeUsuario = controladorNfe.obterNfeUsuario(dadosAuditoria.getUsuario().getLogin());
		if(nfeUsuario==null){
			logProcessamento.append("\r\nUsuário não cadastrado no Portal Nfe. Favor cadastrá-lo para realizar esta operação.");
			logProcessamento.append("\r\nATENÇÃO: O USUÁRIO DEVE SER CADASTRADO COM O MESMO LOGIN QUE POSSUI NO GGAS.");
			retorno = true;
		}
		
		return retorno;
	}

}
