/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.com.ggas.integracao.cliente.IntegracaoCliente;
import br.com.ggas.integracao.contrato.IntegracaoContrato;
import br.com.ggas.parametrosistema.ParametroSistema;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ClienteSituacao;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.ControladorRamoAtividadeSubstituicaoTributaria;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contabil.impl.OperacaoContabil;
import br.com.ggas.faturamento.ControladorDocumentoCobranca;
import br.com.ggas.faturamento.CreditoDebitoSituacao;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaImpressao;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturamentoAnormalidade;
import br.com.ggas.faturamento.NaturezaOperacaoCFOP;
import br.com.ggas.faturamento.RefaturamentoHistorico;
import br.com.ggas.faturamento.creditodebito.ControladorCreditoDebito;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.DadosResumoFatura;
import br.com.ggas.faturamento.fatura.FaturaItemTributacao;
import br.com.ggas.faturamento.fatura.FaturaTributacao;
import br.com.ggas.faturamento.rubrica.RubricaTributo;
import br.com.ggas.faturamento.tributo.ControladorTributo;
import br.com.ggas.faturamento.tributo.RamoAtividadeSubstituicaoTributaria;
import br.com.ggas.faturamento.tributo.TributoAliquota;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.geral.negocio.impl.ControladorNegocioImpl;
import br.com.ggas.integracao.geral.ControladorIntegracao;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.nfe.ControladorNfe;
import br.com.ggas.nfe.Nfe;
import br.com.ggas.nfe.NfeChaveReferenciada;
import br.com.ggas.nfe.NfeEmpresa;
import br.com.ggas.nfe.NfeItem;
import br.com.ggas.nfe.NfeLog;
import br.com.ggas.nfe.NfeMensagem;
import br.com.ggas.nfe.NfeUsuario;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

class ControladorNfeImpl extends ControladorNegocioImpl implements ControladorNfe {

	private static final int CEM = 100;

	private static final int PROCESSO_CANCELAMENTO = 1;

	private static final String COMPLETAR_JUSTIFICATIVA_CANCELAMENTO = "***************";

	private final ControladorIntegracao controladorIntegracao =
			(ControladorIntegracao) ServiceLocator.getInstancia().getControladorNegocio(
							ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
	
	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.ControladorNegocioImpl#criar()
	 */
	@Override
	public EntidadeNegocio criar() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(Nfe.BEAN_ID_NFE);
	}

	/**
	 * Criar nfe item.
	 * 
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarNfeItem() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(NfeItem.BEAN_ID_ITEM);
	}

	@Override
	public Class<?> getClasseEntidade() {

		return ServiceLocator.getInstancia().getClassPorID(Nfe.BEAN_ID_NFE);
	}

	public Class<?> getClasseEntidadeNfeItem() {

		return ServiceLocator.getInstancia().getClassPorID(NfeItem.BEAN_ID_ITEM);
	}

	/**
	 * Criar nfe chave referenciada.
	 * 
	 * @return the entidade negocio
	 */
	public EntidadeNegocio criarNfeChaveReferenciada() {

		return (EntidadeNegocio) ServiceLocator.getInstancia().getBeanPorID(NfeChaveReferenciada.BEAN_ID_NFE_CHAVE_REF);
	}

	public Class<?> getClasseEntidadeNfeEmpresa() {

		return ServiceLocator.getInstancia().getClassPorID(NfeEmpresa.BEAN_ID_EMPRESA);
	}

	public Class<?> getClasseEntidadeNfeUsuario() {

		return ServiceLocator.getInstancia().getClassPorID(NfeUsuario.BEAN_ID_USUARIO);
	}

	public Class<?> getClasseEntidadeNfePais() {

		return ServiceLocator.getInstancia().getClassPorID(NfeEmpresa.BEAN_ID_EMPRESA);
	}

	public Class<?> getClasseEntidadeDocumentoFiscal() {

		return ServiceLocator.getInstancia().getClassPorID(DocumentoFiscal.BEAN_ID_DOCUMENTO_FISCAL);
	}

	public Class<?> getClasseEntidadeRefaturamentoHistorico() {

		return ServiceLocator.getInstancia().getClassPorID(RefaturamentoHistorico.BEAN_ID_REFATURAMENTO_HISTORICO);

	}

	/**
	 * Popular nfe.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param nfe
	 *            the nfe
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public void popularNfe(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria, Nfe nfe) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorEntidadeConteudo controladorEntidadeConteudo =
						(ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
										ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		ControladorEmpresa controladorEmpresa =
						(ControladorEmpresa) ServiceLocator.getInstancia().getBeanPorID(ControladorEmpresa.BEAN_ID_CONTROLADOR_EMPRESA);
		ControladorParametroSistema controladorParametroSistema =
						(ControladorParametroSistema) ServiceLocator.getInstancia().getBeanPorID(
										ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		Long codigoTipoOperacaoEntrada =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_ENTRADA));
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		NfeEmpresa nfeEmpresa = this.obterNfeEmpresa(empresa.getChavePrimaria());

		nfe.setNfeEmpresa(nfeEmpresa);
		nfe.setIndDanfeImpressa(0);
		
		if (documentoFiscal.getFatura().getNaturezaOperacaoCFOP() != null) {
			nfe.setNaturezaOperacao(documentoFiscal.getFatura().getNaturezaOperacaoCFOP().getCodigoCFOP().toString());
		} else {
			nfe.setNaturezaOperacao(" ");
		}

		nfe.setImpressaContingencia(0);
		nfe.setFormaPagamento(0);
		nfe.setModeloDocumentoFiscal(55);
		nfe.setSerie(Integer.parseInt(documentoFiscal.getSerie().getNumero()));
		nfe.setDocumentoFiscal(Integer.parseInt(documentoFiscal.getNumero().toString()));
		nfe.setEmissao(documentoFiscal.getDataEmissao());
		nfe.setEntradaSaida(documentoFiscal.getDataEmissao());
		if (documentoFiscal.getTipoOperacao().getChavePrimaria() == codigoTipoOperacaoEntrada) {
			nfe.setIndEntradaSaida(0);
		} else {
			nfe.setIndEntradaSaida(1);
		}
		nfe.setCnpj(nfeEmpresa.getCnpj());
		nfe.setRazaoSocial(nfeEmpresa.getNome());
		nfe.setNomeFantasia(nfeEmpresa.getNomeFantasia());

		nfe.setPais(empresa.getCliente().getEnderecoPrincipal().getMunicipio().getUnidadeFederacao().getPais().getNome());
		nfe.setNumeroPais(empresa.getCliente().getEnderecoPrincipal().getMunicipio().getUnidadeFederacao().getPais().getCodigoBacen()
						.toString());

		if ("1".equals(empresa.getCliente().getRegimeRecolhimento())) {
			nfe.setInscricaoEstadual("ISENTO");
		} else if ("2".equals(empresa.getCliente().getRegimeRecolhimento())) {
			nfe.setInscricaoEstadual("ISENTO");
		} else if (empresa.getCliente().getInscricaoEstadual() != null) {
			nfe.setInscricaoEstadual(empresa.getCliente().getInscricaoEstadual());
		} else {
			nfe.setInscricaoEstadual(" ");
		}

		nfe.setInscricaoMunicipal(empresa.getCliente().getInscricaoMunicipal());
		if (nfe.getInscricaoMunicipal() != null && !"".equals(nfe.getInscricaoMunicipal().trim())) {
			nfe.setCnae(empresa.getCliente().getAtividadeEconomica().getCodigoOriginal().toString());
		}

		nfe.setValorProduto(Double.parseDouble(documentoFiscal.getFatura().getValorTotal().toString()));
		nfe.setValorFrete(0D);
		nfe.setValorSeguro(0D);
		nfe.setValorDesconto(0D);
		nfe.setValorIpi(0D);
		nfe.setValorOutros(0D);
		nfe.setValorNotaFiscal(Double.parseDouble(documentoFiscal.getFatura().getValorTotal().toString()));
		nfe.setValorRetPis(0D);
		nfe.setValorRetCsll(0D);
		nfe.setBaseCalculoIrrf(0D);
		nfe.setBaseCalculoRetPrev(0D);
		nfe.setValorRetPrev(0D);

		ClienteEndereco clienteEndereco = documentoFiscal.getFatura().getCliente().getEnderecoPrincipal();
		if (clienteEndereco == null) {
			clienteEndereco = documentoFiscal.getFatura().getCliente().getEnderecos().iterator().next();
		}
		if (clienteEndereco.getMunicipio() != null) {
			nfe.setPaisDestino(clienteEndereco.getMunicipio().getUnidadeFederacao().getPais().getNome());
			nfe.setNumeroPaisDestino(clienteEndereco.getMunicipio().getUnidadeFederacao().getPais().getCodigoBacen().toString());
		} else {
			nfe.setPaisDestino(clienteEndereco.getCep().getMunicipio().getUnidadeFederacao().getPais().getNome());
			nfe.setNumeroPaisDestino(clienteEndereco.getCep().getMunicipio().getUnidadeFederacao().getPais().getCodigoBacen().toString());
		}

		if (documentoFiscal.getFatura().getContrato() != null) {
			nfe.setDescricaoContrato(documentoFiscal.getFatura().getContrato().getNumeroFormatado());
		} else if (documentoFiscal.getFatura().getContratoAtual() != null) {
			nfe.setDescricaoContrato(documentoFiscal.getFatura().getContratoAtual().getNumeroFormatado());
		}
		nfe.setIndCancelamento(0);
		nfe.setIndFinEm(1);
		nfe.setIndProcessoNotaFiscal(0);
		nfe.setIndFmtDanfe("1");
		NfeUsuario nfeUsuario = this.obterNfeUsuario(dadosAuditoria.getUsuario().getLogin());
		nfe.setNfeUsuario(nfeUsuario);
		nfe.setIndEmitContigencia(0);
		nfe.setIndNfAutorizado(0);
		nfe.setDataEntradaSistema(new Date(System.currentTimeMillis()));
		nfe.setSisOperacao("E");

		// forma de emissão normal (1) ou
		// contingencia (3)
		if (nfe.getSerie() < 900) {
			nfe.setIndFormaEmissao("1");
		} else {
			nfe.setIndFormaEmissao("3");
		}

		nfe.setSisDataStatus(new Date(System.currentTimeMillis()));

		Long codigoStatusNfeNaoProcessada =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_NAO_PROCESSADA));
		EntidadeConteudo statusNfeNaoProcessada = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeNaoProcessada);
		nfe.setSisStatus(statusNfeNaoProcessada.getDescricaoAbreviada());

		ClienteEndereco empresaEndereco = empresa.getCliente().getEnderecoPrincipal();
		if (empresaEndereco == null) {
			empresaEndereco = empresa.getCliente().getEnderecos().iterator().next();
		}
		if (empresaEndereco.getMunicipio() != null) {
			nfe.setNumeroIbgeUf(empresaEndereco.getMunicipio().getUnidadeFederacao().getUnidadeFederativaIBGE().toString());
			nfe.setNumeroIbgeMunicipio(empresaEndereco.getMunicipio().getUnidadeFederativaIBGE().toString());
			nfe.setNumeroIbgeMunicipioFg(empresaEndereco.getMunicipio().getUnidadeFederativaIBGE().toString());
		} else {
			nfe.setNumeroIbgeUf(empresaEndereco.getCep().getMunicipio().getUnidadeFederacao().getUnidadeFederativaIBGE().toString());
			nfe.setNumeroIbgeMunicipio(empresaEndereco.getCep().getMunicipio().getUnidadeFederativaIBGE().toString());
			nfe.setNumeroIbgeMunicipioFg(empresaEndereco.getCep().getMunicipio().getUnidadeFederativaIBGE().toString());
		}

		if (clienteEndereco.getMunicipio() != null) {
			nfe.setNumeroIbgeMunicipioDestino(clienteEndereco.getMunicipio().getUnidadeFederativaIBGE().toString());

		} else {
			nfe.setNumeroIbgeMunicipioDestino(clienteEndereco.getCep().getMunicipio().getUnidadeFederativaIBGE().toString());

		}

		nfe.setNomeLogradouro(empresaEndereco.getCep().getLogradouro());
		nfe.setComplementoLogradouro(empresaEndereco.getComplemento());
		nfe.setCep(empresaEndereco.getCep().getCep().replace("-", ""));

		ClienteFone clienteFonePrincipalEmpresa = null;
		for (ClienteFone clienteFone : empresa.getCliente().getFones()) {
			if (clienteFone.getIndicadorPrincipal()) {
				clienteFonePrincipalEmpresa = clienteFone;
				break;
			}
		}
		if (clienteFonePrincipalEmpresa != null) {
			nfe.setTelefone(clienteFonePrincipalEmpresa.getCodigoDDD().toString() + clienteFonePrincipalEmpresa.getNumero().toString());
		}

		if (empresaEndereco.getNumero() != null) {
			nfe.setNumeroLogradouro(empresaEndereco.getNumero());
		} else {
			nfe.setNumeroLogradouro("S/N");
		}

		nfe.setDescricaoBairro(empresaEndereco.getCep().getBairro());
		nfe.setDescricaoMunicipio(empresaEndereco.getCep().getNomeMunicipio());
		nfe.setDescricaoUf(empresaEndereco.getCep().getUf());

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Collection<FaturaTributacao> listaFaturaTributacao =
						controladorFatura.consultarFaturaTributacaoPorFatura(documentoFiscal.getFatura().getChavePrimaria());

		Long idTributoICMS = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ICMS));
		Long idTributoPIS = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PIS));
		Long idTributoCOFINS = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COFINS));

		nfe.setBaseCalculoIcms(0D);
		nfe.setValorIcms(0D);
		nfe.setBaseCalculoIcmsSubst(0D);
		nfe.setValorIcmsSubst(0D);
		nfe.setValorPis(0D);
		nfe.setValorCofins(0D);
		for (FaturaTributacao faturaTributacao : listaFaturaTributacao) {
			if (faturaTributacao.getTributo().getChavePrimaria() == idTributoICMS) {
				nfe.setBaseCalculoIcms(faturaTributacao.getValorBaseCalculo().doubleValue());
				nfe.setValorIcms(faturaTributacao.getValorImposto().doubleValue());

				if (faturaTributacao.getValorBaseSubstituicao() != null) {
					nfe.setBaseCalculoIcmsSubst(faturaTributacao.getValorBaseSubstituicao().doubleValue());
					nfe.setValorProduto(faturaTributacao.getValorBaseCalculo().doubleValue());
				} else {
					nfe.setBaseCalculoIcmsSubst((double) 0);
				}

				if (faturaTributacao.getValorSubstituicao() != null) {
					nfe.setValorIcmsSubst(faturaTributacao.getValorSubstituicao().doubleValue());
					nfe.setValorProduto(faturaTributacao.getValorBaseCalculo().doubleValue());
				} else {
					nfe.setValorIcmsSubst((double) 0);
				}

			} else if (faturaTributacao.getTributo().getChavePrimaria() == idTributoPIS) {
				nfe.setValorPis(faturaTributacao.getValorImposto().doubleValue());
			} else if (faturaTributacao.getTributo().getChavePrimaria() == idTributoCOFINS) {
				nfe.setValorCofins(faturaTributacao.getValorImposto().doubleValue());
			}
		}

		nfe.setDestino(documentoFiscal.getFatura().getCliente().getNome());
		nfe.setLogradouroDestino(clienteEndereco.getCep().getTipoLogradouro() + " " + clienteEndereco.getCep().getLogradouro());
		if (clienteEndereco.getNumero() != null) {
			nfe.setNumeroLogradouroDestino(clienteEndereco.getNumero());
		} else {
			nfe.setNumeroLogradouroDestino("S/N");
		}
		nfe.setComplementoDestino(clienteEndereco.getComplemento());
		nfe.setBairroDestino(clienteEndereco.getCep().getBairro());
		nfe.setMunicipioDestino(clienteEndereco.getCep().getNomeMunicipio());
		nfe.setUfDestino(clienteEndereco.getCep().getUf());
		nfe.setCepDestino(clienteEndereco.getCep().getCep().replace("-", ""));
		if (documentoFiscal.getFatura().getCliente().getCnpj() != null) {
			nfe.setCnpjCpfDestino(documentoFiscal.getFatura().getCliente().getCnpj());
		} else {
			nfe.setCnpjCpfDestino(documentoFiscal.getFatura().getCliente().getCpf());
		}
		ClienteFone clienteFonePrincipal = null;
		for (ClienteFone clienteFone : documentoFiscal.getFatura().getCliente().getFones()) {
			if (clienteFone.getIndicadorPrincipal()) {
				clienteFonePrincipal = clienteFone;
				break;
			}
		}
		if (clienteFonePrincipal != null) {
			nfe.setTelefoneDestino(clienteFonePrincipal.getCodigoDDD().toString() + clienteFonePrincipal.getNumero().toString());
		}
		nfe.setInscricaoEstadualDestino(documentoFiscal.getFatura().getCliente().getInscricaoEstadual());

		Long codigoRegimeEmitente =
						Long.parseLong((String) controladorParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_REGIME_TRIBUTARIO));
		EntidadeConteudo regimeTribEmitente = controladorEntidadeConteudo.obterEntidadeConteudo(codigoRegimeEmitente);
		nfe.setCodRegimeTribEmitente(Integer.parseInt(regimeTribEmitente.getCodigo()));
		nfe.setVersao(1);
		nfe.setHabilitado(true);
		Long indAmbienteSistema =
						Long.parseLong((String) controladorParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_CODIGO_INDICADOR_AMBIENTE_SISTEMA_NFE));
		EntidadeConteudo ambienteSistema = controladorEntidadeConteudo.obterEntidadeConteudo(indAmbienteSistema);
		nfe.setIndAmbienteSistema(Integer.parseInt(ambienteSistema.getCodigo()));

		if (ambienteSistema.getCodigo().equals(Constantes.HOMOLOGACAO)) {
			nfe.setDestino("NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL");
		}

		nfe.setVersaoXml("3.10");

		nfe.setDadosAuditoria(dadosAuditoria);

	}

	/**
	 * Popular nfe item.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param nfe
	 *            the nfe
	 * @throws GGASException
	 *             the GGAS exception
	 */
	public void popularNfeItem(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria, Nfe nfe) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorEntidadeConteudo controladorEntidadeConteudo =
						(ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
										ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);
		ControladorTributo controladorTributo =
						(ControladorTributo) ServiceLocator.getInstancia().getBeanPorID(ControladorTributo.BEAN_ID_CONTROLADOR_TRIBUTO);

		Long codigoTipoOperacaoEntrada =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_ENTRADA));
		Long codigoTipoOperacaoSaida =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_SAIDA));

		Long idTributoICMS = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ICMS));
		Long idTributoPIS = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_PIS));
		Long idTributoCOFINS = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_COFINS));

		TributoAliquota tributoAliquotaIcms =
						controladorTributo.obterAliquotaVigentePorData(idTributoICMS, documentoFiscal.getFatura().getDataEmissao());
		TributoAliquota tributoAliquotaPis =
						controladorTributo.obterAliquotaVigentePorData(idTributoPIS, documentoFiscal.getFatura().getDataEmissao());
		TributoAliquota tributoAliquotaCofins =
						controladorTributo.obterAliquotaVigentePorData(idTributoCOFINS, documentoFiscal.getFatura().getDataEmissao());

		calcularBaseCalculoPisCofins(idTributoCOFINS, documentoFiscal.getFatura());
		calcularBaseCalculoPisCofins(idTributoPIS, documentoFiscal.getFatura());

		// codigo situaçlão tributaria PIS e
		// COFINS (CST)
		Long codigoAliquotaNormalPisCofins =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_ALIQUOTA_NORMAL_PIS_COFINS));
		Long codigoAliquotaDiferenciadaPisCofins =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_ALIQUOTA_DIFERENCIADA_PIS_COFINS));
		Long codigoAliquotaIsentoPisCofins =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_ALIQUOTA_ISENTO_PIS_COFINS));
		Long codigoAliquotaSemTributacaoPisCofins =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_ALIQUOTA_SEM_TRIBUTACAO_PIS_COFINS));

		EntidadeConteudo aliquotaNormalPisCofins = controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaNormalPisCofins);
		EntidadeConteudo aliquotaDiferenciadaPisCofins =
						controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaDiferenciadaPisCofins);
		EntidadeConteudo aliquotaIsentoPisCofins = controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaIsentoPisCofins);
		EntidadeConteudo aliquotaSemTributacaoPisCofins =
						controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaSemTributacaoPisCofins);

		// codigo situaçlão tributaria ICMS (CST)
		Long codigoAliquotaTributadaIntegralmenteIcms =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_ALIQUOTA_TRIBUTADA_INTEGRALMENTE_ICMS));
		Long codigoAliquotaTributadaCobrancaIcmsPorSubstTributaria =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_ALIQUOTA_TRIBUTADA_COBRANCA_ICMS_POR_SUBST_TRIBUTARIA));
		Long codigoAliquotaComReducaoBaseCalculoIcms =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_ALIQUOTA_COM_REDUCAO_BASE_CALCULO_ICMS));
		Long codigoAliquotaIsentaIcms =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_ALIQUOTA_ISENTA_ICMS));
		Long codigoAliquotaNaoTributadaIcms =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_ALIQUOTA_NAO_TRIBUTADA_ICMS));

		Long tipoDebito = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_DEBITO));

		EntidadeConteudo aliquotaTributadaIntegralmenteIcms =
						controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaTributadaIntegralmenteIcms);
		EntidadeConteudo aliquotaTributadaCobrancaIcmsPorSubstTributaria =
						controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaTributadaCobrancaIcmsPorSubstTributaria);
		EntidadeConteudo aliquotaComReducaoBaseCalculoIcms =
						controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaComReducaoBaseCalculoIcms);
		EntidadeConteudo aliquotaIsentaIcms = controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaIsentaIcms);
		EntidadeConteudo aliquotaNaoTributadaIcms = controladorEntidadeConteudo.obterEntidadeConteudo(codigoAliquotaNaoTributadaIcms);

		// obtendo o ponto de consumo e pegando o
		// ponto de consumo da fatura agrupada
		// caso precise
		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		PontoConsumo pontoConsumo = documentoFiscal.getFatura().getPontoConsumo();
		if (pontoConsumo == null) {
			Collection<Fatura> listaFaturasFilhas =
							controladorFatura.consultarFaturaPorFaturaAgrupamento(documentoFiscal.getFatura().getChavePrimaria());
			pontoConsumo = listaFaturasFilhas.iterator().next().getPontoConsumo();
		}

		for (FaturaItem faturaItem : documentoFiscal.getFatura().getListaFaturaItem()) {
			
			if (faturaItem.getRubrica() != null && faturaItem.getRubrica().getIndicadorComposicaoNotaFiscal() != null
						&& faturaItem.getRubrica().getIndicadorComposicaoNotaFiscal()
						&& faturaItem.getRubrica().getLancamentoItemContabil().getTipoCreditoDebito().getChavePrimaria() == tipoDebito) {

				Collection<FaturaItemTributacao> listaFaturaItemTributacao = controladorFatura.listarFaturaItemTributacao(faturaItem);
				NfeItem nfeItem = (NfeItem) criarNfeItem();

				nfeItem.setNfe(nfe);
				nfeItem.setNumeroItem(faturaItem.getNumeroSequencial());
				if (faturaItem.getCreditoDebitoARealizar() != null) {
					nfeItem.setCodigoProduto(String.valueOf(faturaItem.getCreditoDebitoARealizar().getChavePrimaria()));
				} else {
					nfeItem.setCodigoProduto(String.valueOf(faturaItem.getRubrica().getChavePrimaria()));
				}

				nfeItem.setDescricaoProduto(faturaItem.getRubrica().getDescricaoImpressao());

				nfeItem.setCodigoGenero(Integer.parseInt(String.valueOf(
								faturaItem.getRubrica().getNomeclaturaComumMercosul().getNcm().toString()).substring(0, 2)));
				nfeItem.setCodigoNCM(faturaItem.getRubrica().getNomeclaturaComumMercosul().getNcm().toString());
				nfeItem.setCodigoCEST(faturaItem.getRubrica().getCodigoCEST());

				NaturezaOperacaoCFOP naturezaOperacaoCFOP = null;

				if (documentoFiscal.getTipoOperacao().getChavePrimaria() == codigoTipoOperacaoEntrada) {
					naturezaOperacaoCFOP =
									controladorFatura.obterNaturezaOperacaoCFOP(codigoTipoOperacaoEntrada, pontoConsumo.getRamoAtividade(),
													documentoFiscal.getFatura().getSegmento(), faturaItem.getRubrica());
				} else {
					naturezaOperacaoCFOP =
									controladorFatura.obterNaturezaOperacaoCFOP(codigoTipoOperacaoSaida, pontoConsumo.getRamoAtividade(),
													documentoFiscal.getFatura().getSegmento(), faturaItem.getRubrica());
				}
				if (naturezaOperacaoCFOP != null) {
					nfeItem.setCfop(naturezaOperacaoCFOP.getCodigoCFOP());
				}				

				/**
				 * Ao gerar a NFe e enviar para SEFAZ o Valor do Produto difere do produto Valor Unitário de Comercialização e Quantidade
				 * Comercial.
				 * Portanto, a Quantidade Comercial é recalculada no momento de criação da NFe.
				 */

				BigDecimal valorTotal = faturaItem.getValorTotal();
				BigDecimal valorUnitario = faturaItem.getValorUnitario();
				BigDecimal novaQuantidadeConsumida = valorTotal.divide(valorUnitario, 4, RoundingMode.HALF_UP);
				faturaItem.setQuantidade(novaQuantidadeConsumida);

				nfeItem.setDescricaoUnidadeMedidaTributavel(faturaItem.getRubrica().getUnidade().getDescricaoAbreviada().trim());
				nfeItem.setDescricaoUnidadeMedidaComercializacao(faturaItem.getRubrica().getUnidade().getDescricaoAbreviada().trim());
				nfeItem.setQuantidadeTributavel(faturaItem.getQuantidade().doubleValue());
				nfeItem.setQuantidadeComercializacao(faturaItem.getQuantidade().doubleValue());
				nfeItem.setValorTotalBruto(faturaItem.getValorTotal().doubleValue());
				nfeItem.setIndOrigemICMS(0);

				Boolean temICMS = false;
				Boolean temICMSSubs = false;
				Boolean temPIS = false;
				Boolean temCOFINS = false;

				Boolean icmsIsento = false;
				Boolean pisIsento = false;
				Boolean cofinsIsento = false;

				Boolean cofinsDiferenciado = false;
				Boolean pisDiferenciado = false;
				Boolean icmsDiferenciado = false;

				BigDecimal icmsDiferenciadoPercent = BigDecimal.ZERO;
				BigDecimal cofinsDiferenciadoPercent = BigDecimal.ZERO;
				BigDecimal pisDiferenciadoPercent = BigDecimal.ZERO;

				for (PontoConsumoTributoAliquota pcta : pontoConsumo.getListaPontoConsumoTributoAliquota()) {

					if (documentoFiscal.getFatura().getDataEmissao().compareTo(pcta.getDataInicioVigencia()) >= 0
									&& documentoFiscal.getFatura().getDataEmissao().compareTo(pcta.getDataFimVigencia()) <= 0) {

						if (pcta.getTributo().getChavePrimaria() == idTributoICMS) {
							icmsIsento = pcta.getIndicadorIsencao();
							if (!icmsIsento) {
								icmsDiferenciado = true;
								icmsDiferenciadoPercent = pcta.getPorcentagemAliquota();
							}
						} else if (pcta.getTributo().getChavePrimaria() == idTributoPIS) {
							pisIsento = pcta.getIndicadorIsencao();
							if (!pisIsento) {
								pisDiferenciado = true;
								pisDiferenciadoPercent = pcta.getPorcentagemAliquota();
							}
						} else if (pcta.getTributo().getChavePrimaria() == idTributoCOFINS) {
							cofinsIsento = pcta.getIndicadorIsencao();
							if (!cofinsIsento) {
								cofinsDiferenciado = true;
								cofinsDiferenciadoPercent = pcta.getPorcentagemAliquota();
							}
						}

					}
				}

				Double valorPIS = (double) 0;
				Double valorCOFINS = (double) 0;
				Double valorICMS = (double) 0;
				Double valorSubstituicaoICMS = (double) 0;
				Double baseCalculoSubstituicaoICMS = (double) 0;
				for (FaturaItemTributacao faturaItemTributacao : listaFaturaItemTributacao) {
					if (faturaItemTributacao.getTributo().getChavePrimaria() == idTributoICMS) {
						temICMS = true;
						if (faturaItemTributacao.getValorImposto() != null) {
							valorICMS = faturaItemTributacao.getValorImposto().doubleValue();
						}
						if (faturaItemTributacao.getValorSubstituicao() != null) {
							temICMSSubs = true;
							baseCalculoSubstituicaoICMS = faturaItemTributacao.getValorBaseSubstituicao().doubleValue();
							valorSubstituicaoICMS = faturaItemTributacao.getValorSubstituicao().doubleValue();
						}
					} else if (faturaItemTributacao.getTributo().getChavePrimaria() == idTributoPIS) {
						temPIS = true;
						if (faturaItemTributacao.getValorImposto() != null) {
							valorPIS = faturaItemTributacao.getValorImposto().doubleValue();
						}
					} else if (faturaItemTributacao.getTributo().getChavePrimaria() == idTributoCOFINS) {
						temCOFINS = true;
						if (faturaItemTributacao.getValorImposto() != null) {
							valorCOFINS = faturaItemTributacao.getValorImposto().doubleValue();
						}
					}
				}

				if (temICMS && !icmsIsento) {
					nfeItem.setValorBaseCalculoICMS(faturaItem.getValorTotal().doubleValue());
					if (temICMSSubs) {
						nfeItem.setBaseCalculoICMSSubstituto(baseCalculoSubstituicaoICMS);
					}
				}

				if (temPIS && !pisIsento) {
					nfeItem.setBaseCalculoPIS(faturaItem.getValorTotal().doubleValue());
					nfeItem.setQuantidadeVendPIS(faturaItem.getQuantidade().doubleValue());
				}

				if (temCOFINS && !cofinsIsento) {
					nfeItem.setBaseCalculoCOFINS(faturaItem.getValorTotal().doubleValue());
					nfeItem.setQuantidadeVendCOFINS(faturaItem.getQuantidade().doubleValue());
				}

				nfeItem.setValorUnitario(faturaItem.getValorUnitario().doubleValue());

				if (!icmsIsento || !pisIsento || !cofinsIsento) {
					nfeItem.setValorUnitarioTributavel(faturaItem.getValorUnitario().doubleValue());
				}

				nfeItem.setValorOutros(0D);
				nfeItem.setIndicadorTotalizacao(1);
				nfeItem.setDadosAuditoria(dadosAuditoria);
				nfeItem.setUltimaAlteracao(new Date(System.currentTimeMillis()));

				// setar codigo da situação tributaria
				// do PIS (CST)
				if (temPIS && !pisIsento && !pisDiferenciado) {
					nfeItem.setIndSituacaoTribPIS(aliquotaNormalPisCofins.getCodigo());
					nfeItem.setValorPIS(valorPIS);
					nfeItem.setPercentualAliquotaPIS(tributoAliquotaPis.getValorAliquota().doubleValue() * CEM);
				} else if (temPIS && pisDiferenciado) {
					nfeItem.setIndSituacaoTribPIS(aliquotaDiferenciadaPisCofins.getCodigo());
					nfeItem.setValorPIS(valorPIS);
					nfeItem.setPercentualAliquotaPIS(pisDiferenciadoPercent.doubleValue() * CEM);
				} else if (temPIS && pisIsento) {
					nfeItem.setIndSituacaoTribPIS(aliquotaIsentoPisCofins.getCodigo());
				} else {
					nfeItem.setIndSituacaoTribPIS(aliquotaSemTributacaoPisCofins.getCodigo());
				}

				// setar codigo da situação tributaria
				// do COFINS (CST)
				if (temCOFINS && !cofinsIsento && !cofinsDiferenciado) {
					nfeItem.setIndSituacaoTribCOFINS(aliquotaNormalPisCofins.getCodigo());
					nfeItem.setValorCOFINS(valorCOFINS);
					nfeItem.setPercentualAliquotaCOFINS(tributoAliquotaCofins.getValorAliquota().doubleValue() * CEM);
				} else if (temCOFINS && cofinsDiferenciado) {
					nfeItem.setIndSituacaoTribCOFINS(aliquotaDiferenciadaPisCofins.getCodigo());
					nfeItem.setValorCOFINS(valorCOFINS);
					nfeItem.setPercentualAliquotaCOFINS(cofinsDiferenciadoPercent.doubleValue() * CEM);
				} else if (temCOFINS && cofinsIsento) {
					nfeItem.setIndSituacaoTribCOFINS(aliquotaIsentoPisCofins.getCodigo());
				} else {
					nfeItem.setIndSituacaoTribCOFINS(aliquotaSemTributacaoPisCofins.getCodigo());
				}

				boolean temIcmsSubst = false;
				if (nfe.getBaseCalculoIcmsSubst() != 0) {
					temIcmsSubst = true;
					ControladorRamoAtividadeSubstituicaoTributaria controladorRamoAtividadeSubstituicaoTributaria =
									(ControladorRamoAtividadeSubstituicaoTributaria) ServiceLocator
													.getInstancia()
													.getBeanPorID(ControladorRamoAtividadeSubstituicaoTributaria.BEAN_ID_CONTROLADOR_RAMO_ATIVIDADE_SUBS_TRIB);
					Map<String, Object> filtro = new HashMap<String, Object>();
					filtro.put("ramoAtividade", pontoConsumo.getRamoAtividade());
					filtro.put("dataBase", documentoFiscal.getDataEmissao());
					Collection<RamoAtividadeSubstituicaoTributaria> listaRamoAtividadeSubstituicaoTributaria =
									controladorRamoAtividadeSubstituicaoTributaria.consultarRamoAtividadeSubstituicaoTributaria(filtro);
					if (listaRamoAtividadeSubstituicaoTributaria != null && !listaRamoAtividadeSubstituicaoTributaria.isEmpty()) {
						RamoAtividadeSubstituicaoTributaria ramoAtividadeSubstituicaoTributaria =
										listaRamoAtividadeSubstituicaoTributaria.iterator().next();
						if (ramoAtividadeSubstituicaoTributaria.getPercentualSubstituto() != null) {
							nfeItem.setPercentualMargemICMS(ramoAtividadeSubstituicaoTributaria.getPercentualSubstituto().doubleValue());
						}
					}

				}
				// setar codigo da situação tributaria
				// do ICMS (CST)
				if (temICMS && !temIcmsSubst && !icmsIsento && !icmsDiferenciado) {
					nfeItem.setIndTribICMS(aliquotaTributadaIntegralmenteIcms.getCodigo());
					nfeItem.setValorICMS(valorICMS);
					nfeItem.setValorAliquotaICMS(tributoAliquotaIcms.getValorAliquota().doubleValue() * CEM);
				} else if (temICMS && temIcmsSubst && !icmsIsento && !icmsDiferenciado) {
					nfeItem.setIndTribICMS(aliquotaTributadaCobrancaIcmsPorSubstTributaria.getCodigo());
					nfeItem.setValorICMS(valorICMS);
					nfeItem.setValorAliquotaICMS(tributoAliquotaIcms.getValorAliquota().doubleValue() * CEM);
					nfeItem.setValorICMSSituacao(valorSubstituicaoICMS);
					nfeItem.setValorAliquotaSituacaoTribICMS(tributoAliquotaIcms.getValorAliquota().doubleValue() * CEM);
					nfeItem.setIndModBaseCalculoICMS(3);
					nfeItem.setIndModBaseCalculoICMSSubstituto(5);
				} else if (temICMS && icmsDiferenciado) {
					nfeItem.setIndTribICMS(aliquotaComReducaoBaseCalculoIcms.getCodigo());
					nfeItem.setValorICMS(valorICMS);
					nfeItem.setValorAliquotaICMS(icmsDiferenciadoPercent.doubleValue() * CEM);
				} else if (temICMS && icmsIsento) {
					nfeItem.setIndTribICMS(aliquotaIsentaIcms.getCodigo());
				} else {
					nfeItem.setIndTribICMS(aliquotaNaoTributadaIcms.getCodigo());
				}

				nfeItem.setVersao(1);
				nfeItem.setHabilitado(true);

				nfe.getListaNfeItem().add(nfeItem);
			}
		}
	}

	/**
	 * Calcular base calculo pis cofins.
	 * 
	 * @param idTributo
	 *            the id tributo
	 * @param fatura
	 *            the fatura
	 * @return the double
	 */
	private Double calcularBaseCalculoPisCofins(Long idTributo, Fatura fatura) {

		Double valorBaseDeCalculo = 0D;

		for (FaturaItem faturaItem : fatura.getListaFaturaItem()) {

			Boolean temTributo = false;
			Boolean tributoIsento = false;

			ControladorFatura controladorFatura =
							(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
			PontoConsumo pontoConsumo = fatura.getPontoConsumo();
			if (pontoConsumo == null) {
				Collection<Fatura> listaFaturasFilhas = controladorFatura.consultarFaturaPorFaturaAgrupamento(fatura.getChavePrimaria());
				pontoConsumo = listaFaturasFilhas.iterator().next().getPontoConsumo();
			}

			for (PontoConsumoTributoAliquota pcta : pontoConsumo.getListaPontoConsumoTributoAliquota()) {

				if (fatura.getDataEmissao().compareTo(pcta.getDataInicioVigencia()) >= 0
								&& fatura.getDataEmissao().compareTo(pcta.getDataFimVigencia()) <= 0
								&& pcta.getTributo().getChavePrimaria() == idTributo) {

					tributoIsento = pcta.getIndicadorIsencao();
				}
			}

			for (RubricaTributo rubricaTributo : faturaItem.getRubrica().getTributos()) {
				if (rubricaTributo.getTributo().getChavePrimaria() == idTributo) {
					temTributo = true;
				}
			}

			if (temTributo && !tributoIsento) {
				valorBaseDeCalculo = valorBaseDeCalculo + faturaItem.getValorTotal().doubleValue();
			}
		}
		return valorBaseDeCalculo;
	}

	/**
	 * Obter nfe empresa.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the nfe empresa
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public NfeEmpresa obterNfeEmpresa(Long chavePrimaria) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeNfeEmpresa().getSimpleName());
		hql.append(" nfeEmpresa ");
		hql.append(" where nfeEmpresa.chavePrimaria = :chavePrimaria ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setLong(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA, chavePrimaria);

		return (NfeEmpresa) query.uniqueResult();
	}

	/**
	 * Obter nfe usuario.
	 * 
	 * @param chavePrimaria
	 *            the chave primaria
	 * @return the nfe usuario
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@Override
	public NfeUsuario obterNfeUsuario(String login) throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidadeNfeUsuario().getSimpleName());
		hql.append(" nfeUsuario ");
		hql.append(" where nfeUsuario.login = :login ");

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("login", login);

		return (NfeUsuario) query.uniqueResult();
	}

	/**
	 * @param Long numeroDocFiscal
	 * @param String numeroSerie
	 * @param Date dataEmissao
	 * */
	@Override
	public Nfe consultarDadosNotaFiscalEletronica(Long numeroDocFiscal, String numeroSerie, Date dataEmissao) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" nfe ");
		hql.append(" where ");
		hql.append(" nfe.documentoFiscal = :documentoFiscal ");
		hql.append(" and nfe.serie = :serie ");
		hql.append(" and nfe.emissao between (:emissaoInicial) and (:emissaoFinal) ");

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setInteger("documentoFiscal", Integer.parseInt(numeroDocFiscal.toString()));
		query.setInteger("serie", Integer.parseInt(numeroSerie));
		Util.adicionarRestricaoDataSemHora(query, dataEmissao, "emissaoInicial", Boolean.TRUE);
		Util.adicionarRestricaoDataSemHora(query, dataEmissao, "emissaoFinal", Boolean.FALSE);

		return (Nfe) query.uniqueResult();

	}
	/** Consultar Dados Nota Fiscal Eletronica
	 * @param Long numeroDocFiscal - {@link Long}
	 * @param String numeroSerie - {@link String}
	 * @return the nfe
	 * */
	public Nfe consultarDadosNotaFiscalEletronica(Long numeroDocFiscal, String numeroSerie) {

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName()).append(" nfe ");
		hql.append(" where ");
		hql.append(" nfe.documentoFiscal = :documentoFiscal ");
		hql.append(" and nfe.serie = :serie ");		

		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setInteger("documentoFiscal", Integer.parseInt(numeroDocFiscal.toString()));
		query.setInteger("serie", Integer.parseInt(numeroSerie));		

		return (Nfe) query.uniqueResult();

	}
	
	/**
	 * Popular dados nfe cancelamento.
	 * 
	 * @param nfe
	 *            the nfe
	 * @param fatura
	 *            the fatura
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the nfe
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public Nfe popularDadosNfeCancelamento(Nfe nfe, Fatura fatura, DadosAuditoria dadosAuditoria) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorEntidadeConteudo controladorEntidadeConteudo =
						(ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
										ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		Long codigoStatusNfeNaoProcessada =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_NAO_PROCESSADA));
		EntidadeConteudo statusNfeNaoProcessada = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeNaoProcessada);
		nfe.setSisStatus(statusNfeNaoProcessada.getDescricaoAbreviada());

		nfe.setDataCancelamento(Calendar.getInstance().getTime());
		nfe.setIndProcessoNotaFiscal(PROCESSO_CANCELAMENTO);
		if (fatura.getMotivoCancelamento().getDescricao().length() >= 15) {
			nfe.setJustificativaCancelamento(fatura.getMotivoCancelamento().getDescricao());
		} else {
			nfe.setJustificativaCancelamento(fatura.getMotivoCancelamento().getDescricao() + COMPLETAR_JUSTIFICATIVA_CANCELAMENTO);
		}

		nfe.setSisOperacao("C");

		return nfe;

	}

	/**
	 * 
	 * 
	 * @param DocumentoFiscal documentoFiscal
	 * @param DadosAuditoria dadosAuditoria
	 * @return the nfe
	 * @throws GGASException
	 * 
	 */
	@Override
	public Nfe inserirNfeCancelamento(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria) throws GGASException {

		Nfe nfe = (Nfe) this.criar();
		popularNfe(documentoFiscal, dadosAuditoria, nfe);
		this.popularDadosNfeCancelamento(nfe, documentoFiscal.getFatura(), dadosAuditoria);
		this.popularNfeItem(documentoFiscal, dadosAuditoria, nfe);
		nfe.setSisOperacao("E");
		super.inserir(nfe);

		return nfe;

	}

	/**
	 * @param DocumentoFiscal documentoFiscal 
	 * @param Nfe nfe 
	 * @param DadosAuditoria dadosAuditoria
	 * @throws NegocioException
	 */
	@Override
	public void inserirNfeChaveReferenciadaCancelamento(DocumentoFiscal documentoFiscal, Nfe nfe, DadosAuditoria dadosAuditoria)
					throws NegocioException {

		NfeChaveReferenciada nfeChaveReferenciada = (NfeChaveReferenciada) criarNfeChaveReferenciada();

		nfeChaveReferenciada.setChaveAcesso(this.obterChaveAcessoDocumentoFiscal(documentoFiscal));
		nfeChaveReferenciada.setNfe(nfe);
		nfeChaveReferenciada.setUltimaAlteracao(Calendar.getInstance().getTime());
		nfeChaveReferenciada.setDadosAuditoria(dadosAuditoria);

		this.inserir(nfeChaveReferenciada);
	}

	/**
	 * Obter chave acesso documento fiscal.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @return the string
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private String obterChaveAcessoDocumentoFiscal(DocumentoFiscal documentoFiscal) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		String codTipoOperacaoSaida = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_SAIDA);
		String chaveAcesso = null;

		Criteria criteria = this.createCriteria(DocumentoFiscal.class);
		criteria.createAlias("fatura", "fatura", Criteria.INNER_JOIN);
		criteria.add(Restrictions.eq("tipoOperacao.chavePrimaria", Long.valueOf(codTipoOperacaoSaida)));
		criteria.add(Restrictions.eq("fatura.chavePrimaria", documentoFiscal.getFatura().getChavePrimaria()));

		DocumentoFiscal doc = (DocumentoFiscal) criteria.uniqueResult();

		if (doc != null) {

			chaveAcesso = doc.getChaveAcesso();

		}

		return chaveAcesso;

	}

	/**
	 * 
	 * Método responsável por emitir as notas fiscais de saída.
	 * 
	 * @param StringBuilder  logProcessamento
	 * @param DadosAuditoria dadosAuditoria
	 * @throws GGASException
	 */
	@Override
	public void emitirNfeSaida(StringBuilder logProcessamento, DadosAuditoria dadosAuditoria) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorEntidadeConteudo controladorEntidadeConteudo =
						(ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
										ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		Long codigoTipoOperacaoSaida =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_SAIDA));
		Long codigoStatusNfeAguardandoEnvio =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AGUARDANDO_ENVIO));
		Long codigoStatusNfeAguardandoRetorno =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AGUARDANDO_RETORNO));
		Long codigoStatusNfeAutorizada =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AUTORIZADA));

		EntidadeConteudo statusNfeAutorizada = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeAutorizada);
		EntidadeConteudo statusNfeAguardandoRetorno = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeAguardandoRetorno);

		logProcessamento.append("GGAS - Log de Processo \nMódulo: Faturamento - Processo: Emitir Nfe \nData da execução: ");
		logProcessamento.append(DataUtil.converterDataParaString(Calendar.getInstance().getTime(), true));
		logProcessamento.append("\n-----------");
		logProcessamento.append(" Documentos Fiscais com status nfe aguardando envio ");
		logProcessamento.append("-----------\n");
		
		Collection<DocumentoFiscal> listaDocumentoFiscal =
				controladorFatura.consultarDocumentoFiscalPorTipoOperacaoStatusNfe(codigoTipoOperacaoSaida, codigoStatusNfeAguardandoEnvio);
		
		logProcessamento.append("\nTotal documentos fiscais aguardando envios da nfe a serem inseridos: ").append(listaDocumentoFiscal.size());
		ControladorParametroSistema controladorParametroSistema = ServiceLocator.getInstancia().getControladorParametroSistema();
		Boolean parametroValorZero =
						Boolean.valueOf((String) controladorParametroSistema
										.obterValorDoParametroPorCodigo(Constantes.PARAMETRO_FATURA_VALOR_ZERO));

		if (!parametroValorZero) {
			listaDocumentoFiscal = removerDocumentosValorZero(listaDocumentoFiscal);
		}

		for (DocumentoFiscal documentoFiscal : listaDocumentoFiscal) {
			logProcessamento.append("\n" + documentoFiscal.getChavePrimaria()).append(" - ").append(documentoFiscal.getNumero())
					.append(" - ").append(documentoFiscal.getDescricaoPontoConsumo());
			// Pesquisar linhas e atualizar
			// informações.
			Nfe nfe =
							this.consultarDadosNotaFiscalEletronica(documentoFiscal.getNumero(), documentoFiscal.getSerie().getNumero(),
											documentoFiscal.getDataEmissao());
			// Se tiver chave de acesso
			// preenchida, obter NFE e atualizar
			// os dados com informacoes
			// do cancelamento.
			if (!StringUtils.isEmpty(documentoFiscal.getChaveAcesso()) && nfe != null
							&& nfe.getSisStatus().equals(statusNfeAutorizada.getDescricaoAbreviada())) {

				nfe = this.popularDadosNfeCancelamento(nfe, documentoFiscal.getFatura(), dadosAuditoria);
				this.atualizar(nfe);

			} else {
				// Nunca deve ser nulo.
				if (nfe != null) {
					popularNfe(documentoFiscal, dadosAuditoria, nfe);
					nfe.setCodigoLote(null);
					nfe.getListaNfeItem().removeAll(nfe.getListaNfeItem());
					popularNfeItem(documentoFiscal, dadosAuditoria, nfe);
					super.atualizar(nfe);
				} else {
					nfe = (Nfe) this.criar();
					popularNfe(documentoFiscal, dadosAuditoria, nfe);
					popularNfeItem(documentoFiscal, dadosAuditoria, nfe);
					super.inserir(nfe);
				}
			}
			documentoFiscal.setStatusNfe(statusNfeAguardandoRetorno);
			documentoFiscal.setDadosAuditoria(dadosAuditoria);
			controladorFatura.atualizar(documentoFiscal, getClasseEntidadeDocumentoFiscal());

			ControladorParametroSistema controladorParametro = (ControladorParametroSistema) ServiceLocator.getInstancia()
					.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

			ParametroSistema referenciaIntegracaoTituloNota =
					controladorParametro.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO);

			ConstanteSistema referenciaIntegracaoContrato =
					controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO);

			ConstanteSistema situacaoProcessado =
					controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);

			if (referenciaIntegracaoTituloNota.getValor().equals(referenciaIntegracaoContrato.getValor())) {
				IntegracaoContrato contratoIntegrado = controladorIntegracao.obterIntegracaoContrato(
						documentoFiscal.getFatura().getContrato(), documentoFiscal.getFatura().getPontoConsumo());
				if (contratoIntegrado == null || !situacaoProcessado.getValor().equals(contratoIntegrado.getSituacao())) {
					logProcessamento.append("\nNão foi possível continuar o processo para esse cliente devido ao erro: ");
					logProcessamento.append(Constantes.ERRO_CONTRATO_INTEGRACAO_INEXISTENTE);
					throw new NegocioException(Constantes.ERRO_CONTRATO_INTEGRACAO_INEXISTENTE, true);
				}
			} else {
				IntegracaoCliente clienteIntegrado = controladorIntegracao.buscarIntegracaoCliente(
						documentoFiscal.getFatura().getCliente());
				if (clienteIntegrado == null) {
					logProcessamento.append("\nNão foi possível continuar o processo para esse cliente devido ao erro: ");
					logProcessamento.append(Constantes.ERRO_CONTRATO_INTEGRACAO_INEXISTENTE);
					throw new NegocioException(Constantes.ERRO_CLIENTE_INTEGRACAO_INEXISTENTE, true);
				}
			}
			controladorIntegracao.inserirIntegracaoNotaFiscal(documentoFiscal, Boolean.TRUE, dadosAuditoria, null);
			logProcessamento.append("\nDados para integração inserida com sucesso!");
		}

	}

	/**
	 * Remover documentos valor zero.
	 * 
	 * @param listaDocumentoFiscal
	 *            the lista documento fiscal
	 * @return the collection
	 */
	private Collection<DocumentoFiscal> removerDocumentosValorZero(Collection<DocumentoFiscal> listaDocumentoFiscal) {

		Collection<DocumentoFiscal> listaDocumentoFiscalFinal = new ArrayList<DocumentoFiscal>();
		for (DocumentoFiscal documento : listaDocumentoFiscal) {
			if (documento.getValorTotal().compareTo(BigDecimal.valueOf(0)) >= 0) {
				listaDocumentoFiscalFinal.add(documento);
			}
		}
		return listaDocumentoFiscalFinal;

	}

	/**
	 * @param logProcessamento
	 * @param dadosAuditoria
	 * @throws GGASException
	 */
	@Override
	public void emitirNfeEntrada(StringBuilder logProcessamento, DadosAuditoria dadosAuditoria) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		Long codigoTipoOperacaoEntrada =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_ENTRADA));
		Long codigoStatusNfeAguardandoEnvio =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AGUARDANDO_ENVIO));

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Collection<DocumentoFiscal> listaDocumentoFiscalEntrada =
						controladorFatura.consultarDocumentoFiscalPorTipoOperacaoStatusNfe(codigoTipoOperacaoEntrada,
										codigoStatusNfeAguardandoEnvio);

		for (DocumentoFiscal documentoFiscal : listaDocumentoFiscalEntrada) {

			if (!this.isCorrigiuNotaFiscalEletronica(documentoFiscal, dadosAuditoria)) {

				Nfe nfe = this.inserirNfeCancelamento(documentoFiscal, dadosAuditoria);

				ControladorParametroSistema controladorParametro = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

				ParametroSistema referenciaIntegracaoTituloNota =
						controladorParametro.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO);

				ConstanteSistema referenciaIntegracaoContrato =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO);

				ConstanteSistema situacaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);

				if (referenciaIntegracaoTituloNota.getValor().equals(referenciaIntegracaoContrato.getValor())) {
					IntegracaoContrato contratoIntegrado = controladorIntegracao.obterIntegracaoContrato(
							documentoFiscal.getFatura().getContrato(), documentoFiscal.getFatura().getPontoConsumo());
					if (contratoIntegrado == null || !situacaoProcessado.getValor().equals(contratoIntegrado.getSituacao())) {
						throw new NegocioException(Constantes.ERRO_CONTRATO_INTEGRACAO_INEXISTENTE, true);
					}
				} else {
					IntegracaoCliente clienteIntegrado = controladorIntegracao.buscarIntegracaoCliente(
							documentoFiscal.getFatura().getCliente());
					if (clienteIntegrado == null) {
						throw new NegocioException(Constantes.ERRO_CLIENTE_INTEGRACAO_INEXISTENTE, true);
					}
				}

				controladorIntegracao.inserirIntegracaoNotaFiscal(documentoFiscal, Boolean.TRUE, dadosAuditoria, null);
				this.inserirNfeChaveReferenciadaCancelamento(documentoFiscal, nfe, dadosAuditoria);
				this.atualizarDocumentoFiscal(documentoFiscal, dadosAuditoria);

			}

		}

	}

	/**
	 * Checks if is corrigiu nota fiscal eletronica.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @return the boolean
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Boolean isCorrigiuNotaFiscalEletronica(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria) throws GGASException {

		Nfe nfe =
						this.consultarDadosNotaFiscalEletronica(documentoFiscal.getNumero(), documentoFiscal.getSerie().getNumero(),
										documentoFiscal.getDataEmissao());

		if (nfe != null) {

			// Atualiza os dados da NFE
			this.popularNfe(documentoFiscal, dadosAuditoria, nfe);
			this.popularNfeItem(documentoFiscal, dadosAuditoria, nfe);
			this.popularDadosNfeCancelamento(nfe, documentoFiscal.getFatura(), dadosAuditoria);
			this.atualizar(nfe);
			this.atualizarDocumentoFiscal(documentoFiscal, dadosAuditoria);

			return Boolean.TRUE;

		} else {

			return Boolean.FALSE;

		}

	}

	/**
	 * Atualizar documento fiscal.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void atualizarDocumentoFiscal(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorEntidadeConteudo controladorEntidadeConteudo =
						(ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
										ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		Long codigoStatusNfeAguardandoRetorno =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AGUARDANDO_RETORNO));
		EntidadeConteudo statusNfeAguardandoRetorno = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeAguardandoRetorno);

		documentoFiscal.setStatusNfe(statusNfeAguardandoRetorno);
		documentoFiscal.setDadosAuditoria(dadosAuditoria);

		controladorFatura.atualizarDocumentoFiscal(documentoFiscal);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.nfe.ControladorNfe#processarRetornoNfeEntrada(java.lang.StringBuilder, br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void processarRetornoNfeEntrada(StringBuilder logProcessamento, DadosAuditoria dadosAuditoria) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorNfe controladorNfe = (ControladorNfe) ServiceLocator.getInstancia().getBeanPorID(ControladorNfe.BEAN_ID_CONTROLADOR_NFE);

		Long codigoStatusNfeAguardandoRetorno =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AGUARDANDO_RETORNO));
		Long codigoTipoOperacaoEntrada =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_ENTRADA));

		// Consultar dados na tabela de
		// DOCUMENTO_FISCAL, situação: 'Aguardando
		// retorno'.
		Collection<DocumentoFiscal> listaDocumentoFiscal =
						controladorFatura.consultarDocumentoFiscalPorTipoOperacaoStatusNfe(codigoTipoOperacaoEntrada,
										codigoStatusNfeAguardandoRetorno);

		for (DocumentoFiscal documentoFiscal : listaDocumentoFiscal) {

			// Para os resultados encontrados,
			// procurar linha na tabela NFE, ver
			// se houve mudança de
			// status.
			Nfe nfe =
							controladorNfe.consultarDadosNotaFiscalEletronica(documentoFiscal.getNumero(), documentoFiscal.getSerie()
											.getNumero(), documentoFiscal.getDataEmissao());

			if (nfe != null && documentoFiscal.getStatusNfe() != null
							&& !documentoFiscal.getStatusNfe().getDescricaoAbreviada().equals(nfe.getSisStatus()) && !this.isErroNfe(nfe)) {

				this.tratarDadosNotaFiscalEletronica(nfe, documentoFiscal, dadosAuditoria);

				// Se houve mudança de status, de
				// acordo com o código realizar os
				// devidos
				// processamentos.
				// Integracao de NotaFiscal com o ERP
				ControladorIntegracao controladorIntegracao =
								(ControladorIntegracao) ServiceLocator.getInstancia().getControladorNegocio(
												ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);

				ControladorParametroSistema controladorParametro = (ControladorParametroSistema) ServiceLocator.getInstancia()
						.getControladorNegocio(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);

				ParametroSistema referenciaIntegracaoTituloNota =
						controladorParametro.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_INTEGRACAO_NOTA_TITULO);

				ConstanteSistema referenciaIntegracaoContrato =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_REFERENCIA_INTEGRACAO_NOTA_TITULO_CONTRATO);

				ConstanteSistema situacaoProcessado =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_INTEGRACAO_SITUACAO_PROCESSADO);

				if (referenciaIntegracaoTituloNota.getValor().equals(referenciaIntegracaoContrato.getValor())) {
					IntegracaoContrato contratoIntegrado = controladorIntegracao.obterIntegracaoContrato(
							documentoFiscal.getFatura().getContrato(), documentoFiscal.getFatura().getPontoConsumo());
					if (contratoIntegrado == null || !situacaoProcessado.getValor().equals(contratoIntegrado.getSituacao())) {
						throw new NegocioException(Constantes.ERRO_CONTRATO_INTEGRACAO_INEXISTENTE, true);
					}
				} else {
					IntegracaoCliente clienteIntegrado = controladorIntegracao.buscarIntegracaoCliente(
							documentoFiscal.getFatura().getCliente());
					if (clienteIntegrado == null) {
						throw new NegocioException(Constantes.ERRO_CLIENTE_INTEGRACAO_INEXISTENTE, true);
					}
				}

				controladorIntegracao.inserirIntegracaoNotaFiscal(documentoFiscal, Boolean.TRUE, dadosAuditoria, null);

			}

		}

	}

	/**
	 * Tratar dados nota fiscal eletronica.
	 * 
	 * @param nfe
	 *            the nfe
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws GGASException
	 *             the GGAS exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void tratarDadosNotaFiscalEletronica(Nfe nfe, DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria)
					throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorEntidadeConteudo controladorEntidadeConteudo =
						(ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
										ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		Long codigoTipoOperacaoEntrada =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_ENTRADA));
		Long codigoStatusRejeitada =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_REJEITADA));
		Long codigoStatusDenegada =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_DENEGADA));
		Long codigoStatusCancelada =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_CANCELADA));
		Long codigoStatusAutorizada =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AUTORIZADA));

		EntidadeConteudo statusRejeitada = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusRejeitada);
		EntidadeConteudo statusDenegada = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusDenegada);
		EntidadeConteudo statusCancelada = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusCancelada);
		EntidadeConteudo statusAutorizada = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusAutorizada);

		if (nfe.getSisStatus().equals(statusRejeitada.getDescricaoAbreviada())) {

			this.tratarDadosNfeStatusRejeitada(nfe, documentoFiscal, dadosAuditoria, statusRejeitada);

		} else if (nfe.getSisStatus().equals(statusDenegada.getDescricaoAbreviada())) {

			this.tratarDadosNfeStatusDenegada(nfe, documentoFiscal, dadosAuditoria, statusDenegada);

		} else if (nfe.getSisStatus().equals(statusCancelada.getDescricaoAbreviada())) {

			this.tratarDadosNfeEntradaStatusCancelada(documentoFiscal, dadosAuditoria, statusCancelada);
			this.tratarDadosRefaturamentoHistorico(documentoFiscal, dadosAuditoria);

		} else if (nfe.getSisStatus().equals(statusAutorizada.getDescricaoAbreviada())) {

			if (documentoFiscal.getTipoOperacao().getChavePrimaria() == codigoTipoOperacaoEntrada) {

				this.tratarDadosNfeEntradaStatusCancelada(documentoFiscal, dadosAuditoria, statusCancelada);
				this.tratarDadosRefaturamentoHistorico(documentoFiscal, dadosAuditoria);

			} else {

				this.tratarDadosNfeStatusAutorizada(documentoFiscal, dadosAuditoria, statusAutorizada, nfe);

			}

		}

	}

	/**
	 * Tratar dados refaturamento historico.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void tratarDadosRefaturamentoHistorico(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria) throws GGASException {

		RefaturamentoHistorico refaturamentoHistorico =
						this.obterRefaturamentoHistoricoAtual(documentoFiscal.getFatura().getChavePrimaria());

		if (refaturamentoHistorico != null) {

			ControladorFatura controladorFatura =
							(ControladorFatura) ServiceLocator.getInstancia().getControladorNegocio(
											ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
			DadosResumoFatura dadosResumoFatura = refaturamentoHistorico.getDadosResumoFatura();

			controladorFatura.processarInclusaoFatura(dadosResumoFatura, Boolean.TRUE, Boolean.FALSE, dadosAuditoria);

			refaturamentoHistorico.setIndicadorProcessoConcluido(Boolean.TRUE);
			refaturamentoHistorico.setDataConclusaoRefaturamento(new Date());

			this.atualizar(refaturamentoHistorico, this.getClasseEntidadeRefaturamentoHistorico());

		}

	}

	/**
	 * Tratar dados nfe status rejeitada.
	 * 
	 * @param nfe
	 *            the nfe
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param statusRejeitada
	 *            the status rejeitada
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void tratarDadosNfeStatusRejeitada(Nfe nfe, DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria,
					EntidadeConteudo statusRejeitada) throws NegocioException, ConcorrenciaException {

		documentoFiscal.setStatusNfe(statusRejeitada);
		documentoFiscal.setDadosAuditoria(dadosAuditoria);

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		controladorFatura.atualizarDocumentoFiscal(documentoFiscal);

		this.gerarAnomaliaFaturamento(nfe, documentoFiscal, dadosAuditoria);

	}

	/**
	 * Tratar dados nfe status denegada.
	 * 
	 * @param nfe
	 *            the nfe
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param statusDenegada
	 *            the status denegada
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void tratarDadosNfeStatusDenegada(Nfe nfe, DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria,
					EntidadeConteudo statusDenegada) throws NegocioException, ConcorrenciaException {

		documentoFiscal.setStatusNfe(statusDenegada);
		documentoFiscal.setDadosAuditoria(dadosAuditoria);

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		controladorFatura.atualizarDocumentoFiscal(documentoFiscal);

		ControladorCliente controladorCliente =
						(ControladorCliente) ServiceLocator.getInstancia().getBeanPorID(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE);
		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getBeanPorID(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ConstanteSistema constanteSistemaClienteSituacaoInaptoPorDenagacao =
						controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_CLIENTE_SITUACAO_INAPTO_POR_DENEGACAO);
		ClienteSituacao clienteSituacao =
						controladorCliente
										.obterClienteSituacao(Long.parseLong(constanteSistemaClienteSituacaoInaptoPorDenagacao.getValor()));
		Cliente cliente = documentoFiscal.getFatura().getCliente();
		cliente.setClienteSituacao(clienteSituacao);
		controladorCliente.atualizar(cliente);

		this.gerarAnomaliaFaturamento(nfe, documentoFiscal, dadosAuditoria);

	}

	/**
	 * 
	 * @param documentoFiscal
	 * @param dadosAuditoria
	 * @param statusCancelada
	 * @throws NegocioException, ConcorrenciaException  
	 * 
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void tratarDadosNfeEntradaStatusCancelada(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria,
					EntidadeConteudo statusCancelada) throws NegocioException, ConcorrenciaException {

		documentoFiscal.setStatusNfe(statusCancelada);
		documentoFiscal.setDadosAuditoria(dadosAuditoria);

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		controladorFatura.atualizarDocumentoFiscal(documentoFiscal);

		controladorFatura.cancelarFatura(documentoFiscal.getFatura(), documentoFiscal.getFatura().getMotivoCancelamento(), dadosAuditoria,
						false, Constantes.C_PROV_DEV_DUV_MOTIVO_BAIXA_CANCELAMENTO);

		// Integracao de NotaFiscal com o ERP
		ControladorIntegracao controladorIntegracao =
						(ControladorIntegracao) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
		controladorIntegracao.atualizarIntegracaoNotaFiscal(documentoFiscal, dadosAuditoria);
	}

	/**
	 * Tratar dados nfe status autorizada.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @param statusAutorizada
	 *            the status autorizada
	 * @param nfe
	 *            the nfe
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void tratarDadosNfeStatusAutorizada(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria,
					EntidadeConteudo statusAutorizada, Nfe nfe) throws NegocioException, ConcorrenciaException {

		ControladorCobranca controladorCobranca =
						(ControladorCobranca) ServiceLocator.getInstancia().getBeanPorID(ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA);
		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);

		inserirInfoDebitoAutoCobrancaBancaria(documentoFiscal, dadosAuditoria);
		registrarLancamentoContabil(documentoFiscal, dadosAuditoria);

		documentoFiscal.setChaveAcesso(nfe.getChaveAcesso());
		documentoFiscal.setStatusNfe(statusAutorizada);
		documentoFiscal.setDadosAuditoria(dadosAuditoria);
		controladorFatura.atualizarDocumentoFiscal(documentoFiscal);

		Long codigoSituacaoNormal =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_CREDITO_DEBITO_NORMAL));
		CreditoDebitoSituacao creditoDebitoSituacao = controladorCobranca.obterCreditoDebitoSituacao(codigoSituacaoNormal);

		Fatura fatura = documentoFiscal.getFatura();
		fatura.setCreditoDebitoSituacao(creditoDebitoSituacao);
		fatura.setDadosAuditoria(dadosAuditoria);

		controladorFatura.atualizar(fatura);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.nfe.ControladorNfe#tratarDadosNfeStatusAutorizada(br.com.ggas.faturamento.DocumentoFiscal,
	 * br.com.ggas.faturamento.fatura.DadosResumoFatura, br.com.ggas.auditoria.DadosAuditoria, br.com.ggas.geral.EntidadeConteudo)
	 */
	@Override
	public void tratarDadosNfeStatusAutorizada(DocumentoFiscal documentoFiscal, DadosResumoFatura dadosResumoFatura,
					DadosAuditoria dadosAuditoria, EntidadeConteudo statusAutorizada) throws NegocioException, ConcorrenciaException {

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		if (documentoFiscal.getSerie().isIndicadorSerieEletronica()) {
			inserirInfoDebitoAutoCobrancaBancaria(documentoFiscal, dadosAuditoria);
		}
		registrarLancamentoContabilComDadosFatura(documentoFiscal, dadosResumoFatura, dadosAuditoria);
		inserirFaturaImpressao(documentoFiscal, dadosAuditoria);

		documentoFiscal.setStatusNfe(statusAutorizada);
		documentoFiscal.setDadosAuditoria(dadosAuditoria);

		controladorFatura.atualizarDocumentoFiscal(documentoFiscal);

	}

	/**
	 * Obter refaturamento historico atual.
	 * 
	 * @param idFatura
	 *            the id fatura
	 * @return the refaturamento historico
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private RefaturamentoHistorico obterRefaturamentoHistoricoAtual(Long idFatura) throws NegocioException {

		Criteria criteria = this.createCriteria(RefaturamentoHistorico.class);
		criteria.add(Restrictions.eq("fatura.chavePrimaria", idFatura));
		criteria.add(Restrictions.eq("indicadorProcessoConcluido", Boolean.FALSE));
		criteria.setProjection(Projections.max(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA));

		Long id = (Long) criteria.uniqueResult();

		if (id != null) {

			criteria = this.createCriteria(RefaturamentoHistorico.class);
			criteria.add(Restrictions.idEq(id));

			return (RefaturamentoHistorico) criteria.uniqueResult();

		} else {

			return null;

		}

	}

	/**
	 * Gerar anomalia faturamento.
	 * 
	 * @param nfe
	 *            the nfe
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void gerarAnomaliaFaturamento(Nfe nfe, DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria) throws NegocioException {

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		String mensagem = "";

		NfeMensagem nfeMensagem = null;

		if (nfe.getNumeroMensagemC() != null) {

			nfeMensagem = this.obterNfeMensagem(Long.valueOf(nfe.getNumeroMensagemC()));
			mensagem = nfeMensagem.getDescricaoMensagem();

		} else if (nfe.getNumeroMensagemE() != null) {

			nfeMensagem = this.obterNfeMensagem(Long.valueOf(nfe.getNumeroMensagemE()));
			mensagem = nfeMensagem.getDescricaoMensagem();

		} else {
			NfeLog nfeLog = this.obterNfeLogPorNfe(nfe.getChavePrimaria());
			mensagem = nfeLog.getDescricao();
		}

		PontoConsumo pontoConsumo = documentoFiscal.getFatura().getPontoConsumo();
		if (pontoConsumo == null) {
			Collection<Fatura> listaFaturasAgrupadas =
							controladorFatura.consultarFaturaPorFaturaAgrupamento(documentoFiscal.getFatura().getChavePrimaria());
			pontoConsumo = listaFaturasAgrupadas.iterator().next().getPontoConsumo();
		}

		if (mensagem.length() > CEM) {
			mensagem = mensagem.substring(0, CEM);
		}

		controladorFatura.inserirAnormalidade(FaturamentoAnormalidade.CODIGO_ERRO_GERACAO_NOTA_FISCAL_ELETRONICA, pontoConsumo,
						documentoFiscal.getFatura().getAnoMesReferencia(), documentoFiscal.getFatura().getNumeroCiclo(), dadosAuditoria,
						mensagem);

	}

	/**
	 * Obter nfe mensagem.
	 * 
	 * @param idNfeMensagem
	 *            the id nfe mensagem
	 * @return the nfe mensagem
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private NfeMensagem obterNfeMensagem(Long idNfeMensagem) throws NegocioException {

		Criteria criteria = this.createCriteria(NfeMensagem.class);
		criteria.add(Restrictions.idEq(idNfeMensagem));

		return (NfeMensagem) criteria.uniqueResult();

	}

	/**
	 * Obter nfe log por nfe.
	 * 
	 * @param idNfe
	 *            the id nfe
	 * @return the nfe log
	 * @throws NegocioException
	 *             the negocio exception
	 */
	@SuppressWarnings("unchecked")
	private NfeLog obterNfeLogPorNfe(Long idNfe) throws NegocioException {

		NfeLog nfeLog = null;

		Criteria criteria = this.createCriteria(NfeLog.class);
		criteria.add(Restrictions.eq("nfe.chavePrimaria", idNfe));
		criteria.addOrder(Order.desc(EntidadeNegocio.ATRIBUTO_CHAVE_PRIMARIA));

		Collection<NfeLog> listaNfeLog = criteria.list();

		if (listaNfeLog != null && !listaNfeLog.isEmpty()) {
			nfeLog = listaNfeLog.iterator().next();
		}

		return nfeLog;

	}

	/**
	 * Checks if is erro nfe.
	 * 
	 * @param nfe
	 *            the nfe
	 * @return the boolean
	 */
	private Boolean isErroNfe(Nfe nfe) {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorEntidadeConteudo controladorEntidadeConteudo =
						(ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
										ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		Long codigoNaoProcessada =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_NAO_PROCESSADA));
		Long codigoAguardandoEnvio =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AGUARDANDO_ENVIO));
		Long codigoAguardandoRetorno =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AGUARDANDO_RETORNO));

		Long codigoErroValidacao =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_ERRO_VALIDACAO));
		Long codigoErroMontagemXml =
						Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_ERRO_MONTAGEM_XML));
		Long codigoErroEnvioSefaz =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_ERRO_ENVIO_PARA_SEFAZ));
		Long codigoErroBuscaRetornoSefaz =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_ERRO_BUSCA_RETORNO_SEFAZ));
		Long codigoErroContigenciaEnvioSefaz =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_CONTINGENCIA_ENVIO_PARA_SEFAZ));
		Long codigoErroContigenciaRetornoEnvioSefaz =
						Long.valueOf(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_CONTINGENCIA_RETORNO_ENVIO_PARA_SEFAZ));
		Long codigoErroGeral = Long.valueOf(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_ERRO_GERAL));

		EntidadeConteudo naoProcessada = controladorEntidadeConteudo.obterEntidadeConteudo(codigoNaoProcessada);
		EntidadeConteudo aguardandoEnvio = controladorEntidadeConteudo.obterEntidadeConteudo(codigoAguardandoEnvio);
		EntidadeConteudo aguardandoRetorno = controladorEntidadeConteudo.obterEntidadeConteudo(codigoAguardandoRetorno);

		EntidadeConteudo erroValidacao = controladorEntidadeConteudo.obterEntidadeConteudo(codigoErroValidacao);
		EntidadeConteudo erroMontagemXml = controladorEntidadeConteudo.obterEntidadeConteudo(codigoErroMontagemXml);
		EntidadeConteudo erroEnvioSefaz = controladorEntidadeConteudo.obterEntidadeConteudo(codigoErroEnvioSefaz);
		EntidadeConteudo erroBuscaRetornoSefaz = controladorEntidadeConteudo.obterEntidadeConteudo(codigoErroBuscaRetornoSefaz);
		EntidadeConteudo erroContigenciaEnvioSefaz = controladorEntidadeConteudo.obterEntidadeConteudo(codigoErroContigenciaEnvioSefaz);
		EntidadeConteudo erroContigenciaRetornoEnvioSefaz =
						controladorEntidadeConteudo.obterEntidadeConteudo(codigoErroContigenciaRetornoEnvioSefaz);
		EntidadeConteudo erroGeral = controladorEntidadeConteudo.obterEntidadeConteudo(codigoErroGeral);

		if (nfe.getSisStatus().equals(erroValidacao.getDescricaoAbreviada())
						|| nfe.getSisStatus().equals(erroMontagemXml.getDescricaoAbreviada())
						|| nfe.getSisStatus().equals(erroEnvioSefaz.getDescricaoAbreviada())
						|| nfe.getSisStatus().equals(erroBuscaRetornoSefaz.getDescricaoAbreviada())
						|| nfe.getSisStatus().equals(erroContigenciaEnvioSefaz.getDescricaoAbreviada())
						|| nfe.getSisStatus().equals(erroContigenciaRetornoEnvioSefaz.getDescricaoAbreviada())
						|| nfe.getSisStatus().equals(erroGeral.getDescricaoAbreviada())
						|| nfe.getSisStatus().equals(naoProcessada.getDescricaoAbreviada())
						|| nfe.getSisStatus().equals(aguardandoEnvio.getDescricaoAbreviada())
						|| nfe.getSisStatus().equals(aguardandoRetorno.getDescricaoAbreviada())) {

			return Boolean.TRUE;

		} else {

			return Boolean.FALSE;

		}

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.nfe.ControladorNfe#processarRetornoNfeSaida(java.lang.StringBuilder, br.com.ggas.auditoria.DadosAuditoria)
	 */
	@Override
	public void processarRetornoNfeSaida(StringBuilder logProcessamento, 
				DadosAuditoria dadosAuditoria, String dataEmissao) throws GGASException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Long codigoStatusNfeAguardandoRetorno =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AGUARDANDO_RETORNO));
		
		Long codigoTipoOperacaoSaida =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_TIPO_OPERACAO_SAIDA));
		ControladorIntegracao controladorIntegracao =
								(ControladorIntegracao) ServiceLocator.getInstancia().getControladorNegocio(
												ControladorIntegracao.BEAN_ID_CONTROLADOR_INTEGRACAO);
		
		/* Altera os dados das linhas da tabela NFE com as linhas referentes
		 * em TI_NOTA_FISCAL altualizadas pelo portal nfe do ERP externo. 
		 */		
		controladorIntegracao.atualizarCamposNFE(dataEmissao);		

		// Consultar dados na tabela de DOCUMENTO_FISCAL, situação: 'Aguardando retorno'.
		Collection<DocumentoFiscal> listaDocumentoFiscal =
				controladorFatura.consultarDocumentoFiscalPorTipoOperacaoStatusNfe(codigoTipoOperacaoSaida,
						codigoStatusNfeAguardandoRetorno);
		
		for (DocumentoFiscal documentoFiscal : listaDocumentoFiscal) {
			// Para os resultados encontrados, procurar linha na tabela NFE, ver se houve
			// mudança de status.
			Nfe nfe = this.consultarDadosNotaFiscalEletronica(documentoFiscal.getNumero(),
					documentoFiscal.getDescricaoSerie());

			if (nfe != null) {
				atualizarSituacaoDenegado(documentoFiscal.getFatura().getCliente().getChavePrimaria(),
						nfe.getSisStatus());
			}
			// Se houve mudança de status, de acordo com o código realizar os devidos
			// processamentos.
			if (nfe != null && documentoFiscal.getStatusNfe() != null
					&& !documentoFiscal.getStatusNfe().getDescricaoAbreviada().equals(nfe.getSisStatus())
					&& !this.isErroNfe(nfe)) {

				this.tratarDadosNotaFiscalEletronica(nfe, documentoFiscal, dadosAuditoria);
				
			}
		}
	}

	/**
	 * Atualizar situacao denegado.
	 * 
	 * @param idCliente
	 *            the id cliente
	 * @param statusNfe
	 *            the status nfe
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void atualizarSituacaoDenegado(Long idCliente, String statusNfe) throws NegocioException, ConcorrenciaException {

		Cliente cliente = (Cliente) ServiceLocator.getInstancia().getControladorCliente().obter(idCliente);

		ConstanteSistema constanteSistema =
						ServiceLocator.getInstancia().getControladorConstanteSistema().obterConstantePorCodigo(Constantes.C_NFE_DENEGADA);

		EntidadeConteudo entidadeConteudo =
						(EntidadeConteudo) ServiceLocator.getInstancia().getControladorEntidadeConteudo()
										.obter(Long.parseLong(constanteSistema.getValor()));

		String statusNfeDenegada = entidadeConteudo.getCodigo();

		if (Boolean.TRUE.equals(statusNfe.equals(statusNfeDenegada))) {
			cliente.setIndicadorDenegado(true);
		} else {
			cliente.setIndicadorDenegado(false);
		}

		ServiceLocator.getInstancia().getControladorCliente().atualizar(cliente);
	}

	/**
	 * Inserir info debito auto cobranca bancaria.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 */
	private void inserirInfoDebitoAutoCobrancaBancaria(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria)
					throws NegocioException {

		ControladorDocumentoCobranca controladorDocumentoCobranca =
						(ControladorDocumentoCobranca) ServiceLocator.getInstancia().getBeanPorID(
										ControladorDocumentoCobranca.BEAN_ID_CONTROLADOR_DOCUMENTO_COBRANCA);
		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		Fatura fatura = documentoFiscal.getFatura();
		Collection<DocumentoCobranca> listaDocumentosCobranca =
						controladorDocumentoCobranca.consultarDocumentoCobrancaPelaFatura(fatura.getChavePrimaria(), null);
		if (listaDocumentosCobranca != null && !listaDocumentosCobranca.isEmpty()) {
			controladorFatura.inserirInfoDebitoAutoCobrancaBancaria(new ArrayList<DocumentoCobranca>(listaDocumentosCobranca),
							dadosAuditoria, fatura);
		}
	}

	/**
	 * Registrar lancamento contabil com dados fatura.
	 * 
	 * @param documentoFiscal
	 *            the documento fiscal
	 * @param dadosResumoFatura
	 *            the dados resumo fatura
	 * @param dadosAuditoria
	 *            the dados auditoria
	 * @throws NumberFormatException
	 *             the number format exception
	 * @throws NegocioException
	 *             the negocio exception
	 * @throws ConcorrenciaException
	 *             the concorrencia exception
	 */
	private void registrarLancamentoContabilComDadosFatura(DocumentoFiscal documentoFiscal, DadosResumoFatura dadosResumoFatura,
					DadosAuditoria dadosAuditoria) throws NegocioException, ConcorrenciaException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorCreditoDebito controladorCreditoDebito =
						(ControladorCreditoDebito) ServiceLocator.getInstancia().getBeanPorID(
										ControladorCreditoDebito.BEAN_ID_CONTROLADOR_CREDITO_DEBITO);

		Long idTipoFaturamentoProduto =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_FATURAMENTO_PRODUTO));
		Map<String, Long> parametros = controladorFatura.parametrosMontarCodigoChaveEventoComercial();

		Boolean isProduto = documentoFiscal.getTipoFaturamento().getChavePrimaria() == idTipoFaturamentoProduto;
		Fatura fatura = documentoFiscal.getFatura();
		fatura.setDadosAuditoria(dadosAuditoria);

		controladorFatura.registrarLancamentoContabilComDadosResumoFatura(fatura, OperacaoContabil.INCLUIR_FATURA, parametros,
						dadosResumoFatura, isProduto);

		for (FaturaItem faturaItem : fatura.getListaFaturaItem()) {
			if (faturaItem.getCreditoDebitoARealizar() != null) {
				controladorCreditoDebito.registrarLancamentoContabilTransferenciaLongoCurtoPrazo(faturaItem.getCreditoDebitoARealizar(),
								dadosAuditoria);
			}
		}
	}
	/**
	 * @param documentoFiscal
	 * @param dadosAuditoria
	 * @throws NegocioException
	 * @throws ConcorrenciaException
	 */
	private void registrarLancamentoContabil(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria) throws NegocioException,
					ConcorrenciaException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);
		ControladorCreditoDebito controladorCreditoDebito =
						(ControladorCreditoDebito) ServiceLocator.getInstancia().getBeanPorID(
										ControladorCreditoDebito.BEAN_ID_CONTROLADOR_CREDITO_DEBITO);

		Long idTipoFaturamentoProduto =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_FATURAMENTO_PRODUTO));
		Map<String, Long> parametros = controladorFatura.parametrosMontarCodigoChaveEventoComercial();

		Boolean isProduto = documentoFiscal.getTipoFaturamento().getChavePrimaria() == idTipoFaturamentoProduto;
		Fatura fatura = documentoFiscal.getFatura();
		fatura.setDadosAuditoria(dadosAuditoria);
		controladorFatura.registrarLancamentoContabilAposAutorizacaoFiscal(fatura, OperacaoContabil.INCLUIR_FATURA, parametros, isProduto);
		for (FaturaItem faturaItem : fatura.getListaFaturaItem()) {
			if (faturaItem.getCreditoDebitoARealizar() != null) {
				controladorCreditoDebito.registrarLancamentoContabilTransferenciaLongoCurtoPrazo(faturaItem.getCreditoDebitoARealizar(),
								dadosAuditoria);
			}
		}
	}

	/**
	 * 
	 * @param DocumentoFiscal documentoFiscal
	 * @param DadosAuditoria dadosAuditoria
	 */
	@Override
	public void inserirFaturaImpressao(DocumentoFiscal documentoFiscal, DadosAuditoria dadosAuditoria) throws NegocioException {

		ControladorFatura controladorFatura =
						(ControladorFatura) ServiceLocator.getInstancia().getBeanPorID(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA);

		Fatura fatura = documentoFiscal.getFatura();

		Rota rota = null;
		if (documentoFiscal.getFatura().getPontoConsumo() != null) {
			rota = documentoFiscal.getFatura().getPontoConsumo().getRota();
		}

		GrupoFaturamento grupoFaturamento = null;
		if (rota != null) {
			grupoFaturamento = rota.getGrupoFaturamento();
		}

		if (fatura.getFaturaAgrupada() == null) {
			// Não gerar impressão para faturas
			// não agrupadas
			FaturaImpressao faturaImpressao = (FaturaImpressao) controladorFatura.criarFaturaImpressao();

			faturaImpressao.setGrupoFaturamento(grupoFaturamento);
			faturaImpressao.setFatura(fatura);
			faturaImpressao.setDataGeracao(Calendar.getInstance().getTime());
			
			if (fatura.getContrato() != null) {
				if(fatura.getContrato().getFormaCobranca() == null){
					faturaImpressao.setFormaCobranca(ServiceLocator.getInstancia()
							.getControladorArrecadacao().obterArrecadadorContratoConvenioParaBoletoBancario().getTipoConvenio());
				}else{
					faturaImpressao.setFormaCobranca(fatura.getContrato().getFormaCobranca());
				}
			} else {
				faturaImpressao.setFormaCobranca(ServiceLocator.getInstancia()
						.getControladorArrecadacao().obterArrecadadorContratoConvenioParaBoletoBancario().getTipoConvenio());
			}

			faturaImpressao.setAnoMesReferencia(fatura.getAnoMesReferencia());
			faturaImpressao.setValor(fatura.getValorTotal());
			faturaImpressao.setRota(rota);
			faturaImpressao.setDadosAuditoria(dadosAuditoria);

			controladorFatura.inserir(faturaImpressao);
		}
	}

	/**
	 * Obter status nfe por descricao abreviada.
	 * 
	 * @param descricaoAbreviada
	 *            the descricao abreviada
	 * @return the entidade conteudo
	 * @throws NegocioException
	 *             the negocio exception
	 */
	public EntidadeConteudo obterStatusNfePorDescricaoAbreviada(String descricaoAbreviada) throws NegocioException {

		ControladorConstanteSistema controladorConstanteSistema =
						(ControladorConstanteSistema) ServiceLocator.getInstancia().getControladorNegocio(
										ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA);
		ServiceLocator.getInstancia().getBeanPorID(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA);
		ControladorEntidadeConteudo controladorEntidadeConteudo =
						(ControladorEntidadeConteudo) ServiceLocator.getInstancia().getBeanPorID(
										ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO);

		Map<String, EntidadeConteudo> mapaStatus = new HashMap<String, EntidadeConteudo>();
		EntidadeConteudo entidadeConteudo;

		Long codigoStatusNfeNaoProcessada =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_NAO_PROCESSADA));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeNaoProcessada);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeAguardandoEnvio =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AGUARDANDO_ENVIO));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeAguardandoEnvio);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeAguardandoRetorno =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AGUARDANDO_RETORNO));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeAguardandoRetorno);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeAutorizada =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_AUTORIZADA));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeAutorizada);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeRejeitada =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_REJEITADA));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeRejeitada);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeDenegada =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_DENEGADA));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeDenegada);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeCancelada =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_CANCELADA));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeCancelada);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeErroValidacao =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_ERRO_VALIDACAO));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeErroValidacao);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeErroMontagemXml =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_ERRO_MONTAGEM_XML));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeErroMontagemXml);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeErroEnvioParaSefaz =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_ERRO_ENVIO_PARA_SEFAZ));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeErroEnvioParaSefaz);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeErroBuscaRetornoSefaz =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_ERRO_BUSCA_RETORNO_SEFAZ));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeErroBuscaRetornoSefaz);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeContingenciaEnvioParaSefaz =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_CONTINGENCIA_ENVIO_PARA_SEFAZ));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeContingenciaEnvioParaSefaz);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeContingenciaRetornoEnvioParaSefaz =
						Long.parseLong(controladorConstanteSistema
										.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_CONTINGENCIA_RETORNO_ENVIO_PARA_SEFAZ));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeContingenciaRetornoEnvioParaSefaz);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		Long codigoStatusNfeErroGeral =
						Long.parseLong(controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(Constantes.C_NFE_ERRO_GERAL));
		entidadeConteudo = controladorEntidadeConteudo.obterEntidadeConteudo(codigoStatusNfeErroGeral);
		mapaStatus.put(entidadeConteudo.getDescricaoAbreviada(), entidadeConteudo);

		return mapaStatus.get(descricaoAbreviada);

	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.nfe.ControladorNfe#consultarNfe(java.lang.Long, java.lang.Long, java.lang.Boolean, java.lang.String,
	 * java.lang.String[])
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Nfe> consultarNfe(Long documentoFiscal, Long serie, Boolean habilitado, String ordenacao, String... propriedadesLazy)
					throws NegocioException {

		Query query = null;

		StringBuilder hql = new StringBuilder();
		hql.append(" from ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" nfe ");
		hql.append(" where 1 = 1 ");

		if (documentoFiscal != null) {
			hql.append(" and nfe.documentoFiscal = :documentoFiscal ");
		}

		if (serie != null) {
			hql.append(" and nfe.serie = :serie ");

		}

		if (habilitado != null) {
			hql.append(" and nfe.habilitado = :habilitado ");
		}

		if (ordenacao != null && !"null".equals(ordenacao)) {

			hql.append(" order by  ").append(ordenacao);

		}

		query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());

		if (documentoFiscal != null) {
			query.setLong("documentoFiscal", documentoFiscal);
		}

		if (serie != null) {
			query.setLong("serie", serie);
		}

		if (habilitado != null) {
			query.setBoolean("habilitado", habilitado);
		}

		Collection<Object> lista = query.list();

		if (propriedadesLazy != null && propriedadesLazy.length > 0) {

			lista = super.inicializarLazyColecao(lista, propriedadesLazy);
		}

		return (Collection<Nfe>) (Collection<?>) lista;
	}

	/**
	 * Consultar nota fiscal em Nfe que tiverem o Situação Operacao igual E 
	 * e o Status for igual a AR (Aguardando Retorno)
	 * @param codigoTipoOperacao
	 * @param codigoSituacao
	 * @throws NegocioException
	 */
	public Collection<Nfe> consultarIntegracaoNotaFiscalTipoOperacaoNfe(String codigoTipoOperacao, String codigoSituacao)
			throws NegocioException {

		StringBuilder hql = new StringBuilder();
		
		hql.append(" FROM ");
		hql.append(getClasseEntidade().getSimpleName());
		hql.append(" nfe ");
		
		hql.append(" WHERE ");
		
		hql.append(" nfe.sisOperacao = :codigoTipoOperacao ");
		hql.append(" and nfe.sisStatus = :codigoSituacao ");
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql.toString());
		query.setString("codigoTipoOperacao", codigoTipoOperacao);
		query.setString("codigoSituacao", codigoSituacao);

		return query.list();
	}
	
	
}
