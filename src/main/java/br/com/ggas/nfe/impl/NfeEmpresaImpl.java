/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe.impl;

import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.nfe.NfeEmpresa;

class NfeEmpresaImpl extends EntidadeNegocioImpl implements NfeEmpresa {

	private static final long serialVersionUID = 1L;

	private String nomeFantasia;

	// EMPR_NM_FANTASIA
	// VARCHAR2(60),

	private String nome;

	// EMPR_NM_EMPRESA
	// VARCHAR2(60) not null,

	private String complementoEmpresa;

	// EMPR_DS_COMPL_EMPRESA
	// VARCHAR2(30),

	private String numeroInscricaoEmpresa;

	// EMPR_NR_INSCR_EMPRESA
	// VARCHAR2(20)
	// not
	// null,

	private String cnpj;

	// EMPR_NR_CNPJ_EMPRESA
	// VARCHAR2(20) not null,

	private byte[] logo;

	// EMPR_MM_LOGO BLOB,

	private String imprimeLogo;

	// EMPR_IN_IMPRIME_LOGO
	// VARCHAR2(1) not
	// null,

	private Integer codigoIbgeUF;

	@Override
	public Integer getCodigoIbgeUF() {

		return codigoIbgeUF;
	}

	@Override
	public void setCodigoIbgeUF(Integer codigoIbgeUF) {

		this.codigoIbgeUF = codigoIbgeUF;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#getNomeFantasia()
	 */
	@Override
	public String getNomeFantasia() {

		return nomeFantasia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#setNomeFantasia(java.lang.String)
	 */
	@Override
	public void setNomeFantasia(String nomeFantasia) {

		this.nomeFantasia = nomeFantasia;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#getNome()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#getComplementoEmpresa()
	 */
	@Override
	public String getComplementoEmpresa() {

		return complementoEmpresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa
	 * #setComplementoEmpresa(java.lang.String)
	 */
	@Override
	public void setComplementoEmpresa(String complementoEmpresa) {

		this.complementoEmpresa = complementoEmpresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#getNumeroInscricaoEmpresa()
	 */
	@Override
	public String getNumeroInscricaoEmpresa() {

		return numeroInscricaoEmpresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa
	 * #setNumeroInscricaoEmpresa(java.lang
	 * .String)
	 */
	@Override
	public void setNumeroInscricaoEmpresa(String numeroInscricaoEmpresa) {

		this.numeroInscricaoEmpresa = numeroInscricaoEmpresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#getCnpj()
	 */
	@Override
	public String getCnpj() {

		return cnpj;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#setCnpj(java.lang.String)
	 */
	@Override
	public void setCnpj(String cnpj) {

		this.cnpj = cnpj;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#getLogo()
	 */
	@Override
	public byte[] getLogo() {

		return logo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#setLogo(byte[])
	 */
	@Override
	public void setLogo(byte[] logo) {

		this.logo = logo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#getImprimeLogo()
	 */
	@Override
	public String getImprimeLogo() {

		return imprimeLogo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Empresa#setImprimeLogo(byte[])
	 */
	@Override
	public void setImprimeLogo(String imprimeLogo) {

		this.imprimeLogo = imprimeLogo;
	}
}
