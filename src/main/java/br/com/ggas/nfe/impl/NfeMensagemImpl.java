/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe.impl;

import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.nfe.NfeMensagem;

class NfeMensagemImpl extends EntidadeNegocioImpl implements NfeMensagem {

	private static final long serialVersionUID = 1L;

	private NfeMensagem nfeMensagem;

	// NFME_CD_MSG
	// NUMBER
	// not
	// null,

	private String descricaoMensagem;

	// NFME_DS_MSG
	// VARCHAR2(100),

	private Integer indTipo;

	// NFME_IN_TIPO
	// NUMBER(1) not
	// null,

	private Integer indAcao;

	// NFME_IN_ACAO
	// NUMBER(1)

	@Override
	public NfeMensagem getNfeMensagem() {

		return nfeMensagem;
	}

	@Override
	public void setNfeMensagem(NfeMensagem nfeMensagem) {

		this.nfeMensagem = nfeMensagem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Mensagem#getDescricaoMensagem()
	 */
	@Override
	public String getDescricaoMensagem() {

		return descricaoMensagem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Mensagem
	 * #setDescricaoMensagem(java.lang.String)
	 */
	@Override
	public void setDescricaoMensagem(String descricaoMensagem) {

		this.descricaoMensagem = descricaoMensagem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Mensagem#getIndTipo()
	 */
	@Override
	public Integer getIndTipo() {

		return indTipo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Mensagem#setIndTipo(Integer)
	 */
	@Override
	public void setIndTipo(Integer indTipo) {

		this.indTipo = indTipo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Mensagem#getIndAcao()
	 */
	@Override
	public Integer getIndAcao() {

		return indAcao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .Mensagem#setIndAcao(java.lang.Integer)
	 */
	@Override
	public void setIndAcao(Integer indAcao) {

		this.indAcao = indAcao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}
}
