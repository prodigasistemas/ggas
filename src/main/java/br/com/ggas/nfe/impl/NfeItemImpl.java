/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe.impl;

import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.nfe.Nfe;
import br.com.ggas.nfe.NfeItem;

public class NfeItemImpl extends EntidadeNegocioImpl implements NfeItem {

	private static final long serialVersionUID = -2567211525560244640L;

	private Nfe nfe;

	private Integer numeroItem;

	private String codigoProduto;

	private String codigoEAN;

	private String descricaoProduto;

	private String codigoNCM;

	private String codigoExTIPI;

	private Integer codigoGenero;

	private Integer cfop;

	private String descricaoUnidadeMedidaTributavel;

	private String descricaoUnidadeMedidaComercializacao;

	private Double quantidadeTributavel = 0.0;

	private Double quantidadeComercializacao = 0.0;

	private Double valorTotalBruto = 0.0;

	private Double valorTotalFrete = 0.0;

	private Double valorTotalSeguro = 0.0;

	private Double valorDesconto = 0.0;

	private String indTribICMS;

	private Integer indOrigemICMS = 0;

	private Integer indModBaseCalculoICMS = 0;

	private Double valorBaseCalculoICMS = 0.0;

	private Double valorAliquotaICMS = 0.0;

	private Double valorICMS = 0.0;

	private Integer indModBaseCalculoICMSSubstituto = 0;

	private Double percentualMargemICMS;

	private Double percentualReducaoICMS;

	private Double baseCalculoICMSSubstituto = 0.0;

	private Double valorAliquotaSituacaoTribICMS = 0.0;

	private Double valorICMSSituacao = 0.0;

	private Double percentualReducaoICMSSituacao;

	private String codigoClasseIPI;

	private String cpnjIPI;

	private String codigoSeloIPI;

	private Integer numeroSeloIPI;

	private String codigoEnqIPI;

	private String indSituacaoTribIPI;

	private Double baseCalculoIPI = 0.0;

	private Double numeroUnidadePrdIPI = 0.0;

	private Double valorUnidadeTribIPI = 0.0;

	private Double valorAliquotaIPI = 0.0;

	private Double valorIPI = 0.0;

	private String indSituacaoTribPIS;

	private Double baseCalculoPIS = 0.0;

	private Double percentualAliquotaPIS = 0.0;

	private Double valorPIS = 0.0;

	private Double quantidadeVendPIS = 0.0;

	private Double valorAliquotaPIS = 0.0;

	private String indSituacaoTribCOFINS;

	private Double baseCalculoCOFINS = 0.0;

	private Double percentualAliquotaCOFINS = 0.0;

	private Double valorCOFINS = 0.0;

	private Double valorAliquotaCOFINS = 0.0;

	private Double quantidadeVendCOFINS = 0.0;

	private Double baseCalculoISSQN = 0.0;

	private Double valorAliquotaISSQN = 0.0;

	private Double valorISSQN = 0.0;

	private Integer numeroIbgeMunicipioISSQN;

	private String info;

	private Double valorUnitario = 0.0;

	private Double valorUnitarioTributavel = 0.0;

	private String codigoListaServico;

	private Double baseCalculoImpostoImportacao = 0.0;

	private Double valorDespesasAduaneiras = 0.0;

	private Double valorImpostoImportacao = 0.0;

	private Double valorIof = 0.0;

	private String codigoEanTrib;

	private Double baseCalculoPISSubstituto = 0.0;

	private Double valorPaliqPISSubstituto = 0.0;

	private Double quantidadePISSubstituto = 0.0;

	private Double valorPISSubstituto = 0.0;

	private Double baseCalculoCOFINSSubstituto = 0.0;

	private Double valorPaliqCOFINSSubstituto = 0.0;

	private Double quantidadeCOFINSSubstituto = 0.0;

	private Double valorAliqCOFINSSubstituto = 0.0;

	private Double valorCOFINSSubstituto = 0.0;

	private Double valorRaliqPISSubstituto = 0.0;

	private Double valorOutros = 0.0;

	private Integer indicadorTotalizacao = 0;

	private Double valorICMSDesonerado = 0.0;

	private Double valorICMSOperacao = 0.0;

	private Double percentualDiferimentoICMS = 0.0;

	private Double valorICMSDiferido = 0.0;

	private Integer motivoDesoneracaoICMS;

	private String numeroRECOPI;

	private Double valorDeducaoISSQN;

	private Double valorOutrasISSQN;

	private Double valorDescontoIncondicionadoISSQN;

	private Double valorDescontoCondicionadoISSQN;

	private Double valorISSRetido;

	private Integer indicadorExigibilidadeISSQN;

	private String codigoServico;

	private String codigoMunicipioServico;

	private String codigoPaisServico;

	private String numeroProcessoExigibilidadeISSQN;

	private Integer indicadorIncentivoFiscalISSQN;

	private String codigoSituacaoTributariaISSQN;

	private Double valorTotalImpostos;

	private String numeroPedidoCompra;

	private Integer numeroItemPedidoCompra;

	private String codigoFCI;
	
	private String codigoCEST;

	@Override
	public void setNumeroItem(Integer numeroItem) {

		this.numeroItem = numeroItem;
	}

	@Override
	public Integer getNumeroItem() {

		return numeroItem;
	}

	@Override
	public String getCodigoProduto() {

		return codigoProduto;
	}

	@Override
	public void setCodigoProduto(String codigoProduto) {

		this.codigoProduto = codigoProduto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getCodigoEAN()
	 */
	@Override
	public String getCodigoEAN() {

		return codigoEAN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setCodigoEAN(java.lang.String)
	 */
	@Override
	public void setCodigoEAN(String codigoEAN) {

		this.codigoEAN = codigoEAN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getDescricaoProduto()
	 */
	@Override
	public String getDescricaoProduto() {

		return descricaoProduto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setDescricaoProduto(java.lang.String)
	 */
	@Override
	public void setDescricaoProduto(String descricaoProduto) {

		this.descricaoProduto = descricaoProduto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getCodigoNCM()
	 */
	@Override
	public String getCodigoNCM() {

		return codigoNCM;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setCodigoNCM(java.lang.String)
	 */
	@Override
	public void setCodigoNCM(String codigoNCM) {

		this.codigoNCM = codigoNCM;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getCodigoExTIPI()
	 */
	@Override
	public String getCodigoExTIPI() {

		return codigoExTIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setCodigoExTIPI(java.lang.String)
	 */
	@Override
	public void setCodigoExTIPI(String codigoExTIPI) {

		this.codigoExTIPI = codigoExTIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getCodigoGenero()
	 */
	@Override
	public Integer getCodigoGenero() {

		return codigoGenero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setCodigoGenero(java.lang.Integer)
	 */
	@Override
	public void setCodigoGenero(Integer codigoGenero) {

		this.codigoGenero = codigoGenero;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getCfop()
	 */
	@Override
	public Integer getCfop() {

		return cfop;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setCfop(Integer)
	 */
	@Override
	public void setCfop(Integer cfop) {

		this.cfop = cfop;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getDescricaoUnidadeTributaria()
	 */
	@Override
	public String getDescricaoUnidadeMedidaTributavel() {

		return descricaoUnidadeMedidaTributavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setDescricaoUnidadeTributaria(java.lang.String
	 * )
	 */
	@Override
	public void setDescricaoUnidadeMedidaTributavel(String descricaoUnidadeMedidaTributavel) {

		this.descricaoUnidadeMedidaTributavel = descricaoUnidadeMedidaTributavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getDescricaoUnidadeCompradora()
	 */
	@Override
	public String getDescricaoUnidadeMedidaComercializacao() {

		return descricaoUnidadeMedidaComercializacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setDescricaoUnidadeCompradora(java.lang.String
	 * )
	 */
	@Override
	public void setDescricaoUnidadeMedidaComercializacao(String descricaoUnidadeMedidaComercializacao) {

		this.descricaoUnidadeMedidaComercializacao = descricaoUnidadeMedidaComercializacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getQuantidadeTrib()
	 */
	@Override
	public Double getQuantidadeTributavel() {

		return quantidadeTributavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setQuantidadeTrib(Double)
	 */
	@Override
	public void setQuantidadeTributavel(Double quantidadeTrib) {

		this.quantidadeTributavel = quantidadeTrib;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getQuantidadeCompradora()
	 */
	@Override
	public Double getQuantidadeComercializacao() {

		return quantidadeComercializacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setQuantidadeCompradora(Double)
	 */
	@Override
	public void setQuantidadeComercializacao(Double quantidadeComercializacao) {

		this.quantidadeComercializacao = quantidadeComercializacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorTotalBruto()
	 */
	@Override
	public Double getValorTotalBruto() {

		return valorTotalBruto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorTotalBruto(Double)
	 */
	@Override
	public void setValorTotalBruto(Double valorTotalBruto) {

		this.valorTotalBruto = valorTotalBruto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorTotalFrete()
	 */
	@Override
	public Double getValorTotalFrete() {

		return valorTotalFrete;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorTotalFrete(java.lang.Double)
	 */
	@Override
	public void setValorTotalFrete(Double valorTotalFrete) {

		this.valorTotalFrete = valorTotalFrete;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorTotalSeguro()
	 */
	@Override
	public Double getValorTotalSeguro() {

		return valorTotalSeguro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorTotalSeguro(java.lang.Double)
	 */
	@Override
	public void setValorTotalSeguro(Double valorTotalSeguro) {

		this.valorTotalSeguro = valorTotalSeguro;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorDesconto()
	 */
	@Override
	public Double getValorDesconto() {

		return valorDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorDesconto(java.lang.Double)
	 */
	@Override
	public void setValorDesconto(Double valorDesconto) {

		this.valorDesconto = valorDesconto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getIndTribICMS()
	 */
	@Override
	public String getIndTribICMS() {

		return indTribICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setInd
	 * @OverrideTribICMS(java.lang.String)
	 */
	@Override
	public void setIndTribICMS(String indTribICMS) {

		this.indTribICMS = indTribICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getIndOrigemICMS()
	 */
	@Override
	public Integer getIndOrigemICMS() {

		return indOrigemICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setIndOrigemICMS(Integer)
	 */
	@Override
	public void setIndOrigemICMS(Integer indOrigemICMS) {

		this.indOrigemICMS = indOrigemICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getIndModBaseCalculoICMS()
	 */
	@Override
	public Integer getIndModBaseCalculoICMS() {

		return indModBaseCalculoICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setIndModBaseCalculoICMS(java.lang.Integer)
	 */
	@Override
	public void setIndModBaseCalculoICMS(Integer indModBaseCalculoICMS) {

		this.indModBaseCalculoICMS = indModBaseCalculoICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorBaseCalculoICMS()
	 */
	@Override
	public Double getValorBaseCalculoICMS() {

		return valorBaseCalculoICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorBaseCalculoICMS(java.lang.Double)
	 */
	@Override
	public void setValorBaseCalculoICMS(Double valorBaseCalculoICMS) {

		this.valorBaseCalculoICMS = valorBaseCalculoICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorAliquotaICMS()
	 */
	@Override
	public Double getValorAliquotaICMS() {

		return valorAliquotaICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorAliquotaICMS(java.lang.Double)
	 */
	@Override
	public void setValorAliquotaICMS(Double valorAliquotaICMS) {

		this.valorAliquotaICMS = valorAliquotaICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorICMS()
	 */
	@Override
	public Double getValorICMS() {

		return valorICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorICMS(java.lang.Double)
	 */
	@Override
	public void setValorICMS(Double valorICMS) {

		this.valorICMS = valorICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getIndModBaseCalculoICMSSubstituto()
	 */
	@Override
	public Integer getIndModBaseCalculoICMSSubstituto() {

		return indModBaseCalculoICMSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setIndModBaseCalculoICMSSubstituto(java.lang
	 * .Integer)
	 */
	@Override
	public void setIndModBaseCalculoICMSSubstituto(Integer indModBaseCalculoICMSSubstituto) {

		this.indModBaseCalculoICMSSubstituto = indModBaseCalculoICMSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getPercentualMargemICMS()
	 */
	@Override
	public Double getPercentualMargemICMS() {

		return percentualMargemICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setPercentualMargemICMS(java.lang.Double)
	 */
	@Override
	public void setPercentualMargemICMS(Double percentualMargemICMS) {

		this.percentualMargemICMS = percentualMargemICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getPercentualReducaoICMS()
	 */
	@Override
	public Double getPercentualReducaoICMS() {

		return percentualReducaoICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setPercentualReducaoICMS(java.lang.Double)
	 */
	@Override
	public void setPercentualReducaoICMS(Double percentualReducaoICMS) {

		this.percentualReducaoICMS = percentualReducaoICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getBaseCalculoICMSSubstituto()
	 */
	@Override
	public Double getBaseCalculoICMSSubstituto() {

		return baseCalculoICMSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setBaseCalculoICMSSubstituto(java.lang.Double
	 * )
	 */
	@Override
	public void setBaseCalculoICMSSubstituto(Double baseCalculoICMSSubstituto) {

		this.baseCalculoICMSSubstituto = baseCalculoICMSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorAliquotaSituacaoTribICMS()
	 */
	@Override
	public Double getValorAliquotaSituacaoTribICMS() {

		return valorAliquotaSituacaoTribICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setValorAliquotaSituacaiICMS(java.lang.Double
	 * )
	 */
	@Override
	public void setValorAliquotaSituacaoTribICMS(Double valorAliquotaSituacaoTribICMS) {

		this.valorAliquotaSituacaoTribICMS = valorAliquotaSituacaoTribICMS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorICMSSituacao()
	 */
	@Override
	public Double getValorICMSSituacao() {

		return valorICMSSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorICMSSituacao(java.lang.Double)
	 */
	@Override
	public void setValorICMSSituacao(Double valorICMSSituacao) {

		this.valorICMSSituacao = valorICMSSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getPercentualReducaoICMSSituacao()
	 */
	@Override
	public Double getPercentualReducaoICMSSituacao() {

		return percentualReducaoICMSSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setPercentualReducaoICMSSituacao(java.lang.
	 * Double)
	 */
	@Override
	public void setPercentualReducaoICMSSituacao(Double percentualReducaoICMSSituacao) {

		this.percentualReducaoICMSSituacao = percentualReducaoICMSSituacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getCodigoClasseIPI()
	 */
	@Override
	public String getCodigoClasseIPI() {

		return codigoClasseIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setCodigoClasseIPI(java.lang.String)
	 */
	@Override
	public void setCodigoClasseIPI(String codigoClasseIPI) {

		this.codigoClasseIPI = codigoClasseIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getCpnjIPI()
	 */
	@Override
	public String getCpnjIPI() {

		return cpnjIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setCpnjIPI(java.lang.String)
	 */
	@Override
	public void setCpnjIPI(String cpnjIPI) {

		this.cpnjIPI = cpnjIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getCodigoSeloIPI()
	 */
	@Override
	public String getCodigoSeloIPI() {

		return codigoSeloIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setCodigoSeloIPI(java.lang.String)
	 */
	@Override
	public void setCodigoSeloIPI(String codigoSeloIPI) {

		this.codigoSeloIPI = codigoSeloIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getNumeroSeloIPI()
	 */
	@Override
	public Integer getNumeroSeloIPI() {

		return numeroSeloIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setNumeroSeloIPI(java.lang.Integer)
	 */
	@Override
	public void setNumeroSeloIPI(Integer numeroSeloIPI) {

		this.numeroSeloIPI = numeroSeloIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getCodigoEnqIPI()
	 */
	@Override
	public String getCodigoEnqIPI() {

		return codigoEnqIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setCodigoEnqIPI(java.lang.String)
	 */
	@Override
	public void setCodigoEnqIPI(String codigoEnqIPI) {

		this.codigoEnqIPI = codigoEnqIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getIndSituacaoTribIPI()
	 */
	@Override
	public String getIndSituacaoTribIPI() {

		return indSituacaoTribIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setIndSituacaoTribIPI(java.lang.String)
	 */
	@Override
	public void setIndSituacaoTribIPI(String indSituacaoTribIPI) {

		this.indSituacaoTribIPI = indSituacaoTribIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getBaseCalculoIPI()
	 */
	@Override
	public Double getBaseCalculoIPI() {

		return baseCalculoIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setBaseCalculoIPI(java.lang.Double)
	 */
	@Override
	public void setBaseCalculoIPI(Double baseCalculoIPI) {

		this.baseCalculoIPI = baseCalculoIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getNumeroUnidadePrdIPI()
	 */
	@Override
	public Double getNumeroUnidadePrdIPI() {

		return numeroUnidadePrdIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setNumeroUnidadePrdIPI(java.lang.Double)
	 */
	@Override
	public void setNumeroUnidadePrdIPI(Double numeroUnidadePrdIPI) {

		this.numeroUnidadePrdIPI = numeroUnidadePrdIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorUnidadeTribIPI()
	 */
	@Override
	public Double getValorUnidadeTribIPI() {

		return valorUnidadeTribIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorUnidadeTribIPI(java.lang.Double)
	 */
	@Override
	public void setValorUnidadeTribIPI(Double valorUnidadeTribIPI) {

		this.valorUnidadeTribIPI = valorUnidadeTribIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorAliquotaIPI()
	 */
	@Override
	public Double getValorAliquotaIPI() {

		return valorAliquotaIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorAliquotaIPI(java.lang.Double)
	 */
	@Override
	public void setValorAliquotaIPI(Double valorAliquotaIPI) {

		this.valorAliquotaIPI = valorAliquotaIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValor
	 * @OverrideIPI()
	 */
	@Override
	public Double getValorIPI() {

		return valorIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorIPI(java.lang.Double)
	 */
	@Override
	public void setValorIPI(Double valorIPI) {

		this.valorIPI = valorIPI;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getIndSituacaoTribPIS()
	 */
	@Override
	public String getIndSituacaoTribPIS() {

		return indSituacaoTribPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setIndSituacaoTribPIS(java.lang.String)
	 */
	@Override
	public void setIndSituacaoTribPIS(String indSituacaoTribPIS) {

		this.indSituacaoTribPIS = indSituacaoTribPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getBaseCalculoPIS()
	 */
	@Override
	public Double getBaseCalculoPIS() {

		return baseCalculoPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setBaseCalculoPIS(java.lang.Double)
	 */
	@Override
	public void setBaseCalculoPIS(Double baseCalculoPIS) {

		this.baseCalculoPIS = baseCalculoPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getPercentualAliquotaPIS()
	 */
	@Override
	public Double getPercentualAliquotaPIS() {

		return percentualAliquotaPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setPercentualAliquotaPIS(java.lang.Double)
	 */
	@Override
	public void setPercentualAliquotaPIS(Double percentualAliquotaPIS) {

		this.percentualAliquotaPIS = percentualAliquotaPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorPIS()
	 */
	@Override
	public Double getValorPIS() {

		return valorPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorPIS(java.lang.Double)
	 */
	@Override
	public void setValorPIS(Double valorPIS) {

		this.valorPIS = valorPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getQuantidadeVendPIS()
	 */
	@Override
	public Double getQuantidadeVendPIS() {

		return quantidadeVendPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setQuantidadeVendPIS(java.lang.Double)
	 */
	@Override
	public void setQuantidadeVendPIS(Double quantidadeVendPIS) {

		this.quantidadeVendPIS = quantidadeVendPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorAliquotaPIS()
	 */
	@Override
	public Double getValorAliquotaPIS() {

		return valorAliquotaPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorAliquotaPIS(java.lang.Double)
	 */
	@Override
	public void setValorAliquotaPIS(Double valorAliquotaPIS) {

		this.valorAliquotaPIS = valorAliquotaPIS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getIndSituacaoTribCOFINS()
	 */
	@Override
	public String getIndSituacaoTribCOFINS() {

		return indSituacaoTribCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setIndSituacaoTribCOFINS(java.lang.String)
	 */
	@Override
	public void setIndSituacaoTribCOFINS(String indSituacaoTribCOFINS) {

		this.indSituacaoTribCOFINS = indSituacaoTribCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getBaseCalculoCOFINS()
	 */
	@Override
	public Double getBaseCalculoCOFINS() {

		return baseCalculoCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setBaseCalculoCOFINS(java.lang.Double)
	 */
	@Override
	public void setBaseCalculoCOFINS(Double baseCalculoCOFINS) {

		this.baseCalculoCOFINS = baseCalculoCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getPercentualAliquotaCOFINS()
	 */
	@Override
	public Double getPercentualAliquotaCOFINS() {

		return percentualAliquotaCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setPercentualAliquotaCOFINS(java.lang.Double
	 * )
	 */
	@Override
	public void setPercentualAliquotaCOFINS(Double percentualAliquotaCOFINS) {

		this.percentualAliquotaCOFINS = percentualAliquotaCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorCOFINS()
	 */
	@Override
	public Double getValorCOFINS() {

		return valorCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorCOFINS(java.lang.Double)
	 */
	@Override
	public void setValorCOFINS(Double valorCOFINS) {

		this.valorCOFINS = valorCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorAliquotaCOFINS()
	 */
	@Override
	public Double getValorAliquotaCOFINS() {

		return valorAliquotaCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorAliquotaCOFINS(java.lang.Double)
	 */
	@Override
	public void setValorAliquotaCOFINS(Double valorAliquotaCOFINS) {

		this.valorAliquotaCOFINS = valorAliquotaCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getQuantidadeVendCOFINS()
	 */
	@Override
	public Double getQuantidadeVendCOFINS() {

		return quantidadeVendCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setQuantidadeVendCOFINS(java.lang.Double)
	 */
	@Override
	public void setQuantidadeVendCOFINS(Double quantidadeVendCOFINS) {

		this.quantidadeVendCOFINS = quantidadeVendCOFINS;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getBaseCalculoISSQN()
	 */
	@Override
	public Double getBaseCalculoISSQN() {

		return baseCalculoISSQN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setBaseCalculoISSQN(java.lang.Double)
	 */
	@Override
	public void setBaseCalculoISSQN(Double baseCalculoISSQN) {

		this.baseCalculoISSQN = baseCalculoISSQN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorAliquotaISSQN()
	 */
	@Override
	public Double getValorAliquotaISSQN() {

		return valorAliquotaISSQN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorAliquotaISSQN(java.lang.Double)
	 */
	@Override
	public void setValorAliquotaISSQN(Double valorAliquotaISSQN) {

		this.valorAliquotaISSQN = valorAliquotaISSQN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorISSQN()
	 */
	@Override
	public Double getValorISSQN() {

		return valorISSQN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorISSQN(java.lang.Double)
	 */
	@Override
	public void setValorISSQN(Double valorISSQN) {

		this.valorISSQN = valorISSQN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getNumeroIbgeMunicipioISSQN()
	 */
	@Override
	public Integer getNumeroIbgeMunicipioISSQN() {

		return numeroIbgeMunicipioISSQN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setNumeroIbgeMunicipioISSQN(java.lang.Integer
	 * )
	 */
	@Override
	public void setNumeroIbgeMunicipioISSQN(Integer numeroIbgeMunicipioISSQN) {

		this.numeroIbgeMunicipioISSQN = numeroIbgeMunicipioISSQN;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getInfo()
	 */
	@Override
	public String getInfo() {

		return info;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setInfo(java.lang.String)
	 */
	@Override
	public void setInfo(String info) {

		this.info = info;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorUnitario()
	 */
	@Override
	public Double getValorUnitario() {

		return valorUnitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorUnitario(java.lang.Double)
	 */
	@Override
	public void setValorUnitario(Double valorUnitario) {

		this.valorUnitario = valorUnitario;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorUnitarioTributavel()
	 */
	@Override
	public Double getValorUnitarioTributavel() {

		return valorUnitarioTributavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setValorUnitarioTributavel(java.lang.Double
	 * )
	 */
	@Override
	public void setValorUnitarioTributavel(Double valorUnitarioTributavel) {

		this.valorUnitarioTributavel = valorUnitarioTributavel;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getCodigoListaServico()
	 */
	@Override
	public String getCodigoListaServico() {

		return codigoListaServico;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setCodigoListaServico(java.lang.String)
	 */
	@Override
	public void setCodigoListaServico(String codigoListaServico) {

		this.codigoListaServico = codigoListaServico;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getBaseCalculoImpostoImportacao()
	 */
	@Override
	public Double getBaseCalculoImpostoImportacao() {

		return baseCalculoImpostoImportacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setBaseCalculoImpostoImportacao(java.lang.
	 * Double)
	 */
	@Override
	public void setBaseCalculoImpostoImportacao(Double baseCalculoImpostoImportacao) {

		this.baseCalculoImpostoImportacao = baseCalculoImpostoImportacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorDespesasAduaneiras()
	 */
	@Override
	public Double getValorDespesasAduaneiras() {

		return valorDespesasAduaneiras;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setValorDespesasAduaneiras(java.lang.Double
	 * )
	 */
	@Override
	public void setValorDespesasAduaneiras(Double valorDespesasAduaneiras) {

		this.valorDespesasAduaneiras = valorDespesasAduaneiras;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorImpostoImportacao()
	 */
	@Override
	public Double getValorImpostoImportacao() {

		return valorImpostoImportacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setValorImpostoImportacao(java.lang.Double)
	 */
	@Override
	public void setValorImpostoImportacao(Double valorImpostoImportacao) {

		this.valorImpostoImportacao = valorImpostoImportacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorIof()
	 */
	@Override
	public Double getValorIof() {

		return valorIof;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorIof(java.lang.Double)
	 */
	@Override
	public void setValorIof(Double valorIof) {

		this.valorIof = valorIof;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getCodigoEanTrib()
	 */
	@Override
	public String getCodigoEanTrib() {

		return codigoEanTrib;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setCodigoEanTrib(java.lang.Double)
	 */
	@Override
	public void setCodigoEanTrib(String codigoEanTrib) {

		this.codigoEanTrib = codigoEanTrib;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getBaseCalculoPISSubstituto()
	 */
	@Override
	public Double getBaseCalculoPISSubstituto() {

		return baseCalculoPISSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setBaseCalculoPISSubstituto(java.lang.Double
	 * )
	 */
	@Override
	public void setBaseCalculoPISSubstituto(Double baseCalculoPISSubstituto) {

		this.baseCalculoPISSubstituto = baseCalculoPISSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorPaliqPISSubstituto()
	 */
	@Override
	public Double getValorPaliqPISSubstituto() {

		return valorPaliqPISSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setValorPaliqPISSubstituto(java.lang.Double
	 * )
	 */
	@Override
	public void setValorPaliqPISSubstituto(Double valorPaliqPISSubstituto) {

		this.valorPaliqPISSubstituto = valorPaliqPISSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getQuantidadePISSubstituto()
	 */
	@Override
	public Double getQuantidadePISSubstituto() {

		return quantidadePISSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setQuantidadePISSubstituto(java.lang.Double
	 * )
	 */
	@Override
	public void setQuantidadePISSubstituto(Double quantidadePISSubstituto) {

		this.quantidadePISSubstituto = quantidadePISSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorPISSubstituto()
	 */
	@Override
	public Double getValorPISSubstituto() {

		return valorPISSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorPISSubstituto(java.lang.Double)
	 */
	@Override
	public void setValorPISSubstituto(Double valorPISSubstituto) {

		this.valorPISSubstituto = valorPISSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getBaseCalculoCOFINSSubstituto()
	 */
	@Override
	public Double getBaseCalculoCOFINSSubstituto() {

		return baseCalculoCOFINSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setBaseCalculoCOFINSSubstituto(java.lang.Double
	 * )
	 */
	@Override
	public void setBaseCalculoCOFINSSubstituto(Double baseCalculoCOFINSSubstituto) {

		this.baseCalculoCOFINSSubstituto = baseCalculoCOFINSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorPaliqCOFINSSubstituto()
	 */
	@Override
	public Double getValorPaliqCOFINSSubstituto() {

		return valorPaliqCOFINSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setValorPaliqCOFINSSubstituto(java.lang.Double
	 * )
	 */
	@Override
	public void setValorPaliqCOFINSSubstituto(Double valorPaliqCOFINSSubstituto) {

		this.valorPaliqCOFINSSubstituto = valorPaliqCOFINSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getQuantidadeCOFINSSubstituto()
	 */
	@Override
	public Double getQuantidadeCOFINSSubstituto() {

		return quantidadeCOFINSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setQuantidadeCOFINSSubstituto(java.lang.Double
	 * )
	 */
	@Override
	public void setQuantidadeCOFINSSubstituto(Double quantidadeCOFINSSubstituto) {

		this.quantidadeCOFINSSubstituto = quantidadeCOFINSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorAliqCOFINSSubstituto()
	 */
	@Override
	public Double getValorAliqCOFINSSubstituto() {

		return valorAliqCOFINSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setValorAliqCOFINSSubstituto(java.lang.Double
	 * )
	 */
	@Override
	public void setValorAliqCOFINSSubstituto(Double valorAliqCOFINSSubstituto) {

		this.valorAliqCOFINSSubstituto = valorAliqCOFINSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorCOFINSSubstituto()
	 */
	@Override
	public Double getValorCOFINSSubstituto() {

		return valorCOFINSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #setValorCOFINSSubstituto(java.lang.Double)
	 */
	@Override
	public void setValorCOFINSSubstituto(Double valorCOFINSSubstituto) {

		this.valorCOFINSSubstituto = valorCOFINSSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #getValorRaliqPISSubstituto()
	 */
	@Override
	public Double getValorRaliqPISSubstituto() {

		return valorRaliqPISSubstituto;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #
	 * setValorRaliqPISSubstituto(java.lang.Double
	 * )
	 */
	@Override
	public void setValorRaliqPISSubstituto(Double valorRaliqPISSubstituto) {

		this.valorRaliqPISSubstituto = valorRaliqPISSubstituto;
	}

	/**
	 * @return the valorOutros
	 */
	@Override
	public Double getValorOutros() {

		return valorOutros;
	}

	/**
	 * @param valorOutros
	 *            the valorOutros to set
	 */
	@Override
	public void setValorOutros(Double valorOutros) {

		this.valorOutros = valorOutros;
	}

	/**
	 * @return the indicadorTotalizacao
	 */
	@Override
	public Integer getIndicadorTotalizacao() {

		return indicadorTotalizacao;
	}

	/**
	 * @param indicadorTotalizacao
	 *            the indicadorTotalizacao to set
	 */
	@Override
	public void setIndicadorTotalizacao(Integer indicadorTotalizacao) {

		this.indicadorTotalizacao = indicadorTotalizacao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.notafiscaleletronica.impl.NfeItem
	 * #validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public void setNfe(Nfe nfe) {

		this.nfe = nfe;
	}

	@Override
	public Nfe getNfe() {

		return nfe;
	}

	@Override
	public Double getValorICMSDesonerado() {

		return valorICMSDesonerado;
	}

	@Override
	public void setValorICMSDesonerado(Double valorICMSDesonerado) {

		this.valorICMSDesonerado = valorICMSDesonerado;
	}

	@Override
	public Double getValorICMSOperacao() {

		return valorICMSOperacao;
	}

	@Override
	public void setValorICMSOperacao(Double valorICMSOperacao) {

		this.valorICMSOperacao = valorICMSOperacao;
	}

	@Override
	public Double getPercentualDiferimentoICMS() {

		return percentualDiferimentoICMS;
	}

	@Override
	public void setPercentualDiferimentoICMS(Double percentualDiferimentoICMS) {

		this.percentualDiferimentoICMS = percentualDiferimentoICMS;
	}

	@Override
	public Double getValorICMSDiferido() {

		return valorICMSDiferido;
	}

	@Override
	public void setValorICMSDiferido(Double valorICMSDiferido) {

		this.valorICMSDiferido = valorICMSDiferido;
	}

	@Override
	public Integer getMotivoDesoneracaoICMS() {

		return motivoDesoneracaoICMS;
	}

	@Override
	public void setMotivoDesoneracaoICMS(Integer motivoDesoneracaoICMS) {

		this.motivoDesoneracaoICMS = motivoDesoneracaoICMS;
	}

	@Override
	public String getNumeroRECOPI() {

		return numeroRECOPI;
	}

	@Override
	public void setNumeroRECOPI(String numeroRECOPI) {

		this.numeroRECOPI = numeroRECOPI;
	}

	@Override
	public Double getValorDeducaoISSQN() {

		return valorDeducaoISSQN;
	}

	@Override
	public void setValorDeducaoISSQN(Double valorDeducaoISSQN) {

		this.valorDeducaoISSQN = valorDeducaoISSQN;
	}

	@Override
	public Double getValorOutrasISSQN() {

		return valorOutrasISSQN;
	}

	@Override
	public void setValorOutrasISSQN(Double valorOutrasISSQN) {

		this.valorOutrasISSQN = valorOutrasISSQN;
	}

	@Override
	public Double getValorDescontoIncondicionadoISSQN() {

		return valorDescontoIncondicionadoISSQN;
	}

	@Override
	public void setValorDescontoIncondicionadoISSQN(Double valorDescontoIncondicionadoISSQN) {

		this.valorDescontoIncondicionadoISSQN = valorDescontoIncondicionadoISSQN;
	}

	@Override
	public Double getValorDescontoCondicionadoISSQN() {

		return valorDescontoCondicionadoISSQN;
	}

	@Override
	public void setValorDescontoCondicionadoISSQN(Double valorDescontoCondicionadoISSQN) {

		this.valorDescontoCondicionadoISSQN = valorDescontoCondicionadoISSQN;
	}

	@Override
	public Double getValorISSRetido() {

		return valorISSRetido;
	}

	@Override
	public void setValorISSRetido(Double valorISSRetido) {

		this.valorISSRetido = valorISSRetido;
	}

	@Override
	public Integer getIndicadorExigibilidadeISSQN() {

		return indicadorExigibilidadeISSQN;
	}

	@Override
	public void setIndicadorExigibilidadeISSQN(Integer indicadorExigibilidadeISSQN) {

		this.indicadorExigibilidadeISSQN = indicadorExigibilidadeISSQN;
	}

	@Override
	public String getCodigoServico() {

		return codigoServico;
	}

	@Override
	public void setCodigoServico(String codigoServico) {

		this.codigoServico = codigoServico;
	}

	@Override
	public String getCodigoMunicipioServico() {

		return codigoMunicipioServico;
	}

	@Override
	public void setCodigoMunicipioServico(String codigoMunicipioServico) {

		this.codigoMunicipioServico = codigoMunicipioServico;
	}

	@Override
	public String getCodigoPaisServico() {

		return codigoPaisServico;
	}

	@Override
	public void setCodigoPaisServico(String codigoPaisServico) {

		this.codigoPaisServico = codigoPaisServico;
	}

	@Override
	public String getNumeroProcessoExigibilidadeISSQN() {

		return numeroProcessoExigibilidadeISSQN;
	}

	@Override
	public void setNumeroProcessoExigibilidadeISSQN(String numeroProcessoExigibilidadeISSQN) {

		this.numeroProcessoExigibilidadeISSQN = numeroProcessoExigibilidadeISSQN;
	}

	@Override
	public Integer getIndicadorIncentivoFiscalISSQN() {

		return indicadorIncentivoFiscalISSQN;
	}

	@Override
	public void setIndicadorIncentivoFiscalISSQN(Integer indicadorIncentivoFiscalISSQN) {

		this.indicadorIncentivoFiscalISSQN = indicadorIncentivoFiscalISSQN;
	}

	@Override
	public String getCodigoSituacaoTributariaISSQN() {

		return codigoSituacaoTributariaISSQN;
	}

	@Override
	public void setCodigoSituacaoTributariaISSQN(String codigoSituacaoTributariaISSQN) {

		this.codigoSituacaoTributariaISSQN = codigoSituacaoTributariaISSQN;
	}

	@Override
	public Double getValorTotalImpostos() {

		return valorTotalImpostos;
	}

	@Override
	public void setValorTotalImpostos(Double valorTotalImpostos) {

		this.valorTotalImpostos = valorTotalImpostos;
	}

	@Override
	public String getNumeroPedidoCompra() {

		return numeroPedidoCompra;
	}

	@Override
	public void setNumeroPedidoCompra(String numeroPedidoCompra) {

		this.numeroPedidoCompra = numeroPedidoCompra;
	}

	@Override
	public Integer getNumeroItemPedidoCompra() {

		return numeroItemPedidoCompra;
	}

	@Override
	public void setNumeroItemPedidoCompra(Integer numeroItemPedidoCompra) {

		this.numeroItemPedidoCompra = numeroItemPedidoCompra;
	}

	@Override
	public String getCodigoFCI() {

		return codigoFCI;
	}

	@Override
	public void setCodigoFCI(String codigoFCI) {

		this.codigoFCI = codigoFCI;
	}
	
	@Override
	public String getCodigoCEST() {

		return codigoCEST;
	}

	@Override
	public void setCodigoCEST(String codigoCEST) {

		this.codigoCEST = codigoCEST;
	}
	
}
