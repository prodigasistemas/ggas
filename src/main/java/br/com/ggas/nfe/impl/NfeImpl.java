/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.nfe.Nfe;
import br.com.ggas.nfe.NfeChaveReferenciada;
import br.com.ggas.nfe.NfeEmpresa;
import br.com.ggas.nfe.NfeItem;
import br.com.ggas.nfe.NfeUsuario;

public class NfeImpl extends EntidadeNegocioImpl implements Nfe {

	private static final long serialVersionUID = 7724144044047529256L;

	private NfeEmpresa nfeEmpresa;

	private String indFormaEmissao;

	private Integer indDanfeImpressa;

	private String naturezaOperacao;

	private Integer formaPagamento;

	private Integer modeloDocumentoFiscal;

	private Integer serie;

	private Integer documentoFiscal;

	private Date emissao;

	private Date entradaSaida;

	private Integer indEntradaSaida;

	private String cnpj;

	private String razaoSocial;

	private String nomeFantasia;

	private String numeroPais;

	private String pais;

	private String inscricaoEstadual;

	private String inscricaoMunicipal;

	private Double valorProduto;

	private Double valorFrete;

	private Double valorSeguro;

	private Double valorDesconto;

	private Double valorIpi;

	private Double valorOutros;

	private Double valorNotaFiscal;

	private Double valorRetPis;

	private Double valorRetCofins;

	private Double valorRetCsll;

	private Double baseCalculoIrrf;

	private Double valorIrrf;

	private Double baseCalculoRetPrev;

	private Double valorRetPrev;

	private String infoFisco;

	private String infoContribuinte;

	private String numeroPaisDestino;

	private String paisDestino;

	private String cnae;

	private String descricaoEmpenho;

	private String descricaoContrato;

	private Integer indCancelamento;

	private Integer indFinEm;

	private Integer indProcessoNotaFiscal;

	private String indFmtDanfe;

	private NfeUsuario nfeUsuario;

	private Integer indEmitContigencia;

	private Integer indNfAutorizado;

	private Date dataEntradaSistema;

	private String sisOperacao;

	private String sisStatus;

	private Date sisDataStatus;

	private Integer codRegimeTribEmitente;

	private Collection<NfeChaveReferenciada> listaNfeChaveReferenciada;

	private Collection<NfeItem> listaNfeItem = new HashSet<NfeItem>();

	private Date dataCancelamento;

	private String justificativaCancelamento;

	private Usuario usuarioCancelamento;

	private Usuario usuarioContigencia;

	private String numeroIbgeUf;

	private String numeroIbgeMunicipio;

	private String numeroIbgeMunicipioDestino;

	private String numeroIbgeMunicipioFg;

	private String nomeLogradouro;

	private String complementoLogradouro;

	private String numeroLogradouro;

	private String telefone;

	private String descricaoBairro;

	private String descricaoMunicipio;

	private String descricaoUf;

	private Double baseCalculoIcms;

	private Double valorIcms;

	private Double baseCalculoIcmsSubst;

	private Double valorIcmsSubst;

	private Double valorPis;

	private Double valorCofins;

	private String destino;

	private String logradouroDestino;

	private String numeroLogradouroDestino;

	private String complementoDestino;

	private String bairroDestino;

	private String municipioDestino;

	private String ufDestino;

	private String cepDestino;

	private String cnpjCpfDestino;

	private String telefoneDestino;

	private String inscricaoEstadualDestino;

	private String numeroMensagemC;

	private String numeroMensagemE;

	private Integer indAmbienteSistema;

	private String versaoXml;

	private Long codigoLote;

	private String chaveAcesso;

	private Integer impressaContingencia;

	private byte[] danfePdf;

	private byte[] dsArquivoXml;

	private String cep;

	private Integer indPresenca;

	private Integer indConsumidorFinal;

	private Integer indIEDestinatario;

	private Double valorIcmsDesonerado;

	private Integer indLocalDestinoOperacao;
	
	private String numeroProtocoloEnvio;

	@Override
	public String getNumeroProtocoloEnvio() {
		return numeroProtocoloEnvio;
	}
	@Override
	public void setNumeroProtocoloEnvio(String numeroProtocoloEnvio) {
		this.numeroProtocoloEnvio = numeroProtocoloEnvio;
	}

	@Override
	public NfeEmpresa getNfeEmpresa() {

		return nfeEmpresa;
	}

	@Override
	public void setNfeEmpresa(NfeEmpresa nfeEmpresa) {

		this.nfeEmpresa = nfeEmpresa;
	}

	@Override
	public Integer getIndDanfeImpressa() {

		return indDanfeImpressa;
	}

	@Override
	public void setIndDanfeImpressa(Integer indDanfeImpressa) {

		this.indDanfeImpressa = indDanfeImpressa;
	}

	@Override
	public String getNaturezaOperacao() {

		return naturezaOperacao;
	}

	@Override
	public void setNaturezaOperacao(String naturezaOperacao) {

		this.naturezaOperacao = naturezaOperacao;
	}

	@Override
	public Integer getFormaPagamento() {

		return formaPagamento;
	}

	@Override
	public void setFormaPagamento(Integer formaPagamento) {

		this.formaPagamento = formaPagamento;
	}

	@Override
	public Integer getModeloDocumentoFiscal() {

		return modeloDocumentoFiscal;
	}

	@Override
	public void setModeloDocumentoFiscal(Integer modeloDocumentoFiscal) {

		this.modeloDocumentoFiscal = modeloDocumentoFiscal;
	}

	@Override
	public Integer getSerie() {

		return serie;
	}

	@Override
	public void setSerie(Integer serie) {

		this.serie = serie;
	}

	@Override
	public Integer getDocumentoFiscal() {

		return documentoFiscal;
	}

	@Override
	public void setDocumentoFiscal(Integer documentoFiscal) {

		this.documentoFiscal = documentoFiscal;
	}

	@Override
	public Date getEmissao() {
		Date data = null;
		if (this.emissao != null) {
			data = (Date) emissao.clone();
		}
		return data;
	}

	@Override
	public void setEmissao(Date emissao) {
		if (emissao != null) {
			this.emissao = (Date) emissao.clone();
		} else {
			this.emissao = null;
		}
	}

	@Override
	public Integer getIndEntradaSaida() {

		return indEntradaSaida;
	}

	@Override
	public void setIndEntradaSaida(Integer indEntradaSaida) {

		this.indEntradaSaida = indEntradaSaida;
	}

	@Override
	public String getCnpj() {

		return cnpj;
	}

	@Override
	public void setCnpj(String cnpj) {

		this.cnpj = cnpj;
	}

	@Override
	public String getRazaoSocial() {

		return razaoSocial;
	}

	@Override
	public void setRazaoSocial(String razaoSocial) {

		this.razaoSocial = razaoSocial;
	}

	@Override
	public String getNomeFantasia() {

		return nomeFantasia;
	}

	@Override
	public void setNomeFantasia(String nomeFantasia) {

		this.nomeFantasia = nomeFantasia;
	}

	@Override
	public String getNumeroPais() {

		return numeroPais;
	}

	@Override
	public void setNumeroPais(String numeroPais) {

		this.numeroPais = numeroPais;
	}

	@Override
	public String getPais() {

		return pais;
	}

	@Override
	public void setPais(String pais) {

		this.pais = pais;
	}

	@Override
	public String getInscricaoEstadual() {

		return inscricaoEstadual;
	}

	@Override
	public void setInscricaoEstadual(String inscricaoEstadual) {

		this.inscricaoEstadual = inscricaoEstadual;
	}

	@Override
	public String getInscricaoMunicipal() {

		return inscricaoMunicipal;
	}

	@Override
	public void setInscricaoMunicipal(String inscricaoMunicipal) {

		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	@Override
	public Double getValorProduto() {

		return valorProduto;
	}

	@Override
	public void setValorProduto(Double valorProduto) {

		this.valorProduto = valorProduto;
	}

	@Override
	public Double getValorFrete() {

		return valorFrete;
	}

	@Override
	public void setValorFrete(Double valorFrete) {

		this.valorFrete = valorFrete;
	}

	@Override
	public Double getValorSeguro() {

		return valorSeguro;
	}

	@Override
	public void setValorSeguro(Double valorSeguro) {

		this.valorSeguro = valorSeguro;
	}

	@Override
	public Double getValorDesconto() {

		return valorDesconto;
	}

	@Override
	public void setValorDesconto(Double valorDesconto) {

		this.valorDesconto = valorDesconto;
	}

	@Override
	public Double getValorIpi() {

		return valorIpi;
	}

	@Override
	public void setValorIpi(Double valorIpi) {

		this.valorIpi = valorIpi;
	}

	@Override
	public Double getValorOutros() {

		return valorOutros;
	}

	@Override
	public void setValorOutros(Double valorOutros) {

		this.valorOutros = valorOutros;
	}

	@Override
	public Double getValorNotaFiscal() {

		return valorNotaFiscal;
	}

	@Override
	public void setValorNotaFiscal(Double valorNotaFiscal) {

		this.valorNotaFiscal = valorNotaFiscal;
	}

	@Override
	public Double getValorRetPis() {

		return valorRetPis;
	}

	@Override
	public void setValorRetPis(Double valorRetPis) {

		this.valorRetPis = valorRetPis;
	}

	@Override
	public Double getValorRetCofins() {

		return valorRetCofins;
	}

	@Override
	public void setValorRetCofins(Double valorRetCofins) {

		this.valorRetCofins = valorRetCofins;
	}

	@Override
	public Double getValorRetCsll() {

		return valorRetCsll;
	}

	@Override
	public void setValorRetCsll(Double valorRetCsll) {

		this.valorRetCsll = valorRetCsll;
	}

	@Override
	public Double getBaseCalculoIrrf() {

		return baseCalculoIrrf;
	}

	@Override
	public void setBaseCalculoIrrf(Double baseCalculoIrrf) {

		this.baseCalculoIrrf = baseCalculoIrrf;
	}

	@Override
	public Double getValorIrrf() {

		return valorIrrf;
	}

	@Override
	public void setValorIrrf(Double valorIrrf) {

		this.valorIrrf = valorIrrf;
	}

	@Override
	public Double getBaseCalculoRetPrev() {

		return baseCalculoRetPrev;
	}

	@Override
	public void setBaseCalculoRetPrev(Double baseCalculoRetPrev) {

		this.baseCalculoRetPrev = baseCalculoRetPrev;
	}

	@Override
	public Double getValorRetPrev() {

		return valorRetPrev;
	}

	@Override
	public void setValorRetPrev(Double valorRetPrev) {

		this.valorRetPrev = valorRetPrev;
	}

	@Override
	public String getInfoFisco() {

		return infoFisco;
	}

	@Override
	public void setInfoFisco(String infoFisco) {

		this.infoFisco = infoFisco;
	}

	@Override
	public String getInfoContribuinte() {

		return infoContribuinte;
	}

	@Override
	public void setInfoContribuinte(String infoContribuinte) {

		this.infoContribuinte = infoContribuinte;
	}

	@Override
	public String getNumeroPaisDestino() {

		return numeroPaisDestino;
	}

	@Override
	public void setNumeroPaisDestino(String numeroPaisDestino) {

		this.numeroPaisDestino = numeroPaisDestino;
	}

	@Override
	public String getPaisDestino() {

		return paisDestino;
	}

	@Override
	public void setPaisDestino(String paisDestino) {

		this.paisDestino = paisDestino;
	}

	@Override
	public String getCnae() {

		return cnae;
	}

	@Override
	public void setCnae(String cnae) {

		this.cnae = cnae;
	}

	@Override
	public String getDescricaoEmpenho() {

		return descricaoEmpenho;
	}

	@Override
	public void setDescricaoEmpenho(String descricaoEmpenho) {

		this.descricaoEmpenho = descricaoEmpenho;
	}

	@Override
	public String getDescricaoContrato() {

		return descricaoContrato;
	}

	@Override
	public void setDescricaoContrato(String descricaoContrato) {

		this.descricaoContrato = descricaoContrato;
	}

	@Override
	public Integer getIndCancelamento() {

		return indCancelamento;
	}

	@Override
	public void setIndCancelamento(Integer indCancelamento) {

		this.indCancelamento = indCancelamento;
	}

	@Override
	public Integer getIndFinEm() {

		return indFinEm;
	}

	@Override
	public void setIndFinEm(Integer indFinEm) {

		this.indFinEm = indFinEm;
	}

	@Override
	public Integer getIndProcessoNotaFiscal() {

		return indProcessoNotaFiscal;
	}

	@Override
	public void setIndProcessoNotaFiscal(Integer indProcessoNotaFiscal) {

		this.indProcessoNotaFiscal = indProcessoNotaFiscal;
	}

	@Override
	public String getIndFmtDanfe() {

		return indFmtDanfe;
	}

	@Override
	public void setIndFmtDanfe(String indFmtDanfe) {

		this.indFmtDanfe = indFmtDanfe;
	}

	@Override
	public Integer getIndEmitContigencia() {

		return indEmitContigencia;
	}

	@Override
	public void setIndEmitContigencia(Integer indEmitContigencia) {

		this.indEmitContigencia = indEmitContigencia;
	}

	@Override
	public Integer getIndNfAutorizado() {

		return indNfAutorizado;
	}

	@Override
	public void setIndNfAutorizado(Integer indNfAutorizado) {

		this.indNfAutorizado = indNfAutorizado;
	}

	@Override
	public Date getDataEntradaSistema() {
		Date data = null;
		if (this.dataEntradaSistema != null) {
			data = (Date) dataEntradaSistema.clone();
		}
		return data;
	}

	@Override
	public void setDataEntradaSistema(Date dataEntradaSistema) {
		if (dataEntradaSistema != null) {
			this.dataEntradaSistema = (Date) dataEntradaSistema.clone();
		} else {
			this.dataEntradaSistema = null;
		}
	}

	@Override
	public String getSisOperacao() {

		return sisOperacao;
	}

	@Override
	public void setSisOperacao(String sisOperacao) {

		this.sisOperacao = sisOperacao;
	}

	@Override
	public String getSisStatus() {

		return sisStatus;
	}

	@Override
	public void setSisStatus(String sisStatus) {

		this.sisStatus = sisStatus;
	}

	@Override
	public Date getSisDataStatus() {
		Date data = null;
		if (this.sisDataStatus != null) {
			data = (Date) sisDataStatus.clone();
		}
		return data;
	}

	@Override
	public void setSisDataStatus(Date sisDataStatus) {
		if (sisDataStatus != null) {
			this.sisDataStatus = (Date) sisDataStatus.clone();
		} else {
			this.sisDataStatus = null;
		}
	}

	@Override
	public Integer getCodRegimeTribEmitente() {

		return codRegimeTribEmitente;
	}

	@Override
	public void setCodRegimeTribEmitente(Integer codRegimeTribEmitente) {

		this.codRegimeTribEmitente = codRegimeTribEmitente;
	}

	@Override
	public Collection<NfeChaveReferenciada> getListaNfeChaveReferenciada() {

		return listaNfeChaveReferenciada;
	}

	@Override
	public void setListaNfeChaveReferenciada(Collection<NfeChaveReferenciada> listaNfeChaveReferenciada) {

		this.listaNfeChaveReferenciada = listaNfeChaveReferenciada;
	}

	@Override
	public Collection<NfeItem> getListaNfeItem() {

		return listaNfeItem;
	}

	@Override
	public void setListaNfeItem(Collection<NfeItem> listaNfeItem) {

		this.listaNfeItem = listaNfeItem;
	}

	public static long getSerialversionuid() {

		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

	@Override
	public void setNfeUsuario(NfeUsuario nfeUsuario) {

		this.nfeUsuario = nfeUsuario;
	}

	@Override
	public NfeUsuario getNfeUsuario() {

		return nfeUsuario;
	}

	/**
	 * @return the dataCancelamento
	 */
	@Override
	public Date getDataCancelamento() {
		Date data = null;
		if (this.dataCancelamento != null) {
			data = (Date) dataCancelamento.clone();
		}
		return data;
	}

	/**
	 * @param dataCancelamento
	 *            the dataCancelamento to set
	 */
	@Override
	public void setDataCancelamento(Date dataCancelamento) {
		if (dataCancelamento != null) {
			this.dataCancelamento = (Date) dataCancelamento.clone();
		} else {
			this.dataCancelamento = null;
		}
	}

	/**
	 * @return the justificativaCancelamento
	 */
	@Override
	public String getJustificativaCancelamento() {

		return justificativaCancelamento;
	}

	/**
	 * @param justificativaCancelamento
	 *            the justificativaCancelamento to set
	 */
	@Override
	public void setJustificativaCancelamento(String justificativaCancelamento) {

		this.justificativaCancelamento = justificativaCancelamento;
	}

	/**
	 * @return the usuarioCancelamento
	 */
	@Override
	public Usuario getUsuarioCancelamento() {

		return usuarioCancelamento;
	}

	/**
	 * @param usuarioCancelamento
	 *            the usuarioCancelamento to set
	 */
	@Override
	public void setUsuarioCancelamento(Usuario usuarioCancelamento) {

		this.usuarioCancelamento = usuarioCancelamento;
	}

	@Override
	public void setNumeroIbgeUf(String numeroIbgeUf) {

		this.numeroIbgeUf = numeroIbgeUf;
	}

	@Override
	public String getNumeroIbgeUf() {

		return numeroIbgeUf;
	}

	@Override
	public void setNumeroIbgeMunicipio(String numeroIbgeMunicipio) {

		this.numeroIbgeMunicipio = numeroIbgeMunicipio;
	}

	@Override
	public String getNumeroIbgeMunicipio() {

		return numeroIbgeMunicipio;
	}

	@Override
	public void setNumeroIbgeMunicipioDestino(String numeroIbgeMunicipioDestino) {

		this.numeroIbgeMunicipioDestino = numeroIbgeMunicipioDestino;
	}

	@Override
	public String getNumeroIbgeMunicipioDestino() {

		return numeroIbgeMunicipioDestino;
	}

	@Override
	public void setNumeroIbgeMunicipioFg(String numeroIbgeMunicipioFg) {

		this.numeroIbgeMunicipioFg = numeroIbgeMunicipioFg;
	}

	@Override
	public String getNumeroIbgeMunicipioFg() {

		return numeroIbgeMunicipioFg;
	}

	@Override
	public void setNomeLogradouro(String nomeLogradouro) {

		this.nomeLogradouro = nomeLogradouro;
	}

	@Override
	public String getNomeLogradouro() {

		return nomeLogradouro;
	}

	@Override
	public void setNumeroLogradouro(String numeroLogradouro) {

		this.numeroLogradouro = numeroLogradouro;
	}

	@Override
	public String getNumeroLogradouro() {

		return numeroLogradouro;
	}

	@Override
	public void setDescricaoBairro(String descricaoBairro) {

		this.descricaoBairro = descricaoBairro;
	}

	@Override
	public String getDescricaoBairro() {

		return descricaoBairro;
	}

	@Override
	public void setDescricaoMunicipio(String descricaoMunicipio) {

		this.descricaoMunicipio = descricaoMunicipio;
	}

	@Override
	public String getDescricaoMunicipio() {

		return descricaoMunicipio;
	}

	@Override
	public void setDescricaoUf(String descricaoUf) {

		this.descricaoUf = descricaoUf;
	}

	@Override
	public String getDescricaoUf() {

		return descricaoUf;
	}

	@Override
	public void setBaseCalculoIcms(Double baseCalculoIcms) {

		this.baseCalculoIcms = baseCalculoIcms;
	}

	@Override
	public Double getBaseCalculoIcms() {

		return baseCalculoIcms;
	}

	@Override
	public void setValorIcms(Double valorIcms) {

		this.valorIcms = valorIcms;
	}

	@Override
	public Double getValorIcms() {

		return valorIcms;
	}

	@Override
	public void setBaseCalculoIcmsSubst(Double baseCalculoIcmsSubst) {

		this.baseCalculoIcmsSubst = baseCalculoIcmsSubst;
	}

	@Override
	public Double getBaseCalculoIcmsSubst() {

		return baseCalculoIcmsSubst;
	}

	@Override
	public void setValorIcmsSubst(Double valorIcmsSubst) {

		this.valorIcmsSubst = valorIcmsSubst;
	}

	@Override
	public Double getValorIcmsSubst() {

		return valorIcmsSubst;
	}

	@Override
	public void setValorPis(Double valorPis) {

		this.valorPis = valorPis;
	}

	@Override
	public Double getValorPis() {

		return valorPis;
	}

	@Override
	public void setValorCofins(Double valorCofins) {

		this.valorCofins = valorCofins;
	}

	@Override
	public Double getValorCofins() {

		return valorCofins;
	}

	@Override
	public void setDestino(String destino) {

		this.destino = destino;
	}

	@Override
	public String getDestino() {

		return destino;
	}

	@Override
	public void setLogradouroDestino(String logradouroDestino) {

		this.logradouroDestino = logradouroDestino;
	}

	@Override
	public String getLogradouroDestino() {

		return logradouroDestino;
	}

	@Override
	public void setNumeroLogradouroDestino(String numeroLogradouroDestino) {

		this.numeroLogradouroDestino = numeroLogradouroDestino;
	}

	@Override
	public String getNumeroLogradouroDestino() {

		return numeroLogradouroDestino;
	}

	@Override
	public void setComplementoDestino(String complementoDestino) {

		this.complementoDestino = complementoDestino;
	}

	@Override
	public String getComplementoDestino() {

		return complementoDestino;
	}

	@Override
	public void setBairroDestino(String bairroDestino) {

		this.bairroDestino = bairroDestino;
	}

	@Override
	public String getBairroDestino() {

		return bairroDestino;
	}

	@Override
	public void setMunicipioDestino(String municipioDestino) {

		this.municipioDestino = municipioDestino;
	}

	@Override
	public String getMunicipioDestino() {

		return municipioDestino;
	}

	@Override
	public void setUfDestino(String ufDestino) {

		this.ufDestino = ufDestino;
	}

	@Override
	public String getUfDestino() {

		return ufDestino;
	}

	@Override
	public void setCepDestino(String cepDestino) {

		this.cepDestino = cepDestino;
	}

	@Override
	public String getCepDestino() {

		return cepDestino;
	}

	@Override
	public String getNumeroMensagemC() {

		return numeroMensagemC;
	}

	@Override
	public void setNumeroMensagemC(String numeroMensagemC) {

		this.numeroMensagemC = numeroMensagemC;
	}

	@Override
	public String getNumeroMensagemE() {

		return numeroMensagemE;
	}

	@Override
	public void setNumeroMensagemE(String numeroMensagemE) {

		this.numeroMensagemE = numeroMensagemE;
	}

	@Override
	public void setCnpjCpfDestino(String cnpjCpfDestino) {

		this.cnpjCpfDestino = cnpjCpfDestino;
	}

	@Override
	public String getCnpjCpfDestino() {

		return cnpjCpfDestino;
	}

	@Override
	public void setTelefoneDestino(String telefoneDestino) {

		this.telefoneDestino = telefoneDestino;
	}

	@Override
	public String getTelefoneDestino() {

		return telefoneDestino;
	}

	@Override
	public void setInscricaoEstadualDestino(String inscricaoEstadualDestino) {

		this.inscricaoEstadualDestino = inscricaoEstadualDestino;
	}

	@Override
	public String getInscricaoEstadualDestino() {

		return inscricaoEstadualDestino;
	}

	@Override
	public void setIndFormaEmissao(String indFormaEmissao) {

		this.indFormaEmissao = indFormaEmissao;
	}

	@Override
	public String getIndFormaEmissao() {

		return indFormaEmissao;
	}

	@Override
	public void setEntradaSaida(Date entradaSaida) {
		if (entradaSaida != null) {
			this.entradaSaida = (Date) entradaSaida.clone();
		} else {
			this.entradaSaida = null;
		}
	}

	@Override
	public Date getEntradaSaida() {
		Date data = null;
		if (this.entradaSaida != null) {
			data = (Date) entradaSaida.clone();
		}
		return data;
	}

	@Override
	public void setComplementoLogradouro(String complementoLogradouro) {

		this.complementoLogradouro = complementoLogradouro;
	}

	@Override
	public String getComplementoLogradouro() {

		return complementoLogradouro;
	}

	@Override
	public void setTelefone(String telefone) {

		this.telefone = telefone;
	}

	@Override
	public String getTelefone() {

		return telefone;
	}

	@Override
	public void setIndAmbienteSistema(Integer indAmbienteSistema) {

		this.indAmbienteSistema = indAmbienteSistema;
	}

	@Override
	public Integer getIndAmbienteSistema() {

		return indAmbienteSistema;
	}

	@Override
	public void setVersaoXml(String versaoXml) {

		this.versaoXml = versaoXml;
	}

	@Override
	public String getVersaoXml() {

		return versaoXml;
	}

	@Override
	public void setCodigoLote(Long codigoLote) {

		this.codigoLote = codigoLote;
	}

	@Override
	public Long getCodigoLote() {

		return codigoLote;
	}

	@Override
	public void setChaveAcesso(String chaveAcesso) {

		this.chaveAcesso = chaveAcesso;
	}

	@Override
	public String getChaveAcesso() {

		return chaveAcesso;
	}

	@Override
	public Integer getImpressaContingencia() {

		return impressaContingencia;
	}

	@Override
	public void setImpressaContingencia(Integer impressaContingencia) {

		this.impressaContingencia = impressaContingencia;
	}

	@Override
	public byte[] getDanfePdf() {
		byte[] retorno = null;
		if (this.danfePdf != null) {
			retorno = this.danfePdf.clone();
		}
		return retorno;
	}

	@Override
	public void setDanfePdf(byte[] danfePdf) {
		if (danfePdf != null) {
			this.danfePdf = danfePdf.clone();
		} else {
			this.danfePdf = null;
		}
	}

	@Override
	public byte[] getDsArquivoXml() {
		byte[] retorno = null;
		if (this.dsArquivoXml != null) {
			retorno = this.dsArquivoXml.clone();
		}
		return retorno;
	}

	@Override
	public void setDsArquivoXml(byte[] dsArquivoXml) {
		if (dsArquivoXml != null) {
			this.dsArquivoXml = dsArquivoXml.clone();
		} else {
			this.dsArquivoXml = null;
		}
	}

	@Override
	public String getCep() {

		return cep;
	}

	@Override
	public void setCep(String cep) {

		this.cep = cep;
	}

	@Override
	public Usuario getUsuarioContigencia() {

		return usuarioContigencia;
	}

	@Override
	public void setUsuarioContigencia(Usuario usuarioContigencia) {

		this.usuarioContigencia = usuarioContigencia;
	}

	@Override
	public Integer getIndPresenca() {

		return indPresenca;
	}

	@Override
	public void setIndPresenca(Integer indPresenca) {

		this.indPresenca = indPresenca;
	}

	@Override
	public Integer getIndConsumidorFinal() {

		return indConsumidorFinal;
	}

	@Override
	public void setIndConsumidorFinal(Integer indConsumidorFinal) {

		this.indConsumidorFinal = indConsumidorFinal;
	}

	@Override
	public Integer getIndIEDestinatario() {

		return indIEDestinatario;
	}

	@Override
	public void setIndIEDestinatario(Integer indIEDestinatario) {

		this.indIEDestinatario = indIEDestinatario;
	}

	@Override
	public Double getValorIcmsDesonerado() {

		return valorIcmsDesonerado;
	}

	@Override
	public void setValorIcmsDesonerado(Double valorIcmsDesonerado) {

		this.valorIcmsDesonerado = valorIcmsDesonerado;
	}

	@Override
	public Integer getIndLocalDestinoOperacao() {

		return indLocalDestinoOperacao;
	}

	@Override
	public void setIndLocalDestinoOperacao(Integer indLocalDestinoOperacao) {

		this.indLocalDestinoOperacao = indLocalDestinoOperacao;
	}

}
