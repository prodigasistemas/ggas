/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.nfe.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.nfe.NfePapel;
import br.com.ggas.nfe.NfeUsuario;
import br.com.ggas.util.Constantes;

/**
 * 
 *
 */
class NfeUsuarioImpl extends EntidadeNegocioImpl implements NfeUsuario {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7178167019883677266L;

	private String nome;

	private String email;

	private String login;

	private String senha;

	private Collection<NfePapel> nfePapeis = new HashSet<NfePapel>();

	private Date ultimoAcesso;

	private boolean senhaExpirada;

	private int tentativasSenhaErrada;

	private boolean habilitado = true;

	private Date ultimaAlteracao = new Date();

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * getTentativasSenhaErrada()
	 */
	@Override
	public int getTentativasSenhaErrada() {

		return tentativasSenhaErrada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * setTentativasSenhaErrada(int)
	 */
	@Override
	public void setTentativasSenhaErrada(int tentativasSenhaErrada) {

		this.tentativasSenhaErrada = tentativasSenhaErrada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * getUltimoAcesso()
	 */
	@Override
	public Date getUltimoAcesso() {

		return ultimoAcesso;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * setUltimoAcesso(java.util.Date)
	 */
	@Override
	public void setUltimoAcesso(Date ultimoAcesso) {

		this.ultimoAcesso = ultimoAcesso;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * isSenhaExpirada()
	 */
	@Override
	public boolean isSenhaExpirada() {

		return senhaExpirada;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.Usuario#
	 * setSenhaExpirada(boolean)
	 */
	@Override
	public void setSenhaExpirada(boolean senhaExpirada) {

		this.senhaExpirada = senhaExpirada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #getNome()
	 */
	@Override
	public String getNome() {

		return nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #setNome(java.lang.String)
	 */
	@Override
	public void setNome(String nome) {

		this.nome = nome;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #getEmail()
	 */
	@Override
	public String getEmail() {

		return email;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(String email) {

		this.email = email;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #getLogin()
	 */
	@Override
	public String getLogin() {

		return login;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #setLogin(java.lang.String)
	 */
	@Override
	public void setLogin(String login) {

		this.login = login;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #getSenha()
	 */
	@Override
	public String getSenha() {

		return senha;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #setSenha(java.lang.String)
	 */
	@Override
	public void setSenha(String senha) {

		this.senha = senha;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #getPapeis()
	 */
	@Override
	public Collection<NfePapel> getNfePapeis() {

		return nfePapeis;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.impl.Usuario
	 * #setPapeis(java.util.Collection)
	 */
	@Override
	public void setNfePapeis(Collection<NfePapel> nfePapeis) {

		this.nfePapeis = nfePapeis;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder sb = new StringBuilder();
		String camposObrigatorios = null;

		if(StringUtils.isEmpty(this.nome)) {
			sb.append(USUARIO_ROTULO_NOME);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(this.email)) {
			sb.append(USUARIO_ROTULO_EMAIL);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		if(StringUtils.isEmpty(this.login)) {
			sb.append(USUARIO_ROTULO_LOGIN);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}
		if(StringUtils.isEmpty(this.senha)) {
			sb.append(USUARIO_ROTULO_SENHA);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		camposObrigatorios = sb.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, sb.toString().length() - 2));
		}

		return erros;
	}

	/**
	 * @return the habilitado
	 */
	@Override
	public boolean isHabilitado() {

		return habilitado;
	}

	/**
	 * @param habilitado
	 *            the habilitado to set
	 */
	@Override
	public void setHabilitado(boolean habilitado) {

		this.habilitado = habilitado;
	}

	/**
	 * @return the ultimaAlteracao
	 */
	@Override
	public Date getUltimaAlteracao() {

		return ultimaAlteracao;
	}

	/**
	 * @param ultimaAlteracao
	 *            the ultimaAlteracao to set
	 */
	@Override
	public void setUltimaAlteracao(Date ultimaAlteracao) {

		this.ultimaAlteracao = ultimaAlteracao;
	}
}
