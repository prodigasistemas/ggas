/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe.impl;

import java.util.Date;
import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.nfe.Nfe;
import br.com.ggas.nfe.NfeChaveReferenciada;
import br.com.ggas.nfe.NfeEmpresa;
import br.com.ggas.nfe.NfeItem;

class NfeChaveReferenciadaImpl extends EntidadeNegocioImpl implements NfeChaveReferenciada {

	private static final long serialVersionUID = 1L;

	private NfeChaveReferenciada nfeChaveReferenciada;

	private NfeItem nfeItem;

	// NFCH_CD_ITEM NUMBER
	// not null,

	private String chaveAcesso;

	// NFCH_CD_CHAVE_ACESSO_REF
	// VARCHAR2(44),

	private Nfe nfe;

	// NFCH_CD_NF NUMBER not null,

	private NfeEmpresa nfeEmpresa;

	// NFCH_CD_EMPRESA
	// NUMBER(4)
	// not null,

	private Integer numeroIbgeUf;

	// NFCH_NR_IBGE_UF
	// VARCHAR2(2),

	private Date emissao;

	// NFCH_DT_EMISSAO DATE,

	private String cnpj;

	// NFCH_NR_CNPJ
	// VARCHAR2(14),

	private Integer numeroModelo;

	// NFCH_NR_MODELO
	// NUMBER(2),

	private Integer numeroSerie;

	// NFCH_NR_SERIE
	// NUMBER(3),

	private Integer numeroDocumento;

	// NFCH_NR_DOCUMENTO
	// NUMBER(9)

	@Override
	public NfeChaveReferenciada getNfeChaveReferenciada() {

		return nfeChaveReferenciada;
	}

	@Override
	public void setNfeChaveReferenciada(NfeChaveReferenciada nfeChaveReferenciada) {

		this.nfeChaveReferenciada = nfeChaveReferenciada;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada#getItem()
	 */
	public NfeItem getNfeItem() {

		return nfeItem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada
	 * #setItem(br.com.procenge.notafiscaleletronica
	 * .Item)
	 */
	public void setNfeItem(NfeItem nfeItem) {

		this.nfeItem = nfeItem;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada#getChaveAcesso()
	 */
	@Override
	public String getChaveAcesso() {

		return chaveAcesso;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada
	 * #setChaveAcesso(java.lang.String)
	 */
	@Override
	public void setChaveAcesso(String chaveAcesso) {

		this.chaveAcesso = chaveAcesso;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada#getNfe()
	 */
	@Override
	public Nfe getNfe() {

		return nfe;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada
	 * #setNfe(br.com.procenge.notafiscaleletronica
	 * .NotaFiscalEletronica)
	 */
	@Override
	public void setNfe(Nfe nfe) {

		this.nfe = nfe;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada#getEmpresa()
	 */
	@Override
	public NfeEmpresa getNfeEmpresa() {

		return nfeEmpresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada
	 * #setEmpresa(br.com.procenge
	 * .notafiscaleletronica.Empresa)
	 */
	@Override
	public void setNfeEmpresa(NfeEmpresa nfeEmpresa) {

		this.nfeEmpresa = nfeEmpresa;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada#getNumeroIbgeUf()
	 */
	@Override
	public Integer getNumeroIbgeUf() {

		return numeroIbgeUf;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada
	 * #setNumeroIbgeUf(java.lang.Integer)
	 */
	@Override
	public void setNumeroIbgeUf(Integer numeroIbgeUf) {

		this.numeroIbgeUf = numeroIbgeUf;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada#getEmissao()
	 */
	@Override
	public Date getEmissao() {

		return emissao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada
	 * #setEmissao(java.sql.Date)
	 */
	@Override
	public void setEmissao(Date emissao) {

		this.emissao = emissao;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada#getCnpj()
	 */
	@Override
	public String getCnpj() {

		return cnpj;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada
	 * #setCnpj(java.lang.String)
	 */
	@Override
	public void setCnpj(String cnpj) {

		this.cnpj = cnpj;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada#getNumeroModelo()
	 */
	@Override
	public Integer getNumeroModelo() {

		return numeroModelo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada
	 * #setNumeroModelo(java.lang.Integer)
	 */
	@Override
	public void setNumeroModelo(Integer numeroModelo) {

		this.numeroModelo = numeroModelo;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada#getNumeroSerie()
	 */
	@Override
	public Integer getNumeroSerie() {

		return numeroSerie;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada
	 * #setNumeroSerie(java.lang.Integer)
	 */
	@Override
	public void setNumeroSerie(Integer numeroSerie) {

		this.numeroSerie = numeroSerie;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada#getNumeroDocumento()
	 */
	@Override
	public Integer getNumeroDocumento() {

		return numeroDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.procenge.notafiscaleletronica.impl
	 * .ChaveReferenciada
	 * #setNumeroDocumento(java.lang.Integer)
	 */
	@Override
	public void setNumeroDocumento(Integer numeroDocumento) {

		this.numeroDocumento = numeroDocumento;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		return null;
	}

}
