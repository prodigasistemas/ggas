/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

/**
 * 
 */

package br.com.ggas.nfe.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import br.com.ggas.geral.negocio.impl.EntidadeNegocioImpl;
import br.com.ggas.nfe.NfePapel;
import br.com.ggas.nfe.NfePermissao;
import br.com.ggas.util.Constantes;

/**
 * 
 *
 */
class NfePapelImpl extends EntidadeNegocioImpl implements NfePapel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1898607474475915701L;

	private String descricao;

	private boolean admin;

	private Collection<NfePermissao> nfePermissoes = new HashSet<NfePermissao>();

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Papel#isAdmin()
	 */
	@Override
	public boolean isAdmin() {

		return admin;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * br.com.ggas.controleacesso.Papel#setAdmin
	 * (boolean)
	 */
	@Override
	public void setAdmin(boolean admin) {

		this.admin = admin;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.impl.Papel#
	 * getDescricao()
	 */
	@Override
	public String getDescricao() {

		return descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.impl.Papel#
	 * setDescricao(java.lang.String)
	 */
	@Override
	public void setDescricao(String descricao) {

		this.descricao = descricao;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.impl.Papel#
	 * getPermissoes()
	 */
	@Override
	public Collection<NfePermissao> getNfePermissoes() {

		return nfePermissoes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.controleacesso.impl.Papel#
	 * setPermissoes(java.util.Collection)
	 */
	@Override
	public void setNfePermissoes(Collection<NfePermissao> nfePermissoes) {

		this.nfePermissoes = nfePermissoes;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ggas.comum.negocio.impl.
	 * EntidadeNegocioImpl#validarDados()
	 */
	@Override
	public Map<String, Object> validarDados() {

		Map<String, Object> erros = new HashMap<String, Object>();
		StringBuilder sb = new StringBuilder();
		String camposObrigatorios = null;

		if(this.descricao == null || this.descricao.length() == 0) {
			sb.append(PAPEL_ROTULO_DESCRICAO);
			sb.append(Constantes.STRING_VIRGULA_ESPACO);
		}

		for (Iterator<NfePermissao> iterator = this.nfePermissoes.iterator(); iterator.hasNext();) {
			Map<String, Object> validarDados = (iterator.next()).validarDados();
			erros.putAll(validarDados);
		}

		camposObrigatorios = sb.toString();

		if(camposObrigatorios.length() > 0) {
			erros.put(Constantes.ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS, camposObrigatorios.substring(0, sb.toString().length() - 2));
		}

		return erros;
	}

}
