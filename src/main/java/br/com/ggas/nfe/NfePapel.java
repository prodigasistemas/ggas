/*
 Copyright (C) <2011> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás

 Este programa é um software livre; você pode redistribuí-lo e/ou
 modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 publicada pela Free Software Foundation; versão 2 da Licença.

 O GGAS é distribuído na expectativa de ser útil,
 mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 Consulte a Licença Pública Geral GNU para obter mais detalhes.

 Você deve ter recebido uma cópia da Licença Pública Geral GNU
 junto com este programa; se não, escreva para Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.


 Copyright (C) 2011-2011 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás

 This file is part of GGAS, a commercial management system for Gas Distribution Services

 GGAS is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.

 GGAS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.nfe;

import java.util.Collection;

import br.com.ggas.geral.negocio.EntidadeNegocio;

public interface NfePapel extends EntidadeNegocio {

	/**
	 * PAPEL_ROTULO_DESCRICAO
	 */
	String ENTIDADE_ROTULO_PAPEL = "ENTIDADE_ROTULO_PAPEL";

	String PAPEL_ROTULO_DESCRICAO = "PAPEL_ROTULO_DESCRICAO";

	String PAPEL_ROTULO_ADMIN = "PAPEL_ROTULO_ADMIN";

	String BEAN_ID_PAPEL = "papel";

	/**
	 * @return the descricao
	 */
	String getDescricao();

	/**
	 * @param descricao
	 *            the descricao to set
	 */
	void setDescricao(String descricao);

	/**
	 * @return the permissoes
	 */
	Collection<NfePermissao> getNfePermissoes();

	/**
	 * @param nfePermissoes Set Nfe permissões.
	 */
	void setNfePermissoes(Collection<NfePermissao> nfePermissoes);

	/**
	 * @return the admin
	 */
	boolean isAdmin();

	/**
	 * @param admin
	 *            the admin to set
	 */
	void setAdmin(boolean admin);

}
