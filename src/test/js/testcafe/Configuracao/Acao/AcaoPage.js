import { Selector, t} from 'testcafe';
import Page from '../../ggasPage';

const page = new Page();

export default class AcaoPage {

    constructor(){

        this.botaoIncluir = Selector('input#buttonIncluir');
        
        this.acaoPrecedente = Selector('#acaoPrecedente');
        this.acaoPrecedenteOptions = this.acaoPrecedente.find('option');
        
        this.descricao = Selector('textarea#descricao');

        this.modulo = Selector('#modulo');
        this.moduloOptions = this.modulo.find('option');

        this.operacao = Selector('#operacao');
        this.operacaoOptions = this.operacao.find('option');

        this.numeroDiasValidade = Selector('input#numeroDiasValidade');
        this.tipoNumeroDiasValidade = Selector('#tipoNumeroDiasValidade'); 
        this.tipoNumeroDiasValidadeOptions = this.tipoNumeroDiasValidade.find('option');

        this.numeroDiasVencimento = Selector('input#numeroDiasVencimento');
        this.tipoNumeroDiasVencimento = Selector('#tipoNumeroDiasVencimento'); 
        this.tipoNumeroDiasVencimentoOptions = this.tipoNumeroDiasVencimento.find('option');

        this.tipoDocumentoGerado = Selector('#tipoDocumento');
        this.tipoDocumentoGeradoOptions = this.tipoDocumentoGerado.find('option');

        this.ordemDeServico = Selector('#indicadorGerarAutorizacaoServico');

        this.tipoServico = Selector('#servicoTipo');
        this.tipoServicoOptions = this.tipoServico.find('option');

        this.equipe = Selector('#equipe');
        this.equipeOptions = this.equipe.find('option');

        this.consideraDebitosVencer = Selector('#considerarDebitosAVencer');

        this.bloquearDocumentosPagaveis = Selector('#bloquearDocumentosPagaveis');

        this.botaoSubmit = Selector('#buttonSalvar');

        this.chamadoAssuntoPesquisa = Selector('#chamadoAssuntoPesquisa');
        this.chamadoAssuntoPesquisaOptions = this.chamadoAssuntoPesquisa.find('option');

        this.unidadeOrganizacionalPesquisa = Selector('#unidadeOrganizacionalPesquisa');
        this.unidadeOrganizacionalPesquisaOptions = this.unidadeOrganizacionalPesquisa.find('option');

        this.servicoTipoPesquisa = Selector('#servicoTipoPesquisa');
        this.servicoTipoPesquisaOptions = this.servicoTipoPesquisa.find('option');
        
        this.questionario = Selector('#questionario');
        this.questionarioOptions = this.questionario.find('option');
        
        this.descricaoPesquisa = Selector('#descricao');

        this.tipoDocumentoPesquisa = Selector('#tipoDocumento');
        this.tipoDocumentoPesquisaOptions = this.tipoDocumentoPesquisa.find('option');

        this.buttonPesquisar = Selector('input[value=Pesquisar]');

        this.buttonLimpar = Selector('input[value=Limpar]');

        this.buttonAlterar = Selector('input#buttonAlterar');

        this.buttonRemover = Selector('input#buttonRemover');


    }

    async irParaAcao(){
        await t // navigate to url
            .navigateTo(page.url+"/exibirPesquisaAcao");
    }

    async pesquisarAcao(descricao, tipoDeDocumento){
        if(descricao){
            await this.setDescricaoPesquisa(descricao);
        }
        if(tipoDeDocumento){
            await this.setTipoDocumentoPesquisa(tipoDeDocumento);
        }
       
        await t.click(this.buttonPesquisar);
    }

    async inclusaoAcao(acaoPrecedente
        , descricao
        , modulo
        , operacao
        , numeroDiasValidade
        , tipoNumeroDiasValidade
        , numeroDiasVencimento
        , tipoNumeroDiasVencimento
        , tipoDocumentoGerado
        , ordemDeServico
        , tipoServico
        , equipe
        , consideraDebitosVencer
        , bloquearDocumentosPagaveis
        , chamadoAssuntoPesquisa
        , unidadeOrganizacionalPesquisa
        , servicoTipoPesquisa
        , questionario){

        await t.click(this.botaoIncluir);
    
        await this.setAcaoPrecedente(acaoPrecedente);
        await this.setDescricao(descricao);
        await this.setModulo(modulo);
        await this.setOperacao(operacao);

        await this.setNumeroDiasValidade(numeroDiasValidade);
        await this.setTipoNumeroDiasValidade(tipoNumeroDiasValidade);
        await this.setNumeroDiasVencimento(numeroDiasVencimento);
        await this.setTipoNumeroDiasVencimento(tipoNumeroDiasVencimento);
        await this.setTipoDocumentoGerado(tipoDocumentoGerado);
        await this.setOrdemDeServico(ordemDeServico);
        await this.setTipoServico(tipoServico);
        await this.setEquipe(equipe);
        await this.setConsideraDebitosVencer(consideraDebitosVencer);
        await this.setBloquearDocumentosPagaveis(bloquearDocumentosPagaveis);

        await this.setChamadoAssuntoPesquisa(chamadoAssuntoPesquisa);
        await this.setUnidadeOrganizacionalPesquisa(unidadeOrganizacionalPesquisa);
        await this.setServicoTipoPesquisa(servicoTipoPesquisa);
        await this.setQuestionario(questionario);

        await t.click(this.botaoSubmit);
    }

    async removerAcao(acao){

        await t.click(this.buttonPesquisar);
        await t.click(Selector("#acao tbody td").withText(acao).parent("tr").find("input"));
        
        await t.setNativeDialogHandler(() => true).click(this.buttonRemover);

    }

    async alterarAcao(acao
        , acaoPrecedente
        , descricao
        , modulo
        , operacao
        , numeroDiasValidade
        , tipoNumeroDiasValidade
        , numeroDiasVencimento
        , tipoNumeroDiasVencimento
        , tipoDocumentoGerado
        , ordemDeServico
        , tipoServico
        , equipe
        , consideraDebitosVencer
        , bloquearDocumentosPagaveis
        , chamadoAssuntoPesquisa
        , unidadeOrganizacionalPesquisa
        , servicoTipoPesquisa
        , questionario){

        await t.click(this.buttonPesquisar);
        
        await t.click(Selector("#acao tbody td").withText(acao).parent("tr").find("input"));

        await t.click(this.buttonAlterar);

        await t.click(Selector('input[value=Limpar]'));
    
        await this.setAcaoPrecedente(acaoPrecedente);
        await this.setDescricao(descricao);
        await this.setModulo(modulo);
        await this.setOperacao(operacao);
        await this.setNumeroDiasValidade(numeroDiasValidade);
        await this.setTipoNumeroDiasValidade(tipoNumeroDiasValidade);
        await this.setNumeroDiasVencimento(numeroDiasVencimento);
        await this.setTipoNumeroDiasVencimento(tipoNumeroDiasVencimento);
        await this.setTipoDocumentoGerado(tipoDocumentoGerado);
        await this.setOrdemDeServico(ordemDeServico);
        await this.setTipoServico(tipoServico);
        await this.setEquipe(equipe);
        await this.setConsideraDebitosVencer(consideraDebitosVencer);
        await this.setBloquearDocumentosPagaveis(bloquearDocumentosPagaveis);

        await this.setChamadoAssuntoPesquisa(chamadoAssuntoPesquisa);
        await this.setUnidadeOrganizacionalPesquisa(unidadeOrganizacionalPesquisa);
        await this.setServicoTipoPesquisa(servicoTipoPesquisa);
        await this.setQuestionario(questionario);

        await t.click(this.botaoSubmit);
    }

    async setAcaoPrecedente(acaoPrecedente){
        await t.click(this.acaoPrecedente);
        await t.click(this.acaoPrecedenteOptions.withText(acaoPrecedente));
    }

    async setDescricao(descricao){
        await t.typeText(this.descricao, descricao);
    }
    
    async setModulo(modulo){
        await t.click(this.modulo);
        await t.click(this.moduloOptions.withText(modulo)).wait(500);
    }

    async setOperacao(operacao){
        await t.click(this.operacao);
        await t.click(this.operacaoOptions.withText(operacao));
    }
    
    async setNumeroDiasValidade(numeroDiasValidade){
        await t.typeText(this.numeroDiasValidade, numeroDiasValidade);
    }

    async setTipoNumeroDiasValidade(tipoNumeroDiasValidade){
        await t.click(this.tipoNumeroDiasValidade);
        await t.click(this.tipoNumeroDiasValidadeOptions.withText(tipoNumeroDiasValidade));
    }
    
    async setNumeroDiasVencimento(numeroDiasVencimento){
        await t.typeText(this.numeroDiasVencimento, numeroDiasVencimento);
    }

    async setTipoNumeroDiasVencimento(tipoNumeroDiasVencimento){
        await t.click(this.tipoNumeroDiasVencimento);
        await t.click(this.tipoNumeroDiasVencimentoOptions.withText(tipoNumeroDiasVencimento));
        }

    async setTipoDocumentoGerado(tipoDocumentoGerado) {
        await t.click(this.tipoDocumentoGerado);
        await t.click(this.tipoDocumentoGeradoOptions.withText(tipoDocumentoGerado));
    }

    async setOrdemDeServico(ordemDeServico) {
        if(ordemDeServico)
            await t.click(this.ordemDeServico);
    }

    async setTipoServico(tipoServico) {
        await t.click(this.tipoServico);
        await t.click(this.tipoServicoOptions.withText(tipoServico));
    }

    async setEquipe(equipe) {
        await t.click(this.equipe);
        await t.click(this.equipeOptions.withText(equipe));
    }

    async setConsideraDebitosVencer(consideraDebitosVencer) {
        if(consideraDebitosVencer) 
            await t.click(this.consideraDebitosVencer);
    }

    async setBloquearDocumentosPagaveis(bloquearDocumentosPagaveis) {
        if(bloquearDocumentosPagaveis)
            await t.click(this.bloquearDocumentosPagaveis);
    }

    async setChamadoAssuntoPesquisa(chamadoAssuntoPesquisa) {
        await t.click(this.chamadoAssuntoPesquisa);
        await t.click(this.chamadoAssuntoPesquisaOptions.withText(chamadoAssuntoPesquisa));
    }

    async setUnidadeOrganizacionalPesquisa(unidadeOrganizacionalPesquisa) {
        await t.click(this.unidadeOrganizacionalPesquisa);
        await t.click(this.unidadeOrganizacionalPesquisaOptions.withText(unidadeOrganizacionalPesquisa));
    }

    async setServicoTipoPesquisa(servicoTipoPesquisa) {
        await t.click(this.servicoTipoPesquisa);
        await t.click(this.servicoTipoPesquisaOptions.withText(servicoTipoPesquisa));
    }

    async setQuestionario(questionario) {
        await t.click(this.questionario);
        await t.click(this.questionarioOptions.withText(questionario));
    }

    async setDescricaoPesquisa(descricaoPesquisa){
        await t.typeText(this.descricaoPesquisa, descricaoPesquisa);
    }

    async setTipoDocumentoPesquisa(tipoDocumentoPesquisa) {
        await t.click(this.tipoDocumentoPesquisa);
        await t.click(this.tipoDocumentoPesquisaOptions.withText(tipoDocumentoPesquisa));
    }

}