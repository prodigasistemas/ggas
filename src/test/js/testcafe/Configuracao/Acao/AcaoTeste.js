import { Selector, t} from 'testcafe';
import Page from '../../ggasPage';
import AcaoPage from './AcaoPage';


const page = new Page();
const acaoPage = new AcaoPage();

fixture("Configuração-Ação")
    .page(page.url)
    .beforeEach(async t => {

        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await acaoPage.irParaAcao();

    });

test('Ação - inclusão', async t => {

    await acaoPage.inclusaoAcao('ATENDIMENTO'
     , 'Ação teste'
     , 'Atendimento Ao Público'
     , 'Gerar Pesquisa de Satisfação em Lote'
     , '15'
     , 'DIAS CORRIDOS'
     , '23'
     , 'DIAS CORRIDOS'
     , 'FATURA'
     , true
     , 'RELIGACAO NORMAL'
     , 'EQUIPE TESTE'
     , true
     , true
     , 'PESQUISA DE SATISFACAO'
     , 'UNIDADE TESTE'
     , 'RELIGACAO URGENCIA'
     , 'SAC - QUESTIONÁRIO DE SATISFAÇÃO');

     await t.expect(Selector('.notification').innerText)
        .eql('Sucesso: Ação(ões) inserido(a) com sucesso.');
});



test("Ação - Pesquisa", async t => {

    await acaoPage.pesquisarAcao('AÇÃO TESTE');
    
});

test('Ação - alterar', async t => {

    await acaoPage.alterarAcao('AÇÃO TESTE'
     , 'ATENDIMENTO'
     , 'Ação teste'
     , 'Atendimento Ao Público'
     , 'Gerar Pesquisa de Satisfação em Lote'
     , '17'
     , 'DIAS CORRIDOS'
     , '19'
     , 'DIAS CORRIDOS'
     , 'FATURA'
     , false
     , 'RELIGACAO NORMAL'
     , 'EQUIPE TESTE'
     , false
     , false
     , 'PESQUISA DE SATISFACAO'
     , 'UNIDADE TESTE'
     , 'RELIGACAO URGENCIA'
     , 'SAC - QUESTIONÁRIO DE SATISFAÇÃO');

    await t.expect(Selector('.notification').innerText)
        .eql('Sucesso: Ação(ões) alterado(a) com sucesso.');

});

test('Ação - Remover', async t => {

    await acaoPage.removerAcao('AÇÃO TESTE');

    await t.expect(Selector('.notification').innerText)
        .eql('Sucesso: Ação removido(s) com sucesso.');
});


