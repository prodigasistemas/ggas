import { Selector } from 'testcafe';
import Page from '../../ggasPage';
import ParametrizacaoPage from './ParametrizacaoPage';

const page = new Page();
const parametrizacaoPage = new ParametrizacaoPage();

fixture('Parametrizacao')
.page(page.url)
.beforeEach(async t => {
    await t.maximizeWindow();
    await page.fazerLogin('admin', 'admin');
    parametrizacaoPage.irParaParametrizacao();   
});

test("Verificar Campos Arrecadacao SANTANDER", async t =>{
	await parametrizacaoPage.configuracoesIniciais();
	const arrecadacaoCampoArquivoCodigoConvenio = Selector('#arrecadacaoCampoArquivoCodigoConvenio');
	const arrecadacaoCodigoConvenio = Selector('#arrecadacaoCodigoConvenio');
	await parametrizacaoPage.selecionarArrecadacao();
	await parametrizacaoPage.verificarCamposArrecadacaoSantander();
	
});

test("Alterar Campos Arrecacadao SANTANDER", async t=>{
	await parametrizacaoPage.selecionarArrecadacao();
	await parametrizacaoPage.alterarCamposArrecadacaoSantander('COBRANCA', '12345');
    await t
    .expect(Selector(".notification").innerText)
    .eql("Sucesso: Operação realizada com sucesso.");
	
});

test("Teste Obrigatoriedade Campos Arrecadacao SANTANDER", async t=>{
	await parametrizacaoPage.selecionarArrecadacao();
	await parametrizacaoPage.testeObrigatoriedadeCamposArrecadacaoSantander(true, false);
    await t
    .expect(Selector(".notification").innerText)
    .eql("Erro: Na aba Arrecadação, o valor do parametro Parametro para identificar valor do campo codigoConvenio de layout de retorno (CNAB) é obrigatório.");
	
	await parametrizacaoPage.selecionarArrecadacao();
	await parametrizacaoPage.testeObrigatoriedadeCamposArrecadacaoSantander(false, true);
    await t
    .expect(Selector(".notification").innerText)
    .eql("Erro: Na aba Arrecadação, o valor do parametro Parametro para recuperar o código do convenio da tabela ARRECADADOR_CONTRATO_CONVENIO é obrigatório.");
	
	await parametrizacaoPage.selecionarArrecadacao();
	await parametrizacaoPage.testeObrigatoriedadeCamposArrecadacaoSantander(false, false);
    await t
    .expect(Selector(".notification").innerText)
    .eql("Erro: Na aba Arrecadação, o valor do parametro Parametro para identificar valor do campo codigoConvenio de layout de retorno (CNAB) é obrigatório.");
	
	await parametrizacaoPage.selecionarArrecadacao();
	await parametrizacaoPage.alterarCamposArrecadacaoSantander('COBRANCA', '12345');
    await t
    .expect(Selector(".notification").innerText)
    .eql("Sucesso: Operação realizada com sucesso.");
});

test("Teste Mudanca Opcao Notificacao Corte", async t=>{
	await parametrizacaoPage.selecionarCobranca();
	await parametrizacaoPage.testMudarOpcaoNotificacaoCorte();
    await t
    .expect(Selector(".notification").innerText)
    .eql("Sucesso: Operação realizada com sucesso.");
});

test("Teste Mudar Mensagem Notificacao Corte", async t=>{
	await parametrizacaoPage.selecionarCobranca();
	await parametrizacaoPage.testMudarMensagemNotificacaoCorte();
    await t
    .expect(Selector(".notification").innerText)
    .eql("Sucesso: Operação realizada com sucesso.");
});


test("Teste Mudar Parametros Criticidade", async t=>{
	await parametrizacaoPage.selecionarAtendimento();
	await parametrizacaoPage.testMudarParametrosCriticidade();
    await t
    .expect(Selector(".notification").innerText)
    .eql("Sucesso: Operação realizada com sucesso.");
});