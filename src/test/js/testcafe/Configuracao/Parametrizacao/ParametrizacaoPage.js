import { Selector, t } from 'testcafe';
import Page from '../../ggasPage';

const page = new Page();


export default class ParametrizacaoPage extends Page{
	
    constructor() {
    	super();
    	
    	this.arrecadacaoCampoArquivoCodigoConvenio = Selector('#arrecadacaoCampoArquivoCodigoConvenio');
    	this.arrecadacaoCodigoConvenio = Selector('#arrecadacaoCodigoConvenio');    
        this.botaoSubmit = Selector("input[value=Salvar]");
        this.abaArrecadacao = Selector('#tabs').find("a").withText("Arrecadação");
        this.assinaturaEmail = Selector("#assinaturaEmail");
        this.porta = Selector("#servidorAutenticacaoAdPortaGeral");
        this.abaCobranca = Selector('#tabs').find("a").withText("Cobrança");
        this.abaAtendimento = Selector('#tabs').find("a").withText("Atendimento");
        this.comboBoxCobrancaNotificacao = Selector('#exibirMensagemNotificacaoCorte2');
        this.mensagemNotificacaoCorte = Selector('textarea[name=mensagemNotificacaoCorte]');
        this.prazoMinimoCriticidade = Selector('#prazoMinimoCriticidade');
        this.prazoMaximoCriticidade = Selector('#prazoMaximoCriticidade');
        this.valorMinimoCriticidade = Selector('#valorMinimoCriticidade');
        this.valorMedioCriticidade = Selector('#valorMedioCriticidade');
        this.valorMaximoCriticidade = Selector('#valorMaximoCriticidade');
    }
		
    async irParaParametrizacao() {
        await t.navigateTo(page.url+"/exibirParametrizacao");
    }
    
    async selecionarArrecadacao(){
    	await this.clicarElemento(this.abaArrecadacao);
    }
    
    async selecionarCobranca(){
    	await this.clicarElemento(this.abaCobranca);
    }
    
    async selecionarAtendimento(){
    	await this.clicarElemento(this.abaAtendimento);
    }
    
    async verificarCamposArrecadacaoSantander(){
    	await this.checarInput(this.arrecadacaoCampoArquivoCodigoConvenio, 'COBRANCA', true);
    	await this.checarInput(this.arrecadacaoCodigoConvenio, '12345', true);
    	
    }
    
    async alterarCamposArrecadacaoSantander(arquivoCodigoConvenio, codigoConvenio){
    	await t
        .click(this.arrecadacaoCampoArquivoCodigoConvenio)
        .pressKey('ctrl+a delete');
    	
    	await t.typeText(this.arrecadacaoCampoArquivoCodigoConvenio, arquivoCodigoConvenio);	
        
    	await t
        .click(this.arrecadacaoCodigoConvenio)
        .pressKey('ctrl+a delete');
    	
    	await t.typeText(this.arrecadacaoCodigoConvenio, codigoConvenio);
        
        await t.setNativeDialogHandler(() => true).click(this.botaoSubmit);
              
    }
    
    async testeObrigatoriedadeCamposArrecadacaoSantander(arquivoCodigoConvenio, codigoConvenio){
    	await t
        .click(this.arrecadacaoCampoArquivoCodigoConvenio)
        .pressKey('ctrl+a delete');
    	
    	await t
        .click(this.arrecadacaoCodigoConvenio)
        .pressKey('ctrl+a delete');
    	
    	if(arquivoCodigoConvenio == true){
    		await t.typeText(this.arrecadacaoCodigoConvenio, '12345');
    	} else if (codigoConvenio == true){
    		await t.typeText(this.arrecadacaoCampoArquivoCodigoConvenio, 'COBRANCA');
    	}
    	
    	await t.setNativeDialogHandler(() => true).click(this.botaoSubmit);
    	
    }
    
    async testMudarOpcaoNotificacaoCorte(){
    	await t
    	.click(this.comboBoxCobrancaNotificacao)
    	
    	await t.setNativeDialogHandler(() => true).click(this.botaoSubmit);    	
    }
    
    async testMudarMensagemNotificacaoCorte(){
    	await t
        .click(this.mensagemNotificacaoCorte)
        .pressKey('ctrl+a delete');
    	
    	await t.typeText(this.mensagemNotificacaoCorte, 'TESTE MUDANCA MENSAGEM');
    	
    	await t.setNativeDialogHandler(() => true).click(this.botaoSubmit);    	
    }
    
    async testMudarParametrosCriticidade(){
    	await t
        .click(this.prazoMinimoCriticidade)
        .pressKey('ctrl+a delete');
    	
    	await t.typeText(this.prazoMinimoCriticidade, '9');
    	
    	await t
        .click(this.prazoMaximoCriticidade)
        .pressKey('ctrl+a delete');
    	
    	await t.typeText(this.prazoMaximoCriticidade, '21');
    	
    	await t
        .click(this.valorMinimoCriticidade)
        .pressKey('ctrl+a delete');
    	
    	await t.typeText(this.valorMinimoCriticidade, '2');
    	
    	await t
        .click(this.valorMedioCriticidade)
        .pressKey('ctrl+a delete');
    	
    	await t.typeText(this.valorMedioCriticidade, '10');
    	
    	await t
        .click(this.valorMaximoCriticidade)
        .pressKey('ctrl+a delete');
    	
    	await t.typeText(this.valorMaximoCriticidade, '20');
    	
    	await t.setNativeDialogHandler(() => true).click(this.botaoSubmit);    	
    }
    
    async configuracoesIniciais(){
    	await t.typeText(this.assinaturaEmail, "teste@teste.com");
    	await t.typeText(this.porta, "1");
    	await t.setNativeDialogHandler(() => true).click(this.botaoSubmit);
    }
    
	
}