import { Selector} from 'testcafe';
import Page from '../../ggasPage';
import EmissaoAvisoCortePage from './EmissaoAvisoCortePage';

const page = new Page()
const emissaoAvisoCortePage = new EmissaoAvisoCortePage();

fixture('Emissao de 2° via de Aviso de Corte')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await emissaoAvisoCortePage.irParaEmissaoAvisoCorte();
    });

test.skip('CRUD', async t => {

    await emissaoAvisoCortePage.pesquisarPorCliente("7209");
    await t
        .expect(emissaoAvisoCortePage.tabelaFatura.withText("R$ 300,00").exists)
        .ok();

    await emissaoAvisoCortePage.pesquisarPorImovel("11164");
    await t
        .expect(emissaoAvisoCortePage.tabelaFatura.withText("R$ 300,00").exists)
        .ok();
});
