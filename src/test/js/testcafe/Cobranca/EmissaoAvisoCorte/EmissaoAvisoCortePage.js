import { Selector, t} from 'testcafe';
import Page from '../../ggasPage';

const page = new Page();

export default class EmissaoAvisoCortePage {

    constructor() {

        this.botaoPesquisar = Selector('#botaoPesquisar');
        this.botaoLimpar = Selector('input[value=Limpar]');

        this.tabelaFatura = Selector("#documento tbody td");
    }

    async irParaEmissaoAvisoCorte(){
        await t
            .navigateTo(page.url+"/pesquisarEmissaoNotificacaoAvisoCorte");
    }

    async pesquisarPorCliente(cliente) {

        await page.preencherFormularioPessoa(cliente);
        await t.click(this.botaoPesquisar);
    }

    async pesquisarPorImovel(imovel) {
        
        await page.preencherFormularioImovel(imovel);
        await t.click(this.botaoPesquisar);
    }
}
