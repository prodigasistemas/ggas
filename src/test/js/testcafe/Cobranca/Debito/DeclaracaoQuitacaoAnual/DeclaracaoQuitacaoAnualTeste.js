import { Selector} from 'testcafe';
import Page from '../../../ggasPage';
import Util from '../../../Util';
import DeclaracaoQuitacaoAnualPage from './DeclaracaoQuitacaoAnualPage';

const page = new Page();
const util = new Util();
const declaracaoQuitacaoAnualPage = new DeclaracaoQuitacaoAnualPage();
/*
fixture('Declaração de Quitação Anual')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await declaracaoQuitacaoAnualPage.irParaDeclaracaoQuitacaoAnual();
    });

test('Pesquisas', async t => {

    //Pesquisa Geral
    await declaracaoQuitacaoAnualPage.pesquisar(util.ano(-1));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: O processo de geração da declaração de quitação anual de débitos foi iniciado.');

    await declaracaoQuitacaoAnualPage.pesquisar(util.ano(-2));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: O processo de geração da declaração de quitação anual de débitos foi iniciado.');

    await declaracaoQuitacaoAnualPage.pesquisar(util.ano(-10));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: O processo de geração da declaração de quitação anual de débitos foi iniciado.');

    //Pesquisa Por Pessoa
    await declaracaoQuitacaoAnualPage.pesquisarPorPessoa("7209", util.ano(-1));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Erro: Não foram encontradas faturas pagas para o ano selecionado.');
    
    await declaracaoQuitacaoAnualPage.pesquisarPorPessoa("7209", util.ano(-2));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Erro: Não foram encontradas faturas pagas para o ano selecionado.');
    
    await declaracaoQuitacaoAnualPage.pesquisarPorPessoa("7209", util.ano(-10));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Erro: Não foram encontradas faturas pagas para o ano selecionado.');

    //Pesquisa Por Imóvel
    await declaracaoQuitacaoAnualPage.pesquisarPorImovel("11164", "3911",util.ano(-1));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Erro: Não foram encontradas faturas pagas para o ano selecionado.');
    
    await declaracaoQuitacaoAnualPage.pesquisarPorImovel("11164", "3911",util.ano(-2));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Erro: Não foram encontradas faturas pagas para o ano selecionado.');

    await declaracaoQuitacaoAnualPage.pesquisarPorImovel("11164", "3911",util.ano(-10));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Erro: Não foram encontradas faturas pagas para o ano selecionado.');
    
    //Pesquisa Por Contrato
    await declaracaoQuitacaoAnualPage.pesquisarPorContrato("3911", util.ano(-1));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Erro: Não foram encontradas faturas pagas para o ano selecionado.');

    await declaracaoQuitacaoAnualPage.pesquisarPorContrato("3911", util.ano(-2));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Erro: Não foram encontradas faturas pagas para o ano selecionado.');

    await declaracaoQuitacaoAnualPage.pesquisarPorContrato("3911", util.ano(-10));
    await t
        .expect(Selector('.notification').innerText)
        .eql('Erro: Não foram encontradas faturas pagas para o ano selecionado.');
});

test('Teste de Obrigatoriedade', async t => {

    page.preencherFormularioPessoa();
    await t
        .click(declaracaoQuitacaoAnualPage.indicarPessoa)
        .click(declaracaoQuitacaoAnualPage.botaoPesquisarGerar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Ano para Geração é (são) de preenchimento obrigatório.");

    page.preencherFormularioImovel("11164");
    await t
        .click(declaracaoQuitacaoAnualPage.indicadorImovel)
        .click(declaracaoQuitacaoAnualPage.botaoPesquisarGerar)
        .click(declaracaoQuitacaoAnualPage.tabelaImovel.withText("3911").parent("tr").find("input"))
        .click(declaracaoQuitacaoAnualPage.botaoPesquisarGerar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Ano para Geração é (são) de preenchimento obrigatório.");

    await t
        .click(declaracaoQuitacaoAnualPage.botaoLimpar)
        .click(declaracaoQuitacaoAnualPage.indicadorContrato)
        .click(declaracaoQuitacaoAnualPage.botaoPesquisarGerar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Ano para Geração é (são) de preenchimento obrigatório.");

    await t
        .click(declaracaoQuitacaoAnualPage.botaoLimpar)
        .click(declaracaoQuitacaoAnualPage.indicadorContrato)
        .click(declaracaoQuitacaoAnualPage.ano)
        .click(declaracaoQuitacaoAnualPage.anoOption.withText(util.ano( - 1)))
        .click(declaracaoQuitacaoAnualPage.botaoPesquisarGerar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Preencha um dos campos para a pesquisa.")

    await t
        .click(declaracaoQuitacaoAnualPage.botaoLimpar)
        .click(declaracaoQuitacaoAnualPage.indicadorContrato)
        .typeText(declaracaoQuitacaoAnualPage.contrato, "3911")
        .click(declaracaoQuitacaoAnualPage.botaoPesquisarGerar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Ano para Geração é (são) de preenchimento obrigatório.");
});
*/