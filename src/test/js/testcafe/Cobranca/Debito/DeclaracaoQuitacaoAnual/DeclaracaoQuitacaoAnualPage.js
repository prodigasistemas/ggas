import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class DeclaracaoQuitacaoAnualPage {

    constructor() {

        this.botaoPesquisarGerar = Selector('#botaoPesquisarGerar');
        this.botaoLimpar = Selector('input[value=Limpar]');

        this.tabelaImovel = Selector("#contratoPontoConsumo tbody td");
        this.ano = Selector("#anoGeracao");
        this.anoOption = this.ano.find("option");

        this.indicadorGeral = Selector("#indicadorPesquisaGeral");
        this.indicarPessoa = Selector("#indicadorPesquisaCliente");
        this.indicadorImovel = Selector("#indicadorPesquisaImovel");
        this.indicadorContrato = Selector("#indicadorPesquisaContrato");

        this.contrato = Selector("#numeroContrato");
    }

    async irParaDeclaracaoQuitacaoAnual(){
        await t
            .navigateTo(page.url+"/exibirPesquisaExtratoQuitacaoAnual");
    }

    async selecionar(opcao) {
        await t.click(this.tabelaImovel.withTex(opcao).parent("tr").find("input"));
    }

    async pesquisar(ano) {
        await t
            .click(this.indicadorGeral)
            .click(this.ano)
            .click(this.anoOption.withText(ano))
            .click(this.botaoPesquisarGerar);
    }

    async pesquisarPorPessoa(idPessoa, ano) {
        page.preencherFormularioPessoa(idPessoa);
        await t
            .click(this.indicarPessoa)
            .click(this.ano)
            .click(this.anoOption.withText(ano))
            .click(this.botaoPesquisarGerar);
    }

    async pesquisarPorImovel(idImovel, opcao, ano) {
        page.preencherFormularioImovel(idImovel);
        await t
            .click(this.indicadorImovel)
            .click(this.botaoPesquisarGerar)
            .click(this.tabelaImovel.withText(opcao).parent("tr").find("input"))
            .click(this.ano)
            .click(this.anoOption.withText(ano))
            .click(this.botaoPesquisarGerar);
    }

    async pesquisarPorContrato(contrato, ano) {
        await t
            .click(this.botaoLimpar)
            .click(this.indicadorContrato)
            .typeText(this.contrato, contrato)
            .click(this.ano)
            .click(this.anoOption.withText(ano))
            .click(this.botaoPesquisarGerar);
    }

}
