import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class ParcelamentoPage {

    constructor() {

        this.botaoPesquisar = Selector("#botaoPesquisar");
        this.botaoIncluir = Selector('#botaoIncluir');
        this.botaoAlterar = Selector('#');
        this.botaoRemover = Selector('#');
        this.botaoSalvar = Selector('#botaoSalvar');
        this.botaoLimpar = Selector('input[value=Limpar]');
        this.botaoGerarParcelas = Selector('#botaoGerarParcelas');

        this.tabelaContratoPontoConsumo = Selector("#contratoPontoConsumo tbody td");
        this.tabelaFatura = Selector("#fatura tbody td");
        this.indicadorPesquisaCliente = Selector("#indicadorPesquisaCliente");
        this.perfilParcelamento = Selector("#idPerfilParcelamento");
        this.perfilParcelamentoOption = this.perfilParcelamento.find("option");
        this.percentualMaximo = Selector("#percentualDesconto");
        this.numeroParcelas = Selector("#numeroParcelas");
        this.dataVencimentoNota = Selector("#dataPrimeiroVencimento");
        this.dataVencimentoNotaMaisUmMes = Selector("#dataPrimeiroVencimentoMaisUmMes");
    }

    async irParaParcelamento(){
        await t
            .navigateTo(page.url+"/exibirPesquisaParcelamentoDebitos");
    }

    async incluir(contratoPontoConsumo, fatura, perfilParcelamento, numeroParcelas, emitirNotaProvisoria, emitirTermoConfissaoDivida) {
        await t
            .click(this.botaoIncluir)
            .click(this.indicadorPesquisaCliente);

        await page.preencherFormularioPessoa("7209");
        await page.liberarBotao("#botaoPesquisar");
        await t.click(this.botaoPesquisar);
        
        await this.setContratoPontoCosumo(contratoPontoConsumo);
        await this.setFatura(fatura);
        await this.setPerfilParcelamento(perfilParcelamento);    
        await this.setNumeroParcelas(numeroParcelas);
        await this.setEmitirNotaProvisoria(emitirNotaProvisoria);
        await this.setEmitirTermoConfissaoDivida(emitirTermoConfissaoDivida);

        await page.preencherFormularioPessoa("7209");

        await t
            .click(this.botaoGerarParcelas)
            .click(this.botaoSalvar);
    }

    async setContratoPontoCosumo(contratoPontoConsumo) {
        await t.click(this.tabelaContratoPontoConsumo.withText(contratoPontoConsumo));
    }

    async setFatura(fatura) {
        await t.click(this.tabelaFatura.withText(fatura).parent("tr").find("input"));
    }

    async setPerfilParcelamento(perfilParcelamento) {
        await t
            .click(this.perfilParcelamento)
            .click(this.perfilParcelamentoOption.withText(perfilParcelamento));
    }

    async setPercentualMaximo(percentualMaximo) {
        await t.typeText(this.percentualMaximo, percentualMaximo);
    }

    async setNumeroParcelas(numeroParcelas) {
        await t.typeText(this.numeroParcelas, numeroParcelas);
    }

    async setTipoCobranca(cobrancaEmConta) {
        if(cobrancaEmConta) {
            await t.click(Selector("#cobrancaEmContaSim"));
        } else {
            await t.click(Selector("#cobrancaEmContaNao"));
        }
    }

    async setDataVencimentoNota(dataAtual, dataVencimentoNota) {
        
        if(dataAtual){
            await t
                .click(Selector("#indicadorDataAtualSim"))
                .typeText(this.dataVencimentoNota, dataVencimentoNota);
        } else {
            await t
                .click(Selector("#indicadorDataAtualNao"))
                .typeText(this.dataVencimentoNotaMaisUmMes, dataVencimentoNota);
        }
    }
    
    async setEmitirNotaProvisoria(emitirNotaProvisoria) {

        if(emitirNotaProvisoria) {
            await t.click(Selector("#indicadorNotaPromissoriaSim"));
        } else {
            await t.click(Selector("#indicadorNotaPromissoriaNao"));
        }
    }

    async setEmitirTermoConfissaoDivida(emitirTermoConfissaoDivida) {
        
        if(emitirTermoConfissaoDivida) {
            await t.click(Selector("#indicadorConfissaoDividaSim"));
        } else {
            await t.click(Selector("#indicadorConfissaoDividaNao"));
        }
    }
}
