import { Selector} from 'testcafe';
import Page from '../../../ggasPage';
import Util from '../../../Util';
import ParcelamentoPage from './ParcelamentoPage';

const page = new Page();
const util = new Util();
const parcelamentoPage = new ParcelamentoPage();
/*
fixture('Parcelamento')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await parcelamentoPage.irParaParcelamento();
    });

test('Teste de Obrigatoriedade', async t => {

    await t
        .click(parcelamentoPage.botaoIncluir)
        .click(parcelamentoPage.indicadorPesquisaCliente);

    await page.preencherFormularioPessoa("7209");

    await page.liberarBotao("#botaoPesquisar");
    await t.click(parcelamentoPage.botaoPesquisar);
    await parcelamentoPage.setContratoPontoCosumo("201503911-00");

    await t
        .click(parcelamentoPage.botaoGerarParcelas)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Perfil de parcelamento, Valor total do débito, Valor total líquido, Número de Parcelas é (são) de preenchimento obrigatório.');

    await parcelamentoPage.setEmitirNotaProvisoria(false);
    await t
        .click(parcelamentoPage.botaoGerarParcelas)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Perfil de parcelamento, Valor total do débito, Valor total líquido, Número de Parcelas é (são) de preenchimento obrigatório.');

    await parcelamentoPage.setEmitirTermoConfissaoDivida(false);
    await t
        .click(parcelamentoPage.botaoGerarParcelas)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Perfil de parcelamento, Valor total do débito, Valor total líquido, Número de Parcelas é (são) de preenchimento obrigatório.');

    await page.preencherFormularioPessoa("7209");
    await t
        .click(parcelamentoPage.botaoGerarParcelas)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Perfil de parcelamento, Valor total do débito, Valor total líquido, Número de Parcelas é (são) de preenchimento obrigatório.');

    
    await parcelamentoPage.setTipoCobranca(false);
    await parcelamentoPage.setDataVencimentoNota(false, util.dateNow());
    await t
        .click(parcelamentoPage.botaoGerarParcelas)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Perfil de parcelamento, Valor total do débito, Valor total líquido, Número de Parcelas é (são) de preenchimento obrigatório.');


    await parcelamentoPage.setFatura("2876");
    await t
        .click(parcelamentoPage.botaoGerarParcelas)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Perfil de parcelamento, Número de Parcelas é (são) de preenchimento obrigatório.');

    await parcelamentoPage.setPerfilParcelamento("PERFIL PARCELAMENTO DE TESTE");
    await t
        .click(parcelamentoPage.botaoGerarParcelas)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Número de Parcelas é (são) de preenchimento obrigatório.');
        
    await parcelamentoPage.setNumeroParcelas("3");

    await t.click(parcelamentoPage.botaoGerarParcelas);
});

test('CRUD', async t => {

    await parcelamentoPage.incluir("201503911-00", "2876", "PERFIL PARCELAMENTO DE TESTE", "3", false, false);
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Parcelamento efetuado com sucesso.');
});
*/
