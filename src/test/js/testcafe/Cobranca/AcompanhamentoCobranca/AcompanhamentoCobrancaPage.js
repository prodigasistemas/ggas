import { Selector, t} from 'testcafe';
import Page from '../../ggasPage';

const page = new Page();

export default class AcompanhamentoCobrancaPage {

    constructor() {

        this.botaoPesquisar = Selector('#buttonPesquisar');
        this.botaoPontoConsumo = Selector('#ButtonPesquisarPontoConsumo');
        this.botaoPesquisarCliente = Selector('#botaoPesquisarCliente');
        this.botaoLimpar = Selector('input[value=Limpar]');

        this.indicadorPesquisaCliente = Selector("#indicadorPesquisaCliente");
        this.indicadorPesquisaImovel = Selector("#indicadorPesquisaImovel");

        this.tabelaPontoConsumo = Selector("#pontoConsumo tbody td");

        this.tabelaFatura = Selector("#ServicoAutorizacaoVO tbody td");
    }

    async irParaAcompanhamentoCobranca(){
        await t
            .navigateTo(page.url + "/exibirAcompanhamentoCobranca");
    }

    async pesquisarPorCliente(cliente) {
        await page.preencherFormularioPessoa(cliente);
        await page.liberarBotao("#buttonPesquisar");
        await t.expect(this.botaoPesquisar.exists).ok();
        await t.click(this.botaoPesquisar);
    }

    async pesquisarPorImovel(imovel) {
        await t.click(this.indicadorPesquisaImovel);
        await page.preencherFormularioImovel(imovel);
        await page.liberarBotao("#buttonPesquisar");
        await t.click(this.botaoPesquisar);
    }

    async pontoConsumoCliente(cliente) {
        await t.click(this.indicadorPesquisaCliente);
        await page.preencherFormularioPessoa(cliente);
        await page.liberarBotao("#ButtonPesquisarPontoConsumo");
        await t.click(this.botaoPontoConsumo);
    }

    async pontoConsumoImovel(imovel) {
        await t.click(this.indicadorPesquisaImovel);
        await page.preencherFormularioImovel(imovel);
        await page.liberarBotao("#ButtonPesquisarPontoConsumo");
        await t.click(this.botaoPontoConsumo);
    }

    async selecionarPontoConsumo(pontoConsumo) {
        await t.click(this.tabelaPontoConsumo.withText(pontoConsumo).parent("tr").find('input'));
    }

}
