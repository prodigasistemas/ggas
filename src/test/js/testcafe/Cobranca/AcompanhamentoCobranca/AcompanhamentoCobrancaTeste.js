import { Selector } from 'testcafe';
import Page from '../../ggasPage';
import AcompanhamentoCobrancaPage from './AcompanhamentoCobrancaPage';

const page = new Page();
const acompanhamentoCobrancaPage = new AcompanhamentoCobrancaPage();

fixture `Fixture2`;


fixture('Acompanhamento de Cobrança')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await acompanhamentoCobrancaPage.irParaAcompanhamentoCobranca();
    });

test.skip('CRUD', async t => {

    await acompanhamentoCobrancaPage.pesquisarPorCliente("7264");
    await t
        .expect(acompanhamentoCobrancaPage.tabelaFatura.withText("2879").exists)
        .ok();

    await acompanhamentoCobrancaPage.pesquisarPorImovel("11212");
    await t
        .expect(acompanhamentoCobrancaPage.tabelaFatura.withText("2879").exists)
        .ok();

    await acompanhamentoCobrancaPage.pontoConsumoCliente("7264");
    await t
        .expect(acompanhamentoCobrancaPage.tabelaPontoConsumo.exists).ok();

        await acompanhamentoCobrancaPage.selecionarPontoConsumo("11070");
    await t
        .expect(acompanhamentoCobrancaPage.tabelaFatura.withText("2879").exists)
        .ok();

    await acompanhamentoCobrancaPage.pontoConsumoImovel("11212");
    await t
        .expect(acompanhamentoCobrancaPage.tabelaPontoConsumo.exists).ok();

    await acompanhamentoCobrancaPage.selecionarPontoConsumo("11070");
    await t
        .expect(acompanhamentoCobrancaPage.tabelaFatura.withText("2879").exists)
        .ok();
});
