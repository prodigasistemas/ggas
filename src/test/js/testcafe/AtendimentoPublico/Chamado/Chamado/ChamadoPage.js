import { Selector, t } from 'testcafe';
import Page from '../../../ggasPage';

export default class ChamadoPage extends Page {
    constructor() {

        super();

        this.abaFormChamado = Selector('#caret-aba-chamado');

        this.statusAberto = Selector('#aberto');
        this.statusEmAndamento = Selector('#emAndamento');
        this.statusRascunho = Selector('#rascunho');
        this.statusFinalizado = Selector('#status_finalizado');

        this.descricao = Selector('#descricao');
        this.selectSegmento = Selector('#idSegmentoChamadoTipo');
        this.selectCanalAtendimento = Selector('#canalAtendimento');
        this.selectUnidadeOrganizacional = Selector('#unidadeOrganizacional');
        this.selectUsuarioResponsavel = Selector('#usuarioResponsavel');
        this.selectChamadoTipo = Selector('#chamadoTipo');
        this.selectChamadoAssunto = Selector('#chamadoAssunto');
        this.selectMotivoEncerramento = Selector('#idMotivo');
        this.selectListaEquipe = Selector('#listaEquipe');

        this.selectSegmentoOption = Selector('#idSegmentoChamadoTipo').find('option');
        this.selectCanalAtendimentoOption = Selector('#canalAtendimento').find('option');
        this.selectUnidadeOrganizacionalOption = Selector('#unidadeOrganizacional').find('option');
        this.selectUsuarioResponsavelOption = Selector('#usuarioResponsavel').find('option');
        this.selectChamadoTipoOption = Selector('#chamadoTipo').find('option');
        this.selectChamadoAssuntoOption = Selector('#chamadoAssunto').find('option');
        this.selectMotivoEncerramentoOption = Selector('#idMotivo').find('option');
        this.selectListaEquipeOption = Selector('#listaEquipe').find('option');

        this.abaSolicitante = Selector('#tab-solicitante');
        this.rgSolicitante = Selector('#rgSolicitante');
        this.telefoneSolicitante = Selector('#telefoneSolicitante');
        this.nomeSolicitante = Selector('#nomeSolicitante');
        this.cpfCnpjSolicitante = Selector('#cpfCnpjSolicitante');
        this.emailSolicitante = Selector('#emailSolicitante');
        this.selectTipoContatoSolicitante = Selector('#tipoContatoSolicitante');
        this.selectTipoContatoSolicitanteOption = Selector('#tipoContatoSolicitante').find('option');

        this.abaCliente = Selector('#tab-cliente');
        this.nomeCliente = Selector('#nome')
        this.opcaoCliente = Selector('#ui-id-12');
        this.usuarioResponsavelNome = Selector('#usuarioResponsavelNome');
        this.contratoCliente = Selector('#contrato');
        this.nomeFantasiaCliente = Selector('#nomeFantasia');
        this.cpfCnpjCliente = Selector('#cpfCnpj');
        this.rgCliente = Selector('#rg');
        this.passaporteCliente = Selector('#numeroPassaporte');
        this.emailCliente = Selector('#email');
        this.telefoneCliente = Selector('#telefone');

        this.abaDocumentos = Selector('#tab-docs');
        this.documento = Selector('#arquivoAnexo');
        this.descricaoDocumento = Selector('#descricaoAnexo');
        this.botaoAdicionarDocumento = Selector('#botaoIncluirAnexo');

        this.abaEmail = Selector('#tab-email');
        this.email = Selector('#descricaoEmail');
        this.botaoAdicionarEmail = Selector('#bottonAdicionarEmail');

        this.abaImovel = Selector('#tab-imovel');
        this.abaHistorico = Selector('#tab-historico');

        this.abaControleOperacional = Selector('#tab-controle-operacional');
        this.informacaoGasista = Selector('#divInformacaoGasista').find('label');
        this.informacaoAdicionalGasista = Selector('#informacaoAdicional');
        this.indicadorAcionamentoGasistaSim = Selector('#indicadorAcionamentoSim');
        this.acionamentoGasista = Selector('#acionamentoGasista');

        this.botaoIncluir = Selector('#botaoIncluir');
        this.botaoIncluirRascunho = Selector('#botaoIncluirRascunho');
        this.botaoAlterar = Selector('#botaoAlterar');
        this.botaoCapturar = Selector('#botaoCapturar');
        this.botaoTramitar = Selector('#botaoTramitar');
        this.botaoEncerrar = Selector('#botaoEncerrar');
        this.botaoReiterar = Selector('#botaoReiterar');
        this.botaoServicoDisponivel = Selector('#button-salvar-servico-disponivel');

        this.botaoPesquisar = Selector('#botaoPesquisar');
        this.botaoAbrirAlteracao = Selector('#abrirAlteracao');
        this.botaoAbrirInclusao = Selector('#buttonIncluir');
        this.botaoAbrirCaptura = Selector('[name="buttonCapturar"]');
        this.botaoAbrirAS = Selector('[name="botaoGerarServicoAutorizacao"]');
        this.botaoAbrirTramitacao = Selector('#abrirTramitacao');
        this.botaoAbrirEncerramento = Selector('#abrirEncerramento');
        this.botaoAbrirReiteracao = Selector('#abrirReiteracao');
        this.botaoGerarAS = Selector('#botaoGerar');
        
    }

    async irParaChamado() {
        await t.navigateTo(this.url + '/exibirPesquisaChamado');
    }

    async preencherDescricao(descricao) {
        if(descricao != null) {
            await t
            .selectText(this.descricao)
            .typeText(this.descricao, descricao);
        }
    }

    async selecionaSegmento(segmento) {
        if(segmento != null) {
            await t
                .click(this.selectSegmento)
                .click(this.selectSegmentoOption.withText(segmento))
        }
    }

    async selecionaChamadoTipo(tipo) {
        if(tipo != null) {
            await t
                .click(this.selectChamadoTipo)
                .click(this.selectChamadoTipoOption.withText(tipo))
        }
    }

    async selecionaAssunto(assunto) {
        if(assunto != null) {
            await t
                .click(this.selectChamadoAssunto)
                .click(this.selectChamadoAssuntoOption.withText(assunto))
        }
    }

    async selecionaUnidade(unidade) {
        if(unidade != null) {
            await t
                .click(this.selectUnidadeOrganizacional)
                .click(this.selectUnidadeOrganizacionalOption.withText(unidade))
        }
    }

    async selecionaCanalAtendimento(canalAtendimento) {
        if(canalAtendimento != null) {
            await t
                .click(this.selectCanalAtendimento)
                .click(this.selectCanalAtendimentoOption.withText(canalAtendimento))
        }
    }

    async selecionaResponsavel(responsavel) {
        if(responsavel != null) {
            await t
                .click(this.selectUsuarioResponsavel)
                .click(this.selectUsuarioResponsavelOption.withText(responsavel))
        }
    }

    async selecionaMotivo(motivo) {
        if(motivo != null) {
            await t
                .click(this.selectMotivoEncerramento)
                .click(this.selectMotivoEncerramentoOption.withText(motivo))
        }
    }

    async selecionaEquipeAS(equipe) {
        if(equipe != null) {
            await t
                .click(this.selectListaEquipe)
                .click(this.selectListaEquipeOption.withText(equipe))
        }
    }

    async preencherSolicitante(nome, rg, telefone, tipoContato, email) {

        if(nome != null && rg != null && telefone != null) {
            await t
            .click(this.abaSolicitante)
            .selectText(this.nomeSolicitante)
            .typeText(this.nomeSolicitante, nome)
            .typeText(this.rgSolicitante, rg)
            .typeText(this.telefoneSolicitante, telefone)
            .selectText(this.emailSolicitante)
            .typeText(this.emailSolicitante, email)
            .click(this.selectTipoContatoSolicitante)
            .click(this.selectTipoContatoSolicitanteOption.withText(tipoContato));
        }
        
    }

    async preencherCliente(nomeCliente) {
        if(nomeCliente != null) {
            await t
                .click(this.abaCliente)
                .typeText(this.nomeCliente, nomeCliente)
                .setNativeDialogHandler(() => true)
                .click(this.opcaoCliente);
        }
    }

    async preencherGasista(preencheGasista, informacaoAdicional) {
        if(preencheGasista) {
            await t
                .click(this.abaControleOperacional)
                .click(this.informacaoGasista.withText('Sim'))
                .click(Selector('label').withText('Gasista'))
                .typeText(this.informacaoAdicionalGasista, informacaoAdicional);
        }
    }

    async preencherDocumento(documento, descricao) {
        if(documento != null) {
            await t
                .click(this.abaDocumentos)
                .setFilesToUpload(this.documento, documento)
                .typeText(this.descricaoDocumento, descricao)
                .click(this.botaoAdicionarDocumento);
        }
    }

    async preencherEmail(email) {
        if(email != null) {
            await t
                .click(this.abaEmail)
                .typeText(this.email, email)
                .click(this.botaoAdicionarEmail);
        }
    }

    async apagarEmail(email) {
        await t
            .click(this.abaEmail)
            .expect(Selector('a').withText(email).exists).ok()
            .click(Selector('a').withAttribute('title', 'Excluir e-mail'))
            .expect(Selector('a').withText(email).exists).notOk();
    }

    async clicarBotaoIncluir() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoIncluir);
    }

    async clicarBotaoIncluirRascunho() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoIncluirRascunho);
    }

    async clicarBotaoAlterar() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoAlterar);
    }

    async clicarBotaoCapturar() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoCapturar);
    }

    async clicarBotaoTramitar() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoTramitar);
    }

    async clicarBotaoEncerrar() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoEncerrar);
    }

    async clicarBotaoReiterar() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoReiterar);
    }

    async clicarBotaoGerarAS() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoGerarAS);
    }

    async clicarBotaoSalvarServicoDisponivel() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoServicoDisponivel);
    }

    async clicarBotaoAbrirInclusao() {
        await t.click(this.botaoAbrirInclusao);
    }

    async clicarBotaoAbrirAlteracao() {
        await t.click(this.botaoAbrirAlteracao);
    }

    async clicarBotaoAbrirCaptura() {
        await t.click(this.botaoAbrirCaptura);
    }

    async clicarBotaoAbrirTramitacao() {
        await t.click(this.botaoAbrirTramitacao);
    }

    async clicarBotaoAbrirEncerramento() {
        await t.click(this.botaoAbrirEncerramento);
    }

    async clicarBotaoAbrirReiteracao() {
        await t.click(this.botaoAbrirReiteracao);
    }

    async clicarBotaoAbrirAS() {
        await t.click(this.botaoAbrirAS);
    }

    async preencherChamado(descricao, segmento, tipoChamado, assunto, unidade, canal, responsavel, nomeSolicitante, rgSolicitante, telefoneSolicitante, 
        contatoSolicitante, nomeCliente, documento, descricaoDocumento, email, acionamentoGasista, infoGasista) {

            this.preencherDescricao(descricao);
            this.selecionaSegmento(segmento);
            this.selecionaChamadoTipo(tipoChamado);
            this.selecionaAssunto(assunto);
            this.selecionaUnidade(unidade);
            this.selecionaCanalAtendimento(canal);
            this.selecionaResponsavel(responsavel);
            this.preencherCliente(nomeCliente);
            this.preencherDocumento(documento, descricaoDocumento);
            this.preencherEmail(email);
            this.preencherGasista(acionamentoGasista, infoGasista);
            this.preencherSolicitante(nomeSolicitante, rgSolicitante, telefoneSolicitante, contatoSolicitante, email);

    }

    async preencheChamadoEncerramento(descricao, documento, descricaoDocumento, motivoEncerramento) {
        this.aguardarCarregarPagina("Encerramento do Chamado", Selector('div.card-header'));
        this.preencherDescricao(descricao);
        this.preencherDocumento(documento, descricaoDocumento);
        this.selecionaMotivo(motivoEncerramento);
    }

    async incluirChamado(descricao, segmento, tipoChamado, assunto, unidade, canal, responsavel, nomeSolicitante, rgSolicitante, telefoneSolicitante, 
        contatoSolicitante, nomeCliente, documento, descricaoDocumento, email, acionamentoGasista, infoGasista) {

            this.clicarBotaoAbrirInclusao();
            this.preencherChamado(descricao, segmento, tipoChamado, assunto, unidade, canal, responsavel, nomeSolicitante, rgSolicitante, telefoneSolicitante, 
                contatoSolicitante, nomeCliente, documento, descricaoDocumento, email, acionamentoGasista, infoGasista);
            this.clicarBotaoIncluir();
    }

    async incluirChamadoRascunho(descricao, segmento, tipoChamado, assunto, unidade, canal, responsavel, nomeSolicitante, rgSolicitante, telefoneSolicitante, 
        contatoSolicitante, nomeCliente, documento, descricaoDocumento, email, acionamentoGasista, infoGasista) {

            this.clicarBotaoAbrirInclusao();
            this.preencherChamado(descricao, segmento, tipoChamado, assunto, unidade, canal, responsavel, nomeSolicitante, rgSolicitante, telefoneSolicitante, 
                contatoSolicitante, nomeCliente, documento, descricaoDocumento, email, acionamentoGasista, infoGasista);
            this.clicarBotaoIncluirRascunho();
    }

    async alterarChamado(descricao, documento, descricaoDocumento, email) {
            
        this.preencherChamado(descricao, null, null, null, null, null, null, null, null, null, null, null, documento, descricaoDocumento, email, false, null);
        this.clicarBotaoAlterar();

    }

    async alterarChamadoRascunho(descricao, segmento, tipoChamado, assunto, unidade, canal, responsavel, nomeSolicitante, rgSolicitante, telefoneSolicitante, 
        contatoSolicitante, nomeCliente, documento, descricaoDocumento, email, acionamentoGasista, infoGasista) {

            this.preencherChamado(descricao, segmento, tipoChamado, assunto, unidade, canal, responsavel, nomeSolicitante, rgSolicitante, telefoneSolicitante, 
                contatoSolicitante, nomeCliente, documento, descricaoDocumento, email, acionamentoGasista, infoGasista);
            this.clicarBotaoAlterar();
    }

    /**
     * Verifica os dados do chamado na alteração do chamado.
     * 
     * @param {boolean} chamadoPosAlteracao Identifica se a checagem é após ou durante a alteração. Após a alteração, o campo de descrição aparece desabilitado.
     * @param {boolean} chamadoEmRascunho Identifica se o chamado que está sendo alterado está com status EM RASCUNHO. Chamado com status EM RASCUNHO mostra os campos habilitados.
     * @param {string} descricao Descrição do chamado.
     * @param {string} tipoChamado Tipo do chamado.
     * @param {string} assunto Assunto do chamado.
     * @param {string} unidade Unidade do chamado.
     * @param {string} canal Canal de atendimento do chamado.
     * @param {string} responsavel Responsável do chamado.
     */
    async checarDadosChamadoAlteracao(chamadoPosAlteracao, chamadoEmRascunho, descricao, tipoChamado, assunto, unidade, canal, responsavel) {

        const campoHabilitado = chamadoEmRascunho;

        await t.wait(1000);

        await this.checarInput(this.descricao, descricao, !chamadoPosAlteracao);
        await this.checarSelect(this.selectChamadoTipo, tipoChamado, campoHabilitado);
        await this.checarSelect(this.selectChamadoAssunto, assunto, campoHabilitado);
        await this.checarSelect(this.selectUnidadeOrganizacional, unidade, campoHabilitado);
        await this.checarSelect(this.selectCanalAtendimento, canal, campoHabilitado);
        await this.checarInput(this.usuarioResponsavelNome, responsavel, false);

    }

    async checarDadosChamadoEncerramento(descricao, tipoChamado, assunto, unidade, canal, responsavel) {

        await t.wait(1000);

        await this.checarInput(this.descricao, descricao, true);
        await this.checarSelect(this.selectChamadoTipo, tipoChamado, false);
        await this.checarSelect(this.selectChamadoAssunto, assunto, false);
        await this.checarSelect(this.selectUnidadeOrganizacional, unidade, false);
        await this.checarSelect(this.selectCanalAtendimento, canal, false);
        await this.checarInput(this.usuarioResponsavelNome, responsavel, false);
        await this.checarSelect(this.selectMotivoEncerramento, null, true);

    }

    async checarDadosSolicitanteAlteracao(chamadoEmRascunho, nomeSolicitante, rgSolicitante, telefoneSolicitante, cpfCnpjSolicitante, emailSolicitante, contatoSolicitante) {

        const campoHabilitado = chamadoEmRascunho;

        await t.click(this.abaSolicitante);

        await this.checarInput(this.nomeSolicitante, nomeSolicitante, campoHabilitado);
        await this.checarInput(this.rgSolicitante, rgSolicitante, campoHabilitado);
        await this.checarInput(this.telefoneSolicitante, telefoneSolicitante, campoHabilitado);
        await this.checarInput(this.cpfCnpjSolicitante, cpfCnpjSolicitante, campoHabilitado);
        await this.checarInput(this.emailSolicitante, emailSolicitante, campoHabilitado);
        await this.checarSelect(this.selectTipoContatoSolicitante, contatoSolicitante, campoHabilitado);
            
    }

    async checarDadosClienteAlteracao(chamadoEmRascunho, nomeCliente, contratoCliente, nomeFantasiaCliente, cpfCnpjCliente, rgCliente, passaporteCliente, emailCliente, telefoneCliente) {

        const campoHabilitado = chamadoEmRascunho;

        await t.click(this.abaCliente);

        await this.checarInput(this.nomeCliente, nomeCliente, campoHabilitado);
        await this.checarInput(this.contratoCliente, contratoCliente, campoHabilitado);
        await this.checarInput(this.nomeFantasiaCliente, nomeFantasiaCliente, campoHabilitado);
        await this.checarInput(this.cpfCnpjCliente, cpfCnpjCliente, campoHabilitado);
        await this.checarInput(this.rgCliente, rgCliente, campoHabilitado);
        await this.checarInput(this.passaporteCliente, passaporteCliente, campoHabilitado);
        await this.checarInput(this.emailCliente, emailCliente, campoHabilitado);
        await this.checarInput(this.telefoneCliente, telefoneCliente, campoHabilitado);
            
    }

    async checarDadosImovelAlteracao(imovel) {

        if(imovel != null) {
            await t
                .click(this.abaImovel)
                .expect(Selector('#table-grid-pontos-consumo').find('td').withText(imovel).exists).ok();
        } else {
            await t
                .click(this.abaImovel)
                .expect(Selector('#table-grid-pontos-consumo').find('td').withText('Nenhum registro foi encontrado').exists).ok();
        }
        
    }

    async checarDadosEmailAlteracao(email) {

        if(email != null) {
            await t
                .click(this.abaEmail)
                .expect(Selector('a').withText(email).exists).ok();
        }
        
    }

    async checarDadosDocumentoAdicionado(documento) {

        if(documento!= null) {
            await t
            .click(this.abaHistorico)
            .expect(Selector('#table-grid-chamado-historico-anexo').find('td').withText(documento).exists).ok();
        }

    }

    async checarDadosGasistaAlteracao(informacaoAdicional) {

        if(informacaoAdicional != null) {
            await t.click(this.abaControleOperacional);

            await this.checarInputDesabilitado(this.indicadorAcionamentoGasistaSim);
            await this.checarInputDesabilitado(this.acionamentoGasista);
            await this.checarInput(this.informacaoAdicionalGasista, informacaoAdicional, false);
        }

    }

    /**
     * Abre a tela de pesquisa de chamado, realiza a pesquisa e seleciona todos os chamados do resultado.
     * 
     * @param {boolean} formOpened Identifica se o filtro de chamado está aberto ao entrar na tela de pesquisa. 
     * @param {string} nomeSolicitante Nome do solicitante do chamado.
     * @param {boolean} aberto Busca por chamados abertos.
     * @param {boolean} emAtendimento Busca por chamados em atendimento.
     * @param {boolean} rascunho Busca por chamados em rascunho.
     * @param {boolean} finalizado Busca por chamados finalizados.
     */
    async pesquisarChamado(formOpened, nomeSolicitante, aberto, emAtendimento, rascunho, finalizado) {

        if(formOpened) {
            await t
            .navigateTo(this.url + '/exibirPesquisaChamado')
            .typeText(this.nomeSolicitante, nomeSolicitante);
        } else {
            await t
            .navigateTo(this.url + '/exibirPesquisaChamado')
            .click(this.abaFormChamado)
            .typeText(this.nomeSolicitante, nomeSolicitante);
        }
        
        if(!aberto) {
            await this.clicarRadioCheckBox(this.statusAberto);
        }

        if(!emAtendimento) {
            await this.clicarRadioCheckBox(this.statusEmAndamento);
        }

        if(rascunho) {
            await this.clicarRadioCheckBox(this.statusRascunho);
        }

        if(finalizado) {
            await this.clicarRadioCheckBox(this.statusFinalizado);
        }

        await t
            .click(this.botaoPesquisar)
            .click(Selector('.custom-control-label.p-0').parent(0));

    }
    
}