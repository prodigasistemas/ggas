import {Selector} from 'testcafe';
import Page from '../../../ggasPage';
import ChamadoPage from './ChamadoPage';
import AutorizacaoServicoPage from '../../AutorizacaoServico/AutorizacaoServico/AutorizacaoServicoPage';

const page = new Page();
const chamadoPage = new ChamadoPage();
const autorizacaoServicoPage = new AutorizacaoServicoPage();

const segmento = 'RESIDENCIAL';
const tipoChamado = 'TESTE ATENDIMENTO PUBLICO';
const assuntoChamado = 'DADOS NAO OBRIGATORIOS';
const assuntoChamadoSemGasista = 'DADOS NAO OBRIGATORIOS SEM GASISTA';
const unidadeOrganizacional = 'GCRC RESIDENCIAL';
const canalAtendimento = 'SAC';
const responsavel = 'ADMINISTRADOR';
const nomeSolicitante = ['CT0001A', 'CT0002', 'CT0003', 'CT0004', 'CT1030'];
const rgSolicitante = '1111111';
const telefoneSolicitante = '8188888888';
const cpfCnpjSolicitante = '42.150.391/0022-03';
const emailSolicitante = 'contato@braskem.com.br';
const funcaoSolicitante = 'ADMINISTRADOR';
const contratoCliente = '201400886';
const nomeCliente = 'BRASKEM SA';
const nomeFantasiaCliente = 'BRASKEM';
const descricaoImovel = 'TESTE';
const documentoValido = './_uploads_/TesteUploadArquivo.pdf';
const descricaoDocumento = 'DOCUMENTO';
const email = 'email@ggas.com.br';
const descricaoGasista = 'Gasista';
const motivoEncerramento = 'CANCELADO CLIENTE';

fixture('Teste de encerramento de chamado')
    .page( page.url );

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamado('CT01 TESTE ENCERRAR CHAMADO', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
        nomeSolicitante[0], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, documentoValido, descricaoDocumento, email, true, descricaoGasista);

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");
})

test.skip('Tentando ENCERRAR chamado ABERTO', async t => {

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[0], true, true, false, false);
    await chamadoPage.clicarBotaoAbrirEncerramento();

    //await chamadoPage.checarDadosChamadoEncerramento(null, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel);
    await chamadoPage.checarDadosSolicitanteAlteracao(false, nomeSolicitante[0], rgSolicitante, telefoneSolicitante, cpfCnpjSolicitante, emailSolicitante, funcaoSolicitante);
    await chamadoPage.checarDadosClienteAlteracao(false, nomeCliente, contratoCliente, nomeFantasiaCliente, cpfCnpjSolicitante, null, null, emailSolicitante, null);
    await chamadoPage.checarDadosImovelAlteracao(descricaoImovel);
    //await chamadoPage.checarDadosEmailAlteracao(email);
    await chamadoPage.checarDadosDocumentoAdicionado(descricaoDocumento);
    await chamadoPage.checarDadosGasistaAlteracao(descricaoGasista);

    await chamadoPage.preencheChamadoEncerramento('CT01 ENCERRANDO CHAMADO',  documentoValido, 'SEGUNDO ANEXO', motivoEncerramento);
    await chamadoPage.clicarBotaoEncerrar();

    await chamadoPage.checarDadosDocumentoAdicionado('SEGUNDO ANEXO');

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Chamado encerrado(a) com sucesso.");

});

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamado('CT02 TESTE ENCERRAR CHAMADO', segmento, tipoChamado, assuntoChamadoSemGasista, unidadeOrganizacional, canalAtendimento, null, 
        nomeSolicitante[1], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, null, null, email, false, null);

    await chamadoPage.clicarBotaoSalvarServicoDisponivel();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[1], true, true, false, false);
    await chamadoPage.clicarBotaoAbrirTramitacao();
    await chamadoPage.preencherChamado('TRAMITANDO CHAMADO', null, null, null, 'UNIDADE TESTE', null, null, null, null, null, null, null, null, null, null, false, null);
    await chamadoPage.clicarBotaoTramitar();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Chamado tramitado(a) com sucesso.");

    await chamadoPage.clicarBotaoAbrirAS();
    await chamadoPage.selecionaEquipeAS('EQUIPE TESTE');
    await chamadoPage.clicarBotaoGerarAS();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Autorização de Serviço Gerada com Sucesso.");

    await autorizacaoServicoPage.cancelar('ENCERRANDO AS', 'ENCERRAMENTO');
    await t
        .expect(Selector(".notification.mensagemSucesso").innerText)
        .eql("Sucesso: Autorização de Serviço encerrado(a) com sucesso.");


})

test.skip('Tentando ENCERRAR chamado EM ANDAMENTO com AS encerrada', async t => {

    await chamadoPage.pesquisarChamado(true, nomeSolicitante[1], false, true, false, false);
    await chamadoPage.clicarBotaoAbrirEncerramento();

    //await chamadoPage.checarDadosChamadoEncerramento(null, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel);
    await chamadoPage.checarDadosSolicitanteAlteracao(false, nomeSolicitante[1], rgSolicitante, telefoneSolicitante, cpfCnpjSolicitante, emailSolicitante, null);
    await chamadoPage.checarDadosClienteAlteracao(false, nomeCliente, contratoCliente, nomeFantasiaCliente, cpfCnpjSolicitante, null, null, emailSolicitante, null);

    await chamadoPage.preencheChamadoEncerramento('CT02 ENCERRANDO CHAMADO',  null, null, motivoEncerramento);
    await chamadoPage.clicarBotaoEncerrar();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Chamado encerrado(a) com sucesso.");

});

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamadoRascunho('CT03 TESTE ENCERRAR CHAMADO', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
    nomeSolicitante[2], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, documentoValido, descricaoDocumento, email, true, descricaoGasista);
    
    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");
})

('Tentando ENCERRAR chamado em RASCUNHO', async t => {

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[2], true, true, true, false);
    await chamadoPage.clicarBotaoAbrirEncerramento();

    await t.expect(Selector('.notification.failure').innerText).contains("Erro: Esta operação não pode ser realizada para um Chamado de Status RASCUNHO.");

});

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamado('CT04 TESTE ENCERRAR CHAMADO', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
    nomeSolicitante[3], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, null, null, email, true, descricaoGasista);;
    
    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[3], true, true, false, false);
    await chamadoPage.clicarBotaoAbrirEncerramento();
    await chamadoPage.preencheChamadoEncerramento('ENCERRANDO CHAMADO', null, null, motivoEncerramento);
    await chamadoPage.clicarBotaoEncerrar();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Chamado encerrado(a) com sucesso.");
})

('Tentando ENCERRAR chamado FINALIZADO', async t => {

    await chamadoPage.pesquisarChamado(true, nomeSolicitante[3], false, false, false, true);
    await chamadoPage.clicarBotaoAbrirEncerramento();

    await t.expect(Selector('.notification.failure').innerText).contains("Erro: Esta operação não pode ser realizada para um Chamado de Status FINALIZADO.");

});

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamado('CT30 TESTE ENCERRAR CHAMADO', segmento, tipoChamado, assuntoChamadoSemGasista, unidadeOrganizacional, canalAtendimento, null, 
        nomeSolicitante[4], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, null, null, email, false, null);

    await chamadoPage.clicarBotaoSalvarServicoDisponivel();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[4], true, true, false, false);
    await chamadoPage.clicarBotaoAbrirTramitacao();
    await chamadoPage.preencherChamado('TRAMITANDO CHAMADO', null, null, null, 'UNIDADE TESTE', null, null, null, null, null, null, null, null, null, null, false, null);
    await chamadoPage.clicarBotaoTramitar();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Chamado tramitado(a) com sucesso.");

    await chamadoPage.clicarBotaoAbrirAS();
    await chamadoPage.selecionaEquipeAS('EQUIPE TESTE');
    await chamadoPage.clicarBotaoGerarAS();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Autorização de Serviço Gerada com Sucesso.");

})

test.skip('Tentando ENCERRAR chamado EM ANDAMENTO com AS aberta', async t => {

    await chamadoPage.pesquisarChamado(true, nomeSolicitante[4], false, true, false, false);
    await chamadoPage.clicarBotaoAbrirEncerramento();

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O chamado não pode ser encerrado pois possui autorizações de serviço com status Aberto.");

});