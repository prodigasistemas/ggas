import {Selector} from 'testcafe';
import Page from '../../../ggasPage';
import ChamadoPage from './ChamadoPage';

const page = new Page();
const chamadoPage = new ChamadoPage();

const segmento = 'RESIDENCIAL';
const tipoChamado = 'TESTE ATENDIMENTO PUBLICO';
const assuntoChamado = 'DADOS NAO OBRIGATORIOS';
const assuntoChamadoSemGasista = 'DADOS NAO OBRIGATORIOS SEM GASISTA';
const unidadeOrganizacional = 'GCRC RESIDENCIAL';
const canalAtendimento = 'SAC';
const responsavel = 'ADMINISTRADOR';
const nomeSolicitante = ['CT01', 'CT02', 'CT03', 'CT04', 'CT05'];
const rgSolicitante = '1111111';
const telefoneSolicitante = '8188888888';
const cpfCnpjSolicitante = '42.150.391/0022-03';
const emailSolicitante = 'contato@braskem.com.br';
const funcaoSolicitante = 'ADMINISTRADOR';
const contratoCliente = '201400886';
const nomeCliente = 'BRASKEM SA';
const nomeFantasiaCliente = 'BRASKEM';
const descricaoImovel = 'TESTE';
const documentoValido = './_uploads_/TesteUploadArquivo.pdf';
const descricaoDocumento = 'DOCUMENTO';
const email = 'email@ggas.com.br';
const descricaoGasista = 'Gasista';
const motivoEncerramento = 'CANCELADO CLIENTE';

fixture('Teste de alteração de chamado')
    .page( page.url );

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamado('CT01 Teste alterar chamado', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
    nomeSolicitante[0], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, documentoValido, descricaoDocumento, email, true, descricaoGasista);

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");
})

test.skip('Tentando ALTERAR chamado com dados válidos e com gasista', async t => {

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[0], true, true, false, false);
    await chamadoPage.clicarBotaoAbrirAlteracao();

    //await chamadoPage.checarDadosChamadoAlteracao(false, false, null, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel);
    await chamadoPage.checarDadosSolicitanteAlteracao(false, nomeSolicitante[0], rgSolicitante, telefoneSolicitante, cpfCnpjSolicitante, emailSolicitante, funcaoSolicitante);
    await chamadoPage.checarDadosClienteAlteracao(false, nomeCliente, contratoCliente, nomeFantasiaCliente, cpfCnpjSolicitante, null, null, emailSolicitante, null);
    await chamadoPage.checarDadosImovelAlteracao(descricaoImovel);
    await chamadoPage.checarDadosEmailAlteracao(email);
    await chamadoPage.checarDadosDocumentoAdicionado(descricaoDocumento);
    await chamadoPage.checarDadosGasistaAlteracao(descricaoGasista);

    await chamadoPage.apagarEmail(email);

    await chamadoPage.alterarChamado('CT01 ALTERACAO DE CHAMADO VALIDO', null, null, null);

    await t.expect(Selector('.notification.mensagemSucesso').innerText).contains("Sucesso: Chamado alterado(a) com sucesso.");

});

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamado('CT02 Teste alterar chamado', segmento, tipoChamado, assuntoChamadoSemGasista, unidadeOrganizacional, canalAtendimento, null, 
        null, null, null, null, null, null, null, null, false, null);
    
    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");
})

test.skip('Tentando ALTERAR chamado com dados válidos e sem gasista', async t => {

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[1], true, true, false, false);
    await chamadoPage.clicarBotaoAbrirAlteracao();

    //await chamadoPage.checarDadosChamadoAlteracao(false, false, null, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel);
    await chamadoPage.checarDadosSolicitanteAlteracao(false, null, null, null, null, null, null);
    await chamadoPage.checarDadosClienteAlteracao(false, null, null, null, null, null, null, null, null);
    await chamadoPage.checarDadosImovelAlteracao(null);
    await chamadoPage.checarDadosDocumentoAdicionado(null);
    await chamadoPage.checarDadosGasistaAlteracao(null);

    await chamadoPage.alterarChamado('CT02 ALTERACAO DE CHAMADO VALIDO', documentoValido, descricaoDocumento, email);

    await t.expect(Selector('.notification.mensagemSucesso').innerText).contains("Sucesso: Chamado alterado(a) com sucesso.");

});

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamadoRascunho('CT03 TESTE ALTERAR CHAMADO', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
    nomeSolicitante[2], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, documentoValido, descricaoDocumento, email, true, descricaoGasista);
    
    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");
})

test.skip('Tentando ALTERAR chamado em RASCUNHO', async t => {

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[2], true, true, true, false);
    await chamadoPage.clicarBotaoAbrirAlteracao();

    //await chamadoPage.checarDadosChamadoAlteracao(false, true, 'CT03 TESTE ALTERAR CHAMADO', tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel);
    await chamadoPage.checarDadosSolicitanteAlteracao(true, nomeSolicitante[2], rgSolicitante, telefoneSolicitante, cpfCnpjSolicitante, emailSolicitante, funcaoSolicitante);
    await chamadoPage.checarDadosClienteAlteracao(true, nomeCliente, contratoCliente, nomeFantasiaCliente, cpfCnpjSolicitante, null, null, emailSolicitante, null);
    await chamadoPage.checarDadosImovelAlteracao(descricaoImovel);
    await chamadoPage.checarDadosDocumentoAdicionado(descricaoDocumento);
    await chamadoPage.checarDadosGasistaAlteracao(descricaoGasista);

    await chamadoPage.apagarEmail(email);

    await chamadoPage.alterarChamadoRascunho('CT03 ALTERACAO DE CHAMADO EM RASCUNHO', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
    nomeSolicitante[2], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, documentoValido, descricaoDocumento, email, true, descricaoGasista);

    await t.expect(Selector('.notification.mensagemSucesso').innerText).contains("Sucesso: Chamado alterado(a) com sucesso.");

});

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamado('CT04 Teste alterar chamado', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
    nomeSolicitante[3], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, documentoValido, descricaoDocumento, email, true, descricaoGasista);;
    
    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[3], true, true, false, false);
    await chamadoPage.clicarBotaoAbrirTramitacao();
    await chamadoPage.preencherChamado('TRAMITANDO CHAMADO', null, null, null, 'UNIDADE TESTE', null, null, null, null, null, null, null, null, null, null, false, null);
    await chamadoPage.clicarBotaoTramitar();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Chamado tramitado(a) com sucesso.");
})

test.skip('Tentando ALTERAR chamado em ANDAMENTO', async t => {

    await chamadoPage.pesquisarChamado(true, nomeSolicitante[3], false, true, false, false);
    await chamadoPage.clicarBotaoAbrirAlteracao();

    //await chamadoPage.checarDadosChamadoAlteracao(false, false, null, tipoChamado, assuntoChamado, 'UNIDADE TESTE', canalAtendimento, responsavel);
    await chamadoPage.checarDadosSolicitanteAlteracao(false, nomeSolicitante[3], rgSolicitante, telefoneSolicitante, cpfCnpjSolicitante, emailSolicitante, funcaoSolicitante);
    await chamadoPage.checarDadosClienteAlteracao(false, nomeCliente, contratoCliente, nomeFantasiaCliente, cpfCnpjSolicitante, null, null, emailSolicitante, null);
    await chamadoPage.checarDadosImovelAlteracao(descricaoImovel);
    await chamadoPage.checarDadosDocumentoAdicionado(descricaoDocumento);
    await chamadoPage.checarDadosGasistaAlteracao(descricaoGasista);

    await chamadoPage.alterarChamado('CT04 ALTERACAO DE CHAMADO VALIDO', null, null, null);

    await t.expect(Selector('.notification.mensagemSucesso').innerText).contains("Sucesso: Chamado alterado(a) com sucesso.");

    await chamadoPage.checarDadosChamadoAlteracao(true, false, 'CT04 ALTERACAO DE CHAMADO VALIDO', tipoChamado, assuntoChamado, 'UNIDADE TESTE', canalAtendimento, responsavel);

    await chamadoPage.pesquisarChamado(true, nomeSolicitante[3], false, true, false, false);

    await t.expect(Selector('a.linkStatusChamado').innerText).contains('EM ANDAMENTO');
});

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamado('CT05 Teste alterar chamado', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
    nomeSolicitante[4], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, null, null, email, true, descricaoGasista);;
    
    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[4], true, true, false, false);
    await chamadoPage.clicarBotaoAbrirEncerramento();
    await chamadoPage.preencheChamadoEncerramento('ENCERRANDO CHAMADO', null, null, motivoEncerramento);
    await chamadoPage.clicarBotaoEncerrar();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Chamado encerrado(a) com sucesso.");
})

('Tentando ALTERAR chamado FINALIZADO', async t => {

    await chamadoPage.pesquisarChamado(true, nomeSolicitante[4], false, false, false, true);
    await chamadoPage.clicarBotaoAbrirAlteracao();

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: Esta operação não pode ser realizada para um Chamado de Status FINALIZADO.");
});

test.skip('Tentando ALTERAR chamado sem informar a descrição', async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[0], true, false, false, false);
    await chamadoPage.clicarBotaoAbrirAlteracao();

    await chamadoPage.alterarChamado(null, null, null, null);

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");

});

