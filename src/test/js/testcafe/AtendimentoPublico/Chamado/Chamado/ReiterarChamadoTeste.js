import {Selector} from 'testcafe';
import Page from '../../../ggasPage';
import ChamadoPage from './ChamadoPage';

const page = new Page();
const chamadoPage = new ChamadoPage();

const segmento = 'RESIDENCIAL';
const tipoChamado = 'TESTE ATENDIMENTO PUBLICO';
const assuntoChamado = 'DADOS NAO OBRIGATORIOS';
const unidadeOrganizacional = 'GCRC RESIDENCIAL';
const canalAtendimento = 'SAC';
const responsavel = 'ADMINISTRADOR';
const nomeSolicitante = ['CT00001', 'CT00002A', 'CT00003C', 'CT00004'];
const rgSolicitante = '1111111';
const telefoneSolicitante = '8188888888';
const cpfCnpjSolicitante = '42.150.391/0022-03';
const emailSolicitante = 'contato@braskem.com.br';
const funcaoSolicitante = 'ADMINISTRADOR';
const contratoCliente = '201400886';
const nomeCliente = 'BRASKEM SA';
const nomeFantasiaCliente = 'BRASKEM';
const descricaoImovel = 'TESTE';
const documentoValido = './_uploads_/TesteUploadArquivo.pdf';
const descricaoDocumento = 'DOCUMENTO';
const email = 'email@ggas.com.br';
const descricaoGasista = 'Gasista';
const motivoEncerramento = 'CANCELADO CLIENTE';

fixture('Teste de reiteração de chamado')
    .page( page.url );

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamado('CT01 TESTE REITERAR CHAMADO', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
        nomeSolicitante[0], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, documentoValido, descricaoDocumento, email, true, descricaoGasista);

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");
})

test.skip('Tentando REITERAR chamado ABERTO', async t => {

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[0], true, true, false, false);
    await chamadoPage.clicarBotaoAbrirReiteracao();

    //await chamadoPage.checarDadosChamadoEncerramento(null, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel);
    await chamadoPage.checarDadosSolicitanteAlteracao(false, nomeSolicitante[0], rgSolicitante, telefoneSolicitante, cpfCnpjSolicitante, emailSolicitante, funcaoSolicitante);
    await chamadoPage.checarDadosClienteAlteracao(false, nomeCliente, contratoCliente, nomeFantasiaCliente, cpfCnpjSolicitante, null, null, emailSolicitante, null);
    await chamadoPage.checarDadosImovelAlteracao(descricaoImovel);
    //await chamadoPage.checarDadosEmailAlteracao(email);
    await chamadoPage.checarDadosGasistaAlteracao(descricaoGasista);

    await chamadoPage.preencherDescricao('CT01 REITERANDO CHAMADO')
    await chamadoPage.clicarBotaoReiterar();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Chamado reiterado(a) com sucesso.");

});

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamado('CT02 TESTE REITERAR CHAMADO', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
        nomeSolicitante[1], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, documentoValido, descricaoDocumento, email, true, descricaoGasista);;
    
    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[1], true, true, false, false);
    await chamadoPage.clicarBotaoAbrirTramitacao();
    await chamadoPage.preencherChamado('TRAMITANDO CHAMADO', null, null, null, 'UNIDADE TESTE', null, null, null, null, null, null, null, null, null, null, false, null);
    await chamadoPage.clicarBotaoTramitar();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Chamado tramitado(a) com sucesso.");
})

test.skip('Tentando REITERAR chamado EM ANDAMENTO', async t => {

    await chamadoPage.pesquisarChamado(true, nomeSolicitante[1], false, true, false, false);
    await chamadoPage.clicarBotaoAbrirReiteracao();

    //await chamadoPage.checarDadosChamadoEncerramento(null, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel);
    await chamadoPage.checarDadosSolicitanteAlteracao(false, nomeSolicitante[1], rgSolicitante, telefoneSolicitante, cpfCnpjSolicitante, emailSolicitante, funcaoSolicitante);
    await chamadoPage.checarDadosClienteAlteracao(false, nomeCliente, contratoCliente, nomeFantasiaCliente, cpfCnpjSolicitante, null, null, emailSolicitante, null);
    await chamadoPage.checarDadosImovelAlteracao(descricaoImovel);
    //await chamadoPage.checarDadosEmailAlteracao(email);
    await chamadoPage.checarDadosGasistaAlteracao(descricaoGasista);

    await chamadoPage.preencherDescricao('CT02 REITERANDO CHAMADO')
    await chamadoPage.clicarBotaoReiterar();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Chamado reiterado(a) com sucesso.");

});

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamadoRascunho('CT03 TESTE REITERAR CHAMADO', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
        nomeSolicitante[2], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, documentoValido, descricaoDocumento, email, true, descricaoGasista);
    
    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");
})

('Tentando REITERAR chamado EM RASCUNHO', async t => {

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[2], false, false, true, false);
    await chamadoPage.clicarBotaoAbrirReiteracao();

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: Esta operação não pode ser realizada para um Chamado de Status RASCUNHO.");

});

test.before(async t => {
    await page.fazerLogin('admin', 'admin');
    await chamadoPage.irParaChamado();

    await chamadoPage.incluirChamado('CT04 TESTE REITERAR CHAMADO', segmento, tipoChamado, assuntoChamado, unidadeOrganizacional, canalAtendimento, responsavel, 
        nomeSolicitante[3], rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, documentoValido, descricaoDocumento, email, true, descricaoGasista);;
    
    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");

    await chamadoPage.pesquisarChamado(false, nomeSolicitante[3], true, true, false, false);
    await chamadoPage.clicarBotaoAbrirEncerramento();
    await chamadoPage.preencheChamadoEncerramento('ENCERRANDO CHAMADO',  null, null, motivoEncerramento);
    await chamadoPage.clicarBotaoEncerrar();

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Chamado encerrado(a) com sucesso.");
})

('Tentando TRAMITAR chamado FINALIZADO', async t => {

    await chamadoPage.pesquisarChamado(true, nomeSolicitante[3], false, false, false, true);
    await chamadoPage.clicarBotaoAbrirReiteracao();

    await t.expect(Selector('.notification.failure').innerText).contains("Erro: Esta operação não pode ser realizada para um Chamado de Status FINALIZADO.");

});