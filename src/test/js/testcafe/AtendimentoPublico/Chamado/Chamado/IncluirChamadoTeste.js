import {Selector} from 'testcafe';
import Page from '../../../ggasPage';
import ChamadoPage from './ChamadoPage';

const page = new Page();
const chamadoPage = new ChamadoPage();

const segmento = 'RESIDENCIAL';
const tipoChamado = 'TESTE ATENDIMENTO PUBLICO';
const assuntoDadosNaoObrigatorios = 'DADOS NAO OBRIGATORIOS';
const assuntoDadosObrigatorios = 'DADOS OBRIGATORIOS';
const assuntoSolicitanteObrigatorio = 'SOLICITANTE OBRIGATORIO';
const assuntoClienteObrigatorio = 'CLIENTE OBRIGATORIO';
const assuntoImovelObrigatorio = 'IMOVEL OBRIGATORIO';
const unidadeOrganizacional = 'GCRC RESIDENCIAL';
const canalAtendimento = 'SAC';
const responsavel = 'ADMINISTRADOR';
const nomeSolicitante = 'BRASKEM';
const rgSolicitante = '1111111';
const telefoneSolicitante = '8188888888';
const funcaoSolicitante = 'ADMINISTRADOR';
const nomeCliente = 'BRASKEM';
const documentoValido = './_uploads_/TesteUploadArquivo.pdf';
const descricaoDocumento = 'DOCUMENTO';
const email = 'email@ggas.com.br';
const descricaoGasista = 'Gasista';

fixture('Teste de inclusão de chamado')
    .page( page.url )
    .beforeEach(async t => {
        await page.fazerLogin('admin', 'admin');
        await chamadoPage.irParaChamado();
    });

test('Tentando INCLUIR chamado com dados válidos e sem gasista', async t => {
	await t.expect(chamadoPage.botaoAbrirInclusao.exists).ok();
    await chamadoPage.incluirChamado('CT01 Teste de inclusão de chamado', segmento, tipoChamado, assuntoDadosObrigatorios, unidadeOrganizacional, canalAtendimento, 
                                    null, nomeSolicitante, rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, null, null, email, false, null);

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");
});

test('Tentando INCLUIR chamado com dados válidos e com gasista', async t => {
	await t.expect(chamadoPage.botaoAbrirInclusao.exists).ok();
    await chamadoPage.incluirChamado('CT02 Teste de inclusão de chamado', null, tipoChamado, assuntoDadosNaoObrigatorios, unidadeOrganizacional, canalAtendimento, 
                                    responsavel, nomeSolicitante, rgSolicitante, telefoneSolicitante, funcaoSolicitante, nomeCliente, 
                                    documentoValido, descricaoDocumento, email, true, descricaoGasista);

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");
});

test('Tentando INCLUIR chamado com dados válidos e sem dados não obrigatórios', async t => {
	await t.expect(chamadoPage.botaoAbrirInclusao.exists).ok();
    await chamadoPage.incluirChamado('CT03 Teste de inclusão de chamado', null, tipoChamado, assuntoDadosNaoObrigatorios, unidadeOrganizacional, canalAtendimento, 
                                    responsavel, '', null, null, null, null, null, null, null, true, descricaoGasista);

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("inserido com sucesso.");
});

test('Tentando INCLUIR chamado sem descrição', async t => {
	await t.expect(chamadoPage.botaoAbrirInclusao.exists).ok();
    await chamadoPage.incluirChamado(null, null, tipoChamado, assuntoDadosNaoObrigatorios, unidadeOrganizacional, canalAtendimento, 
                                    responsavel, '', null, null, null, null, null, null, null, true, descricaoGasista);

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");
});

test.skip('Tentando INCLUIR chamado sem tipo de chamado', async t => {

    await page.aguardarCarregarPagina("Pesquisar Chamado", Selector('div.card').find('h5'));


    await chamadoPage.incluirChamado('CT05 Teste de inclusão de chamado', null, null, null, unidadeOrganizacional, canalAtendimento, 
                                    null, '', null, null, null, null, null, null, null, false, null);

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Assunto do Chamado, Tipo de Chamado é (são) de preenchimento obrigatório.");
});

test('Tentando INCLUIR chamado com tipo inválido', async t => {

    const tipoDoChamado = chamadoPage.selectChamadoTipoOption.withText(tipoChamado);

    await page.aguardarCarregarPagina("Pesquisar Chamado", Selector('div.card').find('h5'));

    await chamadoPage.clicarBotaoAbrirInclusao();
    await chamadoPage.selecionaSegmento('COMERCIAL');
    
    await t.expect(tipoDoChamado.exists).notOk();
});

test('Tentando INCLUIR chamado com tipo inativo', async t => {

    const tipoChamado = chamadoPage.selectChamadoTipoOption.withText('TIPO INATIVO');

    await page.aguardarCarregarPagina("Pesquisar Chamado", Selector('div.card').find('h5'));


    await chamadoPage.clicarBotaoAbrirInclusao();
    await chamadoPage.selecionaSegmento(segmento);
    
    await t.expect(tipoChamado.exists).notOk();
});

test.skip('Tentando INCLUIR chamado sem assunto', async t => {

    await page.aguardarCarregarPagina("Pesquisar Chamado", Selector('div.card').find('h5'));

    await chamadoPage.incluirChamado('CT08 Teste de inclusão de chamado', null, tipoChamado, 'Selecione um assunto do chamado', unidadeOrganizacional, canalAtendimento, 
                                    null, '', null, null, null, null, null, null, null, false, null);

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Assunto do Chamado é (são) de preenchimento obrigatório.");
});

test('Tentando INCLUIR chamado com assunto inválido', async t => {

    const assuntoChamadoOption = chamadoPage.selectChamadoAssuntoOption;
    const assuntoChamadoInvalido = assuntoChamadoOption.withText(assuntoClienteObrigatorio);
    const assuntoChamadoValido = assuntoChamadoOption.withText('ASSUNTO RESIDENCIAL');

    await page.aguardarCarregarPagina("Pesquisar Chamado", Selector('div.card').find('h5'));

    await chamadoPage.clicarBotaoAbrirInclusao();

    await chamadoPage.selecionaSegmento(segmento);
    await chamadoPage.selecionaChamadoTipo(tipoChamado);
    await chamadoPage.selecionaAssunto(assuntoClienteObrigatorio);
    await chamadoPage.selecionaChamadoTipo('TESTE TIPO INVALIDO');

    await t
        .expect(assuntoChamadoOption.count).eql(2)
        .expect(assuntoChamadoInvalido.exists).notOk()
        .expect(assuntoChamadoValido.exists).ok();
});

test('Tentando INCLUIR chamado sem unidade organizacional', async t => {
	await t.expect(chamadoPage.botaoAbrirInclusao.exists).ok();
    await chamadoPage.incluirChamado('CT10 Teste de inclusão de chamado', null, tipoChamado, assuntoDadosNaoObrigatorios, 'Selecione uma unidade...', canalAtendimento, 
                                    responsavel, '', null, null, null, null, null, null, null, true, descricaoGasista);

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Unidade Organizacional é (são) de preenchimento obrigatório.");
});


test('Tentando INCLUIR chamado sem canal de atendimento', async t => {
	await t.expect(chamadoPage.botaoAbrirInclusao.exists).ok();
    await chamadoPage.incluirChamado('CT11 Teste de inclusão de chamado', null, tipoChamado, assuntoDadosNaoObrigatorios, unidadeOrganizacional, 'Selecione um canal de atendimento...', 
                                    responsavel, '', null, null, null, null, null, null, null, true, descricaoGasista);

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Canal de Atendimento é (são) de preenchimento obrigatório.");
});


test('Tentando INCLUIR chamado com responsável inválido', async t => {

    const responsavelOption = chamadoPage.selectUsuarioResponsavelOption;
    const responsavelPadrao = responsavelOption.withText('Selecione');
    const responsavelValido = responsavelOption.withText(responsavel);

    await page.aguardarCarregarPagina("Pesquisar Chamado", Selector('div.card').find('h5'));

    await chamadoPage.clicarBotaoAbrirInclusao();

    await chamadoPage.selecionaSegmento(segmento);
    await chamadoPage.selecionaChamadoTipo(tipoChamado);
    await chamadoPage.selecionaAssunto(assuntoDadosNaoObrigatorios);

    await t
        .expect(responsavelOption.count).eql(2)
        .expect(responsavelPadrao.exists).ok()
        .expect(responsavelValido.exists).ok();
});


test('Tentando INCLUIR chamado sem dados de solicitante obrigatórios', async t => {
	await t.expect(chamadoPage.botaoAbrirInclusao.exists).ok();
    await chamadoPage.incluirChamado('CT13 Teste de inclusão de chamado', null, tipoChamado, assuntoSolicitanteObrigatorio, unidadeOrganizacional, canalAtendimento, 
                                    responsavel, '', null, null, null, null, null, null, null, false, null);

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Nome do solicitante, CPF/CNPJ do solicitante, RG do solicitante, Telefone do solicitante, E-mail do solicitante é (são) de preenchimento obrigatório.");
});

test('Tentando INCLUIR chamado sem dados de cliente obrigatórios', async t => {
	await t.expect(chamadoPage.botaoAbrirInclusao.exists).ok();
    await chamadoPage.incluirChamado('CT14 Teste de inclusão de chamado', null, tipoChamado, assuntoClienteObrigatorio, unidadeOrganizacional, canalAtendimento, 
                                    responsavel, '', null, null, null, null, null, null, null, false, null);

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Cliente é (são) de preenchimento obrigatório.");
});

test('Tentando INCLUIR chamado sem dados de imóvel obrigatórios', async t => {
	await t.expect(chamadoPage.botaoAbrirInclusao.exists).ok();
    await chamadoPage.incluirChamado('CT15 Teste de inclusão de chamado', null, tipoChamado, assuntoImovelObrigatorio, unidadeOrganizacional, canalAtendimento, 
                                    responsavel, '', null, null, null, null, null, null, null, false, null);

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Imóvel é (são) de preenchimento obrigatório.");
});

test('Tentando INCLUIR chamado com documento com 0 byte', async t => {

    await page.aguardarCarregarPagina("Pesquisar Chamado", Selector('div.card').find('h5'));

    await chamadoPage.clicarBotaoAbrirInclusao();
    await chamadoPage.preencherDocumento('./_uploads_/TesteDocumentoVazio.txt', 'documento 0 bytes');

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Incluir Documento é (são) de preenchimento obrigatório.");
});

test('Tentando INCLUIR chamado com documento > 10240KB', async t => {

    await page.aguardarCarregarPagina("Pesquisar Chamado", Selector('div.card').find('h5'));

    await chamadoPage.clicarBotaoAbrirInclusao();
    await chamadoPage.preencherDocumento('./_uploads_/TesteDocumentoGrande.jpg', 'documento > 10240KB');

    await t
        .expect(Selector('#documentoAnexoFeedback').innerText)
        .contains("O tamanho do arquivo (15510.50 kb) excede o limite (10240.00 kb)");
});

test('Tentando INCLUIR chamado com extensão inválida', async t => {

    await page.aguardarCarregarPagina("Pesquisar Chamado", Selector('div.card').find('h5'));

    await chamadoPage.clicarBotaoAbrirInclusao();
    await chamadoPage.preencherDocumento('ChamadoPage.js', 'documento com extensão inválida');

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: Escolha um Arquivo com uma das seguintes extensões: .pdf .txt .doc .docx .xls .xlsx .png .jpg .jpeg .bmp .ppt .pptx .");
});

test.skip('Tentando INCLUIR chamado com e-mail inválido', async t => {

    await chamadoPage.clicarBotaoAbrirInclusao();
    await chamadoPage.preencherEmail('teste!@email.com');

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: E-mail(s) inválido(s).");

    await chamadoPage.preencherEmail('testeemail.com');

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: E-mail(s) inválido(s).");
});

test.skip('Tentando INCLUIR chamado sem dados obrigatórios de gasista/plantonista', async t => {

    await page.aguardarCarregarPagina("Pesquisar Chamado", Selector('div.card').find('h5'));

    await chamadoPage.incluirChamado('CT21 Teste de inclusão de chamado', null, tipoChamado, assuntoDadosNaoObrigatorios, unidadeOrganizacional, canalAtendimento, 
                                    responsavel, '', null, null, null, null, null, null, false, '');

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: É preciso informar se houve necessidade de acionamento de gasista ou plantonista.");

});
