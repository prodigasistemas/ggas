import { Selector, t } from 'testcafe';
import Page from '../../../ggasPage';

export default class ChamadoAssuntoPage extends Page {

    constructor() {
        super();

        this.descricaoChamadoTipo = Selector('#descricao');
        this.segmentoChamadoTipo = Selector('#idSegmentoChamadoTipo');
        this.categoriaChamadoTipo = Selector('#categoriaChamado');
        this.situacaoAtivo = Selector('#situacaoAtivo');
        this.situacaoInativo = Selector('#situacaoInativo');
        this.situacaoTodos = Selector('#situacaoTodos');
        this.habilitadoSim = Selector('#habilitadoSim');
        this.habilitadoNao = Selector('#habilitadoNao');
        this.descricaoAssunto = Selector('#descricaoAssunto');
        this.quantidadeHorasPrevistaAtendimento = Selector('#quantidadeHorasPrevistaAtendimento');
        this.prazoDiferenciadoSim = Selector('#indicadorPrazoDiferenciadoSim');
        this.prazoDiferenciadoNao = Selector('#indicadorPrazoDiferenciadoNao');
        this.diaCorrido = Selector('#indicadorDiaCorridoUtil1');
        this.diaUtil = Selector('#indicadorDiaCorridoUtil2');
        this.indicadorCreditoSim = Selector('#indicadorCredito1');
        this.indicadorCreditoNao = Selector('#indicadorCredito2');
        this.rubricaCredito = Selector('#rubricaCredito');
        this.indicadorDebitoSim = Selector('#indicadorDebito1');
        this.indicadorDebitoNao = Selector('#indicadorDebito2');
        this.rubricaDebito = Selector('#rubricaDebito');
        this.canalAtendimento = Selector('#canalAtendimento');
        this.unidadeOrganizacional = Selector('#unidadeOrganizacional');
        this.usuarioResponsavel = Selector('#usuarioResponsavel');
        this.indicadorGeraServicoAutorizacaoSim = Selector('#indicadorGeraServicoAutorizacao1');
        this.indicadorGeraServicoAutorizacaoNao = Selector('#indicadorGeraServicoAutorizacao2');
        this.indicadorFechamentoAutomaticoSim = Selector('#indicadorFechamentoAutomatico1');
        this.indicadorFechamentoAutomaticoNao = Selector('#indicadorFechamentoAutomatico2');
        this.indicadorTramitacaoSim = Selector('#indicadorTramitacao1');
        this.indicadorTramitacaoNao = Selector('#indicadorTramitacao2');
        this.indicadorVerificaRestServSim = Selector('#indicadorVerificaRestServ1');
        this.indicadorVerificaRestServNao = Selector('#indicadorVerificaRestServ2');
        this.indicadorEncerraAutoASRelSim = Selector('#indicadorEncerraAutoASRel1');
        this.indicadorEncerraAutoASRelNao = Selector('#indicadorEncerraAutoASRel2');
        this.indicadorVerificarVazamentoSim = Selector('#indicadorVerificarVazamento1');
        this.indicadorVerificarVazamentoNao = Selector('#indicadorVerificarVazamento2');
        this.indicadorAcionamentoGasistaSim = Selector('#indicadorAcionamentoGasistaSim');
        this.indicadorAcionamentoGasistaNao = Selector('#indicadorAcionamentoGasistaNao');
        this.questionario = Selector('#questionario');
        this.indicadorUnidadesOrganizacionalVisualizadoraSim = Selector('#indicadorUnidadesOrganizacionalVisualizadoraSim');
        this.indicadorUnidadesOrganizacionalVisualizadoraNao = Selector('#indicadorUnidadesOrganizacionalVisualizadoraNao');
        this.unidadeOrganizacionalVisualizadoras = Selector('#unidadeOrganizacionalVisualizadoras');
        this.permiteAbrirEmLoteSim = Selector('#permiteAbrirEmLoteSim');
        this.permiteAbrirEmLoteNao = Selector('#permiteAbrirEmLoteNao');
        this.indicadorComGarantiaSim = Selector('#indicadorComGarantiaSim');
        this.indicadorComGarantiaNao = Selector('#indicadorComGarantiaNao');
        this.indicadorClienteObrigatorioSim = Selector('#indicadorClienteObrigatorio1');
        this.indicadorClienteObrigatorioNao = Selector('#indicadorClienteObrigatorio2');
        this.indicadorImovelObrigatorioSim = Selector('#indicadorImovelObrigatorio1');
        this.indicadorImovelObrigatorioNao = Selector('#indicadorImovelObrigatorio2');
        this.indicadorRgSolicitanteObrigatorioSim = Selector('#indicadorRgSolicitanteObrigatorio1');
        this.indicadorRgSolicitanteObrigatorioNao = Selector('#indicadorRgSolicitanteObrigatorio2');
        this.indicadorCpfCnpjSolicitanteObrigatorioSim = Selector('#indicadorCpfCnpjSolicitanteObrigatorio1');
        this.indicadorCpfCnpjSolicitanteObrigatorioNao = Selector('#indicadorCpfCnpjSolicitanteObrigatorio2');
        this.indicadorNomeSolicitanteObrigatorioSim = Selector('#indicadorNomeSolicitanteObrigatorio1');
        this.indicadorNomeSolicitanteObrigatorioNao = Selector('#indicadorNomeSolicitanteObrigatorio2');
        this.indicadorTelefoneSolicitanteObrigatorioSim = Selector('#indicadorTelefoneSolicitanteObrigatorio1');
        this.indicadorTelefoneSolicitanteObrigatorioNao = Selector('#indicadorTelefoneSolicitanteObrigatorio2');
        this.indicadorEmailSolicitanteObrigatorioSim = Selector('#indicadorEmailSolicitanteObrigatorio1');
        this.indicadorEmailSolicitanteObrigatorioNao = Selector('#indicadorEmailSolicitanteObrigatorio2');
        this.enviarEmailCadastroRESPONSAVEL = Selector('#enviarEmailCadastroRESPONSAVEL');
        this.enviarEmailCadastroUNIDADE_SUPERIOR = Selector('#enviarEmailCadastroUNIDADE_SUPERIOR');
        this.enviarEmailCadastroUNIDADES_VISUALIZADORAS = Selector('#enviarEmailCadastroUNIDADES_VISUALIZADORAS');
        this.enviarEmailTramitarRESPONSAVEL = Selector('#enviarEmailTramitarRESPONSAVEL');
        this.enviarEmailTramitarUNIDADE_SUPERIOR = Selector('#enviarEmailTramitarUNIDADE_SUPERIOR');
        this.enviarEmailTramitarUNIDADES_VISUALIZADORAS = Selector('#enviarEmailTramitarUNIDADES_VISUALIZADORAS');
        this.enviarEmailReiterarRESPONSAVEL = Selector('#enviarEmailReiterarRESPONSAVEL');
        this.enviarEmailReiterarUNIDADE_SUPERIOR = Selector('#enviarEmailReiterarUNIDADE_SUPERIOR');
        this.enviarEmailReiterarUNIDADES_VISUALIZADORAS = Selector('#enviarEmailReiterarUNIDADES_VISUALIZADORAS');
        this.enviarEmailEncerrarRESPONSAVEL = Selector('#enviarEmailEncerrarRESPONSAVEL');
        this.enviarEmailEncerrarUNIDADE_SUPERIOR = Selector('#enviarEmailEncerrarUNIDADE_SUPERIOR');
        this.enviarEmailEncerrarUNIDADES_VISUALIZADORAS = Selector('#enviarEmailEncerrarUNIDADES_VISUALIZADORAS');
        this.enviarEmailPrioridadeProtocoloAMBOS = Selector('#enviarEmailPrioridadeProtocoloAMBOS');
        this.enviarEmailPrioridadeProtocoloCLIENTE = Selector('#enviarEmailPrioridadeProtocoloCLIENTE');
        this.enviarEmailPrioridadeProtocoloSOLICITANTE = Selector('#enviarEmailPrioridadeProtocoloSOLICITANTE');
        this.orientacaoAssunto = Selector('#orientacaoAssunto');
        this.servicoTipo = Selector('#servicoTipo');

        this.abaCaracteristicas = Selector('#tab-caracteristicas');
        this.abaObrigatoriedade = Selector('#tab-campo-obrigatorio');
        this.abaEnvioEmail = Selector('#tab-envio-email');
        this.abaServicos = Selector('#tab-servicos');

        this.botaoAdicionarServicoTipo = Selector('#botaoIncluirTurno');
        this.botaoIncluirAssunto = Selector('#botaoIncluirAssunto');
        this.botaoPesquisar = Selector('#botaoPesquisar');
        this.botaoAlterarAssunto = Selector('#botaoAlterarAssunto');
        this.botaoAlterarChamadoAssunto = Selector('button').withAttribute('onclick', 'alterar();');
        this.botaoSalvar = Selector('#botaoSalvar');
        this.botaoIncluir = Selector('#botaoIncluir');
        this.botaoRemover = Selector('#buttonRemover');
    }

    async irParaChamadoAssunto() {
        await t.navigateTo(this.url + '/exibirPesquisaChamadoAssunto');
    }

    async clicarBotaoIncluir() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoIncluir);
    }

    async clicarBotaoRemover() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }

    async clicarBotaoSalvar() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoSalvar);
    }

    async clicarBotaoIncluirAssunto() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoIncluirAssunto);
    }

    async incluirServicoTipo(servico) {
        if(servico != null) {
            this.selecionarOpcao(this.servicoTipo, servico);
            this.clicarBotao(this.botaoAdicionarServicoTipo);
        }
    }

    async desmarcarEnvioEmail() {

        this.clicarRadioCheckBox(this.enviarEmailCadastroRESPONSAVEL);
        this.clicarRadioCheckBox(this.enviarEmailCadastroUNIDADES_VISUALIZADORAS);
        this.clicarRadioCheckBox(this.enviarEmailTramitarRESPONSAVEL);
        this.clicarRadioCheckBox(this.enviarEmailTramitarUNIDADES_VISUALIZADORAS);
        this.clicarRadioCheckBox(this.enviarEmailReiterarRESPONSAVEL);
        this.clicarRadioCheckBox(this.enviarEmailReiterarUNIDADES_VISUALIZADORAS);
        this.clicarRadioCheckBox(this.enviarEmailEncerrarRESPONSAVEL);
        this.clicarRadioCheckBox(this.enviarEmailEncerrarUNIDADES_VISUALIZADORAS);

    }

    async marcarEnvioEmail() {
        this.clicarRadioCheckBox(this.enviarEmailCadastroUNIDADE_SUPERIOR);
        this.clicarRadioCheckBox(this.enviarEmailTramitarUNIDADE_SUPERIOR);
        this.clicarRadioCheckBox(this.enviarEmailReiterarUNIDADE_SUPERIOR);
        this.clicarRadioCheckBox(this.enviarEmailEncerrarUNIDADE_SUPERIOR);
    }

    async preencherAssuntoIndicadoresNao(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento, unidade, 
        usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao) {

        this.clicarElemento(this.abaCaracteristicas);
        this.preencherInput(this.descricaoAssunto, descricaoAssunto);
        this.preencherInput(this.quantidadeHorasPrevistaAtendimento, quantidadeHorasAtendimento);
        this.clicarRadioCheckBox(this.permiteAbrirEmLoteSim);
        this.selecionarOpcao(this.canalAtendimento, canalAtendimento);
        this.selecionarOpcao(this.unidadeOrganizacional, unidade);
        this.selecionarOpcao(this.usuarioResponsavel, usuarioResponsavel);
        this.selecionarOpcao(this.questionario, questionario);
        this.selecionarOpcao(this.rubricaCredito, rubricaCredito);
        this.selecionarOpcao(this.rubricaDebito, rubricaDebito);
        this.selecionarOpcao(this.unidadeOrganizacionalVisualizadoras, unidadeVisualizadora);

        this.clicarElemento(this.abaEnvioEmail);
        this.preencherInput(this.orientacaoAssunto, orientacao);

        this.clicarElemento(this.abaServicos);
        this.incluirServicoTipo(servicoTipo);
    }

    async preencherAssuntoIndicadoresSim(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento, unidade, 
        usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao) {

        this.clicarElemento(this.abaObrigatoriedade);
        this.clicarRadioCheckBox(this.indicadorClienteObrigatorioSim);
        this.clicarRadioCheckBox(this.indicadorImovelObrigatorioSim);

        this.clicarElemento(this.abaCaracteristicas);
        this.clicarRadioCheckBox(this.indicadorCreditoSim);
        this.clicarRadioCheckBox(this.indicadorDebitoSim);
        if(unidadeVisualizadora != null) {
            this.clicarRadioCheckBox(this.indicadorUnidadesOrganizacionalVisualizadoraSim);
        }

        this.preencherAssuntoIndicadoresNao(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento, unidade, 
            usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao);

        this.clicarElemento(this.abaCaracteristicas);
        this.clicarRadioCheckBox(this.permiteAbrirEmLoteNao);
        this.clicarRadioCheckBox(this.indicadorUnidadesOrganizacionalVisualizadoraSim);
        this.clicarRadioCheckBox(this.prazoDiferenciadoSim);
        this.clicarRadioCheckBox(this.diaCorrido);
        this.clicarRadioCheckBox(this.indicadorGeraServicoAutorizacaoSim);
        this.clicarRadioCheckBox(this.indicadorFechamentoAutomaticoSim);
        this.clicarRadioCheckBox(this.indicadorTramitacaoSim);
        this.clicarRadioCheckBox(this.indicadorVerificaRestServSim);
        this.clicarRadioCheckBox(this.indicadorEncerraAutoASRelSim);
        this.clicarRadioCheckBox(this.indicadorVerificarVazamentoSim);
        this.clicarRadioCheckBox(this.indicadorAcionamentoGasistaSim);
        this.clicarRadioCheckBox(this.indicadorComGarantiaSim);

        this.clicarElemento(this.abaObrigatoriedade);
        this.clicarRadioCheckBox(this.indicadorRgSolicitanteObrigatorioSim);
        this.clicarRadioCheckBox(this.indicadorCpfCnpjSolicitanteObrigatorioSim);
        this.clicarRadioCheckBox(this.indicadorNomeSolicitanteObrigatorioSim);
        this.clicarRadioCheckBox(this.indicadorTelefoneSolicitanteObrigatorioSim);
        this.clicarRadioCheckBox(this.indicadorEmailSolicitanteObrigatorioSim);

        this.clicarElemento(this.abaEnvioEmail);
        this.marcarEnvioEmail();
        //this.desmarcarEnvioEmail();
    }

    async preencherChamadoAssunto(descricaoChamadoTipo, segmento, categoria, ativo) {

        this.selecionarOpcao(this.segmentoChamadoTipo, segmento);
        this.selecionarOpcao(this.categoriaChamadoTipo, categoria);
        this.selecionarIndicadorUso(ativo);
        this.preencherInput(this.descricaoChamadoTipo, descricaoChamadoTipo);
    }

    /**
     * Inclui um tipo de chamado com assunto com os parâmetros da aba Características marcados com Não exceto
     * o campo "Permite abrir chamado em lote?"
     * 
     * @param {*} descricaoAssunto 
     * @param {*} quantidadeHorasAtendimento 
     * @param {*} rubricaCredito 
     * @param {*} rubricaDebito 
     * @param {*} canalAtendimento 
     * @param {*} unidade 
     * @param {*} usuarioResponsavel 
     * @param {*} questionario 
     * @param {*} unidadeVisualizadora 
     * @param {*} servicoTipo 
     * @param {*} orientacao 
     * @param {*} descricaoChamadoTipo 
     * @param {*} segmento 
     * @param {*} categoria 
     */
    async incluirChamadoAssuntoIndicadoresNao(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidade, usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao, descricaoChamadoTipo, segmento, categoria) {

            this.clicarBotao(this.botaoIncluir);
            this.clicarRadioCheckBox(this.permiteAbrirEmLoteSim);
            this.preencherAssuntoIndicadoresNao(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento, unidade, 
                usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao);
            this.clicarElemento(this.abaEnvioEmail);
            this.desmarcarEnvioEmail();
            this.clicarBotao(this.botaoIncluirAssunto);
            this.preencherChamadoAssunto(descricaoChamadoTipo, segmento, categoria, null);
            this.clicarBotao(this.botaoSalvar);
    }

    /**
     * Inclui um tipo de chamado com assunto com os parâmetros da aba Características marcados com Sim exceto
     * o campo "Permite abrir chamado em lote?"
     * 
     * @param {*} descricaoAssunto 
     * @param {*} quantidadeHorasAtendimento 
     * @param {*} rubricaCredito 
     * @param {*} rubricaDebito 
     * @param {*} canalAtendimento 
     * @param {*} unidade 
     * @param {*} usuarioResponsavel 
     * @param {*} questionario 
     * @param {*} unidadeVisualizadora 
     * @param {*} servicoTipo 
     * @param {*} orientacao 
     * @param {*} descricaoChamadoTipo 
     * @param {*} segmento 
     * @param {*} categoria 
     */
    async incluirChamadoAssuntoIndicadoresSim(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidade, usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao, descricaoChamadoTipo, segmento, categoria) {

            this.clicarBotao(this.botaoIncluir);
            this.preencherAssuntoIndicadoresSim(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento, unidade, 
                usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao);
            this.clicarBotao(this.botaoIncluirAssunto);
            this.preencherChamadoAssunto(descricaoChamadoTipo, segmento, categoria, null);
            this.clicarBotao(this.botaoSalvar);
    }

    async checarDadosChamadoTipo(descricaoChamadoTipo, segmento, categoria, ativo, habilitado) {

        this.pesquisarChamadoAssunto(descricaoChamadoTipo, ativo);
        this.clicarElemento(Selector('a').withText(descricaoChamadoTipo));
        this.checarInput(this.descricaoChamadoTipo, descricaoChamadoTipo, false);
        this.checarInput(this.segmentoChamadoTipo, segmento, false);
        this.checarInput(this.categoriaChamadoTipo, categoria, false);
        this.checarSituacao(ativo, habilitado);
    }

    async checarChamadoAssuntoIndicadoresNao(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidade, usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao, descricaoChamadoTipo, segmento, categoria, ativo, habilitado) {
        
        this.checarDadosChamadoTipo(descricaoChamadoTipo, segmento, categoria, ativo, habilitado);

        this.clicarElemento(Selector('td').withText(descricaoAssunto).prevSibling(0).find('input'));
        
        this.checarAbaCaracteristicas(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
            unidade, usuarioResponsavel, questionario, unidadeVisualizadora, false);

        this.checarAbaObrigatoriedade(false);
        this.checarAbaEnvioEmail(false, orientacao);

        this.clicarElemento(this.abaServicos);
        this.checarServico(servicoTipo, '1');
    }

    async checarChamadoAssuntoIndicadoresSim(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidade, usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao, descricaoChamadoTipo, segmento, categoria, ativo, habilitado) {
        
        this.checarDadosChamadoTipo(descricaoChamadoTipo, segmento, categoria, ativo, habilitado);

        this.clicarElemento(Selector('td').withText(descricaoAssunto).prevSibling(0).find('input'));
        
        this.checarAbaCaracteristicas(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
            unidade, usuarioResponsavel, questionario, unidadeVisualizadora, true);
        this.checarAbaObrigatoriedade(true);
        this.checarAbaEnvioEmail(true, orientacao);

        this.clicarElemento(this.abaServicos);
        this.checarServico(servicoTipo, '1');
    }

    async pesquisarChamadoAssunto(descricao, ativo) {

        this.preencherInput(this.descricaoChamadoTipo, descricao);
        if(ativo == null) {
            this.clicarRadioCheckBox(this.situacaoTodos);
        }
        else if(ativo) {
            this.clicarRadioCheckBox(this.situacaoAtivo);
        } else {
            this.clicarRadioCheckBox(this.situacaoInativo);
        }
        this.clicarBotao(this.botaoPesquisar);
    }

    async checarSituacao(status, enabled) {

        if(status) {
            this.checarRadioCheckboxMarcado(this.habilitadoSim, enabled);
        } else {
            this.checarRadioCheckboxMarcado(this.habilitadoNao, enabled);
        }
    }

    async checarLabel(label, valor) {

        if(label != null) {
            await t.expect(Selector('label').withText(label).nextSibling().innerText).contains(valor);
        }
    }

    async checarServico(servico) {

        if(servico != null) {
            await t.expect(Selector('td').withText(servico).exists).ok();
        }
    }

    async checarAbaCaracteristicas(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidade, usuarioResponsavel, questionario, unidadeVisualizadora, indicadores) {

        this.clicarElemento(this.abaCaracteristicas);
        this.checarInput(this.descricaoAssunto, descricaoAssunto, false);
        this.checarInput(this.quantidadeHorasPrevistaAtendimento, quantidadeHorasAtendimento, false);
        this.checarInput(this.rubricaCredito, rubricaCredito, false);
        this.checarInput(this.rubricaDebito, rubricaDebito, false);
        this.checarInput(this.canalAtendimento, canalAtendimento, false);
        this.checarInput(this.unidadeOrganizacional, unidade, false);
        this.checarInput(this.questionario, questionario, false);
        this.checarInput(this.usuarioResponsavel, usuarioResponsavel, false);
        this.checarSelectMultiplo(this.unidadeOrganizacionalVisualizadoras, unidadeVisualizadora, false);

        if(indicadores) {
            this.checarRadioCheckboxMarcado(this.indicadorGeraServicoAutorizacaoSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorFechamentoAutomaticoSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorTramitacaoSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorVerificaRestServSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorEncerraAutoASRelSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorVerificarVazamentoSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorAcionamentoGasistaSim, false);
            this.checarRadioCheckboxMarcado(this.permiteAbrirEmLoteNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorComGarantiaSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorUnidadesOrganizacionalVisualizadoraSim, false);
            this.checarRadioCheckboxMarcado(this.prazoDiferenciadoSim, false);
            this.checarRadioCheckboxMarcado(this.diaCorrido, false);
            this.checarRadioCheckboxMarcado(this.indicadorCreditoSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorDebitoSim, false);
        } else {
            this.checarRadioCheckboxMarcado(this.indicadorGeraServicoAutorizacaoNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorFechamentoAutomaticoNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorTramitacaoNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorVerificaRestServNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorEncerraAutoASRelNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorVerificarVazamentoNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorAcionamentoGasistaNao, false);
            this.checarRadioCheckboxMarcado(this.permiteAbrirEmLoteSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorComGarantiaNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorUnidadesOrganizacionalVisualizadoraNao, false);
            this.checarRadioCheckboxMarcado(this.prazoDiferenciadoNao, false);
            this.checarRadioCheckboxMarcado(this.diaUtil, false);
            this.checarRadioCheckboxMarcado(this.indicadorCreditoNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorDebitoNao, false);
        }
    }

    async checarAbaEnvioEmail(todasUnidades, orientacao) {
        if(todasUnidades) {
            this.clicarElemento(this.abaEnvioEmail);
            this.checarLabel('Enviar Email ao cadastrar Chamado:', 'Unidade Organizacional');
            this.checarLabel('Enviar Email ao cadastrar Chamado:', 'Unidades Visualizadoras');
            this.checarLabel('Enviar Email ao cadastrar Chamado:', 'Unidade Superior');

            this.checarLabel('Enviar Email ao tramitar Chamado:', 'Unidade Organizacional');
            this.checarLabel('Enviar Email ao tramitar Chamado:', 'Unidades Visualizadoras');
            this.checarLabel('Enviar Email ao tramitar Chamado:', 'Unidade Superior');

            this.checarLabel('Enviar Email ao reiterar Chamado:', 'Unidade Organizacional');
            this.checarLabel('Enviar Email ao reiterar Chamado:', 'Unidades Visualizadoras');
            this.checarLabel('Enviar Email ao reiterar Chamado:', 'Unidade Superior');

            this.checarLabel('Enviar Email ao encerrar Chamado:', 'Unidade Organizacional');
            this.checarLabel('Enviar Email ao encerrar Chamado:', 'Unidades Visualizadoras');
            this.checarLabel('Enviar Email ao encerrar Chamado:', 'Unidade Superior');
            
            this.checarInput(this.orientacaoAssunto, orientacao, false);
        } else {
            this.clicarElemento(this.abaEnvioEmail);
            this.checarLabel('Enviar Email ao cadastrar Chamado:', '');
            this.checarLabel('Enviar Email ao tramitar Chamado:', '');
            this.checarLabel('Enviar Email ao reiterar Chamado:', '');
            this.checarLabel('Enviar Email ao encerrar Chamado:', '');
            this.checarInput(this.orientacaoAssunto, orientacao, false);
        }
    }

    async checarAbaObrigatoriedade(indicadores) {
        if(indicadores) {
            this.clicarElemento(this.abaObrigatoriedade);
            this.checarRadioCheckboxMarcado(this.indicadorClienteObrigatorioSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorImovelObrigatorioSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorRgSolicitanteObrigatorioSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorCpfCnpjSolicitanteObrigatorioSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorNomeSolicitanteObrigatorioSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorTelefoneSolicitanteObrigatorioSim, false);
            this.checarRadioCheckboxMarcado(this.indicadorEmailSolicitanteObrigatorioSim, false);
        } else {
            this.clicarElemento(this.abaObrigatoriedade);
            this.checarRadioCheckboxMarcado(this.indicadorClienteObrigatorioNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorImovelObrigatorioNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorRgSolicitanteObrigatorioNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorCpfCnpjSolicitanteObrigatorioNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorNomeSolicitanteObrigatorioNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorTelefoneSolicitanteObrigatorioNao, false);
            this.checarRadioCheckboxMarcado(this.indicadorEmailSolicitanteObrigatorioNao, false);
        }
        
    }

    async alterarAssunto(descricaoAssunto, novaDescricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidade, usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao) {

        this.clicarElemento(Selector('td').withText(descricaoAssunto).nextSibling(0).find('a'));
        this.preencherAssuntoIndicadoresSim(novaDescricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
            unidade, usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao);
        this.desmarcarEnvioEmail();
        this.clicarBotao(this.botaoAlterarAssunto);
        this.espera(1000);
    }

    async alterarChamadoAssunto(descricaoAssunto, novaDescricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidade, usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao, descricaoChamadoTipo, novadescricaoChamadoTipo, segmento, categoria, ativo) {

        this.pesquisarChamadoAssunto(descricaoChamadoTipo, ativo);
        this.clicarElemento(Selector('a').withText(descricaoChamadoTipo));
        this.clicarBotao(this.botaoAlterarChamadoAssunto);
        this.alterarAssunto(descricaoAssunto, novaDescricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
            unidade, usuarioResponsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao);
        this.preencherChamadoAssunto(novadescricaoChamadoTipo, segmento, categoria, !ativo);
        this.clicarBotao(this.botaoAlterarChamadoAssunto);
    }

    async removerChamadoAssunto(descricaoChamadoTipo, ativo) {

        this.pesquisarChamadoAssunto(descricaoChamadoTipo, ativo);
        this.clicarElemento(Selector('td').withAttribute('tabindex', '0').find('div'));
        this.clicarBotaoRemover();
    }

    async selecionarIndicadorUso(ativo) {
        
        if(ativo != null) {
            if(ativo) {
                this.clicarRadioCheckBox(this.habilitadoSim);
            } else {
                this.clicarRadioCheckBox(this.habilitadoNao);
            } 
        }
    }
    
}