import {Selector} from 'testcafe';
import ChamadoAssuntoPage from './ChamadoAssuntoPage';
import Page from '../../../ggasPage';

const chamadoAssuntoPage = new ChamadoAssuntoPage();
const page = new Page();

/*const descricaoAssunto = 'ASSUNTO CT01';
const descricaoAssunto2 = 'ASSUNTO CT02';
const descricaoAssunto3 = 'ASSUNTO CT03';
const descricaoAssunto4 = 'ASSUNTO CT04';
const quantidadeHorasAtendimento = '8';
const rubricaCredito = 'Credito de volume';
const rubricaDebito = 'ICMS Retido';
const canalAtendimento = 'SAC'
const unidadeOrganizacional = 'GCRC RESIDENCIAL';
const responsavel = 'ADMINISTRADOR';
const questionario = 'QUESTIONARIO';
const unidadeVisualizadora = 'UNIDADE TESTE';
const servicoTipo = 'RELIGACAO NORMAL';
const servicoTipo2 = 'SERVIÇO SEM OBRIGATORIEDADE';
const orientacao = 'ORIENTAÇÃO PARA O CHAMADO';
const descricaoChamadoTipo = 'CHAMADO TIPO CT01';
const descricaoChamadoTipo2 = 'CHAMADO TIPO CT02';
const descricaoChamadoTipo3 = 'CHAMADO TIPO CT03';
const segmento = 'RESIDENCIAL';
const categoria = 'INFORMAÇÃO';*/

const descricaoAssunto = 'ASSUNTO CT01';
const descricaoAssunto2 = 'ASSUNTO CT02';
const descricaoAssunto3 = 'ASSUNTO CT03';
const descricaoAssunto4 = 'ASSUNTO CT04';
const quantidadeHorasAtendimento = '8';
const rubricaCredito = 'Credito de volume';
const rubricaDebito = 'ICMS Retido';
const canalAtendimento = 'SAC';
const unidadeOrganizacional = 'GCRC RESIDENCIAL';
const responsavel = 'RAFAEL CABRAL RIBEIRO';
const questionario = 'QUESTIONÁRIO DE SATISFAÇÃO';
const unidadeVisualizadora = 'DIPON';
const servicoTipo = 'CENSO';
const servicoTipo2 = 'SUSPENSAO INADIMPLENCIA';
const orientacao = 'ORIENTAÇÃO PARA O CHAMADO';
const descricaoChamadoTipo = 'CHAMADO TIPO CT01';
const descricaoChamadoTipo2 = 'CHAMADO TIPO CT02';
const descricaoChamadoTipo3 = 'CHAMADO TIPO CT03';
const segmento = 'RESIDENCIAL';
const categoria = 'INFORMAÇÃO';

fixture('Teste de tipo de chamado com assunto')
    .page( chamadoAssuntoPage.url )
    .beforeEach(async t => {
        await t.maximizeWindow();
        await chamadoAssuntoPage.fazerLogin('admin', 'admin');
        await chamadoAssuntoPage.irParaChamadoAssunto();
    });

test.skip('Tentando INCLUIR tipo de chamado com assunto sem serviço', async t => {

    await chamadoAssuntoPage.incluirChamadoAssuntoIndicadoresNao(descricaoAssunto, quantidadeHorasAtendimento, null, null, null,
        unidadeOrganizacional, null, null, null, null, null, descricaoChamadoTipo, null, null);

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Tipo de Chamado com Assunto inserido(a) com sucesso.");
});

test.skip('Tentando INCLUIR tipo de chamado com assunto com serviço e sem unidade visualizadora', async t => {

    await chamadoAssuntoPage.incluirChamadoAssuntoIndicadoresSim(descricaoAssunto2, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidadeOrganizacional, responsavel, questionario, null, servicoTipo, orientacao, descricaoChamadoTipo2, segmento, categoria);

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Tipo de Chamado com Assunto inserido(a) com sucesso.");
}).after(async t => {
    await chamadoAssuntoPage.checarChamadoAssuntoIndicadoresSim(descricaoAssunto2, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidadeOrganizacional, responsavel, questionario, null, servicoTipo, orientacao, descricaoChamadoTipo2, segmento, categoria, true, false);

    await t.expect(Selector('h5').innerText).contains("Detalhar Chamado com Assunto");
});

test.skip('Tentando INCLUIR tipo de chamado com assunto sem serviço e com unidade visualizadora', async t => {

    await chamadoAssuntoPage.incluirChamadoAssuntoIndicadoresSim(descricaoAssunto3, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidadeOrganizacional, responsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao, descricaoChamadoTipo3, null, null);

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Tipo de Chamado com Assunto inserido(a) com sucesso.");
}).after(async t => {
    await chamadoAssuntoPage.checarChamadoAssuntoIndicadoresSim(descricaoAssunto3, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidadeOrganizacional, responsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao, descricaoChamadoTipo3, null, null, true, false);

    await t.expect(Selector('h5').innerText).contains("Detalhar Chamado com Assunto");
});

test('Tentando INCLUIR tipo de chamado sem descrição', async t => {

    await chamadoAssuntoPage.incluirChamadoAssuntoIndicadoresNao(descricaoAssunto, quantidadeHorasAtendimento, null, null, null,
        unidadeOrganizacional, null, null, null, null, null, null, null, null);

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");

});

test('Tentando INCLUIR tipo de chamado sem assunto', async t => {
    await page.aguardarCarregarPagina("Pesquisar Tipo de Chamado com Assunto", Selector('div.card').find('h5'));
    await chamadoAssuntoPage.clicarBotaoIncluir();
    await chamadoAssuntoPage.preencherChamadoAssunto(descricaoChamadoTipo, segmento, categoria);
    await chamadoAssuntoPage.clicarBotaoSalvar();

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: É necessário ter no mínimo um assunto cadastrado.");

});

test.skip('Tentando INCLUIR assunto sem descrição', async t => {

	await t.expect(chamadoAssuntoPage.botaoIncluir.exists).ok();
    await chamadoAssuntoPage.clicarBotaoIncluir();
    await chamadoAssuntoPage.preencherAssuntoIndicadoresNao(null, quantidadeHorasAtendimento, null, null, null,
        unidadeOrganizacional, null, null, null, null, null, null, null, null);
    await chamadoAssuntoPage.clicarBotaoIncluirAssunto();

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Descrição do Assunto é (são) de preenchimento obrigatório.");

});

test.skip('Tentando INCLUIR assunto sem quantidade de horas previstas', async t => {

    await chamadoAssuntoPage.clicarBotaoIncluir();
    await chamadoAssuntoPage.preencherAssuntoIndicadoresNao(descricaoAssunto, null, null, null, null,
        unidadeOrganizacional, null, null, null, null, null, null, null, null);
    await chamadoAssuntoPage.clicarBotaoIncluirAssunto();

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Quantidade de Horas é (são) de preenchimento obrigatório.");

});

test('Tentando INCLUIR assunto com quantidade de horas previstas inválida', async t => {

    await chamadoAssuntoPage.clicarBotaoIncluir();
    await chamadoAssuntoPage.preencherAssuntoIndicadoresNao(descricaoAssunto, 'horas', null, null, null,
        unidadeOrganizacional, null, null, null, null, null, null, null, null);
    await chamadoAssuntoPage.clicarBotaoIncluirAssunto();

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Quantidade de Horas é (são) de preenchimento obrigatório.");

});

test.skip('Tentando INCLUIR assunto sem rubrica crédito', async t => {

    await chamadoAssuntoPage.clicarBotaoIncluir();
    await chamadoAssuntoPage.preencherAssuntoIndicadoresSim(descricaoAssunto, quantidadeHorasAtendimento, null, rubricaDebito, canalAtendimento,
        unidadeOrganizacional, responsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao);
    await chamadoAssuntoPage.clicarBotaoIncluirAssunto();

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Rubrica Crédito é (são) de preenchimento obrigatório.");
});

test.skip('Tentando INCLUIR assunto sem rubrica débito', async t => {

    await chamadoAssuntoPage.clicarBotaoIncluir();
    await chamadoAssuntoPage.preencherAssuntoIndicadoresSim(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, null, canalAtendimento,
        unidadeOrganizacional, responsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao);
    await chamadoAssuntoPage.clicarBotaoIncluirAssunto();

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Rubrica Débito é (são) de preenchimento obrigatório.");
});

test.skip('Tentando INCLUIR assunto sem unidade organizacional', async t => {

    await chamadoAssuntoPage.clicarBotaoIncluir();
    await chamadoAssuntoPage.preencherAssuntoIndicadoresSim(descricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        null, null, questionario, unidadeVisualizadora, servicoTipo, orientacao);
    await chamadoAssuntoPage.clicarBotaoIncluirAssunto();

    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Unidade Organizacional é (são) de preenchimento obrigatório.");
});

test('Tentando marcar suspeita de vazamento e acionamento de gasista com o assunto permitindo abrir chamado em lote', async t => {

    await chamadoAssuntoPage.clicarBotaoIncluir();
    await chamadoAssuntoPage.clicarRadioCheckBox(chamadoAssuntoPage.permiteAbrirEmLoteSim);
    await chamadoAssuntoPage.clicarRadioCheckBox(chamadoAssuntoPage.indicadorVerificarVazamentoSim);
    await chamadoAssuntoPage.clicarRadioCheckBox(chamadoAssuntoPage.indicadorAcionamentoGasistaSim);

    await t.expect(chamadoAssuntoPage.indicadorVerificarVazamentoNao.withAttribute('checked').exists).ok();
    await t.expect(chamadoAssuntoPage.indicadorVerificarVazamentoNao.withAttribute('disabled').exists).ok();
    await t.expect(chamadoAssuntoPage.indicadorVerificarVazamentoSim.withAttribute('checked').exists).notOk();
    await t.expect(chamadoAssuntoPage.indicadorVerificarVazamentoSim.withAttribute('disabled').exists).ok();

    await t.expect(chamadoAssuntoPage.indicadorAcionamentoGasistaNao.withAttribute('checked').exists).ok();
    await t.expect(chamadoAssuntoPage.indicadorAcionamentoGasistaNao.withAttribute('disabled').exists).ok();
    await t.expect(chamadoAssuntoPage.indicadorAcionamentoGasistaSim.withAttribute('checked').exists).notOk();
    await t.expect(chamadoAssuntoPage.indicadorAcionamentoGasistaSim.withAttribute('disabled').exists).ok();
});

test.skip('Tentando adicionar serviço inválido', async t => {

    await chamadoAssuntoPage.clicarBotaoIncluir();
    await chamadoAssuntoPage.clicarElemento(chamadoAssuntoPage.abaServicos);
    await chamadoAssuntoPage.incluirServicoTipo(servicoTipo2);
    
    await t.expect(chamadoAssuntoPage.servicoTipo.child().withText(servicoTipo).exists).notOk();

});

test.skip('Tentando ALTERAR tipo de chamado com assunto', async t => {

    await chamadoAssuntoPage.alterarChamadoAssunto(descricaoAssunto, novaDescricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidadeOrganizacional, responsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao, descricaoChamadoTipo, novadescricaoChamadoTipo, segmento, categoria, ativo);

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Tipo de Chamado com Assunto alterado(a) com sucesso.");

}).after(async t => {
    await chamadoAssuntoPage.checarChamadoAssuntoIndicadoresSim(novaDescricaoAssunto, quantidadeHorasAtendimento, rubricaCredito, rubricaDebito, canalAtendimento,
        unidadeOrganizacional, responsavel, questionario, unidadeVisualizadora, servicoTipo, orientacao, novadescricaoChamadoTipo, segmento, categoria, !ativo, false);

    await t.expect(Selector('h5').innerText).contains("Detalhar Chamado com Assunto");
});

test.skip('Tentando REMOVER tipo de chamado com assunto', async t => {

    await chamadoAssuntoPage.removerChamadoAssunto(novadescricaoChamadoTipo, !ativo);

    await t
        .expect(Selector('.notification.mensagemSucesso').innerText)
        .contains("Sucesso: Tipo de Chamado com Assunto removida(s) com sucesso.");

}).after(async t => {
    await chamadoAssuntoPage.pesquisarChamadoAssunto(novadescricaoChamadoTipo, null);

    await t
        .expect(Selector('#tableChamadoAssunto').find('td').innerText)
        .contains('Nenhum registro foi encontrado');
})
