import {Selector} from 'testcafe';
import Util from '../../../Util';
import AutorizacaoServicoPage from './AutorizacaoServicoPage';

const autorizacaoServicoPage = new AutorizacaoServicoPage();
const util = new Util();

const descricao = 'AUTORIZAÇÃO DE SERVIÇO CT01';
const descricaoAlteracao = 'ALTERAÇÃO AUTORIZAÇÃO DE SERVIÇO CT01';
const descricaoRemanejamento = 'REMANEJAMENTO AUTORIZAÇÃO DE SERVIÇO CT01';
const servicoTipoSemObrigatoriedade = 'SERVIÇO SEM OBRIGATORIEDADE';
const servicoTipoTudoObrigatorio = 'SERVIÇO TUDO OBRIGATÓRIO';
const servicoTipoClienteObrigatorio = 'SERVIÇO CLIENTE OBRIGATÓRIO';
const servicoTipoImovelObrigatorio = 'SERVIÇO IMÓVEL OBRIGATÓRIO';
const servicoTipoPontoObrigatorio = 'SERVIÇO PONTO OBRIGATÓRIO';
const equipe = 'EQUIPE TESTE';
const equipe2 = 'EQUIPE 2';
const prioridade = 'MEDIA';
const nomeCliente = 'BRASKEM SA';
const imovel1 = 'IMOVEL SEM PONTO DE CONSUMO';
const imovel2 = 'TESTE';
const pontoConsumo2 = 'TESTE';
const dataSolicitacaoRamal = '01/08/2018';
const dataSolicitacaoRamalAlteracao = '01/09/2018';
const orientacaoSemObrigatoriedade = 'OBSERVAÇÃO TIPO SEM OBRIGATORIEDADE';
const orientacaoTudoObrigatorio = 'OBSERVAÇÃO TIPO DE SERVIÇO TUDO OBRIGATÓRIO';
const orientacaoClienteObrigatorio = 'OBSERVAÇÃO TIPO DE SERVIÇO CLIENTE OBRIGATÓRIO';
const orientacaoImovelObrigatorio = 'OBSERVAÇÃO TIPO DE SERVIÇO IMÓVEL OBRIGATÓRIO';
const orientacaoPontoObrigatorio = 'OBSERVAÇÃO TIPO DE SERVIÇO PONTO OBRIGATÓRIO';
const documentoValido = '../../Chamado/Chamado/_uploads_/TesteUploadArquivo.PNG';
const documentoInvalido1 = '../../Chamado/Chamado/_uploads_/TesteDocumentoVazio.txt';
const documentoInvalido2 = '../../Chamado/Chamado/_uploads_/TesteDocumentoGrande.jpg';
const descricaoDocumento = 'DOCUMENTO';
const descricaoDocumento2 = 'REMANEJAMENTO';
const material = 'MATERIAL DEPENDÊNCIA';
const qntMaterial = '2';
const equipamento = 'GUINCHO';
const dataPrevEncerramento = util.dateNow(0,0,2);
const dataPrevEncerramentoAlteracao = util.dateNow(0,0,3);
const motivoEncerramento = "ENCERRAMENTO";
const statusAberta = 'ABERTA';
const statusEncerrada = 'ENCERRADA';


fixture('Autorização de Serviços')
    .page(autorizacaoServicoPage.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await autorizacaoServicoPage.fazerLogin('admin', 'admin');
        await autorizacaoServicoPage.irParaAutorizacaoServico();
    });

test.skip("Tentando INCLUIR Autorização de Serviço válida sem dados não obrigatórios", async t => {
    await autorizacaoServicoPage.incluir(descricao, servicoTipoSemObrigatoriedade, equipe, prioridade, null, null, null, orientacaoSemObrigatoriedade, null, null, null);

    await t
        .expect(Selector(".notification.mensagemSucesso").innerText)
        .eql("Sucesso: Autorização de Serviço inserido(a) com sucesso.");
}).after(async t => {
    await autorizacaoServicoPage.checarAutorizacaoServico(null, servicoTipoSemObrigatoriedade, statusAberta, equipe, prioridade, '', null, null, null, 
        orientacaoSemObrigatoriedade, dataPrevEncerramento, null, null, null, null, null);

})

test.skip("Tentando INCLUIR Autorização de Serviço válida com dados não obrigatórios", async t => {
    await autorizacaoServicoPage.incluir(descricao, servicoTipoSemObrigatoriedade, equipe, prioridade, nomeCliente, 
        imovel2, pontoConsumo2, orientacaoSemObrigatoriedade, dataSolicitacaoRamal, null, null, null, null, null);
    
    await t.expect(Selector(".notification.mensagemSucesso").exists).ok()
    await t
        .expect(Selector(".notification.mensagemSucesso").innerText)
        .eql("Sucesso: Autorização de Serviço inserido(a) com sucesso.");
}).after(async t => {
    await autorizacaoServicoPage.checarAutorizacaoServico(null, servicoTipoSemObrigatoriedade, statusAberta, equipe, prioridade, '', nomeCliente, imovel2, pontoConsumo2, 
        orientacaoSemObrigatoriedade, dataPrevEncerramento, null, null, null, null, null);

})

test("Tentando INCLUIR Autorização de Serviço válida com dados obrigatórios", async t => {
    await autorizacaoServicoPage.incluir(descricao, servicoTipoTudoObrigatorio, equipe, prioridade, nomeCliente, 
        imovel2, pontoConsumo2, orientacaoTudoObrigatorio, dataSolicitacaoRamal, documentoValido, descricaoDocumento);

    await t
        .expect(Selector(".notification.mensagemSucesso").innerText)
        .eql("Sucesso: Autorização de Serviço inserido(a) com sucesso.");
}).after(async t => {
    await autorizacaoServicoPage.checarAutorizacaoServico(null, servicoTipoTudoObrigatorio, statusAberta, equipe, prioridade, '', nomeCliente, imovel2, pontoConsumo2, 
        orientacaoTudoObrigatorio, dataPrevEncerramento, null, descricaoDocumento, null, null, null);

})

test.skip("Tentando INCLUIR Autorização de Serviço sem descrição", async t => {
    await autorizacaoServicoPage.incluir(null, servicoTipoSemObrigatoriedade, equipe, null, null, null, null, null, null, null, null);

    await t.expect(Selector('.notification.failure').exists).ok()
    await t
        .expect(Selector(".notification.failure").innerText)
        .eql("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");
})

test.skip("Tentando INCLUIR Autorização de Serviço sem tipo de serviço", async t => {
    await autorizacaoServicoPage.incluir(descricao, null, equipe, null, null, null, null, null, null, null, null);
    
    await t.expect(Selector('.notification.failure').exists).ok()
    await t
        .expect(Selector(".notification.failure").innerText)
        .eql("Erro: O(s) campo(s) Data de previsão do encerramento, Tipo de Serviço, Prioridade do Tipo de Serviço é (são) de preenchimento obrigatório.");
})

test.skip("Tentando INCLUIR Autorização de Serviço sem equipe", async t => {
    await autorizacaoServicoPage.incluir(descricao, servicoTipoSemObrigatoriedade, null, prioridade, null, null, null, null, null, null, null);
    
    await t.expect(Selector('.notification.failure').exists).ok()
    await t
        .expect(Selector(".notification.failure").innerText)
        .eql("Erro: O(s) campo(s) Equipe é (são) de preenchimento obrigatório.");
})

test.skip("Tentando INCLUIR Autorização de Serviço sem cliente", async t => {
    await autorizacaoServicoPage.incluir(descricao, servicoTipoClienteObrigatorio, equipe, null, null, null, null, orientacaoClienteObrigatorio, null, null, null);

    await t.expect(Selector('.notification.failure').exists).ok()
    await t
        .expect(Selector(".notification.failure").innerText)
        .eql("Erro: O(s) campo(s) Cliente é (são) de preenchimento obrigatório.");
})

test.skip("Tentando INCLUIR Autorização de Serviço sem imóvel", async t => {
    await autorizacaoServicoPage.incluir(descricao, servicoTipoImovelObrigatorio, equipe, null, null, null, null, orientacaoImovelObrigatorio, null, null, null);

    await t.expect(Selector('.notification.failure').exists).ok()
    await t
        .expect(Selector(".notification.failure").innerText)
        .eql("Erro: O(s) campo(s) Imóvel é (são) de preenchimento obrigatório.");
})


test.skip("Tentando INCLUIR Autorização de Serviço sem ponto de consumo", async t => {
    await autorizacaoServicoPage.incluir(descricao, servicoTipoPontoObrigatorio, equipe, null, null, imovel1, null, orientacaoPontoObrigatorio, null, null, null);

    await t.expect(Selector('.notification.failure').exists).ok()
    await t
        .expect(Selector(".notification.failure").innerText)
        .eql("Erro: O(s) campo(s) Ponto de Consumo é (são) de preenchimento obrigatório.");
})

test.skip("Tentando INCLUIR Autorização de Serviço com documento com 0 byte", async t => {
    await autorizacaoServicoPage.clicarBotaoAbrirInclusao();
    await autorizacaoServicoPage.setDocumento(documentoInvalido1, descricaoDocumento);

    await t.expect(Selector('.notification.failure').exists).ok()
    await t
        .expect(Selector('.notification.failure').innerText)
        .contains("Erro: O(s) campo(s) Incluir Documento é (são) de preenchimento obrigatório.");
})

test.skip("Tentando INCLUIR Autorização de Serviço com documento com tamanho > 1024KB", async t => {
    await autorizacaoServicoPage.clicarBotaoAbrirInclusao();
    await autorizacaoServicoPage.setDocumento(documentoInvalido2, descricaoDocumento);

    await t.expect(Selector('#documentoAnexoFeedback').exists).ok()
    await t
        .expect(Selector('#documentoAnexoFeedback').innerText)
        .contains("O tamanho do arquivo (15510.50 kb) excede o limite (1024.00 kb)");
})

test.skip("Tentando INCLUIR Autorização de Serviço com documento com extensão inválida", async t => {
    await autorizacaoServicoPage.clicarBotaoAbrirInclusao();
    await autorizacaoServicoPage.setDocumento('AutorizacaoServicoTeste.js', descricaoDocumento);

    await t.expect(Selector('.notification.failure').exists).ok()
    await t
    .expect(Selector('.notification.failure').innerText)
        .contains("Erro: Escolha um Arquivo com uma das seguintes extensões: .pdf .txt .doc .docx .xls .xlsx .png .jpg .jpeg .bmp .ppt .pptx .");
})

test.skip("Tentando INCLUIR Autorização de Serviço para cliente e tipo com AS aberta", async t => {
    await autorizacaoServicoPage.incluir(descricao, servicoTipoTudoObrigatorio, equipe, prioridade, nomeCliente, 
        imovel2, pontoConsumo2, orientacaoTudoObrigatorio, dataSolicitacaoRamal, null, null);
    
    await t.expect(Selector('.notification.failure').exists).ok()
    await t
        .expect(Selector(".notification.failure").innerText)
        .contains("Erro: Não foi possível gerar A.S para o serviço SERVIÇO TUDO OBRIGATÓRIO pois esse ponto de consumo já possui a A.S");
})

test.skip("Tentando INCLUIR Autorização de Serviço com data de solicitação de ramal inválida", async t => {
    await autorizacaoServicoPage.clicarBotaoAbrirInclusao();
    await t
            .setNativeDialogHandler(() => true)
            .selectText(Selector('#dataSolicitacaoRamal'))
            .typeText(Selector('#dataSolicitacaoRamal'), '31/02/2018')
            .pressKey('tab');
        
        const history = await t.getNativeDialogHistory();

        await t.expect(history[0].text).eql('Data Invalida.');
})

test.skip("Tentando ALTERAR Autorização de Serviço incluindo material e equipamento", async t => {

    await autorizacaoServicoPage.alterarAutorizacaoServico(null, servicoTipoTudoObrigatorio, statusAberta, descricaoAlteracao, dataPrevEncerramentoAlteracao, 
        dataSolicitacaoRamalAlteracao, material, qntMaterial, equipamento);
    
   
    await t
        .expect(Selector(".notification.mensagemSucesso").innerText)
        .eql("Sucesso: Autorização de Serviço alterado(a) com sucesso.");
}).after(async t => {
    
    await autorizacaoServicoPage.checarAutorizacaoServico(null, servicoTipoTudoObrigatorio, statusAberta, equipe, prioridade, '', nomeCliente, imovel2, pontoConsumo2, 
        orientacaoTudoObrigatorio, dataPrevEncerramentoAlteracao, null, descricaoDocumento, material, qntMaterial, equipamento);
})

test.skip("Tentando REMANEJAR Autorização de Serviço", async t => {

    await autorizacaoServicoPage.remanejarAutorizacaoServico(null, servicoTipoTudoObrigatorio, statusAberta, descricaoRemanejamento, equipe2, documentoValido, descricaoDocumento2);

    await t.expect(Selector('.notification.mensagemSucesso').exists).ok()

    await t
        .expect(Selector(".notification.mensagemSucesso").innerText)
        .eql("Sucesso: Autorização de Serviço remanejado(a) com sucesso.");
}).after(async t => {
    
    await autorizacaoServicoPage.checarAutorizacaoServico(null, servicoTipoTudoObrigatorio, statusAberta, equipe2, prioridade, '', nomeCliente, imovel2, pontoConsumo2, 
        orientacaoTudoObrigatorio, dataPrevEncerramentoAlteracao, null, descricaoDocumento2, material, qntMaterial, equipamento);
})

test.skip("Tentando ENCERRAR Autorização de Serviço", async t => {

    await autorizacaoServicoPage.encerrarAutorizacaoServico(null, servicoTipoTudoObrigatorio, statusAberta, descricaoRemanejamento, motivoEncerramento);

    await t.expect(Selector('.notification.mensagemSucesso').exists).ok()
    
    await t
        .expect(Selector(".notification.mensagemSucesso").innerText)
        .eql("Sucesso: Autorização de Serviço encerrado(a) com sucesso.");
}).after(async t => {
    
    await autorizacaoServicoPage.checarAutorizacaoServico(null, servicoTipoTudoObrigatorio, statusEncerrada, equipe2, prioridade, '', nomeCliente, imovel2, pontoConsumo2, 
        orientacaoTudoObrigatorio, dataPrevEncerramentoAlteracao, null, descricaoDocumento2, material, qntMaterial, equipamento);
})

/*
test("Incluir/alterar/remanejar/cancelar", async t => {

    await autorizacaoServicoPage.incluir("Inclusão", "RELIGACAO NORMAL", "EQUIPE TESTE", "BRASKEM SA", "TESTE");
    await t
        .expect(Selector('.notification').innerText)
        .eql("Sucesso: Autorização de Serviço inserido(a) com sucesso.");

    
    await autorizacaoServicoPage.selecionar("RELIGACAO NORMAL");
    await autorizacaoServicoPage.alterar("Alteração");
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Autorização de Serviço alterado(a) com sucesso.");
    
    await autorizacaoServicoPage.selecionar("RELIGACAO NORMAL");
    await autorizacaoServicoPage.remanejar("Remanejamento", "EQUIPE TESTE");
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Autorização de Serviço remanejado(a) com sucesso.");

    await autorizacaoServicoPage.selecionar("RELIGACAO NORMAL");
    await autorizacaoServicoPage.cancelar("Cancelamento", "ENCERRAMENTO");
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Autorização de Serviço encerrado(a) com sucesso.");
});
test("testar obrigatoriedade dos campos", async t => {

    await autorizacaoServicoPage.setDataGeracao("20/02/2021", "20/02/2020");
    await t
        .click(autorizacaoServicoPage.botaoPesquisar)

        .expect(Selector(".notification").innerText)
        .eql("Erro: A data final não pode ser anterior a data inicial.")
        
        .click(autorizacaoServicoPage.botaoLimpar);


    await autorizacaoServicoPage.setDataPrevisao("20/02/2021", "20/02/2020");
    await t
        .click(autorizacaoServicoPage.botaoPesquisar)
        
        .expect(Selector(".notification").innerText)
        .eql("Erro: A data final não pode ser anterior a data inicial.")
        
        .click(autorizacaoServicoPage.botaoLimpar);

    await autorizacaoServicoPage.setDataEncerramento("20/02/2021", "20/02/2020");
    await t
        .click(autorizacaoServicoPage.botaoPesquisar)
        
        .expect(Selector(".notification").innerText)
        .eql("Erro: A data final não pode ser anterior a data inicial.")
        
        .click(autorizacaoServicoPage.botaoLimpar);

    await autorizacaoServicoPage.setDataExecucao("20/02/2021", "20/02/2020");
    await t
        .click(autorizacaoServicoPage.botaoPesquisar)
        
        .expect(Selector(".notification").innerText)
        .eql("Erro: A Data de Início não pode ser maior que a Data Final.")
        
        .click(autorizacaoServicoPage.botaoLimpar);
    

    //OPERAÇÃO COM DOCUMENTO CANCELADO
    const listaCheckboxes = ["Aberta","Em Execução","Encerrada"];

    await autorizacaoServicoPage.setStatus(listaCheckboxes);
    await t.click(autorizacaoServicoPage.botaoPesquisar);    
    await autorizacaoServicoPage.selecionar("ENCERRADA");
    await t
        .click(autorizacaoServicoPage.botaoAlterar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Esta operação não pode ser realizada para uma Autorização de Serviço de Status ENCERRADA.");

    await autorizacaoServicoPage.selecionar("ENCERRADA");
    await t
        .setNativeDialogHandler(() => true)
        .click(autorizacaoServicoPage.botaoEmExecucao)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Esta operação não pode ser realizada para uma Autorização de Serviço de Status ENCERRADA.");

    await autorizacaoServicoPage.selecionar("ENCERRADA");
    await t
        .setNativeDialogHandler(() => true)
        .click(autorizacaoServicoPage.botaoExecutar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Esta operação não pode ser realizada para uma Autorização de Serviço de Status ENCERRADA.");

    await autorizacaoServicoPage.selecionar("ENCERRADA");
    await t
        .click(autorizacaoServicoPage.botaoRemanejar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Esta operação não pode ser realizada para uma Autorização de Serviço de Status ENCERRADA.");

    await autorizacaoServicoPage.selecionar("ENCERRADA");
    await t
        .click(autorizacaoServicoPage.botaoEncerrar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Esta operação não pode ser realizada para uma Autorização de Serviço de Status ENCERRADA.");

    // OBRIGATORIEDADE DOS CAMPOS DE INCLUSÃO
    await t
        .click(autorizacaoServicoPage.botaoIncluir)
        .click(autorizacaoServicoPage.botaoSalvar)
        
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, Data de previsão do encerramento, Equipe, Tipo de Serviço, Prioridade do Tipo de Serviço é (são) de preenchimento obrigatório.");
    
    await autorizacaoServicoPage.setDescricao("Inclusão");
    await t
        .click(autorizacaoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Data de previsão do encerramento, Equipe, Tipo de Serviço, Prioridade do Tipo de Serviço é (são) de preenchimento obrigatório.");
    
    await autorizacaoServicoPage.setServicoTipo("RELIGACAO NORMAL");
    await t
        .click(autorizacaoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Equipe, Cliente, Imóvel, Contrato, Ponto de Consumo é (são) de preenchimento obrigatório.");

    await autorizacaoServicoPage.setEquipe("EQUIPE TESTE");
    await t
        .click(autorizacaoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Cliente, Imóvel, Contrato, Ponto de Consumo é (são) de preenchimento obrigatório.");

    await autorizacaoServicoPage.setNome("BRASKEM SA");
    await t
        .click(autorizacaoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Autorização de Serviço inserido(a) com sucesso.");
    
    // OBRIGATORIEDADE DOS CAMPOS DE ALTERAÇÃO
    await autorizacaoServicoPage.selecionar("ABERTA");
    await t
        .click(autorizacaoServicoPage.botaoAlterar)
        .click(autorizacaoServicoPage.botaoSalvar)
        
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");
    
    await autorizacaoServicoPage.setDescricao("Alteração");
    await t
        .click(autorizacaoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Autorização de Serviço alterado(a) com sucesso.");


    // OBRIGATORIEDADE DOS CAMPOS DE REMANEJAMENTO
    await autorizacaoServicoPage.selecionar("ABERTA");
    await t
        .click(autorizacaoServicoPage.botaoRemanejar)
        .click(autorizacaoServicoPage.botaoSalvarRemanejamento)
        
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");
    
    await autorizacaoServicoPage.setDescricao("Remanejamento");
    await t
        .click(autorizacaoServicoPage.botaoSalvarRemanejamento)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Autorização de Serviço remanejado(a) com sucesso.");


    //OBRIGATORIEDADE DOS CAMPOS DE ENCERRAMENTO
    await autorizacaoServicoPage.selecionar("ABERTA");
    await t
        .click(autorizacaoServicoPage.botaoEncerrar)
        .click(autorizacaoServicoPage.botaoSalvar)

        .expect(Selector(".notification").innerText)
        .eql("Erro: A Autorização de Serviço precisa de um motivo para ser encerrada.");
    
    await autorizacaoServicoPage.setMotivo("ENCERRAMENTO");

    await t.click(autorizacaoServicoPage.botaoSalvar)

        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Autorização de Serviço encerrado(a) com sucesso.");
});
*/