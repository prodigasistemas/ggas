import { Selector, t } from 'testcafe';
import Page from '../../../ggasPage';
import Util from '../../../Util';

const label = Selector("label");
const util = new Util();
const dataAtual = util.dateNow(0,0,0);
const dataPrevEncerramento = util.dateNow(0,0,2);

class Checkbox {
    constructor(visibleText) {
        this.label = label.withText(visibleText);
        this.checkbox = label.parent("div").find('input');
    }
}

export default class AutorizacaoServicoPage extends Page {
    
    constructor() {

        super();

        this.dataGeracaoInicio = Selector("#dataGeracaoInicio");
        this.dataGeracaoFim = Selector("#dataGeracaoFim");
        this.dataGeracao = Selector('#dataGeracao');
        this.dataPrevisaoInicio = Selector("#dataPrevisaoInicio");
        this.dataPrevisaoFim = Selector("#dataPrevisaoFim");
        this.dataEncerramentoInicio = Selector("#dataEncerramentoInicio");
        this.dataEncerramentoFim = Selector("#dataEncerramentoFim");
        this.dataExecucaoInicio = Selector("#dataexecucacao");
        this.dataExecucaoFim = Selector("input[name=dataExecucaoFim]");
        this.dataPrevisaoEncerramento = Selector('#dataPrevisaoEncerramento');

        this.tabelaServicos = Selector("table#table-servicos tbody td");
        
        this.codigoAS = Selector("#chavePrimaria");
        this.servicoTipoDetalhamento = Selector('input').withAttribute('name', 'servicoTipo');
        this.equipeDetalhamento = Selector('input').withAttribute('name', 'equipe');
        this.executanteDetalhamento = Selector('input').withAttribute('name', 'executante');
        this.status = Selector('#status').prevSibling('input');

        this.descricao = Selector("#descricao");
        this.servicoTipo = Selector("#servicoTipo");
        this.servicoTipoOptions = this.servicoTipo.find("option");
        this.equipe = Selector("#equipe");
        this.equipeOptions = this.equipe.find("option");
        this.orientacaoServicoTipo = Selector('#orientacaoServicoTipo');
        this.dataSolicitacaoRamal = Selector('#dataSolicitacaoRamal');
        this.servicoTipoPrioridade = Selector('#rotuloMarca').nextSibling(0);
        this.dataExecucao = Selector("#dataExecucao");

        //PESSOA
        this.abaCliente = Selector('#tab-content').find("a").withText("Cliente");
        this.nome = Selector("#nome");
        this.cpfCnpj = Selector("#cpfCnpj");

        //Imóvel
        this.abaImovel = Selector('#tab-content').find("a").withText("Imóvel");
        this.nomeImovel = Selector("#nomeImovel");
        this.tabelaImovel = Selector("table#table-grid-imoveis").find('tbody').find('tr');
        this.pontoConsumo = Selector("#checkPontoConsumo");

        //DOCUMENTO
        this.abaDocumentos = Selector('#tab-content').find("a").withText("Documentos");
        this.abaHistoricoDocumentos = Selector('#tab-content').find("a").withText("Histórico de Autorização de Serviço");
        this.documento = Selector('#arquivoAnexo');
        this.descricaoDocumento = Selector('#descricaoAnexo');
        this.botaoAdicionarDocumento = Selector('#botaoIncluirAnexo');

        this.abaMateriaisNecessarios = Selector('#tab-content').find("a").withText("Materiais Necessários");
        this.material = Selector('#material');
        this.quantidadeMaterial = Selector('#quantidadeMaterial');
        this.materialAdicionado = Selector("#servicoAutorizacaoMaterial");

        this.abaEquipamentosNecessarios = Selector('#tab-content').find("a").withText("Equipamento Especial Necessário");
        this.equipamento = Selector('#equipamento');
        this.equipamentoAdicionado = Selector("#datatable-equipamento-especial");

        this.motivoEncerramento = Selector("#servicoAutorizacaoMotivoEncerramento");
        this.motivoEncerramentoOptions = this.motivoEncerramento.find('option');

        this.botaoIncluir = Selector("#buttonIncluir");
        this.botaoPesquisar = Selector("#botaoPesquisar");
        this.botaoLimpar = Selector("#botaoLimpar");
        this.botaoSalvar = Selector("#btnSalvar");
        this.botaoAlterar = Selector("#botaoAlterar");
        this.botaoEncerrar = Selector("button[name=buttonReiterar]");
        this.botaoAbrirEncerramento = Selector("#botaoEncerrar");
		this.botaoRemanejar = Selector("button[name=buttonTramitar]");
        this.botaoSalvarRemanejamento = Selector("#botaoRemanejar");
        this.botaoEmExecucao = Selector("button[name=buttonEmExecucao]");
        this.botaoExecutar = Selector("button[name=buttonCopiar]");
        this.botaoCancelar = Selector("#botaoCancelar");
        this.botaoIncluirMaterial = Selector("#botaoIncluirMaterial");
        this.botaoIncluirEquipamento = Selector("#botaoIncluirEquipamento");
        this.btnAcao = Selector(".btn-danger.btn-sm.dropdown-toggle");
        this.botaoLimparDropdown = Selector("input[value=Limpar]");

        this.statusAberta = Selector('#aberta');
        this.statusCancelada = Selector('#cancelada');
        this.statusEncerrada = Selector('#encerrada');
        this.statusPendenteAtualizacao = Selector('#pendente');
        this.statusEmExecucao = Selector('#em-execucacao');
        this.statusExecutada = Selector('#executada');

    }

    async irParaAutorizacaoServico() {
        await t.navigateTo(this.url+'/exibirPesquisaServicoAutorizacao');
    }

	async selecionar(opcao) {
        await t
            .click(this.tabelaServicos.withText(opcao).parent("tr").find('input'));
    }

    async clicarBotaoAbrirInclusao() {
        await t.expect(this.botaoIncluir.exists).ok()
        await t.click(this.botaoIncluir);
    }

    async clicarBotaoAbrirAlteracao() {
        await t.expect(this.botaoAlterar.exists).ok();
        await t.click(this.botaoAlterar);
    }

    async clicarBotaoAbrirRemanejamento() {
        await t.expect(this.botaoSalvarRemanejamento.exists).ok();
        await t.click(this.botaoSalvarRemanejamento);
    }

    async clicarBotaoAbrirEncerramento() {
        await t.expect(this.botaoAbrirEncerramento.exists).ok();
        await t.click(this.botaoAbrirEncerramento);
    }

    async incluir(descricao, servicoTipo, equipe, prioridade, nome, imovel, pontoConsumo, orientacao, dataSolicitacaoRamal, documento, descricaoDocumento) {
        
        await t.wait(1000);

        await this.clicarBotaoAbrirInclusao();

        await this.preencherDados(descricao, servicoTipo, equipe, nome, imovel, pontoConsumo, dataSolicitacaoRamal, documento, descricaoDocumento);
        
        if(servicoTipo != null) {
            await this.checarInput(this.orientacaoServicoTipo, orientacao, false);
            await this.checarPropriedade(this.servicoTipoPrioridade, 'value', prioridade, false);
            await this.checarInput(this.dataPrevisaoEncerramento, dataPrevEncerramento, false);
            await this.checarInput(this.dataExecucao, '', null)
        }

        await t.click(this.botaoSalvar);
    }

    async preencherDados(descricao, servicoTipo, equipe, nome, imovel, pontoConsumo, dataSolicitacaoRamal, documento, descricaoDocumento) {

        await this.preencherInput(this.descricao, descricao);
        await this.selecionarOpcao(this.servicoTipo, servicoTipo);
        await this.selecionarOpcao(this.equipe, equipe);
        await this.preencherInput(this.dataSolicitacaoRamal, dataSolicitacaoRamal);
        await this.setNome(nome);
        await this.setImovel(imovel);
        await this.setPontoConsumo(pontoConsumo);
        await this.setDocumento(documento, descricaoDocumento);
    }

    async alterar(descricao) {

        await t.click(this.botaoAlterar);

        await this.setDescricao(descricao);

        await t.click(this.botaoSalvar);
    }

    async remanejar(descricao, equipe) {
        await t.click(this.botaoRemanejar);

        await this.setDescricao(descricao);
        await this.setEquipe(equipe);

        await t.click(this.botaoSalvarRemanejamento);
	}

    async cancelar(descricao, motivoEncerramento) {
        await t.click(this.botaoEncerrar);

        await this.setDescricao(descricao);
        await this.setMotivo(motivoEncerramento);

        await t.click(this.botaoSalvar);
    }
    
    async setStatus(listaStatus) {
        for(const status of listaStatus){
            const checkbox = new Checkbox(status);
            await t.click(checkbox.label);
        }
    }

    async setDataGeracao(dataInicio, dataFinal) {

        await t
            .selectText(this.dataGeracaoInicio)
            .pressKey('delete')
            .typeText(this.dataGeracaoInicio, dataInicio)
            .selectText(this.dataGeracaoFim)
            .pressKey('delete')
            .typeText(this.dataGeracaoFim, dataFinal);
    }

    async setDataPrevisao(dataInicio, dataFinal) {

        await t
            .typeText(this.dataPrevisaoInicio, dataInicio)
            .typeText(this.dataPrevisaoFim, dataFinal);
    }

    async setDataEncerramento(dataInicio, dataFinal) {

        await t
            .selectText(this.dataEncerramentoInicio)
            .pressKey('delete')
            .typeText(this.dataEncerramentoInicio, dataInicio)
            .selectText(this.dataEncerramentoFim)
            .pressKey('delete')
            .typeText(this.dataEncerramentoFim, dataFinal);
    }

    async setDataExecucao(dataInicio, dataFinal) {

        await t
            .typeText(this.dataExecucaoInicio, dataInicio)
            .typeText(this.dataExecucaoFim, dataFinal);
    }

    async setDescricao(descricao) {
        await t
            .typeText(this.descricao, descricao);
    }

    async setServicoTipo(servicoTipo) {
        await t
            .click(this.servicoTipo)
            .click(this.servicoTipoOptions.withText(servicoTipo));
    }

    async setEquipe(equipe) {
        await t
            .click(this.equipe)
            .click(this.equipeOptions.withText(equipe));
    }
            
    async setNome(nome) {

        if(nome != null) {
            await t
            .selectText(this.nome)
            .typeText(this.nome, nome)
            .click(Selector(".ui-menu-item").find('a'));
        }
    }

    async setImovel(imovel) {

        if(imovel != null) {
            await t
            .click(this.abaImovel)
            .selectText(this.nomeImovel)
            .typeText(this.nomeImovel, imovel)
            .click(this.botaoPesquisar)
            .click(this.tabelaImovel.find('a').withText(imovel).parent("tr").find("input"))
        }
    }

    async setPontoConsumo(pontoConsumo) {
        if(pontoConsumo != null) {
            await t
                .click(Selector('#table-grid-pontos-consumo')
                .find('td')
                .withText(pontoConsumo)
                .prevSibling('td')
                .find('input'));
        }
        
    }

    async setDocumento(documento, descricaoDocumento) {

        if(documento != null) {
            await t
                .click(this.abaDocumentos)
                .setFilesToUpload(this.documento, documento)
                .typeText(this.descricaoDocumento, descricaoDocumento)
                .click(this.botaoAdicionarDocumento);
        }
    }

    async setMotivo(motivo){
        await t
            .click(this.motivoEncerramento)
            .click(this.motivoEncerramentoOptions.withText(motivo));
    }

    async setMaterial(material, qntMaterial) {

        if(material != null) {
            this.clicarElemento(this.abaMateriaisNecessarios);
            this.selecionarOpcao(this.material, material);
            this.preencherInput(this.quantidadeMaterial, qntMaterial);
            this.clicarBotao(this.botaoIncluirMaterial);
        }
    }

    async setEquipamento(equipamento) {

        if(equipamento != null) {
            this.clicarElemento(this.abaEquipamentosNecessarios);
            this.selecionarOpcao(this.equipamento, equipamento);
            this.clicarBotao(this.botaoIncluirEquipamento);
        }
    }

    async pesquisarAutorizacaoServico(codigo, servicoTipo, status) {

        this.preencherInput(this.codigoAS, codigo);
        this.selecionarOpcao(this.servicoTipo, servicoTipo);
        await this.selecionarStatus(status);
        this.clicarBotao(this.botaoPesquisar);
    }

    async selecionarStatus(status) {
        await t.expect(this.statusAberta.exists).ok()
        
        switch (status) {
            case 'CANCELADA':
                await this.desmarcarCheckbox(this.statusAberta);
                await this.marcarCheckbox(this.statusCancelada);
                await this.desmarcarCheckbox(this.statusEmExecucao);
                await this.desmarcarCheckbox(this.statusEncerrada);
                await this.desmarcarCheckbox(this.statusExecutada);
                await this.desmarcarCheckbox(this.statusPendenteAtualizacao);
                break;
            case 'ENCERRADA':
                await this.desmarcarCheckbox(this.statusAberta);
                await this.desmarcarCheckbox(this.statusCancelada);
                await this.desmarcarCheckbox(this.statusEmExecucao);
                await this.marcarCheckbox(this.statusEncerrada);
                await this.desmarcarCheckbox(this.statusExecutada);
                await this.desmarcarCheckbox(this.statusPendenteAtualizacao);
                break;
            case 'PENDENTE':
                await this.desmarcarCheckbox(this.statusAberta);
                await this.desmarcarCheckbox(this.statusCancelada);
                await this.desmarcarCheckbox(this.statusEmExecucao);
                await this.desmarcarCheckbox(this.statusEncerrada);
                await this.desmarcarCheckbox(this.statusExecutada);
                await this.marcarCheckbox(this.statusPendenteAtualizacao);
                break;
            case 'EM EXECUÇÃO':
                await this.desmarcarCheckbox(this.statusAberta);
                await this.desmarcarCheckbox(this.statusCancelada);
                await this.marcarCheckbox(this.statusEmExecucao);
                await this.desmarcarCheckbox(this.statusEncerrada);
                await this.desmarcarCheckbox(this.statusExecutada);
                await this.desmarcarCheckbox(this.statusPendenteAtualizacao);
                break;
            case 'EXECUTADO':
                await this.desmarcarCheckbox(this.statusAberta);
                await this.desmarcarCheckbox(this.statusCancelada);
                await this.desmarcarCheckbox(this.statusEmExecucao);
                await this.desmarcarCheckbox(this.statusEncerrada);
                await this.marcarCheckbox(this.statusExecutada);
                await this.desmarcarCheckbox(this.statusPendenteAtualizacao);
                break;
            case 'ABERTA':
                await this.marcarCheckbox(this.statusAberta);
                await this.desmarcarCheckbox(this.statusCancelada);
                await this.desmarcarCheckbox(this.statusEmExecucao);
                await this.desmarcarCheckbox(this.statusEncerrada);
                await this.desmarcarCheckbox(this.statusExecutada);
                await this.desmarcarCheckbox(this.statusPendenteAtualizacao);
                break;
            default:
                break;
        } 
    }

    

    async checarPontoConsumo(pontoConsumo) {

        if(pontoConsumo != null) {
            await t.expect(Selector('#table-grid-pontos-consumo').find('td').withText(pontoConsumo).exists).ok();
        }
    }

    async checarDocumento(descricaoDocumento) {

        if(descricaoDocumento != null) {
            await t.expect(Selector('#table-servico-autorizacao-historico-anexos').find('td').withText(descricaoDocumento).exists).ok();
        }
    }

    async checarMaterial(material, qntMaterial) {

        if(material != null) {
            await t.expect(this.materialAdicionado.find("td").withText(material).exists).ok();
            await t.expect(this.materialAdicionado.find("td").withText(qntMaterial).exists).ok();
        }
    }

    async checarEquipamento(equipamentoEspecial) {

        if(equipamentoEspecial != null) {
            await t.expect(this.equipamentoAdicionado.find("td").withText(equipamentoEspecial).exists).ok();
        }
    }

    async checarAutorizacaoServico(codigo, servicoTipo, status, equipe, prioridade, executante, nome, imovel, pontoConsumo, orientacao, dataPrevEncerramento, 
        dataSolicitacaoRamal, descricaoDocumento, material, qntMaterial, equipamentoEspecial) {

        await this.pesquisarAutorizacaoServico(null, servicoTipo, status);
        await this.clicarElemento(Selector('a').withText(servicoTipo));
        await this.checarPropriedade(this.codigoAS, 'value', codigo, false);
        await this.checarPropriedade(this.dataGeracao, 'value', dataAtual, false);
        await this.checarPropriedade(this.servicoTipoDetalhamento, 'value', servicoTipo, false);
        await this.checarPropriedade(this.dataPrevisaoEncerramento, 'value', dataPrevEncerramento, false);
        await this.checarPropriedade(this.servicoTipoPrioridade, 'value', prioridade, false);
        await this.checarPropriedade(this.equipeDetalhamento, 'value', equipe, false);
        await this.checarPropriedade(this.executanteDetalhamento, 'value', executante, false);
        await this.checarPropriedade(this.dataSolicitacaoRamal, 'value', dataSolicitacaoRamal, false);
        await this.checarPropriedade(this.dataExecucao, 'value', '', false);
        await this.checarPropriedade(this.status, 'value', status, false);
        await this.checarInput(this.orientacaoServicoTipo, orientacao, false);
        await this.clicarElemento(this.abaCliente);
        await this.checarPropriedade(this.nome, 'value', nome, false);
        await this.clicarElemento(this.abaImovel);
        await this.checarPropriedade(this.nomeImovel, 'value', imovel, false);
        await this.checarPontoConsumo(pontoConsumo);
        await this.clicarElemento(this.abaHistoricoDocumentos);
        await this.checarDocumento(descricaoDocumento);
        await this.clicarElemento(this.abaMateriaisNecessarios);
        await this.checarMaterial(material, qntMaterial);
        await this.clicarElemento(this.abaEquipamentosNecessarios);
        await this.checarEquipamento(equipamentoEspecial);
    }

    async alterarAutorizacaoServico(codigo, servicoTipo, status, descricao, dataPrevEnc, dataSolicitacaoRamal, material, qntMaterial, equipamentoEspecial) {

        await this.pesquisarAutorizacaoServico(codigo, servicoTipo, status);
        await this.clicarElemento(Selector('a').withText(servicoTipo));
        await this.clicarBotaoAbrirAlteracao();
        await this.preencherInput(this.descricao, descricao);
        await this.preencherInput(this.dataPrevisaoEncerramento, dataPrevEnc);
        await this.preencherInput(this.dataSolicitacaoRamal, dataSolicitacaoRamal);
        await this.clicarElemento(this.abaMateriaisNecessarios);
        await this.setMaterial(material, qntMaterial);
        await this.setEquipamento(equipamentoEspecial);
        await this.clicarBotao(this.botaoSalvar);

    }

    async remanejarAutorizacaoServico(codigo, servicoTipo, status, descricao, equipe, documento, descricaoDocumento) {

        await this.pesquisarAutorizacaoServico(codigo, servicoTipo, status);
        await this.clicarElemento(Selector('a').withText(servicoTipo));
        await this.clicarBotaoAbrirRemanejamento();
        await this.preencherInput(this.descricao, descricao);
        await this.selecionarOpcao(this.equipe, equipe);
        await this.setDocumento(documento, descricaoDocumento);
        await this.clicarBotaoAbrirRemanejamento();
    }

    async encerrarAutorizacaoServico(codigo, servicoTipo, status, descricao, motivo) {

        await this.pesquisarAutorizacaoServico(codigo, servicoTipo, status);
        await this.clicarElemento(Selector('a').withText(servicoTipo));
        await this.clicarBotaoAbrirEncerramento();
        await this.preencherInput(this.descricao, descricao);
        await this.selecionarOpcao(this.motivoEncerramento, motivo);
        await t.click(this.botaoSalvar)
    }
}
