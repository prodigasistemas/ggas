import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';
import TipoServicoPage from './TipoServicoPage';

const page = new Page();
const tipoServicoPage = new TipoServicoPage();

fixture('Tipo de Serviço')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await tipoServicoPage.irParaTipoServico();
    });

test('CRUD', async t => {

    await page.aguardarCarregarPagina('Pesquisar Tipo de Serviço', Selector("div.card").find("h5"));
    
    await tipoServicoPage.incluir("INCLUSÃO", "BAIXA", "Relatorio de aviso de corte", "MATERIAL DEPENDÊNCIA", "5", "TARDE", "10");
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo(s) de Serviço(s) inserido(a) com sucesso.');

    await tipoServicoPage.selecionar("INCLUSÃO");
    await tipoServicoPage.alterar("ALTERAÇÃO", "ALTA", "relatorio nota de debito");
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo(s) de Serviço(s) alterado(a) com sucesso.');

    await tipoServicoPage.selecionar("ALTERAÇÃO");
    await tipoServicoPage.remover();
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo(s) de Serviço(s) removido(s) com sucesso.');
});

test('Teste de Obrigatoriedade', async t => {

    await page.aguardarCarregarPagina('Pesquisar Tipo de Serviço', Selector("div.card").find("h5"));

    //Tela Inclusão
    await t
        .click(tipoServicoPage.botaoIncluir)
        .click(tipoServicoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Descrição, Prioridade, Layout do documento é (são) de preenchimento obrigatório.');

    await tipoServicoPage.servicoRegulamentado(false);
    await tipoServicoPage.podeSerAgendado(false);
    await tipoServicoPage.seraCobrado(false, false);
    await tipoServicoPage.equipamentoEspecial(false, "GUINCHO");
    await tipoServicoPage.atualizacaoCadastral(0, "TELA CLIENTE");
    await tipoServicoPage.necessitaVeiculo(false);
    await tipoServicoPage.pesquisaSatisfacaoAutomatica(false);
    await tipoServicoPage.gerarLoteAutorizacaoServico(false);
    await tipoServicoPage.encerramentoAutomantico(false);

    await t.click(tipoServicoPage.abaCamposObrigatorios);
    await tipoServicoPage.camposObrigatorios(false, false, false, false, false);
    
    await t.click(tipoServicoPage.abaOperacaoEnvioEmail);
    await tipoServicoPage.enviarEmail(false, false, false, false, false);

    await t.click(tipoServicoPage.abaMaterial);
    await tipoServicoPage.setMaterial("MATERIAL DEPENDÊNCIA", "5");
    await t
        .click(tipoServicoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Descrição, Prioridade, Layout do documento é (são) de preenchimento obrigatório.');

    await t.click(tipoServicoPage.abaTurno);
    await tipoServicoPage.setTurno("TARDE", "10");
    await t
        .click(tipoServicoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Descrição, Prioridade, Layout do documento é (são) de preenchimento obrigatório.');

    await tipoServicoPage.setDescricao("INCLUSÃO");
    await t
        .click(tipoServicoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Prioridade, Layout do documento é (são) de preenchimento obrigatório.');

    await tipoServicoPage.setPrioridade("BAIXA");
    await t
        .click(tipoServicoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Layout do documento é (são) de preenchimento obrigatório.');

    await tipoServicoPage.setLayoutDocumento("Relatorio de aviso de corte");
    await t
        .click(tipoServicoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo(s) de Serviço(s) inserido(a) com sucesso.');


    //Tela Alteração
    await tipoServicoPage.selecionar("INCLUSÃO");
    await t
        .click(tipoServicoPage.botaoAlterar)
        .click(tipoServicoPage.botaoAcao)
        .click(tipoServicoPage.botaoLimpar)
        .click(tipoServicoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Descrição, Prioridade, Layout do documento é (são) de preenchimento obrigatório.');

    await tipoServicoPage.setDescricao("ALTERAÇÃO");
    await t
        .click(tipoServicoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Prioridade, Layout do documento é (são) de preenchimento obrigatório.');

    await tipoServicoPage.setPrioridade("ALTA");
    await t
        .click(tipoServicoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Erro: O(s) campo(s) Layout do documento é (são) de preenchimento obrigatório.');

    await tipoServicoPage.setLayoutDocumento("relatorio nota de debito");
    await t
        .click(tipoServicoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo(s) de Serviço(s) alterado(a) com sucesso.');

    await tipoServicoPage.selecionar("ALTERAÇÃO");
    await tipoServicoPage.remover();
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo(s) de Serviço(s) removido(s) com sucesso.');
});
