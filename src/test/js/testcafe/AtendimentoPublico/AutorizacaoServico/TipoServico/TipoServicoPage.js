import { Selector, t, ClientFunction} from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class TipoServicoPage {

    constructor() {

        this.botaoIncluir = Selector('#buttonIncluir');
        this.botaoAlterar = Selector('#buttonAlterar');
        this.botaoRemover = Selector('#buttonRemover');
        this.botaoSalvar = Selector('#buttonSalvar');
        this.botaoAcao = Selector('#btnGroupDropInclusaoCancelar');
        this.botaoLimpar = Selector('input[value=Limpar]');
        this.botaoAdicionarMaterial = Selector("#botaoIncluirMaterial");
        this.botaoAdicionarTurno = Selector("#botaoIncluirTurno");

        this.tabelaRegistros = Selector("#table-servicos tbody td");

        this.abaMaterial = Selector("#tab-material-necessario");
        this.abaTurno = Selector("#tab-quantidade-agendamento");
        this.abaCamposObrigatorios = Selector("#tab-campo-obrigatorio");
        this.abaOperacaoEnvioEmail = Selector("#tab-envio-email");

        this.descricao = Selector("#descricao");
        this.prioridade = Selector("#servicoTipoPrioridade");
        this.prioridadeOption = this.prioridade.find('option');
        this.layoutDocumento = Selector("#documentoImpressaoLayout");
        this.layoutDocumentoOption = this.layoutDocumento.find('option');
        this.material = Selector("#material");
        this.materialOption = this.material.find('option');
        this.quantidadeMaterial = Selector("#quantidadeMaterial");
        this.turno = Selector("#turno");
        this.turnoOption = this.turno.find('option');
        this.quantidadeTurno = Selector("#quantidadeAgendamentosTurno");
    }

    async irParaTipoServico(){
        await t
            .navigateTo(page.url+"/exibirPesquisaServicoTipo");
    }

    async selecionar(opcao) {
        const clicarCheckbox = ClientFunction(opcao => {
            const tr = $("#table-servicos tbody td:contains("+opcao+")").parent("tr");
            const input = tr.find("input");
            input.siblings("label").trigger("click");
            return false;
        });

        await clicarCheckbox(opcao);
    }

    async incluir(descricao, prioridade, layoutDocumento, material, quantidadeMaterial, turno, quantidadeTurno) {
        
        await t.click(this.botaoIncluir);

        await this.setDescricao(descricao);
        await this.setPrioridade(prioridade);
        await this.setLayoutDocumento(layoutDocumento);

        await t.click(this.abaMaterial);
        await this.setMaterial(material, quantidadeMaterial);

        await t.click(this.abaTurno);
        await this.setTurno(turno, quantidadeTurno);
            
        await t.click(this.botaoSalvar);
    }

    async alterar(descricao, prioridade, layoutDocumento) {
        
        await t
            .click(this.botaoAlterar)
            .click(this.botaoAcao)
            .click(this.botaoLimpar);

        await this.setDescricao(descricao);
        await this.setPrioridade(prioridade);
        await this.setLayoutDocumento(layoutDocumento);
    
        await t.click(this.botaoSalvar);
    }

    async remover() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }

    //Set's dos campos
    async setDescricao(descricao) {
        await t
            .typeText(this.descricao, descricao);
    }

    async setPrioridade(prioridade) {
        await t
            .click(this.prioridade)
            .click(this.prioridadeOption.withText(prioridade));
    }

    async setLayoutDocumento(layoutDocumento) {
        await t
            .click(this.layoutDocumento)
            .click(this.layoutDocumentoOption.withText(layoutDocumento));
    }

    async setMaterial(material, quantidadeMaterial) {
        await t
            .click(this.material)
            .click(this.materialOption.withText(material))
            .typeText(this.quantidadeMaterial, quantidadeMaterial)
            .click(this.botaoAdicionarMaterial);
    }

    async setTurno(turno, quantidadeTurno) {
        await t
            .click(this.turno)
            .click(this.turnoOption.withText(turno))
            .typeText(this.quantidadeTurno, quantidadeTurno)
            .click(this.botaoAdicionarTurno);
   }

    //CARACTERISTICAS
    async servicoRegulamentado(servicoRegulamentado) {
        if(servicoRegulamentado){
            await t.click(Selector("label").withAttribute( 'for', 'indicadorServicoRegulamento'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorServicoRegulamentoFalse'));
        }
    }

    async podeSerAgendado(podeSerAgendado) {
        if(podeSerAgendado) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorAgendamento'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorAgendamentoFalse'));
        }
    }

    async seraCobrado(seraCobrado, proximaFatura = false) {
        if(seraCobrado) {
            
            await t.click(Selector("label").withAttribute( 'for', 'indicadorCobranca'));
            
            if(proximaFatura) {
                await t.click(Selector("label").withAttribute( 'for', 'indicadorProximaFaturaSim'));
            } else {
                await t.click(Selector("label").withAttribute( 'for', 'indicadorProximaFaturaNao'));
            }
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorCobrancaFalse'));
        }
    }

    async equipamentoEspecial(equipamentoEspecial, equipamento = "") {
        if(equipamentoEspecial) {
            await t
                .click(Selector("label").withAttribute( 'for', 'indicadorEquipamento'))
                .click(Selector("#equipamento"))
                .click(Selector("#equipamento").find('option').withText(equipamento))
                .click(Selector("#botaoIncluirEquipamento"));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEquipamentoFalse'));
        }
    }

    async atualizacaoCadastral(atualizacaoCadastral = 0, Tela = "") {
        if(atualizacaoCadastral == 1) {
            await t
                .click(Selector("label").withAttribute( 'for', 'indicadorAtualizacaoCadastral'))
                .click(Selector("#telaAtualizacao"))
                .click(Selector("#telaAtualizacao").find('option').withText(Tela));
        }else if(atualizacaoCadastral == 2) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorAtualizacaoCadastralPosterior'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorAtualizacaoCadastralFalse'));
        }
    }

    async necessitaVeiculo(necessitaVeiculo) {
        if(necessitaVeiculo) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorVeiculo'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorVeiculoFalse'));
        }
    }

    async pesquisaSatisfacaoAutomatica(pesquisaSatisfacaoAutomatica) {
        if(pesquisaSatisfacaoAutomatica) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorPesquisaSatisfacao'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorPesquisaSatisfacaoFalse'));
        }
    }

    async gerarLoteAutorizacaoServico(gerarLoteAutorizacaoServico) {
        if(gerarLoteAutorizacaoServico) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorGeraLote'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorGeraLoteFalse'));
        }
    }

    async encerramentoAutomantico(encerramentoAutomantico) {
        if(encerramentoAutomantico) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEncerramentoAuto'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEncerramentoAutoFalse'));
        }
    }

    // ABA CAMPOS OBRIGATÓRIOS
    async camposObrigatorios(cliente, imovel, pontoConsumo, contrato, formulario) {
        if(cliente) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorClienteObrigatorio'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorClienteObrigatorioFalse'));
        }

        if(imovel) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorImovelObrigatorio'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorImovelObrigatorioFalse'));
        }

        if(pontoConsumo) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorPontoConsumoObrigatorio'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorPontoConsumoObrigatorioFalse'));
        }

        if(contrato) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorContratoObrigatorio'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorContratoObrigatorioFalse'));
        }

        if(formulario) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorFormularioObrigatorioTrue'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorFormularioObrigatorioFalse'));
        }
    }

    // ABA ENVIO DE E-MAIL
    async enviarEmail(aoCadastrar, aoColocarServicoExecucao, aoExecutarServico, aoEncerrarServico, aoAlterarServico) {
        if(aoCadastrar) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEmailCadastrar'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEmailCadastrarFalse'));
        }

        if(aoColocarServicoExecucao) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEmailEmExecucao'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEmailEmExecucaoFalse'));
        }

        if(aoExecutarServico) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEmailExecucao'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEmailExecucaoFalse'));
        }

        if(aoEncerrarServico) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEmailEncerrar'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEmailEncerrarFalse'));
        }
        
        if(aoAlterarServico) {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEmailAlterar'));
        } else {
            await t.click(Selector("label").withAttribute( 'for', 'indicadorEmailAlterarFalse'));
        }
    }
}
