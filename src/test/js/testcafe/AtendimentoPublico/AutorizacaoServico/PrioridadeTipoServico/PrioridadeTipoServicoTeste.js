import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';
import PrioridadeTipoServicoPage from './PrioridadeTipoServicoPage';

const page = new Page()
const prioridadeTipoServicoPage = new PrioridadeTipoServicoPage();

fixture('Prioridade do Tipo de Serviço')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await prioridadeTipoServicoPage.irParaPrioridadeTipoServico();
    });

test('CRUD', async t => {

    await prioridadeTipoServicoPage.incluir("ALTISSÍSSIMA", "28", true);
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Prioridade do Tipo de Serviço inserido(a) com sucesso.');

    await prioridadeTipoServicoPage.selecionar('ALTISSÍSSIMA');
    await prioridadeTipoServicoPage.alterar("IMPORTANTÍSSIMA", "78", false);
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Prioridade do Tipo de Serviço alterado(a) com sucesso.');

    await prioridadeTipoServicoPage.selecionar('IMPORTANTISSIMA');
    await prioridadeTipoServicoPage.remover();
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Prioridade do Tipo de Serviço removida(s) com sucesso.');
});

test('Teste de Obrigatoriedade', async t => {

    //Tela de Inclusão
    await t
        .click(prioridadeTipoServicoPage.botaoIncluir)
        .click(prioridadeTipoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, Quantidade máxima de horas para a execução do serviço é (são) de preenchimento obrigatório.");

    await prioridadeTipoServicoPage.setDias(true);
    await t
        .click(prioridadeTipoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, Quantidade máxima de horas para a execução do serviço é (são) de preenchimento obrigatório.");

    await prioridadeTipoServicoPage.setDescricao("ALTISSÍSSIMA");
    await t
        .click(prioridadeTipoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Quantidade máxima de horas para a execução do serviço é (são) de preenchimento obrigatório.");

    await prioridadeTipoServicoPage.setQtdHoraMaxima("28");
    await t
        .click(prioridadeTipoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Prioridade do Tipo de Serviço inserido(a) com sucesso.");

    //Tela de ALteração
    await prioridadeTipoServicoPage.selecionar("ALTISSÍSSIMA");
    await t
        .click(prioridadeTipoServicoPage.botaoAlterar)
        .click(prioridadeTipoServicoPage.botaoLimpar)
        .click(prioridadeTipoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, Quantidade máxima de horas para a execução do serviço é (são) de preenchimento obrigatório.");

    await prioridadeTipoServicoPage.setDias(false);
    await t
        .click(prioridadeTipoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, Quantidade máxima de horas para a execução do serviço é (são) de preenchimento obrigatório.");

    await prioridadeTipoServicoPage.setDescricao("IMPORTANTISSIMA");
    await t
        .click(prioridadeTipoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Quantidade máxima de horas para a execução do serviço é (são) de preenchimento obrigatório.");

    await prioridadeTipoServicoPage.setQtdHoraMaxima("28");
    await t
        .click(prioridadeTipoServicoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Prioridade do Tipo de Serviço alterado(a) com sucesso.");
    
    //Remoção
    await prioridadeTipoServicoPage.selecionar('IMPORTANTISSIMA');
    await prioridadeTipoServicoPage.remover();
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Prioridade do Tipo de Serviço removida(s) com sucesso.');
});