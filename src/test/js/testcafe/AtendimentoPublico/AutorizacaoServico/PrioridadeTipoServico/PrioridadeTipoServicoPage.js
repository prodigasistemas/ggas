import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class PrioridadeTipoServicoPage {

    constructor() {

        this.botaoPesquisar = Selector("#botaoPesquisar");
        this.botaoIncluir = Selector('#buttonIncluir');
        this.botaoAlterar = Selector('#buttonAlterar');
        this.botaoRemover = Selector('#buttonRemover');
        this.botaoSalvar = Selector('input[value=Salvar]');
        this.botaoLimpar = Selector('input[value=Limpar]');

        this.descricao = Selector("#descricao");
        this.qtdHoraMaxima = Selector("#quantidadeHorasMaxima");

        this.indicadorCorrido = Selector("#indicadorCorrido");
        this.indicadorUtil = Selector("#indicadorUtil");
    }

    async irParaPrioridadeTipoServico(){
        await t
            .navigateTo(page.url+"/exibirPesquisarServicoTipoPrioridade");
    }

    async selecionar(opcao) {
        await t.click(Selector("#servicoTipo tbody td").withText(opcao).parent("tr").find("input"));
    }

    async incluir(descricao, qtdHoraMaxima, dias) {

        await t.click(this.botaoIncluir);

        await this.setDescricao(descricao);
        await this.setQtdHoraMaxima(qtdHoraMaxima);
        await this.setDias(dias);

        await t.click(this.botaoSalvar);
    }

    async alterar(descricao, qtdHoraMaxima, dias) {
        
        await t
            .click(this.botaoAlterar)
            .click(this.botaoLimpar);

        await this.setDescricao(descricao);
        await this.setQtdHoraMaxima(qtdHoraMaxima);
        await this.setDias(dias);

        await t.click(this.botaoSalvar);
    }

    async remover() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }

    async setDescricao(descricao) {
        await t
            .typeText(this.descricao, descricao);
    }

    async setQtdHoraMaxima(qtdHoraMaxima) {
        await t
            .typeText(this.qtdHoraMaxima, qtdHoraMaxima);
    }

    async setDias(dias) {
        if(dias) {
            await t.click(this.indicadorCorrido);
        }else {
            await t.click(this.indicadorUtil);
        }
    }

}
