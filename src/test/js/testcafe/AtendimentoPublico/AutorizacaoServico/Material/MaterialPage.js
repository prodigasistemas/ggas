import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class MaterialPage {

    constructor() {

        this.botaoPesquisar = Selector("#botaoPesquisar");
        this.botaoIncluir = Selector('#buttonIncluir');
        this.botaoAlterar = Selector('#buttonAlterar');
        this.botaoRemover = Selector('#buttonRemover');
        this.botaoSalvar = Selector('input[value=Salvar]');
        this.botaoLimpar = Selector('input[value=Limpar]');

        this.descricao = Selector("#descricao");
        this.unidadeMedida = Selector("#unidadeMedida");
        this.unidadeMedidaOption = this.unidadeMedida.find("option");
        this.codigoERP = Selector("#codigoMaterial");
    }

    async irParaMaterial(){
        await t
            .navigateTo(page.url+"/pesquisarMaterial");
    }

    async selecionar(opcao) {
        await t
            .click(Selector("table#material tbody td").withText(opcao).parent("tr").find("input"));
    }

    async incluir(descricao, unidadeMedida, codigoERP) {
        await t.click(this.botaoIncluir);

        await this.setDescricao(descricao);
        await this.setUnidadeMedida(unidadeMedida);
        await this.setCodigoERP(codigoERP);

        await t.click(this.botaoSalvar);
    }

    async alterar(descricao, unidadeMedida, codigoERP) {
        await t
            .click(this.botaoAlterar)
            .click(this.botaoLimpar);

        await this.setDescricao(descricao);
        await this.setUnidadeMedida(unidadeMedida);
        await this.setCodigoERP(codigoERP);

        await t.click(this.botaoSalvar);
    }

    async remover() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }

    async setDescricao(descricao) {
        await t
            .typeText(this.descricao, descricao);
    }

    async setUnidadeMedida(unidadeMedida) {
        await t
            .click(this.unidadeMedida)
            .click(this.unidadeMedidaOption.withText(unidadeMedida));
    }

    async setCodigoERP(codigoERP) {
        await t
            .typeText(this.codigoERP, codigoERP);
    }

    async setIndicador(indicador) {
        await t
            .click(Selector("#habilitado[value="+indicador+"]"));
    }

}
