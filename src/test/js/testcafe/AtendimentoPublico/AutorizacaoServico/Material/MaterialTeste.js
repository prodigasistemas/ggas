import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';
import MaterialPage from './MaterialPage';

const page = new Page()
const materialPage = new MaterialPage();

fixture('Material')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await materialPage.irParaMaterial();
    });

test('CRUD', async t => {

    await materialPage.incluir("Mangueiras Para Medição De Pressão", "Bar", "F8GFUGQ4F8G43F8UG348FUH");
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Material inserido(a) com sucesso.');

    await materialPage.selecionar('F8GFUGQ4F8G43F8UG348FUH');
    await materialPage.alterar("Termômetro", "Celsius", "ASNDV943QNV3Q489VHH");
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Material alterado(a) com sucesso.');

    await materialPage.selecionar('ASNDV943QNV3Q489VHH');
    await materialPage.remover();
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Material removido(s) com sucesso.');
});

test('Teste de Obrigatoriedade', async t => {

    //Tela de Inclusão
    await t
        .click(materialPage.botaoIncluir)
        .click(materialPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição do Material, Unidade de Medida é (são) de preenchimento obrigatório.");

    await materialPage.setCodigoERP("F8GFUGQ4F8G43F8UG348FUH");
    await t
        .click(materialPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição do Material, Unidade de Medida é (são) de preenchimento obrigatório.");

    await materialPage.setDescricao("Mangueiras Para Medição De Pressão");
    await t
        .click(materialPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Unidade de Medida é (são) de preenchimento obrigatório.");

    await materialPage.setUnidadeMedida("Bar");
    await t
        .click(materialPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Material inserido(a) com sucesso.");

    //Tela de Alteração
    await materialPage.selecionar('F8GFUGQ4F8G43F8UG348FUH');
    await t
        .click(materialPage.botaoAlterar)
        .click(materialPage.botaoLimpar)
        .click(materialPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição do Material, Unidade de Medida é (são) de preenchimento obrigatório.");

    await materialPage.setCodigoERP("ASNDV943QNV3Q489VHH");
    await t
        .click(materialPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição do Material, Unidade de Medida é (são) de preenchimento obrigatório.");

    await materialPage.setDescricao("Termômetro");
    await t
        .click(materialPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Unidade de Medida é (são) de preenchimento obrigatório.");

    await materialPage.setUnidadeMedida("Celsius");
    await t
        .click(materialPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Material alterado(a) com sucesso.");

    //Remoção
    await materialPage.selecionar('ASNDV943QNV3Q489VHH');
    await materialPage.remover();
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Material removido(s) com sucesso.');
});