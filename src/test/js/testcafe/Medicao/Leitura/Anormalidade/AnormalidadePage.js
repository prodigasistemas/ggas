import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class AnormalidadePage {

    constructor(){
        this.botaoIncluir = Selector('#buttonIncluir');

        this.descricao = Selector('#descricao');
        this.descricaoAbreviada = Selector('#descricaoAbreviada');
        
        this.mensagemLeitura = Selector('#mensagemLeitura');
        this.mensagemManutencao = Selector('#mensagemManutencao');
        this.mensagemPrevencao = Selector('#mensagemPrevencao');
        this.numeroOcorrenciaConsecutivasAnormalidade = Selector('#numeroOcorrenciaConsecutivasAnormalidade');
        this.regra = Selector('textarea[name=regra]');

        this.acaoAnormalidadeComLeitura = Selector('#acaoAnormalidadeComLeitura'); 
        this.acaoAnormalidadeComLeituraOptions = this.acaoAnormalidadeComLeitura.find('option');

        this.acaoAnormalidadeSemLeitura = Selector('#idAcaoAnormalidadeSemLeitura'); 
        this.acaoAnormalidadeSemLeituraOptions = this.acaoAnormalidadeSemLeitura.find('option');

        this.acaoAnormalidadeComConsumo = Selector('#acaoAnormalidadeComConsumo'); 
        this.acaoAnormalidadeComConsumoOptions = this.acaoAnormalidadeComConsumo.find('option');

        this.acaoAnormalidadeSemConsumo = Selector('#idAcaoAnormalidadeSemConsumo'); 
        this.acaoAnormalidadeSemConsumoOptions = this.acaoAnormalidadeSemConsumo.find('option');
        
        this.tipoAnormalidadeSelect = Selector('#tipoAnormalidade');
        
        this.tipoAnormalidadeOption = this.tipoAnormalidadeSelect.find('option');
        
        this.indicadorUsoPesquisa = Selector('#indicadorUso'); 
        this.indicadorUsoPesquisaOptions = this.indicadorUsoPesquisa.find('option');

        this.aceitaLeitura = Selector('label[for=aceitaLeituraSim]');

        this.relativoMedidor = Selector('label[for=relativoMedidorSim]');

        this.bloquearFaturamento = Selector('label[for=bloquearFaturamentoSim]');

        this.indicadorUsoNao = Selector('#indicadorUsoNao');

        this.botaoSubmit = Selector('#salvar');

        this.botaoPesquisar = Selector('#botaoPesquisar');

        this.botaoAlterar = Selector('#btnAlterar');

        this.botaoRemover = Selector('#btnRemover');
        
    }

    async irParaAnormalidade() {
        await t.navigateTo(page.url+"/exibirPesquisaAnormalidade");
    }

    async removerAnormalidade(anormalidade){

        await this.setDescricao(anormalidade);
        await this.setIndicadorUsoPesquisa("Todos");
        await this.setTipoAnormalidade("Anormalidade de Leitura");
        await t.click(this.botaoPesquisar);
        await t.click(Selector("#table-anormalidades-leitura tbody td").withText(anormalidade).parent("tr").find("div.custom-control"));
        
        await t.click(this.botaoRemover);
        await t.click(Selector('button.swal2-confirm'));

    }

    async inclusãoAnormalidade(descricao
        , descricaoAbreviada
        , mensagemLeitura
        , mensagemManutencao
        , mensagemPrevencao
        , numeroOcorrenciaConsecutivasAnormalidade
        , regra
        , acaoAnormalidadeComLeitura
        , acaoAnormalidadeComConsumo
        , acaoAnormalidadeSemLeitura
        , acaoAnormalidadeSemConsumo
        , aceitaLeitura
        , relativoMedidor
        , bloquearFaturamento
    ) {

        await t.expect(this.botaoIncluir.exists).ok().click(this.botaoIncluir);

        await this.setDescricao(descricao);
        await this.setDescricaoAbreviada(descricaoAbreviada);
        await this.setMensagemLeitura(mensagemLeitura);
        await this.setMensagemManutencao(mensagemManutencao);
        await this.setMensagemPrevencao(mensagemPrevencao);
        await this.setNumeroOcorrenciaConsecutivasAnormalidade(numeroOcorrenciaConsecutivasAnormalidade);
        await this.setRegra(regra);
        await this.setAcaoAnormalidadeComLeitura(acaoAnormalidadeComLeitura);
        await this.setAcaoAnormalidadeComConsumo(acaoAnormalidadeComConsumo);

        await this.setAcaoAnormalidadeSemLeitura(acaoAnormalidadeSemLeitura);
        await this.setAcaoAnormalidadeSemConsumo(acaoAnormalidadeSemConsumo);
        await this.setAceitaLeitura(aceitaLeitura);
        await this.setRelativoMedidor(relativoMedidor);
        await this.setBloquearFaturamento(bloquearFaturamento);

        await t.click(this.botaoSubmit);

    }

    async alterarAnormalidade(anormalidade
        , descricao
        , descricaoAbreviada
        , mensagemLeitura
        , mensagemManutencao
        , mensagemPrevencao
        , regra
        , indicadorUso
        , acaoAnormalidadeComLeitura
        , acaoAnormalidadeComConsumo
        , acaoAnormalidadeSemLeitura 
        , acaoAnormalidadeSemConsumo
        , aceitaLeitura
        , relativoMedidor
        , bloquearFaturamento
    ) {
        await t.typeText(Selector('#descricao'), anormalidade);
        
        await this.setTipoAnormalidade("Anormalidade de Leitura");
        
        await t.click(this.botaoPesquisar);
         
        await t.click(Selector("#table-anormalidades-leitura tbody td").withText(anormalidade).parent("tr").find("div.custom-control"));

        await this.setTipoAnormalidade("Anormalidade de Leitura");
        
        await t.click(this.botaoAlterar);

        await this.limparFormulario();

        await this.setDescricao(descricao);
        await this.setDescricaoAbreviada(descricaoAbreviada);
        await this.setMensagemLeitura(mensagemLeitura);
        await this.setMensagemManutencao(mensagemManutencao);
        await this.setMensagemPrevencao(mensagemPrevencao);
        await this.setRegra(regra);
        await this.setIndicadorUsoNao(indicadorUso);
        await this.setAcaoAnormalidadeComLeitura(acaoAnormalidadeComLeitura);
        await this.setAcaoAnormalidadeSemLeitura(acaoAnormalidadeSemLeitura);
        await this.setAcaoAnormalidadeComConsumo(acaoAnormalidadeComConsumo);
        await this.setAcaoAnormalidadeSemConsumo(acaoAnormalidadeSemConsumo);
        await this.setAceitaLeitura(aceitaLeitura);
        await this.setRelativoMedidor(relativoMedidor);
        await this.setBloquearFaturamento(bloquearFaturamento);

        await t.click(this.botaoSubmit);

    }

    async testeObrigatoriedade(descricao
        , acaoAnormalidadeComLeitura
        , acaoAnormalidadeComConsumo
        , acaoAnormalidadeSemLeitura
        , acaoAnormalidadeSemConsumo
    ) {

        await t.click(this.botaoIncluir);

        await t.click(this.botaoSubmit);

        await this.setDescricao(descricao);
        await t.click(this.botaoSubmit);

        await this.setAcaoAnormalidadeComLeitura(acaoAnormalidadeComLeitura);
        await t.click(this.botaoSubmit);

        await this.setAcaoAnormalidadeComConsumo(acaoAnormalidadeComConsumo);
        await t.click(this.botaoSubmit);


        await this.setAcaoAnormalidadeSemLeitura(acaoAnormalidadeSemLeitura);
        await t.click(this.botaoSubmit);

        await this.setAcaoAnormalidadeSemConsumo(acaoAnormalidadeSemConsumo);
        await t.click(this.botaoSubmit);

    }

    async setDescricao(descricao){
        await t.typeText(this.descricao, descricao);
    }

    async setDescricaoAbreviada(descricaoAbreviada){
        await t.typeText(this.descricaoAbreviada, descricaoAbreviada);
    }

    async setMensagemLeitura(mensagemLeitura){
        await t.typeText(this.mensagemLeitura, mensagemLeitura);
    }

    async setMensagemManutencao(mensagemManutencao){
        await t.typeText(this.mensagemManutencao, mensagemManutencao);
    }

    async setMensagemPrevencao(mensagemPrevencao){
        await t.typeText(this.mensagemPrevencao, mensagemPrevencao);
    }

    async setNumeroOcorrenciaConsecutivasAnormalidade(numeroOcorrenciaConsecutivasAnormalidade){
        await t.typeText(this.numeroOcorrenciaConsecutivasAnormalidade, numeroOcorrenciaConsecutivasAnormalidade);
    }

    async setRegra(regra){
        await t.typeText(this.regra, regra);
    }

    async setAcaoAnormalidadeComLeitura(acaoAnormalidadeComLeitura){
        await t.click(this.acaoAnormalidadeComLeitura);
        await t.click(this.acaoAnormalidadeComLeituraOptions.withText(acaoAnormalidadeComLeitura));
    }

    async setAcaoAnormalidadeSemLeitura(acaoAnormalidadeSemLeitura){
        await t.click(this.acaoAnormalidadeSemLeitura);
        await t.click(this.acaoAnormalidadeSemLeituraOptions.withText(acaoAnormalidadeSemLeitura));
    }
    
    async setIndicadorUsoPesquisa(indicadorUsoPesquisa){
        await t.click(this.indicadorUsoPesquisa);
        await t.click(this.indicadorUsoPesquisaOptions.withText(indicadorUsoPesquisa));
    }
    
    async setTipoAnormalidade(tipoAnormalidade){
        await t.click(this.tipoAnormalidadeSelect);
        await t.click(this.tipoAnormalidadeOption.withText(tipoAnormalidade));
    }

    async setAcaoAnormalidadeComConsumo(acaoAnormalidadeComConsumo){
        await t.click(this.acaoAnormalidadeComConsumo);
        await t.click(this.acaoAnormalidadeComConsumoOptions.withText(acaoAnormalidadeComConsumo));
    }

    async setAcaoAnormalidadeSemConsumo(acaoAnormalidadeSemConsumo){
        await t.click(this.acaoAnormalidadeSemConsumo);
        await t.click(this.acaoAnormalidadeSemConsumoOptions.withText(acaoAnormalidadeSemConsumo));
    }

    async setAceitaLeitura(aceitaLeitura) {
        if(aceitaLeitura)
            await t.click(this.aceitaLeitura);
    }

    async setRelativoMedidor(relativoMedidor) {
        if(relativoMedidor)
            await t.click(this.relativoMedidor);
    }

    async setBloquearFaturamento(bloquearFaturamento) {
        if(bloquearFaturamento)
            await t.click(this.bloquearFaturamento);
    }

    async setIndicadorUsoNao(indicadorUsoNao) {
        if(!indicadorUsoNao)
            await t.click(this.indicadorUsoNao);
    }

    async limparFormulario(){

        page.limparCampo(this.descricao);
        page.limparCampo(this.descricaoAbreviada);
        page.limparCampo(this.mensagemLeitura);
        page.limparCampo(this.mensagemManutencao);
        page.limparCampo(this.mensagemPrevencao);
        page.limparCampo(this.regra);
        
    }

    

}