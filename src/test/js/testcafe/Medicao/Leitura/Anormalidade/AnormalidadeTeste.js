import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';
import AnormalidadePage from './AnormalidadePage';


const page = new Page();
const anormalidadePage = new AnormalidadePage();

fixture.skip("Mediação-Leitura-Anormalidade-")
    .page(page.url)
    .beforeEach(async t => {

        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await anormalidadePage.irParaAnormalidade();

    });

test('Inclusão - Anormalidade', async t => {

    await anormalidadePage.inclusãoAnormalidade(
        'TESTE ANORMALIDADE 1'
        , 'TESTE ANORMALIDADE'
        , 'TESTE ANORMALIDADE'
        , 'TESTE ANORMALIDADE'
        , 'TESTE ANORMALIDADE'
        , 'TESTE ANORMALIDADE'
        , 'TESTE ANORMALIDADE'
        , 'Anterior'
        , 'MEDIA'
        , 'Anterior'
        , 'MEDIA'
        , true
        , true
        , true
    );

    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Anormalidade leitura inserido(a) com sucesso.");

});

test('Alterar - Anormalidade', async t => {
    await  anormalidadePage.alterarAnormalidade(
          'TESTE ANORMALIDADE 1'
        , 'TESTE ANORMALIDADE 1'
        , 'TESTE ANORMALIDADE 1'
        , 'TESTE ANORMALIDADE 1'
        , 'TESTE ANORMALIDADE 1'
        , 'TESTE ANORMALIDADE 1'
        , 'TESTE ANORMALIDADE 1'
        , false
        , 'Anterior'
        , 'MEDIA'
        , 'Anterior'
        , 'MEDIA'
        , false
        , false
        , false
    );

    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Anormalidade leitura alterado(a) com sucesso.");
    
});

test('Remover - Anormalidade', async t => {
    await  anormalidadePage.removerAnormalidade("TESTE ANORMALIDADE 1");

    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Anormalidade leitura removida(s) com sucesso.");
});

test('Obrigatoriedade - Anormalidade', async t => {
    await anormalidadePage.testeObrigatoriedade('TESTE ANORMALIDADE 1'
        , 'Anterior'
        , 'MEDIA'
        , 'Anterior'
        , 'MEDIA'
    );

    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Anormalidade leitura inserido(a) com sucesso.");
});



