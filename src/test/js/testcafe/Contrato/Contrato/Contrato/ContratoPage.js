import { Selector, t, ClientFunction} from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class ContratoPage {

    constructor() {

        //botoes
        this.botaoPesquisar = Selector("#botaoPesquisar");
        this.botaoIncluir = Selector('#botaoIncluir');
        this.botaoAditar = Selector('#aditar-contrato');
        this.botaoAlterar = Selector('input.botaoAlterar[value=Alterar]');
        this.botaoEncerrarRescindir = Selector('#botaoEncerrarRescindir');
        this.botaoRemover = Selector('input[value=Remover]');
        this.finalizarEncerrarRescindir = Selector('#botaoFinalizar');
        this.botaoEncerrar = Selector('#botaoEncerrarRescindir');
        this.botaoMigrarContrato = Selector("input[value='Migrar Contrato']");
        this.botaoMigrarSaldo = Selector("input[value='Migrar Saldo']");
        this.botaoIncluirComplementar = Selector("input[value='Incluir Complementar']");
        this.botaoOrdenarFaturamento = Selector('input[value="Ordenar Faturamento"');
        this.botaoSalvar = Selector('input[value=Salvar]');
        this.botaoLimpar = Selector('input[value=Limpar]');
        this.botaoSalvarParcial = Selector("input[value='Salvar Parcial']");
        this.botaoCancelar = Selector("input[value='Cancelar']");
        this.botaoExibir = Selector("#botaoExibirModeloContrato");
        this.botaoOkQDC = Selector("#botaoOKQDC");
        this.botaoExcluirTakeOrPay = Selector("#take_or_pay input[value=Excluir]");
        this.botaoExcluirPenalidade = Selector("#conteinerRetiradaMaiorMenor input[value=Excluir]");
        this.botaoAdicionarPenalidade = Selector("#botaoAdicionarRetirMaiorMenor");
        this.botaoAvancar = Selector("#avancar-contrato");
        this.botaoVoltar = Selector("button[value='<< Voltar'");
        this.botaoAdicionarLocalAmostragem = Selector("#adicionar-local-amostragem");
        this.botaoRetirarTodosLocalAmostragem = Selector("#conteinerLocalAmostragem input[value='Todos <<']");
        this.botaoAdicionarIntervaloAmostragem = Selector("#adicionar-intervalo-amostragem");
        this.botaoRetirarTodosIntervaloAmostragem = Selector("#conteinerIntervaloAmostragem input[value='Todos <<']");
        this.botaoAdicionarFaturamento = Selector("#botaoAdicionarItemFaturamento");
        this.botaoExcluirFaturamento = Selector("#botaoExcluirItemFaturamento");
        this.botaoAdicionarModalidade = Selector("#botaoAdicionar");
        this.botaoExcluirModalidade = Selector("fieldset#modalidades .conteinerBotoesAba input[value=Excluir]");
        this.botaoAdicionarQDCModalidade = Selector("#botaoOKQDC");
        this.botaoAdicionarTakeOrPayModalidade = Selector("#botaoAdicionarTakeOrPay");
        this.botaoAdicionarResponsabilidade = Selector("#botaoIncluirContratoResponsabilidade");
        this.botaoExcluirResponsabilidade = Selector("img[title='Excluir Responsabilidade']");
        this.botaoAplicar = Selector("#botaoAplicarIncluirContratoPontoConsumo");
        this.botaoEditarTakeOrPay = Selector("#botaoEditarTakeOrPay");
        this.botaoEditarPenalidade = Selector("#abaContratoPenalidades input[value=Editar]");
        this.botaoTransferir = Selector("input[value=Transferir]");

        //Campos da tela de pesquisa
        this.indicadorPesquisaCliente = Selector("#indicadorPesquisaCliente");
        this.indicadorPesquisaImovel = Selector("#indicadorPesquisaImovel");
        this.numeroContrato = Selector("#numeroContrato");
        this.situacaoContrato = Selector("#situacaoContrato");
        this.situacaoContratoOption = this.situacaoContrato.find("option");
        this.tipoContrato = Selector("#idTipoContrato");
        this.tipoContratoOption = this.tipoContrato.find("option");
        this.modeloContrato = Selector("#idModeloContrato");
        this.modeloContratoOption = this.modeloContrato.find("option");
        this.tabelaContrato = Selector("table#contrato tbody td");

        //Campos da tela incluir

        this.modeloContratoInclusao = Selector("#listaModeloContrato");
        this.modeloContratoInclusaoOption = this.modeloContratoInclusao.find("option");

        this.aba = Selector("li[role=tab]");
        this.subAba = Selector("#abaTakeOrPay #menuAbasInternas #abasInternas ul li");

        //Dados Gerais
        this.dataAssinatura = Selector("#dataAssinatura");
        this.dataVencimentoObrigacoesContratuais = Selector("#dataVencObrigacoesContratuais");
        this.valorContrato = Selector("#valorContrato");
        this.dataAditivo = Selector("#dataAditivo");
        this.descricaoAditivo = Selector("#descricaoAditivo");
        this.descricao = Selector('#descricaoContrato');
        this.volumeReferencia = Selector('#volumeReferencia');

        // PROPOSTA
        this.gastoMensalcomGN = Selector("#gastoEstimadoGNMes");
        this.economiaMensalcomGN = Selector("#economiaEstimadaGNMes");
        this.economiaAnualcomGN = Selector("#economiaEstimadaGNAno");
        this.descontoEfetivoEconomia = Selector("#descontoEfetivoEconomia");
        this.diaVencimentoFinanciamento = Selector("#diaVencFinanciamento");
        this.diaVencimentoFinanciamentoOption = this.diaVencimentoFinanciamento.find("option");
        this.valorParticipacaoCliente = Selector("#valorParticipacaoCliente");
        this.quantidadeParcelasFinanciamento = Selector("#qtdParcelasFinanciamento");
        this.percentualJurosFinanciamento = Selector("#percentualJurosFinanciamento");
        this.sistemaAmortizacao = Selector("#sistemaAmortizacao");
        this.sistemaAmortizacaoOption = this.sistemaAmortizacao.find("option");
        this.valorInvestimento = Selector("#valorInvestimento");
        this.dataInvestimento = Selector("#dataInvestimento");

        // DADOS FINANCEIROS
        this.tipoAgrupamento = Selector("#tipoAgrupamento");
        this.tipoAgrupamentoOption = this.tipoAgrupamento.find("option");
        this.tipoConvenio = Selector("#formaCobranca");
        this.tipoConvenioOption = this.tipoConvenio.find("option");
        this.arrecadadorConvenio = Selector("#arrecadadorConvenio");
        this.arrecadadorConvenioOption = this.arrecadadorConvenio.find("option");
        this.percentualMulta = Selector("#percentualMulta");
        this.percentualJurosMora = Selector("#percentualJurosMora");
        this.debitoAutomaticoSim = Selector("#incluirContrato-debitoAutomatico").withAttribute('value', 'true');
        this.debitoAutomaticoNao = Selector("#incluirContrato-debitoAutomatico").withAttribute('value', 'false');
        this.banco = Selector("#banco");
        this.bancoOption = this.banco.find("option");
        this.agencia = Selector("#agencia");
        this.contaCorrente = Selector("#contaCorrente");

        // TAKE OR PAY
        //// ABA QDC
        this.dataInicioVigenciaQDC = Selector("#QDCData");
        this.QDCValor = Selector("#QDCValor");
        this.QDC = Selector("#QDCContrato").find("option");
        this.botaoExcluirQDC = Selector("#botaoExcluirQDC");

        //// ABA TAKE OR PAY
        this.periodicidadeToP = Selector("#periodicidadeTakeOrPayC");
        this.periodicidadeToPOption = this.periodicidadeToP.find("option");
        this.referenciaQuantidadeFaltanteParadaProgramada = Selector("#tipoReferenciaQFParadaProgramada");
        this.referenciaQuantidadeFaltanteParadaProgramadaOption = this.referenciaQuantidadeFaltanteParadaProgramada.find("option");
        this.consumoReferencialToP = Selector("#mensalConsumoReferenciaInferior");
        this.consumoReferencialToPOption = this.consumoReferencialToP.find("option");
        this.compromissoRetiradaToP = Selector("#margemVariacaoTakeOrPayC");
        this.dataInicioVigencia = Selector("#dataInicioVigenciaC");
        this.dataFimVigencia = Selector("#dataFimVigenciaC");
        this.tabelaPrecosCobranca = Selector("#precoCobrancaC");
        this.tabelaPrecosCobrancaOption = this.tabelaPrecosCobranca.find("option");
        this.formaCalculoCobrancaRecuperacao = Selector("#tipoApuracaoC");
        this.formaCalculoCobrancaRecuperacaoOption = this.formaCalculoCobrancaRecuperacao.find("option");
        this.percentualNaoRecuperavel = Selector("#percentualNaoRecuperavelC");
        this.formaApuracaoVolumeParadaProgramada = Selector("#apuracaoParadaProgramadaC");
        this.formaApuracaoVolumeParadaProgramadaOption = this.formaApuracaoVolumeParadaProgramada.find("option");
        this.formaApuracaoVolumeFalhaFornecimento = Selector("#apuracaoFalhaFornecimentoC");
        this.formaApuracaoVolumeFalhaFornecimentoOption = this.formaApuracaoVolumeFalhaFornecimento.find("option");
        this.formaApuracaoVolumeCasoFortuitoForcaMaior = Selector("#apuracaoCasoFortuitoC");
        this.formaApuracaoVolumeCasoFortuitoForcaMaiorOption = this.formaApuracaoVolumeCasoFortuitoForcaMaior.find("option");

        this.botaoAdicionarTakeOrPay = Selector("#botaoAdicionarTakeOrPay");

        // ABA PENALIDADES
        this.penalidade = Selector("#penalidadeRetMaiorMenor");
        this.penalidadeOption = this.penalidade.find("option");
        this.periodicidade = Selector("#periodicidadeRetMaiorMenor");
        this.periodicidadeOption = this.periodicidade.find("option");
        this.baseApuracao = Selector("#baseApuracaoRetMaiorMenor");
        this.baseApuracaoOption = this.baseApuracao.find("option");
        this.dataInicioVigenciaPenalidades = Selector("#dataIniVigRetirMaiorMenor");
        this.dataFimVigenciaPenalidades = Selector("#dataFimVigRetirMaiorMenor");
        this.tabelaPrecoCobranca = Selector("#precoCobrancaRetirMaiorMenor");
        this.tabelaPrecoCobrancaOption = this.tabelaPrecoCobranca.find("option");
        this.formaCalculoCobranca = Selector("#tipoApuracaoRetirMaiorMenor");
        this.formaCalculoCobrancaOption = this.formaCalculoCobranca.find("option");
        this.percentualCobranca = Selector("#percentualCobRetMaiorMenor");
        this.percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao = Selector("#percentualCobIntRetMaiorMenor");
        this.consumoReferencia = Selector("#consumoReferenciaRetMaiorMenor");
        this.consumoReferenciaOption = this.consumoReferencia.find("option");
        this.percentualRetirada = Selector("#percentualRetMaiorMenor");

        // PONTO DE CONSUMO
        this.adicionarPontoConsumo = ClientFunction( idImovel => {
            selecionarImovel( idImovel );
        });

        this.tabelaPontoConsumo = Selector("#pesquisaImovelContrato table tbody td");


        // SEGUNDA PARTE

        // PRINCIPAL
        this.ddd = Selector("#faxDDD");
        this.numero = Selector("#faxNumero");
        this.email = Selector("#email");

        // DADOS TÉCNICOS
        this.pressaoFornecimento = Selector("#faixaPressaoFornecimento");
        this.pressaoFornecimentoOption = this.pressaoFornecimento.find("option");
        this.pressaoColetada = Selector("#pressaoColetada");
        this.unidadePressaoColetada = Selector("#unidadePressaoColetada");
        this.unidadePressaoColetadaOption = this.unidadePressaoColetada.find("option");

        // Consumo
        this.localAmostragem = Selector("#locaisAmostragemDisponiveis").find("option");
        this.intervaloAmostragem = Selector("#intervalosAmostragemDisponiveis").find("option");
        this.tamanhoReducaoRecuperacaoPCS = Selector("#tamReducaoRecuperacaoPCS");

        //Faturamento
        this.tarifa = Selector("#tarifaConsumo");
        this.tarifaOption = this.tarifa.find("option");

        //Modalidades Consumo Faturamento
        this.modalidade = Selector("#tipoModalidade");
        this.modalidadeOption = this.modalidade.find("option");
        this.percentualQNR = Selector('#percentualQNR');
        this.inicioVigenciaQDCModalidade = Selector("#QDCData");
        this.QDCModalidade = Selector("#QDCValor");
        this.prazoRevisaoQuantidadesContratadas = Selector("#prazoRevisaoQuantidadesContratadas");
        this.dataInicioVigenciaModalidade = Selector("#dataInicioVigencia");
        this.dataFimVigenciaModalidade = Selector("#dataFimVigencia");
        this.tabelaPrecosCobrancaModalidade = Selector("#precoCobranca");
        this.tabelaPrecosCobrancaModalidadeOptions = this.tabelaPrecosCobrancaModalidade.find('option');
        this.formaCalculoCobrancaRecuperacaoModalidade = Selector("#tipoApuracao");
        this.formaCalculoCobrancaRecuperacaoModalidadeOptions = this.formaCalculoCobrancaRecuperacaoModalidade.find("option");
        this.percentualNaoRecuperavelModalidade = Selector("#percentualNaoRecuperavel");
        
        //Responsabilidades
        this.tipoResponsabilidade = Selector("#tipoResponsabilidade");
        this.tipoResponsabilidadeOption = this.tipoResponsabilidade.find("option");
        this.dataInicioRelacionamento = Selector("#dataInicioRelacao");
    
        // Migrar Saldo
        this.valorQNR = Selector("#valorTransferidoQNR");
        this.valorQPNR = Selector("#valorTransferidoQPNR");
    }

    async irParaContrato(){
        await t.navigateTo(page.url+"/exibirPesquisaContrato");
    }

    async selecionar(opcao) {
        await t.click(this.tabelaContrato.withExactText(opcao).parent("tr").find("input"));
    }
    
    async detalhar(opcao) {
        await t.click(this.tabelaContrato.withExactText(opcao));
    }

    async pesquisar(numeroContrato, situacaoContrato, tipoContrato, modeloContrato) {
        await t
            .typeText(this.numeroContrato, numeroContrato)
            .click(this.situacaoContrato)
            .click(this.situacaoContratoOption.withExactText(situacaoContrato))
            .click(this.tipoContrato)
            .click(this.tipoContratoOption.withExactText(tipoContrato))
            .click(this.modeloContrato)
            .click(this.modeloContratoOption.withExactText(modeloContrato))
            .click(this.botaoPesquisar);
    }

    async incluirContratoIndustrial(situacaoContrato, dataAssinatura, dataVencimentoObrigacoesContratuais, definicaoAnoContratual, tipoPeriodicidadePenalidade,
        gastoMensalcomGN, economiaMensalcomGN, economiaAnualcomGN, descontoEfetivoEconomia, diaVencimentoFinanciamento, valorParticipacaoCliente, quantidadeParcelasFinanciamento, percentualJurosFinanciamento, sistemaAmortizacao, valorInvestimento, dataInvestimento,
        faturamentoAgrupado, tipoAgrupamento, arrecadadorConvenio, contratoPassivelMulta, percentualMulta, contratoPassivelJurosMora, percentualJurosMora,
        dataInicioVigenciaQDC, valorQDC,
        periodicidadeToP, referenciaQuantidadeFaltanteParadaProgramada, consumoReferencialToP, compromissoRetiradaToP, dataInicioVigencia, dataFimVigencia, tabelaPrecosCobranca, formaCalculoCobrancaRecuperacao, recuperavel, percentualNaoRecuperavel, considerarParadasProgramadas, considerarFalhaFornecimento, considerarCasosFortuitos, formaApuracaoVolumeParadaProgramada, formaApuracaoVolumeFalhaFornecimento, formaApuracaoVolumeCasoFortuitoForcaMaior,
        penalidade, periodicidade, baseApuracao, dataInicioVigenciaPenalidades, dataFimVigenciaPenalidades, tabelaPrecoCobranca, formaCalculoCobranca, percentualCobranca, percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao, consumoReferencia, percentualRetirada,
        imovel, pontoConsumo,
        pressaoFornecimento,
        localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS,
        tarifa,
        modalidade, inicioVigenciaQDCModalidade, ValorQDCModalidade, prazoRevisaoQuantidadesContratadas, dataInicioVigenciaModalidade, dataFimVigenciaModalidade, tabelaPrecosCobrancaModalidade, formaCalculoCobrancaRecuperacaoModalidade, percentualNaoRecuperavelModalidade,
        pessoa, tipoResponsabilidade, dataInicioRelacionamento) {

        await t.click(this.botaoIncluir);
        await this.setModeloContrato("CONTRATO BRASKEM");

        await this.setDadosGerais(pessoa, situacaoContrato, dataAssinatura, dataVencimentoObrigacoesContratuais, definicaoAnoContratual, tipoPeriodicidadePenalidade);

        await this.irParaAba("Proposta");
        await this.setProposta(gastoMensalcomGN, economiaMensalcomGN, economiaAnualcomGN, descontoEfetivoEconomia, diaVencimentoFinanciamento, valorParticipacaoCliente, quantidadeParcelasFinanciamento, percentualJurosFinanciamento, sistemaAmortizacao, valorInvestimento, dataInvestimento);
        
        await this.irParaAba("Dados Financeiros");
        await this.setDadosFinanceiros(faturamentoAgrupado, tipoAgrupamento, arrecadadorConvenio, contratoPassivelMulta, percentualMulta, contratoPassivelJurosMora, percentualJurosMora);
        
        await this.irParaAba("Take or Pay");
        
        await this.irParaSubAba("QDC");
        await this.setQDCContrato(dataInicioVigenciaQDC, valorQDC);
        
        await this.irParaSubAba("Take or Pay");
        await this.setTakeOrPay(periodicidadeToP, referenciaQuantidadeFaltanteParadaProgramada, consumoReferencialToP, compromissoRetiradaToP, dataInicioVigencia, dataFimVigencia, tabelaPrecosCobranca, formaCalculoCobrancaRecuperacao, recuperavel, percentualNaoRecuperavel, considerarParadasProgramadas, considerarFalhaFornecimento, considerarCasosFortuitos, formaApuracaoVolumeParadaProgramada, formaApuracaoVolumeFalhaFornecimento, formaApuracaoVolumeCasoFortuitoForcaMaior);
        
        await t.wait(10000);

        await this.irParaAba("Penalidades");
        
        await t.wait(1500);
        
        await this.setPenalidades(penalidade, periodicidade, baseApuracao, dataInicioVigenciaPenalidades, dataFimVigenciaPenalidades, tabelaPrecoCobranca, formaCalculoCobranca, percentualCobranca, percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao, consumoReferencia, percentualRetirada);
        
        await this.irParaAba("Pontos de Consumo");
        await this.setPontoConsumo(imovel, pontoConsumo);
        await t.click(this.botaoAvancar);
        
        //Segunda parte

        await this.setDadosTecnicos(pressaoFornecimento);

        await this.irParaAba("Faturamento");
        await this.setFaturamento(tarifa);

        await this.irParaAba("Modalidades Consumo Faturamento");
        await this.setQDCModalidade(inicioVigenciaQDCModalidade, ValorQDCModalidade);
        await this.setTakeOrPayModalidade(prazoRevisaoQuantidadesContratadas, dataInicioVigenciaModalidade, dataFimVigenciaModalidade, tabelaPrecosCobrancaModalidade, formaCalculoCobrancaRecuperacaoModalidade, percentualNaoRecuperavelModalidade);
        await this.irParaAba("Modalidades Consumo Faturamento");
        await this.setModalidadeConsumoFaturamento(modalidade);

        await this.irParaAba("Responsabilidades");
        await this.setResponsabilidades(pessoa, tipoResponsabilidade, dataInicioRelacionamento);
        
        await this.irParaAba("Consumo");
        await this.setConsumo(localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS);

        await t
            .click(this.botaoAplicar)
            .click(this.botaoSalvar);
    }

    async incluirContratoResidencial(situacaoContrato, dataAssinatura, dataVencimentoObrigacoesContratuais, definicaoAnoContratual, valorContrato, tipoPeriodicidadePenalidade,
        faturamentoAgrupado, tipoAgrupamento, tipoConvenio, arrecadadorConvenio, contratoPassivelMulta, percentualMulta, contratoPassivelJurosMora, percentualJurosMora, debitoAutomatico, banco, agencia, contaCorrente,
        dataInicioVigenciaQDC, valorQDC,
        imovel, pontoConsumo,
        ddd, numero, email,
        pressaoFornecimento, pressaoColetada, unidadePressaoColetada,
        localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS,
        tarifa,
        modalidade,
        pessoa, tipoResponsabilidade, dataInicioRelacionamento) {
        
        await t.click(this.botaoIncluir);
        await this.setModeloContrato("RESIDENCIAL");

        // Dados Gerais
        await this.setDadosGerais(pessoa, situacaoContrato, dataAssinatura, dataVencimentoObrigacoesContratuais, definicaoAnoContratual, tipoPeriodicidadePenalidade);
        await this.setValorContrato(valorContrato);

        //Dados Financeiros
        await this.irParaAba("Dados Financeiros");
        await this.setTipoConvenio(tipoConvenio);
        await this.setDadosFinanceiros(faturamentoAgrupado, tipoAgrupamento, arrecadadorConvenio, contratoPassivelMulta, percentualMulta, contratoPassivelJurosMora, percentualJurosMora);
        await this.setDadosBancario(debitoAutomatico, banco, agencia, contaCorrente);
        
        //Take or Pay
        await this.irParaAba("Take or Pay");
        await this.irParaSubAba("QDC");
        await this.setQDCContrato(dataInicioVigenciaQDC, valorQDC);
        
        //Ponto de Consumo
        await this.irParaAba("Pontos de Consumo");
        await this.setPontoConsumo(pontoConsumo);

        await t.click(this.botaoAvancar);
        
        //SEGUNDA PARTE
        
        //Principal
        await this.setPrincipal(ddd, numero, email);

        //Dados Técnicos
        await this.irParaAba("Dados Técnicos");
        await this.setPressaoFornecimento(pressaoFornecimento);
        await this.setPressaoColetada(pressaoColetada);
        await this.setUnidadePressaoColetada(unidadePressaoColetada);

        //Faturamento
        await this.irParaAba("Faturamento");
        await this.setFaturamento(tarifa);

        await this.irParaAba("Modalidades Consumo Faturamento");
        await this.setModalidadeConsumoFaturamento(modalidade);

        await this.irParaAba("Responsabilidades");
        await this.setResponsabilidades(pessoa, tipoResponsabilidade, dataInicioRelacionamento);
        
        await this.irParaAba("Consumo");
        await this.setConsumo(localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS);

        await t
            .click(this.botaoAplicar)
            .click(this.botaoSalvar);
    }

    async aditar(dataAditivo, descricaoAditivo) {
        await t.click(this.botaoAditar);
        await this.setDataAditivo(dataAditivo);
        await this.setDescricaoAditivo(descricaoAditivo);
        await t.click(this.botaoSalvar);
    }

    async alterarContratoIndustrial(situacaoContrato, dataAssinatura, dataVencimentoObrigacoesContratuais, definicaoAnoContratual, tipoPeriodicidadePenalidade,
        gastoMensalcomGN, economiaMensalcomGN, economiaAnualcomGN, descontoEfetivoEconomia, diaVencimentoFinanciamento, valorParticipacaoCliente, quantidadeParcelasFinanciamento, percentualJurosFinanciamento, sistemaAmortizacao, valorInvestimento, dataInvestimento,
        faturamentoAgrupado, tipoAgrupamento, arrecadadorConvenio, contratoPassivelMulta, percentualMulta, contratoPassivelJurosMora, percentualJurosMora,
        dataInicioVigenciaQDC, valorQDC,
        periodicidadeToP, referenciaQuantidadeFaltanteParadaProgramada, consumoReferencialToP, compromissoRetiradaToP, dataInicioVigencia, dataFimVigencia, tabelaPrecosCobranca, formaCalculoCobrancaRecuperacao, recuperavel, percentualNaoRecuperavel, considerarParadasProgramadas, considerarFalhaFornecimento, considerarCasosFortuitos, formaApuracaoVolumeParadaProgramada, formaApuracaoVolumeFalhaFornecimento, formaApuracaoVolumeCasoFortuitoForcaMaior,
        penalidade, periodicidade, baseApuracao, dataInicioVigenciaPenalidades, dataFimVigenciaPenalidades, tabelaPrecoCobranca, formaCalculoCobranca, percentualCobranca, percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao, consumoReferencia, percentualRetirada,
        pressaoFornecimento,
        localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS,
        tarifa,
        modalidade, inicioVigenciaQDCModalidade, ValorQDCModalidade, prazoRevisaoQuantidadesContratadas, dataInicioVigenciaModalidade, dataFimVigenciaModalidade, tabelaPrecosCobrancaModalidade, formaCalculoCobrancaRecuperacaoModalidade, percentualNaoRecuperavelModalidade,
        pessoa, tipoResponsabilidade, dataInicioRelacionamento) {
        
        await t
            .click(this.botaoAlterar)
            .click(this.botaoLimpar);

        await this.setDadosGerais(pessoa, situacaoContrato, dataAssinatura, dataVencimentoObrigacoesContratuais, definicaoAnoContratual, tipoPeriodicidadePenalidade);
        
        await this.irParaAba("Proposta");
        await this.setProposta(gastoMensalcomGN, economiaMensalcomGN, economiaAnualcomGN, descontoEfetivoEconomia, diaVencimentoFinanciamento, valorParticipacaoCliente, quantidadeParcelasFinanciamento, percentualJurosFinanciamento, sistemaAmortizacao, valorInvestimento, dataInvestimento);
        
        await this.irParaAba("Dados Financeiros");
        await this.setDadosFinanceiros(faturamentoAgrupado, tipoAgrupamento, arrecadadorConvenio, contratoPassivelMulta, percentualMulta, contratoPassivelJurosMora, percentualJurosMora);
        
        await this.irParaAba("Take or Pay");
        
        await this.irParaSubAba("QDC");
        await t.setNativeDialogHandler(() => true);
        await this.setQDCContrato(dataInicioVigenciaQDC, valorQDC);
        
        await this.irParaSubAba("Take or Pay");
        await this.setTakeOrPay(periodicidadeToP, referenciaQuantidadeFaltanteParadaProgramada, consumoReferencialToP, compromissoRetiradaToP, dataInicioVigencia, dataFimVigencia, tabelaPrecosCobranca, formaCalculoCobrancaRecuperacao, recuperavel, percentualNaoRecuperavel, considerarParadasProgramadas, considerarFalhaFornecimento, considerarCasosFortuitos, formaApuracaoVolumeParadaProgramada, formaApuracaoVolumeFalhaFornecimento, formaApuracaoVolumeCasoFortuitoForcaMaior);
        
        await t.wait(1000);

        await this.irParaAba("Penalidades");
        await this.setPenalidades(penalidade, periodicidade, baseApuracao, dataInicioVigenciaPenalidades, dataFimVigenciaPenalidades, tabelaPrecoCobranca, formaCalculoCobranca, percentualCobranca, percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao, consumoReferencia, percentualRetirada);

        await t
            .click(this.botaoAvancar);

        
        //Segunda parte

        await this.setDadosTecnicos(pressaoFornecimento);

        await this.irParaAba("Faturamento");
        await this.selecionarFaturamento();
        await t.click(this.botaoExcluirFaturamento);
        await this.irParaAba("Faturamento");
        await this.setFaturamento(tarifa);

        await this.irParaAba("Modalidades Consumo Faturamento");
        await this.selecionarModalidade();
        await t.click(this.botaoExcluirModalidade);
        await this.irParaAba("Modalidades Consumo Faturamento");
        await this.setQDCModalidade(inicioVigenciaQDCModalidade, ValorQDCModalidade);
        await this.setTakeOrPayModalidade(prazoRevisaoQuantidadesContratadas, dataInicioVigenciaModalidade, dataFimVigenciaModalidade, tabelaPrecosCobrancaModalidade, formaCalculoCobrancaRecuperacaoModalidade, percentualNaoRecuperavelModalidade);
        await this.irParaAba("Modalidades Consumo Faturamento");
        await this.setModalidadeConsumoFaturamento(modalidade);

        await this.irParaAba("Responsabilidades");
        await t.click(this.botaoExcluirResponsabilidade);
        await this.irParaAba("Responsabilidades");
        await this.setResponsabilidades(pessoa, tipoResponsabilidade, dataInicioRelacionamento);
        
        await this.irParaAba("Consumo");
        await t
            .click(this.botaoRetirarTodosLocalAmostragem)
            .click(this.botaoRetirarTodosIntervaloAmostragem);
        await this.setConsumo(localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS);

        await t
            .click(this.botaoAplicar)
            .click(this.botaoSalvar);
    }

    async alterarContratoResidencial(situacaoContrato, dataAssinatura, dataVencimentoObrigacoesContratuais, definicaoAnoContratual, valorContrato, tipoPeriodicidadePenalidade,
        faturamentoAgrupado, tipoAgrupamento, tipoConvenio, arrecadadorConvenio, contratoPassivelMulta, percentualMulta, contratoPassivelJurosMora, percentualJurosMora, debitoAutomatico, banco, agencia, contaCorrente,
        dataInicioVigenciaQDC, valorQDC,
        ddd, numero, email,
        pressaoFornecimento, pressaoColetada, unidadePressaoColetada,
        localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS,
        tarifa,
        modalidade,
        pessoa, tipoResponsabilidade, dataInicioRelacionamento) {
        
        await t
            .click(this.botaoAlterar)
            .click(this.botaoLimpar);

        // Dados Gerais
        await this.setDadosGerais(pessoa, situacaoContrato, dataAssinatura, dataVencimentoObrigacoesContratuais, definicaoAnoContratual, tipoPeriodicidadePenalidade);
        await this.setValorContrato(valorContrato);

        //Dados Financeiros
        await this.irParaAba("Dados Financeiros");
        await this.setTipoConvenio(tipoConvenio);
        await this.setDadosFinanceiros(faturamentoAgrupado, tipoAgrupamento, arrecadadorConvenio, contratoPassivelMulta, percentualMulta, contratoPassivelJurosMora, percentualJurosMora);
        await this.setDadosBancario(debitoAutomatico, banco, agencia, contaCorrente);
        
        //Take or Pay
        await this.irParaAba("Take or Pay");
        await this.irParaSubAba("QDC");
        await this.selecionarQDCContrato();
        await t.click(this.botaoExcluirQDC);
        await this.setQDCContrato(dataInicioVigenciaQDC, valorQDC);

        await t.click(this.botaoAvancar);
        
        //SEGUNDA PARTE
        
        //Principal
        await page.limparCampo(this.ddd);
        await page.limparCampo(this.numero);
        await page.limparCampo(this.email);
        await this.setPrincipal(ddd, numero, email);

        //Dados Técnicos
        await this.irParaAba("Dados Técnicos");
        await page.limparCampo(this.pressaoColetada);
        await this.setPressaoFornecimento(pressaoFornecimento);
        await this.setPressaoColetada(pressaoColetada);
        await this.setUnidadePressaoColetada(unidadePressaoColetada);

        //Faturamento
        await this.irParaAba("Faturamento");
        await this.selecionarFaturamento();
        await t.click(this.botaoExcluirFaturamento);
        await this.irParaAba("Faturamento");
        await this.setFaturamento(tarifa);

        await this.irParaAba("Modalidades Consumo Faturamento");
        await this.selecionarModalidade();
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoExcluirModalidade);
        await this.irParaAba("Modalidades Consumo Faturamento");
        await this.setModalidadeConsumoFaturamento(modalidade);

        await this.irParaAba("Responsabilidades");
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoExcluirResponsabilidade);
        await this.irParaAba("Responsabilidades");
        await this.setResponsabilidades(pessoa, tipoResponsabilidade, dataInicioRelacionamento);
        
        await this.irParaAba("Consumo");
        await t
            .click(this.botaoRetirarTodosLocalAmostragem)
            .click(this.botaoRetirarTodosIntervaloAmostragem);
        await this.setConsumo(localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS);

        await t
            .click(this.botaoAplicar)
            .click(this.botaoSalvar);
    }

    async encerrar() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoEncerrar);
            
        await t.click(this.finalizarEncerrarRescindir)
    }

    async remover() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }

    async migrarContrato(tipoContrato) {
        await t
            .click(this.botaoMigrarContrato)
            .click(this.modeloContrato)
            .click(this.modeloContratoOption.withText(tipoContrato))
            .click(this.botaoSalvar)
            .click(this.botaoSalvar);
    }

    async migrarSaldo(nomePontoConsumo, nomeCliente, imovelPontoConsumo, valorQNR, valorQPNR) {
        await t.click(this.botaoMigrarSaldo);
        await this.selecionarPontoConsumo(nomePontoConsumo);
        await t.click(this.botaoPesquisar);
        await this.selecionarContrato(nomeCliente);
        await this.selecionarImovelPontoConsumo(imovelPontoConsumo);
        await page.limparCampo("#valorTransferidoQNR");
        await this.setValorQNR(valorQNR);
        await page.limparCampo("#valorTransferidoQPNR");
        await this.setValorQPNR(valorQPNR);
        await t.click(this.botaoTransferir);
    }

    async incluirContratoComplementar(descricao, volumeReferencia,
        debitoAutomatico, banco, agencia, contaCorrente,
        dataInicioVigenciaQDC, valorQDC,
        periodicidadeToP, referenciaQuantidadeFaltanteParadaProgramada, consumoReferencialToP, compromissoRetiradaToP, dataInicioVigencia, dataFimVigencia, tabelaPrecosCobranca, formaCalculoCobrancaRecuperacao, recuperavel, percentualNaoRecuperavel, considerarParadasProgramadas, considerarFalhaFornecimento, considerarCasosFortuitos, formaApuracaoVolumeParadaProgramada, formaApuracaoVolumeFalhaFornecimento, formaApuracaoVolumeCasoFortuitoForcaMaior,
        penalidade, periodicidade, baseApuracao, dataInicioVigenciaPenalidades, dataFimVigenciaPenalidades, tabelaPrecoCobranca, formaCalculoCobranca, percentualCobranca, percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao, consumoReferencia, percentualRetirada,
        pressaoFornecimento,
        localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS,
        tarifa,
        modalidade, percentualQNR) {

        await t.click(this.botaoIncluirComplementar);
        await this.setModeloContrato("CONTRATO COMPLEMENTAR");

        await this.setDescricao(descricao);
        await this.setVolumeReferencia(volumeReferencia);

        await this.irParaAba("Dados Financeiros");
        await this.setDadosBancario(debitoAutomatico, banco, agencia, contaCorrente);
        
        await this.irParaAba("Take or Pay");
        
        await this.irParaSubAba("QDC");
        await this.setQDCContrato(dataInicioVigenciaQDC, valorQDC);
        
        await t.wait(1000);

        await this.irParaSubAba("Take or Pay");
        await this.setTakeOrPay(periodicidadeToP, referenciaQuantidadeFaltanteParadaProgramada, consumoReferencialToP, compromissoRetiradaToP, dataInicioVigencia, dataFimVigencia, tabelaPrecosCobranca, formaCalculoCobrancaRecuperacao, recuperavel, percentualNaoRecuperavel, considerarParadasProgramadas, considerarFalhaFornecimento, considerarCasosFortuitos, formaApuracaoVolumeParadaProgramada, formaApuracaoVolumeFalhaFornecimento, formaApuracaoVolumeCasoFortuitoForcaMaior);
        
        await t.wait(1000);

        await this.irParaAba("Penalidades");
        await this.setPenalidades(penalidade, periodicidade, baseApuracao, dataInicioVigenciaPenalidades, dataFimVigenciaPenalidades, tabelaPrecoCobranca, formaCalculoCobranca, percentualCobranca, percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao, consumoReferencia, percentualRetirada);

        await t.click(this.botaoAvancar);
        
        //Segunda parte

        await this.setDadosTecnicos(pressaoFornecimento);

        await this.irParaAba("Faturamento");
        await this.setFaturamento(tarifa);

        await this.irParaAba("Modalidades Consumo Faturamento");
        await this.setPercentualQNR(percentualQNR);
        await this.setModalidadeConsumoFaturamento(modalidade);
        
        await this.irParaAba("Consumo");
        await this.setConsumo(localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS);

        await t
            .click(this.botaoAplicar)
            .click(this.botaoSalvar);
    }

    async alterarContratoComplementar(descricao, volumeReferencia,
        debitoAutomatico, banco, agencia, contaCorrente,
        periodicidadeToP, referenciaQuantidadeFaltanteParadaProgramada, consumoReferencialToP, compromissoRetiradaToP, dataInicioVigencia, dataFimVigencia, tabelaPrecosCobranca, formaCalculoCobrancaRecuperacao, recuperavel, percentualNaoRecuperavel, considerarParadasProgramadas, considerarFalhaFornecimento, considerarCasosFortuitos, formaApuracaoVolumeParadaProgramada, formaApuracaoVolumeFalhaFornecimento, formaApuracaoVolumeCasoFortuitoForcaMaior,
        penalidade, periodicidade, baseApuracao, dataInicioVigenciaPenalidades, dataFimVigenciaPenalidades, tabelaPrecoCobranca, formaCalculoCobranca, percentualCobranca, percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao, consumoReferencia, percentualRetirada,
        pressaoFornecimento,
        localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS,
        tarifa,
        modalidade, percentualQNR) {
            await t.click(this.botaoAlterar);

            await page.limparCampo('#descricaoContrato');
            await this.setDescricao(descricao);
            await page.limparCampo('#volumeReferencia');
            await this.setVolumeReferencia(volumeReferencia);

            await this.irParaAba("Dados Financeiros");
            await this.setDadosBancario(debitoAutomatico, banco, agencia, contaCorrente);
            
            await this.irParaAba("Take or Pay");
            await this.selecionarTakeOrPay();
            await t
                .setNativeDialogHandler(() => true)
                .click(this.botaoExcluirTakeOrPay);

            await t.wait(2000);
            
            await this.setTakeOrPay(periodicidadeToP, referenciaQuantidadeFaltanteParadaProgramada, consumoReferencialToP, compromissoRetiradaToP, dataInicioVigencia, dataFimVigencia, tabelaPrecosCobranca, formaCalculoCobrancaRecuperacao, recuperavel, percentualNaoRecuperavel, considerarParadasProgramadas, considerarFalhaFornecimento, considerarCasosFortuitos, formaApuracaoVolumeParadaProgramada, formaApuracaoVolumeFalhaFornecimento, formaApuracaoVolumeCasoFortuitoForcaMaior);

            await this.irParaAba("Penalidades");
            await this.selecionarPenalidade();
            await t
                .setNativeDialogHandler(() => true)
                .click(this.botaoExcluirPenalidade);

            await t.wait(1000);
            
            await this.irParaAba("Penalidades");
            await this.setPenalidades(penalidade, periodicidade, baseApuracao, dataInicioVigenciaPenalidades, dataFimVigenciaPenalidades, tabelaPrecoCobranca, formaCalculoCobranca, percentualCobranca, percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao, consumoReferencia, percentualRetirada);

            await t.click(this.botaoAvancar);
            
            //Segunda parte

            await this.setDadosTecnicos(pressaoFornecimento);

            await this.irParaAba("Faturamento");
            await this.selecionarFaturamento();
            await t
                .setNativeDialogHandler(() => true)
                .click(this.botaoExcluirFaturamento);
            await this.irParaAba("Faturamento");
            await this.setFaturamento(tarifa);

            await this.irParaAba("Modalidades Consumo Faturamento");
            await page.limparCampo('#percentualQNR');
            await this.setPercentualQNR(percentualQNR);
            await this.selecionarModalidade();
            await t
                .setNativeDialogHandler(() => true)
                .click(this.botaoExcluirModalidade);
            await this.irParaAba('Modalidades Consumo Faturamento');
            await this.setModalidadeConsumoFaturamento(modalidade);
            
            await this.irParaAba("Consumo");
            await t
                .click(this.botaoRetirarTodosIntervaloAmostragem)
                .click(this.botaoRetirarTodosLocalAmostragem);
            await this.setConsumo(localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS);

            await t
                .click(this.botaoAplicar)
                .click(this.botaoSalvar);
    }

    async ordenarContrato() {
        await t
            .click(this.botaoOrdenarFaturamento)
            .click(Selector('img[alt="Alterar Posição Serviço"]'))
            .click(this.botaoSalvar);
    }

    //Setters Abas

    async setDadosGerais(pessoa, situacaoContrato, dataAssinatura, dataVencimentoObrigacoesContratuais, definicaoAnoContratual, tipoPeriodicidadePenalidade) {
        await ClientFunction((pessoa)=>{ selecionarCliente(pessoa)})(pessoa);

        await this.setSituacaoContrato(situacaoContrato);
        await this.setDataAssinatura(dataAssinatura);
        await this.setDataVencimentoObrigacoesContratuais(dataVencimentoObrigacoesContratuais);
        await this.setDefinicaoAnoContratual(definicaoAnoContratual);
        await this.setTipoPeriodicidadePenalidade(tipoPeriodicidadePenalidade);        
    }

    async setProposta(gastoMensalcomGN, economiaMensalcomGN, economiaAnualcomGN, descontoEfetivoEconomia, diaVencimentoFinanciamento, 
        valorParticipacaoCliente, quantidadeParcelasFinanciamento, percentualJurosFinanciamento, sistemaAmortizacao, valorInvestimento, 
        dataInvestimento) {
        
        await page.preencherCampo("#numeroProposta", "201700002");
        await this.setGastoMensalcomGN(gastoMensalcomGN);
        await this.setEconomiaMensalcomGN(economiaMensalcomGN);
        await this.setEconomiaAnualcomGN(economiaAnualcomGN);
        await this.setDescontoEfetivoEconomia(descontoEfetivoEconomia);
        await this.setDiaVencimentoFinanciamento(diaVencimentoFinanciamento);
        await this.setValorParticipacaoCliente(valorParticipacaoCliente);
        await this.setQuantidadeParcelasFinanciamento(quantidadeParcelasFinanciamento);
        await this.setPercentualJurosFinanciamento(percentualJurosFinanciamento);
        await this.setSistemaAmortizacao(sistemaAmortizacao);
        await this.setValorInvestimento(valorInvestimento);
        await this.setDataInvestimento(dataInvestimento);     
    }

    async setDadosFinanceiros(faturamentoAgrupado, tipoAgrupamento, arrecadadorConvenio, contratoPassivelMulta, percentualMulta, 
        contratoPassivelJurosMora, percentualJurosMora) {
        
        await this.setFaturamentoAgrupado(faturamentoAgrupado, tipoAgrupamento);
        await this.setArrecadadorConvenio(arrecadadorConvenio);
        await this.setContratoPassivelMulta(contratoPassivelMulta, percentualMulta);
        await this.setContratoPassivelJurosMora(contratoPassivelJurosMora, percentualJurosMora);
    }

    async setTakeOrPay(periodicidadeToP, referenciaQuantidadeFaltanteParadaProgramada, consumoReferencialToP, 
        compromissoRetiradaToP, dataInicioVigencia, dataFimVigencia, tabelaPrecosCobranca, formaCalculoCobrancaRecuperacao, recuperavel, 
        percentualNaoRecuperavel, considerarParadasProgramadas, considerarFalhaFornecimento, considerarCasosFortuitos, 
        formaApuracaoVolumeParadaProgramada, formaApuracaoVolumeFalhaFornecimento, formaApuracaoVolumeCasoFortuitoForcaMaior) {
        
        await this.setPeriodicidadeToP(periodicidadeToP);
        await this.setReferenciaQuantidadeFaltanteParadaProgramada(referenciaQuantidadeFaltanteParadaProgramada);
        await this.setConsumoReferencialToP(consumoReferencialToP);
        await this.setCompromissoRetiradaToP(compromissoRetiradaToP);
        await this.setDataInicioVigencia(dataInicioVigencia);
        await this.setDataFimVigencia(dataFimVigencia);
        await this.setTabelaPrecosCobranca(tabelaPrecosCobranca);
        await this.setRecuperavel(recuperavel);
        await this.setPercentualNaoRecuperavel(percentualNaoRecuperavel);
        await this.setFormaCalculoCobrancaRecuperacao(formaCalculoCobrancaRecuperacao);
        await this.setConsiderarParadasProgramadas(considerarParadasProgramadas, formaApuracaoVolumeParadaProgramada);
        await this.setConsiderarFalhaFornecimento(considerarFalhaFornecimento, formaApuracaoVolumeFalhaFornecimento);
        await this.setConsiderarCasosFortuitos(considerarCasosFortuitos, formaApuracaoVolumeCasoFortuitoForcaMaior);
        
        await t.click(this.botaoAdicionarTakeOrPay);
    }

    async setPenalidades(penalidade, periodicidade, baseApuracao, dataInicioVigenciaPenalidades, dataFimVigenciaPenalidades, tabelaPrecoCobranca, 
        formaCalculoCobranca, percentualCobranca, percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao, consumoReferencia, percentualRetirada) {
        
        await this.setPenalidade(penalidade);
        await this.setPeriodicidade(periodicidade);
        await this.setBaseApuracao(baseApuracao);
        await this.setDataInicioVigenciaPenalidades(dataInicioVigenciaPenalidades);
        await this.setDataFimVigenciaPenalidades(dataFimVigenciaPenalidades);
        await this.setTabelaPrecoCobranca(tabelaPrecoCobranca);
        await this.setFormaCalculoCobranca(formaCalculoCobranca);
        await this.setPercentualCobranca(percentualCobranca);
        await this.setPercentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao(percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao);
        await this.setConsumoReferencia(consumoReferencia);
        await this.setPercentualRetirada(percentualRetirada);
        
        await t.click(this.botaoAdicionarPenalidade);
    }

    async setPontoConsumo(imovel, pontoConsumo) {
        await ClientFunction((imovel) => { selecionarImovel(imovel) })(imovel);
        await this.irParaAba("Pontos de Consumo");
        await t.expect(this.tabelaPontoConsumo.exists).ok({tiemOut: 6000});
        if(pontoConsumo){
            await t.click(this.tabelaPontoConsumo.withText(pontoConsumo).find("input"));
        } else {
            await t.click(this.tabelaPontoConsumo.find("input"));
        }
    }

    async setPrincipal(ddd, numero, email) {
        await this.setNumero(ddd, numero);
        await this.setEmail(email);
    }

    async setDadosTecnicos(pressaoFornecimento) {
        await this.setPressaoFornecimento(pressaoFornecimento);
    }

    async setConsumo(localAmostragens, intervaloAmostragens, tamanhoReducaoRecuperacaoPCS) {
        
        
        await this.setLocalAmostragem(localAmostragens);
        await this.setIntervaloAmostragens(intervaloAmostragens);
        await this.setTamanhoReducaoRecuperacaoPCS(tamanhoReducaoRecuperacaoPCS);
    }

    async setFaturamento(tarifa) {
        
        await this.setTarifa(tarifa);
        await t.click(this.botaoAdicionarFaturamento);
    }

    async setModalidadeConsumoFaturamento(modalidade) {

        await this.setModalidade(modalidade);
        await t.click(this.botaoAdicionarModalidade);
    }

    async setResponsabilidades(pessoa, tipoResponsabilidade, dataInicioRelacionamento) {
       
        //await page.selecionarClientePorNome(pessoa);
        await this.setTipoResponsabilidade(tipoResponsabilidade);
        await this.setDataInicioRelacionamento(dataInicioRelacionamento);
        await t.click(this.botaoAdicionarResponsabilidade);
    }

    async selecionarTakeOrPay() {
        await t.click(Selector("#compromissosTakeOrPayVO tbody tr td").find("input"));
    }

    async selecionarPenalidade() {
        await t.click(Selector("#penalidadesRetMaiorMenorVO tbody tr td").find("input"));
    }

    async selecionarFaturamento() {
        await t.click(Selector("#itemVencimento tbody td").find("input"));
    }

    async selecionarFaturamento() {
        await t.click(Selector("#itemVencimento tbody td").find("input"));
    }

    async selecionarModalidade() {
        await t.click(Selector("#modalidadeConsumoFaturamentoVO tbody td").find("input"));
    }

    // SETTERS

    async irParaAba(aba) {
        await t
            .setNativeDialogHandler(()=> true)
            .click(this.aba.withText(aba));
    }

    async irParaSubAba(subAba) {
        await t
            .click(this.subAba.withText(subAba));
    }
    
    async setModeloContrato(modeloContrato) {
        await this.modeloContratoInclusao;
        await t
            .click(this.modeloContratoInclusao)
            .click(this.modeloContratoInclusaoOption.withExactText(modeloContrato))
            .click(this.botaoExibir);  
    }
    
    //Setters Campos Dados Gerais

    async setSituacaoContrato(situacaoContrato) {
        await t
            .click(this.situacaoContrato)
            .click(this.situacaoContratoOption.withExactText(situacaoContrato));
    }

    async setDataAssinatura(dataAssinatura) {
        await t
            .typeText(this.dataAssinatura, dataAssinatura);
    }

    async setDataVencimentoObrigacoesContratuais(dataVencimentoObrigacoesContratuais) {
        await t
            .typeText(this.dataVencimentoObrigacoesContratuais, dataVencimentoObrigacoesContratuais);
    }

    async setDefinicaoAnoContratual(definicaoAnoContratual) {
        if(definicaoAnoContratual) {
            await t.click(Selector("#anoContratualMaiusculo"));
        } else {
            await t.click(Selector("#anoContratualMinusculo"));
        }
    }

    async setTipoPeriodicidadePenalidade(tipoPeriodicidadePenalidade) {
        if(tipoPeriodicidadePenalidade) {
            await t.click(Selector("#periodicidadeContinua"));
        } else {
            await t.click(Selector("input[name=tipoPeriodicidadePenalidade][value=false]"));
        }
    }

    async setValorContrato(valorContrato) {
        await t
            .typeText(this.valorContrato, valorContrato);
    }
    
    async setDataAditivo(dataAditivo) {
        await t
            .typeText(this.dataAditivo, dataAditivo);
    }

    async setDescricaoAditivo(descricaoAditivo) {
        await t
            .typeText(this.descricaoAditivo, descricaoAditivo);
    }

    async setDescricao(descricao) {
        await t
            .typeText(this.descricao, descricao);
    }

    async setVolumeReferencia(volumeReferencia) {
        await t
            .typeText(this.volumeReferencia, volumeReferencia);
    }

    //Setters Campos Proposta

    async setGastoMensalcomGN(gastoMensalcomGN) {
        await t
            .typeText(this.gastoMensalcomGN, gastoMensalcomGN);
    }

    async setEconomiaMensalcomGN(economiaMensalcomGN) {
        await t
            .typeText(this.economiaMensalcomGN, economiaMensalcomGN);
    }

    async setEconomiaAnualcomGN(economiaAnualcomGN) {
        await t
            .typeText(this.economiaAnualcomGN, economiaAnualcomGN);
    }

    async setDescontoEfetivoEconomia(descontoEfetivoEconomia) {
        await t
            .typeText(this.descontoEfetivoEconomia, descontoEfetivoEconomia);
    }

    async setDiaVencimentoFinanciamento(diaVencimentoFinanciamento) {
        await t
            .click(this.diaVencimentoFinanciamento)
            .click(this.diaVencimentoFinanciamentoOption.withExactText(diaVencimentoFinanciamento));
    }

    async setValorParticipacaoCliente(valorParticipacaoCliente) {
        await t
            .typeText(this.valorParticipacaoCliente, valorParticipacaoCliente);
    }

    async setQuantidadeParcelasFinanciamento(quantidadeParcelasFinanciamento) {
        await t
            .typeText(this.quantidadeParcelasFinanciamento, quantidadeParcelasFinanciamento);
    }

    async setPercentualJurosFinanciamento(percentualJurosFinanciamento) {
        await t
            .typeText(this.percentualJurosFinanciamento, percentualJurosFinanciamento);
    }

    async setSistemaAmortizacao(sistemaAmortizacao) {
        await t
            .click(this.sistemaAmortizacao)
            .click(this.sistemaAmortizacaoOption.withExactText(sistemaAmortizacao));
    }

    async setValorInvestimento(valorInvestimento) {
        await t
            .typeText(this.valorInvestimento, valorInvestimento);
    }

    async setDataInvestimento(dataInvestimento) {
        await t
            .typeText(this.dataInvestimento, dataInvestimento); 
    }

    //Setters Campos Dados Financeiros

    async setFaturamentoAgrupado(faturamentoAgrupado, tipoAgrupamento) {
        if(faturamentoAgrupado) {
            await t
                .click(Selector("#incluirContrato-faturamentoAgrupamento[value=true]"))
                .click(this.tipoAgrupamento)
                .click(this.tipoAgrupamentoOption.withExactText(tipoAgrupamento));
        } else {
            await t.click(Selector("#incluirContrato-faturamentoAgrupamento[value=false]"));
        }
    }

    async setTipoConvenio(tipoConvenio) {
        await t
            .click(this.tipoConvenio)
            .click(this.tipoConvenioOption.withExactText(tipoConvenio));
    }

    async setArrecadadorConvenio(arrecadadorConvenio) {
        await t
            .click(this.arrecadadorConvenio)
            .click(this.arrecadadorConvenioOption.withExactText(arrecadadorConvenio));
    }
    
    async setContratoPassivelMulta(contratoPassivelMulta, percentualMulta) {
        if(contratoPassivelMulta) {
            await t
                .click(Selector("#indicadorMultaSim"))
                .selectText(this.percentualMulta)
                .pressKey('delete')
                .typeText(this.percentualMulta, percentualMulta);
        } else {
            await t.click(Selector("#indicadorMultaNao"));
        }
    }
    
    async setContratoPassivelJurosMora(contratoPassivelJurosMora, percentualJurosMora) {
        if(contratoPassivelJurosMora) {
            await t
                .click(Selector("#incluirContrato-indicadorJurosMora[value=true]"))
                .selectText(this.percentualJurosMora)
                .pressKey('delete')
                .typeText(this.percentualJurosMora, percentualJurosMora);
        } else {
            await t.click(Selector("#incluirContrato-indicadorJurosMora[value=false]"));
        }
    }

    async setDadosBancario(debitoAutomatico, banco, agencia, contaCorrente) {
        if(debitoAutomatico) {
            await this.setDebitoAutomaticoSim();
            await this.setBanco(banco);
            await this.setAgencia(agencia);
            await this.setContaCorrente(contaCorrente);
        } else {
            await this.setDebitoAutomaticoNao();
        }
    }

    async setDebitoAutomaticoSim() {
        await t
            .click(this.debitoAutomaticoSim);
    }

    async setDebitoAutomaticoNao() {
        await t
            .click(this.debitoAutomaticoNao);
    }

    async setBanco(banco) {
        await t
            .click(this.banco)
            .click(this.bancoOption.withText(banco));
    }

    async setAgencia(agencia) {
        await t
            .typeText(this.agencia, agencia);
    }

    async setContaCorrente(contaCorrente) {
        await t
            .typeText(this.contaCorrente, contaCorrente);
    }
    
    //Setters Campos Take Or Pay

    async setQDCContrato(dataInicioVigenciaQDC, valorQDC) {
        await this.setDataInicioVigenciaQDC(dataInicioVigenciaQDC);
        await this.setValorQDC(valorQDC);
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoOkQDC);
    }

    async setDataInicioVigenciaQDC(dataInicioVigenciaQDC) {
        await t
            .typeText(this.dataInicioVigenciaQDC, dataInicioVigenciaQDC);
    }
    
    async setValorQDC(valorQDC) {
        await t
            .typeText(this.QDCValor, valorQDC);
    }

    async selecionarQDCContrato() {
        await t
            .click(this.QDC);
    }
    
    async setPeriodicidadeToP(periodicidadeToP) {
        await t.expect(this.periodicidadeToP.exists).ok();
        await t
            .click(this.periodicidadeToP)
            .click(this.periodicidadeToPOption.withExactText(periodicidadeToP));
    }
    
    async setReferenciaQuantidadeFaltanteParadaProgramada(referenciaQuantidadeFaltanteParadaProgramada) {
        await t
            .click(this.referenciaQuantidadeFaltanteParadaProgramada)
            .click(this.referenciaQuantidadeFaltanteParadaProgramadaOption.withExactText(referenciaQuantidadeFaltanteParadaProgramada));
    }
    
    async setConsumoReferencialToP(consumoReferencialToP) {
        await t
            .click(this.consumoReferencialToP)
            .click(this.consumoReferencialToPOption.withExactText(consumoReferencialToP));
    }
    
    async setCompromissoRetiradaToP(compromissoRetiradaToP) {
        await t
            .typeText(this.compromissoRetiradaToP, compromissoRetiradaToP);
    }
    
    async setDataInicioVigencia(dataInicioVigencia) {
        await t
            .typeText(this.dataInicioVigencia, dataInicioVigencia);
    }
    
    async setDataFimVigencia(dataFimVigencia) {
        await t
            .typeText(this.dataFimVigencia, dataFimVigencia);
    }
    
    async setTabelaPrecosCobranca(tabelaPrecosCobranca) {
        await t
            .click(this.tabelaPrecosCobranca)
            .click(this.tabelaPrecosCobrancaOption.withExactText(tabelaPrecosCobranca));
    }
    
    async setRecuperavel(recuperavel) {
        if(recuperavel) {
            await t.click(Selector("#TOPrecuperacaoAutoTakeOrPaySim"));
        } else {
            await t.click(Selector("#TOPrecuperacaoAutoTakeOrPayNao"));
        }
    }
    
    async setPercentualNaoRecuperavel(percentualNaoRecuperavel) {
        await t
            .typeText(this.percentualNaoRecuperavel, percentualNaoRecuperavel);
    }
    
    async setFormaCalculoCobrancaRecuperacao(formaCalculoCobrancaRecuperacao) {
        await t
            .click(this.formaCalculoCobrancaRecuperacao)
            .click(this.formaCalculoCobrancaRecuperacaoOption.withExactText(formaCalculoCobrancaRecuperacao));
    }
    
    async setConsiderarParadasProgramadas(considerarParadasProgramadas, formaApuracaoVolumeParadaProgramada) {
        if(considerarParadasProgramadas) {
            await t
                .click(Selector("#consideraParadaProgramadaCSim"))
                .click(this.formaApuracaoVolumeParadaProgramada)
                .click(this.formaApuracaoVolumeParadaProgramadaOption.withExactText(formaApuracaoVolumeParadaProgramada));
        } else {
            await t.click(Selector("#consideraParadaProgramadaCNao"));
        }
    }
    
    async setConsiderarFalhaFornecimento(considerarFalhaFornecimento, formaApuracaoVolumeFalhaFornecimento) {
        if(considerarFalhaFornecimento) {
            await t
                .click(Selector("#consideraFalhaFornecimentoCSim"))
                .click(this.formaApuracaoVolumeFalhaFornecimento)
                .click(this.formaApuracaoVolumeFalhaFornecimentoOption.withExactText(formaApuracaoVolumeFalhaFornecimento));
        } else {
            await t.click(Selector("#consideraFalhaFornecimentoCNao"));
        }
    }
    
    async setConsiderarCasosFortuitos(considerarCasosFortuitos, formaApuracaoVolumeCasoFortuitoForcaMaior) {
        if(considerarCasosFortuitos) {
            await t
                .click(Selector("#consideraCasoFortuitoCSim"))
                .click(this.formaApuracaoVolumeCasoFortuitoForcaMaior)
                .click(this.formaApuracaoVolumeCasoFortuitoForcaMaiorOption.withExactText(formaApuracaoVolumeCasoFortuitoForcaMaior));
        } else {
            await t.click(Selector("#consideraCasoFortuitoCNao"));
        }  
    }
    
    //Setters Campos Penalidades

    async setPenalidade(penalidade) {
        await t.expect(this.penalidade.exists).ok();
        await t
            .click(this.penalidade)
            .click(this.penalidadeOption.withExactText(penalidade));
    }
    
    async setPeriodicidade(periodicidade) {
        await t
            .click(this.periodicidade)
            .click(this.periodicidadeOption.withExactText(periodicidade));
    }
    
    async setBaseApuracao(baseApuracao) {
        await t
            .click( this.baseApuracao)
            .click(this.baseApuracaoOption.withExactText(baseApuracao));
    }
    
    async setDataInicioVigenciaPenalidades(dataInicioVigenciaPenalidades) {
        await t
            .typeText(this.dataInicioVigenciaPenalidades, dataInicioVigenciaPenalidades);
    }
    
    async setDataFimVigenciaPenalidades(dataFimVigenciaPenalidades) {
        await t
            .typeText(this.dataFimVigenciaPenalidades, dataFimVigenciaPenalidades); 
    }
    
    async setTabelaPrecoCobranca(tabelaPrecoCobranca) {
        await t
            .click(this.tabelaPrecoCobranca)
            .click(this.tabelaPrecoCobrancaOption.withExactText(tabelaPrecoCobranca));
    }
    
    async setFormaCalculoCobranca(formaCalculoCobranca) {
        await t
            .click(this.formaCalculoCobranca)
            .click(this.formaCalculoCobrancaOption.withExactText(formaCalculoCobranca));
    }
    
    async setPercentualCobranca(percentualCobranca) {
        await t
            .typeText(this.percentualCobranca, percentualCobranca);
    }
    
    async setPercentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao(percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao) {
        await t
        .typeText(this.percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao, percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao);
    }
    
    async setConsumoReferencia(consumoReferencia) {
        await t
            .click(this.consumoReferencia)
            .click(this.consumoReferenciaOption.withExactText(consumoReferencia));
    }
    
    async setPercentualRetirada(percentualRetirada) {
        await t
            .typeText(this.percentualRetirada, percentualRetirada);
    }

    // Setters Principal

    async setNumero(ddd, numero) {
        await t
            .typeText(this.ddd, ddd)
            .typeText(this.numero, numero);
    }

    async setEmail(email) {
        await t
            .typeText(this.email, email);
    }
    
    // Setters Campos Dados Técnicos

    async setPressaoFornecimento(pressaoFornecimento) {
        await t.expect(this.pressaoFornecimento.exists).ok();
        await t
            .click(this.pressaoFornecimento)
            .click(this.pressaoFornecimentoOption.withExactText(pressaoFornecimento));
    }

    async setPressaoColetada(pressaoColetada) {
        await t
            .typeText(this.pressaoColetada, pressaoColetada);
    }

    async setUnidadePressaoColetada(unidadePressaoColetada) {
        await t
            .click(this.unidadePressaoColetada)
            .click(this.unidadePressaoColetadaOption.withExactText(unidadePressaoColetada));
    }
    
    // Setters Campos Consumo

    async setLocalAmostragem(localAmostragens) {
        for(const localAmostragem of localAmostragens) {
            await t
                .click(this.localAmostragem.withExactText(localAmostragem))
                .click(this.botaoAdicionarLocalAmostragem);
        }   
    }
    
    async setIntervaloAmostragens(intervaloAmostragens) {
        for(const intervaloAmostragem of intervaloAmostragens) {
            await t
                .click(this.intervaloAmostragem.withExactText(intervaloAmostragem))
                .click(this.botaoAdicionarIntervaloAmostragem);
        }
    }
    
    async setTamanhoReducaoRecuperacaoPCS(tamanhoReducaoRecuperacaoPCS) {
        await t
            .typeText(this.tamanhoReducaoRecuperacaoPCS, tamanhoReducaoRecuperacaoPCS);    
    }
    
    //Setters Campos Tarifa

    async setTarifa(tarifa) {
        await t
            .click(this.tarifa)
            .click(this.tarifaOption.withExactText(tarifa));
    }

    //Setters Campos Modalidade Consumo Faturamento
    
    async setQDCModalidade(inicioVigenciaQDCModalidade, ValorQDCModalidade) {
        await this.setInicioVigenciaQDCModalidade(inicioVigenciaQDCModalidade);
        await this.setValorQDCModalidade(ValorQDCModalidade);
        await t.click(this.botaoAdicionarQDCModalidade);
    }

    async setTakeOrPayModalidade(prazoRevisaoQuantidadesContratadas, dataInicioVigenciaModalidade, dataFimVigenciaModalidade, tabelaPrecosCobrancaModalidade, formaCalculoCobrancaRecuperacaoModalidade, percentualNaoRecuperavelModalidade) {
        await this.setPrazoRevisaoQuantidadesContratadas(prazoRevisaoQuantidadesContratadas);
        await this.setDataInicioVigenciaModalidade(dataInicioVigenciaModalidade);
        await this.setDataFimVigenciaModalidade(dataFimVigenciaModalidade);
        await this.setTabelaPrecosCobrancaModalidade(tabelaPrecosCobrancaModalidade);
        await this.setFormaCalculoCobrancaRecuperacaoModalidade(formaCalculoCobrancaRecuperacaoModalidade);
        await this.setPercentualNaoRecuperavelModalidade(percentualNaoRecuperavelModalidade);
        await t.click(this.botaoAdicionarTakeOrPayModalidade);
    }

    async setModalidade(modalidade) {
        await t
            .click(this.modalidade)
            .click(this.modalidadeOption.withExactText(modalidade));
    }

    async setPercentualQNR(percentualQNR) {
        await t
            .typeText(this.percentualQNR, percentualQNR);
    }

    async setInicioVigenciaQDCModalidade(inicioVigenciaQDCModalidade) {
        await t
            .typeText(this.inicioVigenciaQDCModalidade,inicioVigenciaQDCModalidade);
    }

    async setValorQDCModalidade(QDCModalidade) {
        await t
            .typeText(this.QDCModalidade,QDCModalidade);
    }

    async setPrazoRevisaoQuantidadesContratadas(prazoRevisaoQuantidadesContratadas) {
        await t
            .typeText(this.prazoRevisaoQuantidadesContratadas,prazoRevisaoQuantidadesContratadas);
    }

    async setDataInicioVigenciaModalidade(dataInicioVigenciaModalidade) {
        await t
            .typeText(this.dataInicioVigenciaModalidade,dataInicioVigenciaModalidade);
    }

    async setDataFimVigenciaModalidade(dataFimVigenciaModalidade) {
        await t
            .typeText(this.dataFimVigenciaModalidade,dataFimVigenciaModalidade);
    }
    
    async setTabelaPrecosCobrancaModalidade(tabelaPrecosCobrancaModalidade) {
        await t
            .click(this.tabelaPrecosCobrancaModalidade)
            .click(this.tabelaPrecosCobrancaModalidadeOptions.withText(tabelaPrecosCobrancaModalidade));
    }
    async setFormaCalculoCobrancaRecuperacaoModalidade(formaCalculoCobrancaRecuperacaoModalidade) {
        await t
            .click(this.formaCalculoCobrancaRecuperacaoModalidade)
            .click(this.formaCalculoCobrancaRecuperacaoModalidadeOptions.withText(formaCalculoCobrancaRecuperacaoModalidade));
    }

    async setPercentualNaoRecuperavelModalidade(percentualNaoRecuperavelModalidade) {
        await t
            .typeText(this.percentualNaoRecuperavelModalidade,percentualNaoRecuperavelModalidade);
    }
    
    //Setters Campos Responsabilidades

    async setTipoResponsabilidade(tipoResponsabilidade) {
        await t
            .click(this.tipoResponsabilidade)
            .click(this.tipoResponsabilidadeOption.withExactText(tipoResponsabilidade));
    }
    
    async setDataInicioRelacionamento(dataInicioRelacionamento) {
        await t
            .typeText(this.dataInicioRelacionamento, dataInicioRelacionamento);
    }

    //Setters da Tela Migrar Saldo

    async selecionarPontoConsumo(nomePontoConsumo) {
        await t
            .click(Selector("#pontoConsumoVO tbody td").withText(nomePontoConsumo).parent("tr").find("#idsPontosConsumo"));
    }

    async selecionarContrato(opcao) {
        await t
            .click(Selector("#contrato tbody td").withText(opcao).parent("tr").find("input"));
    }

    async selecionarImovelPontoConsumo(imovelPontoConsumo) {
        await t
            .click(Selector("#pontoConsumo tbody td").withText(imovelPontoConsumo).parent("tr").find("input"));
    }

    async setValorQNR(valorQNR) {
        await t
            .typeText(this.valorQNR, valorQNR);
    }

    async setValorQPNR(valorQPNR) {
        await t
            .typeText(this.valorQPNR, valorQPNR);
    }

    // Getters Campos Dados Gerais

    getSituacaoContrato() {
        return t.eval(() => $('#situacaoContrato option:selected').text().trim());
    }
    getDataAssinatura() {
        return this.dataAssinatura.value;
    }
    getDataVencimentoObrigacoesContratuais() {
        return this.dataVencimentoObrigacoesContratuais.value;
    }
    getValorContrato() {
        return this.valorContrato.value;
    }
    getDataAditivo() {
        return this.dataAditivo.value;
    }
    getDescricaoAditivo() {
        return this.descricaoAditivo.value;
    }
    getModelo() {
        return t.eval(() => $('#detalhamentoDescricao').text().trim());
    }
    getDescricao() {
        return this.descricao.value;
    }
    getVolumeReferencia() {
        return this.volumeReferencia.value;
    }

    // Getters Campos Proposta

    getGastoMensalcomGN() {
        return this.gastoMensalcomGN.value;
    }

    getEconomiaMensalcomGN() {
        return this.economiaMensalcomGN.value;
    }

    getEconomiaAnualcomGN() {
        return this.economiaAnualcomGN.value;
    }

    getDescontoEfetivoEconomia() {
        return this.descontoEfetivoEconomia.value;
    }

    getDiaVencimentoFinanciamento() {
        return t.eval(() => $('#diaVencFinanciamento option:selected').text().trim());
    }

    getValorParticipacaoCliente() {
        return this.valorParticipacaoCliente.value;
    }

    getQuantidadeParcelasFinanciamento() {
        return this.quantidadeParcelasFinanciamento.value;
    }

    getPercentualJurosFinanciamento() {
        return this.percentualJurosFinanciamento.value;
    }

    getSistemaAmortizacao() {
        return t.eval(() => $('#sistemaAmortizacao option:selected').text().trim());
    }

    getValorInvestimento() {
        return this.valorInvestimento.value;
    }

    getDataInvestimento() {
        return this.dataInvestimento.value;
    }

    // Getters Campos Dados financeiros

    getTipoAgrupamento() {
        return t.eval(() => $('#tipoAgrupamento option:selected').text().trim());
    }

    getTipoConvenio() {
        return t.eval(() => $('#formaCobranca option:selected').text().trim());
    }

    getArrecadadorConvenio() {
        return t.eval(() => $('#arrecadadorConvenio option:selected').text().trim());
    }

    getPercentualMulta() {
        return this.percentualMulta.value;
    }

    getPercentualJurosMora() {
        return this.percentualJurosMora.value;
    }
    
    getDebitoAutomatico() {
        return this.debitoAutomatico.value;
    }

    getBanco() {
        return t.eval(() => $('#banco option:selected').text().trim());
    }

    getAgencia() {
        return this.agencia.value;
    }

    getContaCorrente() {
        return this.contaCorrente.value;
    }

    // Getters Campos Take Or Pay

    getPeriodicidadeToP() {
        return t.eval(() => $('#periodicidadeTakeOrPayC option:selected').text().trim());
    }

    getTabelaPrecosCobranca() {
        return t.eval(() => $('#precoCobrancaC option:selected').text().trim());
    }

    getReferenciaQuantidadeFaltanteParadaProgramada() {
        return t.eval(() => $('#tipoReferenciaQFParadaProgramada option:selected').text().trim());
    }

    getConsumoReferencialToP() {
        return t.eval(() => $('#mensalConsumoReferenciaInferior option:selected').text().trim());
    }

    getCompromissoRetiradaToP() {
        return this.compromissoRetiradaToP.value;
    }

    getDataInicioVigencia() {
        return this.dataInicioVigencia.value;
    }

    getDataFimVigencia() {
        return this.dataFimVigencia.value;
    }

    getFormaCalculoCobrancaRecuperacao() {
        return t.eval(() => $('#tipoApuracaoC option:selected').text().trim());
    }

    getPercentualNaoRecuperavel() {
        return this.percentualNaoRecuperavel.value;
    }

    getFormaApuracaoVolumeParadaProgramada() {
        return t.eval(() => $('#apuracaoParadaProgramadaC option:selected').text().trim());
    }
    
    getFormaApuracaoVolumeFalhaFornecimento() {
        return t.eval(() => $('#apuracaoFalhaFornecimentoC option:selected').text().trim());
    }

    getFormaApuracaoVolumeCasoFortuitoForcaMaior() {
        return t.eval(() => $('#apuracaoCasoFortuitoC option:selected').text().trim());
    }

    // Getters Campos Penalidades

    getPenalidade() {
        return t.eval(() => $('#penalidadeRetMaiorMenor option:selected').text().trim());
    }

    getPeriodicidade() {
        return t.eval(() => $('#periodicidadeRetMaiorMenor option:selected').text().trim());
    }

    getBaseApuracao() {
        return t.eval(() => $('#baseApuracaoRetMaiorMenor option:selected').text().trim());
    }

    getDataInicioVigenciaPenalidades() {
        return this.dataInicioVigenciaPenalidades.value;
    }

    getDataFimVigenciaPenalidades() {
        return this.dataFimVigenciaPenalidades.value;
    }

    getTabelaPrecoCobranca() {
        return t.eval(() => $('#precoCobrancaRetirMaiorMenor option:selected').text().trim());
    }

    getFormaCalculoCobranca() {
        return t.eval(() => $('#tipoApuracaoRetirMaiorMenor option:selected').text().trim());
    }

    getPercentualCobranca() {
        return this.percentualCobranca.value;
    }

    getPercentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao() {
        return this.percentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao.value;
    }

    getConsumoReferencia() {
        return t.eval(() => $('#consumoReferenciaRetMaiorMenor option:selected').text().trim());
    }
    
    getPercentualRetirada() {
        return this.percentualRetirada.value;
    }

    // Getters Campos Principal

    getDDD() {
        return this.ddd.value;
    }

    getNumero() {
        return this.numero.value;
    }

    getEmail() {
        return this.email.value;
    }

    // Getters Campos Dados Técnicos

    getPressaoFornecimento() {
        return t.eval(() => $('#faixaPressaoFornecimento option:selected').text().trim());
    }

    getPressaoColetada() {
        return this.pressaoColetada.value;
    }

    getUnidadePressaoColetada() {
        return t.eval(() => $('#unidadePressaoColetada option:selected').text().trim());
    }

    // Getters Campos Consumo

    getTamanhoReducaoRecuperacaoPCS() {
        return this.tamanhoReducaoRecuperacaoPCS.value;
    }
}
