import { Selector, ClientFunction } from 'testcafe';
import Page from '../../../ggasPage';
import Util from '../../../Util';
import ContratoPage from './ContratoPage';

const page = new Page();
const util = new Util();
const contratoPage = new ContratoPage();

fixture('Contrato')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await contratoPage.irParaContrato();
    });

test('CRUD', async t => {

    

    var tabelaCliente = Selector("#cliente tbody td a");
    var nomeCliente = Selector("#nome");
    var botaoPesquisar = Selector("#botaoPesquisar");
    
    await t.navigateTo(page.url+"/exibirPesquisaClientePopup");
    await page.aguardarCarregarPagina("Pesquisar Pessoa", Selector(".tituloInternoPopup"));
    await t.typeText(nomeCliente, "SEVERINO DA SILVA");
    await t.click(botaoPesquisar);
    await t.expect(tabelaCliente.exists).ok();
    var idCliente = await tabelaCliente.withText("SEVERINO DA SILVA").nth(0).getAttribute("href");
    idCliente = idCliente.replace(/([^\d])+/gim, '');

    var tabelaImovel = Selector("#imovel tbody td a");
    var descricao = Selector("#nomeFantasia");

    await t.navigateTo(page.url+"/exibirPesquisaImovelCompletoPopup?postBack=true");
    await page.aguardarCarregarPagina("Pesquisar Imóvel", Selector(".tituloInternoPopup"));
    await t.typeText(descricao, "BRASKEM POLO");
    await t.click(botaoPesquisar);
    await t.expect(tabelaImovel.exists).ok({tiemOut: 6000});
    var idImovel = await tabelaImovel.withText("BRASKEM POLO").parent("tr").child("td").nth(0).innerText;
    
    await contratoPage.irParaContrato();

    //PESQUISAR POR PESSOA
    await t.click(contratoPage.botaoLimpar);
    await contratoPage.pesquisar("201400886", "ATIVO", "Contrato Principal", "CONTRATO BRASKEM");
    await t
        .expect(contratoPage.tabelaContrato.withText("BRASKEM SA").exists)
        .ok();
    console.log(" ✓ PESQUISAR POR PESSOA");



    //PESQUISAR POR IMÓVEL
    await t.click(contratoPage.botaoLimpar);
    await contratoPage.pesquisar("201400886", "ATIVO", "Contrato Principal", "CONTRATO BRASKEM");
    await t
        .expect(contratoPage.tabelaContrato.withText("BRASKEM SA").exists)
        .ok();
    console.log(" ✓ PESQUISAR POR IMÓVEL");
    await t.click(contratoPage.botaoLimpar);

    

    //INCLUIR MODELO INDUSTRIAL(BRASKEM)

    await contratoPage.incluirContratoIndustrial("ATIVO", util.dateNow(), util.dateNow(5), true, false,
    "1", "2", "3", "4", "5", "6", "8", "7", "PRICE", "8", util.dateNow(),
    true, "POR VALOR", "31 - CR - BT", true, "7", true, "12",
    util.dateNow(0,0,5), "7000",
    "MENSAL", "QDR", "QDP", "5", util.dateNow(), util.dateNow(0, 0, 30), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação da QDC na Cascata Tarifária", true, "7", false, false, false, "", "", "",
    "RETIRADA A MAIOR", "MENSAL", "MENSAL", util.dateNow(1), util.dateNow(2), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação do Volume de Penalidade na Cascata Tarifária", "1", "1", "QDP", "1",
    idImovel, "BRASKEM POLO PONTO CONSUMO",
    "2,3000 kPa",
    ["PONTO DE CONSUMO"], ["REDUZIDO"], "5",
    "RESIDENCIAL TESTE",
    "FIRME FLEXIVEL", util.dateNow(1), "5000", util.dateNow(3), util.dateNow(2), util.dateNow(3), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação da QDC na Cascata Tarifária", "1",
    idCliente, "GESTOR FINANCEIRO", util.dateNow());
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso");
    console.log(" ✓ INCLUIR MODELO INDUSTRIAL(BRASKEM)");


    //INCLUIR CONTRATO COMPLEMENTAR
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.incluirContratoComplementar("Complemento", "5000",
    false, "", "", "",
    util.dateNow(), "5000",
    "MENSAL", "QDR", "QDP", "5", util.dateNow(), util.dateNow(0, 0, 30), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação da QDC na Cascata Tarifária", true, "7", false, false, false, "", "", "",
    "RETIRADA A MAIOR", "MENSAL", "MENSAL", util.dateNow(1), util.dateNow(2), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação do Volume de Penalidade na Cascata Tarifária", "1", "1", "QDP", "1",
    "2,3000 kPa",
    ["PONTO DE CONSUMO"], ["REDUZIDO"], "5",
    "TARIFA TESTE",
    "FIRME FLEXIVEL", "7");
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso");
    console.log(" ✓ INCLUIR CONTRATO COMPLEMENTAR");


    //ALTERAR CONTRATO COMPLEMENTAR
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("Complementar");
    await contratoPage.alterarContratoComplementar("CONTRATO COMPLEMENTAR", "1500",
    false, "", "", "",
    "TRIMESTRAL", "Maior dentre QDP e QDR", "MENOR ENTRE QDC E QDP", "7,00", util.dateNow(), util.dateNow(0, 0, 30), "Tarifa Média do Período", "Pela Aplicação da QDC na Cascata Tarifária", true, "7,00", true, true, true, "QDC", "QDC", "QDC",
    "RETIRADA A MENOR", "TRIMESTRAL", "DIÁRIA", util.dateNow(1), util.dateNow(2), "Tarifa Média do Período", "Pela Aplicação da QDC na Cascata Tarifária", "4,00", "0", "QDC", "7,00",
    "1,0000 atm",
    ["SUPRIDORA"], ["REDUZIDO"], "5",
    "TARIFA TESTE",
    "INTERRUPTIVEL", "7,00");
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso");
    console.log(" ✓ ALTERAR CONTRATO COMPLEMENTAR");



    //ORDENAR CONTRATOS
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.ordenarContrato();
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso: Contratos ordenados com sucesso.");
    console.log(" ✓ ORDENAR CONTRATOS");

    //REMOVER CONTRATO COMPLEMENTAR
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("Complementar");
    await contratoPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso: Contrato(s) removido(s) com sucesso.");
    console.log(" ✓ REMOVER CONTRATO COMPLEMENTAR");


    //ADITAR MODELO INDUSTRIAL(BRASKEM)
    await t.click(contratoPage.botaoLimpar);
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.aditar(util.dateNow(2), "Descrição Aditiva");
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso");
    console.log(" ✓ ADITAR MODELO INDUSTRIAL(BRASKEM)");

    /* //MIGRAR MODELO INDUSTRIAL(BRASKEM) PARA MODELO RESIDENCIAL
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.migrarContrato("RESIDENCIAL");
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso: Contrato(s) migrado(s) com sucesso. Verifique o Log de Migração.");
    console.log(" ✓ MIGRAR MODELO INDUSTRIAL(BRASKEM) PARA MODELO RESIDENCIAL");

    //ALTERAR MODELO RESIDENCIAL
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.alterarContratoResidencial("ATIVO", util.dateNow(), util.dateNow(5), true, "5000", false,
    true, "POR VALOR", "Boleto Bancário", "103032 - CR - BT", true, "8", true, "9", true, "BANCO BRADESCO SA - BRA", "5421", "765421",
    util.dateNow(0,0,5), "9000",
    "99", "88888888", "Teste@gmail.com",
    "1,0000 atm", "632", "atm",
    ["PONTO DE CONSUMO", "SUPRIDORA", "FIXO POR CITY GATE", "DISTRIBUIDORA"], ["ESTENDIDO", "REDUZIDO", "REAL"], "5",
    "RESIDENCIAL TESTE",
    "FIRME FLEXIVEL",
    idCliente, "GESTOR FINANCEIRO", util.dateNow());
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso");
    console.log(" ✓ ALTERAR MODELO RESIDENCIAL");

    //REMOVER MODELO RESIDENCIAL
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso: Contrato(s) removido(s) com sucesso.");
    console.log(" ✓ REMOVER MODELO RESIDENCIAL");

    //INCLUIR MODELO RESIDENCIAL
    await contratoPage.incluirContratoResidencial("ATIVO", util.dateNow(), util.dateNow(5), true, "5000", false,
    true, "POR VALOR", "Boleto Bancário", "103032 - CR - BT", true, "8", true, "9", false, "BANCO BRADESCO SA - BRA", "5421", "765421",
    util.dateNow(0,0,5), "9000",
    "11163",
    "99", "88888888", "Teste@gmail.com",
    "1,0000 atm", "632", "atm",
    ["PONTO DE CONSUMO"], ["REDUZIDO"], "5",
    "RESIDENCIAL TESTE",
    "FIRME FLEXIVEL",
    idCliente, "GESTOR FINANCEIRO", util.dateNow());
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso");
    console.log(" ✓ INCLUIR MODELO RESIDENCIAL");

    //ADITAR MODELO RESIDENCIAL
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.aditar(util.dateNow(2), "Descrição Aditiva");
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso");
    console.log(" ✓ ADITAR MODELO RESIDENCIAL");

    //MIGRAR MODELO RESIDENCIAL PARA MODELO INDUSTRIAL(BRASKEM)
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.migrarContrato("CONTRATO BRASKEM");
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso: Contrato(s) migrado(s) com sucesso. Verifique o Log de Migração.");
    console.log(" ✓ MIGRAR MODELO RESIDENCIAL PARA MODELO INDUSTRIAL(BRASKEM)");
    
    //ALTERAR MODELO INDUSTRIAL(BRASKEM)
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.alterarContratoIndustrial("ATIVO", util.dateNow(), util.dateNow(5), true, false,
    "1", "2", "3", "4", "5", "6", "8", "7", "PRICE", "8", util.dateNow(),
    true, "POR VALOR", "31 - CR - BT", true, "7", true, "12",
    util.dateNow(0,0,5), "7000",
    "MENSAL", "QDR", "QDP", "5", util.dateNow(), util.dateNow(0, 0, 30), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação da QDC na Cascata Tarifária", true, "7", false, false, false, "", "", "",
    "RETIRADA A MAIOR", "MENSAL", "MENSAL", util.dateNow(1), util.dateNow(2), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação do Volume de Penalidade na Cascata Tarifária", "1", "1", "QDP", "1",
    "2,3000 kPa",
    ["PONTO DE CONSUMO", "SUPRIDORA", "FIXO POR CITY GATE", "DISTRIBUIDORA"], ["ESTENDIDO", "REDUZIDO", "REAL"], "5",
    "RESIDENCIAL TESTE",
    "FIRME FLEXIVEL", util.dateNow(1), "5000", util.dateNow(3), util.dateNow(2), util.dateNow(3), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação da QDC na Cascata Tarifária", "1",
    idCliente, "GESTOR FINANCEIRO", util.dateNow());
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso");
    console.log(" ✓ ALTERAR MODELO INDUSTRIAL(BRASKEM)");

    //ENCERRAR CONTRATO BRASKEM
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("BRASKEM SA");
    await t
        .expect(contratoPage.botaoEncerrar.exists).ok();
    await contratoPage.encerrar();
    await t
        .expect(Selector(".notification").exists).ok();
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso");
    console.log(" ✓ ENCERRAR CONTRATO BRASKEM");

    //MIGRAR SALDO DO CONTRATO BRASKEM PARA CONTRATO INDUSTRIAL
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("BRASKEM SA");
    await contratoPage.migrarSaldo("TESTE", idCliente, idImovel, "48000", "0");
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso: Saldo Migrado com sucesso.");
    console.log(" ✓ MIGRAR SALDO DO CONTRATO BRASKEM PARA CONTRATO INDUSTRIAL");

    //REMOVER MODELO INDUSTRIAL(BRASKEM)
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("BRASKEM SA");
    await contratoPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso: Contrato(s) removido(s) com sucesso.");
    console.log(" ✓ REMOVER MODELO INDUSTRIAL(BRASKEM)");*/


    //ALTERANDO MODELO INDUSTRIAL(SEVERINO) PARA REMOÇÃO
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.alterarContratoIndustrial("EM NEGOCIACAO", util.dateNow(), util.dateNow(5), true, false,
    "1", "2", "3", "4", "5", "6", "8", "7", "PRICE", "8", util.dateNow(),
    true, "POR VALOR", "31 - CR - BT", true, "7", true, "12",
    util.dateNow(0,0,5), "7000",
    "MENSAL", "QDR", "QDP", "5", util.dateNow(), util.dateNow(0, 0, 30), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação da QDC na Cascata Tarifária", true, "7", false, false, false, "", "", "",
    "RETIRADA A MAIOR", "MENSAL", "MENSAL", util.dateNow(1), util.dateNow(2), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação do Volume de Penalidade na Cascata Tarifária", "1", "1", "QDP", "1",
    "2,3000 kPa",
    ["PONTO DE CONSUMO", "SUPRIDORA", "FIXO POR CITY GATE", "DISTRIBUIDORA"], ["ESTENDIDO", "REDUZIDO", "REAL"], "5",
    "RESIDENCIAL TESTE",
    "FIRME FLEXIVEL", util.dateNow(1), "5000", util.dateNow(3), util.dateNow(2), util.dateNow(3), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação da QDC na Cascata Tarifária", "1",
    idCliente, "GESTOR FINANCEIRO", util.dateNow());
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso");
    console.log(" ✓ ALTERAR MODELO INDUSTRIAL(SEVERINO)");

    //REMOVER MODELO INDUSTRIAL(SEVERINO)
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso: Contrato(s) removido(s) com sucesso.");
    console.log(" ✓ REMOVER MODELO INDUSTRIAL(BRASKEM)");




});

test('Teste de obrigatoriedade', async t => {

    const periodicidadeToP = "MENSAL";
    const referenciaQuantidadeFaltanteParadaProgramada = "QDR";
    const consumoReferencialToP = "QDP";
    const compromissoRetiradaToP = "5,00";
    const dataInicioVigenciaPenalidades = util.dateNow();
    const dataFimVigenciaPenalidades = util.dateNow(0, 0, 30);
    const tabelaPrecosCobranca = "Tabela de Preços Vigente no Último Dia do Período de Penalidade";
    const formaCalculoCobrancaRecuperacao = "Pela Aplicação da QDC na Cascata Tarifária";
    const considerarParadasProgramadas = true;
    const considerarFalhaFornecimento = true;
    const considerarCasosFortuitos = true;
    const recuperavel = false;
    const porcentagemRecuperavel = "7,9";
    const formaApuracaoVolumeParadaProgramada = "QDC" ;
    const formaApuracaoVolumeFalhaFornecimento = "QDC";
    const formaApuracaoVolumeCasoFortuitoForcaMaior  = "QDP - QRetD";

    const penalidade = "RETIRADA A MAIOR";
    const periodicidade = "MENSAL";
    const baseApuracao = "DIÁRIA";
    const dataInicioVigencia = util.dateNow(1);
    const dataFimVigencia = util.dateNow(2);
    const tabelaPrecoCobranca = "Tabela de Preços Vigente no Último Dia do Período de Penalidade";
    const formaCalculoCobranca = "Pela Aplicação do Volume de Penalidade na Cascata Tarifária";
    const percentualCobranca = "1,00";
    const percentualCobrancaConsumo = "2,00";
    const consumoReferencia = "QDP";
    const percentualRetirada = "3,00";

    var tabelaCliente = Selector("#cliente tbody td a");
    var nomeCliente = Selector("#nome");
    var botaoPesquisar = Selector("#botaoPesquisar");
    
    await t.navigateTo(page.url+"/exibirPesquisaClientePopup");
    await page.aguardarCarregarPagina("Pesquisar Pessoa", Selector(".tituloInternoPopup"));
    await t.typeText(nomeCliente, "SEVERINO DA SILVA");
    await t.click(botaoPesquisar);
    await t.expect(tabelaCliente.exists).ok();
    var idCliente = await tabelaCliente.withText("SEVERINO DA SILVA").nth(0).getAttribute("href");
    idCliente = idCliente.replace(/([^\d])+/gim, '');

    var tabelaImovel = Selector("#imovel tbody td a");
    var descricao = Selector("#nomeFantasia");

    await t.navigateTo(page.url+"/exibirPesquisaImovelCompletoPopup?postBack=true");
    await page.aguardarCarregarPagina("Pesquisar Imóvel", Selector(".tituloInternoPopup"));
    await t.typeText(descricao, "BRASKEM POLO");
    await t.click(botaoPesquisar);
    await t.expect(tabelaImovel.exists).ok({tiemOut: 6000});
    var idImovel = await tabelaImovel.withText("BRASKEM POLO").parent("tr").child("td").nth(0).innerText;

    await contratoPage.irParaContrato();
    
    await t
        .typeText(contratoPage.numeroContrato, "abc<*>")
        .click(contratoPage.botaoPesquisar)

        .expect(Selector(".notification").innerText)
        .contains("Erro: Dado(s) inválido(s) para o(s) campo(s) Número do Contrato.");

    /*
     *   MODELO INDUSTRIAL
     */

    //Tela de inclusão
    await t.click(contratoPage.botaoIncluir);
    await contratoPage.setModeloContrato("CONTRATO BRASKEM");

    await page.preencherFormularioPessoa(idCliente);
    await contratoPage.irParaAba("Take or Pay");
    await contratoPage.setTakeOrPay(periodicidadeToP, referenciaQuantidadeFaltanteParadaProgramada, consumoReferencialToP, compromissoRetiradaToP, dataInicioVigenciaPenalidades, dataFimVigenciaPenalidades, tabelaPrecosCobranca, formaCalculoCobrancaRecuperacao, recuperavel, porcentagemRecuperavel, considerarParadasProgramadas, considerarFalhaFornecimento, considerarCasosFortuitos, formaApuracaoVolumeParadaProgramada, formaApuracaoVolumeFalhaFornecimento, formaApuracaoVolumeCasoFortuitoForcaMaior);
    await contratoPage.irParaAba("Pontos de Consumo");
    await contratoPage.setPontoConsumo(idImovel, "BRASKEM POLO PONTO CONSUMO");
    await t
        .click(contratoPage.botaoAvancar);

    await contratoPage.irParaAba("Dados Gerais")
    await contratoPage.setDataAssinatura(util.dateNow());
    await t
        .click(contratoPage.botaoAvancar)

    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Arrecadador Contrato Convenio, Vencimento das Obrigações Contratuais é (são) de preenchimento obrigatório.");

    await contratoPage.irParaAba("Dados Gerais")
    await contratoPage.setDataVencimentoObrigacoesContratuais(util.dateNow(5));
    await t
        .click(contratoPage.botaoAvancar)

    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Arrecadador Contrato Convenio é (são) de preenchimento obrigatório.");
    

    await contratoPage.irParaAba("Dados Financeiros");
    await contratoPage.setArrecadadorConvenio("31 - CR - BT");
    await t
        .click(contratoPage.botaoAvancar)
        
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Percentual de Juros de Mora, Percentual de Multa é (são) de preenchimento obrigatório.");

    
    await contratoPage.irParaAba("Dados Financeiros");
    await contratoPage.setContratoPassivelJurosMora(true, "3");
    await t
        .click(contratoPage.botaoAvancar)

    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Percentual de Multa é (são) de preenchimento obrigatório.");


    await contratoPage.irParaAba("Dados Financeiros");
    await contratoPage.setContratoPassivelMulta(true, "5");

    await contratoPage.irParaAba("Penalidades");
    await contratoPage.setPenalidades(penalidade, periodicidade, baseApuracao, dataInicioVigencia, dataFimVigencia, tabelaPrecoCobranca, formaCalculoCobranca, percentualCobranca, percentualCobrancaConsumo, consumoReferencia, percentualRetirada);


    await t
        .click(contratoPage.botaoAvancar)
        .click(contratoPage.botaoAplicar)
    
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Pressão de Fornecimento da(s) aba(s) Dados Técnicos é(são) de preenchimento obrigatório.");

    await t.click(contratoPage.botaoSalvarParcial);
    await contratoPage.setPressaoFornecimento("1,0000 atm");
    await t
        .click(contratoPage.botaoAplicar)
    
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Local de Amostragem do PCS, Intervalo de Amostragem do PCS da(s) aba(s) Consumo é(são) de preenchimento obrigatório.");

    await contratoPage.irParaAba("Consumo");

    await contratoPage.setLocalAmostragem(["DISTRIBUIDORA"]);
    await t
        .click(contratoPage.botaoAdicionarLocalAmostragem)
        .click(contratoPage.botaoAplicar)
    
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Intervalo de Amostragem do PCS da(s) aba(s) Consumo é(são) de preenchimento obrigatório.");

    await contratoPage.irParaAba("Consumo");
    await contratoPage.setIntervaloAmostragens(["REAL"]);
    await t
        .click(contratoPage.botaoAdicionarIntervaloAmostragem)
        .click(contratoPage.botaoAplicar)
    
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Item da Fatura da(s) aba(s) Faturamento é(são) de preenchimento obrigatório.");

    
    
    
                                
    await t.click(contratoPage.botaoAplicar);

    await contratoPage.irParaAba("Faturamento");
    await t
        .click(contratoPage.botaoAdicionarFaturamento)

    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Tarifa para Preço do Gás da(s) aba(s) Faturamento é(são) de preenchimento obrigatório.");
    

    await contratoPage.irParaAba("Faturamento");
    await contratoPage.setFaturamento("RESIDENCIAL TESTE");


    await contratoPage.irParaAba("Responsabilidades");
    await t
        .click(contratoPage.botaoAdicionarResponsabilidade)

    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Tipo de Responsabilidade, Data de Início do Relacionamento é (são) de preenchimento obrigatório.");



    await contratoPage.irParaAba("Responsabilidades");
    //await page.preencherFormularioPessoa(idCliente);
    await t
        .click(contratoPage.botaoAdicionarResponsabilidade)

    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Tipo de Responsabilidade, Data de Início do Relacionamento é (são) de preenchimento obrigatório.");
    

    await contratoPage.irParaAba("Responsabilidades");
    await contratoPage.setDataInicioRelacionamento(util.dateNow(0,0,5));
    await t
        .click(contratoPage.botaoAdicionarResponsabilidade)

    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s) Tipo de Responsabilidade é (são) de preenchimento obrigatório.");


    await contratoPage.irParaAba("Responsabilidades");
    await contratoPage.setTipoResponsabilidade("GESTOR FINANCEIRO");

    await contratoPage.irParaAba("Responsabilidades");
    await contratoPage.setResponsabilidades(idCliente, "GESTOR FINANCEIRO", util.dateNow())
    await contratoPage.irParaAba("Responsabilidades");

    await t
        .click(contratoPage.botaoAdicionarResponsabilidade)

    await contratoPage.irParaAba("Responsabilidades");
    await t.click(contratoPage.botaoAplicar);

    await contratoPage.irParaAba("Modalidades Consumo Faturamento");
    await contratoPage.setQDCModalidade(util.dateNow(1),"5000");
    await contratoPage.setTakeOrPayModalidade(util.dateNow(3), util.dateNow(2), util.dateNow(3), "Tabela de Preços Vigente no Último Dia do Período de Penalidade", "Pela Aplicação da QDC na Cascata Tarifária", "1");
    await contratoPage.irParaAba("Modalidades Consumo Faturamento");
    
    await contratoPage.setModalidadeConsumoFaturamento("FIRME FLEXIVEL");
    await t.click(contratoPage.botaoAplicar);

    await contratoPage.irParaAba("Consumo");
    await contratoPage.setConsumo(["PONTO DE CONSUMO", "SUPRIDORA", "FIXO POR CITY GATE"],
                                ["ESTENDIDO", "REDUZIDO"], "5");

    await t.setNativeDialogHandler(() => true);
    
    await t.click(contratoPage.botaoAplicar);
    await t.click(contratoPage.botaoSalvar);

    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await t.click(contratoPage.botaoAlterar);
    //await contratoPage.irParaAba("Take or Pay");
    //await t.wait(1000);
    //await contratoPage.selecionarTakeOrPay();
    //await t.click(contratoPage.botaoEditarTakeOrPay);

    /*await t
        .expect(contratoPage.getPeriodicidadeToP()).contains(periodicidadeToP)
        .expect(contratoPage.getReferenciaQuantidadeFaltanteParadaProgramada()).contains(referenciaQuantidadeFaltanteParadaProgramada)
        .expect(contratoPage.getConsumoReferencialToP()).contains(consumoReferencialToP)
        .expect(contratoPage.getCompromissoRetiradaToP()).contains(compromissoRetiradaToP)
        .expect(contratoPage.getDataInicioVigencia()).contains(dataInicioVigenciaPenalidades)
        .expect(contratoPage.getDataFimVigencia()).contains(dataFimVigenciaPenalidades)
        .expect(contratoPage.getTabelaPrecosCobranca()).contains(tabelaPrecosCobranca)
        .expect(contratoPage.getFormaCalculoCobrancaRecuperacao()).contains(formaCalculoCobrancaRecuperacao)
        //.expect(contratoPage.getPercentualNaoRecuperavel()).contains(porcentagemRecuperavel)
        .expect(contratoPage.getFormaApuracaoVolumeParadaProgramada()).contains(formaApuracaoVolumeParadaProgramada)
        .expect(contratoPage.getFormaApuracaoVolumeFalhaFornecimento()).contains(formaApuracaoVolumeFalhaFornecimento)
        .expect(contratoPage.getFormaApuracaoVolumeCasoFortuitoForcaMaior()).contains(formaApuracaoVolumeCasoFortuitoForcaMaior);
    */
    await contratoPage.irParaAba("Penalidades");
    await contratoPage.selecionarPenalidade();
    await t.click(contratoPage.botaoEditarPenalidade);

    await t
        .expect(contratoPage.getPenalidade()).contains(penalidade)
        .expect(contratoPage.getPeriodicidade()).contains(periodicidade)
        .expect(contratoPage.getBaseApuracao()).contains(baseApuracao)
        .expect(contratoPage.getDataInicioVigenciaPenalidades()).contains(dataInicioVigencia)
        .expect(contratoPage.getDataFimVigenciaPenalidades()).contains(dataFimVigencia)
        .expect(contratoPage.getTabelaPrecoCobranca()).contains(tabelaPrecoCobranca)
        .expect(contratoPage.getFormaCalculoCobranca()).contains(formaCalculoCobranca)
        .expect(contratoPage.getPercentualCobranca()).contains(percentualCobranca)
        .expect(contratoPage.getPercentualCobrancaConsumoSuperiorDuranteAvisoInterrupcao()).contains(percentualCobrancaConsumo)
        .expect(contratoPage.getConsumoReferencia()).contains(consumoReferencia)
        .expect(contratoPage.getPercentualRetirada()).contains(percentualRetirada);

    await t.click(contratoPage.botaoCancelar);
    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso: Contrato(s) removido(s) com sucesso.");

    /*
     *   MODELO RESIDENCIAL
     */
    
    //Tela de inclusão
    await t.click(contratoPage.botaoIncluir);
    await contratoPage.setModeloContrato("RESIDENCIAL");

    await contratoPage.irParaAba("Pontos de Consumo");
    await contratoPage.setPontoConsumo(idImovel, "BRASKEM POLO PONTO CONSUMO");
    await t
        .click(contratoPage.botaoAvancar)

    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");

    await page.preencherFormularioPessoa(idCliente);
    await t
        .click(contratoPage.botaoAvancar)

    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");

    await contratoPage.irParaAba("Dados Gerais")
    await contratoPage.setDataAssinatura(util.dateNow());
    await t
        .click(contratoPage.botaoAvancar)

    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");

    await contratoPage.irParaAba("Dados Gerais")
    await contratoPage.setDataVencimentoObrigacoesContratuais(util.dateNow(5));
    await t
        .click(contratoPage.botaoAvancar)

    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");


    await contratoPage.irParaAba("Dados Financeiros");
    await contratoPage.setDebitoAutomaticoSim();
    await t
        .click(contratoPage.botaoAvancar)
        
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");


    await contratoPage.irParaAba("Dados Financeiros");
    await contratoPage.setBanco("CAIXA");
    await t
        .click(contratoPage.botaoAvancar)
        
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");


    await contratoPage.irParaAba("Dados Financeiros");
    await contratoPage.setAgencia("1234");
    await t
        .click(contratoPage.botaoAvancar)
        
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");


    await contratoPage.irParaAba("Dados Financeiros");
    await contratoPage.setContaCorrente("1234567");
    await t
        .click(contratoPage.botaoAvancar)
        
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");
    
    await contratoPage.irParaAba("Dados Financeiros");
    await contratoPage.setArrecadadorConvenio("31 - CR - BT");
    await t
        .click(contratoPage.botaoAvancar);
    
    await contratoPage.irParaAba("Dados Financeiros");
    await contratoPage.setBanco("CAIXA");
    await contratoPage.setAgencia("1234");
    await contratoPage.setContaCorrente("1234567");

    await t
        .click(contratoPage.botaoAvancar);

    //SEGUNDA PARTE
    await contratoPage.irParaAba("Dados Técnicos");
    await contratoPage.setPressaoFornecimento("2,3000 kPa");
    await t
        .click(contratoPage.botaoAplicar)
         
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");


    await contratoPage.irParaAba("Consumo");
    await contratoPage.setLocalAmostragem(["DISTRIBUIDORA"]);
    await t
        .click(contratoPage.botaoAplicar)
        
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");


    await contratoPage.irParaAba("Consumo");
    await contratoPage.setIntervaloAmostragens(["REAL"]);
    await t
        .click(contratoPage.botaoAplicar)
        
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");


    await contratoPage.irParaAba("Faturamento");
    await contratoPage.setFaturamento("RESIDENCIAL TESTE");
    await t
        .click(contratoPage.botaoAplicar)
        
    .expect(Selector(".notification").innerText)
    .contains("Erro: O(s) campo(s)");

    await contratoPage.irParaAba("Modalidades Consumo Faturamento");
    await contratoPage.setQDCModalidade(util.dateNow(1),"5000");
    await contratoPage.setModalidadeConsumoFaturamento("FIRME FLEXIVEL");
    await t
        .click(contratoPage.botaoAplicar);
    

    await contratoPage.irParaAba("Responsabilidades");
    await contratoPage.setResponsabilidades(idCliente, "GESTOR FINANCEIRO", util.dateNow());
    await t
        .click(contratoPage.botaoAplicar)
        .click(contratoPage.botaoSalvar);

    await t.click(contratoPage.botaoPesquisar);
    await contratoPage.selecionar("SEVERINO DA SILVA");
    await contratoPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .contains("Sucesso");
});
