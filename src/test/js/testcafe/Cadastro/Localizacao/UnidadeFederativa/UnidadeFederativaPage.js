import { Selector, t } from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class UnidadeFederativaPage {

    constructor() {

        this.table = Selector("table#unidadeFederacao tbody tr td");
        
        this.botaoPesquisar = Selector("#botaoPesquisar");
        this.botaoIncluir = Selector("#buttonIncluir");
        this.botaoAlterar = Selector("#botaoAlterar");
        this.botaoRemover = Selector("#botaoRemover");

        this.descricao = Selector("#descricao");
        this.sigla = Selector("#sigla");
        this.pais = Selector("#idPais");
        this.paisOptioon = this.pais.find("option");
        this.codigoIBGE = Selector("#codigoIBGE");

        this.botaoLimpar = Selector("#limparFormulario");
        this.botaoSalvar = Selector("#botaoSalvar");
    }

    async irParaUnidadeFederativa() {
        await t.navigateTo(page.url+"/exibirPesquisaUnidadeFederacao");
    }

    async selecionar(descricao) {

        await t.click(this.table.withText(descricao).parent("tr").find("input"));
    }

	async incluir(descricao, sigla, pais, codigoIBGE) {
        await t.click(this.botaoIncluir);

        await this.setDescricao(descricao);
        await this.setSigla(sigla);
        await this.setPais(pais);
        await this.setCodigoIBGE(codigoIBGE);

        await t.click(this.botaoSalvar);
    }

    async alterar(descricao, sigla, pais, codigoIBGE) {
        await t
            .click(this.botaoAlterar)
            .click(this.botaoLimpar);

        await this.setDescricao(descricao);
        await this.setSigla(sigla);
        await this.setPais(pais);
        await this.setCodigoIBGE(codigoIBGE);

        await t.click(this.botaoSalvar);
    }

    async remover() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }

    async setDescricao(descricao) {
        await t.typeText(this.descricao, descricao);
    }

    async setSigla(sigla) {
        await t.typeText(this.sigla, sigla);
    }

    async setPais(pais) {
        await t
            .click(this.pais)
            .click(this.paisOptioon.withText(pais));
    }

    async setCodigoIBGE(codigoIBGE) {
        await t.typeText(this.codigoIBGE, codigoIBGE);
    }
}
