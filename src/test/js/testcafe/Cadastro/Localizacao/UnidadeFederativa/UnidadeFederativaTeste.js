import { Selector } from 'testcafe';
import Page from '../../../ggasPage';
import UnidadeFederativaPage from './UnidadeFederativaPage';

const page = new Page();
const unidadeFederativaPage = new UnidadeFederativaPage();

fixture('Unidade Federativa')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        unidadeFederativaPage.irParaUnidadeFederativa();
    });

test("CRUD", async t => {
	await t.expect(unidadeFederativaPage.botaoIncluir.exists).ok();
    await unidadeFederativaPage.incluir("UNIDADE FEDERATIVA TESTE", "UT", "BRASIL", "101");
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Unidade Federativa inserido(a) com sucesso.");
    

    await unidadeFederativaPage.selecionar("UNIDADE FEDERATIVA TESTE");
    await unidadeFederativaPage.alterar("BACU", "BC", "AZERBAIJAO, REPUBLICA DO", "010");
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Unidade Federativa alterado(a) com sucesso.");


    await unidadeFederativaPage.selecionar("BACU");
    await unidadeFederativaPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Unidade Federativa removida(s) com sucesso.");
});

test("Testando Obrigatoriedade", async t => {

    await t
        .click(unidadeFederativaPage.botaoIncluir)
        .click(unidadeFederativaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, País é (são) de preenchimento obrigatório.");
    

    await unidadeFederativaPage.setCodigoIBGE("101");
    await t
        .click(unidadeFederativaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, País é (são) de preenchimento obrigatório.");


    await unidadeFederativaPage.setSigla("UT");
    await t
        .click(unidadeFederativaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, País é (são) de preenchimento obrigatório.");


    await unidadeFederativaPage.setDescricao("UNIDADE FEDERATIVA TESTE");
    await t
        .click(unidadeFederativaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) País é (são) de preenchimento obrigatório.");

        
    await unidadeFederativaPage.setPais("BRASIL");
    await t
        .click(unidadeFederativaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Unidade Federativa inserido(a) com sucesso.");

    
    await unidadeFederativaPage.selecionar("UNIDADE FEDERATIVA TESTE");

    await t
        .click(unidadeFederativaPage.botaoAlterar)
        .click(unidadeFederativaPage.botaoLimpar)
        .click(unidadeFederativaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, País é (são) de preenchimento obrigatório.");

    await unidadeFederativaPage.setCodigoIBGE("010");
    await t
        .click(unidadeFederativaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, País é (são) de preenchimento obrigatório.");


    await unidadeFederativaPage.setSigla("BC");
    await t
        .click(unidadeFederativaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, País é (são) de preenchimento obrigatório.");


    await unidadeFederativaPage.setDescricao("BACU");
    await t
        .click(unidadeFederativaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) País é (são) de preenchimento obrigatório.");

        
    await unidadeFederativaPage.setPais("AZERBAIJAO, REPUBLICA DO");
    await t
        .click(unidadeFederativaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Unidade Federativa alterado(a) com sucesso.");

    await unidadeFederativaPage.selecionar("BACU");
    await unidadeFederativaPage.remover();
});