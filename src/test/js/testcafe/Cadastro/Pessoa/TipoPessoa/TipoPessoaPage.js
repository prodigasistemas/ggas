import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class TipoPessoaPage {

    constructor() {

        this.botaoIncluir = Selector("#incluir");
        this.botaoAlterar = Selector("#botaoAlterar");
        this.botaoRemover = Selector("#botaoRemover");
        this.botaoSalvar = Selector("input[value=Salvar]");
        this.botaoLimpar = Selector("input[value=Limpar]")

        this.descricao = Selector("#descricao");
        this.tipoPessoa = Selector("#idTipoPessoa");
        this.tipoPessoaOption = this.tipoPessoa.find("option");
        this.esferaPoder = Selector("#idEsferaPoder");
        this.esferaPoderOption = this.esferaPoder.find("option");
    }

    async irParaTipoPessoaPage(){

        await t.navigateTo(page.url+"/exibirPesquisaTipoPessoa");
    }

    async selecionar(opcao) {
        await t.click(Selector("table#tabelaAuxiliar tbody tr td").withText(opcao).parent("tr").find("input"));
    }

    async incluir(descricao, tipoPessoa, esferaPoder) {

        await t.click(this.botaoIncluir);

        await this.setDescricao(descricao);
        await this.setTipoPessoa(tipoPessoa);
        await this.setEsferaPoder(esferaPoder);

        await t
            .click(this.botaoSalvar);
    }

    async alterar(descricao, tipoPessoa, esferaPoder) {
        await t
            .click(this.botaoAlterar)
            .click(this.botaoLimpar);

        await this.setDescricao(descricao);
        await this.setTipoPessoa(tipoPessoa);
        await this.setEsferaPoder(esferaPoder);

        await t
            .click(this.botaoSalvar);
    }

    async remover() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }


    async setDescricao(descricao) {
        await t.typeText(this.descricao, descricao);
    }

    async setTipoPessoa(tipoPessoa) {
        await t
            .click(this.tipoPessoa)
            .click(this.tipoPessoaOption.withText(tipoPessoa));
    }

    async  setEsferaPoder(esferaPoder) {
        await t
            .click(this.esferaPoder)
            .click(this.esferaPoderOption.withText(esferaPoder));
    }
}
