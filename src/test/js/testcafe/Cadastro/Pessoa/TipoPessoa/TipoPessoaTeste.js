import {Selector} from 'testcafe';
import Page from '../../../ggasPage';
import TipoPessoaPage from './TipoPessoaPage';

const page = new Page();
const tipoPessoaPage = new TipoPessoaPage();

fixture('Tipo de Pessoa')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await tipoPessoaPage.irParaTipoPessoaPage();
    });

test('CRUD', async t => {

    await tipoPessoaPage.incluir("PESSOA ESCPECIAL", "Pessoa Física", "FEDERAL");
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Pessoa inserido(a) com sucesso.");


    await tipoPessoaPage.selecionar("PESSOA ESCPECIAL");
    await tipoPessoaPage.alterar("PESSOA VIP", "Pessoa Jurídica", "ESTADUAL");
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Pessoa alterado(a) com sucesso.");


    await tipoPessoaPage.selecionar("PESSOA VIP");
    await tipoPessoaPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Pessoa removido(s) com sucesso.");
});

test("Obrigatoriedade dos campos", async t => {

    //Tela de Inclusão
    await t
        .click(tipoPessoaPage.botaoIncluir)
        .click(tipoPessoaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, Tipo Pessoa é (são) de preenchimento obrigatório.");

    
    await tipoPessoaPage.setEsferaPoder("FEDERAL");
    await t
        .click(tipoPessoaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, Tipo Pessoa é (são) de preenchimento obrigatório.");
    

    await tipoPessoaPage.setDescricao("PESSOA ESPECIAL");
    await t
        .click(tipoPessoaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Tipo Pessoa é (são) de preenchimento obrigatório.");

    
    await tipoPessoaPage.setTipoPessoa("Pessoa Física");
    await t
        .click(tipoPessoaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Pessoa inserido(a) com sucesso.");

    
    //Tela de Alteração
    await tipoPessoaPage.selecionar("PESSOA ESPECIAL");
    await t
        .click(tipoPessoaPage.botaoAlterar)
        .click(tipoPessoaPage.botaoLimpar)
        .click(tipoPessoaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, Tipo Pessoa é (são) de preenchimento obrigatório.");

    
    await tipoPessoaPage.setEsferaPoder("ESTADUAL");
    await t
        .click(tipoPessoaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição, Tipo Pessoa é (são) de preenchimento obrigatório.");


    await tipoPessoaPage.setDescricao("PESSOA VIP");
    await t
        .click(tipoPessoaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Tipo Pessoa é (são) de preenchimento obrigatório.");

    
    await tipoPessoaPage.setTipoPessoa("Pessoa Jurídica");
    await t
        .click(tipoPessoaPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Pessoa alterado(a) com sucesso.");

    //Remoção
    await tipoPessoaPage.selecionar("PESSOA VIP");
    await tipoPessoaPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Pessoa removido(s) com sucesso.");
});