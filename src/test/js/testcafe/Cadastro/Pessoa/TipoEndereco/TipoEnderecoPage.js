import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class TipoEnderecoPage {

    constructor() {

        this.botaoIncluir = Selector('#botaoIncluir');
        this.botaoAlterar = Selector('#botaoAlterar');
        this.botaoRemover = Selector('#botaoRemover');
        this.botaoSalvar = Selector('#botaoSalvar');
        this.botaoLimpar = Selector('#limparFormulario');

        this.descricao = Selector("#descricao");
    }

    async irParaTipoEndereco(){
        await t
            .navigateTo(page.url+"/pesquisarTipoEndereco");
    }

    async selecionar(descricao) {
        await t
            .click(Selector("#tipoEndereco tbody td").withText(descricao).parent("tr").find("input"));
    }

    async incluir(descricao) {
        await t.click(this.botaoIncluir);
        await this.setDescricao(descricao);
        await t.click(this.botaoSalvar);
    }

    async alterar(descricao) {
        await t
            .click(this.botaoAlterar)
            .click(this.botaoLimpar);
        await this.setDescricao(descricao);
        await t.click(this.botaoSalvar);
    }

    async remover() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }

    async setDescricao(descricao) {
        await t
            .typeText(this.descricao, descricao);
    }

}
