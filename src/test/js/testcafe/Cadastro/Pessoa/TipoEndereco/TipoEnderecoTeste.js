import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';
import TipoEnderecoPage from './TipoEnderecoPage';

const page = new Page()
const tipoEnderecoPage = new TipoEnderecoPage();
/*
fixture('Tipo de Endereço')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await tipoEnderecoPage.irParaTipoEndereco();
    });

test('CRUD', async t => {

    await tipoEnderecoPage.incluir("GLOBAL");
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo de Endereço inserido(a) com sucesso.');

    await tipoEnderecoPage.selecionar('GLOBAL');
    await tipoEnderecoPage.alterar("PARTICULAR");
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo de Endereço alterado(a) com sucesso.');

    await tipoEnderecoPage.selecionar('PARTICULAR');
    await tipoEnderecoPage.remover();
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo de Endereço removida(s) com sucesso.');
});

test('Teste de Obrigatoriedade', async t => {

    await t
        .click(tipoEnderecoPage.botaoIncluir)
        .click(tipoEnderecoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");


    await tipoEnderecoPage.setDescricao("GLOBAL");
    await t
        .click(tipoEnderecoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo de Endereço inserido(a) com sucesso.');


    await tipoEnderecoPage.selecionar("GLOBAL");
    await t
        .click(tipoEnderecoPage.botaoAlterar)
        .click(tipoEnderecoPage.botaoLimpar)
        .click(tipoEnderecoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");


    await tipoEnderecoPage.setDescricao("PARTICULAR");
    await t
        .click(tipoEnderecoPage.botaoSalvar)
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo de Endereço alterado(a) com sucesso.');


    await tipoEnderecoPage.selecionar('PARTICULAR');
    await tipoEnderecoPage.remover();
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Tipo de Endereço removida(s) com sucesso.');
});
*/