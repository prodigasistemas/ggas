import {Selector} from 'testcafe';
import Page from '../../../ggasPage';
import TipoRelacionamentoPage from './TipoRelacionamentoPage';

const page = new Page();
const tipoRelacionamentoPage = new TipoRelacionamentoPage();
/*
fixture('Teste Relacionamento')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await tipoRelacionamentoPage.irParaTipoRelacionamento();
    });

test('CRUD', async t => {

    await tipoRelacionamentoPage.incluir("AGENOR", false);
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Relacionamento inserido(a) com sucesso.");


    await tipoRelacionamentoPage.selecionar("AGENOR");
    await tipoRelacionamentoPage.alterar("JUAREZ", false);
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Relacionamento alterado(a) com sucesso.");


    await tipoRelacionamentoPage.selecionar("JUAREZ");
    await tipoRelacionamentoPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Relacionamento removido(s) com sucesso.");
});

test("Obrigatoriedade dos campos", async t => {

    await t
        .click(tipoRelacionamentoPage.botaoIncluir)
        .click(tipoRelacionamentoPage.botaoSalvar)

        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");
    

    await tipoRelacionamentoPage.setPrincipal(false);
    await t
        .click(tipoRelacionamentoPage.botaoSalvar)

        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");


    await tipoRelacionamentoPage.setDescricao("AGENOR");
    await t
        .click(tipoRelacionamentoPage.botaoSalvar)

        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Relacionamento inserido(a) com sucesso.");

    
    await tipoRelacionamentoPage.selecionar("AGENOR");
    await t
        .click(tipoRelacionamentoPage.botaoAlterar)
        .click(tipoRelacionamentoPage.botaoLimpar)
        .click(tipoRelacionamentoPage.botaoSalvar)

        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");

    await tipoRelacionamentoPage.setPrincipal(false);
    await t
        .click(tipoRelacionamentoPage.botaoSalvar)

        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Descrição é (são) de preenchimento obrigatório.");


    await tipoRelacionamentoPage.setDescricao("JUAREZ");
    await t
        .click(tipoRelacionamentoPage.botaoSalvar)

        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Relacionamento alterado(a) com sucesso.");

    
    await tipoRelacionamentoPage.selecionar("JUAREZ");
    await tipoRelacionamentoPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Tipo de Relacionamento removido(s) com sucesso.");
});
*/