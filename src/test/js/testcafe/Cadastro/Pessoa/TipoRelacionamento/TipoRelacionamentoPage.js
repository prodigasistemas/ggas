import { Selector, t} from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class TipoRelacionamentoPage {

    constructor() {
        this.descricao = Selector("#descricao");
        this.tabelaRelacionamento = Selector("table#relacionamento tbody tr td")

        this.botaoIncluir = Selector("#incluir");
        this.botaoAlterar = Selector("#botaoAlterar");
        this.botaoRemover = Selector("#botaoRemover");
        this.botaoPesquisar = Selector("#botaoPesquisar");

        this.botaoLimpar = Selector("#limparFormulario");
        this.botaoSalvar = Selector("#botaoSalvar");
        this.botaoCancelar = Selector("input[value=Cancelar]");
    }

    async irParaTipoRelacionamento() {
        await t.navigateTo(page.url+"/exibirPesquisaTipoRelacionamento");
    }

    async selecionar(descricao) {
        await t
            .click(this.tabelaRelacionamento.withText(descricao).parent('tr').find('input'));
    }


    async incluir(descricao, principal) {

        await t.click(this.botaoIncluir);

        await this.setDescricao(descricao);
        await this.setPrincipal(principal);

        await t.click(this.botaoSalvar);
    }


    async alterar(descricao, principal) {
        
        await t
            .click(this.botaoAlterar)
            .click(this.botaoLimpar);

        await this.setDescricao(descricao);
        await this.setPrincipal(principal);

        await t.click(this.botaoSalvar);
    }


    async remover() {

        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }

    async setDescricao(descricao) {

        await t.typeText(this.descricao, descricao);
    }

    async setPrincipal(principal) {

        await t.click(Selector("input#indicadorPrincipal[value="+principal+"]"));
    }
}
