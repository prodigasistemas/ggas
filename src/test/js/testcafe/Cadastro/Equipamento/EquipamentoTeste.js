import {Selector} from 'testcafe';
import Page from '../../ggasPage';
import EquipamentoPage from '././EquipamentoPage';

const page = new Page();
const equipamentoPage = new EquipamentoPage();


fixture('Equipamento')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await equipamentoPage.irParaEquipamentoPage();
    });

test('inclusão - equipamente', async t => {

    await equipamentoPage.inclusãoEquipamento(
        'equipamento teste',
        'RESIDENCIAL',
        '123',
        '123',
        '123',
        '123'
    );
    
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Equipamento inserido(a) com sucesso.");
    
        
});
test('alterar - equipamento', async t => {

    await equipamentoPage.alterarEquipamento(
        'EQUIPAMENTO TESTE',
        'equipamento teste',
        'RESIDENCIAL',
        '125',
        '125',
        '125',
        '125'
    );
    
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Equipamento alterado(a) com sucesso.");

});

test('excluir - equipamento', async t => {

    await equipamentoPage.removerEquipamento('EQUIPAMENTO TESTE');
    
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Equipamento removido(s) com sucesso.");

});

test('obrigatoriedade - equipamente', async t => {

    await equipamentoPage.testeObrigatoriedade(
        'equipamento teste',
        'RESIDENCIAL',
        '125',
        '125',
        '125',
        '125'
    );
    
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Equipamento inserido(a) com sucesso.");

});