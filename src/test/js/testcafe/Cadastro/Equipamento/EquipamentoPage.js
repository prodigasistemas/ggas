import { Selector, t} from 'testcafe';
import Page from '../../ggasPage';

const page = new Page();

export default class EquipamentoPage {

    
    constructor() {

        this.botaoIncluir = Selector("#botaoIncluir");
        this.botaoAlterar = Selector("#botaoAlterar");
        this.botaoPesquisar = Selector("#botaoPesquisar");
        this.botaoRemover = Selector("#botaoRemover");
        this.botaoSubmit = Selector("input[value=Salvar]");
        this.botaoLimpar = Selector("input[value=Limpar]")

        this.descricao = Selector("#descricao");

        this.segmento = Selector("#idSegmento");
        this.segmentoOptions = this.segmento.find("option");
        this.potenciaPadrao = Selector("#potenciaPadrao");

        this.potenciaFixaAlta = Selector("#potenciaFixaAlta");
        this.potenciaFixaMedia = Selector("#potenciaFixaMedia");
        this.potenciaFixaBaixa = Selector("#potenciaFixaBaixa");

    }

    async irParaEquipamentoPage(){

        await t.navigateTo(page.url+"/exibirPesquisaEquipamento");
    }

    async removerEquipamento(equipamento){

        await this.setDescricao(equipamento);
        await t.click(this.botaoPesquisar);
        await t.click(Selector("#equipamento tbody td").withExactText(equipamento).parent("tr").find("input"));

        await t.setNativeDialogHandler(() => true).click(this.botaoRemover);

    }

    async inclusãoEquipamento(descricao
        , segmento
        , potenciaPadrao
        , potenciaFixaAlta
        , potenciaFixaMedia
        , potenciaFixaBaixa
    ) {

        await t.click(this.botaoIncluir);

        await this.setDescricao(descricao);
        await this.setSegmento(segmento);
        if(segmento == "RESIDENCIAL"){
            await this.setPotenciaFixaAlta(potenciaFixaAlta);
            await this.setPotenciaFixaMedia(potenciaFixaMedia);
            await this.setPotenciaFixaBaixa(potenciaFixaBaixa);
        } else await this.setPotenciaPadrao(potenciaPadrao);
        

        await t.click(this.botaoSubmit);

    }

    async alterarEquipamento(equipamento
        , descricao
        , segmento
        , potenciaPadrao
        , potenciaFixaAlta
        , potenciaFixaMedia
        , potenciaFixaBaixa
    ) {
        await t.typeText(Selector('#descricao'), equipamento);

        await t.click(this.botaoPesquisar);
         
        await t.click(Selector("#equipamento tbody td").withExactText(equipamento).parent("tr").find("input"));

        await t.click(this.botaoAlterar);

        await t.click(this.botaoLimpar);

        await this.setDescricao(descricao);
        await this.setSegmento(segmento);
        if(segmento == "RESIDENCIAL"){
            await this.setPotenciaFixaAlta(potenciaFixaAlta);
            await this.setPotenciaFixaMedia(potenciaFixaMedia);
            await this.setPotenciaFixaBaixa(potenciaFixaBaixa);
        } else await this.setPotenciaPadrao(potenciaPadrao);
        

        await t.click(this.botaoSubmit);

    }

    async testeObrigatoriedade(descricao
        , segmento
        , potenciaPadrao
        , potenciaFixaAlta
        , potenciaFixaMedia
        , potenciaFixaBaixa
    ) {

        await t.click(this.botaoIncluir);

        await t.click(this.botaoSubmit);

        await this.setDescricao(descricao);

        await t.click(this.botaoSubmit);

        await this.setSegmento(segmento);

        await t.click(this.botaoSubmit);
        if(segmento == "RESIDENCIAL"){
            await t.click(this.botaoSubmit);

            await this.setPotenciaFixaAlta(potenciaFixaAlta);

            await t.click(this.botaoSubmit);

            await this.setPotenciaFixaMedia(potenciaFixaMedia);

            await t.click(this.botaoSubmit);

            await this.setPotenciaFixaBaixa(potenciaFixaBaixa);

            await t.click(this.botaoSubmit);

            await this.setPotenciaFixaAlta(potenciaFixaAlta);
            await this.setPotenciaFixaMedia(potenciaFixaMedia);
            await this.setPotenciaFixaBaixa(potenciaFixaBaixa);
        } else await this.setPotenciaPadrao(potenciaPadrao);
        

        await t.click(this.botaoSubmit);

    }

    async setDescricao(descricao){
        await t.typeText(this.descricao, descricao);
    }
    
    async setSegmento(segmento){
        await t.click(this.segmento);
        await t.click(this.segmentoOptions.withText(segmento));
    }
    
    async setPotenciaPadrao(potenciaPadrao){
        await t.typeText(this.potenciaPadrao, potenciaPadrao);
    }
    
    async setPotenciaFixaAlta(potenciaFixaAlta){
        await t.typeText(this.potenciaFixaAlta, potenciaFixaAlta);
    }
    
    async setPotenciaFixaMedia(potenciaFixaMedia){
        await t.typeText(this.potenciaFixaMedia, potenciaFixaMedia);
    }
    
    async setPotenciaFixaBaixa(potenciaFixaBaixa){
        await t.typeText(this.potenciaFixaBaixa, potenciaFixaBaixa);
    }
    


    
}
