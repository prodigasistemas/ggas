import { Selector, t } from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class MensagensFaturamentoPage {

    constructor() {

        this.botaoPesquisar = Selector("#botaoPesquisar");
        this.botaoIncluir = Selector("#botaoIncluir");
        this.botaoRemover = Selector("#botaoRemover");

        this.tipoMensagem = Selector("#idTipoMensagem");
        this.tipoMensagemOption = this.tipoMensagem.find('option');
        this.dataVigenciaInicial = Selector("#dataInicioVigencia");
        this.dataVigenciaFinal = Selector("#dataFinalVigencia");
        this.segmentos = Selector("#segmentosDisponiveis").find("option");
        this.ramosAtividade = Selector("#ramoAtividadeDisponiveis").find("option");
        this.formaCobranca = Selector("#formaCobrancaDisponiveis").find("option");
        this.dataVencimento = Selector("#dataVencimentoDisponiveis").find("option");
        this.mensagem = Selector("#mensagem");

        this.botaoMoverDireitaSegmentos = Selector("#botaoMoverDireitaConteinerSegmentos");
        this.botaoMoverDireitaRamoAtividade = Selector("#ramoAtividadeDisponiveis").parent("fieldset").find("input[value='>>']");
        this.botaoMoverDireitaFormasCobranca = Selector("#botaoMoverDireitaConteinerFormasCobranca");
        this.botaoMoverDireitaDataVencimento = Selector("#conteinerDiaVencimento").find("input[value='>>']");

        this.botaoMoverTodosEsquerdaDataVencimento = this.botaoMoverDireitaDataVencimento.parent("fieldset").find("input:last-child");

        this.botaoSalvar = Selector("#botaoSalvar");
        this.botaoLimpar = Selector("input[value=Limpar]");

        this.botaoAlterar = Selector("#botaoAlterar");
    }

    async irParaMensagensFaturamento() {

        await t.navigateTo(page.url+"/pesquisarMensagemFaturamento");
    }


    async selecionar(mensagem) {
        await t.click(Selector("table#mensagemFaturamento tbody tr td").withText(mensagem).parent("tr").find("input"));
    }


    async incluir(tipoMensagem, dataVigenciaInicial, dataVigenciaFinal, mensagem, segmentos, ramosAtividade, formasCobranca, datasVencimento) {

        await t
            .click(this.botaoIncluir)
            .click(this.botaoLimpar)
            .click(this.tipoMensagem)
            .click(this.tipoMensagemOption.withText(tipoMensagem))
            .typeText(this.dataVigenciaInicial, dataVigenciaInicial)
            .typeText(this.dataVigenciaFinal, dataVigenciaFinal)
            .typeText(this.mensagem, mensagem);

        await page.preencherFormularioImovel();    
        
        for(const segmento of segmentos) {
            await t
                .click(this.segmentos.withText(segmento))
                .click(this.botaoMoverDireitaSegmentos);
        }

        for(const ramoAtividade of ramosAtividade) {
            await t
                .click(this.ramosAtividade.withText(ramoAtividade))
                .click(this.botaoMoverDireitaRamoAtividade);
        }

        for(const formaCobranca of formasCobranca) {
            await t
                .click(this.formaCobranca.withText(formaCobranca))
                .click(this.botaoMoverDireitaFormasCobranca);
        }

        for(const dataVencimento of datasVencimento) {

            await t
                .click(this.dataVencimento.withText(dataVencimento))
                .click(this.botaoMoverDireitaDataVencimento);
        }

        await t.click(this.botaoSalvar);
    }


    async alterar(tipoMensagem, dataVigenciaFinal, mensagem, segmentos, ramosAtividade, formasCobranca, datasVencimento) {

        await t
            .click(this.botaoAlterar)
            .click(this.tipoMensagem)
            .click(this.tipoMensagemOption.withText(tipoMensagem))
            .selectText(this.dataVigenciaFinal)
            .pressKey('delete')
            .typeText(this.dataVigenciaFinal, dataVigenciaFinal)
            .selectText(this.mensagem)
            .pressKey('delete')
            .typeText(this.mensagem, mensagem)
            
            .click(this.segmentos.parent("fieldset").find("fieldset").find("input[value='Todos <<']"));

        for(const segmento of segmentos) {
            await t
                .click(this.segmentos.withText(segmento))
                .click(Selector("#conteinerSegmentos").find("input[value='>>']"));
        }

        for(const ramoAtividade of ramosAtividade) {
            await t
                .click(this.ramosAtividade.withText(ramoAtividade))
                .click(this.botaoMoverDireitaRamoAtividade);
        }

        for(const formaCobranca of formasCobranca) {
            
            if(formaCobranca.exists){
                await t
                    .click(this.formaCobranca.withText(formaCobranca))
                    .click(this.botaoMoverDireitaFormasCobranca);
            }
        }

        if(datasVencimento.length > 0) {
            
            await t.click(this.botaoMoverTodosEsquerdaDataVencimento);
            
            for(const dataVencimento of datasVencimento) {

                await t
                    .click(this.dataVencimento.withText(dataVencimento))
                    .click(this.botaoMoverDireitaDataVencimento);
            }
        }

        await t.click(this.botaoAlterar);
    }


    async remover() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }


    //SET's dos campos de inclusão e alteração

    async setTipoMensagem(tipoMensagem) {
        
        await t
            .click(this.tipoMensagem)
            .click(this.tipoMensagemOption.withText(tipoMensagem));
    }


    async setDataVigenciaInicial(dataVigenciaInicial) {
        
        await t.typeText(this.dataVigenciaInicial, dataVigenciaInicial);
    }


    async setDataVigenciaFinal(dataVigenciaFinal) {
        
        await t.typeText(this.dataVigenciaFinal, dataVigenciaFinal);
    }


    async setMensagem(mensagem) {
        
        await t.typeText(this.mensagem, mensagem);
    }


    async setSegmentos(segmentos) {
        
        for(const segmento of segmentos) {
            
            await t
                .click(this.segmentos.withText(segmento))
                .click(this.botaoMoverDireitaSegmentos);
        }
    }


    async setRamoAtividade(ramosAtividade) {

        for(const ramoAtividade of ramosAtividade) {
            
            await t
                .click(this.ramosAtividade.withText(ramoAtividade))
                .click(this.botaoMoverDireitaRamoAtividade);
        }
    }

    
    async setFormaPagamento(formasCobranca) {

        for(const formaCobranca of formasCobranca) {

            await t
                .click(this.formaCobranca.withText(formaCobranca))
                .click(this.botaoMoverDireitaFormasCobranca);
        }
    }

    async setDataVencimento(datasVencimento) {

        for(const dataVencimento of datasVencimento) {

            await t
                .click(this.dataVencimento.withText(dataVencimento))
                .click(this.botaoMoverDireitaDataVencimento)
        }
    }
}
