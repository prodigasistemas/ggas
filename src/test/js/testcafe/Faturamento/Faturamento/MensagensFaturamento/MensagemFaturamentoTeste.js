import { Selector } from 'testcafe';
import Page from '../../../ggasPage';
import MensagemFaturamentoPage from './MensagemFaturamentoPage';
import Util from '../../../Util';

const page = new Page();
const mensagemFaturamentoPage = new MensagemFaturamentoPage();
const util = new Util();

fixture.skip("Mensagens Faturamento")
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await mensagemFaturamentoPage.irParaMensagensFaturamento();
    });

test("CRUD", async t => {

    await mensagemFaturamentoPage.incluir("COMERCIAL", util.dateNow(), util.dateNow(1), "Mensagem", 
    ["COMERCIAL", "INDUSTRIAL"], 
    ["QUIMICO - Segmento: INDUSTRIAL"], 
    ["Arrecadação",  "Boleto Bancário"],
    ["1", "4", "2", "7", "14", "21"]);

    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Mensagens de Faturamento inserido(a) com sucesso.");

    await mensagemFaturamentoPage.selecionar("MENSAGEM");
    await mensagemFaturamentoPage.alterar("COMERCIAL", util.dateNow(2), "DESCRIÇÃO99", 
    ["COMERCIAL", "INDUSTRIAL"],
    ["QUIMICO"],
    ["Boleto Bancário"],
    ["13", "5", "9", "10", "11", "27"]);
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Mensagens de Faturamento alterado(a) com sucesso.");


    await mensagemFaturamentoPage.selecionar("DESCRIÇÃO99");
    await mensagemFaturamentoPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Mensagens de Faturamento removida(s) com sucesso.");
});

test("Testar obrigatoriedade dos campos", async t => {


    //Tela de Inclusão
    await t
        .click(mensagemFaturamentoPage.botaoIncluir)
        .click(mensagemFaturamentoPage.botaoSalvar)

        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Início de Vigência.");


    await mensagemFaturamentoPage.setDataVencimento(["1", "4", "2", "7", "14", "21"]);
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Início de Vigência.");


    await mensagemFaturamentoPage.setSegmentos(["COMERCIAL", "INDUSTRIAL"]);
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Início de Vigência.");


    await mensagemFaturamentoPage.setRamoAtividade(["QUIMICO"]);
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Início de Vigência.");
        
        
    await mensagemFaturamentoPage.setFormaPagamento(["Arrecadação",  "Boleto Bancário"]);
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Início de Vigência.");


    await mensagemFaturamentoPage.setDataVigenciaInicial(util.dateNow());
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Fim de Vigência.");


    await mensagemFaturamentoPage.setDataVigenciaFinal(util.dateNow(1));
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Tipo de Mensagem , Mensagem é (são) de preenchimento obrigatório.");

    
    await mensagemFaturamentoPage.setTipoMensagem("COMERCIAL");
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Mensagem é (são) de preenchimento obrigatório.");
    
    
    await mensagemFaturamentoPage.setMensagem("mensagem");
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Mensagens de Faturamento inserido(a) com sucesso.");

    //Tela de Alteração


    //Remoção
    await mensagemFaturamentoPage.selecionar("MENSAGEM");
    await mensagemFaturamentoPage.remover();
    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Mensagens de Faturamento removida(s) com sucesso.");
    
});

test("TESTE", async t => {
    await mensagemFaturamentoPage.selecionar("MENSAGEM");
    await t
        .click(mensagemFaturamentoPage.botaoAlterar)
        .click(mensagemFaturamentoPage.botaoLimpar)
        .click(mensagemFaturamentoPage.botaoAlterar)

        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Início de Vigência.");


    await mensagemFaturamentoPage.setDataVencimento(["1", "4", "2", "7", "14", "21"]);
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Início de Vigência.");


    await mensagemFaturamentoPage.setSegmentos(["COMERCIAL", "INDUSTRIAL"]);
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Início de Vigência.");


    await mensagemFaturamentoPage.setRamoAtividade(["QUIMICO"]);
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Início de Vigência.");
        
        
    await mensagemFaturamentoPage.setFormaPagamento(["Arrecadação",  "Boleto Bancário"]);
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Início de Vigência.");


    await mensagemFaturamentoPage.setDataVigenciaInicial(util.dateNow());
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: Formato inválido para o(s) campo(s) Fim de Vigência.");


    await mensagemFaturamentoPage.setDataVigenciaFinal(util.dateNow(1));
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Tipo de Mensagem , Mensagem é (são) de preenchimento obrigatório.");

    
    await mensagemFaturamentoPage.setTipoMensagem("COMERCIAL");
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Mensagem é (são) de preenchimento obrigatório.");
    
    
    await mensagemFaturamentoPage.setMensagem("mensagem");
    await t
        .click(mensagemFaturamentoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Mensagens de Faturamento inserido(a) com sucesso.");
});
