import {Selector} from 'testcafe';
import Page from '../../../ggasPage';
import Util from '../../../Util.js';
import NotasDebitoCreditoPage from './NotasDebitoCreditoPage';

const page = new Page();
const notasDebitoCreditoPage = new NotasDebitoCreditoPage();
const util = new Util();

/*
fixture('Devem obter sucesso')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await notasDebitoCreditoPage.irParaNotasCreditoDebito();
    });

test('Inclusão/Prorrogação/Visuzalização/Cancelamento', async t => {

    await notasDebitoCreditoPage.pesquisarPessoa();
    await notasDebitoCreditoPage.incluir("TESTE - TESTE", util.dateNow(1), "Multa por atraso", "5", "25,0");

    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Nota de Crédito / Débito inserido(a) com sucesso.");

    await notasDebitoCreditoPage.selecionarDocumento("NORMAL")
    await notasDebitoCreditoPage.prorrogar(util.dateNow(2));

    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Nota(s) Crédito/Débito Prorrogada(s) com Sucesso.");
    
    await notasDebitoCreditoPage.vizualizarDocumento("NORMAL");
    await t.click(notasDebitoCreditoPage.botaoVoltar);

    await notasDebitoCreditoPage.selecionarDocumento("NORMAL")
    await notasDebitoCreditoPage.cancelar("FATURAMENTO INDEVIDO", "TESTE - TESTE");

    await t
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Nota de Crédito / Débito cancelada(s) com sucesso.");
});

test('Testando campos da tela', async t => {

    await notasDebitoCreditoPage.pesquisarPessoa();

    await notasDebitoCreditoPage.prencherDataValidade(util.dateNow(1), util.dateNow());
    await t.click(notasDebitoCreditoPage.botaoPesquisar);
    await t
        .expect(Selector(".notification").innerText)
        .eql("Erro: A data do Período de Emissão Inicial não pode ser maior que a data do Período de Emissão Final.");

    await t.click(notasDebitoCreditoPage.botaoIncluir);
    await t
        .click(notasDebitoCreditoPage.botaoIncluir)

        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Tipo de Nota, Valor, Rubrica, Quantidade é (são) de preenchimento obrigatório.");

    await notasDebitoCreditoPage.setPontoConsumo("TESTE - TESTE");
    await t
        .click(notasDebitoCreditoPage.botaoIncluir)

        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Tipo de Nota, Valor, Rubrica, Quantidade é (são) de preenchimento obrigatório.");


    //setIndicador : true para Débito e false para Crédito
    await notasDebitoCreditoPage.setIndicador(false);
    await t
        .click(notasDebitoCreditoPage.botaoIncluir)

        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Valor, Rubrica, Quantidade é (são) de preenchimento obrigatório.");

    
    await notasDebitoCreditoPage.setIndicador(true);
    await t
        .click(notasDebitoCreditoPage.botaoIncluir)

        .expect(Selector(".notification").innerText)
		.eql("Erro: O(s) campo(s) Vencimento, Valor, Rubrica, Quantidade é (são) de preenchimento obrigatório.");
	
	
	await notasDebitoCreditoPage.setDataVencimento(util.dateNow(-1));
	await t
        .click(notasDebitoCreditoPage.botaoIncluir)

        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Valor, Rubrica, Quantidade é (são) de preenchimento obrigatório.");

    await notasDebitoCreditoPage.setRubrica("Multa por atraso");
    await t
        .click(notasDebitoCreditoPage.botaoIncluir)

        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Valor, Quantidade é (são) de preenchimento obrigatório.");
    

    await notasDebitoCreditoPage.setQuantidade("5");
	await t
		.setNativeDialogHandler(() => true)
        .click(notasDebitoCreditoPage.botaoIncluir)

        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Valor é (são) de preenchimento obrigatório.");


    await notasDebitoCreditoPage.setValorUnitario("25,0");
	await t
		.setNativeDialogHandler(() => true)
        .click(notasDebitoCreditoPage.botaoIncluir)

        .expect(Selector(".notification").innerText)
        .eql("Erro: A data de vencimento não pode ser menor que a data atual.")

        .click(notasDebitoCreditoPage.botaoLimpar)
        .click(notasDebitoCreditoPage.botaoIncluir)
        
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Tipo de Nota, Valor, Rubrica, Quantidade é (são) de preenchimento obrigatório.")
        
        .click(notasDebitoCreditoPage.botaoCancelarVoltar);

    //CAMPOS DA TELA DE PRORROGAÇÃO
    await notasDebitoCreditoPage.pesquisarPessoa();
    await notasDebitoCreditoPage.selecionarDocumento("CANCELADA")
	await t
		.click(notasDebitoCreditoPage.botaoProrrogar)
		.click(notasDebitoCreditoPage.botaoSalvar)

        .expect(Selector(".notification").innerText)
		.eql("Erro: Dado(s) inválido(s) para o(s) campo(s) Data Prorrogação.");
	
	await notasDebitoCreditoPage.setDataProrrogacao(util.dateNow(-1));

	await t
		.click(notasDebitoCreditoPage.botaoSalvar)

		.expect(Selector(".notification").innerText)
		.eql("Erro: Nova Data Vencimento Deve ser Maior que a Atual.");
});

test('Cancelar nota cancelada', async t => {

    await notasDebitoCreditoPage.pesquisarPessoa();
    await notasDebitoCreditoPage.selecionarDocumento("CANCELADA");
    await t
        .click(notasDebitoCreditoPage.botaoCancelarNota)

        .expect(Selector(".notification").innerText)
        .eql("Erro: Apenas notas com a situação do documento \"Normal\" podem ser canceladas.");
});
*/
