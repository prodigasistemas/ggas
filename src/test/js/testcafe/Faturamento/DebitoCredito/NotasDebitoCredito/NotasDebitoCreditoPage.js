import { Selector, t, ClientFunction } from 'testcafe';
import Page from '../../../ggasPage';

const page = new Page();

export default class NotasDebitoCreditoPage {
    
    constructor() {
    
        this.dataVencimentoInicial = Selector('#dataVencimentoInicial');
        this.dataVencimentoFinal = Selector('#dataVencimentoFinal');
        this.indicadorDebito = Selector('input[value=debito]');
        this.indicadorCredito = Selector('input[value=credito]');
        this.tabelaNotaCreditoDebito = Selector("#notaDebitoCredito").find('tbody').find('td');

        this.dataProrrogacao = Selector('#dataProrrogacao');
        this.observacao = Selector("#observacoesNota");

        this.motivoCancelamento = Selector('#idMotivoCancelamento');
        this.motivoCancelamentoOptions = this.motivoCancelamento.find("option");
        this.notasSelecionada = Selector("input[name=chavesNotasSelecionadas]");

        this.pontoConsumo = Selector("table#pontoConsumo").find('tbody').find('td');
        this.dataVencimento = Selector('#dataVencimento');
        this.rubrica = Selector('#idRubrica');
        this.rubricaOptions = this.rubrica.find('option');
        this.quantidade = Selector('#quantidade');
        this.valorUnitario = Selector('#valorTotal');

        this.botaoPesquisar = Selector('#botaoPesquisar');
        this.botaoIncluir = Selector('#botaoIncluir');
        this.botaoProrrogar = Selector("#prorrogarNota");
        this.botaoCancelarNota = Selector('#cancelarNota');
        this.botaoCancelar = Selector('#buttonCancelar');
        this.botaoCancelarVoltar = Selector("input[value=Cancelar]");
        this.botaoSalvar = Selector("input[value=Salvar]");
        this.botaoLimpar = Selector("input[value=Limpar]");
        this.botaoVoltar = Selector("input[value=Voltar]");
    }

    async irParaNotasCreditoDebito() {
        await t.navigateTo(page.url+"/exibirPesquisaNotaDebitoCredito");
    }

    async pesquisarPessoa(){
        
        const liberarBotao = ClientFunction(() => {
            $('#botaoPesquisar').prop("disabled", false);
            $('#botaoIncluir').prop("disabled", false);
        });

        await liberarBotao();
        await page.preencherFormularioPessoa();
    }

    async pesquisarImovel(){
        const liberarBotao = ClientFunction(() => {
            $('#botaoPesquisar').prop("disabled", false);
        });

        await liberarBotao();
        await page.preencherFormularioImovel();
        await t.click(this.botaoPesquisar); 
    }

    async selecionarDocumento(situacao) {
        await t
            .click(this.botaoPesquisar)
            .click(this.tabelaNotaCreditoDebito.withText(situacao).parent('tr').find('input'));
    }

    async vizualizarDocumento(situacao) {
        await t
            .click(this.tabelaNotaCreditoDebito.withText(situacao));
    }

    async incluir(pontoConsumo, dataVencimento, rubrica, quantidade, valorUnitario) {
        await t.click(this.botaoIncluir);

        await this.setPontoConsumo(pontoConsumo);
        await t.click(this.indicadorDebito);
        await this.setDataVencimento(dataVencimento);
        await this.setRubrica(rubrica);
        await this.setQuantidade(quantidade);
        await this.setValorUnitario(valorUnitario);

        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoIncluir);
    }

    async prorrogar(dataProrrogacao, observacao = ".") {

        await t.click(this.botaoProrrogar);

        await this.setDataProrrogacao(dataProrrogacao);
        await this.setObservacao(observacao);

        await t.click(this.botaoSalvar);
    }

    async cancelar(motivoCancelamento) {

        await t
            .click(this.botaoCancelarNota)
            .click(this.motivoCancelamento)
            .click(this.motivoCancelamentoOptions.withText(motivoCancelamento))
            .click(this.notasSelecionada)
            .click(this.botaoCancelar);
    }

    async setObservacao(observacao) {
        await t
            .typeText(this.observacao, observacao);
    }

    async setPontoConsumo(pontoConsumo){
        await t.click(this.pontoConsumo.withText(pontoConsumo).parent('tr').find('input'))
    }

    async setIndicador(indicador){
        if(indicador){
            await t.click(this.indicadorDebito);
        } else {
            await t.click(this.indicadorCredito);
        }
    }

    async setDataVencimento(dataVencimento) {
        await t
            .selectText(this.dataVencimento)
            .pressKey('delete')
            .typeText(this.dataVencimento, dataVencimento);
    }

    async setDataProrrogacao(dataProrrogacao) {
        await t
            .selectText(this.dataProrrogacao)
            .pressKey('delete')
            .typeText(this.dataProrrogacao, dataProrrogacao);
    }

    async setRubrica(rubrica) {
        await t
            .click(this.rubrica)
            .click(this.rubricaOptions.withText(rubrica))
    }

    async setQuantidade(quantidade) {
        await t
            .selectText(this.quantidade)
            .pressKey('delete')
            .typeText(this.quantidade, quantidade);
    }

    async setValorUnitario(valorUnitario) {
        await t 
            .selectText(this.valorUnitario)
            .pressKey('delete')
            .typeText(this.valorUnitario, valorUnitario);
    }

    async prencherDataValidade(dataVencimentoInicial, dataVencimentoFinal){
        await t
            .selectText(this.dataVencimentoInicial)
            .pressKey('delete')
            .typeText(this.dataVencimentoInicial, dataVencimentoInicial)
            .selectText(this.dataVencimentoFinal)
            .pressKey('delete')
            .typeText(this.dataVencimentoFinal, dataVencimentoFinal);
    }
}
