import { Selector, t} from 'testcafe';
import Page from '../../ggasPage';
import BancoPage from './BancoPage';

const page = new Page()
const bancoPage = new BancoPage();

/*
fixture('Banco')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await bancoPage.irParaBanco();
    });


test('CRUD', async t => {

    await bancoPage.incluir("BANCO DO CABO", "341", "BC", "true");
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Banco inserido(a) com sucesso.');

    await bancoPage.selecionar('BANCO DO CABO');
    await bancoPage.alterar("CABO BANK", "707", "CB", "true");
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Banco alterado(a) com sucesso.');

    await bancoPage.selecionar('CABO BANK');
    await bancoPage.remover();
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Banco(s) removido(s) com sucesso.');
});

test('Teste de Obrigatoriedade', async t => {

    //Tela de inclusão
    await t
        .click(bancoPage.botaoIncluir)
        .click(bancoPage.botaoIncluir)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Nome do Banco, Código do Banco é (são) de preenchimento obrigatório.");

    await bancoPage.setIndicador("true");
    await t
        .click(bancoPage.botaoIncluir)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Nome do Banco, Código do Banco é (são) de preenchimento obrigatório.");
    
    await bancoPage.setNomeAbreviado("BC");
    await t
        .click(bancoPage.botaoIncluir)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Nome do Banco, Código do Banco é (são) de preenchimento obrigatório.");

    await bancoPage.setNome("BANCO DO CABO");
    await t
        .click(bancoPage.botaoIncluir)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Código do Banco é (são) de preenchimento obrigatório.");

    await bancoPage.setCodigoBanco("341");
    await t
        .click(bancoPage.botaoIncluir)
        .expect(Selector(".notification").innerText)
        .eql("Sucesso: Banco inserido(a) com sucesso.");

    //Tela de alteração
    await bancoPage.selecionar("341");
    await t
        .click(bancoPage.botaoAlterar)
        .click(bancoPage.botaoLimpar)
        .click(bancoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Nome do Banco, Código do Banco é (são) de preenchimento obrigatório.");

    await bancoPage.setIndicador("true");
    await t
        .click(bancoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Nome do Banco, Código do Banco é (são) de preenchimento obrigatório.");

    await bancoPage.setNomeAbreviado("CB");
    await t
        .click(bancoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Nome do Banco, Código do Banco é (são) de preenchimento obrigatório.");

    await bancoPage.setNome("CABO BANK");
    await t
        .click(bancoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql("Erro: O(s) campo(s) Código do Banco é (são) de preenchimento obrigatório.");

    await bancoPage.setCodigoBanco("707");
    await t
        .click(bancoPage.botaoSalvar)
        .expect(Selector(".notification").innerText)
        .eql('Sucesso: Banco alterado(a) com sucesso.');

    //Remoção
    await bancoPage.selecionar('CABO BANK');
    await bancoPage.remover();
    await t
        .expect(Selector('.notification').innerText)
        .eql('Sucesso: Banco(s) removido(s) com sucesso.');
});
*/
