import { Selector, t} from 'testcafe';
import Page from '../../ggasPage';

const page = new Page();

export default class BancoPage {

    constructor() {

        this.botaoIncluir = Selector('#buttonIncluir');
        this.botaoAlterar = Selector('input[value=Alterar]');
        this.botaoRemover = Selector('input[value=Remover]');
        this.botaoSalvar = Selector('input[value=Salvar]');
        this.botaoLimpar = Selector('input[value=Limpar]');

        this.nome = Selector("#nome");
        this.codigoBanco = Selector("#codigoBanco");
        this.nomeAbreviado = Selector("#nomeAbreviado");
    }

    async irParaBanco(){
        await t
            .navigateTo(page.url+"/exibirPesquisarBanco");
    }

    async selecionar(opcao) {
        await t
            .click(Selector("table#banco tbody tr td").withText(opcao).parent("tr").find("input"));
    }

    async incluir(nome, codigoBanco, nomeAbreviado, indicador) {
        await t.click(this.botaoIncluir);

        await this.setNome(nome);
        await this.setCodigoBanco(codigoBanco);
        await this.setNomeAbreviado(nomeAbreviado);
        await this.setIndicador(indicador);

        await t.click(this.botaoIncluir);
    }

    async alterar(nome, codigoBanco, nomeAbreviado, indicador) {
        await t
            .click(this.botaoAlterar)
            .click(this.botaoLimpar);

        await this.setNome(nome);
        await this.setCodigoBanco(codigoBanco);
        await this.setNomeAbreviado(nomeAbreviado);
        await this.setIndicador(indicador);

        await t.click(this.botaoSalvar);
    }

    async remover() {
        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }

    async setNome(nome) {
        await t
            .typeText(this.nome, nome);
    }

    async setCodigoBanco(codigoBanco) {
        await t
            .typeText(this.codigoBanco, codigoBanco);
    }

    async setNomeAbreviado(nomeAbreviado) {
        await t
            .typeText(this.nomeAbreviado, nomeAbreviado);
    }

    async setIndicador(indicador) {
        await t
            .click(Selector("#habilitado[value="+indicador+"]"));
    }
}
