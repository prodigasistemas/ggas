import { Selector, t } from 'testcafe';
import Page from '../../ggasPage';

const page = new Page();

export default class ArrecadadorPage {
    
    constructor() {
        
        this.selectBanco = Selector('#banco');
        this.selectBancoOption = this.selectBanco.find('option');
        this.banco = Selector('a');
        this.codigoAgente = Selector('#codigoAgente');
        this.indicadorDeUso = Selector('input#habilitado');

        this.botaoPesquisar = Selector("#botaoPesquisar");
        this.botaoIncluir = Selector("#botaoIncluir");
        this.botaoIncluirSalvar = Selector("#ButtonIncluir");
        this.botaoAlterar = Selector('input[value=Alterar]');
        this.botaoSalvar = Selector('#buttonSalvar');
        this.botaoRemover = Selector("#buttonRemover");
        this.botaoLimpar = Selector("input[value=Limpar]");
    }

    async irParaArrecadador() {
        await t.navigateTo(page.url+'/pesquisarArrecadador');
    }

    async selecionar(opcao) {

        await t
            .click(Selector('#arrecadador tbody tr td').find('a').withText(opcao).parent('tr').find('input'));
    }


    async incluir(nomeBanco, codigoAgente) {

        await t.click(this.botaoIncluir);

        await page.preencherFormularioPessoa();
        await this.setBanco(nomeBanco);
        await this.setCodigoAgente(codigoAgente);

        await t.click(this.botaoIncluirSalvar);
    }


    async alterar(nomeBanco, codigoAgente) {

        await t
            .click(this.botaoAlterar)
            .click(this.botaoLimpar);

            await page.preencherFormularioPessoa();
            await this.setBanco(nomeBanco);
            await this.setCodigoAgente(codigoAgente);

            await t.click(this.botaoSalvar);
    }

    async remover() {

        await t
            .setNativeDialogHandler(() => true)
            .click(this.botaoRemover);
    }

    async setBanco(nomeBanco){
        await t
            .click(this.selectBanco)
            .click(this.selectBancoOption.withText(nomeBanco));
    }

    async setCodigoAgente(codigoAgente) {
        await t.typeText(this.codigoAgente, codigoAgente);
    }
}
