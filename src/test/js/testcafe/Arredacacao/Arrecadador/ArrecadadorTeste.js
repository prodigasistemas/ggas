import {Selector} from 'testcafe';
import Page from '../../ggasPage';
import ArrecadadorPage from './ArrecadadorPage';

const page = new Page();
const arrecadadorPage = new ArrecadadorPage();

fixture('Arrecadador')
    .page(page.url)
    .beforeEach(async t => {
        await t.maximizeWindow();
        await page.fazerLogin('admin', 'admin');
        await arrecadadorPage.irParaArrecadador();
    });

//INCLUSÃO
test('CRUD', async t => {

    
    await arrecadadorPage.incluir("BANCO TESTE", "134");
    await t
        .expect(Selector('.notification').find('p').innerText)
        .eql("Sucesso: Arrecadador inserido(a) com sucesso.");


    await arrecadadorPage.selecionar("134");
    await arrecadadorPage.alterar("BANCO BRADESCO SA", "789");
    await t
        .expect(Selector('.notification').find('p').innerText)
        .eql("Sucesso: Arrecadador alterado(a) com sucesso.");


    await arrecadadorPage.selecionar("789");
    await arrecadadorPage.remover();
    await t
        .expect(Selector('.notification').find('p').innerText)
        .eql("Sucesso: Arrecadador(es) removido(s) com sucesso.");
});

test('Testar Obrigatoriedade', async t => {

    //Tela de Inclusão
    await t
        .click(arrecadadorPage.botaoIncluir)
        .click(arrecadadorPage.botaoIncluirSalvar)
        
        .expect(Selector('.notification').find('p').innerText)
        .eql("Erro: O(s) campo(s) Código Agente, Banco é (são) de preenchimento obrigatório.");


    await arrecadadorPage.setBanco("BANCO TESTE");
    await t
        .click(arrecadadorPage.botaoIncluirSalvar)
        .expect(Selector('.notification').find('p').innerText)
        .eql("Erro: O(s) campo(s) Código Agente é (são) de preenchimento obrigatório.");


    await arrecadadorPage.setCodigoAgente("999");
    await t
        .click(arrecadadorPage.botaoIncluirSalvar)
        .expect(Selector('.notification').find('p').innerText)
        .eql("Erro: O(s) campo(s) Banco é (são) de preenchimento obrigatório.");

    
    await t.click(arrecadadorPage.botaoLimpar);
    await arrecadadorPage.setBanco("BANCO TESTE");
    await arrecadadorPage.setCodigoAgente("999");
    await t
        .click(arrecadadorPage.botaoIncluirSalvar)
        .expect(Selector('.notification').find('p').innerText)
        .eql("Sucesso: Arrecadador inserido(a) com sucesso.");


    //Tela de Alteração
    await arrecadadorPage.selecionar("999");
    await t
        .click(arrecadadorPage.botaoAlterar)
        .click(arrecadadorPage.botaoLimpar)
        .click(arrecadadorPage.botaoSalvar)
    
        .expect(Selector('.notification').find('p').innerText)
        .eql("Erro: O(s) campo(s) Código Agente, Banco é (são) de preenchimento obrigatório.");

    
    await arrecadadorPage.setBanco("BANCO BRADESCO SA");
    await t
    .click(arrecadadorPage.botaoSalvar)
        .expect(Selector('.notification').find('p').innerText)
        .eql("Erro: O(s) campo(s) Código Agente é (são) de preenchimento obrigatório.");


    await t.click(arrecadadorPage.botaoLimpar);
    await arrecadadorPage.setCodigoAgente("101");
    await t
        .click(arrecadadorPage.botaoSalvar)
        .expect(Selector('.notification').find('p').innerText)
        .eql("Erro: O(s) campo(s) Banco é (são) de preenchimento obrigatório.");


    await t.click(arrecadadorPage.botaoLimpar);
    await arrecadadorPage.setBanco("BANCO BRADESCO SA");    
    await arrecadadorPage.setCodigoAgente("101");
    await t
        .click(arrecadadorPage.botaoSalvar)
        .expect(Selector('.notification').find('p').innerText)
        .eql("Sucesso: Arrecadador alterado(a) com sucesso.");
    
    //Remover
    await arrecadadorPage.selecionar("101");
    await arrecadadorPage.remover();
    await t
        .expect(Selector('.notification').find('p').innerText)
        .eql("Sucesso: Arrecadador(es) removido(s) com sucesso.");
});
