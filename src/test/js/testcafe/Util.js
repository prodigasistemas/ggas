export default class Util {

    dateNow(acrescimoAno=0, acrescimoMes=0, acrescimoDia=0) {

        var d = new Date();

        d.setDate(d.getDate()+acrescimoDia);
        var dia = d.getDate();

        d.setMonth(d.getMonth()+acrescimoMes);
        var mes = d.getMonth() + 1;

        d.setFullYear(d.getFullYear()+acrescimoAno);
        var ano = d.getFullYear();

        if(dia < 10) {
            dia = "0"+dia;
        }
    
        if(mes < 10) {
            mes = "0"+mes;
        }
    
        return dia+"/"+mes+"/"+ano;
    }

    dia(acrescimo=0) {

        var data = new Date();
        data.setDate(data.getDate() + acrescimo);

        var dia = data.getDate();

        if(dia < 10) {
            return "0" + dia;
        }
        
        return String(dia);
    }

    mes(acrescimo=0) {

        var data = new Date();
        data.setMonth(data.getMonth() + acrescimo + 1);

        var mes = data.getMonth();

        if(mes < 10) {
            return "0" + mes;
        }

        return String(mes);
    }

    ano(acrescimo=0) {
        return String(new Date().getFullYear() + acrescimo);
    }

    listaAnos(ano) { 
        
        let listaAnos = [];

        for(let i = 0; i < (ano - 2000); i++) {
            listaAnos[i] = String(2000+i);
        }

        return listaAnos;
    }
}
