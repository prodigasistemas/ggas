import { Selector, t, ClientFunction } from 'testcafe';
import config from './config';

export default class Page {
    
    
    constructor() {
        this.url = config.baseUrl;
        this.login = Selector("#login");
        this.senha = Selector("#senha");
        this.botaoLogon = Selector('#button3');
    }

    async fazerLogin(login, senha) {
        await t
            .typeText(this.login, login)
            .typeText(this.senha, senha)
            .click(this.botaoLogon);
    }


    async preencherFormularioPessoa(idCliente = "7208") {
        
        const preencheForm = ClientFunction(idCliente => {
            $('#idCliente').val(idCliente);
        });

        await preencheForm(idCliente);
    }


    async preencherFormularioImovel(idImovel = "11163") {

        const preencheForm = ClientFunction(idImovel => {
            $('#idImovel').val(idImovel);
        });

        await preencheForm(idImovel);
    }

    async liberarBotao(botao){
        
        const liberarBotao = ClientFunction(botao => {
            $(botao).prop("disabled", false);
            return true;
        });

        await liberarBotao(botao);
    }

    async preencherCampo(botao, valor){

        const preencherCampo = ClientFunction((botao, valor) => {
            $(botao).val(valor);
            return true;
        });

        await preencherCampo(botao, valor);
    }

    async limparCampo(campo) {

        await t
            .selectText(campo)
            .pressKey('delete');
    }

    async preencherInput(selector, value) {
        if(value != null) {
            await t.selectText(selector).typeText(selector, value);
        }
    }

    async selecionarOpcao(selector, value) {
        if(value != null) {
            await t
                .click(selector)
                .click(selector.find('option').withText(value));
        }
    }

    async clicarBotao(selector) {
        await t
            .setNativeDialogHandler(() => true)
            .click(selector);
    }

    async clicarElemento(selector) {
        await t.click(selector);
    }

    async clicarRadioCheckBox(selector) {
        await t.click(selector.nextSibling(0));
    }

    async marcarCheckbox(selector) {
        if(!await selector.hasAttribute('checked')) {
            await t.click(selector.nextSibling(0));
        }
    }

    async desmarcarCheckbox(selector) {
        if(await selector.hasAttribute('checked')) {
            await t.click(selector.nextSibling(0));
        }
    }

    /**
     * Verifica se o input tem certo valor e está habilitado/desabilitado para edição.
     *
     * @param {*} selector Selector referente ao input.
     * @param {string} value Valor que o input deve ter.
     * @param {boolean} enabled Identifica se o input está habilitado para edição.
     */
    async checarInput(selector, value, enabled) {

        if(enabled) {

            if(value != null) {
                this.checarValorPreenchido(selector, value);
            }

            this.checarInputHabilitado(selector);
        } else {

            if(value != null) {
                this.checarValorPreenchido(selector, value);
            }

            this.checarInputDesabilitado(selector);
        }

    }

    /**
     * Verifica se o selectbox tem certo valor e está habilitado/desabilitado para edição.
     *
     * @param {*} selector Selector referente ao selectbox.
     * @param {string} value Valor da opção selecionada no selectbox.
     * @param {boolean} enabled Identifica se o selectbox está habilitado para edição.
     */
    async checarSelect(selector, value, enabled) {

        if(enabled) {

            if(value != null) {
                this.checarValorSelecionado(selector, value);
            }

            this.checarInputHabilitado(selector);
        } else {

            if(value != null) {
                this.checarValorSelecionado(selector, value);
            }

            this.checarInputDesabilitado(selector);
        }

    }

    async checarSelectMultiplo(selector, value, enabled) {
        if(enabled) {

            if(value != null) {
                this.checarValorSelectMultiplo(selector, value);
            }

            this.checarInputHabilitado(selector);
        } else {

            if(value != null) {
                this.checarValorSelectMultiplo(selector, value);
            }

            this.checarInputDesabilitado(selector);
        }
    }

    async checarInputDesabilitado(selector) {
        await t.expect(selector.hasAttribute('disabled')).ok();
    }

    async checarInputHabilitado(selector) {
        await t.expect(selector.hasAttribute('disabled')).notOk();
    }

    async checarValorSelecionado(selector, valor) {
        await t.expect(selector.child().withText(valor).hasAttribute('selected')).ok();
    }

    async checarValorPreenchido(selector, valor) {
        await t.expect(selector.value).eql(valor);
    }

    async checarRadioCheckboxMarcado(selector, enabled) {

        if(enabled) {
            await t
                .expect(selector.hasAttribute('checked')).ok()
                .expect(selector.hasAttribute('disabled')).notOk();
        } else {
            await t
                .expect(selector.hasAttribute('checked')).ok()
                .expect(selector.hasAttribute('disabled')).ok();
        }
    }

    async checarValorSelectMultiplo(selector, valor) {
        await t.expect(selector.child().withText(valor).exists).ok();
    }

    async espera(tempo) {
        await t.wait(tempo);
    }

    async checarPropriedade(selector, propriedade, valor, enabled) {

        if(valor != null) {
            
            await t.expect(selector.withAttribute(propriedade, valor).exists).ok();
            if(enabled) {
                await t.expect(selector.hasAttribute('disabled')).notOk();
            } else {
                await t.expect(selector.hasAttribute('disabled')).ok();
            }
        }
    }

    async aguardarCarregarPagina(titulo, seletor = Selector(".tituloInterno")) {

        await t.expect(seletor.innerText).eql(titulo);
        
    };
}
