
package br.com.ggas.contrato;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.lang.SerializationUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.chamado.dominio.AssociacaoVO;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ContratoPenalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidade;
import br.com.ggas.contrato.contrato.ContratoPontoConsumoModalidadeQDC;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.SituacaoContrato;
import br.com.ggas.contrato.contrato.impl.ControladorContratoImpl;
import br.com.ggas.contrato.programacao.SolicitacaoConsumoPontoConsumo;
import br.com.ggas.faturamento.apuracaopenalidade.ControladorApuracaoPenalidade;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Util;
import br.com.ggas.web.contrato.contrato.ContratoAction;
import br.com.ggas.web.medicao.associacao.AssociacaoAction;

public class TesteContrato extends GGASTestCase {

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_CONTRATO.sql";
	
	@Autowired
	@Qualifier("controladorContrato")
	private ControladorContrato controladorContrato;

	@Autowired
	@Qualifier("controladorPontoConsumo")
	private ControladorPontoConsumo controladorPontoConsumo;	

	@Autowired
	@Qualifier("controladorApuracaoPenalidade")
	private ControladorApuracaoPenalidade controladorApuracaoPenalidade;

	private final int INTERVALO_VIGENCIA_PENALIDADE = 5;

	private final int VALOR_VOLUME_QDC = 120000000;

	private final int VALOR_VOLUME_QDP = 1500000;

	private final int ESCALA_VALOR_QDC = 4;

	private final int ESCALA_VALOR_QDP = 2;
	
	@Before
	public void setup() throws FileNotFoundException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}
	
	@Test
	@Transactional
	public void selecionarContratoPenalidadePeriodoEmVigencia() {

		List<ContratoPenalidade> listaContratoPenalidade = Arrays.asList(criarContratoPenalidade());

		Date dataAtual = Util.getDataCorrente(Boolean.TRUE);

		Date dataInicio = Util.adicionarDiasData(dataAtual, 1);
		Date dataFim = Util.adicionarDiasData(dataAtual, INTERVALO_VIGENCIA_PENALIDADE - 1);

		ContratoPenalidade contratoPenalidade =
				controladorContrato.selecionarContratoPenalidadePorPeriodoEmVigencia(dataInicio, dataFim, listaContratoPenalidade);
		assertNotNull(contratoPenalidade);
	}

	@Test
	@Transactional
	public void selecionarContratoPenalidadePeriodoForaDeVigencia() {

		List<ContratoPenalidade> listaContratoPenalidade = Arrays.asList(criarContratoPenalidade());

		Date dataAtual = Util.getDataCorrente(Boolean.TRUE);

		Date dataInicio = Util.removerDiasData(dataAtual, 1);
		Date dataFim = Util.adicionarDiasData(dataAtual, INTERVALO_VIGENCIA_PENALIDADE + 1);

		ContratoPenalidade contratoPenalidade =
				controladorContrato.selecionarContratoPenalidadePorPeriodoEmVigencia(dataInicio, dataFim, listaContratoPenalidade);
		assertNull(contratoPenalidade);
	}

	@Test
	@Transactional
	public void calculoQDCMediaDiaria() throws NegocioException {
		Date hoje = DataUtil.gerarDataHmsZerados(new Date());

		BigDecimal mediaEsperada = BigDecimal.valueOf(VALOR_VOLUME_QDC, ESCALA_VALOR_QDC);

		BigDecimal valorDiarioQDC =
				controladorContrato.calcularQDCMedia(Arrays.asList(criarContratoPontoConsumoModalidade(1L)), hoje, hoje);

		assertEquals(mediaEsperada, valorDiarioQDC);
	}

	@Test
	@Transactional
	public void calculoQDCMediaNula() throws NegocioException {
		Date hoje = DataUtil.gerarDataHmsZerados(new Date());

		BigDecimal mediaEsperada = BigDecimal.valueOf(0);

		ContratoPontoConsumoModalidade contratoPontoConsumoModalidade = criarContratoPontoConsumoModalidade(1L);
		contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC().clear();

		BigDecimal valorDiarioQDC = controladorContrato.calcularQDCMedia(Arrays.asList(contratoPontoConsumoModalidade), hoje, hoje);

		assertEquals(mediaEsperada, valorDiarioQDC);
	}

	@Test
	@Transactional
	public void calculoQDCMediaDiariaComVigenciaMaiorQueSolicitacao() throws NegocioException {

		Date hoje = DataUtil.gerarDataHmsZerados(new Date());

		BigDecimal mediaEsperada = BigDecimal.valueOf(VALOR_VOLUME_QDC, ESCALA_VALOR_QDC);

		ContratoPontoConsumoModalidade contratoPontoConsumoModalidade = criarContratoPontoConsumoModalidade(1L);

		ContratoPontoConsumoModalidadeQDC consumoModalidadeQDC = criarContratoPontoConsumoModalidadeQDC();
		consumoModalidadeQDC.setChavePrimaria(2l);
		consumoModalidadeQDC.setDataVigencia(DataUtil.incrementarDia(hoje, 1));

		contratoPontoConsumoModalidade.getListaContratoPontoConsumoModalidadeQDC().add(consumoModalidadeQDC);

		BigDecimal valorDiarioQDC = controladorContrato.calcularQDCMedia(Arrays.asList(contratoPontoConsumoModalidade), hoje, hoje);

		assertEquals(mediaEsperada, valorDiarioQDC);
	}

	@Test
	@Transactional
	public void calculoQDCMediaEmPeriodo() throws NegocioException {
		Date primeiroDiaPeriodo = DataUtil.gerarDataHmsZerados(new Date());
		Date ultimoDiaPeriodo = DataUtil.incrementarDia(primeiroDiaPeriodo, 3);

		BigDecimal mediaEsperada = BigDecimal.valueOf(VALOR_VOLUME_QDC, ESCALA_VALOR_QDC);

		BigDecimal mediaPeriodoQDC = controladorContrato.calcularQDCMedia(Arrays.asList(criarContratoPontoConsumoModalidade(1L)),
				primeiroDiaPeriodo, ultimoDiaPeriodo);

		assertEquals(mediaEsperada, mediaPeriodoQDC);
	}

	@Test
	@Transactional
	public void calculoQDCMediaEmPeriodoComMultiplasModalidades() throws NegocioException {
		Date primeiroDiaPeriodo = DataUtil.gerarDataHmsZerados(new Date());
		Date ultimoDiaPeriodo = DataUtil.incrementarDia(primeiroDiaPeriodo, 3);

		BigDecimal mediaEsperada = BigDecimal.valueOf(2 * VALOR_VOLUME_QDC, ESCALA_VALOR_QDC);

		List<ContratoPontoConsumoModalidade> modalidades =
				Arrays.asList(criarContratoPontoConsumoModalidade(1L), criarContratoPontoConsumoModalidade(2L));

		BigDecimal mediaPeriodoQDC = controladorContrato.calcularQDCMedia(modalidades, primeiroDiaPeriodo, ultimoDiaPeriodo);

		assertEquals(mediaEsperada, mediaPeriodoQDC);
	}

	@Test
	@Transactional
	public void calculoQDPMediaMensal() throws NegocioException {

		Calendar calendario = new GregorianCalendar();
		calendario.set(Calendar.MONTH, Calendar.FEBRUARY);

		Date dataPeriodicidade = calendario.getTime();

		int menorDiaMes = calendario.getActualMinimum(Calendar.DAY_OF_MONTH);
		int maiorDiaMes = calendario.getActualMaximum(Calendar.DAY_OF_MONTH);

		calendario.set(Calendar.DATE, maiorDiaMes);

		BigDecimal mediaEsperada = BigDecimal.valueOf(maiorDiaMes * VALOR_VOLUME_QDP, ESCALA_VALOR_QDP)
				.divide(BigDecimal.valueOf(maiorDiaMes), ESCALA_VALOR_QDP, BigDecimal.ROUND_HALF_UP);

		ContratoPontoConsumoModalidade consumoModalidade = criarContratoPontoConsumoModalidade(1L);
		ContratoPontoConsumo contratoPontoConsumo = consumoModalidade.getContratoPontoConsumo();
		PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();

		Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> mapaModalidadesPorPontoConsumo =
				controladorContrato.agruparContratoPontoConsumoModalidadePorPontoConsumo(Arrays.asList(consumoModalidade));

		MultiKeyMap mapaValorQDPPorPontoConsumo = new MultiKeyMap();

		while (menorDiaMes <= maiorDiaMes) {
			calendario.set(Calendar.DATE, menorDiaMes);
			mapaValorQDPPorPontoConsumo.put(pontoConsumo.getChavePrimaria(), DataUtil.minimizarHorario(calendario.getTime()),
					BigDecimal.valueOf(VALOR_VOLUME_QDP, ESCALA_VALOR_QDP));
			menorDiaMes += 1;

		}

		BigDecimal calculoQDPMedia = controladorApuracaoPenalidade.calcularQDPMedia(mapaModalidadesPorPontoConsumo,
				mapaValorQDPPorPontoConsumo, dataPeriodicidade);

		assertEquals(mediaEsperada, calculoQDPMedia);
	}

	@Test
	@Transactional
	public void calculoQDPMediaMensalPendenteUmDia() throws NegocioException {

		Calendar calendario = new GregorianCalendar();
		calendario.set(Calendar.MONTH, Calendar.FEBRUARY);

		Date dataPeriodicidade = calendario.getTime();

		int menorDiaMes = calendario.getActualMinimum(Calendar.DAY_OF_MONTH);
		int maiorDiaMes = calendario.getActualMaximum(Calendar.DAY_OF_MONTH);

		calendario.set(Calendar.DATE, maiorDiaMes);
		Date ultimoDiaMes = calendario.getTime();

		BigDecimal mediaEsperada = BigDecimal.valueOf((maiorDiaMes - 1) * VALOR_VOLUME_QDP, ESCALA_VALOR_QDP)
				.add(BigDecimal.valueOf(VALOR_VOLUME_QDC, ESCALA_VALOR_QDC))
				.divide(BigDecimal.valueOf(maiorDiaMes), ESCALA_VALOR_QDP, BigDecimal.ROUND_HALF_UP);

		ContratoPontoConsumoModalidade consumoModalidade = criarContratoPontoConsumoModalidade(1L);

		ContratoPontoConsumoModalidadeQDC contratoPontoConsumoModalidadeQDC = criarContratoPontoConsumoModalidadeQDC();
		contratoPontoConsumoModalidadeQDC.setDataVigencia(ultimoDiaMes);

		consumoModalidade.getListaContratoPontoConsumoModalidadeQDC().clear();
		consumoModalidade.getListaContratoPontoConsumoModalidadeQDC().add(contratoPontoConsumoModalidadeQDC);

		ContratoPontoConsumo contratoPontoConsumo = consumoModalidade.getContratoPontoConsumo();
		PontoConsumo pontoConsumo = contratoPontoConsumo.getPontoConsumo();

		Map<PontoConsumo, List<ContratoPontoConsumoModalidade>> mapaModalidadesPorPontoConsumo =
				controladorContrato.agruparContratoPontoConsumoModalidadePorPontoConsumo(Arrays.asList(consumoModalidade));

		MultiKeyMap mapaValorQDPPorPontoConsumo = new MultiKeyMap();

		while (menorDiaMes < maiorDiaMes) {
			calendario.set(Calendar.DATE, menorDiaMes);
			mapaValorQDPPorPontoConsumo.put(pontoConsumo.getChavePrimaria(), DataUtil.minimizarHorario(calendario.getTime()),
					BigDecimal.valueOf(VALOR_VOLUME_QDP, ESCALA_VALOR_QDP));
			menorDiaMes += 1;

		}

		BigDecimal calculoQDPMedia = controladorApuracaoPenalidade.calcularQDPMedia(mapaModalidadesPorPontoConsumo,
				mapaValorQDPPorPontoConsumo, dataPeriodicidade);

		assertEquals(mediaEsperada, calculoQDPMedia);
	}
	
	@Test
	@Transactional
	public void testeAlterarContrato() throws Throwable {
		Contrato contrato = (Contrato) controladorContrato.obter(999999l);	
		Contrato contrato1 = criarContratoAlterado(contrato);	
		
		Long chave = controladorContrato.atualizarContrato(contrato1, contrato, true);
		
		assertNotNull(chave);
	}
	
	@Test
	@Transactional
	public void testeAditarContrato() throws Throwable {

		Contrato contrato = (Contrato) controladorContrato.obter(999999l);
		Contrato contrato1 = criarContratoAlterado(contrato);	
		
		Long chave = controladorContrato.aditarContrato(contrato1, contrato);
		assertNotNull(chave);
	}
		
	@Test
	@Transactional
	public void testeAtivarDesativarPontoConsumo() throws NegocioException, ConcorrenciaException {
		Contrato contrato = (Contrato) controladorContrato.obter(999999l);
		ContratoAction contratoAction = new ContratoAction();
		contratoAction.desativarPontoConsumo(contrato);
		PontoConsumo ponto = (PontoConsumo) controladorPontoConsumo.obter(999999l);
		ponto.setHabilitado(Boolean.FALSE);
		contratoAction.ativarPontoConsumo(ponto);
		
	}
	

	public ContratoPontoConsumoModalidade criarContratoPontoConsumoModalidade(Long chavePrimaria) {
		ContratoPontoConsumoModalidade consumoModalidade =
				(ContratoPontoConsumoModalidade) controladorContrato.criarContratoPontoConsumoModalidade();
		consumoModalidade.setChavePrimaria(chavePrimaria);
		consumoModalidade.getListaContratoPontoConsumoModalidadeQDC().add(criarContratoPontoConsumoModalidadeQDC());
		consumoModalidade.setContratoPontoConsumo(criarContratoPontoConsumo(chavePrimaria));
		return consumoModalidade;

	}

	public ContratoPontoConsumoModalidadeQDC criarContratoPontoConsumoModalidadeQDC() {
		ContratoPontoConsumoModalidadeQDC consumoModalidadeQDC =
				(ContratoPontoConsumoModalidadeQDC) controladorContrato.criarContratoPontoConsumoModalidadeQDC();
		consumoModalidadeQDC.setChavePrimaria(1L);
		consumoModalidadeQDC.setDataVigencia(DataUtil.gerarDataHmsZerados(new Date()));
		consumoModalidadeQDC.setMedidaVolume(BigDecimal.valueOf(VALOR_VOLUME_QDC, ESCALA_VALOR_QDC));
		return consumoModalidadeQDC;
	}

	public ContratoPontoConsumo criarContratoPontoConsumo(Long chavePrimaria) {

		ContratoPontoConsumo contratoPontoConsumo = (ContratoPontoConsumo) controladorContrato.criarContratoPontoConsumo();
		Contrato contrato = (Contrato) controladorContrato.criar();
		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.criar();

		contratoPontoConsumo.setChavePrimaria(chavePrimaria);
		pontoConsumo.setChavePrimaria(chavePrimaria);
		contrato.setChavePrimaria(chavePrimaria);

		contratoPontoConsumo.setPontoConsumo(pontoConsumo);
		contratoPontoConsumo.setContrato(contrato);

		return contratoPontoConsumo;
	}

	public ContratoPenalidade criarContratoPenalidade() {

		ContratoPenalidade contratoPenalidade = (ContratoPenalidade) controladorContrato.criarContratoPenalidade();
		contratoPenalidade.setChavePrimaria(1l);
		Date dataInicioVigencia = Util.getDataCorrente(Boolean.TRUE);
		Date dataFimVigencia = Util.adicionarDiasData(dataInicioVigencia, INTERVALO_VIGENCIA_PENALIDADE);
		contratoPenalidade.setDataInicioVigencia(dataInicioVigencia);
		contratoPenalidade.setDataFimVigencia(dataFimVigencia);
		return contratoPenalidade;
	}
	
	public Contrato criarContratoAlterado(Contrato contrato) throws GGASException {
		ContratoPontoConsumo contratoPontoConsumo = fachada.criarContratoPontoConsumo();
		SituacaoContrato situacaoContrato = controladorContrato.obterSituacaoContrato(9);
		situacaoContrato.setDescricao("TESTE");

		Contrato contrato1 = (Contrato) controladorContrato.criar();
		contrato1.setDataAssinatura(contrato.getDataAssinatura());

		contrato1.setSituacao(situacaoContrato);
		contrato1.setArrecadadorContratoConvenio(contrato.getArrecadadorContratoConvenio());
		contrato1.setTipoPeriodicidadePenalidade(Boolean.FALSE);
		contrato1.setClienteAssinatura(contrato.getClienteAssinatura());
		
		Collection<ContratoPontoConsumo> listaContratoPontoConsumo = new HashSet<ContratoPontoConsumo>();
		PontoConsumo pontoConsumo = fachada.obterPontoConsumo(999999);
		contratoPontoConsumo.setPontoConsumo(pontoConsumo);
		contratoPontoConsumo.setContrato(contrato1);
		contratoPontoConsumo.setHabilitado(true);
		contratoPontoConsumo.setUltimaAlteracao(Calendar.getInstance().getTime());
		contratoPontoConsumo
				.setMedidaPressao(contrato.getListaContratoPontoConsumo().iterator().next().getMedidaPressao());
		contratoPontoConsumo
				.setUnidadePressao(contrato.getListaContratoPontoConsumo().iterator().next().getUnidadePressao());
		listaContratoPontoConsumo.add(contratoPontoConsumo);
		
		contrato1.setListaContratoPontoConsumo(listaContratoPontoConsumo);	
		
		return contrato1;
	}

}
