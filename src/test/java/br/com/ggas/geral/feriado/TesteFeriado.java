package br.com.ggas.geral.feriado;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.geografico.ControladorMunicipio;
import br.com.ggas.cadastro.geografico.Municipio;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.DataUtil;

public class TesteFeriado extends GGASTestCase {

	@Autowired
	@Qualifier("controladorFeriado")
	private ControladorFeriado controladorFeriado;

	@Autowired
	@Qualifier("controladorMunicipio")
	private ControladorMunicipio controladorMunicipio;

	@Test
	@Transactional
	public void testeDiaNaoUtilSemFeriados() throws NegocioException {

		Calendar calendario = Calendar.getInstance();
		calendario.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

		assertFalse(controladorFeriado.isDiaUtil(null, calendario.getTime()));
	}

	@Test
	@Transactional
	public void testeDiaUtilSemFeriados() throws NegocioException {

		Calendar calendario = Calendar.getInstance();
		calendario.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);

		assertTrue(controladorFeriado.isDiaUtil(null, calendario.getTime()));
	}

	@Test
	@Transactional
	public void testeFeriadoEsferaMunicipal() throws NegocioException {

		Calendar calendario = Calendar.getInstance();
		calendario.set(Calendar.DATE, 8);
		calendario.set(Calendar.MONTH, 11);
		calendario.set(Calendar.YEAR, 2017);

		Date dataFeriado = DataUtil.gerarDataHmsZerados(calendario.getTime());

		Feriado feriado = criarFeriado(dataFeriado);
		feriado.setIndicadorMunicipal(Boolean.TRUE);
		super.salvar(feriado);

		Municipio municipio = feriado.getMunicipio();

		Map<Long, Collection<Date>> feriados = controladorFeriado.consultarFeriados(Arrays.asList(municipio));

		assertFalse(feriados.isEmpty());
		assertFalse(controladorFeriado.isDiaUtil(feriados.get(municipio.getChavePrimaria()), dataFeriado));
	}

	@Test
	@Transactional
	public void testeFeriadoEsferaNaoMunicipal() throws NegocioException {

		Calendar calendario = Calendar.getInstance();
		calendario.set(Calendar.DATE, 25);
		calendario.set(Calendar.MONTH, 11);
		calendario.set(Calendar.YEAR, 2017);

		Date dataFeriado = DataUtil.gerarDataHmsZerados(calendario.getTime());

		Feriado feriado = criarFeriado(dataFeriado);
		feriado.setIndicadorMunicipal(Boolean.FALSE);
		super.salvar(feriado);

		Collection<Date> feriados = controladorFeriado.consultarFeriadosNaoMunicipais();

		assertFalse(feriados.isEmpty());
		assertFalse(controladorFeriado.isDiaUtil(feriados, dataFeriado));
	}

	@Test
	@Transactional
	public void testeDiaUtil() throws NegocioException {

		Calendar calendario = Calendar.getInstance();
		calendario.set(Calendar.DATE, 8);
		calendario.set(Calendar.MONTH, 11);
		calendario.set(Calendar.YEAR, 2017);

		Date dataFeriado = DataUtil.gerarDataHmsZerados(calendario.getTime());
		Date diaUtil = DataUtil.decrementarDia(dataFeriado, 1);

		Feriado feriado = criarFeriado(dataFeriado);
		feriado.setIndicadorMunicipal(Boolean.TRUE);
		super.salvar(feriado);

		Municipio municipio = feriado.getMunicipio();

		Map<Long, Collection<Date>> feriados = controladorFeriado.consultarFeriados(Arrays.asList(municipio));

		assertFalse(feriados.isEmpty());

		assertTrue(controladorFeriado.isDiaUtil(feriados.get(municipio.getChavePrimaria()), diaUtil));
	}

	private Feriado criarFeriado(Date data) throws NegocioException {

		Feriado feriado = (Feriado) controladorFeriado.criar();
		Municipio municipio = controladorMunicipio.obterMunicipioPorNomeUF("RECIFE", "PE");
		feriado.setMunicipio(municipio);
		feriado.setIndicadorTipo(Boolean.FALSE);
		feriado.setDescricao("TESTE");
		feriado.setData(data);
		feriado.setIndicadorMunicipal(Boolean.TRUE);
		return feriado;
	}
}
