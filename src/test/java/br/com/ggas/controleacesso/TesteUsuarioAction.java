
package br.com.ggas.controleacesso;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import org.easymock.EasyMock;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.ControladorMenu;
import br.com.ggas.geral.Menu;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

/**
 * @author Edmilson Santana
 */

public class TesteUsuarioAction extends GGASTestCase {

	@Autowired
	@Qualifier("controladorUsuario")
	private ControladorUsuario controladorUsuario;

	@Autowired
	@Qualifier("controladorAcesso")
	private ControladorAcesso controladorAcesso;

	@Autowired
	@Qualifier("controladorMenu")
	private ControladorMenu controladorMenu;

	@Test
	@Transactional
	public void testeValidarLogonSemCadastroSenha() throws GGASException {
		Usuario usuarioLogado = montarObjetoUsuario();

		Usuario usuarioPesquisado = controladorUsuario.buscar(usuarioLogado.getLogin());
		assertNotNull(usuarioPesquisado);

		usuarioPesquisado.setPrimeiroAcesso(Boolean.FALSE);
		usuarioPesquisado.setSenha(usuarioPesquisado.getSenha().replaceAll(".*", " "));
		assertTrue(controladorAcesso.validarLogon(usuarioLogado));
	}

	@Test
	@Transactional
	public void testeValidarLogonPrimeiroAcesso() throws GGASException {
		Usuario usuarioLogado = montarObjetoUsuario();

		Usuario usuarioPesquisado = controladorUsuario.buscar(usuarioLogado.getLogin());
		assertNotNull(usuarioPesquisado);
		usuarioPesquisado.setSenha(Util.criptografarSenha(usuarioLogado.getSenha(), usuarioLogado.getSenha(),
				Constantes.HASH_CRIPTOGRAFIA));

		usuarioPesquisado.setPrimeiroAcesso(Boolean.TRUE);
		assertFalse(controladorAcesso.validarLogon(usuarioLogado));
	}

	@Test
	@Transactional
	public void testeValidarLogonSenhaExpirada() throws GGASException {
		Usuario usuarioLogado = montarObjetoUsuario();

		Usuario usuarioPesquisado = controladorUsuario.buscar(usuarioLogado.getLogin());
		assertNotNull(usuarioPesquisado);

		usuarioPesquisado.setSenha(Util.criptografarSenha(usuarioLogado.getSenha(), usuarioLogado.getSenha(),
				Constantes.HASH_CRIPTOGRAFIA));
		usuarioPesquisado.setPrimeiroAcesso(Boolean.FALSE);
		usuarioPesquisado.setPeriodicidadeSenha(Integer.valueOf(2));

		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		gc.add(Calendar.DATE, -3);

		usuarioPesquisado.setDataCriacaoSenha(gc.getTime());
		assertFalse(controladorAcesso.validarLogon(usuarioLogado));
	}

	@Test
	@Transactional
	public void testeAutenticarUsuarioLoginInvalido() throws GGASException {
		Usuario usuarioLogado = montarObjetoUsuario();
		String login = usuarioLogado.getLogin();
		usuarioLogado.setLogin(login.replaceAll(".*", " "));
		try {
			controladorAcesso.autenticarUsuario(usuarioLogado);
			assertTrue(Boolean.FALSE);
		} catch (NegocioException e) {
			assertTrue(Boolean.TRUE);
		}
	}

	@Test
	@Transactional
	public void testeAutenticarUsuarioSenhaInvalida() throws GGASException {
		Usuario usuarioLogado = montarObjetoUsuario();

		Usuario usuarioPesquisado = controladorUsuario.buscar(usuarioLogado.getLogin());

		String senhaOriginal = usuarioLogado.getSenha();
		usuarioPesquisado.setTentativasSenhaErrada(0);

		try {
			usuarioLogado.setSenha(senhaOriginal.replaceAll(".*", " "));
			controladorAcesso.autenticarUsuario(usuarioLogado);
			assertTrue(Boolean.FALSE);
		} catch (NegocioException e) {
			assertTrue(Boolean.TRUE);
			assertTrue(usuarioPesquisado.getTentativasSenhaErrada() > 0);

		}
	}

	@Test
	@Transactional
	public void testeAutenticarUsuarioSenhaExpirada() throws GGASException {
		Usuario usuarioLogado = montarObjetoUsuario();

		Usuario usuarioPesquisado = controladorUsuario.buscar(usuarioLogado.getLogin());
		assertNotNull(usuarioPesquisado);

		try {
			usuarioPesquisado.setSenhaExpirada(Boolean.TRUE);
			controladorAcesso.autenticarUsuario(usuarioLogado);
			assertTrue(Boolean.FALSE);
		} catch (NegocioException e) {
			assertTrue(Boolean.TRUE);
		}
	}

	@Test
	@Transactional
	public void testeAutenticarUsuarioLogonAutenticado() throws GGASException {

		Usuario usuarioLogado = montarObjetoUsuario();

		Usuario usuarioPesquisado = controladorUsuario.buscar(usuarioLogado.getLogin());
		assertNotNull(usuarioPesquisado);

		usuarioPesquisado.setSenha(Util.criptografarSenha(usuarioLogado.getSenha(), usuarioLogado.getSenha(),
				Constantes.HASH_CRIPTOGRAFIA));
		usuarioPesquisado.setPrimeiroAcesso(Boolean.FALSE);

		Usuario usuarioAutenticado = controladorAcesso.autenticarUsuario(usuarioLogado);
		assertNotNull(usuarioAutenticado.getDadosAuditoria());
		assertTrue(usuarioAutenticado.getDadosAuditoria().equals(usuarioLogado.getDadosAuditoria()));
		assertTrue(usuarioAutenticado.getTentativasSenhaErrada() == 0);
	}

	@Test
	@Transactional
	public void testeUsuarioPermissoesModulos() throws NegocioException {

		Usuario usuarioPesquisado = controladorUsuario.buscar("admin");
		assertNotNull(usuarioPesquisado);

		Papel papel = usuarioPesquisado.getPapeis().iterator().next();
		
		Collection<Menu> modulos = controladorMenu.listarModulos(usuarioPesquisado);
		
		assertNotNull(modulos);
		assertTrue(modulos.size() > 0);

		for (Menu menu : modulos) {
			for (Permissao permissao : menu.getListaPermissao()) {
				assertTrue(permissao.getPapel().getDescricao().equals(papel.getDescricao()));
			}
		}
	}

	public Usuario montarObjetoUsuario() throws GGASException {
		DadosAuditoria dadosAuditoria = EasyMock.createMock(DadosAuditoria.class);
		Usuario usuario = (Usuario) controladorUsuario.criar();
		usuario.setDadosAuditoria(dadosAuditoria);
		usuario.setLogin("admin");
		usuario.setSenha("admin");
		return usuario;
	}

}
