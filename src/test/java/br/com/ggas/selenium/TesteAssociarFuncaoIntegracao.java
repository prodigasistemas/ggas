package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteAssociarFuncaoIntegracao extends SeleniumTestCase {

	@Test
	public void testAssociacaoIntegracao() throws Exception {
		inserirAssociacaoFuncaoIntegracao();
		alterarAssociacaoFuncaoIntegracao();
		removerAssociacaoFuncaoIntegracao();
	}

	public void inserirAssociacaoFuncaoIntegracao() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='178']/a/span[normalize-space(text()) = 'Integração']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		webDriver.findElement(By.xpath("(//input[@value='Associar'])")).click();
		new Select(webDriver.findElement(By.id("integracaoSistema"))).selectByVisibleText("PIRAMIDE");
		new Select(webDriver.findElement(By.id("integracaoFuncao"))).selectByVisibleText("Cadastro de Bens");
		webDriver.findElement(By.xpath("(//input[@id='destinatariosEmail'])")).clear();
		webDriver.findElement(By.xpath("(//input[@id='destinatariosEmail'])")).sendKeys("teste@teste.com");
		webDriver.findElement(By.xpath("(//input[@value='Salvar'])")).click();
		assertEquals("Sucesso: Associação inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void alterarAssociacaoFuncaoIntegracao() {
		webDriver.findElement(By.id("botaoPesquisar")).click();
		webDriver.findElement(By.name("chavesPrimarias")).click();
		webDriver.findElement(By.xpath("(//input[@value='Alterar'])")).click();
		webDriver.findElement(By.xpath("(//input[@id='destinatariosEmail'])")).clear();
		webDriver.findElement(By.xpath("(//input[@id='destinatariosEmail'])")).sendKeys("email@teste.com");
		webDriver.findElement(By.xpath("(//input[@value='Salvar'])")).click();
		assertEquals("Sucesso: Associação alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void removerAssociacaoFuncaoIntegracao() {
		webDriver.findElement(By.id("botaoPesquisar")).click();
		webDriver.findElement(By.name("chavesPrimarias")).click();
		webDriver.findElement(By.id("Remover")).click();
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		assertEquals("Sucesso: Associação removida(s) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

}
