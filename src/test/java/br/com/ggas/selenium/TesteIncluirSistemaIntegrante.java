package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class TesteIncluirSistemaIntegrante extends SeleniumTestCase {

	@Test
	public void testSistemaIntegracao() throws Exception {
		inserirSistemaIntegracao();
		alterarSistemaIntegracao();
		removerSistemaIntegracao();
	}

	public void inserirSistemaIntegracao() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='177']/a/span[normalize-space(text()) = 'Sistema Integrante']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		
		aguardarCarregarPagina("Incluir Sistema Integrante");
		
		webDriver.findElement(By.id("sigla")).clear();
		webDriver.findElement(By.id("sigla")).sendKeys("SA");
		webDriver.findElement(By.id("nome")).clear();
		webDriver.findElement(By.id("nome")).sendKeys("SOCIEDADE ANONIMA");
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("UMA SOCIEDADE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertMessage("Sucesso: Sistema Integrante inserido(a) com sucesso.");
	}

	public void alterarSistemaIntegracao() {
		isClickable(By.id("chavesPrimariasCheckbox")).click();
		isClickable(By.id("botaoAlterar")).click();
		
		aguardarCarregarPagina("Alterar Sistema Integrante");

		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("UMA SOCIEDADE ANONIMA");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertMessage("Sucesso: Sistema Integrante alterado(a) com sucesso.");
		
	}

	public void removerSistemaIntegracao() {
		isClickable(By.id("chavesPrimariasCheckbox")).click();
		isClickable(By.id("botaoRemover")).click();
		final String textoDoAlerta = super.obterTextoDoAlerta();
		assertEquals(textoDoAlerta, "Deseja remover os itens selecionados?\nEsta Operação não poderá ser desfeita.");
		waitForElementNotPresent(buscarElemento(By.id("chavesPrimariasCheckbox")));
		assertMessage("Sucesso: Sistema Integrante removido(s) com sucesso.");
	}
	
}
