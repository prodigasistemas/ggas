
package br.com.ggas.selenium;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TesteEquipamento extends SeleniumTestCase {

	@Test
	public void testeEquipamento() {

		testeInserirEquipamento();
		testeAlterarEquipamento();
		//testeRemoverEquipamento();

	}

	private void testeRemoverEquipamento() {
		
		JavascriptExecutor executor = (JavascriptExecutor) webDriver;
		waitForElement(By.cssSelector("fieldset.conteinerBotoes	 > #botaoAlterar"));
			
		new Select(webDriver.findElement(By.id("idSegmento"))).selectByVisibleText("COMERCIAL");
		
		WebElement  botaoPesquisar = waitForElement(By.cssSelector("fieldset.conteinerBotoesPesquisarDir > #botaoPesquisar"));
		executor.executeScript("arguments[0].click()", botaoPesquisar);
		
		
		ArrayList<WebElement> allrows = (ArrayList<WebElement>) webDriver.findElements(By.xpath("//table[@id='equipamento']/tbody/tr"));
		
		boolean found = false;
		for(WebElement row: allrows){
			ArrayList<WebElement> cells = (ArrayList<WebElement>) row.findElements(By.tagName("td"));
			if(cells.get(2).getText().equals("EQUIPAMENTO TESTANTE UNICO")) {
				executor.executeScript("arguments[0].checked = true", cells.get(0));
				found = true;
			}
	    }
		assertTrue(found);
		WebElement  botaoRemover = waitForElement(By.cssSelector("fieldset.conteinerBotoes > #botaoRemover"));
		executor.executeScript("arguments[0].click()", botaoRemover);
	}

	private void testeAlterarEquipamento() {
		
		JavascriptExecutor executor = (JavascriptExecutor) webDriver;
		
		WebElement linkEquipamento = waitForElement(By.linkText("EQUIPAMENTO TESTANTE UNICO"));
		executor.executeScript("arguments[0].click()", linkEquipamento);
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Equipamento')]"));
		
		clicarEmElemento(isClickable(By.xpath("//input[@value='Alterar']")));
		//WebElement botaoAlterar = waitForElement(By.xpath("//input[@value='Alterar']"));
		//executor.executeScript("arguments[0].click()", botaoAlterar);
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Atualizar Equipamento')]"));
		
		assertEquals("0,02", waitForElement(By.id("potenciaFixaBaixa")).getAttribute("value"));
		assertEquals("1,00", waitForElement(By.id("potenciaFixaMedia")).getAttribute("value"));
		assertEquals("9.999,00", waitForElement(By.id("potenciaFixaAlta")).getAttribute("value"));
		
		new Select(waitForElement(By.id("idSegmento"))).selectByVisibleText("COMERCIAL");

		isClickable(By.id("potenciaPadrao"));
		waitForElement(By.id("potenciaPadrao")).clear();
		waitForElement(By.id("potenciaPadrao")).sendKeys("9999");
		
		WebElement botaoSalvar = waitForElement(By.cssSelector("fieldset.conteinerBotoes > #botaoSalvar"));
		executor.executeScript("arguments[0].click()", botaoSalvar);
		
		assertEquals("Sucesso: Equipamento alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	    
	}
	

	private void testeInserirEquipamento() {
		
		JavascriptExecutor executor = (JavascriptExecutor) webDriver;
		
		WebElement element = webDriver.findElement(By.xpath("id('261')/a/span"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		WebElement botaoIncluir = waitForElement(By.id("botaoIncluir"));
		executor.executeScript("arguments[0].click()", botaoIncluir);

		obterElementoVisivel(By.id("potenciaPadrao"));
		new Select(webDriver.findElement(By.id("idSegmento"))).selectByVisibleText("COMERCIAL");
		
		assertTrue(!waitForElement(By.id("potenciaFixaBaixa")).isEnabled());
		assertTrue(!waitForElement(By.id("potenciaFixaMedia")).isEnabled());
		assertTrue(!waitForElement(By.id("potenciaFixaAlta")).isEnabled());
		
		element = webDriver.findElement(By.xpath("id('261')/a/span"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		botaoIncluir = waitForElement(By.id("botaoIncluir"));
		executor.executeScript("arguments[0].click()", botaoIncluir);
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Equipamento')]"));
		
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("EQUIPAMENTO TESTANTE UNICO");
		new Select(waitForElement(By.id("idSegmento"))).selectByVisibleText("RESIDENCIAL");
		waitForElement(By.id("potenciaFixaBaixa")).isEnabled();
		isClickable(By.id("potenciaFixaBaixa"));
		waitForElement(By.id("potenciaFixaBaixa")).clear();
		waitForElement(By.id("potenciaFixaBaixa")).sendKeys("0,02");
		
		waitForElement(By.id("potenciaFixaMedia")).clear();
		waitForElement(By.id("potenciaFixaMedia")).sendKeys("1");
		
		waitForElement(By.id("potenciaFixaAlta")).clear();
		waitForElement(By.id("potenciaFixaAlta")).sendKeys("9999");
		
		WebElement botaoSalvar = waitForElement(By.id("botaoSalvar"));
		executor.executeScript("arguments[0].click()", botaoSalvar);
		
		assertEquals("Sucesso: Equipamento inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
}
