
package br.com.ggas.selenium;

import java.util.ResourceBundle;

import org.apache.log4j.Logger;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class SeleniumConfiguration {
	
	private static SeleniumConfiguration instance = null;

	private static final Logger LOG = Logger.getLogger(SeleniumConfiguration.class);

	private ComboPooledDataSource dataSource;

	private ResourceBundle hibernateProperties;

	private ResourceBundle seleniumProperties;

	private boolean scriptRuns;

	private boolean errorOcurred = Boolean.TRUE;

	private SeleniumConfiguration() {

		this.hibernateProperties = ResourceBundle.getBundle("hibernate");
		this.seleniumProperties = ResourceBundle.getBundle("selenium");
		this.dataSource = new ComboPooledDataSource();
		try {
			this.dataSource.setUser(this.hibernateProperties.getString("hibernate.connection.username"));
			this.dataSource.setPassword(this.hibernateProperties.getString("hibernate.connection.password"));
			this.dataSource.setDriverClass(this.hibernateProperties.getString("hibernate.connection.driver_class"));
			this.dataSource.setJdbcUrl(this.hibernateProperties.getString("hibernate.connection.url"));
			this.dataSource.setIdleConnectionTestPeriod(
					Integer.valueOf(this.hibernateProperties.getString("hibernate.c3p0.idle_test_period")));
			this.dataSource.setLoginTimeout(
					Integer.valueOf(this.hibernateProperties.getString("hibernate.c3p0.timeout")));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

	}

	public static synchronized SeleniumConfiguration getInstance() {

		if (instance == null) {
			instance = new SeleniumConfiguration();
		}
		return instance;
	}

	public static void destroyInstance() {

		if (instance != null) {
			instance.cleanUpResources();
			instance = null;
		}
	}

	public void cleanUpResources() {

		if (dataSource != null) {
			dataSource.close();
			dataSource = null;
		}
	}

	public void insertDependenciesOfFunctionalTests() {

		String arquivoCarga = seleniumProperties.getString("selenium.arquivoCarga");
		executeScript(arquivoCarga);
	}

	public Boolean executeSeleniumScripts() {

		if (!scriptRuns) {
			try {
				this.insertDependenciesOfFunctionalTests();
				this.errorOcurred = Boolean.FALSE;
			} catch (Exception e) {
				throw e;
			} finally {
				this.scriptRuns = Boolean.TRUE;
				cleanUpResources();
			}
		}
		return this.errorOcurred;
	}

	public void cleanUpDatabaseOfDataFromFunctionalTests() {

		String arquivoCarga = seleniumProperties.getString("selenium.arquivoIntegridade");
		executeScript(arquivoCarga);
	}

	public void executeScript(String scriptPath) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(this.dataSource);

		String workingDirectory = System.getProperty("user.dir").replace("\\", "/");
		Resource resource = new FileSystemResource(workingDirectory + scriptPath);
		
		JdbcTestUtils.executeSqlScript(jdbcTemplate, resource, Boolean.FALSE);
	}

}
