package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class TesteImovelAreaConstruida extends SeleniumTestCase {

	@Test
	public void testeAreaConstruida() throws Exception {
		listandoAreasConstruidas();
		registandoAreasConstruidas();
		removendoAreaRegistrada();
	}

	public void listandoAreasConstruidas() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='150']/a/span[normalize-space(text()) = 'Área Construída']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.xpath("//input[@value='Limpar']")));
	}

	public void registandoAreasConstruidas() {
		clicarEmElemento(isClickable(By.xpath("//input[@value='Incluir']")));
		clicarEmElemento(isClickable(By.id("maiorFaixa")));
		isClickable(By.id("maiorFaixa")).sendKeys("500");
		clicarEmElemento(isClickable(By.xpath("//input[@title='Adicionar']")));
		clicarEmElemento(isClickable(By.xpath("(//input[@id='maiorFaixa'])[2]")));
		isClickable(By.xpath("(//input[@id='maiorFaixa'])[2]")).sendKeys("600");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Área Construída alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void removendoAreaRegistrada() {
		clicarEmElemento(isClickable(By.xpath("//input[@value='Incluir']")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Área Construída')]"));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		obterElementoVisivel(By.xpath("//input[@value='Salvar']"));
		clicarEmElemento(isClickable(By.xpath("//input[@value='Salvar']")));
		assertEquals("Sucesso: Área Construída alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}
}
