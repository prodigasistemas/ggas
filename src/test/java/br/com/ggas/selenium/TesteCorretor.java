
package br.com.ggas.selenium;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TesteCorretor extends SeleniumTestCase {

	@Test
	public void testeCorretorMarca() {

		testeInserirCorretorMarca();
		testeAlterarCorretorMarca();
		testeRemoverCorretorMarca();
	}

	private void testeRemoverCorretorMarca() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.xpath("//input[@name='buttonRemover' and @value='Remover']")).click();
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		espera(3000);
		obterElementoVisivel(By.linkText("MARCA INDUSTRIAL"));
		assertEquals("Sucesso: Corretor Marca removido(s) com sucesso.", webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarCorretorMarca() {
		webDriver.findElement(By.xpath("//table[@id='corretorMarca']/tbody/tr/td[3]/a")).click();
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricaoAbreviada")).clear();
		webDriver.findElement(By.id("descricaoAbreviada")).sendKeys("");
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.name("buttonRemover")).click();
		webDriver.findElement(By.id("descricaoAbreviada")).clear();
		webDriver.findElement(By.id("descricaoAbreviada")).sendKeys("Mc");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Corretor Marca alterado(a) com sucesso.", webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirCorretorMarca() {
		WebElement element = webDriver.findElement(By.xpath("//span[contains(text(),'Marca')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);			
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("Mercu");
		webDriver.findElement(By.id("botaoPesquisar")).click();
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("Corretor Marca3");
		webDriver.findElement(By.id("descricaoAbreviada")).clear();
		webDriver.findElement(By.id("descricaoAbreviada")).sendKeys("Mc");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Corretor Marca inserido(a) com sucesso.", webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeCorretorModelo() {

		testeInserirCorretorModelo();
		testeAlterarCorretorModelo();
		testeRemoverCorretorModelo();
	}

	private void testeRemoverCorretorModelo() {
		webDriver.findElement(By.cssSelector("td > input[name='chavesPrimarias']")).click();
		webDriver.findElement(By.name("buttonRemover")).click();
		webDriver.findElement(By.id("descricaoAbreviada")).clear();
		webDriver.findElement(By.id("descricaoAbreviada")).sendKeys("Mc");
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.linkText("CORRETO MODELO")).click();
		webDriver.findElement(By.name("Button")).click();
		webDriver.findElement(By.id("descricaoAbreviada")).clear();
		webDriver.findElement(By.id("descricaoAbreviada")).sendKeys("Mc");
		webDriver.findElement(By.id("botaoPesquisar")).click();
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.xpath("//input[@name='buttonRemover' and @value='Remover']")).click();
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		assertEquals("Sucesso: Corretor Modelo removido(s) com sucesso.", webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarCorretorModelo() {
		webDriver.findElement(By.linkText("CORRETO MODELO")).click();
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricaoAbreviada")).clear();
		webDriver.findElement(By.id("descricaoAbreviada")).sendKeys("");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Corretor Modelo alterado(a) com sucesso.", webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirCorretorModelo() {

		WebElement element = webDriver.findElement(By.xpath("id('128')/a"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		webDriver.findElement(By.id("botaoPesquisar")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("Xartu");
		webDriver.findElement(By.id("botaoPesquisar")).click();
		obterElementoVisivel(By.xpath("//input[@name='Button' and @value='Limpar']"));
		webDriver.findElement(By.xpath("//input[@name='Button' and @value='Limpar']")).click();
		webDriver.findElement(By.id("botaoPesquisar")).click();
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("Correto Modelo");
		webDriver.findElement(By.id("descricaoAbreviada")).clear();
		webDriver.findElement(By.id("descricaoAbreviada")).sendKeys("Cm");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Corretor Modelo inserido(a) com sucesso.", webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

}
