
package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteEmpresa extends SeleniumTestCase {

	@Test
	public void testeUnidadeOrganizacional() {

		testeInserirUnidadeOrganizacional();
		testeAlterarUnidadeOrganizacional();
		testeRemoverUnidadeOrganizacional();
	}

	private void testeRemoverUnidadeOrganizacional() {
		webDriver.findElement(By.cssSelector("td > #chavesPrimarias")).click();
		webDriver.findElement(By.cssSelector("input[value='Remover']")).click();
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > #chavesPrimarias")));
		assertEquals("Sucesso: Unidade Organizacional removida(s) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarUnidadeOrganizacional() {
		webDriver.findElement(By.cssSelector("td > #chavesPrimarias")).click();
		webDriver.findElement(By.cssSelector("input[value='Alterar']")).click();
		new Select(webDriver.findElement(By.id("idMunicipio"))).selectByVisibleText("CAMPO ALEGRE");
		preencherCampo("inicioExpediente", "08:00");
		preencherCampo("fimExpediente", "16:00");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Unidade Organizacional alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirUnidadeOrganizacional() {
		WebElement element = webDriver.findElement(By.xpath("//span[contains(text(),'Unidade Organizacional')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		waitForElement(By.name("button"));
		webDriver.findElement(By.name("button")).click();
		new Select(webDriver.findElement(By.id("idUnidadeTipo"))).selectByVisibleText("AGENCIA");
		new Select(webDriver.findElement(By.id("idGerenciaRegional"))).selectByVisibleText("PROCENGE INFORMATICA");
		new Select(webDriver.findElement(By.id("idMunicipio"))).selectByVisibleText("CAMPINAS");
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("TESTE");
		preencherCampo("inicioExpediente", "12:00");
		preencherCampo("fimExpediente", "20:00");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Unidade Organizacional inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Ignore
	@Test
	public void testeUnidadeNegocio() {

		testeInserirUnidadeNegocio();
		testeAlterarUnidadeNegocio();
		testeRemoverUnidadeNegocio();
	}

	private void testeRemoverUnidadeNegocio() {
		webDriver.findElement(By.cssSelector("td > input[name='chavesPrimarias']")).click();
		webDriver.findElement(By.xpath("//input[@name='buttonRemover' and @value='Remover']")).click();
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name='chavesPrimarias']")));
		assertEquals("Sucesso: Unidade de Negócio removida(s) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarUnidadeNegocio() {
		webDriver.findElement(By.linkText("TESTE UNIDADE")).click();
		waitForElement(By.name("button"));
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("TESTE");
		webDriver.findElement(By.id("descricaoAbreviada")).clear();
		webDriver.findElement(By.id("descricaoAbreviada")).sendKeys("TES");
		webDriver.findElement(By.xpath("//input[@value='Salvar']")).click();
		assertEquals("Sucesso: Unidade de Negócio alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirUnidadeNegocio() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='157']/a/span[normalize-space(text()) = 'Unidade Negocio']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		waitForElement(By.name("button"));
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("TESTE UNIDADE");
		webDriver.findElement(By.id("descricaoAbreviada")).clear();
		webDriver.findElement(By.id("descricaoAbreviada")).sendKeys("UNI");
		new Select(webDriver.findElement(By.id("gerenciaRegional"))).selectByVisibleText("PROCENGE INFORMATICA");
		webDriver.findElement(By.xpath("//input[@value='Salvar']")).click();
		assertEquals("Sucesso: Unidade de Negócio inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeFuncionario() {

		testeInserirFuncionario();
		testeAlterarFuncionario();
		testeRemoverFuncionario();
	}

	private void testeRemoverFuncionario() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.xpath("//input[@name='buttonRemover' and @value='Remover']")).click();
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Funcionário removido(s) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarFuncionario() {
		webDriver.findElement(By.linkText("HENRIQUE CANTARELLI")).click();
		waitForElement(By.name("buttonAlterar"));
		webDriver.findElement(By.name("buttonAlterar")).click();
		webDriver.findElement(By.id("descricaoCargo")).clear();
		webDriver.findElement(By.id("descricaoCargo")).sendKeys("ANALISTA DE SISTEMAS");
		webDriver.findElement(By.id("telefone")).clear();
		webDriver.findElement(By.id("telefone")).sendKeys("33250510");
		webDriver.findElement(By.xpath("//input[@value='Salvar']")).click();
		assertEquals("Sucesso: Funcionário alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirFuncionario() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='26']/a/span[normalize-space(text()) = 'Funcionário']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		waitForElement(By.name("buttonIncluir"));
		webDriver.findElement(By.name("buttonIncluir")).click();
		webDriver.findElement(By.id("matricula")).clear();
		webDriver.findElement(By.id("matricula")).sendKeys("0011");
		webDriver.findElement(By.id("nome")).clear();
		webDriver.findElement(By.id("nome")).sendKeys("HENRIQUE CANTARELLI");
		webDriver.findElement(By.id("descricaoCargo")).clear();
		webDriver.findElement(By.id("descricaoCargo")).sendKeys("ANALISTA");
		webDriver.findElement(By.id("codigoDDD")).clear();
		webDriver.findElement(By.id("codigoDDD")).sendKeys("81");
		webDriver.findElement(By.id("telefone")).clear();
		webDriver.findElement(By.id("telefone")).sendKeys("33250510");
		webDriver.findElement(By.id("email")).clear();
		webDriver.findElement(By.id("email")).sendKeys("procenge@procenge.com.br");
		espera();
		new Select(webDriver.findElement(By.id("idEmpresa")))
				.selectByVisibleText("C A DE CARVALHO CORREIA TECNOLOGIA ME");
		element = super.waitForElement(By.id("idUnidadeOrganizacional"));
		new Select(element).selectByVisibleText("UNIDADE TESTE");
		webDriver.findElement(By.id("matricula")).clear();
		webDriver.findElement(By.id("matricula")).sendKeys("2563");
		webDriver.findElement(By.xpath("//input[@value='Salvar']")).click();
		assertEquals("Sucesso: Funcionário inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeEmpresa() {
	
		// obtém o dados da janela principal
		String janelaInicio = webDriver.getWindowHandle();

		WebElement element = waitForElement(By.xpath("//li[@id='24']/a/span[normalize-space(text()) = 'Empresa']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Empresa')]"));

		String path = super.getPathImagem("ggas_marca_login.jpg");
		espera();
		JavascriptExecutor jse = (JavascriptExecutor) webDriver;
		jse.executeScript("document.getElementById('logo').setAttribute('value', '" + path + "');");
		clicarEmElemento(isClickable(By.id("botaoMoverTodosDireita")));

		clicarEmElemento( isClickable(By.id("botaoPesquisarClientePesssoaJuridica")));

		// muda o foco para a janela pesquisar
		for (String winHandle : webDriver.getWindowHandles()) {
			if( !winHandle.equals(janelaInicio) ) {
		         webDriver.switchTo().window(winHandle);
		    }
		}

		clicarEmElemento( isClickable(By.id("botaoPesquisar")));
		espera(10000);
		clicarEmElemento( isClickable(By.partialLinkText("BRASKEM")));

		webDriver.switchTo().window(janelaInicio);

		elementoPossuiAtributoValor(By.id("emailClienteTexto"), "contato@braskem.com.br");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals("Sucesso: Empresa inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		clicarEmElemento( isClickable(By.partialLinkText("BRASKEM")));
		waitForElementNotPresent(buscarElemento(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		clicarEmElemento(isClickable(By.cssSelector("input[id=\"habilitado\"][value=\"false\"]")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Empresa alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.partialLinkText("BRASKEM")));
		assertEquals("Sucesso: Empresa(s) removida(s) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
	}

}
