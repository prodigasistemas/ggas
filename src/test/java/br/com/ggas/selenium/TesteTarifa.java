
package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import br.com.ggas.util.Util;

public class TesteTarifa extends SeleniumTestCase {

	@Test
	public void testeTarifa() {

		testeInserirTarifa();
		testeAlterarTarifa();
		testeRemoverTarifa();
	}

	private void testeRemoverTarifa() {
		clicarEmElemento(isClickable(By.name("chavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(
				obterTextoDoAlerta().matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.name("chavesPrimarias")));
		assertEquals("Sucesso: Tarifa removido(s) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarTarifa() {
		clicarEmElemento(isClickable(By.linkText("RESIDENCIAL")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Detalhar Tarifa')]"));
		clicarEmElemento(isClickable(By.id("buttonAlterar")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Alterar Tarifa')]"));
		obterElementoVisivel(By.id("buttonSalvar"));
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("RES");
		obterElementoVisivel(By.id("idDescontoCadastrado"));
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		aceitarAlerta();
		
		assertEquals("Sucesso: Tarifa alterado(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirTarifa() {
		String dataAtual = Util.getDataCorrente();
		WebElement element = webDriver.findElement(By.xpath("//li[@id='60']/a/span[contains(text(),'Tarifa')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Incluir Tarifa')]"));
		isClickable(By.id("descricao")).sendKeys("RESIDENCIAL");
		isClickable(By.id("descricaoAbreviada")).sendKeys("RES");
		new Select(isClickable(By.id("tipoContrato"))).selectByVisibleText("Contrato Principal");
		new Select(isClickable(By.id("idItemFatura"))).selectByVisibleText("GÁS");
		new Select(isClickable(By.id("idSegmento"))).selectByVisibleText("RESIDENCIAL");
		isClickable(By.id("qtdCasasDecimais")).sendKeys("4");
		espera(1000);
		preencherCampo("dataVigencia", dataAtual);
		new Select(isClickable(By.id("idTipoCalculo"))).selectByVisibleText("CASCATA");
		new Select(isClickable(By.id("idBaseApuracao"))).selectByVisibleText("PERIÓDICO");
		new Select(isClickable(By.id("idUnidadeMonetaria"))).selectByVisibleText("REAL");
		clicarEmElemento(isClickable(By.xpath("//option[@title='ICMS']")));
		clicarEmElemento(isClickable(By.id("botaoDireita")));

		isClickable(By.id("observacaoVigencia")).sendKeys("RESIDENCIAL");

		isClickable(By.id("primeiraFI_0")).sendKeys("100");
		isClickable(By.id("valorFixoSemImposto_0")).sendKeys("1,4700");
		isClickable(By.id("valor_variavel_sem_imposto_0")).sendKeys("1,2113");

		clicarEmElemento(isClickable(By.id("Descontos")));
		espera(1000);
		isClickable(By.id("dataDescontoVigenciaDe"));
		preencherCampo("dataDescontoVigenciaDe", dataAtual);
		espera(1000);
		isClickable(By.id("dataDescontoVigenciaPara"));
		preencherCampo("dataDescontoVigenciaPara", dataAtual);
		new Select(isClickable(By.id("idTipoDesconto"))).selectByVisibleText("VALOR($)");
		isClickable(By.id("descricaoDescontoVigencia")).sendKeys("DESCONTO");

		isClickable(By.id("desconto_fixo_sem_imposto_0")).sendKeys("0,0000");
		isClickable(By.id("desconto_variavel_sem_imposto_0")).sendKeys("0,7000");

		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Tarifa inserido(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeAutorizarVigencia() {

		WebElement element = webDriver.findElement(By.xpath("//li[@id='60']/a/span[normalize-space(text()) = 'Tarifa']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		WebElement botaoPesquisar = isClickable(By.id("botaoPesquisar"));

		isClickable(By.id("descricaoModelo")).sendKeys("AUTORIZAR VIGENCIA");

		botaoPesquisar.click();

		isClickable(By.name("chavesPrimarias")).click();

		isClickable(By.id("buttonAutorizarTarifaVigencia")).click();

		new Select(isClickable(By.id("tarifasVigencia"))).selectByVisibleText("16/08/2016");

		isClickable(By.id("buttonAutorizarVigencia")).click();

		assertEquals("Sucesso: Vigência alterado(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));

		isClickable(By.name("chavesPrimarias")).click();

	}

}
