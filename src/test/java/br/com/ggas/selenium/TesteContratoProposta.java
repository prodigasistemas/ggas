package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import br.com.ggas.util.DataUtil;

public class TesteContratoProposta extends SeleniumTestCase {

	@Test
	public void testePropostaContrato() throws Exception {

		clicarEmElemento(waitForElement(By.xpath("//li[@id='30']/a/span[normalize-space(text()) = 'Proposta']")));

		String janelaPrincipal = webDriver.getWindowHandle();
		
		clicarEmElemento(isClickable(waitForElement(By.id("botaoIncluir"))));

		new Select(isClickable(By.id("idSituacaoProposta"))).selectByVisibleText("ENTREGUE");
		waitForElement(By.id("versaoProposta")).clear();
		waitForElement(By.id("versaoProposta")).sendKeys("1");
		
		preencherCampo("dataEmissao", DataUtil.gerarDataAtual());
		preencherCampo("dataVigencia", DataUtil.gerarDataAtual());
		preencherCampo("dataEntrega", DataUtil.gerarDataAtual());
		preencherCampo("dataAssembleiaCondominio", DataUtil.gerarDataAtual());
		preencherCampo("dataEleicaoSindico", DataUtil.gerarDataAtual());
		
		new Select(isClickable(By.id("idSegmento"))).selectByVisibleText("INDUSTRIAL");
		waitForElement(By.id("percentualTIR")).clear();
		waitForElement(By.id("percentualTIR")).sendKeys("2");
		waitForElement(By.id("valorMaterial")).clear();
		waitForElement(By.id("valorMaterial")).sendKeys("30000");
		waitForElement(By.id("valorMaoDeObra")).clear();
		waitForElement(By.id("valorMaoDeObra")).sendKeys("15000");
		clicarEmElemento(isClickable(waitForElement(By.id("indicadorParticipacao1"))));
		waitForElement(By.id("percentualCliente")).clear();
		waitForElement(By.id("percentualCliente")).sendKeys("40");
		
		waitForElement(By.id("valorCliente")).clear();
		waitForElement(By.id("valorCliente")).sendKeys("0");
		
		
		waitForElement(By.id("quantidadeParcela")).clear();
		waitForElement(By.id("quantidadeParcela")).sendKeys("10");
		waitForElement(By.id("valorParcela")).clear();
		waitForElement(By.id("valorParcela")).sendKeys("1800");
		waitForElement(By.id("percentualJuros")).clear();
		waitForElement(By.id("percentualJuros")).sendKeys("0");
		new Select(isClickable(By.id("idPeriodicidadeJuros"))).selectByVisibleText("ANUAL");

		clicarEmElemento(isClickable(By.id("botaoPesquisarImovel")));
		
		for (String windowHandle : webDriver.getWindowHandles()) {
			if(!windowHandle.equals(janelaPrincipal)) {
				 webDriver.switchTo().window(windowHandle);
			}
		}

		waitForElement(By.id("nomeFantasia")).clear();
		waitForElement(By.id("nomeFantasia")).sendKeys("TESTE");
		waitForElement(By.id("matriculaImovel")).clear();
		//waitForElement(By.id("matriculaImovel")).sendKeys("11161");
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		obterElementoVisivel(By.linkText("TESTE"));
		clicarEmElemento(isClickable(waitForElement(By.linkText("TESTE"))));

		webDriver.switchTo().window(janelaPrincipal);

		clicarEmElemento(isClickable(waitForElement(By.id("ui-id-2"))));

		new Select(isClickable(By.id("idModalidadeUso"))).selectByVisibleText("AQUECIMENTO");
		new Select(isClickable(By.id("idUnidadeConsumoAnual"))).selectByVisibleText("m³");
		waitForElement(By.id("consumoMedioAnual")).clear();
		waitForElement(By.id("consumoMedioAnual")).sendKeys("15");
		waitForElement(By.id("consumoMedioMensal")).clear();
		waitForElement(By.id("consumoMedioMensal")).sendKeys("15");
		waitForElement(By.id("valorGastoMensal")).clear();
		waitForElement(By.id("valorGastoMensal")).sendKeys("10");
		waitForElement(By.id("consumoUnidadeConsumidora")).clear();
		waitForElement(By.id("consumoUnidadeConsumidora")).sendKeys("15");
		waitForElement(By.id("consumoMedioAnualGN")).clear();
		waitForElement(By.id("consumoMedioAnualGN")).sendKeys("2");
		new Select(isClickable(By.id("idTarifa"))).selectByVisibleText("INDUSTRIAL APL");
		waitForElement(By.id("valorMedioGN")).clear();
		waitForElement(By.id("valorMedioGN")).sendKeys("2");
		waitForElement(By.id("valorGastoMensalGN")).clear();
		waitForElement(By.id("valorGastoMensalGN")).sendKeys("3");
		clicarEmElemento(isClickable(waitForElement(By.id("botaoSalvar"))));

		assertEquals("Sucesso: Proposta inserido(a) com sucesso.",
				getTexto(waitForElement(By.className("notification"))));

		clicarEmElemento(isClickable(waitForElement(By.id("botaoPesquisar"))));
		
		clicarEmElemento(isClickable(waitForElement(By.id("checkboxChavesPrimarias"))));
		clicarEmElemento(isClickable(waitForElement(By.id("botaoAlterar"))));
		new Select(isClickable(By.id("idSituacaoProposta"))).selectByVisibleText("CANCELADA");

		clicarEmElemento(isClickable(waitForElement(By.id("botaoSalvar"))));
		assertEquals("Sucesso: Proposta alterado(a) com sucesso.",
				super.getTexto(waitForElement(By.className("notification"))));

		clicarEmElemento(isClickable(waitForElement(By.id("checkboxChavesPrimarias"))));
		clicarEmElemento(isClickable(waitForElement(By.id("botaoRemover"))));

		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.linkText("TESTE")));
		assertEquals("Sucesso: Proposta removida(s) com sucesso.",
				super.getTexto(waitForElement(By.className("notification"))));
	}
}
