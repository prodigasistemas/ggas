package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteMedicaoLeituraAnormalidade extends SeleniumTestCase {


	//@Test
	@Ignore
	public void testMedicaoLeituraAnormalidade() throws Exception {
		inserirLeituraAnormalidade();
		alterarLeituraAnormalidade();
		removerLeituraAnormalidade();
	}

	public void inserirLeituraAnormalidade() {
		espera(1000);
		WebElement element = webDriver.findElement(By.xpath("//li[@id='52']/a/span[normalize-space(text()) = 'Anormalidade']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		espera(1000);
		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		espera(1000);
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("ERRO DE LEITURA");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("EL");
		waitForElement(By.id("mensagemLeitura")).clear();
		waitForElement(By.id("mensagemLeitura")).sendKeys("ERRO DE LEITURA");
		waitForElement(By.id("mensagemManutencao")).clear();
		waitForElement(By.id("mensagemManutencao")).sendKeys("ESPERAR");
		waitForElement(By.id("mensagemPrevencao")).clear();
		waitForElement(By.id("mensagemPrevencao")).sendKeys("CHAMAR UM TECNICO");
		new Select(webDriver.findElement(By.id("acaoAnormalidadeComLeitura"))).selectByVisibleText("Informada");
		new Select(webDriver.findElement(By.id("acaoAnormalidadeComConsumo"))).selectByVisibleText("MEDIA");
		new Select(webDriver.findElement(By.id("idAcaoAnormalidadeSemLeitura"))).selectByVisibleText("Anterior + Média");
		new Select(webDriver.findElement(By.id("idAcaoAnormalidadeSemConsumo"))).selectByVisibleText("MEDIA");
		clicarEmElemento(isClickable(By.id("salvar")));

		assertEquals("Sucesso: Anormalidade leitura inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@class='mensagensSpring']/div/p")).getAttribute("textContent").replaceAll("[\\n\\r\\t]",
						""));
	}

	public void alterarLeituraAnormalidade() {
		waitForElement(By.xpath("//*[@id=\"table-anormalidades-leitura\"]"));
		webDriver.findElement(By.xpath("//*[@id=\"table-anormalidades-leitura\"]/tbody/tr[1]/td[1]/div")).click();
		clicarEmElemento(isClickable(By.id("btnAlterar")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Alterar Anormalidade de Leitura')]"));
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("ERRO DE LEITURA TESTE");
		webDriver.findElement(By.id("descricaoAbreviada")).clear();
		webDriver.findElement(By.id("descricaoAbreviada")).sendKeys("ELT");
		webDriver.findElement(By.id("mensagemLeitura")).clear();
		webDriver.findElement(By.id("mensagemLeitura")).sendKeys("ERRO DE LEITURA TESTE");
		clicarEmElemento(isClickable(By.id("salvar")));
		assertEquals("Sucesso: Anormalidade leitura alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@class='mensagensSpring']/div/p")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void removerLeituraAnormalidade() {
		waitForElement(By.xpath("//*[@id=\"table-anormalidades-leitura\"]"));
		webDriver.findElement(By.xpath("//*[@id=\"table-anormalidades-leitura\"]/tbody/tr[1]/td[1]/div")).click();
		clicarEmElemento(isClickable(By.id("btnRemover")));
		clicarEmElemento(isClickable(By.className("swal2-confirm")));
		espera(1000);
		waitForElementNotPresent(By.linkText("ERRO DE LEITURA TESTE"));
		assertEquals("Sucesso: Anormalidade leitura removida(s) com sucesso.",
				webDriver.findElement(By.xpath("//div[@class='mensagensSpring']/div/p")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}
}