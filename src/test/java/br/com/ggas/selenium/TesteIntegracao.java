package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;


public class TesteIntegracao extends SeleniumTestCase {
	
	
	@Test
	  public void testeValidarMedicoesSupervisorio() throws Exception {
			    
		WebElement element = webDriver.findElement(By.xpath("//li[@id='176']/a/span[normalize-space(text()) = 'Validação Medições do Supervisório']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		WebElement radio = webDriver.findElement(By.id("analisadaTodas"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", radio);
		
	    webDriver.findElement(By.id("botaoPesquisar")).click();
	    webDriver.findElement(By.id("checkboxChavesPrimarias")).click();
	    webDriver.findElement(By.name("botaoDesfazerConsolidarSupervisorio")).click();
	    assertEquals("Sucesso: Operação realizada com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	  }

}