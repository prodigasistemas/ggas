package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class TesteImovelTipoCilindro extends SeleniumTestCase {

	@Test
	public void testTipoCilindro() throws Exception {
		registraTipoCilindro();
		alteraTipoCilindro();
		removeTipoCilindro();
	}

	public void registraTipoCilindro() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='149']/a/span[normalize-space(text()) = 'Tipo de Cilindro']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		webDriver.findElement(By.id("botaoPesquisar")).click();
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("NOVO CILINDRO");
		webDriver.findElement(By.id("botaoSalvar")).click();

		assertEquals("Sucesso: Tipo de Cilindro inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void alteraTipoCilindro() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.name("buttonRemover")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("CILINDRO NOVO");
		webDriver.findElement(By.name("button")).click();

		assertEquals("Sucesso: Tipo de Cilindro alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void removeTipoCilindro() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.cssSelector("input[value='Remover']")).click();
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Tipo de Cilindro removido(s) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}
}
