package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteContabilFinanceiroConfigurarEventoComercial extends SeleniumTestCase {

	@Test
	public void testConfigurarEventoComercial() throws Exception {
		//inserirConfiguracaoEventoComercial();
		//alterarConfiguracaoEventoComercial();
		//removerConfiguracaoEventoComercial();
	}

	public void inserirConfiguracaoEventoComercial() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='183']/a/span[normalize-space(text()) = 'Configurar Evento Comercial']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento( isClickable(By.id("botaoPesquisar")) );
		obterElementoVisivel(By.id("eventoComercial"));
		obterElementoVisivel(By.xpath("//input[@id='botaoConfigurar' and @disabled='']"));
		clicarEmElemento( isClickable(By.id("checkEventoComercial")) );
		clicarEmElemento( isClickable(By.id("botaoConfigurar")) );
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Configurar Evento Comercial')]"));
		isClickable(By.id("botaoAdicionar"));
		obterElementoVisivel(By.xpath("//input[@value='Limpar']"));
		waitForElementNotPresent(buscarElemento(By.id("acao")));
		obterElementoVisivel(By.id("botaoAdicionar"));
		obterElementoVisivel(By.id("botaoSalvar"));
		new Select(waitForElement( By.id("idDebito"))).selectByVisibleText("9.1.2.3.45.678 - CONTA TESTE EVENTO");
		new Select(waitForElement(By.id("idCredito"))).selectByVisibleText("9.1.2.3.45.678 - CONTA TESTE EVENTO");
		new Select(waitForElement(By.id("idSegmento"))).selectByVisibleText("COMERCIAL");
		new Select(waitForElement(By.id("idItemContabil"))).selectByVisibleText("Extenções da Rede");
		new Select(waitForElement(By.id("idTributo"))).selectByVisibleText("ISS");
		clicarEmElemento( waitForElement(By.id("botaoAdicionar")) );
		waitForElement(By.id("acao"));
		waitForElementNotPresent(buscarElemento(By.id("acao")));
		obterElementoVisivel(By.xpath("//img[@alt='Alterar Lançamento']"));
		clicarEmElemento( isClickable(By.id("botaoSalvar")) );
		assertEquals("Sucesso: Evento Comercial Configurado com Sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void alterarConfiguracaoEventoComercial() {
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Evento Comercial')]"));
		clicarEmElemento( isClickable(By.id("checkEventoComercial")) );
		clicarEmElemento( isClickable(By.id("botaoConfigurar")) );
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Configurar Evento Comercial')]"));
		obterElementoVisivel(By.xpath("//input[@value='Limpar']"));
		waitForElementNotPresent(buscarElemento(By.id("acao")));
		obterElementoVisivel(By.id("botaoSalvar"));
		obterElementoVisivel(By.xpath("//img[@alt='Alterar Lançamento']"));
		clicarEmElemento( isClickable(By.xpath("//img[@alt='Alterar Lançamento']")) );
		waitForElementNotPresent(buscarElemento(By.id("acao")));
		isClickable(By.id("botaoAlterar"));
		obterElementoVisivel(By.xpath("//input[@id='botaoAdicionar' and @disabled='']"));
		obterElementoVisivel(By.id("botaoAlterar"));
		obterElementoVisivel(By.id("idItemContabil"));
		obterElementoVisivel(By.id("botaoSalvar"));
		obterElementoVisivel(By.id("indicadorRegimeContabilCompetencia"));
		obterElementoVisivel(By.id("idSegmento"));
		new Select(waitForElement(By.id("idSegmento"))).selectByVisibleText("RESIDENCIAL");
		new Select(waitForElement(By.id("idItemContabil"))).selectByVisibleText("Indenizações");
		new Select(waitForElement(By.id("idItemContabil"))).selectByVisibleText("Multa por Atraso");
		clicarEmElemento( isClickable(By.id("botaoAlterar")) );
		clicarEmElemento( isClickable(By.id("botaoSalvar")) );
		assertEquals("Sucesso: Evento Comercial Configurado com Sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void removerConfiguracaoEventoComercial() {
		clicarEmElemento( isClickable(By.id("checkEventoComercial")) );
		clicarEmElemento( isClickable(By.id("botaoConfigurar")) );
		clicarEmElemento( isClickable(By.cssSelector("img[alt=\"Excluir Lançamento\"]")) );
		assertTrue(super.obterTextoDoAlerta().matches("^Deseja excluir o registro[\\s\\S]$"));
		clicarEmElemento( isClickable(By.id("botaoSalvar")) );
		assertEquals("Sucesso: Evento Comercial Configurado com Sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}
}
