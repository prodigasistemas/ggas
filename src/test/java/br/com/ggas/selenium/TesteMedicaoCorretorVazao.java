package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteMedicaoCorretorVazao extends SeleniumTestCase {

	@Test
	public void testeMedicaoCorretorVazao() throws Exception {
		inserirCorretorVazao();
		alterarCorretorVazao();
		removerCorretorVazao();
	}

	public void inserirCorretorVazao() {
		WebElement element = waitForElement(By.xpath("//li[@id='46']/a/span[normalize-space(text()) = 'Corretor de Vazão']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.xpath("(//input[@value='Incluir'])")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Corretor de Vazão')]"));
		new Select(waitForElement(By.id("marca"))).selectByVisibleText("MARCA INDUSTRIAL");
		waitForElement(By.id("numeroSerie")).clear();
		waitForElement(By.id("numeroSerie")).sendKeys("123456789");
		new Select(waitForElement(By.id("modelo"))).selectByVisibleText("MODELO INDUSTRIAL");
		new Select(waitForElement(By.id("numeroAnoFabricacao"))).selectByVisibleText("2016");
		new Select(waitForElement(By.id("codigoTipoMostrador"))).selectByVisibleText("Digital");
		new Select(waitForElement(By.id("idProtocoloComunicacao"))).selectByVisibleText("MODBUS");
		waitForElement(By.id("tombamento")).clear();
		waitForElement(By.id("tombamento")).sendKeys("2");

		clicarEmElemento(isClickable(By.id("ui-id-2")));
		new Select(waitForElement(By.id("numeroDigitosLeituraVazaoCorretor"))).selectByVisibleText("2");
		new Select(waitForElement(By.id("localArmazenagem"))).selectByVisibleText("ALMOXARIFADO");
		new Select(waitForElement(By.id("idTipoTransdutorPressao"))).selectByVisibleText("MANOMETRICO");
		new Select(waitForElement(By.id("idTipoTransdutorTemperatura"))).selectByVisibleText("PT-100");
		new Select(waitForElement(By.id("idTemperaturaMaximaTransdutor"))).selectByVisibleText("71.11 cº");
		new Select(waitForElement(By.id("corretorPressao"))).selectByVisibleText("Não");
		new Select(waitForElement(By.id("corretorTemperatura"))).selectByVisibleText("Sim");
		new Select(waitForElement(By.id("corretorPressao"))).selectByVisibleText("Sim");
		new Select(waitForElement(By.id("controleVazao"))).selectByVisibleText("Sim");
		clicarEmElemento(isClickable(By.xpath("(//input[@value='Incluir'])")));
		assertEquals("Sucesso: Corretor de vazão inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void alterarCorretorVazao() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.xpath("(//input[@value='Alterar'])")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Corretor de Vazão')]"));
		waitForElement(By.id("tombamento")).clear();
		waitForElement(By.id("tombamento")).sendKeys("3");
		clicarEmElemento(isClickable(By.xpath("(//input[@value='Salvar'])")));
		assertEquals("Sucesso: Corretor de vazão alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void removerCorretorVazao() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.xpath("(//input[@value='Remover'])")));
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		espera(2000);
		waitForElementNotPresent(buscarElemento(By.cssSelector("form > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Corretor(es) de vazão removido(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}
}
