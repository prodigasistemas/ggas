
package br.com.ggas.selenium;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TesteConfiguracao extends SeleniumTestCase {

	@Test
	public void testeEntidadeClasse() {

		testeInserirEntidadeClasse();
		testeAlterarEntidadeClasse();
		testeRemoverEntidadeClasse();
	}

	private void testeRemoverEntidadeClasse() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Entidade Classe removido(s) com sucesso.", isClickable(By.xpath("//div[@id='inner']/div/div/p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarEntidadeClasse() {
		isClickable(By.linkText("ENTIDADE CLASSE")).click();
		waitForElement(By.name("button"));
		isClickable(By.name("button")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("TESTE");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("TS");
		isClickable(By.name("button")).click();
		assertEquals("Sucesso: Entidade Classe alterado(a) com sucesso.",
						isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirEntidadeClasse() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='123']/a/span[normalize-space(text()) = 'Entidade Classe']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("TIPO DE");
		isClickable(By.id("botaoPesquisar")).click();
		isClickable(By.name("button")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("ENTIDADE CLASSE");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("EC");
		isClickable(By.id("botaoSalvar")).click();
		assertEquals("Sucesso: Entidade Classe inserido(a) com sucesso.", isClickable(By.xpath("//div[@id='inner']/div/div/p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeEntidadeConteudo()  {

		testeInserirEntidadeConteudo();
		testeAlterarEntidadeConteudo();
		testeRemoverEntidadeConteudo();
	}

	private void testeRemoverEntidadeConteudo() {
		isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		isClickable(By.xpath("//input[@name='buttonRemover' and @value='Remover']")).click();
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Entidade Conteúdo removida(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
	}

	private void testeAlterarEntidadeConteudo() {
		isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		isClickable(By.xpath("//input[@name='buttonRemover' and @value='Alterar']")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("Entidade teste alterado");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("Altera");
		new Select(isClickable(By.id("idEntidadeClasse"))).selectByVisibleText("Ambiente sistema nfe");
		isClickable(By.xpath("//input[@name='button' and @value='Salvar']")).click();
		assertEquals("Sucesso: Entidade Conteúdo alterado(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
	}

	private void testeInserirEntidadeConteudo() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='124']/a/span[normalize-space(text()) = 'Entidade Conteúdo']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		isClickable(By.xpath("//input[@name='button' and @value='Incluir']")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("Entidade teste");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("Ent");
		isClickable(By.id("codigo")).clear();
		isClickable(By.id("codigo")).sendKeys("1");
		new Select(isClickable(By.id("idEntidadeClasse"))).selectByVisibleText("Amortização");
		isClickable(By.id("botaoSalvar")).click();
		assertEquals("Sucesso: Entidade Conteúdo inserido(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
		}

	@Test
	public void testeUnidadeClasse() {

		testeInserirUnidadeClasse();
		testeAlterarUnidadeClasse();
		testeRemoverUnidadeClasse();
	}

	private void testeRemoverUnidadeClasse() {
		isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		isClickable(By.xpath("//input[@name='buttonRemover' and @value='Remover']")).click();
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Unidade Classe removida(s) com sucesso.", isClickable(By.xpath("//div[@id='inner']/div/div/p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarUnidadeClasse() {
		isClickable(By.xpath("//table[@id='tabelaAuxiliar']/tbody/tr/td[3]/a")).click();
		isClickable(By.name("button")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("Unidade Class) 1");
		isClickable(By.name("button")).click();
		isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		isClickable(By.cssSelector("input[value='Alterar']")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("Unidad) Classe");
		isClickable(By.name("button")).click();
		isClickable(By.xpath("//div[@id='inner']/div/div/p"));
		assertEquals("Sucesso: Unidade Classe alterado(a) com sucesso.", isClickable(By.xpath("//div[@id='inner']/div/div/p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirUnidadeClasse() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='125']/a/span[normalize-space(text()) = 'Unidade Classe']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		isClickable(By.id("incluir")).click();
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Unidade Classe')]"));
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("Unidade Classe");
		isClickable(By.name("button")).click();
		assertEquals("Sucesso: Unidade Classe inserido(a) com sucesso.", isClickable(By.xpath("//div[@id='inner']/div/div/p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}
	
	
	@Test
	public void testeUnidade()  throws Exception {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='126']/a/span[normalize-space(text()) = 'Unidade']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		isClickable(By.xpath("//input[@value='Incluir']")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("TESTE UNIDADE");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("TU");
		new Select(isClickable(By.name("classeUnidade"))).selectByVisibleText("Potência");
		isClickable(By.id("botaoSalvar")).click();
		assertEquals("Sucesso: Unidade inserido(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));

		isClickable(By.xpath("//input[@value='Limpar']")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("TESTE UNIDADE");
		isClickable(By.xpath("//input[@value='Pesquisar']")).click();
		isClickable(By.xpath("//input[@name='chavesPrimarias' and @tabindex='10']")).click();
		isClickable(By.xpath("//input[@value='Alterar']")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("UNIT TEST");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("UT");
		isClickable(By.xpath("//input[@value='Salvar']")).click();
		assertEquals("Sucesso: Unidade alterado(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
		
		isClickable(By.xpath("//input[@value='Limpar']")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("UNIT TEST");
		isClickable(By.xpath("//input[@value='Pesquisar']")).click();
		
		element = isClickable(By.xpath("//input[@name='chavesPrimarias' and @tabindex='10']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		Actions action = new Actions(webDriver);
		WebElement web2 = isClickable(By.xpath("//input[@name='chavesPrimarias' and @tabindex='10']"));
		action.moveToElement(web2).build().perform();
		
		isClickable(By.xpath("//input[@value='Remover']")).click();
		super.obterTextoDoAlerta();
		assertEquals("Sucesso: Unidade removida(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}
}
