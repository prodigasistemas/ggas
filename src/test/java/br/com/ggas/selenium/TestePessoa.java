
package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class TestePessoa extends SeleniumTestCase {

	@Test
	@Ignore
	public void testeAtividadeEconomica() {

		testeInserirAtividadeEconomica();
		testeAlterarAtividadeEconomica();
		testeRemoverAtividadeEconomica();
	}

	private void testeRemoverAtividadeEconomica() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Atividade Econômica removido(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarAtividadeEconomica() {
		clicarEmElemento(isClickable(By.linkText("ATIVIDADE CRIACAO")));
		clicarEmElemento(isClickable(By.name("button")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Atividade Econômica alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirAtividadeEconomica() {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Atividade Econômica')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		espera(1000);
		clicarEmElemento(isClickable(By.name("button")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Atividade Econômica')]"));
		isClickable(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("ATIVIDADE CRIACAO");
		isClickable(By.id("codigoCNAE")).clear();
		waitForElement(By.id("codigoCNAE")).sendKeys("1234569");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Atividade Econômica inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeProfissao() {

		testeInserirProfissao();
		testeAlterarProfissao();
		testeRemoverProfissao();
	}

	private void testeRemoverProfissao() {
		
		espera();
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Profissão removida(s) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarProfissao() {
		clicarEmElemento(isClickable(By.linkText("PROFISSAO TESTE")));

		espera();
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		waitForElement(By.xpath("//h1[normalize-space(text()) = 'Alterar Profissão']"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Profissão alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	private void testeInserirProfissao() {
		WebElement element = waitForElement(By.xpath("//li[@id='153']/a/span[normalize-space(text()) = 'Profissão']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		espera();
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		waitForElement(By.xpath("//h1[normalize-space(text()) = 'Incluir Profissão']"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("PROFISSAO TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Profissão inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeSituacaoPessoa() {
		testeInserirSituacaoPessoa();
		testeAlterarSituacaoPessoa();
		testeRemoverSituacaoPessoa();
	}

	private void testeRemoverSituacaoPessoa() {
		espera(1000);
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='Remover' and @value='Remover']")));
		espera(1000);
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Situação da Pessoa removida(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarSituacaoPessoa() {
		espera(1500);
		clicarEmElemento(isClickable(By.linkText("SITUACAO TESTE")));
		waitForElement(By.xpath("//h1[@class='tituloInterno' and normalize-space(text()) = 'Detalhar Situação da Pessoa']"));
		clicarEmElemento(isClickable(By.name("button")));
		waitForElement(By.xpath("//h1[@class='tituloInterno' and normalize-space(text()) = 'Alterar Situação da Pessoa']"));
		isClickable(waitForElement(By.id("descricao"))).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Situação da Pessoa alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirSituacaoPessoa() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='216']/a/span[normalize-space(text()) = 'Situação da Pessoa']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		espera(1000);
		clicarEmElemento(isClickable(By.name("button")));
		isClickable(waitForElement(By.id("descricao"))).clear();
		waitForElement(By.id("descricao")).sendKeys("SITUACAO TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Situação da Pessoa inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	@Test
	public void testeTipoRelacionamento() {

		testeInserirTipoRelacionamento();
		testeAlterarTipoRelacionamento();
		testeRemoverTipoRelacionamento();
	}

	private void testeRemoverTipoRelacionamento() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='buttonRemover' and @value='Remover']")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		assertEquals("Sucesso: Tipo de Relacionamento removido(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarTipoRelacionamento() {
		clicarEmElemento(isClickable(By.linkText("RELACIONAMENTO TESTE")));
		clicarEmElemento(isClickable(By.name("button")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Tipo de Relacionamento alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirTipoRelacionamento() {
		espera(1000);
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='155']/a/span[normalize-space(text()) = 'Tipo de Relacionamento']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		espera(2000);
		clicarEmElemento(isClickable(By.name("button")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("RELACIONAMENTO TESTE");
		clicarEmElemento(isClickable(By.xpath("//input[@value='false']")));
		clicarEmElemento(isClickable(By.xpath("//input[@value='Salvar']")));
		assertEquals("Sucesso: Tipo de Relacionamento inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testePessoa() {

		testeInserirPessoa();
		testeAlterarPessoa();
		testeRemoverPessoa();
	}

	private void testeRemoverPessoa() {
		espera(1000);
		clicarEmElemento(waitForElement(By.name("chavesPrimarias")));
		espera(1000);
		clicarEmElemento(isClickable(By.id("buttonRemover")));
		espera(1000);
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.name("chavesPrimarias")));
		waitForElement(By.className("notification"));
		assertEquals("Sucesso: Pessoa(s) removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarPessoa() {

		clicarEmElemento(isClickable( By.linkText("J V DE NEGREIROS")));
		clicarEmElemento(isClickable(By.id("buttonAlterar")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Alterar Pessoa')]"));
		waitForElement(By.id("nomeAbreviado")).clear();
		waitForElement(By.id("nomeAbreviado")).sendKeys("VN");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		clicarEmElemento(isClickable(By.id("sim")));
		assertEquals("Sucesso: Pessoa alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirPessoa() {
		espera();
		WebElement element = waitForElement(By.xpath("//li[@id='27']/a/span[normalize-space(text()) = 'Pessoa']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		waitForElement(By.id("buttonIncluir")).click();
		espera(1000);
		new Select(waitForElement(By.id("idTipoCliente"))).selectByVisibleText("PESSOA JURIDICA");
		waitForElement(By.id("nome")).clear();
		waitForElement(By.id("nome")).sendKeys("J V DE NEGREIROS");
		waitForElement(By.id("emailPrincipal")).clear();
		waitForElement(By.id("emailPrincipal")).sendKeys("valternegreiros@orubetecnologia.com.br");
		String winHandleBefore = webDriver.getWindowHandle();
		waitForElement(By.id("nomeFantasia")).clear();
		waitForElement(By.id("nomeFantasia")).sendKeys("DREAMASTER INFO");
		preencherCampo("cnpj", "57.546.859/0001-18");
		
		element = waitForElement(By.id("naoPossuiIndicador"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		element = waitForElement(By.id("botaoPesquisarAtividadeEconomica"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		for (String winHandle : webDriver.getWindowHandles()) {
			webDriver.switchTo().window(winHandle);
		}

		isClickable(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("INFORMATICA");
		waitForElement(By.id("botaoPesquisar")).click();
		isClickable(By.linkText("COMERCIO ATACADISTA DE EQUIPAMENTOS DE INFORMATICA")).click();

		webDriver.switchTo().window(winHandleBefore);

		element = waitForElement(By.id("endereco-tab"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		new Select(waitForElement(By.id("idTipoEndereco"))).selectByVisibleText("CADASTRAL");
		preencherCampo("cep", "57608-300");
		//waitForElement(By.xpath("id('cep')")).click();
		//waitForElement(By.xpath("id('cep')")).sendKeys("57608-300");
	//	Actions action = new Actions(webDriver);
		//WebElement we = waitForElement(By.xpath("id('ui-id-2')"));
		//action.moveToElement(we).build().perform();
		waitForElement(By.id("numeroEndereco")).clear();
		waitForElement(By.id("numeroEndereco")).click();
		waitForElement(By.id("numeroEndereco")).sendKeys("1890");
		waitForElement(By.id("botaoIncluirEndereco")).click();
		espera(500);
		element = waitForElement(By.id("telefone-tab"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		espera(1000);
		new Select(waitForElement(By.id("idTipoTelefone"))).selectByVisibleText("FIXO");
		espera(500);
		waitForElement(By.id("dddTelefone")).clear();
		waitForElement(By.id("dddTelefone")).sendKeys("81");
		waitForElement(By.id("numeroTelefone")).clear();
		waitForElement(By.id("numeroTelefone")).sendKeys("34566543");
		waitForElement(By.id("botaoIncluirTelefone")).click();
		waitForElement(By.id("buttonIncluir")).click();
		assertEquals("Sucesso: Pessoa inserido(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));
	}

	
	@Test
	public void testeTipoDeContato() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='154']/a/span[normalize-space(text()) = 'Tipo de Contato']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.xpath("//input[@name='button' and @value='Incluir']")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("CONTATO TESTE");
		clicarEmElemento(isClickable(By.xpath("//input[@name='pessoaTipo' and @value='0']")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Tipo de Contato inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		waitForElement(By.xpath("//input[@name='habilitado' and @value='false']")).click();
		waitForElement(By.xpath("//input[@name='button' and @value='Salvar']")).click();
		assertEquals("Sucesso: Tipo de Contato alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(By.linkText("CONTATO TESTE"));
		assertEquals("Sucesso: Tipo de Contato removida(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	
	@Test
	public void testeTipoDeEndereco() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='173']/a/span[normalize-space(text()) = 'Tipo de Endereço']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("ENDERECO TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Tipo de Endereço inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		waitForElement(By.xpath("//input[@name='habilitado' and @value='false']")).click();
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Tipo de Endereço alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(By.linkText("ENDERECO TESTE"));
		assertEquals("Sucesso: Tipo de Endereço removida(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeTipoDeTelefone() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='172']/a/span[normalize-space(text()) = 'Tipo de Telefone']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.xpath("//input[@name='button' and @value='Incluir']")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TELEFONE TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Tipo de Telefone inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		espera();
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='buttonRemover' and @value='Alterar']")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='habilitado' and @value='false']")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='button' and @value='Salvar']")));
		assertEquals("Sucesso: Tipo de Telefone alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='buttonRemover' and @value='Remover']")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]"));
		assertEquals("Sucesso: Tipo de Telefone removido(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	@Test
	public void testeGrupoEconomico() throws Exception {

		// obtém o nome da janela principal
		String janelaInicio = webDriver.getWindowHandle();

		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='233']/a/span[normalize-space(text()) = 'Grupo Econômico']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Grupo Econômico')]"));

		clicarEmElemento(isClickable(By.id("buttonIncluir")));

		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Grupo Econômico')]"));

		obterElementoVisivel(By.id("descricao"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("GRUPO ECONOMICO TESTE");
		new Select(waitForElement(By.id("idSegmento"))).selectByVisibleText("COMERCIAL");
		clicarEmElemento(isClickable(By.id("pesquisarPessoa")));

		// muda o foco para a janela pesquisar
		for (String winHandle : webDriver.getWindowHandles()) {
			webDriver.switchTo().window(winHandle);
		}

		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='chavesPrimarias' and @value='35']")));
		clicarEmElemento(isClickable(By.id("buttonSelecionar")));

		// volta o foco para a janela principal
		webDriver.switchTo().window(janelaInicio);

		obterElementoVisivel(By.cssSelector("img[alt=\"Remover\"]"));
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		assertEquals("Sucesso: Grupo Econômico inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.name("chavesPrimarias")));
		clicarEmElemento(isClickable(By.id("buttonAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Grupo Econômico')]"));

		clicarEmElemento(isClickable(By.xpath("//fieldset/input[3]")));
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Grupo Econômico alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Grupo Econômico')]"));

		clicarEmElemento(isClickable(By.name("chavesPrimarias")));
		clicarEmElemento(isClickable(By.id("buttonRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.name("chavesPrimarias")));
		assertEquals("Sucesso: Grupo Econômico removido(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

}
