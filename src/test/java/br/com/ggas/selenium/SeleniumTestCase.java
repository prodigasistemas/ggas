package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.time.Duration;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.test.context.TestContextManager;

import br.com.ggas.comum.GGASTestCase;

public abstract class SeleniumTestCase extends GGASTestCase {

	private static final Logger LOG = Logger.getLogger(SeleniumTestCase.class);

	private static ResourceBundle seleniumProperties = ResourceBundle.getBundle("selenium");

	private static final int TIMEOUT = 60;

	protected WebDriver webDriver;

	protected String urlBase;

	public SeleniumTestCase() {
		LOG.info("Construtor da classe selenium");
		try {
			inicializarPropriedades();
		} catch (Exception e) {
			LOG.warn(e.getMessage(), e);
		}
	}

	public void aguardarCarregarPagina(String tituloPagina) {
		this.aguardarCarregarPagina(By.className("tituloInterno"), tituloPagina);
	}

	public void aguardarCarregarPagina(By seletorTitulo, String tituloPagina) {
		this.elementoPossuiTexto(seletorTitulo, tituloPagina);
	}

	public void assertMessage(String msg) {

		assertMessage(By.xpath("//div[@id='inner']/div/div/p"), msg);
	}

	public void assertMessage(By msgSelector, String msg) {

		new WebDriverWait(this.webDriver, TIMEOUT).ignoring(StaleElementReferenceException.class).until(webDriver -> {
			String texto = getTexto(webDriver.findElement(msgSelector));
			return msg.equals(texto);
		});

		assertEquals(msg, getTexto(msgSelector));
	}

	public void espera() {
		try {
			Thread.sleep(300L);
		} catch (InterruptedException e) {
			LOG.debug(e.getMessage());
		}
	}

	public void espera(int s) {
		try {
			Thread.sleep(Long.valueOf(s));
		} catch (InterruptedException e) {
			LOG.debug(e.getMessage());
		}
	}

	public void inicializarContextoSpring() throws Exception {

		new TestContextManager(getClass()).prepareTestInstance(this);
	}

	public void inicializarPropriedades() {

		urlBase = seleniumProperties.getString("selenium.url");
	}

	@BeforeClass
	public static void setUpClass() {
		LOG.info("Iniciando testes do selenium");
		boolean execute = Boolean.valueOf(seleniumProperties.getString("selenium.execute"));
		boolean executeSql = Boolean.valueOf(seleniumProperties.getString("selenium.executeSql"));

		LOG.info("selenium.execute = " + execute + " executeSql = " + executeSql);
		Assume.assumeTrue(Boolean.valueOf(execute));
		if (execute && executeSql) {
			Boolean errorOcurred = SeleniumConfiguration.getInstance().executeSeleniumScripts();
			LOG.info("Houve algum erro durante inicialização de configuração ? " + errorOcurred);
			Assume.assumeFalse(errorOcurred);
		}
	}

	@Before
	public void openResources() {
		try {
			if (webDriver == null) {
				String geckodriver = seleniumProperties.getString("selenium.caminhoGeckodriver");

				try {
					String browser = seleniumProperties.getString("selenium.browser");

					if (!StringUtils.isEmpty(browser) && browser.toLowerCase().equals("chrome")) {
						String path = seleniumProperties.getString("selenium.browser.path");
						System.setProperty("webdriver.chrome.driver", path);
						ChromeOptions options = new ChromeOptions();
						options.addArguments("--headless", "window-size=1600,1200", "--no-sandbox");
						webDriver = new ChromeDriver(options);
					} else {
						System.setProperty("webdriver.gecko.driver", geckodriver);

						criaFirefoxDriver();
					}
				} catch (MissingResourceException e) {
					System.setProperty("webdriver.gecko.driver", geckodriver);

					criaFirefoxDriver();
				}
			}

			webDriver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
			webDriver.manage().timeouts().pageLoadTimeout(TIMEOUT, TimeUnit.SECONDS);
			webDriver.manage().timeouts().setScriptTimeout(TIMEOUT, TimeUnit.SECONDS);
			efetuarLogin();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@After
	public void closeResources() {
		webDriver.quit();
	}

	protected void efetuarLogin() {

		this.webDriver.get(this.urlBase);
		this.webDriver.findElement(By.id("login")).clear();
		this.webDriver.findElement(By.id("login")).sendKeys("admin");
		this.webDriver.findElement(By.id("senha")).clear();
		this.webDriver.findElement(By.id("senha")).sendKeys("admin");
		this.webDriver.findElement(By.id("button3")).click();
	}

	/**
	 * Instancia o webdriver com o driver do Firefox. Caso seja lançada a exceção
	 * WebDriverException, outra tentativa é feita.
	 * 
	 */
	private void criaFirefoxDriver() {
		try {
			webDriver = new FirefoxDriver();
		} catch (WebDriverException ex) {
			espera(10000);
			LOG.info("Falha na primeira tentativa de abertura do firefox, realizando segunda tentativa");
			webDriver = new FirefoxDriver();
		}
	}

	protected String obterTextoDoAlerta() {

		Alert alerta = this.webDriver.switchTo().alert();
		String texto = alerta.getText();
		alerta.accept();
		return texto;
	}

	protected void takeScreenshot(String filename) throws Exception {
		File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(filename));
	}

	protected void waitForAlertPresent() {
		WebDriverWait wait = new WebDriverWait(webDriver, 2);
		wait.until(ExpectedConditions.alertIsPresent());
	}

	protected String trocarJanela(By botaoTrocaJanela) {

		final String janelaPrincipal = webDriver.getWindowHandle();
		webDriver.findElement(botaoTrocaJanela).click();

		new WebDriverWait(webDriver, 20).until(d -> d.getWindowHandles().size() != 1);

		for (String janelaAtiva : webDriver.getWindowHandles()) {
			if (!janelaAtiva.equals(janelaPrincipal)) {
				webDriver.switchTo().window(janelaAtiva);
			}
		}

		return janelaPrincipal;
	}

	protected WebElement waitForElement(By by) {
		espera();
		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(TIMEOUT))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class);
		return wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	protected void waitForElementNotPresent(WebElement elem) {

		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(TIMEOUT / 2))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class).ignoring(TimeoutException.class);
		wait.until(ExpectedConditions.invisibilityOf(elem));
	}

	protected void waitForElementNotPresent(By by) {

		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(TIMEOUT / 2))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class).ignoring(TimeoutException.class);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	protected WebElement obterElementoVisivel(By by) {
		espera();
		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(TIMEOUT))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class).ignoring(ElementNotVisibleException.class);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	protected WebElement isClickable(By by) {
		espera(500);
		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(TIMEOUT))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class);
		return wait.until(ExpectedConditions.elementToBeClickable(by));
	}

	protected WebElement isClickable(WebElement elem) {
		espera(500);
		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(TIMEOUT))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class);
		return wait.until(ExpectedConditions.elementToBeClickable(elem));
	}

	protected void aceitarAlerta() {

		Wait<WebDriver> wait = new FluentWait<WebDriver>(webDriver).withTimeout(Duration.ofSeconds(TIMEOUT))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class)
				.ignoring(StaleElementReferenceException.class).ignoring(NoAlertPresentException.class)
				.ignoring(UnhandledAlertException.class);

		wait.until(ExpectedConditions.alertIsPresent());

		Alert alert = webDriver.switchTo().alert();
		alert.accept();
	}

	protected Boolean elementoPossuiAtributoValor(By by, String texto) {
		return (new WebDriverWait(webDriver, TIMEOUT))
				.until(ExpectedConditions.textToBePresentInElementValue(by, texto));
	}

	protected Boolean elementoPossuiTexto(By by, String texto) {
		return (new WebDriverWait(webDriver, TIMEOUT))
				.until(ExpectedConditions.textToBePresentInElementLocated(by, texto));
	}

	protected String getTexto(WebElement element) {
		return element.getAttribute("textContent").replaceAll("[\\n\\r\\t]", "");
	}

	protected String getTexto(By by) {
		return this.getTexto(this.buscarElemento(by));
	}

	protected WebElement buscarElemento(By by) {
		return webDriver.findElement(by);
	}

	protected void mostrarElemento(WebElement elemento) {
		this.executarScript("arguments[0].setAttribute('style', 'display:block')", elemento);
	}

	protected WebElement alterarValorAtributo(By by, String texto) {
		WebElement elemento = this.buscarElemento(by);
		this.alterarValorAtributo(elemento, texto);
		return elemento;
	}

	protected void alterarValorAtributo(WebElement elemento, String texto) {
		((JavascriptExecutor) webDriver).executeScript("arguments[0].value = arguments[1]", elemento, texto);
	}

	protected void executarScript(String script, WebElement elemento) {
		((JavascriptExecutor) webDriver).executeScript(script, elemento);
	}

	protected void clicarEmElemento(WebElement elem) {
		executarScript("arguments[0].click()", elem);
	}

	protected void selecionarCheckBox(WebElement elem) {
		executarScript("arguments[0].checked = true", elem);

	}

	protected void preencherCampo(String elem, String value) {
		JavascriptExecutor jse = (JavascriptExecutor) webDriver;
		jse.executeScript("$('#" + elem + "').val('" + value + "')");
	}

	protected void clicarEmBotao(WebElement botao) {
		JavascriptExecutor jse = (JavascriptExecutor) webDriver;
		jse.executeScript("arguments[0].click();", botao);
	}

	/**
	 * Captura o elemento da árvore DOM via expressão XPATH e após a captura executa
	 * o evento click via javascript
	 * 
	 * @param string
	 */
	protected void clickJS(String xpath) {
		clickJS(xpath, false);
	}

	/**
	 * 
	 * Captura o elemento da árvore DOM via expressão XPATH e após a captura executa
	 * o evento click via javascript. O paramêtro waitFotElement permite excolher se
	 * haverá ou não uma espera para capturar o elemento.
	 * 
	 * @param string
	 * @param xpath
	 * @param waitForElement
	 */
	protected void clickJS(String xpath, boolean waitForElement) {
		WebElement elemento = null;
		if (waitForElement) {
			waitForElement(By.xpath("//input[@value='Incluir']"));
			elemento = waitForElement(By.xpath("//input[@value='Incluir']"));
		} else {
			elemento = webDriver.findElement(By.xpath(xpath));
		}
		executarScript("arguments[0].click()", elemento);
	}

	protected void clickJS(By by) {
		clickJS(by, false);
	}

	protected void clickJS(By by, boolean waitForElement) {
		WebElement elemento = null;
		if (waitForElement) {
			elemento = waitForElement(by);
		} else {
			elemento = webDriver.findElement(by);
		}
		executarScript("arguments[0].click()", elemento);
	}

	protected void waitSeconds() {
		webDriver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
	}

	protected Double stringParaDecimal(String valor) {
		Double decimal = null;
		if (!StringUtils.isBlank(valor)) {
			DecimalFormat formato = new DecimalFormat();
			DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
			simbolos.setDecimalSeparator(',');
			formato.setDecimalFormatSymbols(simbolos);
			try {
				decimal = formato.parse(valor.trim()).doubleValue();
			} catch (ParseException e) {
				throw new RuntimeException(e.getMessage(), e);
			}
		}
		return decimal;
	}

	protected String decimalParaString(Double decimal, char separador) {
		String valor = null;
		if (decimal != null) {
			DecimalFormat formato = new DecimalFormat();
			formato.setMinimumFractionDigits(2);
			formato.setMaximumFractionDigits(2);
			DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
			simbolos.setDecimalSeparator(separador);
			formato.setDecimalFormatSymbols(simbolos);
			valor = formato.format(decimal);
		}
		return valor;
	}

	protected WebElement scroll(By by) {
		WebElement elemento = webDriver.findElement(by);
		this.executarScript("arguments[0].scrollIntoView(true);", elemento);
		return elemento;
	}

	protected String getPathImagem(String imagem) {

		String os = System.getProperty("os.name").toLowerCase();
		String path = getClass().getClassLoader().getResource("META-INF").getPath();
		path = path.replace("%20", " ").replace("/bin/META-INF", "").replace("\\bin\\META-INF", "")
				.replace("/build/classes/main/META-INF", "").replace("\\build\\classes\\main\\META-INF", "");

		String file = "\\src\\main\\webapp\\imagens\\" + imagem;

		if (!path.contains("\\")) {
			file = file.replace("\\", "/");
		}
		if (os.startsWith("win") && path.startsWith("/")) {
			path = path.substring(1);
			return (path + file).replace("/", "\\");
		}
		return path + file;

	}

	protected void checarQuantidadeDeElementos(By by, Integer quantidadeEsperada) {
		new WebDriverWait(webDriver, TIMEOUT / 2)
				.until(ExpectedConditions.numberOfElementsToBe(by, quantidadeEsperada));
	}

}