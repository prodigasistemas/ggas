
package br.com.ggas.selenium;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Date;
import org.openqa.selenium.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TesteAtendimentoPublico extends SeleniumTestCase {

	private static final Logger LOG = Logger.getLogger(TesteAtendimentoPublico.class);


	@Test
	public void testeCanalAtendimento() {

		testeInserirCanalAtendimento();
		testeAlterarCanalAtendimento();
		testeRemoverCanalAtendimento();
	}

	private void testeRemoverCanalAtendimento() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Canal de Atendimento removido(s) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarCanalAtendimento() {
		clicarEmElemento(isClickable(By.linkText("TESTE")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Canal de Atendimento')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Canal de Atendimento')]"));
		clicarEmElemento(isClickable(By.xpath("(//input[@id='habilitado'])[2]")));
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("TE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertTrue(obterTextoDoAlerta().matches("^Deseja inativar o Canal de Atendimento[\\s\\S]$"));

	}

	private void testeInserirCanalAtendimento() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='158']/a/span[normalize-space(text()) = 'Canal Atendimento']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.id("incluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Canal de Atendimento')]"));
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Canal de Atendimento inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeEquipe() throws Exception {

		WebElement element = webDriver.findElement(By.xpath("//li[@id='198']/a/span[normalize-space(text()) = 'Equipe']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Equipe')]"));

		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Equipe')]"));
		isClickable(By.id("nome")).clear();
		isClickable(By.id("nome")).sendKeys("BRASKEM SA");
		new Select(isClickable(By.id("unidadeOrganizacional"))).selectByVisibleText("UNIDADE TESTE");
		isClickable(By.id("quantidadeHorasDia")).clear();
		isClickable(By.id("quantidadeHorasDia")).sendKeys("8");

		// obtém o dados da janela principal
		String janelaInicio = webDriver.getWindowHandle();
		
		clicarEmElemento(isClickable(By.id("adicionarComponente")));

		// muda o foco para a janela pesquisar
		for (String winHandle : webDriver.getWindowHandles()) {
			webDriver.switchTo().window(winHandle);
		}
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("ADMINISTRADOR")));
		
		// volta o foco para a janela principal
		webDriver.switchTo().window(janelaInicio);

		obterElementoVisivel(By.xpath("//img[@alt='Excluir Componente']"));
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		
		assertEquals("Sucesso: Equipe inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.id("buttonAlterar")));
		isClickable(By.id("descricaoPlacaVeiculo")).clear();
		isClickable(By.id("descricaoPlacaVeiculo")).sendKeys("PFC5563");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Equipe alterado(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		element = isClickable(By.id("buttonRemover"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Equipe removida(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeMaterial() throws Exception {
		testeInserirMaterial();
		testeAlterarMaterial();
		testeRemoverMaterial();
	}

	private void testeRemoverMaterial() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.cssSelector("input.bottonRightCol.bottonLeftColUltimo")));
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Material removido(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarMaterial() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.cssSelector("fieldset.conteinerBotoes > input.bottonRightCol2")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Material')]"));
		new Select(isClickable(By.id("unidadeMedida"))).selectByVisibleText("Celsius");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Material alterado(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirMaterial() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='199']/a/span[normalize-space(text()) = 'Material']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("MATERIAL TESTE");
		new Select(isClickable(By.id("unidadeMedida"))).selectByVisibleText("Atmosferas");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Material inserido(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testePrioridadeDoTipoDeServico() throws Exception {
		testeInserirPrioridadeDoTipoDeServico();
		testeAlterarPrioridadeDoTipoDeServico();
		testeRemoverPrioridadeDoTipoDeServico();
	}

	private void testeRemoverPrioridadeDoTipoDeServico() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.cssSelector("input.bottonRightCol.bottonLeftColUltimo")));
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Prioridade do Tipo de Serviço removida(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarPrioridadeDoTipoDeServico() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.cssSelector("fieldset.conteinerBotoes > input.bottonRightCol2")));
		isClickable(By.id("quantidadeHorasMaxima")).clear();
		isClickable(By.id("quantidadeHorasMaxima")).sendKeys("6");
		clicarEmElemento(isClickable(By.id("indicadorCorrido")));
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Prioridade do Tipo de Serviço alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirPrioridadeDoTipoDeServico() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='197']/a/span[normalize-space(text()) = 'Prioridade do tipo de serviço']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Prioridade do Tipo de Serviço')]"));
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("INDUSTRIAL TESTE");
		webDriver.findElement(By.id("quantidadeHorasMaxima")).clear();
		webDriver.findElement(By.id("quantidadeHorasMaxima")).sendKeys("4");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Prioridade do Tipo de Serviço inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeTipoDeServico() throws Exception {

		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='196']/a/span[normalize-space(text()) = 'Tipo de Serviço']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		espera(1000);
		obterElementoVisivel(By.id("indicadorPontoConsumoObrigatorio"));
		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Incluir Tipo de Serviço')]"));
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("INCLUSÃO DO TIPO DE SERVIÇO");
		new Select(isClickable(By.id("servicoTipoPrioridade"))).selectByVisibleText("ALTA");
		new Select(isClickable(By.id("documentoImpressaoLayout")))
				.selectByVisibleText("Relatorio de aviso de corte");

		element = waitForElement(By.id("tab-material-necessario"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		espera(1000);
		obterElementoVisivel(By.id("material"));
		new Select(isClickable(By.id("material"))).selectByVisibleText("MATERIAL DEPENDÊNCIA");
		isClickable(By.id("quantidadeMaterial")).clear();
		isClickable(By.id("quantidadeMaterial")).sendKeys("1");
		clicarEmElemento(isClickable(By.id("botaoIncluirMaterial")));

		element = isClickable(By.xpath("//li/a/i[@class='fa fa-calendar-alt']")); ////*[@id="tab-quantidade-agendamento"]/i
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		obterElementoVisivel(By.id("turno"));
		new Select(webDriver.findElement(By.id("turno"))).selectByVisibleText("MANHÃ");
		isClickable(By.id("quantidadeAgendamentosTurno")).clear();
		isClickable(By.id("quantidadeAgendamentosTurno")).sendKeys("1");
		clicarEmElemento(isClickable(By.id("botaoIncluirTurno")));
		clicarEmElemento(isClickable(By.id("buttonSalvar")));

		assertEquals("Sucesso: Tipo(s) de Serviço(s) inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@class='mensagensSpring']/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		clicarEmElemento(waitForElement(By.xpath("//tr/td/div/input[@name='chavesPrimarias']")));
		clicarEmElemento(isClickable(By.id("buttonAlterar")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("ALTERAÇÃO DO TIPO DE SERVIÇO");
		espera();
		clicarEmElemento(waitForElement(By.id("indicadorServicoRegulamento")));
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Tipo(s) de Serviço(s) alterado(a) com sucesso.",
				isClickable(By.xpath("//div[@class='mensagensSpring']/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		espera(1000);
		buscarElemento(By.xpath("//table[@id='table-servicos']/tbody/tr/td"));
		obterElementoVisivel(By.id("indicadorPontoConsumoObrigatorio"));
		clicarEmElemento(buscarElemento(By.xpath("//table[@id='table-servicos']/tbody/tr/td/div/input")));
		clicarEmElemento(isClickable(By.id("buttonRemover")));
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.xpath("//table[@id='table-servicos']/tbody/tr/td")));
		assertEquals("Sucesso: Tipo(s) de Serviço(s) removido(s) com sucesso.",
				webDriver.findElement(By.xpath("//div[@class='mensagensSpring']/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
	}

	@Ignore
	@Test
	public void testeAgendarAutorizacaoDeServico() throws Exception {

		String diaMesAno = Util.converterDataParaStringSemHora(new Date(), Constantes.FORMATO_DATA_BR);

		WebElement element = waitForElement(By.xpath("//li[@id='196']/a/span[normalize-space(text()) = 'Tipo de Serviço']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		espera(1000);
		obterElementoVisivel(By.id("indicadorPontoConsumoObrigatorio"));
		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Incluir Tipo de Serviço')]"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("MANUTENCAO");
		new Select(waitForElement(By.id("servicoTipoPrioridade"))).selectByVisibleText("ALTA");
		new Select(waitForElement(By.id("documentoImpressaoLayout")))
				.selectByVisibleText("Relatorio de aviso de corte");
		isClickable(By.id("quantidadeTempoMedio")).clear();
		isClickable(By.id("quantidadeTempoMedio")).sendKeys("2");
		isClickable(By.id("quantidadePrazoExecucao")).clear();
		isClickable(By.id("quantidadePrazoExecucao")).sendKeys("48");
		element = isClickable(By.xpath("//li/a/i[@class='fa fa-calendar-alt']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		new Select(isClickable(By.id("turno"))).selectByVisibleText("MANHÃ");
		isClickable(By.id("quantidadeAgendamentosTurno")).clear();
		isClickable(By.id("quantidadeAgendamentosTurno")).sendKeys("1");
		clicarEmElemento(isClickable(By.id("botaoIncluirTurno")));
		clicarEmElemento(isClickable(By.name("button")));

		element = waitForElement(
				By.xpath("//li[@id='227']/a/span[normalize-space(text()) = 'Agendar Autorização de Serviço']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoVoltarAutorizacaoServico")));

		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Pesquisar Autorização de Serviço')]"));
		
		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("Manutenção");
		new Select(waitForElement(By.id("servicoTipo"))).selectByVisibleText("MANUTENCAO");
		new Select(waitForElement(By.id("equipe"))).selectByVisibleText("EQUIPE TESTE");
		clicarEmElemento(isClickable(By.xpath("//a[contains(text(),'Cliente')]")));
		isClickable(By.id("nome")).clear();
		isClickable(By.id("nome")).sendKeys("BRASKEM SA");
		isClickable(By.id("nomeFantasia")).clear();
		isClickable(By.id("nomeFantasia")).sendKeys("BRASKEM");
		isClickable(By.id("cpfCnpj")).clear();
		isClickable(By.id("cpfCnpj")).sendKeys("42150391002203");
		isClickable(By.id("email")).clear();
		isClickable(By.id("email")).sendKeys("contato@braskem.com.br");
		clicarEmElemento(isClickable(By.xpath("//a[contains(text(),'Imóvel')]")));
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.cssSelector("td > input[id=\"chaveImovel\"]")));
		clicarEmElemento(isClickable(By.id("checkPontoConsumo")));
		clicarEmElemento(isClickable(By.id("btnSalvar")));

		element = waitForElement(
				By.xpath("//li[@id='227']/a/span[normalize-space(text()) = 'Agendar Autorização de Serviço']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		String janelaInicio = webDriver.getWindowHandle();

		isClickable(By.id("servicoTipoCol1"));

		new Select(isClickable(By.id("servicoTipo"))).selectByVisibleText("MANUTENCAO");
		new Select(isClickable(By.id("servicoTipo"))).selectByVisibleText("RELIGACAO NORMAL");
		new Select(isClickable(By.id("servicoTipo"))).selectByVisibleText("MANUTENCAO");
		
		webDriver.manage().window().maximize();

		isClickable(By.id("calendarioDatePicker"));

		clicarEmElemento(isClickable(By.cssSelector("[data=\"" + diaMesAno + "\"]")));

		isClickable(By.id("total"));
		obterElementoVisivel(By.id("servicoAutorizacaoAgenda"));

		clicarEmElemento(isClickable(By.id("botaoAdicionarAutorizacao")));

		for (String winHandle : webDriver.getWindowHandles()) {
			if (!winHandle.equals(janelaInicio)) {
				webDriver.switchTo().window(winHandle);
			}
		}

		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		espera(2000);
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		espera(2000);
		clicarEmElemento(isClickable(By.id("botaoIncluirAgenda")));

		webDriver.switchTo().window(janelaInicio);
		
		espera(2000);
		takeScreenshot("confirmar_agendamento.png");

		scroll(By.id("botaoImprimir"));
		espera();
		clicarEmElemento(waitForElement(By.id("confirmarAgendamento")));
		assertTrue(obterTextoDoAlerta().matches("^Deseja confirmar o agendamento[\\s\\S]$"));

		scroll(By.id("botaoImprimir"));
		espera();
		clicarEmElemento(waitForElement(By.id("desconfirmarAgendamento")));
		assertTrue(obterTextoDoAlerta().matches("^Deseja desconfirmar o agendamento[\\s\\S]$"));

		scroll(By.id("botaoImprimir"));
		espera();
		clicarEmElemento(waitForElement(By.id("excluirAgendamento")));
		assertTrue(obterTextoDoAlerta().matches("^Deseja excluir o agendamento[\\s\\S]$"));

	}

	@Test
	public void testeQuestionario() throws Exception {
		WebElement element = waitForElement(By.xpath("//li[@id='222']/a/span[normalize-space(text()) = 'Questionário']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		//InserirQuestionario
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Questionário')]"));
		isClickable(By.id("nomeQuestionario")).clear();
		isClickable(By.id("nomeQuestionario")).sendKeys("QUESTIONÁRIO PARA TESTE");
		new Select(isClickable(By.id("segmento"))).selectByVisibleText("COMERCIAL");
		clicarEmElemento(isClickable(By.id("tipoQuestionarioAS")));
		
		isClickable(By.id("nomePergunta")).clear();
		isClickable(By.id("nomePergunta")).sendKeys("PERGUNTA PARA REALIZAÇÃO DE TESTE");
		clicarEmElemento(isClickable(By.cssSelector("[id=\"objetiva\"][value=\"true\"]")));
		isClickable(By.id("notaMinima")).clear();
		isClickable(By.id("notaMinima")).sendKeys("5");
		isClickable(By.id("notaMaxima")).clear();
		isClickable(By.id("notaMaxima")).sendKeys("10");
		clicarEmElemento(isClickable(By.id("botaoAdicionar")));
		clicarEmElemento(isClickable(By.id("botaoIncluir")));

		assertEquals("Sucesso: Questionario inserido(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		//AlterarQuestionario
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Questionário')]"));
		clicarEmElemento(isClickable(By.id("alterarPergunta")));
		clicarEmElemento(isClickable(By.id("botaoAlterarPergunta")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		
		assertEquals("Sucesso: Questionario alterado(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		//RemoverQuestionario
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		assertTrue(super.obterTextoDoAlerta().matches("^Deseja excluir o Questionario[\\s\\S]$"));
		
		assertEquals("Sucesso: Questionario removida(s) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
	}

	@Test
	public void testeChamado() throws Exception {
		
 	testeChamadoTipoAssunto();
 	
	String numeroProtocolo = testeIncluirChamado();
	
	testeGerarRelatorioChamado();
	
	}

	
	private void testeChamadoTipoAssunto() {

		JavascriptExecutor executor = (JavascriptExecutor) webDriver;
		WebElement element = webDriver.findElement(By.xpath("id('193')/a"));
		executor.executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		espera(1000);
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("RES – RECLAMAÇÕES");

		new Select(isClickable(By.id("idSegmentoChamadoTipo"))).selectByVisibleText("RESIDENCIAL");
		
		//isClickable(By.className("pesquisarChamadoAssunto"));

		isClickable(By.id("descricaoAssunto")).sendKeys("FALTA DE GÁS");
		isClickable(By.id("quantidadeHorasPrevistaAtendimento")).sendKeys("08");
		//clicarEmElemento(isClickable(By.cssSelector("input[name=\"indicadorClienteObrigatorio\"][value=\"true\"]")));
		//clicarEmElemento(isClickable(By.cssSelector("input[name=\"indicadorImovelObrigatorio\"][value=\"true\"]")));

		new Select(isClickable(By.id("unidadeOrganizacional"))).selectByVisibleText("UNIDADE TESTE");
		//clicarEmElemento(isClickable(By.cssSelector("input[name=\"indicadorTramitacao\"][value=\"true\"]")));

		clicarEmElemento(isClickable(By.id("botaoIncluirAssunto")));

		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals("Sucesso: Tipo de Chamado com Assunto inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@name='Tipo de Chamado com Assunto inserido(a) com sucesso.']/p")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private String testeIncluirChamado() throws InterruptedException {

		JavascriptExecutor executor = (JavascriptExecutor) webDriver;
		WebElement element = waitForElement(By.xpath("id('191')/a"));
		executor.executeScript("arguments[0].click()", element);
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Pesquisar Chamado')]"));

		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		obterElementoVisivel(By.id("tab-cliente"));

		String numeroProtocolo = buscarElemento(By.id("numeroProtocolo")).getAttribute("value");

		new Select(isClickable(By.id("idSegmentoChamadoTipo"))).selectByVisibleText("RESIDENCIAL");
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("TESTE INCLUSÃO");
		new Select(waitForElement(By.id("chamadoTipo"))).selectByVisibleText("RES RECLAMAÇÕES");
		espera(1500);
		isClickable(waitForElement(By.id("chamadoAssunto")));
		new Select(buscarElemento(By.id("chamadoAssunto"))).selectByVisibleText("FALTA DE GÁS");
		new Select(isClickable(By.id("canalAtendimento"))).selectByVisibleText("AGENCIA VIRTUAL");

		clicarEmElemento(obterElementoVisivel(By.id("tab-cliente")));
		isClickable(By.id("nome")).sendKeys("SEV");
		clicarEmElemento(isClickable(By.xpath("//li[@class='ui-menu-item']")));
		obterTextoDoAlerta();
		elementoPossuiAtributoValor(By.id("cpfCnpj"), "000.417.045-84");
		

		clicarEmElemento(buscarElemento(By.id("tab-imovel")));

		new Select(waitForElement(By.id("chamadoAssunto"))).selectByVisibleText("FALTA DE GÁS");
		new Select(isClickable(By.id("unidadeOrganizacional"))).selectByVisibleText("GDIS OPERACAO");

		clicarEmElemento(isClickable(By.xpath("//label[@for='flagUnidadesVisualizadorasSim']")));
		isClickable(By.id("unidadeOrganizacionalVisualizadoras"));
		new Select(waitForElement(By.id("unidadeOrganizacionalVisualizadoras"))).selectByIndex(1);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));

		assertEquals("Sucesso: Chamado com Número de Protocolo " + numeroProtocolo + " inserido com sucesso.",
				waitForElement(By.xpath("//div[@class='mensagensSpring']/div/p")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
		element = waitForElement(By.xpath("id('191')/a"));
		executor.executeScript("arguments[0].click()", element);

		return numeroProtocolo;
	}
	
	private void testeGerarRelatorioChamado() {
		
		JavascriptExecutor executor = (JavascriptExecutor) webDriver;
		WebElement element = waitForElement(By.xpath("//li[@id=225]/a"));
		executor.executeScript("arguments[0].click()", element);
		
		espera(2000);
		alterarValorAtributo(By.id("dataInicioCriacao"), "01012019");
		alterarValorAtributo(By.id("dataFimCriacao"), "31122030");
		//isClickable(By.xpath("//input[@id='dataInicioCriacao']")).sendKeys("01012019");
		//isClickable(By.xpath("//input[@id='dataFimCriacao']")).sendKeys("31122030");
		clicarEmElemento(isClickable(By.xpath("//input[@class='campoRadio' and @value='ABERTO']")));
		clicarEmElemento(isClickable(By.xpath("//input[@class='bottonRightCol2' and @value='Quantitativo']")));
		clicarEmElemento(isClickable(By.xpath("//input[@class='bottonRightCol' and @value='OK']")));
		espera(3000);
		WebElement mensagemSpring = webDriver.findElement(By.xpath("//div[@class='mensagensSpring']"));
		assertNotEquals("", mensagemSpring.getAttribute("style"));
			

	}
}
