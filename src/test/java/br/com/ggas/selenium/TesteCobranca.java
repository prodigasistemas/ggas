package br.com.ggas.selenium;

import br.com.ggas.util.DataUtil;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TesteCobranca extends SeleniumTestCase {

	@Test
	public void testeControleEntregaDocumento() throws Exception {

		WebElement element = waitForElement(
				By.xpath("//li[@id='202']/a/span[normalize-space(text()) = 'Controle de Entrega de Documento']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Controle de Entrega de Documento')]"));
		obterElementoVisivel(By.id("tipoDocumento"));
		String janelaInicio = super.trocarJanela(By.cssSelector("#botaoPesquisarCliente"));

		super.waitForElement(By.id("nome")).sendKeys("SEVERINO DA SILVA");
		super.waitForElement(By.id("botaoPesquisar")).click();
		waitForElement(By.linkText("SEVERINO DA SILVA")).click();

		webDriver = webDriver.switchTo().window(janelaInicio);

		new Select(waitForElement(By.id("tipoDocumento"))).selectByVisibleText("PROTOCOLO");
		clicarEmElemento(isClickable(By.id("documentos")));
		preencherCampo("dataSituacao", DataUtil.gerarDataAtual());
		new Select(waitForElement(By.id("situacaoEntrega"))).selectByVisibleText("Não Realizada");
		new Select(waitForElement(By.id("motivoNaoEntrega"))).selectByVisibleText("NÃO ENCONTRADO");
		waitForElement(By.id("botaoIncluir")).click();
		assertEquals("Sucesso: Controle de entrega de documento inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
		
		super.selecionarCheckBox(waitForElement(By.id("chavesPrimarias")));
		waitForElement(By.id("botaoAlterar")).click();
		waitForElement(By.xpath("//input[@value='false']")).click();
		waitForElement(By.id("botaoIncluir")).click();
		assertEquals("Sucesso: Controle de entrega de documento alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		super.selecionarCheckBox(waitForElement(By.id("chavesPrimarias")));
		waitForElement(By.name("buttonRemover")).click();
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("chavesPrimarias")));
		assertEquals("Sucesso: Controle de entrega de documento removida(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
	}

	@Test
	public void testeTipoDocumento() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='201']/a/span[normalize-space(text()) = 'Tipo de Documento']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		waitForElement(By.id("botaoIncluir")).click();
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TIPO DE DOCUMENTO TESTE");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("TDT");
		waitForElement(By.name("buttonIncluir")).click();
		assertEquals("Sucesso: Tipo de documento inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
		webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		waitForElement(By.id("chavesPrimarias")).click();
		waitForElement(By.id("buttonAlterar")).click();
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TIPO DE DOCUMENTO TESTE ALTERA");
		waitForElement(By.name("buttonSalvar")).click();
		assertEquals("Sucesso: Tipo de documento alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
		webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		waitForElement(By.id("chavesPrimarias")).click();
		waitForElement(By.name("buttonRemover")).click();
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("chavesPrimarias")));
		assertEquals("Sucesso: Tipo de documento removida(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
	}

	@Test
	public void testePerfilDeParcelamento() throws Exception {
		WebElement element = waitForElement(
				By.xpath("//li[@id='76']/a/span[normalize-space(text()) = 'Perfil de Parcelamento de Débitos']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		waitForElement(By.id("botaoIncluir")).click();
		isClickable(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("PERFIL TESTE");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("TESTES");
		waitForElement(By.id("maximoParcelas")).clear();
		waitForElement(By.id("maximoParcelas")).sendKeys("1");
		preencherCampo("perfilVigenciaInicial", DataUtil.converterDataParaString(new Date()));
		waitForElement(By.id("concedeDescontoSim")).click();
		waitForElement(By.id("valorDesconto")).clear();
		waitForElement(By.id("valorDesconto")).sendKeys("2");
		waitForElement(By.id("aplicaJurosSim")).click();
		waitForElement(By.id("taxaJuros")).clear();
		waitForElement(By.id("taxaJuros")).sendKeys("2");
		new Select(waitForElement(By.id("idAmortizacao"))).selectByVisibleText("PRICE");

		WebElement elemento = waitForElement(By.id("aplicaCorrecaoMonetariaSim"));

		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", elemento);

		new Select(waitForElement(By.id("idIndiceFinanceiro")))
				.selectByVisibleText("BASE - IGP-M IND GERAL PRECOS MERCADO");

		waitForElement(By.xpath("//input[@id='botaoDireitaTodos' and @value='Todos >>' and @tabindex='18']")).click();
		waitForElement(By.xpath("//input[@id='botaoDireitaTodos' and  @value='Todos >>' and @tabindex='24']")).click();

		waitForElement(By.id("botaoIncluir")).click();
		assertEquals("Sucesso: Perfil de Parcelamento inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		waitForElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		waitForElement(By.id("botaoAlterar")).click();
		waitForElement(By.id("descricao")).sendKeys("PERFIL TESTE ALTERAR");
		waitForElement(By.id("botaoSalvar")).click();
		assertEquals("Sucesso: Perfil de Parcelamento alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		waitForElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		waitForElement(By.name("buttonRemover")).click();
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Perfil de Parcelamento removido(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeEmissaoSegundaViaAvisoDeCorte() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='229']/a/span[normalize-space(text()) = 'Emissão 2-Via Aviso Corte']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		waitForElement(By.xpath("//input[@id='botaoPesquisar' and @tabindex='5' and @tabindex='5']")).click();
		waitForElement(By.cssSelector("img[alt=\"Emitir 2 Via\"]")).click();
	}
	
	
	@Test
	public void testeDesabilitarAvisoDeCorte() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='301']/a/span[normalize-space(text()) = 'Controle de Aviso de Corte']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		isClickable(By.id("numeroFatura")).sendKeys("2876");

		waitForElement(By.xpath("//button[@id='botaoPesquisar' and @tabindex='24' and @tabindex='24']")).click();
		waitForElement(By.xpath("//div[@data-identificador-check='chk2876']")).click();
		waitForElement(By.xpath("//button[@id='buttonIncluirChamadosEmLote']")).click();
		
		assertEquals("Sucesso: Fatura(s) para o Aviso de Corte desabilitado(s) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));
				
	}
	
	@Test
	public void testeHabilitarAvisoDeCorte() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='301']/a/span[normalize-space(text()) = 'Controle de Aviso de Corte']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		isClickable(By.id("numeroFatura")).sendKeys("2876");
		waitForElement(By.xpath("//button[@id='botaoPesquisar' and @tabindex='24' and @tabindex='24']")).click();
		waitForElement(By.xpath("//div[@data-identificador-check='chk2876']")).click();
		waitForElement(By.xpath("//button[@id='buttonIncluir']")).click();
		
		assertEquals("Sucesso: Fatura(s) para o Aviso de Corte habilitado(s) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));
				
	}
	
	
	@Test
	public void testeBuscaAvisoCorteSemRegistros() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='301']/a/span[normalize-space(text()) = 'Controle de Aviso de Corte']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		isClickable(By.id("numeroFatura")).sendKeys("2877");

		waitForElement(By.xpath("//button[@id='botaoPesquisar' and @tabindex='24' and @tabindex='24']")).click();	
		assertEquals("Nenhum registro foi encontrado",
		waitForElement(By.xpath("//td[@class='dataTables_empty']")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));
				
	}
	
	@Test
	public void testePriorizacaoCorte() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='302']/a/span[normalize-space(text()) = 'Priorização e Liberação de Faturas']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		waitForElement(By.xpath("//button[@id='botaoPesquisar' and @tabindex='5' and @tabindex='5']")).click();
		waitForElement(By.xpath("//div[@data-identificador-check='chk99999']")).click();
		waitForElement(By.xpath("//button[@id='botaoRoterizar' and @tabindex='9' and @tabindex='9']")).click();
		
		assertEquals("Sucesso: !Priorização de Corte salvo com sucesso!",
				isClickable(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));
		
	}
	
	@Test
	public void testeRoteirizacaoCorte() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='303']/a/span[normalize-space(text()) = 'Roteirização Faturas']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		waitForElement(By.xpath("//button[@id='botaoEmitirAS' and @tabindex='4' and @tabindex='4']")).click();
		
		assertEquals("Sucesso: !Processo para gerar Autorizaçoes de Serviço em Lote inserido com sucesso!",
				isClickable(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));
		
	}
	
/*
	@Test
	public void testDeclaracaoQuitacaoAnual(){
		
		inserirDependenciaLogotipoParaTeste();
		inserirDependenciaFaturamentoCreditoDebitoParaTeste();
		inserirDependenciaArrecadacaoRecebimentoParaTeste();
		testeGerarDeclaracaoQuitacaoAnual(); 
		
	}
	
	private void inserirDependenciaLogotipoParaTeste() {
		WebElement element = waitForElement(By.xpath("//li[@id='24']/a[normalize-space(text()) = 'Empresa']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		String path = super.getPathImagem("ggas_marca_login.jpg");
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("ALGAS - GAS DE ALAGOAS SA")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));

		waitForElement(By.id("logoEmpresa")).clear();
		waitForElement(By.id("logoEmpresa")).sendKeys(path);
		
		clicarEmElemento(isClickable(By.cssSelector("input[name=\"right\"][value=\"Todos >>\"]")));
		
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
	}
	
	private void inserirDependenciaFaturamentoCreditoDebitoParaTeste() {
		WebElement element = waitForElement(By.xpath("//li[@id='69']/a[normalize-space(text()) = 'Crédito / Débito a Realizar']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		String janelaPrimaria = webDriver.getWindowHandle();
		
		clicarEmElemento(isClickable(By.id("indicadorPesquisaCliente")));
		clicarEmElemento(isClickable(By.id("botaoPesquisarCliente")));
		
		for (String janelaSecundaria : webDriver.getWindowHandles()) {
			if(!janelaSecundaria.equals(janelaPrimaria)) {
				webDriver.switchTo().window(janelaSecundaria);
			}
		}
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("SEVERINO DA SILVA")));
		
		webDriver.switchTo().window(janelaPrimaria);
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		clicarEmElemento(isClickable(By.id("idPontoConsumo11038")));
		clicarEmElemento(isClickable(By.id("indicadorDebitoSim")));
		
		new Select(waitForElement(By.id("idRubrica"))).selectByValue("26");
		waitForElement(By.id("quantidade")).clear();
		waitForElement(By.id("quantidade")).sendKeys("1");
		
		executarScript("atualizarValor()", waitForElement(By.id("valorUnitario")));		
		
		waitForElement(By.id("valorEntrada")).clear();
		waitForElement(By.id("valorEntrada")).sendKeys("50");
		waitForElement(By.id("parcelas")).clear();
		waitForElement(By.id("parcelas")).sendKeys("1");
		new Select(waitForElement(By.id("idPeriodicidade"))).selectByValue("82");
		waitForElement(By.id("complementoDescricao")).clear();
		waitForElement(By.id("complementoDescricao")).sendKeys("TESTE INCLUIR DEBITO");
		clicarEmElemento(isClickable(By.id("indicadorInicioCobrancaDias")));
		waitForElement(By.id("qtdDiasCobranca")).clear();
		waitForElement(By.id("qtdDiasCobranca")).sendKeys("1");
		new Select(waitForElement(By.id("idInicioCobranca"))).selectByValue("193");
		clicarEmElemento(isClickable(By.id("botaoGerarParcelas")));
		waitForElement(By.id("dadosParcela"));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
	}
	
	private void inserirDependenciaArrecadacaoRecebimentoParaTeste() {
		WebElement element = waitForElement(By.xpath("//li[@id='74']/a[normalize-space(text()) = 'Recebimento']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		String janelaPrimaria = webDriver.getWindowHandle();
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		
		clicarEmElemento(isClickable(By.id("indicadorPesquisaCliente")));
		clicarEmElemento(isClickable(By.id("botaoPesquisarCliente")));
		
		for (String janelaSecundaria : webDriver.getWindowHandles()) {
			if(!janelaSecundaria.equals(janelaPrimaria)) {
				webDriver.switchTo().window(janelaSecundaria);
			}
		}
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("SEVERINO DA SILVA")));
		
		webDriver.switchTo().window(janelaPrimaria);
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		
		new Select(waitForElement(By.id("idArrecadador"))).selectByValue("8");
		new Select(waitForElement(By.id("formaArrecadacao"))).selectByValue("575");
		
		waitForElement(By.id("valorRecebimento")).clear();
		waitForElement(By.id("valorRecebimento")).sendKeys("50");
		clicarEmElemento(isClickable(By.cssSelector("input[name=\"chavesPrimariasFaturas\"][tabindex=\"13\"][type=\"checkbox\"]")));
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		
	}
	
	private void testeGerarDeclaracaoQuitacaoAnual() {
		WebElement element = waitForElement(By.xpath("//li[@id='79']/a[normalize-space(text()) = 'Declaração de Quitação Anual']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		int anoAtual = Calendar.getInstance().get(Calendar.YEAR) - 1;
		
		String janelaPrimaria = webDriver.getWindowHandle();
		
		clicarEmElemento(isClickable(By.id("indicadorPesquisaGeral")));
		new Select(waitForElement(By.id("anoGeracao"))).selectByValue(Integer.toString(anoAtual));
		clicarEmElemento(isClickable(By.id("botaoPesquisarGerar")));
		
		assertEquals("Sucesso: O processo de geração da declaração de quitação anual de débitos foi iniciado.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		
		clicarEmElemento(isClickable(By.id("indicadorPesquisaCliente")));
		clicarEmElemento(isClickable(By.id("botaoPesquisarCliente")));
		
		for (String janelaSecundaria : webDriver.getWindowHandles()) {
			if(!janelaSecundaria.equals(janelaPrimaria)) {
				webDriver.switchTo().window(janelaSecundaria);
			}
		}
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("SEVERINO DA SILVA")));
		
		webDriver.switchTo().window(janelaPrimaria);
		
		new Select(waitForElement(By.id("anoGeracao"))).selectByValue(Integer.toString(anoAtual));
		
		clicarEmElemento(isClickable(By.id("botaoPesquisarGerar")));
		
		waitForElement(By.id("botaoPesquisarGerar"));
		
	}
	*/
	
	@Test
	public void testeParcelamento() throws Exception {

		WebElement element = waitForElement(By.xpath("//li[@id='77']/a/span[normalize-space(text()) = 'Parcelamento']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		//obtendo o identificador da janela principal
		String janelaPrincipal = webDriver.getWindowHandle();
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Parcelamento de Débitos - Selecionar Pontos de Consumo')]"));
		
		clicarEmElemento(isClickable(By.cssSelector("#indicadorPesquisaImovel")));
		clicarEmElemento(isClickable(By.cssSelector("#indicadorPesquisaCliente")));
		
		obterElementoVisivel(By.id("botaoPesquisarCliente"));
		clicarEmElemento(buscarElemento(By.id("botaoPesquisarCliente")));
		
		for(String windowHandle : webDriver.getWindowHandles()) {
			//mudando o foco para janela secundaria
		    if( !windowHandle.equals(janelaPrincipal) ) {
		         webDriver.switchTo().window(windowHandle);
		    }
		}
		
		waitForElement(By.id("nome")).sendKeys("SEVERINO DA SILVA");
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		
		clicarEmElemento(isClickable(By.linkText("SEVERINO DA SILVA")));
		
		//mudando o foco para janela principal
		webDriver.switchTo().window(janelaPrincipal); 
		
		clicarEmElemento(isClickable(scroll(By.id("botaoPesquisar"))));
		
		clicarEmElemento(isClickable(scroll(By.linkText("201503911-00"))));
		
		String id = waitForElement((By.cssSelector("input[type=\"checkbox\"][tabindex=\"4\"]"))).getAttribute("id");
		
		clicarEmElemento(isClickable(waitForElement(By.id(id))));
		
		new Select(waitForElement(By.id("idPerfilParcelamento")))
				.selectByVisibleText("PERFIL PARCELAMENTO DE TESTE");
		
		waitForElement(By.id("numeroParcelas")).clear();
		waitForElement(By.id("numeroParcelas")).sendKeys("2");
		clicarEmElemento(isClickable(waitForElement(By.id("botaoGerarParcelas"))));
		
		obterElementoVisivel(By.id("botaoSalvar"));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Parcelamento efetuado com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
}
