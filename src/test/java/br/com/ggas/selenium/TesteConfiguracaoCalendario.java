package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteConfiguracaoCalendario extends SeleniumTestCase {

	@Test
	public void testConfiguracaoCalendario() throws Exception {
		
		WebElement element = webDriver.findElement(By.xpath("//li[@id='163']/a/span[normalize-space(text()) = 'Calendário']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		espera(2000);

		clicarEmElemento(isClickable(By.xpath("(//a[contains(text(),'24')])[6]")));
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("SÃO JOÃO");
		clicarEmElemento(isClickable(By.xpath("//input[@id='indicadorTipo' and @value='false']")));
		clicarEmElemento(isClickable(By.xpath("//input[@id='indicadorMunicipal' and @value='true']")));
		new Select(webDriver.findElement(By.id("idMunicipio"))).selectByVisibleText("RECIFE");
		clicarEmElemento(isClickable(By.id("adicionarCampos")));
		assertEquals("Sucesso: Feriado inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
	}
}
