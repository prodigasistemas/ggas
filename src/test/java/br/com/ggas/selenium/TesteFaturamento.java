package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Util;

public class TesteFaturamento extends SeleniumTestCase {

	private static final Logger LOG = Logger.getLogger(TesteFaturamento.class);

	WebElement calendario;
	@Test
	public void testeGrupoFaturamento() {

		testeInserirGrupoFaturamento();
		testeAlterarGrupoFaturamento();
		testeRemoverGrupoFaturamento();
	}

	private void testeRemoverGrupoFaturamento() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));

		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		assertEquals(
				isClickable(By.className("notification")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""),
				"Sucesso: Grupo de Faturamento removido(s) com sucesso.");
	}

	private void testeAlterarGrupoFaturamento() {
		clicarEmElemento(isClickable(By.linkText("TESTE FATURA")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Grupo de Faturamento')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.id("descricao"));
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals(isClickable(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""), "Sucesso: Grupo de Faturamento alterado(a) com sucesso.");
	}

	private void testeInserirGrupoFaturamento() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='156']/a/span[normalize-space(text()) = 'Grupo Faturamento']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Grupo de Faturamento')]"));

		clicarEmElemento(isClickable(By.id("incluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Grupo de Faturamento')]"));

		obterElementoVisivel(By.id("descricao"));
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("TESTE FATURA");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("TES");
		isClickable(By.id("numeroCiclo")).clear();
		isClickable(By.id("numeroCiclo")).sendKeys("01");
		isClickable(By.id("diaVencimento")).clear();
		isClickable(By.id("diaVencimento")).sendKeys("15");
		
		espera();
		new Select(waitForElement(By.id("periodicidade"))).selectByVisibleText("MENSAL COLETOR");
		new Select(waitForElement(By.id("tipoLeitura"))).selectByVisibleText("PLANILHA");
		
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals(isClickable(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""), "Sucesso: Grupo de Faturamento inserido(a) com sucesso.");
	}

	@Test
	public void testeCronogramaFaturamento() {

		testeInserirCronogramaFaturamento();
		testeAlterarCronogramaFaturamento();
	}

	private void testeInserirCronogramaFaturamento() {
		
		WebElement element = waitForElement(By.xpath("//li[@id='71']/a/span[contains(text(),'Cronograma')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		int anoAtual = Calendar.getInstance().get(Calendar.YEAR);
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Incluir Cronograma')]"));

		espera(3000);
		new Select(isClickable(By.id("grupoFaturamento"))).selectByVisibleText("GRUPO ORUBE");
		
		isClickable(By.id("quantidadeCronogramas")).clear();
		isClickable(By.id("quantidadeCronogramas")).sendKeys("2");
		
		clicarEmElemento(isClickable(By.xpath("(//input[@name='chavesPrimariasMarcadas'])[1]")));
		isClickable(By.id("duracao1")).clear();
		isClickable(By.id("duracao1")).sendKeys("2");
		
		alterarValorAtributo(calendario = isClickable(By.id("datasPrevista1")), "05/09/" + (anoAtual-1));
		
		clicarEmElemento(isClickable(By.xpath("(//input[@name='chavesPrimariasMarcadas'])[3]")));
		isClickable(By.id("duracao2")).clear();
		isClickable(By.id("duracao2")).sendKeys("2");
		
		alterarValorAtributo(calendario = isClickable(By.id("datasPrevista2")), "05/09/" + (anoAtual-1));
		
		clicarEmElemento(isClickable(By.xpath("(//input[@name='chavesPrimariasMarcadas'])[4]")));
		isClickable(By.id("duracao3")).clear();
		isClickable(By.id("duracao3")).sendKeys("2");
		
		alterarValorAtributo(calendario = isClickable(By.id("datasPrevista3")), "05/09/" + (anoAtual-1));
		
		clicarEmElemento(isClickable(By.xpath("(//input[@name='chavesPrimariasMarcadas'])[5]")));
		isClickable(By.id("duracao4")).clear();
		isClickable(By.id("duracao4")).sendKeys("2");
		
		alterarValorAtributo(calendario = isClickable(By.id("datasPrevista4")), "09/09/" + (anoAtual-1));
		
		clicarEmElemento(isClickable(By.xpath("(//input[@name='chavesPrimariasMarcadas'])[9]")));
		isClickable(By.id("duracao8")).clear();
		isClickable(By.id("duracao8")).sendKeys("3");
		
		alterarValorAtributo(calendario = isClickable(By.id("datasPrevista8")), "17/09/" + (anoAtual-1));
		
		clicarEmElemento(isClickable(By.xpath("(//input[@name='chavesPrimariasMarcadas'])[11]")));
		isClickable(By.id("duracao9")).clear();
		isClickable(By.id("duracao9")).sendKeys("2");

		alterarValorAtributo(calendario = isClickable(By.id("datasPrevista9")), "19/09/" + (anoAtual-1));
		
		clicarEmElemento(isClickable(By.xpath("(//input[@name='chavesPrimariasMarcadas' and @value='19'])")));
		espera(1000);
		isClickable(By.id("duracao19")).clear();
		isClickable(By.id("duracao19")).sendKeys("7");
		
		alterarValorAtributo(calendario = isClickable(By.id("datasPrevista19")), "30/09/" + (anoAtual-1));

		clicarEmElemento(isClickable(By.id("buttonGerar")));
		
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Incluir Cronograma - Resumo por Ciclo')]"));
		
		clicarEmElemento(isClickable(By.id("buttonGerar")));
		
		assertEquals("Sucesso: Cronograma Faturamento inserido(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	private void testeAlterarCronogramaFaturamento() {
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("GRUPO ORUBE")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Detalhar Cronograma')]"));
		clicarEmElemento(isClickable(By.id("buttonAlterar")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Alterar Cronograma')]"));
		isClickable(By.id("duracao2")).clear();
		isClickable(By.id("duracao2")).sendKeys("3");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		
		assertEquals("Sucesso: Cronograma Faturamento alterado(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeMensagemDeFaturamento() throws Exception {
		
		// obtém o dados da janela principal
		String janelaInicio = webDriver.getWindowHandle();
		
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Mensagens de Faturamento')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		//teste Inserir MensagemDeFaturamento;
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Mensagens de Faturamento')]"));

		new Select(isClickable(By.id("idTipoMensagem"))).selectByVisibleText("COMERCIAL");
		Date dataInicioVigencia = new Date();
		Date dataFimVigencia = DataUtil.incrementarDia(dataInicioVigencia, 11);
		preencherCampo("dataInicioVigencia", DataUtil.converterDataParaString(dataInicioVigencia));
		preencherCampo("dataFinalVigencia", DataUtil.converterDataParaString(dataFimVigencia));

		new Select(isClickable(By.id("dataVencimentoDisponiveis"))).selectByValue("7");
		clicarEmElemento(isClickable(By.id("conteinerDiaVencimento")));
		
		new Select(isClickable(By.id("segmentosDisponiveis"))).selectByValue("2");
		clicarEmElemento(isClickable(By.id("botaoMoverDireitaConteinerSegmentos")));
		clicarEmElemento(isClickable(By.id("botaoPesquisarImovel")));

		// muda o foco para a janela secundaria
		for (String winHandle : webDriver.getWindowHandles()) {
			if(!winHandle.equals(janelaInicio)) {
				webDriver.switchTo().window(winHandle);
			}
		}

		isClickable(By.id("nomeFantasia")).clear();
		isClickable(By.id("nomeFantasia")).sendKeys("BRASKEM POLO");
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("BRASKEM POLO")));

		// volta o foco para a janela principal
		webDriver.switchTo().window(janelaInicio);
		
		obterElementoVisivel(By.id("mensagem"));
		isClickable(By.id("mensagem")).clear();
		isClickable(By.id("mensagem")).sendKeys("MENSAGEM DE FATURAMENTO CRIADA PARA REALIZAÇÃO DE TESTES.");

		new Select(isClickable(By.id("formaCobrancaDisponiveis"))).selectByValue("575");
		clicarEmElemento(isClickable(By.id("botaoMoverDireitaConteinerFormasCobranca")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Mensagens de Faturamento inserido(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		//teste Alterar MensagemDeFaturamento;
		
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		dataFimVigencia = DataUtil.incrementarDia(dataInicioVigencia, 12);
		preencherCampo("dataFinalVigencia", DataUtil.converterDataParaString(dataFimVigencia));
		clicarEmElemento(isClickable(By.xpath("//input[@value='Salvar']")));
		
		assertEquals("Sucesso: Mensagens de Faturamento alterado(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		//teste Remover MensagemDeFaturamento;

		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		assertEquals("Sucesso: Mensagens de Faturamento removida(s) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testePeriodicidade() {
		testeInserirPeriodicidade();
		testeAlterarPeriodicidade();
		testeRemoverPeriodicidade();
	}
		
	private void testeRemoverPeriodicidade() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		assertEquals("Sucesso: Periodicidade removida(s) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarPeriodicidade() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Periodicidade')]"));

		obterElementoVisivel(By.id("descricao"));
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("MENSAL TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Periodicidade alterado(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirPeriodicidade() {

		espera();
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Periodicidade')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		espera();
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Periodicidade')]"));

		obterElementoVisivel(By.id("descricao"));
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("MENSAL");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("MEN");
		isClickable(By.id("quantidadeDias")).clear();
		isClickable(By.id("quantidadeDias")).sendKeys("30");
		isClickable(By.id("numeroCiclo")).clear();
		isClickable(By.id("numeroCiclo")).sendKeys("4");
		isClickable(By.id("numeroMinimoDiasCiclo")).clear();
		isClickable(By.id("numeroMinimoDiasCiclo")).sendKeys("15");
		isClickable(By.id("numeroMaximoDiasCiclo")).clear();
		isClickable(By.id("numeroMaximoDiasCiclo")).sendKeys("30");
		isClickable(By.id("indicadorConsideraMesCivil")).click();
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Periodicidade inserido(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testePrecoGas() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='62']/a/span[normalize-space(text()) = 'Registro de Preço do Gás']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		isClickable(By.id("numeroNotaFiscal")).clear();
		isClickable(By.id("numeroNotaFiscal")).sendKeys("1");
		
		new Select(isClickable(By.id("idContratoCompra"))).selectByVisibleText("CLP");
		preencherCampo("dataFornecimentoInicio", "01042017");
		preencherCampo("dataFornecimentoFim", "01042017");
		preencherCampo("dataEmissaoNotaFiscal", "01042017");
		new Select(isClickable(By.id("idItemFatura"))).selectByVisibleText("GÁS");
		preencherCampo("dataVencimento", "01042017");
		isClickable(By.id("volume")).clear();
		isClickable(By.id("volume")).sendKeys("100");
		isClickable(By.id("valor")).clear();
		isClickable(By.id("valor")).sendKeys("13");
		clicarEmElemento(isClickable(By.id("botaoAdicionar")));
		waitForElement(By.xpath("//img[@alt='Exluir Tronco']"));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Preço Gás inserido(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
						
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Preço Gás alterado(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		assertEquals("Sucesso: Preço Gás removido(s) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeIndiceFinanceiro() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='61']/a/span[normalize-space(text()) = 'Índice Financeiro']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(isClickable(By.id("idIndiceFinanceiro"))).selectByVisibleText("IGP-M IND GERAL PRECOS MERCADO");
		clicarEmElemento(isClickable(By.cssSelector("option[value=\"1\"]")));
		new Select(isClickable(By.id("mesInicio"))).selectByVisibleText("Setembro");
		new Select(isClickable(By.id("anoInicio"))).selectByVisibleText("2016");
		new Select(isClickable(By.id("mesFim"))).selectByVisibleText("Abril");
		new Select(isClickable(By.id("anoFim"))).selectByVisibleText("2017");
		clicarEmElemento(isClickable(By.name("Button")));
		isClickable(By.xpath("//input[@id='valorIndice' and @tabindex='8']")).sendKeys("1.0");
		isClickable(By.xpath("//input[@id='valorIndice' and @tabindex='9']")).sendKeys("2.0");
		isClickable(By.xpath("//input[@id='valorIndice' and @tabindex='10']")).sendKeys("3.0");
		isClickable(By.xpath("//input[@id='valorIndice' and @tabindex='11']")).sendKeys("4.0");
		isClickable(By.xpath("//input[@id='valorIndice' and @tabindex='12']")).sendKeys("5.0");
		isClickable(By.xpath("//input[@id='valorIndice' and @tabindex='13']")).sendKeys("6.0");
		isClickable(By.xpath("//input[@id='valorIndice' and @tabindex='14']")).sendKeys("7.0");
		isClickable(By.xpath("//input[@id='valorIndice' and @tabindex='15']")).sendKeys("8.0");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Índice Financeiro atualizado(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeRubrica() throws Exception {
		WebElement element = waitForElement(By.xpath("//li[@id='58']/a/span[normalize-space(text()) = 'Rubrica']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Rubrica')]"));
		waitForElement(By.id("radioRegulamentadaSim"));
		isClickable(By.id("radioRegulamentadaSim")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("RUBRICA TESTE");
		isClickable(By.id("descricaoImpressao")).clear();
		isClickable(By.id("descricaoImpressao")).sendKeys("Incluir rubrica para test");
		isClickable(By.id("radioRegulamentadaSim")).click();
		isClickable(By.id("valorReferencia")).clear();
		isClickable(By.id("valorReferencia")).sendKeys("1500");
		new Select(isClickable(By.id("unidade"))).selectByVisibleText("Metro Cúbico");
		new Select(isClickable(By.id("tipoRubrica"))).selectByVisibleText("DOACOES");
		new Select(isClickable(By.id("lancamentoItemContabil"))).selectByVisibleText("Penalidade Retirada a Maior");
		new Select(isClickable(By.id("nomeclaturaComumMercosul"))).selectByVisibleText("GÁS NATURAL LIQUEFEITO");
		clicarEmElemento(isClickable(By.id("botaoDireitaTodos")));
		preencherCampo("dataInicioVigencia", DataUtil.converterDataParaString(new Date()));
		obterElementoVisivel(By.id("observacao"));
		isClickable(By.id("observacao")).clear();
		isClickable(By.id("observacao")).sendKeys("REALIZACAO DE TESTES");
		isClickable(By.id("numeroMaximoParcelas")).clear();
		isClickable(By.id("numeroMaximoParcelas")).sendKeys("4");
		isClickable(By.id("percentualMinimoEntrada")).clear();
		isClickable(By.id("percentualMinimoEntrada")).sendKeys("15");
		new Select(isClickable(By.id("sistemaAmortizacao"))).selectByVisibleText("PRICE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Rubrica inserido(a) com sucesso.", isClickable(By.className("notification"))
				.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Rubrica')]"));
		obterElementoVisivel(By.id("descricaoImpressao"));
		isClickable(By.id("descricaoImpressao")).clear();
		isClickable(By.id("descricaoImpressao")).sendKeys("Alterar rubrica para test");
		isClickable(By.xpath("//input[@id='indicadorUso' and @value='false']")).click();
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Rubrica alterado(a) com sucesso.", isClickable(By.className("notification"))
				.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.id("habilitadoTodos")));
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		
		assertEquals("Sucesso: Rubrica removida(s) com sucesso.", isClickable(By.className("notification"))
				.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	@Ignore
	@Test
	public void testeCreditoDebitoRealizar() throws Exception {

		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='69']/a/span[normalize-space(text()) = 'Crédito / Débito a Realizar']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("indicadorPesquisaImovel")));
		clicarEmElemento(isClickable(By.id("indicadorPesquisaCliente")));

		String janelaInicial = super.trocarJanela(By.id("botaoPesquisarCliente"));

		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("ALGAS - GAS DE ALAGOAS SA")));

		webDriver.switchTo().window(janelaInicial);

		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.id("botaoIncluir")));

		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Crédito / Débito a Realizar')]"));


		obterElementoVisivel(By.id("quantidade"));
		obterElementoVisivel(By.id("parcelas"));
		obterElementoVisivel(By.id("indicadorDebitoSim"));
		obterElementoVisivel(By.id("idRubrica"));
		isClickable(By.id("quantidade")).clear();
		isClickable(By.id("quantidade")).sendKeys("24");
		isClickable(By.id("parcelas")).clear();
		isClickable(By.id("parcelas")).sendKeys("5");
		clicarEmElemento(isClickable(By.id("indicadorDebitoSim")));
		obterElementoVisivel(By.id("valorEntrada"));
		new Select(isClickable(By.id("idRubrica"))).selectByVisibleText("ICMS Retido");
		isClickable(By.id("valorUnitario")).clear();
		isClickable(By.id("valorUnitario")).sendKeys("24,0000");
		isClickable(By.id("valorEntrada")).clear();
		isClickable(By.id("valorEntrada")).sendKeys("2,00");


		new Select(isClickable(By.id("idPeriodicidade"))).selectByVisibleText("MENSAL");
		isClickable(By.id("melhorDiaVencimento"));
		new Select(isClickable(By.id("melhorDiaVencimento"))).selectByVisibleText("1");
		preencherCampo("dataInicioCobranca", DataUtil.converterDataParaString(new Date()));
		clicarEmElemento(isClickable(By.id("botaoGerarParcelas")));
		obterElementoVisivel(By.cssSelector("input.bottonRightCol2.botaoGrande1"));
		clicarEmElemento(isClickable(By.cssSelector("input.bottonRightCol2.botaoGrande1")));

		assertEquals("Sucesso: Crédito / Débito efetuado com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		obterElementoVisivel(By.linkText("Em Andamento"));
		clicarEmElemento(isClickable(isClickable(By.name("chavesPrimarias"))));
		WebElement botaoCancelar = obterElementoVisivel(By.xpath("//input[@value='Cancelar Lançamento']"));
		clicarEmBotao(botaoCancelar);
		new Select(obterElementoVisivel(By.id("motivoCancelamento"))).selectByVisibleText("ERRO OPERACIONAL");
		clicarEmElemento(isClickable(By.cssSelector("#cancelamentoLancamentoPopup > input.bottonRightCol2")));
		obterElementoVisivel(By.linkText("Cancelado"));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Crédito / Débito a Realizar')]"));
		assertEquals("Sucesso: Lançamento cancelado com sucesso", isClickable(By.xpath("//div[@id='inner']/div/div"))
				.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));

	}

	@Test
	public void testeTarifaTributo() {
		
		String data = Util.getDataCorrente();
		
		WebElement element = waitForElement(By.xpath("//li[@id='134']/a/span[normalize-space(text()) = 'Tributo']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("incluir")));
		obterElementoVisivel(By.id("indicadorProduto"));
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("NOVO TRIBUTO");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("NT");
		executarScript("arguments[0].style.display = 'none';", waitForElement(By.xpath("//fieldset[@id='pesquisarTabelaAuxiliarCol2']")));
		new Select(waitForElement(By.xpath("//select[@name='esferaPoder']"))).selectByVisibleText("FEDERAL");
		executarScript("arguments[0].style.display = 'block';", waitForElement(By.xpath("//fieldset[@id='pesquisarTabelaAuxiliarCol2']")));
		clicarEmElemento(isClickable(isClickable(By.id("indicadorProduto"))));

		preencherCampo("dataVigencia", data);
		new Select(isClickable(By.id("idValoresAliquota"))).selectByVisibleText("Percentual (%)");
		isClickable(By.id("conteudoValorTributoAliquota")).clear();
		isClickable(By.id("conteudoValorTributoAliquota")).sendKeys("20");
		new Select(isClickable(By.id("idMunicipio"))).selectByVisibleText("RECIFE");
		clicarEmElemento(isClickable(By.id("botaoIncluirTributoAliquota")));
		waitForElement(By.xpath("//img[@alt='Excluir Tributo Alíquota']"));
		clicarEmElemento(isClickable(isClickable(By.id("botaoSalvar"))));
		
		assertEquals("Sucesso: Tributo inserido(a) com sucesso.", isClickable(By.className("notification"))
				.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(isClickable(By.id("checkboxChavesPrimarias"))));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Tributo')]"));
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("TRIBUTO NOVO");
		clicarEmElemento(isClickable(isClickable(By.id("botaoSalvar"))));
		
		assertEquals("Sucesso: Tributo alterado(a) com sucesso.", isClickable(By.className("notification"))
				.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(isClickable(By.id("checkboxChavesPrimarias"))));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		obterTextoDoAlerta();
		assertEquals("Sucesso: Tributo removido(s) com sucesso.", isClickable(By.className("notification"))
				.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}
	
	@Ignore
	@Test
	public void testeNotaCreditoDebito() throws Exception {

		Integer quantidade = 2;
		Double valorUnitario = 25.0;

		super.clicarEmElemento(waitForElement(
				By.xpath("//li[@id='68']/a/span[normalize-space(text()) = 'Notas de Crédito / Débito']")));

		clicarEmElemento(isClickable(By.id("indicadorPesquisaCliente")));
		espera();
		isClickable(By.id("botaoPesquisarCliente"));
		String janelaPrincipal = super.trocarJanela(By.id("botaoPesquisarCliente"));

		isClickable(By.id("nome")).sendKeys("BRASKEM SA");
		isClickable(By.id("botaoPesquisar")).click();
		isClickable(By.linkText("BRASKEM SA")).click();

		webDriver = webDriver.switchTo().window(janelaPrincipal);

		super.elementoPossuiAtributoValor(By.id("nomeClienteTexto"), "BRASKEM SA");

		super.clicarEmElemento(isClickable(By.name("botaoIncluir")));
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Nota de Crédito / Débito')]"));
		
		isClickable(By.id("indicadorDebitoSim")).click();

		super.clicarEmElemento(isClickable(By.xpath("//td[contains(text(), 'TESTE - TESTE')]/../td/input")));

		new Select(isClickable(By.id("idRubrica"))).selectByVisibleText("Juros de mora");
		isClickable(By.id("quantidade")).clear();
		isClickable(By.id("quantidade")).sendKeys(String.valueOf(quantidade));
		isClickable(By.id("valorTotal")).clear();
		isClickable(By.id("valorTotal")).sendKeys(super.decimalParaString(valorUnitario, ','));
		
		super.alterarValorAtributo(isClickable(By.id("dataVencimento")),
				DataUtil.converterDataParaString(new Date()));
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));

		LOG.info("Mensagem de alerta teste Notas de crédito/débito: " + obterTextoDoAlerta());

		assertEquals("Sucesso: Nota de Crédito / Débito inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));

		Double valor = valorUnitario * quantidade;
		super.isClickable(By.partialLinkText(String.valueOf(valor.intValue()))).click();
		super.isClickable(By.cssSelector("input[value='Voltar']")).click();

		super.selecionarCheckBox(super.isClickable(By.name("chavesPrimarias")));
		super.clicarEmElemento(webDriver.findElement(By.id("prorrogarNota")));
		String dataProrrogacao = DataUtil.converterDataParaString(DataUtil.incrementarDia(new Date(), 1));
		isClickable(By.id("dataProrrogacao")).sendKeys(dataProrrogacao);
		isClickable(By.name("buttonSalvar")).click();

		assertEquals("Sucesso: Nota(s) Crédito/Débito Prorrogada(s) com Sucesso.",
				obterElementoVisivel(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));

		new Select(isClickable(By.id("idSituacao"))).selectByVisibleText("NORMAL");
		super.isClickable(By.id("botaoPesquisar")).click();

		super.selecionarCheckBox(super.isClickable(By.name("chavesPrimarias")));
		super.clicarEmElemento(webDriver.findElement(By.id("cancelarNota")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Cancelar Nota de Crédito / Débito')]"));

		Select select = new Select(super.isClickable(By.id("idMotivoCancelamento")));
		select.selectByVisibleText("FATURAMENTO INDEVIDO");
		espera();

		super.selecionarCheckBox(isClickable(By.name("chavesNotasSelecionadas")));
		super.clicarEmElemento(isClickable(By.name("buttonCancelar")));

		assertEquals("Sucesso: Nota de Crédito / Débito cancelada(s) com sucesso.",
				obterElementoVisivel(By.className("notification")).getAttribute("textContent")
				.replaceAll("[\\n\\r\\t]", ""));

	}

	@Test
	public void testSimularCalculoDadosFicticios() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='66']/a/span[normalize-space(text()) = 'Dados Fictícios']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		testeCamposObrigatorios();
		testeMensagensErrros();

	}

	private void testeCamposObrigatorios() {

		new Select(isClickable(By.id("idTarifa"))).selectByVisibleText("INDUSTRIAL APL");
		isClickable(By.id("consumoApurado")).clear();
		isClickable(By.id("consumoApurado")).sendKeys("100");
		clicarEmElemento(isClickable(By.cssSelector("img.ui-datepicker-trigger")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.xpath("(//img[@alt='Exibir Calendário'])[2]")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		obterElementoVisivel(By.cssSelector("legend.conteinerBlocoTitulo"));

	}
	
	private void limparCampos() {
		isClickable(By.id("consumoApurado")).sendKeys("");
		isClickable(By.id("consumoApurado")).clear();
		preencherCampo("dataInicio", "");
		preencherCampo("dataFim", "");
	}

	private void testeMensagensErrros() {

		limparCampos();
		new Select(isClickable(By.id("idTarifa"))).selectByVisibleText("Selecione");
		isClickable(By.id("consumoApurado")).sendKeys("100");
		clicarEmElemento(isClickable(By.cssSelector("img.ui-datepicker-trigger")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.xpath("(//img[@alt='Exibir Calendário'])[2]")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		assertEquals("Erro: O(s) campo(s) Tarifa é (são) de preenchimento obrigatório.",
				super.getTexto(isClickable(By.xpath("//div[@id='inner']/div/div"))));
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='66']/a/span[normalize-space(text()) = 'Dados Fictícios']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(isClickable(By.name("idTarifa"))).selectByVisibleText("INDUSTRIAL APL");
		clicarEmElemento(isClickable(By.xpath("(//img[@alt='Exibir Calendário'])[2]")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		assertEquals("Erro: O(s) campo(s) Data de início, Consumo é (são) de preenchimento obrigatório.",
				super.getTexto(isClickable(By.xpath("//div[@id='inner']/div/div"))));
		element = webDriver
				.findElement(By.xpath("//li[@id='66']/a/span[normalize-space(text()) = 'Dados Fictícios']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(isClickable(By.cssSelector("#idTarifa"))).selectByVisibleText("INDUSTRIAL APL");
		clicarEmElemento(isClickable(By.cssSelector("img.ui-datepicker-trigger")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.xpath("(//img[@alt='Exibir Calendário'])[2]")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		assertEquals("Erro: O(s) campo(s) Consumo é (são) de preenchimento obrigatório.",
				super.getTexto(isClickable(By.xpath("//div[@id='inner']/div/div"))));
		element = webDriver
				.findElement(By.xpath("//li[@id='66']/a/span[normalize-space(text()) = 'Dados Fictícios']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(isClickable(By.id("idTarifa"))).selectByVisibleText("INDUSTRIAL APL");
		isClickable(By.id("consumoApurado")).sendKeys("100");
		clicarEmElemento(isClickable(By.cssSelector("img.ui-datepicker-trigger")));
		clicarEmElemento(isClickable(By.linkText("22")));
		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		assertEquals("Erro: O(s) campo(s) Data final é (são) de preenchimento obrigatório.",
				super.getTexto(isClickable(By.xpath("//div[@id='inner']/div/div"))));
		element = webDriver
				.findElement(By.xpath("//li[@id='66']/a/span[normalize-space(text()) = 'Dados Fictícios']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(isClickable(By.id("idTarifa"))).selectByVisibleText("Selecione");
		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		assertEquals(
				"Erro: O(s) campo(s) Tarifa, Data de início, Data final, Consumo é (são) de preenchimento obrigatório.",
				super.getTexto(isClickable(By.xpath("//div[@id='inner']/div/div"))));
		element = webDriver
				.findElement(By.xpath("//li[@id='66']/a/span[normalize-space(text()) = 'Dados Fictícios']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(isClickable(By.id("idTarifa"))).selectByVisibleText("RESIDENCIAL TEST");
		clicarEmElemento(isClickable(By.cssSelector("img.ui-datepicker-trigger")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.xpath("(//img[@alt='Exibir Calendário'])[2]")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		assertEquals("Erro: O(s) campo(s) Consumo é (são) de preenchimento obrigatório.",
				super.getTexto(isClickable(By.xpath("//div[@id='inner']/div/div"))));
		element = webDriver
				.findElement(By.xpath("//li[@id='66']/a/span[normalize-space(text()) = 'Dados Fictícios']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(isClickable(By.id("idTarifa"))).selectByVisibleText("RESIDENCIAL TEST");
		isClickable(By.id("consumoApurado")).sendKeys("100");
		clicarEmElemento(isClickable(By.xpath("(//img[@alt='Exibir Calendário'])[2]")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		assertEquals("Erro: O(s) campo(s) Data de início é (são) de preenchimento obrigatório.",
				super.getTexto(isClickable(By.xpath("//div[@id='inner']/div/div"))));
		element = webDriver
				.findElement(By.xpath("//li[@id='66']/a/span[normalize-space(text()) = 'Dados Fictícios']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(isClickable(By.id("idTarifa"))).selectByVisibleText("RESIDENCIAL TEST");
		isClickable(By.id("consumoApurado")).sendKeys("100");
		clicarEmElemento(isClickable(By.cssSelector("img.ui-datepicker-trigger")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		assertEquals("Erro: O(s) campo(s) Data final é (são) de preenchimento obrigatório.",
				super.getTexto(isClickable(By.xpath("//div[@id='inner']/div/div"))));
		element = webDriver
				.findElement(By.xpath("//li[@id='66']/a/span[normalize-space(text()) = 'Dados Fictícios']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(isClickable(By.id("idTarifa"))).selectByVisibleText("RESIDENCIAL TESTE");
		clicarEmElemento(isClickable(By.cssSelector("img.ui-datepicker-trigger")));
		clicarEmElemento(isClickable(By.xpath("(//img[@alt='Exibir Calendário'])[2]")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.cssSelector("img.ui-datepicker-trigger")));
		clicarEmElemento(isClickable(By.linkText("19")));
		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		assertEquals("Erro: O(s) campo(s) Consumo é (são) de preenchimento obrigatório.",
				super.getTexto(isClickable(By.xpath("//div[@id='inner']/div/div"))));
		element = webDriver
				.findElement(By.xpath("//li[@id='66']/a/span[normalize-space(text()) = 'Dados Fictícios']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(isClickable(By.id("idTarifa"))).selectByVisibleText("RESIDENCIAL TESTE");
		isClickable(By.id("consumoApurado")).clear();
		isClickable(By.id("consumoApurado")).sendKeys("100");
		preencherCampo("dataInicio", "19/07/2018");
		preencherCampo("dataFim", "19/07/2018");
		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		espera(1000);
		final String texto = super.getTexto(isClickable(By.xpath("//div[@id='inner']/div/div")));
		LOG.info("testeMensagensErrros: " + texto);
		assertEquals("Erro: Não existem vigências para a tarifa utilizada no período especificado.", texto);
		element = webDriver
				.findElement(By.xpath("//li[@id='66']/a/span[normalize-space(text()) = 'Dados Fictícios']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(isClickable(By.id("idTarifa"))).selectByVisibleText("INDUSTRIAL APL");
		isClickable(By.id("consumoApurado")).sendKeys("100");
		clicarEmElemento(isClickable(By.cssSelector("img.ui-datepicker-trigger")));
		clicarEmElemento(isClickable(By.linkText("1")));
		clicarEmElemento(isClickable(By.xpath("(//img[@alt='Exibir Calendário'])[2]")));

		if(Calendar.getInstance().get(Calendar.MONTH)==1) {
			clicarEmElemento(isClickable(By.linkText("28")));
		} else {
			clicarEmElemento(isClickable(By.linkText("30")));
		}

		clicarEmElemento(isClickable(By.id("botaoCalcular")));
		assertEquals(
				"Erro: Para simular com dados fictícios e base de apuração diária é preciso limitar o intervalo de consumo a um(1) dia.",
				super.getTexto(isClickable(By.xpath("//div[@id='inner']/div/div"))));
		clicarEmElemento(isClickable(By.id("botaoCancelar")));

	}
}
