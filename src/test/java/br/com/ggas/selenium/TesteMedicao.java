
package br.com.ggas.selenium;

import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.FixMethodOrder;
import org.junit.Ignore;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TesteMedicao extends SeleniumTestCase {

	@Test
	public void testeCorretorMarca() {

		testeInserirCorretorMarca();
		testeAlterarCorretorMarca();
		testeRemoverCorretorMarca();
	}

	private void testeRemoverCorretorMarca() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElement(By.xpath("//div[@id='inner']/div/div/p"));
		assertEquals("Sucesso: Corretor Marca removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarCorretorMarca() {
		clicarEmElemento(isClickable(By.linkText("MARCA CORRETOR")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Corretor Marca')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("CORRETOR MARCA");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Corretor Marca alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	

	private void testeInserirCorretorMarca() {
		WebElement element = waitForElement(By.xpath("id('127')/a"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable((By.id("incluir"))));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Corretor Marca')]"));
		waitForElement(By.id("descricao")).sendKeys("MARCA CORRETOR");
		waitForElement(By.id("descricaoAbreviada")).sendKeys("COR");
		clicarEmElemento(isClickable((By.id("botaoSalvar"))));
		
		assertEquals("Sucesso: Corretor Marca inserido(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeCorretorModelo() {
		testeInserirCorretorModelo();
		testeAlterarCorretorModelo();
		testeRemoverCorretorModelo();
	}

	private void testeRemoverCorretorModelo() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		obterElementoVisivel(By.xpath("//div[@id='inner']/div/div/p"));
		assertEquals("Sucesso: Corretor Modelo removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarCorretorModelo() {
		clicarEmElemento(isClickable(By.linkText("MODELO CORRETOR")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Corretor Modelo')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("CORRETOR MODELO");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Corretor Modelo alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirCorretorModelo() {
		WebElement element = webDriver.findElement(By.xpath("id('128')/a"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("incluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Corretor Modelo')]"));
		waitForElement(By.id("descricao")).sendKeys("MODELO CORRETOR");
		waitForElement(By.id("descricaoAbreviada")).sendKeys("COR");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Corretor Modelo inserido(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeCorretorVazao() {

		testeInserirCorretorVazao();
		testeAlterarCorretorVazao();
		testeRemoverCorretorVazao();
	}

	private void testeRemoverCorretorVazao() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(By.linkText("999995"));
		assertEquals("Sucesso: Corretor(es) de vazão removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarCorretorVazao() {
		clicarEmElemento(isClickable(By.linkText("999999")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Corretor de vazão')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.id("ui-id-1"));
		waitForElement(By.id("numeroSerie")).clear();
		waitForElement(By.id("numeroSerie")).sendKeys("999995");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Corretor de vazão alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirCorretorVazao() {
		WebElement element = waitForElement(By.xpath("id('46')/a"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));

		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Corretor de Vazão')]"));

		obterElementoVisivel(By.id("numeroSerie")).clear();
		waitForElement(By.id("numeroSerie")).sendKeys("999999");
		new Select(waitForElement(By.id("marca"))).selectByVisibleText("MARCA INDUSTRIAL");
		new Select(waitForElement(By.id("modelo"))).selectByVisibleText("MODELO INDUSTRIAL");

		clicarEmElemento(isClickable(By.id("ui-id-2")));

		new Select(waitForElement(By.id("numeroDigitosLeituraVazaoCorretor"))).selectByVisibleText("6");
		new Select(waitForElement(By.id("localArmazenagem"))).selectByVisibleText("ALMOXARIFADO");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals("Sucesso: Corretor de vazão inserido(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeMedidorMarca() {

		testeInserirMedidorMarca();
		testeAlterarMedidorMarca();
		testeRemoverMedidorMarca();
	}

	private void testeRemoverMedidorMarca() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Medidor Marca removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarMedidorMarca() {
		obterElementoVisivel(By.linkText("MARCA MEDIDOR"));
		clicarEmElemento(isClickable(By.linkText("MARCA MEDIDOR")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Medidor Marca')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Medidor Marca')]"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("MEDIDOR MARCA");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));;
		
		assertEquals("Sucesso: Medidor Marca alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirMedidorMarca() {
		WebElement element = waitForElement(By.xpath("id('255')/a"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("incluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Medidor Marca')]"));
		obterElementoVisivel(By.id("descricaoAbreviada"));
		preencherCampo("descricao", "MARCA MEDIDOR");
		waitForElement(By.id("descricaoAbreviada")).sendKeys("MED");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Medidor Marca inserido(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeMedidorModelo() {

		testeInserirMedidorModelo();
		testeAlterarMedidorModelo();
		testeRemoverMedidorModelo();
	}

	private void testeRemoverMedidorModelo() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Medidor Modelo removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarMedidorModelo() {
		clicarEmElemento(isClickable(By.linkText("MODELO MEDIDOR")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Medidor Modelo')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Medidor Modelo')]"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("MEDIDOR MODELO");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Medidor Modelo alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirMedidorModelo() {
		WebElement element = waitForElement(By.xpath("id('254')/a"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("incluir")));

		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Medidor Modelo')]"));

		preencherCampo("descricao", "MODELO MEDIDOR");
		waitForElement(By.id("descricaoAbreviada")).sendKeys("MED");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Medidor Modelo inserido(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeMedidor() {

		testeInserirMedidor();
		testeAlterarMedidor();
		testeRemoverMedidor();
	}

	private void testeRemoverMedidor() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));

		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElement(By.xpath("//div[@id='inner']/div/div/p"));
		espera(1000);
		assertEquals("Sucesso: Medidor(es) removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarMedidor() {
		clicarEmElemento(isClickable(By.linkText("2017")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Medidor')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.id("ui-id-1"));
		new Select(waitForElement(By.id("tipo"))).selectByVisibleText("CARRETEL");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals("Sucesso: Medidor alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirMedidor() {
		WebElement element = waitForElement(By.xpath("id('43')/a/span"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Medidor')]"));
		obterElementoVisivel(By.id("ui-id-1"));
		obterElementoVisivel(By.id("numeroSerie"));

		isClickable(By.id("numeroSerie")).clear();
		isClickable(By.id("numeroSerie")).sendKeys("2017");
		waitForElement(By.id("rotuloTipo")).click();
		new Select(waitForElement(By.id("tipo"))).selectByVisibleText("ROTATIVO");
		new Select(waitForElement(By.id("marca"))).selectByVisibleText("MARCA ORUBE");
		new Select(waitForElement(By.id("modelo"))).selectByVisibleText("MODELO ORUBE");
		waitForElement(By.id("rotuloModelo")).click();
		new Select(waitForElement(By.id("capacidadeMinima"))).selectByVisibleText("4 m3h");
		new Select(waitForElement(By.id("capacidadeMaxima"))).selectByVisibleText("6 m3h");
		preencherCampo("dataAquisicao", "22/03/2016");
		clicarEmElemento(isClickable(By.id("ui-id-2")));
		waitForElement(By.id("numeroDigitosLeitura")).clear();
		waitForElement(By.id("numeroDigitosLeitura")).sendKeys("5");
		new Select(waitForElement(By.id("localArmazenagem"))).selectByVisibleText("CLIENTE");
		clicarEmElemento(isClickable(By.id("botaoIncluir")));

		assertEquals("Sucesso: Medidor inserido(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeLeiturista() {

		testeInserirLeiturista();
		testeAlterarLeiturista();
		testeRemoverLeiturista();
	}

	private void testeRemoverLeiturista() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Leiturista removido(s) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarLeiturista() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")) );
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Leiturista')]"));
		clicarEmElemento(isClickable(By.id("habilitado2")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")) );
		
		assertEquals("Sucesso: Leiturista alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirLeiturista() {

		WebElement element = waitForElement(By.xpath("id('48')/a/span"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		String janelaPrincipal = webDriver.getWindowHandle();
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")) );
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Leiturista')]"));

		clicarEmElemento(isClickable(By.id("botaoPesquisarFuncionario")));
		
		for (String janelaSecundaria : webDriver.getWindowHandles()) {
			
			if(!janelaSecundaria.equals(janelaPrincipal)) {
				webDriver.switchTo().window(janelaSecundaria);
			}
		}
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		
		isClickable(By.linkText("ERICK SIMOES")).click();

		webDriver.switchTo().window(janelaPrincipal);
		
		super.elementoPossuiAtributoValor(By.id("nomeFuncionarioTexto"), "ERICK SIMOES");
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")) );
		
		assertEquals("Sucesso: Leiturista inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	
	@Test
	public void testeAssociarPontoConsumo() {
		WebElement element = waitForElement(By.xpath(
				"//li[@id='57']/a/span[normalize-space(text()) = 'Associar Ponto de Consumo ao Medidor / Corretor de Vazão']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Associação')]"));
		clicarEmElemento(isClickable(By.id("indicadorPesquisaImovel")));
		obterElementoVisivel(By.id("indicadorPesquisaImovel")).isEnabled();
		String janelaPrincipal = super.trocarJanela(By.id("botaoPesquisarImovel"));
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("TESTE")));
		
		webDriver = webDriver.switchTo().window(janelaPrincipal);
		super.elementoPossuiAtributoValor(By.id("nomeImovelTexto"), "TESTE");
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		obterElementoVisivel(By.id("checkPontoConsumo"));
		clicarEmElemento(isClickable(By.id("checkPontoConsumo")));
		clicarEmElemento(isClickable(By.id("tipoAssociacao1")));
		
		WebElement elemento = waitForElement(By.id("operacaoMedidor"));
		new Select(elemento).selectByVisibleText("Instalação");

		clicarEmElemento(isClickable(By.id("botao-OK")));

		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Instalação - Medidor')]"));
		waitForElementNotPresent(buscarElemento(By.xpath("//fieldset[@id='dadosPontoConsumo']/fieldset")));
		janelaPrincipal = super.trocarJanela(By.id("botaoPesquisarMedidor"));
		
		//obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Medidor')]"));
		obterElementoVisivel(By.id("modoUso"));
		new Select(waitForElement(By.id("marca"))).selectByVisibleText("MARCA ORUBE");
		new Select(waitForElement(By.id("modelo"))).selectByVisibleText("MODELO ORUBE");
		new Select(waitForElement(By.id("tipo"))).selectByVisibleText("TURBINA");
		clicarEmElemento(isClickable(By.name("buttonPesquisar")));
		obterElementoVisivel(By.linkText("2016"));
		clicarEmElemento(isClickable(By.linkText("2016")));
		
		webDriver = webDriver.switchTo().window(janelaPrincipal);
		super.elementoPossuiAtributoValor(By.id("numeroSerieMedidorAtual"), "2016");
		
		new Select(waitForElement(By.id("localInstalacaoMedidor"))).selectByVisibleText("JARDIM");
		waitForElement(By.id("leituraAtual")).clear();
		waitForElement(By.id("leituraAtual")).sendKeys("40");
		new Select(waitForElement(By.id("medidorProtecao"))).selectByVisibleText("FIBRA DE VIDRO");
		new Select(waitForElement(By.id("funcionario"))).selectByVisibleText("ERICK SIMOES");
		new Select(waitForElement(By.id("medidorMotivoOperacao"))).selectByVisibleText("INSTALACAO");
		preencherCampo("dataMedidor", "01/04/2016");

		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals("Sucesso: Operação realizada com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		
		obterElementoVisivel(By.id("botaoPesquisar"));
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));

		obterElementoVisivel(By.id("checkPontoConsumo"));
		obterElementoVisivel(By.id("operacaoMedidor"));
		obterElementoVisivel(By.xpath("//div[@id='inner']/form/fieldset[2]/div"));
		clicarEmElemento(isClickable(By.id("checkPontoConsumo")));
		clicarEmElemento(isClickable(By.id("tipoAssociacao1")));
		
		elemento = waitForElement(By.id("operacaoMedidor"));
		new Select(elemento).selectByVisibleText("Ativação");
		
		clicarEmElemento(isClickable(By.id("botao-OK")));
		
		elemento = waitForElement(By.id("funcionario"));
		new Select(elemento).selectByVisibleText("ERICK SIMOES");
		
		waitForElement(By.id("leituraAnterior")).clear();
		waitForElement(By.id("leituraAnterior")).sendKeys("40");
		preencherCampo("dataMedidor", "01/04/2016");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Operação realizada com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeRota() {

		testeInserirRota();
		testeAlterarRota();
		testeRemoverRota();
	}

	private void testeRemoverRota() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));

		assertTrue(
				obterTextoDoAlerta().matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Rota(s) removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarRota() {
		clicarEmElemento(isClickable(By.linkText("20161")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Detalhar Rota')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		waitForElement(By.id("numeroMaximoPontosConsumo")).clear();
		waitForElement(By.id("numeroMaximoPontosConsumo")).sendKeys("6");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Rota alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirRota() {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Rota')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));

		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Incluir Rota')]"));

		new Select(waitForElement(By.id("idPeriodicidade"))).selectByVisibleText("MENSAL COLETOR");
		new Select(waitForElement(By.id("idTipoLeitura"))).selectByVisibleText("COLETOR DE DADOS");
		element = waitForElement(By.id("idGrupoFaturamento"));
		new Select(element).selectByVisibleText("GRUPO ORUBE");
		waitForElement(By.id("numeroRota")).clear();
		waitForElement(By.id("numeroRota")).sendKeys("20161");
		waitForElement(By.id("numeroMaximoPontosConsumo")).clear();
		waitForElement(By.id("numeroMaximoPontosConsumo")).sendKeys("5");
		new Select(waitForElement(By.id("idSetorComercial"))).selectByVisibleText("SETOR TESTE");
		
		new Select(waitForElement(By.id("idEmpresa"))).selectByVisibleText("C A DE CARVALHO CORREIA TECNOLOGIA ME");
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		
		assertEquals("Sucesso: Rota inserido(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testPressao() throws Exception {

		testeInserirPressao();
		testeAlterarPressao();
		testeRemoverPressao();
	}

	private void testeRemoverPressao() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Pressão de Fornecimento removida(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarPressao() {
		obterElementoVisivel(By.linkText("COMERCIAL"));
		clicarEmElemento(isClickable(By.linkText("COMERCIAL")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		waitForElement(By.id("numeroFatorCorrecaoPTZPCS")).clear();
		
		new Select(waitForElement(By.id("corrigiPT"))).selectByVisibleText("Sim (Contrato)");
		new Select(waitForElement(By.id("corrigiZ"))).selectByVisibleText("Sim (Valor)");
		waitForElement(By.id("numeroFatorZ")).clear();
		waitForElement(By.id("numeroFatorZ")).sendKeys("1");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Pressão de Fornecimento alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirPressao() {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Pressão de Fornecimento')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.cssSelector("input.botaoAdicionarFaixas.botaoAdicionar"));
		new Select(waitForElement(By.id("idSegmento"))).selectByVisibleText("COMERCIAL");
		new Select(waitForElement(By.id("idUnidadePressao"))).selectByVisibleText("Quilopascal");
		waitForElement(By.id("faixaFinal")).clear();
		waitForElement(By.id("faixaFinal")).sendKeys("5");
		new Select(waitForElement(By.id("classe"))).selectByVisibleText("BAIXA");
		waitForElement(By.id("numeroFatorCorrecaoPTZPCS")).clear();
		waitForElement(By.id("numeroFatorCorrecaoPTZPCS")).sendKeys("1");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Pressão de Fornecimento inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeDadosCompraCityGate() {

		testeAlterarDadosCompraCityGate();
	}

	private void testeAlterarDadosCompraCityGate() {
		webDriver.manage().window().maximize();

		WebElement element = waitForElement(By.xpath("//li[@id='221']/a/span[normalize-space(text()) = 'City Gate']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		obterElementoVisivel(By.xpath("//input[@id='botaoExibirPCSCityGate' and @disabled='disabled']"));
		new Select(waitForElement(By.id("idCityGate"))).selectByVisibleText("ETCPIL001");
		new Select(isClickable(By.id("ano"))).selectByVisibleText("2016");
		new Select(isClickable(By.id("mes"))).selectByVisibleText("Abril");
		clicarEmElemento(isClickable(By.id("botaoExibirPCSCityGate")));

		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals("Sucesso: Dados de Compra de Gás alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeCromatografia() throws Exception {

		testeIncluirCromatografia();
		testeAlterarCromatografia();
		testeRemoverCromatografia();
	}

	private void testeRemoverCromatografia() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Cromatografia removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarCromatografia() {
		obterElementoVisivel(By.id("checkboxChavesPrimarias"));
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("alterarCromatografia")));
		preencherCampo("dataCromatografia", "20/02/2017");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Cromatografia alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeIncluirCromatografia() {
		WebElement element = waitForElement(By.xpath("//li[@id='246']/a/span[normalize-space(text()) = 'Cromatografia']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("pesquisarCG")));
		
		clicarEmElemento(isClickable(By.id("radioCityGateChavesPrimarias")));
		
		clicarEmElemento(isClickable(By.id("incluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Cromatografia')]"));
		preencherCampo("dataCromatografia", "22/02/2017");
		waitForElement(By.id("metano")).clear();
		waitForElement(By.id("metano")).sendKeys("5");
		waitForElement(By.id("nitrogenio")).clear();
		waitForElement(By.id("nitrogenio")).sendKeys("5");
		waitForElement(By.id("dioxidoCarbono")).clear();
		waitForElement(By.id("dioxidoCarbono")).sendKeys("10");
		waitForElement(By.id("etano")).clear();
		waitForElement(By.id("etano")).sendKeys("5");
		waitForElement(By.id("propano")).clear();
		waitForElement(By.id("propano")).sendKeys("5");
		waitForElement(By.id("agua")).clear();
		waitForElement(By.id("agua")).sendKeys("10");
		waitForElement(By.id("sulfetoHidrogenio")).clear();
		waitForElement(By.id("sulfetoHidrogenio")).sendKeys("5");
		waitForElement(By.id("hidrogenio")).clear();
		waitForElement(By.id("hidrogenio")).sendKeys("5");
		waitForElement(By.id("monoxidoCarbono")).clear();
		waitForElement(By.id("monoxidoCarbono")).sendKeys("10");
		waitForElement(By.id("oxigenio")).clear();
		waitForElement(By.id("oxigenio")).sendKeys("5");
		waitForElement(By.id("iButano")).clear();
		waitForElement(By.id("iButano")).sendKeys("5");
		waitForElement(By.id("nButano")).clear();
		waitForElement(By.id("nButano")).sendKeys("10");
		waitForElement(By.id("iPentano")).clear();
		waitForElement(By.id("iPentano")).sendKeys("5");
		waitForElement(By.id("nPentano")).clear();
		waitForElement(By.id("nPentano")).sendKeys("5");
		waitForElement(By.id("nHexano")).clear();
		waitForElement(By.id("nHexano")).sendKeys("10");
		clicarEmElemento(isClickable(By.id("calcularZ")));
		clicarEmElemento(isClickable(By.id("recalcularZ")));
		waitForElement(By.id("pressao")).clear();
		waitForElement(By.id("pressao")).sendKeys("290");
		waitForElement(By.id("temperatura")).clear();
		waitForElement(By.id("temperatura")).sendKeys("130");
		clicarEmElemento(isClickable(By.id("calcularZ")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
	}

	@Test
	public void testeFaixaVariacaoConsumo() {

		testeInserirFaixaVariacaoConsumo();
		testeAlterarFaixaVariacaoConsumo();

	}

	private void testeAlterarFaixaVariacaoConsumo() {
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));

		clicarEmElemento(isClickable(By.linkText("TESTE")));

		waitForElement(By.id("estouroConsumo")).clear();
		waitForElement(By.id("estouroConsumo")).sendKeys("170");

		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals("Sucesso: Faixa de Variação do Consumo alterada com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirFaixaVariacaoConsumo() {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Faixa de Variação do Consumo')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Faixa de Variação do Consumo')]"));

		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Inserir Faixa de Variação do Consumo')]"));

		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");

		new Select(webDriver.findElement(By.id("idSegmento"))).selectByValue("2"); //COMERCIAL

		waitForElement(By.id("faixaSuperior")).clear();
		waitForElement(By.id("faixaSuperior")).sendKeys("10");
		waitForElement(By.id("percentualInferior")).clear();
		waitForElement(By.id("percentualInferior")).sendKeys("10");
		waitForElement(By.id("percentualSuperior")).clear();
		waitForElement(By.id("percentualSuperior")).sendKeys("10");
		waitForElement(By.id("baixoConsumo")).clear();
		waitForElement(By.id("baixoConsumo")).sendKeys("50");
		waitForElement(By.id("altoConsumo")).clear();
		waitForElement(By.id("altoConsumo")).sendKeys("120");
		waitForElement(By.id("estouroConsumo")).clear();
		waitForElement(By.id("estouroConsumo")).sendKeys("150");

		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals("Sucesso: Faixa de Variação do Consumo inserida com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeDadosPressaoTemperatura() {

		testeAlterarPressaoTemperatura();
		testeRemoverPressaoTemperatura();

	}

	private void testeRemoverPressaoTemperatura() {
		obterElementoVisivel(By.id("iconeExcluir"));
		clicarEmElemento(isClickable(By.id("iconeExcluir")));
		
		waitForAlertPresent();
		
		assertTrue(super.obterTextoDoAlerta().matches("^Deseja excluir o dado de temperatura e pressão[\\s\\S]$"));
		waitForElementNotPresent(buscarElemento(By.id("iconeExcluir")));
		assertEquals("Sucesso: Dado de Temperatura e Pressão removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarPressaoTemperatura() {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Dados de Pressão e Temperatura')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		obterElementoVisivel(By.id("codigoTipoAbrangenciaPesquisa"));
		obterElementoVisivel(By.id("dataInicial"));
		obterElementoVisivel(By.id("idAbrangenciaPesquisa"));
		obterElementoVisivel(By.xpath("(//input[@name='Button'])[2]"));

		new Select(webDriver.findElement(By.id("codigoTipoAbrangenciaPesquisa"))).selectByVisibleText("Estado");

		isClickable(By.id("idAbrangenciaPesquisa")).isEnabled();
		new Select(obterElementoVisivel(By.id("idAbrangenciaPesquisa"))).selectByVisibleText("ACRE");

		obterElementoVisivel(By.id("botaoExibirTemperaturaPressao")).isEnabled();
		clicarEmElemento(isClickable(By.id("botaoExibirTemperaturaPressao")));
		espera(1000);
		
		waitForElement(By.name("temperatura")).clear();
		waitForElement(By.name("temperatura")).sendKeys("29");

		preencherCampo("dataVigencia0", "26/08/2016");

		new Select(waitForElement(By.id("unidadeTemperatura"))).selectByVisibleText("Celsius");

		waitForElement(By.name("pressao")).clear();
		waitForElement(By.name("pressao")).sendKeys("24");

		new Select(waitForElement(By.id("unidadePressao"))).selectByVisibleText("Quilopascal");

		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals("Sucesso: Dados de Temperatura e Pressão alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	@Test
	public void testeHistoricoLeituraConsumo() {
		WebElement element = waitForElement(By.xpath("//li[@id='51']/a/span[normalize-space(text()) = 'Histórico de Leitura e Consumo']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		obterElementoVisivel(By.xpath("//input[@id='botaoPesquisar' and @disabled='disabled']"));
		new Select(waitForElement(By.id("idGrupoFaturamento"))).selectByVisibleText("GRUPO ORUBE");
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("EDIFICIO PRADA - APT 101")));

		clicarEmElemento(isClickable(By.linkText("Dados do Imóvel")));

		assertTrue(getTexto(waitForElement(By.id("dadosImovel"))).contains("EDIFICIO PRADA - APT 101"));
		assertTrue(getTexto(waitForElement(By.id("dadosImovel")))
				.contains("RUA FERNANDO CALIXTO, 760, PALMEIRA DE FORA, PALMEIRA DOS INDIOS, AL"));
		
		new Select(waitForElement(By.id("mesFinal"))).selectByVisibleText("Janeiro");
		new Select(waitForElement(By.id("anoFinal"))).selectByVisibleText("2017");
		new Select(waitForElement(By.id("mesInicial"))).selectByVisibleText("Janeiro");
		new Select(waitForElement(By.id("anoInicial"))).selectByVisibleText("2016");

		clicarEmElemento(isClickable(By.id("botaoExibir")));
		
		obterElementoVisivel(By.xpath("//table[@id='historicoMedicao']//tr[contains(@class, 'odd')]"));
		espera(3000);
		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoMedicao']//tr[contains(@class, 'odd')]//td)[1]"))).contains("02/2016-2"));
		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoMedicao']//tr[contains(@class, 'odd')]//td)[2]"))).contains("01/01/2016: 00:00"));
		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoMedicao']//tr[contains(@class, 'odd')]//td)[3]"))).contains("20"));
		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoMedicao']//tr[contains(@class, 'odd')]//td)[5]"))).contains("01/01/2016"));
		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoMedicao']//tr[contains(@class, 'odd')]//td)[10]"))).contains("Confirmada"));

		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoConsumo']//tr[contains(@class, 'odd')]//td)[1]"))).contains("02/2016-1"));
		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoConsumo']//tr[contains(@class, 'odd')]//td)[4]"))).contains("10"));
		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoConsumo']//tr[contains(@class, 'odd')]//td)[5]"))).contains("0.9359")
				|| getTexto(waitForElement(By.xpath("(//table[@id='historicoConsumo']//tr[contains(@class, 'odd')]//td)[5]"))).contains("0,9359"));
		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoConsumo']//tr[contains(@class, 'odd')]//td)[6]"))).contains("9"));
		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoConsumo']//tr[contains(@class, 'odd')]//td)[7]"))).contains("0.3103")
				|| getTexto(waitForElement(By.xpath("(//table[@id='historicoConsumo']//tr[contains(@class, 'odd')]//td)[7]"))).contains("0,3103"));
		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoConsumo']//tr[contains(@class, 'odd')]//td)[9]"))).contains("1.0042")
				|| getTexto(waitForElement(By.xpath("(//table[@id='historicoConsumo']//tr[contains(@class, 'odd')]//td)[9]"))).contains("1,0042"));
		assertTrue(getTexto(waitForElement(By.xpath("(//table[@id='historicoConsumo']//tr[contains(@class, 'odd')]//td)[11]"))).contains("7.6364")
				|| getTexto(waitForElement(By.xpath("(//table[@id='historicoConsumo']//tr[contains(@class, 'odd')]//td)[11]"))).contains("7,6364"));
	}
	
	@Ignore
	public void testeMedicaoAnalise() {
		WebElement element = waitForElement(By.xpath("//li[@id='262']/a/span[normalize-space(text()) = 'Análise de Medição']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		testePesquisarAnaliseMedicao();
		testeChecarQuantidadeMedicoes("Gerado", 1);
		testeChecarQuantidadeMedicoes("Em Leitura", 1);
		testeAlterarAnaliseMedicao("40", "Gerado");
		testeAlterarAnaliseMedicao("50", "Em Leitura");
		testeChecarQuantidadeMedicoes("Leitura Informada", 2);
		testeRegistrarLeitura();
		espera(12000);
		testeChecarRegistrarLeituraSucesso();
	}
	
	private void testePesquisarAnaliseMedicao() {
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Análise de Medição')]"));
		
		new Select(isClickable(By.id("grupoFaturamento"))).selectByVisibleText("GRUPO ORUBE");
		espera();
		new Select(isClickable(By.id("grupoFaturamento"))).selectByVisibleText("GRUPO MEDICAO");
		
		waitForElement(By.name("ciclo")).clear();
		waitForElement(By.name("ciclo")).sendKeys("1");
		
		// ROTA 201408
		waitForElement(By.xpath("//input[@type='search']")).clear();
		waitForElement(By.xpath("//input[@type='search']")).sendKeys("201408");
		waitForElement(By.xpath("//input[@type='search']")).sendKeys(Keys.RETURN);
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
	}
	
	/**
	 * Altera a leitura atual da medição com a situação situacao.
	 * 
	 * @param leituraAtual Nova leitura.
	 * @param situacao Situação da leitura a ser alterada.
	 */
	private void testeAlterarAnaliseMedicao(String leituraAtual, String situacao) {
		String diaMesAno = Util.converterDataParaStringSemHora(new Date(), Constantes.FORMATO_DATA_BR);
		
		badgeSituacao(situacao);
		
		// Altera a leitura atual do registro.
		clicarEmElemento(isClickable(By.xpath("//span[contains(text(), '" + situacao + "')]/../../td[contains(@class, 'leituraAtual')]/i")));
		waitForElement(By.id("inputAlterarLeitura")).clear();
		waitForElement(By.id("inputAlterarLeitura")).sendKeys(leituraAtual);
		
		clicarEmElemento(isClickable(By.id("confirmarDadosMedicao")));
		
		clicarEmElemento(isClickable(By.xpath("//span[contains(text(), '" + situacao + "')]/../../td[contains(@class, 'dataLeitura')]/i")));
		preencherCampo("inputAlterarDataLeitura", diaMesAno);
		
		clicarEmElemento(isClickable(By.id("confirmarDadosMedicao")));
		
		clicarEmElemento(isClickable(By.xpath("//span[contains(text(), '" + situacao + "')]/../../td[contains(@class, 'pressao')]/i")));
		waitForElement(By.id("inputAlterarPressao")).clear();
		waitForElement(By.id("inputAlterarPressao")).sendKeys("0.04");
		
		clicarEmElemento(isClickable(By.id("confirmarDadosMedicao")));
		
		clicarEmElemento(isClickable(By.id("salvarLeituras")));
		
		assertTrue(getTexto(obterElementoVisivel(By.className("swal2-content"))).contains("Deseja realmente alterar os dados de medições de 1 leituras?"));
		clicarEmElemento(isClickable(By.className("swal2-confirm")));
		
		assertTrue(getTexto(obterElementoVisivel(By.className("swal2-content"))).contains("Dados de Medição de 1 leituras alteradas com sucesso!"));
		clicarEmElemento(isClickable(By.className("swal2-confirm")));
		
		// Verifica se o registro que foi alterado não aparece mais na tela.
		waitForElementNotPresent(By.xpath("//*[@id='tabelaLeituras']/tbody/tr[1]/td[1]/span[contains(text(),'"+situacao+"')]"));
	}

	/**
	 * Verifica se a tela exibe a quantidade esperada de registros de leitura por situação.
	 * 
	 * @param badgeSituacao Classe do ícone que representa a situação.
	 * @param quantidadeEsperada Quantidade de leituras esperadas da situação.
	 */
	private void testeChecarQuantidadeMedicoes(String situacao, Integer quantidadeEsperada) {
		String badgeSituacao = badgeSituacao(situacao);
		
		waitForElement(By.className(badgeSituacao));
		
		checarQuantidadeDeElementos(By.className(badgeSituacao), quantidadeEsperada);
	}
	
	private String badgeSituacao(String situacao) {
		String badgeSituacao;
		
		switch(situacao.toLowerCase()) {
			case "gerado":
				badgeSituacao = "badge-secondary";
				break;
			case "em leitura":
				badgeSituacao = "badge-warning";
				break;
			case "leitura informada":
				badgeSituacao = "badge-info";
				break;
			case "processado":
				badgeSituacao = "badge-success";
				break;
			default:
				throw new IllegalArgumentException("Situação inválida");
		}
		
		return badgeSituacao;
	}

	private void testeRegistrarLeitura() {
		WebElement element = waitForElement(By.xpath("//li[@id='71']/a/span[contains(text(),'Cronograma')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		new Select(isClickable(By.id("grupoFaturamento"))).selectByVisibleText("GRUPO MEDICAO");
		new Select(isClickable(By.id("situacao"))).selectByValue("Em Andamento");
		
		clicarEmElemento(obterElementoVisivel(By.id("botaoPesquisar")));
		
		clicarEmElemento(obterElementoVisivel(By.linkText("GRUPO MEDICAO")));
		
		// Clica na atividade REGISTRAR LEITURA. Abre a tela de análise de medição.
		// tr[2] = segunda linha e td[8] = a coluna de 'Ação'
		clicarEmElemento(obterElementoVisivel(By.xpath("//*[@id=\"atividade\"]/tbody/tr[2]/td[8]/a/i")));
		
		testeChecarQuantidadeMedicoes("Leitura Informada", 2);

		espera(10000);
		webDriver.findElement(By.xpath("//*[@id=\"tabelaLeituras\"]/thead/tr/th[1]/div")).click();

		clicarEmElemento(obterElementoVisivel(By.id("registrarLeituras")));
		
		assertTrue(getTexto(obterElementoVisivel(By.className("swal2-content"))).contains("Deseja executar o processo de registrar leituras?"));
		clicarEmElemento(isClickable(By.className("swal2-confirm")));

		espera(1500);
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Pesquisar Processo')]"));
	}
	
	private void testeChecarRegistrarLeituraSucesso() {
		WebElement element = waitForElement(By.xpath("//li[@id='262']/a/span[normalize-space(text()) = 'Análise de Medição']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		new Select(isClickable(By.id("grupoFaturamento"))).selectByVisibleText("GRUPO ORUBE");
		espera();
		new Select(isClickable(By.id("grupoFaturamento"))).selectByVisibleText("GRUPO MEDICAO");
		
		waitForElement(By.name("ciclo")).clear();
		waitForElement(By.name("ciclo")).sendKeys("1");
		
		// ROTA 201408
		waitForElement(By.xpath("//input[@type='search']")).clear();
		waitForElement(By.xpath("//input[@type='search']")).sendKeys("201408");
		waitForElement(By.xpath("//input[@type='search']")).sendKeys(Keys.RETURN);
		
		clicarEmElemento(waitForElement(By.id("chkGerado")));
		clicarEmElemento(waitForElement(By.id("chkEmLeitura")));
		clicarEmElemento(waitForElement(By.id("chkLeituraRetornada")));
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		
		testeChecarQuantidadeMedicoes("Processado", 2);
	}
}
