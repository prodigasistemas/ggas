package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteHistoricoCorretoresVazao extends SeleniumTestCase {

	@Test
	public void testHistoricoCorretoresVazao() throws Exception {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='181']/a/span[normalize-space(text()) = 'Histórico Corretor de Vazão']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(webDriver.findElement(By.id("marcaCorretor"))).selectByVisibleText("MARCA INDUSTRIAL");
		new Select(webDriver.findElement(By.id("modelo"))).selectByVisibleText("MODELO INDUSTRIAL");
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("923456781")));
		webDriver.findElement(By.cssSelector("a.linkExibirDetalhes > img")).click();
		webDriver.findElement(By.id("ui-id-2")).click();
		assertEquals(true, getTexto(webDriver.findElement(By.id("dadosVazao"))).contains("923456781"));
		assertEquals(true, getTexto(webDriver.findElement(By.id("dadosVazao"))).contains("2016"));
		assertEquals(true, getTexto(webDriver.findElement(By.id("dadosVazao"))).contains("MODELO INDUSTRIAL"));
		assertEquals(true, getTexto(webDriver.findElement(By.id("dadosVazao"))).contains("MARCA INDUSTRIAL"));
	}

}
