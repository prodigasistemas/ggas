package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import br.com.ggas.util.DataUtil;

public class TesteConfiguracaoComandoAcao extends SeleniumTestCase {

	@Test
	public void testeConfiguracaoComandoAcao() throws Exception {
		incluirConfiguracaoComandoAcao();
		alterarConfiguracaoComandoAcao();
		removerConfiguracaoComandoAcao();
	}

	public void incluirConfiguracaoComandoAcao() {
		
		WebElement element = webDriver.findElement(By.xpath("//li[@id='214']/a/span[normalize-space(text()) = 'Comando de Ação']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Comando de Ação')]"));
		
		obterElementoVisivel(By.xpath("//input[@name='indicadorSimulacao']"));
		waitForElement(By.id("nome")).clear();
		waitForElement(By.id("nome")).sendKeys("TESTE ACAO");
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE ACAO");
		new Select(obterElementoVisivel(By.name("acao"))).selectByVisibleText("ATENDIMENTO");
		clicarEmElemento(isClickable(By.xpath("//input[@name='indicadorSimulacao' and @value='true']")));
		new Select(webDriver.findElement(By.id("grupoFaturamento"))).selectByVisibleText("GRUPO ORUBE");
		new Select(webDriver.findElement(By.id("localidade"))).selectByVisibleText("LOCALIDADE 7");
		new Select(webDriver.findElement(By.id("setorComercial"))).selectByVisibleText("SETOR TESTE");
		new Select(webDriver.findElement(By.id("municipio"))).selectByVisibleText("RECIFE");
		new Select(webDriver.findElement(By.id("marcaMedidor"))).selectByVisibleText("MARCA ORUBE");
		new Select(webDriver.findElement(By.id("modeloMedidor"))).selectByVisibleText("MODELO ORUBE");
		new Select(webDriver.findElement(By.id("marcaCorretor"))).selectByVisibleText("MARCA INDUSTRIAL");
		new Select(webDriver.findElement(By.id("modeloCorretor"))).selectByVisibleText("MODELO INDUSTRIAL");
		new Select(webDriver.findElement(By.id("anoFabricacaoInicio"))).selectByVisibleText("2015");
		new Select(webDriver.findElement(By.id("anoFabricacaoFim"))).selectByVisibleText("2017");
		preencherCampo("dataInstalacaoInicio", DataUtil.converterDataParaString(new Date()));
		preencherCampo("dataInstalacaoFim", DataUtil.converterDataParaString(new Date()));
		clicarEmElemento(isClickable(By.xpath("//input[@id='indicadorCondominio' and @value='true']")));
		new Select(webDriver.findElement(By.id("modalidadeMedicaoImovel"))).selectByVisibleText("Coletiva");
		new Select(webDriver.findElement(By.id("situacaoConsumo"))).selectByVisibleText("ATIVO");
		webDriver.findElement(By.id("qtdPontoConsumo")).clear();
		webDriver.findElement(By.id("qtdPontoConsumo")).sendKeys("1");
		new Select(webDriver.findElement(By.id("segmento"))).selectByVisibleText("RESIDENCIAL");
		new Select(webDriver.findElement(By.id("cityGate"))).selectByVisibleText("ETCPIL001");
		new Select(webDriver.findElement(By.id("tronco"))).selectByVisibleText("PIL50001 PILAR MACEIO");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Ação(ões) inserido(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void alterarConfiguracaoComandoAcao() {
		
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Comando de Ação')]"));
		obterElementoVisivel(By.xpath("//input[@name='indicadorSimulacao']"));
		obterElementoVisivel(By.name("nome"));
		waitForElement(By.name("nome")).clear();
		waitForElement(By.name("nome")).sendKeys("TESTE COMANDO");
		preencherCampo("descricao", "TESTE COMANDO");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Ação(ões) alterado(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void removerConfiguracaoComandoAcao() {
		
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Ação Comando removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}
}
