
package br.com.ggas.selenium;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TesteContrato extends SeleniumTestCase {
	int anoAtual = Calendar.getInstance().get(Calendar.YEAR);

	@Ignore // teste intermitente.
	public void testeContrato() throws IOException {
		
		testeImprimirContrato();
		
	}
	
	private void testeImprimirContrato() {
		
		clicarEmElemento(waitForElement(By.xpath("id('32')/a")));
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Contrato')]"));
		obterElementoVisivel(By.id("botaoPesquisar"));
		
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		
		isClickable(By.xpath("//table[@id='contrato']"));
		
		clicarEmElemento(buscarElemento(By.xpath("//a[contains(text(),'BRASKEM SA')]/parent::td/parent::tr/td[1]/input")));

		clicarEmElemento(isClickable(By.id("botaoImprimir")));
		
		String parentWindowHandler = webDriver.getWindowHandle(); // SALVA A JANELA PRINCIPAL
		String subWindowHandler = null;

		Set<String> handles = webDriver.getWindowHandles(); // RETORNA TODOS OS DESCRITORES DAS JANELAS ABERTAS
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}

		webDriver.switchTo().window(subWindowHandler); // MUDA FOCO PARA POP-UP
		
		obterElementoVisivel(By.id("innerPopup"));
		assertTrue(webDriver.getCurrentUrl().contains("exibirImprimirContrato"));
		
		webDriver.switchTo().window(parentWindowHandler); // MUDA FOCO PARA POP-UP
			
	}

	private void testeAditarContrato() {
		
		clicarEmElemento(waitForElement(By.xpath("id('32')/a")));

		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Contrato')]"));

		obterElementoVisivel(By.id("botaoPesquisar"));
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));

		clicarEmElemento(obterElementoVisivel(By.linkText("01/01/1975")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhamento de Contrato')]"));
		obterElementoVisivel(By.xpath("//input[@value='Aditar']"));

		/*ArrayList<WebElement> allrows = (ArrayList<WebElement>) webDriver.findElements(By.xpath("//table[@id='contrato']/tbody/tr"));

		boolean found = false;
		for (WebElement row : allrows) {
			ArrayList<WebElement> cells = (ArrayList<WebElement>) row.findElements(By.tagName("td"));
			if (cells.get(8).getText().equals("01/01/1975")) {
				
				clicarEmElemento(cells.get(8).findElement(By.tagName("a")));
				found = true;
			}
		}
		assertTrue(found);*/
		
		clicarEmElemento(buscarElemento(By.xpath("//input[@value='Aditar']")));

		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Aditar Contrato')]"));
		obterElementoVisivel(By.id("ui-id-1"));
		obterElementoVisivel(By.id("dataAditivo"));
		preencherCampo("dataAditivo", "05/06/2017");
		isClickable(By.id("descricaoAditivo")).sendKeys("ADITIVO TESTE");

		clicarEmElemento(isClickable(By.id("aditar-contrato")));

		assertEquals("Sucesso: Contrato "+String.valueOf(anoAtual)+"00001-01 aditado com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));

	}

	private void testeInserirContrato() {
		
		clicarEmElemento(waitForElement(By.xpath("//li[@id='32']/a/span[normalize-space(text()) = 'Contrato']")) );
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Contrato')]"));

		obterElementoVisivel(By.id("botaoIncluir"));
		clicarEmElemento(isClickable(By.id("botaoIncluir")));

		obterElementoVisivel(By.id("listaModeloContrato"));
		new Select(isClickable(By.id("listaModeloContrato"))).selectByVisibleText("CONTRATO BRASKEM");
		new Select(isClickable(By.id("listaModeloContrato"))).selectByVisibleText("Selecione");
		new Select(isClickable(By.id("listaModeloContrato"))).selectByVisibleText("CONTRATO BRASKEM");
		
		waitForElement(By.id("botaoExibirModeloContrato")).isEnabled();
		clicarEmElemento(isClickable(By.id("botaoExibirModeloContrato")));

		obterElementoVisivel(By.id("situacaoContrato"));
		new Select(isClickable(By.id("situacaoContrato"))).selectByVisibleText("ATIVO");

		preencherCampo("dataAssinatura", "01/01/1975");
		preencherCampo("dataVencObrigacoesContratuais", "01/01/2025");
		
		clicarEmElemento(isClickable(By.id("anoContratualMinusculo")));
		clicarEmElemento(isClickable(By.id("periodicidadeContinua")));
		
		clicarEmElemento(isClickable(By.linkText("* Proposta")));
		obterElementoVisivel(By.xpath("//input[@value='Pesquisar Proposta']"));

		clicarEmElemento(isClickable(By.xpath("//input[@value='Pesquisar Proposta']")));
		
		String parentWindowHandler = webDriver.getWindowHandle(); // SALVA A JANELA PRINCIPAL
		String subWindowHandler = null;

		Set<String> handles = webDriver.getWindowHandles(); // RETORNA TODOS OS DESCRITORES DAS JANELAS ABERTAS
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()) {
			subWindowHandler = iterator.next();
		}

		webDriver.switchTo().window(subWindowHandler); // MUDA FOCO PARA POP-UP
		
		clicarEmElemento(isClickable(By.xpath("//input[@value='Pesquisar']")));

		clicarEmElemento(isClickable(By.linkText("BRASKEM POLO")));

		webDriver.switchTo().window(parentWindowHandler); // VOLTA PARA A JANELA PRINCIPAL

		aceitarAlerta();
		
		isClickable(By.id("avancar-contrato"));

		new Select(isClickable(By.id("diaVencFinanciamento"))).selectByVisibleText("15");

		clicarEmElemento(isClickable(By.linkText("* Dados Financeiros")));
		obterElementoVisivel(By.id("abaDadosFinanceiros"));

		List<WebElement> findElements = webDriver.findElements(By.id("incluirContrato-faturamentoAgrupamento"));
		for (WebElement webElement : findElements) {
			if (webElement.getAttribute("value").equals("true")) {
				clicarEmElemento(webElement);
			}
		}

		obterElementoVisivel(By.id("tipoAgrupamento"));
		scroll(By.id("tipoAgrupamento"));
		new Select(isClickable(By.id("tipoAgrupamento"))).selectByVisibleText("POR VALOR");

		assertTrue(webDriver.findElement(By.id("formaCobrancaValorFixo")).getAttribute("readonly").equals("true"));

		new Select(isClickable(By.id("arrecadadorConvenio"))).selectByVisibleText("103032 - CR - BT");

		isClickable(By.id("percentualMulta")).sendKeys("5");
		isClickable(By.id("percentualJurosMora")).sendKeys("7");

	
		clicarEmElemento(isClickable(By.linkText("* Take or Pay")));
		obterElementoVisivel(By.id("periodicidadeTakeOrPayC"));

		new Select(isClickable(By.id("periodicidadeTakeOrPayC"))).selectByVisibleText("MENSAL");
		new Select(isClickable(By.id("tipoReferenciaQFParadaProgramada"))).selectByVisibleText("QDR");
		new Select(isClickable(By.id("mensalConsumoReferenciaInferior"))).selectByVisibleText("QDC");

		isClickable(By.id("margemVariacaoTakeOrPayC")).sendKeys("80");
		preencherCampo("dataInicioVigenciaC", "01/01/1975");
		preencherCampo("dataFimVigenciaC", "01/01/2025");

		new Select(isClickable(By.id("precoCobrancaC"))).selectByVisibleText("Tabela de Preços Vigente no Momento da Cobrança");
		new Select(isClickable(By.id("tipoApuracaoC"))).selectByVisibleText("Pela Aplicação da QDC na Cascata Tarifária");

		clicarEmElemento(isClickable(By.id("consideraParadaProgramadaCSim")));
		clicarEmElemento(isClickable(By.id("consideraFalhaFornecimentoCSim")));
		clicarEmElemento(isClickable(By.id("consideraCasoFortuitoCSim")));
		clicarEmElemento(isClickable(By.id("TOPrecuperacaoAutoTakeOrPaySim")));
		
		isClickable(By.id("percentualNaoRecuperavelC")).sendKeys("10");

		new Select(isClickable(By.id("apuracaoParadaProgramadaC"))).selectByVisibleText("QDC - Maior entre QDPD e QRetD");
		new Select(isClickable(By.id("apuracaoCasoFortuitoC"))).selectByVisibleText("QDC");
		new Select(isClickable(By.id("apuracaoFalhaFornecimentoC"))).selectByVisibleText("QDP - QRetD");

		clicarEmElemento( isClickable(By.id("botaoAdicionarTakeOrPay")));
		waitForElement(By.name("indicePeriodicidadeTakeOrPay"));
		
		clicarEmElemento( isClickable(By.linkText("* Penalidades")) );
		obterElementoVisivel(By.id("conteinerRetiradaMaiorMenor"));

		new Select(isClickable(By.id("penalidadeRetMaiorMenor"))).selectByVisibleText("RETIRADA A MENOR");
		new Select(isClickable(By.id("periodicidadeRetMaiorMenor"))).selectByVisibleText("MENSAL");
		new Select(isClickable(By.id("baseApuracaoRetMaiorMenor"))).selectByVisibleText("DIÁRIA");

		preencherCampo("dataIniVigRetirMaiorMenor", "01/01/1975");
		preencherCampo("dataFimVigRetirMaiorMenor", "01/01/2025");

		new Select(isClickable(By.id("precoCobrancaRetirMaiorMenor"))).selectByVisibleText("Tarifa Média do Período");
		new Select(isClickable(By.id("tipoApuracaoRetirMaiorMenor")))
				.selectByVisibleText("Pela Aplicação da QDC na Cascata Tarifária");

		isClickable(By.id("percentualCobRetMaiorMenor")).sendKeys("90");
		new Select(isClickable(By.id("consumoReferenciaRetMaiorMenor"))).selectByVisibleText("MENOR ENTRE QDC E QDP");

		isClickable(By.id("percentualRetMaiorMenor")).sendKeys("80");
		clicarEmElemento(isClickable(By.id("botaoAdicionarRetirMaiorMenor")));
		obterElementoVisivel(By.name("indicePenalidadeRetiradaMaiorMenor"));

		clicarEmElemento(isClickable(By.linkText("* Pontos de Consumo")));

		isClickable(By.id("pontoConsumo"));

		ArrayList<WebElement> allrows = (ArrayList<WebElement>) webDriver.findElements(By.xpath("//table[@id='pontoConsumo']/tbody/tr"));
		for (WebElement row : allrows) {
			ArrayList<WebElement> cells = (ArrayList<WebElement>) row.findElements(By.tagName("td"));
			
			selecionarCheckBox(cells.get(0).findElement(By.tagName("input")));
		}
		
		clicarEmElemento(isClickable(By.id("avancar-contrato")));
		obterElementoVisivel(By.id("dadosTecnicosCol1"));

		assertTrue(isClickable(By.id("vazaoInstantaneaMaximaValorFixo")).getAttribute("readonly").equals("true"));
		new Select(isClickable(By.id("faixaPressaoFornecimento"))).selectByVisibleText("2,3000 kPa");

		clicarEmElemento(isClickable(By.linkText("Consumo")));
		obterElementoVisivel(By.id("usoPCS"));

		new Select(isClickable(By.id("locaisAmostragemDisponiveis"))).selectByVisibleText("DISTRIBUIDORA");
		clicarEmElemento(isClickable(By.id("adicionar-local-amostragem")));

		new Select(isClickable(By.id("intervalosAmostragemDisponiveis"))).selectByVisibleText("REAL");
		clicarEmElemento(isClickable(By.id("adicionar-intervalo-amostragem")));

		assertTrue(isClickable(By.id("fornecimentoMaximoDiarioValorFixo")).getAttribute("readonly").equals("true"));
		assertTrue(isClickable(By.id("fornecimentoMinimoMensalValorFixo")).getAttribute("readonly").equals("true"));

		clicarEmElemento(isClickable(By.id("abaFaturamentoPontoConsumo")));
		obterElementoVisivel(By.id("dadosItensFaturamento"));
		new Select(isClickable(By.id("tarifaConsumo"))).selectByVisibleText("RESIDENCIAL TESTE");

		assertTrue(isClickable(By.id("diaVencimentoItemFaturaValorFixo")).getAttribute("readonly").equals("true"));

		new Select(isClickable(By.id("listaDiasDisponiveis"))).selectByVisibleText("15");
		clicarEmElemento(isClickable(By.id("botaoDireita")));

		clicarEmElemento(isClickable(By.id("botaoAdicionarItemFaturamento")));
		
		obterElementoVisivel(By.id("faixaPressaoFornecimento"));

		clicarEmElemento(isClickable(By.linkText("Modalidades Consumo Faturamento")));
		
		obterElementoVisivel(By.id("botaoAdicionar"));
		clicarEmElemento(isClickable(By.id("botaoAdicionar")));
		
		obterElementoVisivel(By.id("faixaPressaoFornecimento"));
		
		clicarEmElemento(isClickable(By.linkText("Responsabilidades")));
		obterElementoVisivel(By.id("tipoResponsabilidade"));

		new Select(buscarElemento(By.id("tipoResponsabilidade"))).selectByVisibleText("GESTOR FINANCEIRO");
		preencherCampo("dataInicioRelacao", "01/01/1975");
		
		clicarEmElemento(isClickable(By.id("botaoIncluirContratoResponsabilidade")));
		
		obterElementoVisivel(By.id("faixaPressaoFornecimento"));

		clicarEmElemento(isClickable(By.id("botaoAplicarIncluirContratoPontoConsumo")));

		clicarEmElemento(isClickable(By.id("salvar-incluir-contrato")));
		
		assertEquals("Sucesso: Contrato "+ String.valueOf(anoAtual)+"00001-00 inserido com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));

	}

	@Test
	public void testeModeloContrato() throws IOException {

		testeInserirModeloContrato();
		testeAlterarModeloContrato();
		testeRemoverModeloContrato();

	}

	private void testeRemoverModeloContrato() {
		isClickable(By.name("chavesPrimarias")).click();
		isClickable(By.name("buttonRemover")).click();
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.name("chavesPrimarias")));
		assertEquals("Sucesso: Modelos de contrato removido(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		isClickable(By.id("descricaoModelo")).clear();
		isClickable(By.id("descricaoModelo")).sendKeys("RESIDENCIAL ORUBE");
		isClickable(By.cssSelector(".campoRadio[value='false']")).click();
		isClickable(By.id("botaoPesquisar")).click();
		isClickable(By.name("chavesPrimarias")).click();
		isClickable(By.name("buttonRemover")).click();
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		assertEquals("Sucesso: Modelos de contrato removido(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		isClickable(By.id("descricaoModelo")).clear();
		isClickable(By.id("descricaoModelo")).sendKeys("RESIDENCIA INDIVIDUAL");
		isClickable(By.cssSelector(".campoRadio[value='false']")).click();
		isClickable(By.id("botaoPesquisar")).click();
		isClickable(By.name("chavesPrimarias")).click();
		isClickable(By.name("buttonRemover")).click();
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		assertEquals("Sucesso: Modelos de contrato removido(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarModeloContrato() {
		isClickable(By.linkText("RESIDENCIA INDIVIDUAL")).click();
		isClickable(By.name("button")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("RESIDENCIAL ORUBE");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("RESID");

		isClickable(By.id("ui-id-3")).click();

		new Select(isClickable(By.id("faixaPressaoFornecimento")))
				.selectByVisibleText("RESIDENCIAL - 0,0000 kPa");
		isClickable(By.name("button")).click();
		isClickable(By.linkText("RESIDENCIAL ORUBE")).click();
		isClickable(By.name("button"));
		isClickable(By.name("button")).click();
		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("RESIDENCIA INDIVIDUAL");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("RESORB");

		isClickable(By.id("ui-id-3")).click();

		new Select(isClickable(By.id("faixaPressaoFornecimento")))
				.selectByVisibleText("RESIDENCIAL - 2,3000 kPa");
		isClickable(By.name("button")).click();
		assertEquals("Sucesso: Modelo de contrato alterado(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirModeloContrato() {
		WebElement element = waitForElement(By.xpath("id('31')/a/span"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		isClickable(By.id("botaoIncluirModeloContrato")).click();

		isClickable(By.id("descricao")).clear();
		isClickable(By.id("descricao")).sendKeys("RESIDENCIA INDIVIDUAL");
		isClickable(By.id("descricaoAbreviada")).clear();
		isClickable(By.id("descricaoAbreviada")).sendKeys("RESORB");
		new Select(isClickable(By.id("tipoModelo"))).selectByVisibleText("Contrato Principal");
		isClickable(By.cssSelector("option[value=\"516\"]")).click();

		clicarEmElemento(isClickable(By.id("valorFixoSituacaoContrato")));

		new Select(webDriver.findElement(By.id("situacaoContrato"))).selectByVisibleText("ATIVO");
		clicarEmElemento(isClickable(By.cssSelector("option[value=\"1\"]")));
		clicarEmElemento(isClickable(By.id("indicadorMultaAtrasoSim")));
		clicarEmElemento(isClickable(By.id("indicadorJurosMoraSim")));
		clicarEmElemento(isClickable(By.id("selIndicadorMultaAtraso")));
		clicarEmElemento(isClickable(By.cssSelector("input#obgIndicadorMultaAtraso.checkObrigatorio1")));
		clicarEmElemento(isClickable(By.id("selIndicadorJurosMora")));
		clicarEmElemento(isClickable(By.id("obgIndicadorJurosMora")));

		clicarEmElemento(isClickable(By.id("valorFixoFormaCobranca")));

		element = isClickable(By.xpath("//li/a[contains(@href,'#Tecnicos')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("selVazaoInstantaneaMaxima")));
		clicarEmElemento(isClickable(By.id("obgVazaoInstantaneaMaxima")));

		clicarEmElemento(isClickable(By.id("valorFixoVazaoInstantaneaMaxima")));

		clicarEmElemento(isClickable(By.id("vazaoInstantaneaMaxima")));
		webDriver.findElement(By.id("vazaoInstantaneaMaxima")).sendKeys("1,2900");
		new Select(webDriver.findElement(By.id("faixaPressaoFornecimento")))
				.selectByVisibleText("RESIDENCIAL - 2,3000 kPa");

		element = isClickable(By.xpath("//li/a[contains(@href,'#Consumo')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("selRegimeConsumo")));
		clicarEmElemento(isClickable(By.id("obgRegimeConsumo")));

		clicarEmElemento(isClickable(By.id("valorFixoRegimeConsumo")));

		new Select(webDriver.findElement(By.id("regimeConsumo"))).selectByVisibleText("DOM-SÁB 24HS/DIA");
		webDriver.findElement(By.cssSelector("#regimeConsumo > option[value=\"14\"]")).click();
		webDriver.findElement(By.id("fornecimentoMaximoDiario")).clear();
		webDriver.findElement(By.id("fornecimentoMaximoDiario")).sendKeys("2,00");
		clicarEmElemento(isClickable(By.id("selFornecimentoMaximoDiario")));
		clicarEmElemento(isClickable(By.id("obgFornecimentoMaximoDiario")));
		clicarEmElemento(isClickable(By.id("selFornecimentoMinimoMensal")));
		clicarEmElemento(isClickable(By.id("obgFornecimentoMinimoMensal")));
		webDriver.findElement(By.id("fornecimentoMinimoMensal")).clear();
		webDriver.findElement(By.id("fornecimentoMinimoMensal")).sendKeys("10,00");
		clicarEmElemento(isClickable(By.id("selConsumoFatFalhaMedicao")));
		new Select(webDriver.findElement(By.id("consumoFatFalhaMedicao"))).selectByVisibleText("MEDIA");
		clicarEmElemento(isClickable(By.cssSelector("#consumoFatFalhaMedicao > option[value=\"2\"]")));
		clicarEmElemento(isClickable(By.id("selLocalAmostragemPCS")));
		clicarEmElemento(isClickable(By.id("obgLocalAmostragemPCS")));

		element = isClickable(By.xpath("//li/a[contains(@href,'#Faturamento')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(webDriver.findElement(By.id("fatPeriodicidade"))).selectByVisibleText("MENSAL COLETOR");
		clicarEmElemento(isClickable(By.cssSelector("#fatPeriodicidade > option[value=\"3\"]")));
		new Select(webDriver.findElement(By.id("tarifaConsumo"))).selectByVisibleText("RESIDENCIAL TESTE");
		clicarEmElemento(isClickable(By.id("selEndFisEntFatCEP")));
		clicarEmElemento(isClickable(By.id("obgEndFisEntFatCEP")));

		clicarEmElemento(isClickable(By.id("valorFixoDiaVencimentoItemFatura")));

		new Select(webDriver.findElement(By.id("diaVencimentoItemFatura"))).selectByVisibleText("15");
		clicarEmElemento(isClickable(By.cssSelector("#diaVencimentoItemFatura > option[value=\"15\"]")));
		new Select(webDriver.findElement(By.id("opcaoVencimentoItemFatura"))).selectByVisibleText("DIA FIXO");
		clicarEmElemento(isClickable(By.cssSelector("#opcaoVencimentoItemFatura > option[value=\"28\"]")));
		clicarEmElemento(isClickable(By.id("selEndFisEntFatCEP")));
		clicarEmElemento(isClickable(By.id("selEndFisEntFatEmail")));
		new Select(webDriver.findElement(By.id("contratoCompra"))).selectByVisibleText("CLP");
		// webDriver.findElement(By.cssSelector("option[value='104']")).click();

		element = isClickable(By.xpath("//li/a[contains(@href,'#regrasFaturamento')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(waitForElement(By.id("modalidade"))).selectByVisibleText("FIRME INFLEXIVEL");
		clicarEmElemento(isClickable(By.cssSelector("#modalidade > option[value='1']")));

		element = isClickable(By.id("selModalidade"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		element = isClickable(By.xpath("//li/a[contains(@href,'#responsabilidade')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);


		clicarEmElemento(isClickable(By.id("selClienteResponsavel")));

		element = isClickable(By.xpath("//li/a[contains(@href,'#impressao')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		waitForElement(By.id("layoutRelatorio"));

		clicarEmElemento(isClickable(By.id("botaoIncluirModeloContrato")));
		assertEquals("Sucesso: Modelo de contrato inserido(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	@Test
	public void testeFalhaDeFornecimento() throws Exception {
		// obtÃ©m o nome da janela principal
		String janelaInicio = webDriver.getWindowHandle();

		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Gás Fora de Especificação')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento( isClickable(By.id("botaoPesquisarCliente")) );

		// muda o foco para a janela pesquisar
		for (String winHandle : webDriver.getWindowHandles()) {
			webDriver.switchTo().window(winHandle);
		}

		isClickable(By.id("nome")).clear();
		isClickable(By.id("nome")).sendKeys("BRASKEM SA");
		clicarEmElemento( isClickable(By.id("botaoPesquisar")) );
		isClickable(By.linkText("BRASKEM SA")).click();

		// volta o foco para a janela principal
		webDriver.switchTo().window(janelaInicio);

		clicarEmElemento( isClickable(By.name("chavesPrimarias")));
		scroll(By.id("buttonManter"));
		clicarEmElemento( isClickable(By.id("buttonManter")));

		isClickable(By.id("dataAviso")).sendKeys("20022017");

		isClickable(By.xpath("//input[contains(@id,'indicadorSolicitante1')]"));
		element = isClickable(By.xpath("//input[contains(@id,'indicadorSolicitante1')]"));
		element.click();

		clicarEmElemento( isClickable(By.id("botaoAtualizar")) );
		clicarEmElemento( isClickable(By.id("botaoSalvar")) );
		assertEquals("Sucesso: Parada(s) alterada(s) com sucesso.", isClickable(By.xpath("//div[@id='inner']/div/div/p"))
				.getAttribute("textContent").replaceAll("[\\n\\r\\t]", "").trim());
	}

	@Test
	public void testeParadaProgramada() throws Exception {

		// obtÃ©m o nome da janela principal
		String janelaInicio = webDriver.getWindowHandle();
		WebElement element = waitForElement(By.xpath("//li[@id='36']/a/span[normalize-space(text()) = 'Programadas']"));
		//WebElement element = waitForElement(By.xpath("//span[contains(text(),'Programadas')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoPesquisarCliente")) );

		// muda o foco para a janela pesquisar
		for (String winHandle : webDriver.getWindowHandles()) {
			webDriver.switchTo().window(winHandle);
		}

		isClickable(By.id("nome")).clear();
		isClickable(By.id("nome")).sendKeys("BRASKEM SA");
		isClickable(By.id("botaoPesquisar")).click();
		obterElementoVisivel(By.linkText("BRASKEM SA"));
		isClickable(By.linkText("BRASKEM SA")).click();

		// volta o foco para a janela principal
		webDriver.switchTo().window(janelaInicio);

		isClickable(By.xpath("//tr/td/input[@name='chavesPrimarias']")).click();
		isClickable(By.id("buttonManter")).click();
		espera();
		isClickable(By.id("dataAviso")).clear();
		isClickable(alterarValorAtributo(By.id("dataAviso"), "16022017"));

		isClickable(By.xpath("//input[contains(@id,'indicadorSolicitante1')]"));
		element = isClickable(By.xpath("//input[contains(@id,'indicadorSolicitante1')]"));
		element.click();

		clicarEmElemento(isClickable(By.id("botaoSalvar")) );
		assertEquals("Sucesso: Parada(s) alterada(s) com sucesso.", isClickable(By.xpath("//div[@id='inner']/div/div/p"))
				.getAttribute("textContent").replaceAll("[\\n\\r\\t]", "").trim());
	}

	@Test
	public void testeParadaNaoProgramada() throws Exception {

		// obtÃ©m o nome da janela principal
		String janelaInicio = webDriver.getWindowHandle();

		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Não Programadas')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		isClickable(By.id("botaoPesquisarCliente")).click();

		// muda o foco para a janela pesquisar
		for (String winHandle : webDriver.getWindowHandles()) {
			webDriver.switchTo().window(winHandle);
		}

		isClickable(By.id("nome")).clear();
		webDriver.findElement(By.id("nome")).sendKeys("BRASKEM SA");
		webDriver.findElement(By.id("botaoPesquisar")).click();
		isClickable(By.linkText("BRASKEM SA")).click();

		// volta o foco para a janela principal
		webDriver.switchTo().window(janelaInicio);

		isClickable(By.xpath("//tr/td/input[@name='chavesPrimarias']")).click();
		isClickable(By.id("buttonManter")).click();

		isClickable(By.xpath("//input[contains(@id,'indicadorSolicitante1')]"));
		element = isClickable(By.xpath("//input[contains(@id,'indicadorSolicitante1')]"));
		isClickable(element);
		element.click();

		preencherCampo("dataAviso", "16/02/2017");

		clicarEmElemento( isClickable(By.id("botaoSalvar")) );
		assertEquals("Sucesso: Parada(s) alterada(s) com sucesso.", isClickable(By.xpath("//div[@id='inner']/div/div/p"))
				.getAttribute("textContent").replaceAll("[\\n\\r\\t]", "").trim());
	}

	@Test
	public void testeProgramacaoDeConsumo() throws Exception {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Solicitação de Consumo')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento( isClickable(By.id("indicadorPesquisDiarioIntradiario")) );
		clicarEmElemento( isClickable(By.id("botaoPesquisar")) );
		clicarEmElemento( isClickable(By.id("botaoSalvar")) );
	}

	@Test
	public void testeAgente() throws Exception {

		WebElement element = waitForElement(By.xpath("//li[@id='252']/a/span[normalize-space(text()) = 'Agente']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento( isClickable(By.cssSelector("input[value='Incluir']")) );
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Agente')]"));

		isClickable(By.id("botaoPesquisarFuncionario"));
		String janelaInicio = super.trocarJanela(By.id("botaoPesquisarFuncionario"));

		clicarEmElemento( isClickable(By.id("botaoPesquisar")) );
		clicarEmElemento( isClickable(By.linkText("ADMINISTRADOR")));

		webDriver.switchTo().window(janelaInicio);
		
		waitForElement(By.id("nomeFuncionarioTexto"));
		elementoPossuiAtributoValor(By.id("nomeFuncionarioTexto"), "ADMINISTRADOR");
	
		isClickable(By.cssSelector("input[value='Salvar']")).submit();
		
		assertEquals("Sucesso: Agente inserido(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento( isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento( isClickable(By.xpath("//input[@value='Alterar']")));
		clicarEmElemento( isClickable(By.xpath("//input[@name='habilitado' and @value='false']")));
		
		isClickable(By.xpath("//input[@name='button' and @value='Salvar']")).submit();

		assertEquals("Sucesso: Agente alterado(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento( isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento( isClickable(By.xpath("//input[@value='Remover']")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Agente removida(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

	}

}
