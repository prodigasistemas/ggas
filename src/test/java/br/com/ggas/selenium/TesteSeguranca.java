
package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteSeguranca extends SeleniumTestCase {

	private static final Logger LOG = Logger.getLogger(TesteSeguranca.class);

	@Test
	public void testeControleAcesso() {
		testeInserirControleAcesso();
		testeAlterarControleAcesso();
		testeRemoverControleAcesso();
	}
		
	private void testeRemoverControleAcesso() {
		clicarEmElemento(isClickable(By.cssSelector("#habilitado[value='false'][tabindex='4']")));
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		espera(1000);
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		espera(1000);
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		
		assertEquals("Erro: O Usuário selecionado não pode ser removido porque existem registros associados ao mesmo.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarControleAcesso() {
		waitForElement(By.id("botaoPesquisar"));
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		espera(1000);
		waitForElement(By.id("checkboxChavesPrimarias"));
		waitForElement(By.id("botaoAlterar"));
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		espera(1000);
		waitForElement(By.id("senhaExpirada"));
		waitForElement(By.id("botaoSalvar"));
		clicarEmElemento(isClickable(By.id("senhaExpirada")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		espera(1000);
		assertEquals("Sucesso: Operação realizada com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirControleAcesso() {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Controle de Acesso')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Controle de Acesso de Usuários')]"));
		
		espera(3000);
		new Select(isClickable(waitForElement(By.id("idEmpresa")))).selectByIndex(2);
		new Select(isClickable(waitForElement(By.id("idUnidadeOrganizacional")))).selectByIndex(1);
		new Select(isClickable(waitForElement(By.id("funcionario")))).selectByIndex(1);
		isClickable(By.id("loginFunc")).clear();
		isClickable(By.id("loginFunc")).sendKeys("erick.simoes");
		new Select(waitForElement(By.id("papelCadastrado"))).selectByValue("1");
		clicarEmElemento(isClickable(By.id("botaoMoverDireita")));
		isClickable(By.id("periodicidadeSenha")).clear();
		isClickable(By.id("periodicidadeSenha")).sendKeys("15");
		isClickable(By.id("email")).clear();
		isClickable(By.id("email")).sendKeys("erick.simoes@teste.com.br");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		espera(1000);
		assertEquals("Sucesso: Operação realizada com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeControlePerfil() {
		testeInserirControlePerfil();
		testeAlterarControlePerfil();
		testeRemoverControlePerfil();
	}

	private void testeRemoverControlePerfil() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Papel removido(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarControlePerfil() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
	
		obterElementoVisivel(By.xpath("//h1[text()='Alterar Perfil']"));
		obterElementoVisivel(By.id("browser"));
		
		clicarEmElemento(isClickable(By.id("botaoLimpar")));
		
		new Select(waitForElement(By.id("idResponsavel"))).selectByVisibleText("ADMINISTRADOR");
		
		alterarValorAtributo(By.id("descricao"), "ERICK SIMOES");
		waitForElement(By.id("descricaoPapel")).clear();
		preencherCampo("descricaoPapel", "Perfil atualizado para realização de testes.");
		
		obterElementoVisivel(By.id("descricao"));
		obterElementoVisivel(By.id("descricaoPapel"));
		obterElementoVisivel(By.id("idResponsavel"));

		clicarEmElemento(isClickable(By.cssSelector("div.hitarea.closed-hitarea")));
		clicarEmElemento(isClickable(By.xpath("//li[@id='74']/div")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		final String mensagemNotificacao = webDriver.findElement(By.className("notification")).getAttribute("textContent");
		LOG.info("Mensagem de notificação Alteracao Controle Perfil: " + mensagemNotificacao);
		assertEquals("Sucesso: Papel alterado(a) com sucesso.", mensagemNotificacao.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirControlePerfil() {
		
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Controle de Perfil')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));

		obterElementoVisivel(By.xpath("//h1[text()='Incluir Perfil']"));
		obterElementoVisivel(By.id("browser"));

		new Select(waitForElement(By.id("idResponsavel"))).selectByVisibleText("ADMINISTRADOR");
		
		preencherCampo("descricao", "LEITURISTA");
		preencherCampo("descricaoPapel", "Perfil criado para realização de testes.");
		
		obterElementoVisivel(By.id("descricao"));
		obterElementoVisivel(By.id("descricaoPapel"));
		obterElementoVisivel(By.id("idResponsavel"));

		clicarEmElemento(isClickable(By.cssSelector("div.hitarea.closed-hitarea")));
		clicarEmElemento(isClickable(By.xpath("//li[@id='74']/div")));
		clicarEmElemento(isClickable(By.id("1107")));
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
				
		assertEquals("Sucesso: Papel inserido(a) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

}
