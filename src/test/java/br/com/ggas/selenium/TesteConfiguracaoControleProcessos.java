package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import br.com.ggas.util.DataUtil;

public class TesteConfiguracaoControleProcessos extends SeleniumTestCase {

	@Test
	public void testControleDeProcessos() throws Exception {
		executarControleProcesso();
		agendarControleProcesso();
		cancelarAgendamentoControleProcesso();
	}

	public void agendarControleProcesso() {
		clicarEmElemento(isClickable(By.id("botaoAgendar")));
		obterElementoVisivel(By.id("idModulo"));
		new Select(isClickable(By.id("idModulo"))).selectByVisibleText("Atendimento Ao Público");
		new Select(isClickable(By.id("idOperacao"))).selectByVisibleText("Gerar Pesquisa de Satisfação em Lote");
		new Select(isClickable(By.id("idPeriodicidade"))).selectByVisibleText("Mensal");

		Calendar dataAtual = Calendar.getInstance();
		int diaHoje = dataAtual.get(Calendar.DAY_OF_WEEK);
		Date dataAgendada = dataAtual.getTime();

		if (diaHoje == Calendar.SUNDAY || diaHoje == Calendar.SATURDAY) {
			dataAgendada = DataUtil.incrementarDia(dataAgendada, 2);
		} else if (diaHoje == Calendar.FRIDAY) {
			dataAgendada = DataUtil.incrementarDia(dataAgendada, 3);
		} else {
			dataAgendada = DataUtil.incrementarDia(dataAgendada, 1);
		}
		String dataParaAgendar = DataUtil.converterDataParaString(dataAgendada);
		
		preencherCampo("dataAgendada", dataParaAgendar);
        
		new Select(webDriver.findElement(By.id("hora"))).selectByVisibleText("9");
		webDriver.findElement(By.id("emailResponsavel")).clear();
		webDriver.findElement(By.id("emailResponsavel")).sendKeys("teste@teste.com");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		waitForElement(By.className("notification"));
		assertEquals("Sucesso: O processo foi agendado com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	public void cancelarAgendamentoControleProcesso() {
		clicarEmElemento(waitForElement(By.xpath("//input[@name='chavesPrimarias' and @type='checkbox']")));
		clicarEmElemento(isClickable(By.id("btnCancelar")));
		assertTrue(super.obterTextoDoAlerta().matches("^Tem certeza que deseja cancelar o agendamento[\\s\\S] $"));

		waitForElementNotPresent(buscarElemento(By.xpath("//i[@title='Em espera']")));
		waitForElement(By.className("notification"));
		assertEquals("Sucesso: O(s) processo(s) foi(foram) cancelado(s) com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	public void executarControleProcesso() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='85']/a/span[normalize-space(text()) = 'Controle de Processos']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoExecutar")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Executar Processos')]"));
		obterElementoVisivel(By.id("idModulo"));
		new Select(isClickable(By.id("idModulo"))).selectByVisibleText("Atendimento Ao Público");
		isClickable((By.id("idOperacao")));
		new Select(webDriver.findElement(By.id("idOperacao")))
				.selectByVisibleText("Gerar Pesquisa de Satisfação em Lote");
		clicarEmElemento(isClickable(By.id("botaoExecutar")));

		waitForElement(By.className("notification"));
		assertEquals("Sucesso: O processo foi executado com sucesso.",
				webDriver.findElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
}
