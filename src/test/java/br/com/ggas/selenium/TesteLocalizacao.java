
package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteLocalizacao extends SeleniumTestCase {

	@Test
	public void testeGerenciaRegional() {

		//testeInserirGerenciaRegional();
		//testeAlterarGerenciaRegional();
		//testeRemoverGerenciaRegional();
	}

	private void testeRemoverGerenciaRegional() {
		clicarEmElemento(isClickable(By.name("chavesPrimarias")));
		clicarEmElemento(isClickable(By.name("buttonAlterar")));
		clicarEmElemento(isClickable(By.name("button")));
		clicarEmElemento(isClickable(By.name("chavesPrimarias")));
		clicarEmElemento(isClickable(By.name("buttonRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.name("chavesPrimarias")));
		assertEquals("Sucesso: Gerência Regional removida(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarGerenciaRegional() {
		clicarEmElemento(isClickable(By.linkText("TESTE1")));
		clicarEmElemento(isClickable(By.name("button")));
		waitForElement(By.id("complemento")).clear();
		waitForElement(By.id("complemento")).sendKeys("PISO 65");
		waitForElement(By.id("nome")).clear();
		waitForElement(By.id("nome")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Gerência Regional alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirGerenciaRegional() {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Gerência Regional')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.name("button")));
		waitForElement(By.id("nome")).clear();
		waitForElement(By.id("nome")).sendKeys("TESTE1");
		waitForElement(By.id("nomeAbreviado")).clear();
		waitForElement(By.id("nomeAbreviado")).sendKeys("RG");
		waitForElement(By.id("dddTelefone")).clear();
		waitForElement(By.id("dddTelefone")).sendKeys("81");
		waitForElement(By.id("fone")).clear();
		waitForElement(By.id("fone")).sendKeys("33250510");
		waitForElement(By.id("ramalFone")).clear();
		waitForElement(By.id("ramalFone")).sendKeys("54");
		waitForElement(By.id("dddFax")).clear();
		waitForElement(By.id("dddFax")).sendKeys("81");
		waitForElement(By.id("fax")).clear();
		waitForElement(By.id("fax")).sendKeys("33250510");
		waitForElement(By.id("email")).clear();
		waitForElement(By.id("email")).sendKeys("procenge@procenge.com.br");
		waitForElement(By.id("cep")).clear();
		waitForElement(By.id("cep")).sendKeys("57052-790");
		waitForElement(By.id("numero")).clear();
		waitForElement(By.id("numero")).sendKeys("54");
		waitForElement(By.id("complemento")).clear();
		waitForElement(By.id("complemento")).sendKeys("656");
		waitForElement(By.id("enderecoReferencia")).clear();
		waitForElement(By.id("enderecoReferencia")).sendKeys("32323");
		waitForElement(By.id("complemento")).clear();
		waitForElement(By.id("complemento")).sendKeys("PISO 6");
		waitForElement(By.id("enderecoReferencia")).clear();
		waitForElement(By.id("enderecoReferencia")).sendKeys("PROXIMO AO SHOPPING");
		clicarEmElemento(isClickable(By.xpath("//input[@name='Button' and @value='Salvar']")));
		assertEquals("Sucesso: Gerência Regional inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeLocalidade() {

		testeInserirLocalidade();
		testeAlterarLocalidade();
		testeRemoverLocalidade();
	}

	private void testeRemoverLocalidade() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.name("buttonRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Localidades removida(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarLocalidade() {
		clicarEmElemento(isClickable(By.linkText("TESTE")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		waitForElement(By.name("numeroEndereco")).clear();
		waitForElement(By.name("numeroEndereco")).sendKeys("8623");
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Localidade alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirLocalidade() {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Localidade')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.name("button")));
		waitForElement(By.name("codigo")).clear();
		waitForElement(By.name("codigo")).sendKeys("111");
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		waitForElement(By.name("numeroEndereco")).clear();
		waitForElement(By.name("numeroEndereco")).sendKeys("862");
		waitForElement(By.name("complementoEndereco")).clear();
		waitForElement(By.name("complementoEndereco")).sendKeys("PISO6");
		waitForElement(By.id("dddTelefone")).clear();
		waitForElement(By.id("dddTelefone")).sendKeys("81");
		waitForElement(By.name("fone")).clear();
		waitForElement(By.name("fone")).sendKeys("33250510");
		waitForElement(By.name("ramalFone")).clear();
		waitForElement(By.name("ramalFone")).sendKeys("5623");
		waitForElement(By.id("dddFax")).clear();
		waitForElement(By.id("dddFax")).sendKeys("81");
		waitForElement(By.name("fax")).clear();
		waitForElement(By.name("fax")).sendKeys("33255510");
		waitForElement(By.name("email")).clear();
		waitForElement(By.name("email")).sendKeys("procenge@procenge.com.br");
		new Select(waitForElement(By.id("idGerenciaRegional"))).selectByVisibleText("PROCENGE INFORMATICA");
		new Select(waitForElement(By.id("idLocalidadeClasse"))).selectByVisibleText("CIDADE");
		new Select(waitForElement(By.id("idLocalidadePorte"))).selectByVisibleText("PEQUENO");
		waitForElement(By.name("codigoCentroCusto")).clear();
		waitForElement(By.name("codigoCentroCusto")).sendKeys("TG");
		clicarEmElemento(isClickable(By.name("informatizada")));
		new Select(waitForElement(By.id("idMedidorLocalArmazenagem"))).selectByVisibleText("ALX");
		waitForElement(By.name("codigoCentroCusto")).clear();
		waitForElement(By.name("codigoCentroCusto")).sendKeys("TGS");
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Localidade inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeMunicipio() {
		testeInserirMunicipio();
		testeAlterarMunicipio();
		testeRemoverMunicipio();
	}

	private void testeRemoverMunicipio() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.cssSelector("input[value='Remover']")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Município removido(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarMunicipio() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.name("Alterar")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Município alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirMunicipio() {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Município')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.name("button")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("CAPIXABA");
		obterElementoVisivel(By.id("cepInicio"));
		preencherCampo("cepInicio", "54725-000");
		obterElementoVisivel(By.id("cepFim"));
		preencherCampo("cepFim", "54725-000");
		waitForElement(By.id("ddd")).clear();
		waitForElement(By.id("ddd")).sendKeys("81");
		new Select(waitForElement(By.id("idUnidadeFederacao"))).selectByVisibleText("TESTE UF");
		new Select(waitForElement(By.id("idRegiao"))).selectByVisibleText("TESTE REGIAO");
		new Select(waitForElement(By.id("idMicrorregiao"))).selectByVisibleText("TESTE MICRORREGIAO");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Município inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeSetorComercial() {

		testeInserirSetorComercial();
		testeAlterarSetorComercial();
		testeRemoverSetorComercial();
	}

	private void testeRemoverSetorComercial() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.name("buttonRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Setor(s) Comercial(s) removida(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarSetorComercial() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.name("buttonAlterar")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE SETOR");
		new Select(waitForElement(By.id("idMunicipio"))).selectByVisibleText("ANADIA");
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Setor Comercial alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirSetorComercial() {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Setor Comercial')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.name("button")));
		clicarEmElemento(isClickable(By.id("idGerenciaRegional")));
		new Select(waitForElement(By.id("idGerenciaRegional"))).selectByVisibleText("PROCENGE INFORMATICA");
		waitForElement(By.id("codigo")).clear();
		waitForElement(By.id("codigo")).sendKeys("111");
		new Select(waitForElement(By.id("idUnidadeFederacao"))).selectByVisibleText("ALAGOAS");
		element = waitForElement(By.id("idLocalidade"));
		new Select(element).selectByVisibleText("LOCALIDADE 7");
		clicarEmElemento(isClickable(By.cssSelector("option[value=\"2\"]")));
		element = waitForElement(By.id("idMunicipio"));
		new Select(element).selectByVisibleText("CAMPESTRE");
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTEd");
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Setor Comercial inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testePerfilQuadra() {

		testeInserirPerfilQuadra();
		testeAlterarPerfilQuadra();
		testeRemoverPerfilQuadra();
	}

	private void testeRemoverPerfilQuadra() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Perfil Quadra removida(s) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarPerfilQuadra() {
		
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		
		espera();
		
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("Perfil Testet");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Perfil de Quadra')]"));
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.linkText("PERFIL TESTET")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Perfil de Quadra ')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Perfil de Quadra')]"));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Perfil Quadra alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirPerfilQuadra() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='136']/a/span[normalize-space(text()) = 'Perfil Quadra']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("Perfil Teste");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals("Sucesso: Perfil Quadra inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeSetorCensitario() {

		testeInserirSetorCensitario();
		testeAlterarSetorCensitario();
		testeRemoverSetorCensitario();
	}

	private void testeRemoverSetorCensitario() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Setor Censitário removido(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarSetorCensitario() {
		clicarEmElemento(isClickable(By.linkText("SETOR")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		new Select(waitForElement(By.id("municipio"))).selectByVisibleText("CANAPI");
		clicarEmElemento(isClickable(By.xpath("(//input[@id='habilitado'])[2]")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));

		assertEquals("Sucesso: Setor Censitário alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		clicarEmElemento(isClickable(By.id("habilitado")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Setor Censitário alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirSetorCensitario() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='160']/a/span[normalize-space(text()) = 'Setor Censitário']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("SETOR");
		new Select(waitForElement(By.id("municipio"))).selectByIndex(1);
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Pesquisar Setor Censitário')]"));
		assertEquals("Sucesso: Setor Censitário inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

	}

	@Test
	public void testeZeis() {

		WebElement element = waitForElement(By.xpath("//li[@id='159']/a/span[normalize-space(text()) = 'Zeis']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.name("button")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("ZEIS TESTE");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("ZEIS");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Zeis inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		clicarEmElemento(isClickable(By.linkText("ZEIS TESTE")));
		clicarEmElemento(isClickable(By.name("button")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("TES");
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Zeis alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='buttonRemover' and @value='Remover']")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
	}

	@Test
	public void testeQuadra() {

		WebElement element = webDriver.findElement(By.xpath("//li[@id='17']/a/span[normalize-space(text()) = 'Quadra']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		webDriver.findElement(By.name("button")).click();
		new Select(webDriver.findElement(By.id("idGerenciaRegional"))).selectByVisibleText("PROCENGE INFORMATICA");
		element = waitForElement(By.id("idLocalidade"));
		new Select(element).selectByVisibleText("LOCALIDADE 7");
		element = waitForElement(By.id("idSetorComercial"));
		new Select(element).selectByVisibleText("SETOR TESTE");
		webDriver.findElement(By.id("numeroQuadra")).clear();
		webDriver.findElement(By.id("numeroQuadra")).sendKeys("321");
		new Select(webDriver.findElement(By.id("idPerfilQuadra"))).selectByVisibleText("CLASSE MEDIA");
		webDriver.findElement(By.id("cep")).clear();
		webDriver.findElement(By.id("linkPesquisarCEP")).click();
		webDriver.findElement(By.id("logradouro")).clear();
		webDriver.findElement(By.id("logradouro")).sendKeys("FERNANDO CALIXTO");
		webDriver.findElement(By.id("cidade")).clear();
		webDriver.findElement(By.id("cidade")).sendKeys("PALMEIRA DOS INDIOS");
		new Select(webDriver.findElement(By.id("uf"))).selectByVisibleText("AL");
		element = webDriver.findElement(By.cssSelector("input#botaoPesquisarCEPcep.bottonRightCol.botaoPesquisarCEP"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		element = super.waitForElement(
				By.cssSelector("option[title='RUA FERNANDO CALIXTO, PALMEIRA DE FORA, 57608-010'"));
		element.click();
		webDriver.findElement(By.id("linkPesquisarCEP")).click();
		webDriver.findElement(By.id("numeroFace")).clear();
		webDriver.findElement(By.id("numeroFace")).sendKeys("321");
		webDriver.findElement(By.id("botaoIncluirFace")).click();
		webDriver.findElement(By.cssSelector("input[value='Incluir']")).click();
		assertEquals("Sucesso: Quadra inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.name("buttonAlterar")).click();
		webDriver.findElement(By.id("numeroDeQuadra")).clear();
		webDriver.findElement(By.id("numeroDeQuadra")).sendKeys("333");
		new Select(webDriver.findElement(By.id("idPerfilQuadra"))).selectByVisibleText("CLASSE BAIXA");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Quadra alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.name("buttonRemover")).click();
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Quadra removida(s) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeTipoDeArea() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='162']/a/span[normalize-space(text()) = 'Tipo de área']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.name("button")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("AREA PARA TESTE");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("APT");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Tipo de Área inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='buttonRemover' and @value='Alterar']")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='habilitado' and @value='false']")));
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Tipo de Área alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='buttonRemover' and @value='Remover']")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Tipo de Área removida(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeUnidadeFederativa() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='138']/a/span[normalize-space(text()) = 'Unidade Federativa']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("UNIDADE FEDERATIVA TESTE");
		waitForElement(By.id("sigla")).clear();
		waitForElement(By.id("sigla")).sendKeys("UT");
		new Select(waitForElement(By.id("idPais"))).selectByVisibleText("BRASIL");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Unidade Federativa inserido(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='habilitado' and @value='false']")));
		clicarEmElemento(isClickable(By.xpath("//input[@name='button' and @value='Salvar']")));
		assertEquals("Sucesso: Unidade Federativa alterado(a) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());

		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Unidade Federativa removida(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

}
