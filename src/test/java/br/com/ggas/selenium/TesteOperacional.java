
package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteOperacional extends SeleniumTestCase {

	@Test
	public void testeCityGate() {

		testeInserirCityGate();
		testeAlterarCityGate();
		testeRemoverCityGate();
	}

	private void testeRemoverCityGate() {
		
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: City Gate removido(s) com sucesso.", webDriver.findElement(By.className("notification"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarCityGate() {
		
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[text()='Alterar City Gate']"));
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("Tes");
		waitForElement(By.id("endereco")).clear();
		waitForElement(By.id("endereco")).sendKeys("TESTE");
		waitForElement(By.id("numeroImovel")).clear();
		waitForElement(By.id("numeroImovel")).sendKeys("4");
		waitForElement(By.id("complemento")).clear();
		waitForElement(By.id("complemento")).sendKeys("dfdfd");
		clicarEmElemento(isClickable(By.id("habilitado")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: City Gate alterado(a) com sucesso.", webDriver.findElement(By.className("notification"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirCityGate() {
		WebElement element = webDriver.findElement(By.xpath("//span[contains(text(),'City Gate')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("incluir")));
		
		obterElementoVisivel(By.xpath("//h1[text()='Incluir City Gate']"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		new Select(webDriver.findElement(By.id("idLocalidade"))).selectByVisibleText("LOCALIDADE 7");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: City Gate inserido(a) com sucesso.", webDriver.findElement(By.className("notification"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeTronco() {

		testeInserirTronco();
		testeAlterarTronco();
		testeRemoverTronco();
	}

	private void testeRemoverTronco() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.xpath("//input[@name='buttonRemover' and @value='Remover']")).click();
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Tronco removido(s) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarTronco() {
		webDriver.findElement(By.linkText("TESTE TRONCO")).click();
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("TESTE");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Tronco alterado(a) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirTronco() {
		WebElement element = webDriver.findElement(By.xpath("//span[contains(text(),'Tronco')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("TESTE TRONCO");
		new Select(webDriver.findElement(By.id("idCityGate"))).selectByVisibleText("TESTE3");
		webDriver.findElement(By.id("botaoSalvar")).click();
		assertEquals("Sucesso: Tronco inserido(a) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeRedeDiametro() {

		testeInserirRedeDiametro();
		testeAlterarRedeDiametro();
		testeRemoverRedeDiametro();
	}

	private void testeRemoverRedeDiametro() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Diâmetro de Rede removido(s) com sucesso.",
						webDriver.findElement(By.className("notification")).getAttribute("textContent")
										.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarRedeDiametro() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Diâmetro de Rede')]"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Diâmetro de Rede alterado(a) com sucesso.",
						webDriver.findElement(By.className("notification")).getAttribute("textContent")
										.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirRedeDiametro() {
		WebElement element = webDriver.findElement(By.xpath("//span[contains(text(),'Diâmetro da Rede')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Diâmetro de Rede')]"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("testeD");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Diâmetro de Rede inserido(a) com sucesso.",
						webDriver.findElement(By.className("notification")).getAttribute("textContent")
										.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeRedeMaterial() {

		testeInserirRedeMaterial();
		testeAlterarRedeMaterial();
		testeRemoverRedeMaterial();
	}

	private void testeRemoverRedeMaterial() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		espera(1000);
		assertEquals("Sucesso: Material de Rede removido(s) com sucesso.",
						webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p")).getAttribute("textContent")
										.replaceAll("[\\n\\r\\t]", "").replaceAll("  ", " "));
	}

	private void testeAlterarRedeMaterial() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.id("botaoAlterar")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("TESTE");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Material de Rede alterado(a) com sucesso.",
						webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p")).getAttribute("textContent")
										.replaceAll("[\\n\\r\\t]", "").replaceAll("  ", " "));
	}

	private void testeInserirRedeMaterial() {
		WebElement element = webDriver.findElement(By.xpath("//span[contains(text(),'Material da Rede')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		webDriver.findElement(By.id("botaoPesquisar")).click();
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("INOX");
		webDriver.findElement(By.id("botaoSalvar")).click();
		assertEquals("Sucesso: Material de Rede inserido(a) com sucesso.",
						webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p")).getAttribute("textContent")
										.replaceAll("[\\n\\r\\t]", "").replaceAll("  ", " "));
	}


	@Ignore
	public void testeRedeExterna() {

		testeInserirRedeExterna();
		testeAlterarRedeExterna();
		testeRemoverRedeExterna();
	}

	private void testeRemoverRedeExterna() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.name("buttonRemover")).click();
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Rede removida(s) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarRedeExterna() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.name("buttonAlterar")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("TESTE");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Rede alterado(a) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirRedeExterna() {
		WebElement element = webDriver.findElement(By.xpath("//span[contains(text(),'Rede Externa')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.name("button")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Rede Externa')]"));
		webDriver.findElement(By.id("descricao")).clear();
		espera();
		webDriver.findElement(By.id("descricao")).sendKeys("REDET");
		espera();
		webDriver.findElement(By.id("extensao")).clear();
		espera();
		webDriver.findElement(By.id("extensao")).sendKeys("123");
		new Select(webDriver.findElement(By.id("idUnidadeExtensao"))).selectByVisibleText("Metro Cúbico");
		webDriver.findElement(By.id("pressao")).clear();
		webDriver.findElement(By.id("pressao")).sendKeys("123");
		new Select(webDriver.findElement(By.id("idUnidadePressao"))).selectByVisibleText("Libras/Polegada Quadrada");
		espera();
		new Select(webDriver.findElement(By.id("idLocalidade"))).selectByVisibleText("LOCALIDADE 7");
		espera();
		new Select(webDriver.findElement(By.id("idRedeDiametro"))).selectByVisibleText("DIAMETRO");
		espera();
		new Select(webDriver.findElement(By.id("idRedeMaterial"))).selectByVisibleText("FERRO");
		espera();
		new Select(webDriver.findElement(By.id("idCityGate"))).selectByVisibleText("TESTE3");
		espera();
		element = super.waitForElement(By.id("idTronco"));
		new Select(element).selectByVisibleText("TESTE1");
		espera();
		clicarEmElemento(isClickable(By.id("botaoIncluirTronco")));
		clicarEmElemento(isClickable(By.name("button")));
		assertEquals("Sucesso: Rede inserido(a) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeRamal() {

		testeInserirRamal();
		testeAlterarRamal();
		testeRemoverRamal();
	}

	private void testeRemoverRamal() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.cssSelector("input[value='Remover']")).click();
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Ramal removida(s) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarRamal() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.name("buttonRemover")).click();
		webDriver.findElement(By.id("nome")).clear();
		webDriver.findElement(By.id("nome")).sendKeys("TESTE");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Ramal alterado(a) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirRamal() {
		WebElement element = webDriver.findElement(By.xpath("//span[contains(text(),'Ramal')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		webDriver.findElement(By.id("botaoPesquisar")).click();
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("nome")).clear();
		webDriver.findElement(By.id("nome")).sendKeys("TESTED");
		new Select(webDriver.findElement(By.id("idRede"))).selectByVisibleText("REDE");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Ramal inserido(a) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeEstacao() {

		testeInserirEstacao();
		testeAlterarEstacao();
		testeRemoverEstacao();
	}

	private void testeRemoverEstacao() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		
		webDriver.findElement(By.xpath("//input[@name='buttonRemover' and @value='Remover']")).click();
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		espera();
		//waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: ERP removida(s) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarEstacao() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.name("buttonRemover")).click();
		webDriver.findElement(By.id("numero")).clear();
		webDriver.findElement(By.id("numero")).sendKeys("89");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Estação alterado(a) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirEstacao() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='21']/a/span[normalize-space(text()) = 'Estação']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		webDriver.findElement(By.name("button")).click();
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Inserir Estação')]"));

		webDriver.findElement(By.cssSelector("input[value='true']")).click();

		new Select(webDriver.findElement(By.id("idGerenciaRegional"))).selectByVisibleText("PROCENGE INFORMATICA");
		webDriver.findElement(By.id("nome")).clear();
		webDriver.findElement(By.id("nome")).sendKeys("Estacao 001");
		element = waitForElement(By.id("idLocalidade"));
		new Select(element).selectByVisibleText("LOCALIDADE 7");
		new Select(webDriver.findElement(By.id("idTroncoAnterior"))).selectByVisibleText("TESTE1");
		new Select(webDriver.findElement(By.id("idTroncoPosterior"))).selectByVisibleText("TESTE1");
		webDriver.findElement(By.id("nome")).clear();
		webDriver.findElement(By.id("nome")).sendKeys("Estacao 001");
		webDriver.findElement(By.id("numero")).clear();
		webDriver.findElement(By.cssSelector("input[value='true']")).click();
		webDriver.findElement(By.id("numero")).sendKeys("56");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Estação inserido(a) com sucesso.", webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p"))
						.getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeZonaBloqueio() {

		testeInserirZonaBloqueio();
		testeAlterarZonaBloqueio();
		testeRemoverZonaBloqueio();
	}

	private void testeRemoverZonaBloqueio() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		assertTrue(obterTextoDoAlerta()
						.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Zona de Bloqueio removido(s) com sucesso.",
						webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p")).getAttribute("textContent")
										.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarZonaBloqueio() {
		
		webDriver.findElement(By.linkText("TES")).click();
		webDriver.findElement(By.id("botaoAlterar")).click();
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Zona de Bloqueio alterado(a) com sucesso.",
						webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p")).getAttribute("textContent")
										.replaceAll("[\\n\\r\\t]", ""));
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		webDriver.findElement(By.id("botaoAlterar")).click();
		webDriver.findElement(By.xpath("(//input[@id='habilitado'])[2]")).click();
		
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("TESTE");
		
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Zona de Bloqueio alterado(a) com sucesso.",
						webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p")).getAttribute("textContent")
										.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirZonaBloqueio() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='161']/a/span[normalize-space(text()) = 'Zona de Bloqueio']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		webDriver.findElement(By.id("botaoPesquisar")).click();
		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("TES");
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Zona de Bloqueio inserido(a) com sucesso.",
				webDriver.findElement(By.cssSelector("div[id='inner'] > div > div > p")).getAttribute("textContent")
								.replaceAll("[\\n\\r\\t]", ""));
		
		
	}
}
