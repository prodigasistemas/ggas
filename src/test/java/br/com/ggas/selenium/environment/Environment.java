
package br.com.ggas.selenium.environment;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebDriver;

public abstract class Environment {

	protected static final Log logger = LogFactory.getLog(Environment.class);

	public WebDriver getWebDriver(HashMap<String, Object> parameters) {
		WebDriver webDriver = this.initializeWebDriver(parameters);
		this.before(webDriver);
		return webDriver;
	}
	

	protected abstract WebDriver initializeWebDriver(HashMap<String, Object> parameters);

	public abstract void before(WebDriver webDriver);

}
