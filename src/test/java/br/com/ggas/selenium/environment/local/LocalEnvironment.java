
package br.com.ggas.selenium.environment.local;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import br.com.ggas.selenium.environment.Environment;


public class LocalEnvironment extends Environment {

	@Override
	protected WebDriver initializeWebDriver(HashMap<String, Object> parameters) {

		return new FirefoxDriver();
	}

	@Override
	public void before(WebDriver webDriver) {
		webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

}
