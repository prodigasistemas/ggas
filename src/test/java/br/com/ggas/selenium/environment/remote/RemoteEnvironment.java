
package br.com.ggas.selenium.environment.remote;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;

import br.com.ggas.selenium.environment.Environment;


public class RemoteEnvironment extends Environment {

	@Value("#{systemEnvironment['SAUCE_USERNAME']}")
	private String username;

	@Value("#{systemEnvironment['SAUCE_ACCESS_KEY']}")
	private String accessKey;

	@Value("#{systemEnvironment['SELENIUM_BROWSER']}")
	private String browserName;

	@Value("#{systemEnvironment['SELENIUM_VERSION']}")
	private String browserVersion;

	@Value("#{systemEnvironment['SELENIUM_PLATFORM']}")
	private String plataform;

	@Value("#{systemEnvironment['BUILD_TAG']}")
	private String buildTag;

	private final String seleniumURI = "@ondemand.saucelabs.com:80";

	@Override
	protected WebDriver initializeWebDriver(HashMap<String, Object> parameters) {

		URL sauceUrl;
		try {
			DesiredCapabilities capabilities = this.getDesiredCapabilities(parameters);
			sauceUrl = new URL("http://" + username + ":" + accessKey + seleniumURI + "/wd/hub");
			return new RemoteWebDriver(sauceUrl, capabilities);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}

	public DesiredCapabilities getDesiredCapabilities(HashMap<String, Object> parameters) {

		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

		if (!StringUtils.isEmpty(browserName)) {
			desiredCapabilities.setCapability(CapabilityType.BROWSER_NAME, browserName);
		}
		if (!StringUtils.isEmpty(browserVersion)) {
			desiredCapabilities.setCapability(CapabilityType.VERSION, browserVersion);
		}
		if (!StringUtils.isEmpty(plataform)) {
			desiredCapabilities.setCapability(CapabilityType.PLATFORM, plataform);
		}
		if (!StringUtils.isEmpty(buildTag)) {
			desiredCapabilities.setCapability("build", buildTag);
		}
		
		String testName = (String) parameters.get("testName");
		if (!StringUtils.isEmpty(testName)) {
			desiredCapabilities.setCapability("name", testName);
		}

		desiredCapabilities.setCapability("screenResolution", "1280x1024");

		desiredCapabilities.setCapability("autoAcceptAlerts", true);
		
		return desiredCapabilities;
	}

	public void before(WebDriver webDriver) {

		RemoteWebDriver remoteWebDriver = (RemoteWebDriver) webDriver;
		String sessionId = remoteWebDriver.getSessionId().toString();
		String testName = (String) remoteWebDriver.getCapabilities().getCapability("name");
		String message = String.format("SauceOnDemandSessionID=%1$s job-name=%2$s", sessionId, testName);
		logger.info(message);

	}

}
