
package br.com.ggas.selenium;

import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TesteImovel extends SeleniumTestCase {

	@Test
	public void testePadraoConstrucao() {
	
		testeInserirPadraoConstrucao();
		testeAlterarPadraoConstrucao();
		testeRemoverPadraoConstrucao();
	}

	private void testeRemoverPadraoConstrucao() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.linkText("TESTE")));
		assertEquals("Sucesso: Padrão de Construção removida(s) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	private void testeAlterarPadraoConstrucao() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Padrão de Construção')]"));
		waitForElement(By.xpath("(//input[@id='habilitado'])[2]")).click();
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		clicarEmElemento(isClickable(By.linkText("TESTE")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Padrão de Construção')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Padrão de Construção')]"));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Padrão de Construção alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	private void testeInserirPadraoConstrucao() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='148']/a/span[normalize-space(text()) = 'Padrão de construção']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
	 
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Padrão de Construção inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testePavimentoCalcada() {
	
		testeInserirPavimentoCalcada();
		testeAlterarPavimentoCalcada();
		testeRemoverPavimentoCalcada();
	}

	private void testeRemoverPavimentoCalcada() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Pavimento da Calçada removido(s) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarPavimentoCalcada() {
		clicarEmElemento(isClickable(By.linkText("PAVIMENTO TESTE")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Pavimento da Calçada')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Pavimento da Calçada')]"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("TES");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Pavimento da Calçada alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirPavimentoCalcada() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='146']/a/span[normalize-space(text()) = 'Pavimento da Calçada']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("incluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Pavimento da Calçada')]"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("PAVIMENTO TESTE");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("PAV");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Pavimento da Calçada inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	@Test
	public void testePavimentoRua() {
	
		testeInserirPavimentoRua();
		testeAlterarPavimentoRua();
		testeRemoverPavimentoRua();
	}

	private void testeRemoverPavimentoRua() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Pavimento de Rua removido(s) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarPavimentoRua() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Pavimento de Rua')]"));
		waitForElement(By.xpath("(//input[@id='habilitado'])[2]")).click();
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		clicarEmElemento(isClickable(By.linkText("TESTE2")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Pavimento de Rua')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Pavimento de Rua')]"));
		waitForElement(By.id("limparFormulario")).click();
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE3");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Pavimento de Rua alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	private void testeInserirPavimentoRua() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='147']/a/span[normalize-space(text()) = 'Pavimento da Rua']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("incluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Pavimento de Rua')]"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE2");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("TES");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Pavimento de Rua inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	@Test
	public void testePerfilImovel() {
	
		testeInserirPerfilImovel();
		testeAlterarPerfilImovel();
		testeRemoverPerfilImovel();
	}

	private void testeRemoverPerfilImovel() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(By.linkText("TESTE"));
		assertEquals("Sucesso: Perfil de Imóvel removida(s) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarPerfilImovel() {
		clicarEmElemento(isClickable(By.linkText("PERFIL TESTE")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Perfil de Imóvel')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Perfil de Imóvel')]"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Perfil de Imóvel alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirPerfilImovel() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='171']/a/span[normalize-space(text()) = 'Perfil do Imóvel']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("PERFIL TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Perfil de Imóvel inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeSegmento() {
	
		testeInserirSegmento();
		testeAlterarSegmento();
		testeRemoverSegmento();
	}

	private void testeRemoverSegmento() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Segmento(s) removida(s) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarSegmento() {
		clicarEmElemento(isClickable(By.linkText("SEGMENTO TESTE")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("TESTE");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("TES");
		
		new Select(waitForElement(By.id("idTipoSegmento"))).selectByVisibleText("PARTICULAR");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Segmento(s) alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirSegmento() {
		WebElement element = waitForElement(By.xpath("//li[@id='29']/a/span[normalize-space(text()) = 'Segmento']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Segmento')]"));
		obterElementoVisivel(By.id("ui-id-1"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("SEGMENTO TESTE");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("SEG");
		
		new Select(waitForElement(By.id("idTipoSegmento"))).selectByVisibleText("PUBLICO");
		waitForElement(By.name("numeroCiclos")).clear();
		waitForElement(By.name("numeroCiclos")).sendKeys("12");
		
		new Select(waitForElement(By.id("periodicidade"))).selectByVisibleText("MENSAL COLETOR");
		waitForElement(By.name("numeroDiasFaturamento")).clear();
		waitForElement(By.name("numeroDiasFaturamento")).sendKeys("15");
		
		new Select(waitForElement(By.id("tipoConsumoFaturamento"))).selectByVisibleText("Tarifa Média");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Segmento(s) inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeTipoSegmento() {
	
		testeInserirTipoSegmento();
		testeAlterarTipoSegmento();
		testeRemoverTipoSegmento();
	}

	private void testeRemoverTipoSegmento() {
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Tipo de Segmento removido(s) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarTipoSegmento() {
		clicarEmElemento(isClickable(By.linkText("TESTE")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Tipo de Segmento')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Tipo de Segmento')]"));
		waitForElement(By.xpath("(//input[@id='habilitado'])[2]")).click();
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Tipo de Segmento alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirTipoSegmento() {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='140']/a/span[normalize-space(text()) = 'Tipo de Segmento']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("incluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Tipo de Segmento')]"));
		waitForElement(By.id("descricao")).clear();
		waitForElement(By.id("descricao")).sendKeys("Teste");
		waitForElement(By.id("descricaoAbreviada")).clear();
		waitForElement(By.id("descricaoAbreviada")).sendKeys("Teste");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Tipo de Segmento inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeImovel() throws IOException {
	
		testeInserirImovel();
		testeAlterarImovel();
		testeRemoverImovel();
	}

	private void testeRemoverImovel() {
		clicarEmElemento(isClickable(By.name("chavesPrimarias")));
		clicarEmElemento(isClickable(scroll(By.id("botaoRemover"))));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.name("chavesPrimarias")));
		assertEquals("Sucesso: Imóvel(s) removido(s) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarImovel() {
		clicarEmElemento(isClickable (By.linkText("TESTE IMOVEL")) );
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Imóvel')]"));
		clicarEmElemento(isClickable(By.id("botaoAlterar")) );
		obterElementoVisivel(By.id("ui-id-1"));
		waitForElement(By.id("complementoImovel")).clear();
		waitForElement(By.id("complementoImovel")).sendKeys("RUA");
		clicarEmElemento(isClickable(By.id("botaoSalvar")) );
		
		assertEquals("Sucesso: Imóvel alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirImovel() {
		WebElement element = waitForElement(By.xpath("//li[@id='28']/a/span[normalize-space(text()) = 'Imóvel']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(waitForElement(By.id("botaoIncluir")));
		obterElementoVisivel(By.id("ui-id-1"));
		waitForElement(By.id("nome")).sendKeys("");
		waitForElement(By.id("nome")).clear();
		waitForElement(By.id("nome")).sendKeys("TESTE IMOVEL");
		waitForElement(By.id("numeroImovel")).sendKeys("");
		waitForElement(By.id("numeroImovel")).clear();
		waitForElement(By.id("numeroImovel")).sendKeys("100");
		waitForElement(By.id("complementoImovel")).clear();
		waitForElement(By.id("complementoImovel")).sendKeys("TESTE");
		waitForElement(By.id("enderecoReferencia")).clear();
		waitForElement(By.id("enderecoReferencia")).sendKeys("TESTE");
		preencherCampo("cepImovel", "57608-010");
		obterElementoVisivel(By.id("cepImovel")).sendKeys(Keys.TAB);
		
		waitForElement(By.id("ImovelAbaIdLoc2"));
		waitForElement(By.id("idQuadraImovel"));
		
		espera();
		
		new Select(isClickable(waitForElement(By.id("idQuadraImovel")))).selectByVisibleText("123");
		new Select(isClickable(waitForElement(By.id("idQuadraFace")))).selectByVisibleText("123");
		new Select(isClickable(waitForElement(By.id("idSituacao")))).selectByVisibleText("POTENCIAL");
		clicarEmElemento(isClickable(By.id("botaoIncluir")) );
		
		assertEquals("Sucesso: Imóvel inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Ignore
	@Test
	public void testePrevisaoDeCaptacao() throws Exception {
		WebElement element = webDriver
		.findElement(By.xpath("//li[@id='238']/a/span[normalize-space(text()) = 'Previsão de Captação']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
	
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		
		new Select(waitForElement(By.id("segmentoPrevisao"))).selectByVisibleText("COMERCIAL");
		
		isClickable(By.id("previsaoJaneiro"));
		waitForElement(By.id("previsaoJaneiro")).clear();
		waitForElement(By.id("previsaoJaneiro")).sendKeys("5");
		waitForElement(By.id("previsaoFevereiro")).clear();
		waitForElement(By.id("previsaoFevereiro")).sendKeys("10");
		waitForElement(By.id("previsaoMarco")).clear();
		waitForElement(By.id("previsaoMarco")).sendKeys("15");
		waitForElement(By.id("previsaoAbril")).clear();
		waitForElement(By.id("previsaoAbril")).sendKeys("20");
		waitForElement(By.id("previsaoMaio")).clear();
		waitForElement(By.id("previsaoMaio")).sendKeys("25");
		waitForElement(By.id("previsaoJunho")).clear();
		waitForElement(By.id("previsaoJunho")).sendKeys("5");
		waitForElement(By.id("previsaoJulho")).clear();
		waitForElement(By.id("previsaoJulho")).sendKeys("10");
		waitForElement(By.id("previsaoAgosto")).clear();
		waitForElement(By.id("previsaoAgosto")).sendKeys("15");
		waitForElement(By.id("previsaoSetembro")).clear();
		waitForElement(By.id("previsaoSetembro")).sendKeys("20");
		waitForElement(By.id("previsaoOutubro")).clear();
		waitForElement(By.id("previsaoOutubro")).sendKeys("25");
		waitForElement(By.id("previsaoNovembro")).clear();
		waitForElement(By.id("previsaoNovembro")).sendKeys("5");
		waitForElement(By.id("previsaoDezembro")).clear();
		waitForElement(By.id("previsaoDezembro")).sendKeys("10");
		
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Previsão(ões) inserido(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		new Select(waitForElement(By.id("segmentoPrevisao"))).selectByVisibleText("INDUSTRIAL");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Previsão(ões) alterado(a) com sucesso.",
				waitForElement(By.className("notification")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

}
