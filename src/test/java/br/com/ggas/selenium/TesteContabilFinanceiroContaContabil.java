package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class TesteContabilFinanceiroContaContabil extends SeleniumTestCase {

	
	@Test
	public void testContaContabil() throws Exception {
		incluirContaContabil();
		alterarContaContabil();
		removerContaContabil();
	}

	public void incluirContaContabil() {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='184']/a/span[normalize-space(text()) = 'Conta Contábil']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		//waitForElementNotPresent(buscarElemento(obterElementoVisivel(By.id("botaoPesquisar")));
		waitForElement(By.id("botaoSalvar"));
		webDriver.findElement(By.id("numeroConta")).clear();
		preencherCampo("numeroConta", "1.2.3.4.56.788");
		webDriver.findElement(By.id("nomeConta")).clear();
		webDriver.findElement(By.id("nomeConta")).sendKeys("CONTA TESTE");
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		
		assertEquals("Sucesso: Conta Contábil inserida com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void alterarContaContabil() {
		isClickable(By.linkText("CONTA TESTE")).click();
		clicarEmElemento(isClickable(By.id("botaoAlterar")));		
		waitForElement(By.xpath("//input[@id='habilitado' and @value='false']")).click();
		clicarEmElemento(isClickable(By.id("botaoSalvar")));
		assertEquals("Sucesso: Conta Contábil alterada com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void removerContaContabil() {
		WebElement chaves = waitForElement(By.name("chavesPrimarias"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", chaves);

		WebElement remover = waitForElement(By.id("botaoRemover"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", remover);

		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.name("chavesPrimarias")));
		assertEquals("Sucesso: Conta Contábil removida(s) com sucesso.",
				waitForElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));

	}
}
