package br.com.ggas.selenium;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TesteArrecadacao extends SeleniumTestCase {

	@Test
	public void testeBanco() throws Exception {
		WebElement element = waitForElement(By.xpath("//span[contains(text(),'Banco')]"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		element = isClickable(By.xpath("//input[@value='Incluir']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Banco')]"));

		String path = super.getPathImagem("ggas_marca_login.jpg");

		obterElementoVisivel(By.id("nome"));
		obterElementoVisivel(By.id("codigoBanco"));
		obterElementoVisivel(By.id("nomeAbreviado"));
		obterElementoVisivel(By.id("buttonIncluir"));
		isClickable(By.id("nome")).clear();
		isClickable(By.id("nome")).sendKeys("BANCO PARA TESTE");
		isClickable(By.id("codigoBanco")).clear();
		isClickable(By.id("codigoBanco")).sendKeys("341");
		isClickable(By.id("nomeAbreviado")).clear();
		isClickable(By.id("nomeAbreviado")).sendKeys("BPT");
		JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        jse.executeScript("document.getElementById('logoBanco').setAttribute('value', '" + path + "');");
		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		assertEquals("Sucesso: Banco inserido(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.name("buttonAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Banco')]"));
		isClickable(By.id("nome")).clear();
		isClickable(By.id("nome")).sendKeys("BANCO TESTE 2");
        jse.executeScript("document.getElementById('logoBanco').setAttribute('value', '" + path + "');");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Banco alterado(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));

		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.name("buttonRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		isClickable(By.xpath("//div[@id='inner']/div/div/p"));
		waitForElement(By.xpath("//form[@id='bancoForm']/div[text()='Não foram encontrados.']"));
		assertEquals("Sucesso: Banco(s) removido(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
	
	@Test
	public void testeArrecadador() throws Exception {
		// obtém o dados da janela principal
		String janelaInicio = webDriver.getWindowHandle();

		//WebElement element = waitForElement(By.xpath("//a[contains(text(),'Arrecadador')]"));
		//((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		WebElement element = waitForElement(By.xpath("id('205')/a/span"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		//element = waitForElement(By.xpath("//input[@value='Incluir']"));
		//clicarEmElemento(isClickable(By.id("botaoIncluir")));

		//inserirArrecadador
		
		//waitForElement(By.id("botaoIncluir")).click();
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Arrecadador')]"));
		
		//waitForElement(By.id("botaoPesquisarCliente")).click();
		clicarEmElemento(isClickable(By.id("botaoPesquisarCliente")));

		// muda o foco para a janela pesquisar
		for (String winHandle : webDriver.getWindowHandles()) {
			//webDriver.switchTo().window(winHandle);
			if( !winHandle.equals(janelaInicio) ) {
				webDriver.switchTo().window(winHandle);
		    }
		}

		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("ALGAS - GAS DE ALAGOAS SA")));

		// volta o foco para a janela principal
		webDriver.switchTo().window(janelaInicio);

		isClickable(By.cssSelector("p.orientacaoInicial")).click();
		new Select(isClickable(By.id("banco"))).selectByVisibleText("BANCO DO BRASIL");
		isClickable(By.id("codigoAgente")).clear();
		isClickable(By.id("codigoAgente")).sendKeys("002");
		clicarEmElemento(isClickable(By.id("ButtonIncluir")));
		
		assertEquals("Sucesso: Arrecadador inserido(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		
		//alterarArrecadador
		
		//waitForElement(By.name("chavesPrimarias")).click();
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		//waitForElement(By.id("buttonAlterar")).click();
		clicarEmElemento(isClickable(By.id("buttonAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Arrecadador')]"));
		new Select(isClickable(By.id("banco"))).selectByVisibleText("CAIXA ECONOMICA FEDERAL");
		isClickable(By.id("codigoAgente")).clear();
		isClickable(By.id("codigoAgente")).sendKeys("002");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		
		assertEquals("Sucesso: Arrecadador alterado(a) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
		
		//removerArrecadador
		
		//waitForElement(By.name("chavesPrimarias")).click();
		clicarEmElemento(isClickable(By.id("checkboxChavesPrimarias")));
		//waitForElement(By.id("buttonRemover")).click();
		clicarEmElemento(isClickable(By.id("buttonRemover")));
		
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.id("checkboxChavesPrimarias")));
		assertEquals("Sucesso: Arrecadador(es) removido(s) com sucesso.",
				isClickable(By.className("notification")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeContratoArrecadador() {

		testeInserirContratoArrecadador();
		testeAlterarContratoArrecadador();
		testeRemoverContratoArrecadador();

	}

	private void testeRemoverContratoArrecadador() {
		isClickable(By.name("numeroContrato")).sendKeys("9998");
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.id("botaoRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		isClickable(By.xpath("//div[@id='inner']/div/div/p"));
		assertEquals("Sucesso: Contrato Arecadador removido(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarContratoArrecadador() {
		clicarEmElemento(isClickable(By.linkText("999")));
		clicarEmElemento(isClickable(By.id("buttonAlterar")));
		isClickable(By.id("numeroContrato")).clear();
		isClickable(By.id("numeroContrato")).sendKeys("9998");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Contrato Arecadador alterado(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirContratoArrecadador() {
		WebElement element = waitForElement(By.xpath("id('250')/a/span"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		isClickable(By.cssSelector("fieldset.conteinerBotoesPesquisarDir > #botaoPesquisar"));
		clicarEmElemento(isClickable(By.cssSelector("fieldset.conteinerBotoesPesquisarDir > #botaoPesquisar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Contrato Arrecadador')]"));
		isClickable(By.id("numeroContrato")).clear();
		isClickable(By.id("numeroContrato")).sendKeys("999");
		preencherCampo("dataInicial", "01/04/2008");
		preencherCampo("dataFinal", "01/08/2016");
		new Select(isClickable(By.id("chaveArrecadador")))
				.selectByVisibleText("104 - CAIXA ECONOMICA FEDERAL");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Contrato Arecadador inserido(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeConvenioArrecadador() {

		testeInserirConvenioArrecadador();
		testeAlterarConvenioArrecadador();
		testeRemoverConvenioArrecadador();

	}

	private void testeRemoverConvenioArrecadador() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.name("buttonRemover")));
		assertTrue(obterTextoDoAlerta().matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Contrato Convênio Arrecadador removido(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarConvenioArrecadador() {
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Contrato Convênio Arrecadador')]"));
		clicarEmElemento(isClickable(By.xpath("//input[@id='habilitado' and @value='false']")));
		clicarEmElemento(isClickable(By.id("botaoAlterar")));
		assertEquals("Sucesso: Contrato Convênio Arrecadador alterado(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirConvenioArrecadador() {
		WebElement element = waitForElement(By.xpath("id('206')/a/span"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		clicarEmElemento(isClickable(By.name("buttonIncluir")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Incluir Contrato Convênio Arrecadador')]"));
		new Select(isClickable(By.id("arrecadadorContrato"))).selectByVisibleText("003");
		isClickable(By.id("codigoConvenio")).clear();
		isClickable(By.id("codigoConvenio")).sendKeys("1004562");
		new Select(isClickable(By.id("tipoConvenio"))).selectByVisibleText("Arrecadação");
		new Select(isClickable(By.id("leiaute"))).selectByVisibleText("DACNAB150BancoBrasil - Arrecadação");
		
		clicarEmElemento(isClickable(By.id("botaoIncluir")));
		assertEquals("Sucesso: Contrato Convênio Arrecadador inserido(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeCarteiraCobranca() {

		testeInserirCarteiraCobranca();
		testeAlterarCarteiraCobranca();
		testeRemoverCarteiraCobranca();
	}

	private void testeRemoverCarteiraCobranca() {
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		clicarEmElemento(isClickable(By.id("buttonRemover")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Carteira Cobrança removida(s) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarCarteiraCobranca() {
		clicarEmElemento(isClickable(By.id("botaoPesquisar")));
		clicarEmElemento(isClickable(By.linkText("CARTEIRA TESTE")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Detalhar Carteira de Cobrança')]"));
		clicarEmElemento(isClickable(By.id("buttonAlterar")));
		obterElementoVisivel(By.xpath("//h1[contains(text(), 'Alterar Carteira de Cobrança')]"));
		new Select(isClickable(By.id("chaveCodigoCarteira"))).selectByVisibleText("I");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Carteira Cobrança alterado(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirCarteiraCobranca() {
		WebElement element = waitForElement(By.xpath("id('231')/a/span"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		isClickable(By.id("descricao")).sendKeys("CARTEIRA TESTE");
		isClickable(By.id("numero")).sendKeys("198");
		new Select(isClickable(By.id("chaveArrecadador")))
				.selectByVisibleText("001 - BANCO DO BRASIL");
		new Select(isClickable(By.id("chaveCodigoCarteira"))).selectByVisibleText("E");
		new Select(isClickable(By.id("chaveTipoCarteira"))).selectByVisibleText("DIRETA");
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Carteira Cobrança inserido(a) com sucesso.",
				isClickable(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	@Test
	public void testeAgenciaBancaria() {

		testeInserirAgenciaBancaria();
		testeAlterarAgenciaBancaria();
		testeRemoverAgenciaBancaria();
	}

	private void testeRemoverAgenciaBancaria() {
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Pesquisar Agência Bancária')]"));
		clicarEmElemento(isClickable(By.xpath("/html/body/div[1]/div[2]/div/div[2]/form/div/div[2]/div[2]/div[2]/div/div/div[2]/label")));
		obterElementoVisivel(By.linkText("AGENCIA TESTE"));
		clicarEmElemento(isClickable(By.xpath("//a[contains(text(),'AGENCIA TESTE')]/../../td[1]/input")));
		clicarEmElemento(isClickable(By.xpath("//*[@id=\"buttonRemover\"]")));
		assertTrue(obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(By.linkText("AGENCIA TESTE"));
		assertEquals("Sucesso: Agência Bancária removida(s) com sucesso.",
				isClickable(By.xpath("/html/body/div[1]/div[2]/div/div[1]/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeAlterarAgenciaBancaria() {
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Pesquisar Agência Bancária')]"));
		clicarEmElemento(obterElementoVisivel(By.linkText("AGENCIA TESTE")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Detalhar Agência Bancária')]"));
		clicarEmElemento(isClickable(By.xpath("//*[@id=\"buttonAlterar\"]")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Alterar Agência Bancária')]"));
		clicarEmElemento(isClickable(By.xpath("/html/body/div[1]/div[2]/div/div[2]/form/div/div[2]/div[2]/div[2]/div[4]/div/div[2]/label")));
		clicarEmElemento(obterElementoVisivel(By.xpath("/html/body/div[1]/div[2]/div/div[2]/form/div/div[3]/div[2]/div/div/div/table/tbody/tr/td[5]/a/i")));
		//clicarEmElemento(obterElementoVisivel(By.cssSelector("img[alt=\"Excluir Conta Bancária\"]")));
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Agência Bancária alterado(a) com sucesso.",
				isClickable(By.xpath("/html/body/div[1]/div[2]/div/div[1]/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}

	private void testeInserirAgenciaBancaria() {
		WebElement element = waitForElement(By.xpath("id('230')/a/span"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		clicarEmElemento(isClickable(By.id("buttonIncluir")));
		obterElementoVisivel(By.xpath("//h5[contains(text(), 'Incluir Agência Bancária')]"));
		new Select(isClickable(By.id("chaveBanco"))).selectByVisibleText("BANCO DO BRASIL");
		isClickable(By.id("codigo")).sendKeys("198");
		isClickable(By.id("nome")).sendKeys("AGENCIA TESTE");
		isClickable(By.id("descricao")).sendKeys("CONTA TESTE");
		isClickable(By.id("numeroConta")).sendKeys("100456283");
		isClickable(By.id("numeroDigito")).sendKeys("12");
		clicarEmElemento(isClickable(By.id("botaoIncluirContaBancaria")));
		clicarEmElemento(isClickable(By.id("buttonSalvar")));
		assertEquals("Sucesso: Agência Bancária inserido(a) com sucesso.",
				isClickable(By.xpath("/html/body/div[1]/div[2]/div/div[1]/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", ""));
	}
}
