package br.com.ggas.selenium;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteLeituraDadosMedicao extends SeleniumTestCase {

	@Test
	public void testPesquisarDadosMedicao() throws Exception {
		WebElement element = webDriver
				.findElement(By.xpath("//li[@id='237']/a/span[normalize-space(text()) = 'Dados de Medição']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);
		
		new Select(webDriver.findElement(By.id("chaveGrupoFaturamento"))).selectByVisibleText("GRUPO ORUBE");
		
		webDriver.findElement(By.id("mesAnoReferencia")).sendKeys("02/2016");
		new Select(webDriver.findElement(By.id("ciclo"))).selectByVisibleText("1");
		
		webDriver.findElement(By.id("botaoPesquisar")).click();
		webDriver.findElement(By.linkText("02/2016-1")).click();
		webDriver.findElement(By.id("pesquisaChamado")).click();
		
		String formLeituraMovimento = super.getTexto(By.id("mostrarFormLeituraMovimento"));
		
		assertTrue(formLeituraMovimento.contains("201602-1"));
		assertTrue(formLeituraMovimento.contains("GRUPO ORUBE"));
		assertTrue(formLeituraMovimento.contains("RESIDENCIAL - ROTA 14"));
		assertTrue(formLeituraMovimento.contains("ALGAS - GAS DE ALAGOAS SA"));
		assertTrue(formLeituraMovimento.contains("RESIDENCIAL - ROTA 14"));
	}
}