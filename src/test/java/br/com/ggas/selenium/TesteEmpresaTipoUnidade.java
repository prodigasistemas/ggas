package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteEmpresaTipoUnidade extends SeleniumTestCase {

	@Test
	public void testTipoUnidade() throws Exception {
		registraUmTipoUnidade();
		alteraUmTipoUnidade();
		removeUmTipoUnidade();
	}

	public void registraUmTipoUnidade() {

		WebElement element = webDriver.findElement(By.xpath("//li[@id='133']/a/span[normalize-space(text()) = 'Tipo de Unidade']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		webDriver.findElement(By.name("button")).click();
		webDriver.findElement(By.id("descricao")).clear();
		webDriver.findElement(By.id("descricao")).sendKeys("NADA");
		webDriver.findElement(By.id("descricaoAbreviada")).clear();
		webDriver.findElement(By.id("descricaoAbreviada")).sendKeys("ND");
		webDriver.findElement(By.id("nivel")).clear();
		webDriver.findElement(By.id("nivel")).sendKeys("1");
		new Select(webDriver.findElement(By.id("tipo"))).selectByVisibleText("C");
		webDriver.findElement(By.id("botaoSalvar")).click();

		assertEquals("Sucesso: Tipo de Unidade inserido(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void alteraUmTipoUnidade() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.cssSelector("input[value='Alterar']")).click();
		webDriver.findElement(By.name("button")).click();
		assertEquals("Sucesso: Tipo de Unidade alterado(a) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}

	public void removeUmTipoUnidade() {
		webDriver.findElement(By.cssSelector("td > input[name=\"chavesPrimarias\"]")).click();
		webDriver.findElement(By.cssSelector("input[value='Remover']")).click();
		assertTrue(super.obterTextoDoAlerta()
				.matches("^Deseja remover os itens selecionados[\\s\\S]\nEsta Operação não poderá ser desfeita\\.$"));
		waitForElementNotPresent(buscarElemento(By.cssSelector("td > input[name=\"chavesPrimarias\"]")));
		assertEquals("Sucesso: Tipo de Unidade removida(s) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div")).getAttribute("textContent").replaceAll("[\\n\\r\\t]", ""));
	}
}
