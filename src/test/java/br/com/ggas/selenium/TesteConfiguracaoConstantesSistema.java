package br.com.ggas.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TesteConfiguracaoConstantesSistema extends SeleniumTestCase {

	@Test
	public void testeConstantesSistema() throws Exception {
		WebElement element = webDriver.findElement(By.xpath("//li[@id='186']/a/span[normalize-space(text()) = 'Constantes do Sistema']"));
		((JavascriptExecutor) webDriver).executeScript("arguments[0].click()", element);

		new Select(webDriver.findElement(By.id("tabela"))).selectByVisibleText("Tributo");
		webDriver.findElement(By.xpath("//input[@value='Ok']")).click();

		new Select(webDriver.findElement(By.xpath("//select[@name='valorConstante' and @tabindex='3']"))).selectByVisibleText("ISS");
		new Select(webDriver.findElement(By.xpath("//select[@name='valorConstante' and @tabindex='4']"))).selectByVisibleText("ISS");
		new Select(webDriver.findElement(By.xpath("//select[@name='valorConstante' and @tabindex='5']"))).selectByVisibleText("ISS");
		webDriver.findElement(By.xpath("//input[@value='Salvar']")).click();

		assertTrue(super.obterTextoDoAlerta().matches("^Confirma as alterações[\\s\\S]$"));
		assertEquals("Sucesso: Constante(s) do sistema alterada(s) com sucesso.",
				webDriver.findElement(By.xpath("//div[@id='inner']/div/div/p")).getAttribute("textContent")
						.replaceAll("[\\n\\r\\t]", "").trim());
	}

}
