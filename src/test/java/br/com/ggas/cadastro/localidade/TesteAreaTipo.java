
package br.com.ggas.cadastro.localidade;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.localidade.impl.AreaTipoImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;

/**
 * @author Valter Negreiros
 * @author Erick Simões
 */
public class TesteAreaTipo extends GGASTestCase {

	@Before
	public void setUp() {

		cargaInicial.carregar();
	}

	/**
	 * Inserir area tipo test.
	 */
	@Test
	@Transactional
	public void testeInserirAreaTipo() throws GGASException {

		int qtdInicio = fachada.consultarAreaTipo(null).size();
		AreaTipo areaTipo = montarAreaTipo();
		super.salvar(areaTipo);
		int qtdFim = fachada.consultarAreaTipo(null).size();
		Assert.assertEquals(qtdInicio + 1, qtdFim);
	}

	@Test
	@Transactional
	public void testeRemoverAreaTipo() throws GGASException {

		AreaTipo areaTipoInserida = (montarAreaTipo());
		super.salvar(areaTipoInserida);

		int qtdInicio = fachada.consultarAreaTipo(null).size();
		getSesstion().delete(areaTipoInserida);
		int qtdFim = fachada.consultarAreaTipo(null).size();
		Assert.assertEquals(qtdInicio - 1, qtdFim);
	}

	@Test
	@Transactional
	public void testeAtualizarAreaTipo() throws GGASException {

		AreaTipo areaTipoInserida = (AreaTipo) buscar(AreaTipoImpl.class);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chavePrimaria", areaTipoInserida.getChavePrimaria());

		String descricaoAntesAlteracao = areaTipoInserida.getDescricao();
		areaTipoInserida.setDescricao("TESTE");
		getSesstion().update(areaTipoInserida);

		Collection<AreaTipo> lista = fachada.consultarAreaTipo(filtro);
		Assert.assertTrue(lista.size() == 1);

		String descricaoPosAlteracao = ((AreaTipo) lista.iterator().next()).getDescricao();
		Assert.assertNotNull(descricaoPosAlteracao);
		Assert.assertNotEquals(descricaoAntesAlteracao, descricaoPosAlteracao);
	}

	@Test
	@Transactional
	public final void testeConsultarAreaTipo() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();

		AreaTipo areaTipoInserida = (AreaTipo) buscar(AreaTipoImpl.class);
		super.salvar(montarAreaTipo());

		filtro.put("descricao", areaTipoInserida.getDescricao());
		filtro.put("descricaoAbrevida", areaTipoInserida.getDescricaoAbreviada());
		filtro.put("chavePrimaria", areaTipoInserida.getChavePrimaria());
		Collection<AreaTipo> listaAreaTipo = fachada.consultarAreaTipo(filtro);

		AreaTipo areaTipoConsultada = (AreaTipo) listaAreaTipo.iterator().next();
		Assert.assertNotNull(areaTipoConsultada);
		Assert.assertEquals(areaTipoConsultada.getChavePrimaria(), areaTipoInserida.getChavePrimaria());
		Assert.assertEquals(areaTipoConsultada.getDescricao(), areaTipoInserida.getDescricao());
		Assert.assertEquals(areaTipoConsultada.getDescricaoAbreviada(), areaTipoInserida.getDescricaoAbreviada());
	}

	@Test
	@Transactional
	public final void testeConsultarAreaTipoPorChave() throws GGASException {

		AreaTipo areaTipoInserida = montarAreaTipo();
		super.salvar(areaTipoInserida);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chavePrimaria", areaTipoInserida.getChavePrimaria());
		AreaTipo areaTipoConsultada = (AreaTipo) fachada.consultarAreaTipo(filtro).iterator().next();

		Assert.assertNotNull(areaTipoConsultada);
		Assert.assertEquals(areaTipoConsultada.getChavePrimaria(), areaTipoInserida.getChavePrimaria());
		Assert.assertEquals(areaTipoConsultada.getDescricao(), areaTipoInserida.getDescricao());
		Assert.assertEquals(areaTipoConsultada.getDescricaoAbreviada(), areaTipoInserida.getDescricaoAbreviada());
	}

	private AreaTipo montarAreaTipo() {

		AreaTipo areaTipo = new AreaTipoImpl();
		areaTipo.setDescricao("AREA");
		areaTipo.setDescricaoAbreviada("AR");
		areaTipo.setUltimaAlteracao(new Date());
		areaTipo.setHabilitado(true);
		areaTipo.setVersao(1);

		return areaTipo;
	}

}
