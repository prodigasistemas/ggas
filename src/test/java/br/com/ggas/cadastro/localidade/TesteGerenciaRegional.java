
package br.com.ggas.cadastro.localidade;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;

/**
 * @author Erick Simões
 */
public class TesteGerenciaRegional extends GGASTestCase {

	/**
	 * Sets the up.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Before
	public void setUp() throws GGASException {

		cargaInicial.carregar();
	}

	/**
	 * Teste buscar gerencia regional por chave.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Test
	@Transactional
	public void testeBuscarGerenciaRegionalPorChave() throws GGASException {

		GerenciaRegional gerenciaRegional = montarObjetoGerenciaRegional();
		long chave = fachada.inserirGerenciaRegional(gerenciaRegional);

		GerenciaRegional gerenciaRegional2 = fachada.buscarGerenciaRegionalPorChave(chave);
		Assert.assertEquals(chave, gerenciaRegional2.getChavePrimaria());
	}

	/**
	 * Teste consultar gerencia regional.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Test
	@Transactional
	public void testeConsultarGerenciaRegional() throws GGASException {

		GerenciaRegional gerenciaRegional = montarObjetoGerenciaRegional();
		fachada.inserirGerenciaRegional(gerenciaRegional);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("nome", "Gerência Capital");
		filtro.put("nomeAbreviado", "CAPIT");
		filtro.put("habilitado", true);
		GerenciaRegional gerenciaRegionalConsultada = fachada.consultarGerenciaRegional(filtro).iterator().next();

		Assert.assertNotNull(gerenciaRegionalConsultada);
		Assert.assertEquals(gerenciaRegional.getChavePrimaria(), gerenciaRegionalConsultada.getChavePrimaria());
	}

	/**
	 * Teste inserir gerencia regional.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Test
	@Transactional
	public void testeInserirGerenciaRegional() throws GGASException {

		int qntInicio = fachada.consultarGerenciaRegional(null).size();

		GerenciaRegional gerenciaRegional = montarObjetoGerenciaRegional();
		fachada.inserirGerenciaRegional(gerenciaRegional);

		int qntFim = fachada.consultarGerenciaRegional(null).size();
		Assert.assertEquals(qntInicio + 1, qntFim);
	}

	/**
	 * Teste atualizar gerencia regional.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Test
	@Transactional
	public void testeAtualizarGerenciaRegional() throws GGASException {

		GerenciaRegional gerenciaRegional = montarObjetoGerenciaRegional();
		long chave = fachada.inserirGerenciaRegional(gerenciaRegional);
		String nomeAntes = gerenciaRegional.getNome();

		gerenciaRegional.setNome("Teste");
		fachada.atualizarGerenciaRegional(gerenciaRegional);
		String nomeAtualizado = ((GerenciaRegional) fachada.buscarGerenciaRegionalPorChave(chave)).getNome();

		Assert.assertNotNull(nomeAtualizado);
		Assert.assertNotEquals(nomeAntes, nomeAtualizado);
	}

	/**
	 * Teste remover gerencia regional.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Test
	@Transactional
	public void testeRemoverGerenciaRegional() throws GGASException {

		GerenciaRegional gerenciaRegional = montarObjetoGerenciaRegional();
		long chave = fachada.inserirGerenciaRegional(gerenciaRegional);

		fachada.removerGerenciaRegional(new Long[] {gerenciaRegional.getChavePrimaria()}, null);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chavePrimaria", chave);

		Assert.assertEquals(0, fachada.consultarGerenciaRegional(filtro).size());
	}

	/**
	 * Montar objeto endereco.
	 * 
	 * @return the endereco
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Endereco montarObjetoEndereco() throws GGASException {

		Cep cep = fachada.criarCep();
		cep.setCep("51000000");
		super.salvar(cep);

		Endereco endereco;
		endereco = fachada.criarEndereco();
		endereco.setCep(cep);
		endereco.setEnderecoReferencia("Perto de algum lugar");
		return endereco;
	}

	/**
	 * Montar objeto gerencia regional.
	 * 
	 * @return the gerencia regional
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private GerenciaRegional montarObjetoGerenciaRegional() throws GGASException {

		GerenciaRegional gerenciaRegional = fachada.criarGerenciaRegional();
		gerenciaRegional.setNome("Gerência Capital");
		gerenciaRegional.setNomeAbreviado("CAPIT");
		gerenciaRegional.setFone("34389027");
		gerenciaRegional.setRamalFone("13");
		gerenciaRegional.setFax("34387898");
		gerenciaRegional.setEmail("capitalgas@algas.com.br");
		gerenciaRegional.setEndereco(montarObjetoEndereco());
		gerenciaRegional.setHabilitado(true);

		return gerenciaRegional;
	}
}
