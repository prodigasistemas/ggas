
package br.com.ggas.cadastro.localidade;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.localidade.impl.GerenciaRegionalImpl;
import br.com.ggas.cadastro.localidade.impl.LocalidadePorteImpl;
import br.com.ggas.cadastro.localidade.impl.UnidadeNegocioImpl;
import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.cadastro.operacional.impl.MedidorLocalArmazenagemImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.medicao.vazaocorretor.impl.ClasseUnidadeImpl;
import br.com.ggas.util.DadosAuditoriaUtil;

/**
 * @author Valter Negreiros
 */
public class TesteLocalidade extends GGASTestCase {

	@Autowired
	@Qualifier(ControladorUnidadeNegocio.BEAN_ID_CONTROLADOR_UNIDADE_NEGOCIO)
	ControladorUnidadeNegocio controlador;

	@Autowired
	@Qualifier(ControladorLocalidade.BEAN_ID_CONTROLADOR_LOCALIDADE)
	ControladorLocalidade controladorLocalidade;

	@Before
	@Transactional
	public void setUp() throws GGASException {

		cargaInicial.carregar();
		this.inserirObjetoLocalidadeClasse();
		this.inserirObjetoGerenciaRegional();
		this.inserirObjetoLocalidadePorte();
		this.inserirObjetoMedidorLocalArmazenagem();
		this.inserirObjetoUnidadeNegocio();
		this.inserirObjetoClasseUnidade();
	}

	@Test
	@Transactional
	public void testeBuscarLocalidadePorChave() throws GGASException {

		Localidade localidade = montarObjetoLocalidade();
		long chave = fachada.inserirLocalidade(localidade);
		Localidade localidade2 = fachada.buscarLocalidadeChave(chave);
		assertEquals(chave, localidade2.getChavePrimaria());
	}

	@Test
	@Transactional
	public void testeInserirLocalidade() throws GGASException {

		int qntInicio = fachada.consultarLocalidades(null).size();

		fachada.inserirLocalidade(montarObjetoLocalidade());

		int qntFim = fachada.consultarLocalidades(null).size();
		assertEquals((qntInicio + 1), qntFim);
	}
	
	@Test
	@Transactional
	public void testeConsultarLocalidade() throws GGASException {

		Localidade localidade = montarObjetoLocalidade();
		fachada.inserirLocalidade(localidade);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("fone", "3456.6543");
		filtro.put("descricao", "Descrição");
		filtro.put("ramal", "13");
		filtro.put("codigocentrocusto", "123");

		GerenciaRegional gerenciaRegional = (GerenciaRegional) buscar(GerenciaRegionalImpl.class);
		filtro.put("gerenciaregional", gerenciaRegional.getChavePrimaria());

		filtro.put("habilitado", true);
		
		Localidade localidadeConsultada = fachada.consultarLocalidades(filtro).iterator().next();

		Assert.assertNotNull(localidadeConsultada);
		Assert.assertEquals(localidade.getChavePrimaria(), localidadeConsultada.getChavePrimaria());
	}
	
	@Test
	@Transactional
	public void testeAtualizarLocalidade() throws GGASException {

		Localidade localidadeInserida = montarObjetoLocalidade();
		fachada.inserirLocalidade(localidadeInserida);

		String descricaoAntesAlteracao = localidadeInserida.getDescricao();
		localidadeInserida.setDescricao("Teste");
		fachada.atualizarLocalidade(localidadeInserida);

		String descricaoPosAlteracao = ((Localidade) controladorLocalidade.obter(localidadeInserida.getChavePrimaria())).getDescricao();
		
		Assert.assertNotNull(descricaoPosAlteracao);
		Assert.assertNotEquals(descricaoAntesAlteracao, descricaoPosAlteracao);
	}
	
	
	@Test
	@Transactional
	public void testeRemoverLocalidade() throws GGASException {

		Localidade localidade = montarObjetoLocalidade();
		fachada.inserirLocalidade(localidade);
		int qtdInicio = fachada.consultarLocalidades(null).size();

		controladorLocalidade.remover(localidade);
		int qtdFim = fachada.consultarLocalidades(null).size();

		Assert.assertEquals(qtdInicio - 1, qtdFim);
	}
	
	private Localidade montarObjetoLocalidade() throws GGASException {

		Localidade localidade = fachada.criarLocalidade();
		localidade.setChavePrimaria(1L);
		localidade.setDescricao("Descrição");
		localidade.setGerenciaRegional((GerenciaRegional) buscar(GerenciaRegionalImpl.class));
		localidade.setEndereco(montarObjetoEndereco());
		localidade.setFone("3456.6543");
		localidade.setRamalFone("13");
		localidade.setFax("3456.6543");
		localidade.setEmail("capitalalgas@algas.com.br");
		localidade.setUnidadeNegocio((UnidadeNegocio) buscar(UnidadeNegocioImpl.class));
		localidade.setClasse((LocalidadeClasse) buscar(LocalidadeClasse.class));
		localidade.setPorte((LocalidadePorte) buscar(LocalidadePorteImpl.class));
		localidade.setCodigoCentroCusto("123");
		localidade.setInformatizada(Boolean.TRUE);
		localidade.setCliente((Cliente) buscar(ClienteImpl.class));
		localidade.setMedidorLocalArmazenagem((MedidorLocalArmazenagem) buscar(MedidorLocalArmazenagemImpl.class));
		localidade.setCodigoFaxDDD(81);
		localidade.setCodigoFoneDDD(81);

		return localidade;
	}

	private Endereco montarObjetoEndereco() throws GGASException {

		Cep cep = fachada.criarCep();
		cep.setCep("51000000");
		super.salvar(cep);

		Endereco endereco;
		endereco = fachada.criarEndereco();
		endereco.setCep(cep);
		endereco.setEnderecoReferencia("Perto de algum lugar");
		return endereco;
	}

	private void inserirObjetoMedidorLocalArmazenagem() throws GGASException {

		MedidorLocalArmazenagem medidorLocalArmazenagem = new MedidorLocalArmazenagemImpl();
		medidorLocalArmazenagem.setDadosAuditoria(DadosAuditoriaUtil.getDadosAuditoria());
		medidorLocalArmazenagem.setDescricao("Descrição");
		medidorLocalArmazenagem.setDescricaoAbreviada("desc");
		medidorLocalArmazenagem.setHabilitado(Boolean.TRUE);
		medidorLocalArmazenagem.setOficina(Boolean.TRUE);
		medidorLocalArmazenagem.setUltimaAlteracao(Calendar.getInstance().getTime());
		medidorLocalArmazenagem.setVersao(0);

		super.salvar(medidorLocalArmazenagem);
	}

	private void inserirObjetoUnidadeNegocio() throws GGASException {

		UnidadeNegocio unidadeNegocio = (UnidadeNegocio) controlador.criar();
		unidadeNegocio.setAnoMesReferencia(201501);
		unidadeNegocio.setClasseUnidade((ClasseUnidade) buscar(ClasseUnidadeImpl.class));
		unidadeNegocio.setCliente((Cliente) buscar(ClienteImpl.class));
		unidadeNegocio.setCodigo("123");
		unidadeNegocio.setData(Calendar.getInstance().getTime());
		unidadeNegocio.setDescricao("Descrição");
		unidadeNegocio.setDescricaoAbreviada("desc");
		unidadeNegocio.setGerenciaRegional((GerenciaRegional) buscar(GerenciaRegionalImpl.class));

		super.salvar(unidadeNegocio);
	}

	private void inserirObjetoGerenciaRegional() throws GGASException {

		GerenciaRegional gerenciaRegional = fachada.criarGerenciaRegional();
		gerenciaRegional.setNome("Gerência Capital");
		gerenciaRegional.setNomeAbreviado("CAP");
		gerenciaRegional.setFone("34389027");
		gerenciaRegional.setRamalFone("13");
		gerenciaRegional.setFax("34387898");
		gerenciaRegional.setEmail("capitalgas@algas.com.br");
		gerenciaRegional.setEndereco(montarObjetoEndereco());
		gerenciaRegional.setHabilitado(Boolean.TRUE);
		gerenciaRegional.setVersao(0);

		super.salvar(gerenciaRegional);
	}

	private void inserirObjetoLocalidadeClasse() throws GGASException {

		LocalidadeClasse localidadeClasse = (LocalidadeClasse) controladorLocalidade.criarLocalidadeClasse();
		localidadeClasse.setDescricao("Descrição");
		localidadeClasse.setHabilitado(Boolean.TRUE);
		localidadeClasse.setVersao(0);
		
		super.salvar(localidadeClasse);
	}

	private void inserirObjetoLocalidadePorte() throws GGASException {

		LocalidadePorte localidadePorte = (LocalidadePorte) controladorLocalidade.criarLocalidadePorte();
		localidadePorte.setDescricao("Descrição");
		localidadePorte.setHabilitado(true);
		localidadePorte.setVersao(0);

		super.salvar(localidadePorte);
	}

	private void inserirObjetoClasseUnidade() throws GGASException {

		ClasseUnidade classeUnidade = (ClasseUnidade) buscar(ClasseUnidadeImpl.class);
		classeUnidade.setCliente((Cliente) buscar(ClienteImpl.class));
		classeUnidade.setDescricao("descrição");
		classeUnidade.setDescricaoAbreviada("desc");
		
		super.salvar(classeUnidade);
	}

}
