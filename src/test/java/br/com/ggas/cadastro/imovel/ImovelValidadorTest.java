/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.cadastro.imovel;

import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.localidade.impl.QuadraImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.rota.impl.RotaImpl;
import org.easymock.EasyMock;
import org.easymock.EasyMockRule;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Teste unitário do {@link ImovelValidador}
 * @author jose.victor@logiquesistemas.com.br
 */
public class ImovelValidadorTest extends GGASTestCase {


	@Rule
	public EasyMockRule mock = new EasyMockRule(this);

	@Mock
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	@Qualifier("imovelValidador")
	@TestSubject
	private ImovelValidador validador;

	@Test
	public void testarImovelSemCep() {

		final ImovelImpl imovel = new ImovelImpl();
		imovel.setQuadra(new QuadraImpl());

		String erro = "";
		try {
			validador.validar(imovel);
			fail("Validou com sucesso um imóvel sem cep");
		} catch (NegocioException ex) {
			erro = ex.getChaveErro();
		}

		assertThat(erro, equalTo("ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS"));
	}

	@Test
	public void testarImovelSemQuadra() {

		final ImovelImpl imovel = new ImovelImpl();
		imovel.setCep("59075-340");

		String erro = "";
		try {
			validador.validar(imovel);
			fail("Validou com sucesso um imóvel sem quadra");
		} catch (NegocioException ex) {
			erro = ex.getChaveErro();
		}

		assertThat(erro, equalTo("ERRO_NEGOCIO_CAMPOS_OBRIGATORIOS"));
	}

	@Test
	public void testarImovelValido() throws NegocioException {

		final ImovelImpl imovel = new ImovelImpl();
		imovel.setCep("59075-340");
		imovel.setQuadra(new QuadraImpl());

		validador.validar(imovel);
	}

	@Test
	public void testarMaximoRotasInvalido() throws NegocioException {
		final ImovelImpl imovel = new ImovelImpl();
		imovel.setCep("59075-340");
		imovel.setQuadra(new QuadraImpl());

		PontoConsumo p1 = new PontoConsumoImpl();
		final RotaImpl r1 = new RotaImpl();
		r1.setChavePrimaria(1L);
		r1.setNumeroRota("r1");
		final List<PontoConsumo> pontosRota1 = Arrays.asList(new PontoConsumoImpl(), new PontoConsumoImpl());
		r1.setPontosConsumo(pontosRota1);
		r1.setNumeroMaximoPontosConsumo(2);
		p1.setRota(r1);

		PontoConsumo p2 = new PontoConsumoImpl();
		final RotaImpl r2 = new RotaImpl();
		r2.setChavePrimaria(2L);
		r2.setNumeroRota("r2");
		final List<PontoConsumo> pontosRota2 = Collections.singletonList(new PontoConsumoImpl());
		r2.setPontosConsumo(pontosRota2);
		r2.setNumeroMaximoPontosConsumo(1);
		p2.setRota(r2);


		List<PontoConsumo> pontos = Arrays.asList(p1, p2);
		imovel.setListaPontoConsumo(pontos);
		String chaveErro = "";
		String parametro = "";

		expect(controladorPontoConsumo.consultarPontoConsumoPorRota(r1)).andReturn(pontosRota1);
		expect(controladorPontoConsumo.consultarPontoConsumoPorRota(r2)).andReturn(pontosRota2);
		replay(controladorPontoConsumo);

		try {
			validador.validar(imovel);
			fail("Validou com suesso um imóvel com máximo de rotas extrapoladas");
		} catch (NegocioException e) {
			chaveErro = e.getChaveErro();
			parametro = (String) e.getParametrosErro()[0];
		}

		assertThat(chaveErro, equalTo("ERRO_NEGOCIO_MAXIMO_PONTO_EXTRAPOLADO_NAS_ROTAS"));
		assertThat(parametro, equalTo("r1, r2"));


	}

	@Test
	public void testarMaximoRotasValido() throws NegocioException {
		final ImovelImpl imovel = new ImovelImpl();
		imovel.setCep("59075-340");
		imovel.setQuadra(new QuadraImpl());

		PontoConsumo p1 = new PontoConsumoImpl();
		final RotaImpl r1 = new RotaImpl();
		r1.setChavePrimaria(1L);
		r1.setNumeroRota("r1");
		final List<PontoConsumo> pontosConsumo = Arrays.asList(new PontoConsumoImpl(), new PontoConsumoImpl());
		r1.setPontosConsumo(pontosConsumo);
		r1.setNumeroMaximoPontosConsumo(3);
		p1.setRota(r1);


		List<PontoConsumo> pontos = Collections.singletonList(p1);
		imovel.setListaPontoConsumo(pontos);

		expect(controladorPontoConsumo.consultarPontoConsumoPorRota(EasyMock.anyObject())).andReturn(pontosConsumo);
		replay(controladorPontoConsumo);
		validador.validar(imovel);
	}

}
