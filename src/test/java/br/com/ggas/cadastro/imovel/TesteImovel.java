package br.com.ggas.cadastro.imovel;

import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.imovel.impl.ImovelImpl;
import br.com.ggas.cadastro.imovel.impl.ModalidadeMedicaoImovelImpl;
import br.com.ggas.cadastro.localidade.Localidade;
import br.com.ggas.cadastro.localidade.Quadra;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.localidade.SetorComercial;
import br.com.ggas.cadastro.localidade.impl.LocalidadeImpl;
import br.com.ggas.cadastro.localidade.impl.QuadraFaceImpl;
import br.com.ggas.cadastro.localidade.impl.QuadraImpl;
import br.com.ggas.cadastro.localidade.impl.SetorComercialImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.cadastro.imovel.ControladorImovel;

public class TesteImovel extends GGASTestCase{

	Imovel imovel;
	
	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_IMOVEL.sql";
	
	@Autowired
	@Qualifier(ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL)
	ControladorImovel controladorImovel;
	
	@Autowired
	@Qualifier(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO)
	ControladorPontoConsumo controladorPontoConsumo;

	@Before
	public void setup() throws FileNotFoundException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
		imovel = new ImovelImpl();
	}

	@Test
	@Transactional
	public void testeSetEGetDataEntrega() {
		Assert.assertNull(imovel.getDataEntrega());

		Date dataEntrega = new Date();
		imovel.setDataEntrega(dataEntrega);
		Assert.assertNotEquals(System.identityHashCode(dataEntrega), System.identityHashCode(imovel.getDataEntrega()));
	}

	@Test
	@Transactional
	public void testeGetInscricao() {
		Assert.assertTrue(StringUtils.isBlank(imovel.getInscricao()));
		imovel.setQuadraFace(prepararQuadraFace());
		Assert.assertEquals("1.10.100.1000", imovel.getInscricao());

		imovel.setNumeroLote(10000);
		Assert.assertEquals("1.10.100.1000.10000", imovel.getInscricao());

		imovel.setNumeroSublote(100000);
		Assert.assertEquals("1.10.100.1000.10000.100000", imovel.getInscricao());
	}

	private QuadraFace prepararQuadraFace() {
		QuadraFace quadraFace = new QuadraFaceImpl();
		Quadra quadra = new QuadraImpl();
		SetorComercial setorComercial = new SetorComercialImpl();
		Localidade localidade = new LocalidadeImpl();
		localidade.setChavePrimaria(1);
		setorComercial.setCodigo("10");
		quadra.setNumeroQuadra("100");
		quadraFace.setChavePrimaria(1000);

		setorComercial.setLocalidade(localidade);
		quadra.setSetorComercial(setorComercial);
		quadraFace.setQuadra(quadra);

		return quadraFace;
	}

	@Test
	@Transactional
	@SuppressWarnings("deprecation")
	public void testeIsIndividual() {
		Assert.assertNull(imovel.isIndividual());
		imovel.setModalidadeMedicaoImovel(new ModalidadeMedicaoImovelImpl(ModalidadeMedicaoImovel.COLETIVA, "Coletiva"));

		Assert.assertNotNull(imovel.isIndividual());
		Assert.assertFalse(imovel.isIndividual());
	}
	
	@Test
	@Transactional
	public void testeBuscasImovel() throws NegocioException {
		Map<String, Object> filtro = montarFiltro();
		Collection<Imovel> listaImovel = controladorImovel.consultarImoveis(filtro);
		
		assertNotNull(listaImovel);
		
	}
	
	@Test
	@Transactional
	public void testeConsultaOrdenacaoPontoConsumo() throws NegocioException, ConcorrenciaException {
		controladorPontoConsumo.ordenarPontoConsumo();
		
		PontoConsumo pontoConsumo = controladorPontoConsumo.obterPontoConsumo(999999l);
		
		assertNotNull(pontoConsumo.getNumeroSequenciaLeitura());
	}
	
	public Map<String, Object> montarFiltro(){
		Map<String, Object> filtro = new HashMap<>();
		filtro.put("idSegmentoPontoConsumo", 4l);
		filtro.put("numeroImovel", "760");
		filtro.put("indicadorCondominio", null);
		filtro.put("nome", "EDIFICIO PRADA - APT 109");
		filtro.put("habilitado", true);
		filtro.put("cepImovel", "57608-010");
		filtro.put("tipoMedicao", null);
		filtro.put("descricaoPontoConsumo", "EDIFICIO PRADA - APT 102");
		filtro.put("complementoImovel", "COND PRADA  - APT 102");
		filtro.put("pontoConsumoLegado", "1025");
		filtro.put("idRota", 9999l);
		
		return filtro;
	}
}
