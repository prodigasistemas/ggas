
package br.com.ggas.cadastro.imovel;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.imovel.impl.ControladorSegmentoImpl;
import br.com.ggas.cadastro.imovel.impl.RamoAtividadeImpl;
import br.com.ggas.cadastro.imovel.impl.TipoSegmentoImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.medicao.rota.Periodicidade;
import br.com.ggas.medicao.rota.impl.PeriodicidadeImpl;

public class TesteSegmento extends GGASTestCase {

	@Autowired
	ControladorSegmentoImpl controladorSegmento;
	
	@Before
	public void setUp() throws GGASException {

		cargaInicial.carregar();
		this.inserirTipoSegmento();
	}

	@Test
	@Transactional
	public void testeBuscarSegmentoPorChave() throws GGASException {

		Segmento segmento = (Segmento) fachada.obterSegmento(1L);
		Assert.assertEquals("IND", segmento.getDescricaoAbreviada());
	}

	@Test
	@Transactional
	public void testeConsultarSegmento() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("descricao", "INDUSTRIAL");
		filtro.put("descricaoAbreviada", "IND");

		Segmento segmentoConsultado = fachada.consultarSegmento(filtro).iterator().next();
		Assert.assertNotNull(segmentoConsultado);
		Assert.assertEquals("INDUSTRIAL", segmentoConsultado.getDescricao());
		Assert.assertEquals("IND", segmentoConsultado.getDescricaoAbreviada());
	}

	@Test
	@Transactional
	public void testeAtualizarSegmento() throws GGASException {

		Segmento segmento = fachada.obterSegmento(1L);
		String descricaoAntesAlteracao = segmento.getDescricao();
		segmento.setDescricao("ABISTRUAL");

		fachada.atualizarSegmento(segmento, null);

		String descricaoPosAlteracao = ((Segmento) fachada.obterSegmento(segmento.getChavePrimaria())).getDescricao();
		Assert.assertNotNull(descricaoPosAlteracao);
		Assert.assertNotEquals(descricaoAntesAlteracao, descricaoPosAlteracao);
	}

	@Test
	@Transactional
	public void testeRemoverSegmento() throws GGASException {

		Segmento segmento = montarObjetoSegmento();
		fachada.inserirSegmento(segmento);

		int qtdInicio = fachada.consultarSegmento(null).size();
		segmento.setRamosAtividades(null);
		fachada.removerSegmento(segmento);
		int qtdFim = fachada.consultarSegmento(null).size();

		Assert.assertEquals(qtdInicio - 1, qtdFim);
	}

	@Test
	@Transactional
	public void testeInserirSegmento() throws GGASException {

		int qtdInicio = fachada.consultarSegmento(null).size();
		fachada.inserirSegmento(montarObjetoSegmento());
		int qtdFim = fachada.consultarSegmento(null).size();

		Assert.assertEquals(qtdInicio + 1, qtdFim);
	}

	private Segmento montarObjetoSegmento() throws GGASException {
		
		Segmento segmento = fachada.criarSegmento();
		segmento.setDescricao("Segmento teste");
		segmento.setDescricaoAbreviada("SEG");
		segmento.setNumeroCiclos(2);
		segmento.setNumeroDiasFaturamento(2);
		segmento.setTipoConsumoFaturamento((EntidadeConteudo) buscar(EntidadeConteudoImpl.class)); 
		segmento.setUsaProgramacaoConsumo(Boolean.TRUE);
		segmento.setTipoSegmento((TipoSegmento) buscar(TipoSegmentoImpl.class));
		segmento.setPeriodicidade((Periodicidade) buscar(PeriodicidadeImpl.class));
		segmento.setTipoConsumoFaturamento((EntidadeConteudo) buscar(EntidadeConteudo.class));

		// a collection foi necessria pq o segmento pode ter um ou mais ramos de atividades
		Collection<RamoAtividade> ramosAtividades = new HashSet<RamoAtividade>();
		// pegando um ramo atividade da carga inicial
		ramosAtividades.add((RamoAtividade) buscar(RamoAtividadeImpl.class));
		segmento.setRamosAtividades(ramosAtividades);

		return segmento;
	}

	private void inserirTipoSegmento() throws NegocioException {

		TipoSegmento tipoSegmento = (TipoSegmento) controladorSegmento.criarTipoSegmento();
		tipoSegmento.setDescricao("Tipo segmento teste");
		super.salvar(tipoSegmento);
	}

}
