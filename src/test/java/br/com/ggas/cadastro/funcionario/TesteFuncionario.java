
package br.com.ggas.cadastro.funcionario;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.empresa.impl.EmpresaImpl;
import br.com.ggas.cadastro.funcionario.impl.ControladorFuncionarioImpl;
import br.com.ggas.cadastro.funcionario.impl.FuncionarioImpl;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeTipo;
import br.com.ggas.cadastro.unidade.impl.UnidadeOrganizacionalImpl;
import br.com.ggas.cadastro.unidade.impl.UnidadeTipoImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.Leiturista;

/**
 * @author Roberto Alencar
 */
public class TesteFuncionario extends GGASTestCase {

	@Autowired
	ControladorFuncionarioImpl controladorFuncionario;

	/**
	 * Sets the up.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Before
	public void setUp() throws GGASException {

		super.cargaInicial.carregar();
		this.inserirUnidadeOrganizacional();
		this.inserirFuncionario();
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a consulta de funcionário por chave
	// -----------------------------------------------------------------------------------------------------

	/**
	 * Teste buscar funcionario por chave.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Test
	@Transactional
	public void testeBuscarFuncionarioPorChaveSucesso() throws GGASException {

		long chave = 1L;
		Funcionario funcionario = (Funcionario) fachada.obterFuncionario(chave);
		Assert.assertEquals(chave, funcionario.getChavePrimaria());
	}

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeBuscarFuncionarioPorChaveFalha() throws GGASException {

		fachada.obterFuncionario(-1L);
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a a busca por filtros de um funcionário
	// -----------------------------------------------------------------------------------------------------

	@Test
	@Transactional
	public void testeConsultarFuncionarioVazio() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("matricula", "111");
		Collection<Funcionario> retorno = fachada.consultarFuncionarios(filtro);
		Assert.assertTrue(retorno.isEmpty());
	}

	/**
	 * Teste consultar funcionario.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Test
	@Transactional
	public void testeConsultarFuncionario() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("matricula", "164");
		filtro.put("nome", "João da Silva");
		filtro.put("descricaoCargo", "Supervisor de TI");

		Empresa empresa = (Empresa) buscar(EmpresaImpl.class);
		filtro.put("idEmpresa", empresa.getChavePrimaria());

		UnidadeOrganizacional unidadeOrganizacional = (UnidadeOrganizacionalImpl) buscar(UnidadeOrganizacionalImpl.class);
		filtro.put("idUnidadeOrganizacional", unidadeOrganizacional.getChavePrimaria());

		filtro.put("habilitado", true);
		Funcionario funcionarioConsultado = fachada.consultarFuncionarios(filtro).iterator().next();

		Assert.assertNotNull(funcionarioConsultado);
		Assert.assertEquals("164", funcionarioConsultado.getMatricula());
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a inclusão de funcionário
	// -----------------------------------------------------------------------------------------------------

	// Para enxergar os fluxos de execeção do inserir, ver o método preInsercao do controlador da entidade

	// Para enxergar os campos obrigatórios, ver o método validarDados da entidade

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeInserirFuncionarioMatriculaExistente() throws GGASException {

		fachada.inserirFuncionario(montarObjetoFuncionario());
	}

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeInserirFuncionarioCamposObrigatorios() throws GGASException {

		Funcionario funcionario = montarObjetoFuncionario();
		funcionario.setNome(null);
		funcionario.setDescricaoCargo(null);
		funcionario.setEmpresa(null);
		funcionario.setUnidadeOrganizacional(null);
		fachada.inserirFuncionario(funcionario);
	}

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeInserirFuncionarioEmailInvalido() throws GGASException {

		Funcionario funcionario = montarObjetoFuncionario();
		funcionario.setEmail("abc");
		fachada.inserirFuncionario(funcionario);
	}

	/**
	 * Teste inserir funcionario.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Test
	@Transactional
	public void testeInserirFuncionarioSucesso() throws GGASException {

		int qtdInicio = fachada.consultarFuncionarios(null).size();

		Funcionario funcionario = montarObjetoFuncionario();
		funcionario.setMatricula("721"); // Alteração realizada para não dar erro no método que valida um matricula existente
		fachada.inserirFuncionario(funcionario);

		int qtdFim = fachada.consultarFuncionarios(null).size();
		Assert.assertEquals(qtdInicio + 1, qtdFim);
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a alteração de funcionário
	// -----------------------------------------------------------------------------------------------------

	// Para enxergar os fluxos de execeção do inserir, ver o método preAtualizacao do controlador da entidade

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeNegaInativacaoFuncionario() throws Throwable {

		Funcionario funcionarioComLeituristas = (Funcionario) buscar(FuncionarioImpl.class);
		inserirLeituristasFuncionario(funcionarioComLeituristas);
		funcionarioComLeituristas.setHabilitado(Boolean.FALSE);
		super.getSesstion().evict(funcionarioComLeituristas);
		chamarMetodoPrivado(controladorFuncionario, "validarInativacaoFuncionario", funcionarioComLeituristas);
	}

	@Test
	@Transactional
	public void testePermiteInativacaoFuncionario() throws Throwable {

		Funcionario funcionarioSemLeituristas = montarObjetoFuncionario();
		funcionarioSemLeituristas.setMatricula("336"); // Alteração realizada para não dar erro no método que valida um matricula existente
		fachada.inserirFuncionario(funcionarioSemLeituristas);
		funcionarioSemLeituristas.setHabilitado(Boolean.FALSE);

		try {
			chamarMetodoPrivado(controladorFuncionario, "validarInativacaoFuncionario", funcionarioSemLeituristas);
			Assert.assertTrue(Boolean.TRUE);
		} catch (NegocioException e) {
			Assert.assertTrue(Boolean.FALSE);
		}
	}

	/**
	 * Teste atualizar funcionario.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Test
	@Transactional
	public void testeAtualizarFuncionarioSucesso() throws GGASException {

		Funcionario funcionario = (Funcionario) fachada.obterFuncionario(1L);
		String nomeAntesAlteracao = funcionario.getNome();

		funcionario.setNome("Novo Nome");
		funcionario.setUnidadeOrganizacional((UnidadeOrganizacional) buscar(UnidadeOrganizacionalImpl.class)); 
		// é necessário inclusir essa
		// dependência por a mesma não está
		// presente na carga inicial
		
		fachada.atualizarFuncionario(funcionario);

		Funcionario funcionarioAlterado = (Funcionario) fachada.obterFuncionario(1L);
		String nomePosAlteracao = funcionarioAlterado.getNome();

		Assert.assertNotEquals(nomeAntesAlteracao, nomePosAlteracao);
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a remoção de funcionário
	// -----------------------------------------------------------------------------------------------------

	/**
	 * Teste remover funcionario.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	@Test
	@Transactional
	public void testeRemoverFuncionario() throws GGASException {

		Funcionario funcionario = montarObjetoFuncionario();
		funcionario.setMatricula("123");
		fachada.inserirFuncionario(funcionario);

		int qtdInicio = fachada.consultarFuncionarios(null).size();
		controladorFuncionario.remover(funcionario);
		int qtdFim = fachada.consultarFuncionarios(null).size();

		Assert.assertEquals(qtdInicio - 1, qtdFim);
	}

	// -----------------------------------------------------------------------------------------------------
	// Testes de métodos privados existentes no controlador da entidade
	// -----------------------------------------------------------------------------------------------------

	/**
	 * Teste consultar funcionario por matricula.
	 * 
	 * @throws Throwable
	 *             the throwable
	 */
	@Test
	@Transactional
	public void testeConsultarFuncionarioPorMatriculaFalha() throws Throwable {

		Funcionario funcionario = (Funcionario) chamarMetodoPrivado(controladorFuncionario, "consultarFuncionarioPorMatricula", "888");
		Assert.assertNull(funcionario);
	}

	@Test
	@Transactional
	public void testeConsultarFuncionarioPorMatriculaSucesso() throws Throwable {

		Funcionario funcionario = (Funcionario) chamarMetodoPrivado(controladorFuncionario, "consultarFuncionarioPorMatricula", "999999");

		Assert.assertNotNull(funcionario);
		Assert.assertEquals("999999", funcionario.getMatricula());
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos privados utilitários para esta classe
	// -----------------------------------------------------------------------------------------------------

	private void inserirFuncionario() throws GGASException {

		super.salvar(montarObjetoFuncionario());
	}

	/**
	 * Montar objeto funcionario.
	 * 
	 * @return the funcionario
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private Funcionario montarObjetoFuncionario() throws GGASException {

		Funcionario funcionario = fachada.criarFuncionario();
		funcionario.setMatricula("164");
		funcionario.setDescricaoCargo("Supervisor de TI");
		funcionario.setNome("João da Silva");
		funcionario.setEmpresa((Empresa) buscar(EmpresaImpl.class));
		funcionario.setUnidadeOrganizacional((UnidadeOrganizacionalImpl) buscar(UnidadeOrganizacionalImpl.class));
		funcionario.setEmail("joao.silva@companhia.com.br");
		funcionario.setCodigoDDD(81);
		funcionario.setFone("8888.4322");
		return funcionario;
	}

	/**
	 * Inserir unidade organizacional.
	 * 
	 * @throws GGASException
	 *             the GGAS exception
	 */
	private void inserirUnidadeOrganizacional() throws GGASException {

		UnidadeOrganizacional unidadeOrganizacional = fachada.criarUnidadeOrganizacional();
		unidadeOrganizacional.setUnidadeTipo((UnidadeTipo) buscar(UnidadeTipoImpl.class));
		unidadeOrganizacional.setEmpresa((Empresa) buscar(EmpresaImpl.class));
		unidadeOrganizacional.setDescricao("Descrição da unidade organizacional");
		unidadeOrganizacional.setInicioExpediente(Calendar.getInstance().getTime());
		unidadeOrganizacional.setFimExpediente(Calendar.getInstance().getTime());
		super.salvar(unidadeOrganizacional);
	}

	private void inserirLeituristasFuncionario(Funcionario funcionario) throws GGASException {

		for (int i = 0; i < 5; i++) {
			Leiturista leiturista = fachada.criarLeiturista();
			leiturista.setFuncionario(funcionario);
			leiturista.setHabilitado(Boolean.TRUE);
			super.salvar(leiturista);
		}
	}
}
