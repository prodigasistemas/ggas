package br.com.ggas.cadastro.empresa;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.impl.ControladorUnidadeOrganizacionalImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.DadosAuditoriaUtil;

/**
 * @author Wanderson Costa
 */
public class TesteUnidadeOrganizacional extends GGASTestCase {

	@Autowired
	ControladorUnidadeOrganizacionalImpl controladorUnidadeOrganizacional;
	
	@Before
	@Transactional
	public void setUp() throws GGASException {

		super.cargaInicial.carregar();
	}

	@Test
	@Transactional
	public void testeInserirUnidadeOrganizacional() throws GGASException, ParseException {

		int qntInicio = fachada.consultarUnidadeOrganizacional(null).size();
		fachada.inserirUnidadeOrganizacional(montarObjetoUnidadeOrganizacional());
		int qntFim = fachada.consultarUnidadeOrganizacional(null).size();

		assertEquals((qntInicio + 1), qntFim);
	}
	
	@Test
	@Transactional
	public void testeBuscarUnidadeOrganizacionalPorChaveSucesso() throws GGASException, ParseException {

		UnidadeOrganizacional unidadeOrganizacional = montarObjetoUnidadeOrganizacional();
		super.salvar(unidadeOrganizacional);
		
		long chave = unidadeOrganizacional.getChavePrimaria();
		UnidadeOrganizacional unidadeOrganizacionalPesquisa = (UnidadeOrganizacional) fachada.obterUnidadeOrganizacional(chave);
		Assert.assertEquals(chave, unidadeOrganizacionalPesquisa.getChavePrimaria());
	}

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeBuscarUnidadeOrganizacionalPorChaveFalha() throws GGASException, ParseException {

		UnidadeOrganizacional unidadeOrganizacional = montarObjetoUnidadeOrganizacional();
		super.salvar(unidadeOrganizacional);
		
		fachada.obterUnidadeOrganizacional(-1L);
	}

	@Test
	@Transactional
	public void testeConsultarUnidadeOrganizacionalVazia() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("descricaoUnidade", "Casa da Silvia");
		Collection<UnidadeOrganizacional> retorno = fachada.consultarUnidadeOrganizacional(filtro);
		Assert.assertTrue(retorno.isEmpty());
	}

	@Test
	@Transactional
	public void testeConsultarUnidadeOrganizacional() throws GGASException, ParseException {

		UnidadeOrganizacional unidadeOrganizacional = montarObjetoUnidadeOrganizacional();
		super.salvar(unidadeOrganizacional);
		
		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("descricaoUnidade", "Teste de Unidade Organizacional");
		filtro.put("siglaUnidade", "TSO");

		UnidadeOrganizacional unidadeOrganizacionalConsultada = fachada.consultarUnidadeOrganizacional(filtro).iterator().next();

		Assert.assertNotNull(unidadeOrganizacionalConsultada);
		Assert.assertEquals("TSO", unidadeOrganizacionalConsultada.getSigla());
	}

	@Test
	@Transactional
	public void testeRemoverUnidadeOrganizacional() throws GGASException, ParseException {

		UnidadeOrganizacional unidadeOrganizacional = montarObjetoUnidadeOrganizacional();
		super.salvar(unidadeOrganizacional);
		
		int qtdInicio = fachada.consultarUnidadeOrganizacional(null).size();
		controladorUnidadeOrganizacional.remover(unidadeOrganizacional);
		int qtdFim = fachada.consultarUnidadeOrganizacional(null).size();

		Assert.assertEquals(qtdInicio - 1, qtdFim);
	}

	private UnidadeOrganizacional montarObjetoUnidadeOrganizacional() throws GGASException, ParseException {

		UnidadeOrganizacional unidadeOrganizacional = fachada.criarUnidadeOrganizacional();
		
		unidadeOrganizacional.setChavePrimaria(9999L);
		unidadeOrganizacional.setUnidadeTipo(fachada.obterUnidadeTipo(13L)); // CDL
		unidadeOrganizacional.setEmpresa(fachada.obterEmpresa(1L));
		unidadeOrganizacional.setDescricao("Teste de Unidade Organizacional");
		unidadeOrganizacional.setSigla("TSO");
		unidadeOrganizacional.setInicioExpediente(Calendar.getInstance().getTime());
		unidadeOrganizacional.setFimExpediente(Calendar.getInstance().getTime());
		unidadeOrganizacional.setVersao(1);
		unidadeOrganizacional.setHabilitado(Boolean.TRUE);
		unidadeOrganizacional.setDadosAuditoria(DadosAuditoriaUtil.getDadosAuditoria());
		unidadeOrganizacional.setUltimaAlteracao(Calendar.getInstance().getTime());

		return unidadeOrganizacional;
	}

}
