
package br.com.ggas.cadastro.empresa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.cliente.AtividadeEconomica;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ClienteAnexo;
import br.com.ggas.cadastro.cliente.ClienteEndereco;
import br.com.ggas.cadastro.cliente.ClienteFone;
import br.com.ggas.cadastro.cliente.ClienteSituacao;
import br.com.ggas.cadastro.cliente.ContatoCliente;
import br.com.ggas.cadastro.cliente.FaixaRendaFamiliar;
import br.com.ggas.cadastro.cliente.Nacionalidade;
import br.com.ggas.cadastro.cliente.OrgaoExpedidor;
import br.com.ggas.cadastro.cliente.PessoaSexo;
import br.com.ggas.cadastro.cliente.Profissao;
import br.com.ggas.cadastro.cliente.TipoCliente;
import br.com.ggas.cadastro.cliente.TipoContato;
import br.com.ggas.cadastro.cliente.TipoEndereco;
import br.com.ggas.cadastro.cliente.TipoFone;
import br.com.ggas.cadastro.cliente.impl.AtividadeEconomicaImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteSituacaoImpl;
import br.com.ggas.cadastro.cliente.impl.ControladorClienteImpl;
import br.com.ggas.cadastro.cliente.impl.FaixaRendaFamiliarImpl;
import br.com.ggas.cadastro.cliente.impl.NacionalidadeImpl;
import br.com.ggas.cadastro.cliente.impl.OrgaoExpedidorImpl;
import br.com.ggas.cadastro.cliente.impl.PessoaSexoImpl;
import br.com.ggas.cadastro.cliente.impl.ProfissaoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoClienteImpl;
import br.com.ggas.cadastro.cliente.impl.TipoContatoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoEnderecoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoFoneImpl;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.geografico.impl.UnidadeFederacaoImpl;
import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.cadastro.grupoeconomico.impl.GrupoEconomicoImpl;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.UnidadeNegocio;
import br.com.ggas.cadastro.localidade.impl.ControladorUnidadeNegocioImpl;
import br.com.ggas.cadastro.localidade.impl.GerenciaRegionalImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.medicao.vazaocorretor.impl.ClasseUnidadeImpl;
import br.com.ggas.util.DadosAuditoriaUtil;

/**
 * @author Wanderson Costa
 */
public class TesteEmpresa extends GGASTestCase {

	@Autowired
	ControladorEmpresa controladorEmpresa;

	@Autowired
	ControladorClienteImpl controladorCliente;

	@Autowired
	ControladorUnidadeNegocioImpl controladorUnidadeNegocio;

	@Before
	@Transactional
	public void setUp() throws GGASException {

		super.cargaInicial.carregar();
		this.inserirObjetoGerenciaRegional();
		this.inserirObjetoUnidadeNegocio();
		this.inserirObjetoGrupoEconomico();
	}

	@Test
	@Transactional
	public void testeBuscarEmpresaPorChaveSucesso() throws GGASException {

		long chave = 1L;
		Empresa empresa = (Empresa) fachada.obterEmpresa(chave);
		Assert.assertEquals(chave, empresa.getChavePrimaria());
	}

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeBuscarEmpresaPorChaveFalha() throws GGASException {

		fachada.obterEmpresa(-1L);
	}

	@Test
	@Transactional
	public void testeConsultarEmpresaVazia() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("nomeCliente", "99999");
		Collection<Empresa> retorno = fachada.consultarEmpresas(filtro);
		Assert.assertTrue(retorno.isEmpty());
	}

	@Test
	@Transactional
	public void testeConsultarEmpresa() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("nomeCliente", "ALGAS - GAS DE ALAGOAS SA");
		filtro.put("cnpjCliente", "69983484000132");

		Empresa empresaConsultada = fachada.consultarEmpresas(filtro).iterator().next();

		Assert.assertNotNull(empresaConsultada);
		Assert.assertEquals(1, empresaConsultada.getChavePrimaria());
	}

	@Test
	@Transactional
	public void testeRemoverEmpresa() throws GGASException, ParseException {

		Empresa empresa = montarObjetoEmpresa();
		super.salvar(empresa);

		int qtdInicio = fachada.consultarEmpresas(null).size();
		controladorEmpresa.remover(empresa);
		int qtdFim = fachada.consultarEmpresas(null).size();

		Assert.assertEquals(qtdInicio - 1, qtdFim);
	}

	private Empresa montarObjetoEmpresa() throws GGASException, ParseException {

		Empresa empresa = fachada.criarEmpresa();

		empresa.setVersao(1);
		empresa.setHabilitado(Boolean.TRUE);
		empresa.setPrincipal(Boolean.FALSE);
		empresa.setLogoEmpresa(new byte[0]);
		empresa.setCliente(inserirObjetoCliente());
		empresa.setServicosPrestados(null);
		empresa.setDadosAuditoria(DadosAuditoriaUtil.getDadosAuditoria());
		empresa.setUltimaAlteracao(Calendar.getInstance().getTime());

		return empresa;
	}

	private Cliente inserirObjetoCliente() throws GGASException, ParseException {

		Cliente cliente = fachada.criarCliente();
		cliente.setNome("Empresa de Gas");
		cliente.setNomeAbreviado("EmpG");
		cliente.setEmailPrincipal("capitalalgas@algas.com.br");
		cliente.setEmailSecundario("naocapital@algas.com.br");
		cliente.setCpf("10628636407");
		cliente.setRg("8642162");

		Collection<ClienteFone> fones = new HashSet<ClienteFone>();
		fones.add(montarObjetoClienteFone());
		cliente.setFones(fones);

		Collection<ClienteEndereco> enderecos = new HashSet<ClienteEndereco>();
		enderecos.add(montarObjetoClienteEndereco());
		cliente.setEnderecos(enderecos);

		cliente.setDataEmissaoRG(new SimpleDateFormat("dd/MM/yyyy").parse("01/10/2015"));
		cliente.setNumeroPassaporte("123456");
		cliente.setDataNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("28/05/1984"));
		cliente.setNomePai("Valter Pai");
		cliente.setNomeMae("Ana Maria");
		cliente.setNomeFantasia("Paranoico");
		cliente.setCnpj("57340896000175");
		cliente.setInscricaoMunicipal("InscritoM");
		cliente.setInscricaoRural("Inscrito Rural");
		cliente.setClienteSituacao((ClienteSituacao) buscar(ClienteSituacaoImpl.class));
		cliente.setTipoCliente((TipoCliente) buscar(TipoClienteImpl.class));
		cliente.setOrgaoExpedidor((OrgaoExpedidor) buscar(OrgaoExpedidorImpl.class));
		cliente.setUnidadeFederacao((UnidadeFederacao) buscar(UnidadeFederacaoImpl.class));
		cliente.setNacionalidade((Nacionalidade) buscar(NacionalidadeImpl.class));
		cliente.setProfissao((Profissao) buscar(ProfissaoImpl.class));
		cliente.setSexo((PessoaSexo) buscar(PessoaSexoImpl.class));
		cliente.setRendaFamiliar((FaixaRendaFamiliar) buscar(FaixaRendaFamiliarImpl.class));
		cliente.setAtividadeEconomica((AtividadeEconomica) buscar(AtividadeEconomicaImpl.class));
		cliente.setSenha("123456");
		cliente.setClientePublico(Boolean.TRUE);
		cliente.setContaAuxiliar("123123");
		cliente.setDadosAuditoria(DadosAuditoriaUtil.getDadosAuditoria());
		cliente.setDataEnvioBrindes(Calendar.getInstance().getTime());
		cliente.setDataUltimoAcesso(Calendar.getInstance().getTime());
		cliente.setGrupoEconomico((GrupoEconomico) buscar(GrupoEconomicoImpl.class));

		Long[] chavesPrimarias = {35L};
		cliente.setClienteResponsavel((Cliente) controladorCliente.consultarCliente(chavesPrimarias).iterator().next());

		Collection<ContatoCliente> contatoCliente = new HashSet<ContatoCliente>();
		contatoCliente.add(montarObjetoContatoCliente());
		cliente.setContatos(contatoCliente);

		Collection<ClienteAnexo> anexos = new HashSet<ClienteAnexo>();
		anexos.add(montarObjetoClienteAnexo());
		cliente.setAnexos(anexos);

		super.salvar(cliente);

		return cliente;
	}

	private ClienteAnexo montarObjetoClienteAnexo() throws GGASException {

		ClienteAnexo anexos = (ClienteAnexo) controladorCliente.criarClienteAnexo();
		anexos.setDescricaoAnexo("descricao");
		EntidadeConteudo tipoAnexo = (EntidadeConteudo) super.buscar(EntidadeConteudoImpl.class);
		anexos.setTipoAnexo(tipoAnexo);
		anexos.setAbaAnexo(tipoAnexo);
		anexos.setUltimaAlteracao(Calendar.getInstance().getTime());
		byte[] documentoAnexo = new byte[10];
		anexos.setDocumentoAnexo(documentoAnexo);

		return (anexos);
	}

	private ContatoCliente montarObjetoContatoCliente() throws GGASException {

		ContatoCliente contatoCliente = fachada.criarContatoCliente();
		contatoCliente.setChavePrimaria(99L);
		contatoCliente.setTipoContato((TipoContato) controladorCliente.obter(1, TipoContatoImpl.class));
		contatoCliente.setNome("Valter Negreiros");
		contatoCliente.setUltimaAlteracao(Calendar.getInstance().getTime());

		return (contatoCliente);
	}

	private ClienteEndereco montarObjetoClienteEndereco() throws GGASException {

		ClienteEndereco clienteEndereco = fachada.criarClienteEndereco();
		clienteEndereco.setCep(montarObjetoCep());
		clienteEndereco.setComplemento("Apart");
		clienteEndereco.setIndicadorPrincipal(1);
		clienteEndereco.setEnderecoReferencia("Perto ao BPM");
		clienteEndereco.setNumero("1891");
		clienteEndereco.setTipoEndereco((TipoEndereco) controladorCliente.obter(1, TipoEnderecoImpl.class));
		clienteEndereco.setCorrespondencia(Boolean.TRUE);
		clienteEndereco.setDadosAuditoria(DadosAuditoriaUtil.getDadosAuditoria());
		clienteEndereco.setUltimaAlteracao(Calendar.getInstance().getTime());

		return clienteEndereco;
	}

	private Cep montarObjetoCep() throws GGASException {

		Cep cep = fachada.criarCep();
		cep.setBairro("Pixete");
		cep.setCep("54725000");
		cep.setHabilitado(Boolean.TRUE);
		cep.setIntervaloNumeracao("101");
		cep.setLogradouro("Francisco Correia");
		cep.setNomeMunicipio("Sao Lourenco");
		cep.setTipoLogradouro("Apartamento");
		cep.setUf("AL");
		cep.setDadosAuditoria(DadosAuditoriaUtil.getDadosAuditoria());

		super.salvar(cep);

		return cep;
	}

	private Endereco montarObjetoEndereco() throws GGASException {

		Cep cep = fachada.criarCep();
		cep.setCep("51000000");
		super.salvar(cep);

		Endereco endereco;
		endereco = fachada.criarEndereco();
		endereco.setCep(cep);
		endereco.setEnderecoReferencia("Perto de algum lugar");

		return endereco;
	}

	private ClienteFone montarObjetoClienteFone() throws GGASException {

		ClienteFone fone = (ClienteFone) controladorCliente.criarClienteFone();

		fone.setHabilitado(Boolean.TRUE);
		fone.setNumero(34566543);
		fone.setCodigoDDD(81);
		fone.setUltimaAlteracao(Calendar.getInstance().getTime());
		fone.setIndicadorPrincipal(Boolean.TRUE);
		fone.setRamal(13);
		fone.setVersao(256);
		fone.setTipoFone((TipoFone) controladorCliente.obter(1, TipoFoneImpl.class));

		return fone;
	}

	private void inserirObjetoUnidadeNegocio() throws GGASException {

		UnidadeNegocio unidadeNegocio = (UnidadeNegocio) controladorUnidadeNegocio.criar();
		unidadeNegocio.setAnoMesReferencia(201501);
		unidadeNegocio.setClasseUnidade((ClasseUnidade) buscar(ClasseUnidadeImpl.class));
		unidadeNegocio.setCliente((Cliente) buscar(ClienteImpl.class));
		unidadeNegocio.setCodigo("123");
		unidadeNegocio.setData(Calendar.getInstance().getTime());
		unidadeNegocio.setDescricao("Descrição");
		unidadeNegocio.setDescricaoAbreviada("desc");
		unidadeNegocio.setGerenciaRegional((GerenciaRegional) buscar(GerenciaRegionalImpl.class));

		super.salvar(unidadeNegocio);
	}

	private void inserirObjetoGerenciaRegional() throws GGASException {

		GerenciaRegional gerenciaRegional = fachada.criarGerenciaRegional();
		gerenciaRegional.setNome("Gerência Capital");
		gerenciaRegional.setNomeAbreviado("CAP");
		gerenciaRegional.setFone("34389027");
		gerenciaRegional.setRamalFone("13");
		gerenciaRegional.setFax("34387898");
		gerenciaRegional.setEmail("capitalgas@algas.com.br");
		gerenciaRegional.setEndereco(montarObjetoEndereco());
		gerenciaRegional.setHabilitado(Boolean.TRUE);
		gerenciaRegional.setVersao(0);

		super.salvar(gerenciaRegional);
	}

	private void inserirObjetoGrupoEconomico() throws GGASException {

		GrupoEconomico grupoEconomico = fachada.criarGrupoEconomico();
		grupoEconomico.setDescricao("Esta é uma descrição");
		grupoEconomico.setQtdCliente(1);

		super.salvar(grupoEconomico);
	}

}
