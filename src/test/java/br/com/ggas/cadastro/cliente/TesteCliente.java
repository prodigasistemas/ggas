
package br.com.ggas.cadastro.cliente;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.cliente.impl.AtividadeEconomicaImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteImpl;
import br.com.ggas.cadastro.cliente.impl.ClienteSituacaoImpl;
import br.com.ggas.cadastro.cliente.impl.ControladorClienteImpl;
import br.com.ggas.cadastro.cliente.impl.FaixaRendaFamiliarImpl;
import br.com.ggas.cadastro.cliente.impl.NacionalidadeImpl;
import br.com.ggas.cadastro.cliente.impl.OrgaoExpedidorImpl;
import br.com.ggas.cadastro.cliente.impl.PessoaSexoImpl;
import br.com.ggas.cadastro.cliente.impl.ProfissaoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoClienteImpl;
import br.com.ggas.cadastro.cliente.impl.TipoContatoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoEnderecoImpl;
import br.com.ggas.cadastro.cliente.impl.TipoFoneImpl;
import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.geografico.UnidadeFederacao;
import br.com.ggas.cadastro.geografico.impl.UnidadeFederacaoImpl;
import br.com.ggas.cadastro.grupoeconomico.GrupoEconomico;
import br.com.ggas.cadastro.grupoeconomico.impl.GrupoEconomicoImpl;
import br.com.ggas.cadastro.localidade.GerenciaRegional;
import br.com.ggas.cadastro.localidade.UnidadeNegocio;
import br.com.ggas.cadastro.localidade.impl.ControladorUnidadeNegocioImpl;
import br.com.ggas.cadastro.localidade.impl.GerenciaRegionalImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.medicao.vazaocorretor.ClasseUnidade;
import br.com.ggas.medicao.vazaocorretor.impl.ClasseUnidadeImpl;
import br.com.ggas.util.DadosAuditoriaUtil;

/**
 * @author Valter Negreiros
 */
public class TesteCliente extends GGASTestCase {

	@Autowired
	ControladorClienteImpl controladorCliente;

	@Autowired
	ControladorUnidadeNegocioImpl controladorUnidadeNegocio;

	@Before
	@Transactional
	public void setUp() throws GGASException {

		super.cargaInicial.carregar();
		this.inserirObjetoGerenciaRegional();
		this.inserirObjetoUnidadeNegocio();
		this.inserirObjetoGrupoEconomico();
	}

	@Test
	@Transactional
	public void testeBuscarClientePorChaveSucesso() throws GGASException {

		long chave = 35L;
		Cliente cliente = (Cliente) fachada.obterCliente(chave);
		Assert.assertEquals(chave, cliente.getChavePrimaria());
	}

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeBuscarClientePorChaveFalha() throws GGASException {

		fachada.obterCliente(-1L);
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a a busca por filtros de um cliente
	// -----------------------------------------------------------------------------------------------------

	@Test
	@Transactional
	public void testeConsultarClienteVazio() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("nome", "111");
		Collection<Cliente> retorno = fachada.consultarClientes(filtro);
		Assert.assertTrue(retorno.isEmpty());
	}

	@Test
	@Transactional
	public void testeConsultarCliente() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("nome", "ALGAS - GAS DE ALAGOAS SA");
		filtro.put("nomeAbreviado", "ALGAS");
		filtro.put("cnpj", "69983484000132");
		filtro.put("emailPrincipal", "ti@algas.com.br");
		filtro.put("atividadeEconomica", "164");
		filtro.put("contaAuxiliar", "3949");

		Cliente clienteConsultado = fachada.consultarClientes(filtro).iterator().next();

		Assert.assertNotNull(clienteConsultado);
		Assert.assertEquals(35, clienteConsultado.getChavePrimaria());
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a inclusão de cliente
	// -----------------------------------------------------------------------------------------------------

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeInserirClienteCamposObrigatorios() throws GGASException, Exception {

		Cliente cliente = montarObjetoCliente();
		cliente.setNome(null);
		cliente.setNomeAbreviado(null);
		cliente.setCnpj(null);
		cliente.setEmailPrincipal(null);
		cliente.setAtividadeEconomica(null);
		cliente.setContaAuxiliar(null);
		fachada.inserirCliente(cliente);
	}

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeInserirClienteEmailInvalido() throws GGASException, Exception {

		Cliente cliente = montarObjetoCliente();
		cliente.setEmailPrincipal("abc");
		fachada.inserirCliente(cliente);
	}

	@Test
	@Transactional
	public void testeInserirCliente() throws GGASException, ParseException {

		int qntInicio = fachada.consultarCliente(null).size();
		fachada.inserirCliente(montarObjetoCliente());
		int qntFim = fachada.consultarCliente(null).size();

		assertEquals((qntInicio + 1), qntFim);
	}

	@Test
	@Transactional
	public void testeAtualizarCliente() throws GGASException, ParseException {

		Long chave = fachada.inserirCliente(montarObjetoCliente());
		Cliente cliente = (Cliente) controladorCliente.obter(chave);
		String nomeAntesAlteracao = cliente.getNome();

		cliente.setNome("Teste Alteração");
		fachada.alterarCliente(cliente);

		String nomePosAlteracao = ((Cliente) controladorCliente.obter(chave)).getNome();
		Assert.assertNotEquals(nomeAntesAlteracao, nomePosAlteracao);
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a remoção de cliente
	// -----------------------------------------------------------------------------------------------------

	@Test
	@Transactional
	public void testeRemoverCliente() throws GGASException, ParseException {

		Cliente cliente = montarObjetoCliente();
		cliente.setFones(null);
		super.salvar(cliente);

		int qtdInicio = fachada.consultarClientes(null).size();
		controladorCliente.remover(cliente);
		int qtdFim = fachada.consultarClientes(null).size();

		Assert.assertEquals(qtdInicio - 1, qtdFim);
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos privados utilitários para esta classe
	// -----------------------------------------------------------------------------------------------------

	private Cliente montarObjetoCliente() throws GGASException, ParseException {

		Cliente cliente = fachada.criarCliente();
		cliente.setNome("Valter Negreiros");
		cliente.setNomeAbreviado("ValN");
		cliente.setEmailPrincipal("capitalalgas@algas.com.br");
		cliente.setEmailSecundario("naocapital@algas.com.br");
		cliente.setCpf("10628636407");
		cliente.setRg("8642162");

		Collection<ClienteFone> fones = new HashSet<ClienteFone>();
		fones.add(montarObjetoClienteFone());
		cliente.setFones(fones);

		Collection<ClienteEndereco> enderecos = new HashSet<ClienteEndereco>();
		enderecos.add(montarObjetoClienteEndereco());
		cliente.setEnderecos(enderecos);

		cliente.setDataEmissaoRG(new SimpleDateFormat("dd/MM/yyyy").parse("01/10/2015"));
		cliente.setNumeroPassaporte("123456");
		cliente.setDataNascimento(new SimpleDateFormat("dd/MM/yyyy").parse("28/05/1984"));
		cliente.setNomePai("Valter Pai");
		cliente.setNomeMae("Ana Maria");
		cliente.setNomeFantasia("Paranoico");
		cliente.setCnpj("57340896000175");
		cliente.setRegimeRecolhimento("1");
		cliente.setInscricaoMunicipal("InscritoM");
		cliente.setInscricaoRural("Inscrito Rural");
		cliente.setClienteSituacao((ClienteSituacao) buscar(ClienteSituacaoImpl.class));
		cliente.setTipoCliente((TipoCliente) buscar(TipoClienteImpl.class));
		cliente.setOrgaoExpedidor((OrgaoExpedidor) buscar(OrgaoExpedidorImpl.class));
		cliente.setUnidadeFederacao((UnidadeFederacao) buscar(UnidadeFederacaoImpl.class));
		cliente.setNacionalidade((Nacionalidade) buscar(NacionalidadeImpl.class));
		cliente.setProfissao((Profissao) buscar(ProfissaoImpl.class));
		cliente.setSexo((PessoaSexo) buscar(PessoaSexoImpl.class));
		cliente.setRendaFamiliar((FaixaRendaFamiliar) buscar(FaixaRendaFamiliarImpl.class));
		cliente.setAtividadeEconomica((AtividadeEconomica) buscar(AtividadeEconomicaImpl.class));
		cliente.setSenha("123456");
		cliente.setClientePublico(Boolean.TRUE);
		cliente.setContaAuxiliar("123123");
		cliente.setDadosAuditoria(DadosAuditoriaUtil.getDadosAuditoria());
		cliente.setDataEnvioBrindes(Calendar.getInstance().getTime());
		cliente.setDataUltimoAcesso(Calendar.getInstance().getTime());
		cliente.setGrupoEconomico((GrupoEconomico) buscar(GrupoEconomicoImpl.class));

		Long[] chavesPrimarias = {35L};
		cliente.setClienteResponsavel((Cliente) controladorCliente.consultarCliente(chavesPrimarias).iterator().next());

		Collection<ContatoCliente> contatoCliente = new HashSet<ContatoCliente>();
		contatoCliente.add(montarObjetoContatoCliente());
		cliente.setContatos(contatoCliente);

		Collection<ClienteAnexo> anexos = new HashSet<ClienteAnexo>();
		anexos.add(montarObjetoClienteAnexo());
		cliente.setAnexos(anexos);

		return cliente;
	}

	private ClienteAnexo montarObjetoClienteAnexo() throws GGASException {

		ClienteAnexo anexos = (ClienteAnexo) controladorCliente.criarClienteAnexo();
		anexos.setDescricaoAnexo("descricao");
		EntidadeConteudo tipoAnexo = (EntidadeConteudo) super.buscar(EntidadeConteudoImpl.class);
		anexos.setTipoAnexo(tipoAnexo);
		anexos.setAbaAnexo(tipoAnexo);
		anexos.setUltimaAlteracao(Calendar.getInstance().getTime());
		byte[] documentoAnexo = new byte[10];
		anexos.setDocumentoAnexo(documentoAnexo);

		return (anexos);
	}

	private ContatoCliente montarObjetoContatoCliente() throws GGASException {

		ContatoCliente contatoCliente = fachada.criarContatoCliente();
		contatoCliente.setChavePrimaria(99L);
		contatoCliente.setTipoContato((TipoContato) controladorCliente.obter(1, TipoContatoImpl.class));
		contatoCliente.setNome("Valter Negreiros");
		contatoCliente.setUltimaAlteracao(Calendar.getInstance().getTime());

		return (contatoCliente);
	}

	private ClienteEndereco montarObjetoClienteEndereco() throws GGASException {

		ClienteEndereco clienteEndereco = fachada.criarClienteEndereco();
		clienteEndereco.setCep(montarObjetoCep());
		clienteEndereco.setComplemento("Apart");
		clienteEndereco.setIndicadorPrincipal(1);
		clienteEndereco.setEnderecoReferencia("Perto ao BPM");
		clienteEndereco.setNumero("1891");
		clienteEndereco.setTipoEndereco((TipoEndereco) controladorCliente.obter(1, TipoEnderecoImpl.class));
		clienteEndereco.setCorrespondencia(Boolean.TRUE);
		clienteEndereco.setDadosAuditoria(DadosAuditoriaUtil.getDadosAuditoria());
		clienteEndereco.setUltimaAlteracao(Calendar.getInstance().getTime());

		return clienteEndereco;
	}

	private Cep montarObjetoCep() throws GGASException {

		Cep cep = fachada.criarCep();
		cep.setBairro("Pixete");
		cep.setCep("54725000");
		cep.setHabilitado(Boolean.TRUE);
		cep.setIntervaloNumeracao("101");
		cep.setLogradouro("Francisco Correia");
		cep.setNomeMunicipio("Sao Lourenco");
		cep.setTipoLogradouro("Apartamento");
		cep.setUf("AL");
		cep.setDadosAuditoria(DadosAuditoriaUtil.getDadosAuditoria());

		super.salvar(cep);

		return cep;
	}

	private Endereco montarObjetoEndereco() throws GGASException {

		Cep cep = fachada.criarCep();
		cep.setCep("51000000");
		super.salvar(cep);

		Endereco endereco;
		endereco = fachada.criarEndereco();
		endereco.setCep(cep);
		endereco.setEnderecoReferencia("Perto de algum lugar");

		return endereco;
	}

	private ClienteFone montarObjetoClienteFone() throws GGASException {

		ClienteFone fone = (ClienteFone) controladorCliente.criarClienteFone();

		fone.setHabilitado(Boolean.TRUE);
		fone.setNumero(34566543);
		fone.setCodigoDDD(81);
		fone.setUltimaAlteracao(Calendar.getInstance().getTime());
		fone.setIndicadorPrincipal(Boolean.TRUE);
		fone.setRamal(13);
		fone.setVersao(256);
		fone.setTipoFone((TipoFone) controladorCliente.obter(1, TipoFoneImpl.class));

		return fone;
	}

	private void inserirObjetoUnidadeNegocio() throws GGASException {

		UnidadeNegocio unidadeNegocio = (UnidadeNegocio) controladorUnidadeNegocio.criar();
		unidadeNegocio.setAnoMesReferencia(201501);
		unidadeNegocio.setClasseUnidade((ClasseUnidade) buscar(ClasseUnidadeImpl.class));
		unidadeNegocio.setCliente((Cliente) buscar(ClienteImpl.class));
		unidadeNegocio.setCodigo("123");
		unidadeNegocio.setData(Calendar.getInstance().getTime());
		unidadeNegocio.setDescricao("Descrição");
		unidadeNegocio.setDescricaoAbreviada("desc");
		unidadeNegocio.setGerenciaRegional((GerenciaRegional) buscar(GerenciaRegionalImpl.class));

		super.salvar(unidadeNegocio);
	}

	private void inserirObjetoGerenciaRegional() throws GGASException {

		GerenciaRegional gerenciaRegional = fachada.criarGerenciaRegional();
		gerenciaRegional.setNome("Gerência Capital");
		gerenciaRegional.setNomeAbreviado("CAP");
		gerenciaRegional.setFone("34389027");
		gerenciaRegional.setRamalFone("13");
		gerenciaRegional.setFax("34387898");
		gerenciaRegional.setEmail("capitalgas@algas.com.br");
		gerenciaRegional.setEndereco(montarObjetoEndereco());
		gerenciaRegional.setHabilitado(Boolean.TRUE);
		gerenciaRegional.setVersao(0);

		super.salvar(gerenciaRegional);
	}

	private void inserirObjetoGrupoEconomico() throws GGASException {

		GrupoEconomico grupoEconomico = fachada.criarGrupoEconomico();
		grupoEconomico.setDescricao("Esta é uma descrição");
		grupoEconomico.setQtdCliente(1);

		super.salvar(grupoEconomico);
	}

}
