package br.com.ggas.suite;

import org.junit.extensions.cpsuite.ClasspathSuite;
import org.junit.extensions.cpsuite.ClasspathSuite.ClassnameFilters;
import org.junit.runner.RunWith;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;

@RunWith(ClasspathSuite.class)
@ClassnameFilters({"br.com.ggas.*", 
	"!br.com.ggas.selenium.*"})
@Component
@ContextConfiguration(locations = {"file:src/main/webapp/WEB-INF/spring-config-test.xml"})
public class GgasSuiteCenario {

}
