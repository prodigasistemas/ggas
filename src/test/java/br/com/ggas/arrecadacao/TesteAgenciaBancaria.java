package br.com.ggas.arrecadacao;

import java.util.Collection;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.arrecadacao.agenciabancaria.dominio.AgenciaBancariaVO;
import br.com.ggas.arrecadacao.agenciabancaria.negocio.ControladorAgenciaBancaria;
import br.com.ggas.arrecadacao.impl.BancoImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;

/**
 * 
 * @author voliveira
 *
 */
public class TesteAgenciaBancaria extends GGASTestCase {

	private static final String NOME_INSERIDO = "NOME ENTIDADE NOVA";
	
	private static final String CODIGO_INSERIDO = "11AA";
	
	private static final String NOME_ATUALIZADO = "NOME AGENCIA ATUALIZADO";

	@Autowired
	private ControladorAgenciaBancaria controladorAgenciaBancaria;

	@Before
	public void setUp() throws GGASException {
		
	}
	
	@Test
	@Transactional
	public void testeConsultarEntidade() throws Exception, 
				NumberFormatException {
		
		AgenciaBancariaVO agenciaBancariaVO = new AgenciaBancariaVO();
		agenciaBancariaVO.setNome(NOME_INSERIDO);
		agenciaBancariaVO.setCodigo(CODIGO_INSERIDO);
		
		BancoImpl buscar = (BancoImpl) buscar(BancoImpl.class);
		agenciaBancariaVO.setChaveBanco(String.valueOf(buscar.getChavePrimaria()));
		
		controladorAgenciaBancaria.inserirAgenciaBancaria(agenciaBancariaVO);
		super.getSesstion().evict(agenciaBancariaVO);
		
		HashMap<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("nome", NOME_INSERIDO);
		Agencia AgenciaBancariaInserida = 
						controladorAgenciaBancaria.consultarAgenciaBancaria(filtro).iterator().next();
		
		Assert.assertNotNull(AgenciaBancariaInserida);
		Assert.assertEquals(NOME_INSERIDO, AgenciaBancariaInserida.getNome());
		Assert.assertEquals(CODIGO_INSERIDO, AgenciaBancariaInserida.getCodigo());
		
	}

	@Test
	@Transactional
	public void testeConsultarListaEntidadeVazia() throws GGASException {
		
		HashMap<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("nome", NOME_ATUALIZADO);
		Collection<Agencia> consultarAgenciaBancaria = 
						controladorAgenciaBancaria.consultarAgenciaBancaria(filtro);
		Assert.assertTrue(consultarAgenciaBancaria.isEmpty());
		
	}
	
	@Test
	@Transactional
	public void testInserirAgenciaBancaria() throws NegocioException, Exception {
		
		HashMap<String, Object> filtro = new HashMap<String, Object>();
		int qtdInicio = controladorAgenciaBancaria.consultarAgenciaBancaria(filtro).size();;
		
		AgenciaBancariaVO agenciaBancariaVO = new AgenciaBancariaVO();
		agenciaBancariaVO.setNome(NOME_INSERIDO);
		agenciaBancariaVO.setCodigo(CODIGO_INSERIDO);
		
		BancoImpl buscar = (BancoImpl) buscar(BancoImpl.class);
		agenciaBancariaVO.setChaveBanco(String.valueOf(buscar.getChavePrimaria()));
		
		controladorAgenciaBancaria.inserirAgenciaBancaria(agenciaBancariaVO);
		super.getSesstion().evict(agenciaBancariaVO);

		int qtdFim = controladorAgenciaBancaria.consultarAgenciaBancaria(filtro).size();
		
		Assert.assertEquals(qtdInicio + 1, qtdFim);

	}

}
