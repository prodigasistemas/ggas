package br.com.ggas.arrecadacao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;

import br.com.ggas.arrecadacao.batch.GerarMovimentoCobrancaBatch;
import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.parametrosistema.ParametroSistema;

public class TesteGerarMovimentoCobrancaBatch extends GGASTestCase {

	private static final String ARQUIVO_PREPARAR_CENARIO = "/sql/SETUP_DEPENDENCIAS_TESTE_GERAR_MOVIMENTO_COBRANCA.sql";
	private static final String ARQUIVO_LIMPAR_CENARIO = "/sql/SETUP_INTEGRIDADE_TESTE_GERAR_MOVIMENTO_COBRANCA.sql";
	private static final String DIRETORIO_ARQUIVOS_REMESSA = "DIRETORIO_ARQUIVOS_REMESSA";

	private static final String DIRETORIO = System.getProperty("user.home") + File.separator + "TesteGerarMovimentoCobrancaBatch";

	@Before
	@Transactional
	public void setup() throws GGASException, IOException {
		prepararCenario(ARQUIVO_PREPARAR_CENARIO);
		alterarParametroDiretorio();
	}

	@After
	public void limpar() {
		limparCenario(ARQUIVO_LIMPAR_CENARIO);
		FileSystemUtils.deleteRecursively(new File(DIRETORIO));
	}

	@Transactional
	@Test
	public void testeGerarMovimentoCobrancaBatch() throws GGASException, IOException {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("processo", new ProcessoImpl());

		GerarMovimentoCobrancaBatch batch = new GerarMovimentoCobrancaBatch();

		String logProcessamento = batch.processar(parametros);

		assertNotNull(logProcessamento);
		assertTrue(logProcessamento.contains("Foram gerados 1 arquivos."));
	}
	
	private void alterarParametroDiretorio() throws GGASException {
		ParametroSistema parametro = fachada.obterParametroPorCodigo(DIRETORIO_ARQUIVOS_REMESSA);
		parametro.setValor(DIRETORIO);
		super.salvar(parametro);
	}
}
