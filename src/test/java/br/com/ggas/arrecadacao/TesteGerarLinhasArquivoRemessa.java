package br.com.ggas.arrecadacao;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.arrecadacao.documento.ControladorDocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.ControladorDocumentoCobranca;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.ServiceLocator;

public class TesteGerarLinhasArquivoRemessa extends GGASTestCase {
	
	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_GERAR_MOVIMENTO_COBRANCA.sql";
	
	private ControladorDocumentoLayout controladorDocumentoLayout;
	private CobrancaBancariaMovimento cobrancaBancariaMovimento;
	private ArrecadadorContratoConvenio arrecadadorContratoConvenio;
	private EntidadeConteudo tipoConvenio;
	private ArrecadadorContrato arrecadadorContrato;
	private Arrecadador arrecadador;
	private Long nsaRemessaNovo;
	
	@Autowired
	@Qualifier(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO)
	ControladorArrecadacao controladorArrecadacao;
	
	@Autowired
	@Qualifier(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO)
	ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	@Autowired
	@Qualifier(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA)
	ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	@Qualifier(ControladorArrecadadorConvenio.BEAN_ID_CONTROLADOR_ARRECADADOR_CONVENIO)
	ControladorArrecadadorConvenio controladorArrecadadorConvenio;
	
	@Autowired
	@Qualifier(ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA)
	ControladorCobranca controladorCobranca;
	
	@Autowired
	@Qualifier(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA)
	ControladorFatura controladorFatura;
	
	@Autowired
	@Qualifier(ControladorDocumentoCobranca.BEAN_ID_CONTROLADOR_DOCUMENTO_COBRANCA)
	ControladorDocumentoCobranca controladorDocumentoCobranca;
	
	@Before
	@Transactional
	public void setup() throws GGASException, IOException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
		
		controladorDocumentoLayout = ServiceLocator.getInstancia().getControladorDocumentoLayout();
		
		cobrancaBancariaMovimento = getCobrancaBancariaMovimento();
		
		arrecadadorContratoConvenio = controladorArrecadacao.obterArrecadadorContratoConvenio(
				cobrancaBancariaMovimento.getArrecadadorContratoConvenio().getChavePrimaria());
		
		tipoConvenio = (EntidadeConteudo) controladorEntidadeConteudo.obter(
				arrecadadorContratoConvenio.getTipoConvenio().getChavePrimaria());
		
		arrecadadorContrato = controladorArrecadadorConvenio.obterArrecadadorContrato(
				arrecadadorContratoConvenio.getArrecadadorContrato().getChavePrimaria());
		
		arrecadador = controladorArrecadacao.obterArrecadador(arrecadadorContrato.getArrecadador().getChavePrimaria());
		
		nsaRemessaNovo = controladorArrecadacao.gerarNSAArrecadador(arrecadador.getChavePrimaria(), tipoConvenio.getChavePrimaria());
	}

	@Transactional
	@Test
	public void testeHeader() throws GGASException, IOException {
		String header = "01REMESSA01COBRANCA       37370059136001300391ALGAS - GAS DE ALAGOAS SA     033SANTANDER      "
				+ getData(Constantes.FORMATO_DATA_DDMMYY) + "0000000000000000                                          "
				+ "                                                                                                    "
				+ "                                                                                                    "
				+ "                                 000000001";
		
		assertEquals(header, gerarLinha(1, "cabecalhoRemessa"));
	}
	
	@Transactional
	@Test
	public void testeDetalhe() throws GGASException, IOException {
		String detalhe = "1026998348400013237370059136001300391                         00011665000000 402000000000000"
				+ "00000    0000005019999999   " + getData(Constantes.FORMATO_DATA_DDMMYY) + "00000000178910333737001N" 
				+ getData(Constantes.FORMATO_DATA_DDMMYY) + "000000000000000000000000000000000000000000000000000000000"
				+ "000000100026495731477SEVERINO DA SILVA                       RUA HELIO PRADINES, 1234              "
				+ "  PONTA VERDE 57035220MACEIO         AL                               I83      00 000002";
		
		assertEquals(detalhe, gerarLinha(2, "detalheRemessa"));
	}
	
	@Transactional
	@Test
	public void testeDetalheContratoNulo() throws GGASException, IOException {
		String detalhe = "1026998348400013237370059136001300391                         00011665000000 000000000000000"
				+ "00000    0000005019999999   " + getData(Constantes.FORMATO_DATA_DDMMYY) + "00000000178910333737001N" 
				+ getData(Constantes.FORMATO_DATA_DDMMYY) + "000000000000000000000000000000000000000000000000000000000"
				+ "000000100026495731477SEVERINO DA SILVA                       RUA HELIO PRADINES, 1234              "
				+ "  PONTA VERDE 57035220MACEIO         AL                               I83      00 000002";
		
		cobrancaBancariaMovimento.getDocumentoCobranca().getItens().iterator().next().getFaturaGeral()
				.getFaturaAtual().setContratoAtual(null);
		
		assertEquals(detalhe, gerarLinha(2, "detalheRemessa"));
	}
	
	@Transactional
	@Test
	public void testeDetalheEnderecoNulo() throws GGASException, IOException {
		String detalhe = "1026998348400013237370059136001300391                         00011665000000 402000000000000"
				+ "00000    0000005019999999   " + getData(Constantes.FORMATO_DATA_DDMMYY) + "00000000178910333737001N"
				+ getData(Constantes.FORMATO_DATA_DDMMYY) + "000000000000000000000000000000000000000000000000000000000"
				+ "000000100026495731477SEVERINO DA SILVA                                                             "
				+ "              00000000                                                I83      00 000002";
		
		cobrancaBancariaMovimento.getDocumentoCobranca().getCliente().getEnderecos().clear();
		
		assertEquals(detalhe, gerarLinha(2, "detalheRemessa"));
	}
	
	@Transactional
	@Test
	public void testeTrailler() throws GGASException, IOException {
		String trailer = "90000030000000017891000000000000000000000000000000000000000000000000000000000000000000000000"
				+ "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
				+ "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
				+ "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
				+ "00000000003";
		
		assertEquals(trailer, gerarLinha(3, "trilhaRemessa"));
	}

	@Transactional
	@Test
	public void testeDetalheCancelamento() throws Throwable {
		String detalhe = "1026998348400013237370059136001300391                         00011665000000 402000000000000"
				+ "00000    0000005029999999   " + getData(Constantes.FORMATO_DATA_DDMMYY) + "00000000178910333737001N" 
				+ getData(Constantes.FORMATO_DATA_DDMMYY) + "000000000000000000000000000000000000000000000000000000000"
				+ "000000100026495731477SEVERINO DA SILVA                       RUA HELIO PRADINES, 1234              "
				+ "  PONTA VERDE 57035220MACEIO         AL                               I83      00 000002";

		Fatura fatura = (Fatura) controladorFatura.obter(9999999l);

		long chave = controladorArrecadacao.inserirMovimentoDeCancelamento(fatura, null);

		cobrancaBancariaMovimento = controladorArrecadacao.obterCobrancaBancariaMovimento(chave);
		assertEquals(detalhe, gerarLinha(2, "detalheRemessa"));
	}
	
	@Transactional
	@Test
	public void testeDetalheAlteracaoVencimento() throws Throwable {
		String detalhe = "1026998348400013237370059136001300391                         00011665000000 402000000000000"
				+ "00000    0000005069999999   " + getData(Constantes.FORMATO_DATA_DDMMYY) + "00000000178910333737001N" 
				+ getData(Constantes.FORMATO_DATA_DDMMYY) + "000000000000000000000000000000000000000000000000000000000"
				+ "000000100026495731477SEVERINO DA SILVA                       RUA HELIO PRADINES, 1234              "
				+ "  PONTA VERDE 57035220MACEIO         AL                               I83      00 000002";
		
		Fatura fatura = (Fatura) controladorFatura.obter(9999999l);

		DocumentoCobranca documentoCobranca = (DocumentoCobranca) controladorDocumentoCobranca.obter(9999999l);

		long chave = controladorArrecadacao.inserirCobrancaBancariaMovimentoPorOcorrencia(
				fatura, null, Constantes.C_OCORRENCIA_ENVIO_ALTERACAO_VENCIMENTO_TITULO_SANTANDER, documentoCobranca);

		cobrancaBancariaMovimento = controladorArrecadacao.obterCobrancaBancariaMovimento(chave);

		assertEquals(detalhe, gerarLinha(2, "detalheRemessa"));
	}
	
	@Transactional
	@Test(expected = NegocioException.class)
	public void testeDetalheAlteracaoVencimentoDocumentoCobrancaNulo() throws Throwable {
		Fatura fatura = (Fatura) controladorFatura.obter(9999999l);
		
		controladorArrecadacao.inserirCobrancaBancariaMovimentoPorOcorrencia(
				fatura, null, Constantes.C_OCORRENCIA_ENVIO_ALTERACAO_VENCIMENTO_TITULO_SANTANDER, null);
	}
	
	@Transactional
	@Test(expected = NegocioException.class)
	public void testeDetalheAlteracaoVencimentoOcorrenciaNula() throws Throwable {
		Fatura fatura = (Fatura) controladorFatura.obter(9999999l);
		DocumentoCobranca documentoCobranca = (DocumentoCobranca) controladorDocumentoCobranca.obter(9999999l);
		
		controladorArrecadacao.inserirCobrancaBancariaMovimentoPorOcorrencia(fatura, null, "OCORRENCIA_NULA", documentoCobranca);
	}
	
	private CobrancaBancariaMovimento getCobrancaBancariaMovimento() throws NegocioException {
		String codigoCobrancaRegistrada = controladorConstanteSistema.obterValorConstanteSistemaPorCodigo(
				Constantes.C_TIPO_CONVENIO_COBRANCA_REGISTRADA);
		
		EntidadeConteudo cobrancaRegistrada = controladorEntidadeConteudo.obter(Long.valueOf(codigoCobrancaRegistrada));
		
		Collection<CobrancaBancariaMovimento> lista = controladorArrecadacao.listarCobrancaBancariaMovimento(cobrancaRegistrada);
		
		return ((Collection<CobrancaBancariaMovimento>) lista).iterator().next();
	}

	private static String getData(String format) {
		DateFormat df = new SimpleDateFormat(format);
		return df.format(new Date());
	}
	
	private String gerarLinha(int totalRegistros, String tipo) throws GGASException, IOException {
		DocumentoLayoutRegistro documentoLayoutRegistro = controladorArrecadacao.criarMapaDocumentoLayoutRegistro(
				arrecadadorContratoConvenio.getLeiaute().getRegistros()).get(tipo);
		
		Map<String, Object> dados = controladorDocumentoLayout.gerarDadosDocumentoRemessaCobranca(totalRegistros, 
				cobrancaBancariaMovimento.getDocumentoCobranca().getValorTotal(),
				cobrancaBancariaMovimento, 
				documentoLayoutRegistro, 
				ServiceLocator.getInstancia());

		dados.put("sequencialArquivo", nsaRemessaNovo);

		return controladorDocumentoLayout.gerarLinhaRemessa(dados, documentoLayoutRegistro);
	}
}
