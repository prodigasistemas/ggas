package br.com.ggas.arrecadacao.batch;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.arrecadacao.Arrecadador;
import br.com.ggas.arrecadacao.ArrecadadorContrato;
import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.ControladorArrecadadorConvenio;
import br.com.ggas.arrecadacao.DebitoAutomatico;
import br.com.ggas.arrecadacao.DebitoNaoProcessado;
import br.com.ggas.arrecadacao.documento.ControladorDocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayout;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutCampo;
import br.com.ggas.arrecadacao.documento.DocumentoLayoutRegistro;
import br.com.ggas.arrecadacao.documento.impl.DocumentoLayoutImpl;
import br.com.ggas.arrecadacao.impl.ArrecadadorContratoImpl;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.controleacesso.impl.OperacaoImpl;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.impl.EntidadeConteudoImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;

public class TesteProcessarArquivoRetornoBatch extends GGASTestCase {

	private static final String USER_HOME = System.getProperty("user.home");
	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_PROCESSAR_ARQUIVO_RETORNO_BATCH.sql";
	private static final String PROCESSO = "processo";
	private static final String ARRECADADOR_STRING = "COBRANCA";

	@Autowired
	@Qualifier(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA)
	ControladorParametroSistema controladorParametroSistema;

	@Autowired
	@Qualifier(ControladorArrecadadorConvenio.BEAN_ID_CONTROLADOR_ARRECADADOR_CONVENIO)
	ControladorArrecadadorConvenio controladorArrecadadorConvenio;

	@Autowired
	@Qualifier(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO)
	ControladorArrecadacao controladorArrecadacao;

	@Autowired
	@Qualifier(ControladorDocumentoLayout.BEAN_ID_CONTROLADOR_DOCUMENTO_LAYOUT)
	ControladorDocumentoLayout controladorDocumentoLayout;

	@Transactional
	@Before
	public void setup() throws FileNotFoundException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Test
	public void testProcessarCadastroDebitoAutomaticoComDebitoProcessado() throws Throwable {
		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		DocumentoLayout filtro = controladorDocumentoLayout.criarLeiauteFiltro();
		filtro.getRegistros().iterator().next().setIdentificadorRegistro("B");
		filtro.getConvenios().iterator().next().getArrecadadorContrato().getArrecadador().getBanco().setCodigoBanco("104");
		filtro.setTamanhoRegistro(150);
		DocumentoLayout cnabCaixa = controladorDocumentoLayout.obterDocumentoLayout(filtro);
		Collection<DocumentoLayoutRegistro> registros = cnabCaixa.getRegistros();
		DocumentoLayoutRegistro registro =
				(DocumentoLayoutRegistro) chamarMetodoPrivado(batch, "obterDocumentoLayoutRegistro", registros, "B");
		Map<EntidadeConteudo, DocumentoLayoutCampo> metodoArgumentos =
				(Map<EntidadeConteudo, DocumentoLayoutCampo>) chamarMetodoPrivado(batch, "construirMapaCampos", registro, null);
		Arrecadador arrecadador = controladorArrecadacao.obterArrecadador(999l);

		Processo processoCadastroDebito = (Processo) criarParametrosParaProcesso().get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();
		Collection<DebitoNaoProcessado> colecaoDebitoNaoProcessado = new ArrayList<>();
		String linha = "B3673                     0264161618        20181022000000000020000032645             "
				+ "                                           2000000041704584   11";
		DebitoAutomatico debitoAutomatico = (DebitoAutomatico) controladorArrecadacao.criarDebitoAutomatico();

		colecaoDebitoNaoProcessado = (Collection<DebitoNaoProcessado>) chamarMetodoPrivado(batch, "processarCadastroDebitoAutomatico",
				processoCadastroDebito, registro, colecaoDebitoNaoProcessado, 2, linha, logProcessamento, arrecadador, debitoAutomatico,
				criarParametrosParaProcesso(), metodoArgumentos);

		assertTrue(colecaoDebitoNaoProcessado.isEmpty());

	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Test
	public void testProcessarCadastroDebitoAutomaticoComDebitoNaoProcessado() throws Throwable {
		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		DocumentoLayout filtro = controladorDocumentoLayout.criarLeiauteFiltro();
		filtro.getRegistros().iterator().next().setIdentificadorRegistro("B");
		filtro.getConvenios().iterator().next().getArrecadadorContrato().getArrecadador().getBanco().setCodigoBanco("104");
		filtro.setTamanhoRegistro(150);

		DocumentoLayout cnabCaixa = controladorDocumentoLayout.obterDocumentoLayout(filtro);
		Collection<DocumentoLayoutRegistro> registros = cnabCaixa.getRegistros();

		DocumentoLayoutRegistro registro =
				(DocumentoLayoutRegistro) chamarMetodoPrivado(batch, "obterDocumentoLayoutRegistro", registros, "B");
		Map<EntidadeConteudo, DocumentoLayoutCampo> metodoArgumentos =
				(Map<EntidadeConteudo, DocumentoLayoutCampo>) chamarMetodoPrivado(batch, "construirMapaCampos", registro, null);
		Arrecadador arrecadador = controladorArrecadacao.obterArrecadador(999l);

		Processo processoCadastroDebito = (Processo) criarParametrosParaProcesso().get(PROCESSO);
		StringBuilder logProcessamento = new StringBuilder();
		Collection<DebitoNaoProcessado> colecaoDebitoNaoProcessado = new ArrayList<>();
		String linha = "B3683                     0264161618        20181022000000000020000032645"
				+ "                                                        2000000041704584   11";
		DebitoAutomatico debitoAutomatico = (DebitoAutomatico) controladorArrecadacao.criarDebitoAutomatico();

		colecaoDebitoNaoProcessado = (Collection<DebitoNaoProcessado>) chamarMetodoPrivado(batch, "processarCadastroDebitoAutomatico",
				processoCadastroDebito, registro, colecaoDebitoNaoProcessado, 2, linha, logProcessamento, arrecadador, debitoAutomatico,
				criarParametrosParaProcesso(), metodoArgumentos);

		assertFalse(colecaoDebitoNaoProcessado.isEmpty());

	}

	@Transactional
	@Test
	public void testeProcessarArquivoRetornoBatch() throws GGASException, IOException {
		criarOuApagarDiretorios(true);
		criarArquivoRetorno();
		alterarDiretorios();

		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		String logProcessamento = batch.processar(criarParametrosParaProcesso());
		
		criarOuApagarDiretorios(false);
		assertNotNull(logProcessamento);
	}

	@Transactional
	@Test(expected = GGASException.class)
	public void testeProcessarArquivoRetornoBatchComArquivoInvalido() throws GGASException, IOException {
		criarOuApagarDiretorios(true);
		criarAquivoRetornoInvalido();
		alterarDiretorios();

		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		batch.processar(criarParametrosParaProcesso());
	}
	
	@Transactional
	@Test
	public void testeProcessarArquivoRetornoBatchComCodigoConvenioNumerico() throws GGASException, IOException {
		criarOuApagarDiretorios(true);
		criarArquivoRetornoComCodigoConvenioNumerico();
		alterarDiretorios();

		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		String logProcessamento = batch.processar(criarParametrosParaProcesso());
		
		criarOuApagarDiretorios(false);
		assertNotNull(logProcessamento);
	}

	@Transactional
	@Test(expected = GGASException.class)
	public void testeProcessarArquivoRetornoBatchComCodigoConvenioInvalido() throws GGASException, IOException {
		criarOuApagarDiretorios(true);
		criarArquivoRetornoComCodigoConvenioInvalido();
		alterarDiretorios();

		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		String logProcessamento = batch.processar(criarParametrosParaProcesso());
		
		criarOuApagarDiretorios(false);
		assertNotNull(logProcessamento);
	}
	
	@Transactional
	@Test
	public void testeProcessarArquivoRetornoBatchComEntradaDeTituloRejeitada() throws GGASException, IOException {
		criarOuApagarDiretorios(true);
		criarArquivoRetornoComEntradaDeTituloRejeitada();
		alterarDiretorios();

		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		String logProcessamento = batch.processar(criarParametrosParaProcesso());
		
		criarOuApagarDiretorios(false);
		assertNotNull(logProcessamento);
	}
	
	@Transactional
	@Test
	public void testVerificarIgualdadeComChavePrimariaOuCodigoDoTipoArrecadacaoComIgualdadeDeChave() throws Throwable {
		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		DocumentoLayoutImpl documentoLayout = new DocumentoLayoutImpl();
		EntidadeConteudoImpl tipoArrecadacao = new EntidadeConteudoImpl();
		EntidadeConteudoImpl tipoArrecadacaoLeiaute = new EntidadeConteudoImpl();
		tipoArrecadacaoLeiaute.setChavePrimaria(1000l);
		documentoLayout.setFormaArrecadacao(tipoArrecadacaoLeiaute);
		tipoArrecadacao.setChavePrimaria(1000l);
		tipoArrecadacao.setCodigo("1001");
		boolean retornoIgualdadeDeChave = (boolean) chamarMetodoPrivado(batch,
				"verificarIgualdadeComChavePrimariaOuCodigoDoTipoArrecadacao", documentoLayout, tipoArrecadacao);
		assertTrue(retornoIgualdadeDeChave);
	}

	@Transactional
	@Test
	public void testVerificarIgualdadeComChavePrimariaOuCodigoDoTipoArrecadacaoComIgualdadeDeCodigo() throws Throwable {
		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		DocumentoLayoutImpl documentoLayout = new DocumentoLayoutImpl();
		EntidadeConteudoImpl tipoArrecadacao = new EntidadeConteudoImpl();
		EntidadeConteudoImpl tipoArrecadacaoLeiaute = new EntidadeConteudoImpl();
		tipoArrecadacaoLeiaute.setChavePrimaria(1001l);
		documentoLayout.setFormaArrecadacao(tipoArrecadacaoLeiaute);
		tipoArrecadacao.setChavePrimaria(1000l);
		tipoArrecadacao.setCodigo("1001");
		boolean retornoIgualdadeDeCodigo = (boolean) chamarMetodoPrivado(batch,
				"verificarIgualdadeComChavePrimariaOuCodigoDoTipoArrecadacao", documentoLayout, tipoArrecadacao);
		assertTrue(retornoIgualdadeDeCodigo);
	}

	@Transactional
	@Test
	public void testVerificarIgualdadeComChavePrimariaOuCodigoDoTipoArrecadacaoComIgualdadeDeChaveECodigo() throws Throwable {
		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		DocumentoLayoutImpl documentoLayout = new DocumentoLayoutImpl();
		EntidadeConteudoImpl tipoArrecadacao = new EntidadeConteudoImpl();
		EntidadeConteudoImpl tipoArrecadacaoLeiaute = new EntidadeConteudoImpl();
		tipoArrecadacaoLeiaute.setChavePrimaria(1000l);
		documentoLayout.setFormaArrecadacao(tipoArrecadacaoLeiaute);
		tipoArrecadacao.setChavePrimaria(1000l);
		tipoArrecadacao.setCodigo("1000");
		boolean retornoIgualdadeDeChaveECodigo = (boolean) chamarMetodoPrivado(batch,
				"verificarIgualdadeComChavePrimariaOuCodigoDoTipoArrecadacao", documentoLayout, tipoArrecadacao);
		assertTrue(retornoIgualdadeDeChaveECodigo);
	}

	@Transactional
	@Test
	public void testVerificarIgualdadeComChavePrimariaOuCodigoDoTipoArrecadacaoSemIgualdadeDeChaveECodigo() throws Throwable {
		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		DocumentoLayoutImpl documentoLayout = new DocumentoLayoutImpl();
		EntidadeConteudoImpl tipoArrecadacao = new EntidadeConteudoImpl();
		EntidadeConteudoImpl tipoArrecadacaoLeiaute = new EntidadeConteudoImpl();
		tipoArrecadacaoLeiaute.setChavePrimaria(1000l);
		documentoLayout.setFormaArrecadacao(tipoArrecadacaoLeiaute);
		tipoArrecadacao.setChavePrimaria(1001l);
		tipoArrecadacao.setCodigo("1002");
		boolean retornoSemIgualdadeDeChaveECodigo = (boolean) chamarMetodoPrivado(batch,
				"verificarIgualdadeComChavePrimariaOuCodigoDoTipoArrecadacao", documentoLayout, tipoArrecadacao);
		assertFalse(retornoSemIgualdadeDeChaveECodigo);
	}

	@Transactional
	@Test
	public void testObterTipoArrecacaoEntidadeConteudoPorCodigoConvenio() throws Throwable {
		String numeroContrato = "9999";
		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		Collection<ArrecadadorContrato> colecaoArrecadorContrato = controladorArrecadadorConvenio.listarArrecadadorContrato();
		ArrecadadorContratoImpl tipoArrecadacao = new ArrecadadorContratoImpl();
		for (ArrecadadorContrato contratoArrecadacao : colecaoArrecadorContrato) {
			if (numeroContrato.equals(contratoArrecadacao.getNumeroContrato())) {
				tipoArrecadacao = (ArrecadadorContratoImpl) contratoArrecadacao;
				break;
			}
		}
		EntidadeConteudo retornoTipoArrecadacaoPorCodigoConvenio =
				(EntidadeConteudo) chamarMetodoPrivado(batch, "obterTipoArrecacaoEntidadeConteudo", tipoArrecadacao.getNumeroContrato());
		assertNotNull(retornoTipoArrecadacaoPorCodigoConvenio);
	}

	@Transactional
	@Test
	public void testObterTipoArrecacaoEntidadeConteudoPorString() throws Throwable {
		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		EntidadeConteudo retornoTipoArrecadacaoPorString =
				(EntidadeConteudo) chamarMetodoPrivado(batch, "obterTipoArrecacaoEntidadeConteudo", ARRECADADOR_STRING);
		assertNotNull(retornoTipoArrecadacaoPorString);
	}

	@Transactional
	@Test
	public void testObterTipoArrecacaoEntidadeConteudoComStringNula() throws Throwable {
		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		EntidadeConteudo retornoTipoArrecadacaoPorString =
				(EntidadeConteudo) chamarMetodoPrivado(batch, "obterTipoArrecacaoEntidadeConteudo", "");
		assertNull(retornoTipoArrecadacaoPorString);
	}
	
	@Transactional
	@Test
	public void testObterTipoArrecacaoEntidadeConteudoComValorErrado() throws Throwable {
		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		EntidadeConteudo retornoTipoArrecadacaoPorString =
				(EntidadeConteudo) chamarMetodoPrivado(batch, "obterTipoArrecacaoEntidadeConteudo", "Arrecadação");
		assertNull(retornoTipoArrecadacaoPorString);
	}
	
	@Transactional
	@Test
	public void testProcessarArquivoRetornoComMovimentoOcorrenciaInvalido() throws IOException, GGASException {
		criarOuApagarDiretorios(true);
		criarArquivoRetornoComMovimentoOcorrenciaInvalido();
		alterarDiretorios();

		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		String logProcessamento = batch.processar(criarParametrosParaProcesso());
		
		criarOuApagarDiretorios(false);
		assertNotNull(logProcessamento);
		
	}
	
	@Transactional
	@Test
	public void testProcessarArquivoRetornoAtualizandoCobrancaMovimento() throws IOException, GGASException {
		criarOuApagarDiretorios(true);
		criarArquivoRetornoComCobrancaMovimento();
		alterarDiretorios();

		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		String logProcessamento = batch.processar(criarParametrosParaProcesso());
		
		criarOuApagarDiretorios(false);
		assertNotNull(logProcessamento);
	}
	
	@Transactional
	@Test
	public void testProcessarArquivoRetornoAtualizandoCobrancaMovimentoAtualizada() throws IOException, GGASException {
		criarOuApagarDiretorios(true);
		criarArquivoRetornoComCobrancaMovimentoAtualizada();
		alterarDiretorios();

		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		String logProcessamento = batch.processar(criarParametrosParaProcesso());
		
		criarOuApagarDiretorios(false);
		assertNotNull(logProcessamento);
	}
	
	@Transactional
	@Test
	public void testeProcessarArquivoRetornoBatchSemArquivo() throws GGASException, IOException {
		criarOuApagarDiretorios(true);
		alterarDiretorios();

		ProcessarArquivoRetornoBatch batch = new ProcessarArquivoRetornoBatch();
		String logProcessamento = batch.processar(criarParametrosParaProcesso());
		
		criarOuApagarDiretorios(false);
		assertNotNull(logProcessamento);
	}


	private Map<String, Object> criarParametrosParaProcesso() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		ProcessoImpl processo = new ProcessoImpl();
		OperacaoImpl operacao = new OperacaoImpl();
		processo.setChavePrimaria(999998L);
		processo.setOperacao(operacao);
		parametros.put("processo", processo);
		return parametros;
	}

	private void criarOuApagarDiretorios(boolean criaDiretorio) throws IOException {
		if (criaDiretorio) {
			new File(USER_HOME + File.separator + "retorno").mkdirs();
			new File(USER_HOME + File.separator + "pdf").mkdirs();
			new File(USER_HOME + File.separator + "upload").mkdirs();
		} else {
			new File(USER_HOME + File.separator + "retorno").delete();
			new File(USER_HOME + File.separator + "pdf").delete();
			new File(USER_HOME + File.separator + "pdf").delete();
		}
	}

	private void alterarDiretorios() throws NegocioException, ConcorrenciaException {
		ParametroSistema parametroSistema = controladorParametroSistema.obterParametroPorCodigo("DIRETORIO_ARQUIVOS_UPLOAD");
		parametroSistema.setValor(USER_HOME + File.separator + "upload");
		controladorParametroSistema.atualizar(parametroSistema);

		parametroSistema = controladorParametroSistema.obterParametroPorCodigo("DIRETORIO_ARQUIVOS_RETORNO");
		parametroSistema.setValor(USER_HOME + File.separator + "retorno");
		controladorParametroSistema.atualizar(parametroSistema);

		parametroSistema = controladorParametroSistema.obterParametroPorCodigo("DIRETORIO_RELATORIO_PDF");
		parametroSistema.setValor(USER_HOME + File.separator + "pdf" + File.separator);
		controladorParametroSistema.atualizar(parametroSistema);

	}

	//Criacao de arquivos retorno para teste
	public void criarArquivoRetorno() throws IOException {
		FileWriter writer = new FileWriter(USER_HOME + File.separator + "upload" + File.separator + "arquivoRetornoTeste.ret");
		writer.write(
				"02RETORNO01COBRANCA       37371300391813003918GAS DE ALAGOAS S A ALGAS      033SANTANDER      02011900000000000591360                                                                                                                                                                                                                                                                                  010000001\n");
		writer.write(
				"10269983484000132373713003918130039181172761#1074002#1535     20000189                                     50602011900009999992000018900          02011900000016140210330000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000161402100000000000000000000000000 N 030119CDG COMBUSTIVEIS LTDA                00000000000000000000000000000000001614021C           010000002\n");
		writer.close();
	}
	
	public void criarArquivoRetornoComEntradaDeTituloRejeitada() throws IOException {
		FileWriter writer = new FileWriter(USER_HOME + File.separator + "upload" + File.separator + "arquivoRetornoTeste.ret");
		writer.write(
				"02RETORNO01COBRANCA       37371300391813003918GAS DE ALAGOAS S A ALGAS      033SANTANDER      02011900000000000591360                                                                                                                                                                                                                                                                                  010000001\n");
		writer.write(
				"10269983484000132373713003918130039180#0#7216                 20000138                                      0327121800000000132000013802051051051 01010100000000000000330000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 N 271218                                     00000000000000000000000000000000000000000C           009000002\n");
		writer.close();
	}
	
	public void criarArquivoRetornoComCodigoConvenioNumerico() throws IOException {
		FileWriter writer = new FileWriter(USER_HOME + File.separator + "upload" + File.separator + "arquivoRetornoTeste.ret");
		writer.write(
				"02RETORNO0112345          37371300391813003918GAS DE ALAGOAS S A ALGAS      033SANTANDER      02011900000000000591360                                                                                                                                                                                                                                                                                  010000001\n");
		writer.write(
				"10269983484000132373713003918130039181172761#1074002#1535     20000189                                     50602011918001099112000018900          02011900000016140210330000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000161402100000000000000000000000000 N 030119CDG COMBUSTIVEIS LTDA                00000000000000000000000000000000001614021C           010000002\n");
		writer.close();
	}
	
	public void criarArquivoRetornoComCodigoConvenioInvalido() throws IOException {
		FileWriter writer = new FileWriter(USER_HOME + File.separator + "upload" + File.separator + "arquivoRetornoTeste.ret");
		writer.write(
				"02RETORNO01323456         37371300391813003918GAS DE ALAGOAS S A ALGAS      033SANTANDER      02011900000000000591360                                                                                                                                                                                                                                                                                  010000001\n");
		writer.write(
				"10269983484000132373713003918130039181172761#1074002#1535     20000189                                     50602011918001099112000018900          02011900000016140210330000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000161402100000000000000000000000000 N 030119CDG COMBUSTIVEIS LTDA                00000000000000000000000000000000001614021C           010000002\n");
		writer.close();
	}

	private void criarAquivoRetornoInvalido() throws IOException {
		FileWriter writer = new FileWriter(USER_HOME + File.separator + "upload" + File.separator + "arquivoRetornoTeste.ret");
		writer.write("Invalido");
		writer.close();
	}
	
	public void criarArquivoRetornoComMovimentoOcorrenciaInvalido() throws IOException {
		FileWriter writer = new FileWriter(USER_HOME + File.separator + "upload" + File.separator + "arquivoRetornoTeste.ret");
		writer.write(
				"02RETORNO01COBRANCA       37371300391813003918GAS DE ALAGOAS S A ALGAS      033SANTANDER      02011900000000000591360                                                                                                                                                                                                                                                                                  010000001\n");
		writer.write(
				"10269983484000132373713003918130039181172761#1074002#1535     20000189                                     51602011918001099112000018900          02011900000016140210330000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000161402100000000000000000000000000 N 030119CDG COMBUSTIVEIS LTDA                00000000000000000000000000000000001614021C           010000002\n");
		writer.close();
	}
	
	public void criarArquivoRetornoComCobrancaMovimento() throws IOException {
		FileWriter writer = new FileWriter(USER_HOME + File.separator + "upload" + File.separator + "arquivoRetornoTeste.ret");
		writer.write(
				"02RETORNO01COBRANCA       37371300391813003918GAS DE ALAGOAS S A ALGAS      033SANTANDER      02011900000000000591360                                                                                                                                                                                                                                                                                  010000001\n");
		writer.write(
				"10269983484000132373713003918130039181172761#1074002#1535     20000189                                     50602011900009999982000018900          02011900000016140210330000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000161402100000000000000000000000000 N 030119CDG COMBUSTIVEIS LTDA                00000000000000000000000000000000001614021C           010000002\n");
		writer.close();
	}
	
	public void criarArquivoRetornoComCobrancaMovimentoAtualizada() throws IOException {
		FileWriter writer = new FileWriter(USER_HOME + File.separator + "upload" + File.separator + "arquivoRetornoTeste.ret");
		writer.write(
				"02RETORNO01COBRANCA       37371300391813003918GAS DE ALAGOAS S A ALGAS      033SANTANDER      02011900000000000591360                                                                                                                                                                                                                                                                                  010000001\n");
		writer.write(
				"10269983484000132373713003918130039181172761#1074002#1535     20000189                                     50602011900009999972000018900          02011900000016140210330000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000161402100000000000000000000000000 N 030119CDG COMBUSTIVEIS LTDA                00000000000000000000000000000000001614021C           010000002\n");
		writer.close();
	}

}
