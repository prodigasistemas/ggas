package br.com.ggas.arrecadacao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.Util;

/**
 * 
 * @author voliveira
 *
 */
public class TesteBanco extends GGASTestCase {

	private static final String NOME_INSERIDO = "BANCO NOVO";
	
	private static final String CODIGO_BANCO_INSERIDO = "11AA22BB";
	
	private static final String NOME_ABREVIADO_INSERIDO = "BNOV";
	
	private static final byte[] LOGO_BANCO_INSERIDO = new byte[] {};
	
	private static final String NOME_BANCO_ATUALIZADO = "NOVO NOME BANCO";
	
	private static final String NOME_ABREVIADO_ATUALIZADO = "NNBN";
	
	private static final String CODIGO_BANCO_ATUALIZADO = "33CC44DD";
	
	@Autowired
	private ControladorBanco controladorBanco;
	
	@Before
	public void setUp() throws GGASException {

		cargaInicial.carregar();
		
	}
	
	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a consulta da entidade por chave
	// -----------------------------------------------------------------------------------------------------
	
	@Test
	@Transactional
	public void testeBuscarEntidadePorChaveSucesso() throws GGASException {

		long chave = 1L;
		Banco entidade = fachada.obterBanco(chave);
		Assert.assertEquals(chave, entidade.getChavePrimaria());
	}

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeBuscarEntidadePorChaveFalha() throws GGASException {

		fachada.obterBanco(-1L);
	}
	
	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a a busca por filtros de uma entidade
	// -----------------------------------------------------------------------------------------------------

	@Test
	@Transactional
	public void testeConsultarListaEntidadeVazia() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(Banco.ATTR_CODIGO_BANCO, CODIGO_BANCO_ATUALIZADO);
		Collection<Banco> entidades = fachada.consultarBanco(filtro);
		Assert.assertTrue(entidades.isEmpty());
	}
	
	@Test
	@Transactional
	public void testeConsultarEntidade() throws GGASException {

		this.inserirEntidade();
		
		Map<String, Object> filtro = montarFiltro();
		Banco entidade = Util.primeiroElemento(fachada.consultarBanco(filtro));

		Assert.assertNotNull(entidade);
		Assert.assertEquals(NOME_INSERIDO, entidade.getNome());
		Assert.assertEquals(CODIGO_BANCO_INSERIDO, entidade.getCodigoBanco());
		Assert.assertEquals(NOME_ABREVIADO_INSERIDO, entidade.getNomeAbreviado());
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a inclusão da entidade
	// -----------------------------------------------------------------------------------------------------

	// Para enxergar os fluxos de exceção do inserir, 
	// ver o método preInsercao do controlador da entidade
	// Para enxergar os campos obrigatórios, ver o método 
	// validarDados da entidade


	@Test(expected = NegocioException.class)
	@Transactional
	public void testeInserirEntidadeCamposObrigatorios() throws GGASException {

		Banco entidade = montarEntidade();
		entidade.setNome(null);
		entidade.setCodigoBanco(null);
		entidade.setNomeAbreviado(null);
		fachada.inserirBanco(entidade);
	}
	
	@Test
	@Transactional
	public void testeInserirEntidadeSucesso() throws GGASException {

		int qtdInicio = fachada.consultarBanco(null).size();

		Banco entidade = montarEntidade();
		// Alteração realizada para não dar erro no método 
		// que valida um matricula existente
//		entidade.setMatricula("721"); 
		fachada.inserirBanco(entidade);

		int qtdFim = fachada.consultarBanco(null).size();
		
		Assert.assertEquals(qtdInicio + 1, qtdFim);
		
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a alteração da entidade
	// -----------------------------------------------------------------------------------------------------
	
	@Test
	@Transactional
	public void testeAtualizarEntidadeSucesso() throws GGASException {

		Banco entidade = fachada.obterBanco(1L);
		
		String nome = entidade.getNome();
		String codigoBanco = entidade.getCodigoBanco();
		String nomeAbreviado = entidade.getNomeAbreviado();

		entidade.setNome(NOME_BANCO_ATUALIZADO);
		entidade.setCodigoBanco(CODIGO_BANCO_ATUALIZADO);
		entidade.setNomeAbreviado(NOME_ABREVIADO_ATUALIZADO);
		
		fachada.atualizarBanco(entidade);

		Banco entidadeAlterada = fachada.obterBanco(1L);

		Assert.assertNotEquals(nome, entidadeAlterada.getNome());
		Assert.assertNotEquals(codigoBanco, entidadeAlterada.getCodigoBanco());
		Assert.assertNotEquals(nomeAbreviado, entidadeAlterada.getNomeAbreviado());
	}

	// -----------------------------------------------------------------------------------------------------
	// Métodos para testar a remoção da entidade
	// -----------------------------------------------------------------------------------------------------
	
	@Test
	@Transactional
	public void testeRemoverEntidade() throws GGASException {

		this.inserirEntidade();
		
		Map<String, Object> filtro = montarFiltro();
		Banco entidade = Util.primeiroElemento(fachada.consultarBanco(filtro));

		int qtdInicio = fachada.consultarBanco(null).size();
		controladorBanco.remover(entidade);
		int qtdFim = fachada.consultarBanco(null).size();

		Assert.assertEquals(qtdInicio - 1, qtdFim);
	}
	
	// -----------------------------------------------------------------------------------------------------
	// Métodos privados utilitários para esta classe
	// -----------------------------------------------------------------------------------------------------

	private void inserirEntidade() throws GGASException {

		super.salvar(montarEntidade());
	}

	private Banco montarEntidade() throws GGASException {

		Banco banco = fachada.criarBanco();
		banco.setNome(NOME_INSERIDO);
		banco.setCodigoBanco(CODIGO_BANCO_INSERIDO);
		banco.setNomeAbreviado(NOME_ABREVIADO_INSERIDO);
		banco.setLogoBanco(LOGO_BANCO_INSERIDO);
		return banco;
		
	}

	private Map<String, Object> montarFiltro() {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put(Banco.ATTR_NOME, NOME_INSERIDO);
		filtro.put(Banco.ATTR_CODIGO_BANCO, CODIGO_BANCO_INSERIDO);
		filtro.put(Banco.ATTR_NOME_ABREVIADO, NOME_ABREVIADO_INSERIDO);
		filtro.put(Banco.ATTR_HABILITADO, true);
		return filtro;
	}

}
