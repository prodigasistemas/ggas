package br.com.ggas.comum;

import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import br.com.ggas.arrecadacao.Agencia;
import br.com.ggas.arrecadacao.agenciabancaria.dominio.AgenciaBancariaVO;
import br.com.ggas.arrecadacao.impl.AgenciaImpl;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.Util;

public class TesteUtil {
	
	private static final String NOME_INSERIDO = "NOME ENTIDADE NOVA";
	private static final String CODIGO_INSERIDO = "11AA";
	private static final DateTime[] DATAS = { new DateTime(2018, 12, 31, 0, 0, 0, 0), 
			new DateTime(2019, 1, 15, 0, 0, 0, 0), 
			new DateTime(2019, 1, 30, 0, 0, 0, 0), 
			new DateTime(2019, 1, 31, 0, 0, 0, 0), 
			new DateTime(2019, 2, 14, 0, 0, 0, 0), 
			new DateTime(2019, 2, 28, 0, 0, 0, 0), 
			new DateTime(2019, 3, 15, 0, 0, 0, 0), 
			new DateTime(2019, 3, 30, 0, 0, 0, 0) };
	private static final int[] CICLOS = { 1, 2, 3, 1, 2, 1, 2 };
	
	
	@Test
	public void testeCopyProperties(){
		
		AgenciaBancariaVO agenciaBancariaVO = new AgenciaBancariaVO();
		agenciaBancariaVO.setNome(NOME_INSERIDO);
		agenciaBancariaVO.setCodigo(CODIGO_INSERIDO);
		
		Agencia agencia = new AgenciaImpl();
		Util.copyProperties(agencia, agenciaBancariaVO);
		
		Assert.assertEquals(agencia.getNome(), agenciaBancariaVO.getNome());
		
		AgenciaBancariaVO vo2 = new AgenciaBancariaVO();
		Util.copyProperties(vo2, agenciaBancariaVO);
		
		Assert.assertEquals( vo2.getNome(), agenciaBancariaVO.getNome());
	}
	
	@Test
	public void testeRolarReferenciaCicloSemanalSemViradaDeMes() {
		Integer referencia = 201811;
		int cicloInicial = 2;
		DateTime hoje = new DateTime(2018, 11, 14, 0, 0, 0, 0);
		Map<String, Integer> retorno = Util.gerarProximaReferenciaCiclo(referencia, cicloInicial, 7, hoje);
		Assert.assertEquals(referencia, retorno.get(Constantes.REFERENCIA));
		Assert.assertEquals(cicloInicial + 1, retorno.get(Constantes.CICLO).intValue());
	}

	@Test
	public void testeRolarReferenciaCicloSemanalComViradaDeMes() {
		Integer referencia = 201811;
		int cicloInicial = 4;
		DateTime hoje = new DateTime(2018, 11, 29, 0, 0, 0, 0);
		Map<String, Integer> retorno = Util.gerarProximaReferenciaCiclo(referencia, cicloInicial, 7, hoje);
		Assert.assertEquals(referencia, retorno.get(Constantes.REFERENCIA));
		Assert.assertEquals(cicloInicial + 1, retorno.get(Constantes.CICLO).intValue());
	}

	@Test
	public void testeRolarReferenciaCicloSemanalComViradaDeMesNoUltimoDiaDoMes() {
		Integer referencia = 201811;
		int cicloInicial = 5;
		DateTime hoje = new DateTime(2018, 11, 30, 0, 0, 0, 0);
		Map<String, Integer> retorno = Util.gerarProximaReferenciaCiclo(referencia, cicloInicial, 7, hoje);
		Assert.assertEquals(201812, retorno.get(Constantes.REFERENCIA).intValue());
		Assert.assertEquals(1, retorno.get(Constantes.CICLO).intValue());
	}
	
	@Test
	public void testeRolarReferenciaCicloQuinzenalComViradaDeMesNoUltimoDiaDoMes() {
		Integer referencia = 201904;
		int cicloInicial = 1;
		DateTime hoje = new DateTime(2019, 4, 16, 0, 0, 0, 0);
		Map<String, Integer> retorno = Util.gerarProximaReferenciaCiclo(referencia, cicloInicial, 15, hoje);
		Assert.assertEquals(201904, retorno.get(Constantes.REFERENCIA).intValue());
		Assert.assertEquals(2, retorno.get(Constantes.CICLO).intValue());
		
		cicloInicial = 1;
		hoje = new DateTime(2019, 4, 16, 0, 0, 0, 0);
		retorno = Util.gerarProximaReferenciaCiclo(referencia, cicloInicial, 10, hoje);
		Assert.assertEquals(201904, retorno.get(Constantes.REFERENCIA).intValue());
		Assert.assertEquals(2, retorno.get(Constantes.CICLO).intValue());
		
		cicloInicial = 2;
		hoje = new DateTime(2019, 4, 26, 0, 0, 0, 0);
		retorno = Util.gerarProximaReferenciaCiclo(referencia, cicloInicial, 15, hoje);
		Assert.assertEquals(201904, retorno.get(Constantes.REFERENCIA).intValue());
		Assert.assertEquals(3, retorno.get(Constantes.CICLO).intValue());
	}
	
	@Test
	public void testeFormatarNomeProprio() {
		Assert.assertEquals("Rio de Janeiro", Util.formatarNomeProprio("RIO DE JANEIRO"));
	}

	@Test
	public void testeRolarReferenciaCicloQuinzenalNVezes() {
		final int quinzena = 15;
		Integer referencia = 201812;
		int ciclo = 3;
		Map<String, Integer> retorno = null;
		for (int i = 0; i < DATAS.length - 1; i++) {
			DateTime hoje = DATAS[i];
			retorno = Util.gerarProximaReferenciaCiclo(referencia, ciclo, quinzena, hoje);
			ciclo = retorno.get(Constantes.CICLO).intValue();
			if (ciclo == 1)
				referencia = Util.adicionarMesEmAnoMes(referencia);
			Assert.assertEquals(Util.converterDataParaReferenciaAnoMes(DATAS[i + 1]), 
					retorno.get(Constantes.REFERENCIA).intValue());
			Assert.assertEquals(CICLOS[i], retorno.get(Constantes.CICLO).intValue());
		}
	}
	
	@Test
	public void testeRolarReferenciaCicloMensal() {
		Integer referencia = 201812;
		int cicloInicial = 1;
		DateTime hoje = new DateTime(2018, 12, 30, 0, 0, 0, 0);
		Map<String, Integer> retorno = Util.gerarProximaReferenciaCiclo(referencia, cicloInicial, 30, hoje);
		Assert.assertEquals(201901, retorno.get(Constantes.REFERENCIA).intValue());
		Assert.assertEquals(1, retorno.get(Constantes.CICLO).intValue());
	}
	
	@Test
	public void testeRolarReferenciaCicloMensalNVezes() {
		int n = 15;
		Integer referencia = 201812;
		int cicloInicial = 1;
		DateTime hoje = new DateTime(2018, 12, 31, 0, 0, 0, 0);
		for (int i = 0; i < n; i++) {
			Map<String, Integer> retorno = Util.gerarProximaReferenciaCiclo(referencia, cicloInicial, 30, hoje);
			hoje = hoje.plusMonths(1);
			Assert.assertEquals(Util.converterDataParaReferenciaAnoMes(hoje), retorno.get(Constantes.REFERENCIA).intValue());
			Assert.assertEquals(1, retorno.get(Constantes.CICLO).intValue());
			referencia = Util.adicionarMesEmAnoMes(referencia);
		}
	}
	
}
