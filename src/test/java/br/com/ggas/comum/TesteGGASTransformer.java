package br.com.ggas.comum;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Map;

import org.hibernate.Query;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeClasse;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.util.GGASTransformer;
import br.com.ggas.util.Util;

public class TesteGGASTransformer extends GGASTestCase {

	private static final String PARAMETRO_CODIGO = "codigo";

	private static final String CODIGO_ENTIDADE_CONTEUDO = "EC_TESTE";

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	private ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Test
	@Transactional
	public void testeConsultarAtributosDeEntidadeConteudo() {

		EntidadeConteudo entidadeConteudo = this.inserirEntidadeConteudo();

		StringBuilder campos = new StringBuilder();

		campos.append(" entidadeConteudo.codigo as codigo, ");
		campos.append(" entidadeConteudo.descricao as descricao ");

		Query query = super.getSesstion().createQuery(this.criarConsultaEntidadeConteudo(campos.toString()));

		query.setString(PARAMETRO_CODIGO, CODIGO_ENTIDADE_CONTEUDO);

		query.setResultTransformer(
				new GGASTransformer(controladorEntidadeConteudo.getClasseEntidade(), super.sessionFactory.getAllClassMetadata()));

		Collection<EntidadeConteudo> entidades = query.list();

		assertTrue(entidades.size() == 1);

		EntidadeConteudo entidadeConteudoObtida = Util.primeiroElemento(entidades);

		assertEquals(entidadeConteudo.getCodigo(), entidadeConteudoObtida.getCodigo());
		assertEquals(entidadeConteudo.getDescricao(), entidadeConteudoObtida.getDescricao());
		assertNull(entidadeConteudoObtida.getEntidadeClasse());
	}

	@Test(expected = IllegalStateException.class)
	@Transactional
	public void testeConsultarAtributosDeEntidadeConteudoSemMetadados() {

		this.inserirEntidadeConteudo();

		StringBuilder campos = new StringBuilder();

		campos.append(" entidadeConteudo.codigo as codigo, ");
		campos.append(" entidadeConteudo.descricao as descricao ");

		Query query = super.getSesstion().createQuery(this.criarConsultaEntidadeConteudo(campos.toString()));

		query.setString(PARAMETRO_CODIGO, CODIGO_ENTIDADE_CONTEUDO);

		query.setResultTransformer(new GGASTransformer(controladorEntidadeConteudo.getClasseEntidade()));

		query.list();

	}

	@Test
	@Transactional
	public void testeConsultarEntidadeConteudo() {

		EntidadeConteudo entidadeConteudo = this.inserirEntidadeConteudo();

		StringBuilder campos = new StringBuilder();

		campos.append(" entidadeConteudo ");

		Query query = super.getSesstion().createQuery(this.criarConsultaEntidadeConteudo(campos.toString()));

		query.setString(PARAMETRO_CODIGO, CODIGO_ENTIDADE_CONTEUDO);

		query.setResultTransformer(new GGASTransformer(controladorEntidadeConteudo.getClasseEntidade()));

		EntidadeConteudo entidadeConteudoObtida = (EntidadeConteudo) query.uniqueResult();

		assertEquals(entidadeConteudo.getCodigo(), entidadeConteudoObtida.getCodigo());
		assertEquals(entidadeConteudo.getDescricao(), entidadeConteudoObtida.getDescricao());
		assertNotNull(entidadeConteudoObtida.getEntidadeClasse());
	}

	@Test
	@Transactional
	public void testeConsultarAtributosDeEntidadeConteudoPorEntidadeClasse() {

		EntidadeConteudo entidadeConteudo = this.inserirEntidadeConteudo();
		EntidadeClasse entidadeClasse = entidadeConteudo.getEntidadeClasse();

		String aliasCampoChave = "entidadeClasse_chavePrimaria";

		StringBuilder campos = new StringBuilder();

		campos.append("entidadeClasse.chavePrimaria as ");
		campos.append(aliasCampoChave);
		campos.append(", ");
		campos.append(" entidadeConteudo.codigo as codigo, ");
		campos.append(" entidadeConteudo.descricao as descricao, ");
		campos.append(" entidadeClasse.descricao as entidadeClasse_descricao ");

		Query query = super.getSesstion().createQuery(this.criarConsultaEntidadeConteudo(campos.toString()));

		query.setString(PARAMETRO_CODIGO, CODIGO_ENTIDADE_CONTEUDO);

		query.setResultTransformer(new GGASTransformer(controladorEntidadeConteudo.getClasseEntidade(),
				super.sessionFactory.getAllClassMetadata(), aliasCampoChave));

		Map<EntidadeClasse, Collection<EntidadeConteudo>> entidadesPorClasse =
				(Map<EntidadeClasse, Collection<EntidadeConteudo>>) query.uniqueResult();

		assertTrue(entidadesPorClasse.size() == 1);

		EntidadeConteudo entidadeConteudoConsultada = Util.primeiroElemento(entidadesPorClasse.get(entidadeClasse.getChavePrimaria()));
		EntidadeClasse entidadeClasseConsultada = entidadeConteudoConsultada.getEntidadeClasse();

		assertEquals(entidadeConteudo.getCodigo(), entidadeConteudoConsultada.getCodigo());
		assertEquals(entidadeConteudo.getDescricao(), entidadeConteudoConsultada.getDescricao());
		assertEquals(entidadeClasse.getDescricao(), entidadeClasseConsultada.getDescricao());
	}

	@Test
	@Transactional
	public void testeConsultarEntidadeConteudoPorEntidadeClasse() {

		EntidadeConteudo entidadeConteudo = this.inserirEntidadeConteudo();
		EntidadeClasse entidadeClasse = entidadeConteudo.getEntidadeClasse();

		String aliasCampoChave = "entidadeClasse_chavePrimaria";
		StringBuilder campos = new StringBuilder();

		campos.append("entidadeClasse.chavePrimaria as ");
		campos.append(aliasCampoChave);
		campos.append(", ");
		campos.append(" entidadeConteudo ");

		Query query = super.getSesstion().createQuery(this.criarConsultaEntidadeConteudo(campos.toString()));

		query.setString(PARAMETRO_CODIGO, CODIGO_ENTIDADE_CONTEUDO);

		query.setResultTransformer(new GGASTransformer(controladorEntidadeConteudo.getClasseEntidade(), aliasCampoChave));

		Map<EntidadeClasse, Collection<EntidadeConteudo>> entidadesPorClasse =
				(Map<EntidadeClasse, Collection<EntidadeConteudo>>) query.uniqueResult();

		assertTrue(entidadesPorClasse.size() == 1);

		EntidadeConteudo entidadeConteudoConsultada = Util.primeiroElemento(entidadesPorClasse.get(entidadeClasse.getChavePrimaria()));
		EntidadeClasse entidadeClasseConsultada = entidadeConteudoConsultada.getEntidadeClasse();

		assertEquals(entidadeConteudo.getCodigo(), entidadeConteudoConsultada.getCodigo());
		assertEquals(entidadeConteudo.getDescricao(), entidadeConteudoConsultada.getDescricao());
		assertEquals(entidadeClasse, entidadeClasseConsultada);

	}

	@Test(expected = IllegalStateException.class)
	@Transactional
	public void testeConsultarAtributosDeEntidadeConteudoPorEntidadeClasseSemMetadados() {

		this.inserirEntidadeConteudo();

		String aliasCampoChave = "entidadeClasse_chavePrimaria";
		StringBuilder campos = new StringBuilder();

		campos.append("entidadeClasse.chavePrimaria as ");
		campos.append(aliasCampoChave);
		campos.append(", ");
		campos.append(" entidadeConteudo.codigo as codigo, ");
		campos.append(" entidadeConteudo.descricao as descricao ");

		Query query = super.getSesstion().createQuery(this.criarConsultaEntidadeConteudo(campos.toString()));

		query.setString(PARAMETRO_CODIGO, CODIGO_ENTIDADE_CONTEUDO);

		query.setResultTransformer(new GGASTransformer(controladorEntidadeConteudo.getClasseEntidade(), aliasCampoChave));

		query.uniqueResult();
	}

	@Test
	@Transactional
	public void testeConsultarUnicoAtributoDeEntidadeConteudoPorEntidadeClasse() {

		EntidadeConteudo entidadeConteudo = this.inserirEntidadeConteudo();
		EntidadeClasse entidadeClasse = entidadeConteudo.getEntidadeClasse();

		String aliasCampoChave = "entidadeClasse_chavePrimaria";

		StringBuilder campos = new StringBuilder();

		campos.append("entidadeClasse.chavePrimaria as ");
		campos.append(aliasCampoChave);
		campos.append(", ");
		campos.append(" entidadeConteudo.codigo as codigo ");

		Query query = super.getSesstion().createQuery(this.criarConsultaEntidadeConteudo(campos.toString()));

		query.setString(PARAMETRO_CODIGO, CODIGO_ENTIDADE_CONTEUDO);

		query.setResultTransformer(new GGASTransformer(controladorEntidadeConteudo.getClasseEntidade(),
				super.sessionFactory.getAllClassMetadata(), aliasCampoChave));

		Map<EntidadeClasse, Collection<EntidadeConteudo>> entidadesPorClasse =
				(Map<EntidadeClasse, Collection<EntidadeConteudo>>) query.uniqueResult();

		assertTrue(entidadesPorClasse.size() == 1);

		EntidadeConteudo entidadeConteudoConsultada = Util.primeiroElemento(entidadesPorClasse.get(entidadeClasse.getChavePrimaria()));

		assertEquals(entidadeConteudo.getCodigo(), entidadeConteudoConsultada.getCodigo());
		assertNull(entidadeConteudoConsultada.getDescricao());
		assertNull(entidadeConteudoConsultada.getEntidadeClasse());
	}

	private String criarConsultaEntidadeConteudo(String camposSelecionados) {

		StringBuilder hql = new StringBuilder();
		hql.append(" select ");
		hql.append(camposSelecionados);
		hql.append(" from ");
		hql.append(controladorEntidadeConteudo.getClasseEntidade().getSimpleName());
		hql.append(" entidadeConteudo ");
		hql.append(" where ");
		hql.append(" entidadeConteudo.codigo = :codigo ");

		return hql.toString();
	}

	private EntidadeConteudo inserirEntidadeConteudo() {
		EntidadeConteudo entidadeConteudo = criarEntidadeConteudo();
		super.salvar(entidadeConteudo);
		return entidadeConteudo;
	}

	private EntidadeConteudo criarEntidadeConteudo() {
		EntidadeConteudo entidadeConteudo = (EntidadeConteudo) controladorEntidadeConteudo.criar();
		entidadeConteudo.setDescricao("Entidade Conteudo Teste");
		entidadeConteudo.setCodigo(CODIGO_ENTIDADE_CONTEUDO);
		entidadeConteudo.setDescricaoAbreviada("ECT");
		entidadeConteudo.setIndicadorPadrao(Boolean.TRUE);
		entidadeConteudo.setHabilitado(Boolean.TRUE);
		entidadeConteudo.setUltimaAlteracao(Util.getDataCorrente(Boolean.FALSE));
		entidadeConteudo.setEntidadeClasse(inserirEntidadeClasse());
		return entidadeConteudo;
	}

	private EntidadeClasse inserirEntidadeClasse() {
		EntidadeClasse entidadeClasse = criarEntidadeClasse();
		super.salvar(entidadeClasse);
		return entidadeClasse;
	}

	private EntidadeClasse criarEntidadeClasse() {
		EntidadeClasse entidadeClasse = (EntidadeClasse) controladorEntidadeConteudo.criarEntidadeClasse();
		entidadeClasse.setDescricao("Entidade Classe Teste");
		entidadeClasse.setDescricaoAbreviada("ECT");
		entidadeClasse.setHabilitado(Boolean.TRUE);
		entidadeClasse.setUltimaAlteracao(Util.getDataCorrente(Boolean.FALSE));
		return entidadeClasse;
	}

}
