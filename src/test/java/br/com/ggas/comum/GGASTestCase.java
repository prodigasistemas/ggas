
package br.com.ggas.comum;

import br.com.ggas.geral.exception.InfraestruturaException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.util.DadosAuditoriaUtil;
import br.com.ggas.util.Fachada;
import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

/**
 * @author Roberto Alencar
 */
public abstract class GGASTestCase extends GGASTestSpring {

	/**
	 * Fachada do sistema
	 */
	protected Fachada fachada = Fachada.getInstancia();

	@Autowired
	private BasicDataSource dataSource;

	@Autowired
	protected SessionFactory sessionFactory;

	@Autowired(required = false)
	protected CargaInicial cargaInicial;

	public Session getSesstion() {

		return sessionFactory.getCurrentSession();
	}

	/**
	 * Salva uma entidade no banco de dados.
	 * 
	 * @author Arthur Carvalho
	 * @param entidadeNegocio
	 *            que será persistida no banco de dados
	 */
	public void salvar(EntidadeNegocio entidadeNegocio) {

		entidadeNegocio.setVersao(0);
		entidadeNegocio.setUltimaAlteracao(Calendar.getInstance().getTime());
		entidadeNegocio.setHabilitado(Boolean.TRUE);
		entidadeNegocio.setDadosAuditoria(DadosAuditoriaUtil.getDadosAuditoria());
		getSesstion().save(entidadeNegocio);
	}

	/**
	 * Salva uma lista de objetos no banco de dados.
	 * 
	 * @author Arthur Carvalho
	 * @param lista
	 *            de objetos que serão persistidos
	 */
	public void salvar(Collection<EntidadeNegocio> lista) {

		for (EntidadeNegocio obj : lista) {
			salvar(obj);
		}
	}

	/**
	 * Obtem um objeto existente no banco.
	 * 
	 * @author Arthur Carvalho
	 * @param obj
	 *            que será obtido
	 * @return objeto consultado no banco de dados
	 */
	public Object buscar(Class<?> obj) {

		List<EntidadeNegocio> lista = getSesstion().createCriteria(obj)
				.addOrder(Order.desc("chavePrimaria"))
				.setMaxResults(1)
				.list();
		
		EntidadeNegocio retorno = null;

		if (!lista.isEmpty()) {
			retorno = lista.iterator().next();
		}

		return retorno;
	}
	
	
	public boolean apagar(Object obj) {
		
		getSesstion().delete(obj);

		return true;
	}

	/**
	 * Executa um método privado de uma classe.
	 * 
	 * @author Roberto Alencar
	 * @param source
	 *            - instância da classe
	 * @param nomeMetodo
	 *            - nome do método privado
	 * @param parametros
	 *            - parâmetros do método privado
	 * @return retorno do método
	 * @throws Throwable
	 *             - exceção levantada pelo método privado
	 */
	protected Object chamarMetodoPrivado(Object source, String nomeMetodo, Object... parametros) throws Throwable {

		Object retorno = null;

		Method[] metodos = source.getClass().getDeclaredMethods();
		boolean metodoFoiChamado = false;
		for (Method metodo : metodos) {
			if (metodo.getName().equals(nomeMetodo) && metodo.getParameterTypes().length == parametros.length) {
				try {
					boolean match = true;
					match = compararParametros(metodo, parametros);
					if (match) {
						metodo.setAccessible(true);
						retorno = metodo.invoke(source, parametros);
						metodoFoiChamado = true;
					} else {
						throw new InfraestruturaException("Método privado não encontrado");
					}
				} catch (InvocationTargetException e) {
					throw e.getTargetException();
				}
			}
		}

		if (!metodoFoiChamado) {
			throw new InfraestruturaException("Método privado " + nomeMetodo + " não encontrado");
		}

		return retorno;
	}

	/**
	 * Compara os parâmetros do método verificando se condiz com os parâmetros informados.
	 * 
	 * @author Roberto Alencar
	 * @param metodo
	 *            the metodo
	 * @param parametros
	 *            - parâmetros do método privado
	 * @return <code>true</code> se os parâmetros do método chamado condizem com os parâmetros informados.
	 */
	private boolean compararParametros(Method metodo, Object... parametros) {

		int i = 0;
		boolean isMatch = true;
		for (Class<?> methodParameterType : metodo.getParameterTypes()) {
			Object parameter = parametros[i++];
			Class<?> tipoParametro = null;
			if (parameter != null) {
				tipoParametro = parameter.getClass();
			}
			if (parameter != null && !isCompativel(methodParameterType, tipoParametro)) {
				isMatch = false;
				break;
			}
		}

		return isMatch;
	}

	/**
	 * É compatível para atribuição.
	 * 
	 * @author Roberto Alencar
	 * @param methodParameterType
	 *            tipo do parâmetro do método
	 * @param parameterType
	 *            tipo do parâmetro que queremos verificar
	 * @return <code>true</code> se for compatível
	 */
	private boolean isCompativel(Class<?> methodParameterType, Class<?> parameterType) {

		boolean result = false;
		Class<?> comparableType = methodParameterType;
		if (methodParameterType.isPrimitive()) {
			comparableType = MethodUtils.getPrimitiveWrapper(methodParameterType);
		}

		if (comparableType.isAssignableFrom(parameterType)) {
			result = true;
		}

		return result;
	}

	/**
	 * Executa um Script para Elaborar um Cenário a partir de um arquivo SQL 
	 * não realizando ROLLBACK após a execução.
	 * 
	 * @author Valter Negreiros
	 * @param String
	 *            Arquivo SQL a ser Executado um Script
	 */
	protected void prepararCenario(String arquivoSql) {

		executeScript(arquivoSql);
	}

	/**
	 * Executa um Script para Limpar um Cenário a partir de um arquivo SQL.
	 * Deve ser executado após o método prepararCenario() garantir a integridade do banco.
	 * 
	 * 
	 * @author Valter Negreiros
	 * @param String
	 *            Arquivo SQL a ser Executado um Script
	 */
	protected void limparCenario(String arquivoSql) {

		executeScript(arquivoSql);
	}

	/**
	 * Executa um Script a partir de um arquivo SQL.
	 * 
	 * @author Valter Negreiros
	 * @param String
	 *            Arquivo SQL a ser Executado um Script
	 */
	private void executeScript(String scriptPath) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(this.dataSource);
		String workingDirectory = System.getProperty("user.dir").replace("\\", "/");
		Resource resource = new FileSystemResource(workingDirectory + scriptPath);
		JdbcTestUtils.executeSqlScript(jdbcTemplate, resource, Boolean.FALSE);
	}
	
	
	/**
	 * 
	 * Executa script SQL para preparação de cenário. 
	 * Quando executado com anotação Transactional o Rollback será executado
	 * 
	 * 
	 * @param fileName
	 * @throws FileNotFoundException
	 */
	
	public void setupSQL(String fileName) throws FileNotFoundException{
	
		String path = getClass().getClassLoader().getResource("").getFile();
		path = path.replace("bin", "sql");
		path = path.replace("build/classes/test", "sql");
		File f = new File(path);
				
		File[] arquivos = f.listFiles();
		if (arquivos != null) {
			for (final File fileEntry : arquivos) {
				if( fileEntry.getName().endsWith( fileName )){
					Scanner sc = new Scanner(fileEntry);
					
					executarStatement(sc);
				}
			}
		}else{
			throw new FileNotFoundException("Arquivo não encontrado: " + fileName);
		}
		
	}
	
	
	/**
	 * Executa script SETUP_DEPENDENCIAS_SELENIUM.sql para preparação de cenário. 
	 * Quando executado com anotação Transactional o Rollback será executado
	 */
	
	public void setupSql(){
		try {
			setupSQL("SETUP_DEPENDENCIAS_SELENIUM.sql");
		} catch (FileNotFoundException e) {
			throw new RuntimeException( e );
		}
	}
	

	private void executarStatement( Scanner sc ) {
		Session session = getSesstion();
		
		StringBuffer sql = new StringBuffer();
		while( sc.hasNextLine() ){
			String linha = sc.nextLine().trim();
			if( !linha.equals(" ") && !linha.isEmpty() && !linha.startsWith("-") && !linha.startsWith("COMMIT") && !linha.startsWith("commit")){
				sql.append( linha );
				if( linha.endsWith(";")){					
					String command = sql.toString().replace(";", "");
					try{
						session.createSQLQuery( command ).executeUpdate();							
						sql = new StringBuffer();						
					}catch(RuntimeException e){
						e.printStackTrace();
						throw new RuntimeException("Erro: "+command, e );
					}
				}
			}
		}
	}
	
}
