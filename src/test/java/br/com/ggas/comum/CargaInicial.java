
package br.com.ggas.comum;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.NestedRuntimeException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class CargaInicial {

	@Value("${arquivoCarga}")
	String arquivoCarga;

	private static final String H2DB = "CURRENT_TIMESTAMP()";

	private static final String POSTGRES = "NOW()";

	private String driverName = null;

	private String lastTable = null;

	Map<String, StringBuilder> mapa = new HashMap<String, StringBuilder>();

	@Autowired(required = false)
	protected BasicDataSource dataSource;

	JdbcTemplate t;

	/**
	 * Carregar.
	 */
	public void carregar() {

		if (dataSource != null) {
			t = new JdbcTemplate();
			t.setDataSource(dataSource);

			if (validarInsert()) {
				try {
					inserir();
				} catch (IOException e) {

				}
			}
		}
	}

	/**
	 * Inserir.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void inserir() throws IOException {

		verificarDB();
		InputStream is = getClass().getResourceAsStream(arquivoCarga);
		Scanner sc = new Scanner(is, "UTF-8");
		String home = System.getProperty("user.home");
		FileWriter fw = new FileWriter(home + File.separator + "ggas_import.sql");

		while (sc.hasNextLine()) {
			String row = sc.nextLine().toUpperCase().trim();
			if (row.startsWith("INSERT INTO")) {
				if (verificaTabela(row)) {
					try {
						t.execute(format(row));
						t.execute("commit");
					} catch (NestedRuntimeException e) {
						fw.write(row + "\n");
					}
				}
			}
		}
		fw.close();
	}

	/**
	 * Verifica tabela.
	 * 
	 * @param row
	 *            the row
	 * @return true, if successful
	 */
	private boolean verificaTabela(String row) {

		String tabela = row.split(" ")[2].replace("GGAS_ADMIN.", "");
		if (lastTable == null || tabela.equals(lastTable)) {
			mapa.put(tabela, new StringBuilder());
		}
		try {
			
			if (!tabela.equals("CEP_TIPO") && !tabela.startsWith("NFE_") && !tabela.equals("RATEIO_TIPO") && !tabela.equals("SEMANA_DIA")) {
				if (driverName.equals(POSTGRES)) {
					t.execute("ALTER Table " + tabela + " DISABLE TRIGGER ALL");
				} else {
					t.execute("ALTER Table " + tabela + " SET REFERENTIAL_INTEGRITY FALSE");
				}
	
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Format.
	 * 
	 * @param sql
	 *            the sql
	 * @return the string
	 */
	private String format(String sql) {

		if (sql.contains("to_timestamps".toUpperCase())) {
			String sql2 = sql.substring(sql.indexOf("to_timestamp(".toUpperCase()),
							sql.indexOf("HH24:MI:SS,FF\')") + "HH24:MI:SS,FF\')".length());
			return sql.replace(sql2, driverName).replace("GGAS_ADMIN.", "");
		}
		return sql.replace("GGAS_ADMIN.", "");

	}

	/**
	 * Verificar db.
	 */
	private void verificarDB() {

		if (driverName == null && dataSource.getDriverClassName().contains("postgresql")) {
			driverName = POSTGRES;
		} else if (driverName == null && dataSource.getDriverClassName().contains("h2")) {
			driverName = H2DB;
		}
	}

	/**
	 * Validar insert.
	 * 
	 * @return true, if successful
	 */
	private boolean validarInsert() {

		List<Map<String, Object>> qnt = t.queryForList("SELECT * FROM ATIVIDADE_ECONOMICA");

		if (qnt.size() > 0) {
			return false;
		}
		return true;
	}

}