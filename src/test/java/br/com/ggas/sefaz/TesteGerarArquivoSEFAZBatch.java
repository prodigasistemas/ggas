package br.com.ggas.sefaz;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.sefaz.batch.GerarArquivoSefazBatch;

public class TesteGerarArquivoSEFAZBatch extends GGASTestCase {
	
	@Autowired
	GerarArquivoSefazBatch batch;
	
	Map<String, Object> parametros;

	@Before
	public void setup() {
		parametros = new HashMap<String, Object>();
		parametros.put("anoMesReferencia", "2018/08");
	}
	
	@Test
	@Transactional
	public void testProcessarSemDados() throws GGASException {
		assertEquals("DIRETORIO SELECIONADO: /tmp/\n" + 
				"ANO MES REFERENCIA: 2018/08\n" + 
				"Séries válidas do período: \n" +
				"Gerando arquivo Item Documento Fiscal...\n" + 
				"Gerando arquivo Mestre Documento Fiscal...\n" + 
				"Gerando arquivo Dados Cadastrais...", batch.processar(parametros));
	}

}
