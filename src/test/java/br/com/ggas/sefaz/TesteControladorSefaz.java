package br.com.ggas.sefaz;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.sefaz.ControladorSefaz;
import br.com.ggas.integracao.sefaz.repositorio.RegistroSefazTipoEnum;

public class TesteControladorSefaz extends GGASTestCase {

	@Autowired
	ControladorSefaz controladorSefazImpl;

	private final String ANOMESREFERENCIA = "201811";
	private final String SERIE = "7";
	private final String NOME_ARQUIVO = "teste.txt";

	@Test
	@Transactional
	public void testConsultarSeries() {
		List<String> series = controladorSefazImpl.consultarSeries(ANOMESREFERENCIA);
		assertEquals(Collections.EMPTY_LIST, series);
	}

	@Test
	@Transactional
	public void testGerarArquivoMestre() throws GGASException {
		assertNotNull(
				controladorSefazImpl.gerarArquivo(NOME_ARQUIVO, ANOMESREFERENCIA, SERIE, RegistroSefazTipoEnum.MESTRE));
	}

	@Test
	@Transactional
	public void testGerarArquivoItem() throws GGASException {
		assertNotNull(
				controladorSefazImpl.gerarArquivo(NOME_ARQUIVO, ANOMESREFERENCIA, SERIE, RegistroSefazTipoEnum.ITEM));
	}

	@Test
	@Transactional
	public void testGerarArquivoDados() throws GGASException {
		assertNotNull(controladorSefazImpl.gerarArquivo(NOME_ARQUIVO, ANOMESREFERENCIA, SERIE,
				RegistroSefazTipoEnum.DADOSCADASTRAIS));
	}
	
	@Test
	@Transactional
	public void testGerarArquivoPassandoSeries() throws GGASException {
		List<String> series = new ArrayList<String>();
		series.add(SERIE);
		
		assertNotNull(controladorSefazImpl.gerarArquivo(NOME_ARQUIVO, ANOMESREFERENCIA, series,
				RegistroSefazTipoEnum.DADOSCADASTRAIS));
	}

	@After
	public void removeArquivoGerado() {
		File f = new File(NOME_ARQUIVO);
		f.delete();
	}
}
