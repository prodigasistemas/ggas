package br.com.ggas.sefaz;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.ggas.integracao.sefaz.arquivos.DadosCadastraisLayout;
import br.com.ggas.integracao.sefaz.arquivos.DadosCadastraisLinha;

public class TesteDadosCadastraisLayout {

	private DadosCadastraisLinha dados;
	private DadosCadastraisLayout layout;

	@Before
	public void setup() {
		dados = new DadosCadastraisLinha();
		dados.setCnpjCpf("07165156000252");
		dados.setInscricaoEstadual("242326420     ");
		dados.setRazaoSocial("MAXI POSTO IV LTDA EPP             ");
		dados.setLogradouro("AVENIDA ASSIS CHATEAUBRIAND                  ");
		dados.setNumeroLogradouro("02890");
		dados.setComplemento("               ");
		dados.setCep("57010070");
		dados.setBairro("PRADO          ");
		dados.setMunicipio("Maceió                        ");
		dados.setUf("AL");
		dados.setTelefoneContato("            ");
		dados.setCodigoConsumidor("44853       ");
		dados.setDataEmissao("20181130");
		dados.setModelo("01");
		dados.setSerie("137");
		dados.setNumero("000102711");
		dados.setCodigoMunicipio("2704302");

		layout = new DadosCadastraisLayout(dados);
	}
	
	@Test
	public void testPreenchimentoTelefoneContato() {
		DadosCadastraisLinha linha = new DadosCadastraisLinha();
		linha.setTelefoneContato("");
		
		assertEquals("            ", linha.getTelefoneContato());
	}
	
	@Test
	public void testPreenchimentoTelefoneContatoNull() {
		DadosCadastraisLinha linha = new DadosCadastraisLinha();
		linha.setTelefoneContato(null);
		
		assertEquals("            ", linha.getTelefoneContato());
	}
	
	@Test
	public void testPreenchimentoTelefoneContatoZero() {
		DadosCadastraisLinha linha = new DadosCadastraisLinha();
		linha.setTelefoneContato("00");
		
		assertEquals("00          ", linha.getTelefoneContato());
	}
	
	@Test
	public void testPreenchimentoTelefoneContatoNoveDigitos() {
		DadosCadastraisLinha linha = new DadosCadastraisLinha();
		linha.setTelefoneContato("123456789");
		
		assertEquals("123456789   ", linha.getTelefoneContato());
	}
	
	@Test
	public void testPreenchimentoTelefoneContatoOitoDigitos() {
		DadosCadastraisLinha linha = new DadosCadastraisLinha();
		linha.setTelefoneContato("12345678");
		
		assertEquals("12345678    ", linha.getTelefoneContato());
	}
	
	@Test
	public void testPreenchimentoTelefoneContatoRamal() {
		DadosCadastraisLinha linha = new DadosCadastraisLinha();
		linha.setTelefoneContato("1234");
		
		assertEquals("1234        ", linha.getTelefoneContato());
	}

	@Test
	public void testGerarLinha() {
		String linha = "07165156000252242326420     MAXI POSTO IV LTDA EPP             AVENIDA ASSIS CHATEAUBRIAND                  02890               57010070PRADO          Maceió                        AL            44853                     20181130011370001027112704302     ";
		assertEquals(linha, layout.getLinha());
		assertEquals(255, layout.getLinha().length());
	}

	@Test
	public void testGerarLinhaInscricaoEstadualIsento() {
		String linha = "07165156000252ISENTO        MAXI POSTO IV LTDA EPP             AVENIDA ASSIS CHATEAUBRIAND                  02890               57010070PRADO          Maceió                        AL            44853                     20181130011370001027112704302     ";

		dados.setInscricaoEstadual("ISENTA        ");
		layout = new DadosCadastraisLayout(dados);

		assertEquals(linha, layout.getLinha());
		assertEquals(255, layout.getLinha().length());
	}

	@Test
	public void testGerarLinhaCnpjCpfVazio() {
		String linha = "00000000000000242326420     MAXI POSTO IV LTDA EPP             AVENIDA ASSIS CHATEAUBRIAND                  02890               57010070PRADO          Maceió                        AL            44853                     20181130011370001027112704302     ";

		dados.setCnpjCpf(null);
		layout = new DadosCadastraisLayout(dados);

		assertEquals(linha, layout.getLinha());
		assertEquals(255, layout.getLinha().length());
	}
}
