package br.com.ggas.sefaz;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.ggas.integracao.sefaz.arquivos.ItemDocumentoFiscalLayout;
import br.com.ggas.integracao.sefaz.arquivos.ItemDocumentoFiscalLinha;

public class TesteItemDocumentoFiscalLayout {

	private ItemDocumentoFiscalLinha dados;
	private ItemDocumentoFiscalLayout layout;
	
	@Before
	public void setup() {
		dados = new ItemDocumentoFiscalLinha();
		
		dados.setCnpjCpf("07165156000252");
		dados.setUf("AL");
		dados.setDataEmissao("20181130");
		dados.setSerie("137");
		dados.setNumero("102711");
		dados.setCodigoItem("PB 011    ");
		dados.setDescricaoItem("GAS NATURAL                             ");
		dados.setCfop("5405");
		dados.setNumeroOrdemItem("001");
		dados.setCodigoClassificacaoItem("5005");
		dados.setUnidade("m3    ");
		dados.setQuantidadeMedida("000019320000");
		dados.setTotal("00004164807");
		dados.setDescontoRedutores("00000000000");
		dados.setAcrescimosDespesasAcessorias("00000000000");
		dados.setBcIcms("00000000000");
		dados.setIcms("00000000000");
		dados.setOperacoesIsentasNaoTributaveis("00000000000");
		dados.setOutrosValores("00004164807");
		dados.setAliquotaIcms("0000");
		dados.setSituacao("N");
		dados.setAnoMesReferencia("1811");
		dados.setQuantidadeFaturada("000018688000");
		dados.setAliquotaPisPasep("076000");
		dados.setPisPasep("00031652533");
		dados.setAliquotaCofins("016500");
		dados.setCofins("00006871932");
		
		layout = new ItemDocumentoFiscalLayout(dados);
	}

	@Test
	public void testGerarLinha() {
		String linha = "07165156000252AL010020181130011370001027115405001PB 011    GAS NATURAL                             5005m3    000000000000000019320000000041648070000000000000000000000000000000000000000000000000000000000041648070000N1811               000018688000000000000000760000003165253301650000006871932 00     ";
		assertEquals(linha, layout.getLinha());
		assertEquals(299, layout.getLinha().length());
	}

}
