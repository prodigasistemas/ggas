package br.com.ggas.sefaz;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.ggas.integracao.sefaz.arquivos.ArquivoSefaz;
import br.com.ggas.integracao.sefaz.arquivos.DadosCadastraisLayout;
import br.com.ggas.integracao.sefaz.arquivos.DadosCadastraisLinha;

public class TesteArquivoSefazDadosCadastrais {

	private DadosCadastraisLinha dados;
	private List<DadosCadastraisLayout> linhas;
	private ArquivoSefaz<DadosCadastraisLayout> arquivo;
	private Map<String, String> arquivoParams;
	
	private String mesAnoReferencia = "072018";
	private List<String> series = Arrays.asList(new String[] {"1234", "222"});
	
	@Before
	public void setup() {
		arquivoParams = new HashMap<String, String>();
		arquivoParams.put(ArquivoSefaz.DIRETORIO, System.getProperty("user.home"));
		arquivoParams.put(ArquivoSefaz.CNPJ, "69983484000132");
		arquivoParams.put(ArquivoSefaz.UF, "AL");
		
		linhas = new ArrayList<DadosCadastraisLayout>();
		arquivo = new ArquivoSefaz<DadosCadastraisLayout>(mesAnoReferencia, series.get(0), DadosCadastraisLayout.getTipo());

		dados = new DadosCadastraisLinha();
		dados.setCnpjCpf("07165156000252");
		dados.setInscricaoEstadual("242326420     ");
		dados.setRazaoSocial("MAXI POSTO IV LTDA EPP             ");
		dados.setLogradouro("AVENIDA ASSIS CHATEAUBRIAND                  ");
		dados.setNumeroLogradouro("02890");
		dados.setComplemento("               ");
		dados.setCep("57010070");
		dados.setBairro("PRADO          ");
		dados.setMunicipio("Maceió                        ");
		dados.setUf("AL");
		dados.setTelefoneContato("            ");
		dados.setCodigoConsumidor("44853       ");
		dados.setNumeroTelefonicoConsumidora("            ");
		dados.setUfTelefonico("  ");
		dados.setDataEmissao("20181130");
		dados.setModelo("01");
		dados.setSerie("137");
		dados.setNumero("000102711");
		dados.setCodigoMunicipio("2704302");
	}
	
	@After
	public void apagarArquivo() {
		new File(arquivoParams.get(ArquivoSefaz.DIRETORIO) + arquivo.getNomeArquivo()).delete();
	}
	
	@Test
	public void testGerarArquivo() {
		linhas.add(new DadosCadastraisLayout(dados));
		
		assertTrue(arquivo.gerarArquivo(arquivoParams, linhas));
	}
	
	@Test
	public void testQuebraLinha() {
		linhas.add(new DadosCadastraisLayout(dados));
		linhas.add(new DadosCadastraisLayout(dados));
		
		assertTrue(arquivo.gerarArquivo(arquivoParams, linhas));
	}
	
	@Test
	public void testGetMD5HashParaStringNula() {
		assertEquals(ArquivoSefaz.getMD5Hash(null), "");
	}
}
