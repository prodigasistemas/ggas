package br.com.ggas.sefaz;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.ggas.integracao.sefaz.arquivos.MestreDocumentoFiscalLayout;
import br.com.ggas.integracao.sefaz.arquivos.MestreDocumentoFiscalLinha;

public class TesteMestreDocumentoFiscalLayout {

	private MestreDocumentoFiscalLinha dados;
	private MestreDocumentoFiscalLayout layout;
	
	@Before
	public void setup() {
		dados = new MestreDocumentoFiscalLinha();
		dados.setCnpjCpf("07165156000252");
		dados.setInscricaoEstadual("242326420     ");
		dados.setRazaoSocial("MAXI POSTO IV LTDA EPP             ");
		dados.setUf("AL");
		dados.setCodigoIdentificacaoConsumidorAssinante("44853       ");
		dados.setDataEmissao("20181130");
		dados.setModelo("01");
		dados.setSerie("137");
		dados.setNumero("000102711");
		dados.setValorTotal("000004164807");
		dados.setBcIcms("000000000000");
		dados.setIcmsDestacado("000000000000");
		dados.setOperacoesIsentasNaoTributadas("000000000000");
		dados.setOutrosValores("000004164807");
		dados.setSituacaoDocumento("N");
		dados.setAnoMesReferenciaApuracao("1811");
		dados.setReferenciaItemNF("000000001");
		dados.setTipoCliente("99");
		dados.setCnpjEmitente("69983484000132");
		dados.setNumeroCodigoFaturaComercial("137102711           ");
		dados.setValorTotalFaturaComercial("000004164807");
		dados.setDataCancelamento("");
		dados.setPontoConsumoTributoAliquotaICMSIsento(null);
		dados.setTipoPessoa("2");
		
		layout = new MestreDocumentoFiscalLayout(dados);
	}

	@Test
	public void testGerarLinha() {
		String linha = "07165156000252242326420     MAXI POSTO IV LTDA EPP             AL010044853       20181130011370001027113dea0820c91ad8f8c5ac0506d7799ba1000004164807000000000000000000000000000000000000000004164807N1811000000001            19900            69983484000132137102711           0000041648070000000000000000                                                  00000000                                   ";
		assertEquals(linha, layout.getLinha());
		assertEquals(393, layout.getLinha().length());
	}
	
	@Test
	public void testGerarLinhaInscricaoEstadualIsento() {
		String linha = "07165156000252ISENTO        MAXI POSTO IV LTDA EPP             AL010044853       20181130011370001027113dea0820c91ad8f8c5ac0506d7799ba1000004164807000000000000000000000000000000000000000004164807N1811000000001            19900            69983484000132137102711           0000041648070000000000000000                                                  00000000                                   ";
		
		dados.setInscricaoEstadual("ISENTA        ");
		layout = new MestreDocumentoFiscalLayout(dados);
		
		assertEquals(linha, layout.getLinha());
		assertEquals(393, layout.getLinha().length());
	}
	
	@Test
	public void testGerarLinhaSemCPF() {
		String linha = "00000000000000242326420     MAXI POSTO IV LTDA EPP             AL010044853       20181130011370001027112ba561cc25fe7f1a21e75182f88cf78c000004164807000000000000000000000000000000000000000004164807N1811000000001            39900            69983484000132137102711           0000041648070000000000000000                                                  00000000                                   ";
		
		dados.setCnpjCpf("00000000000000");
		dados.setIndicacaoTipoInformacaoCampo1("");
		layout = new MestreDocumentoFiscalLayout(dados);
		
		assertEquals("3", layout.getLinha().substring(221, 222));
		assertEquals(linha, layout.getLinha());
		assertEquals(393, layout.getLinha().length());
	}
}
