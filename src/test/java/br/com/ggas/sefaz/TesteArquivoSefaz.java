package br.com.ggas.sefaz;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import br.com.ggas.integracao.sefaz.arquivos.ArquivoSefaz;
import br.com.ggas.integracao.sefaz.arquivos.DadosCadastraisLayout;
import br.com.ggas.integracao.sefaz.arquivos.ItemDocumentoFiscalLayout;
import br.com.ggas.integracao.sefaz.arquivos.MestreDocumentoFiscalLayout;

public class TesteArquivoSefaz {

	private String anoMesReferencia = "201811";
	private String serie = "123";
	private String cnpj = "69983484000132";
	private String uf = "AL";

	@Before
	public void iniciar() {
	}

	@Test
	public void testNomeArquivoMestreDocumentoFiscal() {
		ArquivoSefaz<MestreDocumentoFiscalLayout> arquivo = new ArquivoSefaz<MestreDocumentoFiscalLayout>(
				anoMesReferencia, serie, MestreDocumentoFiscalLayout.getTipo());
		
		arquivo.gerarNomeArquivo(cnpj, uf);
		
		assertNotNull(arquivo.getNomeArquivo());
		assertEquals(arquivo.getNomeArquivo().length(), 33);
		assertEquals(arquivo.getNomeArquivo().substring(28, 29), MestreDocumentoFiscalLayout.getTipo());
	}

	@Test
	public void testNomeArquivoItemDocumentoFiscal() {
		ArquivoSefaz<ItemDocumentoFiscalLayout> arquivo = new ArquivoSefaz<ItemDocumentoFiscalLayout>(anoMesReferencia,
				serie, ItemDocumentoFiscalLayout.getTipo());
		
		arquivo.gerarNomeArquivo(cnpj, uf);
		
		assertNotNull(arquivo.getNomeArquivo());
		assertEquals(arquivo.getNomeArquivo().length(), 33);
		assertEquals(arquivo.getNomeArquivo().substring(28, 29), ItemDocumentoFiscalLayout.getTipo());
	}

	@Test
	public void testNomeArquivoDadosCadastrais() {
		ArquivoSefaz<DadosCadastraisLayout> arquivo = new ArquivoSefaz<DadosCadastraisLayout>(anoMesReferencia, serie,
				DadosCadastraisLayout.getTipo());
		
		arquivo.gerarNomeArquivo(cnpj, uf);
		
		assertNotNull(arquivo.getNomeArquivo());
		assertEquals(arquivo.getNomeArquivo().length(), 33);
		assertEquals(arquivo.getNomeArquivo().substring(28, 29), DadosCadastraisLayout.getTipo());
	}

}
