package br.com.ggas.sefaz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.imovel.PontoConsumoTributoAliquota;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.sefaz.arquivos.DadosCadastraisLayout;
import br.com.ggas.integracao.sefaz.arquivos.DadosCadastraisLinha;
import br.com.ggas.integracao.sefaz.arquivos.ItemDocumentoFiscalLayout;
import br.com.ggas.integracao.sefaz.arquivos.ItemDocumentoFiscalLinha;
import br.com.ggas.integracao.sefaz.arquivos.MestreDocumentoFiscalLayout;
import br.com.ggas.integracao.sefaz.arquivos.MestreDocumentoFiscalLinha;

import br.com.ggas.integracao.sefaz.repositorio.DadosCadastraisPopulator;
import br.com.ggas.integracao.sefaz.repositorio.ItemDocumentoPopulator;
import br.com.ggas.integracao.sefaz.repositorio.MestreDocumentoPopulator;
import br.com.ggas.integracao.sefaz.repositorio.RegistroSefazFactory;
import br.com.ggas.integracao.sefaz.repositorio.RegistroSefazPopulator;
import br.com.ggas.integracao.sefaz.repositorio.RegistroSefazTipoEnum;
import br.com.ggas.integracao.sefaz.repositorio.RepositorioSefaz;

public class TesteRepositorioSefaz extends GGASTestCase {

	private Object[] linhaDadosCadastrais;
	private HashMap<Integer, Integer> dadosCadastraisCampos = new HashMap<>();

	private Object[] linhaMestre;
	private Object[] linhaItemDocumento;

	@Autowired
	RepositorioSefaz repositorioSefaz;
	
	
	RegistroSefazFactory factory;
	RegistroSefazPopulator populator;
	
	String anoMesReferencia;
	String serie;

	private void setupMestreDocumentoFiscal() {
		ArrayList<String> resultadoQryMestre = new ArrayList<>();
		resultadoQryMestre.add("07165156000252");
		resultadoQryMestre.add("242326420     ");
		resultadoQryMestre.add("MAXI POSTO IV LTDA EPP             ");
		resultadoQryMestre.add("AL");
		resultadoQryMestre.add("0");
		resultadoQryMestre.add("1");
		resultadoQryMestre.add("00");
		resultadoQryMestre.add("44853       ");
		resultadoQryMestre.add("20181130");
		resultadoQryMestre.add("01");
		resultadoQryMestre.add("137");
		resultadoQryMestre.add("000102711");
		resultadoQryMestre.add("00000123450000012345123456789012345");
		resultadoQryMestre.add("000004164807");
		resultadoQryMestre.add("000000000000");
		resultadoQryMestre.add("000000000000");
		resultadoQryMestre.add("000000000000");
		resultadoQryMestre.add("000004164807");
		resultadoQryMestre.add("N");
		resultadoQryMestre.add("1811");
		resultadoQryMestre.add("000000001");
		resultadoQryMestre.add("            ");
		resultadoQryMestre.add("1");
		resultadoQryMestre.add("99");
		resultadoQryMestre.add("00");
		resultadoQryMestre.add("            ");
		resultadoQryMestre.add("69983484000132");
		resultadoQryMestre.add("137102711           ");
		resultadoQryMestre.add("000004164807");
		resultadoQryMestre.add("00000000");
		resultadoQryMestre.add("00000000");
		resultadoQryMestre.add("12345678901234567890123456789012345678901234567890");
		resultadoQryMestre.add("00000000");
		resultadoQryMestre.add("                              ");
		resultadoQryMestre.add("     ");
		resultadoQryMestre.add("000000000000");
		resultadoQryMestre.add("000000000000");
		resultadoQryMestre.add("        ");
		resultadoQryMestre.add("5405");
		resultadoQryMestre.add("0899");
		resultadoQryMestre.add("2");
		
		linhaMestre = new String[resultadoQryMestre.size()];
		linhaMestre = resultadoQryMestre.toArray(linhaMestre);
	}

	private void setupDadosCadastrais() {
		ArrayList<String> resultadoQryDadosCadastrais = new ArrayList<>();
		resultadoQryDadosCadastrais.add("07165156000252");
		resultadoQryDadosCadastrais.add("242326420     ");
		resultadoQryDadosCadastrais.add("MAXI POSTO IV LTDA EPP             ");
		resultadoQryDadosCadastrais.add("AVENIDA ASSIS CHATEAUBRIAND                  ");
		resultadoQryDadosCadastrais.add("02890");
		resultadoQryDadosCadastrais.add("               ");
		resultadoQryDadosCadastrais.add("57010070");
		resultadoQryDadosCadastrais.add("PRADO          ");
		resultadoQryDadosCadastrais.add("Maceió                        ");
		resultadoQryDadosCadastrais.add("AL");
		resultadoQryDadosCadastrais.add("            ");
		resultadoQryDadosCadastrais.add("44853       ");
		resultadoQryDadosCadastrais.add("            ");
		resultadoQryDadosCadastrais.add("  ");
		resultadoQryDadosCadastrais.add("20181130");
		resultadoQryDadosCadastrais.add("01");
		resultadoQryDadosCadastrais.add("137");
		resultadoQryDadosCadastrais.add("000102711");
		resultadoQryDadosCadastrais.add("2704302");
		resultadoQryDadosCadastrais.add("     ");
		resultadoQryDadosCadastrais.add("69983484000132");

		linhaDadosCadastrais = new String[resultadoQryDadosCadastrais.size()];
		linhaDadosCadastrais = resultadoQryDadosCadastrais.toArray(linhaDadosCadastrais);

		dadosCadastraisCampos = new HashMap<>();
		dadosCadastraisCampos.put(1,  14);
		dadosCadastraisCampos.put(2,  14);
		dadosCadastraisCampos.put(3,  35);
		dadosCadastraisCampos.put(4,  45);
		dadosCadastraisCampos.put(5,  5);
		dadosCadastraisCampos.put(6,  15);
		dadosCadastraisCampos.put(7,  8);
		dadosCadastraisCampos.put(8,  15);
		dadosCadastraisCampos.put(9,  30);
		dadosCadastraisCampos.put(10, 2);
		dadosCadastraisCampos.put(11, 12);
		dadosCadastraisCampos.put(12, 12);
		dadosCadastraisCampos.put(13, 12);
		dadosCadastraisCampos.put(14, 2);
		dadosCadastraisCampos.put(15, 8);
		dadosCadastraisCampos.put(16, 2);
		dadosCadastraisCampos.put(17, 3);
		dadosCadastraisCampos.put(18, 9);
		dadosCadastraisCampos.put(19, 7);
	}

	private void setupItemDocumentoFiscal() {
		ArrayList<String> resultadoQryItemDocumento = new ArrayList<>();
		resultadoQryItemDocumento.add("07165156000252");
		resultadoQryItemDocumento.add("AL");
		resultadoQryItemDocumento.add("0");
		resultadoQryItemDocumento.add("1");
		resultadoQryItemDocumento.add("00");
		resultadoQryItemDocumento.add("20181130");
		resultadoQryItemDocumento.add("01");
		resultadoQryItemDocumento.add("137");
		resultadoQryItemDocumento.add("102711");
		resultadoQryItemDocumento.add("5405");
		resultadoQryItemDocumento.add("001");
		resultadoQryItemDocumento.add("PB 011    ");
		resultadoQryItemDocumento.add("GAS NATURAL                             ");
		resultadoQryItemDocumento.add("5005");
		resultadoQryItemDocumento.add("m3    ");
		resultadoQryItemDocumento.add("000000000000");
		resultadoQryItemDocumento.add("000019320000");
		resultadoQryItemDocumento.add("00004164807");
		resultadoQryItemDocumento.add("00000000000");
		resultadoQryItemDocumento.add("00000000000");
		resultadoQryItemDocumento.add("00000000000");
		resultadoQryItemDocumento.add("00000000000");
		resultadoQryItemDocumento.add("00000000000");
		resultadoQryItemDocumento.add("00004164807");
		resultadoQryItemDocumento.add("0000");
		resultadoQryItemDocumento.add("N");
		resultadoQryItemDocumento.add("1811");
		resultadoQryItemDocumento.add("               ");
		resultadoQryItemDocumento.add("000018688000");
		resultadoQryItemDocumento.add("00000000000");
		resultadoQryItemDocumento.add("076000");
		resultadoQryItemDocumento.add("00031652533");
		resultadoQryItemDocumento.add("016500");
		resultadoQryItemDocumento.add("00006871932");
		resultadoQryItemDocumento.add(" ");
		resultadoQryItemDocumento.add("00");
		resultadoQryItemDocumento.add("000000000000");
		resultadoQryItemDocumento.add("000000000000");
		resultadoQryItemDocumento.add("        ");
		resultadoQryItemDocumento.add("69983484000132");
		resultadoQryItemDocumento.add("000000000000");
		resultadoQryItemDocumento.add("          ");
		resultadoQryItemDocumento.add("000000000000");

		linhaItemDocumento = new String[resultadoQryItemDocumento.size()];
		linhaItemDocumento = resultadoQryItemDocumento.toArray(linhaItemDocumento);
	}

	private void setupConsultarSeries() {
		anoMesReferencia = "201811";
		serie = "7";
	}

	@Before
	public void setup() {
		factory = RegistroSefazFactory.factory(builder -> {
		      builder.add(RegistroSefazTipoEnum.DADOSCADASTRAIS, DadosCadastraisPopulator::new);
		      builder.add(RegistroSefazTipoEnum.MESTRE, MestreDocumentoPopulator::new);
		      builder.add(RegistroSefazTipoEnum.ITEM, ItemDocumentoPopulator::new);
		});
		setupMestreDocumentoFiscal();
		setupItemDocumentoFiscal();
		setupDadosCadastrais();
		setupConsultarSeries();
	}

	@Test
	@Transactional
	public void testConsultarMestreDocumentoFiscal() throws NegocioException {
		List<MestreDocumentoFiscalLayout> lista = repositorioSefaz.consultarMestreDocumentoFiscal(anoMesReferencia, serie);
		Assert.assertEquals(Collections.EMPTY_LIST, lista);
	}

	@Test
	@Transactional
	public void testConsultarDadosCadastrais() throws NegocioException {
		List<DadosCadastraisLayout> lista = repositorioSefaz.consultarDadosCadastrais(anoMesReferencia, serie);
		Assert.assertEquals(Collections.EMPTY_LIST, lista);
	}

	@Test
	@Transactional
	public void testConsultarItemDocumentoFiscal() throws NegocioException {
		List<ItemDocumentoFiscalLayout> lista = repositorioSefaz.consultarItemDocumentoFiscal(anoMesReferencia, serie);
		Assert.assertEquals(Collections.EMPTY_LIST, lista);
	}

	@Test
	public void testPopulaDadosCadastraisUsandoQueryResult() {
		populator = factory.create(RegistroSefazTipoEnum.DADOSCADASTRAIS);
		DadosCadastraisLinha dadosCadastrais = (DadosCadastraisLinha) populator.popularObjeto(linhaDadosCadastrais);
		Assert.assertEquals(dadosCadastrais.getDataEmissao(), linhaDadosCadastrais[14]);
		Assert.assertEquals(dadosCadastrais.getCodigoMunicipio(), linhaDadosCadastrais[18]);
	}

	@Test
	public void testPopulaMestreDocumentoFiscalUsandoQueryResult() {
		populator = factory.create(RegistroSefazTipoEnum.MESTRE);
		MestreDocumentoFiscalLinha mestreDocumento = (MestreDocumentoFiscalLinha) populator.popularObjeto(linhaMestre);
		Assert.assertEquals(mestreDocumento.getRazaoSocial(), linhaMestre[2]);
		Assert.assertEquals(mestreDocumento.getValorTotalFaturaComercial(), linhaMestre[28]);
	}

	@Test
	public void testPopulaItemDocumentoFiscalUsandoQueryResult() {
		populator = factory.create(RegistroSefazTipoEnum.ITEM);
		ItemDocumentoFiscalLinha itemDocumento = (ItemDocumentoFiscalLinha) populator.popularObjeto(linhaItemDocumento);
		Assert.assertEquals(itemDocumento.getCnpjCpf(), linhaItemDocumento[0]);
		Assert.assertEquals(itemDocumento.getCodigoItem(), linhaItemDocumento[11]);
		Assert.assertEquals(itemDocumento.getCofins(), linhaItemDocumento[33]);
		Assert.assertEquals(itemDocumento.getTipoIsencaoReducaoBaseCalculo(), linhaItemDocumento[35]);
	}

	@Test
	@Transactional
	public void testConsultarSeries() {
		List<String> series = repositorioSefaz.consultarSeries(anoMesReferencia);
		Assert.assertNotEquals(Collections.EMPTY_LIST, series.size());
		series.stream().forEach(serie -> {
			Assert.assertNotNull(serie);
		});
	}

	@Test
	@Transactional
	public void testConsultarAliquotaICMSIsento() {
		PontoConsumoTributoAliquota aliquota = repositorioSefaz.consultarPontoConsumoTributoAliquotaICMSIsento("12345");
		Assert.assertNull(aliquota);
	}
}
