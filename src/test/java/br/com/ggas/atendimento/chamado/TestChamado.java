package br.com.ggas.atendimento.chamado;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.chamado.dominio.AcaoChamado;
import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.chamado.dominio.ChamadoProtocolo;
import br.com.ggas.atendimento.chamado.negocio.ControladorChamado;
import br.com.ggas.atendimento.chamado.negocio.impl.NotificadorEmailChamado;
import br.com.ggas.atendimento.chamadoassunto.dominio.ChamadoAssunto;
import br.com.ggas.atendimento.chamadoassunto.negocio.ControladorChamadoAssunto;
import br.com.ggas.atendimento.chamadotipo.dominio.ChamadoTipo;
import br.com.ggas.atendimento.chamadotipo.negocio.ControladorChamadoTipo;
import br.com.ggas.atendimento.registroatendimento.CanalAtendimento;
import br.com.ggas.atendimento.registroatendimento.impl.CanalAtendimentoImpl;
import br.com.ggas.cadastro.unidade.ControladorUnidadeOrganizacional;
import br.com.ggas.cadastro.unidade.UnidadeOrganizacional;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.tabelaAuxiliar.ControladorTabelaAuxiliar;
import br.com.ggas.util.Constantes;

public class TestChamado extends GGASTestCase{
	
	@Autowired
	ControladorChamadoAssunto controladorChamadoAssunto;
	
	@Autowired
	ControladorChamado controladorChamado;
	
	@Qualifier(ControladorUnidadeOrganizacional.BEAN_ID_CONTROLADOR_UNIDADE_ORGANIZACIONAL)
	@Autowired
	ControladorUnidadeOrganizacional controladorUnidade;
	
	@Autowired
	@Qualifier("controladorUsuario")
	private ControladorUsuario controladorUsuario;
	
	@Autowired
	ControladorChamadoTipo controladorChamadoTipo;
	
	@Autowired
	ControladorEntidadeConteudo controladorEntidadeConteudo;
	
	@Autowired
	ControladorConstanteSistema controladorConstanteSistema;
	
	@Autowired
	ControladorTabelaAuxiliar controladorTabelaAuxiliar;
	
	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_CHAMADO.sql";
	
	@Before
	@Transactional
	public void setup() throws FileNotFoundException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}

	@Test
	@Transactional
	public void testeEnvioEmailChamado() throws Throwable {
		NotificadorEmailChamado notificador = new NotificadorEmailChamado();
		ChamadoAssunto chamadoAssunto = (ChamadoAssunto) controladorChamadoAssunto.obter(999999l);
		UnidadeOrganizacional unidade = (UnidadeOrganizacional) controladorUnidade.obter(999999l);
		Chamado chamado = new Chamado();
		chamado.setChamadoAssunto(chamadoAssunto);
		chamado.setUnidadeOrganizacional(unidade);
		Set<String> destinarios = (Set<String>) chamarMetodoPrivado(notificador, "gerarDestinatarios", AcaoChamado.ENCERRAR, chamado);
		
		assertEquals(destinarios.size(), 0);
	}
	
	@Test
	@Transactional
	public void testeNomeResponavelChamado() throws Throwable {
		Chamado chamado = montarChamado();
		assertNotNull(chamado.getCondeudoEmail());
	}
	
	public Chamado montarChamado() throws NegocioException {
		Chamado chamado = new Chamado();
		
		ChamadoProtocolo protocolo = new ChamadoProtocolo();
		protocolo.setNumeroProtocolo(1l);
		protocolo.setUltimaAlteracao(new Date());
		
		UnidadeOrganizacional unidade = (UnidadeOrganizacional) controladorUnidade.obter(999999l);
		
		Usuario usuario = (Usuario) controladorUsuario.obter(1l);
		
		ChamadoAssunto chamadoAssunto = (ChamadoAssunto) controladorChamadoAssunto.obter(999999l);
		
		ChamadoTipo chamadoTipo = controladorChamadoTipo.consultarChamadoTipo(999999l);
		
		ConstanteSistema constante = controladorConstanteSistema.obterConstantePorCodigo(Constantes.C_STATUS_CHAMADO_RASCUNHO);
		EntidadeConteudo statusAberto = controladorEntidadeConteudo
				.obterEntidadeConteudo(Long.valueOf(constante.getValor()));
		
		ConstanteSistema canalAtendimento = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_CANAL_ATENDIMENTO_AGENCIA_VIRTUAL);
		
		chamado.setCanalAtendimento((CanalAtendimento) controladorTabelaAuxiliar.obter(Long.parseLong(canalAtendimento.getValor()),
				CanalAtendimentoImpl.class, Boolean.TRUE));		
		chamado.setStatus(statusAberto);
		chamado.setChamadoTipo(chamadoTipo);
		chamado.setChamadoAssunto(chamadoAssunto);
		chamado.setUsuarioResponsavel(usuario);
		chamado.setDataPrevisaoEncerramento(new Date());
		chamado.setUnidadeOrganizacional(unidade);
		chamado.setProtocolo(protocolo);
		chamado.setDescricao("Teste");
		return chamado;
	}

}
