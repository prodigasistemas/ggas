package br.com.ggas.atendimento.servicoautorizacao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.chamado.dominio.Chamado;
import br.com.ggas.atendimento.equipe.dominio.Equipe;
import br.com.ggas.atendimento.equipe.negocio.ControladorEquipe;
import br.com.ggas.atendimento.exception.AutorizacaoServicoImovelCicloLeituraException;
import br.com.ggas.atendimento.exception.AutorizacaoServicoImovelServicoTipoRestritoException;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.dominio.ServicoAutorizacaoVO;
import br.com.ggas.atendimento.servicoautorizacao.negocio.ControladorServicoAutorizacao;
import br.com.ggas.atendimento.servicoautorizacao.repositorio.RepositorioServicoAutorizacao;
import br.com.ggas.atendimento.servicotipo.dominio.ServicoTipo;
import br.com.ggas.atendimento.servicotipo.negocio.ControladorServicoTipo;
import br.com.ggas.auditoria.DadosAuditoria;
import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.acaocomando.dominio.AcaoComandoVO;
import br.com.ggas.batch.acaocomando.negocio.ControladorAcaoComando;
import br.com.ggas.cadastro.imovel.ControladorImovel;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.Imovel;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.avisocorte.AvisoCorte;
import br.com.ggas.cobranca.avisocorte.ControladorAvisoCorte;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.controleacesso.ControladorAcesso;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.faturamento.exception.AutorizacaoServicoRepetidaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.geral.negocio.EntidadeNegocio;
import br.com.ggas.medicao.medidor.OperacaoMedidor;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.DadosAuditoriaUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.util.Util;

public class TesteServicoAutorizacao extends GGASTestCase {

	private static final String DESCRICAO_PONTO_CONSUMO = "POCN TESTE AS 1";

	private static final String NOME_FUNCIONARIO = "ADMINISTRADOR";

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_SERVICO_AUTORIZACAO.sql";

	@Autowired
	@Qualifier(ControladorServicoAutorizacao.BEAN_ID_CONTROLADOR_SERVICO_AUTORIZACAO)
	private ControladorServicoAutorizacao controlador;

	@Autowired
	private RepositorioServicoAutorizacao repositorio;

	@Autowired
	@Qualifier(ControladorAvisoCorte.BEAN_ID_CONTROLADOR_AVISO_CORTE)
	private ControladorAvisoCorte controladorAvisoCorte;

	@Autowired
	@Qualifier(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO)
	private ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	@Qualifier(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA)
	private ControladorConstanteSistema controladorConstanteSistema;

	@Autowired
	@Qualifier(ControladorAcesso.BEAN_ID_CONTROLADOR_ACESSO)
	private ControladorAcesso controladorAcesso;

	@Autowired
	@Qualifier(ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO)
	private ControladorProcesso controladorprocesso;

	@Autowired
	@Qualifier(ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO)
	private ControladorUsuario controladorUsuario;

	@Autowired
	@Qualifier(ControladorImovel.BEAN_ID_CONTROLADOR_IMOVEL)
	private ControladorImovel controladorImovel;

	@Autowired
	@Qualifier(ControladorRota.BEAN_ID_CONTROLADOR_ROTA)
	private ControladorRota controladorRota;

	@Autowired
	private ControladorServicoTipo controladorServicoTipo;

	@Autowired
	private ControladorEquipe controladorEquipe;

	@Autowired
	private ControladorAcaoComando controladorAcaoComando;

	@Before
	@Transactional
	public void setup() throws FileNotFoundException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}

	@Test(expected = NegocioException.class)
	@Transactional
	public void testeInserirSemCamposObrigatorios() throws GGASException {
		controlador.inserirServicoAutorizacao(new ServicoAutorizacao(), getDados(), Boolean.FALSE);
	}

	@Test(expected = AutorizacaoServicoRepetidaException.class)
	@Transactional
	public void testeInserirRepetida() throws GGASException {
		ServicoAutorizacao sa = getServicoAutorizacao();
		sa.setServicoTipo(getServicoTipo("SERVICO TIPO 10000 TESTE"));
		sa.setImovel(getImovel("IMOVEL 10000 TESTE AS"));
		sa.setPontoConsumo(getPontoConsumo(201810, 1));

		controlador.inserirServicoAutorizacao(sa, getDados(), Boolean.FALSE);
	}

	@Test(expected = AutorizacaoServicoImovelCicloLeituraException.class)
	@Transactional
	public void testeInserirSubstituicaoComImovelEmCicloDeMedicaoGerado() throws GGASException {
		ServicoAutorizacao sa = getServicoAutorizacao();
		sa.setServicoTipo(getServicoTipoSubstituicao());
		sa.setImovel(getImovel("IMOVEL 10000 TESTE AS"));
		sa.setPontoConsumo(getPontoConsumo(201810, 1));

		controlador.inserirServicoAutorizacao(sa, getDados(), Boolean.FALSE);
	}

	@Test(expected = AutorizacaoServicoImovelCicloLeituraException.class)
	@Transactional
	public void testeInserirSubstituicaoComImovelEmCicloDeMedicaoEmLeitura() throws GGASException {
		ServicoAutorizacao sa = getServicoAutorizacao();
		sa.setServicoTipo(getServicoTipoSubstituicao());
		sa.setImovel(getImovel("IMOVEL 10000 TESTE AS"));
		sa.setPontoConsumo(getPontoConsumo(201811, 1));

		controlador.inserirServicoAutorizacao(sa, getDados(), Boolean.FALSE);
	}

	@Test(expected = AutorizacaoServicoImovelServicoTipoRestritoException.class)
	@Transactional
	public void testeInserirComServicoTipoRestrito() throws GGASException {
		ServicoAutorizacao sa = getServicoAutorizacao();
		sa.setServicoTipo(getServicoTipo("SERVICO TIPO 20000 TESTE"));
		sa.setImovel(getImovel("IMOVEL 20000 TESTE AS"));
		sa.setPontoConsumo(getPontoConsumo(201809, 1));

		controlador.inserirServicoAutorizacao(sa, getDados(), Boolean.FALSE);
	}

	@Test
	@Transactional
	public void testeInserirSemChamado() throws GGASException {
		ServicoAutorizacao sa = getServicoAutorizacao();
		sa.setServicoTipo(getServicoTipo("SERVICO TIPO 30000 TESTE"));
		sa.setImovel(getImovel("IMOVEL 20000 TESTE AS"));
		sa.setPontoConsumo(getPontoConsumo(201809, 1));

		ServicoAutorizacao retorno = controlador.inserirServicoAutorizacao(sa, getDados(), Boolean.FALSE);
		Assert.assertNotNull(retorno);
		Assert.assertEquals(sa.getServicoTipo().getChavePrimaria(), retorno.getServicoTipo().getChavePrimaria());
		Assert.assertEquals(sa.getImovel().getChavePrimaria(), retorno.getImovel().getChavePrimaria());
	}

	@Test
	@Transactional
	public void testeInserirComChamado() throws GGASException {
		ServicoAutorizacao sa = getServicoAutorizacao();
		sa.setChamado(new Chamado());
		sa.setServicoTipo(getServicoTipo("SERVICO TIPO 30000 TESTE"));
		sa.setImovel(getImovel("IMOVEL 20000 TESTE AS"));
		sa.setPontoConsumo(getPontoConsumo(201809, 1));

		ServicoAutorizacao retorno = controlador.inserirServicoAutorizacao(sa, getDados(), Boolean.FALSE);
		Assert.assertNotNull(retorno);
		Assert.assertEquals(sa.getServicoTipo().getChavePrimaria(), retorno.getServicoTipo().getChavePrimaria());
		Assert.assertEquals(sa.getImovel().getChavePrimaria(), retorno.getImovel().getChavePrimaria());
	}

	@Test
	@Transactional
	public void testeAvisoCorte() throws GGASException {
		Collection<AvisoCorte> listaAvisoCorte = getAvisos();

		Collection<AvisoCorte> listaAvisoCorteAtualizada = controlador.checagemExecucaoServicoCorte(listaAvisoCorte);

		assertNotNull(listaAvisoCorteAtualizada);
	}

	@Test
	@Transactional
	public void testePriorizacaoAvisoCorte() throws GGASException {
		Collection<AvisoCorte> listaAvisoCorteAtualizada = controlador.priorizacaoCorte(getAvisos());

		assertNotNull(listaAvisoCorteAtualizada);
	}

	@Test
	@Transactional
	public void testeRoteirizacaoCorte() throws GGASException {
		Collection<AvisoCorte> listaAvisoCorte = controladorAvisoCorte.obterListaAvisoCortePriorizar();

		Collection<AvisoCorte> listaRoteirizada = controlador.roteirizacaoCorte(listaAvisoCorte);

		assertNotNull(listaRoteirizada);
	}

	@Test
	@Transactional
	public void testeInserirProcessoParaCorte() throws GGASException {
		controlador.inserirProcessoEmLoteParaCorte(getAvisos(), getUsuario(), getDadosAuditoria());

		Collection<Processo> processos = controladorprocesso.consultar();

		assertEquals(1, processos.size());
		assertEquals("Emitir Autorizações de Serviço em Lote: CORTE", processos.iterator().next().getDescricao());
	}

	@Test
	@Transactional
	public void testeGerarAutorizacaoServicoLote() throws GGASException, IOException {
		Map<String, Object> dados = new HashMap<>();
		dados.put("usuario", getUsuario());

		controlador.gerarAutorizacaoServicoLote(
				getAcaoComando("GERAR AUTORIZACAO DE SERVICO DE CORTE EM LOTE 1").getChavePrimaria(),
				new StringBuilder(), dados);

		ServicoAutorizacaoVO vo = new ServicoAutorizacaoVO();
		vo.setServicoTipo(getServicoTipo("TESTE CORTE LOTE 1"));
		Collection<ServicoAutorizacao> lista = repositorio.consultarServicoAutorizacao(vo);

		assertNotNull(lista);
		assertEquals(2, lista.size());

		for (ServicoAutorizacao servicoAutorizacao : lista) {
			assertEquals("Autorização de Serviço Gerada em Lote", servicoAutorizacao.getDescricao());
		}
	}

	@Test
	@Transactional
	public void testeGerarAutorizacaoServicoLoteComAcaoComandoPontoConsumo() throws GGASException, IOException {
		Map<String, Object> dados = new HashMap<>();
		dados.put("usuario", getUsuario());

		controlador.gerarAutorizacaoServicoLote(
				getAcaoComando("GERAR AUTORIZACAO DE SERVICO DE CORTE EM LOTE 2").getChavePrimaria(),
				new StringBuilder(), dados);

		ServicoAutorizacaoVO vo = new ServicoAutorizacaoVO();
		vo.setServicoTipo(getServicoTipo("TESTE CORTE LOTE 2"));
		Collection<ServicoAutorizacao> lista = repositorio.consultarServicoAutorizacao(vo);

		assertNotNull(lista);
		assertEquals(1, lista.size());
		assertEquals("Autorização de Serviço Gerada em Lote", lista.iterator().next().getDescricao());
	}

	private EntidadeNegocio getAcaoComando(String descricao) throws NegocioException {
		AcaoComandoVO filtro = new AcaoComandoVO();
		filtro.setDescricao(descricao);

		return Util.primeiroElemento(controladorAcaoComando.consultarAcaoComando(filtro, Boolean.TRUE));
	}

	private ServicoAutorizacao getServicoAutorizacao() throws GGASException {
		ServicoAutorizacao sa = new ServicoAutorizacao();
		sa.setDescricao("TESTE INSERIR AS");

		Equipe equipe = getEquipe("EQUIPE TESTE AS");

		sa.setEquipe(equipe);

		return sa;
	}

	private Equipe getEquipe(String nome) throws NegocioException {
		Equipe equipe = new Equipe();
		equipe.setNome(nome);

		return Util.primeiroElemento(controladorEquipe.consultarEquipe(equipe, Boolean.TRUE));
	}

	private Collection<AvisoCorte> getAvisos() throws NegocioException {
		Map<String, Object> filtro = new HashMap<String, Object>();

		PontoConsumo ponto = getPontoConsumo();

		filtro.put("idPontoConsumo", ponto.getChavePrimaria());

		return controladorAvisoCorte.consultarAvisoCorteAcompanhamentoCobranca(filtro);
	}

	private PontoConsumo getPontoConsumo(int referencia, int ciclo) throws NegocioException {

		PontoConsumo ponto = getPontoConsumo();

		Rota rota = ponto.getRota();
		GrupoFaturamento grupo = rota.getGrupoFaturamento();

		grupo.setAnoMesReferencia(referencia);
		grupo.setNumeroCiclo(ciclo);

		return ponto;
	}

	private PontoConsumo getPontoConsumo() throws NegocioException {

		GrupoFaturamento grupo = controladorRota.obterGrupoPorDescricao("GRUPO 01 TESTE AS");
		Rota rota = controladorRota.obterRotaDeGrupoFaturamentoPorNumero("RESIDENCIAL - ROTA 11", grupo);

		return controladorPontoConsumo.consultarPontoConsumoPorRota(rota).stream()
				.filter((ponto) -> DESCRICAO_PONTO_CONSUMO.equals(ponto.getDescricao())).findAny().get();
	}

	private Imovel getImovel(String nome) throws NegocioException {

		Map<String, Object> filtro = new HashMap<>();
		filtro.put("nome", nome);

		return Util.primeiroElemento(controladorImovel.consultarImoveis(filtro));
	}

	private ServicoTipo getServicoTipoSubstituicao() throws NegocioException {
		ServicoTipo servicoTipo = getServicoTipo("");
		servicoTipo.setIndicadorTipoAssociacaoPontoConsumo("1");
		servicoTipo.setIndicadorOperacaoMedidor(String.valueOf(OperacaoMedidor.CODIGO_SUBSTITUICAO));
		return servicoTipo;
	}

	private ServicoTipo getServicoTipo(String descricao) throws NegocioException {
		ServicoTipo servicoTipo = new ServicoTipo();

		if (StringUtils.isNotBlank(descricao)) {
			servicoTipo.setDescricao(descricao);

			servicoTipo = (ServicoTipo) Util
					.primeiroElemento(controladorServicoTipo.consultarServicoTipo(servicoTipo, Boolean.TRUE));
		}

		servicoTipo.setIndicadorClienteObrigatorio(Boolean.FALSE);
		servicoTipo.setIndicadorImovelObrigatorio(Boolean.TRUE);
		servicoTipo.setIndicadorContratoObrigatorio(Boolean.FALSE);
		servicoTipo.setIndicadorPontoConsumoObrigatorio(Boolean.TRUE);
		servicoTipo.setIndicadorTipoAssociacaoPontoConsumo("2");
		servicoTipo.setIndicadorOperacaoMedidor(String.valueOf(OperacaoMedidor.CODIGO_ATIVACAO));
		servicoTipo.setIndicadorEmailCadastrar(Boolean.FALSE);
		servicoTipo.setIndicadorEmailEmExecucao(Boolean.FALSE);
		servicoTipo.setIndicadorEmailExecucao(Boolean.FALSE);
		servicoTipo.setIndicadorEmailEncerrar(Boolean.FALSE);
		servicoTipo.setIndicadorEmailAlterar(Boolean.FALSE);
		servicoTipo.setIndicadorFormularioObrigatorio(Boolean.FALSE);

		return servicoTipo;
	}

	private Map<String, Object> getDados() throws GGASException {
		Map<String, Object> dados = new HashMap<String, Object>();
		dados.put("usuario", getUsuario());
		return dados;
	}

	private Usuario getUsuario() throws NegocioException {
		Map<String, Object> filtro = new HashMap<>();
		filtro.put("nome", NOME_FUNCIONARIO);
		return Util.primeiroElemento(controladorUsuario.consultarUsuario(filtro));
	}

	private DadosAuditoria getDadosAuditoria() throws NegocioException {
		DadosAuditoria dadosAuditoria = (DadosAuditoria) ServiceLocator.getInstancia()
				.getBeanPorID(DadosAuditoria.BEAN_ID_DADOS_AUDITORIA);
		dadosAuditoria.setDataHora(Calendar.getInstance().getTime());
		dadosAuditoria.setIp("127.0.0.1");
		dadosAuditoria.setOperacao(getOperacao());
		dadosAuditoria.setUsuario(getUsuario());

		DadosAuditoriaUtil.setDadosAuditoria(dadosAuditoria);

		return dadosAuditoria;
	}

	private Operacao getOperacao() {
		ConstanteSistema constante = controladorConstanteSistema
				.obterConstantePorCodigo(Constantes.C_OPERACAO_GERAR_SERV_AUT_LOTE);
		return controladorAcesso.obterOperacaoSistema(Long.parseLong(constante.getValor()));
	}
}