
package br.com.ggas.atendimento.servicotipoprioridade;

import java.util.Collection;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.atendimento.servicotipoprioridade.dominio.ServicoTipoPrioridade;
import br.com.ggas.atendimento.servicotipoprioridade.negocio.impl.ControladorServicoTipoPrioridadeImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.constantesistema.ConstanteSistema;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;

/**
 * @author Genival Barbosa
 */
public class TesteServicoTipoPrioridade extends GGASTestCase {
	
	@Autowired
	ControladorServicoTipoPrioridadeImpl controladorServicoTipoPrioridade;

	@Before
	public void setUp() {

		cargaInicial.carregar();
	}

	
	@Test
	@Transactional
	public void testeServicoTipoPrioridade() throws GGASException {

		int qtdInicio = controladorServicoTipoPrioridade.consultarTodos(null).size();
		ServicoTipoPrioridade servicoTipoPrioridade = montarServicoTipoPrioridade();
		super.salvar(servicoTipoPrioridade);
		int qtdFim = controladorServicoTipoPrioridade.consultarTodos(null).size();
		Assert.assertEquals(qtdInicio + 1, qtdFim);
	}
	
	@Test
	@Transactional
	public void testeAtualizarServicoTipoPrioridade() throws GGASException {

		ServicoTipoPrioridade servicoTipoPrioridadeInserida = (ServicoTipoPrioridade) buscar(ServicoTipoPrioridade.class);		

		String descricaoAntesAlteracao = servicoTipoPrioridadeInserida.getDescricao();
		servicoTipoPrioridadeInserida.setDescricao("TESTE");
		getSesstion().update(servicoTipoPrioridadeInserida);

		Collection<ServicoTipoPrioridade> lista = controladorServicoTipoPrioridade.consultarTodos(servicoTipoPrioridadeInserida);
		Assert.assertTrue(lista.size() == 1);

		String descricaoPosAlteracao = ((ServicoTipoPrioridade) lista.iterator().next()).getDescricao();
		Assert.assertNotNull(descricaoPosAlteracao);
		Assert.assertNotEquals(descricaoAntesAlteracao, descricaoPosAlteracao);
	}

	@Test
	@Transactional
	public void testeRemoverServicoTipoPrioridade() throws GGASException {

		ServicoTipoPrioridade servicoTipoPrioridadeInserida = (montarServicoTipoPrioridade());
		super.salvar(servicoTipoPrioridadeInserida);

		int qtdInicio = controladorServicoTipoPrioridade.consultarTodos(null).size();
		getSesstion().delete(servicoTipoPrioridadeInserida);
		int qtdFim = controladorServicoTipoPrioridade.consultarTodos(null).size();
		Assert.assertEquals(qtdInicio - 1, qtdFim);
	}	

	@Test
	@Transactional
	public final void testeConsultarServicoTipoPrioridade() throws GGASException {		

		ServicoTipoPrioridade servicoTipoPrioridadeInserida = (ServicoTipoPrioridade) buscar(ServicoTipoPrioridade.class);
		super.salvar(montarServicoTipoPrioridade());

		Collection<ServicoTipoPrioridade> listaServicoTipoPrioridade = controladorServicoTipoPrioridade.consultarTodos(servicoTipoPrioridadeInserida);

		ServicoTipoPrioridade servicoTipoPrioridadeConsultada = (ServicoTipoPrioridade) listaServicoTipoPrioridade.iterator().next();
		Assert.assertNotNull(servicoTipoPrioridadeConsultada);
		Assert.assertEquals(servicoTipoPrioridadeConsultada.getChavePrimaria(), servicoTipoPrioridadeInserida.getChavePrimaria());
		Assert.assertEquals(servicoTipoPrioridadeConsultada.getDescricao(), servicoTipoPrioridadeInserida.getDescricao());		
	}

	@Test
	@Transactional
	public final void testeConsultarServicoTipoPrioridadePorChave() throws GGASException {

		ServicoTipoPrioridade servicoTipoPrioridadeInserida = montarServicoTipoPrioridade();
		super.salvar(servicoTipoPrioridadeInserida);
		
		ServicoTipoPrioridade servicoTipoPrioridadeConsultada = (ServicoTipoPrioridade) controladorServicoTipoPrioridade.obterServicoTipoPrioridade(servicoTipoPrioridadeInserida.getChavePrimaria());

		Assert.assertNotNull(servicoTipoPrioridadeConsultada);
		Assert.assertEquals(servicoTipoPrioridadeConsultada.getChavePrimaria(), servicoTipoPrioridadeInserida.getChavePrimaria());
		Assert.assertEquals(servicoTipoPrioridadeConsultada.getDescricao(), servicoTipoPrioridadeInserida.getDescricao());		
	}

	private ServicoTipoPrioridade montarServicoTipoPrioridade() throws NumberFormatException, GGASException {
		
		ServicoTipoPrioridade servicoTipoPrioridade = new ServicoTipoPrioridade();
		servicoTipoPrioridade.setDescricao("SERVICOTIPOPRIORIDADE");		
		ConstanteSistema constanteSistemaDiasCorridos = fachada.obterConstantePorCodigo(Constantes.C_VENCIMENTO_DIAS_CORRIDOS);		
		servicoTipoPrioridade.setIndicadorDiaCorridoUtil(fachada.obterEntidadeConteudo(Long.valueOf(constanteSistemaDiasCorridos.getValor())));
		servicoTipoPrioridade.setQuantidadeHorasMaxima(1);
		servicoTipoPrioridade.setUltimaAlteracao(new Date());
		servicoTipoPrioridade.setHabilitado(true);
		servicoTipoPrioridade.setVersao(1);

		return servicoTipoPrioridade;
	}

}
