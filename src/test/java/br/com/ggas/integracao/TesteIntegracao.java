
package br.com.ggas.integracao;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.map.MultiKeyMap;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.auditoria.ControladorAuditoria;
import br.com.ggas.cadastro.imovel.ControladorSegmento;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.cadastro.imovel.Tabela;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.geral.IntegracaoMapeamento;
import br.com.ggas.integracao.geral.IntegracaoSistema;
import br.com.ggas.integracao.geral.impl.ControladorIntegracaoImpl;

public class TesteIntegracao extends GGASTestCase {

	@Autowired
	private ControladorIntegracaoImpl controladorIntegracao;

	@Autowired
	@Qualifier("controladorAuditoria")
	private ControladorAuditoria controladorAuditoria;

	@Autowired
	@Qualifier("controladorSegmento")
	private ControladorSegmento controladorSegmento;

	private final String CODIGO_SEGMENTO_GGAS = "12";

	private final String CODIGO_SEGMENTO_INTEGRANTE = "0101";

	@Test
	@Transactional
	public void consultarMapeamentosPorSistemasIntegrantesComCodigoNumerico() throws GGASException {

		IntegracaoMapeamento integracaoMapeamento = criarIntegracaoMapeamentoDeSegmento();

		controladorIntegracao.inserirIntegracaoSistemaMapeamento(integracaoMapeamento);

		IntegracaoSistema integracaoSistema = integracaoMapeamento.getIntegracaoSistema();

		List<String> siglasSistemasIntegrantes = new ArrayList<>();
		siglasSistemasIntegrantes.add(integracaoSistema.getSigla());

		Segmento segmento = (Segmento) controladorSegmento.criar();
		segmento.setChavePrimaria(Long.valueOf(CODIGO_SEGMENTO_GGAS));

		Long codigoEsperadoSistemaIntegrante = Long.valueOf(integracaoMapeamento.getCodigoEntidadeSistemaIntegrante());

		MultiKeyMap mapeamentosPorIntegrantes = controladorIntegracao
				.consultarMapeamentosPorSistemasIntegrantes(siglasSistemasIntegrantes);

		Long codigoSistemaIntegrante = controladorIntegracao
				.getCodigoEntidadeSistemaIntegrante(mapeamentosPorIntegrantes, segmento, integracaoSistema);

		assertEquals(codigoEsperadoSistemaIntegrante, codigoSistemaIntegrante);
	}

	@Test
	@Transactional
	public void consultarMapeamentosPorSistemasIntegrantesSemCodigo() throws GGASException {

		IntegracaoMapeamento integracaoMapeamento = criarIntegracaoMapeamentoDeSegmento();
		integracaoMapeamento.setCodigoEntidadeSistemaIntegrante(null);

		controladorIntegracao.inserirIntegracaoSistemaMapeamento(integracaoMapeamento);

		IntegracaoSistema integracaoSistema = integracaoMapeamento.getIntegracaoSistema();

		List<String> siglasSistemasIntegrantes = new ArrayList<>();
		siglasSistemasIntegrantes.add(integracaoSistema.getSigla());

		Segmento segmento = (Segmento) controladorSegmento.criar();
		segmento.setChavePrimaria(Long.valueOf(CODIGO_SEGMENTO_GGAS));

		MultiKeyMap mapeamentosPorIntegrantes = controladorIntegracao
				.consultarMapeamentosPorSistemasIntegrantes(siglasSistemasIntegrantes);

		Long codigoSistemaGgas = Long.valueOf(integracaoMapeamento.getCodigoEntidadeSistemaGgas());

		Long codigoSistemaIntegrante = controladorIntegracao
				.getCodigoEntidadeSistemaIntegrante(mapeamentosPorIntegrantes, segmento, integracaoSistema);

		assertEquals(codigoSistemaGgas, codigoSistemaIntegrante);
	}

	@Test
	@Transactional
	public void consultarMapeamentosPorSistemasIntegrantesComCodigoTexto() throws GGASException {

		IntegracaoMapeamento integracaoMapeamento = criarIntegracaoMapeamentoDeSegmento();
		integracaoMapeamento.setCodigoEntidadeSistemaIntegrante("PF");

		controladorIntegracao.inserirIntegracaoSistemaMapeamento(integracaoMapeamento);

		IntegracaoSistema integracaoSistema = integracaoMapeamento.getIntegracaoSistema();

		List<String> siglasSistemasIntegrantes = new ArrayList<>();
		siglasSistemasIntegrantes.add(integracaoSistema.getSigla());

		Segmento segmento = (Segmento) controladorSegmento.criar();
		segmento.setChavePrimaria(Long.valueOf(CODIGO_SEGMENTO_GGAS));

		MultiKeyMap mapeamentosPorIntegrantes = controladorIntegracao
				.consultarMapeamentosPorSistemasIntegrantes(siglasSistemasIntegrantes);

		Long codigoSistemaGgas = Long.valueOf(integracaoMapeamento.getCodigoEntidadeSistemaGgas());

		Long codigoSistemaIntegrante = controladorIntegracao
				.getCodigoEntidadeSistemaIntegrante(mapeamentosPorIntegrantes, segmento, integracaoSistema);

		assertEquals(codigoSistemaGgas, codigoSistemaIntegrante);
	}

	@Test
	@Transactional
	public void buscarCodigoSistemaIntegranteSemCodigo() throws Throwable {

		String metodoPrivado = "buscarCodigoSistemaIntegranteNumerico";

		IntegracaoMapeamento integracaoMapeamento = criarIntegracaoMapeamentoDeSegmento();
		integracaoMapeamento.setCodigoEntidadeSistemaIntegrante(null);

		controladorIntegracao.inserirIntegracaoSistemaMapeamento(integracaoMapeamento);

		Tabela tabela = integracaoMapeamento.getTabela();
		IntegracaoSistema integracaoSistema = integracaoMapeamento.getIntegracaoSistema();

		Long codigoSistemaGgas = Long.valueOf(integracaoMapeamento.getCodigoEntidadeSistemaGgas());

		Long codigoSistemaIntegrante = (Long) super.chamarMetodoPrivado(controladorIntegracao, metodoPrivado,
				codigoSistemaGgas, tabela.getNomeClasse(), integracaoSistema.getSigla());

		assertEquals(codigoSistemaGgas, codigoSistemaIntegrante);
	}

	@Test
	@Transactional
	public void buscarCodigoSistemaIntegrante() throws Throwable {

		String metodoPrivado = "buscarCodigoSistemaIntegranteNumerico";

		IntegracaoMapeamento integracaoMapeamento = criarIntegracaoMapeamentoDeSegmento();

		controladorIntegracao.inserirIntegracaoSistemaMapeamento(integracaoMapeamento);

		Tabela tabela = integracaoMapeamento.getTabela();
		IntegracaoSistema integracaoSistema = integracaoMapeamento.getIntegracaoSistema();

		Long codigoSistemaGgas = Long.valueOf(integracaoMapeamento.getCodigoEntidadeSistemaGgas());
		Long codigoEsperadoSistemaIntegrante = Long.valueOf(integracaoMapeamento.getCodigoEntidadeSistemaIntegrante());

		Long codigoSistemaIntegrante = (Long) super.chamarMetodoPrivado(controladorIntegracao, metodoPrivado,
				codigoSistemaGgas, tabela.getNomeClasse(), integracaoSistema.getSigla());

		assertEquals(codigoEsperadoSistemaIntegrante, codigoSistemaIntegrante);
	}

	@Test
	@Transactional
	public void buscarCodigoSistemaIntegranteComCodigoTexto() throws Throwable {

		String metodoPrivado = "buscarCodigoSistemaIntegranteNumerico";

		IntegracaoMapeamento integracaoMapeamento = criarIntegracaoMapeamentoDeSegmento();
		integracaoMapeamento.setCodigoEntidadeSistemaIntegrante("PF");

		controladorIntegracao.inserirIntegracaoSistemaMapeamento(integracaoMapeamento);

		Tabela tabela = integracaoMapeamento.getTabela();
		IntegracaoSistema integracaoSistema = integracaoMapeamento.getIntegracaoSistema();

		Long codigoSistemaGgas = Long.valueOf(integracaoMapeamento.getCodigoEntidadeSistemaGgas());

		Long codigoSistemaIntegrante = (Long) super.chamarMetodoPrivado(controladorIntegracao, metodoPrivado,
				codigoSistemaGgas, tabela.getNomeClasse(), integracaoSistema.getSigla());

		assertEquals(codigoSistemaGgas, codigoSistemaIntegrante);
	}

	public IntegracaoMapeamento criarIntegracaoMapeamentoDeSegmento() throws GGASException {

		IntegracaoMapeamento integracaoSegmento = controladorIntegracao.criarIntegracaoMapeamento();

		integracaoSegmento.setCodigoEntidadeSistemaGgas(CODIGO_SEGMENTO_GGAS);
		integracaoSegmento.setCodigoEntidadeSistemaIntegrante(CODIGO_SEGMENTO_INTEGRANTE);

		IntegracaoSistema integracaoSistema = controladorIntegracao.obterIntegracaoSistema(1l);
		integracaoSegmento.setIntegracaoSistema(integracaoSistema);

		Tabela tabelaSegmento = controladorAuditoria.consultarTabelaPorNomeDaClasse(Segmento.class.getName());
		integracaoSegmento.setTabela(tabelaSegmento);

		return integracaoSegmento;
	}

}
