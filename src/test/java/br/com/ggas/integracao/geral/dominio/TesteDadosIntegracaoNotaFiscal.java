package br.com.ggas.integracao.geral.dominio;

import java.io.IOException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.contrato.contrato.Contrato;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.contrato.contrato.impl.ContratoImpl;
import br.com.ggas.faturamento.DocumentoFiscal;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.impl.DocumentoFiscalImpl;
import br.com.ggas.faturamento.impl.FaturaImpl;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;

public class TesteDadosIntegracaoNotaFiscal extends GGASTestCase {

	@Autowired
	@Qualifier(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO)
	private ControladorContrato controladorContrato;
	
	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_DADOS_INTEGRACAO_NOTA_FISCAL.sql";
	

	
	@Before
	@Transactional
	public void setup() throws GGASException, IOException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}
		

	@Test
	@Transactional
	public void testContrato() throws NegocioException {
		DadosIntegracaoNotaFiscal dados = new DadosIntegracaoNotaFiscal();
		
		Contrato contrato = dados.obterContrato(getDocumentoFiscal());
		Assert.assertNotNull(contrato);
		Assert.assertEquals(contrato.getChavePrimaria(), 99999);
		
	}
	
	@Test
	@Transactional
	public void testContratoPai() throws NegocioException {
		DadosIntegracaoNotaFiscal dados = new DadosIntegracaoNotaFiscal();
		
		Contrato contrato = dados.obterContrato(getDocumentoFiscalComContratoPai());
		Assert.assertNotNull(contrato);
		Assert.assertEquals(contrato.getChavePrimaria(), 99997);
		
	}
	
	@Test(expected = NegocioException.class)
	@Transactional
	public void testContratoNulo() throws NegocioException {
		DadosIntegracaoNotaFiscal dados = new DadosIntegracaoNotaFiscal();
		
		dados.obterContrato(getDocumentoFiscalComContratoNulo());
		
	}
	
	private DocumentoFiscal getDocumentoFiscal() {
		Contrato contrato = new ContratoImpl();
		contrato.setChavePrimaria(99999);
		
		Fatura fatura = new FaturaImpl();
		fatura.setContrato(contrato);
		fatura.setContratoAtual(contrato);

		DocumentoFiscal documento = new DocumentoFiscalImpl();
		documento.setFatura(fatura);
		return documento;
	}
	
	private DocumentoFiscal getDocumentoFiscalComContratoPai() {
		Contrato contrato = new ContratoImpl();
		contrato.setChavePrimaria(99997);
		contrato.setChavePrimariaPai(Long.valueOf(99998));
		
		Fatura fatura = new FaturaImpl();
		fatura.setContrato(contrato);
		fatura.setContratoAtual(contrato);

		DocumentoFiscal documento = new DocumentoFiscalImpl();
		documento.setFatura(fatura);
		return documento;
	}
	
	private DocumentoFiscal getDocumentoFiscalComContratoNulo() {
		Contrato contrato = new ContratoImpl();
		
		Fatura fatura = new FaturaImpl();
		fatura.setContrato(contrato);
		fatura.setContratoAtual(contrato);

		DocumentoFiscal documento = new DocumentoFiscalImpl();
		documento.setFatura(fatura);
		return documento;
	}

	
}
