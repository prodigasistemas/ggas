
package br.com.ggas.contabil;

import static org.easymock.EasyMock.createMockBuilder;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.map.MultiKeyMap;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.arrecadacao.ContaBancaria;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.contabil.impl.ControladorContabilImpl;
import br.com.ggas.faturamento.LancamentoItemContabil;
import br.com.ggas.faturamento.tributo.Tributo;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;

public class TesteContabil extends GGASTestCase {

	@Autowired
	@Qualifier("controladorContabil")
	private ControladorContabil controladorContabil;

	private final int VALOR_LANCAMENTO_CONTABIL = 2000000;

	private final int ESCALA_VALOR_CONTABIL = 2;

	@Test
	public void testeObterEventoComercialLancamentoPorAtributosValidos() {

		ControladorContabil controladorContabil = mockControladorContabil(ControladorContabilImpl.class);

		EventoComercialLancamento eventoComercialLancamento = criarEventoComercialLancamento();
		Segmento segmento = eventoComercialLancamento.getSegmento();

		expect(controladorContabil.consultarEventoComercialLancamentoPorSegmento(segmento.getChavePrimaria()))
				.andReturn(Arrays.asList(eventoComercialLancamento));
		replay(controladorContabil);

		MultiKeyMap eventoComercialLancamentoPorAtributos = controladorContabil
				.agruparEventoComercialLancamentoPorSegmento(segmento.getChavePrimaria());

		LancamentoContabilVO lancamentoContabilVO = new LancamentoContabilVO();
		lancamentoContabilVO.setEventoComercial(eventoComercialLancamento.getEventoComercial());
		lancamentoContabilVO.setTributo(eventoComercialLancamento.getTributo());
		lancamentoContabilVO.setLancamentoItemContabil(eventoComercialLancamento.getLancamentoItemContabil());
		lancamentoContabilVO.setContaBancaria(eventoComercialLancamento.getContaBancaria());

		EventoComercialLancamento eventoComercialLancamentoObtido = controladorContabil
				.obterEventoComercialLancamentoPorAtributosMapeados(lancamentoContabilVO,
						eventoComercialLancamentoPorAtributos);
		assertEquals(eventoComercialLancamento, eventoComercialLancamentoObtido);
		assertEquals(eventoComercialLancamento.getSegmento(), eventoComercialLancamentoObtido.getSegmento());
	}

	@Test
	public void testeObterEventoComercialLancamentoPorAtributosNulos() {

		ControladorContabil controladorContabil = mockControladorContabil(ControladorContabilImpl.class);

		EventoComercialLancamento eventoComercialLancamento = criarEventoComercialLancamento();
		eventoComercialLancamento.setContaBancaria(null);

		Segmento segmento = eventoComercialLancamento.getSegmento();

		expect(controladorContabil.consultarEventoComercialLancamentoPorSegmento(segmento.getChavePrimaria()))
				.andReturn(Arrays.asList(eventoComercialLancamento));
		replay(controladorContabil);

		MultiKeyMap eventoComercialLancamentoPorAtributos = controladorContabil
				.agruparEventoComercialLancamentoPorSegmento(segmento.getChavePrimaria());

		LancamentoContabilVO lancamentoContabilVO = new LancamentoContabilVO();
		lancamentoContabilVO.setEventoComercial(eventoComercialLancamento.getEventoComercial());
		lancamentoContabilVO.setTributo(eventoComercialLancamento.getTributo());
		lancamentoContabilVO.setLancamentoItemContabil(eventoComercialLancamento.getLancamentoItemContabil());
		lancamentoContabilVO.setContaBancaria(eventoComercialLancamento.getContaBancaria());

		EventoComercialLancamento eventoComercialLancamentoObtido = controladorContabil
				.obterEventoComercialLancamentoPorAtributosMapeados(lancamentoContabilVO,
						eventoComercialLancamentoPorAtributos);
		assertEquals(eventoComercialLancamento, eventoComercialLancamentoObtido);
		assertEquals(eventoComercialLancamento.getSegmento(), eventoComercialLancamentoObtido.getSegmento());
	}

	@Test
	@Transactional
	public void verificarValorAcumuladoDeContabilizacoesAtualizadas() throws NegocioException, ConcorrenciaException {

		BigDecimal valorAcumulado = BigDecimal.valueOf(16000000, ESCALA_VALOR_CONTABIL);

		LancamentoContabilVO lancamentoContabilVO = criarLancamentoContabilVO();

		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(criarLancamentoContabilAnaliticoVO());
		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(criarLancamentoContabilAnaliticoVO());

		Map<Long, LancamentoContabilSintetico> contabilizacoesAcumuladas = new HashMap<>();

		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);

		lancamentoContabilVO
				.setDataRealizacaoEvento(DataUtil.incrementarDia(lancamentoContabilVO.getDataRealizacaoEvento(), 1));
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);

		lancamentoContabilVO
				.setDataRealizacaoEvento(DataUtil.incrementarDia(lancamentoContabilVO.getDataRealizacaoEvento(), 1));
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);

		controladorContabil.atualizarContabilizacoesAcumuladas(contabilizacoesAcumuladas);

		for (Entry<Long, LancamentoContabilSintetico> entry : contabilizacoesAcumuladas.entrySet()) {

			LancamentoContabilSintetico lancamentoContabilSintetico = entry.getValue();

			assertTrue(lancamentoContabilSintetico.getChavePrimaria() > 0);
			assertEquals(valorAcumulado, lancamentoContabilSintetico.getValor());
			assertEquals(lancamentoContabilVO.getContaAuxiliarCredito(),
					lancamentoContabilSintetico.getContaAuxiliarCredito());
			assertEquals(lancamentoContabilVO.getContaAuxiliarDebito(),
					lancamentoContabilSintetico.getContaAuxiliarDebito());
			assertTrue(!lancamentoContabilSintetico.isIndicadorIntegrado());
		}
	}

	@Test
	@Transactional
	public void verificarLancamentoContabilAnaliticoDeContabilizacoesAtualizadas()
			throws NegocioException, ConcorrenciaException {

		LancamentoContabilVO lancamentoContabilVO = criarLancamentoContabilVO();

		Map<Long, LancamentoContabilSintetico> contabilizacoesAcumuladas = new HashMap<>();

		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().clear();
		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(criarLancamentoContabilAnaliticoVO());
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);

		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(criarLancamentoContabilAnaliticoVO());
		lancamentoContabilVO.getListaLancamentosContabeisAnaliticos().add(criarLancamentoContabilAnaliticoVO());
		controladorContabil.contabilizarLancamentoContabilSintetico(contabilizacoesAcumuladas, lancamentoContabilVO);

		controladorContabil.atualizarContabilizacoesAcumuladas(contabilizacoesAcumuladas);

		LinkedList<LancamentoContabilAnaliticoVO> lancamentosContabeisAnaliticosVOs = new LinkedList<>(
				lancamentoContabilVO.getListaLancamentosContabeisAnaliticos());

		Map<String, Object> mapaParametros = controladorContabil.montarFiltroPesquisaAtributosUnicos(
				lancamentoContabilVO, new Date(), lancamentoContabilVO.getDataRealizacaoEvento());

		Collection<LancamentoContabilSintetico> colecaoLancamentoContabilSintetico = controladorContabil
				.consultarLancamentoContabilSinteticoInserir(mapaParametros);

		LancamentoContabilSintetico lancamentoContabilSintetico = colecaoLancamentoContabilSintetico.iterator().next();

		Collection<LancamentoContabilAnalitico> lancamentosContabeisAnaliticos = lancamentoContabilSintetico
				.getListaLancamentoContabilAnalitico();

		LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVO = lancamentosContabeisAnaliticosVOs.getFirst();

		assertEquals(lancamentosContabeisAnaliticosVOs.size(), lancamentosContabeisAnaliticos.size());

		for (LancamentoContabilAnalitico lancamentoContabilAnalitico : lancamentosContabeisAnaliticos) {
			assertTrue(lancamentoContabilAnalitico.getChavePrimaria() > 0);
			assertEquals(lancamentoContabilAnaliticoVO.getCodigoObjeto(),
					lancamentoContabilAnalitico.getCodigoObjeto());
			assertEquals(lancamentoContabilAnaliticoVO.getValor(), lancamentoContabilAnalitico.getValor());
		}

	}

	private ControladorContabil mockControladorContabil(Class<?> mockClass) {

		String mockedMethod = "consultarEventoComercialLancamentoPorSegmento";

		return (ControladorContabil) createMockBuilder(mockClass).addMockedMethod(mockedMethod).createMock();
	}

	private EventoComercialLancamento criarEventoComercialLancamento() {

		EventoComercialLancamento eventoComercialLancamento = controladorContabil.criarEventoComercialLancamento();
		eventoComercialLancamento.setChavePrimaria(1L);
		eventoComercialLancamento.setContaBancaria(criarContaBancaria());
		eventoComercialLancamento.setEventoComercial(criarEventoComercial());
		eventoComercialLancamento.setTributo(criarTributo());
		eventoComercialLancamento.setSegmento(criarSegmento());
		eventoComercialLancamento.setLancamentoItemContabil(criarLancamentoItemContabil());

		return eventoComercialLancamento;

	}

	private LancamentoItemContabil criarLancamentoItemContabil() {

		LancamentoItemContabil itemContabil = (LancamentoItemContabil) ServiceLocator.getInstancia()
				.getBeanPorID(LancamentoItemContabil.BEAN_ID_LANCAMENTO_ITEM_CONTABIL);
		itemContabil.setChavePrimaria(1L);
		return itemContabil;
	}

	private Segmento criarSegmento() {

		Segmento segmento = (Segmento) ServiceLocator.getInstancia().getBeanPorID(Segmento.BEAN_ID_SEGMENTO);
		segmento.setChavePrimaria(1L);
		return segmento;
	}

	private Tributo criarTributo() {

		Tributo tributo = (Tributo) ServiceLocator.getInstancia().getBeanPorID(Tributo.BEAN_ID_TRIBUTO);
		tributo.setChavePrimaria(1L);
		return tributo;
	}

	private EventoComercial criarEventoComercial() {

		EventoComercial eventoComercial = controladorContabil.criarEventoComercial();
		eventoComercial.setChavePrimaria(1L);
		return eventoComercial;
	}

	private ContaBancaria criarContaBancaria() {

		ContaBancaria contaBancaria = (ContaBancaria) ServiceLocator.getInstancia()
				.getBeanPorID(ContaBancaria.BEAN_ID_CONTA_BANCARIA);
		contaBancaria.setChavePrimaria(1L);
		return contaBancaria;
	}

	private LancamentoContabilVO criarLancamentoContabilVO() {

		LancamentoContabilVO lancamentoContabilVO = new LancamentoContabilVO();

		Date hoje = new Date();

		lancamentoContabilVO.setContaAuxiliarCredito("011");
		lancamentoContabilVO.setContaAuxiliarDebito("007");
		lancamentoContabilVO.setDataRealizacaoEvento(hoje);

		EventoComercialLancamento eventoComercialLancamento = controladorContabil.criarEventoComercialLancamento();

		EventoComercial eventoComercial = criarEventoComercial();

		ContaContabil contaContabil = criarContaContabil();
		eventoComercialLancamento.setContaContabilCredito(contaContabil);
		eventoComercialLancamento.setContaContabilDebito(contaContabil);

		super.salvar(contaContabil);

		eventoComercialLancamento.setContaContabilCredito(contaContabil);
		eventoComercialLancamento.setEventoComercial(eventoComercial);
		eventoComercialLancamento.setContaContabilDebito(contaContabil);
		eventoComercialLancamento.setIndicadorRegimeContabilCompetencia(Boolean.TRUE);

		super.salvar(eventoComercialLancamento);

		lancamentoContabilVO.setEventoComercialLancamento(eventoComercialLancamento);
		lancamentoContabilVO.setEventoComercial(eventoComercialLancamento.getEventoComercial());

		return lancamentoContabilVO;
	}

	private ContaContabil criarContaContabil() {
		ContaContabil contaContabil = (ContaContabil) ServiceLocator.getInstancia()
				.getBeanPorID(ContaContabil.BEAN_ID_CONTA_CONTABIL);
		contaContabil.setNomeConta("CAIXA ECONOMICA FEDERAL");
		contaContabil.setNumeroConta("1.1.2.1.02.002");
		return contaContabil;
	}

	private LancamentoContabilAnaliticoVO criarLancamentoContabilAnaliticoVO() {
		LancamentoContabilAnaliticoVO lancamentoContabilAnaliticoVO = new LancamentoContabilAnaliticoVO();
		lancamentoContabilAnaliticoVO.setValor(BigDecimal.valueOf(VALOR_LANCAMENTO_CONTABIL, ESCALA_VALOR_CONTABIL));
		lancamentoContabilAnaliticoVO.setCodigoObjeto(14655L);
		return lancamentoContabilAnaliticoVO;
	}

}
