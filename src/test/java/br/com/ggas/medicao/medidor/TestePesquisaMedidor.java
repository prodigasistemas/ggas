
package br.com.ggas.medicao.medidor;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.operacional.MedidorLocalArmazenagem;
import br.com.ggas.cadastro.operacional.impl.MedidorLocalArmazenagemImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.leitura.ControladorInstalacaoMedidor;
import br.com.ggas.medicao.leitura.InstalacaoMedidor;
import br.com.ggas.web.medicao.medidor.MedidorAction;

/**
 * Classe de teste do Medidor
 * 
 * @author luiz.neri <luiz.neri@sergipegas.com.br>
 */
public class TestePesquisaMedidor
				extends GGASTestCase {

	@Autowired
	ControladorMedidor controladorMedidor;
	
	@Autowired
	ControladorInstalacaoMedidor controladorInstalacaoMedidor;

	@Before
	@Transactional
	public void setup() throws GGASException {

		super.cargaInicial.carregar();

		TipoMedidor tipoMedidor = (TipoMedidor) super.buscar(TipoMedidor.class);
		MarcaMedidor marcaMedidor = this.insereBuscaMarcaMedidor();
		CapacidadeMedidor capacidadeMedidor = (CapacidadeMedidor) super.buscar(CapacidadeMedidor.class);
		SituacaoMedidor situacaoMedidor = (SituacaoMedidor) super.buscar(SituacaoMedidor.class);
		MedidorLocalArmazenagem localArmazenagem = this.inserirLocalArmazenagem("teste_aaa", "a");

		// Salvando diversos medidores
		this.inserirMedidor(localArmazenagem, tipoMedidor, marcaMedidor, capacidadeMedidor, capacidadeMedidor, situacaoMedidor);
		this.inserirMedidor(localArmazenagem, tipoMedidor, marcaMedidor, capacidadeMedidor, capacidadeMedidor, situacaoMedidor);
		this.inserirMedidor(localArmazenagem, tipoMedidor, marcaMedidor, capacidadeMedidor, capacidadeMedidor, situacaoMedidor);

		localArmazenagem = this.inserirLocalArmazenagem("teste_bbb", "b");
		this.inserirMedidor(localArmazenagem, tipoMedidor, marcaMedidor, capacidadeMedidor, capacidadeMedidor, situacaoMedidor);
	}

	/**
	 * Teste que pesquisa medidores por local de armazenagem e verifica se a quantidade encontrada é a mesma da quantidade previamente cadastrada.
	 * 
	 * @throws GGASException
	 */
	@Test
	@Transactional
	public void testePesquisarMedidorPorLocalArmazenagem() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idLocalArmazenagem", this.buscarUmLocalArmazenagemPorDescricao("teste_aaa").getChavePrimaria());

		Collection<Medidor> medidores = controladorMedidor.consultarMedidor(filtro);
		Assert.assertEquals(3, medidores.size());

		filtro = new HashMap<String, Object>();
		filtro.put("idLocalArmazenagem", this.buscarUmLocalArmazenagemPorDescricao("teste_bbb").getChavePrimaria());
		medidores = controladorMedidor.consultarMedidor(filtro);

		Assert.assertEquals(1, medidores.size());
	}
	
	@Test
	@Transactional
	public void testeRetornarUltimoMedidorInstalacao() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idLocalArmazenagem", this.buscarUmLocalArmazenagemPorDescricao("teste_aaa").getChavePrimaria());

		Collection<Medidor> medidores = controladorMedidor.consultarMedidor(filtro);
		MedidorAction medidorAction = new MedidorAction();
		Medidor medidor = medidores.iterator().next();
		InstalacaoMedidor instalacaoMedidor = (InstalacaoMedidor) controladorInstalacaoMedidor.criar();
		medidor.setInstalacaoMedidor(instalacaoMedidor);
		
		medidorAction.retornarUltimoMedidorInstalacao(medidor);
	}

	/**
	 * insere e busca uma marca de medidor qualquer
	 * 
	 * @return
	 * @throws GGASException
	 */
	private MarcaMedidor insereBuscaMarcaMedidor() throws GGASException {

		MarcaMedidor marcaMedidor = controladorMedidor.criarMarcaMedidor();
		marcaMedidor.setDescricao("MARCA MEDIDOR TESTE");
		marcaMedidor.setDescricaoAbreviada("ABVR");
		super.salvar(marcaMedidor);

		return fachada.listarMarcaMedidor().iterator().next();
	}

	/**
	 * Insere novo local de armazenagem medidor
	 * 
	 * @param descricao
	 * @param descricaoAbreviada
	 * @return
	 */
	private MedidorLocalArmazenagem inserirLocalArmazenagem(String descricao, String descricaoAbreviada) {

		MedidorLocalArmazenagem localArmazenagem = new MedidorLocalArmazenagemImpl();// Novo local de armazenagem
		localArmazenagem.setDescricao(descricao);
		localArmazenagem.setDescricaoAbreviada(descricaoAbreviada);
		localArmazenagem.setHabilitado(Boolean.TRUE);
		super.salvar(localArmazenagem);

		return localArmazenagem;
	}

	/**
	 * Busca apenas um local de armazenagem através da descrição passada por parâmetro
	 * 
	 * @param descricao valor a ser procurado
	 	InstalacaoMedidor medInstalacao = null;* @return O primeiro local armazenagem encontrado
	 * @throws GGASException
	 */
	private MedidorLocalArmazenagem buscarUmLocalArmazenagemPorDescricao(String descricao) throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("descricao", descricao);
		return this.fachada.consultarMedidorLocalArmazenagem(filtro).iterator().next();
	}

	/**
	 * Insere novo medidor
	 * 
	 * @param localArmazenagem Local de Armazenagem
	 * @throws GGASException
	 */
	private void inserirMedidor(MedidorLocalArmazenagem localArmazenagem, TipoMedidor tipoMedidor, MarcaMedidor marcaMedidor,
					CapacidadeMedidor capacidadeMinimaMedidor, CapacidadeMedidor capacidadeMaximaMedidor, SituacaoMedidor situacaoMedidor)
					throws GGASException {

		Medidor medidor = (Medidor) controladorMedidor.criar();
		
		medidor.setNumeroSerie(UUID.randomUUID().toString().substring(0, 5));
		medidor.setTipoMedidor(tipoMedidor);
		medidor.setMarcaMedidor(marcaMedidor);
		medidor.setCapacidadeMinima(capacidadeMinimaMedidor);
		medidor.setCapacidadeMaxima(capacidadeMaximaMedidor);
		medidor.setDataAquisicao(new Date());
		medidor.setDigito(5);
		medidor.setLocalArmazenagem(localArmazenagem);
		medidor.setSituacaoMedidor(situacaoMedidor);
		
		super.salvar(medidor);
	}
}
