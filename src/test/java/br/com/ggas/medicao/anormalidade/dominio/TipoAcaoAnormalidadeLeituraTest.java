package br.com.ggas.medicao.anormalidade.dominio;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

/**
 * Testes unitários para identificacao do tipo de ação de anormalidade de leitura a partir do seu codigo
 */
public class TipoAcaoAnormalidadeLeituraTest {

	@Test
	public void testFromCodigoAcaoNormal() {
		assertThat(TipoAcaoAnormalidadeLeitura.fromCodigoAcao(1), equalTo(TipoAcaoAnormalidadeLeitura.NORMAL));
	}

	@Test
	public void testFromCodigoAcaoInformada() {
		assertThat(TipoAcaoAnormalidadeLeitura.fromCodigoAcao(2), equalTo(TipoAcaoAnormalidadeLeitura.INFORMADA));
	}

	@Test
	public void testFromCodigoAcaoAnterior() {
		assertThat(TipoAcaoAnormalidadeLeitura.fromCodigoAcao(3), equalTo(TipoAcaoAnormalidadeLeitura.ANTERIOR));
	}

	@Test
	public void testFromCodigoAcaoAnteriorSomadaAoConsumo() {
		assertThat(TipoAcaoAnormalidadeLeitura.fromCodigoAcao(4), equalTo(TipoAcaoAnormalidadeLeitura.ANTERIOR_SOMADO_AO_CONSUMO));
	}

	@Test
	public void testFromCodigoAcaoAnteriorSomadaAMedia() {
		assertThat(TipoAcaoAnormalidadeLeitura.fromCodigoAcao(5), equalTo(TipoAcaoAnormalidadeLeitura.ANTERIOR_SOMADO_A_MEDIA));
	}

	@Test
	public void testFromCodigoNaoIdentificado() {
		assertNull(TipoAcaoAnormalidadeLeitura.fromCodigoAcao(0));
	}
}
