/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.medicao.anormalidade.negocio.acaoleitura;

import br.com.ggas.comum.GGASTestSpring;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.medicao.consumo.impl.HistoricoConsumoImpl;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.leitura.impl.HistoricoMedicaoImpl;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author jose.victor@logiquesistemas.com.br
 */
public class AcaoLeituraAnteriorSomadoMediaTest extends GGASTestSpring {

	@Autowired
	@TestSubject
	private AcaoLeituraAnteriorSomadoMedia acaoLeituraAnteriorSomadoMedia;

	@Mock
	private ControladorParametroSistema controladorParametroSistema;

	@Before
	public void before() throws NegocioException {
		expect(controladorParametroSistema.obterValorDoParametroPorCodigo(anyString())).andReturn("1");
		replay(controladorParametroSistema);
	}

	@Test
	public void testAcaoLeituraAnteriorSomadoMedia() throws NegocioException {
		HistoricoMedicao historicoMedicao = new HistoricoMedicaoImpl();
		HistoricoConsumo historicoConsumo = new HistoricoConsumoImpl();

		historicoConsumo.setHistoricoAtual(historicoMedicao);
		historicoConsumo.setConsumoMedio(BigDecimal.valueOf(34.5d));
		historicoMedicao.setNumeroLeituraAnterior(BigDecimal.valueOf(35.6));
		historicoConsumo.setConsumo(BigDecimal.valueOf(31.4));

		final BigDecimal retorno = acaoLeituraAnteriorSomadoMedia.executarAcaoLeituraInformada(historicoConsumo);
		final BigDecimal esperado = BigDecimal.valueOf(70.1);

		assertThat(retorno, equalTo(esperado));
	}
}
