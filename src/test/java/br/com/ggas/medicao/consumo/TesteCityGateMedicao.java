/**
 * 
 */

package br.com.ggas.medicao.consumo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.operacional.CityGate;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.impl.PlanilhaVolumePcsVO;

/**
 * @author Rafael Bruno
 */
public class TesteCityGateMedicao extends GGASTestCase {

	@Autowired
	ControladorCityGateMedicao controladorCityGateMedicao;	

	@Before
	@Transactional
	public void setUp() throws GGASException {

		super.cargaInicial.carregar();
		this.inserirCityGate();
	}

	// ------------------------------------------------------------------------------------
	// Métodos de testes para testar a inclusão de City Gate Medicao
	// ------------------------------------------------------------------------------------

	@Test
	@Transactional
	public void testeInserirCityGateMedicaoCamposObrigatorios() throws GGASException {

		CityGateMedicao cityGateMedicao = fachada.criarCityGateMedicao();

		try {
			cityGateMedicao.setCityGate(null);
			cityGateMedicao.setDadosAuditoria(null);
			controladorCityGateMedicao.inserir(cityGateMedicao);
			
			Assert.assertTrue(Boolean.FALSE);
		} catch (NegocioException e) {
			Assert.assertTrue(Boolean.TRUE);
		}
	}

	@Test
	@Transactional
	public void testeInserirCityGateMedicaoSucesso() throws GGASException {

		int qtdInicio = controladorCityGateMedicao.obterTodas().size();

		inserirCityGateMedicao();

		int qtdFim = controladorCityGateMedicao.obterTodas().size();
		Assert.assertEquals(qtdInicio + 1, qtdFim);
	}

	// ------------------------------------------------------------------------------------
	// Métodos de testes para testar a alteração de City Gate Medicao
	// ------------------------------------------------------------------------------------

	@Test
	@Transactional
	public void testeAtualizarCityGateMedicaoSucesso() throws Throwable {

		CityGateMedicao cityGateMedicao = inserirCityGateMedicao();
		Date DataAntesAlteracao = cityGateMedicao.getDataMedicao();

		cityGateMedicao.setDataMedicao(Calendar.getInstance().getTime());
		cityGateMedicao.setCityGate((CityGate) buscar(CityGate.class));
		getSesstion().update(cityGateMedicao);

		CityGateMedicao cityGateMedicaoAlterado = (CityGateMedicao) controladorCityGateMedicao.obter(cityGateMedicao.getChavePrimaria());
		Date DataPosAlteracao = cityGateMedicaoAlterado.getDataMedicao();

		Assert.assertNotEquals(DataAntesAlteracao, DataPosAlteracao);

	}

	// ------------------------------------------------------------------------------------
	// Métodos de testes para testar a remoção de City Gate Medicao
	// ------------------------------------------------------------------------------------

	@Test
	@Transactional
	public void testeRemoverCityGateMedicaoSucesso() throws Throwable {

		CityGateMedicao cityGateMedicao = inserirCityGateMedicao();

		int qtdInicio = controladorCityGateMedicao.obterTodas().size();
		controladorCityGateMedicao.remover(cityGateMedicao);
		int qtdFim = controladorCityGateMedicao.obterTodas().size();

		Assert.assertEquals(qtdInicio - 1, qtdFim);
	}

	// ------------------------------------------------------------------------------------
	// Métodos de testes para testar a consulta de city gate medicao por chave
	// ------------------------------------------------------------------------------------

	@Test
	@Transactional
	public void testeBuscarCityGateMedicaoPorChaveSucesso() throws GGASException {

		long chave = inserirCityGateMedicao().getChavePrimaria();
		CityGateMedicao cityGateMedicao = (CityGateMedicao) controladorCityGateMedicao.obter(chave);
		Assert.assertEquals(chave, cityGateMedicao.getChavePrimaria());
	}

	@Test
	@Transactional
	public void testeBuscarCityGateMedicaoPorChaveFalha() throws GGASException {

		long chave = 9999L;
		try {
			controladorCityGateMedicao.obter(chave);
			Assert.assertTrue(Boolean.FALSE);
		} catch (NegocioException e) {
			Assert.assertTrue(Boolean.TRUE);
		}
	}

	// ------------------------------------------------------------------------------------
	// Métodos de testes para testar a consulta de City Gate Medicao por filtro
	// ------------------------------------------------------------------------------------

	@Test
	@Transactional
	public void testeBuscarCityGateMedicaoPorCityGateEData() throws GGASException {

		CityGate cityGate = fachada.criarCityGate();
		cityGate.setDescricao("Meu city gate");
		super.salvar(cityGate);

		Date data = Calendar.getInstance().getTime();

		CityGateMedicao cityGateMedicao = fachada.criarCityGateMedicao();
		cityGateMedicao.setCityGate(cityGate);
		cityGateMedicao.setDataMedicao(data);

		super.salvar(cityGateMedicao);

		Assert.assertNotEquals(0, controladorCityGateMedicao.consultarCityGateMedicaoPorCityGateEData(cityGate, data).size());

	}

	@Test
	@Transactional
	public void testeBuscarCityGateMedicaoPorDataSucesso() throws GGASException {

		CityGate cityGate = (CityGate) buscar(CityGate.class);

		Date data = Calendar.getInstance().getTime();

		CityGateMedicao cityGateMedicao = fachada.criarCityGateMedicao();
		cityGateMedicao.setCityGate(cityGate);
		cityGateMedicao.setDataMedicao(data);

		super.salvar(cityGateMedicao);

		Assert.assertNotEquals(0, controladorCityGateMedicao.listarCityGateMedicaoPorData(data).size());

	}

	@Test
	@Transactional
	public void testeBuscarCityGateMedicaoPorDataFalha() throws GGASException {

		CityGate cityGate = (CityGate) buscar(CityGate.class);

		Date data = Calendar.getInstance().getTime();
		Calendar dataAux = Calendar.getInstance();
		dataAux.add(Calendar.DAY_OF_MONTH, 1);
		
		Date dataAlterada = dataAux.getTime();

		CityGateMedicao cityGateMedicao = fachada.criarCityGateMedicao();
		cityGateMedicao.setCityGate(cityGate);
		cityGateMedicao.setDataMedicao(data);

		super.salvar(cityGateMedicao);

		Assert.assertEquals(0, controladorCityGateMedicao.listarCityGateMedicaoPorData(dataAlterada).size());

	}
	
	@Test
	@Transactional
	public void testeImportarPlanilhaExcel() throws GGASException {
		// TODO
		assertTrue(Boolean.TRUE);
	}
	
	@Test
	@Transactional
	public void testeExtrairPeriodoCalendar() throws GGASException {
		//Given 
		String mes = "Nov";
		String ano = "2018";
		PlanilhaVolumePcsVO planilha = new PlanilhaVolumePcsVO(mes, ano);
		SimpleDateFormat
		formatoMes = new SimpleDateFormat("MMM yyyy", new Locale("pt", "BR"));
		//Then
		Calendar calendarExtraido = controladorCityGateMedicao.extrairPeriodoCalendar(planilha);
		String mesAno = formatoMes.format(calendarExtraido.getTime());
		// TODO
		assertEquals(mesAno, mes.toLowerCase() + " " + ano);
	}
	

	// Metodos Privados

	private void inserirCityGate() throws GGASException {

		CityGate cityGate = (CityGate) controladorCityGateMedicao.criarCityGate();
		cityGate.setDescricao("City Gate");

		super.salvar(cityGate);

	}

	@Transactional
	private CityGateMedicao inserirCityGateMedicao() throws GGASException {

		CityGateMedicao cityGateMedicao = (CityGateMedicao) controladorCityGateMedicao.criar();
		cityGateMedicao.setCityGate((CityGate) super.buscar(CityGate.class));
		cityGateMedicao.setDataMedicao(Calendar.getInstance().getTime());

		super.salvar(cityGateMedicao);
		return cityGateMedicao;

	}

}
