package br.com.ggas.medicao.consumo;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.imovel.RamoAtividade;
import br.com.ggas.cadastro.imovel.Segmento;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.anormalidade.AnormalidadeConsumo;
import br.com.ggas.medicao.anormalidade.AnormalidadeSegmentoParametro;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.util.DataUtil;

public class TesteControladorHistoricoConsumo extends GGASTestCase{
	
	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTES_CONSISTIR_LEITURA.sql";
	
	@Autowired
	@Qualifier(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO)
	ControladorHistoricoConsumo controladorHistoricoConsumo;
	
	@Autowired
	@Qualifier(ControladorAnormalidade.BEAN_ID_CONTROLADOR_ANORMALIDADE)
	ControladorAnormalidade controladorAnormalidade;
	
	
	@Transactional
	@Before
	public void setup() throws FileNotFoundException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}
	
	@Test
	@Transactional
	public void testeAnaliseParametrizacaoConsumoZero() throws NegocioException {
		AnormalidadeConsumo anormalidadeConsumo = (AnormalidadeConsumo) fachada.criarAnormalidadeConsumo();
		anormalidadeConsumo.setChavePrimaria(16l);
		Segmento segmento = fachada.criarSegmento();
		segmento.setChavePrimaria(1l);
		RamoAtividade ramoAtividade = fachada.criarRamoAtividade();
		ramoAtividade.setChavePrimaria(99l);
		HistoricoConsumo historicoConsumo = (HistoricoConsumo) controladorHistoricoConsumo.criar();
		Collection<AnormalidadeSegmentoParametro> parametrizacao = controladorAnormalidade.obterAnormalidadeSegmentoParametros(anormalidadeConsumo, segmento, 1, ramoAtividade);
		controladorHistoricoConsumo.enviarHistoricoConsumoParaAnalise(historicoConsumo, parametrizacao);
	}
	
	@Test
	@Transactional
	public void testeObterHistoricoConsumoPorData() throws NegocioException {
		HistoricoConsumo historicoConsumo = (HistoricoConsumo) controladorHistoricoConsumo.obterHistoricoConsumoPorData(99999l, new Date());
		assertNotNull(historicoConsumo);
	}
	
	@Test
	@Transactional
	public void testeObterHistoricoConsumoAnoMesReferencia() {
		Long[] chavesPontoConsumo = {99999l};
		Map<Long, List<HistoricoConsumo>> map = controladorHistoricoConsumo.consultarHistoricoConsumoPorAnoMesFaturamentoEnumeroCiclo(chavesPontoConsumo, 201901, 1, Boolean.FALSE);
		assertNotNull(map);
	}
	
	@Test
	@Transactional
	public void testeConsultarHistoricoConsumoAnaliseExcessoes() throws NegocioException {
		Map<String, Object> filtro = new HashMap<>();
		filtro.put("dataLeituraInicio", DataUtil.decrementarDia(new Date(), 1));
		filtro.put("dataLeituraFim", DataUtil.incrementarDia(new Date(), 1));
		
		Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo.consultarHistoricoConsumoAnaliseExcecoesLeitura(filtro);
		assertNotNull(listaHistoricoConsumo);
	}
	
	@Test
	@Transactional
	public void testeConsultarHistoricoConsumo() throws NegocioException {
		Collection<HistoricoConsumo> listaHistoricoConsumo = controladorHistoricoConsumo
				.consultarHistoricoConsumo(99999l, 201901, 1, Boolean.TRUE, Boolean.FALSE, null);
		assertNotNull(listaHistoricoConsumo);
	}

}
