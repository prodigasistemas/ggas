package br.com.ggas.medicao.leitura;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.dadosmedicao.dominio.DadosMedicaoVO;
import br.com.ggas.medicao.dadosmedicao.repositorio.RepositorioDadosMedicao;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.web.AjaxService;


public class TesteControladorLeituraMovimento extends GGASTestCase {

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTES_CONSISTIR_LEITURA.sql";
	
	@Autowired
	ControladorLeituraMovimento controladorLeituraMovimento;
	
	@Autowired
	ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	ControladorRota controladorRota;
	
	@Autowired
	private RepositorioDadosMedicao repositorioDadosMedicao;
	
	@Autowired
	ControladorHistoricoConsumo controladorHistoricoConsumo;
	
	@Autowired
	ControladorHistoricoMedicao controladorHistoricoMedicao;
	
	@Transactional
	@Before
	public void setup() throws FileNotFoundException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}

		
	@Transactional
	@Test
	public void testeConsolidarMedicoesDiarias() throws Exception {
		Collection<PontoConsumo> listaPontos = this.obterListaPontosConsumo();
		Assert.assertEquals(listaPontos.size(), 2);
		
		GrupoFaturamento grupo = controladorRota.obterGrupoFaturamento(Long.valueOf(997));
		controladorLeituraMovimento.removerPontosConsumoSemLeituraMovimento(listaPontos, grupo);
		Assert.assertEquals(listaPontos.size(), 0);
	}
	
	@Transactional
	@Test 
	public void testAtualizarCronogramaConsistir() throws NegocioException {
		Rota rota = (Rota) controladorRota.obter(999l);
		
		boolean resultado = controladorLeituraMovimento.verificarConsistirLeitura(rota, 201901, 1);
		
	}
	
	@Transactional
	@Test 
	public void testeConsultarLeituraMovimento() throws NegocioException, ParseException {
		DadosMedicaoVO dados = new DadosMedicaoVO();
		dados.setIndicadorProcessado("4");
		
		repositorioDadosMedicao.consultarLeituraMovimento(dados);
		
	}
	
	@Transactional
	@Test 
	public void testAtualizarCronogramaConsistirSupervisorio() throws NegocioException {
		Rota rota = (Rota) controladorRota.obter(999l);
		
		boolean resultado = controladorLeituraMovimento.verificarConsistirLeituraCorretorVazao(rota, 201901, 1);
		
		assertTrue(resultado);
		
	}
	
	@Transactional
	@Test 
	public void testeRemoverLeituraMovimentoAnormalidade() throws NegocioException {
		Collection<LeituraMovimento> colecaoLeituraMovimento = controladorLeituraMovimento
				.consultarLeituraMovimentoPorRota(999l, null, null, 99999l, null);
		
		controladorLeituraMovimento.removerMovimentosNaoFaturar(colecaoLeituraMovimento, null);
		
		assertNotNull(colecaoLeituraMovimento);
	}
	
	@Transactional
	@Test 
	public void testeVerificarLeituraMovimento() throws NegocioException {
		Rota rota = (Rota) controladorRota.obter(999l);

		Boolean verificaRetorno = controladorLeituraMovimento.verificarRetornoLeitura(rota, 201901, 1,
				new StringBuilder());
		
		Boolean verificarRetornoConsistir = controladorLeituraMovimento.verificarConsistirLeitura(rota, 201901, 1);
		
		assertFalse(verificaRetorno && verificarRetornoConsistir);
	}
	
	@Transactional
	@Test 
	public void testeVerificarQtdCronograma() throws NegocioException {
		Long qtdFaturarGrupo = controladorHistoricoConsumo.consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamento(997l,201901,1);
		
		Long qtdFaturarSemFaturar = controladorHistoricoConsumo.consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamentoSemFaturar(997l,201901,1);
		
		assertTrue(qtdFaturarGrupo > 0 && qtdFaturarSemFaturar > 0);
	}
	
	@Transactional
	@Test
	public void testeConsultaHistoricoMedicaoAtual() {
		Long[] chavesPontos = {99999l};
		Map<PontoConsumo, Collection<HistoricoMedicao>> mapaHistoricoAtual = controladorHistoricoMedicao
				.consultarHistoricoMedicaoAtual(chavesPontos);
		
		assertNotNull(mapaHistoricoAtual);
	}
	
	@Transactional
	@Test
	public void testeConsultaHistoricoMedicaoNaoFaturado() throws NegocioException {
		Long[] chavesPontos = {99999l};
		Map<PontoConsumo, Collection<HistoricoMedicao>> mapaHistoricoAtual = controladorHistoricoMedicao
				.consultarHistoricoMedicaoNaoFaturado(chavesPontos, 201901, 1);
		
		assertNotNull(mapaHistoricoAtual);
		
	}
	
	
	private Collection<PontoConsumo> obterListaPontosConsumo() {
		PontoConsumo ponto1 = controladorPontoConsumo.buscarPontoConsumo(new Long(99998));
		PontoConsumo ponto2 = controladorPontoConsumo.buscarPontoConsumo(new Long(99999));
		
		Collection<PontoConsumo> listaPontos = new ArrayList<PontoConsumo>();
		listaPontos.add(ponto1);
		listaPontos.add(ponto2);
		
		return listaPontos;
	}
}

