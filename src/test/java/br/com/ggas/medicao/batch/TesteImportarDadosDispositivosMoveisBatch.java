package br.com.ggas.medicao.batch;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.impl.LeituraMovimentoImpl;

/**
 * @author Pedro Silva
 */
public class TesteImportarDadosDispositivosMoveisBatch extends GGASTestCase {

	@Autowired
	@Qualifier(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO)
	ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	@Qualifier(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO)
	ControladorLeituraMovimento controladorLeituraMovimento;

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_IMPORTAR_DADOS.sql";
	private static final String USER_HOME = System.getProperty("user.home");

	private void criarArquivo() throws IOException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
		long chave = 0;
		try {
			chave = listarPontosConsumo();
		} catch (GGASException e) {
			e.printStackTrace();
		}
		String CENARIO = obterCenario(chave);
		FileWriter writer = new FileWriter(USER_HOME + File.separator + "leitura.xml");

		writer.write(CENARIO);
		writer.close();
	}

	private String obterCenario(long chave) {

		StringBuilder builder = new StringBuilder();
		builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		builder.append("<importacao>\t\n<leitura anoMesFaturamento=\"201701\"");
		builder.append(" ciclo=\"1\" pontoConsumo=\"" + chave + "\"");
		builder.append(" valorLeitura=\"20\" dataLeitura=\"01/02/2017\"");
		builder.append(" codigoAnormalidadeLeitura=\"12\"");
		builder.append(" situacaoLeitura=\"2\"");
		builder.append(" indicadorConfirmacaoLeitura=\"false\"/>\"\n</importacao>");

		return builder.toString();
	}

	@Transactional
	@Test
	public void testeImportarDados() throws GGASException, IOException, InstantiationException, IllegalAccessException {

		criarArquivo();

		ProcessarArquivoLeituraBatch batch = new ProcessarArquivoLeituraBatch();
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("processo", new ProcessoImpl());
		parametros.put("arquivo", USER_HOME + File.separator + "leitura.xml");
		String retorno = batch.processar(parametros);
		LeituraMovimento leituraMovimento = consultarLeituraMovimento();
		BigDecimal leitura = new BigDecimal(20);
		Assert.assertTrue(retorno.contains("Total de registros: 1 Sucesso: 1 Erros: 0 "));
		Assert.assertEquals(3L, leituraMovimento.getSituacaoLeitura().getChavePrimaria());
		Assert.assertEquals(leitura, leituraMovimento.getValorLeitura());

	}

	private Long listarPontosConsumo() throws GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("numeroMedidor", "2048");
		PontoConsumo pontoChave = new PontoConsumoImpl();
		Collection<PontoConsumo> IteradorPontos = controladorPontoConsumo.listarPontosConsumo(filtro);
		Iterator<PontoConsumo> pontos = IteradorPontos.iterator();
		while (pontos.hasNext()) {
			pontoChave = pontos.next();
		}

		return pontoChave.getChavePrimaria();
	}

	private LeituraMovimento consultarLeituraMovimento() throws InstantiationException, IllegalAccessException, GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chaveSituacaoLeitura", 3L);
		LeituraMovimento leituraMovimento = new LeituraMovimentoImpl();
		Collection<LeituraMovimento> leituraMovimentos = controladorLeituraMovimento.consultarLeituraMovimento(filtro);
		Iterator<LeituraMovimento> iteradorLeituraMovimento = leituraMovimentos.iterator();
		while (iteradorLeituraMovimento.hasNext()) {
			leituraMovimento = iteradorLeituraMovimento.next();
		}

		return leituraMovimento;
	}
}
