package br.com.ggas.medicao.batch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.integracao.supervisorio.diaria.impl.SupervisorioMedicaoDiariaImpl;
import br.com.ggas.integracao.supervisorio.horaria.SupervisorioMedicaoHoraria;
import br.com.ggas.integracao.supervisorio.horaria.impl.ProcessadorSupervisorioMedicaoHorariaImpl;
import br.com.ggas.integracao.supervisorio.horaria.impl.SupervisorioMedicaoHorariaImpl;
import br.com.ggas.util.DataUtil;

public class TesteProcessadorSupervisorioMedicaoHoraria {

	@Test
	public void testMedicaoConsumoHoraria13Horas() {
		List<SupervisorioMedicaoHoraria> medicoesHoraria = new ArrayList<SupervisorioMedicaoHoraria>();

		medicoesHoraria.add(getSupervisorioMedicaoHoraria(1, "226800", "803963", "4.89476681", "20.58021545"));
		medicoesHoraria.addAll(getListaMedicoesHorariasJaneiro());
		
		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		
		assertEquals(new BigDecimal(1139), processador.getMediaHorariaConsumoSemCorrecao(medicoesHoraria));
		assertEquals(new BigDecimal(6482), processador.getMediaHorariaConsumoComCorrecao(medicoesHoraria));
	}
	
	@Test
	public void testMedicaoConsumoHoraria12Horas() {
		List<SupervisorioMedicaoHoraria> medicoesHoraria = getListaMedicoesHorariasJaneiro();
		
		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		
		assertEquals(new BigDecimal(1140), processador.getMediaHorariaConsumoSemCorrecao(medicoesHoraria));
		assertEquals(new BigDecimal(6485), processador.getMediaHorariaConsumoComCorrecao(medicoesHoraria));
	}
	
	
	@Test
	public void testGerarMedicaoAnterior() {
		SupervisorioMedicaoHoraria shPosterior = getSupervisorioMedicaoHoraria(2, "227910", "810381", "4.89502907", "20.68844032");
		
		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		SupervisorioMedicaoHoraria shAnterior = processador.gerarMedicaoAnterior(shPosterior, new BigDecimal(1139), new BigDecimal(6482));
		
		
		assertEquals(new BigDecimal(226771), shAnterior.getLeituraSemCorrecaoFatorPTZ());
		assertEquals(new BigDecimal(803899), shAnterior.getLeituraComCorrecaoFatorPTZ());
		assertEquals(new Integer(1), DataUtil.getHora(shAnterior.getDataRealizacaoLeitura()));
	}
	
	@Test
	public void testGerarMedicaoPosterior() {
		SupervisorioMedicaoHoraria shAnterior = getSupervisorioMedicaoHoraria(3, "229030", "816862", "4.89473343", "20.40750694");
		
		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		SupervisorioMedicaoHoraria shPosterior = processador.gerarMedicaoPosterior(shAnterior, new BigDecimal(1139), new BigDecimal(6482));
		
		
		assertEquals(new BigDecimal(230169), shPosterior.getLeituraSemCorrecaoFatorPTZ());
		assertEquals(new BigDecimal(823344), shPosterior.getLeituraComCorrecaoFatorPTZ());
		assertEquals(new Integer(4), DataUtil.getHora(shPosterior.getDataRealizacaoLeitura()));
	}
	
	
	@Test
	public void testGerarPrimeirasMedicoes() {
		List<SupervisorioMedicaoHoraria> medicoesHoraria = getListaMedicoesHorariasJaneiro();

		
		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		
		List<SupervisorioMedicaoHoraria> medicoesCompletas = processador.gerarPrimeirasMedicoes(medicoesHoraria);
		SupervisorioMedicaoHoraria smh0 = medicoesCompletas.get(1);
		SupervisorioMedicaoHoraria smh1 = medicoesCompletas.get(0);
		
		assertEquals(2, medicoesCompletas.size());
		
		assertEquals(new BigDecimal(225630), smh0.getLeituraSemCorrecaoFatorPTZ());
		assertEquals(new BigDecimal(797411), smh0.getLeituraComCorrecaoFatorPTZ());
		assertEquals(new Integer(0), DataUtil.getHora(smh0.getDataRealizacaoLeitura()));
		
		assertEquals(new BigDecimal(226770), smh1.getLeituraSemCorrecaoFatorPTZ());
		assertEquals(new BigDecimal(803896), smh1.getLeituraComCorrecaoFatorPTZ());
		assertEquals(new Integer(1), DataUtil.getHora(smh1.getDataRealizacaoLeitura()));
	}
	
	@Test
	public void testPreencherMedicoesMediaHoraria() {
		
		List<SupervisorioMedicaoHoraria> medicoesHoraria = getListaMedicoesHorariasJaneiro();
		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		
		List<SupervisorioMedicaoHoraria> medicoesCompletas = processador.preencherMedicoesMediaHoraria(medicoesHoraria);

		Map<Integer, SupervisorioMedicaoHoraria> mapMedicoes = processador.getMapMedicoes(medicoesCompletas);
		
		assertEquals(24, medicoesCompletas.size());
		assertTrue(mapMedicoes.containsKey(0));
		assertTrue(mapMedicoes.containsKey(3));
		assertTrue(mapMedicoes.containsKey(4));
		assertTrue(mapMedicoes.containsKey(23));
		
	}
	
	@Test
	public void testPreencherMedicoesMediaHorariaLeituraMenor() {
		List<SupervisorioMedicaoHoraria> medicoesHoraria = getListaMedicoesHorariasJaneiroLeituraMenor();

		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		
		List<SupervisorioMedicaoHoraria> medicoesCompletas = processador.preencherMedicoesMediaHoraria(medicoesHoraria);

		Map<Integer, SupervisorioMedicaoHoraria> mapMedicoes = processador.getMapMedicoes(medicoesCompletas);
		
		SupervisorioMedicaoHoraria smh4 = mapMedicoes.get(6);
		 
		assertEquals(24, medicoesCompletas.size());
		assertEquals(new BigDecimal(229030), smh4.getLeituraSemCorrecaoFatorPTZ());
		
	}
	
	
	@Test
	public void testGetMapMedicoes() {
		List<SupervisorioMedicaoHoraria> medicoesHoraria = getListaMedicoesHorariasJaneiro();

		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		
		Map<Integer, SupervisorioMedicaoHoraria> mapMedicoes = processador.getMapMedicoes(medicoesHoraria);
		
		assertEquals(12, mapMedicoes.size());
		assertTrue(mapMedicoes.containsKey(3));
		assertFalse(mapMedicoes.containsKey(4));
	}
	
	
	@Test
	public void testMediaConsumoDiaria() {
		List<SupervisorioMedicaoDiaria> smdExistentes = getListaMedicoesDiarias();
        
		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		
		assertEquals(new BigDecimal(9), processador.getMediaDiariaConsumoSemCorrecao(smdExistentes));
		assertEquals(new BigDecimal(66), processador.getMediaDiariaConsumoComCorrecao(smdExistentes));
	}
	
	
	@Test
	public void testPreencherMedicoesMediaDiaria() {
		List<SupervisorioMedicaoHoraria> smhExistentes = getListaMedicoesHorariasAgosto();
		List<SupervisorioMedicaoDiaria> smdExistentes = getListaMedicoesDiarias();
		
		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		
		List<SupervisorioMedicaoHoraria> medicoesCompletas = processador.preencherMedicoesMediaDiaria(smhExistentes, smdExistentes);

		Map<Integer, SupervisorioMedicaoHoraria> mapMedicoes = processador.getMapMedicoes(medicoesCompletas);
		
		assertEquals(24, medicoesCompletas.size());
		assertTrue(mapMedicoes.containsKey(0));
		assertTrue(mapMedicoes.containsKey(3));
		assertTrue(mapMedicoes.containsKey(4));
		assertTrue(mapMedicoes.containsKey(23));

	}
	
	@Test
	public void testValidarLeituraMedicaoAnterior() {
		ProcessadorSupervisorioMedicaoHorariaImpl processador = new ProcessadorSupervisorioMedicaoHorariaImpl();
		
		SupervisorioMedicaoHoraria smh1 = getSupervisorioMedicaoHoraria(1, "37600", "254400", "6.0319", "23.787");
		SupervisorioMedicaoHoraria smh2 = getSupervisorioMedicaoHoraria(2, "37680", "254404", "6.0319", "23.787");
		
		assertTrue(processador.validarLeituraSemCorrecao(smh1, smh2));
		assertTrue(processador.validarLeituraComCorrecao(smh1, smh2));
		
		assertTrue(processador.validarLeituraSemCorrecao(smh1, smh1));
		assertTrue(processador.validarLeituraComCorrecao(smh1, smh1));
		
		assertFalse(processador.validarLeituraSemCorrecao(smh2, smh1));
		assertFalse(processador.validarLeituraComCorrecao(smh2, smh1));
	}
	
	
	private SupervisorioMedicaoHoraria getSupervisorioMedicaoHoraria(Integer hora, String leituraSemCorrecao, String leituraComCorrecao, String pressao, String temperatura) {
		SupervisorioMedicaoHoraria sh1 = new SupervisorioMedicaoHorariaImpl();
		sh1.setDataRealizacaoLeitura(getDataHora(hora));
		sh1.setLeituraSemCorrecaoFatorPTZ(new BigDecimal(leituraSemCorrecao));
		sh1.setLeituraComCorrecaoFatorPTZ(new BigDecimal(leituraComCorrecao));
		sh1.setPressao(new BigDecimal(pressao));
		sh1.setTemperatura(new BigDecimal(temperatura));
		
		return sh1;
	}
	
	private Date getDataHora(Integer hora) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY,hora);
		cal.set(Calendar.MINUTE,0);
		cal.set(Calendar.SECOND,0);
		cal.set(Calendar.MILLISECOND,0);

		return cal.getTime();
	}
	
	private SupervisorioMedicaoDiaria getSupervisorioMedicaoDiaria(Integer dia, String leituraSemCorrecao, String leituraComCorrecao, String pressao, String temperatura) {
		SupervisorioMedicaoDiaria smd = new SupervisorioMedicaoDiariaImpl();
		smd.setDataRealizacaoLeitura(getDataDia(dia));
		smd.setLeituraSemCorrecaoFatorPTZ(new BigDecimal(leituraSemCorrecao));
		smd.setLeituraComCorrecaoFatorPTZ(new BigDecimal(leituraComCorrecao));
		smd.setPressao(new BigDecimal(pressao));
		smd.setTemperatura(new BigDecimal(temperatura));
		
		return smd;
	}
	
	private Date getDataDia(Integer dia) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH,dia);
		cal.set(Calendar.MONTH,Calendar.JANUARY);
		cal.set(Calendar.YEAR,2019);

		return cal.getTime();
	}
	
	private List<SupervisorioMedicaoDiaria> getListaMedicoesDiarias() {
List<SupervisorioMedicaoDiaria> smdExistentes = new ArrayList<SupervisorioMedicaoDiaria>();
        
        smdExistentes.add(getSupervisorioMedicaoDiaria(1, "37680", "254404", "6.0319", "23.787")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(2, "37958", "256293", "6.027", "24.5194")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(3, "38234", "258162", "6.0375", "24.9174")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(4, "38439", "259553", "6.0317", "24.8506")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(5, "38439", "259554", "6.0268", "26.4133")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(6, "38699", "261308", "6.0286", "25.5755")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(7, "38968", "263130", "6.0312", "25.6638")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(8, "39225", "264874", "6.0306", "24.895")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(9, "39497", "266709", "6.0272", "25.6469")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(10, "39760", "268485", "6.0285", "25.7504")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(11, "39961", "269845", "6.0254", "25.2486")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(12, "39961", "269845", "6.0192", "26.8782")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(13, "40208", "271514", "6.0129", "25.4795")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(14, "40467", "273264", "6.0144", "25.6387")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(15, "40718", "274955", "6.0173", "25.9976")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(16, "40982", "276731", "6.0219", "26.5935")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(17, "41248", "278520", "6.0263", "26.3875")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(18, "41413", "279634", "6.023", "26.4102")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(19, "41413", "279634", "6.0184", "27.5528")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(20, "41662", "281314", "6.018", "26.0047")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(21, "41929", "283108", "6.0149", "25.5818")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(22, "42207", "284987", "6.014", "25.5204")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(23, "42529", "287153", "6.0086", "25.4416")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(24, "42887", "289568", "6.0356", "25.3971")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(25, "43163", "291431", "6.0244", "26.0082")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(26, "43163", "291431", "6.0182", "28.1624")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(27, "43312", "292428", "6.0187", "26.3575")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(28, "43648", "294693", "6.019", "25.8091")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(29, "44015", "297159", "6.0426", "25.6563")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(30, "44375", "299609", "6.132", "25.2279")); 
        smdExistentes.add(getSupervisorioMedicaoDiaria(31, "44816", "302593", "6.0405", "24.8154"));
        
        return smdExistentes; 
	}
	
	private List<SupervisorioMedicaoHoraria> getListaMedicoesHorariasAgosto() {
		List<SupervisorioMedicaoHoraria> smhExistentes = new ArrayList<SupervisorioMedicaoHoraria>();
		
		smhExistentes.add(getSupervisorioMedicaoHoraria(0, "37447", "252819", "6.04734325", "21.95264053"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(1, "37447", "252819", "6.0486517", "21.64645004"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(2, "37447", "252819", "6.04983854", "21.07169342"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(3, "37447", "252819", "6.04833078", "20.72271919"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(4, "37447", "252819", "6.04623652", "20.82225418"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(5, "37447", "252819", "6.04667711", "20.86359215"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(6, "37447", "252819", "6.04435682", "21.16753769"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(7, "37453", "252864", "6.04081392", "21.51306152"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(8, "37474", "253006", "6.03445196", "22.74954033"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(9, "37502", "253201", "6.02412033", "23.43489456"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(10, "37521", "253330", "6.01660299", "24.34784126"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(11, "37539", "253450", "6.01395321", "25.16687584"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(12, "37557", "253574", "6.02027178", "25.19659042"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(13, "37578", "253715", "6.00789881", "25.68230247"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(14, "37596", "253834", "6.00539303", "25.79447365"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(15, "37610", "253931", "6.00634193", "26.25924492"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(16, "37628", "254048", "6.01954889", "25.95856094"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(17, "37644", "254162", "6.03028536", "25.09796715"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(18, "37656", "254239", "6.03273582", "24.91015053"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(19, "37670", "254338", "6.0353508", "24.72270203"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(20, "37677", "254383", "6.04025269", "24.90787125"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(21, "37680", "254404", "6.03721237", "24.52472496"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(22, "37680", "254404", "6.03773689", "24.94418716"));
		smhExistentes.add(getSupervisorioMedicaoHoraria(23, "37680", "254404", "6.03876638", "24.74141693"));

		return smhExistentes;
	}
	
	private List<SupervisorioMedicaoHoraria> getListaMedicoesHorariasJaneiroLeituraMenor() {
		List<SupervisorioMedicaoHoraria> medicoesHoraria = new ArrayList<SupervisorioMedicaoHoraria>();

		medicoesHoraria.add(getSupervisorioMedicaoHoraria(2, "227910", "810381", "4.89502907", "20.68844032"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(3, "229030", "816862", "4.89473343", "20.40750694"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(7, "229030", "816862", "4.89265060", "22.82153511"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(8, "234640", "849106", "4.88473988", "25.07756424"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(9, "235800", "855652", "4.87575150", "26.66594696"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(11, "238120", "868733", "4.87509060", "26.91302681"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(12, "239270", "875218", "4.87587833", "26.85069084"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(13, "240420", "881708", "4.87656116", "26.66342163"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(14, "241570", "888210", "4.87758875", "26.22393036"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(17, "245000", "907623", "4.87867785", "25.94459534"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(21, "249570", "933514", "4.88264179", "25.50522232"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(22, "250720", "940092", "4.88430119", "23.21763992"));
		
		return medicoesHoraria;
	}
	
	private List<SupervisorioMedicaoHoraria> getListaMedicoesHorariasJaneiro() {
		List<SupervisorioMedicaoHoraria> medicoesHoraria = new ArrayList<SupervisorioMedicaoHoraria>();

		medicoesHoraria.add(getSupervisorioMedicaoHoraria(2, "227910", "810381", "4.89502907", "20.68844032"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(3, "229030", "816862", "4.89473343", "20.40750694"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(7, "233510", "842685", "4.89265060", "22.82153511"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(8, "234640", "849106", "4.88473988", "25.07756424"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(9, "235800", "855652", "4.87575150", "26.66594696"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(11, "238120", "868733", "4.87509060", "26.91302681"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(12, "239270", "875218", "4.87587833", "26.85069084"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(13, "240420", "881708", "4.87656116", "26.66342163"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(14, "241570", "888210", "4.87758875", "26.22393036"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(17, "245000", "907623", "4.87867785", "25.94459534"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(21, "249570", "933514", "4.88264179", "25.50522232"));
		medicoesHoraria.add(getSupervisorioMedicaoHoraria(22, "250720", "940092", "4.88430119", "23.21763992"));
		
		return medicoesHoraria;
	}
}
