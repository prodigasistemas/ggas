
package br.com.ggas.medicao.batch;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.integracao.supervisorio.ControladorSupervisorio;
import br.com.ggas.integracao.supervisorio.ResumoSupervisorioMedicaoVO;
import br.com.ggas.integracao.supervisorio.batch.ConsolidarDadosSupervisorioMedicaoDiariaBatch;
import br.com.ggas.integracao.supervisorio.diaria.SupervisorioMedicaoDiaria;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;

/**
 * @author Valter Negreiros
 */
public class TesteConsolidarDadosDeLeituraBatch extends GGASTestCase {

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_CONSOLIDAR.sql";


	@Autowired
	@Qualifier(ControladorSupervisorio.BEAN_ID_CONTROLADOR_SUPERVISORIO)
	ControladorSupervisorio controladorSupervisorio;

	ConsolidarDadosSupervisorioMedicaoDiariaBatch consolidarDadosSupervisorio;
	
	@Autowired
	@Qualifier(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO)
	ControladorPontoConsumo controladorPontoConsumo;
	
	@Autowired
	@Qualifier(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA)
	ControladorParametroSistema controladorParametroSistema;
	
	@Transactional
	@Before
	public void setup() throws FileNotFoundException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}
	
	@Transactional
	@Test
	public void testeConsolidarMedicoesDiarias() throws Exception {
		alterarParametroTipoIntegracao("169");
		Collection<SupervisorioMedicaoDiaria> medicoesDiarias = consultarMedicoesDiarias();
		int qtdInicial = medicoesDiarias.size();
		consolidarDados();
		medicoesDiarias = consultarMedicoesDiarias();

		Assert.assertNotNull(medicoesDiarias);
		Assert.assertEquals(qtdInicial + 7, medicoesDiarias.size());
	}

	@Transactional
	@Test
	public void testeValoresMedicoesConsolidadas() throws Exception {
		alterarParametroTipoIntegracao("169");
		consolidarDados();

		Collection<SupervisorioMedicaoDiaria> medicoesDiarias = consultarMedicoesDiarias();

		BigDecimal totalConsumoDiariosSemCorrecaoPtz = BigDecimal.ZERO;
		BigDecimal totalConsumoDiariosComCorrecaoPtz = BigDecimal.ZERO;
		BigDecimal totalLeituraDiariasSemCorrecaoPtz = BigDecimal.ZERO;
		BigDecimal totalLeituraDiariasComCorrecaoPtz = BigDecimal.ZERO;
		BigDecimal consumoDiariosSemCorrecaoPtz;
		BigDecimal consumoDiariosComCorrecaoPtz;
		BigDecimal leituraDiariasSemCorrecaoPtz;
		BigDecimal leituraDiariasComCorrecaoPtz;

		for (SupervisorioMedicaoDiaria medicoes : medicoesDiarias) {

			consumoDiariosSemCorrecaoPtz = medicoes.getConsumoSemCorrecaoFatorPTZ();
			consumoDiariosComCorrecaoPtz = medicoes.getConsumoComCorrecaoFatorPTZ();
			leituraDiariasSemCorrecaoPtz = medicoes.getLeituraSemCorrecaoFatorPTZ();
			leituraDiariasComCorrecaoPtz = medicoes.getLeituraComCorrecaoFatorPTZ();

			totalConsumoDiariosSemCorrecaoPtz = totalConsumoDiariosSemCorrecaoPtz.add(consumoDiariosSemCorrecaoPtz);
			totalConsumoDiariosComCorrecaoPtz = totalConsumoDiariosComCorrecaoPtz.add(consumoDiariosComCorrecaoPtz);
			totalLeituraDiariasSemCorrecaoPtz = totalLeituraDiariasSemCorrecaoPtz.add(leituraDiariasSemCorrecaoPtz);
			totalLeituraDiariasComCorrecaoPtz = totalLeituraDiariasComCorrecaoPtz.add(leituraDiariasComCorrecaoPtz);
		}

		Assert.assertEquals(360950, totalConsumoDiariosSemCorrecaoPtz.intValue());
		Assert.assertEquals(1034130, totalConsumoDiariosComCorrecaoPtz.intValue());
		Assert.assertEquals(3747450, totalLeituraDiariasSemCorrecaoPtz.intValue());
		Assert.assertEquals(5925970, totalLeituraDiariasComCorrecaoPtz.intValue());

	}

	@Transactional
	@Test
	public void testeValoresMedicoesConsolidadasPelaMedia() throws Exception {
		alterarParametroTipoIntegracao("169");
		consolidarDados();

		Collection<SupervisorioMedicaoDiaria> medicoesDiarias = consultarMedicoesDiariasPelaMedia();

		BigDecimal totalConsumoDiariosSemCorrecaoPtz = BigDecimal.ZERO;
		BigDecimal totalConsumoDiariosComCorrecaoPtz = BigDecimal.ZERO;
		BigDecimal totalLeituraDiariasSemCorrecaoPtz = BigDecimal.ZERO;
		BigDecimal totalLeituraDiariasComCorrecaoPtz = BigDecimal.ZERO;
		BigDecimal consumoDiariosSemCorrecaoPtz;
		BigDecimal consumoDiariosComCorrecaoPtz;
		BigDecimal leituraDiariasSemCorrecaoPtz;
		BigDecimal leituraDiariasComCorrecaoPtz;

		for (SupervisorioMedicaoDiaria medicao : medicoesDiarias) {

			consumoDiariosSemCorrecaoPtz = medicao.getConsumoSemCorrecaoFatorPTZ();
			consumoDiariosComCorrecaoPtz = medicao.getConsumoComCorrecaoFatorPTZ();
			leituraDiariasSemCorrecaoPtz = medicao.getLeituraSemCorrecaoFatorPTZ();
			leituraDiariasComCorrecaoPtz = medicao.getLeituraComCorrecaoFatorPTZ();

			totalConsumoDiariosSemCorrecaoPtz = totalConsumoDiariosSemCorrecaoPtz.add(consumoDiariosSemCorrecaoPtz);
			totalConsumoDiariosComCorrecaoPtz = totalConsumoDiariosComCorrecaoPtz.add(consumoDiariosComCorrecaoPtz);
			totalLeituraDiariasSemCorrecaoPtz = totalLeituraDiariasSemCorrecaoPtz.add(leituraDiariasSemCorrecaoPtz);
			totalLeituraDiariasComCorrecaoPtz = totalLeituraDiariasComCorrecaoPtz.add(leituraDiariasComCorrecaoPtz);
		}

		Assert.assertEquals(368090, totalConsumoDiariosSemCorrecaoPtz.intValue());
		Assert.assertEquals(1073102, totalConsumoDiariosComCorrecaoPtz.intValue());
		Assert.assertEquals(3288510, totalLeituraDiariasSemCorrecaoPtz.intValue());
		Assert.assertEquals(4766655, totalLeituraDiariasComCorrecaoPtz.intValue());

	}

	@Transactional
	@Test
	public void testeValoresMedicoesConsolidadasPelaMediaComAnormalidadeRegistroAlteradoManualmente() throws Exception {
		alterarParametroTipoIntegracao("169");
		consolidarDados();

		Collection<SupervisorioMedicaoDiaria> medicoesDiarias = consultarMedicoesDiariasPelaMedia();

		for (SupervisorioMedicaoDiaria medicao : medicoesDiarias) {
			if (medicao.getListaSupervisorioMedicaoHoraria().size() < Constantes.NUMERO_HORAS_NO_DIA
					&& medicao.getIndicadorMedidor()) {
				Assert.assertEquals(Constantes.ANORMALIDADE_SUPERVISORIO_ALTERADO_MANUALMENTE.longValue(),
						medicao.getSupervisorioMedicaoAnormalidade().getChavePrimaria());
			}
		}

	}
	
	
	@Transactional
	@Test
	public void testeConsolidarMedicoesDiariasInseridas() throws Exception {
		alterarParametroTipoIntegracao("170");
		PontoConsumo pontoConsumo = (PontoConsumo) controladorPontoConsumo.obter(999999l);
		StringBuilder logProcessamento = new StringBuilder();
		Map<String, List<Object[]>> mapaAnormalidadeSupervisorioMedicaoDiaria = new LinkedHashMap<String, List<Object[]>>();
		ResumoSupervisorioMedicaoVO resumoSupervisorioMedicaoVO = new ResumoSupervisorioMedicaoVO();
		
		controladorSupervisorio.tratarMedicoesDiarias(pontoConsumo, null,
				null, logProcessamento, mapaAnormalidadeSupervisorioMedicaoDiaria, resumoSupervisorioMedicaoVO,
				null);

	}

	private Collection<SupervisorioMedicaoDiaria> consultarMedicoesDiarias()
			throws InstantiationException, IllegalAccessException, GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("codigoPontoConsumoSupervisorio", "999999");
		String ordenacao = null;
		String propriedadesLazy = null;
		return controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, ordenacao, propriedadesLazy);
	}

	private Collection<SupervisorioMedicaoDiaria> consultarMedicoesDiariasPelaMedia()
			throws InstantiationException, IllegalAccessException, GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("codigoPontoConsumoSupervisorio", "999998");
		String ordenacao = null;
		String propriedadesLazy = null;
		return controladorSupervisorio.consultarSupervisorioMedicaoDiaria(filtro, ordenacao, propriedadesLazy);
	}

	private void consolidarDados() throws GGASException {
		new ConsolidarDadosSupervisorioMedicaoDiariaBatch().processar(new HashMap<String, Object>());
	}
	
	private void alterarParametroTipoIntegracao(String valor) throws GGASException {
		ParametroSistema parametro = controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_TIPO_INTEGRACAO_SUPERVISORIOS);
		parametro.setValor(valor);

		controladorParametroSistema.atualizar(parametro);
	}

}
