
package br.com.ggas.medicao.batch;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;

public class TesteProcessarArquivoLeituraBatch extends GGASTestCase {

	private static final String USER_HOME = System.getProperty("user.home");

	private static final String CENARIO = obterCenario();

	@Before
	public void setup() throws IOException {

		FileWriter writer = new FileWriter(USER_HOME + File.separator + "leitura.xml");

		writer.write(CENARIO);
		writer.close();
	}

	private static String obterCenario() {

		StringBuilder builder = new StringBuilder();
		builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		builder.append("<importacao>\t\n<leitura anoMesFaturamento=\"201602\"");
		builder.append(" ciclo=\"1\" pontoConsumo=\"11036\"");
		builder.append(" valorLeitura=\"200\" dataLeitura=\"16/02/2016\"");
		builder.append(" codigoAnormalidadeLeitura=\"\"");
		builder.append(" indicadorConfirmacaoLeitura=\"false\"/>\n</importacao>");

		return builder.toString();
	}

	@Test
	public void testeProcessarLeitura() throws IOException, GGASException {
		ProcessarArquivoLeituraBatch batch = new ProcessarArquivoLeituraBatch();
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("processo", new ProcessoImpl());
		parametros.put("arquivo", USER_HOME + File.separator + "leitura.xml");
		String retorno = batch.processar(parametros);

		Assert.assertNotNull(retorno);

	}

}
