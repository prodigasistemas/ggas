package br.com.ggas.medicao.batch;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.impl.GrupoFaturamentoImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.controleacesso.impl.OperacaoImpl;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.impl.CronogramaAtividadeFaturamentoImpl;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.impl.LeituraMovimentoImpl;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;

/**
 * @author Pedro Silva
 */
public class TesteGerarDadosLeituraBatch extends GGASTestCase {

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_GERAR_DADOS.sql";

	private static final String ARQUIVO_LIMPAR_CENARIO = "/sql/SETUP_INTEGRIDADE_TESTE_GERAR_DADOS.sql";

	@Autowired
	@Qualifier(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO)
	ControladorCronogramaFaturamento controladorCronogramaFaturamento;

	@Autowired
	@Qualifier(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA)
	ControladorFatura controladorFatura;

	@Autowired
	@Qualifier(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO)
	ControladorLeituraMovimento controladorLeituraMovimento;

	@Autowired
	@Qualifier(ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO)
	ControladorProcesso controladorProcesso;

	@Autowired
	@Qualifier(ControladorModulo.BEAN_ID_CONTROLADOR_MODULO)
	ControladorModulo controladorModulo;

	@Autowired
	@Qualifier(ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO)
	ControladorUsuario controladorUsuario;

	@Autowired
	@Qualifier(ControladorRota.BEAN_ID_CONTROLADOR_ROTA)
	ControladorRota controladorRota;

	@Autowired
	private GerarDadosDeLeituraBatch gerarDadosDeLeituraBatch;

	@Transactional
	@Test
	public void testeGerarDadosLeitura() throws Exception {

		setupSQL(ARQUIVO_PREPARAR_CENARIO);
		Processo processo = new ProcessoImpl();
		Operacao operacao = new OperacaoImpl();
		Usuario usuario = buscarUsuario("admin");
		Rota rota = consultarRota();

		gerarDadosDeLeituraBatch.processar(gerarParametros(processo, operacao, usuario, rota));

		LeituraMovimento leituraMovimento = consultarLeituraMovimento(rota);

		Assert.assertNotNull(leituraMovimento);
		Assert.assertEquals("201707", String.valueOf(leituraMovimento.getAnoMesFaturamento()));
		Assert.assertEquals("EDIFICIO PRADA - APT 109", leituraMovimento.getDescicaoPontoConsumo());
		Assert.assertEquals("RESIDENCIAL - GERAR DADOS", leituraMovimento.getNumeroRota());
		Assert.assertEquals("53530-240", leituraMovimento.getCep());
		Assert.assertEquals("#231764", leituraMovimento.getNumeroSerieMedidor());
		Assert.assertEquals("INTERIOR DO IMÓVEL", leituraMovimento.getLocalInstalacaoMedidor());

	}

	private LeituraMovimento consultarLeituraMovimento(Rota rota) throws InstantiationException, IllegalAccessException, GGASException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("grupoFaturamento", rota.getGrupoFaturamento().getChavePrimaria());
		filtro.put("chaveSituacaoLeitura", 1L);
		LeituraMovimento leituraMovimento = new LeituraMovimentoImpl();
		Collection<LeituraMovimento> leituraMovimentos = controladorLeituraMovimento.consultarLeituraMovimento(filtro);
		Iterator<LeituraMovimento> iteradorLeituraMovimento = leituraMovimentos.iterator();
		while (iteradorLeituraMovimento.hasNext()) {
			leituraMovimento = iteradorLeituraMovimento.next();
		}

		return leituraMovimento;
	}

	public CronogramaAtividadeFaturamento consultarCronogramaAtividadeFaturamento(AtividadeSistema atividadeSistema)
			throws NegocioException {

		Collection<CronogramaAtividadeFaturamento> cronogramas =
				controladorCronogramaFaturamento.consultarCronogramaAtividadeFaturamento(atividadeSistema);
		CronogramaAtividadeFaturamento cronograma = new CronogramaAtividadeFaturamentoImpl();
		Iterator<CronogramaAtividadeFaturamento> IteradorCronograma = cronogramas.iterator();
		while (IteradorCronograma.hasNext()) {
			cronograma = IteradorCronograma.next();
		}
		return cronograma;
	}

	public GrupoFaturamento consultarGrupoFaturamento() throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idPeriodicidade", 3L);
		Collection<GrupoFaturamento> IteradorGrupos = controladorFatura.consultarGrupoFaturamento(filtro);
		GrupoFaturamento grupo = new GrupoFaturamentoImpl();
		Iterator<GrupoFaturamento> grupos = IteradorGrupos.iterator();
		while (grupos.hasNext()) {
			grupo = grupos.next();
		}
		return grupo;
	}

	public Usuario buscarUsuario(String login) throws NegocioException {

		return controladorUsuario.buscar(login);
	}

	private Rota consultarRota() throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("numeroRota", "RESIDENCIAL - GERAR DADOS");
		filtro.put("idPeriodicidade", 3L);

		Collection<Rota> IteradorRota = controladorRota.consultarRota(filtro);

		Rota rota = null;
		Iterator<Rota> rotas = IteradorRota.iterator();
		while (rotas.hasNext()) {
			rota = rotas.next();
		}

		return rota;
	}

	private Map<String, Object> gerarParametros(Processo processo, Operacao operacao, Usuario usuario, Rota rota) throws GGASException {

		Map<String, Object> parametros = new HashMap<String, Object>();

		operacao.setDescricao("Gerar Dados para Leitura");
		operacao.setChavePrimaria(20L);

		processo.setOperacao(operacao);
		processo.setSituacao(1);
		processo.setUsuario(usuario);

		AtividadeSistema atividadeSistema = controladorCronogramaFaturamento.obterAtividadeSistemaPorOperacao(20L);
		parametros.put("idRota", String.valueOf(rota.getChavePrimaria()));
		parametros.put("idGrupoFaturamento", String.valueOf(rota.getGrupoFaturamento().getChavePrimaria()));
		parametros.put("idPeriodicidade", String.valueOf(rota.getPeriodicidade()));
		parametros.put("processo", processo);
		parametros.put("atividadeSistema", atividadeSistema);
		parametros.put("idCronogramaAtividadeFaturamento",
				String.valueOf(consultarCronogramaAtividadeFaturamento(atividadeSistema).getChavePrimaria()));

		return parametros;
	}
}
