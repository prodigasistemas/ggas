package br.com.ggas.medicao.batch;

import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.ControladorProcessoDocumento;
import br.com.ggas.batch.Processo;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.controleacesso.ControladorModulo;
import br.com.ggas.controleacesso.ControladorUsuario;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.Usuario;
import br.com.ggas.controleacesso.impl.OperacaoImpl;
import br.com.ggas.faturamento.ControladorFaturamentoAnormalidade;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.batch.ProcessarEmissaoFaturaBatch;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Classe responsável por englobar os testes do Processo de Emitir Fatura
 *
 * @author Clemilson Morais
 */
public class TesteEmitirFaturaBatch extends GGASTestCase {

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_EMITIR_FATURA.sql";
	private static final long CHAVE_PRIMARIA_OPERACAO_EMITIR_FATURA = 65;
	private static final long ID_PERIODICIDADE_MENSAL_COLETOR = 3;

	@Autowired
	@Qualifier(ControladorCronogramaFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_FATURAMENTO)
	ControladorCronogramaFaturamento controladorCronogramaFaturamento;

	@Autowired
	@Qualifier(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA)
	ControladorFatura controladorFatura;

	@Autowired
	@Qualifier(ControladorLeituraMovimento.BEAN_ID_CONTROLADOR_LEITURA_MOVIMENTO)
	ControladorLeituraMovimento controladorLeituraMovimento;

	@Autowired
	@Qualifier(ControladorFaturamentoAnormalidade.BEAN_ID_CONTROLADOR_FATURAMENTO_ANORMALIDADE)
	ControladorFaturamentoAnormalidade controladorFaturamentoAnormalidade;

	@Autowired
	@Qualifier(ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO)
	ControladorProcesso controladorProcesso;

	@Autowired
	@Qualifier(ControladorModulo.BEAN_ID_CONTROLADOR_MODULO)
	ControladorModulo controladorModulo;

	@Autowired
	@Qualifier(ControladorUsuario.BEAN_ID_CONTROLADOR_USUARIO)
	ControladorUsuario controladorUsuario;

	@Autowired
	@Qualifier(ControladorRota.BEAN_ID_CONTROLADOR_ROTA)
	ControladorRota controladorRota;

	@Autowired
	@Qualifier(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA)
	ControladorParametroSistema controladorParametroSistema;

	@Autowired
	@Qualifier(ControladorProcessoDocumento.BEAN_ID_CONTROLADOR_PROCESSO_DOCUMENTO)
	ControladorProcessoDocumento controladorProcessoDocumento;

	@Autowired
	private ProcessarEmissaoFaturaBatch batch;

	@Transactional
	@Test
	@Ignore
	public void testeEmitirFatura() throws Exception {

		setupSQL(ARQUIVO_PREPARAR_CENARIO);
		Constantes.URL_LOGOMARCA_GGAS = "src/main/webapp/images/logo_ggas_login.png";
		Processo processo = (Processo) controladorProcesso.obter(14L);
		System.out.println(controladorProcessoDocumento.consultarProcessoDocumento(new HashMap<>()));

		ParametroSistema parametroDiretorioFatura = controladorParametroSistema.obterParametroPorCodigo("DIRETORIO_ARQUIVOS_FATURA");
		parametroDiretorioFatura.setValor(System.getProperty("java.io.tmpdir") + "testeFaturamentoGGAS");
		controladorParametroSistema.atualizar(parametroDiretorioFatura);

		Operacao operacao = new OperacaoImpl();

		operacao.setDescricao("Emitir Faturas");
		operacao.setChavePrimaria(CHAVE_PRIMARIA_OPERACAO_EMITIR_FATURA);
		AtividadeSistema atividadeSistema = controladorCronogramaFaturamento.obterAtividadeSistemaPorOperacao(CHAVE_PRIMARIA_OPERACAO_EMITIR_FATURA);


		Usuario usuario = buscarUsuario("admin");
		Rota rota = consultarRota();

		batch.processar(gerarParametros(processo, operacao, usuario, rota, atividadeSistema));

		CronogramaAtividadeFaturamento cronograma = consultarCronogramaAtividadeFaturamento(atividadeSistema);
		Assert.assertNotNull(cronograma.getDataRealizacao());

		Map<String, Object> filtroProcessoDocumento = new HashMap<>();
		filtroProcessoDocumento.put("nomeDocumento", "%DEMME");
		Assert.assertNotEquals(0, controladorProcessoDocumento.consultarProcessoDocumento(filtroProcessoDocumento).size());
		Assert.assertNotEquals(1, controladorProcessoDocumento.consultarProcessoDocumento(new HashMap<>()).size());
	}

	public CronogramaAtividadeFaturamento consultarCronogramaAtividadeFaturamento(AtividadeSistema atividadeSistema)
			throws NegocioException {

		Collection<CronogramaAtividadeFaturamento> cronogramas =
				controladorCronogramaFaturamento.consultarCronogramaAtividadeFaturamento(atividadeSistema);
		Iterator<CronogramaAtividadeFaturamento> iteradorCronograma = cronogramas.iterator();
		CronogramaAtividadeFaturamento cronograma = null;
		if (iteradorCronograma.hasNext()) {
			 cronograma = iteradorCronograma.next();
		}
		return cronograma;
	}

	public Usuario buscarUsuario(String login) throws NegocioException {

		return controladorUsuario.buscar(login);
	}

	private Rota consultarRota() throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("numeroRota", "CEN03");
		filtro.put("idPeriodicidade", ID_PERIODICIDADE_MENSAL_COLETOR);

		Collection<Rota> IteradorRota = controladorRota.consultarRota(filtro);

		Rota rota = null;
		Iterator<Rota> rotas = IteradorRota.iterator();
		while (rotas.hasNext()) {
			rota = rotas.next();
		}

		return rota;
	}

	private Map<String, Object> gerarParametros(Processo processo, Operacao operacao, Usuario usuario, Rota rota, AtividadeSistema atividadeSistema) throws GGASException {
		Map<String, Object> parametros = new HashMap<String, Object>();
		processo.setOperacao(operacao);
		processo.setSituacao(1);
		processo.setUsuario(usuario);
		parametros.put("idRota", String.valueOf(rota.getChavePrimaria()));
		parametros.put("idGrupoFaturamento", String.valueOf(rota.getGrupoFaturamento().getChavePrimaria()));
		parametros.put("idPeriodicidade", String.valueOf(rota.getPeriodicidade()));
		parametros.put("processo", processo);
		parametros.put("atividadeSistema", atividadeSistema);
		parametros.put("idCronogramaAtividadeFaturamento",
				String.valueOf(consultarCronogramaAtividadeFaturamento(atividadeSistema).getChavePrimaria()));
		return parametros;
	}

}
