package br.com.ggas.medicao.batch;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.batch.ControladorProcesso;
import br.com.ggas.batch.Processo;
import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.cadastro.imovel.ControladorPontoConsumo;
import br.com.ggas.cadastro.imovel.GrupoFaturamento;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.GrupoFaturamentoImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.controleacesso.Operacao;
import br.com.ggas.controleacesso.impl.OperacaoImpl;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.leitura.ControladorHistoricoMedicao;
import br.com.ggas.medicao.leitura.HistoricoMedicao;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.medicao.rota.Rota;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;

public class TesteRegistrarLeiturasBatch extends GGASTestCase {

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_REGISTRAR_LEITURAS_BATCH.sql";
	private static final String ARQUIVO_LIMPAR_CENARIO = "/sql/SETUP_INTEGRIDADE_TESTE_REGISTRAR_LEITURAS_BATCH.sql";

	@Autowired
	@Qualifier(ControladorHistoricoMedicao.BEAN_ID_CONTROLADOR_HISTORICO_MEDICAO)
	ControladorHistoricoMedicao controladorHistoricoMedicao;

	@Autowired
	@Qualifier(ControladorPontoConsumo.BEAN_ID_CONTROLADOR_PONTO_CONSUMO)
	ControladorPontoConsumo controladorPontoConsumo;

	@Autowired
	@Qualifier(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA)
	ControladorFatura controladorFatura;

	@Autowired
	@Qualifier(ControladorRota.BEAN_ID_CONTROLADOR_ROTA)
	ControladorRota controladorRota;

	@Autowired
	@Qualifier(ControladorProcesso.BEAN_ID_CONTROLADOR_PROCESSO)
	ControladorProcesso controladorProcesso;

	@Autowired
	@Qualifier(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA)
	ControladorParametroSistema controladorParametroSistema;

	@Ignore
	@Transactional
	@Test
	public void testeRegistrarDadosLeitura() throws Exception {

		setupSQL(ARQUIVO_PREPARAR_CENARIO);

		Processo processo = new ProcessoImpl();
		Operacao operacao = new OperacaoImpl();
		Rota rota = consultarRota();
		GrupoFaturamento grupoFaturamento = consultarGrupoFaturamento();

		atualizarCaminhoPdf();

		new RegistrarLeiturasAnormalidadesBatch().processar(gerarParametros(operacao, processo, grupoFaturamento, rota));

		PontoConsumo pontoConsumo = consultarPontoConsumo(rota);

		HistoricoMedicao historicoMedicao = controladorHistoricoMedicao.obterUltimoHistoricoMedicao(pontoConsumo.getChavePrimaria());

		Assert.assertNotNull(historicoMedicao);
		Assert.assertEquals("201706", String.valueOf(historicoMedicao.getAnoMesLeitura()));
		Assert.assertEquals("1", String.valueOf(historicoMedicao.getNumeroCiclo()));
		Assert.assertEquals("200", String.valueOf(historicoMedicao.getNumeroLeituraInformada()));
		Assert.assertEquals("Leitura informada", String.valueOf(historicoMedicao.getAnormalidadeLeituraInformada().getDescricao()));

	}

	private void atualizarCaminhoPdf() throws NegocioException, ConcorrenciaException {
		ParametroSistema ps = controladorParametroSistema.obterParametroPorCodigo("DIRETORIO_RELATORIO_PDF");
		String pathPdf = System.getProperty("user.home") + "/pdf/";
		ps.setValor(pathPdf);
		controladorParametroSistema.atualizar(ps);
	}

	private Rota consultarRota() throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("numeroRota", "RESIDENCIAL - ROTA 13");
		filtro.put("idPeriodicidade", 3L);

		Collection<Rota> IteradorRota = controladorRota.consultarRota(filtro);

		Rota rota = null;
		Iterator<Rota> rotas = IteradorRota.iterator();
		while (rotas.hasNext()) {
			rota = rotas.next();
		}

		return rota;
	}

	public PontoConsumo consultarPontoConsumo(Rota rota) throws GGASException {
		PontoConsumo pontoConsumo = null;
		Collection<PontoConsumo> IteradorPontos = controladorPontoConsumo.consultarPontoConsumoPorRota(rota);

		Iterator<PontoConsumo> pontos = IteradorPontos.iterator();
		while (pontos.hasNext()) {
			pontoConsumo = pontos.next();
		}
		return pontoConsumo;
	}

	public GrupoFaturamento consultarGrupoFaturamento() throws NegocioException {

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("idPeriodicidade", 3L);
		Collection<GrupoFaturamento> IteradorGrupos = controladorFatura.consultarGrupoFaturamento(filtro);
		GrupoFaturamento grupo = new GrupoFaturamentoImpl();
		Iterator<GrupoFaturamento> grupos = IteradorGrupos.iterator();
		while (grupos.hasNext()) {
			grupo = grupos.next();
		}
		return grupo;
	}

	private Map<String, Object> gerarParametros(Operacao operacao, Processo processo, GrupoFaturamento grupoFaturamento, Rota rota) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		operacao.setDescricao("Registrar Leitura");
		operacao.setChavePrimaria(17L);

		processo.setOperacao(operacao);
		processo.setSituacao(1);

		parametros.put("processo", processo);
		parametros.put("idRota", String.valueOf(rota.getChavePrimaria()));
		parametros.put("idGrupoFaturamento", String.valueOf(grupoFaturamento.getChavePrimaria()));
		return parametros;
	}

}
