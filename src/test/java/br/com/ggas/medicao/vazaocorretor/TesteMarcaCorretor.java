
package br.com.ggas.medicao.vazaocorretor;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.vazaocorretor.impl.MarcaCorretorImpl;

public class TesteMarcaCorretor extends GGASTestCase {

	@Before
	public void setUp() {

		cargaInicial.carregar();
	}

	/**
	 * Inserir Marca Corretor
	 */
	@Test
	@Transactional
	public void testeInserirMarcaCorretor() throws GGASException {

		int qtdInicio = fachada.consultarMarcasDosCorretoresDeVazaoCadastrados().size();
		MarcaCorretor marca = montarMarcaCorretor();
		fachada.inserirTabelaAuxiliar(marca);
		int qtdFim = fachada.consultarMarcasDosCorretoresDeVazaoCadastrados().size();

		Assert.assertEquals(qtdInicio + 1, qtdFim);
	}

	/**
	 * Remover Marca Corretor
	 */
	@Test
	@Transactional
	public void testeRemoverMarcaCorretor() throws GGASException {

		MarcaCorretor marcaInserida = (montarMarcaCorretor());
		super.salvar(marcaInserida);

		int qtdInicio = fachada.consultarMarcasDosCorretoresDeVazaoCadastrados().size();
		getSesstion().delete(marcaInserida);
		int qtdFim = fachada.consultarMarcasDosCorretoresDeVazaoCadastrados().size();
		Assert.assertEquals(qtdInicio - 1, qtdFim);
	}

	/**
	 * Atualizar Marca Corretor
	 */
	@Test
	@Transactional
	public void testeAtualizarMarcaCorretor() throws GGASException {

		MarcaCorretor marca = montarMarcaCorretor();
		super.salvar(marca);

		MarcaCorretor marcaInserida = (MarcaCorretor) buscar(MarcaCorretor.class);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chavePrimaria", marcaInserida.getChavePrimaria());

		String descricaoAntesAlteracao = marcaInserida.getDescricao();
		marcaInserida.setDescricao("TESTE");
		getSesstion().update(marcaInserida);

		Collection<MarcaCorretor> lista = fachada.consultarMarcaCorretor(filtro);
		Assert.assertTrue(lista.size() == 1);

		String descricaoPosAlteracao = ((MarcaCorretor) lista.iterator().next()).getDescricao();
		Assert.assertNotNull(descricaoPosAlteracao);
		Assert.assertNotEquals(descricaoAntesAlteracao, descricaoPosAlteracao);
	}

	/**
	 * Atualizar Marca Corretor
	 */
	@Test
	@Transactional
	public final void testeConsultarMarcaCorretor() throws GGASException {

		MarcaCorretor marca = montarMarcaCorretor();
		super.salvar(marca);

		Map<String, Object> filtro = new HashMap<String, Object>();

		MarcaCorretor marcaInserida = (MarcaCorretor) buscar(MarcaCorretor.class);
		super.salvar(montarMarcaCorretor());

		filtro.put("descricao", marcaInserida.getDescricao());
		filtro.put("descricaoAbrevida", marcaInserida.getDescricaoAbreviada());
		filtro.put("chavePrimaria", marcaInserida.getChavePrimaria());
		Collection<MarcaCorretor> listMarcaCorretor = fachada.consultarMarcaCorretor(filtro);

		MarcaCorretor marcaCorretorConsultada = (MarcaCorretor) listMarcaCorretor.iterator().next();
		Assert.assertNotNull(marcaCorretorConsultada);
		Assert.assertEquals(marcaCorretorConsultada.getChavePrimaria(), marcaInserida.getChavePrimaria());
		Assert.assertEquals(marcaCorretorConsultada.getDescricao(), marcaInserida.getDescricao());
		Assert.assertEquals(marcaCorretorConsultada.getDescricaoAbreviada(), marcaInserida.getDescricaoAbreviada());
	}

	@Test
	@Transactional
	public final void testeConsultarMarcaCorretorPorChave() throws GGASException {

		MarcaCorretor marcaInserida = montarMarcaCorretor();
		super.salvar(marcaInserida);

		Map<String, Object> filtro = new HashMap<String, Object>();
		filtro.put("chavePrimaria", marcaInserida.getChavePrimaria());
		MarcaCorretor marcaConsultada = (MarcaCorretor) fachada.consultarMarcaCorretor(filtro).iterator().next();

		Assert.assertNotNull(marcaConsultada);
		Assert.assertEquals(marcaConsultada.getChavePrimaria(), marcaInserida.getChavePrimaria());
		Assert.assertEquals(marcaConsultada.getDescricao(), marcaInserida.getDescricao());
		Assert.assertEquals(marcaConsultada.getDescricaoAbreviada(), marcaInserida.getDescricaoAbreviada());
	}

	private MarcaCorretor montarMarcaCorretor() {

		MarcaCorretor marca = new MarcaCorretorImpl();
		marca.setDescricao("MARCA");
		marca.setDescricaoAbreviada("MR");
		marca.setUltimaAlteracao(new Date());
		marca.setHabilitado(true);
		marca.setVersao(1);

		return marca;
	}

}
