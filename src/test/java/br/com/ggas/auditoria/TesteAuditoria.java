package br.com.ggas.auditoria;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.com.ggas.cadastro.imovel.Coluna;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.contabil.ControladorContabil;
import br.com.ggas.contabil.EventoComercialLancamento;
import br.com.ggas.geral.exception.GGASException;

public class TesteAuditoria extends GGASTestCase {

	@Autowired
	@Qualifier("controladorAuditoria")
	private ControladorAuditoria controladorAuditoria;

	@Autowired
	@Qualifier("controladorContabil")
	private ControladorContabil controladorContabil;

	@Test
	public void consultarColunasPorCodigoVariavel() throws GGASException {

		List<String> codigosEsperados = Arrays.asList("CODIGO_CLIENTE", "CONTA_AUXILIAR", "NUMERO_NOTA", "NOME");

		Collection<EventoComercialLancamento> eventos = Arrays.asList(criarEventoComercialLancamento(1L),
				criarEventoComercialLancamento(2L));

		Set<String> codigos = controladorContabil.getDescricoesEventoComercialLancamento(eventos);

		Map<String, Coluna> colunaPorCodigo = controladorAuditoria.obterColunasPorCodigoVariavel(codigos);

		assertEquals(codigosEsperados.size(), colunaPorCodigo.size());

		for (String codigo : codigosEsperados) {
			assertEquals(codigo, colunaPorCodigo.get(codigo).getCodigoVariavel());
		}

	}

	private EventoComercialLancamento criarEventoComercialLancamento(Long chavePrimaria) {
		EventoComercialLancamento lancamento = controladorContabil.criarEventoComercialLancamento();
		lancamento.setChavePrimaria(chavePrimaria);
		lancamento.setDescricaoContaAuxiliarCredito("#CODIGO_CLIENTE#");
		lancamento.setDescricaoContaAuxiliarDebito("#CONTA_AUXILIAR#");
		lancamento.setDescricaoHistorico("RECEBIMENTO  #NUMERO_NOTA# #NOME#");
		return lancamento;
	}

}
