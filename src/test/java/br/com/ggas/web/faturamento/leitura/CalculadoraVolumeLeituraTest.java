/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura;

import br.com.ggas.comum.GGASTestSpring;
import br.com.ggas.medicao.medidor.ControladorMedidor;
import br.com.ggas.web.faturamento.leitura.dto.LeituraMovimentoDTO;
import org.easymock.EasyMockRule;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

/**
 * @author jose.victor@logiquesistemas.com.br
 */
public class CalculadoraVolumeLeituraTest extends GGASTestSpring {

	@Rule
	public EasyMockRule mock = new EasyMockRule(this);

	@Mock
	private ControladorMedidor controladorMedidor;

	@TestSubject
	@Autowired
	private CalculadoraVolumeLeitura calculadora;

	@Test
	public void testCalcularVolume() {
		LeituraMovimentoDTO leitura = new LeituraMovimentoDTO();
		leitura.setLeituraAtual(2000d);
		leitura.setLeituraAnterior(1001d);
		final Double retorno = calculadora.calcularVolume(leitura);

		assertThat(retorno, equalTo(999d));
	}

	@Test
	public void testCalcularVolumeViradaMedidor() {
		expect(controladorMedidor.obterDigitoMedidorPorLeituraMovimento(1L)).andReturn(6);
		replay(controladorMedidor);

		LeituraMovimentoDTO leitura = new LeituraMovimentoDTO();
		leitura.setChaveLeituraMovimento(1L);
		leitura.setLeituraAnterior(999800d);
		leitura.setLeituraAtual(10d);
		final Double retorno = calculadora.calcularVolume(leitura);

		assertThat(retorno, equalTo(210D));
	}

	@Test
	public void testCalcularVolumeNullSafe() {
		LeituraMovimentoDTO leitura = new LeituraMovimentoDTO();
		final Double retorno = calculadora.calcularVolume(leitura);

		assertNull(retorno);
	}
}
