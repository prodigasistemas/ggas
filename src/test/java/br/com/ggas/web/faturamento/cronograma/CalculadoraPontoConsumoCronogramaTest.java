/*
 * Copyright (C) <2019> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2019 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.cronograma;

import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.faturamento.cronograma.AtividadeSistema;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.util.Fachada;
import org.easymock.EasyMockRule;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.hamcrest.CoreMatchers.equalTo;

/**
 * @author jose.victor@logiquesistemas.com.br
 */
public class CalculadoraPontoConsumoCronogramaTest extends GGASTestCase {

	@Rule
	public EasyMockRule mock = new EasyMockRule(this);

	@Autowired
	@TestSubject
	private CalculadoraPontoConsumoCronograma calculadora;

	@Mock
	private ControladorLeituraMovimento controladorLeituraMovimento;

	@Test
	@Transactional
	public void testVerificarGeracaoAtividadesSistemaNecessarias() throws GGASException {

		final AtividadeSistema gerarDados = Fachada.getInstancia().obterAtividadeSistema(1L);
		final AtividadeSistema realizarLeitura = Fachada.getInstancia().obterAtividadeSistema(2L);
		final AtividadeSistema retornarLeitura = Fachada.getInstancia().obterAtividadeSistema(3L);
		final AtividadeSistema consistirLeitura = Fachada.getInstancia().obterAtividadeSistema(4L);

		expect(controladorLeituraMovimento.consultarQuantidadePontosConsumoNaAtividade(1L, 1L, 1, 1))
				.andReturn(0L);
		expect(controladorLeituraMovimento.consultarQuantidadePontosConsumoNaAtividade(2L, 1L, 1, 1))
				.andReturn(1L);
		expect(controladorLeituraMovimento.consultarQuantidadePontosConsumoNaAtividade(3L, 1L, 1, 1))
				.andReturn(2L);
		expect(controladorLeituraMovimento.consultarQuantidadePontosConsumoNaAtividade(4L, 1L, 1, 1))
				.andReturn(3L);
		replay(controladorLeituraMovimento);

		final Map<AtividadeSistema, Long> mapAtividadeSistema = calculadora.gerarQuantidadesPontoConsumo(1L, 1, 1);

		Assert.assertThat(mapAtividadeSistema.get(gerarDados), equalTo(0L));
		Assert.assertThat(mapAtividadeSistema.get(realizarLeitura), equalTo(1L));
		Assert.assertThat(mapAtividadeSistema.get(retornarLeitura), equalTo(2L));
		Assert.assertThat(mapAtividadeSistema.get(consistirLeitura), equalTo(3L));
	}

}
