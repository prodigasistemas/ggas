/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.web.faturamento.leitura;

import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.impl.GrupoFaturamentoImpl;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.comum.GGASTestSpring;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.contrato.contrato.impl.RepositorioAnaliseMedicao;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaAtividadeFaturamento;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.anormalidade.ControladorAnormalidade;
import br.com.ggas.medicao.leitura.ControladorLeituraMovimento;
import br.com.ggas.medicao.leitura.LeituraMovimento;
import br.com.ggas.medicao.leitura.SituacaoLeituraMovimento;
import br.com.ggas.medicao.leitura.impl.LeituraMovimentoImpl;
import br.com.ggas.medicao.leitura.impl.SituacaoLeituraMovimentoImpl;
import br.com.ggas.medicao.rota.ControladorRota;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.Pair;
import br.com.ggas.web.faturamento.leitura.dto.AnormalidadeDTO;
import br.com.ggas.web.faturamento.leitura.dto.DadosMedicaoDTO;
import br.com.ggas.web.faturamento.leitura.dto.DatatablesServerSideDTO;
import br.com.ggas.web.faturamento.leitura.dto.LeituraMovimentoDTO;
import br.com.ggas.web.faturamento.leitura.dto.PesquisaLeituraMovimento;
import com.google.common.collect.Lists;
import org.easymock.EasyMockRule;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Implementação de testes unitários de leitura movimento negocio
 *
 * @author jose.victor@logiquesistemas.com.br
 */
public class LeituraMovimentoNegocioTest extends GGASTestSpring {

	@Rule
	public EasyMockRule mocks = new EasyMockRule(this);

	@Autowired
	@TestSubject
	private LeituraMovimentoNegocio leituraMovimentoNegocio;

	@Mock
	private ControladorLeituraMovimento controladorLeituraMovimento;

	@Mock
	private ControladorAnormalidade anormalidade;

	@Mock
	private RepositorioAnaliseMedicao repositorioContratoPontoConsumo;

	@Mock
	private ControladorCronogramaAtividadeFaturamento controladorAtvFaturamento;

	@Mock
	private ControladorRota controladorRota;

	@Test
	public void testBuscarLeituras() {
		final LeituraMovimentoImpl leituraPendente = new LeituraMovimentoImpl();
		leituraPendente.setChavePrimaria(1L);
		leituraPendente.setDataLeitura(DataUtil.toDate(LocalDateTime.of(2018, 1, 1, 0, 0)));
		leituraPendente.setLeituraAnterior(BigDecimal.valueOf(200));
		leituraPendente.setValorLeitura(BigDecimal.valueOf(250));
		leituraPendente.setPressaoInformada(BigDecimal.valueOf(1.033));
		leituraPendente.setTemperaturaInformada(BigDecimal.valueOf(24));
		PontoConsumo pontoConsumo = new PontoConsumoImpl();
		pontoConsumo.setChavePrimaria(2L);
		pontoConsumo.setDescricao("Ponto de Consumo Teste");
		leituraPendente.setPontoConsumo(pontoConsumo);
		SituacaoLeituraMovimento situacao = new SituacaoLeituraMovimentoImpl();
		situacao.setDescricao("Pendente");
		leituraPendente.setSituacaoLeitura(situacao);
		leituraPendente.setCodigoAnormalidadeLeitura(2L);

		expect(anormalidade.listarAnormalidadesLeituraLazy()).andReturn(Lists.newArrayList());
		ContratoPontoConsumo contratoPontoConsumo = new ContratoPontoConsumoImpl();
		contratoPontoConsumo.setNumeroFatorCorrecao(BigDecimal.valueOf(1.03d));
		expect(repositorioContratoPontoConsumo.obterContratoParaAnaliseMedicao(2L)).andReturn(contratoPontoConsumo);

		LeituraMovimentoDTO esperado = new LeituraMovimentoDTO();
		esperado.setChaveLeituraMovimento(1L);
		esperado.setPontoConsumo("Ponto de Consumo Teste");
		esperado.setChavePontoConsumo(2L);
		esperado.setDataLeitura("01/01/2018");
		esperado.setLeituraAnterior(200.0);
		esperado.setLeituraAtual(250.0);
		esperado.setVolume(50.0d);
		esperado.setPressao(1.033);
		esperado.setTemperatura(24.0);
		esperado.setFcPtz(1.03);
		esperado.setVolumePtz(51.5);
		esperado.setAnormalidadeLeitura(new AnormalidadeDTO(null, "Anormalidade Não Identificada"));
		esperado.setSituacaoLeitura(situacao);

		expect(controladorLeituraMovimento.criarListaLeituraMovimentoDTO(anyObject())).andReturn(new Pair<>(Lists.newArrayList(esperado), 1L));

		replay(controladorLeituraMovimento, anormalidade, repositorioContratoPontoConsumo);

		final DatatablesServerSideDTO<LeituraMovimentoDTO> retorno = leituraMovimentoNegocio.buscarLeituras(new PesquisaLeituraMovimento());



		assertThat(retorno, equalTo(new DatatablesServerSideDTO<>(0, 1L, Lists.newArrayList(esperado))));
	}

	@Test
	public void testAtualizarDadosMedicao() throws GGASException {
		final DadosMedicaoDTO dados = new DadosMedicaoDTO();
		dados.setChaveAnormalidade(1L);
		dados.setChaveLeitura(2L);
		dados.setNovaLeitura(250d);
		dados.setPressao(1.03d);
		final LocalDate now = LocalDate.now();
		dados.setDataLeitura(now);
		dados.setObservacaoLeitura("Observacao");
		dados.setTemperatura(24d);
		final LeituraMovimento leituraMock = mock(LeituraMovimento.class);

		leituraMock.setValorLeitura(BigDecimal.valueOf(250d));
		expectLastCall().times(1);

		leituraMock.setCodigoAnormalidadeLeitura(1L);
		expectLastCall().times(1);

		leituraMock.setPressaoInformada(BigDecimal.valueOf(1.03d));
		expectLastCall().times(1);

		leituraMock.setDataLeitura(DataUtil.toDate(now));
		expectLastCall().times(1);

		leituraMock.setObservacaoLeitura("Observacao");
		expectLastCall().times(1);

		leituraMock.setTemperaturaInformada(BigDecimal.valueOf(24d));
		expectLastCall().times(1);

		leituraMock.setIndicadorConfirmacaoLeitura(true);
		expectLastCall().times(1);

		leituraMock.setSituacaoLeitura(anyObject());
		expectLastCall().times(1);

		controladorLeituraMovimento.atualizar(leituraMock);
		expectLastCall().times(1);
		expect(controladorLeituraMovimento.obter(2L)).andReturn(leituraMock);
		leituraMock.setOrigem("ANALISE_MEDICAO");
		expectLastCall().times(1);

		final GrupoFaturamentoImpl grupo = new GrupoFaturamentoImpl();
		grupo.setChavePrimaria(1L);
		grupo.setAnoMesReferencia(201809);
		expect(controladorRota.obterGrupoFaturamento(1L)).andReturn(grupo);
		controladorAtvFaturamento.atualizarAtividadeFaturamentoSeExistir(1L, 2, 201809);
		expectLastCall();

		replay(controladorLeituraMovimento, leituraMock, controladorRota, controladorAtvFaturamento);

		leituraMovimentoNegocio.atualizarDadosMedicao(1L, Lists.newArrayList(dados));

		verify(controladorLeituraMovimento, leituraMock);
	}

	@Test
	public void testBuscarRangeLeitura() {

		expect(controladorLeituraMovimento.buscarInicioLeituraMovimento(anyLong(), anyLong()))
				.andReturn(LocalDateTime.of(2018, 12, 1, 0, 0));
		replay(controladorLeituraMovimento);

		final Pair<LocalDateTime, LocalDateTime> retorno = leituraMovimentoNegocio.buscarRangeLeitura(1L, 1L);

		assertThat(retorno.getFirst(), equalTo(LocalDateTime.of(2018, 12, 1, 0, 0)));
		assertThat(retorno.getSecond(), equalTo(LocalDateTime.of(2019, 1, 1, 0, 0)));
	}

	@Test
	public void testBuscaRangeLeituraInexistente() {
		expect(controladorLeituraMovimento.buscarInicioLeituraMovimento(anyLong(), anyLong()))
				.andReturn(null);
		replay(controladorLeituraMovimento);

		final Pair<LocalDateTime, LocalDateTime> retorno = leituraMovimentoNegocio.buscarRangeLeitura(1L, 1L);
		LocalDateTime segundo = retorno.getFirst().plusMonths(1);

		assertThat(retorno.getSecond(), equalTo(segundo));
	}

	@Test
	public void testBuscaRangeLeituraNullSafe() {

		final Pair<LocalDateTime, LocalDateTime> retorno = leituraMovimentoNegocio.buscarRangeLeitura(null, null);
		LocalDateTime segundo = retorno.getFirst().plusMonths(1);

		assertThat(retorno.getSecond(), equalTo(segundo));
	}
}
