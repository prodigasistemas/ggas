package br.com.ggas.faturamento.batch;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.apache.commons.io.FileUtils;

import br.com.ggas.batch.impl.ProcessoImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.controleacesso.impl.OperacaoImpl;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;

public class TesteEncerrarCicloFaturamento extends GGASTestCase{

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_ENCERRAR_CICLO_FATURAMENTO.sql";
	private static final String USER_HOME = System.getProperty("user.home");
	
	@Autowired
	@Qualifier(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA)
	ControladorFatura controladorFatura;
	
	@Autowired
	@Qualifier(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA)
	ControladorParametroSistema controladorParametroSistema;

	@Transactional
	@Before
	public void setup() throws FileNotFoundException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}
	
	@Transactional
	@Test
	public void testeBatchEncerrarCicloFaturamento() throws NegocioException, GGASException, IOException {
		alterarDiretorios();
		criarOuApagarDiretorios(true);
		alterarParametroAnoMes("201408");
		EncerrarCicloFaturamentoBatch batch = new EncerrarCicloFaturamentoBatch();
		String logProcessamento = batch.processar(criarParametrosParaProcesso());
		criarOuApagarDiretorios(false);
		assertNotNull(logProcessamento);
	}
	
	@Transactional
	@Test
	public void testeBatchEncerrarCicloFaturamentoMensal() throws NegocioException, GGASException, IOException {
		alterarDiretorios();
		criarOuApagarDiretorios(true);
		alterarParametroAnoMes("202005");
		EncerrarCicloFaturamentoBatch batch = new EncerrarCicloFaturamentoBatch();
		Map<String, Object> parametros = criarParametrosParaProcesso();
		parametros.put("idGrupoFaturamento", "999998");
		String logProcessamento = batch.processar(parametros);
		criarOuApagarDiretorios(false);
		assertNotNull(logProcessamento);
	}
	
	private Map<String, Object> criarParametrosParaProcesso() throws NegocioException {
		Map<String, Object> parametros = new HashMap<String, Object>();
		ProcessoImpl processo = new ProcessoImpl();
		OperacaoImpl operacao = new OperacaoImpl();
		processo.setChavePrimaria(999998L);
		processo.setOperacao(operacao);
		parametros.put("processo", processo);
		parametros.put("idGrupoFaturamento", "999999");
		return parametros;
	}
	
	
	private void alterarDiretorios() throws NegocioException, ConcorrenciaException {
		ParametroSistema parametroSistema = controladorParametroSistema.obterParametroPorCodigo("DIRETORIO_ARQUIVOS_FATURA");
		parametroSistema.setValor(USER_HOME + File.separator + "fatura");
		controladorParametroSistema.atualizar(parametroSistema);
	}
	
	private void criarOuApagarDiretorios(boolean criaDiretorio) throws IOException {
		if (criaDiretorio) {
			new File(USER_HOME + File.separator + "fatura").mkdirs();
		} else {
			FileUtils.deleteDirectory(new File(USER_HOME + File.separator + "fatura"));
		}
	}
	
	private void alterarParametroAnoMes(String valor) throws GGASException {
		ParametroSistema parametro = fachada.obterParametroPorCodigo(Constantes.PARAMETRO_REFERENCIA_FATURAMENTO);
		parametro.setValor(valor);
		super.salvar(parametro);
	}

}
