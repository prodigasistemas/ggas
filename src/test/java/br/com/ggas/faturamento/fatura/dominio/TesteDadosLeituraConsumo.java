package br.com.ggas.faturamento.fatura.dominio;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.DadosLeituraConsumoFatura;
import br.com.ggas.faturamento.fatura.dominio.impl.DadosLeituraConsumo;
import br.com.ggas.geral.exception.NegocioException;

public class TesteDadosLeituraConsumo extends GGASTestCase{ 

	

	@Autowired
	@Qualifier(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA)
	private ControladorFatura controladorFatura;
	
	
	@Before
	public void iniciar() {
	}

	@Test
	public void testConsumoApuradoComFatorCorrecao() {
		DadosLeituraConsumo dados = new DadosLeituraConsumo();
		
		BigDecimal consumoApurado = dados.verificarConsumoApuradoPeloNumeroFatorCorrecao(criarContratoComFatorCorrecao(), criarDadosLeituraConsumoFatura());
		
		Assert.assertNotNull(consumoApurado);
	}
	
	@Test
	public void testConsumoApuradoSemFatorCorrecao() {
		DadosLeituraConsumo dados = new DadosLeituraConsumo();
		
		BigDecimal consumoApurado = dados.verificarConsumoApuradoPeloNumeroFatorCorrecao(new ContratoPontoConsumoImpl(), criarDadosLeituraConsumoFatura());
		
		Assert.assertNotNull(consumoApurado);
	}
	
	private ContratoPontoConsumo criarContratoComFatorCorrecao() {
		ContratoPontoConsumo contrato = new ContratoPontoConsumoImpl();
		
		contrato.setNumeroFatorCorrecao(BigDecimal.ONE);
		
		return contrato;
	}
	
	private DadosLeituraConsumoFatura criarDadosLeituraConsumoFatura() {
		DadosLeituraConsumoFatura dados;
		try {
			dados = controladorFatura.criarDadosLeituraConsumoFatura();
			dados.setLeituraAnterior(BigDecimal.ZERO);
			dados.setLeituraAtual(BigDecimal.ONE);
			dados.setConsumoFaturado(new BigDecimal(100));

			return dados;
		} catch (NegocioException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}

	