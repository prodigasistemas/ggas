package br.com.ggas.faturamento.fatura;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.faturamento.ControladorCalculoFornecimentoGas;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaItem;
import br.com.ggas.faturamento.FaturaItemImpressao;
import br.com.ggas.faturamento.cronograma.ControladorCronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.cronograma.CronogramaAtividadeFaturamento;
import br.com.ggas.faturamento.fatura.impl.ControladorFaturaImpl;
import br.com.ggas.faturamento.impl.DadosFaixaVO;
import br.com.ggas.faturamento.impl.DadosFornecimentoGasVO;
import br.com.ggas.faturamento.impl.DadosVigenciaVO;
import br.com.ggas.faturamento.rubrica.Rubrica;
import br.com.ggas.faturamento.tarifa.TarifaVigencia;
import br.com.ggas.faturamento.tarifa.TarifaVigenciaDesconto;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;
import br.com.ggas.medicao.consumo.HistoricoConsumo;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.ColecaoPaginada;
import br.com.ggas.util.DataUtil;
import br.com.ggas.util.ServiceLocator;
import br.com.ggas.web.faturamento.fatura.FaturaAction;

public class FaturaTest extends GGASTestCase {

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_FATURA.sql";
	private static final String DIRETORIO_ARQUIVOS_FATURA = "DIRETORIO_ARQUIVOS_FATURA";
	private static final String DIRETORIO = System.getProperty("user.home") + File.separator + "TesteGerarNotaFiscalFatura";

	@Autowired
	@Qualifier(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA)
	ControladorFatura controladorFatura;

	@Autowired
	@Qualifier(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE)
	ControladorCliente controladorCliente;

	@Autowired
	@Qualifier(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO)
	ControladorHistoricoConsumo controladorHistoricoConsumo;
	
	@Autowired
	@Qualifier(ControladorCronogramaAtividadeFaturamento.BEAN_ID_CONTROLADOR_CRONOGRAMA_ATIVIDADE_FATURAMENTO)
	ControladorCronogramaAtividadeFaturamento controladorCronogramaAtividadeFaturamento;
	
	@Autowired
	@Qualifier(ControladorEntidadeConteudo.BEAN_ID_CONTROLADOR_ENTIDADE_CONTEUDO)
	ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Transactional
	@Before
	public void setup() throws IOException, GGASException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
		alterarParametroDiretorio();
	}

	@After
	public void limpar() {
		FileSystemUtils.deleteRecursively(new File(DIRETORIO));
	}

	@Transactional
	@Test
	public void testBuscarFaturaPorTipo() throws NegocioException {
		
		Map<String, Object> filtro = new HashMap<>();
		ColecaoPaginada colecaoPaginada = new ColecaoPaginada();
		colecaoPaginada.setIndex(0);
		colecaoPaginada.setObjectsPerPage(10);
		filtro.put(FaturaAction.TIPO_FATURA, 0);
		filtro.put("colecaoPaginada", colecaoPaginada);
		filtro.put("anoMesReferencia", 82018);
		Collection<Fatura> faturas = controladorFatura.consultarFaturaPesquisa(filtro);
		Assert.assertEquals(1, faturas.size());
		Assert.assertEquals(99999L, faturas.iterator().next().getChavePrimaria());

		filtro.put(FaturaAction.TIPO_FATURA, 1);
		faturas = controladorFatura.consultarFaturaPesquisa(filtro);

		Assert.assertEquals(1, faturas.size());
		Assert.assertEquals(99998L, faturas.iterator().next().getChavePrimaria());
	}

	@Transactional
	@Test
	public void testImprimirSegundaViaNotaFiscalFatura() throws GGASException {
		
		Fatura fatura = fachada.obterFatura(99998l, "listaFaturaItem", "faturaGeral");

		byte[] relatorioNF = fachada.gerarImpressaoFaturaSegundaVia(fatura, null);

		assertNotNull(relatorioNF);

	}

	@Transactional
	@Test
	public void testCriarFaturaItemImpressaoDescontoSemImpressaoDescricao() throws Throwable {
		
		ControladorFaturaImpl controladorFaturaImpl = new ControladorFaturaImpl();
		DadosVigenciaVO dadosVigencia = criarObjetoDadosVigenciaVO();
		Fatura fatura = fachada.obterFatura(99999l, "listaFaturaItem", "faturaGeral");
		Collection<FaturaItem> faturaItem = fachada.listarFaturaItemPorChaveFatura(fatura.getChavePrimaria());
		FaturaItemImpressao faturaItemImpressao = (FaturaItemImpressao) controladorFatura
				.consultarFaturaItemImpressaoPorFaturaItem(faturaItem.iterator().next()).iterator().next();
		TarifaVigenciaDesconto tarifaVigenciaDesconto = fachada.obterTarifaVigenciaDesconto(99998l);

		tarifaVigenciaDesconto.setImpressaoDescricao(false);

		FaturaItemImpressao faturaItemDesconto =
				(FaturaItemImpressao) chamarMetodoPrivado(controladorFaturaImpl, "criarFaturaItemImpressaoDesconto", dadosVigencia,
						faturaItemImpressao, faturaItem.iterator().next(), false, new BigDecimal(10), tarifaVigenciaDesconto);

		assertNotNull(faturaItemDesconto);
	}

	@Transactional
	@Test
	public void testeRemoverFatura() throws Throwable {
		
		Fatura fatura = fachada.obterFatura(99997l, "listaFaturaItem", "faturaGeral");
		Long[] chavesPrimarias = { fatura.getChavePrimaria() };
		controladorFatura.removerFaturas(chavesPrimarias, null);
	}

	@Transactional
	@Test
	public void testePopularNotificaoCorteFatura() throws Throwable {
		
		ControladorFaturaImpl controladorFaturaImpl = new ControladorFaturaImpl();
		Fatura fatura = (Fatura) controladorFatura.criar();
		Cliente cliente = (Cliente) controladorCliente.obter(997788l);
		chamarMetodoPrivado(controladorFaturaImpl, "popularIndicadorNotificacaoCorteFatura", fatura, cliente, Boolean.FALSE);
	}

	@Transactional
	@Test
	public void testeCalculoFornecimentoGas() throws Throwable {
		
		ControladorCalculoFornecimentoGas controladorCalculo = (ControladorCalculoFornecimentoGas) ServiceLocator.getInstancia()
				.getBeanPorID(ControladorCalculoFornecimentoGas.BEAN_ID_CONTROLADOR_CALCULO_FORNECIMENTO_GAS);
		Rubrica rubrica = fachada.obterRubrica(1l);
		Collection<PontoConsumo> colecaoPontoConsumo = new HashSet<>();
		PontoConsumo pontoConsumo = fachada.obterPontoConsumo(999999l);
		colecaoPontoConsumo.add(pontoConsumo);

		Date dataInicio = DataUtil.decrementarDia(new Date(), 1);
		Date dataFim = DataUtil.incrementarDia(dataInicio, 1);
		DadosFornecimentoGasVO dados = controladorCalculo.calcularValorFornecimentoGas(rubrica.getItemFatura(), dataInicio, dataFim,
				BigDecimal.TEN, colecaoPontoConsumo, false, null, false, null, Boolean.FALSE, null, null);

		assertNotNull(dados);
	}

	@Transactional
	@Test
	public void testeConsultarConsumoHistorico() {
		
		List<Long> idsPontoConsumo = new ArrayList<>();

		idsPontoConsumo.add(999999l);

		Map<Long, HistoricoConsumo> listaConsumoHistorico =
				controladorHistoricoConsumo.obterHistoricosDeConsumoNaoFaturados(idsPontoConsumo, 201602, 1);

		assertNotEquals(listaConsumoHistorico.size(), 2);
	}

	@Transactional
	@Test
	public void testeConsultarFatura() throws NegocioException {
		
		Map<String, Object> filtro = new HashMap<>();
		filtro.put("chavePrimaria", 99997l);
		Collection<Fatura> listaFatura = controladorFatura.consultarFatura(filtro);
		assertNotNull(listaFatura);
	}
	
	@Transactional
	@Test
	public void testeFaturaConsumoZero() throws Throwable {
		Collection<PontoConsumo> colecaoPontoConsumo = new HashSet<>();
		PontoConsumo pontoConsumo = fachada.obterPontoConsumo(999999l);
		colecaoPontoConsumo.add(pontoConsumo);
		
		List<HistoricoConsumo> listaHistoricoConsumo = new ArrayList<HistoricoConsumo>();
		HistoricoConsumo historico = (HistoricoConsumo) controladorHistoricoConsumo.obter(999997l);
		listaHistoricoConsumo.add(historico);
		
		List<FaturaItemImpressao> listaFaturaItemImpressao = new ArrayList<FaturaItemImpressao>();
		FaturaItemImpressao faturaItemImpressao = controladorFatura.criarFaturaItemImpressao();
		faturaItemImpressao.setValorTotal(BigDecimal.ZERO);
		faturaItemImpressao.setIndicadorDesconto(Boolean.FALSE);
		listaFaturaItemImpressao.add(faturaItemImpressao);
		
		List<DadosResumoFatura> dadosResumoSemConsumo = new ArrayList<DadosResumoFatura>();
		DadosResumoFatura dadosResumoFatura = controladorFatura.criarDadosResumoFatura();

		dadosResumoFatura.setListaHistoricoConsumo(listaHistoricoConsumo);
		dadosResumoFatura.setListaFaturaItemImpressao(listaFaturaItemImpressao);
		
		dadosResumoSemConsumo.add(dadosResumoFatura);
		
		CronogramaAtividadeFaturamento cronogramaAtividadeFaturamento = (CronogramaAtividadeFaturamento) controladorCronogramaAtividadeFaturamento.obter(999999l);
		
		controladorFatura.validarPontosConsumoSemConsumo(dadosResumoSemConsumo, cronogramaAtividadeFaturamento, null, colecaoPontoConsumo);
		
	}
	
	@Test
	@Transactional
	public void testeValidarSituacaoIntegracao() throws Throwable {
		Fatura fatura = (Fatura) controladorFatura.obter(99995l);
		ControladorFaturaImpl controladorFaturaImpl = new ControladorFaturaImpl();
		
		chamarMetodoPrivado(controladorFaturaImpl, "validarSituacaoIntegracao", fatura);
	}
	
	private void alterarParametroDiretorio() throws GGASException {
		
		ParametroSistema parametro = fachada.obterParametroPorCodigo(DIRETORIO_ARQUIVOS_FATURA);
		parametro.setValor(DIRETORIO);
		super.salvar(parametro);
	}

	public DadosVigenciaVO criarObjetoDadosVigenciaVO() throws GGASException {
		
		TarifaVigencia tarifaVigencia = fachada.obterTarifaVigencia(99998l);
		DadosVigenciaVO dadosVigenciaVO = new DadosVigenciaVO();
		DateTime dataPeriodoFinal = new DateTime();
		DadosFaixaVO dadosFaixa = new DadosFaixaVO();
		List<DadosFaixaVO> faixa = new ArrayList<>();

		dadosFaixa.setValorFixoComImpostoSemDesconto(new BigDecimal(10));
		dadosFaixa.setValorVariavelComImpostoSemDesconto(new BigDecimal(5));

		faixa.add(dadosFaixa);

		dadosVigenciaVO.setTarifaVigencia(tarifaVigencia);
		dadosVigenciaVO.setDataPeriodoFinal(dataPeriodoFinal);
		dadosVigenciaVO.setDataPeriodoInicial(dataPeriodoFinal.minus(1));
		dadosVigenciaVO.setDadosFaixa(faixa);
		dadosVigenciaVO.setConsumoApurado(new BigDecimal(0));

		return dadosVigenciaVO;
	}
}
