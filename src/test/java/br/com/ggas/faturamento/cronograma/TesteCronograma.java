package br.com.ggas.faturamento.cronograma;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.medicao.consumo.ControladorHistoricoConsumo;

public class TesteCronograma extends GGASTestCase{

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_CRONOGRAMA.sql";

	@Autowired
	@Qualifier(ControladorHistoricoConsumo.BEAN_ID_CONTROLADOR_HISTORICO_CONSUMO)
	ControladorHistoricoConsumo controladorHistoricoConsumo;
	
	@Transactional
	@Before
	public void setup() throws FileNotFoundException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}
	
	@Transactional
	@Test
	public void testConsultaQuantitativoPontoConsumoFaturarGrupo() throws GGASException {
		Long qtdPC = controladorHistoricoConsumo
				.consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamentoSupervisorio(8l, 999999l, 201602, 1);
		assertTrue(qtdPC > 0);
	}
	
	@Transactional
	@Test
	public void testConsultaQuantitativoPontoConsumoConsistirLeitura() throws GGASException {
		Long qtdPC = controladorHistoricoConsumo
				.consultarQuantidadeQtdPontoConsumoFaturaGrupoFaturamentoSupervisorio(4l, 999999l, 201602, 1);
		assertTrue(qtdPC > 0);
	}

}
