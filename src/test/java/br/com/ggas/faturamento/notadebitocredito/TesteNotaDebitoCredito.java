package br.com.ggas.faturamento.notadebitocredito;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.ibm.icu.impl.Assert;

import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.util.Constantes;
import br.com.ggas.web.faturamento.notadebitocredito.NotaDebitoCreditoAction;

public class TesteNotaDebitoCredito extends GGASTestCase{

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_NOTA_DEBITO.sql";
	
	@Autowired
	@Qualifier(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA)
	ControladorFatura controladorFatura;
		
	NotaDebitoCreditoAction notaAction = new NotaDebitoCreditoAction();
	
	@Transactional
	@Before
	public void setup() throws IOException, GGASException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}
	
	@Test
	@Transactional
	public void testProrrogarVencimentoNotaDebito() throws Throwable {
		List<Fatura> faturas = new ArrayList<>();
		Fatura notaInserida = (Fatura) controladorFatura.obter(99999l);
		faturas.add(notaInserida);
		controladorFatura.atualizarVencimentoNotaDebito(faturas);	

	}
	
	@Test
	@Transactional
	public void testCancelarNotaDebito() throws GGASException {
		Long[] chavesPrimarias = new Long[1];
		Fatura notaInserida = (Fatura) controladorFatura.obter(99999l);
		chavesPrimarias[0] = notaInserida.getChavePrimaria();
		controladorFatura.cancelarNotaDebitoCredito(chavesPrimarias, 492l, "Teste", null);
	}
	
	@Test
	@Transactional
	public void testGerarSegundaVia() throws GGASException {
		Constantes.URL_LOGOMARCA_BANCO = "src/main/webapp/images/";
		Fatura notaInserida = (Fatura) controladorFatura.obter(99999l);
		byte[] arquivo = controladorFatura.gerarRelatorioNotaDebitoCredito(notaInserida, Boolean.TRUE);
		assertNotNull(arquivo);
	}
	
	@Test
	@Transactional
	public void testGerarNota() throws GGASException {
		Constantes.URL_LOGOMARCA_BANCO = "src/main/webapp/images/";
		Fatura notaInserida = (Fatura) controladorFatura.obter(99999l);
		byte[] arquivo = controladorFatura.gerarRelatorioNotaDebitoCredito(notaInserida, Boolean.FALSE);
		assertNotNull(arquivo);
	}
		
}
