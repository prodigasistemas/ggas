package br.com.ggas.api;

import br.com.ggas.api.autenticador.Autenticador;
import br.com.ggas.api.autenticador.TokenAutenticador;
import org.junit.Test;

import static org.junit.Assert.*;

public class TesteAutenticador {

	private TokenAutenticador token = null;
	private final long CHAVE_PRIMARIA_USUARIO = 1L;

	@Test
	public void testarInstanciaOk() {
		assertNotNull(Autenticador.getInstance());
	}

	@Test
	public void testarSalvarToken() {
		token = Autenticador.getInstance().buildToken();
		Autenticador.getInstance().salvarToken(CHAVE_PRIMARIA_USUARIO, token.getToken());
		assertTrue(Autenticador.getInstance().isTokenValid(token.getToken()));
	}

	@Test
	public void testarTokenAntigoInvalido() {
		token = Autenticador.getInstance().buildToken();
		Autenticador.getInstance().salvarToken(CHAVE_PRIMARIA_USUARIO, token.getToken());
		assertTrue(Autenticador.getInstance().isTokenValid(token.getToken()));

		TokenAutenticador novoToken = Autenticador.getInstance().buildToken();

		Autenticador.getInstance().salvarToken(CHAVE_PRIMARIA_USUARIO, novoToken.getToken());
		assertFalse(Autenticador.getInstance().isTokenValid(token.getToken()));
		assertTrue(Autenticador.getInstance().isTokenValid(novoToken.getToken()));
	}

	@Test
	public void testarTokenCriadoSaoDiferentes() {
		token = Autenticador.getInstance().buildToken();

		assertNotEquals(token, Autenticador.getInstance().buildToken());
	}

	@Test
	public void testarTokenRemovido() {
		token = Autenticador.getInstance().buildToken();
		Autenticador.getInstance().salvarToken(CHAVE_PRIMARIA_USUARIO, token.getToken());
		assertTrue(Autenticador.getInstance().isTokenValid(token.getToken()));

		Autenticador.getInstance().logout(CHAVE_PRIMARIA_USUARIO, token.getToken());
		assertFalse(Autenticador.getInstance().isTokenValid(token.getToken()));
	}
	
	@Test
	public void testarSalvarTokenExistente() {
		token = Autenticador.getInstance().buildToken();
		Autenticador.getInstance().salvarToken(CHAVE_PRIMARIA_USUARIO, token.getToken());
		Autenticador.getInstance().salvarToken(CHAVE_PRIMARIA_USUARIO, token.getToken());
		assertTrue(Autenticador.getInstance().isTokenValid(token.getToken()));
	}
}


