package br.com.ggas.api;

import br.com.ggas.api.dto.LeituraColetorDTO;
import br.com.ggas.api.dto.UsuarioDTO;
import br.com.ggas.api.rest.MedicaoRest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

/**
 * Esse teste só precisa ser executado para o desenvolvimento, é um teste de integração que só deve ser executado com um GGAS rodando.
 *
 * Os parâmetros dos testes foram para o banco GGAS_SUST
 *
 * PONTOS DE VERIFICAÇÃO PARA O TESTE PASSAR:
 * <p>
 * URL_BASE deve apontar para um GGAS que está executando
 * <p>
 * O usuário e senha utilizado no método autenticar() deve existir no banco que está sendo utilizado
 */
@Ignore
public class TesteIntegracaoAPIRest {

	private static String URL_BASE = "http://localhost:8085/ggas/";

	/**
	 * O valor informado aqui deve existir na coluna POCN_CD_LEGADO na tabela PONTO_CONSUMO.
	 * O ponto consumo referenciado com esse valor, deve possuir uma leitura movimento com status 1,2 ou 3;
	 */
	private long ID_CLIENTE_SGM = 6122L;

	private long CHAVE_PRIMARIA_LEITURA_MOVIMENTO = 7023L; //2260L

	private final Double VALOR_LEITURA = 11.0D;

	private final int STATUS_LEITURA_SUCESSO = 200;
	private final int STATUS_CRIADO_SUCESSO = 201;
	private final int STATUS_ERRO_VALIDACAO = 400;
	private final int STATUS_ERRO_AUTENTICACAO = 403;

	@Test public void testarAutenticar() throws IOException {

		HttpResponse response = autenticar();

		String responseJSON = EntityUtils.toString(response.getEntity(), Charset.defaultCharset());

		assertThat(response.getStatusLine().getStatusCode(), equalTo(STATUS_LEITURA_SUCESSO));
		assertEquals(responseJSON.length(), UUID.randomUUID().toString().length());
	}

	@Test
	public void testarEnviarMedicoes() throws IOException {
		String token = autenticarRetornandoToken();

		HttpResponse response = enviarMedicaoOk(token);

		String responseJSON = EntityUtils.toString(response.getEntity(), Charset.defaultCharset());

		assertEquals(responseJSON, MedicaoRest.SUCESSO_ESCRITA_LEITURA_MOVIMENTO);
		assertThat("Deveria ter criado e retornado status 201 :(", response.getStatusLine().getStatusCode(), equalTo(STATUS_CRIADO_SUCESSO));
	}

	@Test
	public void testarEnviarMedicoesLendoEmSeguida() throws IOException {
		String token = autenticarRetornandoToken();

		HttpResponse response = enviarMedicaoOk(token);

		String responseJSON = EntityUtils.toString(response.getEntity(), Charset.defaultCharset());

		assertEquals(responseJSON, MedicaoRest.SUCESSO_ESCRITA_LEITURA_MOVIMENTO);
		assertThat("Deveria ter criado e retornado status 201 :(", response.getStatusLine().getStatusCode(), equalTo(STATUS_CRIADO_SUCESSO));


		response = lerLeituraMovimento(token, ID_CLIENTE_SGM);

		LeituraColetorDTO leitura = getLeituraResponse(response);
		assertThat(response.getStatusLine().getStatusCode(), equalTo(STATUS_LEITURA_SUCESSO));

		assertEquals("Mensagem foi alterada ou gerou outra que não que o teste esperava",
				MedicaoRest.LEITURA_MOVIMENTO_ENCONTRADA, leitura.getMensagem());

		assertEquals(VALOR_LEITURA, leitura.getValorLeitura());
	}

	/**
	 *
	 * Ordem de id cliente do erro:
	 * 4412
	 * 6122 <-
	 * 134799
	 *
	 * 6122
	 */
	@Test public void testarLerLeituraMovimentoErroSession() throws Exception {
		String token = autenticarRetornandoToken();

		HttpResponse response = lerLeituraMovimento(token, 6122L);

		LeituraColetorDTO leitura = getLeituraResponse(response);
		assertEquals("Mensagem foi alterada ou gerou outra que não que o teste esperava",
				MedicaoRest.LEITURA_MOVIMENTO_ENCONTRADA, leitura.getMensagem());
		assertThat(response.getStatusLine().getStatusCode(), equalTo(STATUS_LEITURA_SUCESSO));
	}


	private HttpResponse enviarMedicaoOk(String token) throws IOException {
		LeituraColetorDTO leituraColetorDTO = new LeituraColetorDTO();
		leituraColetorDTO.setAnoMesFaturamento(201812);
		leituraColetorDTO.setCiclo(1);
		leituraColetorDTO.setPontoConsumo(ID_CLIENTE_SGM);
		leituraColetorDTO.setValorLeitura(VALOR_LEITURA);
		leituraColetorDTO.setTemperaturaInformada(25.0);
		leituraColetorDTO.setPressaoInformada(2.8);
		leituraColetorDTO.setDataLeitura("17/12/2018 16:30");
		leituraColetorDTO.setChavePrimariaLeituraMovimento(CHAVE_PRIMARIA_LEITURA_MOVIMENTO);
		return sendMedicoes(token, leituraColetorDTO);
	}

	@Test public void testarEnviarMedicoesFaltandoChavePrimariaLeituraMovimento() throws IOException {
		String token = autenticarRetornandoToken();
		HttpResponse response = sendMedicoes(token, new LeituraColetorDTO());

		String responseJSON = EntityUtils.toString(response.getEntity(), Charset.defaultCharset());

		assertEquals(responseJSON, MedicaoRest.CHAVE_LEITURA_MOVIMENTO_OBRIGATORIO);
		assertThat(response.getStatusLine().getStatusCode(), equalTo(STATUS_ERRO_VALIDACAO));
	}

	@Test
	public void testarEnviarMedicoesFaltandoAnoMesFaturamento() throws IOException {
		String token = autenticarRetornandoToken();
		LeituraColetorDTO leitura = new LeituraColetorDTO();
		leitura.setChavePrimariaLeituraMovimento(CHAVE_PRIMARIA_LEITURA_MOVIMENTO);
		HttpResponse response = sendMedicoes(token, leitura);

		String responseJSON = EntityUtils.toString(response.getEntity(), Charset.defaultCharset());

		assertEquals(MedicaoRest.ANO_MES_FATURAMENTO_OBRIGATORIO, responseJSON);
		assertThat(response.getStatusLine().getStatusCode(), equalTo(STATUS_ERRO_VALIDACAO));
	}

	@Test public void testarEnviarMedicoesFaltandoCiclo() throws IOException {
		String token = autenticarRetornandoToken();
		LeituraColetorDTO leitura = new LeituraColetorDTO();
		leitura.setAnoMesFaturamento(201812);
		HttpResponse response = sendMedicoes(token, leitura);

		String responseJSON = EntityUtils.toString(response.getEntity(), Charset.defaultCharset());

		assertThat(response.getStatusLine().getStatusCode(), equalTo(STATUS_ERRO_VALIDACAO));
		assertEquals(MedicaoRest.CHAVE_LEITURA_MOVIMENTO_OBRIGATORIO, responseJSON);
	}

	private String autenticarRetornandoToken() throws IOException {
		HttpResponse response = autenticar();
		return getToken(response);
	}

	@Test public void testarEnviarMedicoesSemAutenticar() throws IOException {

		HttpResponse response = sendMedicoes(null, null);

		assertNotEquals(response.getStatusLine().getStatusCode(), equalTo(STATUS_CRIADO_SUCESSO));
		assertEquals(response.getStatusLine().getStatusCode(), STATUS_ERRO_AUTENTICACAO);
	}

	@Test public void testarEnviarMedicoesAposLogoff() throws IOException {
		String token = autenticarRetornandoToken();

		HttpResponse response = enviarMedicaoOk(token);

		String responseJSON = EntityUtils.toString(response.getEntity(), Charset.defaultCharset());

		assertThat(response.getStatusLine().getStatusCode(), equalTo(STATUS_CRIADO_SUCESSO));
		assertEquals(responseJSON, MedicaoRest.SUCESSO_ESCRITA_LEITURA_MOVIMENTO);

		logoff(token);

		response = sendMedicoes(null, null);

		assertNotEquals(response.getStatusLine().getStatusCode(), equalTo(STATUS_CRIADO_SUCESSO));
		assertEquals(response.getStatusLine().getStatusCode(), STATUS_ERRO_AUTENTICACAO);
	}

	private static String montarLeitura(LeituraColetorDTO leitura) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(leitura, LeituraColetorDTO.class);
		return json;
	}

	private HttpResponse sendMedicoes(String token, LeituraColetorDTO leitura) throws IOException {

		StringEntity entity = new StringEntity(montarLeitura(leitura));

		Request request = Request.Post(URL_BASE + "api/v1/medicoes/").body(entity).addHeader("Accept", "application/json")
				.addHeader("Content-type", "application/json");

		if (token != null) {
			request.addHeader("Authorization", "Token " + token);
		}
		return request.execute().returnResponse();

	}

	@Test public void testarLerLeituraMovimento() throws Exception {
		String token = autenticarRetornandoToken();

		HttpResponse response = lerLeituraMovimento(token, ID_CLIENTE_SGM);

		LeituraColetorDTO leitura = getLeituraResponse(response);
		assertEquals("Mensagem foi alterada ou gerou outra que não que o teste esperava",
				MedicaoRest.LEITURA_MOVIMENTO_ENCONTRADA, leitura.getMensagem());
		assertThat(response.getStatusLine().getStatusCode(), equalTo(STATUS_LEITURA_SUCESSO));
	}

	@Test public void testarLerLeituraMovimentoInexistente() throws Exception {
		String token = autenticarRetornandoToken();

		HttpResponse response = lerLeituraMovimento(token, Long.MAX_VALUE);

		LeituraColetorDTO leitura = getLeituraResponse(response);
		assertEquals("Mensagem foi alterada ou gerou outra que não que o teste esperava",
				"Ponto de consumo não encontrado para o código legado "+Long.MAX_VALUE, leitura.getMensagem());
		assertThat(response.getStatusLine().getStatusCode(), equalTo(STATUS_ERRO_VALIDACAO));
	}

	private HttpResponse lerLeituraMovimento(String token, Long idClienteSGM) {

		HttpResponse response = null;
		return getHttpResponse(token, idClienteSGM, response, URL_BASE);
	}

	static HttpResponse getHttpResponse(String token, Long idClienteSGM, HttpResponse response, String urlBase) {
		try {
			StringEntity entity = new StringEntity(idClienteSGM.toString());
			Request request =
					Request.Post(urlBase + "api/v1/medicoes/leitura-movimento/").body(entity).addHeader("Accept", "application/json")
							.addHeader("Content-type", "application/json");

			if (token != null) {
				request.addHeader("Authorization", "Token " + token);
			}
			response = request.execute().returnResponse();
			return response;
		} catch (Exception e) {
			return response;
		}
	}

	public static LeituraColetorDTO getLeituraResponse(HttpResponse httpResponse) {
		LeituraColetorDTO leitura = null;
		Gson gson = new Gson();
		try {
			leitura = gson.fromJson(EntityUtils.toString(httpResponse.getEntity()), LeituraColetorDTO.class);
			return leitura;
		} catch (IOException e) {
			return null;
		}
	}

	private String getToken(HttpResponse response) {
		try {
			return IOUtils.toString(response.getEntity().getContent(), "UTF-8");
		} catch (Exception e) {
			return null;
		}
	}

	private HttpResponse autenticar() throws IOException {
		UsuarioDTO usuario = new UsuarioDTO();
		usuario.setLogin("admin");
		usuario.setSenha("admin");
		Gson gson = new Gson();
		StringEntity entity = new StringEntity(gson.toJson(usuario));

		return Request.Post(URL_BASE + "api/v1/login/").body(entity).addHeader("Accept", "application/json")
				.addHeader("Content-type", "application/json").execute().returnResponse();
	}

	private HttpResponse logoff(String token) throws IOException {

		StringEntity entity = new StringEntity(token);

		Request request = Request.Post(URL_BASE + "api/v1/login/logoff/").body(entity).addHeader("Accept", "application/json")
				.addHeader("Content-type", "application/json");
		return request.execute().returnResponse();

	}
}
