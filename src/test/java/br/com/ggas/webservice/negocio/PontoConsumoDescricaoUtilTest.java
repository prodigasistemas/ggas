/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.webservice.negocio;

import br.com.ggas.cadastro.endereco.Cep;
import br.com.ggas.cadastro.endereco.Endereco;
import br.com.ggas.cadastro.endereco.impl.CepImpl;
import br.com.ggas.cadastro.endereco.impl.EnderecoImpl;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cadastro.imovel.SituacaoConsumo;
import br.com.ggas.cadastro.imovel.impl.PontoConsumoImpl;
import br.com.ggas.cadastro.imovel.impl.SituacaoConsumoImpl;
import br.com.ggas.cadastro.localidade.QuadraFace;
import br.com.ggas.cadastro.localidade.impl.QuadraFaceImpl;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.impl.ContratoPontoConsumoImpl;
import br.com.ggas.webservice.PontoConsumoDescricaoTO;
import org.junit.Test;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author jose.victor@logiquesistemas.com.br
 */
public class PontoConsumoDescricaoUtilTest {

	@Test
	public void converter() {

		SituacaoConsumo situacao = new SituacaoConsumoImpl();
		situacao.setDescricao(SituacaoConsumo.ATIVO);

		Cep cep = new CepImpl();
		cep.setTipoLogradouro("rua");
		cep.setLogradouro("logradouro");
		cep.setBairro("bairro");
		cep.setNomeMunicipio("municipio");
		cep.setUf("estado");

		Endereco endereco = new EnderecoImpl();
		endereco.setCep(cep);
		endereco.setComplemento("complemento");

		QuadraFace quadra = new QuadraFaceImpl();
		quadra.setEndereco(endereco);

		PontoConsumo pontoConsumo = new PontoConsumoImpl();
		pontoConsumo.setChavePrimaria(1L);
		pontoConsumo.setDescricao("descricao");
		pontoConsumo.setQuadraFace(quadra);
		pontoConsumo.setSituacaoConsumo(situacao);
		pontoConsumo.setNumeroImovel("123");
		pontoConsumo.setDescricaoComplemento("desc complemento");

		ContratoPontoConsumo contrato = new ContratoPontoConsumoImpl();
		contrato.setPontoConsumo(pontoConsumo);
		contrato.setChavePrimaria(1L);

		final List<PontoConsumoDescricaoTO> retorno = PontoConsumoDescricaoUtil.converter(singletonList(contrato));

		PontoConsumoDescricaoTO pontoEsperado = new PontoConsumoDescricaoTO();
		pontoEsperado.setChavePrimaria(1L);
		pontoEsperado.setDescricao("descricao");
		pontoEsperado.setSituacao("ATIVO");
		pontoEsperado.setEnderecoCompleto("rua, logradouro, 123, desc complemento, complemento, bairro, estado");

		final List<PontoConsumoDescricaoTO> esperado = singletonList(pontoEsperado);

		assertThat(retorno, equalTo(esperado));
	}
}
