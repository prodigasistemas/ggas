package br.com.ggas.util;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import br.com.ggas.geral.exception.FormatoInvalidoException;

public class TesteBigDecimalUtil {

	@Test
	public void testeConverterCampoStringParaValorBigDecimal() throws FormatoInvalidoException {
		BigDecimal retorno = BigDecimalUtil.converterCampoStringParaValorBigDecimal("100");
		assertEquals(retorno, BigDecimalUtil.CEM);
	}
	
	@Test
	public void testeConverterCampoStringParaValorBigDecimalNulo() throws FormatoInvalidoException {
		BigDecimal retorno = BigDecimalUtil.converterCampoStringParaValorBigDecimal(null);
		assertEquals(retorno, BigDecimal.ZERO);
	}
	
	@Test (expected = FormatoInvalidoException.class)
	public void testeConverterCampoStringParaValorBigDecimalErro() throws FormatoInvalidoException {
		BigDecimal retorno = BigDecimalUtil.converterCampoStringParaValorBigDecimal("abc");
		assertEquals(retorno, BigDecimal.ZERO);
	}
	
	
	@Test
	public void testeConverterCampoValorDecimalParaStringNulo() {
		String retorno = BigDecimalUtil.converterCampoValorDecimalParaString(null);
		assertEquals(retorno, StringUtils.EMPTY);

	}
	
	@Test
	public void testeConverterCampoValorDecimalParaString() {
		String retorno = BigDecimalUtil.converterCampoValorDecimalParaString(BigDecimalUtil.CEM);
		assertEquals(retorno, "100,0000");
	}
	
	@Test
	public void testeMaiorQueZero() {
		boolean retorno = BigDecimalUtil.maiorQueZero(BigDecimalUtil.CEM);
		assertEquals(retorno, true);
	}
	
	@Test
	public void testeMaiorQueZeroFalse() {
		boolean retorno = BigDecimalUtil.maiorQueZero(BigDecimal.ZERO);
		assertEquals(retorno, false);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeMaiorQueZeroNulo() {
		BigDecimalUtil.maiorQueZero(null);
	}
	
	@Test
	public void testeNaoNulo() {
		assertEquals(BigDecimalUtil.naoNulo(BigDecimal.ONE), true);
	}
	
	@Test
	public void testeNaoNuloFalse() {
		assertEquals(BigDecimalUtil.naoNulo(null), false);
	}
	
	@Test
	public void testeNulo() {
		assertEquals(BigDecimalUtil.nulo(null), true);
	}
	
	@Test
	public void testeNuloFalse() {
		assertEquals(BigDecimalUtil.nulo(BigDecimal.ONE), false);
	}
	
	@Test
	public void testeMenorQueZero() {
		assertEquals(BigDecimalUtil.menorQueZero(new BigDecimal(-1)), true);
	}
	
	@Test
	public void testeMenorQueZeroFalse() {
		assertEquals(BigDecimalUtil.menorQueZero(BigDecimal.ONE), false);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeMenorQueZeroNulo() {
		assertEquals(BigDecimalUtil.menorQueZero(null), false);
	}
	
	@Test
	public void testeMenorQue() {
		assertEquals(BigDecimalUtil.menorQue(BigDecimal.ZERO, BigDecimal.ONE), true);
	}
	
	@Test
	public void testeMenorQueFalse() {
		assertEquals(BigDecimalUtil.menorQue(BigDecimal.ONE, BigDecimal.ZERO), false);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeMenorQueNulo() {
		assertEquals(BigDecimalUtil.menorQue(null, null), false);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeMenorQueNulo2() {
		assertEquals(BigDecimalUtil.menorQue(BigDecimal.ONE, null), false);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeMenorQueNulo3() {
		assertEquals(BigDecimalUtil.menorQue(null, BigDecimal.ONE), false);
	}
	
	@Test
	public void testeMaiorQue() {
		assertEquals(BigDecimalUtil.maiorQue(BigDecimal.ONE, BigDecimal.ZERO), true);
	}
	
	@Test
	public void testeMaiorQueFalse() {
		assertEquals(BigDecimalUtil.maiorQue(BigDecimal.ZERO, BigDecimal.ONE), false);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeMaiorQueNulo() {
		assertEquals(BigDecimalUtil.maiorQue(null, BigDecimal.ONE), false);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeMaiorQueNulo2() {
		assertEquals(BigDecimalUtil.maiorQue(BigDecimal.ONE, null), false);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeMaiorQueNulo3() {
		assertEquals(BigDecimalUtil.maiorQue(null, null), false);
	}
	
	@Test
	public void testeMaior() {
		assertEquals(BigDecimalUtil.maior(BigDecimal.ZERO, BigDecimal.ONE, BigDecimalUtil.CEM), BigDecimalUtil.CEM);
	}
	
	@Test
	public void testeMaiorZero() {
		assertEquals(BigDecimalUtil.maior(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO), BigDecimal.ZERO);
	}
	
	@Test
	public void testeConverterPercentualParaBigDecimal() throws FormatoInvalidoException {
		BigDecimal retorno = BigDecimalUtil.converterPercentualParaBigDecimal(BigDecimal.ONE);
		assertEquals(retorno, BigDecimalUtil.CEM);
	}
	
	@Test
	public void testeConverterPercentualParaBigDecimalNulo() throws FormatoInvalidoException {
		BigDecimal retorno = BigDecimalUtil.converterPercentualParaBigDecimal(null);
		assertEquals(retorno, null);
	}
	
	@Test
	public void testeConverterPercentualParaBigDecimalString() throws FormatoInvalidoException {
		String retorno = BigDecimalUtil.converterPercentualParaBigDecimalString(BigDecimal.ONE);
		assertEquals(retorno, "100,0000");
	}
	
	@Test
	public void testeSoma() {
		assertEquals(BigDecimalUtil.soma(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE), new BigDecimal(3));
	}
	
	@Test
	public void testeSomaNulo() {
		assertEquals(BigDecimalUtil.soma(null, BigDecimal.ONE, BigDecimal.ONE), new BigDecimal(2));
	}
}
