package br.com.ggas.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StringUtilTest {

	@Test
	public void testPreencherEsquerda() {
		assertEquals("       abc", StringUtil.preencherEsquerda("abc", 10, null));
	}
	
	@Test
	public void testPreencherEsquerdaComPreenchimento() {
		assertEquals("XXXXXXXabc", StringUtil.preencherEsquerda("abc", 10, "X"));
	}
	
	@Test
	public void testPreencherEsquerdaComNumero() {
		assertEquals("0000000123", StringUtil.preencherEsquerda("123", 10, "0"));
	}
	
	@Test
	public void testPreencherDireita() {
		assertEquals("abc       ", StringUtil.preencherDireita("abc", 10, null));
	}
}
