package br.com.ggas.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TesteStringUtil {
	
	@Test
	public void preencherEsquerda() {
		String texto = "11";
		
		texto = StringUtil.preencherEsquerda(texto, 5, "0");
		assertEquals(texto, "00011");
	}

	@Test
	public void preencherEsquerdaSemPreenchimento() {
		String texto = "11";
		
		texto = StringUtil.preencherEsquerda(texto, 5, null);
		assertEquals(texto, "   11");
	}
	
	@Test
	public void preencherEsquerdaStringVazia() {
		String texto = "11";
		
		texto = StringUtil.preencherEsquerda(texto, 5, "");
		assertEquals(texto, "   11");
	}
	
	@Test
	public void preencherDireita() {
		String texto = "11";
		
		texto = StringUtil.preencherDireita(texto, 5, "0");
		assertEquals(texto, "11000");
	}
	
	@Test
	public void preencherDireitaStringVazia() {
		String texto = "11";
		
		texto = StringUtil.preencherDireita(texto, 5, "");
		assertEquals(texto, "11   ");
	}
	
	@Test
	public void preencherDireitaSemPreenchimento() {
		String texto = "11";
		
		texto = StringUtil.preencherDireita(texto, 5, null);
		assertEquals(texto, "11   ");
	}

}
