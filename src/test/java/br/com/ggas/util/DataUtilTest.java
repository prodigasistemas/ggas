/*
 * Copyright (C) <2018> GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * Este arquivo é parte do GGAS, um sistema de gestão comercial de Serviços de Distribuição de Gás
 *
 * Este programa é um software livre; você pode redistribuí-lo e/ou
 * modificá-lo sob os termos de Licença Pública Geral GNU, conforme
 * publicada pela Free Software Foundation; versão 2 da Licença.
 *
 * O GGAS é distribuído na expectativa de ser útil,
 * mas SEM QUALQUER GARANTIA; sem mesmo a garantia implícita de
 * COMERCIALIZAÇÃO ou de ADEQUAÇÃO A QUALQUER PROPÓSITO EM PARTICULAR.
 * Consulte a Licença Pública Geral GNU para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública Geral GNU
 * junto com este programa; se não, escreva para Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 * Copyright (C) 2011-2018 the GGAS – Sistema de Gestão Comercial (Billing) de Serviços de Distribuição de Gás
 *
 * This file is part of GGAS, a commercial management system for Gas Distribution Services
 *
 * GGAS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * GGAS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place – Suite 330, Boston, MA 02111-1307, USA
 */

package br.com.ggas.util;

import org.junit.Test;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;

/**
 * @author jose.victor@logiquesistemas.com.br
 */
public class DataUtilTest {

	@Test
	public void testToDate() throws Exception {

		LocalDateTime localDate = LocalDateTime.of(2017, 2, 1, 5, 5, 5);
		Date localDateEsperado = Date.from(localDate.atZone(ZoneId.systemDefault()).toInstant());

		final Date resultado = DataUtil.toDate(localDate);

		assertThat(resultado, equalTo(localDateEsperado));

	}

	@Test
	public void testToDatefromLocalDate() throws Exception {

		LocalDate localDate = LocalDate.of(2017, 2, 1);
		Date localDateEsperado = Date.from(localDate.atTime(0, 0).atZone(ZoneId.systemDefault()).toInstant());

		final Date resultado = DataUtil.toDate(localDate);

		assertThat(resultado, equalTo(localDateEsperado));

	}

	@Test
	public void testToLocalDateTime() {
		final LocalDateTime esperado = LocalDateTime.of(2017, 2, 1, 5, 5, 5);
		final Date dataTEste = Date.from(esperado.atZone(ZoneId.systemDefault()).toInstant());

		final LocalDateTime resultado = DataUtil.toLocalDateTime(dataTEste);

		assertThat(resultado, equalTo(esperado));
	}

	@Test
	public void testToLocalDateTimeNullSafe() {
		Assert.isNull(DataUtil.toLocalDateTime(null));
		LocalDateTime timeNull = null;
		LocalDate dateNull = null;
		Assert.isNull(DataUtil.toDate(timeNull));
		Assert.isNull(DataUtil.toDate(dateNull));
	}

	@Test
	public void testConverterDataParaString() {
		final LocalDateTime data = LocalDateTime.of(2018, 12, 3, 1, 2, 3);
		final String resultado = DataUtil.converterDataParaString(data);

		assertThat(resultado, equalTo("03/12/2018"));
	}
	
	@Test
	public void testGetHora() {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 10);
		
		assertEquals(new Integer(10), DataUtil.getHora(calendar.getTime()));
	}
	
	@Test
	public void testGetDia() {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 10);
		
		assertEquals(new Integer(10), DataUtil.getDia(calendar.getTime()));
	}
}
