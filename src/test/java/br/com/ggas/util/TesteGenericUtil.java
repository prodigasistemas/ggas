package br.com.ggas.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;

import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.integracao.supervisorio.diaria.impl.SupervisorioMedicaoDiariaImpl;

public class TesteGenericUtil {

	@Test
	public void calcularMediaLista() throws NegocioException {
		Collection<SupervisorioMedicaoDiariaImpl> lista = new ArrayList<SupervisorioMedicaoDiariaImpl>();
		
		SupervisorioMedicaoDiariaImpl obj = new SupervisorioMedicaoDiariaImpl();
		
		for (int i = 0; i < 3; i++) {
			obj.setTemperatura(new BigDecimal(10));
			lista.add(obj);
		}
		
		BigDecimal resultado = new GenericUtil<SupervisorioMedicaoDiariaImpl>().calcularMediaLista(lista, "getTemperatura", 0);
		
		Assert.assertEquals(new BigDecimal(10), resultado);
	}
	
	
	@Test
	public void calcularMediaListaValoresInteiros() throws NegocioException {
		Collection<Integer> lista = new ArrayList<Integer>();
		
		lista.add(new Integer(10));
		lista.add(new Integer(10));
		lista.add(new Integer(10));
		
		BigDecimal resultado = new GenericUtil<Integer>().calcularMediaLista(lista, "toString", 0);
		
		Assert.assertEquals(new BigDecimal(10), resultado);
	}
	
	@Test
	public void calcularMediaListaBigDecimal() throws NegocioException {
		Collection<BigDecimal> lista = new ArrayList<BigDecimal>();
		
		lista.add(new BigDecimal(10));
		lista.add(new BigDecimal(10));
		lista.add(new BigDecimal(10));
		
		BigDecimal resultado = new GenericUtil<BigDecimal>().calcularMediaLista(lista, "abs", 0);
		
		Assert.assertEquals(new BigDecimal(10), resultado);
	}
	
	@Test
	public void calcularMediaListaBigDecimalSemEscala() throws NegocioException {
		Collection<BigDecimal> lista = new ArrayList<BigDecimal>();
		
		lista.add(new BigDecimal(10));
		lista.add(new BigDecimal(10));
		lista.add(new BigDecimal(10));
		
		BigDecimal resultado = new GenericUtil<BigDecimal>().calcularMediaLista(lista, "abs");
		
		Assert.assertEquals(new BigDecimal(10), resultado);
	}
	
	@Test(expected=NegocioException.class)
	public void calcularMediaListaExcecao() throws NegocioException {
		Collection<BigDecimal> lista = new ArrayList<BigDecimal>();
		
		lista.add(new BigDecimal(10));
		
		new GenericUtil<BigDecimal>().calcularMediaLista(lista, null, 0);
	}
}
