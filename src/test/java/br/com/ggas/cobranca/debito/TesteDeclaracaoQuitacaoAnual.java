package br.com.ggas.cobranca.debito;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cadastro.cliente.ControladorCliente;
import br.com.ggas.cadastro.empresa.ControladorEmpresa;
import br.com.ggas.cadastro.empresa.Empresa;
import br.com.ggas.cadastro.imovel.PontoConsumo;
import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.contrato.contrato.ContratoPontoConsumo;
import br.com.ggas.contrato.contrato.ControladorContrato;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.ControladorEntidadeConteudo;
import br.com.ggas.geral.EntidadeConteudo;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.util.Constantes;
import net.sf.jasperreports.engine.JRRuntimeException;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TesteDeclaracaoQuitacaoAnual extends GGASTestCase {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIA_TESTE_DECLARACAO_QUITACAO_ANUAL.sql";

	@Autowired
	@Qualifier("controladorCobranca")
	ControladorCobranca controladorCobranca;

	@Autowired
	@Qualifier(ControladorCliente.BEAN_ID_CONTROLADOR_CLIENTE)
	ControladorCliente controladorCliente;
	
	@Autowired
	@Qualifier("controladorEmpresa")
	ControladorEmpresa controladorEmpresa;
	
	@Autowired
	@Qualifier(ControladorContrato.BEAN_ID_CONTROLADOR_CONTRATO)
	ControladorContrato controladorContrato;

	@Autowired
	@Qualifier(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA)
	ControladorFatura controladorFatura;

	@Autowired
	@Qualifier(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA)
	ControladorParametroSistema controladorParametroSistema;

	@Autowired
	@Qualifier("controladorEntidadeConteudo")
	ControladorEntidadeConteudo controladorEntidadeConteudo;

	@Autowired
	@Qualifier(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA)
	ControladorConstanteSistema controladorConstanteSistema;

	final String anoReferencia = "2017";
	final String numeroContrato = "201503911";
	
	
	@Transactional
	@Test
	public void testeDeclaracaoQuitacaoAnual() throws GGASException, IOException {

		// Roda dependências do teste		
		setupSql();
		setupSQL(ARQUIVO_PREPARAR_CENARIO); 
		
		atualizarLogoEmpresa();
		
		Cliente c = consultarCliente();
		PontoConsumo pc = consultarPontoConsumo(c);

		byte[] relatorio = controladorCobranca.gerarRelatorioDeclaracaoQuitacaoAnual(anoReferencia, c.getChavePrimaria(),
				pc.getChavePrimaria(), numeroContrato);

		Assert.assertNotNull(relatorio);

	}


	@Ignore
	@Transactional
	@Test
	public void testeDeclaracaoQuitacaoAnualFaturasPendentes() throws GGASException, IOException {
		
		// Roda dependências do teste
		setupSql(); 
		setupSQL(ARQUIVO_PREPARAR_CENARIO); 

		atualizarLogoEmpresa();
		
		Cliente c = consultarCliente();
		PontoConsumo pc = consultarPontoConsumo(c);
		
		Collection<Fatura> faturas = controladorFatura.consultarFaturas(c.getChavePrimaria(), pc.getChavePrimaria(),
				new HashMap<String, Object[]>(), null, null, Boolean.FALSE);

		EntidadeConteudo ec =
				controladorEntidadeConteudo.obterEntidadeConteudoPorConstanteSistema(Constantes.C_SITUACAO_RECEBIMENTO_PENDENTE);

		for (Fatura fatura : faturas) {
			fatura.setSituacaoPagamento(ec);
			salvar(fatura);
		}
		
		try {
			controladorCobranca.gerarRelatorioDeclaracaoQuitacaoAnual(anoReferencia, c.getChavePrimaria(), pc.getChavePrimaria(),
					numeroContrato);

		} catch (JRRuntimeException e) {
			// O Relatório de extrato de débitos não pode ser gerado sem o tomcat estar online
			// Por isso a exceção abaixo é esperada
			// O relatório relatorioExtratoDebito.jrxml deve ser corrigdo para que a imagem sejá passada por parâmetro
			Assert.assertTrue(e.getMessage().contains("Error opening input stream from URL"));
		}
	}


	private void atualizarLogoEmpresa() throws NegocioException, IOException {
		Empresa empresa = controladorEmpresa.obterEmpresaPrincipal();
		
		String imagePath = "src/main/webapp/imagens/ggas_marca_login.jpg";
		String path = getClass().getClassLoader().getResource("").getFile();
		path = path.replace("bin/", imagePath);
		path = path.replace("build/classes/test/", imagePath);
		System.out.println(path);
		File fileItem = new File( path );
		
		FileInputStream input = new FileInputStream(fileItem);
		MultipartFile multipartFile = new MockMultipartFile("fileItem",
		            fileItem.getName(), "image/jpg", IOUtils.toByteArray(input));
		
		empresa.setLogoEmpresa(multipartFile.getBytes());		
		salvar(empresa);
	}


	@Transactional
	@Test
	public void testeDeclaracaoQuitacaoAnualFaturasAuxentes() throws GGASException, IOException {

		
		setupSql(); 
		Cliente c = consultarCliente();
		PontoConsumo pc = consultarPontoConsumo(c);

		try {
			controladorCobranca.gerarRelatorioDeclaracaoQuitacaoAnual(anoReferencia, c.getChavePrimaria(), pc.getChavePrimaria(),
					numeroContrato);

		} catch (NegocioException e) {
			Assert.assertEquals(ControladorCobranca.ERRO_NEGOCIO_FATURAS_NAO_ENCONTRADAS_ANO_SELECIONADO, e.getChaveErro());
		}
	}
	

	private Cliente consultarCliente() throws NegocioException {
		Cliente c = null;
		Map<String, Object> filtro = new HashMap<>();
		filtro.put("nome", "SEVERINO DA SILVA");
		Collection<Cliente> clientes = controladorCliente.consultarClientes(filtro);
		
		for (Cliente cliente : clientes) {
			if(cliente.getNome().startsWith("SEVERINO DA SILVA")){
				c = cliente;
			}
		}
		return c;
	}
	

	private PontoConsumo consultarPontoConsumo(Cliente c) throws NegocioException {
		PontoConsumo pc = null;
		Collection<ContratoPontoConsumo> listaCpc = controladorContrato.consultarContratoPontoConsumoPorCliente(c.getChavePrimaria());
		
		for (ContratoPontoConsumo cpc : listaCpc) {
			pc = cpc.getPontoConsumo();
		}
		return pc;
	}

}
