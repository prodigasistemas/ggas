package br.com.ggas.cobranca;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.arrecadacao.ArrecadadorContratoConvenio;
import br.com.ggas.cadastro.cliente.Cliente;
import br.com.ggas.cobranca.impl.ControladorCobrancaImpl;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.FaturaGeral;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;

public class TesteControladorCobranca extends GGASTestCase {
	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_FATURA.sql";
	
	@Autowired
	ControladorCobrancaImpl controladorCobranca;
	
	@Autowired
	ControladorParametroSistema controladorParametroSistema;
	
	@Autowired
	ControladorFatura controladorFatura;
	
	
	ParametroSistema parametroExibirMensagemContaEOuNotificacao;
	

	@Before
	public void setup() throws Exception {
		parametroExibirMensagemContaEOuNotificacao = 
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO);
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
	}
	
	@Test
	@Transactional
	public void testVerificarSeDeveExibirMensagemNoArquivoDeNotificacaoDeCorteComParametroApenasConta() throws Throwable {
		boolean resultado = 
				(boolean) chamarMetodoPrivado(controladorCobranca, 
				"verificarSeDeveExibirMensagemNoArquivoDeNotificacaoDeCorte", parametroExibirMensagemContaEOuNotificacao);
		assertFalse(resultado);
	}
	
	
	@Test
	@Transactional
	public void testVerificarSeDeveExibirMensagemNoArquivoDeNotificacaoDeCorteComParametroApenasArquivo() throws Throwable {
		alterarValorParametroExibirMensagem("2");
		boolean resultado = 
				(boolean) chamarMetodoPrivado(controladorCobranca, 
				"verificarSeDeveExibirMensagemNoArquivoDeNotificacaoDeCorte", parametroExibirMensagemContaEOuNotificacao);
		assertTrue(resultado);
	}
	
	@Test
	@Transactional
	public void testVerificarSeDeveExibirMensagemNoArquivoDeNotificacaoDeCorteComParametroAmbos() throws Throwable {		
		alterarValorParametroExibirMensagem("3");
		boolean resultado = 
				(boolean) chamarMetodoPrivado(controladorCobranca, 
				"verificarSeDeveExibirMensagemNoArquivoDeNotificacaoDeCorte", parametroExibirMensagemContaEOuNotificacao);
		assertTrue(resultado);
	}
	
	@Test
	@Transactional
	public void testVerificarSeDeveExibirMensagemNoArquivoDeNotificacaoDeCorteComParametroInvalido() throws Throwable {
		alterarValorParametroExibirMensagem("4");
		boolean resultado = (boolean) chamarMetodoPrivado(controladorCobranca,
				"verificarSeDeveExibirMensagemNoArquivoDeNotificacaoDeCorte",
				parametroExibirMensagemContaEOuNotificacao);
		assertFalse(resultado);
	}
	
	@Test
	@Transactional
	public void testNotificarCorte() throws IOException, GGASException {
		alterarValorParametroExibirMensagem("3");
		StringBuilder logProcessamento = new StringBuilder();
		Map<Cliente, byte[]> relatorio = controladorCobranca.processarEmissaoNotificacaoCorteBatch(logProcessamento, null);
		assertNotNull(relatorio);
	}
	
	@Test
	@Transactional
	public void testAvisoCorte() throws IOException, GGASException {
		StringBuilder logProcessamento = new StringBuilder();
		Map<Cliente, byte[]> relatorio = controladorCobranca.processarEmissaoAvisoCorteBatch(logProcessamento, null);
		assertNotNull(relatorio);
	}
	
	@Test
	@Transactional
	public void testClientesComFaturasNaoPagas() throws NegocioException {
		Collection<Fatura> listaFatura = controladorCobranca.consultarFaturasNaoPagasCliente(999999l, "2200");
		assertNotNull(listaFatura);
	}
	
	@Test
	@Transactional
	public void testGerarNumeroCodigoBarras() throws NegocioException {
		DocumentoCobranca doc = fachada.obterDocumentoCobranca(999997l);
		Map<String, Object> retorno = controladorCobranca.gerarNumeroCodigoBarra(doc, BigDecimal.ONE);
		assertNotNull(retorno);
	}
		
	private void alterarValorParametroExibirMensagem(String novoValor) throws NegocioException, ConcorrenciaException {
		ParametroSistema parametroExibirMensagemContaEOuNotificacao = 
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO);
		parametroExibirMensagemContaEOuNotificacao.setValor(novoValor);
		controladorParametroSistema.atualizar(parametroExibirMensagemContaEOuNotificacao);
		
	}

}
