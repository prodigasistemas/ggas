package br.com.ggas.cobranca.dominio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

import org.junit.Test;

import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.FaturaMotivoRevisao;
import br.com.ggas.faturamento.impl.FaturaImpl;
import br.com.ggas.faturamento.impl.FaturaMotivoRevisaoImpl;

public class TesteDadosCicloCorte {

	@Test
	public void testeFatura() {
		Collection<Fatura> faturas = montarListaFaturas();
		assertEquals(2, faturas.size());
		
		Collection<Fatura> faturasInvalidas = DadosCicloCorte.obterFaturasInvalidasEmissaoNotificacaoCorte(faturas, true, null);
		assertNotNull(faturasInvalidas);
		assertEquals(1, faturasInvalidas.size());
	}

	public Collection<Fatura> montarListaFaturas() {
		Collection<Fatura> faturas = new ArrayList<Fatura>();
		faturas.add(new FaturaImpl());
		faturas.add(montarFaturaRevisao());
		
		return faturas;
	}
	
	public Fatura montarFaturaRevisao() {
		Calendar dataRevisao = Calendar.getInstance();
		
		dataRevisao.add(Calendar.DAY_OF_MONTH, 15);
		Fatura fatura = new FaturaImpl(); 
		fatura.setDataRevisao(dataRevisao.getTime());
		fatura.setMotivoRevisao(obterMotivoReviao());

		return fatura;
	}
	
	public FaturaMotivoRevisao obterMotivoReviao() {
		FaturaMotivoRevisao motivo = new FaturaMotivoRevisaoImpl();
		
		motivo.setPrazoMaximoDias(10);
		
		return motivo;
	}
	
}
