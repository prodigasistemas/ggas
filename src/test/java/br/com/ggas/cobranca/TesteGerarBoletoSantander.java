package br.com.ggas.cobranca;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;

import br.com.ggas.arrecadacao.ControladorArrecadacao;
import br.com.ggas.arrecadacao.ControladorBanco;
import br.com.ggas.arrecadacao.TipoDocumento;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.constantesistema.ControladorConstanteSistema;
import br.com.ggas.faturamento.Fatura;
import br.com.ggas.faturamento.fatura.ControladorFatura;
import br.com.ggas.faturamento.fatura.impl.ControladorFaturaImpl;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;
import br.com.ggas.util.RelatorioUtil;

public class TesteGerarBoletoSantander extends GGASTestCase {

	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_GERAR_BOLETO_SANTANDER.sql";
	private static final String PONTO_CONSUMO = "pontoConsumo";
	private static final String DIRETORIO_ARQUIVOS_FATURA = "DIRETORIO_ARQUIVOS_FATURA";
	private static final String DIRETORIO = System.getProperty("user.home") + File.separator + "TesteGerarBoletoSantander";

	@Autowired
	@Qualifier(ControladorBanco.BEAN_ID_CONTROLADOR_BANCO)
	ControladorBanco controladorBanco;

	@Autowired
	@Qualifier(ControladorFatura.BEAN_ID_CONTROLADOR_FATURA)
	ControladorFatura controladorFatura;

	@Autowired
	@Qualifier(ControladorConstanteSistema.BEAN_ID_CONTROLADOR_CONSTANTE_SISTEMA)
	ControladorConstanteSistema controladorConstantesSistema;

	@Autowired
	@Qualifier(ControladorArrecadacao.BEAN_ID_CONTROLADOR_ARRECADACAO)
	ControladorArrecadacao controladorArrecadacao;

	@Transactional
	@Before
	public void setup() throws IOException, GGASException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
		alterarParametroDiretorio();
	}
	
	@After
	public void limpar() {
		FileSystemUtils.deleteRecursively(new File(DIRETORIO));
	}

	@Transactional
	@Test
	public void testImprimirSegundaVia() throws Throwable {
		Fatura fatura = fachada.obterFatura(99999l, "listaFaturaItem", "faturaGeral");
		byte[] arquivo = fachada.gerarImpressaoFaturaSegundaVia(fatura, null);
		assertNotNull(arquivo);
	}

	@SuppressWarnings("unchecked")
	@Transactional
	@Test
	public void testGerarBoleto() throws Throwable {
		ControladorFaturaImpl controladorFat = new ControladorFaturaImpl();
		byte[] arquivo = null;
		Fatura fatura = fachada.obterFatura(99999l, "listaFaturaItem", "faturaGeral", PONTO_CONSUMO);
		String constanteTipoDocumento = Constantes.C_TIPO_DOCUMENTO_FATURA;
		String dataVencimento = fatura.getDataVencimentoFormatada();

		TipoDocumento tipoDocumento = null;

		if (constanteTipoDocumento != null && !constanteTipoDocumento.isEmpty()) {
			String parametroTpDoc = controladorConstantesSistema
					.obterValorConstanteSistemaPorCodigo(constanteTipoDocumento);
			tipoDocumento = controladorArrecadacao.obterTipoDocumento(Long.valueOf(parametroTpDoc));
		}

		Map<String, Object> parametros = (Map<String, Object>) chamarMetodoPrivado(controladorFat,
				"popularMapaParametrosBoletoBancarioPadrao", fatura, Boolean.TRUE, BigDecimal.ZERO, tipoDocumento,
				dataVencimento, true);
		
		parametros.put("imagemLogoBanco",
				"https://upload.wikimedia.org/wikipedia/commons/d/dc/Logotipo_del_Banco_Santander.jpg");
		
		arquivo = RelatorioUtil.gerarRelatorioPDF(null, parametros, "relatorioBoletoBancarioPadrao.jasper");
		assertNotNull(arquivo);
	}

	private void alterarParametroDiretorio() throws GGASException {
		ParametroSistema parametro = fachada.obterParametroPorCodigo(DIRETORIO_ARQUIVOS_FATURA);
		parametro.setValor(DIRETORIO);
		super.salvar(parametro);
	}
}
