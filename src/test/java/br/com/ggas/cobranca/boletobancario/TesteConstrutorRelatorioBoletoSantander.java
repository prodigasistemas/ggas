package br.com.ggas.cobranca.boletobancario;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import br.com.ggas.cobranca.ControladorCobranca;
import br.com.ggas.cobranca.boletobancario.impl.ConstrutorRelatorioBoletoSantander;
import br.com.ggas.comum.GGASTestCase;
import br.com.ggas.faturamento.DocumentoCobranca;
import br.com.ggas.geral.exception.ConcorrenciaException;
import br.com.ggas.geral.exception.GGASException;
import br.com.ggas.geral.exception.NegocioException;
import br.com.ggas.parametrosistema.ControladorParametroSistema;
import br.com.ggas.parametrosistema.ParametroSistema;
import br.com.ggas.util.Constantes;

public class TesteConstrutorRelatorioBoletoSantander extends GGASTestCase {
	
	private static final String ARQUIVO_PREPARAR_CENARIO = "SETUP_DEPENDENCIAS_TESTE_CONSTRUTOR_RELATORIO_BOLETO_SANTANDER.sql";
	private static String SQL_BUSCAR_DOCUMENTO_COBRANCA = 
			"SELECT DOCO_CD FROM DOCUMENTO_COBRANCA WHERE CLIE_CD IN (SELECT CLIE_CD FROM CLIENTE WHERE CLIE_NM = "
			+ "'SEVERINO DA SILVA' AND CLIE_NM_ABREVIADO = 'SEVSIL')";

	
	ConstrutorRelatorioBoletoSantander construtorRelatorioBoletoSantander;
	
	@Autowired
	@Qualifier(ControladorCobranca.BEAN_ID_CONTROLADOR_COBRANCA)
	ControladorCobranca controladorCobranca;
	
	@Autowired
	@Qualifier(ControladorParametroSistema.BEAN_ID_CONTROLADOR_PARAMETRO_SISTEMA)
	ControladorParametroSistema controladorParametroSistema;
	
	String VALOR_PADRAO = "1";
	String VALOR_INVALIDO = "4";
	String APENAS_NOTIFICACAO = "2";
	
	String mensagemParametro;
	
	@Transactional
	@Before
	public void setup() throws IOException, GGASException {
		setupSQL(ARQUIVO_PREPARAR_CENARIO);
		DocumentoCobranca documentoCobranca = controladorCobranca.obterDocumentoCobranca(obterIdDocumentoCobrancaDeTeste().longValue());
		construtorRelatorioBoletoSantander = new ConstrutorRelatorioBoletoSantander(documentoCobranca);
		mensagemParametro = controladorParametroSistema.obterParametroPorCodigo(Constantes.MENSAGEM_NOTIFICACAO_CORTE).getValor();
	}
	
	@After
	public void arrumarValores() throws Exception {
		alterarValorParametroExibirMensagem(VALOR_PADRAO);
	}
	
	@Transactional
	@Test
	public void testObterMensagemBoletoComParametroInvalido() throws Exception {
		alterarValorParametroExibirMensagem(VALOR_INVALIDO);
		StringBuilder mensagemBoleto = construtorRelatorioBoletoSantander.obterMensagemBoleto();
		assertFalse(mensagemBoleto.toString().contains(mensagemParametro));
	}
	
	@Transactional
	@Test
	public void testObterMensagemBoletoComMensagemApenasConta() throws Exception {
		StringBuilder mensagemBoleto = construtorRelatorioBoletoSantander.obterMensagemBoleto();
		assertTrue(mensagemBoleto.toString().contains(mensagemParametro));
	}
	
	@Transactional
	@Test
	public void testObterMensagemBoletoComMensagemApenasNotificacao() throws Exception {
		alterarValorParametroExibirMensagem(APENAS_NOTIFICACAO);
		StringBuilder mensagemBoleto = construtorRelatorioBoletoSantander.obterMensagemBoleto();
		assertFalse(mensagemBoleto.toString().contains(mensagemParametro));
	}
	
	private BigDecimal obterIdDocumentoCobrancaDeTeste() {
		return (BigDecimal) getSesstion().createSQLQuery(SQL_BUSCAR_DOCUMENTO_COBRANCA)
				.uniqueResult();
	}
	
	private void alterarValorParametroExibirMensagem(String novoValor) throws NegocioException, ConcorrenciaException {
		ParametroSistema parametroExibirMensagemContaEOuNotificacao = 
				controladorParametroSistema.obterParametroPorCodigo(Constantes.PARAMETRO_EXIBIR_MESANGEM_CONTA_E_OU_NOTIFICACAO);
		parametroExibirMensagemContaEOuNotificacao.setValor(novoValor);
		controladorParametroSistema.atualizar(parametroExibirMensagemContaEOuNotificacao);
		
	}

}
